from setuptools import setup, find_packages

setup(
    name='docuflow',
    description='Invoice and accounting management system',
    author='Admire',
    author_email='admire@originate.co.za',
    url='http://docuflow.co.za',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    package_dir={'': 'src'},
    packages=find_packages('src'),
    include_package_data=True,
    zip_safe=False,
)
