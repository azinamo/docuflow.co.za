function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

	$(document).on('click', '[data-menu-modal]', function (event) {
	event.preventDefault();
	var ajaxModal =$(".modal-ajax");
	if (ajaxModal.length > 0) {
		ajaxModal.remove();
	}

	$.get($(this).data('modal'), function (data) {
		$(data).modal('show');

		console.log('Handle invoice payment here');

		$("#comment_submit").on('click', function (event) {
			event.preventDefault();
			var form = $($(this).parent()).parent()
			submitModalForm(form);
		});

		// $("#create_distribution_journal_submit").on('click', function (e) {
		// 	submitModalForm($("#create_distribution_journals"));
		// 	return false;
		// });
		var date = new Date();
		$(".history_datepicker").datepicker({
			changeMonth		: true,
			changeYear		: true,
			dateFormat 		: "yy-mm-dd",
			maxDate         : -1,
			yearRange       : "1900:"+(date.getFullYear())
		});

		var minDate = new Date()
		var dataMinDate = $("#id_start_date").data('min-date');
		if (dataMinDate) {
			date = new Date(dataMinDate);
			minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
			$(".future_datepicker").datepicker({
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "yy-mm-dd",
				minDate         : minDate,
				yearRange       : "-30:+100"
			});
		} else {
			$(".future_datepicker").datepicker({
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "yy-mm-dd",
				minDate         : +1,
				yearRange       : "-30:+100"
			});

		}

		$(".chosen-select").chosen();

	});

	$('.validate_number').on('blur', function(){
		var value = $(this).val();
		console.log(isNaN(value), value)
		if (isNaN(value)) {
			$(this).val('')
		}
	});
});

function openInvoiceDetail(el)
{
	if (!$(el).hasClass('actions')) {
        var url = $(el).data('detail-url');
        if (url) {
            document.location.href = url;
        }
    }

}

function openDetail(el) {
	if (!$(el).hasClass('actions')) {
        var url = $(el).data('detail-url');
        if (url) {
            document.location.href = url;
        }
    }
}

function addButtonSpinner(button)
{
	if (button.length > 0) {
		button.css('width', $(this).outerWidth());
		button.attr('data-original-html', $(this).html());
		button.html('<i class="fa fa-spinner fa-spin"></i>');
	}
}

function removeButtonSpinner(button)
{
	if (button.length > 0) {
		button.html( $(this).attr('data-original-html') );
	}
}

function showResponse()
{

}

function ajaxSubmitForm(el, form, params)
{
	// disable extra form submits
	form.addClass('submitted');
	el.css('width', el.outerWidth());
	el.attr('data-original-html', el.html());
	el.html('<i class="fa fa-spinner fa-spin"></i>');
	el.prop('disabled', true);

	// remove existing alert & invalid field info
	$('.alert-fixed').remove();
	$('.is-invalid').removeClass('is-invalid');
	$('.is-invalid-message').remove();
	form.ajaxSubmit({
		url: form.attr('action'),
		target: '#loader',   // target element(s) to be updated with server response
		data: params,
		// pre-submit callback
		error: function (data) {
			// handle errors
			var error = {'error': true, 'text': data.statusText};
			responseNotification(error);
			el.prop('disabled', false).html(el.data('data-original-html'));
			form.LoadingOverlay("hide");
		},
		success: function(responseJson, statusText, xhr, $form) {
			responseNotification(responseJson);
			if (responseJson.hasOwnProperty('file')) {
				var invoicePdf = $("#invoice_pdf");
				if ($("#invoice_pdf").length > 0) {
					invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="'+ responseJson.file +'"></div>');
					PDFObject.embed(responseJson.file, "#invoice_pdf");
				}
				if ($("#invoice_image").length > 0) {
					$("#invoice_image").val(responseJson.file);
				}
			}
			el.css('width', el.outerWidth());
			el.html(el.data('original-html'));
			el.prop('disabled', false)
			form.LoadingOverlay("hide");
		},  // post-submit callback
		dataType: 'json',
		type: 'post'
		// $.ajax options can be used here too, for example:
		//timeout:   3000
	});
}

function submitForm(form, params)
{
	var loaderDiv = $("#loader");
	if (loaderDiv !== undefined) {
		loaderDiv.show();
	}
	// disable extra form submits
	form.addClass('submitted');
	var submitBtn = form.find("button[type='button']", "button[type='submit']");
	submitBtn.each(function () {
		$(this).css('width', $(this).outerWidth());
		$(this).attr('data-original-html', $(this).html());
		$(this).html('<i class="fa fa-spinner fa-spin"></i>');
	});

	// remove existing alert & invalid field info
	$('.alert-fixed').remove();
	$('.is-invalid').removeClass('is-invalid');
	$('.is-invalid-message').remove();
	form.ajaxSubmit({
		url: form.attr('action'),
		target: '#loader',   // target element(s) to be updated with server response
		data: params,
		beforeSerialize: function($form, options)
		{
			console.log( $form )
			console.log( options )
			// $.each(options['data'], function(index, value){
			// 	console.log( index, value )
			// });

		},
		beforeSubmit:  function(formData, jqForm, options) {
			var queryString = $.param(formData);
		},  // pre-submit callback
		error: function (data) {
			// handle errors
			responseNotification(data)
		},
		success:       function(responseJson, statusText, xhr, $form)  {
			// handle response
			// if ('url' in responseJson) {
			// 	ajaxNotificationDialog(responseJson.url);
			// }
			responseNotification(responseJson);
			if (responseJson.hasOwnProperty('file')) {
				var invoicePdf = $("#invoice_pdf");
				if ($("#invoice_pdf").length > 0) {
					invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="'+ responseJson.file +'"></div>');
					PDFObject.embed(responseJson.file, "#invoice_pdf");
				}
				if ($("#invoice_image").length > 0) {
					$("#invoice_image").val(responseJson.file);
				}
			}
			submitBtn.each(function () {
				$(this).css('width', $(this).outerWidth());
				$(this).html($(this).data('original-html'));
			});
			if (loaderDiv !== undefined) {
				loaderDiv.hide();
			}

		},  // post-submit callback
		dataType: 'json',
		type: 'post'
		// $.ajax options can be used here too, for example:
		//timeout:   3000
	});
}


function submitModalForm(form)
{
	// disable extra form submits
	form.addClass('submitted');
	form.find("button[type='submit']").each(function () {
		$(this).css('width', $(this).outerWidth());
		$(this).attr('data-original-html', $(this).html());
		$(this).html('<i class="fa fa-spinner fa-spin"></i>');
	});

	// remove existing alert & invalid field info
	$('.alert-fixed').remove();
	$('.is-invalid').removeClass('is-invalid');
	$('.is-invalid-message').remove();
	form.ajaxSubmit({
		url: form.attr('action'),
		target:        '#loader',   // target element(s) to be updated with server response
		beforeSubmit:  function(formData, jqForm, options) {
			var queryString = $.param(formData);
		},  // pre-submit callback
		error: function (data) {
			// handle errors
			var error = {'error': true, 'text': data.statusText};
			responseNotification(error);
			form.LoadingOverlay("hide");
		},
		success:       function(responseJson, statusText, xhr, $form)  {
			// handle response
			if (responseJson.error) {
			   $("#make_payment").prop('disabled', false);
			}
			form.LoadingOverlay("hide");
			responseNotification(responseJson);

			if (responseJson.hasOwnProperty('file')) {
				var invoicePdf = $("#invoice_pdf");
				if ($("#invoice_pdf").length > 0) {
					invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="'+ responseJson.file +'"></div>');
					PDFObject.embed(responseJson.file, "#invoice_pdf");
				}
				if ($("#invoice_image").length > 0) {
					$("#invoice_image").val(responseJson.file);
				}
			}
		},  // post-submit callback
		dataType: 'json',
		type: 'post'
		// $.ajax options can be used here too, for example:
		//timeout:   3000
	});
}

function flash(hasError, alertMessage)
{
	var html = '<div class="alert alert-' + (hasError === false ? 'success' : 'danger') + ' alert-fixed mt-3 mb-0">' + alertMessage + '</div>';
	if (hasError === false) {
		$(html).prependTo('.modal-body').delay(1000).queue(function () {
			$(this).remove()
			//document.location.reload();
		});
	} else {
		$(html).prependTo('.modal-body').delay(1000);
	}
}

function alertErrors(message)
{
	var notificationModal =$("#notification-modal");
	if (notificationModal.length > 0) {
		notificationModal.remove();
	}

	var html = '<div class="modal modal-notification show" tabindex="-1" data-backdrop="static" id="notification-modal">';
	html += '	<div class="modal-dialog modal-lg">';
	html += '		<div class="modal-content">';
	html += '			<div class="modal-header">';
	html += '				<button type="button" class="close notification-close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	html += '					<h4 class="modal-title" id="model-title">Warning</h4>';
	html += '			</div>';
	html += '			<div class="modal-body" id="modalbody">';
	html +=	'				<div class="text-danger">' + message + '</div>';
	html +=	'  			</div>';
	html += '			<div class="modal-footer" id="modalfooter">';
	html += '				<a type="button" class="btn btn-primary notification-close" data-dismiss="modal">Ok</a>';
	html += '			</div>';
	html += '		</div>';
	html += '	</div>';
	html += '</div>';

	$("body").append(html);

	$(".notification-close").on('click', function(e){
		$("#notification-modal").remove();
		e.preventDefault();
	});
}

function highlightFormErrors(errors)
{
	var scrollTo, counter = 0;
	$.each(errors, function (key, value) {
		var element;
		var keyElement = $("#id_" + key);
		var idElement = $("#" + key);
		var chosenElement;
		if (keyElement.length) {
			element = keyElement;
			chosenElement = $("#id_" + key + "_chosen");
		} else if(idElement.length) {
			element = idElement;
			chosenElement = $("#" + key + "_chosen");
		}
		if (chosenElement !== undefined && chosenElement.length > 0) {
			element = chosenElement;
			displayFormError(element, value[0]);
		} else if (element !== undefined && element.length > 0) {
			displayFormError(element, value[0]);
		}
		counter++;
	});
	var errorDiv = $(".error");
	if (errorDiv.length > 0) {
		$('html, body').animate({
			scrollTop: (errorDiv.first().offset().top)
		}, 500);
	}
}

function displayFormError(element, error_message)
{
	if (element.length > 0) {
		if (!element.hasClass("is-invalid")) {
			element.addClass('is-invalid');
			element.after('<div class="is-invalid-message has-error">' + error_message + '</div>');
			// TODO if the form is a horizontal form, look for the second parent div and highlight that
			// TODO otherwise highligh the first parent div
			var secondParent = $(element.parent()).parent();
			if (secondParent.length) {
				if (secondParent[0].nodeName === 'DIV') {
					secondParent.addClass('error').addClass('has-error').addClass('text-danger');
				} else {
					$(element.parent()).addClass('error').addClass('has-error').addClass('text-danger');
				}
			}
		}
	}
}

function removeFormError(element)
{
	if (element.length > 0) {
		if (element.hasClass("is-invalid")) {
			element.removeClass('is-invalid');
			// TODO if the form is a horizontal form, look for the second parent div and highlight that
			// TODO otherwise highligh the first parent div
			var secondParent = $(element.parent()).parent();
			if (secondParent.length) {
				console.log(secondParent)
				if (secondParent[0].nodeName === 'DIV') {
					console.log(secondParent[0])
					secondParent.find('div.is-invalid-message').remove();
					secondParent.removeClass('error').removeClass('has-error').removeClass('text-danger');
				} else {
					$(element.parent()).find('div.is-invalid-message').remove();
					$(element.parent()).removeClass('error').removeClass('has-error').removeClass('text-danger');
				}
			}
		}
	}
}

function responseNotification(jsonResponse, form)
{
	if (jsonResponse.hasOwnProperty('alert')) {
		if (jsonResponse.hasOwnProperty('url')){
			ajaxNotificationDialog(jsonResponse.url)
		} else {
			alertErrors(jsonResponse.text)
		}

		if (jsonResponse.hasOwnProperty('errors')) {
			highlightFormErrors(jsonResponse.errors)
		}
		console.log("1 Is modal ");
		console.log( isAModal() );
	}else {
		console.log("2: Is modal ")
		console.log( isAModal() );
		if (jsonResponse.hasOwnProperty('error')) {
			var isModal = isAModal();

			if (isModal) {
				flash(jsonResponse.error, jsonResponse.text);
			} else {
				$.notify({
					title: (jsonResponse.error ? '' : ''),
					message: jsonResponse.text
				}, {
					type: (jsonResponse.error ? 'danger' : 'success')
				});
			}
		}
		if (jsonResponse.hasOwnProperty('warning')) {
			$.notify({
				title: '',
				message: jsonResponse.text
			}, {
				type: 'warning'
			});
		}

		if (jsonResponse.hasOwnProperty('errors')) {

			var errorMessageContainer = $("#error_messages");
			var html = [];
			html.push("<ul>");
			$.each(jsonResponse.errors, function(key, error){
			    displayFormError($("#id_" + key), error)
				html.push("<li>"+ key + ": " + error + "</li>");
			});
			html.push("</ul>");
			if (errorMessageContainer.length > 0) {
				errorMessageContainer.html(html.join(' '));
			} else {
				$(html).prependTo('.modal-body');
			}

		}
		if (jsonResponse.hasOwnProperty('url')) {
			return ajaxNotificationDialog(jsonResponse.url)
		}
		if (jsonResponse.hasOwnProperty('redirect')) {
			setTimeout(function(){
				document.location.href = jsonResponse.redirect
			}, 500)
		}
		if (jsonResponse.hasOwnProperty('reload') && jsonResponse.reload) {
			setTimeout(function(){
				document.location.reload()
			}, 1500)
		}
        //
		// flash success message

		// dismiss modal
		if (jsonResponse.hasOwnProperty('dismiss_modal')) {
			var modal = $(".modal-ajax");
			if (modal.length) {
				modal.modal('hide');
				modal.remove()
			}
		}

		// reload page
		if (jsonResponse.hasOwnProperty('reload_page')) {
			location.reload();
		}

		// reload datatables
		if (jsonResponse.hasOwnProperty('reload_datatables')) {
			$($.fn.dataTable.tables()).DataTable().ajax.reload();
		}

		// reload sources
		if (jsonResponse.hasOwnProperty('reload_sources')) {
			$('[data-source]').each(function () {
				$.get($(this).data('source'), function (data) {
					$(this).html(data);
				});
			});
		}
	}
}

function isAModal()
{
	var ajaxModal = $(".modal-ajax");
	console.log('Ajax modal --> ', ajaxModal.length)
	if (ajaxModal.length > 0) {
		return true;
		//var hasClassIn = $(".modal-ajax>.in");
		//console.log('has class in  --> ', hasClassIn.length)
		//if (hasClassIn.length > 0) {
		//	return true;
		//}
	}
	return false;
}

function ajaxNotificationDialog(url, params)
{
	var ajaxModal =$(".modal");
	if (ajaxModal.length > 0) {
		$(".modal-backdrop").remove()
		ajaxModal.remove();
	}

	$.get(url, params, function (data) {
		$(data).modal('show');
		var form = $(data).find("form");
		// var btns = form.find("button");
		// $(btns).each(function(){
		// 	var _this = $(this);
		// 	_this.on('click', function(){
		// 		var ajaxUrl = _this.data('ajax-url');
		// 		var cancelBtn = _this.data('data-dismiss');
		// 		if (ajaxUrl !== undefined && )
		// 	});
		// });
		// console.log( form )
		// console.log( submitBtn )
		console.log( "Handle the url " );
		console.log( "Handl e the response dialog" );
		var formEL = $(data).find("form");
		$(".invoice-amount").on("blur", function() {
			var invoiceAmount = parseFloat($("#total_invoice_amount").val());
			var allocatedTotal = 0;
			$(".invoice-amount").each(function(){
				var methodAmount = $(this).val();
				if (methodAmount !== undefined && methodAmount !== "") {
					allocatedTotal += parseFloat(methodAmount);
				}
			});
			var balance = invoiceAmount - allocatedTotal;
			if (balance === 0) {
				$("#save_receipts").prop("disabled", false);
			} else {
				$("#save_receipts").prop("disabled", true);
			}
			$("#balance").html(accounting.formatMoney(balance));
		});

		$(".modal-save").on("click", function(e){
			e.preventDefault();
			//alert("Saving form --> " + form + " " +  $(this) + " -> " +  $(formEL));
			console.log($(this), $(formEL));
			console.log(form);
			var formId = $(formEL).attr("id");
			console.log("Form id -=> ", formId);
			$("#" + formId).ajaxSubmit({
				url: formEL.attr("action"),
				target:        "#loader",   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					// handle errors
					responseNotification(data)
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					// handle response
					responseNotification(responseJson, statusText, xhr)
				},  // post-submit callback
				dataType: "json",
				type: "post",
				timeout:   3000
			});
			//form.submitModal($(this), $(formEL));
			return false;
		});


		$("#sign_invoice_with_variance_submit").on('click', function (event) {
			event.stopImmediatePropagation();
			Invoice.signInvoice($(this));
		});


		$(".discount-invoice-check").on('change', function(e){
			if ($(this).is(":checked")) {
				Payment.addDiscount($(this))
			} else {
				Payment.removeDiscount($(this))
			}
			Payment.allChecked($(".discount-invoice-check"), $("#discount_invoice"))
		});

		$("#discount_invoice").on('change', function(e){
			console.log("Discount all invoices");
			if ($(this).is(":checked")) {
				$(".discount-invoice-check").each(function(){
					var _this = $(this);
					if (!_this.is(":checked")) {
						Payment.addDiscount(_this)
						_this.prop('checked', true)
					}
				})
			} else {
				$(".discount-invoice-check").each(function(){
					var _this = $(this);
					Payment.removeDiscount(_this);
					_this.prop('checked', false)
				})
			}
		});

		$("#id_discount_disallowed").on("blur", function(e){
			Payment.calculateNetPayment();
		});


		$("#make_payment").on('click', function(e){
			e.preventDefault();

			var customElement = $("<div>", {
				"css"   : {
					"border"        : "4px dashed gold",
					"font-size"     : "40px",
					"text-align"    : "center",
					"padding"       : "10px"
				},
				"class" : "loaders",
				"text"  : "Custom!"
			});

			var payInvoices = $("#pay_invoices");
			$(this).prop('disabled', true);
			if (payInvoices.length > 0) {
				payInvoices.LoadingOverlay("show", {
					background  : "rgba(165, 190, 100, 0.5)"
				});
				submitModalForm(payInvoices);
			} else {
				$("#pay_supplier_invoice").LoadingOverlay("show", {
					background  : "rgba(165, 190, 100, 0.5)"
				});
				submitModalForm($("#pay_supplier_invoices"));
			}
		});

		$("#continue_payment").on('click', function(e){
			e.preventDefault();
			var unAllocatedReason = $("#payment_comment").val();
			if (unAllocatedReason === '') {
				alert("Please enter the reason")
				return false;
			} else {
				$("#id_comment").text(unAllocatedReason);
				submitForm($("#sundry_payment"));
			}
		});

		$("#save_invoice_asset").on('click', function(e){
			e.preventDefault();
			submitModalForm($("#create_invoice_asset"));
		});

		$(".datepicker").datepicker({
			dateFormat: 'yy-mm-dd'
		});
		$(".chosen-select").chosen();

	});
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function printHtml(elId){
	var tableToPrint = document.getElementById(elId);
	var htmlToPrint = '';
	htmlToPrint += tableToPrint.outerHTML;
	newWin = window.open("")
	newWin.document.write(htmlToPrint);
	// newWin.print();
	// newWin.close();
}

function ajaxGet(el, params) {
	var ajaxUrl = el.data('ajax-url');
	if (ajaxUrl) {
		var promise = $.getJSON(ajaxUrl, params, function(response){
			responseNotification(response)
			// if ('url' in response) {
			// 	ajaxNotificationDialog(response.url)
			// }
		});
		return promise;
	}
}

function initDatePicker()
{
        var minDate = new Date();

        var datePickerClass = $(".datepicker");
        if (datePickerClass.length > 0) {
            datePickerClass.datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
        var historyDatePickerClass = $(".history_datepicker");
        if (historyDatePickerClass.length > 0) {
            historyDatePickerClass.datepicker({
                changeMonth		: true,
                changeYear		: true,
                dateFormat 		: "yy-mm-dd",
                maxDate         : -1,
                yearRange       : "1900:"+(date.getFullYear())
            });
        }

        var inputMinDate = $("#id_start_date").data('min-date');
        var futureDatePickerClass = $(".future_datepicker");
        if (inputMinDate) {
            date = new Date(inputMinDate);
            minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
        } else {
            minDate = 1
        }
        if (futureDatePickerClass.length > 0) {
            futureDatePickerClass.datepicker({
                changeMonth		: true,
                changeYear		: true,
                dateFormat 		: "yy-mm-dd",
                minDate         : minDate,
                yearRange       : "-30:+100"
            });
        }

}

function initChosenSelect()
{
	var chosenEl = $(".chosen-select");
	if (chosenEl !== undefined && chosenEl.length > 0) {
	   chosenEl.chosen();
	}
}

function calculateSelectWidth(el)
{
	var chosenWidth = 250;
	if (el === undefined) {
		chosenWidth = 250;
	} else {
		chosenWidth = el.width();
	}
	if (chosenWidth === null || chosenWidth === 0) {
		chosenWidth = 250;
	}
	return chosenWidth
};

function calculateChosenSelectWidth(containerClass)
{
	var chosenWidth = 250;
	containerClass = "." + containerClass || '';
	console.log("Container class --> ", containerClass )
	var chosenContainer = $("" + containerClass + " > .chosen-container");
	console.log("Chosen container --> ", )
	console.log( chosenContainer);
	if (chosenContainer.length > 0 ) {
		chosenWidth = chosenContainer.width();
	}
	if (chosenWidth > 0) {
		$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
		$(".line-type").chosen({'width': '40px'});
	} else {
		$(".chosen-select").not('.line-type').chosen({'width': '250px'});
		$(".line-type").chosen({'width': '40px'});
	}
	return chosenWidth;
};

function showContent(el)
{
	var contentId = el.data('content-id');
	$("#content_" + contentId).hide();
	$("#full_content_" + contentId).show();
	el.hide();
	$("#hide_" + contentId).show('slow');
};

function hideContent(el)
{
	var contentId = el.data('content-id');
	$("#content_" + contentId).show('fast');
	$("#full_content_" + contentId).hide('slow');
	el.hide();
	$("#show_" + contentId).show('slow');
};

$(document).ready(function() {

	$(".table-items-list>tbody>tr>td:not(.actions)").on("click", function(){
		var detailUrl = $(this).parent("tr").data("detail-url");
		if (detailUrl !== undefined && detailUrl !== '') {
			document.location.href = detailUrl;
		}
		return false;
	});

	//Form Submit for IE Browser
	$('button[type=\'submit\']').on('click', function() {
		$("form[id*='form-']").submit();
	});

	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});
	var date = new Date();
	// datepicker on all date input with class datepicker
	$(".datepicker").datepicker({
		dateFormat: 'yy-mm-dd'
	});

    $(".history_datepicker").datepicker({
        changeMonth		: true,
        changeYear		: true,
        dateFormat 		: "yy-mm-dd",
        maxDate         : -1,
        yearRange       : "1900:"+(date.getFullYear())
    });

    $(".future_datepicker").datepicker({
        changeMonth		: true,
        changeYear		: true,
        dateFormat 		: "yy-mm-dd",
        minDate         : +1,
        yearRange       : "-30:+100"
    });
    $(".today-future").datepicker({
        changeMonth		: true,
        changeYear		: true,
        dateFormat 		: "yy-mm-dd",
        minDate         : +0,
        yearRange       : "0:+100"
    });

    $(".future").datepicker({
        changeMonth		: true,
        changeYear		: true,
        dateFormat 		: "yy-mm-dd",
        minDate         : +1,
        yearRange       : "0:+100"
    });

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body', html: true});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});


	$.event.special.remove = {
		remove: function(o) {
			if (o.handler) {
				o.handler.apply(this, arguments);
			}
		}
	}
	


	// Set last page opened on the menu
	$('#menu a[href]').on('click', function() {
		sessionStorage.setItem('menu', $(this).attr('href'));
	});

	if (!sessionStorage.getItem('menu')) {
		$('#menu #dashboard').addClass('active');
	} else {
		// Sets active and open to selected page in the left column menu.
		$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parent().addClass('active');
	}
	
	$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li > a').removeClass('collapsed');
	
	$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('ul').addClass('in');
	
	$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').addClass('active');


	// Settings object that controls default parameters for library methods:
	accounting.settings = {
		currency: {
			symbol : "",   // default currency symbol is '$'
			format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
			decimal : ".",  // decimal point separator
			thousand: ",",  // thousands separator
			precision : 2   // decimal places
		},
		number: {
			precision : 0,  // default precision on numbers is 0
			thousand: ",",
			decimal : "."
		}
	};

	// Image Manager
	$(document).on('click', 'a[data-toggle=\'image\']', function(e) {
		var $element = $(this);
		var $popover = $element.data('bs.popover'); // element has bs popover?

		e.preventDefault();

		// destroy all image popovers
		$('a[data-toggle="image"]').popover('destroy');

		// remove flickering (do not re-add popover when clicking for removal)
		if ($popover) {
			return;
		}

		$element.popover({
			html: true,
			placement: 'right',
			trigger: 'manual',
			content: function() {
				return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
			}
		});

		$element.popover('show');

		$('#button-image').on('click', function() {
			var $button = $(this);
			var $icon   = $button.find('> i');

			$('#modal-image').remove();

			$.ajax({
				url: 'index.php?route=common/filemanager&user_token=' + getURLVar('user_token') + '&target=' + $element.parent().find('input').attr('id') + '&thumb=' + $element.attr('id'),
				dataType: 'html',
				beforeSend: function() {
					$button.prop('disabled', true);
					if ($icon.length) {
						$icon.attr('class', 'fa fa-circle-o-notch fa-spin');
					}
				},
				complete: function() {
					$button.prop('disabled', false);

					if ($icon.length) {
						$icon.attr('class', 'fa fa-pencil');
					}
				},
				success: function(html) {
					$('body').append('<div id="modal-image" class="modal">' + html + '</div>');

					$('#modal-image').modal('show');
				}
			});

			$element.popover('destroy');
		});

		$('#button-clear').on('click', function() {
			$element.find('img').attr('src', $element.find('img').attr('data-placeholder'));

			$element.parent().find('input').val('');

			$element.popover('destroy');
		});
	});


	$(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});

	var syncEmails = $("#sync_emails");
	syncEmails.on('click', function(e){
	   e.preventDefault();
	   syncEmails.html("Processing ...");
	   $.get('/sources/emails/fetcher/', function(response){
		   $.notify({ title: response.text, message: response.text},{ type: (response.error ? 'error' : 'success') });
		   syncEmails.html("Get Emails");
		   setTimeout(function(){
			   if (response.error === false) {
				   if ('redirect' in response){
					   document.location.href = response['redirect']
				   } else {
					   document.location.href = '/documents/';
				   }
			   }
		   }, 100)
	   })
	});



	$("#sidebarCollapse").click(function(e) {
		e.preventDefault();
		$("#content").toggleClass("active");
		$("#column-left").toggleClass("hide");
	});

	$(".profile-role").on('click', function(e){
	   e.preventDefault();
	   var ajaxUrl = $(this).data('ajax-url');
	   $.get(ajaxUrl, function(response){
		   $.notify({ title: 'Role updated', message: 'Role updated'},{ type: 'success'});
		   if ('redirect' in response) {
		   	 document.location.href = response.redirect;
		   }
	   }, 'json')
	});

	$(".profile-branch").on('click', function(e){
	   e.preventDefault();
	   var ajaxUrl = $(this).data('ajax-url');
	   $.get(ajaxUrl, function(response){
		   responseNotification(response)
	   }, 'json')
	});
});

// Autocomplete */
(function($) {

	// if ($('.django-select2').length > 0) {
	// 	$('.django-select2').select2();
	// }

	if ($('.chosen-select').length > 0) {
		// $('.chosen-select').select2();
		$('.chosen-select').chosen();
	}



	// $.fn.autocomplete = function(option) {
	// 	return this.each(function() {
	// 		var $this = $(this);
	// 		var $dropdown = $('<ul class="dropdown-menu" />');
    //
	// 		this.timer = null;
	// 		this.items = [];
    //
	// 		$.extend(this, option);
    //
	// 		$this.attr('autocomplete', 'off');
    //
	// 		// Focus
	// 		$this.on('focus', function() {
	// 			this.request();
	// 		});
    //
	// 		// Blur
	// 		$this.on('blur', function() {
	// 			setTimeout(function(object) {
	// 				object.hide();
	// 			}, 200, this);
	// 		});
    //
	// 		// Keydown
	// 		$this.on('keydown', function(event) {
	// 			switch(event.keyCode) {
	// 				case 27: // escape
	// 					this.hide();
	// 					break;
	// 				default:
	// 					this.request();
	// 					break;
	// 			}
	// 		});
    //
	// 		// Click
	// 		this.click = function(event) {
	// 			event.preventDefault();
    //
	// 			var value = $(event.target).parent().attr('data-value');
    //
	// 			if (value && this.items[value]) {
	// 				this.select(this.items[value]);
	// 			}
	// 		}
    //
	// 		// Show
	// 		this.show = function() {
	// 			var pos = $this.position();
    //
	// 			$dropdown.css({
	// 				top: pos.top + $this.outerHeight(),
	// 				left: pos.left
	// 			});
    //
	// 			$dropdown.show();
	// 		}
    //
	// 		// Hide
	// 		this.hide = function() {
	// 			$dropdown.hide();
	// 		}
    //
	// 		// Request
	// 		this.request = function() {
	// 			clearTimeout(this.timer);
    //
	// 			this.timer = setTimeout(function(object) {
	// 				object.source($(object).val(), $.proxy(object.response, object));
	// 			}, 200, this);
	// 		}
    //
	// 		// Response
	// 		this.response = function(json) {
	// 			var html = '';
	// 			var category = {};
	// 			var name;
	// 			var i = 0, j = 0;
    //
	// 			if (json.length) {
	// 				for (i = 0; i < json.length; i++) {
	// 					// update element items
	// 					this.items[json[i]['value']] = json[i];
    //
	// 					if (!json[i]['category']) {
	// 						// ungrouped items
	// 						html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
	// 					} else {
	// 						// grouped items
	// 						name = json[i]['category'];
	// 						if (!category[name]) {
	// 							category[name] = [];
	// 						}
    //
	// 						category[name].push(json[i]);
	// 					}
	// 				}
    //
	// 				for (name in category) {
	// 					html += '<li class="dropdown-header">' + name + '</li>';
    //
	// 					for (j = 0; j < category[name].length; j++) {
	// 						html += '<li data-value="' + category[name][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[name][j]['label'] + '</a></li>';
	// 					}
	// 				}
	// 			}
    //
	// 			if (html) {
	// 				this.show();
	// 			} else {
	// 				this.hide();
	// 			}
    //
	// 			$dropdown.html(html);
	// 		}
    //
	// 		$dropdown.on('click', '> li > a', $.proxy(this.click, this));
	// 		$this.after($dropdown);
	// 	});
	// }
})(window.jQuery);
