(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");

            $("#save_group_level").on('click', function (event) {
                submitModalForm($("#create_journal_level"));
                return false
            });
			$("#update_group_level").on('click', function (event) {
                submitModalForm($("#edit_journal_level"));
                return false
            });
			$("#copy_inventory").on('click', function (event) {
				console.log( 'Copy inventory items ' )
                submitModalForm($("#copy_inventory_form"));
                return false
            });

			var date = new Date();
			// datepicker on all date input with class datepicker
			$(".datepicker").datepicker({
				dateFormat: 'yy-mm-dd'
			});

			$(".history_datepicker").datepicker({
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "yy-mm-dd",
				maxDate         : -1,
				yearRange       : "1900:"+(date.getFullYear())
			});

			var minDate = new Date()
			var inputMinDate = $("#id_start_date").data('min-date');
			if (inputMinDate) {
				date = new Date(inputMinDate);
				minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : minDate,
					yearRange       : "-30:+100"
				});
			} else {
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : +1,
					yearRange       : "-30:+100"
				});

			}

			$(".chosen-select").chosen();

        });
    });

	$("#goods>tr>td:not(.actions)").on('click', function(){
		var detailUrl = $(this).parent('tr').data('detail-url');
		document.location.href = detailUrl;
		return false;
	});


    var sellingPrice = $("#id_selling_price");
    var vatCode = $("#id_vat_code");
    vatCode.on('change', function(e){
    	Inventory.calculateSellingPriceWithVat(sellingPrice.val(), $(this).val())
	});
    sellingPrice.on('blur', function(e){
    	Inventory.calculateSellingPriceWithVat(sellingPrice.val(), $(this).val())
	});


	$("#inventory_item>tr>td:not(.actions)").on('click', function(){
		var detailUrl = $(this).parent('tr').data('detail-url');
		document.location.href = detailUrl;
		return false;
	});

    var viewInventoryItem = $("#view_inventory_item");
    var inventorySearchUrl = viewInventoryItem.data('ajax-url');
    viewInventoryItem.autocomplete({
        source: inventorySearchUrl,
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          // add the selected item
          if (ui.item) {
              Inventory.displayDetail(ui.item.detail_url)
          }
          return false;
        }
    });

    var searchInventoryItem = $("#search_inventory_item");
    searchInventoryItem.on('click', function(e){
    	Inventory.searchAndDisplayDetail(searchInventoryItem)
	});


    var inventoryQuantity = $(".quantity");
    inventoryQuantity.on('blur', function(){
    	var inventoryItemId = $(this).data('inventory-item-id');
    	Inventory.calculateAdjustment(inventoryItemId);
	});

    $("#save_inventory_item").on('click', function(){
    	submitForm($("#create_inventory_item"));
	});

	$("#save_and_copy_to_all").on('click', function(){
    	submitForm($("#create_inventory_item"), {'is_copied': true})
	});

	$("#submit_create_branch_inventory").on('click', function(){
    	submitForm($("#create_branch_inventory"))
	});

	$("#update_inventory").on('click', function(){
    	submitForm($("#update_inventory_form"))
	});

	var searchPurchaseOrder = $("#id_reference");
	var poSearchUrl = searchPurchaseOrder.data('ajax-url');

	var supplier = $("#id_supplier");
	supplier.on('change', function(){
		Inventory.loadSupplierItems($(this));
	});


	$("#id_purchase_order").hide();
	$("#id_purchase_order_chosen").hide();

	var purchaseOrderSupplier = $("#id_purchase_order_supplier");
	purchaseOrderSupplier.on('change', function(e){
		Inventory.loadSupplierPurchaseOrders($(this));
		return false;
	});

	var idReceivedMovement  = $("#id_received_movement");
	if (idReceivedMovement.length > 0) {
		idReceivedMovement.hide();
		$("#id_received_movement_chosen").hide();
		var supplierReturned = $("#id_supplier_returned");
		supplierReturned.on('change', function(){
			Inventory.loadSupplierInventoryItems($(this));
		});
	}

	// var idSupplierReceivedMovement  = $("#supplier_inventory_received_items");
	// if (idsupplierReceivedMovement.length > 0) {
	// 	console.log('Hide the received goods listdsddddddddddddddddddd');
	// 	idsupplierReceivedMovement.hide();
	// 	$("#id_received_movement_chosen").hide();
	// 	var supplierInventoryReturned = $("#id_supplier_returned");
	// 	supplierInventoryReturned.on('change', function(){
	// 		Inventory.loadSupplierInventoryItems($(this));
	// 	});
	// }



	$("#submit_goods_received").on('click', function(e){
		submitForm($("#create_goods_received_form"));
		return false;
	});


	$("#submit_goods_returned").on('click', function(e){
		submitForm($("#create_goods_returned"));
		return false;
	});

	$("#adjust_stock_submit").on('click', function(){
		submitForm($("#adjust_stock"));
		return false;
	});

	var inventoryItemsBody = $("#inventory_items_body");
	if (inventoryItemsBody.length > 0) {
		var ajaxUrl = inventoryItemsBody.data('ajax-url');
		Inventory.loadInventoryItems(ajaxUrl);
	}

	var addLine = $("#add_received_line");
	addLine.on('click', function(){
		var ajaxUrl = addLine.data('ajax-url');
		Inventory.loadInventoryItems(ajaxUrl);
	});

	$("#enter_totals").on("change", function(event){
		Inventory.openTotalInputs($(this))
	});

	$("#id_measure").on('change', function(e){
		Inventory.updateUnits($(this));
	});

	$(".ledger-inventory-item").on('click', function(e){
		e.preventDefault();
		Inventory.displayInventoryBreakdown($(this));
	});

	var viewInventoryDetail = $("#inventory_item_detail");
	if (viewInventoryDetail.length > 0) {
		var detailUrl = viewInventoryDetail.data('ajax-url');
		Inventory.displayDetail(detailUrl)
	}


	var isRecipeItem = $("#id_is_recipe_item");
	isRecipeItem.on('change', function(){
		if ($(this).is(":checked")) {
			$(".not-reciped").hide();
		} else {
			$(".not-reciped").show();
		}
	});

	if (isRecipeItem.is(":checked")) {
		$(".not-reciped").hide();
	} else {
		$(".not-reciped").show();
	}

});


var Inventory = (function(inventory, $){

	return {

		updateUnits: function(el)
		{
			var unitsAjaxUrl = el.data('units-url');
			var measure = el.val();
			$.getJSON(unitsAjaxUrl, {
				measure: measure
			}, function(response){
				console.log(response)
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
				});
				$("#id_sale_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_stock_unit").html(html.join('')).trigger("chosen:updated");
			});
		},

		displayLedgerDetail: function(el)
		{
		    var el = $(el);
		    var lineId = el.data('line-id');
		    console.log("has class open ==> ",  el.hasClass('open') );
            if (el.hasClass('open')) {
                $(".row-" + lineId).hide();
                el.removeClass('open');
            } else {
                if ($("#row-" + lineId).length > 0) {
                    $(".row-" + lineId).show();
                    el.addClass('open');
                } else {
                    var ajaxUrl = el.data('ajax-url');
                    console.log( ajaxUrl );
                    console.log( lineId );
                    $.get(ajaxUrl, function(html){
                        $("#row_" + lineId ).after(html);
                        el.addClass('open');
                    });
                }
            }
		},

		openTotalInputs: function(el)
		{
			var isChecked = el.is(":checked");
			$(".price-incl").each(function(){
				if (isChecked) {
					$(this).prop('type', 'text');
				} else {
					$(this).prop('type', 'hidden');
				}
			});
		},

		loadInventoryItems: function(ajaxUrl)
		{
			var container = $("#inventory_items_placeholder");
			var rowId = 0;
			var lastRow = $(".inventory_item_row").last();
			console.log("Last row");
			console.log( lastRow );
			var chosenEl = $("#"+ rowId + "_inventory_item_chosen");
			var chosenWidth = 0;
			if (chosenEl.length > 0) {
				chosenWidth = chosenEl.width();
            }
			if (lastRow.length > 0) {
				console.log("Row was");
				console.log( lastRow.data('row-id') );
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}

				$("#"+ rowId +"_inventory_item").on('change', function(){
					var selectedOption = $("#"+ rowId +"_inventory_item option:selected");
					var price = selectedOption.data('price');
					var measureId = selectedOption.data('measure');
					var unitId = selectedOption.data('unit-id');
					var vatId = selectedOption.data('vat-id');

					$("#price_" + rowId ).val( price );
					if (measureId !== undefined && measureId !== '') {
						var unitsUrl = selectedOption.data('units-url');
						Inventory.loadUnits(rowId, measureId, unitsUrl, unitId);
					}
					if (vatId !== undefined && vatId !== '') {
						$("#vat_code_" + rowId).val(vatId).trigger("chosen:updated");
					} else {
						$("#vat_code_" + rowId).val('').trigger("chosen:updated");
					}
					Inventory.calculateInventoryItemPrices(rowId);

				});

				$("#quantity_received_"+ rowId +"").on('blur', function(){
					var selectedOption = $("#"+ rowId +"_inventory_item option:selected");
					var maxOrder = selectedOption.data('max-order');
					var receivedQty = parseFloat($(this).val());
					if (maxOrder !== undefined && maxOrder !== '') {
                        if (maxOrder >= receivedQty) {
                            Inventory.calculateInventoryItemPrices(rowId);
                        } else {
                            $(this).val('');
                            alert('Quantity entered is greater than the max quantity(' + maxOrder + ') allowed, please fix')
                        }
                    } else {
						Inventory.calculateInventoryItemPrices(rowId);

                    }
				});

				$("#vat_code_"+ rowId ).on('change', function(){
					console.log('Calculating based on vat change')

					var netTotal = $("#total_price_exl_" + rowId ).text();
					var vatSelected = $("#vat_code_"+ rowId  + " option:selected");
					var percentage = vatSelected.data('percentage');
					console.log( netTotal )
					console.log( vatSelected )
					console.log( percentage )
					if (netTotal !== '' && percentage !== '') {
						var vatAmount = (parseFloat(percentage)/100) * parseFloat(netTotal);
						var vatAmountStr = accounting.formatMoney(vatAmount);
						$("#vat_" + rowId ).html( vatAmountStr );
						$("#vat_amount_" + rowId ).val( vatAmount );
					}
					Inventory.calculateInventoryItemPrices(rowId);
				});

				$("#price_"+ rowId +"").on('blur', function(){
					Inventory.calculateInventoryItemPrices(rowId);
				});
				if (chosenWidth > 0) {
					$(".chosen-select").chosen({'width': chosenWidth + "px"});
				} else {
					$(".chosen-select").chosen({'width': '143px'});
				}
			});
		},

		loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			var measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId == item.id) {
						html.push("<option value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#unit_" + rowId).html(html.join('')).trigger("chosen:updated");

			});
		},

		loadSupplierReceivedItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id,
				date: $("#id_date").val()
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		loadSupplierPurchaseOrders: function(el)
		{
			var ajaxUrl = el.data('purchase-orders-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_purchase_order  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		loadSupplierInventoryItems: function(el)
		{
			var ajaxUrl = el.data('inventory-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_inventory");
			$("#id_inventory_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");

				$.each(response, function(index, inventory_item){
					html.push("<option value='"+ inventory_item.id +"' " +
						"data-ajax-url='"+ inventory_item.received_items_url +"'>"+ inventory_item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_inventory  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					console.log("Update received items ---> ")
					Inventory.loadSupplierInventoryReceivedItems(ajaxUrl);
                });
			});
		},

		loadSupplierInventoryReceivedItems: function(ajaxUrl)
		{
			var supplierId = $("#id_supplier_returned option:selected").val();
			var inventoryId = $("#id_inventory option:selected").val();
			console.log("Load received items for supplier and inventory item---> ", supplierId, inventoryId, ajaxUrl)
			// var supplierId = $("#id_supplier_returned option:selected").val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();
			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplierId,
				inventory_id: inventoryId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displaySupplierInventoryReceivedItems(ajaxUrl, inventoryId);
                });
			});
		},

		displaySupplierInventoryReceivedItems: function(ajaxUrl, inventoryId)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, {
					inventory_id: inventoryId
				}, function(html){
				   $("#supplier_inventory_received_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						var inventoryItemId = $(this).data('item-id');
						Inventory.calculateInventoryItemPrices(inventoryItemId)
				   });
				});
			} else {
				$("#supplier_inventory_received_items").html("");;
			}
		},

		loadSupplierItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_purchase_order  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		calculateAdjustment: function(inventoryItemId)
		{
			var onHand = $("#quantity_on_hand_" + inventoryItemId);
			var newQuantity = $("#new_quantity_" + inventoryItemId);

			if (onHand.val() !== '' && newQuantity.val() !== '') {
				var adjustment = parseInt(newQuantity.val()) - parseInt(onHand.val());
				if (adjustment > 0) {
				    $(".price-choice-" + inventoryItemId ).show();
                } else {
				    $(".price-choice-" + inventoryItemId ).hide();
                }
				$("#adjustment_" + inventoryItemId).val( adjustment );
            }
		},

		calculateSellingPriceWithVat: function(sellingPrice, vatCodeId)
		{
			var sellingPriceWithVat = $("#selling_price_inc_vat");
			var ajaxUrl = sellingPriceWithVat.data('calculate-selling-price');
			$.getJSON(ajaxUrl, {
				vat_code_id: vatCodeId,
				selling_price: sellingPrice
			}, function(response) {
				if (!response.error) {
					sellingPriceWithVat.val(response.price);
				}
			});
		},

		displayMovementBreakdown: function(el, inventoryId)
		{
        	var movementId = el.data('movement-id');
            var movementBreakdown = $("#movement_breakdown_" + inventoryId + "_" + movementId);
            console.log('Display movement breakdown', movementId, $("#movement_breakdown_" + inventoryId + "_" + movementId) )
            console.log(el, movementId)
			if (movementBreakdown.hasClass('open')) {
				console.log('Open the inventory --- > via toggle ')
				movementBreakdown.hide().removeClass('open').addClass('closed');
			} else if(movementBreakdown.hasClass('closed')) {
				movementBreakdown.show().removeClass('closed').addClass('open');
			}
		},

		displayInventoryBreakdown: function(el)
        {
        	var ajaxUrl = el.data('ajax-url');
        	var inventoryId = el.data('inventory-id');
        	var inventoryBreakdown = $("#inventory_breakdown_" + inventoryId);
			console.log('Open the inventory --- > ')
			if (inventoryBreakdown.hasClass('open')) {
				console.log('Open the inventory --- > via toggle ')
				inventoryBreakdown.hide().removeClass('open').addClass('closed');
			} else if(inventoryBreakdown.hasClass('closed')) {
				inventoryBreakdown.show().removeClass('closed').addClass('open');
			} else {
				console.log('Open the inventory --- > via ajax ')
				$(".breakdown").each(function(){
					if ($(this).hasClass('open')) {
						$(this).hide().removeClass('open').addClass('closed');
					}
				});
				$("#breakdown_row_" + inventoryId).show();
				inventoryBreakdown.html('loading breakdown...');
				$.get(ajaxUrl, function(html){
				    inventoryBreakdown.html(html).addClass('open');

					$(".movement-row").on('click', function(e){
						e.preventDefault();
						Inventory.displayMovementBreakdown($(this), inventoryId);
					});
				});
			}
        },

        displayDetail: function(detailUrl)
        {
            $.get(detailUrl, function(html){
               $("#inventory_item_detail").html(html);
            });
        },

		searchAndDisplayDetail: function(el)
        {
            var term = $("#view_inventory_item").val();
            if (term !== '') {
                var ajaxUrl = el.data('ajax-url');
                $.get(ajaxUrl, {
                    term:term
                }, function(html){
                   $("#inventory_item_detail").html(html);
                });
            }
        },

		displayPurchaseOrderInventoryItems: function(ajaxUrl)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, function(html){
				   $("#purchase_order_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						var inventoryItemId = $(this).data('item-id');
						Inventory.calculateInventoryItemPrices(inventoryItemId)
				   });
				});
			} else {
				$("#purchase_order_items").html("");;
			}
		},

		calculateInventoryItemPrices: function(inventoryItemId)
		{
			console.log("Calculate totals --> ", inventoryItemId );
			var price = $("#price_" + inventoryItemId);
			var ordered = $("#quantity_ordered_" + inventoryItemId);
			var received = $("#quantity_received_" + inventoryItemId);
			var short = $("#quantity_short_" + inventoryItemId);
			var vat = $("#vat_amount_" + inventoryItemId);
			var backOrder = $("#bo_" + inventoryItemId);

			var quantityOrdered = 0;
			var shortQuantity = 0;
			var quantityReceived = 0;
			var vatAmount = 0;
			var priceExcl = 0;

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}
			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}
			if (vat !== undefined && vat.length > 0) {
				if (vat.val() !== '') {
					vatAmount += parseFloat(accounting.unformat(vat.val()));
				}
			}
			if (quantityOrdered > 0) {
				if (quantityReceived <= quantityOrdered) {
					shortQuantity =  quantityOrdered - quantityReceived
				} else {
					received.val(quantityOrdered);
					backOrder.prop('disabled', true);
					alert('Please enter valid received quantity')
					return false;
				}
			}
			if (short !== undefined  && short.length > 0) {
                short.val( shortQuantity );
            }
			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}

			var totalExcl = priceExcl * quantityReceived;
			var priceIncl =  priceExcl + vatAmount;
			var totalInc = priceIncl * quantityReceived;

			console.log( " quantityReceived ", quantityReceived );
			console.log( " Price EXCL", priceExcl );
			console.log( " totalExcl ", totalExcl );
			console.log( " vatAmount ", vatAmount );
			console.log( " priceIncl ", priceIncl );
			console.log( " totalInc", totalInc );

			if (backOrder !== undefined) {
				if (shortQuantity === 0) {
					backOrder.prop('disabled', true);
				} else {
					backOrder.prop('disabled', false);
				}
			}

			$("#price_incl_" + inventoryItemId).html( accounting.formatMoney(priceIncl) );
			$("#price_including_" + inventoryItemId).val( priceIncl );

			$("#total_price_exl_" + inventoryItemId).html( accounting.formatMoney(totalExcl) );
			$("#total_price_excluding_" + inventoryItemId).val( totalExcl );

			$("#total_price_incl_" + inventoryItemId).html( accounting.formatMoney(totalInc) );
			$("#total_price_including_" + inventoryItemId).val( totalInc );

			Inventory.calculateTotals()
		},

		calculateTotals: function()
		{
			var totalOrdered = 0;
			var totalReceived = 0;
			var totalShort = 0;
			var priceExc = 0;
			var priceInc = 0;
			var totalPrice = 0;
			var totalTotalExc = 0;
			var totalTotalInc = 0;

			$(".ordered").each(function(){
				totalOrdered += parseInt($(this).val());
			});

			$(".received").each(function(){
				totalReceived += parseInt($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseInt($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});

			$("#total_ordered").html( totalOrdered );
			$("#total_received").html( totalReceived );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.formatMoney(totalPrice) );
			$("#price_excl").html( accounting.formatMoney(priceExc) );
			$("#price_incl").html( accounting.formatMoney(priceInc) );
			$("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
			$("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );

		}



	}
}(Inventory || {}, jQuery))


},{}]},{},[1]);
