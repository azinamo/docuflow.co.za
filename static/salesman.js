$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");
			initDatePicker();

			initChosenSelect();

			var saveBtn = $(".save-btn");

			saveBtn.on("click", function(e){
				console.log("id --> ", saveBtn.attr('id') )
				var submitForm = $("#" + saveBtn.attr('id') + "_form");
				var text = saveBtn.data('processing-text');
				console.log(text);
				console.log("\r\n\n\n---------------\r\n\n");
				submitForm.LoadingOverlay('show', {
					'text': text
				});
				ajaxSubmitForm(saveBtn, submitForm);
			});

        });
    });


});


var Salesman = (function(salesman, $){

	return {

	}

}(Salesman || {}, jQuery))

