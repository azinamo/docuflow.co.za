$(function(){


   var groupSelector = $("#group");
   groupSelector.on("change", function(e){
       var groupPermissionsContainer = $("#group_permission_settings");
       groupPermissionsContainer.html("Loading ...")
       e.preventDefault();
       var ajaxUrl = groupSelector.data("ajax-url");
       var groupId = groupSelector.val();
       $.get(ajaxUrl, {
           group_id: groupId
       }, function(html){
           groupPermissionsContainer.html(html)
       });
   });


   $("#save_permission_group_permissions").on('click', function(e){
        var ajaxUrl = $(this).data('ajax-url');
        e.preventDefault();
        var groupId = groupSelector.val();
        $("#permission_group_permissions").ajaxSubmit({
            url: ajaxUrl,
            data: {'group_id': groupId},
            dataType: 'json',
            success: function(response, statusText, xhr, $form)  {
                $.notify({ title: '', message: response.text},{ type: (response.error ? 'danger' : 'success') });
                // for normal html responses, the first argument to the success callback
                // is the XMLHttpRequest object's responseText property

                // if the ajaxSubmit method was passed an Options Object with the dataType
                // property set to 'xml' then the first argument to the success callback
                // is the XMLHttpRequest object's responseXML property

                // if the ajaxSubmit method was passed an Options Object with the dataType
                // property set to 'json' then the first argument to the success callback
                // is the json data object returned by the server
                //jQuery("#total").html( response.total );
                //jQuery("#total_value").val( response.total );
            }
        });
    });
});