$(document).ready(function() {
	//Form Submit for IE Browser

	$(".default_document_type").on('change', function(e){
		DocumentType.updateDefaultDocumentType(this);
		e.preventDefault();
	});

});


var DocumentType = (function(documentType, $){

	return {

		updateDefaultDocumentType: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url')
			$.getJSON(ajaxUrl, function(response){
				responseNotification(response)
			});
		}
	}
}(DocumentType || {}, jQuery))
