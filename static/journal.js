$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			$("#comment_submit").on('click', function (event) {
                event.preventDefault();
                var form = $($(this).parent()).parent()
                console.log( form )
                console.log($("form[name='comment_journal_form']"))
                submitModalForm(form);
            });

			initDatePicker()

			initChosenSelect();

        });
    });

    var journalLinesContainer = $("#journal_lines");
    if (journalLinesContainer.length > 0) {
        Journal.loadJournalLine(journalLinesContainer, 1, 50)
    }


    $(".accounts").on('change', function(e){
        var rowId = $(this).attr('id').replace('account_', '');
        Journal.loadAccountVatCodes($(this), $(this).val(), rowId);

        var accountType = $(this).find(':selected').data('account-type')
        if (accountType === 1) {
            $("#distribution_" + rowId).prop('disabled', false)
        } else {
            $("#distribution_" + rowId).prop('disabled', true)
        }
    });

    $(".vat_codes").on('change', function(e){
        var rowId = $(this).attr('id').replace('vat_codes_', '');
        var description = $(this).data('description');
        $("#description_" + rowId).html(description);
    })

    $(".credit_amount").on('blur', function(e){
        var rowId = $(this).attr('id').replace('credit_', '');
        if ($(this).val() !== '') {
            $("#debit_" + rowId).prop('disabled', true);
            Journal.calculateTotalCredit();
        } else {
            $("#debit_" + rowId).prop('disabled', false);
            Journal.calculateTotalCredit();
        }
    });

    $(".debit_amount").on('blur', function(e){
        var rowId = $(this).attr('id').replace('debit_', '');
        var debitAmount = $(this).val();
        if (debitAmount !== '') {
            var amount = parseFloat(debitAmount);
            if (amount < 0) {
                $("#credit_" + rowId).val(amount * -1);
                $("#debit_" + rowId).prop('disabled', true);
                $(this).val('');
                Journal.calculateTotalCredit();
            } else {
                 $("#credit_" + rowId).prop('disabled', true);
                Journal.calculateTotalDebit();
            }
        } else {
            $("#credit_" + rowId).prop('disabled', false);
            Journal.calculateTotalDebit();
        }
    });

    $(".distributions").on('change', function(){
        if ($(this).val() === 'yes') {
            var rowId = $(this).attr('id').replace('distribution_', '');
           Journal.openDistributionModal($(this), rowId);
        }
    });

    $(".delete_journal_line").on('click', function(e){
        e.preventDefault();
        if (confirm('Are you sure you want to delete this line')) {
            Journal.removeJournalLine($(this));
        }
    });

    $(".delete_journal").on('click', function(e){
        e.preventDefault();
        if (confirm('Are you sure you want to delete this journal')) {
            Journal.deleteJournal($(this));
        }
    });

    $(".journal-datepicker").datepicker({
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat : 'yy-mm-dd',
        onSelect: function( selectedDate ) {
        },
        onClose: function(t, i) {
            Journal.validatePeriodAndDate();
        }
    });



    $("#add_journal_line").on('click', function(e){
        e.preventDefault();
        var ajaxUrl = $(this).data('ajax-url');
        var row = $(".journal_lines").last();
        var rowNumber = row.attr('id').replace('journal_line_', '');
        Journal.loadJournalLine(journalLinesContainer, rowNumber);
    });

    $("#save_general_journal").on('click', function(e){
       Journal.checkFixedAssetAccountPosting($(this));
    });

});


var Journal = (function(journal, $){

	return {

	    isValidLine: function(rowId)
        {
          var debitEl = $("#debit_" + rowId);
          var creditEl = $("#credit_" + rowId);
          var totalAmount = 0;
          if (debitEl.length > 0 && debitEl.val() !== "") {
              totalAmount += parseFloat(debitEl.val());
          }
          if (creditEl.length > 0 && creditEl.val() !== "") {
              totalAmount += parseFloat(creditEl.val());
          }
          console.log("Total line amount --> ", totalAmount, (totalAmount <= 0))
          if (totalAmount <= 0) {
              return false;
          }

          var modelEl = $(".model-" + rowId);
          console.log("Model element --> ", modelEl, typeof modelEl)
          if (modelEl.length <= 0 || modelEl === undefined || modelEl.val() === '' ) {
             return false;
          }
          return true;
        },

	    loadJournalLine: function(el, startId, rows)
        {
            startId = startId || 1;
            rows = rows || 1;
            console.log( 'start at ', startId , ' and there are rows are ', rows);

            var ajaxUrl = el.data('ajax-url');
            $.get(ajaxUrl,{
                rows: rows,
                row: startId
            }, function(responseHtml){
                el.append(responseHtml);

                $(".journal-lines").each(function(){
                    var itemRowId = $(this).data("row-id");
                    Journal.handleRow(itemRowId);
                });

            });
        },

        handleRow: function(rowId)
        {
            console.log( rowId );
            var ledgerLineSelect = $("#journal_content_type_" + rowId);
            var searchUrl = ledgerLineSelect.data("ajax-url");
            var chosenWidth = calculateSelectWidth(ledgerLineSelect);
            ledgerLineSelect.ajaxChosen({
                url: searchUrl,
                dataType: "json",
                width: "200px",
                allowClear: true,
                placeholder: "Select ..."
            }, {loadingImg: '/static/js/vendor/chosen/loading.gif'});

            $(".chosen-select").chosen({
                allowClear: true,
                placeholder: "Select ...",
                width: "200px",
            }).css({'min-width': '200px !important', 'max-width': '200px !important'});

            var ledgerTypeEl = $("#ledger_" + rowId);
            var ledgerType = ledgerTypeEl.val();
            var lastRow = $(".journal-lines").last();

            ledgerLineSelect.on('change', function(e){
                console.log(" ledger type --> " + ledgerType);
                Journal.loadAccountVatCodes($(this), $(this).val(), rowId);
                if (ledgerType === 1) {
                    Journal.processAccounts($(this));
                } else if(ledgerType === 2) {
                    console.log("Suppliers")
                    //Journal.processAccounts($(this));
                } else if(ledgerType === 3) {
                    console.log("Customers")
                    //Journal.processAccounts($(this));
                }
            });

            $("#credit_" + rowId).on('blur', function(e){
                Journal.processCreditAmount($(this));
                if (lastRow !== undefined) {
                    var lastRowId = lastRow.data('row-id');
                    console.log("Last row --> ", lastRowId);
                    if (Journal.isValidLine(lastRowId)) {
                       //Journal.loadJournalLine($("#journal_lines"), parseInt(lastRowId) + 1, 1);
                    }
                }
            });
            $("#debit_" + rowId).on('blur', function(e){
                Journal.processDebitAmount($(this));
            });
            ledgerTypeEl.on('change', function(e){
                Journal.reloadJournalRow($(this))
            });
        },

        reloadJournalRow: function(el)
		{
			var rowId = el.data('row-id');
			console.log("Row id  --> ", rowId);
			var ajaxUrl = el.data('ajax-url');
			var chosenWidth = $("#"+ rowId + "_estimate_item_chosen").width();
			var ledgetType = $("#ledger_"+ rowId +" option:selected").val();
			$.get(ajaxUrl, {
				row: rowId,
				line_type: ledgetType
			}, function(html) {
				$("#journal_line_" + rowId).replaceWith(html);
                console.log("Journal line updated for different ledger --> ", ledgetType);
				Journal.handleRow(rowId);
				// $(".chosen-select").chosen();
				// if (chosenWidth > 0) {
				// 	$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
				//
				// } else {
				// 	$(".chosen-select").not('.line-type').chosen({'width': '250px'});
				// }

			});

		},

	    checkFixedAssetAccountPosting: function(el)
        {
            var journalForm = $("#create_journal_form");
            journalForm.LoadingOverlay("show", {
                text : "Processing"
            });
            var ajaxUrl = el.data('ajax-check--asset-account-url');
            journalForm.ajaxSubmit({
                url:    ajaxUrl,
                target: '#loader',   // target element(s) to be updated with server response
                beforeSubmit:  function(formData, jqForm, options) {
                    var queryString = $.param(formData);
                },  // pre-submit callback
                error: function (data) {
                    // handle errors
                    var error = {'error': true, 'text': data.statusText};
                    responseNotification(error);
                    journalForm.LoadingOverlay("hide");
                    el.prop('disabled', false);
                },
                success:       function(responseJson, statusText, xhr, $form)  {
                    // handle response
                    if ('account' in responseJson) {
                        responseNotification(responseJson);
                        Journal.createFixedAsset(responseJson);
                    } else {
                        Journal.saveGeneralJournal(el);
                    }


                },  // post-submit callback
                dataType: 'json',
                type: 'post'
            });
        },

        createFixedAsset: function(responseJson)
        {
            $.get(responseJson.create_fixed_asset_url, function (data) {
                $(data).modal('show');

                initDatePicker();

                initChosenSelect();

            });
        },

	    saveGeneralJournal: function(el)
        {
            ajaxSubmitForm(el, $("#create_journal_form"));
            el.prop('disabled', false);
        },

        loadLedgerTypeLists: function(el)
        {
            var rowId = el.data('row-id');
            var contentType = $("#journal_content_type_" + rowId);
            var ledgerTypeEl = $("#ledger_" + rowId + " option:selected");
            var ledgerType = ledgerTypeEl.val();
            console.log("Row id --> " + rowId);
            console.log( ledgerType, typeof ledgerType)
            var defaultAjaxUrl = contentType.data('ajax-url');
            if (ledgerType === '0') { //Accounts
                //Journal.loadAccounts(ledgerTypeEl, rowId)
                defaultAjaxUrl = el.data('ajax-accounts-url');
            } else if (ledgerType === '1') { //Suppliers
                //Journal.loadSuppliers(ledgerTypeEl, rowId);
                defaultAjaxUrl = el.data('ajax-supplier-url');
            } else if (ledgerType === '2') { //Customers
                //Journal.loadCustomers(ledgerTypeEl, rowId)
                defaultAjaxUrl = el.data('ajax-customers-url');
            }
            console.log(defaultAjaxUrl);
            contentType.prop('data-ajax-url', defaultAjaxUrl)
        },

        loadAccounts: function(el, rowId)
        {
            var ajaxUrl = el.data('ajax-url');
            var ledger = el.val();
            $.get(ajaxUrl, {
                ledger: ledger
            },function(html){
                $("#account_" + rowId).html(html);
                $(".chosen-select").chosen();
                $(".accounts").on('change', function(e){
                    Journal.processAccounts($(this));
                });
                $(".credit_amount").on('blur', function(e){
                    Journal.processCreditAmount($(this));
                });
                $(".debit_amount").on('blur', function(e){
                    Journal.processDebitAmount($(this));
                });
            })
        },

        loadSuppliers: function(el, rowId)
        {
            var ajaxUrl = el.data('ajax-suppliers-url');
            var ledger = el.val();
            $.get(ajaxUrl, {
                ledger: ledger
            },function(html){
                $("#account_" + rowId).html(html);
                $(".chosen-select").chosen();
                $(".accounts").on('change', function(e){
                    Journal.processAccounts($(this));
                });
                $(".credit_amount").on('blur', function(e){
                    Journal.processCreditAmount($(this));
                });
                $(".debit_amount").on('blur', function(e){
                    Journal.processDebitAmount($(this));
                });
            })
        },

        loadCustomers: function(el, rowId)
        {
            var ajaxUrl = el.data('ajax-customers-url');
            var ledger = el.val();
            $.get(ajaxUrl, {
                ledger: ledger
            },function(html){
                $("#account_" + rowId).html(html);
                $(".chosen-select").chosen();
                $(".accounts").on('change', function(e){
                    Journal.processAccounts($(this));
                });
                $(".credit_amount").on('blur', function(e){
                    Journal.processCreditAmount($(this));
                });
                $(".debit_amount").on('blur', function(e){
                    Journal.processDebitAmount($(this));
                });
            })
        },

	    processAccounts: function(el)
        {
            var rowId = el.attr('id').replace('account_', '');
            Journal.loadAccountVatCodes(el, el.val(), rowId);

            var accountType = el.find(':selected').data('account-type')
            if (accountType === 1) {
                $("#distribution_" + rowId).prop('disabled', false)
            } else {
                $("#distribution_" + rowId).prop('disabled', true)
            }
        },

        processCreditAmount: function(el)
        {
            var rowId = el.attr('id').replace('credit_', '');
            if (el.val() !== '') {
                $("#debit_" + rowId).prop('readonly', true);
                Journal.calculateTotalCredit();
            } else {
                $("#debit_" + rowId).prop('readonly', false);
                Journal.calculateTotalCredit();
            }
        },

        processDebitAmount: function(el)
        {
            var rowId = el.attr('id').replace('debit_', '');
            var debitAmount = el.val();
            if (debitAmount !== '') {
                var amount = parseFloat(debitAmount);
                if (amount < 0) {
                    $("#credit_" + rowId).val(amount * -1);
                    $("#debit_" + rowId).prop('readonly', true);
                    el.val('');
                    Journal.calculateTotalCredit();
                } else {
                     $("#credit_" + rowId).prop('readonly', true);
                    Journal.calculateTotalDebit();
                }
            } else {
                $("#credit_" + rowId).prop('readonly', false);
                Journal.calculateTotalDebit();
            }
        },

	    validatePeriodAndDate: function()
        {
            var ajaxUrl =  $("#validate_journal_period").data('validate-period-ajax-url')
            var journalDate = $("#id_date").val();
            var period = $("#id_period").val();
            $.getJSON(ajaxUrl, {
                journal_date: journalDate,
                period: period
            }, function(responseJson){
                responseNotification(responseJson);
                var isDisabled = ($("#submit").attr('disabled') === 'disabled');
                var isPeriodIssue = $("#submit").data('invalid-period')

                if (!isDisabled) {
                    if (responseJson.error) {
                        $("#submit").prop('disabled', true);
                        $("#submit").data('invalid-period', 'yes');
                    } else {
                        $("#submit").prop('disabled', false);
                    }
                } else {
                    if (isPeriodIssue == 'yes') {
                     $("#submit").prop('disabled', false);
                    }
                }
            })
        },

	    loadAccountVatCodes: function(el, accountId, rowId)
        {
            var ajaxUrl =  el.data('vat-codes-ajax-url');
            var vatCodeCell = $("#vat_codes_cell_" + rowId);
            vatCodeCell.empty()
            $.getJSON(ajaxUrl, {
                account_id: accountId
            }, function(vat_codes){
                if (vat_codes.length > 0) {
                    var html = [];
                    html.push("<select class='chosen-select vat-codes' name='vat_code_"+ rowId +"' id='vat_code_"+ rowId +"' style='max-width: 100px !important; min-width: 100px !important;' >");
                    $.each(vat_codes, function(index, vat_code){

                        html.push("<option value='"+ vat_code.id +"'>" + vat_code.label + "</option>")
                    })
                    html.push("</select>")
                    vatCodeCell.html(html.join(' '));

                    $("#vat_code_" + rowId).select2().css({'min-width': '100px !important', 'max-width': '100px !important'});
                } else {
                    vatCodeCell.empty();
                }
            })
        },

	    calculateTotalCredit: function()
        {
	        var totalCredit = 0;
            $(".credit_amount").each(function(){
                var amount = parseFloat($(this).val());
                if (amount > 0) {
                    totalCredit += parseFloat($(this).val());
                }
            })
            var totalCreditStr = accounting.formatMoney(totalCredit);
            totalCredit = totalCredit.toFixed(2);
            $("#total_credit").val(totalCredit);
            $("#credit_total").html(totalCreditStr);

            Journal.calculateDifference();
        },

        calculateTotalDebit: function () {
	        var totalDebit = 0;
            $(".debit_amount").each(function(){
                var amount = parseFloat($(this).val());
                if (amount > 0) {
                    totalDebit += amount;
                }
            });
            var totalDebitStr = accounting.formatMoney(totalDebit);
            totalDebit = totalDebit.toFixed(2);
            $("#total_debit").val(totalDebit);
            $("#debit_total").html(totalDebitStr);

            Journal.calculateDifference();
        },

        calculateDifference: function()
        {
            var totalDebit = parseFloat($("#total_debit").val());
            var totalCredit = parseFloat($("#total_credit").val());
            var difference = 0;
            var submitBtn = $("#save_general_journal");

            if (totalCredit || totalDebit) {
                difference = totalDebit - totalCredit;

            }
            if (difference == 0) {
                submitBtn.prop('disabled', false);
            } else {
                submitBtn.prop('disabled', true);
            }
            var differenceStr = accounting.formatMoney(difference);
            $("#difference").html(differenceStr);
        },

        openDistributionModal: function(el, rowId)
        {

            var accountId = $("#account_" + rowId).val()
            var debitAmount = $("#debit_" + rowId).val()
            var creditAmount = $("#credit_" + rowId).val()
            console.log("Total debit ", debitAmount, " credit ", creditAmount, " difference ", accountId)
            if (accountId == '') {
                alert('Please select the account')
                return false
            } else if(debitAmount == '' && creditAmount == '') {
                alert('Please select the amount')
                return false
            } else {
                var modalUrl = el.data('ajax-url')
                var amount = debitAmount || creditAmount
                $.get(modalUrl, {
                    credit_amount: creditAmount,
                    debit_amount: debitAmount,
                    account_id: accountId,
                    amount: amount
                }, function (data) {
                    $(data).modal('show');

                    $("#distribute_journal_accounts").on("click", function(){
                        event.preventDefault();
                        Journal.distributeAccounts($(this));
                    });

                    $("#create_journal_distribution_submit").on('click', function(event){
                        submitModalForm($("#create_journal_distribution"));
                        return false;
                    });
                    var date = new Date();
                    // datepicker on all date input with class datepicker
                    $(".datepicker").datepicker({
                        dateFormat: 'yy-mm-dd'
                    });

                    $(".history_datepicker").datepicker({
                        changeMonth		: true,
                        changeYear		: true,
                        dateFormat 		: "dd-M-yy",
                        maxDate         : -1,
                        yearRange       : "1900:"+(date.getFullYear())
                    });

                    $(".future_datepicker").datepicker({
                        changeMonth		: true,
                        changeYear		: true,
                        dateFormat 		: "dd-M-yy",
                        minDate         : +1,
                        yearRange       : "-30:+100"
                    });

                    $(".chosen-select").chosen();

                });
            }
        },

		distributeAccounts: function(el)
		{
		   var accountIds = [];
		   $(".distribution-invoice-account").each(function(){
		   		if ($(this).is(":checked")) {
		   			accountIds.push($(this).val());
				}
		   });

		   var startDate = $("#id_start_date");
		   var nbMonth = $("#id_month");
		   var amount = $("#amount").val()
		   if (accountIds.length === 0) {
			 alert("Please select the accounts")
		   } else if(!startDate.val()) {
			  alert("Please select start date")
		   } else if(!nbMonth.val()) {
			  alert("Please enter number of months")
		   } else {
			   var self = $("#" + el.attr('id'));
			   var container = $("#account_distribution");
			   container.html("Loading . . .");
			   var url = el.data('ajax-url');
			   $.get(url, {
				 months: nbMonth.val(),
				 start_date: startDate.val(),
				 account_ids: accountIds.join(','),
                 amount: amount
			   }, function(html){
				   container.html(html);
			   });
		   }
		},

        deleteJournalLine: function(el)
        {
            var ajaxUrl = el.data('ajax-url');
            var reference = el.attr('id').replace('delete_line_', '');
            $.getJSON(ajaxUrl, function(responseJson){
               responseNotification(responseJson);
               $("#journal_line_row_" + reference).remove();
            });
        },

        removeJournalLine: function(el)
        {
           var reference = el.attr('id').replace('delete_line_', '');
           $("#journal_line_row_" + reference).remove();
           Journal.calculateTotalCredit()
           Journal.calculateTotalDebit()
        },

        deleteJournal: function(el)
        {
            var ajaxUrl = el.data('ajax-url');
            $.getJSON(ajaxUrl, function(responseJson){
               responseNotification(responseJson);
            });
        }


	}

}(Journal || {}, jQuery))

