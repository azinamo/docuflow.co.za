$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {

			initChosenSelect();

        });
    });

	$("#recipes>tr>td:not(.actions)").on("click", function(){
		var detailUrl = $(this).parent("tr").data("detail-url");
		document.location.href = detailUrl;
		return false;
	});

	$("#submit_recipe").on('click', function(e){
		ajaxSubmitForm($(this), $("#recipe_form"));
		return false;
	});

	var inventoryItemsBody = $("#recipe_inventory_items");
	if (inventoryItemsBody.length > 0) {
		var ajaxUrl = inventoryItemsBody.data('ajax-url');
		Recipe.loadRecipeItems(inventoryItemsBody, ajaxUrl);
	}

	var addInventoryRecipeLine = $("#add_inventory_recipe_line");
	addInventoryRecipeLine.on('click', function(){
		$(this).prop('disabled', true).data('html', addInventoryRecipeLine.text()).text('loading ...')
		var ajaxUrl = addInventoryRecipeLine.data('ajax-url');
		Recipe.loadRecipeItems(addInventoryRecipeLine, ajaxUrl);
	});

});

var Recipe = (function(recipe, $){

	return {

		removeInventoryItem: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			$(this).prop('disabled', true).data('html', el.text()).text('loading ...');
			var rowId = el.data('row-id');
			$.get(ajaxUrl, function(response){
				responseNotification(response);
				if (!response.error) {
					$("#row_" + rowId).remove();
				}
			});
		},

		loadRecipeItems: function(el, ajaxUrl)
		{
			var container = $("#recipe_inventory_placeholder");
			var rowId = 0;
			var lastRow = $(".inventory_item_row").last();
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				console.log( html )
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				var _lastRow = $(".inventory_item_row").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}

				$(".inventory_item_row").each(function(){
					var itemRowId = $(this).data("row-id")
					Recipe.handleRow(itemRowId);
				});

				el.prop('disabled', false).text(el.data('html'));
			});
		},

		handleRow: function(rowId)
		{
			var chosenWidth = calculateChosenSelectWidth("inventory_item_row");
			console.log("With is ----- ", chosenWidth)
			var inventorySelect = $("#"+ rowId +"_inventory_item");
			var inventorySearchUrl = inventorySelect.data("ajax-url");
			var inventoryUrl = inventorySelect.data("ajax-inventory-url");
			inventorySelect.ajaxChosen({
				url: inventorySearchUrl,
				dataType: "json",
				width: chosenWidth + "px",
				allowClear: true,
				placeholder: "Select ..."
			}, {loadingImg: '/static/js/vendor/chosen/loading.gif'}).width(chosenWidth + "px");

			inventorySelect.on('change', function(e){
				var inventoryId = $(this).val();
				$.get(inventoryUrl, {
					inventory_id: inventoryId
				}, function(inventoryData){
					var price = inventoryData['price'];
					var measureId = inventoryData['measure_id'];
					var unitId = inventoryData['unit_id'];
					$("#price_" + rowId ).val(price);
					if (measureId !== undefined && measureId !== '') {
						var unitsUrl = inventoryData['units_url'];
						Recipe.loadUnits(rowId, measureId, unitsUrl, unitId);
					}
				});
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				// Recipe.calculateInventoryItemPrices(rowId);
			});

			$('.remove-recipe-inventory-item').on('click', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				Recipe.removeInventoryItem($(this));
			});
		},

		loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId == item.id) {
						html.push("<option value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#unit_" + rowId).html(html.join('')).trigger("chosen:updated");

			});
		},

		loadSupplierItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_purchase_order  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		}
	}
}(Recipe || {}, jQuery))


