$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");


            initDatePicker();

			initChosenSelect();

        });
    });

	$(".bulk-action").on('click', function(e){
		e.preventDefault();
		BackOrder.bulkInvoicesAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		BackOrder.singleInvoiceAction($(this));
	});

});

var BackOrder = (function(estimate, $){

	return {

		bulkInvoicesAction: function(el)
		{
			var back_orders = BackOrder.getSelectedBackOrders();
			var totalBackOrders = back_orders.length;
			if (totalBackOrders === 0) {
				alert('Please select at least one back order');
			} else {
				var ajaxUrl = el.data('ajax-url');
				var confirm = el.data('confirm');
				if (confirm === '1') {
					var confirmText = el.data('confirm-text')
					return confirm(confirmText)
				}
				console.log( back_orders )
				console.log( ajaxUrl )
				$.get(ajaxUrl, {
					back_orders: back_orders,
					category: el.attr('id')
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singleInvoiceAction: function(el)
		{
			var back_order = BackOrder.getSelectedBackOrders();
			var totalBackOrders = back_order.length;
			if (totalBackOrders === 0) {
				alert('Please select at least one back order');
			} else if (totalBackOrders > 1) {
				alert('Please select one back order to action');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( totalBackOrders )
				console.log( ajaxUrl )
				$.get(ajaxUrl, {
					back_order_id: back_order[0],
					category: el.attr('id')
				}, function(response){
					responseNotification(response);
				});
			}
		},

		getSelectedBackOrders: function()
		{
			var back_orders = [];
			$(".back-order-action").each(function(){
				if ($(this).is(":checked")) {
                    back_orders.push($(this).data('back-order-id'));
                }
			});
			return back_orders;
		},

		actionBackOrder: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			$.getJSON(ajaxUrl, function(response){
				responseNotification(response)
			});
		}

	}
}(BackOrder || {}, jQuery))

