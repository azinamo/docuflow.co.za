$(document).ready(function() {
	//Form Submit for IE Browser

	$(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#decline_invoice_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='decline_invoice_form']"));
            });

            $("#create_vat_report_submit").on('click', function(event){
               event.preventDefault();
               submitModalForm($("form[name='create_vat_report']"));
			});

			$("#add_invoice_comment_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='comment_invoice_form']"));
            });

			$("#add_state_role_submit").on('click', function (event) {
                submitModalForm($("form[name='state_role_form']"));
                return false;
            });

			$("#add_invoice_account_comment_submit").on('click', function (event) {
                submitModalForm($("form[name='comment_invoice_account_form']"));
                return false;
            });

			$("#send_email_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='email_supplier_form']"));
            });

			$("#park_invoice_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='park_invoice_form']"));
            });

			$("#add_invoice_image_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='add_invoice_image_form']"));
            });

			$("#change_invoice_status_submit").on('click', function(event){
				$("#text-reason").show();
				var element = $("#id_approval_comment");
				if (element.val() === '') {
					$(".has-error").remove()
					element.addClass('is-invalid');
					element.after('<div class="is-invalid-message has-error">Comment is required</div>');
					$(element.parent()).parent().addClass('text-danger');
					return false;
				} else {
					event.preventDefault();
					submitModalForm($("form[name='approve_invoice_decline_form']"));
				}
				return false;
			});

			$("#approve_invoice_submit").on('click', function(event){
				event.preventDefault();
				Invoice.approveInvoiceDecline($(this));
			});

			$("#reject_invoice_submit").on('click', function(event){
				event.preventDefault();
				Invoice.rejectInvoice($(this));
			});

			$("#distribute_invoice_accounts").on("click", function(){
				event.preventDefault();
				Invoice.distributeAccounts($(this));
			});

			$("#create_invoice_account_distribution_submit").on('click', function(event){
				submitModalForm($("#create_invoice_account_distribution"));
				return false;
			});

			$("#delete_invoice_account_distribution").on('click', function () {
				Invoice.send($(this))
            });

			$("#create_distribution_journal_submit").on('click', function (e) {
				submitModalForm($("#create_distribution_journals"));
				return false;
            });

			initDatePicker();

			initChosenSelect();

        });
    });

	$(".validate_number").on('keyup', function(){
		PurchaseOrder.validateNumber($(this));
		return false;
	});

	$(".calculate_net").on('blur', function(){
		PurchaseOrder.calculateRowNet($(this));
	});

	$(".calculate_vat").on('change', function(){
		var el = $(this)
		if (el.val() !== '') {
			PurchaseOrder.calculateRowVat(el);
		}
	});

	$(".inventory_item").on('change', function(){
		//PurchaseOrder.displayUnit($(this));
		//PurchaseOrder.notifyPreferredSupplier($(this))
		var itemRowId = $(this).data("row-id")
		PurchaseOrder.handleRow(itemRowId);
	});

	$("#cancel_purchase_order").on('click', function(e){
		ajaxGet($(this));
		e.preventDefault();
	});

	$("#email_purchase_order").on('click', function(e){
		ajaxGet($(this));
		e.preventDefault();
	});

	$("#sign_purchase_order").on('click', function(e){
		e.preventDefault();
		PurchaseOrder.signPurchaseOrder($(this));
	});

	$("#resubmit_purchase_order").on('click', function(e){
		PurchaseOrder.signPurchaseOrder($(this));
		e.preventDefault();
	});

    var purchaseOrderComments = $("#purchase_order_comments");
    var purchaseOrderContainer = $("#purchase_order_detail_container");
    var purchaseOrderLines = $("#purchase_orders");

	if (purchaseOrderLines.length > 0) {
		PurchaseOrder.loadPurchaserOrderLines(purchaseOrderLines);
		PurchaseOrder.loadPurchaserOrderForm($("#purchase_order_lines"));
	}
	if (purchaseOrderComments.length > 0) {
		PurchaseOrder.loadComments(purchaseOrderComments);
	}

	if (purchaseOrderContainer.length > 0) {
		PurchaseOrder.loadPurchaseOrderImage(purchaseOrderContainer)
	}

	$("#create_purchase_order_submit").on("click", function(e){
		ajaxSubmitForm($(this), $("#po-request"));
		return false;
	});

	var inventoryItemsContainer = $("#purchase_order_items");
	if (inventoryItemsContainer.length > 0) {
		var ajaxUrl = inventoryItemsContainer.data('ajax-url');
		PurchaseOrder.loadPurchaseOrderItems(ajaxUrl);
	}

	$("#add_line").on('click', function(e){
		e.preventDefault();
		var ajaxUrl = inventoryItemsContainer.data('ajax-url');
		if (ajaxUrl === undefined) {
			ajaxUrl = $(this).data('ajax-url');
		}
		$(this).prop('disabled', true);
		PurchaseOrder.loadPurchaseOrderItems(ajaxUrl);
	});

});

var PurchaseOrder = (function(purchaseOrder, $){

	return {

		loadPurchaseOrderItems: function(ajaxUrl)
		{
            var container = $("#purchase_order_items_placeholder");
			var rowId = 0;
			var lastRow = $(".purchase_order_row").last();
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
			$.get(ajaxUrl, {
				row: rowId
			}, function(html){
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				rowId = parseInt(rowId) + 1;

				$(".purchase_order_row").each(function(){
					var itemRowId = $(this).data("row-id");
					PurchaseOrder.handleRow(itemRowId);
				});

				$(".delete-row").on('click', function(){
					var itemRowId = $(this).data("row-id");
					$("#row_" + itemRowId).remove();
				});

				$("#add_line").prop('disabled', false);

			});
		},

		handleRow: function(rowId)
		{
			var chosenWidth = calculateChosenSelectWidth("purchase_order_row");
			console.log("With is ----- ", chosenWidth)
			var inventorySelect = $("#"+ rowId +"_inventory_item");
			var inventorySearchUrl = inventorySelect.data("ajax-url");
			var inventoryUrl = inventorySelect.data("ajax-inventory-url");
			inventorySelect.ajaxChosen({
				url: inventorySearchUrl,
				dataType: "json",
				width: chosenWidth + "px",
				allowClear: true,
				placeholder: "Select ..."
			}, {loadingImg: '/static/js/vendor/chosen/loading.gif'}).width(chosenWidth + "px");

			inventorySelect.on('change', function(){
				var inventoryId = $(this).val();
				$.get(inventoryUrl, {
					inventory_id: inventoryId
				}, function(inventoryData){
					var price = inventoryData['current_price'];
					var measureId = inventoryData['measure_id'];
					var unitId = inventoryData['unit_id'];
					var vatId = inventoryData['vat_code_id'];
					var vatCode = $("#" + rowId + "_vat_code");
					if (price !== '') {
						console.log( 'Price --> ', price, rowId  )
						$("#"+ rowId +"_value").val(price);
					}
					if (measureId !== undefined && measureId !== '') {
						var unitsUrl = inventoryData['units_url'];
						console.log( "load units for this measure --> ", unitsUrl );
						PurchaseOrder.loadUnits(rowId, measureId, unitsUrl, unitId);
					}
					if (vatId !== undefined && vatId !== '') {
						vatCode.val(vatId).trigger("chosen:updated");
					} else {
						vatCode.val('').trigger("chosen:updated");
					}
				});
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				// Recipe.calculateInventoryItemPrices(rowId);
			});

			$(".validate_number").on('keyup', function(e){
				PurchaseOrder.validateNumber($(this));
				return false;
			});
			$(".calculate_net").on('blur', function(e){
				PurchaseOrder.calculateRowNet($(this));
			});

			$(".calculate_vat").on('change', function(){
				PurchaseOrder.calculateRowVat($(this));
			});

			$(".inventory_item").on('change', function(){
				PurchaseOrder.notifyPreferredSupplier($(this))
				PurchaseOrder.loadUnits($(this));
			});

		},

        loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId == item.id) {
						html.push("<option value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#" + rowId + "_unit").html(html.join('')).trigger("chosen:updated");

			});
		},

		displayUnitdisplayUnit: function(el)
		{
			var rowId = el.attr('id').split('_')[0];
			var inventoryItemSelected = $("#" + el.attr('id') + " option:selected");
			var unit = inventoryItemSelected.data('unit');

			if (unit !== '') {
				$("#" + rowId + "_unit").val(unit);
			}
		},

		signPurchaseOrder: function(el)
		{

			var ajaxUrl = el.data('ajax-url');
			var ajaxSaveUrl = el.data('save-ajax-url');
			if (ajaxSaveUrl !== undefined) {
				var form = $("#edit_invoice");
				form.ajaxSubmit({
					url:  ajaxSaveUrl,
					target: '#loader',   // target element(s) to be updated with server response
					beforeSubmit:  function(formData, jqForm, options) {
						var queryString = $.param(formData);
					},  // pre-submit callback
					error: function (data) {
						var element;
						// show error for each element
						if (data.responseJSON && 'errors' in data.responseJSON) {
							$.each(data.responseJSON.errors, function (key, value) {
								element.addClass('is-invalid');
								element.after('<div class="is-invalid-message">' + value[0] + '</div>');
							});
						}

						// flash error message
						flash('danger', 'Errors have occurred.');
					},
					success:       function(responseJson, statusText, xhr, $form)  {
						$.getJSON(ajaxUrl, {
							csrf_token: window.csrftoken
						}, function(response) {
							if ('alert' in response) {
								if ('url' in response){
									ajaxNotificationDialog(response.url)
								} else {
									alertErrors(response.text)
								}
							} else {
								responseNotification(response)
							}
						});
					},  // post-submit callback
					dataType: 'json',
					type: 'post'
				});
			} else {
				$.getJSON(ajaxUrl, {
					csrf_token: window.csrftoken
				}, function(response) {
					if ('alert' in response) {
						if ('url' in response){
							ajaxNotificationDialog(response.url)
						} else {
							alertErrors(response.text)
						}
					} else {
						responseNotification(response)
					}
				});
			}
		},

		loadPurchaseOrderImage: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			$.get(ajaxUrl, function(html){
				var containerDiv = $("#purchase_order_detail_container");
				containerDiv.html(html);
			});
		},

		loadComments: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			$.get(ajaxUrl, function(html){
				el.html(html);
			});
		},

		loadPurchaserOrderLines: function(el) {
			var ajaxUrl = el.data('ajax-url');
			var purchaserOrderId = el.data('invoice-id');
			$.get(ajaxUrl, {
				invoice_id: purchaserOrderId,
			}, function(html){
				el.replaceWith(html);

				$(".purchase-order-line>td").on('click', function(e){
					var purchaserOrderId = $(this).parent().data('purchase-order-line-id');

					$("#add_purchase_order_line").attr('disabled', 'disabled');
					$("#update_purchase_order_line").removeAttr('disabled');
					PurchaseOrder.loadPurchaserOrderForm($(this).parent(), purchaserOrderId);
					return false;
				});

				$(".delete-purchase-order").on('click', function(){
					if (confirm('Are you sure you want to delete this purchase order line')) {
						PurchaseOrder.deletePurchaseOrderLine($(this));
					}
					return false;
				});
			});
		},

		loadPurchaserOrderForm: function(el, lineId)
		{
			if (el) {
                var ajaxUrl = el.data('ajax-url');
                var lineId = el.data('purchase-order-line-id');
                $.get(ajaxUrl, {
                    line_id: lineId
                }, function(html){
                   $("#purchase_order_lines").html(html);

                   var quantity = $("#id_quantity");
                   var unitValue = $("#id_order_value");
                   var vatValueEl = $("#id_vat_amount");
                   var poVatAmountEl = $("#po_vat_amount");
                   var netAmountEl = $("#net_amount");
                   var totalAmountEl = $("#total_amount");
                   var selectedVatCode = $("#id_vat_code option:selected");

                   quantity.on('blur', function(e){
                       var netAmount = PurchaseOrder.calculateNet(quantity.val(), unitValue.val());
                       var totalAmount = PurchaseOrder.calculateTotalAmount(quantity.val(), unitValue.val(), poVatAmountEl.val());
                       var vatAmount = PurchaseOrder.calculateVat(netAmount, selectedVatCode.data('percentage'));
                   	   netAmountEl.val(netAmount);
					   totalAmountEl.val(totalAmount);
					   poVatAmountEl.val(vatAmount)
				   });

                   unitValue.on('blur', function(e){
                       var netAmount = PurchaseOrder.calculateNet(quantity.val(), unitValue.val());
                   	   var totalAmount = PurchaseOrder.calculateTotalAmount(quantity.val(), unitValue.val(), vatValueEl.val());
                       var vatAmount = PurchaseOrder.calculateVat(netAmount, selectedVatCode.data('percentage'));
                   	   netAmountEl.val(netAmount);
					   totalAmountEl.val(totalAmount);
					   poVatAmountEl.val(vatAmount)
				   });

                   $("#id_vat_code").on('change', function(){
                   	   var ajaxUrl = $(this).data('calculate-line-amount');
                   	   var vatCode = $(this).val();
                   	   $.ajax({
						   url: ajaxUrl,
						   data: {
							   'vat_code': vatCode,
							   'unit_amount': unitValue.val(),
							   'quantity': quantity.val()
						   },
						   dataType: 'json'
					   }).done(function(responseJson){
						   if ('vat_amount' in responseJson) {
						   	  $("#vat_amount_text").text( responseJson.vat_amount );
						   	  vatValueEl.val( responseJson.vat_amount );
						   }
					   }).then(function(){
							var totalAmount = PurchaseOrder.calculateTotalAmount(quantity.val(), unitValue.val(), vatValueEl.val());
							totalAmountEl.val(totalAmount);
					   });
				   });


                    $("#update_purchase_order_line").on('click', function(e){
                    	e.stopImmediatePropagation();
                        submitForm($("#purchaser_order_form"));
                    })

                    $("#add_purchase_order_line").on("click", function(e){
                        e.stopImmediatePropagation();
                        PurchaseOrder.savePurchaseOrderLine($("#purchase_order_form"));
                        e.preventDefault()
                    });

                    $("#id_amount").on('blur', function(e){
                        PurchaseOrder.calculateAmount($(this));
                    });

                    $(".math-operation").on('blur', function(e){
                        PurchaseOrder.calculateAmount($(this));
                    });

                    $(".money").on('blur', function(e){
                        PurchaseOrder.makeMoney($(this));
                    });

                    $(".chosen-select").chosen();
                });
            }
		},

		savePurchaseOrderLine: function(form)
		{
			form = form || $("#purchase_order_form");
			// disable extra form submits
			form.addClass('submitted');
			var loader = $("#loader");
			loader.show();
			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					loader.hide();
					if (responseJson.error) {
						if (responseJson.hasOwnProperty('errors')) {
							highlightFormErrors(responseJson.errors)
						}
						alertErrors(responseJson.text)
					} else {
						form.find(":input:not('select')").each(function(){
							$(this).val('')
						})
						form.find(".chosen-select").val('').trigger("chosen:updated");
						responseNotification(responseJson);
						var purchaseOrderLines = $("#purchase_orders");
						if (purchaseOrderLines) {
							PurchaseOrder.loadPurchaserOrderForm($("#purchase_order_lines"))
							$("#update_invoice_account_changes").attr('disabled', 'disabled');
							$("#add_invoice_account_posting").removeAttr('disabled');
							PurchaseOrder.loadPurchaserOrderLines(purchaseOrderLines);
						}

					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		deletePurchaseOrderLine: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, {
			}, function(responseJson){
				responseNotification(responseJson);
			}, 'json');
		},

		validateNumber: function(el){
			var value = el.val();
			if (value !== '') {
				if (isNaN(value)) {
					el.val('');
				}
			}
		},

		calculateVatAmount: function(totalAmount, netAmount){
			var vatAmount = parseFloat(totalAmount) - parseFloat(netAmount)
			if (vatAmount > 0) {
				$("#id_vat_amount").val(parseFloat(vatAmount).toFixed(2))
			}
		},

		calculateRowVat: function(el) {
			var rowId = el.attr('id').split('_')[0];

			var netTotal = $("#" + rowId + "_net_total").val();
			var vatSelected = $("#" + el.attr('id') + " option:selected");
			var percentage = vatSelected.data('percentage');
			if (netTotal !== '' && percentage !== '') {
				var vatAmount = (parseFloat(percentage)/100) * parseFloat(netTotal);
				var vatAmountStr = accounting.formatMoney(vatAmount);
				$("#row_vat_amount_" + rowId).html(vatAmountStr);
				$("#" + rowId + "_vat_amount").val(vatAmount);
			}

			PurchaseOrder.calculateOrderValue(rowId);
		},

		calculateRowNet: function(el) {
			var rowId = el.attr('id').split('_')[0];

			var quantity = $("#" + rowId + "_quantity").val();
			var value = $("#" + rowId + "_value").val();
			console.log("Quantity entered is ", quantity)
			if (quantity !== '') {
				PurchaseOrder.validateMinOrderQuantity(rowId, quantity);
				PurchaseOrder.validateMaxOrderQuantity(rowId, quantity);

				if (value !== '') {
					var netValue = PurchaseOrder.calculateNet(quantity, value);
					var netText = accounting.formatMoney(netValue);

					$("#row_net_total_" + rowId).html(netText);
					$("#" + rowId + "_net_total").val(netValue);

					PurchaseOrder.calculateRowVat($("#" + rowId + "_vat_code"))
				}
			}

			PurchaseOrder.calculateOrderValue(rowId);
		},

		validateMinOrderQuantity: function(rowId, quantity)
		{
			var inventoryItem = $("#" + rowId + "_inventory_item  option:selected");
			console.log("Min order validation ", inventoryItem)
			if (inventoryItem.length > 0) {
				var minOrder = inventoryItem.data('min-order');
				console.log( minOrder )
				if (minOrder !== undefined) {
					var minOrder = parseInt(minOrder)
					var qty = parseInt(quantity)
					console.log(" min order ", minOrder, " and qty ", qty)
					if (qty < minOrder) {
						$("#" + rowId + "_quantity").val( minOrder )
						alert("You have entered quantity " + qty+ " which is below minimum order ("+ minOrder + "), " +
							"for this item. Please adjust");
						return false;
					}
				}
			}
		},

		validateMaxOrderQuantity: function(rowId, quantity)
		{
			var inventoryItem = $("#" + rowId + "_inventory_item  option:selected");
			console.log("Max order validation ", inventoryItem)
			if (inventoryItem.length > 0) {
				var maxOrder = inventoryItem.data('max-order');
				if (maxOrder !== undefined) {
					var maxOrder = parseInt(maxOrder);
					var qty = parseInt(quantity);
					console.log(" max order ", maxOrder, " and qty ", qty)
					if (qty > maxOrder) {
						$("#" + rowId + "_quantity").val( maxOrder )
						alert("You have entered quantity " + qty+ " which is above maximum order ("+ maxOrder + "), " +
							"for this item. Please adjust.");
						return false;
					}
				}
			}
		},


		notifyPreferredSupplier: function(el)
		{
			var rowId = el.attr('id').split('_')[0];
			var inventoryItem = $("#" + rowId + "_inventory_item option:selected");
			if (inventoryItem.length > 0) {
				var preferredSupplier = inventoryItem.data('preferred-supplier');
				var preferredSupplierId = inventoryItem.data('preferred-supplier-id');
				var selectedSupplier = $("#id_supplier option:selected")
				if (preferredSupplierId !== undefined && selectedSupplier !== undefined) {
					var selectedSupplierId = parseInt(selectedSupplier.val())
					preferredSupplierId = parseInt(preferredSupplierId)

					if (preferredSupplierId !== selectedSupplierId) {
						alert("Your preferred supplier is  " + preferredSupplier + " which is different from the selected supplier");
						return false;
					}
				}
			}
		},

		calculateVat: function(netAmount, vatPercentage)
		{
			if (netAmount !== '' && vatPercentage !== '') {
                try {
                    if (isNaN(vatPercentage)) {
                        throw new  Error('Vat not valid')
                    }
                    var totalVat = parseFloat(vatPercentage)/100 * parseFloat(netAmount);
                    return totalVat.toFixed(2);
                } catch (e) {
                    console.log('Net ', netAmount, ' and value ', vatPercentage, ' did not valid');
                }
            }
		},

		calculateNet: function(quantity, value)
		{
			if (quantity !== '' && value !== '') {
                try {
                    if (isNaN(quantity)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    if (isNaN(value)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    var totalNet = parseFloat(quantity) * parseFloat(value);
                    return totalNet.toFixed(2);
                } catch (e) {
                    console.log('Quantity ', quantity, ' and value ', value, ' did not valid');
                }
            }
		},

		calculateTotalAmount: function(quantity, value, vatAmount)
		{
			console.log('calculating total for quantity ', quantity, ' and value ', value, ' and vat amount', vatAmount)
			if (quantity !== '' && value !== '') {
                try {
                    if (isNaN(quantity)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    if (isNaN(value)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    var total = parseFloat(quantity) * parseFloat(value);

					if (vatAmount !== '') {
						total += parseFloat(vatAmount);
					}

                    return total.toFixed(2);
                } catch (e) {
                    console.log('Quantity ', quantity, ' and value ', value, ' and vat amount ', vatAmount,' did not valid.', e.message);
                }
            }
		},

		calculateOrderValue: function(rowId){
			var net = $("#" + rowId + "_net_total").val();
			var vat = $("#" + rowId + "_vat_amount").val();
			var total = 0;

			if (net !== '') {
				total += parseFloat(net);
			}
			if (vat !== '') {
				total += parseFloat(vat);
			}
			var totalText = accounting.formatMoney(total);
			$("#row_total_amount_" + rowId).html(totalText);
			$("#" + rowId + "_total").val(total);

			var po_total = PurchaseOrder.calculatePoTotal($(".total"));
			var po_vat = PurchaseOrder.calculatePoTotal($(".vat"));
			var po_net = PurchaseOrder.calculatePoTotal($(".net"));

			$("#total_net_value").html(po_net);
			$("#id_net_amount").val(accounting.toFixed(po_net, 2));

			$("#total_vat_value").html(po_vat);
			$("#id_vat_amount").val(accounting.toFixed(po_vat, 2));

			$("#total_order_value").html(po_total);
			$("#id_total_amount").val(accounting.toFixed(po_total, 2));
			$("#id_open_amount").val(accounting.toFixed(po_total, 2));
		},

		calculatePoTotal: function(el) {
			var total  = 0
			el.each(function(){
				if ($(this).val() !== '') {
					total += parseFloat($(this).val())
				}
			});
			return accounting.formatMoney(total);
		},

		addPurchaseOrderLine: function(el)
		{
			var self  = this;
			var ajaxUrl = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response);
				self.loadInvoiceHistory($("#invoice_comments"));
			});
		},

		savePurchaseOrderRequest: function(el)
		{
			var self = this;
			var flowAjaxUrl = $(el).data('ajax-url');
			var form = $("#logger_edit_invoice");
			var ajaxUrl = $("#logger_save_invoice_changes").data('ajax-url');
			form.ajaxSubmit({
				url: ajaxUrl,
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}
				},
				success:       function(responseJson, statusText, xhr, $form)
				{
					if (responseJson.error) {
						responseNotification(responseJson)
					} else {
						$.getJSON(flowAjaxUrl, {
							csrf_token: window.csrftoken
						}, function(response){
							if (response.error) {
								alertErrors(response.details)
								if(response.errors) {
									highlightFormErrors(response.errors)
								}
								//responseNotification(response)
							}
							if ('redirect' in response) {
								setTimeout(function(){
									document.location.href = response.redirect
								}, 1500)
							}
						});
					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		},

		validateInvoice: function(el)
		{
			var self 	  = this;
			var ajaxUrl   = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response)
			});
		},


		addToFlow: function(el)
		{
			var self 	 = this;
			var ajaxUrl  = $(el).data('ajax-url');
			var invoices = [];
			$('.invoice-check').each(function(index){
				if ($(this).is(":checked")) {
					invoices.push( $(this).val() )
				}
			});
			if (invoices.length > 0) {
				$.getJSON(ajaxUrl, {
					csrf_token: window.csrftoken,
					invoices: invoices.join(',')
				}, function(responseJson){
					responseNotification(responseJson)
				});
			} else {
				responseNotification({'error': true, 'text': 'Select invoices to add to flow'})
			}
		}

	}
}(PurchaseOrder || {}, jQuery))
