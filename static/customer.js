$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");
			initDatePicker();

			initChosenSelect();

			var savePricingBtn = $("#save_pricing");
			var saveBtn = $(".save-btn");

			savePricingBtn.on("click", function(){
				var pricingForm = $("#pricing_form");
				pricingForm.LoadingOverlay('show', {
					'text': 'Processing pricing'
				});
				ajaxSubmitForm(savePricingBtn, pricingForm);
			});

			saveBtn.on("click", function(e){
				console.log("id --> ", saveBtn.attr('id') )
				var submitForm = $("#" + saveBtn.attr('id') + "_form");
				var text = saveBtn.data('text');
				console.log(text);
				console.log("\r\n\n\n---------------\r\n\n");
				submitForm.LoadingOverlay('show', {
					'text': text
				});
				ajaxSubmitForm(saveBtn, submitForm);
			});

        });
    });

	$("#customers>tbody>tr>td:not(.actions)").on("click", function(){
		var detailUrl = $(this).parent("tr").data("detail-url");
		document.location.href = detailUrl;
		return false;
	});


	$("#save_customer").on("click", function(e){
		ajaxSubmitForm($(this), $("#customer_form"));
		return false;
	});

	var paymentTerm = $("#id_payment_term");
	var statementDays = $("#statement_days");
	var invoiceDays = $("#invoice_days");
	var idDays = $("#id_days");
	var idStatementDays = $("#id_statement_days");
	paymentTerm.on("change", function(){
		var paymentTermVal = $(this).val();
		console.log("Value --> ", paymentTermVal, ' type --> ', typeof paymentTermVal);
		if (paymentTermVal == 1) {
			invoiceDays.removeClass('hide').addClass('show');
			statementDays.removeClass('show').addClass('hide');
		} else {
			invoiceDays.removeClass('show').addClass('hide');
			statementDays.removeClass('hide').addClass('show');
		}
	});
	if (idDays.is(":hidden")) {
		idStatementDays.val(idDays.val());
	}
	idStatementDays.on("change", function(){
		idDays.val($(this).val());
	});

	var customerCreditLimitEl = $("#id_credit_limit");
	customerCreditLimitEl.on("blur", function(){
		Customer.calculateAvailableCredit($(this));
	});

});


var Customer = (function(customer, $){

	return {

		calculateAvailableCredit: function (el) {
			console.log("----------calculateAvailableCredit-------------------");
			var creditCart = accounting.unformat(el.val());
			var totalInvoiced = accounting.unformat($("#total_invoiced").val());
			var availableCredit = accounting.unformat($("#available_credit").val());

			var available = creditCart - totalInvoiced;
			$("#available_credit").html( accounting.toFixed(available, 0));
			$("#id_available_credit").val( accounting.toFixed(available, 0))
		}
	}

}(Customer || {}, jQuery))

