$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			$("#create_month_close_submit").on('click', function (event) {
                event.preventDefault();
                var form = $($(this).parent()).parent()
                submitModalForm(form);
            });
			$(".chosen-select").chosen();

        });
    });


    $(".accounts").on('change', function(e){
        var rowId = $(this).attr('id').replace('account_', '');
        Journal.loadAccountVatCodes($(this), $(this).val(), rowId);

        var accountType = $(this).find(':selected').data('account-type')
        if (accountType === 1) {
            $("#distribution_" + rowId).prop('disabled', false)
        } else {
            $("#distribution_" + rowId).prop('disabled', true)
        }
    })

    $(".vat_codes").on('change', function(e){
        var rowId = $(this).attr('id').replace('vat_codes_', '');
        var description = $(this).data('description');
        $("#description_" + rowId).html(description);
    })

    $(".credit_amount").on('blur', function(){
        var rowId = $(this).attr('id').replace('credit_', '');
        if ($(this).val() !== '') {
            $("#debit_" + rowId).prop('disabled', true);
            Journal.calculateTotalCredit();
        }
    })

    $(".debit_amount").on('blur', function(){
        var rowId = $(this).attr('id').replace('debit_', '');
        if ($(this).val() !== '') {
            var amount = parseFloat( $(this).val() );
            if (amount < 0) {
                $("#credit_" + rowId).val(amount * -1);
                $("#debit_" + rowId).prop('disabled', true);
                $(this).val('');
                Journal.calculateTotalCredit();
            } else {
                 $("#credit_" + rowId).prop('disabled', true);
                Journal.calculateTotalDebit();
            }
        }
    });

    $(".distributions").on('change', function(){
        if ($(this).val() === 'yes') {
            var rowId = $(this).attr('id').replace('distribution_', '');
           Journal.openDistributionModal($(this), rowId);
        }
    });

    $(".delete_journal_line").on('click', function(e){
        e.preventDefault();
        if (confirm('Are you sure you want to delete this line')) {
            Journal.removeJournalLine($(this));
        }
    });

    $(".delete_journal").on('click', function(e){
        e.preventDefault();
        if (confirm('Are you sure you want to delete this journal')) {
            Journal.deleteJournal($(this));
        }
    });

    $(".journal-datepicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat : 'yy-mm-dd',
            onSelect: function( selectedDate ) {
            },
            onClose: function( t, i) {
                Journal.validatePeriodAndDate();
            }

        });

});


var Journal = (function(journal, $){

	return {

	    validatePeriodAndDate: function()
        {
            var ajaxUrl =  $("#validate_journal_period").data('validate-period-ajax-url')
            var journalDate = $("#id_date").val();
            var period = $("#id_period").val();
            $.getJSON(ajaxUrl, {
                journal_date: journalDate,
                period: period
            }, function(responseJson){
                responseNotification(responseJson);
                var isDisabled = ($("#submit").attr('disabled') === 'disabled');
                var isPeriodIssue = $("#submit").data('invalid-period')

                if (!isDisabled) {
                    if (responseJson.error) {
                        $("#submit").prop('disabled', true);
                        $("#submit").data('invalid-period', 'yes');
                    } else {
                        $("#submit").prop('disabled', false);
                    }
                } else {
                    if (isPeriodIssue == 'yes') {
                     $("#submit").prop('disabled', false);
                    }
                }
            })
        },

	    loadAccountVatCodes: function(el, accountId, rowId)
        {
            var ajaxUrl =  el.data('vat-codes-ajax-url')
            $.getJSON(ajaxUrl, {
                account_id: accountId
            }, function(vat_codes){
                var html = [];
                html.push("<select class='chosen-select vat-codes' name='vat_code_"+ rowId +"' id='vat_code_"+ rowId +"' style='max-width: 100px !important; min-width: 100px !important;' >");
                $.each(vat_codes, function(index, vat_code){

                    html.push("<option value='"+ vat_code.id +"'>" + vat_code.label + "</option>")
                })
                html.push("</select>")
                $("#vat_codes_cell_" + rowId).html(html.join(' '));

                $("#vat_code_" + rowId).chosen().css({'min-width': '100px !important', 'max-width': '100px !important'});
            })
        },

	    calculateTotalCredit: function()
        {
	        var totalCredit = 0;
            $(".credit_amount").each(function(){
                var amount = parseFloat($(this).val());
                if (amount > 0) {
                    totalCredit += parseFloat($(this).val());
                }
            })
            var totalCreditStr = accounting.formatMoney(totalCredit);
            totalCredit = totalCredit.toFixed(2);
            $("#total_credit").val(totalCredit);
            $("#credit_total").html(totalCreditStr);

            Journal.calculateDifference();
        },

        calculateTotalDebit: function () {
	        var totalDebit = 0;
            $(".debit_amount").each(function(){
                var amount = parseFloat($(this).val());
                if (amount > 0) {
                    totalDebit += amount;
                }
            });
            var totalDebitStr = accounting.formatMoney(totalDebit);
            totalDebit = totalDebit.toFixed(2);
            $("#total_debit").val(totalDebit);
            $("#debit_total").html(totalDebitStr);

            Journal.calculateDifference();
        },

        calculateDifference: function()
        {
            var totalDebit = parseFloat($("#total_debit").val());
            var totalCredit = parseFloat($("#total_credit").val());
            var difference = 0;
            var submitBtn = $("#submit");

            if (totalCredit || totalDebit) {
                difference = totalDebit - totalCredit;

            }
            if (difference == 0) {
                submitBtn.prop('disabled', false);
            } else {
                submitBtn.prop('disabled', true);
            }
            var differenceStr = accounting.formatMoney(difference);
            $("#difference").html(differenceStr);
        },

        openDistributionModal: function(el, rowId)
        {

            var accountId = $("#account_" + rowId).val()
            var debitAmount = $("#debit_" + rowId).val()
            var creditAmount = $("#credit_" + rowId).val()
            console.log("Total debit ", debitAmount, " credit ", creditAmount, " difference ", accountId)
            if (accountId == '') {
                alert('Please select the account')
                return false
            } else if(debitAmount == '' && creditAmount == '') {
                alert('Please select the amount')
                return false
            } else {
                var modalUrl = el.data('ajax-url')
                var amount = debitAmount || creditAmount
                $.get(modalUrl, {
                    credit_amount: creditAmount,
                    debit_amount: debitAmount,
                    account_id: accountId,
                    amount: amount
                }, function (data) {
                    $(data).modal('show');

                    $("#distribute_journal_accounts").on("click", function(){
                        event.preventDefault();
                        Journal.distributeAccounts($(this));
                    });

                    $("#create_journal_distribution_submit").on('click', function(event){
                        submitModalForm($("#create_journal_distribution"));
                        return false;
                    });
                    var date = new Date();
                    // datepicker on all date input with class datepicker
                    $(".datepicker").datepicker({
                        dateFormat: 'yy-mm-dd'
                    });

                    $(".history_datepicker").datepicker({
                        changeMonth		: true,
                        changeYear		: true,
                        dateFormat 		: "dd-M-yy",
                        maxDate         : -1,
                        yearRange       : "1900:"+(date.getFullYear())
                    });

                    $(".future_datepicker").datepicker({
                        changeMonth		: true,
                        changeYear		: true,
                        dateFormat 		: "dd-M-yy",
                        minDate         : +1,
                        yearRange       : "-30:+100"
                    });

                    $(".chosen-select").chosen();

                });
            }
        },

		distributeAccounts: function(el)
		{
		   var accountIds = [];
		   $(".distribution-invoice-account").each(function(){
		   		if ($(this).is(":checked")) {
		   			accountIds.push($(this).val());
				}
		   });

		   var startDate = $("#id_start_date");
		   var nbMonth = $("#id_month");
		   var amount = $("#amount").val()
		   if (accountIds.length === 0) {
			 alert("Please select the accounts")
		   } else if(!startDate.val()) {
			  alert("Please select start date")
		   } else if(!nbMonth.val()) {
			  alert("Please enter number of months")
		   } else {
			   var self = $("#" + el.attr('id'));
			   var container = $("#account_distribution");
			   container.html("Loading . . .");
			   var url = el.data('ajax-url');
			   $.get(url, {
				 months: nbMonth.val(),
				 start_date: startDate.val(),
				 account_ids: accountIds.join(','),
                 amount: amount
			   }, function(html){
				   container.html(html);
			   });
		   }
		},

        deleteJournalLine: function(el)
        {
            var ajaxUrl = el.data('ajax-url');
            var reference = el.attr('id').replace('delete_line_', '');
            $.getJSON(ajaxUrl, function(responseJson){
               responseNotification(responseJson);
               $("#journal_line_row_" + reference).remove();
            });
        },

        removeJournalLine: function(el)
        {
           var reference = el.attr('id').replace('delete_line_', '');
           $("#journal_line_row_" + reference).remove();
           Journal.calculateTotalCredit()
           Journal.calculateTotalDebit()
        },

        deleteJournal: function(el)
        {
            var ajaxUrl = el.data('ajax-url');
            $.getJSON(ajaxUrl, function(responseJson){
               responseNotification(responseJson);
            });
        }


	}

}(Journal || {}, jQuery))

