$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");
            initDatePicker();
			initChosenSelect();

			$("#id_override_key").on("keyup", function(e){
				Sales.validateOverrideKey($(this));
            });

			$("#override_continue").on("click", function(e){
				e.preventDefault();
            	var formId = $("#complete_draft_form");
            	formId.LoadingOverlay("show", {
					text        : "Processing ...",
					progress    : true
				});
                submitModalForm($("form[name='complete_draft_form']"));
			});

        });
    });

	$("#sales_documents>tbody>tr>td:not(.actions)").on("click", function(){
		var detailUrl = $(this).parent("tr").data("detail-url");
		if (detailUrl !== undefined && detailUrl !== "") {
			document.location.href = detailUrl;
		}
		return false;
	});

	$(".save-invoice").on("click", function(e){
		ajaxSubmitForm($(this), $("#sales_invoice_form"));
		return false;
	});


	$("#save_cash_sales_invoice").on("click", function(e){
		ajaxSubmitForm($(this), $("#create_sales_invoice_form"));
		return false;
	});

	var salesInvoiceBody = $("#invoice_items_body");
	if (salesInvoiceBody.length > 0) {
		var ajaxUrl = salesInvoiceBody.data("ajax-url");
		Sales.loadInvoiceItems(salesInvoiceBody, ajaxUrl);
	}

	var addInvoiceItemLine = $("#add_invoice_item_line");
	addInvoiceItemLine.on('click', function(){
		$(this).prop('disabled', true).data('html', addInvoiceItemLine.text()).text('loading ...');
		var ajaxUrl = addInvoiceItemLine.data('ajax-url');
		Sales.loadInvoiceItems(addInvoiceItemLine, ajaxUrl);
	});

	$(".bulk-action").on('click', function(e){
		e.preventDefault();
		Sales.bulkInvoicesAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		Sales.singleInvoiceAction($(this));
	});

	$(".action").on('click', function(e){
		e.preventDefault();
		Sales.invoiceAction($(this));
	});


	var idCustomer = $("#id_customer");
	idCustomer.on('change', function(e){
		e.preventDefault();
		Sales.loadCustomerDetails(idCustomer);
	});

	var idDocumentDate = $("#id_invoice_date");
	idDocumentDate.on('blur', function(e){
		e.preventDefault();
		Sales.calculateDueDate(idDocumentDate);
	});

	var idVatNumber = $("#id_vat_number");
	idVatNumber.on('blur', function(e){
		e.preventDefault();
		Sales.updateVatNumber(idVatNumber);
	});

	var idEstimate = $("#id_estimate");
	idEstimate.on('change', function(e){
		Sales.loadEstimateItems(e)
	});

	$("#view_invoice_payment").on("click", function(e){
	    e.preventDefault();
	    Sales.loadPayment($(this));
    });

	var createUpdate = $(".create-update");
	createUpdate.on('click', function(e){
	   e.preventDefault();
	   Sales.createUpdate($(this));
	});


});

var Sales = (function(sales, $){

	return {

		validateOverrideKey: function(el)
		{
			var overrideKey = el.val();
			if (overrideKey.length > 2) {
				var ajaxUrl = el.data('validate-url');
				var resultEl = $("#override_result");
				resultEl.html("validating ...");
				$.get(ajaxUrl, {
					override_key: overrideKey
				}, function(response) {
					if (response.error) {
						resultEl.addClass('loadn').addClass('ui-state-error-text').html(response.text);
					} else {
						resultEl.addClass('ui-state').addClass('ui-state-ok-text').html(response.text);
						$("#override_continue").prop('disabled', false);
					}
				}, 'json');
			}
		},

		createUpdate: function(el) {
		   var self = $("#" + el.attr('id'));
		   var text = el.text();
		   self.html("Processing ...");
		   var url = el.data('ajax-url');
		   ajaxSubmitForm(el, $("#create_invoice_update"));
		   // $.get(url, {
			//  period: $("#period").val()
		   // }, function(response){
		   // 	   if (response.error) {
		   // 	   	  self.html(text);
			//    }
			//    responseNotification(response);
		   // }, 'json');
		},


	    loadPayment: function()
        {

        },

		loadEstimateItems: function(el)
		{
			var rowId = 0;
			var estimateItemsUrl = $("#id_estimate option:selected").data('estimate-url');
			var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
			var container = $("#invoice_items_body");
			$.get(estimateItemsUrl, {
				row_id:rowId
			}, function(html) {
				container.replaceWith(html);
				console.log( "Row --> ", rowId);
				Sales.handleRow(rowId);
				if (chosenWidth > 0) {
					$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
					$(".line-type").chosen({'width': '40px'});
				} else {
					$(".chosen-select").not('.line-type').chosen({'width': '100px'});
					$(".line-type").chosen({'width': '40px'});
				}
			});
		},

		updateVatNumber: function(el)
		{
			var customerVatNumber = $("#id_customer option:selected").data('vat-number');
			var customerId = $("#id_customer option:selected").val();
			if (customerId !== undefined && customerId !== "") {
				if (el.val() !== "" && customerVatNumber !== el.val()) {
					var ajaxUrl = el.data('update-vat-url');
					$.post(ajaxUrl, {
						vat_number: el.val(),
						customer_id: customerId
					}, function(response){
						console.log( response );
					});
				}
			}
		},

		calculateDueDate: function(el)
		{
			var invoiceDate = new Date(el.val());
			var paymentTerm = $("#id_customer option:selected").data('payment-term');
			var paymentTermDays = $("#id_customer option:selected").data('payment-days');
			console.log('Invoice date, -- payment term and payment days');
			console.log(invoiceDate)
			console.log(paymentTerm)
			console.log(paymentTermDays)
			console.log(typeof paymentTermDays)
			if (paymentTerm === 1) {
				var dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
				console.log(dueDate.format('YYYY-MM-DD'))
				$("#id_due_date").val(dueDate.format('YYYY-MM-DD'));
			} else if (paymentTerm === 2) {
				var dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
				console.log(dueDate.format('YYYY-MM-DD'))
				$("#id_due_date").val(dueDate.format('YYYY-MM-DD'));
			}
		},

		loadCustomerDetails: function(el)
		{
			console.log('Load customer details');
			var ajaxUrl = $("#id_customer option:selected").data('detail-url');
			var vatNumber = $("#id_customer option:selected").data('detail-url');
			console.log( el );
			console.log( ajaxUrl );
			$.getJSON(ajaxUrl, function(response){
				var postalAddressHtml = [];
				var deliveryAddressHtml = [];
				console.log( response.postal )

				if (response.hasOwnProperty('postal')) {
					var postalAddress;
					console.log(typeof response.postal);
					if (typeof response.postal === 'string') {
						postalAddress = JSON.parse(response.postal);
					} else {
						postalAddress = response.postal;
					}
					$("#postal_street").html("<p class='mb-1'>"+postalAddress.address+"</p>");
					$("#postal_city").html("<p class='mb-1'>"+postalAddress.city+"</p>");
					$("#postal_province").html("<p class='mb-1'>"+postalAddress.province+"</p>");
					$("#postal_country").html("<p class='mb-1'>"+postalAddress.country+"</p>");
				}
				if (response.hasOwnProperty('delivery')) {
					var deliveryAddress;
					console.log(typeof response.delivery)
					if (typeof response.delivery === 'string') {
						deliveryAddress = JSON.parse(response.delivery);
					} else {
						deliveryAddress = response.delivery;
					}
					$("#delivery_street").html("<p class='mb-1'>"+deliveryAddress.address+"</p>");
					$("#delivery_city").html("<p class='mb-1'>"+deliveryAddress.city+"</p>");
					$("#delivery_province").html("<p class='mb-1'>"+deliveryAddress.province+"</p>");
					$("#delivery_country").html("<p class='mb-1'>"+deliveryAddress.country+"</p>");
				}
				if (response.hasOwnProperty('discount_terms')) {
					var discountTerms = response.discount_terms;
					if (typeof response.discount_terms === 'string') {
						discountTerms = JSON.parse(response.discount_terms);
					} else {
						discountTerms = response.discount_terms;
					}
					if (discountTerms.hasOwnProperty('line')) {
						if (discountTerms.line.discount && discountTerms.line.is_discounted)
						$("#line_discount").val(discountTerms.line.discount);
						$(".discount").each(function(){
							if ($(this).val() === '') {
								$(this).val(discountTerms.line.discount)
							}
						});
					}
					if (discountTerms.hasOwnProperty('invoice')) {
						if (discountTerms.invoice.discount && discountTerms.invoice.is_discounted) {
                            $("#invoice_discount").val(discountTerms.invoice.discount);
                        }
					}
				}
				if (postalAddressHtml.length > 0) {
					$("#postal").html(postalAddressHtml.join(' '))
				}
				if (deliveryAddressHtml.length > 0) {
					$("#delivery").html(deliveryAddressHtml.join(' '))
				}

				if (response.hasOwnProperty('vat_number')) {
					$("#id_vat_number").val(response.vat_number)
				}

			});
		},

		getSelectedInvoices: function()
		{
			var invoices = []
			$(".invoice-action").each(function(){
				if ($(this).is(":checked")) {
                    invoices.push($(this).data('invoice-id'));
                }
			});
			return invoices;
		},

		bulkInvoicesAction: function(el)
		{
			var invoices = Sales.getSelectedInvoices();
			var totalInvoices = invoices.length;
			if (totalInvoices === 0) {
				alert('Please select at lease one invoice');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( invoices )
				$.post(ajaxUrl, {
					invoices: invoices
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singleInvoiceAction: function(el)
		{
			var invoices = Sales.getSelectedInvoices();
			var totalInvoices = invoices.length;
			if (totalInvoices === 0) {
				alert('Please select at lease one invoice');
			} else if (totalInvoices >1) {
				alert('Please select one invoice to action');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( invoices );
				$.post(ajaxUrl, {
					invoices: invoices
				}, function(response){
					responseNotification(response);
				});
			}
		},

		invoiceAction: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			$.post(ajaxUrl, function(response){
				responseNotification(response);
			});
		},

		reloadInvoiceItemRow: function(el)
		{
			var rowId = el.data('row-id');
			var ajaxUrl = el.data('ajax-url');
			var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
			$.get(ajaxUrl, {
				row_id:rowId,
				line_type: $("#type_"+ rowId +" option:selected").val()
			}, function(html) {
				$("#invoice_item_row_" + rowId).replaceWith(html);

				Sales.handleRow(rowId)

				if (chosenWidth > 0) {
					if ($(".chosen-select").hasClass('line-type"')) {
						$(".chosen-select").chosen({'width': '14px'});
					} else {
						$(".chosen-select").chosen({'width': chosenWidth + "px"});
					}
				} else {
					if ($(".chosen-select").hasClass('line-type"')) {
						$(".chosen-select").chosen({'width': '14px'});
					} else {
						$(".chosen-select").chosen({'width': '100px'});
					}
				}
			});

		},

		loadInvoiceItems: function(el, ajaxUrl)
		{
			var rowId = 0;
			var lastRow = $(".invoice_item_row").last();
			if (lastRow.length > 0) {
				console.log("Row was");
				console.log( lastRow.data('row-id') );
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			Sales.loadInvoiceItemRow(el, ajaxUrl, rowId, lastRow)
		},

		loadInvoiceItemRow: function(el, ajaxUrl, rowId, lastRow)
		{
			var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
			var container = $("#invoice_items_placeholder");
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow !== undefined && lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				var _lastRow = $(".invoice_item_row").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}
				console.log( "Row --> ", rowId);

				Sales.handleRow(rowId);

				if (chosenWidth > 0) {
					$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
					$(".line-type").chosen({'width': '40px'});
				} else {
					$(".chosen-select").not('.line-type').chosen({'width': '100px'});
					$(".line-type").chosen({'width': '40px'});
				}
				el.prop('disabled', false).text(el.data('html'));
			});
		},

		handleRow: function(rowId)
		{
			var lineDiscount = $("#line_discount");
			if (lineDiscount.val() !== '') {
				$("#discount_percentage_" + rowId).val(lineDiscount.val());
			}

			$("#inventory_item_" + rowId).on('change', function(){
				var customerLineDiscount = $("#line_discount").val();
				console.log( "Row --> ", rowId, " and load fields")
				var selectedOption = $("#inventory_item_"+ rowId +" option:selected");
				var price = selectedOption.data('price');
				var priceIncluding = selectedOption.data('price-including');
				var unit = selectedOption.data('unit');
				var binLocation = selectedOption.data('bin-location');
				var description = selectedOption.data('description');
				var vatId = selectedOption.data('vat-id');
				var availableStock = selectedOption.data('stock');
				var unitId = selectedOption.data('unit-id');
				var isDiscountable = selectedOption.data('is-discountable');
				var priceEl =  $("#price_" + rowId);
				var priceIncEl =  $("#price_including_" + rowId);

				console.log( "price --> ", price, ' add to ', $("#price_" + rowId ));
				console.log( "price including--> ", priceIncluding, ' add to ', priceIncEl);
				console.log( "unit --> ", unit, ' add to ', $("#unit_" + rowId ));
				console.log( "is discountable --> ", isDiscountable, ' disable discount field ', typeof isDiscountable);
				priceEl.val( price );
				priceIncEl.val( priceIncluding );
				if (unit !== undefined && unit !== '') {
					$("#unit_" + rowId).val(unit);
				}
				if (unitId !== undefined && unitId !== '') {
					$("#unit_id_" + rowId).val(unitId);
				}

				if (binLocation !== undefined && binLocation !== '') {
					$("#bin_location_" + rowId).val(binLocation)
				}
				if (description !== undefined && description !== '') {
					$("#description_" + rowId).val(description)
				}
				if (vatId !== undefined && vatId !== '') {
					$("#vat_code_" + rowId).val(vatId).trigger("chosen:updated").trigger('change');
				} else {
					$("#vat_code_" + rowId).val('').trigger("chosen:updated").trigger('change');
				}
                Sales.handleRowDiscount(rowId, isDiscountable, customerLineDiscount);
				availableStock = parseFloat(availableStock);
				if (availableStock > 0) {
					$("#order_item_" + rowId).html('No');
				} else {
					$("#order_item_" + rowId).html('Yes');
				}
				$("#available_stock_" + rowId).html(accounting.formatMoney(availableStock));
			});

			$(".line-type").on('change', function(e){
				Sales.reloadInvoiceItemRow($(this));
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				Sales.calculateInventoryItemPrices(rowId);
			});

			$("#price_"+ rowId +"").on('blur', function(){
				Sales.calculateInventoryItemPrices(rowId);
			});

			$("#vat_code_"+ rowId).on('change', function(){
				var selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
				var vatPercentage = selectedVatOption.data('percentage');
				console.log('Vat code changes --> ', vatPercentage);
				if (vatPercentage !== undefined && vatPercentage !== '') {
					$("#vat_percentage_"+ rowId).val(vatPercentage);
				} else {
					$("#vat_percentage_"+ rowId).val(0);
				}
				Sales.calculateInventoryItemPrices(rowId);
			});

			$("#discount_percentage_"+ rowId +"").on('blur', function(){
				Sales.calculateInventoryItemPrices(rowId);
			});

			Sales.calculateInventoryItemPrices(rowId);
		},

		handleRowDiscount: function(rowId, isDiscountable, lineDiscount)
        {
        	console.log($("#line_discount"));
			console.log($("#line_discount").val());
            console.log("Handling row discount, is discountable -> ", isDiscountable, typeof isDiscountable, lineDiscount );
            var discountPercentageEl = $("#discount_percentage_" + rowId);
            if (isDiscountable) {
                var discountVal = lineDiscount || 0;
                console.log("Now discount value is ", lineDiscount)
                discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
            } else {
                discountPercentageEl.val(0).prop('disabled', true).trigger('change');
                $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
                $("#discount_amount_" + rowId).val('0.00').trigger('change');
            }
        },

		calculateInventoryItemPrices: function(rowId)
		{
			console.log("CALCULATING INVOICE INVENTORY ROW ", rowId + " ---------------------------- ");
			var price = $("#price_" + rowId);
			var priceIncl = $("#price_including_" + rowId);
			var ordered = $("#quantity_ordered_" + rowId);
			var received = $("#quantity_" + rowId);
			var short = $("#quantity_short_" + rowId);
			var vat = $("#vat_amount_" + rowId);
			var vatPercentage = $("#vat_percentage_" + rowId);
			var discountPercentage = $("#discount_percentage_" + rowId);
			var discount = $("#discount_" + rowId);

			var quantityOrdered = 0;
			var shortQuantity = 0;
			var quantityReceived = 0;
			var vatAmount = 0;
			var priceExcl = 0;
			var discountAmount = 0;
			var totalVat = 0;
            var totalDiscount = 0;
            var priceIncludingAmount = 0;
            var totalInc = 0;
            var totalExcl = 0;
            var subTotal = 0;

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}
			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}

			if (quantityOrdered > 0) {
				if (quantityReceived <= quantityOrdered) {
					shortQuantity =  quantityOrdered - quantityReceived
				} else {
					received.val(quantityOrdered);
					//backOrder.prop('disabled', true);
					alert('Please enter valid received quantity')
					return false;
				}
			}
			if (short !== undefined  && short.length > 0) {
                short.val( shortQuantity );
            }
			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}
			subTotal = priceExcl * quantityReceived;
			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentage/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                }
            }
			priceIncludingAmount = priceExcl + vatAmount;

			if (discountPercentage.length > 0) {
                if (discountPercentage.val() !== '' && priceExcl !== '') {
                    var discPercentage = parseFloat(discountPercentage.val());
                    console.log('Discount percentage --> ', discPercentage);
                    discountAmount = (discPercentage/100) * priceExcl;
                    console.log('Discount amount --> ', discountAmount);
                    discount.val(discountAmount)
                }
            }
            console.log("Price excluding, not discount ", priceExcl);

            console.log("Price including amount, not discount ", priceIncludingAmount);
            if (discountAmount > 0) {
			    priceExcl = priceExcl - discountAmount
            }

			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentage/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                    vat.val(vatAmount)
                }
            }

			// if (vat !== undefined && vat.length > 0) {
			// 	if (vat.val() !== '') {
			//
			// 		vatAmount += parseFloat(accounting.unformat(vat.val()));
			// 	}
			// }

			if (discountAmount !== '') {
			    totalDiscount = discountAmount * quantityReceived;
            }
            totalExcl = priceExcl * quantityReceived;
			totalInc = totalExcl;
            if (vatAmount !== '') {
            	console.log("Vat amount", vatAmount);
            	console.log("Price including + Vat amount", priceIncludingAmount, vatAmount);
			    totalVat = vatAmount * quantityReceived;
			    totalInc += totalVat;
            }
			//var totalInc = priceIncludingAmount * quantityReceived;
			console.log( " quantityReceived ", quantityReceived );
			console.log( " Price EXCL", priceExcl );
			console.log( " totalExcl ", totalExcl );
			console.log( " vatAmount ", vatAmount );
			console.log( " discountAmount ", discountAmount );
			console.log( " priceIncl ", priceIncludingAmount);
			console.log( " totalInc", totalInc );

			// if (backOrder !== undefined) {
			// 	if (shortQuantity === 0) {
			// 		backOrder.prop('disabled', true);
			// 	} else {
			// 		backOrder.prop('disabled', false);
			// 	}
			// }

			$("#vat_total_" + rowId).html( accounting.formatMoney(totalVat) );
			$("#total_vat_" + rowId).val( totalVat );

			$("#discount_amount_" + rowId).html( accounting.formatMoney(totalDiscount) );
			$("#total_discount_" + rowId).val(totalDiscount);

			$("#price_incl_" + rowId).html( accounting.formatMoney(priceIncl) );
			$("#sub_total_" + rowId).val( subTotal );

			priceIncl.val( priceIncludingAmount );

			$("#total_price_exl_" + rowId).html( accounting.formatMoney(totalExcl) );
			$("#total_price_excluding_" + rowId).val( totalExcl );

			$("#total_price_incl_" + rowId).html( accounting.formatMoney(totalInc) );
			$("#total_price_including_" + rowId).val( totalInc );

			Sales.calculateTotals()
		},

		calculateTotals: function()
		{
			var totalOrdered = 0;
			var totalReceived = 0;
			var totalShort = 0;
			var priceExc = 0;
			var priceInc = 0;
			var totalPrice = 0;
			var totalTotalExc = 0;
			var totalTotalInc = 0;
			var totalVatAmount = 0;
			var totalDiscountAmount = 0;
			var subTotalAmount = 0;
			var netTotal = 0;
			var invoiceDiscountAmount = 0;

			$(".ordered").each(function(){
				totalOrdered += parseInt($(this).val());
			});

			$(".received, .quantity").each(function(){
				totalReceived += parseInt($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseInt($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-vat-amount").each(function(){
				totalVatAmount += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-discount").each(function(){
				totalDiscountAmount += parseFloat(accounting.unformat($(this).val()));
			});
			$(".sub-total").each(function(){
				subTotalAmount += parseFloat(accounting.unformat($(this).val()));
			});
			totalDiscountAmount = totalDiscountAmount * -1;
			netTotal = subTotalAmount + totalDiscountAmount;
			var invoiceDiscount = $("#invoice_discount").val();
			if (invoiceDiscount !== undefined && invoiceDiscount !== '') {
				$("#customer_discounts").show();
				if (netTotal !== '') {
					var totalInvoiceDiscount = ((parseFloat(invoiceDiscount)/100) * netTotal) * -1;
					var defaultVat = parseFloat($("#default_vat").val());
					var vatOnInvoiceDiscount = 0
					if (defaultVat !== undefined && defaultVat !== '') {
						vatOnInvoiceDiscount = 	(defaultVat/100) * totalInvoiceDiscount;
					}
					totalVatAmount += vatOnInvoiceDiscount;
					$("#customer_invoice_discounts").html(totalInvoiceDiscount);
					netTotal = netTotal + totalInvoiceDiscount;
				}
			} else {
				$("#customer_discounts").hide();
			}
			var totalOrderValue = netTotal + totalVatAmount;
			$("#sub_total").html(  accounting.formatMoney(subTotalAmount) );
			$("#net_total").html(  accounting.formatMoney(netTotal) );
			$("#total_ordered").html( totalOrdered );
			$("#total_received").html( totalReceived );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.formatMoney(totalPrice) );
			$("#price_excl").html( accounting.formatMoney(priceExc) );
			$("#price_incl").html( accounting.formatMoney(priceInc) );
			$("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
			$("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );
			$("#total_vat_amount").html( accounting.formatMoney(totalVatAmount) );
			$("#total_discount").html( accounting.formatMoney(totalDiscountAmount)  );
			$("#total_order_value").html( accounting.formatMoney(totalOrderValue) );
		}
	}
}(Sales || {}, jQuery))

