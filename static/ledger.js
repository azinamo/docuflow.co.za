$(function(){

    var ledgerContainer = $("#general_ledger");

    $.get(ledgerContainer.data("modal"), function (data) {
        var modalEl = $(data).modal("show");
        var form = $(data).find("form");
        initDatePicker();
        initChosenSelect()

        // var accountSelect = $(".accounts-select2");
        // var ajaxUrl = accountSelect.data('ajax-url');
        // var options = {
        //     url: function(phrase) {
        //         return ajaxUrl + "?term=" + phrase + "&format=json";
        //     },
        //     getValue: "label",
        //     theme: "square",
        //     list: {
        //         onSelectItemEvent: function(e) {
        //             console.log(e)
        //             var value = $("#function-data").getSelectedItemData().realName;
        //
        //             $("#data-holder").val(value).trigger("change");
        //         }
        //     }
        // };
        // accountSelect.easyAutocomplete(options);

        // var ajaxUrl = accountSelect.data('ajax-url');
        // var chosenWidth = calculateSelectWidth(accountSelect);
        // accountSelect.select2({
        //     ajax: {
        //         url: ajaxUrl,
        //         dataType: "json"
        //     },
        //     width: chosenWidth + "px",
        //     allowClear: true,
        //     placeholder: "Select ..."
        // });


        $(".generate-ledger").on('click', function(e){
            e.preventDefault()
            Ledger.loadLedger($(this));
            // modalEl.modal('hide');
        });

    });

});

var Ledger = (function(ledger, $){

	return {

	    loadLedger: function(el)
        {
            var generalLedgerContainer = $("#general_ledger");
            var ledgerForm = $("#ledger_search");
            console.log("Loading overlay")
            console.log( ledgerForm );
            action = el.data('action');
            ledgerForm.LoadingOverlay('show', {
                text: 'Processing . . . '
            });
            ledgerForm.ajaxSubmit({
                beforeSubmit:  function(formData, jqForm, options) {
                    var queryString = $.param(formData);
                },  // pre-submit callback
                error: function (data) {
                    // handle errors
                },
                success:       function(responseHtml, statusText, xhr, $form)
                {

                   generalLedgerContainer.html(responseHtml);
                   ledgerForm.LoadingOverlay('hide');
                },  // post-submit callback
                type: 'get'
                // $.ajax options can be used here too, for example:
                //timeout:   3000
            });
        }

	}

}(Ledger || {}, jQuery))