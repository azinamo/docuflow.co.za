(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer || {};
    var salesItem = window.Docuflow.SalesItem || {};
    var delivery = window.Docuflow.Delivery = {};

    delivery.init = function()
	{
		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");
				var form = $(data).find("form");
				var submitBtn = form.find("button[type='submit']");

				common.initDatePicker();

				common.initChosenSelect();

			});
		});

        $(".save-delivery").on("click", function(e){
            e.preventDefault();
            var next = $(this).data("action");
            form.submit($(this), $("#delivery_form"), {"next": next});
        });

		$(".ajax-submit").on("click", function(e){
			e.preventDefault();
			var next = $(this).data("action");
			form.submit($(this), $("#delivery_form"), {"next": next});
		});

		var deliveryItemsBody = $("#delivery_items_body");
		if (deliveryItemsBody.length > 0) {
			var ajaxUrl = deliveryItemsBody.data("ajax-url");
			delivery.loadDeliveryItems(deliveryItemsBody, ajaxUrl);
		}

		var deliveryEstimateItemsBody = $("#delivery_estimate_items_body");
		if (deliveryEstimateItemsBody.length > 0) {
			var ajaxUrl = deliveryEstimateItemsBody.data("ajax-url");
			delivery.loadDeliveryItems(deliveryEstimateItemsBody, ajaxUrl, $("#delivery_estimate_items_placeholder"));
		}

		var addDeliveryLine = $("#add_delivery_line");
		addDeliveryLine.on('click', function(){
			$(this).prop('disabled', true).data('html', addDeliveryLine.text()).text('loading ...')
			var ajaxUrl = addDeliveryLine.data('ajax-url');
			delivery.loadDeliveryItems(addDeliveryLine, ajaxUrl);
		});

		$(".action-delivery").on('click', function (e) {
			e.preventDefault();
			delivery.actionDelivery($(this));
		});

		$(".bulk-action").on('click', function(e){
			e.preventDefault();
			delivery.bulkInvoicesAction($(this));
		});

		$(".single-action").on('click', function(e){
			e.preventDefault();
			delivery.singleInvoiceAction($(this));
		});

		$("#id_date").on('change', function(){
			var deliveryDate = $("#id_date").val()
			if (deliveryDate !== "") {
				deliveryDate = moment(deliveryDate);
			}
			var today = moment(moment().format('DD-MMM-YYYY')).add(1, 'h');
			var minTime = moment().format('HH:mm');
			var defaultTime = today.format('HH:mm');
			console.log( minTime );
			console.log( new Date(today.format('DD-MMM-YYYY HH:mm')) );
			if (today.isSame(deliveryDate)) {
				$('#id_delivery_time').timepicker({
					minuteStep: 1,
					showSeconds: false,
					showMeridian: false
				});
			}
		});

		$('#id_delivery_time').timepicker({
			minuteStep: 1,
			showSeconds: false,
			showMeridian: false
		});

		var idCustomer = $("#id_customer");
		idCustomer.on('change', function(e){
			e.preventDefault();
			customer.loadDetails(idCustomer);
		});
	};

	delivery.bulkInvoicesAction = function(el)
	{
		var deliveries = Delivery.getSelectedDeliveries();
		var totalDeliveries = deliveries.length;
		if (totalDeliveries === 0) {
			alert('Please select at lease one delivery');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( totalDeliveries )
			console.log( ajaxUrl )
			$.post(ajaxUrl, {
				deliveries: deliveries
			}, function(response){
				message.responseNotification(response);
			});
		}
	};

	delivery.singleInvoiceAction = function(el)
	{
		var deliveries = Delivery.getSelectedDeliveries();
		var totalDeliveries = deliveries.length;
		if (totalDeliveries === 0) {
			alert('Please select at lease one delivery');
		} else if (totalDeliveries >1) {
			alert('Please select one delivery to action');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( deliveries )
			console.log( ajaxUrl )
			$.post(ajaxUrl, {
				deliveries: deliveries
			}, function(response){
				message.responseNotification(response);
			});
		}
	};

	delivery.getSelectedDeliveries = function()
	{
		var deliveries = []
		$(".delivery-action").each(function(){
			if ($(this).is(":checked")) {
				deliveries.push($(this).data('delivery-id'));
			}
		});
		return deliveries;
	};

	delivery.actionDelivery = function(el)
	{
		console.log("Action deliveries")
		console.log(el);
		var ajaxUrl = el.data('ajax-url');
		console.log(ajaxUrl)
		$.getJSON(ajaxUrl, function(response){
			console.log(response);
			message.responseNotification(response);
		});
	};

	delivery.reloadDeliveryRow = function(el)
	{
		var rowId = el.data('row-id');
		var ajaxUrl = el.data('ajax-url');
		var chosenWidth = $("#"+ rowId + "_delivery_item_chosen").width();
		$.get(ajaxUrl, {
			row_id:rowId,
			line_type: $("#type_"+ rowId +" option:selected").val()
		}, function(html) {
			$("#delivery_row_" + rowId).replaceWith(html);

			salesItem.handleRow(rowId);

		});

	};

	delivery.loadDeliveryItems = function(el, ajaxUrl, container)
	{
		var rowId = 0;
		var lastRow = $(".sales-items").last();
		if (lastRow.length > 0) {
			console.log("Row was");
			console.log( lastRow.data('row-id') );
			rowId = parseInt(lastRow.data('row-id')) + 1;
		}
		delivery.loadDeliveryRow(el, ajaxUrl, rowId, lastRow, container)
	};

	delivery.loadDeliveryRow = function(el, ajaxUrl, rowId, lastRow, container)
	{
		var chosenWidth = $("#"+ rowId + "_delivery_item_chosen").width();
		container = container || $("#delivery_items_placeholder");

		$.get(ajaxUrl, {
			row_id:rowId
		}, function(html) {
			if (lastRow !== undefined && lastRow.length > 0) {
				lastRow.after(html)
			} else {
				container.replaceWith(html);
			}
			var _lastRow = $(".delivery_item_row").last();
			if (_lastRow.length > 0) {
				rowId = parseInt(_lastRow.data('row-id'));
			}
			console.log( "Row --> ", rowId)
			$(".sales-items").each(function(){
				var itemRowId = $(this).data("row-id");
				salesItem.handleRow(itemRowId);
			});

			// $("#vat_code_" + rowId).on('blur', function(e){
			// 	if (_lastRow !== undefined) {
			// 		var lastRowId = _lastRow.data('row-id');
			// 		delivery.loadDeliveryRow(el, ajaxUrl, parseInt(lastRowId) + 1, _lastRow);
			// 	}
			// });

			$(".line-type").on("change", function(e){
				delivery.reloadDeliveryRow($(this));
        	});
			el.prop('disabled', false).text(el.data('html'));
		});
	};

    delivery.init();

})();