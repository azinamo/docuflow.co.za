(function() {
   //"use strict";

	window.Docuflow = window.Docuflow || {};
	var common = window.Docuflow.Common || {};
	var modal = window.Docuflow.Modal || {};
	var sales = window.Docuflow.Sales || {};
	var salesItem = window.Docuflow.SalesItem = {};

	salesItem.init = function()
	{
        console.log("SALES ITEMS ....");
	};

    salesItem.chosenWidth = function()
    {
        var chosenWidth = 250;
        var chosenContainer = $(".sales-items > .chosen-container");
        if (chosenContainer.length > 0 ) {
            chosenWidth = chosenContainer.width();
        }
        if (chosenWidth > 0) {
            $(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
            $(".line-type").chosen({'width': '40px'});
        } else {
            $(".chosen-select").not('.line-type').chosen({'width': '250px'});
            $(".line-type").chosen({'width': '40px'});
        }
        return chosenWidth;
    };

    salesItem.handleRow = function(rowId)
    {
        console.log("--- HANDLE SALES INVENTORY ITEM ROW ....", rowId);
        var lineDiscount = $("#line_discount");
        var discountPercentageEl = $("#discount_percentage_" + rowId);
        if (lineDiscount.val() !== "" && discountPercentageEl.val() === "") {
            discountPercentageEl.val(lineDiscount.val());
        }

        var modelSearchEl = $(".model-search-" + rowId);
        var model = modelSearchEl.data('model');
        var searchUrl = modelSearchEl.data("ajax-url");
        var detailUrl = modelSearchEl.data("ajax-detail-url");
        //var chosenWidth = common.calculateSelectWidth();
        var chosenWidth = salesItem.chosenWidth();
        modelSearchEl.ajaxChosen({
            url: searchUrl,
            dataType: "json",
            width: chosenWidth + "px",
            allowClear: true,
            placeholder: "Select ..."
        }, {loadingImg: '/static/js/vendor/chosen/loading.gif'});

        modelSearchEl.on("change", function(e){
            var modelId = $(this).val();
            var params = {}
            var customerEl = $("#id_customer");
            var pricingEl = $("#id_pricing");
            //var modelIdVal = (model === undefined ? 'inventory_id' : model + "_id");
            if (model === undefined) {
                params['inventory_id'] = modelId
            } else {
                params[model + "_id"] = modelId
            }
            params['customer_id'] = customerEl.val();
            if (pricingEl !== undefined) {
                params['pricing_id'] = pricingEl.val();
            }
            console.log("Get inventory data from database ", modelId, detailUrl);
            console.log("Params")
            console.log( params )

            $.get(detailUrl, params, function(responseData){
                var customerLineDiscount = $("#line_discount").val();
                console.log( "Row --> ", rowId, " and load fields")
                console.log(responseData)

                var price = responseData.price;
                var priceIncluding = responseData.price_incl;
                var unit = responseData.unit;
                var binLocation = responseData.bin_location;
                var description = responseData.description;
                var vatId = responseData.vat_code_id;
                var saleVatId = responseData.sale_vat_code_id;
                var availableStock = responseData.in_stock;
                var unitId = responseData.unit_id;
                var isDiscountable = responseData.is_discountable;
                var allowNegativeStock = responseData.allow_negative;
                var netWeight = responseData.net_weight;
                var grossWeight = responseData.gross_weight;
                var averagePrice = responseData.average_price;

                var priceEl =  $("#price_" + rowId);
                var priceIncEl =  $("#price_including_" + rowId);

                // console.log( "price --> ", price, " add to ", $("#price_" + rowId ));
                // console.log( "price including--> ", priceIncluding, " add to ", priceIncEl);
                // console.log( "unit --> ", unit, " add to ", $("#unit_" + rowId ));
                // console.log( "is discountable --> ", isDiscountable, " disable discount field ", typeof isDiscountable);

                priceEl.val( price );
                priceIncEl.val( priceIncluding );
                if (unit !== undefined && unit !== "") {
                    $("#unit_" + rowId).val(unit);
                }
                if (unitId !== undefined && unitId !== "") {
                    $("#unit_id_" + rowId).val(unitId);
                }

                if (binLocation !== undefined && binLocation !== "") {
                    $("#bin_location_" + rowId).val(binLocation)
                }
                if (description !== undefined && description !== "") {
                    $("#description_" + rowId).val(description)
                }
                if (saleVatId !== undefined && saleVatId !== "") {
                    $("#vat_code_" + rowId).val(saleVatId).trigger("chosen:updated").trigger("change");
                } else {
                    $("#vat_code_" + rowId).val("").trigger("chosen:updated").trigger("change");
                }
                salesItem.handleRowDiscount(rowId, isDiscountable, customerLineDiscount);
                availableStock = parseFloat(availableStock);
                console.log( "availableStock --> ", availableStock);
                if (isNaN(availableStock)) {
                    availableStock = 0;
                }
                console.log(availableStock)
                console.log( "availableStock --> ", availableStock);
                if (availableStock > 0) {
                    $("#order_item_" + rowId).html("No");
                } else {
                    $("#order_item_" + rowId).html("Yes");
                }
                if (netWeight !== undefined) {
                    $("#net_weight_" + rowId).val(netWeight);
                }
                if (grossWeight !== undefined) {
                    $("#gross_weight_" + rowId).val(grossWeight);
                }
                if (averagePrice !== undefined) {
                    $("#average_price_" + rowId).val(averagePrice);
                }
                $("#available_stock_" + rowId).html(accounting.formatMoney(availableStock));
                $("#available_in_stock_" + rowId).html(availableStock);
                $("#in_stock_" + rowId).val(availableStock);
                $("#allow_negative_stock_" + rowId).val(allowNegativeStock);
            }, "json");
        });


        $("#quantity_"+ rowId +"").on("blur", function(){
            var orderedEl = $("#ordered_" + rowId);
            console.log("Ordered el --> ", orderedEl);
            if (orderedEl.length > 0) {
                salesItem.handleOrderedQuantity(rowId, orderedEl);
            } else {
                sales.calculateInventoryItemPrices(rowId);
            }
        });

        $("#ordered_"+ rowId +"").on("blur", function(){
            var quantityEl =  $("#quantity_" + rowId);
            var backOrderEl = $("#back_order_" + rowId);
            var quantity  = salesItem.getCellVal($(this));
            var inStock = salesItem.getCellVal($("#in_stock_" + rowId));
            var allowNegativeStockVal = $("#allow_negative_stock_" + rowId).val();
            var allowNegativeStock = (allowNegativeStockVal === '1');
            console.log("Quantity -> ", quantity, " in stock ", inStock, " allow negative --> (",allowNegativeStockVal,") ", allowNegativeStock);
            if (salesItem.isOrderable(quantity, inStock, allowNegativeStock)) {
                backOrderEl.prop("disabled", true).prop("checked", false);
                if (isNaN(quantity)) {
                   quantityEl.val(0);
                } else {
                    quantityEl.val(quantity);
                }
                sales.calculateInventoryItemPrices(rowId);
            } else {
                console.log("Allow back ordering, we cannot order more than available");
                if (isNaN(inStock)) {
                    quantityEl.val(0);
                    backOrderEl.prop("disabled", true);
                } else {
                    quantityEl.val(inStock);
                    backOrderEl.prop("disabled", false);
                }
                sales.calculateInventoryItemPrices(rowId);
            }
        });

        $("#price_"+ rowId +"").on("blur", function(){
            sales.calculateInventoryItemPrices(rowId);
        });

        $("#vat_code_"+ rowId).on("change", function(){
            var selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
            var vatPercentage = selectedVatOption.data("percentage");
            console.log("*** Vat code changes --> ", vatPercentage);
            if (vatPercentage !== undefined && vatPercentage !== "") {
                $("#vat_percentage_"+ rowId).val(vatPercentage);
            } else {
                $("#vat_percentage_"+ rowId).val(0);
            }
            sales.calculateInventoryItemPrices(rowId);
        });

        $("#discount_percentage_"+ rowId +"").on("blur", function(){
            sales.calculateInventoryItemPrices(rowId);
        });

        $("#delete_" + rowId).on("click", function(){
            var deleteAction = $(this).data("action") || "remove";
            if (deleteAction === "delete") {
                if (confirm("Are you sure you want to delete this row")) {
                    salesItem.deleteRow(rowId);
                }
            } else {
                salesItem.deleteRow(rowId);
            }
        });

        console.log( rowId );
        if (rowId !== undefined) {
            sales.calculateInventoryItemPrices(rowId);
        }
    };


    salesItem.getCellVal = function(el)
    {
        if (el === undefined) {
            return 0;
        }
        if (el.length === 0) {
            return 0;
        }
        var elVal  = el.val();
        if (elVal === "") {
            return 0;
        }
        return parseFloat(elVal);
    }

    salesItem.handleOrderedQuantity  = function(rowId, orderedEl)
    {
        var backOrderEl = $("#back_order_" + rowId);
        var quantity  = parseFloat($("#quantity_" + rowId).val());
        var inStock = parseFloat($("#in_stock_" + rowId).val());
        var orderedQty = parseFloat(orderedEl.val());
        //var allowNegativeStock = $("#inventory_item_" + rowId + " option:selected").data("allow-negative-stock");
        var allowNegativeStock = salesItem.isNegativeStockAllowed(rowId);
        if (salesItem.isOrderable(quantity, inStock, allowNegativeStock)) {
            if (backOrderEl !== undefined) {
               if (quantity < orderedQty) {
                  backOrderEl.prop("disabled", false);
               } else {
                  backOrderEl.prop("disabled", true).prop("checked", false);
               }
            }
            sales.calculateInventoryItemPrices(rowId);
        } else {
            console.log("Does not allow negative stock");
            if (inStock !== undefined) {
                $("#quantity_" + rowId).val(inStock);
            }
            if (backOrderEl !== undefined) {
               backOrderEl.prop("disabled", false);
            }
            sales.calculateInventoryItemPrices(rowId);
        }
    };

    salesItem.isNegativeStockAllowed = function(rowId)
    {
        var allowNegativeStockEl = $("#allow_negative_stock_" + rowId);
        if (allowNegativeStockEl.length) {
            var allowNegativeStockVal = allowNegativeStockEl.val();
            if (allowNegativeStockVal === '1' || allowNegativeStockVal === 1) {
                return true
            }
        }
        return false;
    };

    salesItem.isOrderable = function(quantity, inStock, allowNegativeStock)
    {
        if (quantity < inStock) {
            return true;
        }
        console.log("TRY ----------ALLOW NEGATIVE STOCK ----", typeof allowNegativeStock )
        if (allowNegativeStock) {
            console.log("----------ALLOW NEGATIVE STOCK ----")
            return true;
        }
        return false;
    };

    salesItem.deleteRow = function(rowId)
    {
        $("#sales_item_" + rowId).remove();

        sales.calculateInventoryItemPrices(rowId);
    };

    salesItem.handleRowDiscount = function(rowId, isDiscountable, lineDiscount)
    {
        var discountPercentageEl = $("#discount_percentage_" + rowId);
        if (isDiscountable) {
            var discountVal = lineDiscount || 0;
            discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
        } else {
            discountPercentageEl.val(0).prop('disabled', true).trigger('change');
            $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
            $("#discount_amount_" + rowId).val('0.00').trigger('change');
        }
    };

	salesItem.init();

})();