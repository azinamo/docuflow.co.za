(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var form = window.Docuflow.Form || {};
    var sales = window.Docuflow.Sales = {};


    sales.init = function()
    {
      console.log("Sales initializing ...");
    };

    sales.handleRowDiscount = function(rowId, isDiscountable, lineDiscount)
    {
        console.log($("#line_discount"));
        console.log($("#line_discount").val());
        console.log("Handling row discount, is discountable -> ", isDiscountable, typeof isDiscountable, lineDiscount );
        var discountPercentageEl = $("#discount_percentage_" + rowId);
        if (isDiscountable) {
            var discountVal = lineDiscount || 0;
            console.log("Now discount value is ", lineDiscount)
            discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
        } else {
            discountPercentageEl.val(0).prop('disabled', true).trigger('change');
            $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
            $("#discount_amount_" + rowId).val('0.00').trigger('change');
        }
    };

    sales.calculateInventoryItemPrices = function(rowId)
    {
        console.log("SALES --> CALCULATING INVOICE INVENTORY ROW ", rowId + " ---------------------------- ");
        var price = $("#price_" + rowId);
        var priceIncl = $("#price_including_" + rowId);
        var ordered = $("#quantity_ordered_" + rowId);
        var received = $("#quantity_" + rowId);
        var short = $("#quantity_short_" + rowId);
        var vat = $("#vat_amount_" + rowId);
        var vatPercentage = $("#vat_percentage_" + rowId);
        var discountPercentage = $("#discount_percentage_" + rowId);
        var discount = $("#discount_" + rowId);
        var averagePrice = $("#average_price_" + rowId);

        var quantityOrdered = 0;
        var shortQuantity = 0;
        var quantityReceived = 0;
        var vatAmount = 0;
        var priceExcl = 0;
        var discountAmount = 0;
        var totalVat = 0;
        var totalDiscount = 0;
        var priceIncludingAmount = 0;
        var totalInc = 0;
        var totalExcl = 0;
        var subTotal = 0;

        if (ordered !== undefined && ordered.length > 0) {
            if (ordered.val() !== "") {
                quantityOrdered = parseFloat(ordered.val());
            }
        }
        if (received !== undefined && received.length > 0) {
            if (received.val() !== "") {
                quantityReceived = parseFloat(received.val());
            }
        }

        if (quantityOrdered > 0) {
            if (quantityReceived <= quantityOrdered) {
                shortQuantity =  quantityOrdered - quantityReceived
            } else {
                received.val(quantityOrdered);
                //backOrder.prop('disabled', true);
                alert("Please enter valid received quantity")
                return false;
            }
        }
        if (short !== undefined  && short.length > 0) {
            short.val( shortQuantity );
        }
        if (price !== undefined  && received.length > 0) {
            if (price.val() !== "") {
                priceExcl = parseFloat(accounting.unformat(price.val()));
            }
        }
        subTotal = priceExcl * quantityReceived;
        if (vatPercentage.length > 0) {
            if (vatPercentage.val() !== "" && priceExcl !== "") {
                var percentage = parseFloat(vatPercentage.val());
                console.log("Vat 1 percentage --> ", percentage);
                vatAmount = (percentage/100) * priceExcl;
                console.log("Vat amount --> ", vatAmount);
            }
        }
        priceIncludingAmount = priceExcl + vatAmount;
        console.log("PRICE INCLUDING IS ", priceIncludingAmount, " with VAT ", vatAmount, " and excl ->  ", priceExcl);
        if (discountPercentage.length > 0) {
            if (discountPercentage.val() !== "" && priceExcl !== "") {
                var discPercentage = parseFloat(discountPercentage.val());
                console.log("Discount percentage --> ", discPercentage);
                discountAmount = (discPercentage/100) * priceExcl;
                console.log("Discount amount --> ", discountAmount);
                discount.val(accounting.toFixed(discountAmount, 4));
            }
        }
        console.log("Price excluding, not discount ", priceExcl);

        console.log("Price including amount, not discount ", priceIncludingAmount);
        if (discountAmount > 0) {
            priceExcl = priceExcl - discountAmount;
        }

        if (vatPercentage.length > 0) {
            if (vatPercentage.val() !== "" && priceExcl !== "") {
                var percentageVat = parseFloat(vatPercentage.val());
                console.log("Vat percentage --> ", percentageVat);
                vatAmount = (percentageVat/100) * priceExcl;
                console.log("Vat amount --> ", vatAmount);
                vat.val(accounting.toFixed(vatAmount, 4));
            }
        }
        if (discountAmount !== "") {
            totalDiscount = discountAmount * quantityReceived;
        }
        totalExcl = priceExcl * quantityReceived;
        totalInc = totalExcl;
        if (vatAmount !== "") {
            console.log("Vat amount", vatAmount);
            console.log("Price including + Vat amount", priceIncludingAmount, vatAmount);
            totalVat = vatAmount * quantityReceived;
            totalInc += totalVat;
        }
        //var totalInc = priceIncludingAmount * quantityReceived;
        console.log( " quantityReceived ", quantityReceived );
        console.log( " Price EXCL", priceExcl );
        console.log( " totalExcl ", totalExcl );
        console.log( " vatAmount ", vatAmount );
        console.log( " discountAmount ", discountAmount );
        console.log( " priceIncl ", priceIncludingAmount);
        console.log( " totalInc", totalInc );
        console.log( " averagePrice", averagePrice, averagePrice.val() );

        if (averagePrice !== undefined && averagePrice.length > 0) {
            var _averagePrice = parseFloat(averagePrice.val());
            var grossProfit = (priceExcl - _averagePrice)/priceExcl;
            console.log( " grossProfit ", grossProfit, grossProfit * 100 );
            $("#gross_profit_" + rowId).html(accounting.formatMoney(grossProfit * 100) + '%')
        }
        // if (backOrder !== undefined) {
        // 	if (shortQuantity === 0) {
        // 		backOrder.prop('disabled', true);
        // 	} else {
        // 		backOrder.prop('disabled', false);
        // 	}
        // }

        $("#vat_total_" + rowId).html( accounting.formatMoney(totalVat) );
        $("#total_vat_" + rowId).val(accounting.toFixed( totalVat, 4));

        $("#discount_amount_" + rowId).html( accounting.formatMoney(totalDiscount) );
        $("#total_discount_" + rowId).val(accounting.toFixed(totalDiscount, 4));

        $("#price_incl_" + rowId).html( accounting.formatMoney(priceIncludingAmount) );
        $("#sub_total_" + rowId).val( accounting.toFixed(subTotal, 4) );

        priceIncl.val( priceIncludingAmount );

        $("#total_price_exl_" + rowId).html( accounting.formatMoney(totalExcl) );
        $("#total_price_excluding_" + rowId).val( accounting.toFixed(totalExcl, 4) );

        $("#total_price_incl_" + rowId).html( accounting.formatMoney(totalInc) );
        $("#total_price_including_" + rowId).val( accounting.toFixed(totalInc, 4) );

        sales.calculateTotals();
    };


    sales.calculateTotals = function()
    {
        var totalOrdered = 0;
        var totalReceived = 0;
        var totalShort = 0;
        var priceExc = 0;
        var priceInc = 0;
        var totalPrice = 0;
        var totalTotalExc = 0;
        var totalTotalInc = 0;
        var totalVatAmount = 0;
        var totalDiscountAmount = 0;
        var subTotalAmount = 0;
        var netTotal = 0;
        var invoiceDiscountAmount = 0;
        var netWeight = 0;
        var grossWeight = 0;

        $(".ordered").each(function(){
            totalOrdered += parseInt($(this).val());
        });

        $(".received, .quantity").each(function(){
            totalReceived += parseInt($(this).val());
        });
        $(".short").each(function(){
            totalShort += parseInt($(this).val());
        });
        $(".price").each(function(){
            totalPrice += parseFloat(accounting.unformat($(this).val()));
        });
        $(".price-exc").each(function(){
            priceExc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".price-incl").each(function(){
            priceInc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-price-excl").each(function(){
            totalTotalExc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-price-incl").each(function(){
            totalTotalInc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-vat-amount").each(function(){
            totalVatAmount += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-discount").each(function(){
            totalDiscountAmount += parseFloat(accounting.unformat($(this).val()));
        });
        $(".sub-total").each(function(){
            subTotalAmount += parseFloat(accounting.unformat($(this).val()));
        });
        $(".net-weight").each(function(){
            netWeight += parseFloat(accounting.unformat($(this).val()));
        });
        $(".gross-weight").each(function(){
            grossWeight += parseFloat(accounting.unformat($(this).val()));
        });


        totalDiscountAmount = totalDiscountAmount * -1;
        $("#id_line_discount").val(accounting.toFixed(totalDiscountAmount, 2));
        console.log("Subtotal is ", subTotalAmount, " + ", totalDiscountAmount, " giving ");
        netTotal = subTotalAmount + totalDiscountAmount;
        console.log("netTotal = ", netTotal)
        var invoiceDiscount = $("#invoice_discount").val();
        var defaultVat = parseFloat($("#default_vat").val());
        var totalInvoiceDiscount = 0;
        var vatOnInvoiceDiscount = 0;
        var customerDiscountsEl = $("#customer_discounts");
        if (invoiceDiscount !== undefined && invoiceDiscount !== "") {
            customerDiscountsEl.show();
            console.log("----- CUSTOMER DISCOUNTS ----");
            if (netTotal !== "") {
                console.log("Net total -> ", netTotal)
                totalInvoiceDiscount = ((parseFloat(invoiceDiscount)/100) * netTotal) * -1;
                console.log("Total invoice discount(Customer discounts)", totalInvoiceDiscount);
                console.log("defaultVat -> ", defaultVat)
                if (defaultVat !== undefined && defaultVat !== "") {
                    vatOnInvoiceDiscount = (defaultVat/100) * totalInvoiceDiscount;
                }
                console.log("Vat on discount -> ", vatOnInvoiceDiscount);
                console.log("totalVatAmount -> ", totalVatAmount);
                totalVatAmount += vatOnInvoiceDiscount;

                netTotal = netTotal + totalInvoiceDiscount;
                console.log("netTotal -> ", netTotal);
            }
        } else {
            customerDiscountsEl.hide();
        }
        console.log("netTotal after removing customer discount = ", totalInvoiceDiscount, " = ", netTotal, " --, vat ", totalVatAmount);
        var totalOrderValue = netTotal + totalVatAmount;
        var totalNetWeight = netWeight * totalReceived;
        var totalGrossWeight = grossWeight * totalReceived;
        console.log("Total order value => ", totalOrderValue);
        $("#customer_invoice_discounts").html(accounting.formatMoney(totalInvoiceDiscount));
        $("#id_customer_discount").val(accounting.toFixed(totalInvoiceDiscount, 2));

        $("#sub_total").html(  accounting.formatMoney(subTotalAmount) );
        $("#id_sub_total").val(accounting.toFixed(subTotalAmount, 2));

        $("#net_total").html(  accounting.formatMoney(netTotal) );
        $("#id_net_amount").val(accounting.toFixed(netTotal, 2));

        $("#total_ordered").html( accounting.formatMoney(totalOrdered) );
        $("#total_received").html( accounting.formatMoney(totalReceived) );
        $("#total_net_weight").html( accounting.formatMoney(totalNetWeight) );
        $("#total_gross_weight").html( accounting.formatMoney(totalGrossWeight) );
        $("#total_short").html( totalShort );
        $("#total_price").html( accounting.formatMoney(totalPrice) );
        $("#price_excl").html( accounting.formatMoney(priceExc) );
        $("#price_incl").html( accounting.formatMoney(priceInc) );
        $("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
        $("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );

        $("#total_vat_amount").html( accounting.formatMoney(totalVatAmount) );
        $("#id_vat_amount").val(accounting.toFixed(totalVatAmount, 2));

        $("#total_discount").html( accounting.formatMoney(totalDiscountAmount)  );
        $("#id_discount_amount").val(accounting.toFixed(totalDiscountAmount, 2));

        $("#total_order_value").html( accounting.formatMoney(totalOrderValue) );
        $("#id_total_amount").val(accounting.toFixed(totalOrderValue, 2));

        // var creditLimit = $("#credit_limit");
        // if (creditLimit.length > 0) {
        //     var _limit = parseFloat(creditLimit.val());
        //     var customerAvailableCredit = parseFloat(accounting.unformat(_limit));
        //     if (totalOrderValue > customerAvailableCredit) {
        //         $("#submit_invoice_and_continue").prop('disabled', true);
        //     } else {
        //         $("#submit_invoice_and_continue").prop('disabled', false);
        //     }
        // }

        var customerDiscounts = {"percentage": invoiceDiscount, "total": totalInvoiceDiscount,
            "vat": vatOnInvoiceDiscount};
        console.log("Customer discounts-->");
        console.log(customerDiscounts);
        sales.displayVatSummary(customerDiscounts);
    };

    sales.displayVatSummary = function(customerDiscounts)
    {
       console.log("-----------VAT SUMMARY-----");
       var summary = {"vat_codes": {}, "total_vat": 0, "total_excl": 0, "customer_discounts": {}};
       $(".vat").each(function(){
           var el = $(this);
           var selectedVatOption = el.val();
           if (selectedVatOption !== undefined && selectedVatOption !== "") {
               var rowId = el.data("item-id");
               var percentage = 0;
               var vatText = "";

               if (el.prop("type") === "hidden") {
                  console.log("Hidden -----")
                  percentage = el.data("percentage");
                  vatText = el.data("text");
               } else {
                   var vatCodeEl = $("#vat_code_" + rowId + " option:selected");
                  percentage = vatCodeEl.data("percentage");
                  vatText = vatCodeEl.text();
               }

               var exclAmount = $("#total_price_excluding_" + rowId).val();
               var vatAmount = $("#total_vat_" + rowId).val();
               var totalExcl = parseFloat(exclAmount);
               var totalVat = parseFloat(vatAmount);
               console.log("Total excluding ", exclAmount, " for row id ", rowId );
               console.log(vatText)
               if (selectedVatOption in summary.vat_codes) {
                   summary.vat_codes[selectedVatOption].total_excl += totalExcl;
                   summary.vat_codes[selectedVatOption].total_vat += totalVat;
               } else {
                    summary.vat_codes[selectedVatOption] = {"total_excl": totalExcl, "total_vat": totalVat,
                        "percentage": percentage, "text": vatText };
               }
               summary.total_excl += totalExcl;
               summary.total_vat += totalVat;
           }
       });
       console.log(summary);
       var vatCodesHtml = [];
       var vatCodes = Object.values(summary.vat_codes);
       $(vatCodes).each(function(index, vat_code){
           if (vat_code.text !== undefined && vat_code.percentage !== undefined) {
               vatCodesHtml.push("<tr class='vat_code_summary'>");
                   vatCodesHtml.push("<td>"+ vat_code.text +" - "+ vat_code.percentage +"%</td>");
                   vatCodesHtml.push("<td class='text-right'><span id='total_price_excl'>"+accounting.formatMoney(vat_code.total_excl)+"</span></td>");
                   vatCodesHtml.push("<td class='text-right'>"+accounting.formatMoney(vat_code.total_vat)+"</td>");
               vatCodesHtml.push("</tr>");
           }
       });
       if (customerDiscounts.percentage  > 0) {
           vatCodesHtml.push("<tr class='vat_code_summary'>");
               vatCodesHtml.push("<td>Vat on customer discounts	</td>");
               vatCodesHtml.push("<td class='text-right'><span id='total_price_excl'>"+accounting.formatMoney(customerDiscounts.total)+"</span></td>");
               vatCodesHtml.push("<td class='text-right'>"+accounting.formatMoney(customerDiscounts.vat)+"</td>");
           vatCodesHtml.push("</tr>");
           summary.total_excl += customerDiscounts.total;
           summary.total_vat += customerDiscounts.vat;
       }
       $("#vat_summary_total_excl").html(accounting.formatMoney(summary.total_excl));
       $("#vat_summary_total_vat").html(accounting.formatMoney(summary.total_vat));
       $("#summary_body").html(vatCodesHtml.join(" "));
       $("#calculation_summary").show();
    };

    sales.dfMoney = function(value)
    {
        return accounting.toFixed(accounting.toFixed(value, 2));
    }

    sales.init();

})();