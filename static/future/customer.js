(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer = {};

    customer.init = function()
    {
        $("#customers>tbody>tr>td:not(.actions)").on("click", function(){
            var detailUrl = $(this).parent("tr").data("detail-url");
            document.location.href = detailUrl;
            return false;
        });

        $("#save_customer").on("click", function(e){
            form.submit($(this), $("#customer_form"));
            return false;
        });
    };

    customer.loadDetails = function(el)
    {
        console.log("-------- Customer ------------")
        var ajaxUrl = $("#id_customer option:selected").data("detail-url");
        $.getJSON(ajaxUrl, {
            module: 'sales'
        }, function(response){
            console.log( response )
            console.log( response.is_default )
            if (response.is_default) {
                console.log("load customer form")
                customer.loadCashCustomerForm(response)
            } else {
                console.log("load customer details")
                customer.loadCustomerDetails(response)
            }
        });
    };                                                                                                                                          

    customer.loadCustomerDetails = function(response)
    {
        var postalAddressHtml = [];
        var deliveryAddressHtml = [];
        if (response.hasOwnProperty("postal")) {
            var postalAddress;
            console.log(typeof response.postal);
            if (typeof response.postal === "string") {
                postalAddress = JSON.parse(response.postal);
            } else {
                postalAddress = response.postal;
            }
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.address+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.city+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.province+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.country+"</p>");
        }
        if (response.hasOwnProperty("delivery")) {
            var deliveryAddress;
            console.log(typeof response.delivery)
            if (typeof response.delivery === "string") {
                deliveryAddress = JSON.parse(response.delivery);
            } else {
                deliveryAddress = response.delivery;
            }
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.address+"</p>");
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.city+"</p>");
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.province+"</p>");
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.country+"</p>");
        }
        if (response.hasOwnProperty("discount_terms")) {
            var discountTerms = response.discount_terms;
            if (discountTerms) {
                if (typeof response.discount_terms === "string") {
                    discountTerms = JSON.parse(response.discount_terms);
                } else {
                    discountTerms = response.discount_terms;
                }
                if (discountTerms.hasOwnProperty("line")) {
                    if (discountTerms.line.discount && discountTerms.line.is_discounted)
                    $("#line_discount").val(discountTerms.line.discount);
                    $(".discount").each(function(){
                        if ($(this).val() === "") {
                            $(this).val(discountTerms.line.discount)
                        }
                    });
                }
                if (discountTerms.hasOwnProperty("invoice")) {
                    if (discountTerms.invoice.discount && discountTerms.invoice.is_discounted) {
                        $("#invoice_discount").val(discountTerms.invoice.discount);
                    }
                }
            }
        }
        if (response.hasOwnProperty('df_number') && response.df_number !== "") {
            $("#submit_estimate_and_send_to_df").show();
        }
        // if (response.credit_limit) {
        // 	postalAddressHtml.push("<div class='limits'>");
        // 	postalAddressHtml.push("<input type='hidden' name='credit_limit' id='credit_limit' value='" + accounting.unformat(response.credit_limit) +"'></div>");
        // 	postalAddressHtml.push("<span id='credit_limit_val'>" +accounting.formatMoney(response.credit_limit)+"</span>");
        // 	postalAddressHtml.push("<div>" +accounting.unformat(response.credit_limit)+"</div>");
        // 	postalAddressHtml.push("</div>")
        // } else {
        // 	console.log('Something wrong with limit')
        // }
        // if (response.available_credit || response.credit_limit) {
        // 	postalAddressHtml.push("<div class='limit'>");
        // 	postalAddressHtml.push("<div><span><strong>Available Credit</strong></span>" +accounting.formatMoney(response.available_credit)+"</div>");
        // 	postalAddressHtml.push("<input type='hidden' name='available_credit' id='available_credit' value='" + accounting.unformat(response.credit_limit) +"'></div>");
        // 	postalAddressHtml.push("</div>")
        // } else {
        // 	console.log('Something wrong with limit')
        // }
        if (postalAddressHtml.length > 0) {
            $("#customer_details").html(postalAddressHtml.join(" "))
        }
        if (deliveryAddressHtml.length > 0) {
            var isPickUp = $("#id_is_pick_up");
            if (isPickUp.length) {
               if (isPickUp.is(":checked")) {
                   $("#submit_estimate_with_picking").hide();
                   $("#submit_estimate_with_delivery").hide();
                   $("#shipping_to").html(deliveryAddressHtml.join(" ")).hide()
               }  else {
                   $("#shipping_to").html(deliveryAddressHtml.join(" ")).show()
               }
            } else {
               $("#shipping_to").html(deliveryAddressHtml.join(" "))
            }
        }
        if (response.hasOwnProperty("vat_number")) {
            $("#id_vat_number").val(response.vat_number)
            $("#id_customer_vat_number").val(response.vat_number)
        }

        if (response.hasOwnProperty("pricings")) {
            var html = [];
            $.each(response.pricings, function(index, pricing){
                console.log( pricing )
                html.push("<option value='" + pricing.id + "'>"+ pricing.name+"</option>")
            })
            $("#id_pricing").html(html.join(' '));
        }
        $("#customer_full_name").html("");
        $(".cash").hide();
        $(".customer").show();
        $(".save-cash-invoice").hide();
        var saveBtn = $("#submit_invoice_and_continue");
        saveBtn.show();
    };

    customer.loadCashCustomerForm = function(customerData)
    {
        $("#id_vat_number").val("");
        $("#customer_details").html(customerData['customer_form']);
        var shippingTo = $("#shipping_to");
        shippingTo.html(customerData['delivery_form']);
        var isPickUp = $("#id_is_pick_up");
        if (isPickUp.length) {
           if (isPickUp.is(":checked")) {
               shippingTo.hide();
               $("#submit_estimate_with_picking").hide();
               $("#submit_estimate_with_delivery").hide();
           }  else {
               shippingTo.show();
           }
        } else {
           shippingTo.show();
        }
        var saveCashBtn = $("#submit_cash_invoice_and_continue");
        saveCashBtn.show();
        $(".cash").show();
        $(".customer").hide();
        $(".save-invoice").hide();
    };


    customer.init();

})();