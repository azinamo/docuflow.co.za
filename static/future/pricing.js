(function(){

    window.Docuflow = window.Docuflow || {};
    // var common = window.Docuflow.Common || {};
    // var form = window.Docuflow.Form || {};
    var pricing = window.Docuflow.Pricing || {};


    pricing.init = function()
    {
        console.log("::: ---- Init -------------:::");
    	$("#pricings>tbody>tr>td:not(.actions)").on("click", function(){
            var detailUrl = $(this).parent("tr").data("detail-url");
            if (detailUrl !== undefined && detailUrl !== "") {
                document.location.href = detailUrl;
            }
            return false;
        });

        $("#save_sales_invoice").on("click", function(e){
            form.submit($(this), $("#create_sales_invoice_form"));
            return false;
        });


        $("#save_cash_sales_invoice").on("click", function(e){
            form.submit($(this), $("#create_sales_invoice_form"));
            return false;
        });

        var salesInvoiceBody = $("#invoice_items_body");
        if (salesInvoiceBody.length > 0) {
            var ajaxUrl = salesInvoiceBody.data("ajax-url");
            pricing.loadInvoiceItems(salesInvoiceBody, ajaxUrl);
        }

        var addInvoiceItemLine = $("#add_invoice_item_line");
        addInvoiceItemLine.on('click', function(){
            $(this).prop('disabled', true).data('html', addInvoiceItemLine.text()).text('loading ...');
            var ajaxUrl = addInvoiceItemLine.data('ajax-url');
            pricing.loadInvoiceItems(addInvoiceItemLine, ajaxUrl);
        });

        $(".bulk-action").on('click', function(e){
            e.preventDefault();
            pricing.bulkInvoicesAction($(this));
        });

        $(".single-action").on('click', function(e){
            e.preventDefault();
            pricing.singleInvoiceAction($(this));
        });

        var idCustomer = $("#id_customer");
        idCustomer.on('change', function(e){
            e.preventDefault();
            pricing.loadCustomerDetails(idCustomer);
        });

        var idDocumentDate = $("#id_invoice_date");
        idDocumentDate.on('blur', function(e){
            e.preventDefault();
            pricing.calculateDueDate(idDocumentDate);
        });

        var idVatNumber = $("#id_vat_number");
        idVatNumber.on('blur', function(e){
            e.preventDefault();
            sales.updateVatNumber(idVatNumber);
        });

        var idEstimate = $("#id_estimate");
        idEstimate.on('change', function(e){
            pricing.loadEstimateItems(e)
        });

    };

    pricing.loadEstimateItems = function(el)
    {
        var rowId = 0;
        var estimateItemsUrl = $("#id_estimate option:selected").data('estimate-url');
        var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
        var container = $("#invoice_items_body");
        var lineSelect = $(".line-type");
        var chosenSelect = $(".chosen-select");
        $.get(estimateItemsUrl, {
            row_id:rowId
        }, function(html) {
            container.replaceWith(html);
            console.log( "Row --> ", rowId);
            sales.handleRow(rowId);
            if (chosenWidth > 0) {
                chosenSelect.not('.line-type').chosen({'width': chosenWidth + "px"});
                lineSelect.chosen({'width': '40px'});
            } else {
                chosenSelect.not('.line-type').chosen({'width': '100px'});
                lineSelect.chosen({'width': '40px'});
            }
        });
    };

    pricing.updateVatNumber = function(el)
    {
        var customerVatNumber = $("#id_customer option:selected").data('vat-number');
        var customerId = $("#id_customer option:selected").val();
        if (customerId !== undefined && customerId !== "") {
            if (el.val() !== "" && customerVatNumber !== el.val()) {
                var ajaxUrl = el.data('update-vat-url');
                $.post(ajaxUrl, {
                    vat_number: el.val(),
                    customer_id: customerId
                }, function(response){
                    console.log( response );
                });
            }
        }
    };

    pricing.calculateDueDate = function(el)
    {
        var invoiceDate = new Date(el.val());
        var paymentTerm = $("#id_customer option:selected").data('payment-term');
        var paymentTermDays = $("#id_customer option:selected").data('payment-days');
        console.log('Invoice date, -- payment term and payment days');
        console.log(invoiceDate)
        console.log(paymentTerm)
        console.log(paymentTermDays)
        console.log(typeof paymentTermDays)
        if (paymentTerm === 1) {
            var dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
            console.log(dueDate.format('YYYY-MM-DD'))
            $("#id_due_date").val(dueDate.format('YYYY-MM-DD'));
        } else if (paymentTerm === 2) {
            var dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
            console.log(dueDate.format('YYYY-MM-DD'))
            $("#id_due_date").val(dueDate.format('YYYY-MM-DD'));
        }
    };

    pricing.loadCustomerDetails = function(el)
    {
        console.log('Load customer details');
        var ajaxUrl = $("#id_customer option:selected").data('detail-url');
        var vatNumber = $("#id_customer option:selected").data('detail-url');
        console.log( el );
        console.log( ajaxUrl );
        $.getJSON(ajaxUrl, function(response){
            var postalAddressHtml = [];
            var deliveryAddressHtml = [];
            console.log( response.postal )

            if (response.hasOwnProperty('postal')) {
                var postalAddress = JSON.parse(response.postal);
                postalAddressHtml.push("<p class='mb-1'>"+postalAddress.street+"</p>");
                postalAddressHtml.push("<p class='mb-1'>"+postalAddress.city+"</p>");
                postalAddressHtml.push("<p class='mb-1'>"+postalAddress.province+"</p>");
                postalAddressHtml.push("<p class='mb-1'>"+response.postal_country+"</p>");
            }
            if (response.hasOwnProperty('delivery')) {
                var deliveryAddress = JSON.parse(response.delivery);
                deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.street+"</p>");
                deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.city+"</p>");
                deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.province+"</p>");
                deliveryAddressHtml.push("<p class='mb-1'>"+response.delivery_country+"</p>");
            }
            if (response.hasOwnProperty('discount_terms')) {
                var discountTerms = response.discount_terms;
                if (discountTerms.hasOwnProperty('line')) {
                    if (discountTerms.line.discount && discountTerms.line.is_discounted)
                    $("#line_discount").val(discountTerms.line.discount);
                    $(".discount").each(function(){
                        if ($(this).val() === '') {
                            $(this).val(discountTerms.line.discount)
                        }
                    });
                }
            }
            if (postalAddressHtml.length > 0) {
                $("#postal").html(postalAddressHtml.join(' '))
            }
            if (deliveryAddressHtml.length > 0) {
                $("#delivery").html(deliveryAddressHtml.join(' '))
            }

            if (response.hasOwnProperty('vat_number')) {
                $("#id_vat_number").val(response.vat_number)
            }

        });
    };

    pricing.getSelectedInvoices = function()
    {
        var invoices = []
        $(".invoice-action").each(function(){
            if ($(this).is(":checked")) {
                invoices.push($(this).data('invoice-id'));
            }
        });
        return invoices;
    };

    pricing.bulkInvoicesAction = function(el)
    {
        var invoices = sales.getSelectedInvoices();
        var totalInvoices = invoices.length;
        if (totalInvoices === 0) {
            alert('Please select at lease one invoice');
        } else {
            var ajaxUrl = el.data('ajax-url');
            console.log( invoices )
            $.post(ajaxUrl, {
                invoices: invoices
            }, function(response){
                responseNotification(response);
            });
        }
    };

    pricing.singleInvoiceAction = function(el)
    {
        var invoices = sales.getSelectedInvoices();
        var totalInvoices = invoices.length;
        if (totalInvoices === 0) {
            alert('Please select at lease one invoice');
        } else if (totalInvoices >1) {
            alert('Please select one invoice to action');
        } else {
            var ajaxUrl = el.data('ajax-url');
            console.log( invoices )
            $.post(ajaxUrl, {
                invoices: invoices
            }, function(response){
                responseNotification(response);
            });
        }
    };

    pricing.reloadInvoiceItemRow = function(el)
    {
        var rowId = el.data('row-id');
        var ajaxUrl = el.data('ajax-url');
        var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
        $.get(ajaxUrl, {
            row_id:rowId,
            line_type: $("#type_"+ rowId +" option:selected").val()
        }, function(html) {
            $("#invoice_item_row_" + rowId).replaceWith(html);

            pricing.handleRow(rowId);

            if (chosenWidth > 0) {
                if ($(".chosen-select").hasClass('line-type"')) {
                    $(".chosen-select").chosen({'width': '14px'});
                } else {
                    $(".chosen-select").chosen({'width': chosenWidth + "px"});
                }
            } else {
                if ($(".chosen-select").hasClass('line-type"')) {
                    $(".chosen-select").chosen({'width': '14px'});
                } else {
                    $(".chosen-select").chosen({'width': '100px'});
                }
            }
        });

    };

    pricing.loadInvoiceItems = function(el, ajaxUrl)
    {
        var rowId = 0;
        var lastRow = $(".invoice_item_row").last();
        if (lastRow.length > 0) {
            console.log("Row was");
            console.log( lastRow.data('row-id') );
            rowId = parseInt(lastRow.data('row-id')) + 1;
        }
        pricing.loadInvoiceItemRow(el, ajaxUrl, rowId, lastRow)
    };

    pricing.loadInvoiceItemRow = function(el, ajaxUrl, rowId, lastRow)
    {
        var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
        var container = $("#invoice_items_placeholder");
        $.get(ajaxUrl, {
            row_id:rowId
        }, function(html) {
            if (lastRow !== undefined && lastRow.length > 0) {
                lastRow.after(html)
            } else {
                container.replaceWith(html);
            }
            var _lastRow = $(".invoice_item_row").last();
            if (_lastRow.length > 0) {
                rowId = parseInt(_lastRow.data('row-id'));
            }
            console.log( "Row --> ", rowId);

            pricing.handleRow(rowId);

            if (chosenWidth > 0) {
                $(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
                $(".line-type").chosen({'width': '40px'});
            } else {
                $(".chosen-select").not('.line-type').chosen({'width': '100px'});
                $(".line-type").chosen({'width': '40px'});
            }
            el.prop('disabled', false).text(el.data('html'));
        });
    };

    pricing.handleRow = function(rowId)
    {
        var lineDiscount = $("#line_discount");
        if (lineDiscount.val() !== '') {
            $("#discount_percentage_" + rowId).val(lineDiscount.val());
        }

        $("#inventory_item_" + rowId).on('change', function(){
            console.log( "Row --> ", rowId, " and load fields")
            var selectedOption = $("#inventory_item_"+ rowId +" option:selected");
            var price = selectedOption.data('price');
            var unit = selectedOption.data('unit');
            var binLocation = selectedOption.data('bin-location');
            var description = selectedOption.data('description');
            var vatId = selectedOption.data('vat-id');
            var unitId = selectedOption.data('unit-id');
            console.log( "price --> ", price, ' add to ', $("#price_" + rowId ));
            console.log( "unit --> ", unit, ' add to ', $("#unit_" + rowId ));
            $("#price_" + rowId ).val( price );
            if (unit !== undefined && unit !== '') {
                $("#unit_" + rowId).val(unit)
            }
            if (binLocation !== undefined && binLocation !== '') {
                $("#bin_location_" + rowId).val(binLocation)
            }
            if (description !== undefined && description !== '') {
                $("#description_" + rowId).val(description)
            }
            if (vatId !== undefined && vatId !== '') {
                $("#vat_code_" + rowId).val(vatId).trigger("chosen:updated").trigger('change');
            } else {
                $("#vat_code_" + rowId).val('').trigger("chosen:updated").trigger('change');
            }
        });

        $(".line-type").on('change', function(e){
            pricing.reloadInvoiceItemRow($(this));
        });

        $("#quantity_"+ rowId +"").on('blur', function(){
            pricing.calculateInventoryItemPrices(rowId);
        });

        $("#price_"+ rowId +"").on('blur', function(){
            pricing.calculateInventoryItemPrices(rowId);
        });

        $("#vat_code_"+ rowId).on('change', function(){
            var selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
            var vatPercentage = selectedVatOption.data('percentage');
            console.log('Vat code changes --> ', vatPercentage);
            if (vatPercentage !== undefined && vatPercentage !== '') {
                $("#vat_percentage_"+ rowId).val(vatPercentage);
            } else {
                $("#vat_percentage_"+ rowId).val(0);
            }
            pricing.calculateInventoryItemPrices(rowId);
        });

        $("#discount_percentage_"+ rowId +"").on('blur', function(){
            pricing.calculateInventoryItemPrices(rowId);
        });

    };

    pricing.getLineDiscount = function(rowId, priceExcl)
    {
        var discountPercentage = $("#discount_percentage_" + rowId);
        var discount = $("#discount_" + rowId);
        var discountAmount  = 0;
        if (discountPercentage.length > 0) {
            if (discountPercentage.val() !== '' && priceExcl !== '') {
                var discPercentage = parseFloat(discountPercentage.val());
                console.log('Discount percentage --> ', discPercentage);
                discountAmount = (discPercentage/100) * priceExcl;
                console.log('Discount amount --> ', discountAmount);
                discount.val(discountAmount);
            }
        }
        return discountAmount;
    };

    pricing.getLineVat = function(rowId, priceExcl)
    {
        var vatPercentage = $("#vat_percentage_" + rowId);
        var vat = $("#vat_amount_" + rowId);
        var vatAmount  = 0;
        if (vatPercentage.length > 0) {
            if (vatPercentage.val() !== '' && priceExcl !== '') {
                var percentage = parseFloat(vatPercentage.val());
                console.log('Vat percentage --> ', percentage);
                vatAmount = (percentage/100) * priceExcl;
                console.log('Vat amount --> ', vatAmount);
                vat.val(vatAmount);
            }
        }
        return vatAmount;
    };

    pricing.getPrice = function(rowId)
    {
        var price = $("#price_" + rowId);
        var priceExcl = 0;
        if (price !== undefined) {
            if (price.val() !== '') {
                priceExcl = parseFloat(accounting.unformat(price.val()));
            }
        }
        return priceExcl;
    };

    pricing.getInputValue = function(el)
    {
        var value = 0;
        if (el !== undefined && el.length > 0) {
            if (el.val() !== '') {
                value = parseFloat(el.val());
            }
        }
        return value;
    };

    pricing.setInputValue = function(el, value)
    {
        if (el !== undefined && el.length > 0) {
            el.val( value );
        }
    };


    pricing.isValidQuantity = function(quantityReceived, quantityOrdered)
    {
        if (quantityReceived > quantityOrdered) {
            return false
        }
        return true;
    };

    pricing.calculateShortQuantity = function(quantityReceived, quantityOrdered)
    {
        var shortQuantity = 0;
        if (quantityOrdered > 0) {
            if (quantityReceived <= quantityOrdered) {
                shortQuantity =  quantityOrdered - quantityReceived;
            }
        }
        return shortQuantity;
    };

    pricing.calculateInventoryItemPrices = function(rowId)
    {
        console.log("CALCULATING INVENTORY ROW ", rowId);

        var ordered = $("#quantity_ordered_" + rowId);
        var received = $("#quantity_" + rowId);
        var short = $("#quantity_short_" + rowId);
        var vatPercentage = $("#vat_percentage_" + rowId);


        var shortQuantity = 0;
        var vatAmount = 0;
        var priceExcl = 0;
        var discountAmount = 0;
        var totalVat = 0;
        var totalDiscount = 0;

        var quantityOrdered = sales.getInputValue(ordered, rowId);
        var quantityReceived = sales.getInputValue(received, rowId);

        if (!sales.isValidQuantity(quantityReceived, quantityOrdered)) {
            received.val(quantityOrdered);
            //backOrder.prop('disabled', true);
            alert('Please enter valid received quantity');
            return false;
        }
        var shortQuantity = sales.calculateShortQuantity(quantityReceived, quantityOrdered);
        pricing.setInputValue(short, shortQuantity);
        priceExcl = sales.getPrice();
        console.log( vatPercentage );
        console.log( vatPercentage.length );

        vatAmount = sales.getLineVat(rowId, priceExcl);
        discountAmount = sales.getLineDiscount(rowId, priceExcl);
        // if (vat !== undefined && vat.length > 0) {
        // 	if (vat.val() !== '') {
        //
        // 		vatAmount += parseFloat(accounting.unformat(vat.val()));
        // 	}
        // }
        if (discountAmount > 0) {
            priceExcl = priceExcl - discountAmount;
        }
        var totalExcl = priceExcl * quantityReceived;
        var priceIncl =  priceExcl + vatAmount;
        var totalInc = priceIncl * quantityReceived;
        if (vatAmount !== '') {
            totalVat = vatAmount * quantityReceived;
        }
        if (discountAmount !== '') {
            totalDiscount = discountAmount * quantityReceived;
        }

        console.log( " quantityReceived ", quantityReceived );
        console.log( " Price EXCL", priceExcl );
        console.log( " totalExcl ", totalExcl );
        console.log( " vatAmount ", vatAmount );
        console.log( " totalVat", totalVat );
        console.log( " discountAmount ", discountAmount );
        console.log( " priceIncl ", priceIncl );
        console.log( " totalInc", totalInc );

        // if (backOrder !== undefined) {
        // 	if (shortQuantity === 0) {
        // 		backOrder.prop('disabled', true);
        // 	} else {
        // 		backOrder.prop('disabled', false);
        // 	}
        // }

        $("#price_incl_" + rowId).html( accounting.formatMoney(priceIncl) );
        //$("#price_including_" + rowId).val( priceIncl );
        pricing.setInputValue($("#price_including_" + rowId), priceIncl);

        $("#vat_total_" + rowId).html( accounting.formatMoney(totalVat) );
        //$("#total_vat_" + rowId).val( totalVat );
        pricing.setInputValue($("#total_vat_" + rowId), totalVat);

        $("#discount_amount_" + rowId).html( accounting.formatMoney(totalDiscount) );
        //$("#total_discount_" + rowId).val(totalDiscount);
        pricing.setInputValue($("#total_discount_" + rowId), totalDiscount);

        $("#total_price_exl_" + rowId).html( accounting.formatMoney(totalExcl) );
        //$("#total_price_excluding_" + rowId).val( totalExcl );
        pricing.setInputValue($("#total_price_excluding_" + rowId), totalExcl);

        $("#total_price_incl_" + rowId).html( accounting.formatMoney(totalInc) );
        //$("#total_price_including_" + rowId).val( totalInc );
        pricing.setInputValue($("#total_price_including_" + rowId), totalInc);

        pricing.calculateTotals();
    };

    pricing.calculateTotals = function()
    {
        var totalOrdered = 0;
        var totalReceived = 0;
        var totalShort = 0;
        var priceExc = 0;
        var priceInc = 0;
        var totalPrice = 0;
        var totalTotalExc = 0;
        var totalTotalInc = 0;
        var totalVatAmount = 0;
        var totalDiscountAmount = 0;

        $(".ordered").each(function(){
            totalOrdered += parseInt($(this).val());
        });

        $(".received").each(function(){
            totalReceived += parseInt($(this).val());
        });
        $(".short").each(function(){
            totalShort += parseInt($(this).val());
        });
        $(".price").each(function(){
            totalPrice += parseFloat(accounting.unformat($(this).val()));
        });
        $(".price-exc").each(function(){
            priceExc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".price-incl").each(function(){
            priceInc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-price-excl").each(function(){
            totalTotalExc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-price-incl").each(function(){
            totalTotalInc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-vat-amount").each(function(){
            totalVatAmount += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-discount").each(function(){
            totalDiscountAmount += parseFloat(accounting.unformat($(this).val()));
        });

        $("#total_ordered").html( totalOrdered );
        $("#total_received").html( totalReceived );
        $("#total_short").html( totalShort );
        $("#total_price").html( accounting.formatMoney(totalPrice) );
        $("#price_excl").html( accounting.formatMoney(priceExc) );
        $("#price_incl").html( accounting.formatMoney(priceInc) );
        $("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
        $("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );
        $("#total_vat_amount").html( accounting.formatMoney(totalVatAmount) );
        $("#total_discount").html( accounting.formatMoney(totalDiscountAmount) );

    };

    pricing.init();

})();