(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var objectPosting = window.Docuflow.ObjectPosting = {};

    objectPosting.init = function()
	{
		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");

				console.log("This is is now ---> ")


				$("#save_object_posting_item").on("click", function(e){
					e.preventDefault()
					$(this).prop('disabled', true);
					objectPosting.saveObjectPostingItem($(this));
				});

				$(".chosen-select").chosen();

			});
		});


		$(".save-object-posting").on("click", function(e){
			e.preventDefault();
			var next = $(this).data("action");
			form.submit($(this), $("#estimate_form"), {"next": next});
		});


		var objectPostingItemsBody = $("#object_posting_items");
		if (objectPostingItemsBody.length > 0) {
			var ajaxUrl = objectPostingItemsBody.data("ajax-url");
			objectPosting.loadObjectPostingItems(objectPostingItemsBody, ajaxUrl);
		}

		$(".action-estimate").on('click', function (e) {
			console.log('Submit action ....');
			e.preventDefault();
			estimate.actionEstimate($(this));
		});

		$(".bulk-action").on('click', function(e){
			e.preventDefault();
			estimate.bulkInvoicesAction($(this));
		});

		$(".single-action").on('click', function(e){
			e.preventDefault();
			estimate.singleInvoiceAction($(this));
		});

		var idCustomer = $("#id_customer");
		idCustomer.on('change', function(e){
			e.preventDefault();
			console.log( customer );
			console.log( idCustomer );
			customer.loadDetails(idCustomer);
		});

		$("#id_is_deposit_required").on("change", function (e) {
			var depositRequired = $("#deposit_required");
			var idDeposit = $("#id_deposit");
			if ($(this).is(":checked")) {
				depositRequired.show();
			} else {
				idDeposit.val("");
				depositRequired.hide();
			}
		});

		$(".select-filter").on('change', function(event){

		});

		$(".filter").on('keyup', function(event){

		});
	};

	objectPosting.loadObjectPostingItems = function(el, ajaxUrl)
	{
		var ajaxUrl = el.data('ajax-url');
		$.get(ajaxUrl, {
		}, function(html) {
			el.html(html);

		})
	};

	objectPosting.saveObjectPostingItem = function(el)
	{
		var element = jQuery("#loader");
		var result = jQuery("#result");
		el.prop('disabled', false);
		element.show();
		console.log("<--- Submit form -->< ");
		jQuery("#object_item_posting_form").ajaxSubmit({
			dataType : 'json',
			success: function(response, statusText, xhr, $form)  {
				console.log( response )
				console.log( response.error )
				element.hide();
				if (response.error) {
					console.log( response.error )
					result.html(response.text ).addClass('alert alert-danger');
				} else {
					result.html(response.text ).addClass('alert alert-success');
					// if (response.redirect) {
					//     jQuery('#user_action_modal > div > div > div').load(response.url,function() {
					//         //
					//     });
					//     return false;
					// }
					// element.html( response.text ).show();
					// $(signUpForm).remove();
					// element.removeClass('alert alert-danger').addClass('alert alert-success');
					//
					// $(".fancybox-overlay").remove();
					// $(".fancybox-overlay-fixed").remove();
					//
					document.location.reload();
				}
			}
		});
		// jQuery("#object_item_posting_form").validate({
        //     submitHandler: function(form) {
        //     	console.log( jQuery("#object_item_posting_form") );
		//
        //     }
        // });
	};


    objectPosting.init();

})();