(function() {

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var form = window.Docuflow.Form || {};
    var message = window.Docuflow.Message || {};
    var invoice = window.Docuflow.Invoice || {};

	invoice.init = function() {
		$(document).on('click', '[data-modal]', function (event) {
			event.preventDefault();
			$.get($(this).data('modal'), function (data) {
				$(data).modal('show');

				// $("#approve_invoice_submit").on('click', function (event) {
				//     event.preventDefault();
				//     submitModalForm($("form[name='approve_invoice_form']"));
				// });

				$("#decline_invoice_submit").on('click', function (event) {
					event.preventDefault();
					form.submitModal($("form[name='decline_invoice_form']"));
				});

				$("#create_vat_report_submit").on('click', function (event) {
					event.preventDefault();
					form.submitModal($("form[name='create_vat_report']"));
				});

				$("#add_invoice_comment_submit").on('click', function (event) {
					event.preventDefault();
					form.submitModal($("form[name='comment_invoice_form']"));
				});

				$("#add_state_role_submit").on('click', function (event) {
					form.submitModal($("form[name='state_role_form']"));
					return false;
				});

				$("#add_invoice_account_comment_submit").on('click', function (event) {
					form.submitModal($("form[name='comment_invoice_account_form']"));
					return false;
				});

				$("#send_email_submit").on('click', function (event) {
					event.preventDefault();
					form.submitModal($("form[name='email_supplier_form']"));
				});

				$("#park_invoice_submit").on('click', function (event) {
					event.preventDefault();
					form.submitModal($("form[name='park_invoice_form']"));
				});

				$("#add_invoice_image_submit").on('click', function (event) {
					event.preventDefault();
					form.submitModal($("form[name='add_invoice_image_form']"));
				});

				$("#change_invoice_status_submit").on('click', function (event) {
					$("#text-reason").show();
					var element = $("#id_approval_comment");
					if (element.val() === '') {
						$(".has-error").remove()
						element.addClass('is-invalid');
						element.after('<div class="is-invalid-message has-error">Comment is required</div>');
						$(element.parent()).parent().addClass('text-danger');
						return false;
					} else {
						event.preventDefault();
						form.submitModal($("form[name='approve_invoice_decline_form']"));
					}
					return false;
				});

				$("#approve_invoice_submit").on('click', function (event) {
					event.preventDefault();
					invoice.approveInvoiceDecline($(this));
				});

				$("#reject_invoice_submit").on('click', function (event) {
					event.preventDefault();
					invoice.rejectInvoice($(this));
				});

				console.log("Modal form")
				console.log($(data).find("form"));
				console.log("Modal form submit button from form")
				console.log($($(data).find("form")).find(":submit"))
				console.log("Modal form submit button from modal footer")
				console.log($($(data).find("form")).find(".modal-footer :submit"))
				console.log("Modal form button from modal")
				console.log($($(data).find("form")).find(".modal-footer :button"))
				$($(data).find("form")).find(".modal-footer :submit")

				$("#distribute_invoice_accounts").on("click", function () {
					event.preventDefault();
					invoice.distributeAccounts($(this));
				});

				$("#create_invoice_account_distribution_submit").on('click', function (event) {
					form.submitModalForm($("#create_invoice_account_distribution"));
					return false;
				});

				$("#delete_invoice_account_distribution").on('click', function () {
					invoice.send($(this))
				});
				var date = new Date();
				// datepicker on all date input with class datepicker
				$(".datepicker").datepicker({
					dateFormat: 'yy-mm-dd'
				});


				$(".history_datepicker").datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: "yy-mm-dd",
					maxDate: -1,
					yearRange: "1900:" + (date.getFullYear())
				});

				var minDate = new Date()
				var dataMinDate = $("#id_start_date").data('min-date');
				if (dataMinDate) {
					date = new Date(dataMinDate);
					minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
					$(".future_datepicker").datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						minDate: minDate,
						yearRange: "-30:+100"
					});
				} else {
					$(".future_datepicker").datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						minDate: +1,
						yearRange: "-30:+100"
					});

				}

				$("#create_distribution_journal_submit").on('click', function (e) {
					form.submitModal($("#create_distribution_journals"));
					return false;
				});

				$(".chosen-select").chosen();

			});
		});

		var accountingPostingLine = $("#accounting_posting_line");

		var accountingPostings = $("#account_postings");
		var invoiceComments = $("#invoice_comments");
		var invoiceImage = $("#invoice_detail_container");

		if (accountingPostingLine.length > 0) {
			invoice.loadAccountPostingForm(accountingPostingLine);
		}
		if (accountingPostings.length > 0) {
			invoice.loadAccountPostings(accountingPostings)
		}

		if (invoiceImage.length > 0) {
			invoice.loadInvoiceImage(invoiceImage)
		}

		// $("#approve_invoice_submit").on('click', function(){
		// 	Invoice.saveInvoiceAccount();
		// })

		if (invoiceComments.length > 0) {
			invoice.loadInvoiceHistory(invoiceComments);
		}

		$("#logger_save_invoice_changes").on('click', function (e) {
			var form = $("#logger_edit_invoice");
			invoice.saveInvoiceChange(this, form);
			e.preventDefault();
		});

		$("#save_invoice_changes").on('click', function (e) {
			var form = $("#edit_invoice");
			invoice.saveInvoiceChange(this, form);
			e.preventDefault();
		});

		$(".save_invoice").on('click', function (e) {
			var form = $("#edit_invoice");
			invoice.saveInvoiceChange(this, form);
			e.preventDefault();
		});

		$("#validate_invoice").on('click', function (e) {
			invoice.validateInvoice(this);
			e.preventDefault();
		});

		$("#sign_invoice").on('click', function (e) {
			invoice.signInvoice(this);
			e.preventDefault();
		});

		$("#add_cooment").on('click', function (e) {
			invoice.addInvoiceComment(this);
			e.preventDefault();
		});

		$("#add_invoices_to_flow").on('click', function (e) {
			invoice.addToFlow(this);
			e.preventDefault();
		});

		$("#add_to_flow").on('click', function (e) {
			e.preventDefault();
			//addButtonSpinner($(this));
			invoice.addInvoiceToFlow(this);

			e.preventDefault();
		});

		$("#print_invoice").on('click', function (e) {
			invoice.printInvoice(this);
			e.preventDefault();
		});

		$("#email_supplier").on('click', function (e) {
			invoice.sendSupplerInvoiceEmail(this);
			e.preventDefault();
		});

		$("#park_invoice").on('click', function (e) {
			invoice.parkInvoice(this);
			e.preventDefault();
		});

		$("#unpark_invoice").on('click', function (e) {
			invoice.parkInvoice(this);
			e.preventDefault();
		});


		$(".delete_invoice_flow").on('click', function (e) {
			if (confirm('Are you sure you want to delete this invoice flow')) {
				invoice.deleteInvoiceFlow(this);
				e.preventDefault();
			}
		});

		$(".delete_invoice_flow_role").on('click', function (e) {
			if (confirm('Are you sure you want to delete this invoice flow role')) {
				invoice.deleteInvoiceFlowRole(this);
				e.preventDefault();
			}
		});

		$("form[id='certificate_form'] #id_supplier").on('change', function (e) {
			var supplier = $("form[id='certificate_form'] #id_supplier option:selected").val();
			if (supplier !== '' || supplier !== undefined) {
				invoice.loadSupplierData($(this), supplier);
			}
		});
		$("form[id='create_invoice'] #id_supplier").on('change', function (e) {
			var supplier = $("form[id='create_invoice'] #id_supplier option:selected").val();
			if (supplier !== '' || supplier !== undefined) {
				invoice.loadSupplierData($(this), supplier);
			}
		});
		$("form[id='logger_edit_invoice'] #id_supplier").on('change', function (e) {
			var supplier = $("form[id='logger_edit_invoice'] #id_supplier option:selected").val();
			if (supplier !== '' || supplier !== undefined) {
				invoice.loadSupplierData($(this), supplier);
			}
		});

		// $("#id_net_amount, #id_total_amount").on('blur', function(e){
		// 	var totalAmount = $("#id_total_amount").val();
		// 	var netAmount   = $("#id_net_amount").val();
		// 	if ( totalAmount !== '' && netAmount !== '') {
		// 	   Invoice.calculateVatAmount(totalAmount, netAmount);
		// 	}
		// })

		$("#id_invoice_type").on('change', function (e) {
			$("#save_invoice_adjustments").show()
		});

		$("#id_vat_amount, #id_total_amount").on('blur', function (e) {
			var totalAmount = $("#id_total_amount").val();
			var vatAmount = $("#id_vat_amount").val();
			invoice.loadVatCodes(vatAmount);
			if (totalAmount !== '' && vatAmount !== '') {
				invoice.calculateNetAmount(totalAmount, vatAmount);
				$("#save_invoice_adjustments").show()
			}
		});

		$("#id_invoice_number").on('blur', function () {
			var supplier_id = $("#id_supplier").val();
			var invoice_number = $(this).val();

			if (invoice_number !== '' && supplier_id !== '') {
				invoice.validateDocumentNumber($(this), supplier_id);
			}
		});

		$(".math-operation").on('blur', function (e) {
			invoice.calculateAmount($(this));
		});

		var formartMoney = debounce(function (el) {
			invoice.makeMoney(el);
		}, 650);

		// $(".money").on('blur', function(e){
		// 	Invoice.makeMoney($(this));
		// 	$(".money[readonly='readonly']").trigger('change')
		// });
		//
		// $(".money[readonly='readonly']").on('change', function(e){
		// 	console.log('Change');
		// 	console.log($(this));
		// 	Invoice.makeMoney($(this));
		// });


		$("#change_accounting_date").on('click', function () {
			event.preventDefault();
			var invoices = [];
			var modalUrl = $(this).data('ajax-url');
			$(".invoice-check").each(function () {
				if ($(this).is(":checked")) {
					invoices.push($(this).val())
				}
			});
			if (invoices.length > 0) {

				$.get(modalUrl, function (data) {
					$(data).modal('show');

					$("#accounting_date_datepicker").datepicker({
						onSelect: function (date) {
							$("#accounting_date").val(date)
						}
					})
					$("#update_accounting_date").on('click', function (c) {
						event.preventDefault();
						var ajaxUrl = $(this).data('ajax-url');
						$(this).html("Updating ...");

						$("#update_accounting_date_form").ajaxSubmit({
							url: ajaxUrl,
							target: '#loader',   // target element(s) to be updated with server response
							data: {
								'invoices': invoices
							},
							beforeSubmit: function (formData, jqForm, options) {
								var queryString = $.param(formData);
							},  // pre-submit callback
							error: function (data) {
								// var element;
								// // show error for each element
								// if (data.responseJSON && 'errors' in data.responseJSON) {
								// 	$.each(data.responseJSON.errors, function (key, value) {
								// 		element.addClass('is-invalid');
								// 		element.after('<div class="is-invalid-message">' + value[0] + '</div>');
								// 	});
								// }
								//
								// // flash error message
								// flash('danger', 'Errors have occurred.');
							},
							success: function (responseJson, statusText, xhr, $form) {
								message.responseNotification(responseJson);
							},  // post-submit callback
							dataType: 'json',
							type: 'post'
						});
					});

				})
			}
			return false;
		});

		$("#link_invoice_to_purchase_order").on("click", function () {
			$("#invoice_detail_container").html("Loading ....");
			invoice.loadPurchaseOrders($(this));
		});

		$("#pay_invoice").on("click", function (e) {
			e.preventDefault();
			var el = $(this);
			var supplier_id = el.data('supplier-id');
			var invoice_id = el.data('invoice-id');
			invoice.ajaxGet(el, {
				'supplier_id': supplier_id,
				'invoice_id': invoice_id
			})
		});

		$("#send_email").on("click", function (e) {
			invoice.ajaxGet($(this));
		});

		var fetchEmails = $("#fetch_emails");
		fetchEmails.on('click', function (e) {
			var ajaxUrl = $(this).data('ajax-url');
			e.preventDefault();
			fetchEmails.html("Processing ...");
			$.getJSON(ajaxUrl, function (response) {
				message.responseNotification(response);
				fetchEmails.html("<i class='fa fa-refresh'></i> Update");

			})
		});

		var createJournal = $(".create-journal");
		createJournal.on('click', function (e) {
			e.preventDefault();
			invoice.createJournal($(this));
		});


		$("#save_invoice").on('click', function (e) {
			e.preventDefault();
			invoice.saveInvoice();
		});

		$("#save_payment").on('click', function (e) {
			e.preventDefault();
			invoice.send($(this), {});
		});

		$("#upload_document").on("click", function (e) {
			e.preventDefault();
			invoice.savePayment($(this));
		});

		var pdfViewer = $("#pdf_viewer");
		if (pdfViewer.length > 0) {
			var pdf = pdfViewer.data('pdf-url');
			PDFObject.embed(pdf, "#pdf_viewer");
		}

		$("#id_flow_proposal").on("change", function (e) {
			invoice.getFlowProposalUrl($(this));
		});

		$("#view_flow_proposal").on("click", function (e) {
			invoice.showWorkflowFlowProposal($(this));
		});

		$("#save_certificate").on('click', function (e) {
			var form = $("#certificate_form");
			invoice.saveDocument(this, form);
			e.preventDefault();
		});

		$("#update_certificate").on('click', function (e) {
			var form = $("#certificate_form");
			invoice.saveDocument(this, form);
			e.preventDefault();
		});

		$("#invoice_payment").on('click', function (e) {
			e.preventDefault();
			invoice.openPayments($(this));
		});
	};



	invoice.openPayments = function(el)
	{
		var paymentsUrl = el.data('ajax-url');
		if (paymentsUrl) {
			$.get(paymentsUrl, function(html){
				$("#invoice_pdf").html(html);
				$("#close_invoice_payment_link").on('click', function(e){
					invoice.loadInvoiceImage($(this));
				});
			});
		}
	};

	invoice.showWorkflowFlowProposal = function(el, ajaxUrl)
	{
		var ajaxUrl = ajaxUrl || el.data('ajax-url');
		if (ajaxUrl) {
			$.get(ajaxUrl, function(html){
				$("#invoice_pdf").html(html);
				$("#close_invoice_image").on('click', function(e){
					invoice.loadInvoiceImage($(this));
				});
			});
		}
	};

	invoice.invoicegetFlowProposalUrl = function(el)
	{
		var invoiceId = $("#view_flow_proposal").data('invoice-id')
		var ajaxUrl = el.data('ajax-url');
		if (ajaxUrl && el.val()) {
			ajaxUrl = ajaxUrl + '?workflow_id=' +  el.val() + "&invoice_id=" + invoiceId;

			$("#view_flow_proposal").attr('data-ajax-url', ajaxUrl);
			$("#proposal_link").show();

			$("#view_flow_proposal").on("click", function(e){
				invoice.showWorkflowFlowProposal($(this), ajaxUrl)
			});
		}
	};

	invoice.loadVatCodes = function(vatAmount)
	{
		var ajaxUrl = $("#vat_codes").data('vat-codes-url');
		if (ajaxUrl) {
			$("#id_vat_code_chosen").remove();
			var params = {'vat_amount': vatAmount}
			$.getJSON(ajaxUrl, params, function(vatCodes){
				var html = [];
				html.push("<select id='id_vat_code' name='vat_code' class='chosen-select'>");
				$.each(vatCodes, function(index, vatCode){
					if (vatCode['is_default']) {
						html.push("<option value='" + vatCode['id']+"' selected='selected'>"+ vatCode['label'] +"</option>");
					} else {
						html.push("<option value='" + vatCode['id']+"'>"+ vatCode['label'] +"</option>");
					}
				});
				html.push("<select id='id_vat_code' name='vat_code'>")
				$("#id_vat_code").replaceWith( html.join(' ') )

				$(".chosen-select").chosen().css({'width': '353px'})
				$("#id_vat_code_chosen").css({'width': '353px'})
			});
		}
	};

	invoice.savePayment = function(el)
	{
		var form = $("#payment_form");
		form.submitModal(form)
	};

	invoice.distributeAccounts = function(el)
	{
	   var accountIds = [];
	   $(".distribution-invoice-account").each(function(){
			if ($(this).is(":checked")) {
				accountIds.push($(this).val());
			}
	   });

	   var startDate = $("#id_start_date");
	   var nbMonth = $("#id_month");
	   if (accountIds.length === 0) {
		 alert("Please select the accounts")
	   } else if(!startDate.val()) {
		  alert("Please select start date")
	   } else if(!nbMonth.val()) {
		  alert("Please enter number of months")
	   } else {
		   var self = $("#" + el.attr('id'));
		   var container = $("#account_distribution");
		   container.html("Loading . . .");
		   var url = el.data('ajax-url');
		   $.get(url, {
			 months: nbMonth.val(),
			 start_date: startDate.val(),
			 account_ids: accountIds.join(',')
		   }, function(html){
			   container.html(html);
		   });
	   }
	};

	invoice.send = function(el, params)
	{
		var ajaxUrl = el.data('ajax-url');
		if (ajaxUrl) {
			$.getJSON(ajaxUrl, params, function(response){
				message.responseNotification(response)
			});
		}
	};

	invoice.ajaxGet = function (el, params)
	{
		var ajaxUrl = el.data('ajax-url');
		if (ajaxUrl) {
			$.getJSON(ajaxUrl, params, function(response){
				message.responseNotification(response);
			});
		}
	};

	invoice.saveDocument = function(el)
	{
		var form = $("#certificate_form");
		form.submitModal(form)
	};

	invoice.saveInvoice = function(el)
	{
		var form = $("#create_invoice");
		form.submitModal(form);
	};

	invoice.createJournal =  function(el) {
	   var self = $("#" + el.attr('id'));
	   var text = el.text();
	   self.html("Processing ...");
	   var url = el.data('ajax-url');
	   $.get(url, {
		 period: $("#period").val()
	   }, function(response){
		   if (response.error) {
			  self.html(text);
		   }
		   message.responseNotification(response);
	   }, 'json');
	};

	invoice.loadInvoiceImage = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		var fileUrl = el.data('file-url');
		if (fileUrl) {
			 var pdf  = invoicePdf.data('pdf-url');
			 PDFObject.embed(pdf, "#invoice_pdf");
		} else if (ajaxUrl) {
			$.get(ajaxUrl, function(html){
				var containerDiv;
				if ($("#purchase_order_detail_container").length > 0) {
					containerDiv = $("#purchase_order_detail_container");
				} else if ($("#purchase_orders").length > 0) {
					containerDiv = $("#purchase_orders");
				} else if($("#invoice_detail_container").length > 0) {
					containerDiv = $("#invoice_detail_container");
				}
				containerDiv.html(html);
				var invoicePdf = $("#invoice_pdf");
				if (invoicePdf.length) {
					 var pdf  = invoicePdf.data('pdf-url');
					 PDFObject.embed(pdf, "#invoice_pdf");
				}
			});
		}
	};

	invoice.loadSupplierData = function(el, supplier)
	{
		var ajaxUrl = $("#supplier_detail_url").data('ajax-url');
		$.getJSON(ajaxUrl, {
			supplier : supplier,
		}, function(response){
			$("#id_supplier_name").val(response['name']);
			$("#id_supplier_number").val(response['code']);
			$("#id_vat_number").val(response['vat_number']);
			$("#id_supplier_account_ref").val(response['company_number']);
		});
	};

	invoice.payInvoices = function(el, invoices)
	{
		if (invoices.length > 0) {
			var ajaxUrl = el.data('ajax-url');
			$.getJSON(ajaxUrl, {
				invoices :invoices.join(','),
				payment_method_id: $("#payment_method_id").val()
			}, function(response){
				message.responseNotification(response)
			});
		}
	};

	invoice.addDiscount = function(el)
	{
		var invoiceId = el.val()
		var totalDiscountEl = $("#id_total_discount")
		var discountAmount = parseFloat($("#invoice_discount_" + invoiceId).val())
		var totalDiscount = 0
		if (totalDiscountEl.val() !== '') {
			totalDiscount = parseFloat(totalDiscountEl.val())
		}
		totalDiscount += discountAmount
		totalDiscountEl.val( totalDiscount.toFixed(2) )
		invoice.calculateNetPayment()
	};

	invoice.removeDiscount = function(el)
	{
		var invoiceId = el.val()
		var totalDiscountEl = $("#id_total_discount")
		var discountAmount = parseFloat($("#invoice_discount_" + invoiceId).val())
		var totalDiscount  = 0
		if (totalDiscountEl.val() !== '') {
			totalDiscount = parseFloat(totalDiscountEl.val())
		}
		totalDiscount -= discountAmount
		totalDiscountEl.val( totalDiscount.toFixed(2) )
		invoice.calculateNetPayment()
	};

	invoice.calculateNetPayment = function()
	{
		var selectedDiscount = $("#id_total_discount").val()
		var discountAllowed = $("#id_discount_disallowed").val()
		var totalDiscount = 0
		if (discountAllowed !== '') {
			totalDiscount += parseFloat(discountAllowed);
		}
		if (selectedDiscount !== '') {
			totalDiscount += parseFloat(selectedDiscount);
		}
		var total = $("#id_total_amount").val()
		var netAmount = parseFloat(total) - totalDiscount;
		$("#id_net_amount").val( netAmount.toFixed(2) )
	};

	invoice.allChecked = function(elClass, elId)
	{
		var countChecked = 0
		var countCheckboxes = 0
		elClass.each(function(){
			countCheckboxes++;
			if ($(this).is(":checked")) {
				countChecked++;
			}
		})
		//console.log(elClass, "chjecked --> ", countChecked, " count checkboxes --> ", countCheckboxes, elId)
		if (countCheckboxes > 0  && countChecked > 0) {
			if (countCheckboxes === countChecked) {
				//console.log("Make the element checked")
				elId.attr("checked", "checked")
			} else {
				elId.removeAttr("checked", "checked")
			}
		}
	};

	invoice.loadPurchaseOrders = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		$.get(ajaxUrl, {

		}, function(html) {

			if ($("#purchase_order_detail_container").length > 0) {
				$("#purchase_order_detail_container").replaceWith(html);
			} else if ($("#purchase_orders").length > 0) {
				$("#purchase_orders").replaceWith(html);
			} else if($("#invoice_detail_container").length > 0) {
				$("#invoice_detail_container").replaceWith(html);
			}

			$("#purchase_orders>tr>td:not(.actions)").on('click', function(){

				var purchaseDetails = $("#purchase_order_details");
				var detailUrl = $(this).parent('tr').data('detail-url');
				purchaseDetails.html('loading ...');
				$.get(detailUrl, function(po_html){
					purchaseDetails.html(po_html)
				});
				return false;
			});

			$(".purchase-order-line>td").nextUntil($('.not-editable'), 'td').on('click', function(e){
				var lineId = $(this).data('purchase-order-line-id');
				$("#add_purchase_order_line").attr('disabled', 'disabled');
				$("#update_purchase_order_line").removeAttr('disabled');
				invoice.loadPurchaserOrderForm($(this), lineId);
				return false;
			});

			$("#save_purchase_order_invoice_link").on('click', function(e){
				var po_orders = [];
				$(".purchase-order-check").each(function(){
					if ($(this).is(":checked")) {
						po_orders.push($(this).val())
					}
				});
				if (po_orders.length > 0) {

					invoice.saveInvoicePurchaseOrders($(this), po_orders);
				}
			})
			$("#close_purchase_order_invoice_link").on('click', function(){
				invoice.loadInvoiceImage($(this));
			});
		});
	};

	invoice.saveInvoicePurchaseOrders = function(el, po_orders) {
		var ajaxUrl = el.data('ajax-url');
		console.log( ajaxUrl )
		$.getJSON(ajaxUrl, {
			purchase_orders: po_orders.join(',')
		}, function(response){
			message.responseNotification(response);
			if (!response.error) {
				invoice.loadPurchaseOrders($("#link_invoice_to_purchase_order"))
			}
		});
	};

	invoice.validateDocumentNumber = function(el, supplier_id){
		var ajaxUrl = $("#validate_document_number").data('validate-document-number-url');
		$.get(ajaxUrl, {
			document_number: el.val(),
			supplier_id: supplier_id
		}, function(responseJson) {
			if (responseJson.error) {
				displayFormError(el, responseJson.text);
				el.addClass('text-danger').addClass('error')
			} else {
				message.responseNotification(responseJson);
				removeFormError(el)
				el.removeClass('text-danger').removeClass('error');
			}
		}, 'json');
	};

	invoice.makeMoney = function(el){
		var result = accounting.formatMoney($(el).val());
		var money = parseFloat($(el).val())
		console.log('Format mponey ', $(el).val(), result, money.toLocaleString())
		//el.val(result)
		//return false;
	};

	invoice.approveInvoiceDecline = function(el){
		var ajaxUrl = $(el).data('ajax-url');
		$.get(ajaxUrl, function(responseJson) {
			message.responseNotification(responseJson);
			if ('redirect' in responseJson) {
				document.location.href = responseJson.redirect
			}
		}, 'json');
	};

	invoice.rejectInvoice = function(el){
		var ajaxUrl = $(el).data('ajax-url');
		$.get(ajaxUrl, function(responseJson) {
			message.responseNotification(responseJson);
			if ('redirect' in responseJson) {
				document.location.href = responseJson.redirect
			}
		}, 'json');
	};

	invoice.calculateAmount = function(el)
	{
		var mathOperationElement = $("#math_operation");
		var ajaxUrl = $(this).data('ajax-url');
		if (mathOperationElement.length > 0) {
			ajaxUrl = mathOperationElement.data('ajax-url');
		}

		$.get(ajaxUrl, {
			amount: $(el).val(),
			csrf_token: $("input[name='csrfmiddlewaretoken']").val()
		}, function(responseJson){
			if (responseJson.error === false) {
				if ('amount' in responseJson) {
					$(el).val(responseJson.amount)
				}
			} else {
				message.responseNotification(responseJson);
			}
		}, 'json');
	};

	invoice.calculateVatAmount = function(totalAmount, netAmount)
	{
		var vatAmount = parseFloat(totalAmount) - parseFloat(netAmount)
		if (vatAmount > 0) {
			$("#id_vat_amount").val(parseFloat(vatAmount).toFixed(2))
		}
	};

	invoice.calculateNetAmount = function(totalAmount, vatAmount)
	{
		var netAmount = parseFloat(totalAmount) - parseFloat(vatAmount);
		console.log('Vat amount ', vatAmount, 'total ', parseFloat(totalAmount), ' net ', parseFloat(vatAmount));
		if (netAmount > 0) {
			$("#id_net_amount").val(parseFloat(netAmount).toFixed(2))
		}
	};

	invoice.deleteInvoiceFlow = function(el)
	{
		var ajaxUrl = $(el).data('ajax-url');
		$.get(ajaxUrl, {
		}, function(responseJson){
			message.responseNotification(responseJson);
			if ('redirect' in responseJson) {
				document.location.href = responseJson.redirect
			}
		}, 'json');
	};

	invoice.deleteInvoiceFlowRole = function(el)
	{
		var ajaxUrl = $(el).data('ajax-url');
		$.get(ajaxUrl, {
		}, function(responseJson){
			message.responseNotification(responseJson);
			if ('redirect' in responseJson) {
				document.location.href = responseJson.redirect
			}
		}, 'json');
	};

	invoice.openEditAccountPosting = function(el, accountId)
	{
		$("#add_invoice_account_posting").attr('disabled', 'disabled');
		$("#update_invoice_account_changes").removeAttr('disabled');
		invoice.loadAccountPostingForm($(el), accountId);
	};

	invoice.loadAccountPostings = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		var invoiceId = el.data('invoice-id');
		$.get(ajaxUrl, {
			invoice_id: invoiceId,
			type: 'postings'
		}, function(html){
			el.replaceWith(html);

			$(".invoice-account-posting-line>td").nextUntil($('.not-editable'), 'td').on('click', function(e){
				var accountId = $(this).data('account-id');
				$("#add_invoice_account_posting").attr('disabled', 'disabled');
				$("#update_invoice_account_changes").removeAttr('disabled');
				invoice.loadAccountPostingForm($(this), accountId);
				return false;
			});

			$(".delete-account-posting").on('click', function(){
				if (confirm('Are you sure you want to delete this account posting line')) {
					invoice.deleteAccountPosting($(this));
				}
				return false;
			});
		});
	};

	invoice.loadAccountPostingForm = function(el, accountId)
	{
		var ajaxUrl = el.data('ajax-url');
		var invoiceId = el.data('invoice-id');
		$.get(ajaxUrl, {
			invoice_id: invoiceId,
			account_id: accountId
		}, function(html){
			$("#accounting_posting_line").html(html);
			$("#id_calculation_method").on('change', function(){
				var selectedValue = $(this).val()
				if (selectedValue == 'percentage') {
					$("#percentage").show();
					$("#amount").hide();
				} else {
					$("#percentage").hide();
					$("#amount").show();
				}
				return false;
			});

			$("#approve_invoice_submit").on('click', function(e){
				invoice.saveInvoiceAccount();
				return false;
			});

			$("#edit_invoice_account_submit").on('click', function(){
				invoice.updateInvoiceAccount();
				return false;
			})

			$("#update_invoice_account_changes").on('click', function(e){
				e.stopImmediatePropagation();
				invoice.saveInvoiceAccount($("#invoice_account_form"));
				return false;
			});
			$("#add_invoice_account_posting").on("click", function(e){
				e.stopImmediatePropagation();
				invoice.saveInvoiceAccount($("#invoice_account_form"));
				e.preventDefault()
			 });

			$("#id_amount").on('blur', function(e){
				invoice.calculateAmount($(this));
			});

			$(".math-operation").on('blur', function(e){
				invoice.calculateAmount($(this));
			});

			$(".money").on('blur', function(e){
				invoice.makeMoney($(this));
			});

			$(".chosen-select").chosen();
		});
	};

	invoice.deleteAccountPosting = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		var accountId = el.data('account-id');
		$.get(ajaxUrl, {
			account_id: accountId
		}, function(responseJson){
			message.responseNotification(responseJson);
			var accountingPostings = $("#account_postings");
			if (accountingPostings) {
				invoice.loadAccountPostings(accountingPostings)
			}
		}, 'json');
	};

	invoice.loadPurchaserOrderLines = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		var purchaserOrderId = el.data('invoice-id');
		$.get(ajaxUrl, {
			invoice_id: purchaserOrderId,
		}, function(html){
			el.replaceWith(html);

			$(".purchase-order-line>td").on('click', function(e){
				var purchaserOrderId = $(this).parent().data('purchase-order-line-id');
				console.log(purchaserOrderId);
				$("#add_purchase_order_line").attr('disabled', 'disabled');
				$("#update_purchase_order_line").removeAttr('disabled');
				invoice.loadPurchaserOrderForm($(this).parent(), purchaserOrderId);
				return false;
			});

			$(".delete-purchase-order").on('click', function(){
				if (confirm('Are you sure you want to delete this purchase order line')) {
					invoice.deletePurchaseOrderLine($(this));
				}
				return false;
			});
		});
	};

	invoice.loadPurchaserOrderForm = function(el, lineId)
	{
		if (el) {
			var ajaxUrl = el.data('ajax-url');
			var lineId = el.data('purchase-order-line-id');
			console.log( ajaxUrl )
			console.log( lineId )
			$.get(ajaxUrl, {
				line_id: lineId
			}, function(html){
			   $("#purchase_order_lines").html(html);
				$("#id_calculation_method").on('change', function(){
					var selectedValue = $(this).val()
					if (selectedValue == 'percentage') {
						$("#percentage").show();
						$("#amount").hide();
					} else {
						$("#percentage").hide();
						$("#amount").show();
					}
					return false;
				});

				$("#approve_invoice_submit").on('click', function(e){
					invoice.saveInvoiceAccount();
					return false;
				});

				$("#update_purchase_order_line").on('click', function(e){
					e.stopImmediatePropagation();
					form.submit($(this), $("#purchaser_order_form"));
				})

				$("#update_invoice_account_changes").on('click', function(e){
					e.stopImmediatePropagation();
					invoice.saveInvoiceAccount($("#purchase_order_form"));
					return false;
				});
				$("#add_purchase_order_line").on("click", function(e){
					e.stopImmediatePropagation();
					invoice.savePurchaseOrderLine($("#purchase_order_form"));
					e.preventDefault()
				});

				$("#id_amount").on('blur', function(e){
					invoice.calculateAmount($(this));
				});

				$(".math-operation").on('blur', function(e){
					Invoice.calculateAmount($(this));
				});

				$(".money").on('blur', function(e){
					Invoice.makeMoney($(this));
				});

				$(".chosen-select").chosen();
			});
		}
	};

	invoice.savePurchaseOrderLine = function(form)
	{
		form = form || $("#purchase_order_form");
		// disable extra form submits
		form.addClass('submitted');
		var loader = $("#loader");
		loader.show();
		// remove existing alert & invalid field info
		$('.alert-fixed').remove();
		$('.is-invalid').removeClass('is-invalid');
		$('.is-invalid-message').remove();
		form.ajaxSubmit({
			url: form.attr('action'),
			target:        '#loader',   // target element(s) to be updated with server response
			beforeSubmit:  function(formData, jqForm, options) {
				var queryString = $.param(formData);
			},  // pre-submit callback
			error: function (data) {
				var element;
				// show error for each element
				if (data.responseJSON && 'errors' in data.responseJSON) {
					$.each(data.responseJSON.errors, function (key, value) {
						element.addClass('is-invalid');
						element.after('<div class="is-invalid-message">' + value[0] + '</div>');
					});
				}

				// flash error message
				flash('danger', 'Errors have occurred.');
			},
			success:       function(responseJson, statusText, xhr, $form)  {
				loader.hide();
				if (responseJson.error) {
					if (responseJson.hasOwnProperty('errors')) {
						message.highlightFormErrors(responseJson.errors)
					}
					alertErrors(responseJson.text)
				} else {
					form.find(":input:not('select')").each(function(){
						$(this).val('')
					})
					form.find(".chosen-select").val('').trigger("chosen:updated");
					message.responseNotification(responseJson);
					var purchaseOrderLines = $("#purchase_orders");
					if (purchaseOrderLines) {
						Invoice.loadPurchaserOrderForm($("#purchase_order_lines"))
						$("#update_invoice_account_changes").attr('disabled', 'disabled');
						$("#add_invoice_account_posting").removeAttr('disabled');
						invoice.loadPurchaserOrderLines(purchaseOrderLines);
					}

				}
			},  // post-submit callback
			dataType: 'json',
			type: 'post'
			// $.ajax options can be used here too, for example:
			//timeout:   3000
		});
	};

	invoice.loadInvoiceHistory = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		var invoiceId = el.data('invoice-id');
		$.get(ajaxUrl, {
			invoice_id: invoiceId
		}, function(html){
			el.html(html);
		});
	};

	invoice.addAccount = function(el)
	{
		var ajaxUrl			   = $(el).data('ajax-url');
		var invoiceId 		   = $(el).data('invoice-id');
		var accountId          = $("#id_account");
		var percentageOfAmount = $("#id_percentage");
		var responseDiv 	   = $("#response");
		if (accountId.length > 0 && accountId.val() === '') {
			alert('Please select the account.');
			return false;
		} else if (percentageOfAmount.length > 0 && percentageOfAmount.val() === ''){
			alert('Please enter the percentage of the amount to assign the account.');
			return false;
		} else {
			var _counter = $("#counter");
			var accountDiv = $("#account");

			$.getJSON(ajaxUrl, {
				invoice_id: invoiceId,
				account_id: accountId.val(),
				percentage_of_amount: percentageOfAmount.val()
			}, function(responseJson){
				if (responseJson.error) {
					responseDiv.html(responseJson.text);
				} else {
					responseDiv.html(responseJson.text);
					var html = [];

					$.each(responseJson['invoice_data'].invoice_accounts, function(index, account){

						html.push("<tr>");
						html.push("<td>");
							html.push(account.account);
						html.push("</td>");
						html.push("<td>");
							html.push(account.percentage_of_amount);
						html.push("</td>");
						html.push("<td>");
							html.push(account.amount);
						html.push("</td>");
						html.push("<td></td>");
						html.push("</tr>");
					});
					$("#accounts_table>tbody").empty().append( html.join(' ') )
					$("#accounts_list").show();
					$("#_percentage_of_total_amount").html( responseJson['invoice_data'].outstanding )
					$("#id_account").val('')
					$("#id_percentage").val('')
				}

			});
		}
	};

	invoice.updateInvoiceAccount = function(form)
	{
		var form = $("#edit_invoice_account_form");
		// disable extra form submits
		form.addClass('submitted');
		var loader = $("#loader");
		loader.show();
		// remove existing alert & invalid field info
		$('.alert-fixed').remove();
		$('.is-invalid').removeClass('is-invalid');
		$('.is-invalid-message').remove();
		form.ajaxSubmit({
			url: form.attr('action'),
			target:        '#loader',   // target element(s) to be updated with server response
			beforeSubmit:  function(formData, jqForm, options) {
				var queryString = $.param(formData);
			},  // pre-submit callback
			error: function (data) {
				var element;
				// show error for each element
				if (data.responseJSON && 'errors' in data.responseJSON) {
					$.each(data.responseJSON.errors, function (key, value) {
						element.addClass('is-invalid');
						element.after('<div class="is-invalid-message">' + value[0] + '</div>');
					});
				}

				// flash error message
				flash('danger', 'Errors have occurred.');
			},
			success:       function(responseJson, statusText, xhr, $form)  {
				loader.hide();

				message.responseNotification(responseJson);
				var accountingPostings = $("#account_postings");
				if (accountingPostings) {
					invoice.loadAccountPostings(accountingPostings)
				}
			},  // post-submit callback
			dataType: 'json',
			type: 'post'
			// $.ajax options can be used here too, for example:
			//timeout:   3000
		});
	};

	invoice.saveInvoiceAccount = function(formEl)
	{
		formEl = formEl || $("#create_invoice_account_form");
		// disable extra form submits
		formEl.addClass('submitted');
		var loader = $("#loader");
		loader.show();
		// remove existing alert & invalid field info
		$('.alert-fixed').remove();
		$('.is-invalid').removeClass('is-invalid');
		$('.is-invalid-message').remove();
		formEl.ajaxSubmit({
			url: formEl.attr('action'),
			target:        '#loader',   // target element(s) to be updated with server response
			beforeSubmit:  function(formData, jqForm, options) {
				var queryString = $.param(formData);
			},  // pre-submit callback
			error: function (data) {
				var element;
				// show error for each element
				if (data.responseJSON && 'errors' in data.responseJSON) {
					$.each(data.responseJSON.errors, function (key, value) {
						element.addClass('is-invalid');
						element.after('<div class="is-invalid-message">' + value[0] + '</div>');
					});
				}

				// flash error message
				flash('danger', 'Errors have occurred.');
			},
			success:       function(responseJson, statusText, xhr, $form)  {
				loader.hide();
				if (responseJson.error) {
					if (responseJson.hasOwnProperty('errors')) {
						message.highlightFormErrors(responseJson.errors)
					}
					alertErrors(responseJson.text)
				} else {
					formEl.find(":input:not('select')").each(function(){
						$(this).val('')
					});
					formEl.find(".chosen-select").val('').trigger("chosen:updated");
					message.responseNotification(responseJson);
					var accountingPostings = $("#account_postings");
					if (accountingPostings) {
						invoice.loadAccountPostingForm($("#accounting_posting_line"))
						$("#update_invoice_account_changes").attr('disabled', 'disabled');
						$("#add_invoice_account_posting").removeAttr('disabled');
						invoice.loadAccountPostings(accountingPostings);
					}

				}
			},  // post-submit callback
			dataType: 'json',
			type: 'post'
			// $.ajax options can be used here too, for example:
			//timeout:   3000
		});
	};

	invoice.saveInvoiceChange = function(el, formEl, notify)
	{
		var ajaxUrl = $(el).data('ajax-url');
		formEl.ajaxSubmit({
			url: ajaxUrl,
			target:        '#loader',   // target element(s) to be updated with server response
			beforeSubmit:  function(formData, jqForm, options) {
				var queryString = $.param(formData);
			},  // pre-submit callback
			error: function (data) {
				var element;
				// show error for each element
				if (data.responseJSON && 'errors' in data.responseJSON) {
					$.each(data.responseJSON.errors, function (key, value) {
						element.addClass('is-invalid');
						element.after('<div class="is-invalid-message">' + value[0] + '</div>');
					});
				}

				// flash error message
				flash('danger', 'Errors have occurred.');
			},
			success:       function(responseJson, statusText, xhr, $form)  {
				if (notify === undefined || notify === true) {
					message.responseNotification(responseJson)
				}
			},  // post-submit callback
			dataType: 'json',
			type: 'post'
			// $.ajax options can be used here too, for example:
			//timeout:   3000
		});
	};

	invoice.saveAndSignInvoice = function(el)
	{
		var ajaxUrl = $(el).data('ajax-url');
		var ajaxSaveUrl = $("#save_invoice_changes").data('ajax-url');
		var formEl = $("#edit_invoice");
		formEl.ajaxSubmit({
			url:  ajaxSaveUrl,
			target: '#loader',   // target element(s) to be updated with server response
			beforeSubmit:  function(formData, jqForm, options) {
				var queryString = $.param(formData);
			},  // pre-submit callback
			error: function (data) {
				var element;
				// show error for each element
				if (data.responseJSON && 'errors' in data.responseJSON) {
					$.each(data.responseJSON.errors, function (key, value) {
						element.addClass('is-invalid');
						element.after('<div class="is-invalid-message">' + value[0] + '</div>');
					});
				}

				// flash error message
				flash('danger', 'Errors have occurred.');
			},
			success:       function(responseJson, statusText, xhr, $form)  {
				$.getJSON(ajaxUrl, {
					csrf_token: window.csrftoken
				}, function(response) {
					message.responseNotification(response)
				});
			},  // post-submit callback
			dataType: 'json',
			type: 'post'
		});
	};

	invoice.signInvoice = function(el)
	{
		var ajaxUrl = $(el).data('ajax-url');
		var ajaxSaveUrl = $(el).data('ajax-update-invoice-url');
		if (ajaxSaveUrl) {
			var form = $("#edit_invoice");
			form.ajaxSubmit({
				url:  ajaxSaveUrl,
				target: '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					if (responseJson.error) {
						message.responseNotification(responseJson);
					} else {
						$.getJSON(ajaxUrl, {
							csrf_token: window.csrftoken
						}, function(response) {
							message.responseNotification(response);
						});
					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		} else {
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response) {
				message.responseNotification(response);
			});
		}
	};

	invoice.addInvoiceComment = function(el)
	{
		var ajaxUrl = $(el).data('ajax-url');
		$.getJSON(ajaxUrl, {
			csrf_token: window.csrftoken
		}, function(response){
			message.responseNotification(response)
			invoice.loadInvoiceHistory($("#invoice_comments"));
		});
	};

	invoice.printInvoice = function(el)
	{
		var ajaxUrl   = $(el).data('ajax-url');
		$.getJSON(ajaxUrl, {
			csrf_token: window.csrftoken
		}, function(response){
			message.responseNotification(response)
		});
	};

	invoice.sendSupplerInvoiceEmail = function(el)
	{
		var ajaxUrl   = $(el).data('ajax-url');
		$.getJSON(ajaxUrl, {
			csrf_token: window.csrftoken
		}, function(response){
			message.responseNotification(response)
		});
	};

	invoice.parkInvoice = function(el)
	{
		var self 	  = this;
		var ajaxUrl   = $(el).data('ajax-url');
		$.getJSON(ajaxUrl, {
			csrf_token: window.csrftoken
		}, function(response){
			message.responseNotification(response)
			if ('redirect' in response) {
				setTimeout(function(){
					document.location.href = response.redirect
				}, 1500)
			}
		});
	};

	invoice.addInvoiceToFlow = function(el)
	{
		var flowAjaxUrl = $(el).data('ajax-url');
		var form = $("#logger_edit_invoice");
		var ajaxUrl = $("#logger_save_invoice_changes").data('ajax-url');
		form.ajaxSubmit({
			url: ajaxUrl,
			target:        '#loader',   // target element(s) to be updated with server response
			beforeSubmit:  function(formData, jqForm, options) {
				var queryString = $.param(formData);
			},  // pre-submit callback
			error: function (data) {
				var element;
				// show error for each element
				if (data.responseJSON && 'errors' in data.responseJSON) {
					$.each(data.responseJSON.errors, function (key, value) {
						element.addClass('is-invalid');
						element.after('<div class="is-invalid-message">' + value[0] + '</div>');
					});
				}
			},
			success:       function(responseJson, statusText, xhr, $form)
			{
				message.responseNotification(responseJson)
				if (responseJson.error) {
					//removeButtonSpinner($(el));
				} else {
					//removeButtonSpinner($(el));
					$.getJSON(flowAjaxUrl, {
						csrf_token: window.csrftoken
					}, function(response){
						message.responseNotification(response)
						if (response.error) {
							alertErrors(response.details)
							if(response.errors) {
								message.highlightFormErrors(response.errors)
							}
							//message.responseNotification(response)
						}
						if ('redirect' in response) {
							setTimeout(function(){
								document.location.href = response.redirect
							}, 1500)
						}
					});
				}
			},  // post-submit callback
			dataType: 'json',
			type: 'post'
		});
	};

	invoice.validateInvoice = function(el)
	{
		var self 	  = this;
		var ajaxUrl   = $(el).data('ajax-url');
		$.getJSON(ajaxUrl, {
			csrf_token: window.csrftoken
		}, function(response){
			message.responseNotification(response)
		});
	};

	invoice.addToFlow = function(el)
	{
		var ajaxUrl  = $(el).data('ajax-url');
		var invoices = [];
		$('.invoice-check').each(function(index){
			if ($(this).is(":checked")) {
				invoices.push( $(this).val() )
			}
		});
		if (invoices.length > 0) {
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken,
				invoices: invoices.join(',')
			}, function(responseJson){
				message.responseNotification(responseJson)
			});
		} else {
			message.responseNotification({'error': true, 'text': 'Select invoices to add to flow'})
		}
	};

    invoice.init();

})();