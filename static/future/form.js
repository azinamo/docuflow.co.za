
(function(){
    'use strict';

    window.Docuflow = window.Docuflow || {};
    var form = window.Docuflow.Form = {};
    var message = window.Docuflow.Message || {};

    form.init = function()
    {
		 $("#loadingIndicator").ajaxStart(function(){
		    console.log('Ajax start')
		    $(this).show();
         }).ajaxComplete(function(){
		    console.log('Ajax complete')
		    $(this).hide();
         }).ajaxStop(function(){
		    console.log('Ajax stop')
		    $(this).hide();
         });

		 $.ajaxSetup({
             error: function(xhr, status, error){
                 console.log('An error occurred:' + error)
             }
         });
    };

   form.submit = function(el, formEl, params)
   {
       	formEl.addClass('submitted');
        el.css('width', el.outerWidth());
        el.attr('data-original-html', el.html());
        el.html('<i class="fa fa-spinner fa-spin"></i>');

        // remove existing alert & invalid field info
        $('.alert-fixed').remove();
        $('.is-invalid').removeClass('is-invalid');
        $('.is-invalid-message').remove();
        formEl.ajaxSubmit({
            url: formEl.attr('action'),
            target: '#loader',   // target element(s) to be updated with server response
            data: params,
            beforeSubmit:  function(formData, jqForm, options) {
                var queryString = $.param(formData);
                console.log(queryString)
            },  // pre-submit callback
            error: function (data) {
                // handle errors
                console.log('Handle errors');
                el.css('width', $(this).outerWidth()).html($(this).data('original-html'));
                message.responseNotification(data)
            },
            success:       function(responseJson, statusText, xhr, $form)  {
                // handle response
                form.handleResponse(responseJson, statusText, xhr);
                formEl.LoadingOverlay('hide');

            },  // post-submit callback
            dataType: 'json',
            type: 'post'
            // $.ajax options can be used here too, for example:
            //timeout:   3000
        });
   };

   form.submitModal = function(formEL)
   {
        // disable extra form submits
        formEL.addClass('submitted');
        formEL.find("button[type='submit']").each(function () {
            $(this).css('width', $(this).outerWidth());
            $(this).attr('data-original-html', $(this).html());
            $(this).html('<i class="fa fa-spinner fa-spin"></i>');
        });

        // remove existing alert & invalid field info
        $('.alert-fixed').remove();
        $('.is-invalid').removeClass('is-invalid');
        $('.is-invalid-message').remove();
        formEL.ajaxSubmit({
            url: formEL.attr('action'),
            target:        '#loader',   // target element(s) to be updated with server response
            beforeSubmit:  function(formData, jqForm, options) {
                var queryString = $.param(formData);
            },  // pre-submit callback
            error: function (data) {
                // handle errors
                message.responseNotification(data)
            },
            success:       function(responseJson, statusText, xhr, $form)  {
                // handle response
                form.handleResponse(responseJson, statusText, xhr)
            },  // post-submit callback
            dataType: 'json',
            type: 'post'
            // $.ajax options can be used here too, for example:
            //timeout:   3000
        });
   };

   form.handleResponse = function(responseJson, statusText, xhr)
   {
        message.responseNotification(responseJson);
        if (responseJson.hasOwnProperty('file')) {
            var invoicePdf = $("#invoice_pdf");
            var invoiceImage = $("#invoice_image");
            if (invoicePdf.length > 0) {
                invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="'+ responseJson.file +'"></div>');
                PDFObject.embed(responseJson.file, "#invoice_pdf");
            }
            if (invoiceImage.length > 0) {
                invoiceImage.val(responseJson.file);
            }
        }
   };

   form.init();

})();