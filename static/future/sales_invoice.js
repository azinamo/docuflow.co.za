(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer || {};
    var salesItem = window.Docuflow.SalesItem || {};
    var salesInvoice = window.Docuflow.SalesInvoice = {};

    salesInvoice.init = function()
	{
		console.log('Initializing sales invoice')
		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");
				var form = $(data).find("form");
				var submitBtn = form.find("button[type='submit']");

				common.initDatePicker();
				common.initChosenSelect();


			    $("#id_override_key").on("blur", function(e){
				    salesInvoice.validateOverrideKey($(this).val());
			    });

			    $("#override_continue").on("click", function(){
				   console.log("Handle override validation --> ");
				   form.submit($(this), $("#sales_invoice_form"), {'override': true});
			    });

			});
		});+

		$("#sales_documents>tbody>tr>td:not(.actions)").on("click", function(){
			var detailUrl = $(this).parent("tr").data("detail-url");
			if (detailUrl !== undefined && detailUrl !== "") {
				document.location.href = detailUrl;
			}
			return false;
		});

		$(".save-cash-invoice").on("click", function(e){
			var ajaxUrl = $(this).data('payment-url');
			console.log('Get the amount')
			var params = {'amount': $("#id_total_amount").val(), 'invoice_type': $("#invoice_type").val()}

			console.log("Params")
			console.log(params)

			$.get(ajaxUrl, params, function (data) {
				$(data).modal("show");
				var paymentContinueBtn = $("#cash_invoice_payment_continue");
				var invoicePaymentMethodsAmounts = $(".invoice-payment-amount");
				var balanceEl = $("#balance");
				var idBalanceEl = $("#id_balance");
				var invoiceAmount = parseFloat(accounting.toFixed($("#total_invoice_amount").val(), 2));
				// var _invoiceAmount = parseFloat(accounting.toFixed(invoiceAmount, 2));
				invoicePaymentMethodsAmounts.on("keyup", function() {
					var allocatedTotal = 0;
					console.log('Invoice amount ', invoiceAmount);

					var allocatedTotal = salesInvoice.calculateAmountAllocated(invoicePaymentMethodsAmounts);

					var changeAmount = 0;
					console.log('-------------------');
					console.log("Allocated total --> ", allocatedTotal, typeof allocatedTotal);
					console.log("Invoice total--> ", invoiceAmount, typeof invoiceAmount);
					console.log('\n\n');
					salesInvoice.handlePaymentMethodWithChange($(this), invoiceAmount, allocatedTotal);

					var balance = invoiceAmount - allocatedTotal;
					console.log("Balance--> ", balance, typeof accounting.formatMoney(balance), typeof balance);
					if (balance === 0) {
						paymentContinueBtn.prop("disabled", false);
					} else {
						paymentContinueBtn.prop("disabled", true);
					}
					balanceEl.html(accounting.formatMoney(balance));
					idBalanceEl.html(accounting.formatMoney(balance));

				});

				common.initDatePicker();
				common.initChosenSelect();

			    paymentContinueBtn.on("click", function(e){
			       e.preventDefault();
			       var formEl = $("#cash_invoice_receipt_form");
                   // formEl.LoadingOverlay('show', {
			       	// 'text': 'Processing ...'
				   // });
			       console.log( formEl.serialize() );
				   form.submit($(this), $("#sales_invoice_form"), {'payment': formEl.serialize()});
			    });
			});

			return false;
		});

		$(".save-invoice").on("click", function(e){
			form.submit($(this), $("#sales_invoice_form"));
			return false;
		});

		$("#save_cash_sales_invoice").on("click", function(e){
			form.submit($(this), $("#create_sales_invoice_form"));
			return false;
		});

		var salesInvoiceBody = $("#invoice_items_body");
		if (salesInvoiceBody.length > 0) {
			var ajaxUrl = salesInvoiceBody.data("ajax-url");
			salesInvoice.loadInvoiceItems(salesInvoiceBody, ajaxUrl);
		}

		var addInvoiceItemLine = $("#add_invoice_item_line");
		addInvoiceItemLine.on('click', function(){
			$(this).prop('disabled', true).data('html', addInvoiceItemLine.text()).text('loading ...');
			var ajaxUrl = addInvoiceItemLine.data('ajax-url');
			salesInvoice.loadInvoiceItems(addInvoiceItemLine, ajaxUrl);
		});

		$(".bulk-action").on('click', function(e){
			e.preventDefault();
			salesInvoice.bulkInvoicesAction($(this));
		});

		$(".single-action").on('click', function(e){
			e.preventDefault();
			salesInvoice.singleInvoiceAction($(this));
		});

		$(".action").on('click', function(e){
			e.preventDefault();
			salesInvoice.invoiceAction($(this));
		});

		var idCustomer = $("#id_customer");
		idCustomer.on('change', function(e){
			e.preventDefault();
			salesInvoice.loadCustomerDetails(idCustomer);
		});

		var idDocumentDate = $("#id_invoice_date");
		idDocumentDate.on('blur', function(e){
			e.preventDefault();
			salesInvoice.calculateDueDate(idDocumentDate);
		});

		var idVatNumber = $("#id_vat_number");
		idVatNumber.on('blur', function(e){
			e.preventDefault();
			salesInvoice.updateVatNumber(idVatNumber);
		});

		var idEstimate = $("#id_estimate");
		idEstimate.on('change', function(e){
			salesInvoice.loadEstimateItems(e);
		});

		var pickUp = $("#pick_up");
		var deliveryAddress = $(".delivery-address");
		pickUp.on('change', function() {
			console.log("Pickup changed and is not ", $(this).is(":checked"))
			if ($(this).is(":checked")) {
				deliveryAddress.css({'visibility': 'hidden'});
				console.log("Hide delivery address")
			} else {
				deliveryAddress.css({'visibility': 'visible'});
				console.log("Show delivery address")
			}
		});
	};

    salesInvoice.calculateAmountAllocated = function(elClass)
	{
		var allocatedTotal = 0;
		elClass.each(function(){
			var methodAmount = $(this).val();
			if (methodAmount !== undefined && methodAmount !== "") {
				var amount = parseFloat(accounting.toFixed(methodAmount, 2));
				console.log("amount --> ", accounting.toFixed(methodAmount, 2), amount);
				allocatedTotal += amount;
			}
		});
		return parseFloat(accounting.toFixed(allocatedTotal, 2));
	}

    salesInvoice.handlePaymentMethodWithChange = function(el, invoiceAmount, allocatedAmount)
	{
		var hasChange = el.data('has-change');
		console.log("Has change--> ", hasChange, typeof hasChange, (hasChange === 1));
		if (hasChange === 1) {
			console.log("Handle has change ----> ", (allocatedAmount > invoiceAmount));
			if (allocatedAmount > invoiceAmount) {
				 var changeAmount = allocatedAmount - invoiceAmount;
				 console.log('Change amount -->', changeAmount, typeof changeAmount);
				 $("#change_row").show();
				 $("#change").val(accounting.formatMoney(changeAmount));
				 $("#change_val").html(accounting.formatMoney(changeAmount));
			} else {
				 $("#change_row").hide();
				 $("#change").val(0);
				 $("#change_val").html('');
			}
		}
	};

    salesInvoice.validateOverrideKey = function(el)
	{
        var ajaxUrl = el.data('validate-url');

		$.get(ajaxUrl, {
			override_key: el.val()
		}, function(response) {
		    console.log("Response")
		    console.log( response );
		}, 'json');
	};

	salesInvoice.loadEstimateItems = function(el)
	{
		var rowId = 0;
		var estimateItemsUrl = $("#id_estimate option:selected").data('estimate-url');
		var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
		var container = $("#invoice_items_body");
		$.get(estimateItemsUrl, {
			row_id:rowId
		}, function(html) {
			container.replaceWith(html);
			console.log( "Row --> ", rowId);
			salesItem.handleRow(rowId);
			if (chosenWidth > 0) {
				$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
				$(".line-type").chosen({'width': '40px'});
			} else {
				$(".chosen-select").not('.line-type').chosen({'width': '100px'});
				$(".line-type").chosen({'width': '40px'});
			}
		});
	};

	salesInvoice.updateVatNumber = function(el)
	{
		var customerVatNumber = $("#id_customer option:selected").data('vat-number');
		var customerId = $("#id_customer option:selected").val();
		if (customerId !== undefined && customerId !== "") {
			if (el.val() !== "" && customerVatNumber !== el.val()) {
				var ajaxUrl = el.data('update-vat-url');
				$.post(ajaxUrl, {
					vat_number: el.val(),
					customer_id: customerId
				}, function(response){
					console.log( response );
				});
			}
		}
	};

	salesInvoice.calculateDueDate = function(el)
	{
		console.log("-------calculateDueDate----------")
		var invoiceDate = new Date(el.val());
		var paymentTerm = $("#id_customer option:selected").data('payment-term');
		var paymentTermDays = $("#id_customer option:selected").data('payment-days');
		var idDueDate = $("#id_due_date");
		console.log("Payment term ---> ", paymentTerm)
		if (paymentTerm === 1) {
			var dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
			idDueDate.val(dueDate.format('YYYY-MM-DD'));
		} else if (paymentTerm === 2) {
			var dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
			console.log("Due date --->  ---> ", dueDate)
			var end = dueDate.format('YYYY-MM-') + dueDate.daysInMonth();
			console.log("Month end date ---> ", end, moment().daysInMonth(), dueDate.daysInMonth())
			idDueDate.val(end);
		}
	};

	salesInvoice.loadCustomerDetails = function(el)
	{
		console.log(' ------- Load customer details ------------');
		var ajaxUrl = $("#id_customer option:selected").data('detail-url');
		var vatNumber = $("#id_customer option:selected").data('detail-url');
		$.getJSON(ajaxUrl, {
			module: 'sales'
		}, function(response){
            console.log( response )
            console.log( response.is_default )
            if (response.is_default) {
                console.log("load customer form")
                salesInvoice.loadCashCustomerForm(response)
            } else {
                console.log("load customer details")
                salesInvoice.displayCustomerDetails(response)
            }
            initChosenSelect();

			// if (response.hasOwnProperty('postal')) {
			// 	var postalAddress;
			// 	if (typeof response.postal === 'string') {
			// 		postalAddress = JSON.parse(response.postal);
			// 	} else {
			// 		postalAddress = response.postal;
			// 	}
			// 	$("#postal_street").html("<p class='mb-1'>"+postalAddress.address+"</p>");
			// 	$("#postal_city").html("<p class='mb-1'>"+postalAddress.city+"</p>");
			// 	$("#postal_province").html("<p class='mb-1'>"+postalAddress.province+"</p>");
			// 	$("#postal_country").html("<p class='mb-1'>"+postalAddress.country+"</p>");
			// }
			// if (response.hasOwnProperty('delivery')) {
			// 	var deliveryAddress;
			// 	console.log(typeof response.delivery)
			// 	if (typeof response.delivery === 'string') {
			// 		deliveryAddress = JSON.parse(response.delivery);
			// 	} else {
			// 		deliveryAddress = response.delivery;
			// 	}
			// 	$("#delivery_street").html("<p class='mb-1'>"+deliveryAddress.address+"</p>");
			// 	$("#delivery_city").html("<p class='mb-1'>"+deliveryAddress.city+"</p>");
			// 	$("#delivery_province").html("<p class='mb-1'>"+deliveryAddress.province+"</p>");
			// 	$("#delivery_country").html("<p class='mb-1'>"+deliveryAddress.country+"</p>");
			// }
			// if (response.hasOwnProperty('discount_terms')) {
			// 	var discountTerms = response.discount_terms;
			// 	if (typeof response.discount_terms === 'string') {
			// 		discountTerms = JSON.parse(response.discount_terms);
			// 	} else {
			// 		discountTerms = response.discount_terms;
			// 	}
			// 	if (discountTerms.hasOwnProperty('line')) {
			// 		if (discountTerms.line.discount && discountTerms.line.is_discounted)
			// 		$("#line_discount").val(discountTerms.line.discount);
			// 		$(".discount").each(function(){
			// 			if ($(this).val() === '') {
			// 				$(this).val(discountTerms.line.discount)
			// 			}
			// 		});
			// 	}
			// 	if (discountTerms.hasOwnProperty('invoice')) {
			// 		if (discountTerms.invoice.discount && discountTerms.invoice.is_discounted) {
			// 			$("#invoice_discount").val(discountTerms.invoice.discount);
			// 		}
			// 	}
			// }
			// if (postalAddressHtml.length > 0) {
			// 	$("#postal").html(postalAddressHtml.join(' '))
			// }
			// if (deliveryAddressHtml.length > 0) {
			// 	$("#delivery").html(deliveryAddressHtml.join(' '))
			// }
			//
			// if (response.hasOwnProperty('vat_number')) {
			// 	$("#id_vat_number").val(response.vat_number)
			// }
            // if (response.available_credit || response.credit_limit) {
            //     $(".limit").show();
            //     $("#credit_limit_val").html(accounting.formatMoney(response.credit_limit));
			//
            //     $("#available_credit").val(accounting.unformat(response.available_credit));
			// 	$("#available_credit_val").html(accounting.formatMoney(response.available_credit));
            // } else {
            //     console.log('Something wrong with limit')
            // }
			$("#id_invoice_date").trigger('blur');

		});
	};

	salesInvoice.displayCustomerDetails = function(response)
    {
        var postalAddressHtml = [];
        var deliveryAddressHtml = [];
        if (response.hasOwnProperty("postal")) {
            var postalAddress;
            console.log(typeof response.postal);
            if (typeof response.postal === "string") {
                postalAddress = JSON.parse(response.postal);
            } else {
                postalAddress = response.postal;
            }
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.address+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.city+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.province+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.country+"</p>");
        }
        if (response.hasOwnProperty("delivery")) {
            var deliveryAddress;
            console.log(typeof response.delivery)
            if (typeof response.delivery === "string") {
                deliveryAddress = JSON.parse(response.delivery);
            } else {
                deliveryAddress = response.delivery;
            }
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.address+"</p>");
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.city+"</p>");
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.province+"</p>");
            deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.country+"</p>");
        }
        if (response.hasOwnProperty("discount_terms")) {
            var discountTerms = response.discount_terms;
            if (discountTerms) {
				if (typeof response.discount_terms === "string") {
					discountTerms = JSON.parse(response.discount_terms);
				} else {
					discountTerms = response.discount_terms;
				}
				if (discountTerms.hasOwnProperty("line")) {
					if (discountTerms.line.discount && discountTerms.line.is_discounted)
					$("#line_discount").val(discountTerms.line.discount);
					$(".discount").each(function(){
						if ($(this).val() === "") {
							$(this).val(discountTerms.line.discount)
						}
					});
				}
				if (discountTerms.hasOwnProperty("invoice")) {
					if (discountTerms.invoice.discount && discountTerms.invoice.is_discounted) {
						$("#invoice_discount").val(discountTerms.invoice.discount);
					}
				}
			}
        }
        if (response.credit_limit) {
            postalAddressHtml.push("<div class='limits'>");
            postalAddressHtml.push("<input type='hidden' name='credit_limit' id='credit_limit' value='" + accounting.unformat(response.credit_limit) +"'></div>");
            postalAddressHtml.push("<span id='credit_limit_val'>" +accounting.formatMoney(response.credit_limit)+"</span>");
            postalAddressHtml.push("<div>" +accounting.unformat(response.credit_limit)+"</div>");
            postalAddressHtml.push("</div>")
        } else {
            console.log('Something wrong with limit')
        }
		if (response.available_credit || response.credit_limit) {
            postalAddressHtml.push("<div class='limit'>");
			postalAddressHtml.push("<div><span><strong>Available Credit</strong></span>" +accounting.formatMoney(response.available_credit)+"</div>");
			postalAddressHtml.push("<input type='hidden' name='available_credit' id='available_credit' value='" + accounting.unformat(response.credit_limit) +"'></div>");
            postalAddressHtml.push("</div>")
		} else {
			console.log('Something wrong with limit')
		}
        if (postalAddressHtml.length > 0) {
            $("#customer_details").html(postalAddressHtml.join(" "))
        }
        if (deliveryAddressHtml.length > 0) {
            $("#shipping_to").html(deliveryAddressHtml.join(" "))
        }
        if (response.hasOwnProperty("vat_number")) {
            $("#id_vat_number").val(response.vat_number)
        }

        if (response.hasOwnProperty("pricings")) {
            var html = [];
            $.each(response.pricings, function(index, pricing){
                console.log( pricing )
                html.push("<option value='" + pricing.id + "'>"+ pricing.name+"</option>")
            })
            $("#id_pricing").html(html.join(' '));
        }
		$("#customer_full_name").html("");
        $(".cash").hide();
        $(".customer").show();
		$(".save-cash-invoice").hide();
    	var saveBtn = $("#submit_invoice_and_continue");
    	saveBtn.show();
    	salesInvoice.updateFormAction(saveBtn);
    };

    salesInvoice.loadCashCustomerForm = function(customerData)
    {
    	console.log( customerData );
    	$("#id_vat_number").val("");
		$("#customer_details").html(customerData['customer_form']);
    	$("#shipping_to").html(customerData['delivery_form']);
    	var saveCashBtn = $("#submit_cash_invoice_and_continue");
    	saveCashBtn.show();
		salesInvoice.updateFormAction(saveCashBtn);
    	$(".cash").show();
		$(".customer").hide();
    	$(".save-invoice").hide();
    };

    salesInvoice.updateFormAction = function(el)
	{
		console.log("Update form action");
    	var actionUrl = el.data('action-url');
    	el.show();
    	console.log(actionUrl);
		$("#sales_invoice_form").attr('action', actionUrl);
		console.log("Done updating form action");
	};

	salesInvoice.getSelectedInvoices = function()
	{
		var invoices = [];
		$(".invoice-action").each(function(){
			if ($(this).is(":checked")) {
				invoices.push($(this).data('invoice-id'));
			}
		});
		return invoices;
	};

	salesInvoice.bulkInvoicesAction = function(el)
	{
		var invoices = Sales.getSelectedInvoices();
		var totalInvoices = invoices.length;
		if (totalInvoices === 0) {
			alert('Please select at lease one invoice');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( invoices );
			$.post(ajaxUrl, {
				invoices: invoices
			}, function(response){
				responseNotification(response);
			});
		}
	};

	salesInvoice.singleInvoiceAction = function(el)
	{
		var invoices = Sales.getSelectedInvoices();
		var totalInvoices = invoices.length;
		if (totalInvoices === 0) {
			alert('Please select at lease one invoice');
		} else if (totalInvoices >1) {
			alert('Please select one invoice to action');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( invoices );
			$.post(ajaxUrl, {
				invoices: invoices
			}, function(response){
				responseNotification(response);
			});
		}
	};

	salesInvoice.invoiceAction = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		$.post(ajaxUrl, function(response){
			responseNotification(response);
		});
	};

	salesInvoice.reloadInvoiceItemRow = function(el)
	{
		var rowId = el.data('row-id');
		var ajaxUrl = el.data('ajax-url');
		var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
		$.get(ajaxUrl, {
			row_id:rowId,
			line_type: $("#type_"+ rowId +" option:selected").val()
		}, function(html) {
			$("#sales_item_" + rowId).replaceWith(html);

			salesItem.handleRow(rowId);

			$(".line-type").on("change", function(e){
				salesInvoice.reloadInvoiceItemRow($(this));
        	});


			// if (chosenWidth > 0) {
			// 	if ($(".chosen-select").hasClass('line-type"')) {
			// 		$(".chosen-select").chosen({'width': '14px'});
			// 	} else {
			// 		$(".chosen-select").chosen({'width': chosenWidth + "px"});
			// 	}
			// } else {
			// 	if ($(".chosen-select").hasClass('line-type"')) {
			// 		$(".chosen-select").chosen({'width': '14px'});
			// 	} else {
			// 		$(".chosen-select").chosen({'width': '100px'});
			// 	}
			// }
		});

	};

	salesInvoice.loadInvoiceItems = function(el, ajaxUrl)
	{
		var rowId = 0;
		var lastRow = $(".invoice_item_row").last();
		if (lastRow.length > 0) {
			rowId = parseInt(lastRow.data('row-id')) + 1;
		}
		salesInvoice.loadInvoiceItemRow(el, ajaxUrl, rowId, lastRow)
	};

	salesInvoice.isLineCompleted = function(rowId)
    {
        var inventory = $("#inventory_item_" + rowId).val();
        var ordered = $("#ordered_" + rowId).val();
        var price = $("#price_" + rowId).val();

        if (inventory === '') {
            return false;
        }
        if (ordered === '') {
            return false;
        }
        if (price === '') {
            return false;
        }
        return true;
    };

	salesInvoice.loadInvoiceItemRow = function(el, ajaxUrl, rowId, lastRow)
	{
		var container = $("#invoice_items_placeholder");
		$.get(ajaxUrl, {
			row_id:rowId
		}, function(html) {
			if (lastRow !== undefined && lastRow.length > 0) {
				lastRow.after(html)
			} else {
				container.replaceWith(html);
			}
			var _lastRow = $(".invoice_item_row").last();
			if (_lastRow.length > 0) {
				rowId = parseInt(_lastRow.data('row-id'));
			}

			$(".sales-items").each(function(){
				var itemRowId = $(this).data("row-id")
				salesItem.handleRow(itemRowId);
			});

			$("#vat_code_" + rowId).on('blur', function(e){
				if (_lastRow !== undefined) {
					console.log("Is current line completed --", rowId, salesInvoice.isLineCompleted(rowId));
					if (salesInvoice.isLineCompleted(rowId)) {
						var lastRowId = _lastRow.data('row-id');
						salesInvoice.loadInvoiceItemRow(el, ajaxUrl, parseInt(lastRowId) + 1, _lastRow);
					}
				}
			});

			$(".line-type").on("change", function(e){
				salesInvoice.reloadInvoiceItemRow($(this));
        	});

			el.prop('disabled', false).text(el.data('html'));
		});
	};

	salesInvoice.init();

})();