$(function() {
	'use strict';

	window.Docuflow = window.Docuflow || {};
	var common = window.Docuflow.Common || {};
	var modal = window.Docuflow.Modal || {};
	var inventoryLine = window.Docuflow.InventoryLine || {};

	//Form Submit for IE Browser
	inventoryLine.init = function()
	{

	};
	
    inventoryLine.handleRow: function(rowId)
    {
        $("#inventory_item_" + rowId).on('change', function(){
            var customerLineDiscount = $("#line_discount").val();
            console.log( "Row --> ", rowId, " and load fields")
            var selectedOption = $("#inventory_item_"+ rowId +" option:selected");
            var price = selectedOption.data('price');
            var unit = selectedOption.data('unit');
            var binLocation = selectedOption.data('bin-location');
            var description = selectedOption.data('description');
            var vatId = selectedOption.data('vat-id');
            var availableStock = selectedOption.data('stock');
            var unitId = selectedOption.data('unit-id');
            var isDiscountable = selectedOption.data('is-discountable');
            var priceEl =  $("#price_" + rowId )
            console.log( "price --> ", price, ' add to ', $("#price_" + rowId ));
            console.log( "unit --> ", unit, ' add to ', $("#unit_" + rowId ));
            priceEl.val( price );
            if (unit !== undefined && unit !== '') {
                $("#unit_" + rowId).val(unit)
            }
            if (unitId !== undefined && unitId !== '') {
                $("#unit_id_" + rowId).val(unitId);
            }
            if (binLocation !== undefined && binLocation !== '') {
                $("#bin_location_" + rowId).val(binLocation)
            }
            if (description !== undefined && description !== '') {
                $("#description_" + rowId).val(description)
            }
            if (vatId !== undefined && vatId !== '') {
                $("#vat_code_" + rowId).val(vatId).trigger("chosen:updated").trigger('change');
            } else {
                $("#vat_code_" + rowId).val('').trigger("chosen:updated").trigger('change');
            }
            inventoryLine.handleRowDiscount(rowId, isDiscountable, customerLineDiscount);
            availableStock = parseFloat(availableStock);
            if (availableStock > 0 ) {
                $("#order_item_" + rowId).html('No');
            } else {
                $("#order_item_" + rowId).html('Yes');
                $("#")
            }
        });

        $(".line-type").on('change', function(e){
            inventoryLine.reloadEstimateRow($(this));
        });

        $("#quantity_"+ rowId +"").on('blur', function(){
            inventoryLine.calculateInventoryItemPrices(rowId);
        });

        $("#price_"+ rowId +"").on('blur', function(){
            inventoryLine.calculateInventoryItemPrices(rowId);
        });

        $("#vat_code_"+ rowId).on('change', function(){
            var selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
            var vatPercentage = selectedVatOption.data('percentage');
            console.log('Vat code changes --> ', vatPercentage);
            if (vatPercentage !== undefined && vatPercentage !== '') {
                $("#vat_percentage_"+ rowId).val(vatPercentage);
            } else {
                $("#vat_percentage_"+ rowId).val(0);
            }
            inventoryLine.calculateInventoryItemPrices(rowId);
        });

        $("#discount_percentage_"+ rowId +"").on('blur', function(){
            inventoryLine.calculateInventoryItemPrices(rowId);
        });
    };

    inventoryLine.handleRowDiscount = function(rowId, isDiscountable, lineDiscount)
    {
        console.log($("#line_discount"));
        console.log($("#line_discount").val());
        console.log("Handling row discount, is discountable -> ", isDiscountable, typeof isDiscountable, lineDiscount );
        var discountPercentageEl = $("#discount_percentage_" + rowId);
        if (isDiscountable) {
            var discountVal = lineDiscount || 0;
            console.log("Now discount value is ", lineDiscount);
            discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
        } else {
            discountPercentageEl.val(0).prop('disabled', true).trigger('change');
            $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
            $("#discount_amount_" + rowId).val('0.00').trigger('change');
        }
    };

    inventoryLine.calculateInventoryItemPrices =function(rowId)
    {
        console.log("CALCULATING INVENTORY ROW ", rowId + " ---------------------------- ");
        var price = $("#price_" + rowId);
        var priceIncl = $("#price_including_" + rowId);
        var ordered = $("#quantity_ordered_" + rowId);
        var received = $("#quantity_" + rowId);
        var short = $("#quantity_short_" + rowId);
        var vat = $("#vat_amount_" + rowId);
        var vatPercentage = $("#vat_percentage_" + rowId);
        var discountPercentage = $("#discount_percentage_" + rowId);
        var discount = $("#discount_" + rowId);
        var backOrder = $("#bo_" + rowId);

        var quantityOrdered = 0;
        var shortQuantity = 0;
        var quantityReceived = 0;
        var vatAmount = 0;
        var priceExcl = 0;
        var discountAmount = 0;
        var totalVat = 0;
        var totalDiscount = 0;
        var priceIncludingAmount = 0;
        var totalInc = 0;
        var totalExcl = 0;

        if (ordered !== undefined && ordered.length > 0) {
            if (ordered.val() !== '') {
                quantityOrdered = parseFloat(ordered.val());
            }
        }
        if (received !== undefined && received.length > 0) {
            if (received.val() !== '') {
                quantityReceived = parseFloat(received.val());
            }
        }

        if (quantityOrdered > 0) {
            if (quantityReceived <= quantityOrdered) {
                shortQuantity =  quantityOrdered - quantityReceived
            } else {
                received.val(quantityOrdered);
                //backOrder.prop('disabled', true);
                alert('Please enter valid received quantity')
                return false;
            }
        }
        if (short !== undefined  && short.length > 0) {
            short.val( shortQuantity );
        }
        if (price !== undefined  && received.length > 0) {
            if (price.val() !== '') {
                priceExcl = parseFloat(accounting.unformat(price.val()));
            }
        }
        if (vatPercentage.length > 0) {
            if (vatPercentage.val() !== '' && priceExcl !== '') {
                var percentage = parseFloat(vatPercentage.val());
                console.log('Vat percentage --> ', percentage);
                vatAmount = (percentage/100) * priceExcl;
                console.log('Vat amount --> ', vatAmount);
            }
        }
        priceIncludingAmount = priceExcl + vatAmount;

        if (discountPercentage.length > 0) {
            if (discountPercentage.val() !== '' && priceExcl !== '') {
                var discPercentage = parseFloat(discountPercentage.val());
                console.log('Discount percentage --> ', discPercentage);
                discountAmount = (discPercentage/100) * priceExcl;
                console.log('Discount amount --> ', discountAmount);
                discount.val(discountAmount)
            }
        }
        console.log("Price excluding, not discount ", priceExcl);

        console.log("Price including amount, not discount ", priceIncludingAmount);
        if (discountAmount > 0) {
            priceExcl = priceExcl - discountAmount
        }

        if (vatPercentage.length > 0) {
            if (vatPercentage.val() !== '' && priceExcl !== '') {
                var percentage = parseFloat(vatPercentage.val());
                console.log('Vat percentage --> ', percentage);
                vatAmount = (percentage/100) * priceExcl;
                console.log('Vat amount --> ', vatAmount);
                vat.val(vatAmount)
            }
        }

        // if (vat !== undefined && vat.length > 0) {
        // 	if (vat.val() !== '') {
        //
        // 		vatAmount += parseFloat(accounting.unformat(vat.val()));
        // 	}
        // }

        if (discountAmount !== '') {
            totalDiscount = discountAmount * quantityReceived;
        }
        totalExcl = priceExcl * quantityReceived;
        totalInc = totalExcl;
        if (vatAmount !== '') {
            console.log("Vat amount", vatAmount);
            console.log("Price including + Vat amount", priceIncludingAmount, vatAmount);
            totalVat = vatAmount * quantityReceived;
            totalInc += totalVat;
        }
        //var totalInc = priceIncludingAmount * quantityReceived;
        console.log( " quantityReceived ", quantityReceived );
        console.log( " Price EXCL", priceExcl );
        console.log( " totalExcl ", totalExcl );
        console.log( " vatAmount ", vatAmount );
        console.log( " discountAmount ", discountAmount );
        console.log( " priceIncl ", priceIncludingAmount);
        console.log( " totalInc", totalInc );

        // if (backOrder !== undefined) {
        // 	if (shortQuantity === 0) {
        // 		backOrder.prop('disabled', true);
        // 	} else {
        // 		backOrder.prop('disabled', false);
        // 	}
        // }

        $("#vat_total_" + rowId).html( accounting.formatMoney(totalVat) );
        $("#total_vat_" + rowId).val( totalVat );

        $("#discount_amount_" + rowId).html( accounting.formatMoney(totalDiscount) );
        $("#total_discount_" + rowId).val(totalDiscount);

        $("#price_incl_" + rowId).html( accounting.formatMoney(priceIncl) );
        priceIncl.val( priceIncludingAmount );

        $("#total_price_exl_" + rowId).html( accounting.formatMoney(totalExcl) );
        $("#total_price_excluding_" + rowId).val( totalExcl );

        $("#total_price_incl_" + rowId).html( accounting.formatMoney(totalInc) );
        $("#total_price_including_" + rowId).val( totalInc );

        inventoryLine.calculateTotals()
    },

    inventoryLine.calculateTotals = function()
    {
        var totalOrdered = 0;
        var totalReceived = 0;
        var totalShort = 0;
        var priceExc = 0;
        var priceInc = 0;
        var totalPrice = 0;
        var totalTotalExc = 0;
        var totalTotalInc = 0;

        $(".ordered").each(function(){
            totalOrdered += parseInt($(this).val());
        });

        $(".received").each(function(){
            totalReceived += parseInt($(this).val());
        });
        $(".short").each(function(){
            totalShort += parseInt($(this).val());
        });
        $(".price").each(function(){
            totalPrice += parseFloat(accounting.unformat($(this).val()));
        });
        $(".price-exc").each(function(){
            priceExc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".price-incl").each(function(){
            priceInc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-price-excl").each(function(){
            totalTotalExc += parseFloat(accounting.unformat($(this).val()));
        });
        $(".total-price-incl").each(function(){
            totalTotalInc += parseFloat(accounting.unformat($(this).val()));
        });

        $("#total_ordered").html( totalOrdered );
        $("#total_received").html( totalReceived );
        $("#total_short").html( totalShort );
        $("#total_price").html( accounting.formatMoney(totalPrice) );
        $("#price_excl").html( accounting.formatMoney(priceExc) );
        $("#price_incl").html( accounting.formatMoney(priceInc) );
        $("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
        $("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );

    }

	inventoryLine.init();
});