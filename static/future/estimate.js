(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer || {};
    var salesItem = window.Docuflow.SalesItem || {};
    var estimate = window.Docuflow.Estimate = {};

    estimate.init = function()
	{
		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");
				var form = $(data).find("form");
				var submitBtn = form.find("button[type='submit']");

				common.initDatePicker();

				common.initChosenSelect();

			});
		});

		$("#estimates>tbody>tr>td:not(.actions)").on("click", function(e){
			var detailUrl = $(this).parent("tr").data("detail-url");
			if (detailUrl !== undefined && detailUrl !== "") {
				document.location.href = detailUrl;
			}
			return false;
		});

		$(".save-estimate").on("click", function(e){
			e.preventDefault();
			var next = $(this).data("action");
			form.submit($(this), $("#estimate_form"), {"next": next});
		});

		var estimateItemsBody = $("#estimate_items_body");
		if (estimateItemsBody.length > 0) {
			var ajaxUrl = estimateItemsBody.data("ajax-url");
			estimate.loadEstimateItems(estimateItemsBody, ajaxUrl);
		}

		var addEstimateLine = $("#add_estimate_line");
		addEstimateLine.on('click', function(){
			$(this).prop('disabled', true).data('html', addEstimateLine.text()).text('loading ...')
			var ajaxUrl = addEstimateLine.data('ajax-url');
			estimate.loadEstimateItems(addEstimateLine, ajaxUrl);
		});

		$(".action-estimate").on('click', function (e) {
			console.log('Submit action ....');
			e.preventDefault();
			estimate.actionEstimate($(this));
		});

		$(".bulk-action").on('click', function(e){
			e.preventDefault();
			estimate.bulkInvoicesAction($(this));
		});

		$(".single-action").on('click', function(e){
			e.preventDefault();
			estimate.singleInvoiceAction($(this));
		});

		var idCustomer = $("#id_customer");
		idCustomer.on('change', function(e){
			e.preventDefault();
			console.log( customer );
			console.log( idCustomer );
			customer.loadDetails(idCustomer);
		});

		$("#id_is_deposit_required").on("change", function (e) {
			var depositRequired = $("#deposit_required");
			var idDeposit = $("#id_deposit");
			if ($(this).is(":checked")) {
				depositRequired.show();
			} else {
				idDeposit.val("");
				depositRequired.hide();
			}
		});

		$(".select-filter").on('change', function(event){

		});

		$(".filter").on('keyup', function(event){

		});

		var pickUp = $("#id_is_pick_up");
		var deliveryAddress = $("#shipping_to");
		pickUp.on('change', function(){
			console.log("Pickup changed and is not ", $(this).is(":checked"))
			if ($(this).is(":checked")) {
				deliveryAddress.css({'visibility': 'hidden'}).show();
				console.log("Hide delivery address")
				$("#submit_estimate_with_picking").hide();
				$("#submit_estimate_with_delivery").hide();
			} else {
				deliveryAddress.css({'visibility': 'visible'}).show();
				console.log("Show delivery address")
			    $("#submit_estimate_with_picking").show();
			    $("#submit_estimate_with_delivery").show();
			}
		});
	};

    estimate.bulkInvoicesAction = function(el)
	{
		var estimates = Estimate.getSelectedEstimates();
		var totalEstimates = estimates.length;
		if (totalEstimates === 0) {
			alert('Please select at lease one estimate');
		} else {
			var ajaxUrl = el.data('ajax-url');
			var confirm = el.data('confirm');
			if (confirm === '1') {
				var confirmText = el.data('confirm-text')
				return confirm(confirmText)
			}
			console.log( estimates );
			console.log( ajaxUrl );
			$.post(ajaxUrl, {
				estimates: estimates
			}, function(response){
				responseNotification(response);
			});
		}
	};

	estimate.singleInvoiceAction = function(el)
	{
		var estimates = Estimate.getSelectedEstimates();
		var totalEstimates = estimates.length;
		if (totalEstimates === 0) {
			alert('Please select at lease one estimate');
		} else if (totalEstimates >1) {
			alert('Please select one estimate to action');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( estimates );
			console.log( ajaxUrl );
			$.post(ajaxUrl, {
				estimates: estimates
			}, function(response){
				responseNotification(response);
			});
		}
	};

	estimate.getSelectedEstimates = function()
	{
		var estimates = [];
		$(".estimate-action").each(function(){
			if ($(this).is(":checked")) {
				estimates.push($(this).data('estimate-id'));
			}
		});
		return estimates;
	};

	estimate.actionEstimate = function(el)
	{
		console.log("Action estimate")
		console.log(el)
		var ajaxUrl = el.data('ajax-url');
		console.log(ajaxUrl)
		$.getJSON(ajaxUrl, function(response){
			console.log(response)
			responseNotification(response)
		});
	};

	estimate.reloadEstimateRow = function(el)
	{
		var rowId = el.data('row-id');
		var ajaxUrl = el.data('ajax-url');
		var chosenWidth = $("#"+ rowId + "_estimate_item_chosen").width();
		$.get(ajaxUrl, {
			row_id:rowId,
			line_type: $("#type_"+ rowId +" option:selected").val()
		}, function(html) {
			$("#sales_item_" + rowId).replaceWith(html);

			salesItem.handleRow(rowId);
			// $(".chosen-select").chosen();
			if (chosenWidth > 0) {
				$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
				$(".line-type").chosen({'width': '40px'});
			} else {
				$(".chosen-select").not('.line-type').chosen({'width': '250px'});
				$(".line-type").chosen({'width': '40px'});
			}
		});

	};

	estimate.loadEstimateItems = function(el, ajaxUrl)
	{
		var rowId = 0;
		var lastRow = $(".estimate_item_row").last();
		if (lastRow.length > 0) {
			rowId = parseInt(lastRow.data('row-id')) + 1;
		}
		estimate.loadEstimateRow(el, ajaxUrl, rowId, lastRow)
	};

	estimate.loadEstimateRow = function(el, ajaxUrl, rowId, lastRow)
	{
		var container = $("#estimate_items_placeholder");
		$.get(ajaxUrl, {
			row_id: rowId
		}, function(html) {
			if (lastRow !== undefined && lastRow.length > 0) {
				lastRow.after(html)
			} else {
				container.replaceWith(html);
			}
			var _lastRow = $(".estimate_item_row").last();
			if (_lastRow.length > 0) {
				rowId = parseInt(_lastRow.data('row-id'));
			}
			estimate.updateCalculations();

			// $("#vat_code_" + rowId).on('blur', function(e){
			// 	if (_lastRow !== undefined) {
			// 		var lastRowId = _lastRow.data('row-id');
			// 		estimate.loadEstimateRow(el, ajaxUrl, parseInt(lastRowId) + 1, _lastRow);
			// 	}
			// });

			$(".line-type").on("change", function(e){
				estimate.reloadEstimateRow($(this));
        	});
			el.prop('disabled', false).text(el.data('html'));
		});
	};

	estimate.updateCalculations = function()
	{
		$(".sales-items").each(function(){
			var salesItemRowId = $(this).data('row-id');
			salesItem.handleRow(salesItemRowId);
		});
	};

    estimate.init();

})();