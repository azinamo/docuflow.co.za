$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");

			initDatePicker();

			initChosenSelect();

        });
    });

	$("#goods>tr>td:not(.actions)").on('click', function(){
		var detailUrl = $(this).parent('tr').data('detail-url');
		document.location.href = detailUrl;
		return false;
	});


    var sellingPriceEl = $("#id_selling_price");
    var sellingPriceIncVatEl = $("#selling_price_inc_vat");
    var saleVatCodeEl = $("#id_sale_vat_code");
    saleVatCodeEl.on('change', function(e){
		var sellingPrice = sellingPriceEl.val();
    	InventoryReceived.calculatePriceWithVat(sellingPrice, sellingPriceIncVatEl);
	});
    sellingPriceEl.on('blur', function(e){
    	var sellingPrice = $(this).val();
    	InventoryReceived.calculatePriceWithVat(sellingPrice, sellingPriceIncVatEl)
	});


	$("#inventory_item>tr>td:not(.actions)").on('click', function(){
		var detailUrl = $(this).parent('tr').data('detail-url');
		document.location.href = detailUrl;
		return false;
	});

    var inventoryQuantity = $(".quantity");
    inventoryQuantity.on('blur', function(){
    	var inventoryItemId = $(this).data('inventory-item-id');
    	InventoryReceived.calculateAdjustment(inventoryItemId);
	});

	var searchPurchaseOrder = $("#id_reference");
	var poSearchUrl = searchPurchaseOrder.data('ajax-url');

	var supplier = $("#id_supplier");
	supplier.on('change', function(){
		InventoryReceived.loadSupplierItems($(this));
	});


	$("#id_purchase_order").hide();
	$("#id_purchase_order_chosen").hide();

	var purchaseOrderSupplier = $("#id_purchase_order_supplier");
	purchaseOrderSupplier.on('change', function(e){
		InventoryReceived.loadSupplierPurchaseOrders($(this));
		$("#enter_totals").prop('disabled', true);
		return false;
	});

	var idReceivedMovement  = $("#id_received_movement");
	if (idReceivedMovement.length > 0) {
		idReceivedMovement.hide();
		$("#id_received_movement_chosen").hide();
		var supplierReturned = $("#id_supplier_returned");
		supplierReturned.on('change', function(){
			InventoryReceived.loadSupplierInventoryItems($(this));
		});
	}

	$("#submit_goods_received").on('click', function(e){
		ajaxSubmitForm($(this), $("#create_goods_received_form"));
		return false;
	});

	var inventoryReceivedItemsBody = $("#inventory_received_items_body");
	if (inventoryReceivedItemsBody.length > 0) {
		InventoryReceived.loadInventoryItems(inventoryReceivedItemsBody);
	}

	var addLine = $("#add_received_line");
	addLine.on('click', function(){
		InventoryReceived.loadInventoryItems($(this));
	});

	$("#enter_totals").on("change", function(event){
		InventoryReceived.openInputs($(this))
	});

	$(".ledger-inventory-item").on('click', function(e){
		e.preventDefault();
		InventoryReceived.displayInventoryBreakdown($(this));
	});


});

var InventoryReceived = (function(inventoryReceived, $){

	return {

		updateUnits: function(el)
		{
			var unitsAjaxUrl = el.data('units-url');
			var measure = el.val();
			$.getJSON(unitsAjaxUrl, {
				measure: measure
			}, function(response){
				console.log(response)
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
				});
				$("#id_sale_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_stock_unit").html(html.join('')).trigger("chosen:updated");
			});
		},

		loadInventoryItems: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			el.prop('disabled', true);
			var container = $("#inventory_received_items_placeholder");
			var rowId = 0;
			var lastRow = $(".inventory_item_row").last();

			var chosenEl = $("#"+ rowId + "_inventory_item_chosen");
			var chosenWidth = 0;
			if (chosenEl.length > 0) {
				chosenWidth = chosenEl.width();
            }
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				$(".inventory_item_row").each(function(){
					var itemRowId = $(this).data("row-id")
					InventoryReceived.handleRow(itemRowId);
				});


				el.prop('disabled', false);
			});
		},

		handleRow: function(rowId)
		{
			var chosenWidth = calculateChosenSelectWidth("inventory_item_row");
			console.log("With is ----- ", chosenWidth)
			var inventorySelect = $("#"+ rowId +"_inventory_item");
			var inventorySearchUrl = inventorySelect.data("ajax-url");
			var inventoryUrl = inventorySelect.data("ajax-inventory-url");
			inventorySelect.ajaxChosen({
				url: inventorySearchUrl,
				dataType: "json",
				width: chosenWidth + "px",
				allowClear: true,
				placeholder: "Select ..."
			}, {loadingImg: '/static/js/vendor/chosen/loading.gif'}).width(chosenWidth + "px");


			inventorySelect.on('change', function(){
				var inventoryId = $(this).val();
				$.get(inventoryUrl, {
					inventory_id: inventoryId
				}, function(inventoryData){
					var price = inventoryData['price'];
					var measureId = inventoryData['measure_id'];
					var unitId = inventoryData['unit_id'];
					var unitCode = inventoryData['unit_code'];
					var saleUnit = inventoryData['sale_unit'];
					var saleUnitId = inventoryData['sale_unit_id'];
					var saleUnitCode = inventoryData['sale_unit_code'];
					var stockUnitId = inventoryData['stock_unit_id'];
					var stockUnitCode = inventoryData['stock_unit_code'];
					var vatId = inventoryData['vat_code_id'];

					var maxOrder = 0;
					if (inventoryData['max_reorder']) {
						maxOrder = parseFloat(inventoryData['max_reorder']);
					}
					$("#max_order_" + rowId ).val(maxOrder);
					$("#sale_unit_id_" + rowId ).val(saleUnitId);
					$("#sale_unit_code_" + rowId ).val(saleUnitCode);
					$("#stock_unit_id_" + rowId ).val(stockUnitId);
					$("#stock_unit_code_" + rowId ).val(stockUnitCode);
					$("#unit_code_" + rowId ).val(unitCode);
					$("#unit_id_" + rowId ).val(unitId);

					$("#price_" + rowId ).val(price);
					if (measureId !== undefined && measureId !== '') {
						var unitsUrl = inventoryData['units_url'];
						InventoryReceived.loadUnits(rowId, measureId, unitsUrl, unitId);
					}
					if (vatId !== undefined && vatId !== '') {
						$("#vat_code_" + rowId).val(vatId).trigger("chosen:updated");
					} else {
						$("#vat_code_" + rowId).val('').trigger("chosen:updated");
					}
					$("#control_unit_" + rowId).val(saleUnit);
					InventoryReceived.calculateInventoryItemPrices(rowId);
				});
			});

			$("#quantity_received_"+ rowId +"").on('blur', function(){
				var maxOrder = parseFloat($("#max_order_" + rowId).val());
				var receivedQty = parseFloat($(this).val());
				var quantityOrderedEl = $("#quantity_ordered_" + rowId);
				var controlReceivedQtyEl = $("#control_quantity_received_" + rowId);
				console.log("Max reorder ", maxOrder, typeof maxOrder)
				if (maxOrder !== undefined && maxOrder !== '') {
					if (maxOrder === 0 || maxOrder >= receivedQty) {
						InventoryReceived.calculateInventoryItemPrices(rowId);
						if (InventoryReceived.isDifferentUnit(rowId)) {
							InventoryReceived.updateInventoryControlQuantity(rowId);
						} else {
							controlReceivedQtyEl.val(receivedQty)
							quantityOrderedEl.val(receivedQty)
						}
					} else {
						$(this).val('');
						alert('Quantity entered is greater than the max quantity(' + maxOrder + ') allowed, please fix')
					}
				} else {
					console.log("Quantity Received", receivedQty)
					InventoryReceived.calculateInventoryItemPrices(rowId);
					if (InventoryReceived.isDifferentUnit(rowId)) {
						console.log("Quantity Received", receivedQty)
						InventoryReceived.updateInventoryControlQuantity(rowId);
					} else {
						controlReceivedQtyEl.val(receivedQty)
						quantityOrderedEl.val(receivedQty)
					}
				}
			});

			$("#unit_"+ rowId ).on('change', function(e){
				$("#unit_id_" + rowId ).val($(this).val());
				$("#unit_code_" + rowId ).val($("#unit_"+ rowId + " option:selected").data('unit'));

				var isDifferent =  InventoryReceived.isDifferentUnit(rowId);
				if (isDifferent) {
					InventoryReceived.updateInventoryControlQuantity(rowId);
				} else {
					var quantity = parseFloat($("#quantity_received_"+ rowId ).val());
					$("#control_quantity_received_" + rowId).val( quantity )
				}
			});

			$("#vat_code_"+ rowId ).on('change', function(){
				console.log('Calculating based on vat change')

				var netTotal = $("#total_price_exl_" + rowId ).text();
				var vatSelected = $("#vat_code_"+ rowId  + " option:selected");
				var percentage = vatSelected.data('percentage');
				console.log( netTotal )
				console.log( vatSelected )
				console.log( percentage )
				if (netTotal !== '' && percentage !== '') {
					var vatAmount = (parseFloat(percentage)/100) * parseFloat(netTotal);
					var vatAmountStr = accounting.formatMoney(vatAmount);
					$("#vat_" + rowId ).html( vatAmountStr );
					$("#vat_amount_" + rowId ).val( vatAmount );
				}
				InventoryReceived.calculateInventoryItemPrices(rowId);
			});

			$("#price_"+ rowId +"").on('blur', function(){
				InventoryReceived.calculateInventoryItemPrices(rowId);
			});
		},

		isDifferentUnit: function(rowId)
		{
			console.log("-------------------- isDifferentUnit -----------------------")
			var unitId = parseInt($("#unit_"+ rowId +" option:selected").val());
			var saleUnitId = parseInt($("#sale_unit_id_"+ rowId).val());
			console.log("Unit id --> ", unitId, ' sale unit id ', saleUnitId);
			console.log("type of Unit id --> ", typeof unitId, ' sale unit id ', typeof saleUnitId);
			if (unitId !== saleUnitId) {
				return true;
			}
			return false;
		},

		updateInventoryControlQuantity: function(rowId)
		{
			console.log("-------------------- updateInventoryControlQuantity -----------------------")
            var quantity = parseFloat($("#quantity_received_"+ rowId ).val());
            if (quantity > 0) {
                try {
                        var unit = $("#unit_code_"+ rowId ).val();
                        var saleUnit =  $("#sale_unit_code_"+ rowId ).val();

                        console.log("Convert from ", unit, ' to ', saleUnit)

                        var convert = require('convert-units');

                        var result = convert(quantity).from(unit).to(saleUnit)

                        $("#control_quantity_received_" + rowId).val( result )

                } catch (e) {
                    $("#control_quantity_received_" + rowId).val( quantity )
                }
            } else {
                $("#control_quantity_received_" + rowId).val(0)
            }
		},

		loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			var measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId == item.id) {
						html.push("<option data-unit='" + item.unit +"' value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option data-unit='" + item.unit +"' value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#unit_" + rowId).html(html.join('')).trigger("chosen:updated");

			});
		},

		loadSupplierReceivedItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id,
				date: $("#id_date").val()
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					InventoryReceived.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		loadSupplierPurchaseOrders: function(el)
		{
			var ajaxUrl = el.data('purchase-orders-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					$("#enter_totals").prop('disabled', false);
					var optionSelected = $("#id_purchase_order  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					InventoryReceived.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		loadSupplierInventoryItems: function(el)
		{
			var ajaxUrl = el.data('inventory-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_inventory");
			$("#id_inventory_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");

				$.each(response, function(index, inventory_item){
					html.push("<option value='"+ inventory_item.id +"' " +
						"data-ajax-url='"+ inventory_item.received_items_url +"'>"+ inventory_item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_inventory  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					console.log("Update received items ---> ")
					InventoryReceived.loadSupplierInventoryReceivedItems(ajaxUrl);
                });
			});
		},

		loadSupplierInventoryReceivedItems: function(ajaxUrl)
		{
			var supplierId = $("#id_supplier_returned option:selected").val();
			var inventoryId = $("#id_inventory option:selected").val();
			console.log("Load received items for supplier and inventory item---> ", supplierId, inventoryId, ajaxUrl)
			// var supplierId = $("#id_supplier_returned option:selected").val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();
			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplierId,
				inventory_id: inventoryId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					InventoryReceived.displaySupplierInventoryReceivedItems(ajaxUrl, inventoryId);
                });
			});
		},

		displaySupplierInventoryReceivedItems: function(ajaxUrl, inventoryId)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, {
					inventory_id: inventoryId
				}, function(html){
				   $("#supplier_inventory_received_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						var inventoryItemId = $(this).data('item-id');
						InventoryReceived.calculateInventoryItemPrices(inventoryItemId)
				   });
				});
			} else {
				$("#supplier_inventory_received_items").html("");;
			}
		},

		loadSupplierItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_purchase_order  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					InventoryReceived.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		calculatePriceWithVat: function(sellingPrice, sellingPriceIncVat)
		{
			var percentage = $("#id_sale_vat_code option:selected").data('percentage');
			var vat = InventoryReceived.calculateVat(percentage, sellingPrice);
			var sellingPriceWithVat = parseFloat(sellingPrice) + vat;
			sellingPriceIncVat.val(accounting.formatMoney(sellingPriceWithVat));
		},

		calculateVat: function(percentage, sellingPrice)
		{
			var vat = 0;
			if (percentage !== undefined) {
				var percentageVal = parseFloat(percentage);
				var sellingPriceVal = parseFloat(sellingPrice);
				vat = (percentageVal/100) * sellingPriceVal;
			}
			return vat;
		},

		displayPurchaseOrderInventoryItems: function(ajaxUrl)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, function(html){
				   $("#purchase_order_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						var inventoryItemId = $(this).data('item-id');
						InventoryReceived.calculateInventoryItemPrices(inventoryItemId)
				   });
				});
			} else {
				$("#purchase_order_items").html("");;
			}
		},

		calculateInventoryItemPrices: function(inventoryItemId)
		{
			console.log("CALCULATING INVENTORY ROW ", inventoryItemId," PRICE --> ");
			console.log("Calculate totals --> ", inventoryItemId );
			var price = $("#price_" + inventoryItemId);
			var ordered = $("#quantity_ordered_" + inventoryItemId);
			var received = $("#quantity_received_" + inventoryItemId);
			var short = $("#quantity_short_" + inventoryItemId);
			var vat = $("#vat_amount_" + inventoryItemId);
			var vatPercentage = $("#vat_percentage_" + inventoryItemId);
			var backOrder = $("#back_order" + inventoryItemId);

			var quantityOrdered = 0;
			var shortQuantity = 0;
			var quantityReceived = 0;
			var vatAmount = 0;
			var priceExcl = 0;

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}
			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}

			if (quantityOrdered > 0) {
				if (quantityReceived <= quantityOrdered) {
					shortQuantity =  quantityOrdered - quantityReceived
				} else {
					received.val(quantityOrdered);
					backOrder.prop('disabled', true);
					alert('Please enter valid received quantity')
					return false;
				}
			}
			if (short !== undefined  && short.length > 0) {
                short.val( shortQuantity );
            }
			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}
			console.log( vatPercentage )
			console.log( vatPercentage.length )
			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentage/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                    vat.val(vatAmount)
                }
            }
			// if (vat !== undefined && vat.length > 0) {
			// 	if (vat.val() !== '') {
			//
			// 		vatAmount += parseFloat(accounting.unformat(vat.val()));
			// 	}
			// }

			var totalExcl = priceExcl * quantityReceived;
			var priceIncl =  priceExcl + vatAmount;
			var totalInc = priceIncl * quantityReceived;

			console.log( " quantityReceived ", quantityReceived );
			console.log( " Price EXCL", priceExcl );
			console.log( " totalExcl ", totalExcl );
			console.log( " vatAmount ", vatAmount );
			console.log( " priceIncl ", priceIncl );
			console.log( " totalInc", totalInc );

			if (backOrder !== undefined) {
				if (shortQuantity === 0) {
					backOrder.prop('disabled', true);
				} else {
					backOrder.prop('disabled', false);
				}
			}

			$("#price_incl_" + inventoryItemId).html( accounting.formatMoney(priceIncl) );
			$("#price_including_" + inventoryItemId).val( priceIncl );

			$("#total_price_exl_" + inventoryItemId).html( accounting.formatMoney(totalExcl) );
			$("#total_price_excluding_" + inventoryItemId).val( totalExcl );

			$("#total_price_incl_" + inventoryItemId).html( accounting.formatMoney(totalInc) );
			$("#total_price_including_" + inventoryItemId).val( totalInc );

			InventoryReceived.calculateTotals()
		},

		calculatePriceExcluding: function(totalPriceExcl, inventoryItemId)
		{
			var received = $("#quantity_received_" + inventoryItemId);
			var quantity = 0;
			if (received.val() !== '') {
				quantity = parseFloat(received.val());
			}
			if (quantity > 0) {
                var priceExcl = parseFloat(totalPriceExcl)/quantity;
				$("#price_" + inventoryItemId).val(priceExcl);
            }
			InventoryReceived.calculateInventoryItemPrices(inventoryItemId);
		},

		calculateTotals: function()
		{
			var totalOrdered = 0;
			var totalReceived = 0;
			var totalShort = 0;
			var priceExc = 0;
			var priceInc = 0;
			var totalPrice = 0;
			var totalTotalExc = 0;
			var totalTotalInc = 0;

			$(".ordered").each(function(){
				totalOrdered += parseInt($(this).val());
			});

			$(".received").each(function(){
				totalReceived += parseInt($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseInt($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});

			$("#total_ordered").html( totalOrdered );
			$("#total_received").html( totalReceived );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.formatMoney(totalPrice) );
			$("#price_excl").html( accounting.formatMoney(priceExc) );
			$("#price_incl").html( accounting.formatMoney(priceInc) );
			$("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
			$("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );

		}

	}
}(InventoryReceived || {}, jQuery))

