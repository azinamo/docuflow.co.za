$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save_update").on('click', function (event) {
            	event.preventDefault();
            	var formId = $("#create_depreciation_update");
            	formId.LoadingOverlay("show", {
					text        : "Processing ...",
					progress    : true
				});
                ajaxSubmitForm($(this), formId);
            });

            initDatePicker();

			initChosenSelect();

        });
    });

});


var DepreciationUpdate = (function(depreciationUpdate, $){

	return {

		createUpdate: function(el) {
		   var self = $("#" + el.attr('id'));
		   var text = el.text();
		   self.html("Processing ...");
		   var url = el.data('ajax-url');
		   $.get(url, {
			 period: $("#period").val()
		   }, function(response){
		   	   if (response.error) {
		   	   	  self.html(text);
			   }
			   responseNotification(response);
		   }, 'json');
		},


	}
}(DepreciationUpdate || {}, jQuery));

