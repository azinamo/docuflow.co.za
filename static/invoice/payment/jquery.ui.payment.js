/*!
 * jQuery custom-events plugin boilerplate
 * Licensed under the MIT license
 */

// In this pattern, we use jQuery's custom events to add
// pub/sub (publish/subscribe) capabilities to widgets.
// Each widget would publish certain events and subscribe
// to others. This approach effectively helps to decouple
// the widgets and enables them to function independently.

$.widget( "ui.invoicePayments", {
    options: {
        url: null,
        start: 0,
        limit: 20,
        total 		 	  : 0,
        current			  : 1,
        section			  : "",
        page              : "",
        autoLoad          : true,
        rider_id          : 0,
        allowFilter       : false,
        loading           : false
    },

    _init       : function()
    {
        if (this.options.autoLoad) {
            this.loadPayments();
        }
    },

    _create : function() {
        var self = this;

        console.log(self.url);
        var html = [];
        html.push("<table id=\"payments\" class=\"table table-bordered\" width=\"100%\">");
            html.push(self._header());
            html.push("<tbody id='payments_body'>");
            html.push("</tbody>");
        html.push("</table>");
        $(self.element).append( html.join(' ') );
    },

    loadPayments: function()
    {
        var self = this;
        var paymentsTable = $("#payments");
        paymentsTable.LoadingOverlay('show', {"text" : "Loading payments . . . "});
        $.getJSON(self.options.url, {
            company_id : self.options.id
        }, function(response) {
            $("#payments_body").empty();
            $("#payments_footer").remove();
            console.log(response)
            self.options.total = response.count;
            self._displayPager(response);
            if ($.isEmptyObject(response.results)) {
                var html = [];
                html.push("<tr>");
                html.push("<td colspan='10'>There are no payments found.</td>");
                html.push("</tr>");
                $("#payments").append( html.join(' ') );
            } else {
                self._display( response.results );
            }
            paymentsTable.LoadingOverlay('hide');
        });
    },

    _header: function()
    {
        var self = this;
        var html = [];
        html.push("<thead>");
        html.push("<tr>");
        html.push("<th class='sorting' data-column='reference'>Remittance Number</th>");
        html.push("<th class='sorting' data-column='date_paid'>Date paid</th>");
        html.push("<th class='sorting' data-column='supplier'>Supplier</th>");
        html.push("<th class=\"text-right sorting\"  data-column='amount'>Total amount</th>");
        html.push("<th class='sorting' data-column='payment_method'>Payment method</th>");
        html.push("<th class='sorting' data-column='period'>Period</th>");
        html.push("<th class='sorting' data-column='updated'>Updated</th>");
        html.push("<th class='sorting' data-column='paid'></th>");
        html.push("<th class='sorting' data-column='pending'></th>");
        html.push("</tr>");
        html.push("<tr>");
        html.push("<td class='filter' colspan='2'>");
        html.push("</td>");
        html.push("<td class='filter' colspan='1'>");
        html.push(self._loadSuppliers());
        html.push("</td>");
        html.push("<td class='filter' colspan='1'>");
        html.push("</td>");
        html.push("<td class='filter' colspan='1'>");
        html.push(self._loadSuppliers());
        html.push("</td>");
        html.push("<td class='filter' colspan='1'>");
        html.push(self._loadSuppliers());
        html.push("</td>");
        html.push("</tr>");
        html.push("<tr>");
        html.push("<td class='filter' colspan='8'>");
        html.push("<input type='text' id='search' name='search' class='form-control form-control-sm' />");
        html.push("</td>");
        html.push("</tr>");
        html.push("</thead>");
        return html.join(' ');
    },

    _loadSuppliers: function()
    {
        var self = this;
        var html = [];
        html.push("<select name='supplier' id='supplier' class='chosen-select form-control'>");
        html.push("<option>--</option>");
        html.push("</select>");
        return html.join(' ');
    },

    _display: function(payments)
    {
        var self = this;
        var html = [];
        $.each(payments, function(index, payment){
            html.push("<tr class='payment' data-payments-url='"+ payment.payment_reference +"'>");
            html.push("<td>" + payment.payment_reference + "</td>");
            html.push("<td>" + payment.payment_date + "</td>");
            html.push("<td>" + payment.supplier + "</td>");
            html.push("<td>" + payment.total_amount + "</td>");
            html.push("<td>" + payment.payment_method + "</td>");
            html.push("<td>" + payment.period + "</td>");
            html.push("<td></td>");
            html.push("<td></td>");
            html.push("</tr>");
        });
        $("#payments_body").html( html.join(' '))
        $("#payments_body>tr>td:not(.actions)").on('click', function(){
            var detailUrl = $(this).parent('tr').data('payments-url');
            document.location.href = detailUrl;
            return false;
        });
    },

    _getPages(total, limit)
    {
        try {
            if (total%limit > 0) {
                return Math.ceil(total/limit);
            } else {
                return Math.floor(total/limit);
            }
        } catch (e) {
            return 0
        }
    },

    _displayPager	: function(response)
    {
        var self  = this;
        var html  = [];
        var pages = self._getPages(self.options.total, self.options.limit);
        html.push("<tfoot class='results' id='payments_footer'>");
        html.push("<tr class='results'>");
        html.push("<td colspan='8' class='noborder pagenation'>");
        html.push("<div class='page-navi'>");
        if (response.previous) {
           html.push("<a  data-url='" + response.previous + "'class='page previous-link' id='previous' href='#'>Previous</a>");
        }
        if (pages !== 0)
        {
            html.push("Page <select id='select_page' name='select_page' style='width: 50px;'>");
            for(var p=1; p<=pages; p++)
            {
                html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
            }
            html.push("</select>");
        } else {
            html.push("Page <select id='select_page' name='select_page' disabled='disabled' style='width: 50px;'>");
            html.push("</select>");
        }
        html.push(" of "+(pages === 0 || isNaN(pages) ? 0 : pages)+" pages");
        if (response.next) {
            html.push("<a data-url='" + response.next + "' class='page next-link' id='next' href=''>NExt \u00bb</a>");
        }
        html.push("</div>");
        html.push("</td>");
        html.push("</tr>");
        html.push("</tfoot>");
        $("#payments").append(html.join(' '));


        $("#select_page").on("change", function(){
            if($(this).val() !== "" && parseFloat($(this).val())>0)
            {
                self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
                self.options.current = parseFloat($(this).val());
                self.loadPayments();
            }
        });
        $("#search").on("click", function(e){
            var action_id = $("#a_id").val();
            if(action_id != "")
            {
                self.options.action_id = $("#d_id").val();
                self.loadPayments();
            } else {
                $("#a_id").addClass('ui-state-error');
                $("#a_id").focus().select();
            }
            e.preventDefault();
        });

        $(".page").on("click", function(e){
            e.preventDefault();
            var ajaxUrl = $(this).data('url');
            self.options.url = ajaxUrl;
            console.log(ajaxUrl, '  to ==> ', self.options.url )
            self.loadPayments();
        });

    } ,

    _getNext  			: function(self)
    {
        self.options.current = (self.options.current + 1);
        self.options.start   = (self.options.current-1) * self.options.limit;
        this.loadPayments();
    },

    _getLast  			: function( self )
    {
        self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
        self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
        this.loadPayments();
    },

    _getPrevious    	: function( self )
    {
        self.options.current  = parseFloat(self.options.current) - 1;
        self.options.start 	= (self.options.current-1) * self.options.limit;
        this.loadPayments();
    },

    _getFirst  			: function( self )
    {
        self.options.current  = 1;
        self.options.start 	= 0;
        this.loadPayments();
    } ,


    destroy: function(){
        $.Widget.prototype.destroy.apply( this, arguments );
    },
});
