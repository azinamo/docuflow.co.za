$(document).ready(function(){$(".account-opening-balance").on('blur',function(e){var el=$(this);var amount=el.val();el.removeClass('text-danger')
var amountStr=accounting.format(amount);el.val(amountStr)
var accountId=el.attr('id').replace('opening_balance_','')
OpeningBalance.update(el,accountId,amount)});let copyAccountBalances=$("#copy_opening_balances");let text=copyAccountBalances.text()
copyAccountBalances.on('click',function(e){e.preventDefault();copyAccountBalances.html("Processing ...");$.getJSON($(this).data('ajax-url'),function(response){responseNotification(response)
copyAccountBalances.html("<i class='fa fa-copy'></i> "+text);},function(){console.log('failed')});});});var OpeningBalance=(function(openingBalance,$){return{update:function(el,accountId,amount)
{const ajaxUrl=$(el).data('ajax-url');const unformatedAmount=accounting.unformat(amount);$.post(ajaxUrl,{amount:unformatedAmount},function(responseJson,statusText,xhr,$form){responseNotification(responseJson);if(!responseJson.error){el.val(accounting.formatMoney(responseJson.amount));}},'json');}}}(OpeningBalance||{},jQuery));