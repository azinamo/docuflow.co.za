$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save_update").on('click', function (event) {
            	var formId = $("#create_posting_invoice_update");
            	formId.LoadingOverlay("show", {
					text        : "Processing ...",
					progress    : true
				});
                event.preventDefault();
                submitModalForm($("form[name='create_posting_invoice_update']"));
            });
			initDatePicker();
			initChosenSelect();

        });
    });

});


var PostingUpdate = (function(postingUpdate, $){

	return {


	}
}(PostingUpdate || {}, jQuery))

