function getURLVar(key) {
    const value = [];

    const query = String(document.location).split('?');

    if (query[1]) {
        const part = query[1].split('&');

        for (let i = 0; i < part.length; i++) {
            const data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).on('click', '[data-menu-modal]', function (event) {
    event.preventDefault();
    const ajaxModal = $(".modal-ajax");
    if (ajaxModal.length > 0) {
        ajaxModal.remove();
    }

    $.get($(this).data('modal'), function (data) {
        $(data).modal('show');

        $("#comment_submit").on('click', function (event) {
            event.preventDefault();
            submitModalForm($($(this).parent()).parent());
        });

        // $("#create_distribution_journal_submit").on('click', function (e) {
        // 	submitModalForm($("#create_distribution_journals"));
        // 	return false;
        // });
        let date = new Date();
        let minDate = new Date()
        const dataMinDate = $("#id_start_date").data('min-date');
        $(".history_datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            maxDate: -1,
            yearRange: "1900:" + (date.getFullYear())
        });
        if (dataMinDate) {
            date = new Date(dataMinDate);
            minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
            $(".future_datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                minDate: minDate,
                yearRange: "-30:+100"
            });
        } else {
            $(".future_datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                minDate: +1,
                yearRange: "-30:+100"
            });
        }
        $(".chosen-select").chosen();
    });

    $('.validate_number').on('blur', function () {
        const value = $(this).val();
        if (isNaN(value)) {
            $(this).val('')
        }
    });
});

function openInvoiceDetail(el) {
    if (!$(el).hasClass('actions')) {
        var url = $(el).data('detail-url');
        if (url) {
            document.location.href = url;
        }
    }
}

function openDetail(el) {
    if (!$(el).hasClass('actions')) {
        const url = $(el).data('detail-url');
        if (url) {
            document.location.href = url;
        }
    }
}

function addButtonSpinner(button) {
    if (button.length > 0) {
        button.css('width', $(this).outerWidth());
        button.attr('data-original-html', $(this).html());
        button.html('<i class="fa fa-spinner fa-spin"></i>');
    }
}

function removeButtonSpinner(button) {
    if (button.length > 0) {
        button.html($(this).attr('data-original-html'));
    }
}

function showResponse() {

}

function getAjaxUrl(form, el, useFormUrl) {
    let formActionUrl = form.attr('action');
    if (useFormUrl === false) {
        let ajaxUrl = el.data('ajax-url');
        if (ajaxUrl !== '') {
            return ajaxUrl;
        }
    }
    return formActionUrl;
}

function ajaxSubmit(el, url, params, container = '', method = 'get', dataType = 'json') {
    if (container !== undefined && container.length > 0) {
        container.LoadingOverlay('show', {
            'text': 'Processing ...'
        });
    }
    // disable extra form submits
    ajaxFormInit(el)

    $.ajax({
        url: url,
        method: method,
        data: params,
        dataType: '',
        success: function (response) {
            responseNotification(response);
            el.css('width', el.outerWidth());
            el.html(el.data('original-html'));
            el.prop('disabled', false);
            el.removeClass('submitted');
            if (container !== undefined && container.length > 0) {
                container.LoadingOverlay("hide");
            }
            if (response.hasOwnProperty('download_url')) {
                document.location.href = response.download_url;
            }
        },
        error: function (err) {
            responseNotification({'error': true, 'text': err});
            el.css('width', el.outerWidth());
            el.html(el.data('original-html'));
            el.prop('disabled', false);
            el.removeClass('submitted');
        }
    }).done(function () {
        if (container !== undefined && container.length > 0) {
            container.LoadingOverlay("hide");
        }
    });

    // $.getJSON(url, params, function(response){
    // });
}

function ajaxSubmitForm(el, form, params, useFormUrl = true) {
    const container = $('.modal-content')
    if (container.length > 0) {
        container.LoadingOverlay('show', {
            'text': 'Processing ...'
        });
    }

    // disable extra form submits
    let actionUrl = getAjaxUrl(form, el, useFormUrl);
    form.addClass('submitted');
    ajaxFormInit(el, form);

    $('.alert-fixed').remove();
    $('.is-invalid').removeClass('is-invalid');
    $('.is-invalid-message').remove();

    return form.ajaxSubmit({
        url: actionUrl,
        target: '#loader',   // target element(s) to be updated with server response
        data: {
            ...params,
            csrfmiddlewaretoken: getCookie('csrftoken')
        },
        dataType: 'json',
        type: 'post',
        error: function (data, statusText, xhr) { 		// pre-submit callback
            if (data.status === 400) {
                responseNotification(data.responseJSON);
            } else {
                // handle errors
                const error = {'error': true, 'text': data.statusText};
                responseNotification(error);
            }
            form.removeClass('submitted');
            el.prop('disabled', false);
            container.LoadingOverlay("hide");
        },
        success: function (responseJson, statusText, xhr, $form) {
            responseNotification(responseJson);
            if (responseJson.hasOwnProperty('file')) {
                const invoicePdf = $("#invoice_pdf");
                const invoiceImage = $("#invoice_image")

                if (invoicePdf.length > 0) {
                    invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="' + responseJson.file + '"></div>');
                    PDFObject.embed(responseJson.file, "#invoice_pdf");
                }

                if (invoiceImage.length > 0) {
                    invoiceImage.val(responseJson.file);
                }
                if (jQuery.fn.matchHeight) {
                    $('.height').matchHeight();
                    $('.invoice-col').matchHeight();
                }
            }
            if (responseJson.error) {
                el.prop('disabled', false);
            }
        }
    }).data('jqxhr');
}

function clearModals() {
    const modalAjax = $(".modal-ajax");
    if (modalAjax.length > 0) {
        modalAjax.remove();
    }
    $(".modal-backdrop").remove()
}

function ajaxFormInit(el, form) {
    $('.alert-fixed').remove();
    $('.is-invalid').removeClass('is-invalid');
    $('.is-invalid-message').remove();

    jQuery(document).ajaxStart(function () {
        if (el.tagName === 'BUTTON' || el.tagName === 'A' || el.tagName === 'LABEL') {
            el.addClass('loading');
        } else {
            el.addClass('loading-start');
            el.prop('disabled', true);
        }
    }).ajaxStop(function () {
        el.removeClass('loading-start').removeClass('loading').addClass('loading-stop');
        // el.prop('disabled', false);
        if (form) {
            form.LoadingOverlay("hide");
        }
    }).ajaxError(function () {
        el.removeClass('loading-start').removeClass('loading').removeClass('loading-stop').addClass('loading-error');
        el.prop('disabled', false);
        if (form) {
            form.LoadingOverlay("hide");
        }
    });
}

function submitForm(form, params) {
    const loaderDiv = $("#loader");
    if (loaderDiv !== undefined) {
        loaderDiv.show();
    }
    // disable extra form submits
    form.addClass('submitted');
    const submitBtn = form.find("button[type='button']", "button[type='submit']");
    submitBtn.each(function () {
        $(this).css('width', $(this).outerWidth());
        $(this).attr('data-original-html', $(this).html());
        $(this).html('<i class="fa fa-spinner fa-spin"></i>');
    });

    // remove existing alert & invalid field info
    $('.alert-fixed').remove();
    $('.is-invalid').removeClass('is-invalid');
    $('.is-invalid-message').remove();
    form.ajaxSubmit({
        url: form.attr('action'),
        target: '#loader',   // target element(s) to be updated with server response
        data: params,
        beforeSerialize: function ($form, options) {
            // nothing
        },
        beforeSubmit: function (formData, jqForm, options) {
            var queryString = $.param(formData);
        },  // pre-submit callback
        error: function (data) {
            // handle errors
            responseNotification(data)
        },
        success: function (responseJson, statusText, xhr, $form) {
            // handle response
            // if ('url' in responseJson) {
            // 	ajaxNotificationDialog(responseJson.url);
            // }
            responseNotification(responseJson);
            if (responseJson.hasOwnProperty('file')) {
                const invoicePdf = $("#invoice_pdf");
                const invoiceImg = $("#invoice_image");
                if (invoicePdf.length > 0) {
                    invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="' + responseJson.file + '"></div>');
                    PDFObject.embed(responseJson.file, "#invoice_pdf");
                }
                if (invoiceImg.length > 0) {
                    invoiceImg.val(responseJson.file);
                }
            }
            submitBtn.each(function () {
                $(this).css('width', $(this).outerWidth());
                $(this).html($(this).data('original-html'));
            });
            if (loaderDiv !== undefined) {
                loaderDiv.hide();
            }

        },  // post-submit callback
        dataType: 'json',
        type: 'post'
        // $.ajax options can be used here too, for example:
        //timeout:   3000
    });
}

function submitModalForm(form) {
    // disable extra form submits
    form.addClass('submitted');
    form.find("button[type='submit']").each(function () {
        $(this).css('width', $(this).outerWidth());
        $(this).attr('data-original-html', $(this).html());
        $(this).html('<i class="fa fa-spinner fa-spin"></i>');
    });

    // remove existing alert & invalid field info
    $('.alert-fixed').remove();
    $('.is-invalid').removeClass('is-invalid');
    $('.is-invalid-message').remove();
    form.ajaxSubmit({
        url: form.attr('action'),
        target: '#loader',   // target element(s) to be updated with server response
        beforeSubmit: function (formData, jqForm, options) {
            const queryString = $.param(formData);
        },  // pre-submit callback
        error: function (data) {
            // handle errors
            if (data.status === 400) {
                responseNotification(data.responseJSON);
            } else {
                // handle errors
                const error = {'error': true, 'text': data.statusText};
                responseNotification(error);
            }
            form.LoadingOverlay("hide");
        },
        success: function (responseJson, statusText, xhr, $form) {
            // handle response
            if (responseJson.error) {
                $("#make_payment").prop('disabled', false);
            }
            form.LoadingOverlay("hide");
            responseNotification(responseJson);

            if (responseJson.hasOwnProperty('file')) {
                const invoicePdf = $("#invoice_pdf");
                const invoiceImage = $("#invoice_image");
                if (invoicePdf.length > 0) {
                    invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="' + responseJson.file + '"></div>');
                    PDFObject.embed(responseJson.file, "#invoice_pdf");
                }
                if (invoiceImage.length > 0) {
                    invoiceImage.val(responseJson.file);
                }
            }
        },  // post-submit callback
        dataType: 'json',
        type: 'post'
        // $.ajax options can be used here too, for example:
        //timeout:   3000
    });
}

function flash(hasError, alertMessage, delay = 1000) {
    const html = '<div class="alert alert-' + (hasError === false ? 'success' : 'danger') + ' alert-fixed mt-3 mb-0 text-white text-white-50">' + alertMessage + '</div>';
    if (hasError === false) {
        $(html).prependTo('.modal-body').delay(delay).queue(function () {
            $(this).remove()
            //document.location.reload();
        });
    } else {
        $(html).prependTo('.modal-body').delay(delay);
    }
}

function alertMessage(message, isError = false, title = '') {
    const notificationModal = $("#notification-modal");
    if (notificationModal.length > 0) {
        notificationModal.remove();
    }
    const html = []

    html.push('<div class="modal modal-notification show" tabindex="-1" data-backdrop="static" id="notification-modal">');
    html.push('<div class="modal-dialog modal-lg">');
    html.push('<div class="modal-content">');
    html.push('<div class="modal-header">');
    html.push('<button type="button" class="close notification-close" data-dismiss="modal" aria-hidden="true">&times;</button>');
    html.push('<h4 class="modal-title" id="model-title">' + title + '</h4>');
    html.push('</div>');
    html.push('<div class="modal-body" id="modal_body">');
    if (isError) {
        html.push('<div class="text-danger">' + message + '</div>');
    } else {
        html.push('<div class="text-success">' + message + '</div>');
    }
    html.push('</div>');
    html.push('<div class="modal-footer" id="modalfooter">');
    html.push('<a type="button" class="btn btn-primary notification-close" data-dismiss="modal">Ok</a>');
    html.push('</div>');
    html.push('</div>');
    html.push('</div>');
    html.push('</div>');

    $("body").append(html.join(' '));

    $(".notification-close").on('click', function (e) {
        $("#notification-modal").remove();
        e.preventDefault();
    });
}

function highlightFormErrors(errors) {
    let scrollTo, counter = 0;
    let generalErrors = [];
    $.each(errors, function (key, value) {
        const keyElement = $("#id_" + key);
        const idElement = $("#" + key);
        let chosenElement;
        let element;
        if (keyElement.length) {
            element = keyElement;
            chosenElement = $("#id_" + key + "_chosen");
        } else if (idElement.length) {
            element = idElement;
            chosenElement = $("#" + key + "_chosen");
        }
        if (chosenElement !== undefined && chosenElement.length > 0) {
            element = chosenElement;
            displayFormError(element, value[0]);
        } else if (element !== undefined && element.length > 0) {
            displayFormError(element, value[0]);
        }
        counter++;
    });

    const errorDiv = $(".error");
    if (errorDiv.length > 0) {
        $('html, body').animate({
            scrollTop: (errorDiv.first().offset().top)
        }, 500);
    }
}

function displayFormError(element, error_message) {
    if (element.length > 0) {
        if (!element.hasClass("is-invalid")) {
            element.addClass('is-invalid');
            element.after('<div class="is-invalid-message has-error">' + error_message + '</div>');
            // TODO if the form is a horizontal form, look for the second parent div and highlight that
            // TODO otherwise highligh the first parent div
            const secondParent = $(element.parent()).parent();
            if (secondParent.length) {
                if (secondParent[0].nodeName === 'DIV') {
                    secondParent.addClass('error').addClass('has-error').addClass('text-danger');
                } else {
                    $(element.parent()).addClass('error').addClass('has-error').addClass('text-danger');
                }
            }
        }
    }
}

function removeFormError(element) {
    if (element.length > 0) {
        if (element.hasClass("is-invalid")) {
            element.removeClass('is-invalid');
            // TODO if the form is a horizontal form, look for the second parent div and highlight that
            // TODO otherwise highligh the first parent div
            var secondParent = $(element.parent()).parent();
            if (secondParent.length) {
                if (secondParent[0].nodeName === 'DIV') {
                    secondParent.find('div.is-invalid-message').remove();
                    secondParent.removeClass('error').removeClass('has-error').removeClass('text-danger');
                } else {
                    $(element.parent()).find('div.is-invalid-message').remove();
                    $(element.parent()).removeClass('error').removeClass('has-error').removeClass('text-danger');
                }
            }
        }
    }
}

function getGeneralErrors(errors) {
    let _generalErrors = []
    $.each(errors, function (key, value) {
        if (key === '__all__') {
            _generalErrors.push("<li class='error'><div class='is-invalid-message has-error'>" + value[0] + "</div></li>");
        }
    })
    if (_generalErrors.length > 0) {
        const html = [];
        html.push("<ul>");
        html.push(_generalErrors.join(' '));
        html.push("</ul>");
        return html.join(' ');
    }
    return '';
}

function clearLoaders() {
    const modalContent = $(".modal-content");
    if (modalContent.length > 0) {
        if (jQuery.fn.LoadingOverlay) {
            modalContent.LoadingOverlay("hide");
        }
    } else {
        if (jQuery.fn.LoadingOverlay) {
            $(document).LoadingOverlay("hide");
            const loadingOverlayClass = $(".loadingoverlay");
            if (loadingOverlayClass.length > 0) {
                loadingOverlayClass.remove();
            }
        }
    }

}

function isError(response) {
    if (response.hasOwnProperty('error')) {
        return response.error;
    }
    return false;
}

function responseNotification(jsonResponse, form) {
    if (jsonResponse.hasOwnProperty('alert')) {
        if (jsonResponse.hasOwnProperty('url')) {
            ajaxNotificationDialog(jsonResponse.url)
        } else {
            const html = []
            html.push(jsonResponse.text);
            if (jsonResponse.hasOwnProperty('errors')) {
                html.push("<br />");
                html.push(getGeneralErrors(jsonResponse.errors))
            }
            clearLoaders()
            alertMessage(html.join(' '), isError(jsonResponse), jsonResponse.hasOwnProperty('title') ? jsonResponse.title : '')
        }

        if (jsonResponse.hasOwnProperty('errors')) {
            clearLoaders()
            highlightFormErrors(jsonResponse.errors)
        }

    } else {
        const isModal = isAModal();
        if (jsonResponse.hasOwnProperty('error')) {

            clearLoaders();

            if (isModal) {
                flash(jsonResponse.error, jsonResponse.text);
            } else {
                docuflowNotify(jsonResponse)
            }
        }
        if (jsonResponse.hasOwnProperty('warning')) {
            clearLoaders()
            docuflowNotify(jsonResponse, 'warning')
        }

        if (jsonResponse.hasOwnProperty('errors')) {
            displayErrors(jsonResponse['errors']);
        }
        handleAfterResponse(jsonResponse)
    }
}

function getSelectedItems(className, idDataAtrr) {
    const selectedItems = [];
    $("." + className).each(function () {
        if ($(this).is(":checked")) {
            selectedItems.push($(this).data(idDataAtrr));
        }
    });
    return selectedItems;
}

function docuflowNotify(jsonResponse, flag = '') {
    if (!flag || flag === '') {
        flag = jsonResponse.error ? 'danger' : 'success';
    }
    $.notify({
        title: '',
        message: jsonResponse.text
    }, {
        type: flag
    });
}

function isAModal() {
    const ajaxModal = $(".modal-ajax");
    if (ajaxModal.length > 0) {
        return true;
    }
    return false;
}

function ajaxNotificationDialog(url, params) {
    const ajaxModal = $(".modal");
    if (ajaxModal.length > 0) {
        $(".modal-backdrop").remove()
        ajaxModal.remove();
    }

    $.get(url, params, function (data) {
        $(data).modal('show');
        const formEL = $(data).find("form");
        $(".invoice-amount").on("blur", function () {
            const invoiceAmount = parseFloat($("#total_invoice_amount").val());
            let allocatedTotal = 0;
            $(".invoice-amount").each(function () {
                const methodAmount = $(this).val();
                if (methodAmount !== undefined && methodAmount !== "") {
                    allocatedTotal += parseFloat(methodAmount);
                }
            });
            let balance = invoiceAmount - allocatedTotal;
            if (balance === 0) {
                $("#save_receipts").prop("disabled", false);
            } else {
                $("#save_receipts").prop("disabled", true);
            }
            $("#balance").html(accounting.formatMoney(balance));
        });

        $(".modal-save").on("click", function (e) {
            e.preventDefault();
            ajaxSubmitForm($(this), $("#" + $(formEL).attr("id")))
        });

        $("#sign_invoice_with_variance_submit").on('click', function (event) {
            event.stopImmediatePropagation();
            Invoice.signInvoice($(this));
        });

        $(".discount-invoice-check").on('change', function (e) {
            if ($(this).is(":checked")) {
                Payment.addDiscount($(this))
            } else {
                Payment.removeDiscount($(this))
            }
            Payment.allChecked($(".discount-invoice-check"), $("#discount_invoice"))
        });

        $("#discount_invoice").on('change', function (e) {
            if ($(this).is(":checked")) {
                $(".discount-invoice-check").each(function () {
                    var _this = $(this);
                    if (!_this.is(":checked")) {
                        Payment.addDiscount(_this)
                        _this.prop('checked', true)
                    }
                })
            } else {
                $(".discount-invoice-check").each(function () {
                    var _this = $(this);
                    Payment.removeDiscount(_this);
                    _this.prop('checked', false)
                })
            }
        });

        $("#id_discount_disallowed").on("blur", function (e) {
            Payment.calculateNetPayment();
        });

        $("#id_payment_discount_disallowed").on("blur", function (e) {
            Payment.calculateNetPayment();
        });

        $("#id_payment_total_amount").on("blur", function (e) {
            var netTotalEl = $("#id_payment_net_amount");
            var selectedDiscountEl = $("#id_payment_total_discount");
            var allowedDiscountEl = $("#id_payment_discount_disallowed");
            var totalPaymentAmountEl = $("#id_payment_total_amount") || $("#id_total_amount");
            var selectedDiscount = selectedDiscountEl.val();
            var discountAllowed = allowedDiscountEl.val();
            var totalDiscount = 0;
            if (discountAllowed !== '') {
                totalDiscount += parseFloat(discountAllowed);
            }
            if (selectedDiscount !== '') {
                totalDiscount += parseFloat(selectedDiscount);
            }

            var total = 0;
            if (totalPaymentAmountEl.length > 0) {
                total = totalPaymentAmountEl.val();
            } else {
                total = totalPaymentAmountEl.val();
            }

            var netAmount = parseFloat(total) - totalDiscount;
            netTotalEl.val(netAmount.toFixed(2));
        });

        $("#make_payment").on('click', function (e) {
            e.preventDefault();
            const payInvoices = $("#pay_invoices");
            $(this).prop('disabled', true);
            if (payInvoices.length > 0) {
                ajaxSubmitForm($(this), payInvoices);
            } else {
                ajaxSubmitForm($(this), $("#pay_supplier_invoices"));
            }
        });

        $("#copy_inventory").on('click', function (event) {
            var form = $("#copy_inventory_form");
            form.LoadingOverlay('show', {
                'text': 'Processing ...'
            });
            submitModalForm(form);
            return false
        });

        $(".datepicker").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $(".chosen-select").chosen();

    });
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    let timeout;
    return function () {
        const context = this, args = arguments;
        const later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        const callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
          func.apply(context, args);
        }
    };
};

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function ajaxPost(el, params = {}) {
	el.submitButton('loading');
	$.post(el.data('ajax-url'), {
		...params,
		csrfmiddlewaretoken: getCookie('csrftoken')
	}, function(response){
		el.submitButton('reset');
		responseNotification(response);
	}, 'json');
}

var csrftoken = getCookie('csrftoken');

function printHtml(elId) {
    var tableToPrint = document.getElementById(elId);
    var htmlToPrint = '';
    htmlToPrint += tableToPrint.outerHTML;
    newWin = window.open("")
    newWin.document.write(htmlToPrint);
    // newWin.print();
    // newWin.close();
}

function ajaxGet(el, params) {
    return $.getJSON(el.data('ajax-url'), params, function (response) {
        responseNotification(response)
    });
}

function initDatePicker() {
    let minDate = new Date();
    let inputMinDate = $("#id_start_date").data('min-date');
    if (inputMinDate) {
        let date = new Date(inputMinDate);
        minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
    }
    let datePickerClass = $(".datepicker");
    if (datePickerClass.length > 0) {
        datePickerClass.datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
    }
    let historyDatePickerClass = $(".history_datepicker");
    if (historyDatePickerClass.length > 0) {
        historyDatePickerClass.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            maxDate: -1,
            yearRange: "1900:" + (date.getFullYear())
        });
    }

    let futureDatePickerClass = $(".future_datepicker");
    if (futureDatePickerClass.length > 0) {
        futureDatePickerClass.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            minDate: minDate,
            yearRange: "-30:+100"
        });
    }

    let todayFutureDatePickerClass = $(".today-future");
    if (todayFutureDatePickerClass.length > 0) {
        todayFutureDatePickerClass.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            minDate: +0,
            yearRange: "0:+100"
        });
    }

    let _futureDatePickerClass = $(".future");
    if (_futureDatePickerClass.length > 0) {
        _futureDatePickerClass.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            minDate: +1,
            yearRange: "0:+100"
        });
    }
}

function initChosenSelect() {
    const chosenEl = $(".chosen-select");
    if (chosenEl !== undefined && chosenEl.length > 0) {
        chosenEl.chosen();
    }
}

function initQuantityFormatter() {
    $(".quantity").on('change', function () {
        handleInventoryQuantity($(this));
    });
}

function calculateSelectWidth(el) {
    let chosenWidth = 250;
    if (el === undefined) {
        chosenWidth = 250;
    } else {
        chosenWidth = el.width();
    }
    if (chosenWidth === null || chosenWidth === 0) {
        chosenWidth = 250;
    }
    return chosenWidth
}

function calculateChosenSelectWidth(containerClass) {
    let chosenWidth = 250;
    containerClass = "." + containerClass || '';
    const chosenContainer = $("" + containerClass + " > .chosen-container");
    if (chosenContainer.length > 0) {
        chosenWidth = chosenContainer.width();
    }
    if (chosenWidth > 0) {
        $(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
        $(".line-type").chosen({'width': '40px'});
    } else {
        $(".chosen-select").not('.line-type').chosen({'width': '250px'});
        $(".line-type").chosen({'width': '40px'});
    }
    return chosenWidth;
}

function showContent(el) {
    const contentId = el.data('content-id');
    $("#content_" + contentId).hide();
    $("#full_content_" + contentId).show();
    el.hide();
    $("#hide_" + contentId).show('slow');
}

async function destroyExistingModals(el) {
    await $(".modal-ajax").each(function () {
        $(this).remove();
    });
    return 'done';
}

function hideContent(el) {
    const contentId = el.data('content-id');
    $("#content_" + contentId).show('fast');
    $("#full_content_" + contentId).hide('slow');
    el.hide();
    $("#show_" + contentId).show('slow');
}

function handleInventoryQuantity(el, decimals = 4) {
    const thisQty = accounting.toFixed(el.val(), decimals);
    el.val(thisQty);
}

function highlightFoundErrors() {
    $('.text-danger').each(function () {
        const element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });
}

function calculatePercentageAmount(percentage, amount, fixed = 2) {
    if (typeof percentage === 'string') {
        percentage = parseFloat(percentage);
    }
    if (typeof amount === 'string') {
        amount = parseFloat(amount);
    }
    const result = ((percentage / 100) * amount).toFixed(fixed)
    return parseFloat(result)
}

function hasElement(el) {
    return el !== undefined;
}

function getFieldValue(field) {
    if (field === undefined) {
        return 0;
    }
    if (field.length === 0) {
        return 0;
    }
    const elVal = field.val();
    if (elVal === "") {
        return 0;
    }
    return parseFloat(accounting.unformat(elVal));
}

function calculateGrossProfit(priceExcl, averagePrice) {
    return (priceExcl - averagePrice) / priceExcl;
}

function isNegativeStockAllowed(rowId) {
    const allowNegativeStockEl = $("#allow_negative_stock_" + rowId);
    if (!allowNegativeStockEl) {
        return true;
    }
    const allowNegativeStockVal = allowNegativeStockEl.val();
    if (allowNegativeStockVal === "") {
        return true
    }
    return allowNegativeStockVal === '1' || allowNegativeStockVal === 1;
}

function isPicking(rowId) {
    return $("#picked_" + rowId).length > 0;
}

handleRowDiscount = function (rowId, isDiscountable, lineDiscount) {
    const discountPercentageEl = $("#discount_percentage_" + rowId);
    if (isDiscountable) {
        let discountVal = lineDiscount || 0;
        discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
    } else {
        discountPercentageEl.val(0).prop('disabled', true).trigger('change');
        $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
        $("#discount_amount_" + rowId).val('0.00').trigger('change');
    }
};

function handleOrderedQuantity(rowId, quantityEl) {
    calculateItemPrices(rowId);
}

function calculateItemPrices(rowId) {
    const price = $("#price_" + rowId);
    const priceIncl = $("#price_including_" + rowId);
    const ordered = $("#quantity_ordered_" + rowId);
    const received = $("#quantity_" + rowId);
    const short = $("#quantity_short_" + rowId);
    const vat = $("#vat_amount_" + rowId);
    const vatPercentage = $("#vat_percentage_" + rowId);
    const discountPercentage = $("#discount_percentage_" + rowId);
    const discount = $("#discount_" + rowId);
    const averagePrice = $("#average_price_" + rowId);

    let quantityOrdered = 0;
    let shortQuantity = 0;
    let quantityReceived = 0;
    let vatAmount = 0;
    let priceExcl = 0;
    let discountAmount = 0;
    let totalVat = 0;
    let totalDiscount = 0;
    let priceIncludingAmount = 0;
    let totalInc = 0;
    let totalExcl = 0;
    let subTotal = 0;

    quantityOrdered = getFieldValue(ordered)
    quantityReceived = getFieldValue(received)

    console.log("Ordered quantity --> ", quantityOrdered, ' quantity received', quantityReceived)

    if (quantityOrdered > 0) {
        if (quantityReceived <= quantityOrdered) {
            shortQuantity = quantityOrdered - quantityReceived
        } else {
            received.val(quantityOrdered);
            // backOrder.prop('disabled', true);
            // alert("Please enter valid received quantity")
            return false;
        }
    }
    if (short !== undefined && short.length > 0) {
        short.val(shortQuantity);
    }
    if (price !== undefined && received.length > 0) {
        priceExcl = getFieldValue(price)
    }

    subTotal = priceExcl * quantityReceived;

    const vatPerc = getFieldValue(vatPercentage)
    if (vatPerc && priceExcl) {
        vatAmount = calculatePercentageAmount(vatPerc, priceExcl);
    }

    priceIncludingAmount = priceExcl + vatAmount;

    const discountPerc = getFieldValue(discountPercentage)
    if (discountPerc && priceExcl) {
        discountAmount = calculatePercentageAmount(discountPerc, priceExcl);
        discount.val(discountAmount);
    }

    if (discountAmount > 0) {
        priceExcl = priceExcl - discountAmount;
    }

    if (discountAmount) {
        totalDiscount = discountAmount * quantityReceived;
        totalDiscount = parseFloat(accounting.toFixed(totalDiscount, 2))
    }
    // subTotal = subTotal - totalDiscount;
    totalExcl = priceExcl * quantityReceived;
    totalInc = totalExcl;

    if (vatPerc && priceExcl) {
        vatAmount = calculatePercentageAmount(vatPerc, priceExcl);
        totalVat = calculatePercentageAmount(vatPerc, totalExcl);
        totalInc += totalVat;
    }

    if (averagePrice.length > 0) {
        let _averagePrice = parseFloat(averagePrice.val());
        let grossProfit = calculateGrossProfit(averagePrice, quantityOrdered);
        // console.log( " grossProfit ", grossProfit, grossProfit * 100 );
        $("#gross_profit_" + rowId).html(accounting.toFixed(grossProfit * 100) + '%')
    }

    $("#vat_total_" + rowId).html(accounting.toFixed(totalVat, 2));
    $("#total_vat_" + rowId).val(accounting.toFixed(totalVat, 2));
    vat.val(accounting.toFixed(totalVat, 2));

    $("#discount_amount_" + rowId).html(accounting.toFixed(totalDiscount, 2));
    $("#total_discount_" + rowId).val(accounting.toFixed(totalDiscount, 2));

    $("#price_incl_" + rowId).html(accounting.toFixed(priceIncludingAmount, 2));
    $("#sub_total_" + rowId).val(accounting.toFixed(subTotal, 2));

    priceIncl.val(priceIncludingAmount);

    $("#total_price_exl_" + rowId).html(accounting.toFixed(totalExcl, 2));
    $("#total_price_excluding_" + rowId).val(accounting.toFixed(totalExcl, 2));

    $("#total_price_incl_" + rowId).html(accounting.toFixed(totalInc, 2));
    $("#total_price_including_" + rowId).val(accounting.toFixed(totalInc, 2));

    calculateTotals();
}

function calculateTotals() {
    console.log("<----------------------------------- calculateTotals ------------------------------------------->")
    let totalOrdered = 0;
    let totalReceived = 0;
    let totalShort = 0;
    let priceExc = 0;
    let priceInc = 0;
    let totalPrice = 0;
    let totalTotalExc = 0;
    let totalTotalInc = 0;
    let totalVatAmount = 0;
    let totalDiscountAmount = 0;
    let subTotalAmount = 0;
    let netTotal = 0;
    let invoiceDiscountAmount = 0;
    let netWeight = 0;
    let grossWeight = 0;

    $(".ordered").each(function () {
        totalOrdered += parseInt($(this).val());
    });

    $(".received, .quantity").each(function () {
        totalReceived += parseInt($(this).val());
    });
    $(".short").each(function () {
        totalShort += parseInt($(this).val());
    });
    $(".price").each(function () {
        totalPrice += parseFloat(accounting.unformat($(this).val()));
    });
    $(".price-exc").each(function () {
        priceExc += parseFloat(accounting.unformat($(this).val()));
    });
    $(".price-incl").each(function () {
        priceInc += parseFloat(accounting.unformat($(this).val()));
    });
    $(".total-price-excl").each(function () {
        totalTotalExc += parseFloat(accounting.unformat($(this).val()));
    });
    $(".total-price-incl").each(function () {
        totalTotalInc += parseFloat(accounting.unformat($(this).val()));
    });
    $(".total-vat-amount").each(function () {
        totalVatAmount += parseFloat(accounting.unformat($(this).val()));
    });
    $(".total-discount").each(function () {
        totalDiscountAmount += parseFloat(accounting.unformat($(this).val()));
    });
    $(".sub-total").each(function () {
        subTotalAmount += parseFloat(accounting.unformat($(this).val()));
    });
    $(".net-weight").each(function () {
        const rowId = $(this).data('row-id');
        netWeight += parseFloat(accounting.unformat($(this).val())) * parseFloat(accounting.unformat($("#quantity_" + rowId).val()));
    });
    $(".gross-weight").each(function () {
        const rowId = $(this).data('row-id');
        grossWeight += parseFloat(accounting.unformat($(this).val())) * parseFloat(accounting.unformat($("#quantity_" + rowId).val()));
    });
    subTotalAmount -= totalDiscountAmount;

    totalDiscountAmount = totalDiscountAmount * -1;
    $("#id_line_discount").val(accounting.toFixed(totalDiscountAmount, 2));
    netTotal = subTotalAmount;
    const invoiceDiscount = $("#invoice_discount").val();
    const defaultVat = parseFloat($("#default_vat").val());
    const customerDiscountsEl = $("#customer_discounts");
    let totalInvoiceDiscount = 0;
    let vatOnInvoiceDiscount = 0;

    if (invoiceDiscount !== undefined && invoiceDiscount !== "") {
        customerDiscountsEl.show();

        if (netTotal !== "") {
            totalInvoiceDiscount = (Math.abs(parseFloat(invoiceDiscount) / 100) * netTotal) * -1;
            vatOnInvoiceDiscount = calculateVatAmount(defaultVat, totalInvoiceDiscount)
            totalVatAmount += vatOnInvoiceDiscount;
            netTotal = netTotal + totalInvoiceDiscount;
        }
    } else {
        customerDiscountsEl.hide();
    }
    totalVatAmount = parseFloat(accounting.toFixed(totalVatAmount, 2))
    const totalOrderValue = netTotal + totalVatAmount;
    const totalNetWeight = netWeight;
    const totalGrossWeight = grossWeight;
    $("#customer_invoice_discounts").html(accounting.toFixed(totalInvoiceDiscount, 2));
    $("#id_customer_discount").val(accounting.toFixed(totalInvoiceDiscount, 2));

    $("#sub_total").html(accounting.toFixed(subTotalAmount, 2));
    $("#id_sub_total").val(accounting.toFixed(subTotalAmount, 2));

    $("#net_total").html(accounting.toFixed(netTotal, 2));
    $("#id_net_amount").val(accounting.toFixed(netTotal, 2));

    $("#total_ordered").html(accounting.toFixed(totalOrdered, 2));
    $("#total_received").html(accounting.toFixed(totalReceived, 2));
    $("#total_net_weight").html(accounting.toFixed(totalNetWeight, 2));
    $("#total_gross_weight").html(accounting.toFixed(totalGrossWeight, 2));
    $("#total_short").html(totalShort);
    $("#total_price").html(accounting.toFixed(totalPrice, 2));
    $("#price_excl").html(accounting.toFixed(priceExc, 2));
    $("#price_incl").html(accounting.toFixed(priceInc, 2));
    $("#total_price_excl").html(accounting.toFixed(totalTotalExc), 2);
    $("#total_price_inc").html(accounting.toFixed(totalTotalInc, 2));

    $("#total_vat_amount").html(accounting.toFixed(totalVatAmount, 2));
    $("#id_vat_amount").val(accounting.toFixed(totalVatAmount, 2));

    $("#total_discount").html(accounting.toFixed(totalDiscountAmount, 2));
    $("#id_discount_amount").val(accounting.toFixed(totalDiscountAmount, 2));

    $("#total_order_value").html(accounting.toFixed(totalOrderValue, 2));
    $("#id_total_amount").val(accounting.toFixed(totalOrderValue, 2));

}

function calculateVatAmount(vatPercentage, amount) {
    let _vatAmount = 0;
    let vatAmount = 0
    if (vatPercentage !== undefined && vatPercentage !== 0) {
        _vatAmount = (vatPercentage / 100) * amount;
    }
    let _vatAmountRounded = _vatAmount
    let multiplier = 1;
    if (_vatAmount < 0) {
        _vatAmountRounded = _vatAmount * -1
        multiplier = -1
    }
    vatAmount = accounting.toFixed(_vatAmountRounded, 2) * multiplier
    return vatAmount
}

function getMonthName(month) {
    return {
        0: 'Jan',
        1: 'Feb',
        2: 'Mar',
        3: 'Apr',
        4: 'May',
        5: 'Jun',
        6: 'Jul',
        7: 'Aug',
        8: 'Sept',
        9: 'Oct',
        10: 'Nov',
        11: 'Dec'
    }[month]
}

function dueDays(days) {
    if (days < 0) {
        return "<i class='fa fa-clock-o text-danger' data-toggle='tooltip' data-original-title='Overdue by " + days + "' days'></i>";
    } else if (days === 0) {
        return "<i class='fa fa-clock-o text-primary' data-toggle='tooltip' data-original-title='Due today'></i>";
    } else if (days < 7) {
        return "<i class='fa fa-clock-o text-warning' data-toggle='tooltip' data-original-title='Due in " + days + "' days'></i>";
    } else {
        return "<i class='fa fa-clock-o text-success' data-toggle='tooltip' data-original-title='Due in " + days + "' days'></i>";
    }
}

function displayDate(date) {
    const momentDate = moment(date)
    if (momentDate) {
        return momentDate.format('MMM, DD YYYY');
    }
    return '-';
}

function modalSubmit(el) {
	el.submitButton('loading');
	$("#modal_form").ajaxSubmit({
		beforeSubmit:  function(formData, jqForm, options) {
			const queryString = $.param(formData);
		},
		error: function (data) {
			const error = {'error': true, 'text': data.statusText};
			if (data.status === 400) {
				modalResponseNotification(data.responseJSON);
			} else {
				// handle errors
				const error = {'error': true, 'text': data.statusText};
				modalResponseNotification(error);
			}
			el.submitButton('reset');
		},
		success: function(response, statusText, xhr, $form)
		{
			el.submitButton('reset');
			modalResponseNotification(response);
		}
	})
}

function modalResponseNotification(jsonResponse)
{
	$(".server-error").remove();
	const hasError = jsonResponse.error;

	if (jsonResponse.text) {
		const html = '<div class="alert alert-' + (hasError === false ? 'success' : 'danger') + ' alert-fixed mt-1 mb-1 server-error">' + jsonResponse.text + '</div>';
		$(html).prependTo('.modal-body');
	}

	if (jsonResponse.hasOwnProperty('errors')) {
		displayErrors(jsonResponse.errors, jsonResponse.error)
	}

	handleAfterResponse(jsonResponse)
}

function handleAfterResponse(jsonResponse) {
	if (jsonResponse.hasOwnProperty('url')) {
		return ajaxNotificationDialog(jsonResponse.url)
	} else if (jsonResponse.hasOwnProperty('redirect') || jsonResponse.hasOwnProperty('redirect_url')) {
		document.location.href = jsonResponse.redirect || jsonResponse.redirect_url
	} else if (jsonResponse.hasOwnProperty('reload') && jsonResponse.reload) {
		setTimeout(function(){
			document.location.reload()
		}, 1500)
	}
	//
	// dismiss modal
	if (jsonResponse.hasOwnProperty('dismiss_modal')) {
		const modal = $(".modal-ajax");
		if (modal.length) {
			modal.modal('hide');
			modal.remove()
		}
	}

	// reload page
	if (jsonResponse.hasOwnProperty('reload_page')) {
		location.reload();
	}

	// reload datatables
	if (jsonResponse.hasOwnProperty('reload_datatables')) {
		$($.fn.dataTable.tables()).DataTable().ajax.reload();
	}

	// reload sources
	if (jsonResponse.hasOwnProperty('reload_sources')) {
		$('[data-source]').each(function () {
			$.get($(this).data('source'), function (data) {
				$(this).html(data);
			});
		});
	}

}

function displayErrors(errors, isError=true) {
	const errorMessageContainer = $("#error_messages");
	const html = [];
	let firstEl = null;
	$.each(errors, function(key, error){
		const el = $("#id_" + key);
		if (firstEl === null || firstEl.length === 0) {
			firstEl = el;
		}
		if (el.length) {
			displayFormError(el, error);
		} else {
			if (key !== 'error') {
				if (key === '__all__') {
					$.each(error, function(k, err){
						html.push("<p class='mb-2'>" + err + "</p>");
					});
				} else {
					displayFormError($("#id_" + key), error)
				}
			}
		}
	});
	if (errorMessageContainer.length > 0) {
		if (isError) {
			errorMessageContainer.html(html.join(' ')).addClass('text-danger');
		} else {
			errorMessageContainer.html(html.join(' ')).addClass('text-success');
		}
	} else {
		$(html).prependTo('.modal-body');
	}

	if (firstEl.length) {
		jQuery('html, body').animate({
			scrollTop: firstEl.offset().top - 50
		}, 'slow');
	}
}

function handleLoadingButton(action, el) {
    if (action === 'loading' && el.data('loading-text')) {
        el.data('original-text', el.html()).html(el.data('loading-text')).prop('disabled', true);
    }
    if (action === 'reset' && el.data('original-text')) {
        el.html(el.data('original-text')).prop('disabled', false);
    }
}

$(document).ready(function () {

    $(document).on("click", "#modal_save_btn", function(e){
        e.preventDefault();
        modalSubmit($(this));
    });

    $(".table-items-list>tbody>tr>td:not(.actions)").on("click", function () {
        const detailUrl = $(this).parent("tr").data("detail-url");
        if (detailUrl !== undefined && detailUrl !== '') {
            document.location.href = detailUrl;
        }
        return false;
    });

    $(".content-show").on('click', function (e) {
        e.preventDefault();
        showContent($(this));
    });

    $(".content-hide").on('click', function (e) {
        e.preventDefault();
        hideContent($(this));
    });

    //Form Submit for IE Browser
    $('button[type=\'submit\']').on('click', function () {
        $("form[id*='form-']").submit();
    });

    // Highlight any found errors
    highlightFoundErrors()

    initDatePicker()

    initQuantityFormatter();

    initChosenSelect();

    if (jQuery.fn.tooltip) {
        // tooltips on hover
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body', html: true});
        // Makes tooltips work on ajax generated content
        $(document).ajaxStop(function () {
            $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
        });
    }


    $.event.special.remove = {
        remove: function (o) {
            if (o.handler) {
                o.handler.apply(this, arguments);
            }
        }
    }

    // Set last page opened on the menu
    $('#menu a[href]').on('click', function () {
        sessionStorage.setItem('menu', $(this).attr('href'));
    });

    if (!sessionStorage.getItem('menu')) {
        $('#menu #dashboard').addClass('active');
    } else {
        // Sets active and open to selected page in the left column menu.
        $('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parent().addClass('active');
    }

    $('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li > a').removeClass('collapsed');

    $('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('ul').addClass('in');

    $('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').addClass('active');


    // Settings object that controls default parameters for library methods:
    accounting.settings = {
        currency: {
            symbol: "",   // default currency symbol is '$'
            format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
            decimal: ".",  // decimal point separator
            thousand: ",",  // thousands separator
            precision: 2   // decimal places
        },
        number: {
            precision: 0,  // default precision on numbers is 0
            thousand: ",",
            decimal: "."
        }
    };

    // Image Manager
    $(document).on('click', 'a[data-toggle=\'image\']', function (e) {
        var $element = $(this);
        var $popover = $element.data('bs.popover'); // element has bs popover?

        e.preventDefault();

        // destroy all image popovers
        $('a[data-toggle="image"]').popover('destroy');

        // remove flickering (do not re-add popover when clicking for removal)
        if ($popover) {
            return;
        }

        $element.popover({
            html: true,
            placement: 'right',
            trigger: 'manual',
            content: function () {
                return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
            }
        });

        $element.popover('show');

        $('#button-image').on('click', function () {
            var $button = $(this);
            var $icon = $button.find('> i');

            $('#modal-image').remove();

            $.ajax({
                url: 'index.php?route=common/filemanager&user_token=' + getURLVar('user_token') + '&target=' + $element.parent().find('input').attr('id') + '&thumb=' + $element.attr('id'),
                dataType: 'html',
                beforeSend: function () {
                    $button.prop('disabled', true);
                    if ($icon.length) {
                        $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                    }
                },
                complete: function () {
                    $button.prop('disabled', false);

                    if ($icon.length) {
                        $icon.attr('class', 'fa fa-pencil');
                    }
                },
                success: function (html) {
                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                    $('#modal-image').modal('show');
                }
            });

            $element.popover('destroy');
        });

        $('#button-clear').on('click', function () {
            $element.find('img').attr('src', $element.find('img').attr('data-placeholder'));

            $element.parent().find('input').val('');

            $element.popover('destroy');
        });
    });

    if (jQuery.fn.fancybox) {
        $(".fancybox-thumb").fancybox({
            prevEffect: 'none',
            nextEffect: 'none',
            helpers: {
                title: {
                    type: 'outside'
                },
                thumbs: {
                    width: 50,
                    height: 50
                }
            }
        });
    }

    const syncEmails = $("#sync_emails");
    syncEmails.on('click', function (e) {
        e.preventDefault();
        syncEmails.html("Processing ...");
        $.get('/sources/emails/fetcher/', function (response) {
            $.notify({title: response.text, message: response.text}, {type: (response.error ? 'error' : 'success')});
            syncEmails.html("Get Emails");
            setTimeout(function () {
                if (response.error === false) {
                    if ('redirect' in response) {
                        document.location.href = response['redirect']
                    } else {
                        document.location.href = '/documents/';
                    }
                }
            }, 100)
        })
    });

    $("#sidebarCollapse").on('click', function (e) {
        e.preventDefault();
        $("#content").toggleClass("active");
        $("#column-left").toggleClass("hide");
    });

    $(".profile-item").on('click', function (e) {
        e.preventDefault();
        $.get($(this).data('ajax-url'), function (response) {
            responseNotification(response)
        }, 'json')
    });

    $.fn.button = function (action) {
        handleLoadingButton(action, $(this))
    };

    $.fn.submitButton = function (action) {
        handleLoadingButton(action, $(this))
    };

    $(document).on("click", ".fixed-asset-action", function(e){
        e.preventDefault();
        $(".fixed-asset-action").removeClass('active-button');
        $(".action-options").hide();
        $("." + $(this).data('action')).show();
        $("#id_asset_option").val($(this).data('option-value'));
        $(".modal-footer").show();
        $("#id_asset_category").trigger("chosen:updated");
		$("#id_asset").trigger("chosen:updated");
        initChosenSelect();
        $(this).addClass('active-button')
    });

});
