$(function(){

    const reportContainer = $(".report-container");

    const autoGenerate = reportContainer.data('auto-generate')
    if (autoGenerate === 1) {
        Report.changeFormAction($(this)).then((res) => {
            Report.generateReport($(this));
        });
    }

    $(".generate").on('click', function(e){
        e.preventDefault();
        let action = $(this).data('action');
        if (action === 'print') {
            Report.prepareForPrint($(this)).then((res) => {
                $(".report-form").submit();
            });
        } else if (action === 'download') {
            Report.changeFormAction($(this)).then((res) => {
                $(".report-form").submit();
            });
        }  else {
            Report.changeFormAction($(this)).then((res) => {
                Report.generateReport($(this));
            });
        }
    });

    $("#search_again").on('click', function(e){
        e.preventDefault()
        $("#search_box").slideDown('normal');
    });

    $(".all-company-object").on("change", function(){
        Report.selectObjectItems($(this))
    });

    $("#print").on("click", function(e){
       e.preventDefault();
       const formId = $(this).data('form-name');
       const formEl = $("#"+ formId);
       const actionUrl = formEl.attr('action') + "?print=1";
       formEl.prop('action', actionUrl).submit();
    });

    $(".submit-action").on("click", function(e){
       e.preventDefault();
        let selectedLines = Ledger.getSelectedLines();
        if (selectedLines.length > 0) {
           const url = $(this).data('url');
           const formEl = $("#generate_report");
           formEl.prop('action', url).submit();
        } else {
            alert("Please select at least 1 customer")
        }
    });

    $(".ajax-action").on("click", function(e){
        let ajaxUrl = $(this).data('ajax-url');
        let selectedLines = Ledger.getSelectedLines();
        if (selectedLines.length > 0) {
            const params = {
                'customers': selectedLines,
                'from_period': $("#id_from_period").val(),
                'to_period': $("#id_to_period").val(),
                'movement_type': $("input[name='movement_type'] :checked").val()
            }
            console.log(params);
            ajaxSubmit($(this), ajaxUrl, params, '');
        } else {
            alert("Please select at least 1 customer")
        }
    })

});

var Report = (function(report, $){

	return {

	    getSelectedLines: function(){
	        let selectedLines = [];
            $(".ledger-line").each(function(){
                if ($(this).is(":checked")) {
                    selectedLines.push($(this).val());
                }
            });
            return selectedLines;
        },

	    changeFormAction: async function(el) {
	        if (el.data('ajax-url') !== '') {
                $(".report-form").prop('action', el.data('ajax-url'));
            }
	        return 'done';
        },

	    prepareForPrint: async function(el) {
	        let formEl = $(".ledger-form");
	        formEl.append("<input type='hidden' name='print' value='1' />");
	        return 'done';
        },

	    selectObjectItems: function(el)
        {
            const objectId = el.val();
            const objectItemSelect = $("#object_item_" + objectId);
            if (el.is(":checked")) {
                objectItemSelect.prop('disabled', true)
                $("#object_item_" + objectId + ":options").each(function(){
                });
            } else {
                objectItemSelect.prop('disabled', false)
                $("#object_item_" + objectId + ":options").each(function(){
                });
            }
        },

	    generateReport: function(el)
        {
            const searchBox = $("#search_box");
            const containerEl = $(".report-container");
            const reportForm = $(".report-form");
            reportForm.LoadingOverlay('show', {
                text: 'Processing . . . '
            });
            reportForm.ajaxSubmit({
                data: {'action': el.data('action')},
                beforeSubmit:  function(formData, jqForm, options) {
                    const queryString = $.param(formData);
                },  // pre-submit callback
                error: function (data) {
                    const error = {'error': true, 'text': data.statusText};
                    responseNotification(error);
                    el.prop('disabled', false).html(el.data('data-original-html'));
                    reportForm.LoadingOverlay("hide");
                },
                success: function(responseHtml, statusText, xhr, $form)
                {
                   containerEl.html(responseHtml);
                   reportForm.LoadingOverlay('hide');
                   searchBox.slideUp('slow');

                    $("#ledger_all").on("change", function(){
                       if ($(this).is(":checked")) {
                           $(".ledger-line").prop('checked', true);
                       } else {
                           $(".ledger-line").prop('checked', false);
                       }
                    });

                    $(".open-suffix").on("click", function(e){
                        e.preventDefault();
                        Report.handleSuffix($(this));
                    });

                },  // post-submit callback
                type: 'get'
            });
        },

        handleSuffix(el) {
           $(".suffix-lines").remove();
           const elId = el.data('id');
           $.get(el.data('ajax-url'), {
               row_id: elId
           }, function(html){
                $("#row_" + elId).after(html);

                $(".search-box-" + elId).each(function(index, el){
                    const searchBoxEl = $(el);
                    const rowId = searchBoxEl.data('row-id');
                    const searchUrl = searchBoxEl.data("ajax-url");
                    $("#suffix_" + rowId).autocomplete({
                        minLength: 1,
                        source: searchUrl,
                        focus: function(event, ui) {
                            return false;
                        },
                        select: function(event, ui) {
                            $( "#suffix_" + rowId ).val(ui.item.suffix);
                            return false;
                        }
                    })
                    .autocomplete( "instance" )._renderItem = function( ul, item ) {
                        return $( "<li>" )
                            .append( "<div>" + item.suffix + "</div>" )
                            .appendTo( ul );
                    };
                });

                $("#update_suffix_" + elId).on("click", function(e){
                   e.preventDefault();
                   const el = $(this);
                   ajaxSubmitForm(el, $("#suffix_lines_form_" + elId), {'row_id': elId}).then(function(){
                      Report.generateReport($(".generate"));
                   });
                });
           });
        }

	}

}(Report || {}, jQuery))