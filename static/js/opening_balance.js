$(document).ready(function() {
	//Form Submit for IE Browser
    $(".account-opening-balance").on('blur', function(e){
        let el = $(this);
        let amount = el.val();
        el.removeClass('text-danger')
        let amountStr = accounting.format(amount);
        el.val(amountStr)
        const accountId = el.attr('id').replace('opening_balance_', '')
        OpeningBalance.update(el, accountId, amount)
        //}
	});

    // var copyAccountBalances = $("#copy_opening_balances");
    // copyAccountBalances.on('click', function(e){
    //    var ajaxUrl = $(this).data('ajax-url');
    //    e.preventDefault();
    //    copyAccountBalances.html("Processing ...");
    //    $.getJSON(ajaxUrl, function(response){
    //        responseNotification(response)
    //        copyAccountBalances.html("<i class='fa fa-copy'></i> Copy opening balances");
    //    })
    // });

    let copyAccountBalances = $("#copy_opening_balances");
    let text = copyAccountBalances.text()
    copyAccountBalances.on('click', function(e){
       e.preventDefault();
       copyAccountBalances.html("Processing ...");
       $.getJSON($(this).data('ajax-url'), function(response){
           responseNotification(response)
           copyAccountBalances.html("<i class='fa fa-copy'></i> " + text);
       }, function(){
           console.log('failed')
       });
    });

});

var OpeningBalance = (function(openingBalance, $){

	return {

		update: function(el, accountId, amount)
		{
			$.post($(el).data('ajax-url'), {
			    amount: accounting.unformat(amount)
            }, function(responseJson, statusText, xhr, $form)  {
                responseNotification(responseJson);
                if (!responseJson.error) {
                    el.val(accounting.formatMoney(responseJson.amount));
                }
			}, 'json');
		}

	}
}(OpeningBalance || {}, jQuery))

