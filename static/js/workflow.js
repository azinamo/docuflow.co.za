$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			$("#add_flow_level_submit").on('click', function (event) {
                submitModalForm($("form[name='state_role_form']"));
                return false;
            });

			$("#add_flow_level_role_submit").on('click', function (event) {
                Workflow.createSubmitRoles($("form[name='add_flow_level_role_form']"));
                $(".modal-ajax").modal('hide').remove();
                return false;
            });

			$("#add_flow_level_submit").on('click', function (event) {
                Workflow.createSubmitLevel($("form[name='workflow_level_form']"));
                $(".modal-ajax").modal('hide').remove();
                return false;
            });


			$(".chosen-select").chosen();

        });
    });

	$(".delete_invoice_flow").on('click', function(e){
		if (confirm('Are you sure you want to delete this invoice flow')) {
			Workflow.deleteInvoiceFlow(this);
			e.preventDefault();
		}
	});

	$(".delete_invoice_flow_role").on('click', function(e){
		if (confirm('Are you sure you want to delete this invoice flow role')) {
			Workflow.deleteInvoiceFlowRole(this);
			e.preventDefault();
		}
	});
});


var Workflow = (function(workflow, $){

	return {

		deleteFlowLevel: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, {
			}, function(responseJson){
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			}, 'json');
		},

		deleteFlowLevelRole: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, {
			}, function(responseJson){
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			}, 'json');
		},
		createSubmitLevel: function(form)
		{
			// disable extra form submits
			form.addClass('submitted');
			form.find(':submit').each(function () {
				$("#add_flow_level_submit").css('width', $(this).outerWidth());
				$("#add_flow_level_submit").attr('data-original-html', $(this).html());
				$("#add_flow_level_submit").html('<i class="fa fa-spinner fa-spin"></i>');
			});
			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					var element;
					// perform redirect
					if (responseJson.hasOwnProperty('redirect')) {
						$(location).attr('href', responseJson.redirect);
					}
					if (responseJson.hasOwnProperty('reload') && responseJson['reload']) {
						document.location.reload()
					}

					// flash success message
					if (responseJson.hasOwnProperty('error')) {
						if (!responseJson.error) {
							Workflow.addLevelWithRoles(responseJson)
						}
					}

					if (responseJson.hasOwnProperty('errors')) {
						$.each(responseJson.errors, function (key, value) {
							var element = form.find("#id_" + key )
							if (element.length > 0) {
								element.addClass('is-invalid');
								element.after('<div class="is-invalid-message has-error">' + value[0] + '</div>');
								$(element.parent()).parent().addClass('text-danger');
							}
						});
					}
					// dismiss modal
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		},
		createSubmitRoles: function(form)
		{
			// disable extra form submits
			form.addClass('submitted');
			form.find(':submit').each(function () {
				$("#add_flow_level_role_submit").css('width', $(this).outerWidth());
				$("#add_flow_level_role_submit").attr('data-original-html', $(this).html());
				$("#add_flow_level_role_submit").html('<i class="fa fa-spinner fa-spin"></i>');
			});
			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					var element;
					// perform redirect
					if (responseJson.hasOwnProperty('redirect')) {
						$(location).attr('href', responseJson.redirect);
					}
					if (responseJson.hasOwnProperty('reload') && responseJson['reload']) {
						document.location.reload()
					}
					// flash success message
					if (responseJson.hasOwnProperty('error')) {
						if (!responseJson.error) {
							Workflow.updateLevelRoles(responseJson)
						}
					}

					if (responseJson.hasOwnProperty('errors')) {
						$.each(responseJson.errors, function (key, value) {
							var element = form.find("#id_" + key )
							if (element.length > 0) {
								element.addClass('is-invalid');
								element.after('<div class="is-invalid-message has-error">' + value[0] + '</div>');
								$(element.parent()).parent().addClass('text-danger');
							}
						});
					}
					// dismiss modal
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		},

		updateLevelRoles: function(jsonData)
		{
			$.get(jsonData['url'], function(html){
				$("#level_" + jsonData['level']).replaceWith(html);
			});
		},

		addLevelWithRoles: function(jsonData)
		{
			$.get(jsonData['url'], function(html){
				$("#level_" + jsonData['after']).after(html);


			});

		}
	}
}(Workflow || {}, jQuery))