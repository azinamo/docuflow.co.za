$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save_update").on('click', function (event) {
            	var formId = $("#create_payment_update");
            	formId.LoadingOverlay("show", {
					text        : "Processing ...",
					progress    : true
				});
                event.preventDefault();
                submitModalForm($("form[name='create_payment_update']"));
            });
			initDatePicker();
			initChosenSelect();

        });
    });

});


var PaymentUpdate = (function(paymentUpdate, $){

	return {


	}
}(PaymentUpdate || {}, jQuery))

