(function() {
   //"use strict";

	window.Docuflow = window.Docuflow || {};
	let distribution = window.Docuflow.Distribution = {};

	distribution.init = function()
	{
	};

    distribution.distributeAccounts = function(el, params = {}) {
       const accountIds = [];
       $(".distribution-account").each(function(){
            if ($(this).is(":checked")) {
                accountIds.push($(this).val());
            }
       });

       const startDate = $("#id_starting_date");
       const nbMonth = $("#id_nb_months");
       if (accountIds.length === 0) {
         alert("Please select the accounts")
       } else if(!startDate.val()) {
          alert("Please select start date")
       } else if(!nbMonth.val()) {
          alert("Please enter number of months")
       } else {
           const container = $("#account_distribution");
           container.html("Loading . . .");
           params = params || {}
           params['nb_months'] = nbMonth.val()
           params['starting_date'] = startDate.val()
           params['account_ids'] = accountIds
           $.get(el.data('ajax-url'), params, function(html){
               container.html(html);
           });
       }
    }

	distribution.init();

})();