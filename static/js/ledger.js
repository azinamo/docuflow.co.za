$(function(){

    var ledgerContainer = $("#general_ledger");

    $(".generate").on('click', function(e){
        e.preventDefault();
        let action = $(this).data('action');
        if (action === 'print') {
            Ledger.prepareForPrint($(this)).then((res) => {
                $(".ledger-form").submit();
            });
        } else if (action === 'download') {
            Ledger.changeFormAction($(this)).then((res) => {
                $(".ledger-form").submit();
            });
        }  else {
            Ledger.changeFormAction($(this)).then((res) => {
                Ledger.loadLedger($(this));
            });
        }
    });

    $("#search_again").on('click', function(e){
        e.preventDefault()
        $("#search_box").slideDown('normal');
    });

    $(".all-company-object").on("change", function(){
        Ledger.selectObjectItems($(this))
    });

    $("#print").on("click", function(e){
       e.preventDefault();
       var formId = $(this).data('form-name');
       var formEl = $("#"+ formId);
       var actionUrl = formEl.attr('action') + "?print=1";
       formEl.prop('action', actionUrl).submit();
    });

    $(".submit-action").on("click", function(e){
       e.preventDefault();
        let selectedLines = Ledger.getSelectedLines();
        if (selectedLines.length > 0) {
           const url = $(this).data('url');
           const formEl = $("#generate_report");
           formEl.prop('action', url).submit();
        } else {
            alert("Please select at least 1 customer")
        }

    });

    $(".ajax-action").on("click", function(e){
        let ajaxUrl = $(this).data('ajax-url');
        let selectedLines = Ledger.getSelectedLines();
        if (selectedLines.length > 0) {
            const params = {
                'customers': selectedLines,
                'from_period': $("#id_from_period").val(),
                'to_period': $("#id_to_period").val(),
                'movement_type': $("input[name='movement_type'] :checked").val()
            }
            console.log(params);
            ajaxSubmit($(this), ajaxUrl, params, '');
        } else {
            alert("Please select at least 1 customer")
        }
    })

});

var Ledger = (function(ledger, $){

	return {

	    getSelectedLines: function(){
	        let selectedLines = [];
            $(".ledger-line").each(function(){
                if ($(this).is(":checked")) {
                    selectedLines.push($(this).val());
                }
            });
            return selectedLines;
        },

	    changeFormAction: async function(el) {
	        $(".ledger-form").prop('action', el.data('ajax-url'));
	        return 'done';
        },

	    prepareForPrint: async function(el) {
	        let formEl = $(".ledger-form");
	        formEl.append("<input type='hidden' name='print' value='1' />");
	        return 'done';
        },

	    selectObjectItems: function(el)
        {
            console.log("selectObjectItems")
            var objectId = el.val();
            var objectItemSelect = $("#object_item_" + objectId);
            console.log("select")
            console.log(objectItemSelect)
            if (el.is(":checked")) {
                objectItemSelect.prop('disabled', true)
                $("#object_item_" + objectId + ":options").each(function(){
                     console.log("Add to list of selected items")
                     console.log($(this).val());
                });
            } else {
                objectItemSelect.prop('disabled', false)
                $("#object_item_" + objectId + ":options").each(function(){
                     console.log("Remove to list of selected items")
                     console.log($(this).val());
                });
            }

        },

	    loadLedger: function(el)
        {
            var searchBox = $("#search_box");
            var containerEl = $(".data-container");
            var ledgerForm = $(".ledger-form");
            action = el.data('action');
            ledgerForm.LoadingOverlay('show', {
                text: 'Processing . . . '
            });
            ledgerForm.ajaxSubmit({
                data: {'action': action},
                beforeSubmit:  function(formData, jqForm, options) {
                    var queryString = $.param(formData);
                },  // pre-submit callback
                error: function (data) {
                    var error = {'error': true, 'text': data.statusText};
                    responseNotification(error);
                    el.prop('disabled', false).html(el.data('data-original-html'));
                    ledgerForm.LoadingOverlay("hide");
                },
                success:       function(responseHtml, statusText, xhr, $form)
                {
                   containerEl.html(responseHtml);
                   ledgerForm.LoadingOverlay('hide');
                   searchBox.slideUp('slow');

                    $("#ledger_all").on("change", function(){
                       if ($(this).is(":checked")) {
                           $(".ledger-line").prop('checked', true);
                       } else {
                           $(".ledger-line").prop('checked', false);
                       }
                    });

                },  // post-submit callback
                type: 'get'
                // $.ajax options can be used here too, for example:
                //timeout:   3000
            });
        }

	}

}(Ledger || {}, jQuery))