$( document ).ready(function() {
    if ( $(window).width() < 899 ){
        $( ".cross" ).hide();
        $( "#main_nav" ).hide();
        $( ".hamburger" ).click(function() {
            $( "#main_nav" ).slideToggle( "2000", function() {
                $( ".hamburger" ).hide();
                $( ".cross" ).show();
            });
        });
    } else{
        $( ".cross" ).hide();
        $( ".hamburger" ).hide();
        $( "#main_nav" ).show();
    }

    $( ".cross" ).click(function() {
        $( "#main_nav" ).slideToggle( "2000", function() {
        $( ".cross" ).hide();
        $( ".hamburger" ).show();
        });
    });

    //add materialize carousel
     document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.carousel');
        var instances = M.Carousel.init(elems, options);

        $('.carousel').carousel('fullWidth', true);
        $('.carousel').carousel('indicators', true);
        $('.carousel').carousel('dist', -80);

      });

        jQuery(window).resize(function(){
            const hamburgerClass = $(".hamburger");
            const crossClass = $(".cross");
            const mainNav = $("#main_nav");
            if ( $(window).width() < 899 ){
                crossClass.hide();
                hamburgerClass.show();
                mainNav.hide();
                hamburgerClass.click(function() {
                    mainNav.css( "display", "block", function() {
                        hamburgerClass.hide();
                        crossClass.show();
                    });
                });
            } else {
                crossClass.hide();
                hamburgerClass.hide();
                mainNav.show();
            }
        });

   $('.carousel').carousel();

   $("#send_enquiry").on('click', function(e){
       e.preventDefault();
        ajaxSubmitForm($(this), $("#contact"));
   });

});
