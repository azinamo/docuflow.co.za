var sales = window.Docuflow.Sales || {};

$(document).ready(function() {

	// Form Submit for IE Browser
    $(".fixed-asset-opening-balance").on('blur', function(e){
        const el = $(this);
        const fixedAssetId = el.data('asset-id');

        const assetEl = $("#opening_balance_" + fixedAssetId);
        const depreciationEl = $("#opening_depreciation_value_" + fixedAssetId);

        const assetValue = assetEl.val();
        const depreciationValue = depreciationEl.val();

        FixedAsset.update(el, fixedAssetId, assetValue, depreciationValue )
	});

    const copyAccountBalances = $("#copy_previous_year_opening_balances");
    const text = copyAccountBalances.text()
    copyAccountBalances.on('click', function(e){
       e.preventDefault();
       copyAccountBalances.button("loading");
       $.getJSON($(this).data('ajax-url'), function(response){
           responseNotification(response)
           copyAccountBalances.button("reset");
       })
    });

    $("#confirm_asset_disposal").on('click', function(e){
        e.preventDefault();
        FixedAsset.createDisposalInvoice($(this));
    });

    $("#dispose_assets").on('click', function(e){
        e.preventDefault();
        FixedAsset.confirmDisposal($(this));
    });

    const assetCategory = $("#id_category");
    assetCategory.on('change', function(e){
        e.preventDefault();
        if (assetCategory.val()  !== ''){
            FixedAsset.loadCategoryAccounts(assetCategory, assetCategory.val());
        }
    });

    const disposalSellingPrice = $("#id_selling_price");
    disposalSellingPrice.on('blur', function(e){
        const totalNetBookValue = $("#total_net_book_value").val();
        const netProfitLoss = $("#id_net_profit_loss");
        if (disposalSellingPrice.val() !== '' ) {
            const netProfit = disposalSellingPrice.val() - totalNetBookValue;
            const netProfitStr = accounting.format(netProfit);
            netProfitLoss.val(netProfitStr);
            $("#net_profit_loss").val(netProfit.toFixed(2));
        }
    });

    $("#update_depreciation_adjustments").on('click', function(e){
        e.preventDefault()
        FixedAsset.saveAdjustedDepreciation($(this));
    });

    $(".fixed-asset-closing-depreciation").on('blur', function(e){
        const el = $(this);
        const fixedAssetId = el.data('asset-id');

        const openingEl = $("#opening_depreciation_" + fixedAssetId);
        const closingEl = $("#closing_depreciation_" + fixedAssetId);

        const openingValue = parseFloat(openingEl.val().replace(',', ''));
        const closingValue = parseFloat(closingEl.val().replace(',', ''));

        const adjustmentValue = closingValue - openingValue;

        $("#depreciation_adjustment_"  + fixedAssetId).val( adjustmentValue.toFixed(2) );
        $("#depreciation_adjustment_value_"  + fixedAssetId).val( adjustmentValue.toFixed(2) );
	});

    $(".main-category-header").on("click", function(e){
        e.preventDefault();
        const categoryId = $(this).data('category-id');
        const categoryTable = $(".category-" + categoryId + '-sub-categories');
        const hideCategoryIcon  = $("#hide_category_" + categoryId);
        const showCategoryIcon  = $("#show_category_" + categoryId);

        if (categoryTable.is(":hidden")) {
            categoryTable.addClass('opened').removeClass('closed').show();
            hideCategoryIcon.hide();
            showCategoryIcon.show();
        } else {
            categoryTable.addClass('closed').removeClass('opened').hide();
            hideCategoryIcon.show();
            showCategoryIcon.hide();
        }
    });

    $(".sub-category-header").on("click", function(e){
        e.preventDefault();
        const categoryId = $(this).data('category-id');
        const categoryTable = $(".category-" + categoryId + '-assets');
        const hideSubCategoryIcon  = $("#hide_sub_category_" + categoryId);
        const showSubCategoryIcon  = $("#show_sub_category_" + categoryId);

        if (categoryTable.is(":hidden")) {
            categoryTable.addClass('opened').removeClass('closed').show();
            hideSubCategoryIcon.hide();
            showSubCategoryIcon.show();
        } else {
            categoryTable.addClass('closed').removeClass('opened').hide();
            hideSubCategoryIcon.show();
            showSubCategoryIcon.hide();
        }
    });

    $(document).on("click", ".open-additions", function(e){
        e.preventDefault();
        $(".asset-" + $(this).parent().data('fixed-asset-id') + "-addition").toggle();
    });

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");


			$("#save_fixed_asset_addition").on("click", function(e){
				e.preventDefault();
				const fixedAssetAdditionFormEl = $("#fixed_asset_addition_form");
            	fixedAssetAdditionFormEl.LoadingOverlay("show", {
					text        : "Processing ...",
					progress    : true
				});
				ajaxSubmitForm($(this), fixedAssetAdditionFormEl, false);
			})
        });
    });

    const fixedAssetLinksContainer = $(".fixed-asset-links");
	if (fixedAssetLinksContainer.length) {
	    $(".fixed-asset-links").each(function(){
            FixedAsset.getAssetsLinks($(this));
        })
	}

    const addSplitEl = $(".add-split");
    if (addSplitEl.length > 0) {
        FixedAsset.loadFixedAssetSplitForm(addSplitEl, 0);
    }

    $(document).on("click", ".add-split", function(e){
        e.preventDefault();
        let rowId = 0;
        const lastRow = $(".fixed-asset-percentage-split-row").last();
        if (lastRow.length > 0) {
            rowId = parseInt(lastRow.data('row-id')) + 1;
        }
        FixedAsset.loadFixedAssetSplitForm($(this), rowId)
    });

    $(document).on('change', '.depreciation-object-item', function(){
        const selectedOption = $("#depreciation_split_object_item_" + $(this).data('row')).find(":selected").data('label');
        $("#depreciation_percentage_object_item_label_" + $(this).data('row')).html(selectedOption);
    });

});

var FixedAsset = (function(fixedAsset, $){

	return {

	    loadFixedAssetSplitForm(el, rowId) {
            $.get(el.data('ajax-url'), {
                row: rowId
            } , function (html){
                $("#add_row").before(html);
            });
        },

	    getAssetsLinks(el) {
			el.html("loading ...");
			$.get(el.data('ajax-url'), function(responseHtml){
				el.html(responseHtml);
			});
        },

	    saveAdjustedDepreciation(el)
        {
            el.prop('disabled', true);
            let validReasons = true;
            $(".fixed-asset-depreciation-adjustment").each(function(){
                const assetId = $(this).data('asset-id');
                if ($(this).val() !== '') {
                    if ($("#reason_" + assetId).val() == '') {
                        validReasons = false;
                    }
                }
            });

            if (validReasons) {
               el.prop('disabled', true);
               ajaxSubmitForm(el, $("#depreciation_adjustments_form"))
            } else {
                el.prop('disabled', false);
                alert("Please enter valid reasons for adjustments made")
            }
        },

		update: function(el, fixedAssetId, assetValue, depreciationValue)
		{
			$.post(el.data('ajax-url'), {
			    asset_value: accounting.unformat(assetValue),
                depreciation_value: accounting.unformat(depreciationValue)
            }, function(responseJson, statusText, xhr, $form)  {
                responseNotification(responseJson);
			}, 'json');
		},

        saveAssetDisposal: function(el)
        {
            var el = $(el);
            var confirmDisposal = $("#confirm_disposal_form");

            confirmDisposal.ajaxSubmit({
                data: {'complete': false},
                error: function (data) {
                    // handle errors
                    var error = {'error': true, 'text': data.statusText};
                    //responseNotification(error);
                    el.prop('disabled', false).html(el.data('data-original-html'));
                    confirmDisposal.LoadingOverlay("hide");
                },
                success: function(response){
                    if (response.error) {
                        responseNotification(response);
                    } else {
                       FixedAsset.createDisposalInvoice(el, confirmDisposal)
                    }
                }

            });
            //submitModalForm($("#confirm_disposal_form"));
        },

        createDisposalInvoice: function(el, disposalForm)
        {
            $.get(el.data('modal-url'), function (data) {
                $(data).modal('show');

                initDatePicker();
                initChosenSelect();

                const disposedAssetsContainer = $("#disposed_assets");
                disposedAssetsContainer.html("Loading assets ...")
                FixedAsset.loadAssetsToDispose(disposedAssetsContainer);

                const idCustomer = $("#id_customer");
                idCustomer.on("change", function (e) {
                    e.preventDefault();
                    $("#customer_details").html("Loading customer details ...")
                    sales.loadCustomerDetails(idCustomer).always(function(customer){
                        FixedAsset.calculateDueDate()
                        // const saveCashBtn = $("#submit_invoice_and_continue");
                        // salesInvoice.handleCashButton();
                        // salesInvoice.updateFormAction(saveCashBtn);
                        //
                        // let invoiceDate = new Date();
                        // const dueDate = moment(invoiceDate);
                        // $("#id_due_date").val(dueDate.format('YYYY-MM-DD'));
                        //
                        // $("#customer_full_name").html("");
                        if (sales.isCashCustomer(customer)) {
                            const container = $("#cash_customer_payment_details");
                            console.log("load customer form")
                            FixedAsset.loadCashCustomerPaymentForm(container);
                        } else {
                            // salesInvoice.handleCreditButton()
                        }
                        //
                        // const submitToDfEl = $("#submit_invoice_and_send_to_docuflow");
                        // if (submitToDfEl.length > 0) {
                        //     submitToDfEl.hide();
                        //     if (salesInvoice.hasDfNumber(customer)) {
                        //         submitToDfEl.show();
                        //     }
                        // }

                        $('.invoice-col').matchHeight();
                    });
                });

                $("#save_disposal_invoice").on("click", function(e){
                    e.preventDefault();
                    const invoiceFormEl = $("#create_disposal_invoice");
                    invoiceFormEl.LoadingOverlay('show', {
                       'text': 'Processing ...'
                    });
                   invoiceFormEl.ajaxSubmit({
                        error: function (data) {
                            // handle errors
                            const error = {'error': true, 'text': data.statusText};
                            responseNotification(error);
                            el.prop('disabled', false).html(el.data('data-original-html'));
                            invoiceFormEl.LoadingOverlay("hide");
                        },
                        success: function(response){
                            responseNotification(response);
                            invoiceFormEl.LoadingOverlay("hide");
                        }
                    });
                });

            });
        },

        loadAssetsToDispose: function(el)
        {
            console.log("--Load assets to dispose now --", el.data('ajax-url'));
            $.get(el.data('ajax-url'), function(html){
                el.html(html);
                const vatCodeClass = $(".vat-code");
                vatCodeClass.each(function(){
                    console.log('Calculating based on vat change');
                    const rowId = $(this).data('item-id');
                    const vatSelected = $("#vat_code_"+ rowId  + " option:selected");
                    const vatPercentage = vatSelected.data('percentage');
                    $("#vat_percentage_" + rowId ).val( parseFloat(vatPercentage) );
                });

				vatCodeClass.on('change', function(){
					console.log('Calculating based on vat change');
                    const rowId = $(this).data('item-id');
					const netTotal = $("#total_price_exl_" + rowId ).text();
					const vatSelected = $("#vat_code_"+ rowId  + " option:selected");
					const percentage = vatSelected.data('percentage');
					console.log( "Net total ->", netTotal )
					console.log( "Vat selected -> ", vatSelected )
					console.log( "Percentage -> ", percentage )
					if (netTotal !== '' && percentage !== '') {
						const vatAmount = (parseFloat(percentage)/100) * parseFloat(netTotal);
						const vatAmountStr = accounting.formatMoney(vatAmount);
						$("#vat_" + rowId ).html( vatAmountStr );
						$("#vat_amount_" + rowId ).val( vatAmount );
						$("#vat_percentage_" + rowId ).val( parseFloat(percentage) );
					}
					Inventory.calculateInventoryItemPrices(rowId);

                    const totalAmount = $("#id_total_amount").val();
                    console.log("Invoice assets Total amount -->", totalAmount)
                    $("#total_payment").html( accounting.formatMoney(totalAmount));
					$("#total_invoice_amount").val( accounting.toFixed(totalAmount, 2))
                    console.log( "Before trigger", $(".invoice-payment-amount"), $(".invoice-payment-amount")[0] )
                    FixedAsset.calculatePaymentBalance();
                    FixedAsset.calculateDisposalNetProfitLoss();
                    FixedAsset.calculateLineProfitLoss(rowId);
				});

				$(".price").on('blur', function(){
				    var rowId = $(this).data('item-id');
					Inventory.calculateInventoryItemPrices(rowId);
					var totalAmount = $("#id_total_amount").val();
					console.log("Invoice assets Total amount -->", totalAmount)
                    $("#total_payment").html( accounting.formatMoney(totalAmount));
					$("#total_invoice_amount").val( accounting.toFixed(totalAmount, 2))
                    console.log( "Before trigger", $(".invoice-payment-amount"), $(".invoice-payment-amount")[0] )
                    FixedAsset.calculatePaymentBalance();
                    FixedAsset.calculateDisposalNetProfitLoss();
                    FixedAsset.calculateLineProfitLoss(rowId);
				});
            });
        },

        calculateLineProfitLoss: function(rowId)
        {
            var netBookValue = $("#net_book_value_" + rowId).val();
            var sellingPrice = $("#price_" + rowId).val();
            var quantity = $("#quantity_" + rowId).val();
            if (netBookValue !== '' && sellingPrice !== '' && quantity !== '') {
                var profitLoss = (parseFloat(sellingPrice) * parseFloat(quantity)) - parseFloat(netBookValue);
                if (profitLoss > 0) {
                    var profitAccountEl = $("#profit_account_id_" + rowId );
                    $("#profit_loss_account_" + rowId).html( profitAccountEl.data('account-code') ).attr('title', profitAccountEl.data('account-name')).data('original-title', profitAccountEl.data('account-name'));
                    $("#account_id_" + rowId).val( profitAccountEl.val() );
                    $("#total_price_incl_" + rowId).removeClass('ui-state-error-text').addClass('ui-state-ok-text');
                } else {
                    var lossAccountEl = $("#loss_account_id_" + rowId );
                    $("#profit_loss_account_" + rowId).html( lossAccountEl.data('account-code') ).attr('title', lossAccountEl.data('account-name')).data('original-title', lossAccountEl.data('account-name'));
                    $("#account_id_" + rowId).val( lossAccountEl.val() );
                    $("#total_price_incl_" + rowId).removeClass('ui-state-ok-text').addClass('ui-state-error-text');
                }
            }

        },

        loadCustomer: function(el) {
            console.log(' ------- Load customer details ------------');
            var ajaxUrl = $("#id_customer option:selected").data('detail-url');
            var paymentContinueBtn = $("#save_disposal_invoice");
            $.getJSON(ajaxUrl, {
                module: 'sales'
            }, function (response) {

                console.log(response)
                console.log(response.is_default)
                var container = $("#cash_customer_payment_details");
                if (response.is_default) {
                    console.log("load customer form")
                    FixedAsset.loadCashCustomerForm(response)
                    FixedAsset.loadCashCustomerPaymentForm(container);
                } else {
                    container.html("");
                    console.log("load customer details")
                    FixedAsset.displayCustomerDetails(response)
                    paymentContinueBtn.prop('disabled', false);
                }
                initChosenSelect();
                $("#id_invoice_date").trigger('blur');

            });
        },

        displayCustomerDetails: function(response)
        {
            $("#payments_form").hide()
            var postalAddressHtml = [];
            var deliveryAddressHtml = [];
            if (response.hasOwnProperty("postal")) {
                var postalAddress;
                console.log(typeof response.postal);
                if (typeof response.postal === "string") {
                    postalAddress = JSON.parse(response.postal);
                } else {
                    postalAddress = response.postal;
                }
                postalAddressHtml.push("<p class='mb-1'>"+postalAddress.address+"</p>");
                postalAddressHtml.push("<p class='mb-1'>"+postalAddress.city+"</p>");
                postalAddressHtml.push("<p class='mb-1'>"+postalAddress.province+"</p>");
                postalAddressHtml.push("<p class='mb-1'>"+postalAddress.country+"</p>");
            }
            if (response.hasOwnProperty("delivery")) {
                var deliveryAddress;
                console.log(typeof response.delivery)
                if (typeof response.delivery === "string") {
                    deliveryAddress = JSON.parse(response.delivery);
                } else {
                    deliveryAddress = response.delivery;
                }
                deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.address+"</p>");
                deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.city+"</p>");
                deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.province+"</p>");
                deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.country+"</p>");
            }
            if (response.hasOwnProperty("discount_terms")) {
                var discountTerms = response.discount_terms;
                if (typeof response.discount_terms === "string") {
                    discountTerms = JSON.parse(response.discount_terms);
                } else {
                    discountTerms = response.discount_terms;
                }
                if (discountTerms.hasOwnProperty("line")) {
                    if (discountTerms.line.discount && discountTerms.line.is_discounted)
                    $("#line_discount").val(discountTerms.line.discount);
                    $(".discount").each(function(){
                        if ($(this).val() === "") {
                            $(this).val(discountTerms.line.discount)
                        }
                    });
                }
                if (discountTerms.hasOwnProperty("invoice")) {
                    if (discountTerms.invoice.discount && discountTerms.invoice.is_discounted) {
                        $("#invoice_discount").val(discountTerms.invoice.discount);
                    }
                }
            }
            postalAddressHtml.push("<div class='limits row'>");
            if (response.credit_limit) {
                postalAddressHtml.push("<div class='col-lg-12'><strong>Credit Limit :</strong>" +accounting.unformat(response.credit_limit)+"</div>");
            } else {
                console.log('Something wrong with limit')
            }
            if (response.available_credit || response.credit_limit) {
                postalAddressHtml.push("<div class='col-lg-12'><strong>Available Credit:</strong>" +accounting.unformat(response.available_credit)+"</div>");
                $("#available_credit").val(accounting.unformat(response.available_credit));
            } else {
                console.log('Something wrong with limit')
            }
            postalAddressHtml.push("</div>")

            $("#customer_details").html("<div class='row'><div class='col-lg-6 col-sm-12' id='postal'>"+ postalAddressHtml.join(" ") +"</div><div class='col-lg-6 col-sm-12' id='delivery'>"+ deliveryAddressHtml.join(" ") +"</div></div>")

            if (response.hasOwnProperty("vat_number")) {
                $("#id_vat_number").val(response.vat_number)
            }

            if (response.hasOwnProperty("pricings")) {
                var html = [];
                $.each(response.pricings, function(index, pricing){
                    console.log( pricing )
                    html.push("<option value='" + pricing.id + "'>"+ pricing.name+"</option>")
                })
                $("#id_pricing").html(html.join(' '));
            }

            FixedAsset.calculateDueDate()
        },

        calculateDueDate: function()
        {
            const invoiceDate = new Date($("#id_invoice_date").val());
            const selectedCustomer = $("#id_customer option:selected")
            const paymentTerm = selectedCustomer.data('payment-term');
            const paymentTermDays = selectedCustomer.data('payment-days');
            const idDueDate = $("#id_due_date");
            if (paymentTerm === 1) {
                const dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
                idDueDate.val(dueDate.format('YYYY-MM-DD'));
            } else if (paymentTerm === 2) {
                const dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
                const end = dueDate.format('YYYY-MM-') + dueDate.daysInMonth();
                idDueDate.val(end);
            }
        },

        loadCashCustomerForm: function(customerData)
        {
            $("#id_vat_number").val("")
            $("#id_due_date").val("")
            $("#payments_form").show()
            $("#customer_details").html("<div class='row'><div class='col-lg-6 col-sm-12' id='postal'>"+ customerData['customer_form'] +"</div><div class='col-lg-6 col-sm-12' id='delivery'>"+ customerData['delivery_form'] +"</div></div>")
        },

        loadCashCustomerPaymentForm: function(el)
        {
            el.html('...');
            const totalAmountEl = $("#id_total_amount");
            console.log('Total amount')
            let totalAmount = 0;
            if (totalAmountEl.length > 0) {
              totalAmount = totalAmountEl.val();
              if (totalAmount === '') {
                  totalAmount = 0;
              }
            }
            $.get(el.data('ajax-url'), {
                amount: totalAmount
            }, function(responseHtml){
                el.html(responseHtml);

				const invoicePaymentMethodsAmounts = $(".invoice-payment-amount");
                const paymentContinueBtn = $("#save_disposal_invoice");
				paymentContinueBtn.prop("disabled", true);
				// var _invoiceAmount = parseFloat(accounting.toFixed(invoiceAmount, 2));
				invoicePaymentMethodsAmounts.on("keyup", function() {
                    FixedAsset.calculatePaymentBalance()
				});

            });
        },

        calculatePaymentBalance: function()
        {
            var invoicePaymentMethodsAmounts = $(".invoice-payment-amount");
            var paymentContinueBtn = $("#save_disposal_invoice");
            paymentContinueBtn.prop("disabled", true);
            var balanceEl = $("#balance");
            var idBalanceEl = $("#id_balance");
            var invoiceAmount = parseFloat(accounting.toFixed($("#total_invoice_amount").val(), 2));
            var allocatedTotal = 0;
            console.log('Invoice amount ', invoiceAmount);

            allocatedTotal = FixedAsset.calculateAmountAllocated(invoicePaymentMethodsAmounts);

            var changeAmount = 0;
            console.log('-------------------');
            console.log("Allocated total --> ", allocatedTotal, typeof allocatedTotal);
            console.log("Invoice total--> ", invoiceAmount, typeof invoiceAmount);
            console.log('\n\n');
            FixedAsset.handlePaymentMethodWithChange($(this), invoiceAmount, allocatedTotal);

            var balance = invoiceAmount - allocatedTotal;
            console.log("Balance--> ", balance, typeof accounting.formatMoney(balance), typeof balance);
            if (balance === 0) {
                paymentContinueBtn.prop("disabled", false);
            } else {
                paymentContinueBtn.prop("disabled", true);
            }
            balanceEl.html(accounting.formatMoney(balance));
            idBalanceEl.val(accounting.toFixed(balance, 2));
        },

        calculateAmountAllocated: function(elClass)
        {
            var allocatedTotal = 0;
            elClass.each(function(){
                var methodAmount = $(this).val();
                if (methodAmount !== undefined && methodAmount !== "") {
                    var amount = parseFloat(accounting.toFixed(methodAmount, 2));
                    console.log("amount --> ", accounting.toFixed(methodAmount, 2), amount);
                    allocatedTotal += amount;
                }
            });
            return parseFloat(accounting.toFixed(allocatedTotal, 2));
        },

        handlePaymentMethodWithChange: function(el, invoiceAmount, allocatedAmount)
        {
            var hasChange = el.data('has-change');
            console.log("Has change--> ", hasChange, typeof hasChange, (hasChange === 1));
            if (hasChange === 1) {
                console.log("Handle has change ----> ", (allocatedAmount > invoiceAmount));
                if (allocatedAmount > invoiceAmount) {
                     var changeAmount = allocatedAmount - invoiceAmount;
                     console.log('Change amount -->', changeAmount, typeof changeAmount);
                     $("#change_row").show();
                     $("#change").val(accounting.formatMoney(changeAmount));
                     $("#change_val").html(accounting.formatMoney(changeAmount));
                } else {
                     $("#change_row").hide();
                     $("#change").val(0);
                     $("#change_val").html('');
                }
            }
        },

        confirmDisposal: function(el)
        {
            const assets = [];
            const _disposalCheckClass = "disposal-check";
            $("." + _disposalCheckClass).each(function(){
                if ($(this).is(":checked")) {
                    assets.push($(this).val());
                }
            });
            if (assets.length > 0) {
                const ajaxUrl = $(el).data('ajax-url');
                $.get(ajaxUrl, {
                    fixed_assets: assets
                }, function(responseJson){
                    responseNotification(responseJson);
                }, 'json');
            }
        },

        loadCategoryAccounts: function (el, categoryId) {
		    const ajaxUrl = el.data('ajax-url');
            $.get(ajaxUrl, {
                category_id: categoryId
            }, function(responseHtml){
                $("#asset_accounts").html(responseHtml);
                $(".chosen-select").chosen();
            }, 'html');
        },

        calculateDisposalNetProfitLoss: function()
        {
            var totalNetBookValue = 0;
            var disposalSellingPrice = 0;
            $(".net-book-value").each(function(){
                var nbvAmount = $(this).val();
                if (nbvAmount !== "") {
                    totalNetBookValue += parseFloat(nbvAmount);
                }
            });
            $(".total-price-excl").each(function(){
                var amount = $(this).val();
                if (amount !== "") {
                     disposalSellingPrice += parseFloat(amount);
                }
            });

            var netProfitLoss = $("#id_net_profit_loss");
            var netProfitLossEl = $("#net_profit_loss");
            if (disposalSellingPrice !== '' ) {
                console.log("Selling")
                var netProfit = disposalSellingPrice - totalNetBookValue;
                console.log("Net profit ", netProfit);
                var profitLoss = accounting.toFixed(netProfit, 2);
                netProfitLoss.val(profitLoss);
                netProfitLossEl.html(accounting.formatMoney(netProfit));
                if (profitLoss > 0) {
                    console.log("Net Profit");
                    netProfitLossEl.removeClass('ui-state-error-text').addClass('ui-state-ok-text');
                } else {
                    console.log("Loss");
                    netProfitLossEl.removeClass('ui-state-ok-text').addClass('ui-state-error-text');
                }
            }
        }
	}
}(FixedAsset || {}, jQuery))

