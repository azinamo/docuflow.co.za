$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");

            initDatePicker();
			initChosenSelect();

			$("#continue_generate_cashup").on("click", function(e){
				ajaxSubmitForm($(this), $("#generate_cashup_form"));
				return false;
			});

        });
    });

	$("#cashups>tbody>tr>td:not(.actions)").on("click", function(){
		var detailUrl = $(this).parent("tr").data("detail-url");
		document.location.href = detailUrl;
		return false;
	});

	$("#generate_cashup").on("click", function(e){
		ajaxSubmitForm($(this), $("#cashup_form"));
		return false;
	});

	var criditCardSlipBody = $("#credit_card_slips_body");
	if (criditCardSlipBody.length > 0) {
		var ajaxUrl = criditCardSlipBody.data("ajax-url");
		CashUp.loadCashUpItems(criditCardSlipBody, ajaxUrl);
	}

	var cashSlipBody = $("#cash_slip_body");
	if (cashSlipBody.length > 0) {
		var ajaxUrl = cashSlipBody.data("ajax-url");
		CashUp.loadCashUpItems(cashSlipBody, ajaxUrl);
	}

	var addInvoiceItemLine = $("#add_invoice_item_line");
	addInvoiceItemLine.on('click', function(){
		$(this).prop('disabled', true).data('html', addInvoiceItemLine.text()).text('loading ...');
		var ajaxUrl = addInvoiceItemLine.data('ajax-url');
		CashUp.loadInvoiceItems(addInvoiceItemLine, ajaxUrl);
	});

	$(".bulk-action").on('click', function(e){
		e.preventDefault();
		CashUp.bulkInvoicesAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		CashUp.singleInvoiceAction($(this));
	});



	$("#petty_cash").on("keyup", function(){
		$(this).removeClass('error');
		CashUp.calculateCashBalance($(this));
	});

});

var CashUp = (function(cashUp, $){

	return {

		calculateCashBalance: function(el)
		{
			var pettyCashVal = el.val();
			if (pettyCashVal !== undefined && pettyCashVal !== "") {
				var bankedCashEl = $("#id_bank_cash");
				var totalCashEl = $("#total_cash");
				var cashOutEl = $("#cash_out");
				var totalCash = parseFloat(accounting.unformat(totalCashEl.val()));
			//	var bankedCash = parseFloat(accounting.unformat(bankedCashEl.val()));
				var pettyCash = parseFloat(pettyCashVal);
				console.log("Total cash --> ", totalCash);
				console.log("Petty cash --> ", pettyCash);
				var balance = totalCash;
				if (pettyCash > totalCash) {
					console.log("Petty cash --> ", pettyCash, 'is now more than total cash ->', totalCash);
					el.addClass('error').val(0);
					balance = totalCash;
				} else {
					balance = totalCash - pettyCash;
					console.log("Calculated balance --> ", balance);
				}
				cashOutEl.val(accounting.formatMoney(balance));
				bankedCashEl.val(balance);
			}
		},

		getSelectedInvoices: function()
		{
			var invoices = []
			$(".invoice-action").each(function(){
				if ($(this).is(":checked")) {
                    invoices.push($(this).data('invoice-id'));
                }
			});
			return invoices;
		},

		bulkInvoicesAction: function(el)
		{
			var invoices = Sales.getSelectedInvoices();
			var totalInvoices = invoices.length;
			if (totalInvoices === 0) {
				alert('Please select at lease one invoice');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( invoices )
				$.post(ajaxUrl, {
					invoices: invoices
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singleInvoiceAction: function(el)
		{
			var invoices = Sales.getSelectedInvoices();
			var totalInvoices = invoices.length;
			if (totalInvoices === 0) {
				alert('Please select at lease one invoice');
			} else if (totalInvoices >1) {
				alert('Please select one invoice to action');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( invoices )
				$.post(ajaxUrl, {
					invoices: invoices
				}, function(response){
					responseNotification(response);
				});
			}
		},

		reloadInvoiceItemRow: function(el)
		{
			var rowId = el.data('row-id');
			var ajaxUrl = el.data('ajax-url');
			var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
			$.get(ajaxUrl, {
				row_id:rowId,
				line_type: $("#type_"+ rowId +" option:selected").val()
			}, function(html) {
				$("#invoice_item_row_" + rowId).replaceWith(html);

				Sales.handleRow(rowId)

				if (chosenWidth > 0) {
					if ($(".chosen-select").hasClass('line-type"')) {
						$(".chosen-select").chosen({'width': '14px'});
					} else {
						$(".chosen-select").chosen({'width': chosenWidth + "px"});
					}
				} else {
					if ($(".chosen-select").hasClass('line-type"')) {
						$(".chosen-select").chosen({'width': '14px'});
					} else {
						$(".chosen-select").chosen({'width': '100px'});
					}
				}
			});

		},

		loadCashUpItems: function(el, ajaxUrl)
		{
			var rowId = 0;
			var lastRow = $(".invoice_item_row").last();
			if (lastRow.length > 0) {
				console.log("Row was");
				console.log( lastRow.data('row-id') );
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			CashUp.loadCashUpItemRow(el, ajaxUrl, rowId, lastRow)
		},

		loadCashUpItemRow: function(el, ajaxUrl, rowId, lastRow)
		{
			var key = el.data('key');
			var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
			var container = $("#" + key +"_placeholder");
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow !== undefined && lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				var _lastRow = $(".invoice_item_row").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}
				console.log( "Row --> ", rowId);

				Sales.handleRow(rowId);

				if (chosenWidth > 0) {
					$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
					$(".line-type").chosen({'width': '40px'});
				} else {
					$(".chosen-select").not('.line-type').chosen({'width': '100px'});
					$(".line-type").chosen({'width': '40px'});
				}
				el.prop('disabled', false).text(el.data('html'));
			});
		},

		handleRow: function(rowId)
		{
			$("#inventory_item_" + rowId).on('change', function(){
				console.log( "Row --> ", rowId, " and load fields")
				var selectedOption = $("#inventory_item_"+ rowId +" option:selected");
				var price = selectedOption.data('price');
				var unit = selectedOption.data('unit');
				var binLocation = selectedOption.data('bin-location');
				var description = selectedOption.data('description');
				var vatId = selectedOption.data('vat-id');
				var unitId = selectedOption.data('unit-id');
				console.log( "price --> ", price, ' add to ', $("#price_" + rowId ));
				console.log( "unit --> ", unit, ' add to ', $("#unit_" + rowId ));
				$("#price_" + rowId ).val( price );
				if (unit !== undefined && unit !== '') {
					$("#unit_" + rowId).val(unit)
				}
				if (binLocation !== undefined && binLocation !== '') {
					$("#bin_location_" + rowId).val(binLocation)
				}
				if (description !== undefined && description !== '') {
					$("#description_" + rowId).val(description)
				}
				if (vatId !== undefined && vatId !== '') {
					$("#vat_code_" + rowId).val(vatId).trigger("chosen:updated").trigger('change');
				} else {
					$("#vat_code_" + rowId).val('').trigger("chosen:updated").trigger('change');
				}
			});

			$(".line-type").on('change', function(e){
				Sales.reloadInvoiceItemRow($(this));
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				Sales.calculateInventoryItemPrices(rowId);
			});

			$("#price_"+ rowId +"").on('blur', function(){
				Sales.calculateInventoryItemPrices(rowId);
			});

			$("#vat_code_"+ rowId).on('change', function(){
				var selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
				var vatPercentage = selectedVatOption.data('percentage');
				console.log('Vat code changes --> ', vatPercentage);
				if (vatPercentage !== undefined && vatPercentage !== '') {
					$("#vat_percentage_"+ rowId).val(vatPercentage);
				} else {
					$("#vat_percentage_"+ rowId).val(0);
				}
				Sales.calculateInventoryItemPrices(rowId);
			});

			$("#discount_percentage_"+ rowId +"").on('blur', function(){
				Sales.calculateInventoryItemPrices(rowId);
			});

		},

		calculateInventoryItemPrices: function(rowId)
		{
			console.log("CALCULATING INVENTORY ROW ", rowId);
			var price = $("#price_" + rowId);
			var ordered = $("#quantity_ordered_" + rowId);
			var received = $("#quantity_" + rowId);
			var short = $("#quantity_short_" + rowId);
			var vat = $("#vat_amount_" + rowId);
			var vatPercentage = $("#vat_percentage_" + rowId);
			var discountPercentage = $("#discount_percentage_" + rowId);
			var discount = $("#discount_" + rowId);

			var quantityOrdered = 0;
			var shortQuantity = 0;
			var quantityReceived = 0;
			var vatAmount = 0;
			var priceExcl = 0;
			var discountAmount = 0;
			var totalVat = 0;
            var totalDiscount = 0;

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}
			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}

			if (quantityOrdered > 0) {
				if (quantityReceived <= quantityOrdered) {
					shortQuantity =  quantityOrdered - quantityReceived;
				} else {
					received.val(quantityOrdered);
					//backOrder.prop('disabled', true);
					alert('Please enter valid received quantity');
					return false;
				}
			}
			if (short !== undefined  && short.length > 0) {
                short.val( shortQuantity );
            }
			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}
			console.log( vatPercentage );
			console.log( vatPercentage.length );
			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentage/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                    vat.val(vatAmount);
                }
            }
			if (discountPercentage.length > 0) {
                if (discountPercentage.val() !== '' && priceExcl !== '') {
                    var discPercentage = parseFloat(discountPercentage.val());
                    console.log('Discount percentage --> ', discPercentage);
                    discountAmount = (discPercentage/100) * priceExcl;
                    console.log('Discount amount --> ', discountAmount);
                    discount.val(discountAmount);
                }
            }
			// if (vat !== undefined && vat.length > 0) {
			// 	if (vat.val() !== '') {
			//
			// 		vatAmount += parseFloat(accounting.unformat(vat.val()));
			// 	}
			// }
            if (discountAmount > 0) {
			    priceExcl = priceExcl - discountAmount;
            }
			var totalExcl = priceExcl * quantityReceived;
			var priceIncl =  priceExcl + vatAmount;
			var totalInc = priceIncl * quantityReceived;
			if (vatAmount !== '') {
			    totalVat = vatAmount * quantityReceived;
            }
			if (discountAmount !== '') {
			    totalDiscount = discountAmount * quantityReceived;
            }

			console.log( " quantityReceived ", quantityReceived );
			console.log( " Price EXCL", priceExcl );
			console.log( " totalExcl ", totalExcl );
			console.log( " vatAmount ", vatAmount );
			console.log( " totalVat", totalVat );
			console.log( " discountAmount ", discountAmount );
			console.log( " priceIncl ", priceIncl );
			console.log( " totalInc", totalInc );

			// if (backOrder !== undefined) {
			// 	if (shortQuantity === 0) {
			// 		backOrder.prop('disabled', true);
			// 	} else {
			// 		backOrder.prop('disabled', false);
			// 	}
			// }

			$("#price_incl_" + rowId).html( accounting.formatMoney(priceIncl) );
			$("#price_including_" + rowId).val( priceIncl );

			$("#vat_total_" + rowId).html( accounting.formatMoney(totalVat) );
			$("#total_vat_" + rowId).val( totalVat );

			$("#discount_amount_" + rowId).html( accounting.formatMoney(totalDiscount) );
			$("#total_discount_" + rowId).val(totalDiscount);

			$("#total_price_exl_" + rowId).html( accounting.formatMoney(totalExcl) );
			$("#total_price_excluding_" + rowId).val( totalExcl );

			$("#total_price_incl_" + rowId).html( accounting.formatMoney(totalInc) );
			$("#total_price_including_" + rowId).val( totalInc );

			Sales.calculateTotals();
		},

		calculateTotals: function()
		{
			var totalOrdered = 0;
			var totalReceived = 0;
			var totalShort = 0;
			var priceExc = 0;
			var priceInc = 0;
			var totalPrice = 0;
			var totalTotalExc = 0;
			var totalTotalInc = 0;
			var totalVatAmount = 0;
			var totalDiscountAmount = 0;

			$(".ordered").each(function(){
				totalOrdered += parseInt($(this).val());
			});

			$(".received").each(function(){
				totalReceived += parseInt($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseInt($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-vat-amount").each(function(){
				totalVatAmount += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-discount").each(function(){
				totalDiscountAmount += parseFloat(accounting.unformat($(this).val()));
			});

			$("#total_ordered").html( totalOrdered );
			$("#total_received").html( totalReceived );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.formatMoney(totalPrice) );
			$("#price_excl").html( accounting.formatMoney(priceExc) );
			$("#price_incl").html( accounting.formatMoney(priceInc) );
			$("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
			$("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );
			$("#total_vat_amount").html( accounting.formatMoney(totalVatAmount) );
			$("#total_discount").html( accounting.formatMoney(totalDiscountAmount) );

		}



	}
}(CashUp || {}, jQuery))

