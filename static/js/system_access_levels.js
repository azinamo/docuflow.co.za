$(function(){

   const groupSelector = $("#group");
   groupSelector.on("change", function(e){
       const groupPermissionsContainer = $("#group_permission_settings");
       groupPermissionsContainer.html("Loading ...")
       e.preventDefault();
       $.get(groupSelector.data("ajax-url"), {
           group_id: groupSelector.val()
       }, function(html){
           groupPermissionsContainer.html(html)
       });
   });


   $("#save_permission_group_permissions").on('click', function(e){
        e.preventDefault();
        $("#permission_group_permissions").ajaxSubmit({
            url: $(this).data('ajax-url'),
            data: {'group_id': groupSelector.val()},
            dataType: 'json',
            success: function(response, statusText, xhr, $form)  {
                responseNotification(response);
                // $.notify({ title: '', message: response.text},{ type: (response.error ? 'danger' : 'success') });
                // for normal html responses, the first argument to the success callback
                // is the XMLHttpRequest object's responseText property

                // if the ajaxSubmit method was passed an Options Object with the dataType
                // property set to 'xml' then the first argument to the success callback
                // is the XMLHttpRequest object's responseXML property

                // if the ajaxSubmit method was passed an Options Object with the dataType
                // property set to 'json' then the first argument to the success callback
                // is the json data object returned by the server
                //jQuery("#total").html( response.total );
                //jQuery("#total_value").val( response.total );
            }
        });
    });
});