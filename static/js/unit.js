$(document).ready(function() {
    //Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("#conversion_form"));
            });

        });
    });
})