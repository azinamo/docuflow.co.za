$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');
			const form = $(data).find("form");

			$("#id_new_quantity").on('blur', function(){
				InventoryAdjustment.calculateAdjustment($(this));
			});

            $("#submit_adjustment").on('click', function (event) {
                ajaxSubmitForm($(this), $("#stock_adjustment"));
                return false
            });

            $("input[name='price_type']").on('change', function(){
            	InventoryAdjustment.adjustPrices($(this));
			});

            $("#own_price").on('blur', function(){
                InventoryAdjustment.adjustPrices($("#price_type_own_price"));
            });

            $("#submit_branch_movement").on('click', function(e){
            	e.preventDefault();
                ajaxSubmitForm($(this), $("#move_branch_stock"));
                return false
            });

			initDatePicker();
            initQuantityFormatter();
			initChosenSelect();

        });
    });

	const inventoryItemsBody = $("#inventory_items_body");
	if (inventoryItemsBody.length > 0) {
		InventoryAdjustment.loadInventoryItems(inventoryItemsBody);
	}

	$(document).on("blur", "#code", function(e){
		InventoryAdjustment.loadInventoryItems(inventoryItemsBody);
	});

	$(document).on("blur", "#description", function(e){
		InventoryAdjustment.loadInventoryItems(inventoryItemsBody);
	});

});


var InventoryAdjustment = (function(adjustment, $){

    return {
        loadInventoryItems: function(inventoryItemsBody)
		{
			$.get(inventoryItemsBody.data("ajax-url"), {
				code: $("#code").val(),
				description: $("#description").val()
			}, function(html){
				inventoryItemsBody.html(html);
			})
		},

		adjustPrices: function(el)
		{
			let totalCost = 0;
			if (el.is(":checked")) {
			    const adjustment = parseFloat($("#id_adjustment").val());
			    const key = el.val();
			    const selectedPrice =  $("#" + key);
                if (selectedPrice.length > 0 && selectedPrice.val() !== "") {
                    totalCost = parseFloat(selectedPrice.val()) * adjustment;
			        $("#id_price").val(selectedPrice.val());
                }
			}
			$("#total_price").html(accounting.formatMoney(totalCost))
		},

		calculateAdjustment: function(el)
		{
			const onHand = $("#id_on_hand").val();
			const newQuantity = el.val();
			if (onHand !== "" && newQuantity !== "") {
				const adjustment = accounting.toFixed(parseFloat(newQuantity) - parseFloat(onHand), 4);
				let priceChoice = $(".price-choice");
				let priceType = $("#price_type_own_price");
				if (adjustment > 0) {
				    priceType.prop('checked', false);
				    priceChoice.show();
                } else {
				    priceChoice.hide();
				    $(".price").hide();
				    priceType.prop('checked', true);
                }
				$("#id_adjustment").val( adjustment );
            }
		},

	}
}(InventoryAdjustment || {}, jQuery))

