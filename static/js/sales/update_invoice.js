$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save_update").on('click', function (event) {
                event.preventDefault();
            	var formId = $("#create_sales_invoice_update");
            	formId.LoadingOverlay("show", {
					text        : "Processing ...",
					progress    : true
				});
                submitModalForm($("form[name='create_sales_invoice_update']"));
            });

            var date = new Date();
			// datepicker on all date input with class datepicker
			$(".datepicker").datepicker({
				dateFormat: 'yy-mm-dd'
			});

			$(".history_datepicker").datepicker({
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "yy-mm-dd",
				maxDate         : -1,
				yearRange       : "1900:"+(date.getFullYear())
			});

			var minDate = new Date()
			var dataMinDate = $("#id_start_date").data('min-date');
			if (dataMinDate) {
				date = new Date(dataMinDate);
				minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : minDate,
					yearRange       : "-30:+100"
				});
			} else {
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : +1,
					yearRange       : "-30:+100"
				});
			}

			$(".chosen-select").chosen();

        });
    });

});


var UpdateInvoice = (function(updateInvoice, $){

	return {

		createJournal: function(el) {
		   var self = $("#" + el.attr('id'));
		   var text = el.text();
		   self.html("Processing ...");
		   var url = el.data('ajax-url');
		   $.get(url, {
			 period: $("#period").val()
		   }, function(response){
		   	   if (response.error) {
		   	   	  self.html(text);
			   }
			   responseNotification(response);
		   }, 'json');
		},


	}
}(UpdateInvoice || {}, jQuery))

