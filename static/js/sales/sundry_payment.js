$(document).ready(function() {
	//Form Submit for IE Browser

	var customerEl = $("#id_customer");
	customerEl.on('change', function(e){
		SundyPayment.loadCustomerInvoices(customerEl)
	});

	$(".math-operation").on('blur', function(e){
		SundyPayment.calculateAmount($(this));
	});

	var formartMoney = debounce(function(el){
		SundyPayment.makeMoney(el);
	}, 650);

	$("#pay_customer_invoices").on("click", function(e){
		e.preventDefault();
		SundyPayment.paySelected($(this));
	});

	$("#make_payment").on('click', function(e){
		e.preventDefault();
		SundyPayment.submitPayment($(this))
	});

	var amountEl = $("#id_amount");
	amountEl.on("blur", function(e){
		SundyPayment.addPaymentLine(amountEl)
	});

	$("#allocate_payment").on("click", function(e){
		e.preventDefault();
		var formEl = $("#allocate_sundry_payment");
		formEl.LoadingOverlay('show', {
			'text': 'Processing payment ....'
		});
		ajaxSubmitForm($(this), formEl);
	});

	$(".open-unpaid-supplier>td:not(.selecta)").on('click', function(e){
		SundyPayment.openCusomerInvoices($(this).parent());
		return false;
	});

	$(".open-unpaid>td:not(.selecta)").on('click', function(e){
		SundyPayment.openChildrenTable($(this).parent());
		return false;
	});

	$(".unpaid-check").on('change', function(e){
		e.preventDefault();
		SundyPayment.selectParentInvoices($(this));
		SundyPayment.allocatePayment($(this));
		//return false;
	});

	$("#pay_now").on("click", function(e){
		SundyPayment.paySelected($(this), supplier);
	});

	$(".supplier-invoice").on('change', function(e){
		SundyPayment.allocatePayment($(this));
	});
});


var SundyPayment = (function(sundyPayment, $){

	return {

		submitPayment: function(el)
		{
			var formEL = $("#sundry_payment");
			formEL.LoadingOverlay('show', {
				'text': 'Processing sundry payment ...'
			});
			formEL.addClass('submitted');
			el.css('width', el.outerWidth());
			el.attr('data-original-html', el.html());
			el.html('<i class="fa fa-spinner fa-spin"></i>');
			el.prop('disabled', true)

			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			formEL.ajaxSubmit({
				url: formEL.attr('action'),
				error: function (data) {
					// handle errors
					var error = {'error': true, 'text': data.statusText};
					responseNotification(error);
					el.prop('disabled', false).html(el.data('data-original-html'));
					formEL.LoadingOverlay("hide");
				},
				success: function(responseJson, statusText, xhr, $form) // post-submit callback
				{
					responseNotification(responseJson);
					console.log('-----------handleSundryPaymentResponse------------------');
					// if ('url' in responseJson) {
					// 	$.get(responseJson['url'], {}, function (data) {
					// 		$(data).modal('show');
					// 		$("#submit_with_unallocated_payment").on("click", function(e){
					// 			e.preventDefault();
					// 			ajaxSubmitForm($(this), formEL, {'comment': $("#payment_comment").val()})
					// 		});
					// 	});
					// } else {
					//
					// }

					el.css('width', el.outerWidth());
					el.html(el.data('original-html'));
					el.prop('disabled', false)
					formEL.LoadingOverlay("hide");
				},
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		allocatePayment: function(el)
		{
			var parentId = el.data('customer-id');
			var amountSelected = SundyPayment.totalInvoiceAmount(parentId);
			var availablePayments = SundyPayment.supplierAvailablePayments(parentId);
			if (amountSelected > availablePayments) {
				$(".pay-"+ parentId +"-payment-check").prop('disabled', false).prop('checked', true);
			} else {
				SundyPayment.disableAvailablePayments();
			}
		},

		totalInvoiceAmount: function(supplierId)
		{
			var total = 0;
			$(".pay-" + supplierId + "-invoice-check").each(function(){
				var _this = $(this);
				if (_this.is(":checked")) {
					var invoiceId  = _this.val();
					var amount = $("#parent_" + supplierId + "_open_amount_" + invoiceId).val();
					total += parseFloat(amount)
				}
			});
			return total;
		},

		disableAvailablePayments: function(supplierId)
		{
			$(".pay-"+ supplierId +"-payment-check").prop('disabled', true).prop('checked', false);
		},

		supplierAvailablePayments: function(supplierId)
		{
			var availablePayments = 0;
			$(".supplier-"+ supplierId +"-payment-available").each(function(){
				availablePayments += parseFloat($(this).val()) * - 1;
			});
			return availablePayments;
		},

		getSelectedParent: function()
		{
			var parentId = $(".selected-tr").attr('id');
			return parentId;
		},

		getInvoicesToPay: function(parentId)
		{
			var supplierInvoices = [];
			var _invoicesClass = "pay-"+ parentId +"-invoice-check";
			$("." + _invoicesClass).each(function(){
				if ($(this).is(":checked")) {
					supplierInvoices.push($(this).val());
				}
			});
			return supplierInvoices;
		},

		getSelectedPayments: function(parentId)
		{
			var payments = [];
			var _paymentClass = "pay-"+ parentId +"-payment-check";
			$("." + _paymentClass).each(function(){
				console.log( " amount checked", $(this).is(":checked"), $(this).prop("checked"), $(this).checked )
				if ($(this).is(":checked")) {
					console.log( " customer amount checked", $(this).is(":checked") )
					payments.push($(this).val());
				}
			});
			return payments;
		},

		paySelected: function(el)
		{
			var parentId = SundyPayment.getSelectedParent();
			if (parentId) {
				var parentInvoices = SundyPayment.getInvoicesToPay(parentId);
				var parentPayments = SundyPayment.getSelectedPayments(parentId);
				if (parentInvoices.length > 0) {
					var ajaxUrl = el.data('ajax-url');
					// ajaxNotificationDialog(ajaxUrl, {
					// 	'parent_id': parentId,
					// 	'invoices': parentInvoices.join(','),
					// 	'payments': parentPayments.join(',')
					// });
					$.get(ajaxUrl, {'parent_id': parentId,
						'invoices': parentInvoices.join(','),
						'payments': parentPayments.join(',')
					}, function(response){
						responseNotification(response)
					});
				} else {
					alert("Please select the invoices to pay");
				}
			} else {
				alert("Please select the supplier to pay");
			}
		},

		selectParentInvoices: function(el)
		{
            var parentId = el.val();
            console.log( parentId );
			console.log( 'has it been selected or opened ', $(".parent-" + parentId + "-tr").hasClass('selected-tr') );
            if ( $(".parent-" + parentId + "-tr").hasClass('selected-tr')) {
				if (el.is(":checked")) {
					$(".pay-" + parentId +"-invoice-check").prop('checked', true);
				} else {
					$(".pay-" + parentId +"-invoice-check").prop('checked', false);
				}
			}
		},

		openChildrenTable: function (el)
        {
            var parentId = el.attr('id');
            if ($(".parent-" + parentId + "-tr").hasClass('selected-tr')) {
                $(".parent-" + parentId + "-row").toggle();
            } else {
				$(".unpaid-check").each(function(){
                   if (parentId !== $(this).attr('id')) {
                       $("#unpaid_" + $(this).val()).removeAttr('checked');
                       $(".parent-" + $(this).val() + "-row").hide();
                   }
                });

            	$(".parent").removeClass('selected-tr');
            	$(".parent-" + parentId + "-tr").addClass('selected-tr');
                $(".parent-" + parentId+ "-row").toggle();
            }
        },

		loadCustomerInvoices: function(el)
		{
			var customerInvoiceUrl = el.data('customer-invoices-url');
			if (customerInvoiceUrl !== undefined) {
				$.get(customerInvoiceUrl, {
					customer_id: el.val()
				}, function(invoicesHtml){
					$("#customer_sundry_invoices_payment").replaceWith(invoicesHtml);

					$(".allocationAmount").on('blur', function(e){
						e.preventDefault();
						SundyPayment.calculateInvoicePayment($(this));
						SundyPayment.calculateAllocated();
					});

					$(".allocate-invoice-amount").on('blur', function(e){
						e.preventDefault();
						SundyPayment.calculateInvoicePayment($(this));
						SundyPayment.calculateAllocated();
					});


					$('.allocated').on('blur', function(e){
						e.preventDefault();
						var calculated = SundyPayment.calculateNewOpen($(this));
						console.log("calculated ---> ", calculated)
						if (calculated) {
                            SundyPayment.calculateAllocated();
                            SundyPayment.calculatePaymentBalanceTotal();
						}
					});

				});
			}
		},

		calculateAllocated: function()
		{
			console.log('---- calculateAllocated ----------')
			var updatePaymentBtn = $("#allocate_payment");
			var allocatedInvoiceAmount = SundyPayment.calculateTotalAllocatedAmount();
			var allocatedPayment = SundyPayment.calculatePaymentAllocatedAmount();
			var totalAllocated = allocatedPayment + allocatedInvoiceAmount;
			$("#allocated_amount").html(totalAllocated.toFixed(2));
			console.log('Total invoice allocated --> ', allocatedInvoiceAmount);
			console.log('Total payment allocated --> ', allocatedPayment);
			console.log('Total allocated --> ', totalAllocated, typeof totalAllocated);
			if (totalAllocated === 0) {
				updatePaymentBtn.prop('disabled', false);
			} else {
				updatePaymentBtn.prop('disabled', true);
			}
		},

		calculateNewOpen: function(el)
		{
			var paymentId = el.data('payment-id');
			var amountAllocated = parseFloat(el.val()) * -1;
			var originalAmount = parseFloat($("#payment_amount_" + paymentId).val()) * -1;

			console.log( "Payment id -->", paymentId, " --> amountAllocated --> ", amountAllocated, " -- original Amount -< ", originalAmount);
			console.log( " Is allocation less than original --",  (amountAllocated <= originalAmount) );
			if (amountAllocated <= originalAmount) {
				var newOpen = originalAmount - amountAllocated;
				console.log("New open --> ", newOpen)
				if (newOpen > 0) {
					$("#balance_" + paymentId).val( newOpen.toFixed(2));
				} else {
					$("#balance_" + paymentId).val('0.00');
				}
				return true;
			} else {
				$("#balance_" + paymentId).val('');
				el.val('0.00');
			}
			return false;
		},

		addPaymentLine: function(el)
		{
			$(".payment-line").remove();
			var amount = el.val();
			if (amount !== "") {
				var displayAmount = parseFloat(amount) * -1;
				var allocated = displayAmount;
				var html = [];
				html.push("<tr class='payment-line'>");
					html.push("<td></td>");
					html.push("<td></td>");
					html.push("<td></td>");
					html.push("<td></td>");
					html.push("<td></td>");
					html.push("<td class='text-right'>");
						html.push("<input class='payment-amount' type='hidden' value='"+ amount +"' id='payment_amount' name='payment_amount' />");
						html.push(accounting.formatMoney(displayAmount));
					html.push("</td>");
					html.push("<td class='text-right'>");
						html.push(accounting.formatMoney(displayAmount));
					html.push("</td>");
					html.push("<td class='text-right'>");
						html.push("<input class='payment-allocation allocated form-control text-right' type='text' readonly='readonly' value='"+ accounting.formatMoney(allocated) +"' id='allocated_amount' name='payment_allocated' />");
					html.push("</td>");
					html.push("<td class='text-right'>");
						html.push("<span id='balance_str'>"+ accounting.formatMoney(displayAmount) +"</span>");
					html.push("</td>");
				html.push("</tr>");
				html.push("<input class='balance form-control text-right' type='hidden' readonly='readonly' value='"+ accounting.formatMoney(displayAmount) +"' id='balance' name='balance' />");
				$("#customer_sundry_invoices_payment>tbody").append(html.join(' '));
				SundyPayment.calculatePaymentBalance()
			}
		},

		calculatePaymentAmount: function(el)
		{
			var paymentId = el.data("payment-id");
			var allocatedAmount = parseFloat(el.val());
			var balance = $("#balance_" + paymentId);
			var payment = $("#payment_amount_" + paymentId);
			var paymentAmount = parseFloat(payment.val())
			var balanceAmount = 0;
			var openAmount = 0;
			if (allocatedAmount > paymentAmount) {
				balanceAmount = 0;
				payment.val(0);
			} else {
				balanceAmount = paymentAmount - allocatedAmount;
			}
			// if (balanceAmount > 0) {
			// 	balance.val( balanceAmount.toFixed(2) * -1);
			// }
			SundyPayment.calculatePaymentBalanceTotal()
		},

		calculatePaymentBalanceTotal: function()
		{
			var totalOpen = 0;
			var openElements = $(".open-amount").filter(function(){
				return $(this).val() !== '';
			});
			openElements.map(function(){
				totalOpen += parseFloat($(this).val());
			});
			$("#total_new_open").html( totalOpen.toFixed(2) );
		},

		calculateInvoicePayment: function(el)
		{
			var invoiceId = el.data("invoice-id");
			var allocatedAmount = accounting.unformat(el.val());
			if (allocatedAmount > 0) {
				var openAmount = 0;
				var invoiceAmount = parseFloat($("#open_amount_" + invoiceId).val());
				console.log('invoiceAmount', invoiceAmount);
				console.log('allocatedAmount', allocatedAmount);
				console.log('Difference', (allocatedAmount > invoiceAmount))
				if (allocatedAmount > invoiceAmount) {
					openAmount = 0;
					el.val(invoiceAmount);
				} else {
					openAmount = parseFloat(invoiceAmount) - parseFloat(allocatedAmount);
				}
				$("#open_" + invoiceId).val( openAmount.toFixed(2) );
				SundyPayment.calculatePaymentBalance()
				SundyPayment.calculatePaymentBalanceTotal()
			}
		},

		calculateTotalPaymentAmount: function()
		{
			var paymentAmount = 0
			$(".payment-amount").each(function(){
				paymentAmount += parseFloat($(this).val());
			});
			return paymentAmount;
		},

		calculateTotalAllocatedAmount: function()
		{
			var allocationAmount = 0
			$(".allocate-invoice-amount").each(function(){
				if ($(this).val() !== '') {
					var amount = accounting.unformat($(this).val());
					allocationAmount += parseFloat(amount)
				}
			});
			return allocationAmount;
		},

		calculatePaymentAllocatedAmount: function()
		{
			var allocationAmount = 0;
			$(".allocated").each(function(){
				console.log("Payment --> ", $(this).val(), ' allocated');
				if ($(this).val() !== '') {
					var amount = accounting.unformat($(this).val());
					console.log("Unformated payment --> ", amount, ' allocated');
					allocationAmount += parseFloat(amount)
				}
			});
			return allocationAmount;
		},

		calculatePaymentBalance()
		{
			var paymentBalance = $("#balance");
			var paymentAmount = SundyPayment.calculateTotalPaymentAmount();
			var balance = paymentBalance.val();
			var totalAllocation = SundyPayment.calculateTotalAllocatedAmount();

			balance = paymentAmount - totalAllocation;
			$("#total_allocated").val(accounting.toFixed(paymentAmount, 2));

			var balanceAmount = balance;
			paymentBalance.val( accounting.unformat(accounting.formatMoney(balanceAmount)) );
			balanceAmount  = balance * -1;
			$("#total_balance").html( accounting.formatMoney(balanceAmount) );
		},

		clearAllocations()
		{
			$(".allocate-payment").each(function(){
				var invoiceId =  $(this).data('invoice-id');
				$(this).val('');
				$("#open_" + invoiceId).val('');
			});
			SundyPayment.calculatePaymentBalance();
		},

		send: function(el, params)
		{
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
		},

		ajaxGet: function (el, params) {
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
        },

		addDiscount: function(el)
		{
			var invoiceId = el.val()
			var totalDiscountEl = $("#id_total_discount")
			var discountAmount = parseFloat($("#invoice_discount_" + invoiceId).val())
			var totalDiscount = 0
			if (totalDiscountEl.val() !== '') {
				totalDiscount = parseFloat(totalDiscountEl.val())
			}
			totalDiscount += discountAmount
			totalDiscountEl.val( totalDiscount.toFixed(2) )
			SundyPayment.calculateNetPayment()
		},

		removeDiscount: function(el)
		{
			var invoiceId = el.val();
			var totalDiscountEl = $("#id_total_discount")
			var amount = $("#invoice_discount_" + invoiceId).val();
			if (amount !== '') {
                var discountAmount = parseFloat(amount);
                console.log('Discount amount ---', discountAmount)
				var totalDiscount  = 0;
                if (totalDiscountEl.val() !== '') {
                    totalDiscount = parseFloat(totalDiscountEl.val())
                }
                console.log('Initially total discount is ---', totalDiscount);
                totalDiscount -= discountAmount;
                console.log('Now removed ', discountAmount ,' total discount is ---', totalDiscount);
                totalDiscountEl.val( totalDiscount.toFixed(2) );
                SundyPayment.calculateNetPayment()
            }
		},

		calculateNetPayment: function()
		{
			var selectedDiscount = $("#id_total_discount").val()
			var discountAllowed = $("#id_discount_disallowed").val()
			var totalDiscount = 0
			if (discountAllowed !== '') {
				totalDiscount += parseFloat(discountAllowed);
			}
			if (selectedDiscount !== '') {
				totalDiscount += parseFloat(selectedDiscount);
			}
			var total = $("#id_total_amount").val()
			var netAmount = parseFloat(total) - totalDiscount;
			$("#id_net_amount").val( netAmount.toFixed(2) )
		},

		allChecked: function(elClass, elId)
		{
			var countChecked = 0
			var countCheckboxes = 0
			elClass.each(function(){
				countCheckboxes++;
				if ($(this).is(":checked")) {
					countChecked++;
				}
			})
			//console.log(elClass, "chjecked --> ", countChecked, " count checkboxes --> ", countCheckboxes, elId)
			if (countCheckboxes > 0  && countChecked > 0) {
				if (countCheckboxes === countChecked) {
					//console.log("Make the element checked")
					elId.attr("checked", "checked")
                } else {
					elId.removeAttr("checked", "checked")
				}
			}
		}

	}
}(SundyPayment || {}, jQuery))

