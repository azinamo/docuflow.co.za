$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on("click", "[data-modal]", function (event) {
		event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			const form = $(data).find("form");
			initDatePicker();
			initChosenSelect()

			$("#submit_delete").on("click", function(e){
				e.preventDefault();
				ajaxSubmitForm($(this), $("#delete_receipt_form"))
				document.location.reload()
			});

        });
    });

	const customer = $("#id_customer");
	customer.on('change', function(e){
		Receipt.loadInvoices(customer)
	});

	const receiptDate = $("#id_date");
	receiptDate.on('blur', function(e){
		Receipt.loadInvoices(customer)
	});

	const receiptAmount = $("#id_amount");
	const amount = receiptAmount.val();
	receiptAmount.on('blur', function(e){
		console.log('Original amount ', amount, ' vs ', $(this).val())
		if (amount !== $(this).val()) {
			Receipt.calculateTotals();
			Receipt.calculateBalance();
		}
	});

	const formatMoney = debounce(function(el){
		Receipt.makeMoney(el);
	}, 650);

	$("#pay_customer_invoices").on("click", function(e){
		e.preventDefault();
		Receipt.paySelected($(this));
	});

	$("#save").on('click', function(e){
		e.preventDefault();
		ajaxSubmitForm($(this), $("#create_receipt"));
	});

	$("#id_payment_method").on('change', function(e){
		const isCash = $("#id_payment_method option:selected").data('has-change');
		if (isCash === 1) {
			$("#change_row").show();
		} else {
			$("#change_row").hide();
		}
	});

	$(".customer-invoice").on('change', function(e){
		Receipt.allocatePayment($(this));
	});

	$(".allocate").on('change', function(){
		Receipt.calculateReceiptOpenAmount($(this));
	});

	$('.pay').on('change', function(e){
		e.preventDefault();
		Receipt.handleInvoicePay($(this));
	});

	$('.discount').on('change', function(e){
		e.preventDefault();
		Receipt.handleInvoiceDiscount($*(this));
	});

	$(".invoice-amount").on('blur', function(e){
		e.preventDefault();
		Receipt.calculateInvoiceOpenAmount($(this));
		Receipt.calculateAllocated();
		Receipt.calculateTotals();
		Receipt.invoicesTotals();
		Receipt.calculateBalance();
	});


});


var Receipt = (function(receipt, $){

	return {

		handleInvoicePay(el) {
			const invoiceId = Receipt.getRowId(el);
			Receipt.calculateInvoiceOpenAmount($("#amount_" + invoiceId));
			Receipt.calculateTotals();
			Receipt.invoicesTotals();
			Receipt.calculateBalance();
		},

		handleInvoiceDiscount(el) {
			const invoiceId = Receipt.getRowId(el);
			let invoiceAmount = 0;
			if (el.is(":checked")) {
				invoiceAmount = $("#discounted_invoice_amount_" + invoiceId).val();
			} else {
				invoiceAmount = $("#open_amount_" + invoiceId).val();
			}
			const allocateEl = $("#amount_" + invoiceId);
			allocateEl.val(accounting.formatMoney(invoiceAmount));
			$("#discounted_amount_" + invoiceId).val(invoiceAmount);
			$("#discounted_open_amount_" + invoiceId).html(accounting.formatMoney(invoiceAmount));
			Receipt.calculateInvoiceOpenAmount(allocateEl);
			Receipt.invoicesTotals();
			Receipt.calculateBalance();
		},

		handleAllocateReceiptAllocate: function(el)
		{
			Receipt.calculateReceiptOpenAmount(el);
			Receipt.calculateTotals();
			Receipt.invoicesTotals();
			Receipt.calculateBalance();
		},

		totalAllocatedReceiptAmount: function()
		{
			let total = 0;
			$(".allocate-receipt").each(function() {
				const _this = $(this);
				if (_this.is(":checked")) {
					const receiptId  = _this.data('receipt-id');
					const amount = $("#receipt_open_balance_" + receiptId).val();
					total += parseFloat(accounting.unformat(amount));
				}
			});
			return total;
		},

		totalInvoiceAmount: function()
		{
			let total = 0;
			$(".pay").each(function() {
				const _this = $(this);
				if (_this.is(":checked")) {
					const invoiceId  = Receipt.getRowId(_this);
					const amount = $("#amount_" + invoiceId).val();
					total += parseFloat(accounting.unformat(amount));
				}
			});
			return total;
		},

		invoicesTotals: function()
		{
			let totalDiscounted = 0;
			let totalDiscount = 0;
			$(".pay").each(function(){
				const _this = $(this);
				const invoiceId  = Receipt.getRowId(_this);
				if (_this.is(":checked")) {
					const discountedAmount = $("#discounted_amount_" + invoiceId).val();
					totalDiscounted += parseFloat(accounting.unformat(discountedAmount));
				}
				if ($("#is_discounted_" + invoiceId).is(":checked")) {
					const discountAmount = $("#discount_amount_" + invoiceId).val();
					totalDiscount += parseFloat(accounting.unformat(discountAmount));
				}
			});
			$("#discounted_total").html(accounting.formatMoney(totalDiscounted));
			$("#discount_total").html(accounting.formatMoney(totalDiscount));
		},

		loadInvoices: function(el)
		{
			const customerInvoiceUrl = el.data('customer-invoices-url');
			if (customerInvoiceUrl && el.val() !== '') {
				const customerInvoicesContainer = $("#customer_invoices");
				customerInvoicesContainer.html('Loading ...');
				$.get(customerInvoiceUrl, {
					customer_id: el.val(),
					receipt_date: $("#id_date").val()
				}, function(invoicesHtml){
					customerInvoicesContainer.html(invoicesHtml);

					Receipt.calculateTotals();
					Receipt.invoicesTotals();
					const invoiceAmountClass = $(".invoice-amount");
					invoiceAmountClass.each(function(e){
						Receipt.calculateInvoiceOpenAmount($(this));
					});

					$("input[type='hidden']:not(input[name='csrfmiddlewaretoken'])").each(function(){
						const amountEl = $(this);
						if (amountEl.val() !== "") {
							amountEl.val(accounting.toFixed(amountEl.val(), 2));
						}
					});

					invoiceAmountClass.on('blur', function(e){
						e.preventDefault();
						Receipt.calculateInvoiceOpenAmount($(this));
						Receipt.calculateAllocated();
						Receipt.calculateTotals();
						Receipt.invoicesTotals();
						Receipt.calculateBalance();
					});

					$('.pay').on('change', function(e){
						e.preventDefault();
						Receipt.handleInvoicePay($(this));
					});

					$(".allocate-receipt").on('change', function(e){
						e.preventDefault();
						Receipt.handleAllocateReceiptAllocate($(this));
					});

					$('.discount').on('change', function(e){
						e.preventDefault();
						Receipt.handleInvoiceDiscount($(this));
					});

					const tenderedAmountEl = $("#id_amount");
					if (tenderedAmountEl.val() !== '') {
						$("#payment_amount").html(accounting.formatMoney(tenderedAmountEl.val()));
						$("#id_payment_amount").val(tenderedAmountEl.val());
						// Receipt.addPaymentLine(tenderedAmountEl);
					}

					$("#add_payment_line").on("click", function(e){
						e.preventDefault();
						Receipt.addPaymentLine($(this));
					});

					Receipt.calculateBalance();
				});
			}
		},

		hasChange() {
			 let methodHasChange = $("#id_payment_method option:selected").data('has-change');
			 return methodHasChange === 1;
		},

		calculateBalance() {
			const submitBtn = $("#save");
			let paymentsTotal = Receipt.calculateTotalPaymentAmount();
			paymentsTotal  += Receipt.totalAllocatedReceiptAmount();
			const invoicesTotalAllocated = Receipt.totalInvoiceAmount();

			const varianceAmount = invoicesTotalAllocated - parseFloat($("#id_amount").val());
			console.log(invoicesTotalAllocated)
			console.log("receipt amount -> ", parseFloat($("#id_amount").val()))
			console.log("paymentsTotal -> ", paymentsTotal)
			console.log("varianceAmount -> ", varianceAmount)
			let balance = accounting.toFixed(varianceAmount - paymentsTotal, 2);
			console.log("balance -> ", balance)
			let change = paymentsTotal - invoicesTotalAllocated;
			console.log("change -> ", change)
			const changeHtmlEL = $("#change_val");
			const changeEL = $("#id_change");
			if (change > 0) {
				changeHtmlEL.html(accounting.formatMoney(change));
				changeEL.val(change.toFixed(2));
			} else {
				change = 0;
				changeHtmlEL.html(accounting.formatMoney(change));
				changeEL.val(change.toFixed(2));
			}
			const balanceEL = $("#balance");
			const balanceIdEL = $("#id_balance");
			if (Receipt.hasChange()) {
				if (balance <= 0) {
					balance  = 0;
					balanceEL.html(accounting.formatMoney(balance));
					balanceIdEL.val(accounting.toFixed(balance, 2));
				} else {
					balanceEL.html(accounting.formatMoney(balance));
					balanceIdEL.val(accounting.toFixed(balance, 2));
				}
			} else {
				balanceEL.html(accounting.formatMoney(balance));
				balanceIdEL.val(accounting.toFixed(balance, 2));
			}
			console.log("balance -> ", balance)
			$("#total_allocated").val(accounting.toFixed(paymentsTotal, 2));
			// paymentBalance.val( balanceAmount );
			// balanceAmount  = balance * -1;
			if (Receipt.hasChange()) {
				if (balance < 0) {
					submitBtn.prop('disabled', true);
				} else {
					submitBtn.prop('disabled', false);
				}
			} else {
				if (balance <= 0) {
					submitBtn.prop('disabled', false);
				} else {
					submitBtn.prop('disabled', true);
				}
			}
		},

		calculateTotals() {
			let originalTotal = Receipt.totalByClass("original-amount");
			let openTotal = Receipt.totalByClass("open-amount");
			let discountedAmountTotal = Receipt.totalByClass("discounted-amount");
			let invoiceAmountTotal = Receipt.totalInvoiceAmount(); //Receipt.totalByClass("invoice-amount");
			$("#original_total").html(accounting.formatMoney(originalTotal));
			$("#open_total").html(accounting.formatMoney(openTotal));
			//$("#discounted_total").html(accounting.formatMoney(discountedAmountTotal));
			$("#invoices_total").val(discountedAmountTotal);
			$("#invoice_amount_total").html(accounting.formatMoney(invoiceAmountTotal));

			const varianceAmount = invoiceAmountTotal - parseFloat($("#id_amount").val());
			$("#variance_amount").html(accounting.formatMoney(varianceAmount));
			$("#id_variance").val(varianceAmount);
		},

		totalByClass(className) {
			let total = 0;
			$("." + className).each(function(){
				if ($(this).val() !== '') {
					const value = accounting.unformat($(this).val()).toString();
					total += parseFloat(value);
				}
			});
			return total;
		},

		calculateAllocated: function()
		{
			let updatePaymentBtn = $("#update_payment");
			const allocatedInvoiceAmount = Receipt.calculateTotalAllocatedAmount();
			const allocatedPayment = Receipt.calculatePaymentAllocatedAmount();
			const totalAllocated = allocatedPayment + allocatedInvoiceAmount;
			$("#allocated_amount").html(totalAllocated.toFixed(2));
			if (totalAllocated === 0) {
				updatePaymentBtn.prop('disabled', false);
			} else {
				updatePaymentBtn.prop('disabled', true);
			}
		},

		calculateReceiptOpenAmount: function(el)
		{
			const receiptId = el.data('receipt-id');
			let amountAllocated  = 0;
			const isAllocated = $("#receipt_allocate_" + receiptId).is(":checked");
			if (isAllocated) {
				amountAllocated = accounting.unformat($("#receipt_open_balance_"+ receiptId).val());
			}
			const originalAmount = parseFloat($("#receipt_original_balance_" + receiptId).val());
			let openBalance = originalAmount;
			if (originalAmount <= amountAllocated) {
				const newOpen = originalAmount - amountAllocated;
				if (newOpen <= 0) {
					openBalance = newOpen;
				} else {
					openBalance = 0;
					el.val('0.00');
				}
			} else {
				el.val(originalAmount);
			}
			$("#open_receipt_balance_" + receiptId).html(openBalance.toFixed(2));
			$("#open_balance_" + receiptId).val(openBalance.toFixed(2));
			Receipt.calculateBalance();
		},

		getVatCodePercentage(rowId) {
			return parseFloat($("#vat_percentage_" + rowId).val());
		},

		addPaymentLine: function(el)
		{
			$(".payment-line").remove();
			const paymentAmountUrl = el.data('payment-line-url');
			let rowId = $(".payments").length + 1 || 0;
			let params = {
				'customer_id': $("#id_customer").val(),
				'amount': el.val(),
				'row_id': rowId,
				'rows': 1
			}

			$.get(paymentAmountUrl, params, function(paymentAmountHtml) {
				$(".payment-invoices-break").before(paymentAmountHtml);

				const paymentRow = $(".payment-row-" + rowId);
				const _lastRow = $(".sundry-allocations").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}
				$(".payment-amount").on('blur', function(){
					const _rowId = $(this).data('row-id')
					Receipt.handlePaymentAmount(_rowId);
				});

				$(".search-box").each(function(index, el){
					const _rowId = $(this).data('row-id')
					const model = $(this).data('model');
					const modelSearchEl = $(".model-" + model + "-search-" + rowId);
					const searchUrl = $(this).data('ajax-url');
					$(this).autocomplete({
						minLength: 1,
						source: searchUrl,
						focus: function( event, ui ) {
							$( "#" + model + "_item_" + _rowId ).val(ui.item.code);
							$( "#"+ model + "_" + _rowId ).val(ui.item.id);
							return false;
						},
						select: function(event, ui ) {
							$( "#"+ model + "_" + _rowId ).val(ui.item.id);
							$( "#"+ model + "_item_" + _rowId ).val(ui.item.code);

							if (model === 'vat_code') {
								Receipt.handleVatCodeChange(_rowId, ui.item);
							} else if (model === 'account') {
								Receipt.handleAccountChange(_rowId, ui.item);
							}
							return false;
						}
					})
					.autocomplete( "instance" )._renderItem = function( ul, item ) {
						return $( "<li>" )
							.append( "<div>" + item.code + "<br>" + item.description + "</div>" )
							.appendTo( ul );
					};
				});

				Receipt.calculateBalance();
			});
		},

		handlePaymentAmount: function(rowId)
		{
			const amountPaid = parseFloat($("#id_payment_amount_" + rowId).val());
			const vatCodePercentage = Receipt.getVatCodePercentage(rowId);
			let vatAmount = 0;
			let amountExcludingVat = amountPaid;
			if (vatCodePercentage > 0) {
				vatAmount = vatCodePercentage / (vatCodePercentage + 100) * amountPaid;
				amountExcludingVat = amountPaid - vatAmount;
			}
			$("#vat_amount_" + rowId).val(accounting.toFixed(vatAmount, 2));
			$("#vat_total_" + rowId).html(accounting.formatMoney(vatAmount));
			$("#id_vat_amount_" + rowId).val(accounting.toFixed(vatAmount, 2));
			$("#amount_excluding_vat_" + rowId).html(accounting.formatMoney(amountExcludingVat));
			Receipt.invoicesTotals();
			Receipt.calculateBalance();
		},

		handleAccountChange: function(rowId, selectedAccount)
		{
			$("#account_description_" + rowId).html(selectedAccount.description);
		},

		handleVatCodeChange: function(rowId, selectedVatCode)
		{
			const amountPaid = parseFloat($("#id_payment_amount_" + rowId).val());
			let vatPercentage = 0
			if (selectedVatCode.percentage !== undefined && selectedVatCode.percentage !== "") {
				vatPercentage = selectedVatCode.percentage
			}
			let vatAmount = 0;
			let amountExcludingVat = amountPaid;
			if (vatPercentage > 0) {
				vatAmount = vatPercentage / (vatPercentage + 100) * amountPaid;
				// vatAmount = (vatCode/100) * amountPaid;
				amountExcludingVat = amountPaid - vatAmount;
			}
			$("#vat_percentage_" + rowId).val(vatPercentage);
			$("#vat_description_" + rowId).html(selectedVatCode.description);
			$("#vat_amount_" + rowId).val(accounting.toFixed(vatAmount, 2));
			$("#vat_total_" + rowId).html(accounting.formatMoney(vatAmount));
			$("#id_vat_amount_" + rowId).val(accounting.toFixed(vatAmount, 2));
			$("#amount_excluding_vat_" + rowId).html(accounting.formatMoney(amountExcludingVat));
			Receipt.calculateBalance();
		},

		calculatePaymentAmount: function(el)
		{
			var paymentId = el.data("payment-id");
			var allocatedAmount = parseFloat(el.val());
			var balance = $("#balance_" + paymentId);
			var payment = $("#payment_amount_" + paymentId);
			var paymentAmount = parseFloat(payment.val());
			var balanceAmount = 0;
			var openAmount = 0;
			if (allocatedAmount > paymentAmount) {
				balanceAmount = 0;
				payment.val(0);
			} else {
				balanceAmount = paymentAmount - allocatedAmount;
			}
			// if (balanceAmount > 0) {
			// 	balance.val( balanceAmount.toFixed(2) * -1);
			// }
			Payment.calculatePaymentBalanceTotal()
		},

		calculatePaymentBalanceTotal: function()
		{
			const totalOpen = Receipt.totalByClass('open-amount');
			$("#total_new_open").html( totalOpen.toFixed(2) );
		},

		getRowId(el) {
			return el.data("invoice-id") || el.data("ledger-id");
		},

		getIsLedgerLine(el) {
			return el.data("ledger-id") !== undefined;
		},

		calculateInvoiceOpenAmount: function(el)
		{
			console.log('calculateInvoiceOpenAmount')
			const invoiceId = Receipt.getRowId(el);
			const isLedgerLine = Receipt.getIsLedgerLine(el);
			const allocatedAmount = accounting.unformat(el.val());
			const discountedAmount = accounting.unformat($("#discounted_amount_" + invoiceId).val());
			console.log('discounted amount ', discountedAmount, typeof discountedAmount)
			let checkboxId = 'pay_' + invoiceId;
			if (isLedgerLine) {
				checkboxId = "pay_ledger_" + invoiceId;
			}
			const isPayable = $("#" + checkboxId).is(":checked");
			const invoiceAmount = parseFloat(discountedAmount);
			console.log('Invoice amount is ', invoiceAmount, ' from discounted mount ', discountedAmount)
			let openAmount = invoiceAmount;
			if (isPayable) {
				if (allocatedAmount > invoiceAmount) {
					openAmount = 0;
					el.val(accounting.formatMoney(invoiceAmount));
				} else {
					openAmount = invoiceAmount - allocatedAmount;
				}
			}
			console.log('Open invoice amount is ', invoiceAmount)
			let amount = accounting.unformat($("#amount_" + invoiceId).val());
			$("#open_" + invoiceId).html( accounting.formatMoney(openAmount) );
			$("#invoice_open_amount_" + invoiceId).val( openAmount.toFixed(2) );
			$("#id_amount_" + invoiceId).val(amount.toFixed(2));
		},

		calculateTotalPaymentAmount: function()
		{
			return Receipt.totalByClass("payment-amount");
		},

		calculateTotalAllocatedAmount: function()
		{
			return Receipt.totalByClass("allocate-payment")
		},

		calculatePaymentAllocatedAmount: function()
		{
			return Receipt.totalByClass('allocated')
		},

		clearAllocations()
		{
			$(".allocate-payment").each(function(){
				const invoiceId =  Receipt.getRowId($(this));
				$(this).val('');
				$("#open_" + invoiceId).val('');
			});
			Payment.calculateBalance();
		},

		send: function(el, params)
		{
			const ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
		},

		ajaxGet: function (el, params) {
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
        },

		addDiscount: function(el)
		{
			const invoiceId = el.val();
			const totalDiscountEl = $("#id_total_discount");
			const discountAmount = parseFloat($("#invoice_discount_" + invoiceId).val());
			let totalDiscount = 0;
			if (totalDiscountEl.val() !== '') {
				totalDiscount = parseFloat(totalDiscountEl.val())
			}
			totalDiscount += discountAmount;
			totalDiscountEl.val( totalDiscount.toFixed(2) );
			Payment.calculateNetPayment();
		},

		removeDiscount: function(el)
		{
			var invoiceId = el.val();
			var totalDiscountEl = $("#id_total_discount")
			var amount = $("#invoice_discount_" + invoiceId).val();
			if (amount !== '') {
                var discountAmount = parseFloat(amount);
				var totalDiscount  = 0;
                if (totalDiscountEl.val() !== '') {
                    totalDiscount = parseFloat(totalDiscountEl.val())
                }
                totalDiscount -= discountAmount;
                totalDiscountEl.val( totalDiscount.toFixed(2) );
                Payment.calculateNetPayment()
            }
		},

		calculateNetPayment: function()
		{
			var selectedDiscount = $("#id_total_discount").val()
			var discountAllowed = $("#id_discount_disallowed").val()
			var totalDiscount = 0
			if (discountAllowed !== '') {
				totalDiscount += parseFloat(discountAllowed);
			}
			if (selectedDiscount !== '') {
				totalDiscount += parseFloat(selectedDiscount);
			}
			var total = $("#id_total_amount").val();
			var netAmount = parseFloat(total) - totalDiscount;
			$("#id_net_amount").val( netAmount.toFixed(2) )
		},

		allChecked: function(elClass, elId)
		{
			let countChecked = 0;
			let countCheckboxes = 0;
			elClass.each(function(){
				countCheckboxes++;
				if ($(this).is(":checked")) {
					countChecked++;
				}
			})
			//console.log(elClass, "chjecked --> ", countChecked, " count checkboxes --> ", countCheckboxes, elId)
			if (countCheckboxes > 0  && countChecked > 0) {
				if (countCheckboxes === countChecked) {
					//console.log("Make the element checked")
					elId.attr("checked", "checked")
                } else {
					elId.removeAttr("checked", "checked")
				}
			}
		}

	}
}(Receipt || {}, jQuery));

