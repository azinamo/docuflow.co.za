
(function(){
    'use strict';

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common = {};


    common.init = function()
    {
        // tooltip remove
        $('[data-toggle=\'tooltip\']').on('remove', function() {
            $(this).tooltip('destroy');
        });

        // Tooltip remove fixed
        $(document).on('click', '[data-toggle=\'tooltip\']', function(e) {
            $('body > .tooltip').remove();
        });

        $('#button-menu').on('click', function(e) {
            e.preventDefault();

            $('#column-left').toggleClass('active');
        });

		if ( $(window).width() < 899 ){
			$( ".cross" ).hide();
			$( "#main_nav" ).hide();
			$( ".hamburger" ).click(function() {
				$( "#main_nav" ).slideToggle( "2000", function() {
					$( ".hamburger" ).hide();
					$( ".cross" ).show();
				});
			});
		} else {
			$( ".cross" ).hide();
			$( ".hamburger" ).hide();
			$( "#main_nav" ).show();
		}

		$( ".cross" ).click(function() {
			$( "#main_nav" ).slideToggle( "2000", function() {
			$( ".cross" ).hide();
			$( ".hamburger" ).show();
			});
		});

        var minDate = new Date()
        $(".future_datepicker").datepicker({
            changeMonth		: true,
            changeYear		: true,
            dateFormat 		: "yy-mm-dd",
            minDate         : minDate,
            yearRange       : "-30:+100"
        });
        $(".history_datepicker").datepicker({
            changeMonth		: true,
            changeYear		: true,
            dateFormat 		: "yy-mm-dd",
            maxDate         : -1,
            yearRange       : "1900:"+(minDate.getFullYear())
        });

        $(".datepicker").datepicker({
            dateFormat: 'yy-mm-dd'
        });
		//add materialize carousel
		//  document.addEventListener('DOMContentLoaded', function() {
		// 	var elems = document.querySelectorAll('.carousel');
		// 	var instances = M.Carousel.init(elems, options);
        //
		// 	$('.carousel').carousel('fullWidth', true);
		// 	$('.carousel').carousel('indicators', true);
		// 	$('.carousel').carousel('dist', -80);
		//   });

        // Settings object that controls default parameters for library methods:
        accounting.settings = {
            currency: {
                symbol : "",   // default currency symbol is '$'
                format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
                decimal : ".",  // decimal point separator
                thousand: ",",  // thousands separator
                precision : 2   // decimal places
            },
            number: {
                precision : 4,  // default precision on numbers is 0
                thousand: ",",
                decimal : "."
            }
        };

    };

    common.initDatePicker = function() {
        var minDate = new Date();

        var datePickerClass = $(".datepicker");
        if (datePickerClass.length > 0) {
            datePickerClass.datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
        var historyDatePickerClass = $(".history_datepicker");
        if (historyDatePickerClass.length > 0) {
            historyDatePickerClass.datepicker({
                changeMonth		: true,
                changeYear		: true,
                dateFormat 		: "yy-mm-dd",
                maxDate         : -1,
                yearRange       : "1900:"+(date.getFullYear())
            });
        }

        var inputMinDate = $("#id_start_date").data('min-date');
        var futureDatePickerClass = $(".future_datepicker");
        if (inputMinDate) {
            date = new Date(inputMinDate);
            minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
        } else {
            minDate = 1
        }
        if (futureDatePickerClass.length > 0) {
            futureDatePickerClass.datepicker({
                changeMonth		: true,
                changeYear		: true,
                dateFormat 		: "yy-mm-dd",
                minDate         : minDate,
                yearRange       : "-30:+100"
            });
        }

    };

    common.initChosenSelect = function()
    {
        if ($(".chosen-select").length > 0) {
          $(".chosen-select").chosen();
        }
    }
    /**
     * This function creates a new element in the document with a given class name.
     *
     * @param tag The tag name of the new element.
     * @param className The class name of the new element.
     */
    common.createElement = function (tag, className)
    {
        return $(document.createElement(tag)).addClass(className);
    };
        /**
     * Toogle the css class name of a HTML element
     *
     * @param element The HTML element
     * @param condition A boolean
     * @param className The class name, if this value is undefined 'hide' will be used
     */
    common.toggle = function (element, condition, className)
    {
        className = className !== undefined ? className : 'hide';

        if (condition) {
            element.removeClass(className);
        } else {
            element.addClass(className);
        }
    };

    common.setCookie = function setCookie(cookieName, cookieValue, expiryDays) {
        var d = new Date();
        d.setTime(d.getTime() + (expiryDays * 24 * 60 * 60 * 1000 ));
        var  expires = "expires=" + d.toUTCString();
        document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
    };

   common.getCookie = function setCookie(cookieName) {
      var name = cookieName + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' '){
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
      }
      return "";
   };

   common.launchLightbox = function launchLightbox(target, options) {
        //associate modal event on page
        options = {
          url: null,
          title:''
        };
        $('.btn-modal-action').on('show.bs.modal', function (e) {
          //pull data from the event instigator
          options.url = e.relatedTarget.getAttribute('data-url');
          options.title = e.relatedTarget.getAttribute('data-modal-title');
          options.javascript = e.relatedTarget.getAttribute('data-javascript');

          //inject title
          e.currentTarget.querySelector('.modal-title').textContent = options.title;

          //inject content

          $.get(options.url, function (res) {

            e.currentTarget.querySelector('.modal-body .content').innerHTML = res;
            e.currentTarget.querySelector('.modal-body').classList.remove('ajaxLoading');

            if (options.javascript) {
                $.getScript(options.javascript, function() {

                });
            }
          });
        });
   };

   common.ckeditor = function ckeditor(targetImage) {
    window.addEventListener('load', function () {
      CKEDITOR.replace(targetImage.parentNode.getElementsByClassName('ckeditor-container')[0], {
        customConfig:'/js/vendor/ckeditor/custom_config.js'
      });
    }, { passive: true });
   };

   common.printHtml = function(elId){
        var tableToPrint = document.getElementById(elId);
        var htmlToPrint = '';
        var newWin = window.open("");
        htmlToPrint += tableToPrint.outerHTML;
        newWin.document.write(htmlToPrint);
        newWin.print();
        newWin.close();
    };

    common.resize = function()
    {
        $(window).resize(function(){
            if ( $(window).width() < 899 ){
                $( ".cross" ).hide();
                $( ".hamburger" ).show();
                $( "#main_nav" ).hide();
                $( ".hamburger" ).click(function() {
                    $( "#main_nav" ).css( "display", "block", function() {
                        $( ".hamburger" ).hide();
                        $( ".cross" ).show();
                    });
                });
            } else {
                $( ".cross" ).hide();
                $( ".hamburger" ).hide();
                $( "#main_nav" ).show();
            }
        });
    };
   
    /**
     * Toogle the css class name of a HTML element
     *
     * @param element The HTML element
     * @param condition A boolean
     * @param className The class name, if this value is undefined 'hide' will be used
     */
    common.toggle = function (element, condition, className)
    {
        className = className !== undefined ? className : 'hide';

        if (condition) {
            element.removeClass(className);
        } else {
            element.addClass(className);
        }
    };

    common.calculateSelectWidth = function(el)
    {
        var chosenWidth = 300;
        if (el === undefined) {
            chosenWidth = 300;
        } else {
            chosenWidth = el.width();
        }
        if (chosenWidth === null || chosenWidth === 0) {
            chosenWidth = 300;
        }
        return chosenWidth
    };

    common.init();
    common.resize();

})();