(function () {

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var form = window.Docuflow.Form || {};
    var message = window.Docuflow.Message || {};
    var fixedAsset =  window.Docuflow.FixedAsset || {};

    console.table(window.Docuflow);

	//Form Submit for IE Browser
    fixedAsset.init = function()
    {
        var openingBalances = $(".fixed-asset-opening-balance");
        openingBalances.on('blur', function(e){
            var el = $(this);
            var fixedAssetId = el.data('asset-id');

            var assetEl = $("#opening_balance_" + fixedAssetId);
            var depreciationEl = $("#opening_depreciation_value_" + fixedAssetId);

            var assetValue = assetEl.val();
            var depreciationValue = depreciationEl.val();

            // var assetValueStr = accounting.format(assetValue)
            // assetEl.val(assetValueStr)
            //
            // var depreciationValueStr = accounting.format(depreciationValue)
            // depreciationEl.val(depreciationValueStr)

            fixedAsset.update(el, fixedAssetId, assetValue, depreciationValue )
        });

        var copyAccountBalances = $("#copy_opening_balances");
        var text = copyAccountBalances.text();

        copyAccountBalances.on('click', function(e){
           var ajaxUrl = $(this).data('ajax-url');
           e.preventDefault();
           copyAccountBalances.html("Processing ...");
           $.getJSON(ajaxUrl, function(response){
               message.responseNotification(response)
               copyAccountBalances.html("<i class='fa fa-copy'></i> " + text);
           });
        });

        $("#confirm_asset_disposal").on('click', function(e){
            e.preventDefault();
            fixedAsset.saveAssetDisposal($(this));
        });

        $("#dispose_assets").on('click', function(e){
            e.preventDefault();
            fixedAsset.confirmDisposal($(this));
        });

        var assetCategory = $("#id_category");
        assetCategory.on('change', function(e){
            e.preventDefault();
            if (assetCategory.val()  !== ''){
                fixedAsset.loadCategoryAccounts(assetCategory, assetCategory.val());
            }
        });


        var disposalSellingPrice = $("#id_selling_price");
        disposalSellingPrice.on('blur', function(e){
            var totalNetBookValue = $("#total_net_book_value").val();
            var netProfitLoss = $("#id_net_profit_loss");
            if (disposalSellingPrice.val() !== '' ) {
                var netProfit = disposalSellingPrice.val() - totalNetBookValue;
                var netProfitStr = accounting.format(netProfit);
                netProfitLoss.val(netProfitStr);
                $("#net_profit_loss").val(netProfit.toFixed(2));
            }
        });

        var adjustmentDepreciationBtn = $("#update_depreciation_adjustments");
        adjustmentDepreciationBtn.on('click', function(e){
            e.preventDefault()
            fixedAsset.saveAdjustedDepreciation()
        });

        $(".fixed-asset-closing-depreciation").on('blur', function(e){
            var el = $(this);
            var fixedAssetId = el.data('asset-id');

            var openingEl = $("#opening_depreciation_" + fixedAssetId);
            var closingEl = $("#closing_depreciation_" + fixedAssetId);

            var openingValue = parseFloat(openingEl.val().replace(',', ''));
            var closingValue = parseFloat(closingEl.val().replace(',', ''));

            var adjustmentValue = closingValue - openingValue;

            $("#depreciation_adjustment_"  + fixedAssetId).val( adjustmentValue.toFixed(2) );
            $("#depreciation_adjustment_value_"  + fixedAssetId).val( adjustmentValue.toFixed(2) );
        });
    };

    fixedAsset.hasValidReasons = function()
    {
        var validReasons = true;
        $(".fixed-asset-depreciation-adjustment").each(function(){
            var assetId = $(this).data('asset-id');
            if ($(this).val() !== '') {
                if ($("#reason_" + assetId).val() === '') {
                    validReasons = false;
                }
            }
        });
        return validReasons;
    };

    fixedAsset.saveAdjustedDepreciation = function(e)
    {
        if (fixedAsset.hasValidReasons()) {
           form.submit($("#depreciation_adjustments_form"))
        } else {
            alert("Please enter valid reasons for adjustments made")
        }
    };

    fixedAsset.update = function(el, fixedAssetId, assetValue, depreciationValue)
    {
        var ajaxUrl = $(el).data('ajax-url');
        $.post(ajaxUrl, {
            asset_value: assetValue,
            depreciation_value: depreciationValue
        }, function(responseJson, statusText, xhr, $form)  {
            responseNotification(responseJson);
            // if (!responseJson.error) {
            //     el.val(responseJson.amount);
            // }
        }, 'json');
    };

    fixedAsset.saveAssetDisposal = function(el)
    {
        ajaxSubmitForm(el, $("#confirm_disposal_form"));
    };

    fixedAsset.getSelectedFixedAssets = function(_class)
    {
        var assets = [];;
        $("." + _class).each(function(){
            if ($(this).is(":checked")) {
                assets.push($(this).val());
            }
        });
        return assets;
    };

    fixedAsset.confirmDisposal = function(el)
    {
        var assets = fixedAsset.getSelectedFixedAssets("disposal-check");
        if (assets.length > 0) {
            var ajaxUrl = $(el).data('ajax-url');
            $.get(ajaxUrl, {
                fixed_assets: assets
            }, function(responseJson){
                responseNotification(responseJson);
            }, 'json');
        }
    };

    fixedAsset.loadCategoryAccounts = function (el, categoryId) {
        var ajaxUrl = el.data('ajax-url');
        $.get(ajaxUrl, {
            category_id: categoryId
        }, function(responseHtml){
            $("#asset_accounts").html(responseHtml);
            $(".chosen-select").chosen();
        }, 'html');
    };

    fixedAsset.init();

}());


