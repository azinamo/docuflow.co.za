(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer || {};
    var salesItem = window.Docuflow.SalesItem || {};
    var sales = window.Docuflow.Sales || {};
    var delivery = window.Docuflow.Delivery = {};

    delivery.init = function()
	{
		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");

				common.initDatePicker();
				common.initChosenSelect();

			});
		});

        $(".save-delivery").on("click", function(e){
            e.preventDefault();
            form.submit($(this), $("#delivery_form"), {"next": $(this).data("action")});
        });

		$(".ajax-submit").on("click", function(e){
			e.preventDefault();
			form.submit($(this), $("#delivery_form"), {"next": $(this).data("action")});
		});

		const deliveryItemsBody = $("#delivery_items_body");
		if (deliveryItemsBody.length > 0) {
			delivery.loadDeliveryItems(deliveryItemsBody, deliveryItemsBody.data("ajax-url"));
		}

		const deliveryEstimateItemsBody = $("#delivery_estimate_items_body");
		if (deliveryEstimateItemsBody.length > 0) {
			delivery.loadDeliveryItems(deliveryEstimateItemsBody, deliveryEstimateItemsBody.data("ajax-url"), $("#delivery_estimate_items_placeholder"));
		}

		const addDeliveryLine = $("#add_delivery_line");
		addDeliveryLine.on('click', function(){
			$(this).prop('disabled', true).data('html', addDeliveryLine.text()).text('loading ...')
			let params = {'rows': 1, 'reload': 1}
			delivery.loadDeliveryItems(addDeliveryLine, addDeliveryLine.data('ajax-url'), params);
		});

		$(".action-delivery").on('click', function (e) {
			e.preventDefault();
			delivery.actionDelivery($(this));
		});

		$(".bulk-action").on('click', function(e){
			e.preventDefault();
			delivery.bulkInvoicesAction($(this));
		});

		$(".single-action").on('click', function(e){
			e.preventDefault();
			delivery.singleInvoiceAction($(this));
		});

		$("#id_date").on('change', function(){
			let deliveryDate = $("#id_date").val()
			if (deliveryDate !== "") {
				deliveryDate = moment(deliveryDate);
			}
			const today = moment(moment().format('DD-MMM-YYYY')).add(1, 'h');

			if (today.isSame(deliveryDate)) {
				$('#id_delivery_time').timepicker({
					minuteStep: 1,
					showSeconds: false,
					showMeridian: false
				});
			}
		});

		$('#id_delivery_time').timepicker({
			minuteStep: 1,
			showSeconds: false,
			showMeridian: false
		});

		const idCustomer = $("#id_customer");
		idCustomer.on('change', function(e){
			sales.loadCustomerDetails(idCustomer).always(function(customer){

			});
		});
	};

	delivery.bulkInvoicesAction = function(el)
	{
		const deliveries = Delivery.getSelectedDeliveries();
		const totalDeliveries = deliveries.length;
		if (totalDeliveries === 0) {
			alert('Please select at lease one delivery');
		} else {
			$.post(el.data('ajax-url'), {
				deliveries: deliveries
			}, function(response){
				message.responseNotification(response);
			});
		}
	};

	delivery.singleInvoiceAction = function(el)
	{
		const deliveries = Delivery.getSelectedDeliveries();
		const totalDeliveries = deliveries.length;
		if (totalDeliveries === 0) {
			alert('Please select at lease one delivery');
		} else if (totalDeliveries >1) {
			alert('Please select one delivery to action');
		} else {
			$.post(el.data('ajax-url'), {
				deliveries: deliveries
			}, function(response){
				message.responseNotification(response);
			});
		}
	};

	delivery.getSelectedDeliveries = function()
	{
		let deliveries = []
		$(".delivery-action").each(function(){
			if ($(this).is(":checked")) {
				deliveries.push($(this).data('delivery-id'));
			}
		});
		return deliveries;
	};

	delivery.actionDelivery = function(el)
	{
		$.getJSON(el.data('ajax-url'), function(response){
			message.responseNotification(response);
		});
	};

	delivery.reloadDeliveryRow = function(el)
	{
		const rowId = el.data('row-id');
		let params = {'rows': 1, 'reload': 1, row_id: rowId, line_type: $("#type_"+ rowId +" option:selected").val()}
		$.get(el.data('ajax-url'), params, function(html) {
			$("#sales_item_" + rowId).replaceWith(html);
			salesItem.handleRow(rowId);
		});
	};

	delivery.loadDeliveryItems = function(el, ajaxUrl, container, params)
	{
		let rowId = 0;
		const lastRow = $(".sales-items").last();
		if (lastRow.length > 0) {
			rowId = parseInt(lastRow.data('row-id')) + 1;
		}
		console.log("params ")
        console.log(params)
		delivery.loadDeliveryRow(el, ajaxUrl, rowId, lastRow, container, params)
	};

	delivery.loadDeliveryRow = function(el, ajaxUrl, rowId, lastRow, container, params)
	{
		params = params || {};
		container = container || $("#delivery_items_placeholder");
		params['row_id'] = rowId
		$.get(ajaxUrl, params, function(html) {
			if (lastRow !== undefined && lastRow.length > 0) {
				lastRow.after(html)
			} else {
				container.replaceWith(html);
			}
			const _lastRow = $(".delivery_item_row").last();
			if (_lastRow.length > 0) {
				rowId = parseInt(_lastRow.data('row-id'));
			}
			$(".sales-items").each(function(){
				salesItem.handleRow($(this).data("row-id"));
			});

			$(".line-type").on("change", function(e){
				delivery.reloadDeliveryRow($(this));
        	});
			el.prop('disabled', false).text(el.data('html'));
		});
	};

    delivery.init();

})();