(function() {
   //"use strict";

	window.Docuflow = window.Docuflow || {};
	var common = window.Docuflow.Common || {};
	var modal = window.Docuflow.Modal || {};
	var sales = window.Docuflow.Sales || {};
	var salesItem = window.Docuflow.SalesItem = {};

	salesItem.init = function()
	{
	};

    salesItem.chosenWidth = function()
    {
        let chosenWidth = 150;
        const minWidth = 50;
        const chosenContainer = $(".sales-items > .chosen-container");
        if (chosenContainer.length > 0 ) {
            chosenWidth = chosenContainer.width();
        }
        if (chosenWidth > 0) {
            $(".chosen-select-code").not('.line-type').chosen({'width': chosenWidth - 50 + "px"});
            $(".chosen-select-description").not('.line-type').chosen({'width': chosenWidth + "px"});
            $(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
            $(".line-type").chosen({'width': minWidth + 'px'});
        } else {
            $(".chosen-select-code").not('.line-type').chosen({'width': "100px"});
            $(".chosen-select-description").not('.line-type').chosen({'width': "150px"});
            $(".chosen-select").not('.line-type').chosen({'width': '150px'});
            $(".line-type").chosen({'width': '50px'});
        }
        return chosenWidth;
    };

    salesItem.handleRow = function(rowId, options = {})
    {
        const lineDiscount = $("#line_discount");
        const discountPercentageEl = $("#discount_percentage_" + rowId);
        if (lineDiscount.val() !== "" && discountPercentageEl.val() === "") {
            discountPercentageEl.val(lineDiscount.val());
        }
        $(".search-box").each(function(index, el){
            const searchBoxEl = $(el);
            const model = searchBoxEl.data('model');
            const modelSearchEl = $(".model-" + model + "-search-" + rowId);
            const searchUrl = modelSearchEl.data("ajax-url");
            const detailUrl = modelSearchEl.data("ajax-detail-url");
            modelSearchEl.autocomplete({
                minLength: 1,
                source: searchUrl,
                focus: function( event, ui ) {
                    $( "#" + model + "_item_" + rowId ).val(ui.item.description);
                    return false;
                },
                select: function( event, ui ) {
                    $( "#"+ model + "_" + rowId ).val(ui.item.id);
                    $( "#"+ model + "_item_" + rowId ).val(ui.item.code);

                    if (model === 'vat_code') {
                        salesItem.handleVatSelection(ui.item, rowId);
                    } else if (model === 'inventory') {
                        $( "#"+ model + "_item_description_" + rowId ).html(ui.item.description);
                        salesItem.handleItemData(model, ui.item.id, detailUrl, rowId, options);
                    } else if (model === 'account') {
                        salesItem.handleItemData(model, ui.item.id, detailUrl, rowId);
                    }
                    return false;
                }
            })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .append( "<div>" + item.code + "<br>" + item.description + "</div>" )
                    .appendTo( ul );
            };
        });

        $("#invoice_" + rowId).on("blur", function() {
            handleInventoryQuantity($(this), 2);
            salesItem.handleQuantity(rowId);
        });

        $("#quantity_" + rowId).on("blur", function() {
            handleInventoryQuantity($(this), 2);
            salesItem.handleOrderedQuantity(rowId);
        });

        $("#price_" + rowId).on("blur", function() {
            sales.calculateInventoryItemPrices(rowId);
        });

        $("#vat_code_" + rowId).on("change", function(){
            const selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
            const vatPercentage = selectedVatOption.data("percentage");
            if (vatPercentage !== undefined && vatPercentage !== "") {
                $("#vat_percentage_"+ rowId).val(vatPercentage);
            } else {
                $("#vat_percentage_"+ rowId).val(0);
            }
            sales.calculateInventoryItemPrices(rowId);
        });

        $("#discount_percentage_"+ rowId +"").on("blur", function(){
            sales.calculateInventoryItemPrices(rowId);
        });

        if (rowId) {
            sales.calculateInventoryItemPrices(rowId);
        }
    };

    salesItem.handleVatSelection = function(data, rowId) {
        const vatPercentageEl = $("#vat_percentage_"+ rowId);
        if (data.percentage !== undefined && data.percentage !== "") {
            vatPercentageEl.val(data.percentage);
        } else {
            vatPercentageEl.val(0);
        }
        sales.calculateInventoryItemPrices(rowId);
    };

    salesItem.handleItemData = function(model, modelId, detailUrl, rowId, options = {}) {
        const params = {};
        const customerEl = $("#id_customer");
        const pricingEl = $("#id_pricing");
        if (model === undefined) {
            params['inventory_id'] = modelId
        } else {
            params[model + "_id"] = modelId
        }
        params['customer_id'] = customerEl.val();
        if (pricingEl !== undefined) {
            params['pricing_id'] = pricingEl.val();
        }

        $.get(detailUrl, params, function(responseData){
            const customerLineDiscount = $("#line_discount").val();
            const price = responseData.selling_price;
            const priceIncluding = responseData.selling_price_with_vat;
            const unit = responseData.unit.code;
            const binLocation = responseData.bin_location;
            const description = responseData.text;
            const vatId = responseData.vat_code_id;
            const saleVatId = responseData.sale_vat_code.id;
            const unitId = responseData.unit.id;
            const isDiscountable = responseData.is_discounted;
            const allowNegativeStock = responseData.allow_negative_stocks;
            const netWeight = responseData.weight;
            const grossWeight = responseData.gross_weight;
            const averagePrice = responseData.history.average_price;
            let availableStock = responseData.history.in_stock;

            const priceEl =  $("#price_" + rowId);
            const priceIncEl =  $("#price_including_" + rowId);

            priceEl.val(price);
            priceIncEl.val(priceIncluding);
            if (unit !== undefined && unit !== "") {
                $("#unit_" + rowId).val(unit);
            }
            if (unitId !== undefined && unitId !== "") {
                $("#unit_id_" + rowId).val(unitId);
            }

            if (binLocation !== undefined && binLocation !== "") {
                $("#bin_location_" + rowId).val(binLocation)
            }
            if (description !== undefined && description !== "") {
                const descriptionEl = $("#inventory_description_" + rowId);
                if (descriptionEl) {
                    descriptionEl.html(description)
                }
                $("#description_" + rowId).val(description)
            }
            if (saleVatId !== undefined && saleVatId !== "") {
                $("#vat_code_" + rowId).val(saleVatId);
                $("#vat_percentage_" + rowId).val(responseData.sale_vat_code.percentage);
                $("#vat_code_item_" + rowId).val(responseData.sale_vat_code.code);
            }
            salesItem.handleRowDiscount(rowId, isDiscountable, customerLineDiscount);
            availableStock = parseFloat(availableStock);
            if (isNaN(availableStock)) {
                availableStock = 0;
            }
            if (availableStock > 0) {
                $("#order_item_" + rowId).html("No");
            } else {
                $("#order_item_" + rowId).html("Yes");
            }
            if (netWeight !== undefined) {
                $("#net_weight_" + rowId).val(netWeight);
            }
            if (grossWeight !== undefined) {
                $("#gross_weight_" + rowId).val(grossWeight);
            }
            if (averagePrice !== undefined) {
                $("#average_price_" + rowId).val(averagePrice);
            }
            $("#available_stock_" + rowId).html(accounting.formatMoney(availableStock));
            $("#available_in_stock_" + rowId).html(availableStock);
            $("#in_stock_" + rowId).val(availableStock);
            if ('add_type' in options && ['estimate'].indexOf(options.add_type) > -1) {
                $("#allow_negative_stock_" + rowId).val('1');
            } else {
                $("#allow_negative_stock_" + rowId).val(allowNegativeStock);
            }

        }, "json");
    };

    salesItem.handleQuantity = function(rowId){
        const orderedEl = $("#ordered_" + rowId);
        if (orderedEl.length > 0) {
            salesItem.handleOrderedQuantity(rowId, orderedEl);
        } else {
            sales.calculateInventoryItemPrices(rowId);
        }
    };

    salesItem.getCellVal = function(el)
    {
        if (el === undefined) {
            return 0;
        }
        if (el.length === 0) {
            return 0;
        }
        const elVal  = el.val();
        if (elVal === "") {
            return 0;
        }
        return parseFloat(elVal);
    };

    salesItem.handlePickingQuantity = function(rowId, quantity, availableQty){
        const el = $("#quantity_" + rowId);
        const backOrderEl = $("#back_order_" + rowId);
        const pickingBalanceEl = $("#picking_balance_" + rowId);
        const balancePickingEl = $("#balance_picking_" + rowId);
        const orderedQty = accounting.unformat($("#received_" + rowId).val());
        const pickingBalance = accounting.unformat(pickingBalanceEl.val());

        if (quantity > availableQty) {
            el.val(accounting.formatMoney(availableQty));
            backOrderEl.prop("disabled", true);
        } else if(quantity > orderedQty) {
            el.val(accounting.formatMoney(availableQty));
            backOrderEl.prop("disabled", true);
        } else {
            if (pickingBalance > 0) {
                backOrderEl.prop("checked", true).prop("disabled", false);
            } else if (quantity <= availableQty) {
                backOrderEl.prop("checked", false).prop("disabled", false);
            }
        }
        quantity = accounting.unformat(el.val());
        const balance = orderedQty - quantity;
        balancePickingEl.html(accounting.formatMoney(balance));
        pickingBalanceEl.val(accounting.toFixed(balance, 2));
        if (balance > 0) {
            backOrderEl.prop("disabled", false).prop("checked", true);
        } else {
            backOrderEl.prop("disabled", true).prop("checked", false);
        }
    }

    salesItem.handleOrderedQuantity  = function(rowId, quantityEl)
    {
        quantityEl = quantityEl || $("#quantity_" + rowId);
        const invoicedEl =  $("#invoiced_" + rowId);
        const quantityOrderedEl = $("#quantity_ordered_" + rowId) || $("#ordered_" + rowId);
        const backOrderEl = $("#back_order_" + rowId);
        const invoicedQty  = salesItem.getCellVal(invoicedEl);
        const orderedQty  = salesItem.getCellVal(quantityOrderedEl);
        const quantity = parseFloat(quantityEl.val());
        let inStock = salesItem.getCellVal($("#in_stock_" + rowId));
        let allowNegativeStock = salesItem.isNegativeStockAllowed(rowId);
        const isCredit = $("#is_credit").val();
        if (isCredit) {
            allowNegativeStock = true;
            inStock = orderedQty;
        }

        if (salesItem.isPicking(rowId)) {
            salesItem.handlePickingQuantity(rowId, quantity, inStock)
        } else if (salesItem.isOrderable(quantity, inStock, allowNegativeStock)) {
            if (isNaN(quantity)) {
               invoicedEl.val(0);
            } else {
                invoicedEl.val(quantity);
            }
            if (salesItem.hasElement(backOrderEl)) {
               if (quantity < orderedQty) {
                  backOrderEl.prop("disabled", false).prop("checked", true);
               } else {
                  backOrderEl.prop("disabled", true).prop("checked", false);
               }
            }
        } else {
            if (inStock !== undefined) {
               if (isNaN(inStock)) {
                    invoicedEl.val(0);
                    if (salesItem.hasElement(backOrderEl)) {
                        backOrderEl.prop("disabled", true);
                    }
                } else {
                    invoicedEl.val(inStock);
                    if (!allowNegativeStock) {
                        quantityEl.val(inStock)
                    }
                    if (salesItem.hasElement(backOrderEl)) {
                       backOrderEl.prop("disabled", false);
                    }
                }
            }
        }
        sales.calculateInventoryItemPrices(rowId);
    };

    salesItem.hasElement = function(el)
    {
        return el !== undefined;
    };

    salesItem.isNegativeStockAllowed = function(rowId)
    {
        const allowNegativeStockEl = $("#allow_negative_stock_" + rowId);
        if (allowNegativeStockEl.length) {
            const allowNegativeStockVal = allowNegativeStockEl.val();
            if (allowNegativeStockVal === '1' || allowNegativeStockVal === 1) {
                return true
            }
        }
        return false;
    };

    salesItem.isPicking = function(rowId) {
        if ($("#picked_" + rowId).length > 0) {
            return true;
        }
        return false;
    };

    salesItem.isOrderable = function(quantity, inStock, allowNegativeStock)
    {
        let isCreditVal = $("#invoice_type_id").val();
        if (isCreditVal === '1') {
            return true;
        }
        if (quantity <= inStock) {
            return true;
        }
        if (allowNegativeStock) {
            return true;
        }
        return false;
    };

    salesItem.deleteRow = function(rowId)
    {
        $("#sales_item_" + rowId).remove();

        sales.calculateInventoryItemPrices(rowId);
    };

    salesItem.handleRowDiscount = function(rowId, isDiscountable, lineDiscount)
    {
        const discountPercentageEl = $("#discount_percentage_" + rowId);
        if (isDiscountable) {
            let discountVal = lineDiscount || 0;
            discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
        } else {
            discountPercentageEl.val(0).prop('disabled', true).trigger('change');
            $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
            $("#discount_amount_" + rowId).val('0.00').trigger('change');
        }
    };

	salesItem.init();

})();