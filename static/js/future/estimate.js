(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer || {};
    var salesItem = window.Docuflow.SalesItem || {};
    var sales = window.Docuflow.Sales || {};
    var estimate = window.Docuflow.Estimate = {};

    estimate.init = function()
	{

		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");

				initDatePicker();

				initChosenSelect();

				$("#save_comment").on('click', function (event) {
					event.preventDefault();
					ajaxSubmitForm($(this), $("#comment_form"));
				});

				$("#save_estimate_customer_copy").on('click', function (event) {
					event.preventDefault()
					ajaxSubmitForm($(this), $("#create_customer_estimate_copy_form"));
				});

				const el = $("#id_customer");
				el.on("change", function (e) {
					e.preventDefault();
					$("#customer_details").html("Loading customer details ...")
					estimate.loadCustomerDetails(el).always(function(customer){
						$('.invoice-col').matchHeight();
					});
				});

			});
		});

		if (jQuery().matchHeight) {
			//run plugin
			$(".height").matchHeight()
		}

		$("#estimates>tbody>tr>td:not(.actions)").on("click", function(e){
			const detailUrl = $(this).parent("tr").data("detail-url");
			if (detailUrl !== undefined && detailUrl !== "") {
				document.location.href = detailUrl;
			}
			return false;
		});

		$(".save-estimate").on("click", function(e){
			e.preventDefault();
			const next = $(this).data("action");
			form.submit($(this), $("#estimate_form"), {"next": next});
		});

		const estimateItemsBody = $("#estimate_items_body");
		if (estimateItemsBody.length > 0) {
			estimate.loadEstimateItems(estimateItemsBody, estimateItemsBody.data("ajax-url"), {
				'add_type': 'estimate'
			});
		}

		const addEstimateLine = $("#add_estimate_line");
		addEstimateLine.on('click', function(){
			$(this).prop('disabled', true).data('html', addEstimateLine.text()).text('loading ...')
			let params = {'rows': 1, 'add_type': $(this).data('add-type')}
			estimate.loadEstimateItems(addEstimateLine, addEstimateLine.data('ajax-url'), params);
		});

		$(".action-estimate").on('click', function (e) {
			e.preventDefault();
			let message= $(this).data('confirm-message');
			if (message !== '') {
				if (confirm(message)) {
					estimate.actionEstimate($(this));
				}
			}  else {
				estimate.actionEstimate($(this));
			}
		});

		$(".bulk-action").on('click', function(e){
			e.preventDefault();
			estimate.bulkInvoicesAction($(this));
		});

		$(".single-action").on('click', function(e){
			e.preventDefault();
			estimate.singleInvoiceAction($(this));
		});

		const idCustomerEl = $("#id_customer");
		idCustomerEl.on('change', function(e){
			e.preventDefault();
			sales.loadCustomerDetails(idCustomerEl);
		});

		$("#id_is_deposit_required").on("change", function (e) {
			const depositRequired = $("#deposit_required");
			const idDeposit = $("#id_deposit");
			if ($(this).is(":checked")) {
				depositRequired.show();
			} else {
				idDeposit.val("");
				depositRequired.hide();
			}
		});

	};

	estimate.loadCustomerDetails = function(el)
	{
		console.log('Load customer details');
		return $.getJSON($("#id_customer option:selected").data('detail-url'), {
			module: 'sales'
		}, function(response){
			sales.displayCustomerDetails(response)
			initChosenSelect();
		});
	};

    estimate.bulkInvoicesAction = function(el)
	{
		const estimates = Estimate.getSelectedEstimates();
		const totalEstimates = estimates.length;
		if (totalEstimates === 0) {
			alert('Please select at lease one estimate');
		} else {
			const confirm = el.data('confirm');
			if (confirm === '1') {
				var confirmText = el.data('confirm-text')
				return confirm(confirmText)
			}
			$.post(el.data('ajax-url'), {
				estimates: estimates
			}, function(response){
				responseNotification(response);
			});
		}
	};

	estimate.singleInvoiceAction = function(el)
	{
		var estimates = Estimate.getSelectedEstimates();
		var totalEstimates = estimates.length;
		if (totalEstimates === 0) {
			alert('Please select at lease one estimate');
		} else if (totalEstimates >1) {
			alert('Please select one estimate to action');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( estimates );
			console.log( ajaxUrl );
			$.post(ajaxUrl, {
				estimates: estimates
			}, function(response){
				responseNotification(response);
			});
		}
	};

	estimate.getSelectedEstimates = function()
	{
		var estimates = [];
		$(".estimate-action").each(function(){
			if ($(this).is(":checked")) {
				estimates.push($(this).data('estimate-id'));
			}
		});
		return estimates;
	};

	estimate.actionEstimate = function(el)
	{
		console.log("Action estimate")
		console.log(el)
		var ajaxUrl = el.data('ajax-url');
		console.log(ajaxUrl)
		$.getJSON(ajaxUrl, function(response){
			console.log(response)
			responseNotification(response)
		});
	};

	estimate.reloadEstimateRow = function(el, options = {})
	{
		console.log("RELOAD ESTIMATE ROW --> ", )
		const rowId = el.data('row-id');
		const ajaxUrl = el.data('ajax-url');
		const chosenWidth = $("#"+ rowId + "_estimate_item_chosen").width();
		$.get(ajaxUrl, {
			row_id: rowId,
			line_type: $("#type_"+ rowId +" option:selected").val(),
			reload: 1,
			rows: 1
		}, function(html) {
			$("#sales_item_" + rowId).replaceWith(html);

			salesItem.handleRow(rowId, options);
			$(".line-type").on("change", function(e){
				estimate.reloadEstimateRow($(this), options);
        	});

		});

	};

	estimate.loadEstimateItems = function(el, ajaxUrl, params)
	{
		let rowId = 0;
		const lastRow = $(".estimate_item_row").last();
		if (lastRow.length > 0) {
			rowId = parseInt(lastRow.data('row-id')) + 1;
		}
		estimate.loadEstimateRow(el, ajaxUrl, rowId, lastRow, params)
	};

	estimate.loadEstimateRow = function(el, ajaxUrl, rowId, lastRow, params)
	{
		params = params || {}
		params['row_id'] = rowId
		const container = $("#estimate_items_placeholder");
		$.get(ajaxUrl, params, function(html) {
			if (lastRow !== undefined && lastRow.length > 0) {
				lastRow.after(html)
			} else {
				container.replaceWith(html);
			}
			const _lastRow = $(".estimate_item_row").last();
			if (_lastRow.length > 0) {
				rowId = parseInt(_lastRow.data('row-id'));
			}
			estimate.updateCalculations(params);
			$(".line-type").on("change", function(e){
				estimate.reloadEstimateRow($(this), params);
        	});
			el.prop('disabled', false).text(el.data('html'));
		});
	};

	estimate.updateCalculations = function(params)
	{
		$(".sales-items").each(function(){
			const salesItemRowId = $(this).data('row-id');
			salesItem.handleRow(salesItemRowId, params);
		});
	};

    estimate.init();

})();