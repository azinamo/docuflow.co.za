(function(){
    'use strict';

    window.Docuflow = window.Docuflow || {};
    var form = window.Docuflow.Form || {};
    var message = window.Docuflow.Message = {};

    message.init = function() {

    };

    message.displayFormError = function() {

    };

    message.flash = function(hasError, alertMessage)
    {
        var html = '<div class="alert alert-' + (hasError === false ? 'success' : 'danger') + ' alert-fixed mt-3 mb-0">' + alertMessage + '</div>';
        if (hasError === false) {
            $(html).prependTo('.modal-body').delay(1000).queue(function () {
                $(this).remove()
                //document.location.reload();
            });
        } else {
            $(html).prependTo('.modal-body').delay(1000);
        }
    };

    message.displayFormError = function(element, errorMessage)
    {
        if (element.length > 0) {
            if (!element.hasClass("is-invalid")) {
                element.addClass('is-invalid');
                element.after('<div class="is-invalid-message has-error">' + errorMessage + '</div>');
                // TODO if the form is a horizontal form, look for the second parent div and highlight that
                // TODO otherwise highligh the first parent div
                var secondParent = $(element.parent()).parent();
                if (secondParent.length) {
                    if (secondParent[0].nodeName === 'DIV') {
                        secondParent.addClass('error').addClass('has-error').addClass('text-danger');
                    } else {
                        $(element.parent()).addClass('error').addClass('has-error').addClass('text-danger');
                    }
                }
            }
        }
    };

    message.ajaxNotificationDialog = function(url, params)
    {
        var ajaxModal =$(".modal");
        if (ajaxModal.length > 0) {
            $(".modal-backdrop").remove()
            ajaxModal.remove();
        }

        $.get(url, params, function (data) {
            $(data).modal("show");
            var formEL = $(data).find("form");

            $("#id_override_key").on("blur", function(e){
                //salesInvoice.validateOverrideKey($(this).val());
                var el = $(this);
                var ajaxUrl = el.data('validate-url');

                var resultEl = $("#override_result");
                resultEl.html("validating ...");
                $.get(ajaxUrl, {
                    override_key: el.val()
                }, function(response) {
                    if (response.error) {
                        resultEl.html(response.text);
                    } else {
                        resultEl.html(response.text);
                        $("#override_continue").prop('disabled', false);
                    }
                }, 'json');
            });

            $(".save-document").on("click", function(e){
                e.preventDefault();
                var formId = $(formEL).attr("id");
                var formLEl = $("#sales_invoice_form");
                formLEl.LoadingOverlay("hide");
                var saveType = $(this).data('save-type');
                formLEl.ajaxSubmit({
                    data: {'save_type': saveType},
                    url: formLEl.attr("action"),
                    target:        "#loader",   // target element(s) to be updated with server response
                    beforeSubmit:  function(formData, jqForm, options) {
                        var queryString = $.param(formData);
                    },  // pre-submit callback
                    error: function (data) {
                        // handle errors
                        var error = {'error': true, 'text': data.statusText};
                        message.responseNotification(error)
                        formLEl.LoadingOverlay("hide");
                    },
                    success:       function(responseJson, statusText, xhr, $form)  {
                        // handle response
                        message.responseNotification(responseJson, statusText, xhr)
                        formLEl.LoadingOverlay("hide");
                    },  // post-submit callback
                    dataType: "json",
                    type: "post",
                    timeout:   3000
                });
               //form.submit($(this), $("#sales_invoice_form"), {'override': true});
            });

            $(".invoice-amount").on("keyup", function() {
                var invoiceAmount = accounting.toFixed($("#total_invoice_amount").val(), 2);
                var allocatedTotal = 0;
                $(".invoice-amount").each(function(){
                    var methodAmount = $(this).val();
                    if (methodAmount !== undefined && methodAmount !== "") {
                        allocatedTotal += accounting.toFixed(methodAmount, 2);
                    }
                });
                var hasChange = $(this).data('has-change');
                var changeAmount = 0
                if (hasChange) {
                    if (allocatedTotal > invoiceAmount) {
                         changeAmount = allocatedTotal - invoiceAmount
                         $("#change_row").show()
                         $("#change").val(accounting.formatMoney(changeAmount));
                    }
                }

                var balance = invoiceAmount - allocatedTotal;

                if (balance === 0) {
                    $("#save_receipts").prop("disabled", false);
                } else {
                    $("#save_receipts").prop("disabled", true);
                }
                $("#balance").html(accounting.formatMoney(balance));
            });

            $(".modal-save").on("click", function(e){
                e.preventDefault();
                //alert("Saving form --> " + form + " " +  $(this) + " -> " +  $(formEL));
                console.log($(this), $(formEL));
                console.log(form);
                var formId = $(formEL).attr("id");
                console.log("Form id -=> ", formId);
                $("#" + formId).ajaxSubmit({
                    url: formEL.attr("action"),
                    target:        "#loader",   // target element(s) to be updated with server response
                    beforeSubmit:  function(formData, jqForm, options) {
                        var queryString = $.param(formData);
                    },  // pre-submit callback
                    error: function (data) {
                        // handle errors
                        message.responseNotification(data)
                    },
                    success:       function(responseJson, statusText, xhr, $form)  {
                        // handle response
                        message.responseNotification(responseJson, statusText, xhr)
                    },  // post-submit callback
                    dataType: "json",
                    type: "post",
                    timeout:   3000
                });
                //form.submitModal($(this), $(formEL));
                return false;
            });

            $("#sign_invoice_with_variance_submit").on("click", function (event) {
                event.stopImmediatePropagation();
                Invoice.signInvoice($(this));
            });


            $(".discount-invoice-check").on('change', function(e){
                if ($(this).is(":checked")) {
                    Payment.addDiscount($(this))
                } else {
                    Payment.removeDiscount($(this))
                }
                Payment.allChecked($(".discount-invoice-check"), $("#discount_invoice"))
            });

            $("#discount_invoice").on('change', function(e){
                console.log("Discount all invoices");
                if ($(this).is(":checked")) {
                    $(".discount-invoice-check").each(function(){
                        var _this = $(this);
                        if (!_this.is(":checked")) {
                            Payment.addDiscount(_this)
                            _this.prop('checked', true)
                        }
                    })
                } else {
                    $(".discount-invoice-check").each(function(){
                        var _this = $(this);
                        Payment.removeDiscount(_this);
                        _this.prop('checked', false)
                    })
                }
            });

            $("#id_discount_disallowed").on("blur", function(e){
                Payment.calculateNetPayment();
            });


            $("#make_payment").on('click', function(e){
                e.preventDefault();
                form.submitModal($("#pay_invoices"));
            });

            $("#continue_payment").on('click', function(e){
                e.preventDefault();
                var unAllocatedReason = $("#payment_comment").val();
                if (unAllocatedReason === '') {
                    alert("Please enter the reason")
                    return false;
                } else {
                    $("#id_comment").text(unAllocatedReason);
                    form.submit($("#sundry_payment"));
                }
            });

            $("#save_invoice_asset").on('click', function(e){
                e.preventDefault();
                form.submitModal($("#create_invoice_asset"));
            });

            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $(".chosen-select").chosen();

        });
    };

    message.removeFormError = function(element)
    {
        if (element.length > 0) {
            if (element.hasClass("is-invalid")) {
                element.removeClass('is-invalid');
                // TODO if the form is a horizontal form, look for the second parent div and highlight that
                // TODO otherwise highligh the first parent div
                var secondParent = $(element.parent()).parent();
                if (secondParent.length) {
                    console.log(secondParent)
                    if (secondParent[0].nodeName === 'DIV') {
                        console.log(secondParent[0])
                        secondParent.find('div.is-invalid-message').remove();
                        secondParent.removeClass('error').removeClass('has-error').removeClass('text-danger');
                    } else {
                        $(element.parent()).find('div.is-invalid-message').remove();
                        $(element.parent()).removeClass('error').removeClass('has-error').removeClass('text-danger');
                    }
                }
            }
        }
    };

    message.highlightFormErrors = function (errors)
    {
        var scrollTo, counter = 0;
        $.each(errors, function (key, value) {
            var element;
            var keyElement = $("#id_" + key);
            var idElement = $("#" + key);
            var chosenElement;
            if (keyElement.length) {
                element = keyElement;
                chosenElement = $("#id_" + key + "_chosen");
            } else if(idElement.length) {
                element = idElement;
                chosenElement = $("#" + key + "_chosen");
            }
            if (chosenElement !== undefined && chosenElement.length > 0) {
                element = chosenElement;
                message.displayFormError(element, value[0]);
            } else if (element !== undefined && element.length > 0) {
                message.displayFormError(element, value[0]);
            }
            counter++;
        });
        var errorDiv = $(".error");
        if (errorDiv.length > 0) {
            $('html, body').animate({
                scrollTop: (errorDiv.first().offset().top)
            }, 500);
        }
    };

    message.alertErrors = function(text)
    {
        var notificationModal =$("#notification-modal");
        if (notificationModal.length > 0) {
            notificationModal.remove();
        }

        var html = '<div class="modal modal-notification show" tabindex="-1" data-backdrop="static" id="notification-modal">';
        html += '	<div class="modal-dialog modal-lg">';
        html += '		<div class="modal-content">';
        html += '			<div class="modal-header">';
        html += '				<button type="button" class="close notification-close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        html += '					<h4 class="modal-title" id="model-title">Warning</h4>';
        html += '			</div>';
        html += '			<div class="modal-body" id="modalbody">';
        html +=	'				<div class="text-danger">' + text + '</div>';
        html +=	'  			</div>';
        html += '			<div class="modal-footer" id="modalfooter">';
        html += '				<a type="button" class="btn btn-primary notification-close" data-dismiss="modal">Ok</a>';
        html += '			</div>';
        html += '		</div>';
        html += '	</div>';
        html += '</div>';

        $("body").append(html);

        $(".notification-close").on('click', function(e){
            $("#notification-modal").remove();
            e.preventDefault();
        });
   };

   message.responseNotification = function(jsonResponse, form)
   {
        if (jsonResponse.hasOwnProperty('alert')) {
            if (jsonResponse.hasOwnProperty('url')) {
                message.ajaxNotificationDialog(jsonResponse.url)
            } else {
                message.alertErrors(jsonResponse.text)
            }

            if (jsonResponse.hasOwnProperty('errors')) {
                message.highlightFormErrors(jsonResponse.errors)
            }
        } else {
            if (jsonResponse.hasOwnProperty('error')) {
                $.notify({
                    title: (jsonResponse.error ? 'Error' : 'Success:'),
                    message: jsonResponse.text
                }, {
                    type: (jsonResponse.error ? 'danger' : 'success')
                });
            }

            if (jsonResponse.hasOwnProperty('file')) {
                var invoicePdf = $("#invoice_pdf");
                if (invoicePdf.length > 0) {
                    invoicePdf.replaceWith('<div id="invoice_pdf" data-pdf-url="'+ jsonResponse.file +'"></div>');
                    PDFObject.embed(jsonResponse.file, "#invoice_pdf");
                }
                if ($("#invoice_image").length > 0) {
                    $("#invoice_image").val(responseJson.file);
                }
            }

            if (jsonResponse.hasOwnProperty('errors')) {

                var errorMessageContainer = $("#error_messages");
                var html = [];
                html.push("<ul>");
                $.each(jsonResponse.errors, function(key, error){
                    displayFormError($("#id_" + key), error)
                    html.push("<li>"+ key + ": " + error + "</li>");
                });
                html.push("</ul>");
                if (errorMessageContainer.length > 0) {
                    errorMessageContainer.html(html.join(' '));
                } else {
                    $(html).prependTo('.modal-body');
                }

            }
            if (jsonResponse.hasOwnProperty('url')) {
                return message.ajaxNotificationDialog(jsonResponse.url)
            }
            if (jsonResponse.hasOwnProperty('redirect')) {
                setTimeout(function(){
                    document.location.href = jsonResponse.redirect
                }, 500)
            }
            if (jsonResponse.hasOwnProperty('reload') && jsonResponse.reload) {
                setTimeout(function() {
                    document.location.reload()
                }, 1500)
            }
            //
            // flash success message

            // dismiss modal
            if (jsonResponse.hasOwnProperty('dismiss_modal')) {
                var modal = $(".modal-ajax");
                if (modal.length) {
                    modal.modal('hide');
                    modal.remove()
                }
            }

            // reload page
            if (jsonResponse.hasOwnProperty('reload_page')) {
                location.reload();
            }

            // reload datatables
            if (jsonResponse.hasOwnProperty('reload_datatables')) {
                $($.fn.dataTable.tables()).DataTable().ajax.reload();
            }

            // reload sources
            if (jsonResponse.hasOwnProperty('reload_sources')) {
                $('[data-source]').each(function () {
                    $.get($(this).data('source'), function (data) {
                        $(this).html(data);
                    });
                });
            }
        }
   };

   message.init();

})();