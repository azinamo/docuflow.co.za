$(function() {
	'use strict';

	window.Docuflow = window.Docuflow || {};
	var common = window.Docuflow.Common || {};
	var modal = window.Docuflow.Modal || {};
	var inventory = window.Docuflow.Inventory || {};

	//Form Submit for IE Browser
	inventory.init = function()
	{
		$(document).on('click', '[data-modal]', function (event) {
			event.preventDefault();
			$.get($(this).data('modal'), function (content) {
				modal.create(content)
			});
		});

		$(".list-table>tr>td:not(.actions)").on('click', function(){
			var detailUrl = $(this).parent('tr').data('detail-url');
			if (detailUrl === undefined && detailUrl !== "") {
				document.location.href = detailUrl;
			}
			return false;
		});


		var sellingPrice = $("#id_selling_price");
		var vatCode = $("#id_vat_code");
		vatCode.on('change', function(e){
			inventory.calculateSellingPriceWithVat(sellingPrice.val(), $(this).val())
		});
		sellingPrice.on('blur', function(e){
			inventory.calculateSellingPriceWithVat(sellingPrice.val(), $(this).val())
		});


		$("#inventory_item>tr>td:not(.actions)").on('click', function(){
			var detailUrl = $(this).parent('tr').data('detail-url');
			document.location.href = detailUrl;
			return false;
		});

		var viewInventoryItem = $("#view_inventory_item");
		var inventorySearchUrl = viewInventoryItem.data('ajax-url');
		viewInventoryItem.autocomplete({
			source: inventorySearchUrl,
			focus: function() {
			  // prevent value inserted on focus
			  return false;
			},
			select: function( event, ui ) {
			  // add the selected item
			  if (ui.item) {
				  inventory.displayDetail(ui.item.detail_url)
			  }
			  return false;
			}
		});

		var inventoryQuantity = $(".quantity");
		inventoryQuantity.on('blur', function(){
			var inventoryItemId = $(this).data('inventory-item-id');
			inventory.calculateAdjustment(inventoryItemId);
		});

		$("#save_inventory_item").on('click', function(){
			common.submitForm($("#create_inventory_item"));
		});

		$("#save_and_copy_to_all").on('click', function(){
			common.submitForm($("#create_inventory_item"), {'is_copied': true})
		});

		$("#submit_create_branch_inventory").on('click', function(){
			common.submitForm($("#create_branch_inventory"))
		});

		$("#update_inventory").on('click', function(){
			common.submitForm($("#update_inventory_form"))
		});

		var searchPurchaseOrder = $("#id_reference");
		var poSearchUrl = searchPurchaseOrder.data('ajax-url');

		var supplier = $("#id_supplier");
		supplier.on('change', function(){
			inventory.loadSupplierItems($(this));
		});


		$("#id_purchase_order").hide();
		$("#id_purchase_order_chosen").hide();
		var supplier = $("#id_supplier_returned");
		supplier.on('change', function(){
			inventory.loadSupplierItems($(this));
		});



		$("#submit_goods_received").on('click', function(e){
			common.submitForm($("#create_goods_received_form"));
			return false;
		});


		$("#submit_goods_returned").on('click', function(e){
			common.submitForm($("#create_goods_returned"));
			return false;
		});

		$("#adjust_stock_submit").on('click', function(){
			common.submitForm($("#adjust_stock"));
			return false;
		});

		var inventoryItemsBody = $("#inventory_items_body");
		if (inventoryItemsBody.length > 0) {
			var ajaxUrl = inventoryItemsBody.data('ajax-url');
			inventory.loadInventoryItems(ajaxUrl);
		}

		var addLine = $("#add_received_line");
		addLine.on('click', function(){
			var ajaxUrl = addLine.data('ajax-url');
			inventory.loadInventoryItems(ajaxUrl);
		});

		$("#enter_totals").on("change", function(event){
			inventory.openTotalInputs($(this))
		});

	};

	inventory.displayLedgerDetail = function(el)
	{
		var el = $(el);
		var lineId = el.data('line-id');
		console.log("has class open ==> ",  el.hasClass('open') );
		if (el.hasClass('open')) {
			$(".row-" + lineId).hide();
			el.removeClass('open');
		} else {
			if ($("#row-" + lineId).length > 0) {
				$(".row-" + lineId).show();
				el.addClass('open');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( ajaxUrl );
				console.log( lineId );
				$.get(ajaxUrl, function(html){
					$("#row_" + lineId ).after(html);
					el.addClass('open');
				});
			}
		}
	};

	inventory.openTotalInputs = function(el)
	{
		var isChecked = el.is(":checked");
		$(".price-incl").each(function(){
			if (isChecked) {
				$(this).prop('type', 'text');
			} else {
				$(this).prop('type', 'hidden');
			}
		});
	};

	inventory.loadInventoryItems = function(ajaxUrl)
	{
		var container = $("#inventory_items_placeholder");
		var rowId = 0;
		var lastRow = $(".inventory_item_row").last();
		console.log("Last row");
		console.log( lastRow );
		if (lastRow.length > 0) {
			console.log("Row was");
			console.log( lastRow.data('row-id') );
			rowId = parseInt(lastRow.data('row-id')) + 1;
		}
		console.log("Row id");
		console.log( rowId );
		// var chosenWidth = $("#"+ rowId + "_inventory_item_chosen").width();
		$.get(ajaxUrl, {
			row_id:rowId
		}, function(html) {
			if (lastRow.length > 0) {
				lastRow.after(html)
			} else {
				container.replaceWith(html);
			}
			$("#"+ rowId +"_inventory_item").on('change', function(){
				var selectedOption = $("#"+ rowId +"_inventory_item option:selected");
				var price = selectedOption.data('price');
				var unit = selectedOption.data('unit');
				$("#price_" + rowId ).val( price );
				$("#unit_" + rowId ).val( price );
				inventory.calculateInventoryItemPrices(rowId);
			});

			$("#quantity_received_"+ rowId +"").on('blur', function(){
				inventory.calculateInventoryItemPrices(rowId);
			});

			$("#vat_code_"+ rowId ).on('change', function(){
				console.log('Calculating based on vat change')

				var netTotal = $("#total_price_exl_" + rowId ).text();
				var vatSelected = $("#vat_code_"+ rowId  + " option:selected");
				var percentage = vatSelected.data('percentage');
				console.log( netTotal )
				console.log( vatSelected )
				console.log( percentage )
				if (netTotal !== '' && percentage !== '') {
					var vatAmount = (parseFloat(percentage)/100) * parseFloat(netTotal);
					var vatAmountStr = accounting.formatMoney(vatAmount);
					$("#vat_" + rowId ).html( vatAmountStr );
					$("#vat_amount_" + rowId ).val( vatAmount );
				}
				inventory.calculateInventoryItemPrices(rowId);
			});

			$("#price_"+ rowId +"").on('blur', function(){
				inventory.calculateInventoryItemPrices(rowId);
			});

			$(".chosen-select").chosen();
		});
	};

	inventory.loadSupplierItems = function(el)
	{
		var ajaxUrl = el.data('items-ajax-url');
		var supplier_id = el.val();
		var idReference = $("#id_purchase_order");
		$("#id_purchase_order_chosen").show();

		idReference.html('');
		$.getJSON(ajaxUrl, {
			supplier_id: supplier_id
		}, function(response){
			var html = [];
			html.push("<option value=''>--</option>");
			response.map(function(item){
				html.push("<option value='"+ item.id +"' " +
					"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
			});
			idReference.html(html.join('')).trigger("chosen:updated");

			idReference.on('change', function (e) {
				var optionSelected = $("#id_purchase_order  option:selected");
				var ajaxUrl = optionSelected.data('ajax-url');
				inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
			});
		});
	};

	inventory.calculateAdjustment = function(inventoryItemId)
	{
		var onHand = $("#quantity_on_hand_" + inventoryItemId);
		var newQuantity = $("#new_quantity_" + inventoryItemId);

		if (onHand.val() !== '' && newQuantity.val() !== '') {
			var adjustment = parseInt(onHand.val()) - parseInt(newQuantity.val());
			$("#adjustment_" + inventoryItemId).val( adjustment );
		}
	};

	inventory.calculateSellingPriceWithVat = function(sellingPrice, vatCodeId)
	{
		var sellingPriceWithVat = $("#selling_price_inc_vat");
		var ajaxUrl = sellingPriceWithVat.data('calculate-selling-price');
		$.getJSON(ajaxUrl, {
			vat_code_id: vatCodeId,
			selling_price: sellingPrice
		}, function(response) {
			if (!response.error) {
				sellingPriceWithVat.val(response.price);
			}
		});
	};

	inventory.displayDetail = function(detailUrl)
	{
		$.get(detailUrl, function(html){
		   $("#inventory_item_detail").html(html);
		});
	};

	inventory.displayPurchaseOrderInventoryItems = function(ajaxUrl)
	{
		if (ajaxUrl) {
			$.get(ajaxUrl, function(html){
			   $("#purchase_order_items").html(html);

			   $(".price-exc, .ordered, .received").on('blur', function(){
					var inventoryItemId = $(this).data('item-id');
					Inventory.calculateInventoryItemPrices(inventoryItemId)
			   });
			});
		} else {
			$("#purchase_order_items").html("");;
		}
	};

	inventory.calculateInventoryItemPrices = function(inventoryItemId)
	{
		console.log("Calculate totals --> ", inventoryItemId );
		var price = $("#price_" + inventoryItemId);
		var ordered = $("#quantity_ordered_" + inventoryItemId);
		var received = $("#quantity_received_" + inventoryItemId);
		var short = $("#quantity_short_" + inventoryItemId);
		var vat = $("#vat_amount_" + inventoryItemId);
		var backOrder = $("#bo_" + inventoryItemId);

		var quantityOrdered = 0;
		var shortQuantity = 0;
		var quantityReceived = 0;
		var vatAmount = 0;
		var priceExcl = 0;

		if (ordered !== undefined && ordered.length > 0) {
			if (ordered.val() !== '') {
				quantityOrdered = parseFloat(ordered.val());
			}
		}
		if (received !== undefined && received.length > 0) {
			if (received.val() !== '') {
				quantityReceived = parseFloat(received.val());
			}
		}
		if (vat !== undefined && vat.length > 0) {
			if (vat.val() !== '') {
				vatAmount += parseFloat(accounting.unformat(vat.val()));
			}
		}
		if (quantityOrdered > 0) {
			if (quantityReceived <= quantityOrdered) {
				shortQuantity =  quantityOrdered - quantityReceived
			} else {
				alert('Please enter valid received quantity')
				return false;
			}
		}
		if (short !== undefined  && short.length > 0) {
			short.val( shortQuantity );
		}
		if (price !== undefined  && received.length > 0) {
			if (price.val() !== '') {
				priceExcl = parseFloat(accounting.unformat(price.val()));
			}
		}

		var totalExcl = priceExcl * quantityReceived;
		var priceIncl =  priceExcl + vatAmount;
		var totalInc = priceIncl * quantityReceived;

		console.log( " quantityReceived ", quantityReceived );
		console.log( " Price EXCL", priceExcl );
		console.log( " totalExcl ", totalExcl );
		console.log( " vatAmount ", vatAmount );
		console.log( " priceIncl ", priceIncl );
		console.log( " totalInc", totalInc );



		if (backOrder !== undefined) {
			if (shortQuantity === 0) {
				backOrder.prop('disabled', true);
			} else {
				backOrder.prop('disabled', false);
			}
		}

		$("#price_incl_" + inventoryItemId).html( accounting.formatMoney(priceIncl) );
		$("#price_including_" + inventoryItemId).val( priceIncl );

		$("#total_price_exl_" + inventoryItemId).html( accounting.formatMoney(totalExcl) );
		$("#total_price_excluding_" + inventoryItemId).val( totalExcl );

		$("#total_price_incl_" + inventoryItemId).html( accounting.formatMoney(totalInc) );
		$("#total_price_including_" + inventoryItemId).val( totalInc );

		inventory.calculateTotals()
	};

	inventory.calculateTotals = function()
	{
		var totalOrdered = 0;
		var totalReceived = 0;
		var totalShort = 0;
		var priceExc = 0;
		var priceInc = 0;
		var totalPrice = 0;
		var totalTotalExc = 0;
		var totalTotalInc = 0;

		$(".ordered").each(function(){
			totalOrdered += parseInt($(this).val());
		});

		$(".received").each(function(){
			totalReceived += parseInt($(this).val());
		});
		$(".short").each(function(){
			totalShort += parseInt($(this).val());
		});
		$(".price").each(function(){
			totalPrice += parseFloat(accounting.unformat($(this).val()));
		});
		$(".price-exc").each(function(){
			priceExc += parseFloat(accounting.unformat($(this).val()));
		});
		$(".price-incl").each(function(){
			priceInc += parseFloat(accounting.unformat($(this).val()));
		});
		$(".total-price-excl").each(function(){
			totalTotalExc += parseFloat(accounting.unformat($(this).val()));
		});
		$(".total-price-incl").each(function(){
			totalTotalInc += parseFloat(accounting.unformat($(this).val()));
		});

		$("#total_ordered").html( totalOrdered );
		$("#total_received").html( totalReceived );
		$("#total_short").html( totalShort );
		$("#total_price").html( accounting.formatMoney(totalPrice) );
		$("#price_excl").html( accounting.formatMoney(priceExc) );
		$("#price_incl").html( accounting.formatMoney(priceInc) );
		$("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
		$("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );

	};

	inventory.init();
});