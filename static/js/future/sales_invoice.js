(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer || {};
    var salesItem = window.Docuflow.SalesItem || {};
    var sales = window.Docuflow.Sales || {};
    var salesInvoice = window.Docuflow.SalesInvoice = {};

    salesInvoice.init = function()
	{
		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");
				const form = $(data).find("form");

				common.initDatePicker();
				common.initChosenSelect();

				$("#save_customer_btn").on("click", function(e){
					e.preventDefault();
					event.preventDefault();
					submitModalForm($("#customer_form"));
				});

			    $("#id_override_key").on("blur", function(e){
				    salesInvoice.validateOverrideKey($(this).val());
			    });

			    $("#override_continue").on("click", function(){
				   form.submit($(this), $("#sales_invoice_form"), {'override': true});
			    });

				$("#save_comment").on('click', function (event) {
					event.preventDefault();
					ajaxSubmitForm($(this), $("#comment_form")).then(function(){
						$(".modal-ajax").remove()
						$(".modal-backdrop").remove()

						salesInvoice.getCommentsBox();
					});
				});

			});
		});

		$('.invoice-col').matchHeight();

		$("#sales_documents>tbody>tr>td:not(.actions)").on("click", function(){
			const detailUrl = $(this).parent("tr").data("detail-url");
			if (detailUrl !== undefined && detailUrl !== "") {
				document.location.href = detailUrl;
			}
			return false;
		});

		const idCustomer = $("#id_customer");
		idCustomer.on('change', function(e){
			e.preventDefault();
			sales.loadCustomerDetails(idCustomer).always(function(customer){

				const saveCashBtn = $("#submit_invoice_and_continue");
				salesInvoice.handleCashButton();
				salesInvoice.updateFormAction(saveCashBtn);

				let invoiceDate = new Date();
				const dueDate = moment(invoiceDate);
				$("#id_due_date").val(dueDate.format('YYYY-MM-DD'));

				$("#customer_full_name").html("");
				if (sales.isCashCustomer(customer)) {
					salesInvoice.handleCashButton()
				} else {
					salesInvoice.handleCreditButton()
				}

				const submitToDfEl = $("#submit_invoice_and_send_to_docuflow");
				if (submitToDfEl.length > 0) {
					submitToDfEl.hide();
					if (salesInvoice.hasDfNumber(customer)) {
						submitToDfEl.show();
					}
				}

				$('.invoice-col').matchHeight();
			});
		});

		$(".save-invoice").on("click", function(e){
			salesInvoice.saveInvoice($(this));
		});

		$("#save_cash_sales_invoice").on("click", function(e){
			let action = $(this).data('action')
			ajaxSubmitForm($(this), $("#create_sales_invoice_form"), {'action': action});
			return false;
		});

		const salesInvoiceBody = $("#invoice_items_body");
		if (salesInvoiceBody.length > 0) {
			salesInvoice.loadInvoiceItems(salesInvoiceBody, salesInvoiceBody.data("ajax-url"));
		}

		const addInvoiceItemLine = $("#add_invoice_item_line");
		addInvoiceItemLine.on('click', function(){
			$(this).prop('disabled', true).data('html', addInvoiceItemLine.text()).text('loading ...');
			let params = {'rows': 1}
			salesInvoice.loadInvoiceItems(addInvoiceItemLine, addInvoiceItemLine.data('ajax-url'), params);
		});

		$(".bulk-action").on('click', function(e){
			e.preventDefault();
			salesInvoice.bulkInvoicesAction($(this));
		});

		$(".single-action").on('click', function(e){
			e.preventDefault();
			salesInvoice.singleInvoiceAction($(this));
		});

		$(".action").on('click', function(e){
			e.preventDefault();
			salesInvoice.invoiceAction($(this));
		});

		const idDocumentDate = $("#id_invoice_date");
		idDocumentDate.on('blur', function(e){
			e.preventDefault();
			salesInvoice.calculateDueDate(idDocumentDate);
		});

		const idVatNumber = $("#id_vat_number");
		idVatNumber.on('blur', function(e){
			e.preventDefault();
			salesInvoice.updateVatNumber(idVatNumber);
		});

		const idEstimate = $("#id_estimate");
		idEstimate.on('change', function(e){
			salesInvoice.loadEstimateItems(e);
		});

		const pickUp = $("#pick_up");
		const deliveryAddress = $(".delivery-address");
		pickUp.on('change', function() {
			if ($(this).is(":checked")) {
				deliveryAddress.css({'visibility': 'hidden'});
			} else {
				deliveryAddress.css({'visibility': 'visible'});
			}
		});

		const salesItems = $(".sales-items");
		if (salesItems.length > 0) {
			salesItems.each(function(){
				salesItem.handleRow($(this).data("row-id"));
			});
		}

		$(document).on('click', '.delete-row', function(e){
            e.preventDefault();
            const rowId = $(this).data('row-id');
            const deleteAction = $(this).data("action") || "remove";
            if (deleteAction === "delete") {
                if (confirm("Are you sure you want to delete this row" )) {
                    salesItem.deleteRow(rowId);
                }
            } else {
                salesItem.deleteRow(rowId);
            }
		});

		salesInvoice.getCommentsBox();
	};

	salesInvoice.getCommentsBox = function(){
		const el = $("#add_comments");
		if (el.length) {
			$.get(el.data('ajax-url'), function(responseHtml){
				el.html(responseHtml)
			});
		}
	};

	salesInvoice.saveInvoice = function(el, clear_modals=true, params)
	{
		params = params || {}
		let isCash = salesInvoice.isCash(el);
		let action = el.data('action');
		const invoiceForm = $("#sales_invoice_form");
		const actionUrl = el.data('ajax-url');
		params['action'] = action;

		if (clear_modals) {
			clearModals();
		}

		ajaxFormInit(el, invoiceForm);

		const container = $('.modal-content')
		if (container.length > 0) {
			$(document).ajaxStart(function(event, jqxhr, settings) {
				// $.LoadingOverlay("show");
				container.LoadingOverlay('show', {
						'text': 'Processing ...'
				});
			});
			$(document).ajaxStop(function(event, jqxhr, settings) {
				$.LoadingOverlay("hide");
			});
			el.prop('disabled', true)
		}

		let xhr = invoiceForm.ajaxSubmit({
			url: actionUrl,
			target: '#loader',   // target element(s) to be updated with server response
			data: params,
			dataType: 'json',
			type: 'post',
			error: function (data, statusText, xhr) { 		// pre-submit callback
				if (data.status === 400) {
					responseNotification(data.responseJSON);
				} else {
					// handle errors
					const error = {'error': true, 'text': data.statusText};
					responseNotification(error);
				}
			},
			success: function(responseJson, statusText, xhr, $form) {
				if(responseJson.create_receipt) {
					salesInvoice.createReceipt(el, responseJson)
				} else if (responseJson.not_enough_credit) {
					salesInvoice.overrideCredit(el, responseJson)
				} else if (responseJson.error) {
					responseNotification(responseJson);
				}  else {
					responseNotification(responseJson);
				}
			}
		}).data('jqxhr').done(function(){
			// if (container.length > 0) {
			// 	container.LoadingOverlay("hide");
			// 	el.prop('disabled', false);
			// }
			el.prop('disabled', false);
		});
		return xhr;
	};

	salesInvoice.overrideCredit = function(el, response)
	{
		const params = {
			'amount': accounting.toFixed(response['amount'], 2),
			'customer_id': response.customer_id
		};
		clearModals()
		$.get(response.override_url, params, function (data) {
			$(data).modal("show");

			$("#id_override_key").on("blur", function(e){
				const el = $(this);
				const resultEl = $("#override_result");
				const overrideBtn = $("#override_continue");
				resultEl.html("validating ...");
				$.get(el.data('validate-url'), {
					override_key: el.val()
				}, function(response) {
					if (response.error) {
						resultEl.html(response.text).removeClass('text-success').addClass('text-danger');
					} else {
						resultEl.html(response.text).removeClass('text-danger').addClass('text-success');
						overrideBtn.prop('disabled', false);
						overrideBtn.on("click", function(){
							overrideBtn.prop('disabled', true);
						   	$(data).modal("hide");
						   	let params = {'is_override': true}
						   	salesInvoice.saveInvoice(el, false, params);
						});
					}
				}, 'json');
			});

			$("#save_draft").on("click", function(){
			   $(data).modal("hide");
			   let params = {'save_type': 'draft'}
			   salesInvoice.saveInvoice(el, false, params);
			});

		})
	}

	salesInvoice.createReceipt = function(el, response)
	{
		const params = {
			'amount': accounting.toFixed(response['amount'], 2),
			'customer_id': response.customer_id
		};
		clearModals();
		$.get(response.receipt_url, params, function (data) {
			$(data).modal("show");

			const paymentContinueBtn = $("#cash_invoice_payment_continue");
			const invoicePaymentMethodsAmounts = $(".invoice-payment-amount");
			const balanceEl = $("#balance");
			const idBalanceEl = $("#id_balance");
			const invoiceAmount = parseFloat(accounting.unformat($("#total_invoice_amount").val()));

			salesInvoice.calculatePaymentBalance(invoicePaymentMethodsAmounts, balanceEl, idBalanceEl, invoiceAmount, paymentContinueBtn);
			invoicePaymentMethodsAmounts.on("keyup", function() {
				salesInvoice.calculatePaymentBalance(invoicePaymentMethodsAmounts, balanceEl, idBalanceEl, invoiceAmount, paymentContinueBtn);
			});

			common.initDatePicker();

			common.initChosenSelect();

			paymentContinueBtn.on('click', function(e){
				e.preventDefault();
				paymentContinueBtn.prop('disabled', true);
				const xhr = ajaxSubmitForm(paymentContinueBtn, $("#cash_invoice_receipt_form"));
				xhr.done(function(responseJson, status, xhr){
					if (!responseJson.error) {
						$(data).modal("hide");

						salesInvoice.saveInvoice(el, false).done(function(){
							paymentContinueBtn.prop('disabled', false);
						});
					}
				})
			});
		})
	};

	salesInvoice.calculatePaymentBalance = function(invoicePaymentMethodsAmounts, balanceEl, idBalanceEl, invoiceAmount, paymentContinueBtn)
	{
		let allocatedTotal = 0;
		allocatedTotal = salesInvoice.calculateAmountAllocated(invoicePaymentMethodsAmounts);

		let balance = invoiceAmount - allocatedTotal;
		idBalanceEl.val(accounting.formatMoney(balance));
		balanceEl.html(accounting.formatMoney(balance));
		if (balance === 0) {
			paymentContinueBtn.prop("disabled", false);
		} else {
			paymentContinueBtn.prop("disabled", true);
			salesInvoice.handlePaymentMethodWithChange($(this), invoiceAmount, allocatedTotal);
		}
	};

    salesInvoice.isCash = function(el)
	{
		const isCash = el.data('is-cash');
		if (isCash === undefined || isCash === 0) {
			return false;
		}
		if (isCash === null) {
			return false;
		}
		return true;
	};

    salesInvoice.calculateAmountAllocated = function(elClass)
	{
		let allocatedTotal = 0;
		elClass.each(function(){
			const methodAmount = $(this).val();
			if (methodAmount !== undefined && methodAmount !== "") {
				const amount = parseFloat(accounting.toFixed(methodAmount, 2));
				allocatedTotal += amount;
			}
		});
		return parseFloat(accounting.toFixed(allocatedTotal, 2));
	}

    salesInvoice.handlePaymentMethodWithChange = function(el, invoiceAmount, allocatedAmount)
	{
		let linesAllocatedWithChange = 0;
		$(".invoice-payment-amount").each(function(){
			const hasChange = $(this).data('has-change')
			if (hasChange === 1) {
				const amount = accounting.toFixed($(this).val(), 2)
				if (amount > 0) {
					linesAllocatedWithChange += 1
				}
			}
		});
		if (linesAllocatedWithChange > 0) {
			const changeRowEl = $("#change_row");
			const changeInput = $("#change");
			const changeEl = $("#change_val");
			const balanceEl = $("#id_balance");
			const paymentContinueBtn = $("#cash_invoice_payment_continue")
			if (allocatedAmount > invoiceAmount) {
				 const changeAmount = allocatedAmount - invoiceAmount;
				 changeRowEl.show();
				 changeInput.val(accounting.formatMoney(changeAmount));
				 changeEl.html(accounting.formatMoney(changeAmount));
				 balanceEl.val(0);
				 paymentContinueBtn.prop("disabled", false);
				 $("#balance_row").hide();
			} else {
				 changeRowEl.hide();
				 changeInput.val(0);
				 changeEl.html('');
				 paymentContinueBtn.prop("disabled", true);
				 $("#balance_row").show();
			}
		}
	};

    salesInvoice.validateOverrideKey = function(el)
	{
		$.get(el.data('validate-url'), {
			override_key: el.val()
		}, function(response) {
		    console.log("Response")
		    console.log( response );
		}, 'json');
	};

	salesInvoice.loadEstimateItems = function(el)
	{
		const rowId = 0;
		const estimateItemsUrl = $("#id_estimate option:selected").data('estimate-url');
		const chosenWidth = $("#" + rowId + "_inventory_item_chosen").width();
		const container = $("#invoice_items_body");
		$.get(estimateItemsUrl, {
			row_id:rowId
		}, function(html) {
			container.replaceWith(html);
			salesItem.handleRow(rowId);
		});
	};

	salesInvoice.updateVatNumber = function(el)
	{
		const customerVatNumber = $("#id_customer option:selected").data('vat-number');
		const customerId = $("#id_customer option:selected").val();
		if (customerId !== undefined && customerId !== "") {
			if (el.val() !== "" && customerVatNumber !== el.val()) {
				$.post(el.data('update-vat-url'), {
					vat_number: el.val(),
					customer_id: customerId
				}, function(response){
				});
			}
		}
	};

	salesInvoice.calculateDueDate = function(el)
	{
		const invoiceDate = new Date(el.val());
		const paymentTerm = $("#id_customer option:selected").data('payment-term');
		const paymentTermDays = $("#id_customer option:selected").data('payment-days');
		const idDueDate = $("#id_due_date");
		if (paymentTerm === 1) {
			const dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
			idDueDate.val(dueDate.format('YYYY-MM-DD'));
		} else if (paymentTerm === 2) {
			const dueDate = moment(invoiceDate).add(parseInt(paymentTermDays), 'days');
			const end = dueDate.format('YYYY-MM-') + dueDate.daysInMonth();
			idDueDate.val(end);
		}
	};

	salesInvoice.loadCustomerDetails = function(el)
	{
		$.getJSON($("#id_customer option:selected").data('detail-url'), {
			module: 'sales'
		}, function(response){
            if (response.is_default) {
                salesInvoice.loadCashCustomerForm(response)
            } else {
				let isCashCustomer = response.payment_term === 3;
                salesInvoice.displayCustomerDetails(response, isCashCustomer)
            }
            initChosenSelect();
			$("#id_invoice_date").trigger('blur');

		});
	};

	salesInvoice.displayCustomerDetails = function(response, isCashCustomer)
    {
        var postalAddressHtml = [];
        var deliveryAddressHtml = [];
        if (response.hasOwnProperty("addresses")) {
        	const postalAddress = response.addresses.find((address) => !address.is_delivery)
            postalAddressHtml = salesInvoice.getAddress(postalAddress, 'postal')
			if (isCashCustomer === false) {
				if (response.available_credit) {
					postalAddressHtml.push("<div class='limit'>");
					available_amount = accounting.toFixed(response.available_amount, 0);
					postalAddressHtml.push("<div><span><strong>Available Credit</strong>:&nbsp;&nbsp;</span>" + accounting.format(available_amount) +"</div>");
					postalAddressHtml.push("<input type='hidden' name='available_credit' id='available_credit' value='" + accounting.unformat(response.available_amount) +"'></div>");
					postalAddressHtml.push("</div>")
				}
			}

			if (postalAddressHtml.length > 0) {
				$("#customer_details").html(postalAddressHtml.join(" "))
			} else {
				salesInvoice.getDisplayAddressForm([], 'postal')
			}

        	const deliveryAddresses = response.addresses.filter((address) => address.is_delivery)
			salesInvoice.getDisplayAddressForm(deliveryAddresses, 'delivery')
        }
        
        if (customer.is_line_discount && customer.line_discount) {
            $("#line_discount").val(customer.line_discount);
            $(".discount").each(function(){
                if ($(this).val() === "") {
                    $(this).val(customer.line_discount)
                }
            });
        }

        if (customer.is_invoice_discount && customer.invoice_discount) {
            $("#invoice_discount").val(customer.invoice_discount);
        }

        if (response.hasOwnProperty("vat_number")) {
            $("#id_vat_number").val(response.vat_number)
        }
        if (response.hasOwnProperty("salesman")) {
			let salesmanEl = $("#id_salesman");
            salesmanEl.val(response.salesman);
			salesmanEl.trigger("chosen:updated");
        }
        $("#submit_invoice_and_send_to_docuflow").hide();
        if (salesInvoice.hasDfNumber(response)) {
            $("#submit_invoice_and_send_to_docuflow").show();
        }

        if (response.hasOwnProperty("pricings")) {
            var html = [];
            $.each(response.pricings, function(index, pricing){
                html.push("<option value='" + pricing.id + "'>"+ pricing.name+"</option>")
            })
            $("#id_pricing").html(html.join(' '));
        }
		$("#customer_full_name").html("");
        if (isCashCustomer) {
			salesInvoice.handleCashButton()
		} else {
        	salesInvoice.handleCreditButton()
		}
        $('.invoice-col').matchHeight();
    };

	salesInvoice.hasDfNumber = function(customer)
    {
        if (customer.hasOwnProperty('df_number')  && typeof customer.df_number === 'string') {
            return (customer.df_number !== "" && customer.df_number.length > 0);
        }
        return false;
    };

	salesInvoice.getDisplayAddressForm = function(addresses, addressType, addressTypeId) {
		let addressHtml = [];
		let address;
		if (addressTypeId !== undefined) {
			addresses.map((address) => console.log(typeof address.id));
			address = addresses.find((address) => address.id === addressTypeId);

		} else {
			if (addresses.length > 0) {
				address = addresses[0];
			}
		}

		addressHtml.push("<table class='table'>")
		if (addresses.length > 1) {

			addressHtml.push("<tr>");
			addressHtml.push("<td>");
			addressHtml.push("<select id='select_delivery_address' class='form-control form-control-sm'>");
			addresses.forEach(function(addressOption, index){
				if (addressTypeId && addressTypeId === addressOption.id){
					addressHtml.push("<option value='" + addressOption.id +"' selected='selected'>"+ addressOption.address_line_1 +"</option>");
				} else {
					addressHtml.push("<option value='" + addressOption.id +"'>"+ addressOption.address_line_1 +"</option>");
				}
			});
			addressHtml.push("</select>");
			addressHtml.push("</td>");
			addressHtml.push("</tr>");
		}
		if (addressType === 'postal') {
			addressHtml.push("<tr>");
			addressHtml.push("<td>");
				addressHtml.push("<input id='customer_name' name='customer_name' type='text' class='form-control form-control-sm' placeholder='Customer Name' value='' />");
			addressHtml.push("</td>");
			addressHtml.push("</tr>");
		}
		addressHtml.push("<tr>");
		addressHtml.push("<td>");
		if (address && address.address_line_1) {
			addressHtml.push("<input name='delivery_street' type='text' class='form-control form-control-sm' placeholder='Street' value='"+address.address_line_1+"' />");
		} else {
			addressHtml.push("<input name='delivery_street' type='text' class='form-control form-control-sm' placeholder='Street'   value='' />");
		}
		addressHtml.push("</td>");
		addressHtml.push("</tr>");

		addressHtml.push("<tr>");
		addressHtml.push("<td>")
		if (address && address.city) {
			addressHtml.push("<input name='delivery_city' type='text' class='form-control form-control-sm' placeholder='City' value='"+address.city+"' />");
		} else {
			addressHtml.push("<input name='delivery_city' type='text' class='form-control form-control-sm' placeholder='City' value='' />");
		}
		addressHtml.push("</td>");
		addressHtml.push("</tr>");

		addressHtml.push("<tr>");
		addressHtml.push("<td>")
		if (address && address.province) {
			addressHtml.push("<input name='delivery_province_name' type='text' class='form-control form-control-sm' value='"+address.province+"' placeholder='Province' />");
		} else {
			addressHtml.push("<input name='delivery_province_name' type='text' class='form-control form-control-sm' value='' placeholder='Province' />");
		}
		addressHtml.push("</td>");
		addressHtml.push("</tr>");

		addressHtml.push("<tr>");
		addressHtml.push("<td>")
		if (address && address.postal_code) {
			addressHtml.push("<input name='delivery_postal_code' type='text' class='form-control form-control-sm' value='"+address.postal_code+"' placeholder='Postal Code' />");
		} else {
			addressHtml.push("<input name='delivery_postal_code' type='text' class='form-control form-control-sm' value='' placeholder='Postal Code' />");
		}
		addressHtml.push("</td>");
		addressHtml.push("</tr>");

		addressHtml.push("<tr>");
		addressHtml.push("<td>")
		if (address && address.country) {
			addressHtml.push("<input name='delivery_country_name' type='text' class='form-control form-control-sm' value='"+address.country+"' />");
		} else {
			addressHtml.push("<input name='delivery_country_name' type='text' class='form-control form-control-sm' value='' placeholder='Country' />");
		}
		addressHtml.push("</td>")
		addressHtml.push("</tr>");
		addressHtml.push("</table>");
		if (addressType === 'delivery') {
			$("#shipping_to").html(addressHtml.join(" "))
			$("#select_delivery_address").on("change", function(){
				salesInvoice.getDisplayAddressForm(addresses, addressType, +$(this).val())
			});
		} else if (addressType === 'postal') {
			$("#customer_details").html(addressHtml.join(" "));
		}
		return addressHtml;
	};

	salesInvoice.getAddress = function(address) {
		let addressHtml = [];
		if (address) {
			addressHtml.push("<table class='table'>")
			if (address.address_line_1) {
				addressHtml.push("<tr>")
				addressHtml.push("<td>"+address.address_line_1+"</td>");
				addressHtml.push("</tr>")
			}
			if (address.city) {
				addressHtml.push("<tr>")
				addressHtml.push("<td>"+address.city+"</td>");
				addressHtml.push("</tr>")
			}
			if (address.province) {
				addressHtml.push("<tr>")
				addressHtml.push("<td>"+address.province+"</td>");
				addressHtml.push("</tr>")
			}
			if (address.country) {
				addressHtml.push("<tr>")
				addressHtml.push("<td>"+address.country+"</td>");
				addressHtml.push("</tr>")
			}
			if (address.postal_code) {
				addressHtml.push("<tr>")
				addressHtml.push("<td>"+address.postal_code+"</td>");
				addressHtml.push("</tr>")
			}
			addressHtml.push("</table>")
		}
		return addressHtml
	};

	salesInvoice.handleCashButton = function(){
    	let saveBtn = $("#submit_invoice_and_continue");
		saveBtn.attr('data-action-url', saveBtn.data('cash-action-url'));
		saveBtn.attr('data-is-cash', 1);

    	let saveWithDfBtn = $("#submit_invoice_and_send_to_docuflow");
    	saveWithDfBtn.attr('data-action-url', saveBtn.data('cash-action-url'));

		salesInvoice.updateFormAction(saveBtn, true);
	};

	salesInvoice.handleCreditButton = function(){
    	let saveBtn = $("#submit_invoice_and_continue");
    	saveBtn.attr('data-action-url', saveBtn.data('credit-action-url'));
		saveBtn.attr('data-is-cash', 0);

    	let saveWithDfBtn = $("#submit_invoice_and_send_to_docuflow");
    	saveWithDfBtn.attr('data-action-url', saveBtn.data('credit-action-url'));

		salesInvoice.updateFormAction(saveBtn);
	};

    salesInvoice.loadCashCustomerForm = function(customerData)
    {
    	$("#id_vat_number").val("");
		// $("#customer_details").html(customerData['customer_form']);
    	// $("#shipping_to").html(customerData['delivery_form']);

		salesInvoice.getDisplayAddressForm([], 'postal');
		salesInvoice.getDisplayAddressForm([], 'delivery');

    	var saveCashBtn = $("#submit_invoice_and_continue");
		salesInvoice.handleCashButton();
		salesInvoice.updateFormAction(saveCashBtn);
		let invoiceDate = new Date()
		var dueDate = moment(invoiceDate);
		$("#id_due_date").val(dueDate.format('YYYY-MM-DD'));
    };

    salesInvoice.updateFormAction = function(el, isCash)
	{
		let actionUrl = el.data('action-url');
		if (isCash) {
			actionUrl = el.data('cash-action-url');
		} else if(isCash === undefined || isCash === false) {
			actionUrl = el.data('credit-action-url');
		}
		$("#sales_invoice_form").attr('action', actionUrl);
	};

	salesInvoice.getSelectedInvoices = function()
	{
		var invoices = [];
		$(".invoice-action").each(function(){
			if ($(this).is(":checked")) {
				invoices.push($(this).data('invoice-id'));
			}
		});
		return invoices;
	};

	salesInvoice.bulkInvoicesAction = function(el)
	{
		var invoices = Sales.getSelectedInvoices();
		var totalInvoices = invoices.length;
		if (totalInvoices === 0) {
			alert('Please select at lease one invoice');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( invoices );
			$.post(ajaxUrl, {
				invoices: invoices
			}, function(response){
				responseNotification(response);
			});
		}
	};

	salesInvoice.singleInvoiceAction = function(el)
	{
		var invoices = Sales.getSelectedInvoices();
		var totalInvoices = invoices.length;
		if (totalInvoices === 0) {
			alert('Please select at lease one invoice');
		} else if (totalInvoices >1) {
			alert('Please select one invoice to action');
		} else {
			var ajaxUrl = el.data('ajax-url');
			console.log( invoices );
			$.post(ajaxUrl, {
				invoices: invoices
			}, function(response){
				responseNotification(response);
			});
		}
	};

	salesInvoice.invoiceAction = function(el)
	{
		var ajaxUrl = el.data('ajax-url');
		$.post(ajaxUrl, function(response){
			responseNotification(response);
		});
	};

	salesInvoice.reloadInvoiceItemRow = function(el)
	{
		const rowId = el.data('row-id');
		let params = {'rows': 1, 'reload': 1, 'row_id': rowId, 'line_type': $("#type_"+ rowId +" option:selected").val()}
		$.get(el.data('ajax-url'), params, function(html) {
			$("#sales_item_" + rowId).replaceWith(html);

			salesItem.handleRow(rowId);

			$(".line-type").on("change", function(e){
				salesInvoice.reloadInvoiceItemRow($(this));
        	});
		});

	};

	salesInvoice.loadInvoiceItems = function(el, ajaxUrl, params)
	{
		let rowId = 0;
		const lastRow = $(".invoice_item_row").last();
		if (lastRow.length > 0) {
			rowId = parseInt(lastRow.data('row-id')) + 1;
		}
		salesInvoice.loadInvoiceItemRow(el, ajaxUrl, rowId, lastRow, params)
	};

	salesInvoice.isLineCompleted = function(rowId)
    {
        const inventory = $("#inventory_item_" + rowId).val();
        const ordered = $("#ordered_" + rowId).val();
        const price = $("#price_" + rowId).val();

        if (inventory === '') {
            return false;
        }
        if (ordered === '') {
            return false;
        }
        if (price === '') {
            return false;
        }
        return true;
    };

	salesInvoice.loadInvoiceItemRow = function(el, ajaxUrl, rowId, lastRow, params)
	{
		const container = $("#invoice_items_placeholder");
		params = params || {}
		params['row_id'] = rowId
		params['invoice_type_id'] = $("#invoice_type_id").val();
		$.get(ajaxUrl, params, function(html) {
			if (lastRow !== undefined && lastRow.length > 0) {
				lastRow.after(html)
			} else {
				container.replaceWith(html);
			}
			const _lastRow = $(".invoice_item_row").last();
			if (_lastRow.length > 0) {
				rowId = parseInt(_lastRow.data('row-id'));
			}

			$(".sales-items").each(function(){
				salesItem.handleRow($(this).data("row-id"));
			});

			$(".line-type").on("change", function(e){
				salesInvoice.reloadInvoiceItemRow($(this));
        	});

			el.prop('disabled', false).text(el.data('html'));
		});
	};

	salesInvoice.init();

})();