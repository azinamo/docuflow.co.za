(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var message = window.Docuflow.Message || {};
    var form = window.Docuflow.Form || {};
    var ObjectPosting = window.Docuflow.ObjectPosting = {};

    ObjectPosting.init = function()
	{
		$(document).on("click", "[data-modal]", function (event) {
			event.preventDefault();
			$.get($(this).data("modal"), function (data) {
				$(data).modal("show");

				$("#save_object_posting_item").on("click", function(e){
					e.preventDefault()
					$(this).prop('disabled', true);
					ObjectPosting.saveObjectPostingItem($(this));
				});

				$(".chosen-select").chosen();

			});
		});


		$(".save-object-posting").on("click", function(e){
			e.preventDefault();
			form.submit($(this), $("#estimate_form"), {next: $(this).data("action")});
		});


		const objectPostingItemsBody = $("#object_posting_items");
		if (objectPostingItemsBody.length > 0) {
			ObjectPosting.loadObjectPostingItems(objectPostingItemsBody, objectPostingItemsBody.data("ajax-url"));
		}
	};

	ObjectPosting.loadObjectPostingItems = function(el, ajaxUrl)
	{
		ajaxUrl = ajaxUrl || el.data('ajax-url');
		$.get(ajaxUrl, function(html) {
			el.html(html);
		})
	};

	ObjectPosting.saveObjectPostingItem = function(el)
	{
		const element = jQuery("#loader");
		const result = jQuery("#result");
		el.prop('disabled', false);
		element.show();
		jQuery("#object_item_posting_form").ajaxSubmit({
			dataType : 'json',
			success: function(response, statusText, xhr, $form)  {
				element.hide();
				if (response.error) {
					result.html(response.text ).addClass('alert alert-danger');
				} else {
					result.html(response.text ).addClass('alert alert-success');
					document.location.reload();
				}
			}
		});
	};


    ObjectPosting.init();

})();