(function(){

    window.Docuflow = window.Docuflow || {};
    var common = window.Docuflow.Common || {};
    var form = window.Docuflow.Form || {};
    var customer = window.Docuflow.Customer = {};

    customer.init = function()
    {
        $("#customers>tbody>tr>td:not(.actions)").on("click", function(){
            var detailUrl = $(this).parent("tr").data("detail-url");
            document.location.href = detailUrl;
            return false;
        });

        $("#save_customer").on("click", function(e){
            form.submit($(this), $("#customer_form"));
            return false;
        });
    };

    customer.loadDetails = function(el)
    {
        console.log("-------- Sales Invoice Customer ------------")
        var ajaxUrl = $("#id_customer option:selected").data("detail-url");
        $.getJSON(ajaxUrl, {
            module: 'sales'
        }, function(response){
            console.log( response )
            console.log( response.is_default )
            if (response.is_default) {
                console.log("load customer form")
                customer.loadCashCustomerForm(response)
            } else {
                console.log("load customer details")
                customer.loadCustomerDetails(response)
            }
        });
    };                                                                                                                                          

    customer.loadCustomerDetails = function(response)
    {
        var postalAddressHtml = [];
        var deliveryAddressHtml = [];
        if (response.hasOwnProperty("postal")) {
            var postalAddress;
            console.log(typeof response.postal);
            if (typeof response.postal === "string") {
                postalAddress = JSON.parse(response.postal);
            } else {
                postalAddress = response.postal;
            }
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.address+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.city+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.province+"</p>");
            postalAddressHtml.push("<p class='mb-1'>"+postalAddress.country+"</p>");
        }
        if (response.hasOwnProperty("delivery")) {
            // var deliveryAddress;
            // console.log(typeof response.delivery)
            // if (typeof response.delivery === "string") {
            //     deliveryAddress = JSON.parse(response.delivery);
            // } else {
            //     deliveryAddress = response.delivery;
            // }
            // deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.address+"</p>");
            // deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.city+"</p>");
            // deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.province+"</p>");
            // deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.country+"</p>");
        }

        if (customer.is_line_discount && customer.line_discount) {
            $("#line_discount").val(customer.line_discount);
            $(".discount").each(function(){
                if ($(this).val() === "") {
                    $(this).val(customer.line_discount)
                }
            });
        }
        if (customer.is_invoice_discount && customer.invoice_discount) {
            $("#invoice_discount").val(customer.invoice_discount);
        }

        if (customer.hasDfNumber(response)) {
            $("#submit_estimate_and_send_to_df").show();
        }
        if (postalAddressHtml.length > 0) {
            $("#customer_details").html(postalAddressHtml.join(" "))
        }
        if (deliveryAddressHtml.length > 0) {
            var isPickUp = $("#id_is_pick_up");
            if (isPickUp.length) {
               if (isPickUp.is(":checked")) {
                   console.log("Show pickup")
                   $("#submit_estimate_with_picking").hide();
                   $("#submit_estimate_with_delivery").hide();
                   $("#shipping_to").html(deliveryAddressHtml.join(" ")).hide()
                   $("#delivery_is_pick_up").show();
               }  else {
                   console.log("hide pickup")
                   $("#shipping_to").html(deliveryAddressHtml.join(" ")).show()
                   $("#delivery_is_pick_up").hide();
               }
            } else {
                console.log("2 Hide pickup")
               $("#shipping_to").html(deliveryAddressHtml.join(" "))
                $("#delivery_is_pick_up").hide();
            }
        }
        if (response.hasOwnProperty("vat_number")) {
            $("#id_vat_number").val(response.vat_number)
            $("#id_customer_vat_number").val(response.vat_number)
        }
        if (response.hasOwnProperty("salesman_id")) {
			let salesmanEl = $("#id_salesman");
            salesmanEl.val(response.salesman_id);
			salesmanEl.trigger("chosen:updated");
        }
        if (response.hasOwnProperty("phone_number")) {
			let telephoneEl = $("#id_telephone");
            telephoneEl.val(response.phone_number);
        }

        if (response.hasOwnProperty("pricings")) {
            var html = [];
            $.each(response.pricings, function(index, pricing){
                console.log( pricing )
                html.push("<option value='" + pricing.id + "'>"+ pricing.name+"</option>")
            })
            $("#id_pricing").html(html.join(' '));
        }
        $("#customer_full_name").html("");
        $(".cash").hide();
        $(".customer").show();
        $(".save-cash-invoice").hide();
        var saveBtn = $("#submit_invoice_and_continue");
        saveBtn.show();
    };

    customer.hasDfNumber = function(response)
    {
        if (response.hasOwnProperty('df_number')) {
            console.log("Has df number ", (response.df_number !== "" && response.df_number.length > 0), ' => ', response.df_number);
            if (response.df_number !== "" && response.df_number.length > 0) {
                return true;
            }
        }
        return false;
    };

    customer.loadCashCustomerForm = function(customerData)
    {
        $("#id_vat_number").val("");
        $("#customer_details").html(customerData['customer_form']);
        var shippingTo = $("#shipping_to");
        shippingTo.html(customerData['delivery_form']);
        var isPickUp = $("#id_is_pick_up");
        if (isPickUp.length) {
           if (isPickUp.is(":checked")) {
               shippingTo.hide();
               $("#delivery_is_pick_up").show();
               $("#submit_estimate_with_picking").hide();
               $("#submit_estimate_with_delivery").hide();
           }  else {
               $("#delivery_is_pick_up").hide();
               shippingTo.show();
           }
        } else {
            $("#delivery_is_pick_up").hide();
           shippingTo.show();
        }
        var saveCashBtn = $("#submit_cash_invoice_and_continue");
        saveCashBtn.show();
        $(".cash").show();
        $(".customer").hide();
        $(".save-invoice").hide();
    };


    customer.init();

})();