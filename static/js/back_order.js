$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");

            initDatePicker();

			initChosenSelect();

        });
    });

	$(".bulk-action").on('click', function(e){
		alert("Started")
		e.preventDefault();
		BackOrder.bulkBackOrdersAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		BackOrder.singleBackOrderAction($(this));
	});

});

var BackOrder = (function(estimate, $){

	return {

		bulkBackOrdersAction: function(el)
		{
			const backOrders = BackOrder.getSelectedBackOrders();
			const totalBackOrders = backOrders.length;
			console.log("Back order _> ", totalBackOrders)
			console.log(backOrders)
			if (totalBackOrders === 0) {
				alert('Please select at least one back order');
			} else {
				const confirm = el.data('confirm');
				if (confirm === '1') {
					const confirmText = el.data('confirm-text')
					return confirm(confirmText)
				}
				console.log( backOrders )
				console.log( el.data('ajax-url') )
				$.get(el.data('ajax-url'), {
					back_orders: backOrders,
					category: el.attr('id')
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singleBackOrderAction: function(el)
		{
			const back_order = BackOrder.getSelectedBackOrders();
			const totalBackOrders = back_order.length;
			if (totalBackOrders === 0) {
				alert('Please select at least one back order');
			} else if (totalBackOrders > 1) {
				alert('Please select one back order to action');
			} else {
				console.log( totalBackOrders )
				console.log( el.data('ajax-url') )
				$.get(el.data('ajax-url'), {
					back_order_id: back_order[0],
					category: el.attr('id')
				}, function(response){
					responseNotification(response);
				});
			}
		},

		getSelectedBackOrders: function()
		{
			const back_orders = [];
			$(".back-order-action").each(function(){
				if ($(this).is(":checked")) {
                    back_orders.push($(this).data('back-order-id'));
                }
			});
			return back_orders;
		},

		actionBackOrder: function(el)
		{
			$.getJSON(el.data('ajax-url'), function(response){
				responseNotification(response)
			});
		}

	}
}(BackOrder || {}, jQuery))

