$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save_update").on('click', function (event) {
            	event.preventDefault();
                ajaxSubmitForm($(this), $("#create_depreciation_update"));
            });

            initDatePicker();

			initChosenSelect();

        });
    });

});


var DepreciationUpdate = (function(depreciationUpdate, $){

	return {

		createUpdate: function(el) {
		   let self = $("#" + el.attr('id'));
		   const text = el.text();
		   self.html("Processing ...");
		   $.get(el.data('ajax-url'), {
			 period: $("#period").val()
		   }, function(response){
		   	   if (response.error) {
		   	   	  self.html(text);
			   }
			   responseNotification(response);
		   }, 'json');
		},


	}
}(DepreciationUpdate || {}, jQuery));

