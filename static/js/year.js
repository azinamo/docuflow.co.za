$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			$("#create_month_close_submit").on('click', function (event) {
                event.preventDefault();
                const form = $($(this).parent()).parent()
                submitModalForm(form);
            });
			$(".chosen-select").chosen();

        });
    });

    const processYearStages = $("#process_year_in_stages");
    if (processYearStages.length > 0) {
        Year.processYearStages(processYearStages)
    }

    $(document).on("click", ".year-status", function(e){
        e.preventDefault();
        $.getJSON($(this).data('ajax-url'), function(response){
            responseNotification(response)
        });
    });

});


var Year = (function(year, $){

	return {

	    updateCurrentActiveYear(el)
        {
            $.post($(el).data('ajax-url'), {
                is_default: true,
                csrfmiddlewaretoken: window.csrftoken
            }, function(response){
                responseNotification(response)
            }, 'json');
        },

	    processYearStages(el) {
            Year.processStage(el,0);
        },

        processStage(el, counter) {
	        const processYear = $(".process-year");
            const stage = $(processYear[counter])
            stage.html('Processing ...').addClass('text-info').removeClass('text-danger');
            $.getJSON(el.data('ajax-url'), {
                stage: stage.attr('id')
            }, function(response) {
                if (stage.attr('id') === 'done') {
                    responseNotification(response);
                    stage.html('')
                    setTimeout(function(){
                        document.location.href = response['home']
                    }, 5000);
                } else {
                    stage.html(response.text).addClass('text-success').removeClass('text-info').removeClass('text-danger');
                    counter = counter + 1
                    if (counter < processYear.length) {
                        Year.processStage(el, counter);
                    }
                }
            });
        }

	}

}(Year || {}, jQuery))

