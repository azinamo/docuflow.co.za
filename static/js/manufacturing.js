$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
			initChosenSelect();
        });
    });

	$("#manufacturing>tr>td:not(.actions)").on("click", function(){
		document.location.href = $(this).parent("tr").data("detail-url");
		return false;
	});

	$("#submit_manufacturing").on("click", function(e){
		const manufacturingForm = $("#manufacturing_form");
		manufacturingForm.LoadingOverlay('show', {'text': 'Processing ...'});

		ajaxSubmitForm($(this), manufacturingForm);
		return false;
	});

	const recipeItemsBody = $("#manufacturing_recipe_items");
	if (recipeItemsBody.length > 0) {
		Manufacturing.loadRecipeRow(recipeItemsBody);
	}

	const addRecipeLine = $("#add_inventory_recipe_line");
	addRecipeLine.on('click', function(){
		$(this).prop('disabled', true).data('html', addRecipeLine.text()).text('loading ...')
		Manufacturing.loadRecipeRow(addRecipeLine);
	});


	$(".remove-recipe-inventory-item").on("click", function (e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		Manufacturing.removeInventoryItem($(this));
    });
});

var Manufacturing = (function(manufacturing, $){

	return {

		removeInventoryItem: function(el)
		{
			$("#row_" + el.data("row-id")).remove();
		},

		loadRecipeRow: function(el)
		{
			let recipeRowClass = $(".recipe_item_row");
			const container = $("#recipes__placeholder");
			const lastRow = recipeRowClass.last();
			let rowId = 0;
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(el.data('ajax-url'), {
				row_id:rowId
			}, function(html) {
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				recipeRowClass = $(".recipe_item_row");
				const _lastRow = Manufacturing.getLastRow(recipeRowClass, lastRow);

				recipeRowClass.each(function(){
					Manufacturing.handleRow($(this).data("row-id"));
				});
				el.prop('disabled', false).text(el.data('html'));
			});
		},

		getLastRow: function(rowClass, lastRow)
		{
			const _lastRow = rowClass.last();
			if (_lastRow.length > 0) {
				return parseInt(_lastRow.data('row-id'));
			}
			return lastRow;
		},

		handleRow: function(rowId)
		{
			console.log("_----------------- handleRow --------------------");
			const chosenWidth = $("#"+ rowId + "_recipe_item_chosen").width();
			$("#"+ rowId +"_recipe_item").on('change', function(){
				Manufacturing.handleRecipe(rowId);
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				Manufacturing.handleRecipe(rowId);
			});

			if (chosenWidth > 0) {
				$(".chosen-select").chosen({'width': chosenWidth + "px"});
			} else {
				$(".chosen-select").chosen({'width': '143px'});
			}
		},

		handleSelectedRecipe: function(el, rowId)
		{
			const selectedOption = $("#"+ rowId +"_recipe_item option:selected");
			const price = selectedOption.data('price');
			$("#price_" + rowId ).val( price );
			const recipeItemsUrl = selectedOption.data('ajax-recipe-inventory-url');
			Manufacturing.loadRecipeItems(el.val(), rowId, recipeItemsUrl);
		},

		loadRecipeItems: function(recipeId, rowId, recipeItemsUrl)
		{
			$.get(recipeItemsUrl, function(html){
				$("#recipe_details_" + rowId).html(html);
			});
		},

		handleRecipe: function(rowId)
		{
			const selectedOption = $("#" + rowId + "_recipe_item option:selected");
			const recipeUrl = selectedOption.data('ajax-recipe-url');
			const costingUrl = selectedOption.data('ajax-cost-url');
			const recipeId = selectedOption.val();

			$.get(recipeUrl, function(recipeItems){
				const quantity = $("#quantity_"+ rowId +"").val();
				const params = Manufacturing.getBreakdown(quantity, recipeItems);
				params['quantity'] = quantity
				$.get(costingUrl, params, function(responseHtml) {
					$("#recipe_details_" + rowId).html(responseHtml);
				});
			});
		},

		getBreakdown: function(quantity, recipeItems)
		{
			var recipeRecipeItems = {};
			recipeItems.map(function(recipeItem){
				var result = Manufacturing.convertQuantity(quantity, recipeItem.item_unit_code, recipeItem.stock_unit_code);
				recipeRecipeItems[recipeItem.id] = result;
			});
			return recipeRecipeItems;
		},

		validateInventoryQuantity: function(el, recipeItemId, rowId)
		{
			var ajaxUrl = el.data('ajax-url');
			var quantity = el.val();
			if (recipeItemId !== '' && quantity !== '') {
				var params = Manufacturing.convertAndCalculateQuantities(quantity, recipeItemId, rowId);

				params['recipe_id'] = recipeItemId;
				$.getJSON(ajaxUrl, params, function(response){
				    console.log(response);
				    responseNotification(response);
				    if (!response.error) {
						Manufacturing.buildCostHtml(response);
					}
				});
			}
		},

        buildCostHtml: function(response, rowId, recipeItems)
        {
			console.log( "-------- buildCostHtml(" + rowId + ") --------- ");
			var html = [];
			html.push("<table class='table' id='recipe_inventory_items_"+ rowId +"'>");
			html.push("<thead>");
			html.push("<tr>");
				html.push("<th>Inventory</th>");
				html.push("<th class='text-right'>Quantity</th>");;
				html.push("<th>Unit</th>");
				html.push("<th>Control Unit</th>");
				html.push("<th class='text-right'>Quantity to be used</th>");
				html.push("<th class='text-right'>Unit Cost</th>");
				html.push("<th class='text-right'>Total Cost</th>");
			html.push("</tr>");
			html.push("</thead>");
			html.push("<tbody>");
			response['recipe_items'].map(function(recipeItem){
				html.push("<tr>")
					html.push("<td>" + recipeItem.inventory + " (" + recipeItem.received + ")</td>");
					html.push("<td class='text-right'>" + recipeItem.item_quantity + "</td>");
					html.push("<td>" + recipeItem.item_unit + "</td>");
					html.push("<td>" + recipeItem.sale_unit_code + "</td>");
					html.push("<td class='text-right'>" + recipeItem.quantity + "</td>");
					html.push("<td class='text-right'>" + recipeItem.unit_price + "</td>");
					html.push("<td class='text-right'>" + recipeItem.total_cost + "</td>");
				html.push("</tr>")
			});
			html.push("</tbody>");
			html.push("<tfoot>");
				html.push("<tr>");
					html.push("<th></th>");
					html.push("<th class='text-right'></th>");
					html.push("<th></th>");
					html.push("<th></th>");
					html.push("<th></th>");
					html.push("<th class='text-right'> " + response.unit_cost + "</th>");
					html.push("<th class='text-right'>" + accounting.formatMoney(response.total_cost) + "</th>");
				html.push("</tr>");
			html.push("</tfoot>");
			html.push("</table>");
			$.each(recipeItems, function(_recipeItemId, _recipeItem){
				if (_recipeItem.hasOwnProperty('rate')) {
					html.push("<input type='hidden' name='recipe_"+ _recipeItemId +"_inventory_item_"+ _recipeItemId +"_rate' value='"+_recipeItem.rate+"' />");
					html.push("<input type='hidden' name='recipe_"+ _recipeItemId +"_inventory_item_"+ _recipeItemId +"_quantity' value='"+_recipeItem.value+"' />");
				}
			});

			$("#recipe_details_" + rowId).html( html.join(' '));
        },

		convertAndCalculateQuantities: function(quantity, recipeId, rowId)
		{
			var manufactureQuantities = {};
			console.log("-------------------- convertAndCalculateQuantities --------------------" + quantity + " ---  " + recipeId + " ---- " + rowId)
			console.log("There are ", $(".recipe_qty_" + rowId + "_" + recipeId).length + " lines for recipes")
			$(".recipe_qty_" + rowId + "_" + recipeId).each(function(){
				var inventoryItemRowId =  $(this).data('row-id');
				var inventoryItemQty = parseFloat($(this).val());
				var totalInventoryQty = inventoryItemQty * quantity;
				var recipeInventoryItemUnit = $("#row_" + rowId + "_inventory_recipe_unit_" + inventoryItemRowId).val();
				var inventoryControlUnit = $("#row_" + rowId + "_inventory_recipe_sale_unit_" + inventoryItemRowId).val();

				var result = Manufacturing.convertQuantity(totalInventoryQty, recipeInventoryItemUnit, inventoryControlUnit);
				console.log("Result for ", rowId + " receipe id ", recipeId)
				console.log(result)
				console.log("Inventory item " + inventoryItemRowId + " requires " + inventoryItemQty + " = totalling to  " + totalInventoryQty + "(" + recipeInventoryItemUnit+ ") which is " + result['value'] + "(" + inventoryControlUnit + ")" );
				$("#row_" + rowId + "_quantity_used_" + inventoryItemRowId).html(accounting.toFixed(result['value'], 4) + " ");
				$("#row_" + rowId + "_inventory_recipe_qty_used_" + inventoryItemRowId).val(accounting.toFixed(result['value'], 4));
				$("#rate_" + rowId + "_" + inventoryItemRowId).val(accounting.toFixed(result['rate'], 4));
				manufactureQuantities["quantities_" + inventoryItemRowId] = result['value']
			});
			return manufactureQuantities;
		},

		convertQuantity: function(quantity, fromUnit, toUnit)
		{
			try {
				if (fromUnit !== toUnit) {
					var convert = require('convert-units');

					var rate = convert(1).from(fromUnit).to(toUnit);
					var result = convert(quantity).from(fromUnit).to(toUnit);
					console.log("\n");
					console.log( "Converted quantity " + quantity + " from " + fromUnit + " to " + toUnit + " to give " + result );
					console.log("\r\n\n");
					return {'rate': rate, 'value': result}
				} else {
					return {'rate': 1, 'value': quantity}
				}
			} catch (e) {
				return {'rate': 1, 'value': quantity}
			}
		}
	}
}(Manufacturing || {}, jQuery))

