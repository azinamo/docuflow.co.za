$(function(){

	$(document).on('click', '#save_vat_report', function(e){
	    e.preventDefault();
        VatReport.saveReport($(this))
	});

    $(document).on('click', '#create_vat_report_submit', function(event){
        event.preventDefault();
        $(this).submitButton('loading');
        submitModalForm($("form[name='create_vat_report']"));
    });

});


var VatReport = (function(vat_report, $){

	return {

	    saveReport: function(el)
        {
            el.submitButton("loading");
            $.getJSON(el.data('ajax-url'), function(response){
               responseNotification(response)
               el.submitButton("reset");
            });
        }
	}

}(VatReport || {}, jQuery))