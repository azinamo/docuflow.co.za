$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
			initChosenSelect();
        });
    });

	$("#recipes>tr>td:not(.actions)").on("click", function(){
		document.location.href = $(this).parent("tr").data("detail-url");
		return false;
	});

	$("#submit_recipe").on('click', function(e){
		ajaxSubmitForm($(this), $("#recipe_form"));
		return false;
	});

	const inventoryItemsBody = $("#recipe_inventory_items");
	if (inventoryItemsBody.length > 0) {
		Recipe.loadRecipeItems(inventoryItemsBody, inventoryItemsBody.data('ajax-url'));
	}

	const addInventoryRecipeLine = $("#add_inventory_recipe_line");
	addInventoryRecipeLine.on('click', function() {
		$(this).prop('disabled', true).data('html', addInventoryRecipeLine.text()).text('loading ...')
		Recipe.loadRecipeItems(addInventoryRecipeLine, addInventoryRecipeLine.data('ajax-url'));
	});

});

var Recipe = (function(recipe, $){

	return {

		removeInventoryItem: function(el)
		{
			$(this).prop('disabled', true).data('html', el.text()).text('loading ...');
			const rowId = el.data('row-id');
			$.get(el.data('ajax-url'), function(response){
				responseNotification(response);
				if (!response.error) {
					$("#row_" + rowId).remove();
				}
			});
		},

		loadRecipeItems: function(el, ajaxUrl)
		{
			const container = $("#recipe_inventory_placeholder");
			const lastRow = $(".inventory_item_row").last();
			let rowId = 0;
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				let _lastRow = $(".inventory_item_row").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}

				$(".inventory_item_row").each(function(){
					Recipe.handleRow($(this).data("row-id"));
				});

				el.prop('disabled', false).text(el.data('html'));
			});
		},

		handleRow: function(rowId)
		{
			let chosenWidth = calculateChosenSelectWidth("inventory_item_row");
			const inventorySelect = $("#"+ rowId +"_inventory_item");
			const inventoryUrl = inventorySelect.data("ajax-inventory-url");
			inventorySelect.ajaxChosen({
				url: inventorySelect.data("ajax-url"),
				dataType: "json",
				width: chosenWidth + "px",
				allowClear: true,
				placeholder: "Select ..."
			}, {loadingImg: '/static/js/vendor/chosen/loading.gif'}).width(chosenWidth + "px");

			inventorySelect.on('change', function(e){
				$.get(inventoryUrl, {
					inventory_id: $(this).val()
				}, function(inventoryData){
					const price = inventoryData.selling_price;
					const measureId = inventoryData.measure.id;
					const unitId = inventoryData.unit.id;
					$("#price_" + rowId ).val(price);
					if (measureId !== undefined && measureId !== '') {
						Recipe.loadUnits(rowId, measureId, inventoryData.measure.units_url, unitId);
					}
				});
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				// Recipe.calculateInventoryItemPrices(rowId);
			});

			$('.remove-recipe-inventory-item').on('click', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				Recipe.removeInventoryItem($(this));
			});
		},

		loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId === item.id) {
						html.push("<option value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#unit_" + rowId).html(html.join('')).trigger("chosen:updated");
			});
		},

		loadSupplierItems: function(el)
		{
			const idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(el.data('items-ajax-url'), {
				supplier_id: el.val()
			}, function(response){
				const html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					const optionSelected = $("#id_purchase_order  option:selected");
					Inventory.displayPurchaseOrderInventoryItems(optionSelected.data('ajax-url'));
                });
			});
		}
	}
}(Recipe || {}, jQuery))


