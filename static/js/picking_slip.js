$(document).ready(function() {

	window.Docuflow = window.Docuflow || {};
	var common = window.Docuflow.Common || {};
	var modal = window.Docuflow.Modal || {};
	var sales = window.Docuflow.Sales || {};
	var salesItem = window.Docuflow.SalesItem || {};

	$("#id_is_pick_up").on("change", function(){
		if ($(this).is(":checked")) {
			$("#delivery_to").hide();
			$("#delivery_is_pick_up").show();
		} else {
			$("#delivery_to").show();
			$("#delivery_is_pick_up").hide();
		}
	});

	$(".picked").on("blur", function(){
		PickingSlip.validateQuantity($(this));
	});

	$(".quantity").on("blur", function(e){
		PickingSlip.handleQty($(this));
	});

	$(".back-order").on("change", function(e){
		PickingSlip.calculateTotalForBackOrder();
	});

	$(".create-picking").on("change", function(e){
		PickingSlip.calculateTotalForBackOrder();
	});

	$("#save_picking_slip").on("click", function(e){
		e.preventDefault();
		ajaxSubmitForm($(this), $("#picking_slip_form"));
	});

	$("#save_delivery_note").on("click", function(){
		ajaxSubmitForm($(this), $("#delivery_note_form"));
	});

	$(".action-picking_slip").on('click', function (e) {
		e.preventDefault();
		PickingSlip.singlePickingSlipAction($(this), [$(this).data('picking-slip-id')]);
    });

	const pickingSlipItems = $(".sales-items");
	if (pickingSlipItems.length > 0) {
		pickingSlipItems.each(function(){
			const rowId = $(this).data('row-id');
			if (rowId !== undefined) {
				salesItem.handleRow(rowId);
			}
		});
	}

	PickingSlip.calculateTotalQuantity();
	// PickingSlip.calculateTotalNetWeight();
	// PickingSlip.calculateTotalGrossWeight();
	PickingSlip.calculateTotalForBackOrder();
	PickingSlip.calculateTotalForPicking();

});


var PickingSlip = (function(pickingSlip, $){

	return {

        validateQuantity: function (el) {
            const rowId = el.data("row-id");
            const quantity = accounting.unformat($("#received_" + rowId).val());
            const picked = accounting.unformat(el.val());
            const backOrderEl = $("#back_order_" + rowId);
			// PickingSlip.handleQty(el, rowId, quantity, picked);
            if (picked >= quantity) {
                el.val(accounting.formatMoney(picked));
                backOrderEl.prop("disabled", true).prop("checked", false);
            } else if (picked < quantity) {
				$("#quantity_" + rowId).val(picked);
				backOrderEl.prop("checked", true).prop("disabled", false);
            }
        },

		handleQty: function(el)
		{
			const rowId = el.data("row-id");
			const orderedQty = accounting.unformat($("#received_" + rowId).val());
			const availableQty = accounting.unformat($("#in_stock_" + rowId).val());
			const pickingBalance = accounting.unformat($("#picking_balance_" + rowId).val());
			const backOrderEl = $("#back_order_" + rowId);
			let quantity = accounting.unformat(el.val());

			$("#picked_" + rowId).val(quantity);
            if (quantity > availableQty) {
                el.val(accounting.formatMoney(availableQty));
                backOrderEl.prop("disabled", true);
            } else if(quantity > orderedQty) {
            	el.val(accounting.formatMoney(availableQty));
                backOrderEl.prop("disabled", true);
			} else {
            	if (pickingBalance > 0) {
            		backOrderEl.prop("checked", true).prop("disabled", false);
				} else if (quantity <= availableQty) {
                    backOrderEl.prop("checked", false).prop("disabled", false);
                }
            	if (quantity === 0) {
					$("#is_picked_" + rowId).prop("checked", false).prop("disabled", true);
				} else {
            		$("#is_picked_" + rowId).prop("checked", false).prop("disabled", false);
				}
            }
            quantity = accounting.unformat(el.val());
            const balance = orderedQty - quantity;
            if (balance > 0) {
				$("#balance_picking_" + rowId).html(accounting.formatMoney(balance));
				$("#picking_balance_" + rowId).val(accounting.toFixed(balance, 2));
				backOrderEl.prop("disabled", false);
			} else {
            	backOrderEl.prop("disabled", true);
			}

			PickingSlip.calculateTotalForPicking();
			PickingSlip.calculateTotalForBackOrder();
			PickingSlip.calculateTotalQuantity();
		},

		calculateTotalForPicking: function()
		{
			let qty = 0;
			$(".quantity").each(function(){
				const rowId = $(this).data("row-id");
				const isPicking = $("#is_picked_" + rowId);
				const itemQty = $(this).val();

				if (isPicking.is(":checked") && itemQty !== "") {
					qty += accounting.unformat(itemQty)
				}
			});
			$("#total_send_for_picking").html(accounting.formatMoney(qty))
		},

		calculateTotalForBackOrder: function()
		{
			let qty = 0;
			$(".quantity-balance").each(function(){
				const rowId = $(this).data("row-id");
				const isBackOrder = $("#back_order_" + rowId);
				const itemQty = $(this).val();

				if (isBackOrder.is(":checked") && itemQty !== "") {
					qty += parseFloat(itemQty)
				}
			});
			$("#total_on_back_order").html(accounting.formatMoney(qty))
		},

		calculateTotalQuantity: function()
		{
			console.log("---------calculateTotalQuantity----------");
			let qty = 0;
			$(".quantity-ordered").each(function(){
				qty += parseFloat($(this).val());
			});
			$("#total_received").html(accounting.formatMoney(qty))
		},

		calculateTotalNetWeight: function()
		{
			console.log("---------calculateTotalNetWeight----------");
			let qty = 0;
			$(".net-weight").each(function(){
				qty += parseFloat($(this).val())
			});
			$("#total_net_weight").html(accounting.formatMoney(qty))
		},

		calculateTotalGrossWeight: function()
		{
			console.log("---------calculateTotalGrossWeight----------");
			let qty = 0;
			$(".gross-weight").each(function(){
				qty += parseFloat($(this).val())
			});
			$("#total_gross_weight").html(accounting.formatMoney(qty))
		},

		actionPickingSlip: function(el)
		{
			$.getJSON(el.data('ajax-url'), function(response){
				responseNotification(response)
			});
		},

		getSelectedPickingSlips: function(pickingSlips)
		{
			pickingSlips = pickingSlips || [];
			$(".picking-slip-action").each(function(){
				if ($(this).is(":checked")) {
                    pickingSlips.push($(this).data('picking-slip-id'));
                }
			});
			return pickingSlips.filter(pickingSlipId => pickingSlipId !== undefined);
		},

		singlePickingSlipAction: function(el, pickingSlips)
		{
			const pickingSlipIds = PickingSlip.getSelectedPickingSlips(pickingSlips);
			const totalPickingSlips = pickingSlipIds.length;
			if (totalPickingSlips === 0) {
				alert('Please select at least one picking slip');
				return false;
			} else if (totalPickingSlips  > 1) {
				alert('Please select one picking slip to action');
				return false;
			} else {
				$.get(el.data('ajax-url'), {
					picking_slip_id: pickingSlipIds[0]
				}, function(response) {
					responseNotification(response);
				});
			}
		},
    }

}(PickingSlip || {}, jQuery));

