$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
		let self = $(this);
		destroyExistingModals(self).then((res) => {
			console.log(res);
			Inventory.openModal(self);
		});
    });

	$("#goods>tr>td:not(.actions)").on('click', function(){
		document.location.href = $(this).parent('tr').data('detail-url');
		return false;
	});

    const sellingPriceEl = $("#id_selling_price");
    const sellingPriceIncVatEl = $("#selling_price_inc_vat");
    const saleVatCodeEl = $("#id_sale_vat_code");
    saleVatCodeEl.on('change', function(e){
		const sellingPrice = sellingPriceEl.val();
    	Inventory.calculatePriceWithVat(sellingPrice, sellingPriceIncVatEl);
	});

    sellingPriceEl.on('blur', function(e){
    	const sellingPrice = $(this).val();
    	Inventory.calculatePriceWithVat(sellingPrice, sellingPriceIncVatEl)
	});

	$("#inventory_item>tr>td:not(.actions)").on('click', function(){
		document.location.href = $(this).parent('tr').data('detail-url');
		return false;
	});

    const viewInventoryItem = $("#view_inventory_item");
    const inventorySearchUrl = viewInventoryItem.data('ajax-url');
    viewInventoryItem.autocomplete({
        source: inventorySearchUrl,
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          // add the selected item
          if (ui.item) {
              Inventory.displayDetail(ui.item.detail_url)
          }
          return false;
        }
    });

    const searchInventoryItem = $("#search_inventory_item");
    searchInventoryItem.on('click', function(e){
    	Inventory.searchAndDisplayDetail(searchInventoryItem)
	});

    const inventoryQuantity = $(".quantity");
    inventoryQuantity.on('blur', function(){
    	const inventoryItemId = $(this).data('inventory-item-id');
    	Inventory.calculateAdjustment(inventoryItemId);
	});

    $("#save_inventory_item").on('click', function(){
    	ajaxSubmitForm($(this), $("#create_inventory_item"));
	});

	$("#save_and_copy_to_all").on('click', function(){
    	ajaxSubmitForm($(this), $("#create_inventory_item"), {'is_copied': true})
	});

	$("#submit_create_branch_inventory").on('click', function(){
    	ajaxSubmitForm($(this), $("#create_branch_inventory"))
	});

	$("#update_inventory").on('click', function(){
    	ajaxSubmitForm($(this), $("#update_inventory_form"))
	});

	var searchPurchaseOrder = $("#id_reference");
	var poSearchUrl = searchPurchaseOrder.data('ajax-url');

	var supplier = $("#id_supplier");
	supplier.on('change', function(){
		Inventory.loadSupplierItems($(this));
	});

	$("#id_purchase_order").hide();
	$("#id_purchase_order_chosen").hide();

	var goodsReceivedSelectEl = $("#id_received_movement");
	if (goodsReceivedSelectEl.length > 0) {
		goodsReceivedSelectEl.hide();
		$("#id_received_movement_chosen").hide();
	}

	var purchaseOrderSupplier = $("#id_purchase_order_supplier");
	purchaseOrderSupplier.on('change', function(e){
		Inventory.loadSupplierPurchaseOrders($(this));
		$("#enter_totals").prop('disabled', true);
		return false;
	});


	$("#id_received_movement_chosen").prop('disabled', false);
	$("#id_received_movement").prop('disabled', false);
	var supplierReturned = $("#id_supplier_returned");
	supplierReturned.on('change', function(){
		Inventory.loadSupplierGoodsReceived($(this));
	});


	// var idSupplierReceivedMovement  = $("#supplier_inventory_received_items");
	// if (idsupplierReceivedMovement.length > 0) {
	// 	console.log('Hide the received goods listdsddddddddddddddddddd');
	// 	idsupplierReceivedMovement.hide();
	// 	$("#id_received_movement_chosen").hide();
	// 	var supplierInventoryReturned = $("#id_supplier_returned");
	// 	supplierInventoryReturned.on('change', function(){
	// 		Inventory.loadSupplierInventoryItems($(this));
	// 	});
	// }

	$("#id_sku_type").on("change", function(e){
		console.log( $(this).val(), typeof $(this).val(), ($(this).val() == 2) )
		Inventory.handleSkuType($(this).val());
	});

	Inventory.handleSkuType($("#id_sku_type option:selected").val());

	$("#submit_goods_received").on('click', function(e){
		ajaxSubmitForm($(this), $("#create_goods_received_form"));
		return false;
	});

	$("#submit_goods_returned").on('click', function(e){
		ajaxSubmitForm($(this), $("#create_goods_returned"));
		return false;
	});

	$("#adjust_stock_submit").on('click', function(){
		ajaxSubmitForm($(this), $("#adjust_stock"));
		return false;
	});

	var inventoryItemsBody = $("#inventory_items_body");
	if (inventoryItemsBody.length > 0) {
		Inventory.loadInventoryItems(inventoryItemsBody);
	}

	var addLine = $("#add_received_line");
	addLine.on('click', function(){
		Inventory.loadInventoryItems($(this));
	});

	$("#enter_totals").on("change", function(event){
		Inventory.openInputs($(this))
	});

	$("#id_measure").on('change', function(e){
		Inventory.updateUnits($(this));
	});

	$(".ledger-inventory-item").on('click', function(e){
		e.preventDefault();
		Inventory.displayInventoryBreakdown($(this));
	});

	var viewInventoryDetail = $("#inventory_item_detail");
	if (viewInventoryDetail.length > 0) {
		var detailUrl = viewInventoryDetail.data('ajax-url');
		Inventory.displayDetail(detailUrl)
	}

	var searchInventorySelect = $("#id_inventory");
	searchInventorySelect.on("change", function(e) {
		var viewUrl = searchInventorySelect.data('view-url');
		Inventory.displayDetail(detailUrl, $(this).val());
	});

	var isRecipeItem = $("#id_is_recipe_item");
	isRecipeItem.on('change', function(){
		if ($(this).is(":checked")) {
			$(".not-reciped").hide();
		} else {
			$(".not-reciped").show();
		}
	});

	if (isRecipeItem.is(":checked")) {
		$(".not-reciped").hide();
	} else {
		$(".not-reciped").show();
	}

	$(".adjustment-price").on('change', function(e){
		Inventory.calculateAdjustmentPrice($(this));
	});

	$("#copy_opening_balances").on("click", function(e){
		var ajaxUrl = $(this).data('ajax-url');
		ajaxSubmit($(this), ajaxUrl);
	});

	$(".is-group-level-parent").on("change", function(e){
		e.preventDefault();
		Inventory.loadGroupLevelChildren($(this));
	});
});


var Inventory = (function(inventory, $){

	return {

		loadGroupLevelChildren(el)
		{
			$.getJSON(el.data('ajax-children-url'), {
				parent_id: el.val()
			}, function(data){
				const html = [];
				$.each(data, function(index, group_level_item){
					html.push("<option value='" + group_level_item.id + "'>" + group_level_item.name + "</option>");
				});
				const elId = $(".children-of-" + el.data('parent-id')).attr('id');
				$(elId).html(html.join(' ')).trigger("chosen:updated")
				$(elId).chosen();
				initChosenSelect();
			});
		},

		handleSkuType(skuType) {
			const glAccountEl = $("label[for='id_gl_account']")
			const glAccountLabel = glAccountEl.text();
			if (skuType === '2') { // Service with Cost provision
				$(".service-sku").show();
				$(".sku").hide()
				$( "#tabs" ).tabs( { disabled: [] } );
				glAccountEl.text("Inventory " + glAccountLabel);
			} else if (skuType === '3') { // Usage
				$( "#tabs" ).tabs( { disabled: [2, 3] } );
				glAccountEl.data('inventory-gl-label', glAccountLabel)
				glAccountEl.text(glAccountLabel.replace("Inventory", ''));
			} else {
				$( "#tabs" ).tabs( { disabled: [] } );
				$(".service-sku").hide();
				$(".sku").show()
				glAccountEl.text("Inventory " + glAccountLabel);
			}
		},

		openModal: function(el)
		{
			$.get(el.data('modal'), function (data) {
				$(data).modal('show');
				var form = $(data).find("form");
				var submitBtn = form.find("button[type='submit']");

				$("#save_group_level").on('click', function (event) {
					submitModalForm($("#create_journal_level"));
					return false
				});
				$("#update_group_level").on('click', function (event) {
					submitModalForm($("#edit_journal_level"));
					return false
				});
				// $("#copy_inventory").on('click', function (event) {
				// 	alert("Copy inventory items ")
				// 	console.log( 'Copy inventory items ' );
				// 	var form = $("#copy_inventory_form");
				// 	form.LoadingOverlay('show', {
				// 		'text': 'Processing ...'
				// 	});
				//     ajaxSubmitForm($(this), form);
				//     return false
				// });
				initDatePicker();

				initChosenSelect();

			});
		},

		calculateAdjustmentPrice: function(el)
		{
			var ajaxUrl = el.data('ajax-calculate-price');
			var priceType = el.val();
			var inventoryId = el.data('inventory-item-id');
			var quantity = $("#adjustment_" + inventoryId).val();
			if (quantity !==  '') {
				$.getJSON(ajaxUrl, {
					quantity: quantity,
					price_type: priceType
				}, function(response){
					if (response && response.hasOwnProperty('total')) {
						$("#total_price_" + inventoryId).html(response.total);
					}
				});
			}
		},

		updateUnits: function(el)
		{
			var unitsAjaxUrl = el.data('units-url');
			var measure = el.val();
			$.getJSON(unitsAjaxUrl, {
				measure: measure
			}, function(response){
				console.log(response)
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
				});
				$("#id_sale_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_stock_unit").html(html.join('')).trigger("chosen:updated");
			});
		},

		displayLedgerDetail: function(el)
		{
		    var el = $(el);
		    var lineId = el.data('line-id');
		    console.log("has class open ==> ",  el.hasClass('open') );
            if (el.hasClass('open')) {
                $(".row-" + lineId).hide();
                el.removeClass('open');
            } else {
                if ($("#row-" + lineId).length > 0) {
                    $(".row-" + lineId).show();
                    el.addClass('open');
                } else {
                    var ajaxUrl = el.data('ajax-url');
                    $.get(ajaxUrl, function(html){
                        $("#row_" + lineId ).after(html);
                        el.addClass('open');
                    });
                }
            }
		},

		openInputs: function(el)
		{
			var isChecked = el.is(":checked");
			$(".open-input").each(function(){
				if (isChecked) {
					$(this).prop('type', 'text').addClass('opened');
					$(".price").hide();
				} else {
					$(this).prop('type', 'hidden').removeClass('opened');
					$(".price").show();
				}
			});

			var totalPriceExcl = $(".total-price-excl");
			if (totalPriceExcl.hasClass('opened')) {
				totalPriceExcl.on('blur', function(){
					var inventoryItemId = parseInt($(this).attr('id').replace('total_price_excluding_', ''));
					if (totalPriceExcl.val() !== '') {
                        Inventory.calculatePriceExcluding(totalPriceExcl.val(), inventoryItemId);
                    }
				});
			}
			// $(".open-input").on('change', function(){
			// 	Inventory.calculateTotals();
			// });
		},

		loadInventoryItems: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			el.prop('disabled', true);
			var container = $("#inventory_items_placeholder");
			var rowId = 0;
			var lastRow = $(".inventory_item_row").last();

			var chosenEl = $("#"+ rowId + "_inventory_item_chosen");
			var chosenWidth = 0;
			if (chosenEl.length > 0) {
				chosenWidth = chosenEl.width();
            }
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}

				$("#"+ rowId +"_inventory_item").on('change', function(){
					var selectedOption = $("#"+ rowId +"_inventory_item option:selected");
					var price = selectedOption.data('price');
					var measureId = selectedOption.data('measure');
					var unitId = selectedOption.data('unit-id');
					var vatId = selectedOption.data('vat-id');

					$("#price_" + rowId ).val( price );
					if (measureId !== undefined && measureId !== '') {
						var unitsUrl = selectedOption.data('units-url');
						Inventory.loadUnits(rowId, measureId, unitsUrl, unitId);
					}
					if (vatId !== undefined && vatId !== '') {
						$("#vat_code_" + rowId).val(vatId).trigger("chosen:updated");
					} else {
						$("#vat_code_" + rowId).val('').trigger("chosen:updated");
					}
					Inventory.calculateInventoryItemPrices(rowId);

				});

				$("#quantity_received_"+ rowId +"").on('blur', function(){
					console.log('<---- Quantity received --->')
					var selectedOption = $("#"+ rowId +"_inventory_item option:selected");
					var maxOrder = selectedOption.data('max-order');
					var receivedQty = parseFloat($(this).val());
					if (maxOrder !== undefined && maxOrder !== '') {
                        if (maxOrder >= receivedQty) {
                            Inventory.calculateInventoryItemPrices(rowId);
                        } else {
                            $(this).val('');
                            alert('Quantity entered is greater than the max quantity(' + maxOrder + ') allowed, please fix')
                        }
                    } else {
						Inventory.calculateInventoryItemPrices(rowId);

                    }
				});

				$("#vat_code_"+ rowId ).on('change', function(){
					var netTotal = $("#total_price_exl_" + rowId ).text();
					var vatSelected = $("#vat_code_"+ rowId  + " option:selected");
					var percentage = vatSelected.data('percentage');
					if (netTotal !== '' && percentage !== '') {
						var vatAmount = (parseFloat(percentage)/100) * parseFloat(netTotal);
						var vatAmountStr = accounting.formatMoney(vatAmount);
						$("#vat_" + rowId ).html( vatAmountStr );
						$("#vat_amount_" + rowId ).val( vatAmount );
					}
					Inventory.calculateInventoryItemPrices(rowId);
				});

				$("#price_"+ rowId +"").on('blur', function(){
					Inventory.calculateInventoryItemPrices(rowId);
				});

				if (chosenWidth > 0) {
					$(".chosen-select").chosen({'width': chosenWidth + "px"});
				} else {
					$(".chosen-select").chosen({'width': '143px'});
				}

				el.prop('disabled', false);
			});
		},

		loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			var measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId == item.id) {
						html.push("<option value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#unit_" + rowId).html(html.join('')).trigger("chosen:updated");

			});
		},

		loadSupplierReceivedItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id,
				date: $("#id_date").val()
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		loadSupplierPurchaseOrders: function(el)
		{
			var ajaxUrl = el.data('purchase-orders-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					$("#enter_totals").prop('disabled', false);
					var optionSelected = $("#id_purchase_order  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		loadSupplierGoodsReceived: function(el)
		{
			var ajaxUrl = el.data('goods-received-url');
			var supplierId = $("#id_supplier_returned option:selected").val();
			console.log("Load received items for supplier and inventory item---> ", supplierId, ajaxUrl)
			var receivedSelectEl = $("#id_received_movement");
			var receivedChosenEl = $("#id_received_movement_chosen");
			$("#id_received_movement_chosen").prop('disabled', true);
			receivedSelectEl.empty();
			$.getJSON(ajaxUrl, {
				supplier_id: supplierId,
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
					console.log( item )
					html.push("<option value='"+ item.id +"' data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				receivedSelectEl.html(html.join('')).trigger("chosen:updated");

				receivedSelectEl.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayGoodsReceivedInventoryItems(ajaxUrl, $(this).val());
				});

				//receivedSelectEl.show();
				receivedChosenEl.show();
			});
		},

		displayGoodsReceivedInventoryItems: function(ajaxUrl, goodsReceivedId)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, {
					id: goodsReceivedId
				}, function(html){
				   $("#supplier_inventory_received_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						var inventoryItemId = $(this).data('item-id');
						Inventory.calculateInventoryItemPrices(inventoryItemId)
				   });

				   $(".received").each(function(){
						var inventoryItemId = $(this).data('item-id');
						Inventory.calculateInventoryItemPrices(inventoryItemId)
				   });

				});
			} else {
				$("#supplier_inventory_received_items").html("");;
			}
		},


		loadSupplierInventoryItems: function(el)
		{
			var ajaxUrl = el.data('inventory-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_inventory");
			$("#id_inventory_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");

				$.each(response, function(index, inventory_item){
					html.push("<option value='"+ inventory_item.id +"' " +
						"data-ajax-url='"+ inventory_item.received_items_url +"'>"+ inventory_item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_inventory  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					console.log("Update received items ---> ")
					Inventory.loadSupplierInventoryReceivedItems(ajaxUrl);
                });
			});
		},

		loadSupplierInventoryReceivedItems: function(ajaxUrl)
		{
			var supplierId = $("#id_supplier_returned option:selected").val();
			var inventoryId = $("#id_inventory option:selected").val();
			console.log("Load received items for supplier and inventory item---> ", supplierId, inventoryId, ajaxUrl)
			// var supplierId = $("#id_supplier_returned option:selected").val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();
			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplierId,
				inventory_id: inventoryId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displaySupplierInventoryReceivedItems(ajaxUrl, inventoryId);
                });
			});
		},

		displaySupplierInventoryReceivedItems: function(ajaxUrl, inventoryId)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, {
					inventoryId: inventoryId
				}, function(html){
				   $("#supplier_inventory_received_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						var inventoryItemId = $(this).data('item-id');
						Inventory.calculateInventoryItemPrices(inventoryItemId)
				   });
				});
			} else {
				$("#supplier_inventory_received_items").html("");;
			}
		},

		loadSupplierItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_purchase_order  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					Inventory.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		calculateAdjustment: function(inventoryItemId)
		{
			var onHand = $("#quantity_on_hand_" + inventoryItemId);
			var newQuantity = $("#new_quantity_" + inventoryItemId);

			if (onHand.val() !== '' && newQuantity.val() !== '') {
				var adjustment = parseInt(newQuantity.val()) - parseInt(onHand.val());
				if (adjustment > 0) {
				    $(".price-choice-" + inventoryItemId ).show();
                } else {
				    $(".price-choice-" + inventoryItemId ).hide();
                }
				$("#adjustment_" + inventoryItemId).val( adjustment );
            }
		},

		calculatePriceWithVat: function(sellingPrice, sellingPriceIncVat)
		{
			const vatOptionSelected = $("#id_sale_vat_code option:selected");
			if (vatOptionSelected) {
				const percentage = vatOptionSelected.data('percentage');
				if (percentage) {
					const vat = Inventory.calculateVat(percentage, sellingPrice);
					const sellingPriceWithVat = parseFloat(sellingPrice) + vat;
					sellingPriceIncVat.val(accounting.formatMoney(sellingPriceWithVat));
				}
			}
		},

		calculateVat: function(percentage, sellingPrice)
		{
			let vat = 0;
			if (percentage !== undefined) {
				const percentageVal = parseFloat(percentage);
				const sellingPriceVal = parseFloat(sellingPrice);
				vat = (percentageVal / 100) * sellingPriceVal;
			}
			return vat;
		},

		displayMovementBreakdown: function(el, inventoryId)
		{
        	const movementId = el.data('movement-id');
            const movementBreakdown = $("#movement_breakdown_" + inventoryId + "_" + movementId);
			if (movementBreakdown.hasClass('open')) {
				movementBreakdown.hide().removeClass('open').addClass('closed');
			} else if(movementBreakdown.hasClass('closed')) {
				movementBreakdown.show().removeClass('closed').addClass('open');
			}
		},

		displayInventoryBreakdown: function(el)
        {
        	const inventoryId = el.data('inventory-id');
        	const inventoryBreakdown = $("#inventory_breakdown_" + inventoryId);
			if (inventoryBreakdown.hasClass('open')) {
				inventoryBreakdown.hide().removeClass('open').addClass('closed');
			} else if(inventoryBreakdown.hasClass('closed')) {
				inventoryBreakdown.show().removeClass('closed').addClass('open');
			} else {
				$(".breakdown").each(function(){
					if ($(this).hasClass('open')) {
						$(this).hide().removeClass('open').addClass('closed');
					}
				});
				$("#breakdown_row_" + inventoryId).show();
				inventoryBreakdown.html('loading breakdown...');
				$.get(el.data('ajax-url'), function(html){
				    inventoryBreakdown.html(html).addClass('open');

					$(".movement-row").on('click', function(e){
						e.preventDefault();
						Inventory.displayMovementBreakdown($(this), inventoryId);
					});
				});
			}
        },

        displayDetail: function(detailUrl, inventoryId)
        {
            $.get(detailUrl, {
            	inventory_id: inventoryId
			}, function(html){
               $("#inventory_item_detail").html(html);
            });
        },

		searchAndDisplayDetail: function(el)
        {
            const term = $("#view_inventory_item").val();
            if (term !== '') {
                $.get(el.data('ajax-url'), {
                    term:term
                }, function(html){
                   $("#inventory_item_detail").html(html);
                });
            }
        },

		displayPurchaseOrderInventoryItems: function(ajaxUrl)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, function(html){
				   $("#purchase_order_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						const value = $(this).val();
						if (value !== '') {
							Inventory.calculateInventoryItemPrices($(this).data('item-id'))
						}
				   });
				});
			} else {
				$("#purchase_order_items").html("");
			}
		},

		calculateInventoryItemPrices: function(inventoryItemId)
		{
			console.log("CALCULATING INVENTORY ROW ", inventoryItemId," PRICE --> ");
			console.log("Calculate totals --> ", inventoryItemId );
			var price = $("#price_" + inventoryItemId);
			var ordered = $("#quantity_ordered_" + inventoryItemId);
			var received = $("#quantity_received_" + inventoryItemId);
			var short = $("#quantity_short_" + inventoryItemId);
			var vat = $("#vat_amount_" + inventoryItemId);
			var vatPercentage = $("#vat_percentage_" + inventoryItemId);
			var backOrder = $("#back_order_" + inventoryItemId);
			var extraQuantity = $("#extra_quantity_" + inventoryItemId);

			var quantityOrdered = 0;
			var shortQuantity = 0;
			var quantityReceived = 0;
			var vatAmount = 0;
			var priceExcl = 0;
			var allowedExtraQuantity = 0;

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}
			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}

			if (extraQuantity !== undefined && extraQuantity.length > 0) {
				if (extraQuantity.val() !== '') {
					allowedExtraQuantity = parseFloat(extraQuantity.val())

				}
			}

			if (quantityOrdered > 0) {
				var allowedOrderQty = (quantityOrdered + allowedExtraQuantity);
				if (quantityReceived <= allowedOrderQty) {
					if (quantityReceived < quantityOrdered) {
						shortQuantity =  quantityOrdered - quantityReceived;
					}
				} else {
					received.val(quantityOrdered);
					backOrder.prop('disabled', true);
					alert('Please enter valid received quantity');
					return false;
				}
			}
			if (short !== undefined  && short.length > 0) {
                short.val(accounting.toFixed(shortQuantity, 2));
            }
			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}
			console.log( "Vat percentage" + vatPercentage )
			console.log( "Vat element" + vatPercentage.length )
			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    vatAmount = (percentage/100) * priceExcl;
                }
            }
			vat.val(accounting.toFixed(vatAmount, 2));
			// if (vat !== undefined && vat.length > 0) {
			// 	if (vat.val() !== '') {
			//
			// 		vatAmount += parseFloat(accounting.unformat(vat.val()));
			// 	}
			// }

			var totalExcl = priceExcl * quantityReceived;
			var priceIncl =  priceExcl + vatAmount;
			var totalVat =  vatAmount * quantityReceived;
			var totalInc = totalExcl + totalVat;

			if (backOrder !== undefined) {
				if (shortQuantity === 0) {
					backOrder.prop('disabled', true);
				} else {
					backOrder.prop('disabled', false);
				}
			}

			//$("#vat_amount_" + inventoryItemId).val( accounting.toFixed(totalVat, 2) );
			console.log("Total vat -->", totalVat, accounting.toFixed(totalVat, 2))
			$("#vat_total_" + inventoryItemId).html( accounting.toFixed(totalVat, 2) );
			$("#total_vat_" + inventoryItemId).val( accounting.toFixed(totalVat, 2) );

			$("#price_excluding_" + inventoryItemId).val(accounting.toFixed(priceExcl, 2) );

			$("#price_incl_" + inventoryItemId).html( accounting.toFixed(priceIncl, 2) );
			$("#price_including_" + inventoryItemId).val( accounting.toFixed(priceIncl, 2) );

			$("#total_price_exl_" + inventoryItemId).html( accounting.toFixed(totalExcl, 2) );
			$("#total_price_excluding_" + inventoryItemId).val( accounting.toFixed(totalExcl, 2) );

			$("#total_price_incl_" + inventoryItemId).html( accounting.toFixed(totalInc, 2) );
			$("#total_price_including_" + inventoryItemId).val( accounting.toFixed(totalInc, 2) );

			Inventory.calculateTotals()
		},

		calculatePriceExcluding: function(totalPriceExcl, inventoryItemId)
		{
			var received = $("#quantity_received_" + inventoryItemId);
			var quantity = 0;
			if (received.val() !== '') {
				quantity = parseFloat(received.val());
			}
			if (quantity > 0) {
                var priceExcl = parseFloat(totalPriceExcl)/quantity;
				$("#price_" + inventoryItemId).val(priceExcl);
            }
			Inventory.calculateInventoryItemPrices(inventoryItemId);
		},

		calculateTotals: function()
		{
			var totalOrdered = 0;
			var totalReceived = 0;
			var totalShort = 0;
			var priceExc = 0;
			var priceInc = 0;
			var totalPrice = 0;
			var totalTotalExc = 0;
			var totalTotalInc = 0;
			var totalVat = 0;

			$(".ordered").each(function(){
				totalOrdered += parseInt($(this).val());
			});

			$(".received").each(function(){
				totalReceived += parseInt($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseInt($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".vat-amount").each(function(){
				totalVat += parseFloat(accounting.unformat($(this).val()));
			});

			$("#total_ordered").html( totalOrdered );
			$("#total_received").html( totalReceived );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.toFixed(totalPrice, 2) );
			$("#price_excl").html( accounting.toFixed(priceExc, 2) );
			$("#price_incl").html( accounting.toFixed(priceInc, 2) );

			$("#id_sub_total").val(accounting.toFixed(totalTotalExc, 2));
			$("#sub_total").html(accounting.toFixed(totalTotalExc, 3));

			$("#total_price_excl").html( accounting.toFixed(totalTotalExc, 2) );
			$("#id_net_amount").val( accounting.toFixed(totalTotalExc, 2) );

			$("#total_price_inc").html( accounting.toFixed(totalTotalInc, 3) );

			$("#vat_total").html( accounting.toFixed(totalVat, 2) );
			$("#id_total_vat").val(accounting.toFixed(totalVat, 2));
			$("#id_vat_amount").val(accounting.toFixed(totalVat, 2));

			$("#id_total_amount").val( accounting.toFixed(totalTotalInc, 2) );
		}
	}
}(Inventory || {}, jQuery));

