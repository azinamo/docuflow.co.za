$(document).ready(function() {
	//Form Submit for IE Browser

	$(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			initDatePicker();

			initChosenSelect();

        });
    });

	$("#save").on("click", function(e){
		e.preventDefault()
		ajaxSubmitForm($(this), $("#provisional_tax_estimate"));
	});

	if ($("#estimate_id").length > 0) {
		ProvisionalTaxEstimate.calculations();
	} else {
		ProvisionalTaxEstimate.loadPeriodData().done(function(){
			ProvisionalTaxEstimate.calculations()
		});
	}

	$("#recalculate").on("click", function(){
		ProvisionalTaxEstimate.loadPeriodData().done(function(){
			ProvisionalTaxEstimate.calculations()
		});
	});

	$("#toggle_estimate_income").on("click", function(e){
		e.preventDefault();
		const toggleBtn = $("#toggle_estimate_income");
		const estimatedIncomeEl = $(".estimated-income");
		if (estimatedIncomeEl.is(":hidden")) {
			toggleBtn.text('Hide');
		} else {
			toggleBtn.text('Show');
		}
		estimatedIncomeEl.toggle();
	});

	$("#id_to_period").on("change", function(e){
		e.preventDefault();
		ProvisionalTaxEstimate.loadPeriodData().done(function(){
			ProvisionalTaxEstimate.calculations()
		});
	});

	$("#id_rebates").on("blur", function(){
		ProvisionalTaxEstimate.calculations();
	});

	$("#id_employee_tax").on("blur", function(){
		ProvisionalTaxEstimate.calculations();
	});

	$("#id_foreign_tax").on("blur", function(){
		ProvisionalTaxEstimate.calculations();
	});

	$("#id_tax_paid").on("blur", function(){
		ProvisionalTaxEstimate.calculations();
	});

	$("#id_late_payment_penalty").on("blur", function(){
		ProvisionalTaxEstimate.calculations();
	});

	$("#id_late_payment_interest").on("blur", function(){
		ProvisionalTaxEstimate.calculations();
	});

	$("#id_assesed_loss_previous_year").on("blur", function(){
		ProvisionalTaxEstimate.calculations();
	});
});

var ProvisionalTaxEstimate = (function(provisionalTaxEstimate, $){

	return {

		loadPeriodData: function() {
			const el = $("#id_to_period");
			const selectedPeriod = $("#id_to_period option:selected");
			return $.getJSON(el.data('ajax-url'), {
				period_id: selectedPeriod.val()
			}, function(response) {

				$("#upto_period").html(selectedPeriod.text());

				// Turnover
				let turnover = accounting.toFixed(response.turnover.amount)
				$("#id_turnover").val(turnover);
				$("#turnover").html(accounting.formatMoney(turnover, "", 0));

				$("#id_collapsed_turnover").val(turnover);
				$("#collapsed_turnover").html(accounting.formatMoney(turnover, "", 0));

				let annualizedTurnover = accounting.toFixed(response.turnover.annualized_amount)
				$("#id_annual_turnover").val(annualizedTurnover);
				$("#annual_turnover").html(accounting.formatMoney(annualizedTurnover, "", 0));

				$("#id_collapsed_annual_turnover").val(annualizedTurnover);
				$("#collapsed_annual_turnover").html(accounting.formatMoney(annualizedTurnover, "", 0));

				// Income Deductable
				let incomeDeductable = accounting.toFixed(response.deductable.amount)
				$("#id_income_deductable").val(incomeDeductable);
				$("#collapsed_deductable_expenses").html(accounting.formatMoney(incomeDeductable, "", 0));

				let annualIncomeDeductable = accounting.toFixed(response.deductable.annualized_amount)
				$("#id_collapsed_annual_deductable_expenses").val(annualIncomeDeductable);
				$("#collapsed_annual_deductable_expenses").html(accounting.formatMoney(annualIncomeDeductable, "", 0));

				// Taxable Income
				let incomeTaxable = accounting.toFixed(response.taxable_income.amount)
				$("#id_collapsed_taxable_income").val(incomeTaxable);
				$("#collapsed_taxable_income").html(accounting.formatMoney(incomeTaxable, "", 0));

				let annualIncomeTaxable = accounting.toFixed(response.taxable_income.annualized_amount)
				$("#id_collapsed_annual_taxable_income").val(annualIncomeTaxable);
				$("#collapsed_annual_taxable_income").html(accounting.formatMoney(annualIncomeTaxable, "", 0));

				$("#estimated_income").html(accounting.formatMoney(incomeTaxable, "", 0));
				$("#id_estimated_income").val(incomeTaxable);

				$("#annual_estimated_income").html(accounting.formatMoney(annualIncomeTaxable, "", 0));
				$("#id_annual_estimated_income").val(annualIncomeTaxable);

				// Non Deductable Income
				let incomeNonDeductable = accounting.toFixed(response.non_deductable.amount)
				$("#id_income_non_deductable").val(incomeNonDeductable);
				$("#collapsed_non_deductable_expense").html(accounting.formatMoney(incomeNonDeductable, "", 0));

				let annualIncomeNonDeductable = accounting.toFixed(response.non_deductable.annualized_amount)
				$("#id_collapsed_annual_non_deductable_expense").val(annualIncomeNonDeductable);
				$("#collapsed_annual_non_deductable_expense").html(accounting.formatMoney(annualIncomeNonDeductable, "", 0));

				// Profit / Loss
				let profitLoss = accounting.toFixed(response.profit_loss.amount)
				$("#id_collapsed_profit_loss").val(profitLoss);
				$("#collapsed_profit_loss").html(accounting.formatMoney(profitLoss, "", 0));

				let annualProfitLoss = accounting.toFixed(response.profit_loss.annualized_amount)
				$("#id_collapsed_annual_profit_loss").val(annualProfitLoss);
				$("#collapsed_annual_profit_loss").html(accounting.formatMoney(annualProfitLoss, "", 0));

			});
		},

		calculations: function() {
			ProvisionalTaxEstimate.calculateTaxOnEstimatedIncome().then(function(){
				console.log('calculateTaxForYear')
				return ProvisionalTaxEstimate.calculateTaxForYear();
			}).then(function(){
				console.log('calculateTaxForPeriod')
				return ProvisionalTaxEstimate.calculateTaxForPeriod();
			}).then(function(){
				console.log('calculateTaxPayableForPeriod')
				return ProvisionalTaxEstimate.calculateTaxPayableForPeriod();
			}).then(function(){
				console.log('calculateTotalPayable')
				ProvisionalTaxEstimate.calculateTotalPayable();
			})
		},

		calculateTaxOnEstimatedIncome: async function() {
			console.log('calculateTaxOnEstimatedIncome---')
			const assesedLossPreviousYear = parseFloat($("#id_assesed_loss_previous_year").val());
			const annualEstimatedIncome = parseFloat($("#id_annual_estimated_income").val());

			let assesedLossCurrentYear = annualEstimatedIncome - assesedLossPreviousYear
			console.log("Assesed loss current year -> ", assesedLossCurrentYear, " estimatedIncome ", annualEstimatedIncome , " + assesedLossPreviousYear", assesedLossPreviousYear)

			assesedLossCurrentYear = accounting.toFixed(assesedLossCurrentYear, 0)
			$("#taxable_income_assesed_loss").html(accounting.formatMoney(assesedLossCurrentYear, "", 0));
			$("#id_taxable_income_assesed_loss").val(assesedLossCurrentYear);

			const annualizedAmount = parseFloat($("#id_taxable_income_assesed_loss").val());
			const taxRate = parseFloat($("#id_tax_rate").val());
			console.log("annualizedAmount -> ", annualizedAmount, " taxRate ", taxRate)
			let taxableAmount = 0
			if (assesedLossCurrentYear < 0) {
				taxableAmount = 0
			} else {
				taxableAmount = (taxRate / 100) * annualizedAmount;
			}
			console.log("taxableAmount -> ", taxableAmount, " annualizedAmount ", annualizedAmount)
			taxableAmount = accounting.toFixed(taxableAmount, 0)
			$("#taxable_income").html(accounting.formatMoney(taxableAmount, "", 0));
			$("#id_taxable_income").val(taxableAmount);
		},

		calculateTaxForYear: async function() {

			const taxableIncome = parseFloat($("#id_taxable_income").val());
			const taxRebates = parseFloat($("#id_rebates").val());
			let taxForYear = taxableIncome + taxRebates;

			taxForYear = accounting.toFixed(taxForYear, 0)
			$("#id_year_tax").val(taxForYear)
			$("#year_tax").html(accounting.formatMoney(taxForYear, "", 0))
		},

		calculateTaxForPeriod: async function() {

			const taxForYear = parseFloat($("#id_year_tax").val());
			const taxEstimateType = $("#id_tax_estimate_type").val();
			let taxForPeriod = taxForYear;
			if (taxEstimateType === 'first') {
				taxForPeriod = taxForYear / 2
			} else if (taxEstimateType === 'second') {
				taxForPeriod = taxForYear
			}
			taxForPeriod = accounting.toFixed(taxForPeriod, 0);
			$("#id_period_tax").val(taxForPeriod)
			$("#period_tax").html(accounting.formatMoney(taxForPeriod, "", 0))
		},

		calculateTaxPayableForPeriod: async function() {

			const employeeTax = parseFloat($("#id_employee_tax").val());
			const foreignTax = parseFloat($("#id_foreign_tax").val());
			const paidTax = parseFloat($("#id_tax_paid").val());
			const taxForPeriod = parseFloat($("#id_period_tax").val());

			let taxPayableForPeriod = taxForPeriod - employeeTax - foreignTax - paidTax
			let amount = taxPayableForPeriod
			if (taxPayableForPeriod < 0) {
				amount = 0
			}
			let periodTax = accounting.toFixed(amount, 0)
			$("#id_tax_payable_for_period").val(periodTax)
			$("#tax_payable_for_period").html(accounting.formatMoney(periodTax, "", 0))
		},

		calculateTotalPayable: function() {

			const taxPayable = parseFloat($("#id_tax_payable_for_period").val());
			const latePaymentPenalty = parseFloat($("#id_late_payment_penalty").val());
			const latePaymentInterest = parseFloat($("#id_late_payment_interest").val());

			let totalAmountPayable = taxPayable + latePaymentPenalty + latePaymentInterest
			totalAmountPayable = accounting.toFixed(totalAmountPayable, 0)
			$("#id_total_amount").val(totalAmountPayable)
			$("#total_amount").html(accounting.formatMoney(totalAmountPayable, "", 0))
		},

	}
}(ProvisionalTaxEstimate || {}, jQuery))
