$(document).ready(function() {

    var distribution = window.Docuflow.Distribution || {};

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save_comment").on('click', function (event) {
                event.preventDefault();
                ajaxSubmitForm($(this), $("#comment_form")).then(function(){
                    $(".modal-ajax").remove()
                    $(".modal-backdrop").remove()

                    Journal.getCommentsBox();
                });
            });

            $("#distribute_accounts").on("click", function(e){
                e.preventDefault();
                const accountIds = [];
                $(".distribution-account").each(function(){
                    if ($(this).is(":checked")) {
                        accountIds.push($(this).val());
                    }
                });
                Journal.distributeAccounts($(this), accountIds[0], {
                    amount: $("#id_total_amount").val()
                });
            });

			$("#submit_distribution").on('click', function(e){
			    e.preventDefault();
				ajaxSubmitForm($(this), $("#create_distribution"));
			});

			$("#delete_journal_distribution").on('click', function(e){
			    e.preventDefault();
				$.getJSON($(this).data('ajax-url'), function(response){
				    responseNotification(response)
                })
			});

			$("#submit_distribution_update").on('click', function(e){
			    e.preventDefault();
				ajaxSubmitForm($(this), $("#edit_distribution"));
			});

			initDatePicker();
			initChosenSelect();

        });
    });

	$(document).on("change", "#id_category", function(e){
		e.preventDefault();
		const html = [];
		html.push("<option value=''>--</option>");
		$.getJSON($(this).data('ajax-url'), {
			category: $(this).val()
		}, function(response) {
			$(response).each(function(index, fixed_asset){
				html.push("<option value='"+ fixed_asset.id +"'>"+ fixed_asset.name +"</option>");
			});
			$("#id_asset").html(html.join(' '));
		});
        $("#id_asset_category").trigger("chosen:updated");
		$("#id_asset").trigger("chosen:updated");
        initChosenSelect();
	});


    const journalLinesContainer = $("#journal_lines");
    if (journalLinesContainer.length > 0) {
        Journal.loadJournalLine(journalLinesContainer, {'row': 1, 'rows': 100});
    }

    $(".accounts").on('change', function(e){
        const rowId = $(this).attr('id').replace('account_', '');
        Journal.loadAccountVatCodes($(this), $(this).val(), rowId);

        const accountType = $(this).find(':selected').data('account-type');
        const distributionEl = $("#distribution_" + rowId);
        if (accountType === 1) {
            distributionEl.prop('disabled', false)
        } else {
            distributionEl.prop('disabled', true)
        }
    });

    $("#all_lines").on('click', function(e){
        if (confirm('Are you sure you want to delete all lines')) {
            Journal.markLines($(this));
        }
    });

    $(".delete-line").on('click', function(e){
        if (confirm('Are you sure you want to delete this line ')) {
            Journal.markForDeletion($(this));
        }
    });

    $(".vat_codes").on('change', function(e){
        const rowId = $(this).attr('id').replace('vat_codes_', '');
        const description = $(this).data('description');
        $("#description_" + rowId).html(description);
    });

    $(".credit-amount").on('blur', function(e){
        const rowId = $(this).attr('id').replace('credit_', '');
        Journal.handleCreditAmount($(this), rowId);
    });

    $(".debit-amount").on('blur', function(e){
        const rowId = $(this).attr('id').replace('debit_', '');
        Journal.handleDebitAmount($(this), rowId);
    });

    $(".distributions").on('change', function(){
        if ($(this).val() === 'yes') {
            const rowId = $(this).attr('id').replace('distribution_', '');
           Journal.openDistributionModal($(this), rowId);
        }
    });

    $(".delete_journal_line").on('click', function(e){
        e.preventDefault();
        if (confirm('Are you sure you want to delete this line')) {
            Journal.removeJournalLine($(this));
        }
    });

    $(".delete-journal").on('click', function(e){
        e.preventDefault();
        if (confirm('Are you sure you want to delete this journal')) {
            Journal.deleteJournal($(this));
        }
    });

    $(".journal-datepicker").datepicker({
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat : 'yy-mm-dd',
            onSelect: function( selectedDate ) {
        },
        onClose: function(t, i) {
            Journal.validatePeriodAndDate();
        }
    });

    $("#add_journal_line").on('click', function(e){
        e.preventDefault();
        const row = $(".journal_lines").last();
        const rowNumber = row.attr('id').replace('journal_line_', '');
        const params = {'row': rowNumber, 'row_id': rowNumber}
        Journal.loadJournalLine(journalLinesContainer, params);
    });

    $("#add_ledger_line").on('click', function(e){
        e.preventDefault();
        const journalLinesContainer = $("#ledger_lines");
        const params = {'rows': 1, 'edit': 1, 'row_id': Journal.getNextRow()}
        Journal.loadJournalLine(journalLinesContainer, params);
    });


    $("#save_general_journal").on('click', function(e){
        Journal.checkFixedAssetAccountPosting($(this));
    });

	Journal.getCommentsBox();

});


var Journal = (function(journal, $){

	return {
        getCommentsBox: function(){
            const el = $("#add_comments");
            if (el.length) {
                $.get(el.data('ajax-url'), function(responseHtml){
                    el.html(responseHtml)
                });
            }
        },

        updateJournalLines: function(el)
        {
            const journalForm = $("#update_journal_form");
            journalForm.LoadingOverlay("show", {
                text : "Processing"
            });
            ajaxSubmitForm(el, journalForm);
            el.prop('disabled', false);
        },

	    markLines: function(el)
        {
            $(".delete-line").each(function(){
                Journal.markForDeletion($(this));
            });
            Journal.activateDeleteBtn();
        },

        markForDeletion(el)
        {
            const rowId = el.data('row-id');
            const lineId = el.data('line-id');
            $.getJSON(el.data('ajax-url'), (response) => {
                if (response.error) {
                    responseNotification(response);
                } else {
                    el.addClass('delete');
                    $("#line_" + rowId).val(lineId);
                    $(".ledger-line-"+ rowId +">td:not(.actions)").each(function($this){
                        const text = $(this).html();
                        $(this).addClass('text-danger').html("<span style='text-decoration: line-through;'>"+ text +"</span>");
                    });
                    Journal.activateDeleteBtn();
                }
            })
        },

	    activateDeleteBtn: function()
        {
            let countChecked = 0;
            const updateBtn = $("#save_general_journal");
            $(".delete-line").each(function(){
                if ($(this).is(":checked")) {
                    countChecked++;
                }
            });
            Journal.calculateTotalDebit();
            Journal.calculateTotalCredit();
        },

	    isValidLine: function(rowId)
        {
          const debitEl = $("#debit_" + rowId);
          const creditEl = $("#credit_" + rowId);
          const modelEl = $(".model-" + rowId);
          let totalAmount = 0;
          if (debitEl.length > 0 && debitEl.val() !== "") {
              totalAmount += parseFloat(debitEl.val());
          }
          if (creditEl.length > 0 && creditEl.val() !== "") {
              totalAmount += parseFloat(creditEl.val());
          }
          if (totalAmount <= 0) {
              return false;
          }
          if (modelEl.length <= 0 || modelEl === undefined || modelEl.val() === '' ) {
             return false;
          }
          return true;
        },

        getNextRow: function()
        {
            let rowId = 1;
            const lastRow = $(".journal-lines").last();
            if (lastRow.length > 0) {
                rowId = parseInt(lastRow.data('row-id')) + 1;
            }
            return rowId
        },

	    loadJournalLine: function(el, params)
        {
            params = params || {};
            params['row'] = params['row'] || 1;
            params['rows'] = params['rows'] || 1;
            params['row_id'] = params['row_id'] || 1;
            $.get(el.data('ajax-url'), params, function(responseHtml){
                el.append(responseHtml);

                $(".journal-lines").each(function(){
                    Journal.handleRow($(this).data("row-id"), params);
                });

            });
        },

        handleDebitAmount: function(el, rowId)
        {
            const debitAmount = el.val();
            if (debitAmount !== '') {
                const amount = parseFloat(debitAmount);
                if (amount < 0) {
                    $("#credit_" + rowId).val(amount * -1);
                    $("#debit_" + rowId).prop('disabled', true);
                    $(this).val('');
                    Journal.calculateTotalCredit();
                } else {
                     $("#credit_" + rowId).prop('disabled', true);
                    Journal.calculateTotalDebit();
                }
            } else {
                $("#credit_" + rowId).prop('disabled', false);
                Journal.calculateTotalDebit();
            }
        },

        handleCreditAmount: function(el, rowId)
        {
            const creditAmount = el.val();
            if (creditAmount !== '') {
                $("#debit_" + rowId).prop('disabled', true);
                Journal.calculateTotalCredit();
            } else {
                $("#debit_" + rowId).prop('disabled', false);
                Journal.calculateTotalCredit();
            }
        },

        handleRow: function(rowId, params)
        {
            const ledgerLineSelect = $("#journal_content_type_" + rowId);
            const searchUrl = ledgerLineSelect.data("ajax-url");
            const ledgerTypeEl = $("#ledger_" + rowId);
            const lastRow = $(".journal-lines").last();

            $(".search-box-" + rowId).each(function(index, el){
                const searchBoxEl = $(el);
                const model = searchBoxEl.data('model');
                const searchUrl = searchBoxEl.data("ajax-url");
                const modelSearchEl = $(".model-" + model + "-search-" + rowId);
                modelSearchEl.autocomplete({
                    minLength: 1,
                    source: function( request, response ) {
                        $.ajax({
                          url: searchUrl,
                          dataType: "json",
                          data: {
                             term: request.term,
                             account_id: $("#id_account_" + rowId).val()
                          },
                          success: function( data ) {
                            response(data);
                          }
                        });
                    },
                    focus: function( event, ui ) {
                        if (model !== 'suffix') {
                           $( "#" + model + "_item_" + rowId ).val(ui.item.description);
                        }
                        return false;
                    },
                    select: function( event, ui ) {
                        if (model === 'suffix') {
                            $( "#"+ model + "_" + rowId).val(ui.item.suffix);
                        } else {
                            $( "#id_"+ model + "_" + rowId ).val(ui.item.id);
                            $( "#"+ model + "_item_" + rowId ).val(ui.item.code);
                            $( "#"+ model + "_item_description_" + rowId ).html(ui.item.description);

                            if (model === 'vat_code') {
                                Journal.handleVatSelection(ui.item, rowId);
                            } else if (model === 'customer') {
                                Journal.handleCustomer(ui.item.id, rowId);
                            } else if (model === 'supplier') {
                                Journal.handleSupplier(model, ui.item.id, rowId);
                            } else if (model === 'account') {
                                Journal.handleAccount(ui.item.id, rowId);
                            }
                        }
                        return false;
                    }
                })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                    if (model === 'suffix') {
                        return $( "<li>")
                            .append( "<div>" + item.suffix + "</div>" )
                            .appendTo( ul );
                    } else {
                        return $( "<li>" )
                            .append( "<div>" + item.code + "<br>" + item.description + "</div>" )
                            .appendTo( ul );
                    }
                };
            });

            $("#credit_" + rowId).on('blur', function(e){
                Journal.processCreditAmount($(this));
                if (lastRow !== undefined) {
                    const lastRowId = lastRow.data('row-id');
                    if (Journal.isValidLine(lastRowId)) {
                       //Journal.loadJournalLine($("#journal_lines"), parseInt(lastRowId) + 1, 1);
                    }
                }
            });

            $("#debit_" + rowId).on('blur', function(e){
                Journal.processDebitAmount($(this));
            });
            $("#distribution_" + rowId).on("click", function(e){
                e.preventDefault();
                Journal.openDistributionModal($(this), rowId);
            });

            ledgerTypeEl.on('change', function(e){
                params['rows'] = 1
                Journal.reloadJournalRow($(this), params)
            });
        },

        reloadJournalRow: function(el, options)
		{
			const rowId = el.data('row-id');
			const ajaxUrl = el.data('ajax-url');
			const ledgerType = $("#ledger_"+ rowId +" option:selected").val();
			const tabStart = el.data('tab-start');
			const params = {'row': rowId, 'line_type': ledgerType, 'reload': 1, 'row_id': rowId};
			if ('rows' in options) {
			    params['rows'] = options['rows']
            }
			if ('edit' in options) {
			    params['edit'] = options['edit']
            }
			if (tabStart !== null) {
			    params['tab_start'] = tabStart;
            }
			$.get(ajaxUrl, params, function(html) {
				$("#journal_line_" + rowId).replaceWith(html);
				Journal.handleRow(rowId, params);
			});
		},

	    checkFixedAssetAccountPosting: function(el)
        {
            el.submitButton('loading');
            const journalForm = $("#create_journal_form");
            journalForm.LoadingOverlay("show", {
                text : "Processing"
            });
            journalForm.ajaxSubmit({
                url: el.data('ajax-check--asset-account-url'),
                target: '#loader',   // target element(s) to be updated with server response
                beforeSubmit:  function(formData, jqForm, options) {
                    const queryString = $.param(formData);
                },  // pre-submit callback
                error: function (data) {
                    // handle errors
                    if (data.status === 400) {
                        responseNotification(data.responseJSON);
                    } else {
                        const error = {'error': true, 'text': data.statusText};
                        responseNotification(error);
                    }
                    journalForm.LoadingOverlay("hide");
                    el.submitButton('reset');
                },
                success:  function(responseJson, statusText, xhr, $form)  {
                    el.submitButton('reset');
                    if (responseJson.error) {
                        responseNotification(responseJson);
                    } else {
                        if ('account' in responseJson) {
                            responseNotification(responseJson);
                            Journal.createFixedAsset(responseJson);
                        } else {
                            Journal.saveGeneralJournal(el);
                        }
                    }
                },  // post-submit callback
                dataType: 'json',
                type: 'post'
            });
        },

        createFixedAsset: function(responseJson)
        {
			$(".modal-ajax").remove()
			$(".modal-backdrop").remove();
            $.get(responseJson.create_fixed_asset_url, function (data) {
                $(data).modal('show');
                $("#id_category").trigger("chosen:updated");
                $("#id_asset").trigger("chosen:updated");
                initDatePicker();
                initChosenSelect();
                const accountLineName = $("input[value='"+ responseJson.account_id +"']").prop('name')
                const rowId = accountLineName.substr(8)
                $(document).on('click', '#save_journal_asset', function(e){
                    const createJournalAssetForm = $("#create_journal_asset");

                    e.preventDefault();
                    const params = {
                        'credit': $("#credit_" + rowId).val(),
                        'debit': $("#debit_" + rowId).val(),
                        'date': $("#id_date").val(),
                        'row_id': rowId
                    }
                    const xhr = ajaxSubmitForm($(this), createJournalAssetForm, params);
                    xhr.done(function(responseJson, status, xhr){
                        if (responseJson.error) {
                            $("#save_journal_asset").prop('disabled', false);
                        } else {
                            $(data).modal("hide");
                            Journal.saveGeneralJournal($(this)).then(function(response){
                                $("#save_journal_asset").prop('disabled', !response.error);
                            });
                        }
                    })
                });
            });
        },

	    saveGeneralJournal: function(el)
        {
            return ajaxSubmitForm(el, $("#create_journal_form"));
        },

        loadLedgerTypeLists: function(el)
        {
            const rowId = el.data('row-id');
            const contentType = $("#journal_content_type_" + rowId);
            const ledgerTypeEl = $("#ledger_" + rowId + " option:selected");
            const ledgerType = ledgerTypeEl.val();

            let defaultAjaxUrl = contentType.data('ajax-url');
            if (ledgerType === '0') { //Accounts
                //Journal.loadAccounts(ledgerTypeEl, rowId)
                defaultAjaxUrl = el.data('ajax-accounts-url');
            } else if (ledgerType === '1') { //Suppliers
                //Journal.loadSuppliers(ledgerTypeEl, rowId);
                defaultAjaxUrl = el.data('ajax-supplier-url');
            } else if (ledgerType === '2') { //Customers
                //Journal.loadCustomers(ledgerTypeEl, rowId)
                defaultAjaxUrl = el.data('ajax-customers-url');
            }
            contentType.prop('data-ajax-url', defaultAjaxUrl)
        },

        loadAccounts: function(el, rowId)
        {
            var ajaxUrl = el.data('ajax-url');
            var ledger = el.val();
            $.get(ajaxUrl, {
                ledger: ledger
            },function(html){
                $("#account_" + rowId).html(html);
                $(".chosen-select").chosen();
                $(".accounts").on('change', function(e){
                    Journal.handleAccount($(this), rowId);
                });
                $(".credit_amount").on('blur', function(e){
                    Journal.processCreditAmount($(this));
                });
                $(".debit_amount").on('blur', function(e){
                    Journal.processDebitAmount($(this));
                });
            })
        },

        loadSuppliers: function(el, rowId)
        {
            var ajaxUrl = el.data('ajax-suppliers-url');
            var ledger = el.val();
            $.get(ajaxUrl, {
                ledger: ledger
            },function(html){
                $("#account_" + rowId).html(html);
                $(".chosen-select").chosen();
                $(".accounts").on('change', function(e){
                    Journal.handleAccount($(this), rowId);
                });
                $(".credit_amount").on('blur', function(e){
                    Journal.processCreditAmount($(this));
                });
                $(".debit_amount").on('blur', function(e){
                    Journal.processDebitAmount($(this));
                });
            })
        },

        loadCustomers: function(el, rowId)
        {
            var ledger = el.val();
            $.get(el.data('ajax-customers-url'), {
                ledger: ledger
            },function(html){
                $("#account_" + rowId).html(html);
                $(".chosen-select").chosen();
                $(".accounts").on('change', function(e){
                    Journal.handleAccount($(this), rowId);
                });
                $(".credit_amount").on('blur', function(e){
                    Journal.processCreditAmount($(this));
                });
                $(".debit_amount").on('blur', function(e){
                    Journal.processDebitAmount($(this));
                });
            })
        },

	    handleCustomer: function(customerId, rowId)
        {

        },

	    handleSupplier: function(supplierId, rowId)
        {

        },

	    handleAccount: function(accountId, rowId)
        {
            if (accountId === '') {
                $("#suffix_" + rowId).prop('disabled', true)
                $("#distribution_" + rowId).hide();
            } else {
                $("#suffix_" + rowId).prop('disabled', false)
                $.getJSON('/api/v1/company/accounts/' + accountId, function (response){
                    if (response.account_type !== 'Balance Sheet') {
                        $("#distribution_" + rowId).show();
                    } else {
                        $("#distribution_" + rowId).hide();
                    }
                });
                Journal.loadAccountVatCodes(accountId, rowId);
            }
        },

        processCreditAmount: function(el)
        {
            const rowId = el.attr('id').replace('credit_', '');
            if (el.val() !== '') {
                $("#debit_" + rowId).prop('readonly', true);
                Journal.calculateTotalCredit();
            } else {
                $("#debit_" + rowId).prop('readonly', false);
                Journal.calculateTotalCredit();
            }
        },

        processDebitAmount: function(el)
        {
            const rowId = el.attr('id').replace('debit_', '');
            const debitAmount = el.val();
            if (debitAmount !== '') {
                const amount = parseFloat(debitAmount);
                if (amount < 0) {
                    $("#credit_" + rowId).val(amount * -1);
                    $("#debit_" + rowId).prop('readonly', true);
                    el.val('');
                    Journal.calculateTotalCredit();
                } else {
                     $("#credit_" + rowId).prop('readonly', true);
                    Journal.calculateTotalDebit();
                }
            } else {
                $("#credit_" + rowId).prop('readonly', false);
                Journal.calculateTotalDebit();
            }
        },

	    validatePeriodAndDate: function()
        {
            const ajaxUrl =  $("#validate_journal_period").data('validate-period-ajax-url')
            const journalDate = $("#id_date").val();
            const period = $("#id_period").val();
            $.getJSON(ajaxUrl, {
                journal_date: journalDate,
                period: period
            }, function(responseJson){
                responseNotification(responseJson);
                const submitBtn = $("#submit");
                const isDisabled = (submitBtn.attr('disabled') === 'disabled');
                const isPeriodIssue = submitBtn.data('invalid-period');

                if (!isDisabled) {
                    if (responseJson.error) {
                        submitBtn.prop('disabled', true);
                        submitBtn.data('invalid-period', 'yes');
                    } else {
                        submitBtn.prop('disabled', false);
                    }
                } else {
                    if (isPeriodIssue === 'yes') {
                       submitBtn.prop('disabled', false);
                    }
                }
            })
        },

	    loadAccountVatCodes: function(accountId, rowId)
        {
            const el = $("#account_item_" + rowId);
            const vatCodeCell = $("#vat_codes_cell_" + rowId);
            const tabIndex = el.data('tab-index');
            vatCodeCell.empty();
            $.getJSON(el.data('vat-codes-ajax-url'), {
                account_id: accountId
            }, function(vat_codes){
                if (vat_codes.length > 0) {
                    const html = [];
                    html.push("<select class='chosen-select vat-codes' name='vat_code_"+ rowId +"' " +
                        "id='vat_code_"+ rowId +"' tabindex='"+ tabIndex +"' style='max-width: 200px !important; min-width: 200px !important;' >");
                    $.each(vat_codes, function(index, vat_code){
                        if (vat_code.is_default) {
                            html.push("<option value='"+ vat_code.id +"' selected='selected'>" + vat_code.label + "</option>")
                        } else {
                            html.push("<option value='"+ vat_code.id +"'>" + vat_code.label + "</option>")
                        }
                    });
                    html.push("</select>");
                    vatCodeCell.html(html.join(' '));

                    // $("#vat_code_" + rowId).chosen({
                    //     allowClear: true,
                    //     placeholder: "Select ...",
                    //     width: "200px",
                    // }).css({'min-width': '200px !important', 'max-width': '200px !important'});
                } else {
                    vatCodeCell.empty();
                }
            })
        },

	    calculateTotalCredit: function()
        {
	        let totalCredit = 0;
            $(".credit-amount").each(function(){
                const rowId = $(this).data('row-id');
                const isMarkedForDelete = $("#delete_" + rowId).hasClass("delete");
                if (!isMarkedForDelete) {
                    const amount = parseFloat($(this).val());
                    if (amount > 0) {
                        totalCredit += parseFloat($(this).val());
                    }
                }
            })
            const totalCreditStr = accounting.formatMoney(totalCredit);
            totalCredit = totalCredit.toFixed(2);
            $("#total_credit").val(totalCredit);
            $("#credit_total").html(totalCreditStr);

            Journal.calculateDifference();
        },

        calculateTotalDebit: function ()
        {
	        let totalDebit = 0;
            $(".debit-amount").each(function(){
                const rowId = $(this).data('row-id');
                const isMarkedForDelete = $("#delete_" + rowId).hasClass("delete");
                if (!isMarkedForDelete) {
                    const amount = parseFloat($(this).val());
                    if (amount > 0) {
                        totalDebit += amount;
                    }
                }
            });
            const totalDebitStr = accounting.formatMoney(totalDebit);
            totalDebit = totalDebit.toFixed(2);
            $("#total_debit").val(totalDebit);
            $("#debit_total").html(totalDebitStr);

            Journal.calculateDifference();
        },

        calculateDifference: function()
        {
            const totalDebit = parseFloat($("#total_debit").val());
            const totalCredit = parseFloat($("#total_credit").val());
            const submitBtn = $("#save_general_journal");
            let difference = 0;

            if (totalCredit || totalDebit) {
                difference = totalDebit - totalCredit;
            }
            if (difference === 0) {
                submitBtn.prop('disabled', false);
            } else {
                submitBtn.prop('disabled', true);
            }
            // if (totalCredit > totalDebit) {
            //     $("#debit_difference").html(accounting.formatMoney(difference));
            // } else if (totalDebit > totalCredit) {
            //     $("#difference").html(accounting.formatMoney(difference));
            // }
            $("#difference_text").html(accounting.formatMoney(difference));
            $("#difference").val(accounting.formatMoney(difference));
        },

        openDistributionModal: function(el, rowId)
        {
            clearModals();

            const accountId = $("#id_account_" + rowId).val();
            const debitAmount = $("#debit_" + rowId).val()
            const creditAmount = $("#credit_" + rowId).val()
            const date  = $("#id_date").val();
            if (accountId === '') {
                alert('Please select the account')
                return false
            } else if(debitAmount === '' && creditAmount === '') {
                alert('Please select the amount')
                return false
            } else if(!date) {
                alert('Please select the date')
                return false
            } else {
                const amount = debitAmount || creditAmount;
                const params = {
                    credit_amount: creditAmount,
                    debit_amount: debitAmount,
                    account_id: accountId,
                    amount: amount,
                    date: date,
                    suffix: $("#suffix_" + rowId).val()
                }
                $.get(el.data('ajax-url'), params, function (data) {
                    $(data).modal('show');

                    $("#distribute_journal_accounts").on("click", function(e){
                        e.preventDefault();
                        params['row_id'] = rowId
                        Journal.distributeAccounts($(this), accountId, params);
                    });

                    $("#submit_distribution").on('click', function(e){
                        params['row_id'] = rowId
                        ajaxSubmitForm($(this), $("#create_journal_line_distribution"), params).then(function(){
                           clearModals();
                           $("#distribution_" + rowId).html("Yes");
                        });
                        return false;
                    });
                    initDatePicker();

                    initChosenSelect();

                });
            }
        },

        submitDistribution: function(el, params)
        {
            const form = $("#create_journal_line_distribution")
            // disable extra form submits
            form.addClass('submitted');

            ajaxFormInit($(this), form);

            $('.alert-fixed').remove();
            $('.is-invalid').removeClass('is-invalid');
            $('.is-invalid-message').remove();

            let xhr = form.ajaxSubmit({
                target: '#loader',   // target element(s) to be updated with server response
                data: params,
                dataType: 'json',
                type: 'post',
                error: function (data, statusText, xhr) { 		// pre-submit callback
                    if (data.status === 400) {
                        responseNotification(data.responseJSON);
                    } else {
                        // handle errors
                        const error = {'error': true, 'text': data.statusText};
                        responseNotification(error);
                    }
                    //container.LoadingOverlay("hide");
                },
                success: function(responseJson, statusText, xhr, $form) {
                    responseNotification(responseJson);
                }
            }).data('jqxhr');
            return xhr.done(function(){
                // container.LoadingOverlay("hide");
            })
        },

		distributeAccounts: function(el, accountId, params)
		{
           const startDate = $("#id_starting_date");
           const nbMonth = $("#id_nb_months");
           if (accountId === '') {
             alert("Please select the accounts")
           } else if(!startDate.val()) {
              alert("Please select start date")
           } else if(!nbMonth.val()) {
              alert("Please enter number of months")
           } else {
               const container = $("#account_distribution");
               container.html("Loading . . .");
               params = params || {}
               params['suffix'] = $("#suffix").val()
               params['total_amount'] = params['amount']
               params['nb_months'] = nbMonth.val()
               params['starting_date'] = startDate.val()
               params['account_id'] = accountId
               $.get(el.data('ajax-url'), params, function(html){
                   container.html(html);
               });
           }
		},

        deleteJournalLine: function(el)
        {
            if (confirm('Are you sure you want to delete this journal')) {
                var ajaxUrl = el.data('ajax-url');
                var reference = el.attr('id').replace('delete_line_', '');
                $.getJSON(ajaxUrl, function(responseJson){
                   responseNotification(responseJson);
                   $("#journal_line_row_" + reference).remove();
                });
            }
        },

        removeJournalLine: function(el)
        {
           var reference = el.attr('id').replace('delete_line_', '');
           $("#journal_line_row_" + reference).remove();
           Journal.calculateTotalCredit()
           Journal.calculateTotalDebit()
        },

        deleteJournal: function(el)
        {
            $.getJSON(el.data('ajax-url'), function(responseJson){
               responseNotification(responseJson);
            });
        }


	}

}(Journal || {}, jQuery));

