$(function() {
	//Form Submit for IE Browser
	$("#tabs").tabs({
	  load: function( event, ui ) {
			$(".default_document_type").on('change', function(e){
				DocumentType.updateDefaultDocumentType(this);
				e.preventDefault();
			});
	  },
	  beforeLoad: function( event, ui ) {
			ui.jqXHR.fail(function() {
			  ui.panel.html("Couldn't load this tab. We'll try to fix this as soon as possible.");
			});
		  }
	});

});


var DocumentType = (function(documentType, $){

	return {

		updateDefaultDocumentType: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url')
			$.getJSON(ajaxUrl, function(response){
				responseNotification(response)
			});
		}
	}
}(DocumentType || {}, jQuery))
