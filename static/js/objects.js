$(document).ready(function() {
	//Form Submit for IE Browser

	$(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			$("#save_object_item_submit").on('click', function (event) {
                submitModalForm($("#create_object_item"));
                return false
            });
			$("#update_object_item_submit").on('click', function (event) {
                submitModalForm($("form[name='edit_object_item']"));
                return false
            });
			$(".chosen-select").chosen();
        });
    });
});

var CompanyObject = (function(accountPosting, $){

	return {

		loadObjectItems: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			var accountPostingId = el.data('invoice-id');
			$.get(ajaxUrl, {
				account_posting_id: accountPostingId,
				type: 'edit'
			}, function(html){
				el.html(html);
				$(".delete-account-posting").on('click', function(){
					if (confirm('Are you sure you want to delele this account posting')) {
						Invoice.deleteAccountPosting($(this));
					}
				});

				$(".delete_line").on('click', function(e){
					if (confirm('Are you sure you want to delete this line')) {
						CompanyObject.deleteAccountPostingLine($(this));
					}
					return false;
				});

			});
		},

		deleteObjectItem: function(el) {
			var ajaxUrl = el.data('ajax-url');
			var accountPostingLineId = el.data('line-id');
			$.getJSON(ajaxUrl, {
				account_posting_line_id: accountPostingLineId
			}, function(response){
				CompanyObject.loadAccountPostingLines($("#account_posting_lines"))
			});
		}


	}
}(CompanyObject || {}, jQuery))

