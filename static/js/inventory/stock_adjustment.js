jQuery(function(){

    const inventoryItems = $("#inventory_items");
    if (inventoryItems.length) {
        StockAdjustment.loadInventoryItems(inventoryItems);
    }
    $(".add-row").on("click", function(e){
        e.preventDefault();
        StockAdjustment.loadInventoryItems(inventoryItems);
    });



    $("#save").on("click", function(){
       ajaxSubmitForm($(this), $("#create_stock_adjustment"))
    });

    // const options = {'prefix': 'stock-inventory-adjustment', 'extra-classes': [],
    //     'formCssClass': 'inventory-line', 'keepFieldValues': ''};
    // const childElementSelector = 'input,select,textarea,label,div';
    // const flatExtraClasses = options.extraClasses.join(' ');
    // const totalForms = $('#id_' + options.prefix + '-TOTAL_FORMS');
    // const maxForms = $('#id_' + options.prefix + '-MAX_NUM_FORMS');
    //
    // // Otherwise, use the last form in the formset; this works much better if you've got
    // // extra (>= 1) forms (thnaks to justhamade for pointing this out):
    // let template = $('.' + options.formCssClass + ':last').clone(true).removeAttr('id');
    // template.find('input:hidden[id $= "-DELETE"]').remove();
    // // Clear all cloned fields, except those the user wants to keep (thanks to brunogola for the suggestion):
    // template.find(childElementSelector).not(options.keepFieldValues).each(function() {
    //     var elem = $(this);
    //     // If this is a checkbox or radiobutton, uncheck it.
    //     // This fixes Issue 1, reported by Wilson.Andrew.J:
    //     if (elem.is('input:checkbox') || elem.is('input:radio')) {
    //         elem.attr('checked', false);
    //     } else {
    //         elem.val('');
    //     }
    // });
    //
    // const addButton = $(".add-row");
    // addButton.on("click", function() {
    //     const formCount = parseInt(totalForms.val());
    //     const row = options.formTemplate.clone(true).removeClass('formset-custom-template');
    //     const buttonRow = $($(this).parents('tr.' + options.formCssClass + '-add').get(0) || this);
    //     StockAdjustment.applyExtraClasses(row, formCount);
    //     row.insertBefore(buttonRow).show();
    //     row.find(childElementSelector).each(function() {
    //         StockAdjustment.updateElementIndex($(this), options.prefix, formCount);
    //     });
    //     totalForms.val(formCount + 1);
    //     // Check if we've exceeded the maximum allowed number of forms:
    //     if (!showAddButton()) buttonRow.hide();
    //     // If a post-add callback was supplied, call it with the added form:
    //     if (options.added) options.added(row);
    //     return false;
    // });


});

var StockAdjustment = (function(stockAdjustment, $){
    return {
        prefix: 'inventory_form',
        totalForms: $('#id_inventory_form-TOTAL_FORMS'),
        maxForms: $('#id_inventory_form-MAX_NUM_FORMS'),
        minForms: $('#id_inventory_form-MIN_NUM_FORMS'),
        childElementSelector: 'input,select,textarea,label,div,span',

        deleteInventoryItemRow(el, rowId) {
            const self = this;
            $(".row-" + el.data('row-id')).remove();
            const formCount = parseInt(self.totalForms.val());
            self.totalForms.val(formCount -1);
        },

        loadInventoryItems(el) {
           const self = this;
           const ajaxUrl = el.data('inventory-items-url');
           const formCount = parseInt(self.totalForms.val());
           const buttonRow = $(".add-row").parents('tr.' + self.prefix + '-add').get(0);
           $.get(ajaxUrl, {
               rows: formCount
           }, function(responseHtml){
               $(responseHtml).insertBefore(buttonRow).show();
               $(responseHtml).find(self.childElementSelector).each(function() {
                    StockAdjustment.updateElementIndex($(this), self.prefix, formCount);
               });

               $("#id_"  + self.prefix + '_' + formCount + "_inventory").on("change", function(e){
                   const stockAjaxUrl = $(this).data('ajax-url');
                   StockAdjustment.getInventoryDetail(stockAjaxUrl, {inventory_id: $(this).val()}, formCount)
               });

               $("#id_" + self.prefix + '_' + formCount + "_price_type").on("change", function(e){
                   const priceType = $(this).val();
                   const priceEl = $("#id_" + self.prefix + '_' + formCount + '_price');
                   if (priceType === 'average_price') {
                       priceEl.prop('readonly', true);
                       const price = $("#id_" + self.prefix + '_' + formCount + '_average_price').val();
                       priceEl.val(accounting.toFixed(price, 2));
                   } else if (priceType === 'current_price') {
                       priceEl.prop('readonly', true);
                       const price = $("#id_" + self.prefix + '_' + formCount + '_current_price').val();
                       priceEl.val(accounting.toFixed(price, 2));
                   } else if (priceType === 'own_price') {
                      priceEl.prop('readonly', false);
                   }
                   StockAdjustment.calculateAdjustmentTotals(formCount);
               });

                $("#id_" + self.prefix + '_' + formCount +"_quantity").on("blur", function(){
                    StockAdjustment.calculateAdjustment($(this), formCount);
                });

                $("#id_" + self.prefix + '_' + formCount +"_price").on("blur", function(){
                    StockAdjustment.calculateAdjustmentTotals(formCount);
                });

                $("#delete_" + formCount).on("click", function(e){
                    e.preventDefault();
                    StockAdjustment.deleteInventoryItemRow($(this), formCount);
                });

                self.totalForms.val(formCount + 1);
                $(".chosen-select").chosen({'width': StockAdjustment.chosenWidth()});

                $("#save").prop('disabled', false)
           });
        },

        calculateAdjustment(el, rowId)
		{
		    const self = this;
		    const onHand = $("#id_" + self.prefix + '_' + rowId + "_in_stock").val();
			const newQuantity = el.val();
			if (onHand !== "" && newQuantity !== "") {
				const adjustment = accounting.toFixed(parseFloat(newQuantity) - parseFloat(onHand), 2);
				let priceChoice = $(".price-choice-" + rowId);
				let priceType = $("#id_inventory_form_" + rowId + "_price_type");
				if (adjustment > 0) {
				    priceType.prop('disabled', false);
				    priceChoice.show();
                } else {
				    priceChoice.hide();
				    $(".price").hide();
				    priceType.prop('disabled', true);
                }
				$("#id_inventory_form_" + rowId + "_adjustment").val( adjustment );
            }
		},

        calculateAdjustmentTotals(formCount) {
            const self = this;
            const price = $("#id_" + self.prefix + '_' + formCount + '_price').val();
            const quantity = $("#id_" + self.prefix + '_' + formCount + '_adjustment').val();
            const total = price * quantity
            $("#id_" + self.prefix + '_' + formCount + '_total_value').html(accounting.formatMoney(total));
            $("#id_" + self.prefix + '_' + formCount + '_total').val(total);
        },

        getInventoryDetail(url, params, rowId) {
            const self = this;
            console.log('--- getInventoryDetail ---')
            $.get(url, params, function(responseData){
                const description = responseData.description;
                const availableStock = responseData.in_stock;
                const averagePrice = responseData.average_price;
                const currentPrice = responseData.current_price;

                if (description !== undefined && description !== "") {
                    $("#id_" + self.prefix + '_' + rowId + "_description").html(description);
                }
                if (averagePrice !== undefined) {
                    $("#id_" + self.prefix + '_' + rowId + "_average_price").val(averagePrice);
                } else {
                    $("#id_" + self.prefix + '_' + rowId + "_average_price").val(0);
                }
                if (currentPrice !== undefined) {
                    $("#id_" + self.prefix + '_' + rowId + "_current_price").val(currentPrice);
                } else {
                    $("#id_" + self.prefix + '_' + rowId + "_current_price").val(0);
                }

                $("#id_" + self.prefix + '_' + rowId + "_account").val(responseData.stock_adjustment_account.id).trigger('chosen:updated');
                $("#id_" + self.prefix + '_' + rowId + "_on_hand").html(availableStock);
                $("#id_" + self.prefix + '_' + rowId + "_in_stock").val(availableStock);
            }, "json");
        },

        chosenWidth() {
            let chosenWidth = 250;
            const chosenContainer = $("#inventory_items > .chosen-container");
            if (chosenContainer.length > 0 ) {
                chosenWidth = chosenContainer.width();
            }
            if (chosenWidth > 0) {
                $(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
                $(".line-type").chosen({'width': '40px'});
            } else {
                $(".chosen-select").not('.line-type').chosen({'width': '250px'});
                $(".line-type").chosen({'width': '40px'});
            }
            return chosenWidth;
        },

        applyExtraClasses(row, ndx) {
            if (options.extraClasses) {
                row.removeClass(flatExtraClasses);
                row.addClass(options.extraClasses[ndx % options.extraClasses.length]);
            }
        },

        updateElementIndex(elem, prefix, ndx) {
            var idRegex = new RegExp(prefix + '-(\\d+|__prefix__)-'),
                replacement = prefix + '-' + ndx + '-';
            var underscoreIdRegex = new RegExp(prefix + '_(\\d+|__prefix__)_');
            var underscoreReplacement = prefix + '_' + ndx + '_';
            var elementId = elem.attr('id')
            if (elementId) {
                if (underscoreIdRegex.test(elem.attr('id'))) {
                    elem.attr('id', elementId.replace(underscoreIdRegex, underscoreReplacement));
                } else {
                    elem.attr('id', elementId.replace(idRegex, replacement));
                }

            }
            if (elem.attr('name')) {
                if (underscoreIdRegex.test(elem.attr('id'))) {
                    elem.attr('name', elem.attr('name').replace(underscoreIdRegex, underscoreReplacement));
                } else {
                    elem.attr('name', elem.attr('name').replace(idRegex, replacement));
                }
            }
            if (elem.attr("for")) elem.attr("for", elem.attr("for").replace(idRegex, replacement));

        },

        hasChildElements(row) {
            return row.find(childElementSelector).length > 0;
        },

        showAddButton() {
            return this.maxForms.length == 0 ||   // For Django versions pre 1.2
                (this.maxForms.val() == '' || (this.maxForms.val() - this.totalForms.val() > 0));
        }

    }

}(StockAdjustment || {}, jQuery))