$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			initDatePicker();

			initChosenSelect();

        });
    });

	$("#goods>tr>td:not(.actions)").on('click', function(){
		document.location.href = $(this).parent('tr').data('detail-url');;
		return false;
	});

	$("#inventory_item>tr>td:not(.actions)").on('click', function(){
		document.location.href = $(this).parent('tr').data('detail-url');;
		return false;
	});

    const sellingPriceEl = $("#id_selling_price");
    const sellingPriceIncVatEl = $("#selling_price_inc_vat");
    const saleVatCodeEl = $("#id_sale_vat_code");
    saleVatCodeEl.on('change', function(e){
    	InventoryReceived.calculatePriceWithVat(sellingPriceEl.val(), sellingPriceIncVatEl);
	});
    sellingPriceEl.on('blur', function(e){
    	InventoryReceived.calculatePriceWithVat($(this).val(), sellingPriceIncVatEl)
	});

    const inventoryQuantity = $(".quantity");
    inventoryQuantity.on('blur', function(){
    	InventoryReceived.calculateAdjustment($(this).data('inventory-item-id'));
	});

	const searchPurchaseOrder = $("#id_reference");
	const poSearchUrl = searchPurchaseOrder.data('ajax-url');
	$("#id_purchase_order").hide();
	$("#id_purchase_order_chosen").hide();

	const supplier = $("#id_supplier");
	supplier.on('change', function(e){
		InventoryReceived.loadSupplierPurchaseOrders($(this));
		// InventoryReceived.loadSupplierItems($(this));
		$("#enter_totals").prop('disabled', true);
		return false;
	});

	const idReceivedMovement  = $("#id_received_movement");
	if (idReceivedMovement.length > 0) {
		idReceivedMovement.hide();
		$("#id_received_movement_chosen").hide();
		$("#id_supplier_returned").on('change', function(){
			InventoryReceived.loadSupplierInventoryItems($(this));
		});
	}

	$("#submit_goods_received").on('click', function(e){
		ajaxSubmitForm($(this), $("#create_goods_received_form"));
		return false;
	});

	const inventoryReceivedItemsBody = $("#inventory_received_items_body");
	if (inventoryReceivedItemsBody.length > 0) {
		InventoryReceived.loadInventoryItems(inventoryReceivedItemsBody);
	}

	$("#add_received_line").on('click', function(){
		InventoryReceived.loadInventoryItems($(this));
	});

	$("#enter_totals").on("change", function(event){
		InventoryReceived.openInputs($(this))
	});

	$(".ledger-inventory-item").on('click', function(e){
		e.preventDefault();
		InventoryReceived.displayInventoryBreakdown($(this));
	});

});

var InventoryReceived = (function(inventoryReceived, $){

	return {

		updateUnits: function(el)
		{
			var unitsAjaxUrl = el.data('units-url');
			var measure = el.val();
			$.getJSON(unitsAjaxUrl, {
				measure: measure
			}, function(response){
				console.log(response)
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
				});
				$("#id_sale_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_unit").html(html.join('')).trigger("chosen:updated");
				$("#id_stock_unit").html(html.join('')).trigger("chosen:updated");
			});
		},

		loadInventoryItems: function(el)
		{
			el.prop('disabled', true);

			const container = $("#inventory_received_items_placeholder");
			let rowId = 0;
			const lastRow = $(".inventory-item-row").last();

			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(el.data('ajax-url'), {
				row_id: rowId
			}, function(html) {
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				$(".inventory-item-row").each(function() {
					const itemRowId = $(this).data("row-id")
					InventoryReceived.handleRow(itemRowId);
				});

				$(".delete-row").on('click', function(e){
					e.preventDefault();
				    let rowId = $(this).data('row-id');
				    $("#row_" + rowId).remove();
				    InventoryReceived.calculateInventoryItemPrices(rowId);
                });

				el.prop('disabled', false);
			});
		},

		handleRow: function(rowId)
		{
			const chosenWidth = calculateChosenSelectWidth("inventory_item_row");
			const inventorySelect = $("#"+ rowId +"_inventory_item");
			inventorySelect.ajaxChosen({
				url: inventorySelect.data("ajax-url"),
				dataType: "json",
				width: chosenWidth + "px",
				allowClear: true,
				placeholder: "Select ..."
			}, {loadingImg: '/static/js/vendor/chosen/loading.gif'}).width(chosenWidth + "px");

            const submitBtn = $("#submit_goods_received");
			inventorySelect.on('change', function(e){
			    const hasSupplier = InventoryReceived.validateSupplier();
                if (hasSupplier) {
                    submitBtn.prop('disabled', false);
                    const inventoryId = $(this).val();
                    $.get(inventorySelect.data("ajax-inventory-url"), {
                        inventory_id: inventoryId
                    }, function(inventoryData){
                        const price = inventoryData.cost_price;
                        const measure = inventoryData.measure;
                        const unit = inventoryData.unit;
                        const saleUnit = inventoryData.sale_unit;
                        const stockUnit = inventoryData.stock_unit;
                        const vatCode = inventoryData.vat_code;
                        const preferredSupplier = inventoryData.supplier;
                        let unitId = null;

						if (preferredSupplier) {
							const hasPreferredSupplier = InventoryReceived.hasPreferredSupplier(preferredSupplier.id);
							if (hasPreferredSupplier) {
								InventoryReceived.notifyInventoryPreferredSupplier(preferredSupplier, preferredSupplier.id);
							}
						}

                        submitBtn.prop('disabled', false);
                        let maxOrder = 0;
                        if (inventoryData.max_reorder) {
                            maxOrder = parseFloat(inventoryData.max_reorder);
                        }
                        $("#max_order_" + rowId ).val(maxOrder);
                        if (saleUnit) {
							$("#sale_unit_id_" + rowId ).val(saleUnit.id);
							$("#sale_unit_code_" + rowId ).val(saleUnit.code);
						}
                        if (stockUnit) {
							$("#stock_unit_id_" + rowId ).val(stockUnit.id);
							$("#stock_unit_code_" + rowId ).val(stockUnit.code);
						}
                        if (unit) {
							$("#unit_code_" + rowId ).val(unit.code);
							$("#unit_" + rowId ).val(unit.id);
							unitId = unit.id
						}

                        $("#price_" + rowId ).val(price);
                        if (measure) {
                            InventoryReceived.loadUnits(rowId, measure.id, measure.units_url, unitId);
                        }
                        if (vatCode) {
                            $("#vat_code_" + rowId).val(vatCode.id).prop('selected', true).trigger("chosen:updated");
                            $("#vat_percentage_" + rowId ).val( parseFloat(vatCode.percentage) );
                        } else {
                            $("#vat_code_" + rowId).val('').prop('selected', true).trigger("chosen:updated");
                        }
						if (saleUnit) {
							$("#control_unit_" + rowId).val(saleUnit.id);
						}
                        InventoryReceived.calculateInventoryItemPrices(rowId);
                    });
                } else {
                    inventorySelect.val("").trigger('chosen:updated');
                    submitBtn.prop('disabled', true)
                }
			});

			$("#quantity_received_"+ rowId +"").on('blur', function(){
				const maxOrder = parseFloat($("#max_order_" + rowId).val());
				const receivedQty = parseFloat($(this).val());
				const quantityOrderedEl = $("#quantity_ordered_" + rowId);
				const controlReceivedQtyEl = $("#control_quantity_received_" + rowId);
				if (maxOrder !== undefined && maxOrder !== '') {
					if (maxOrder === 0 || maxOrder >= receivedQty) {
						InventoryReceived.calculateInventoryItemPrices(rowId);
						if (InventoryReceived.isDifferentUnit(rowId)) {
							InventoryReceived.updateInventoryControlQuantity(rowId);
						} else {
							controlReceivedQtyEl.val(receivedQty)
                            quantityOrderedEl.val(receivedQty)
						}
					} else {
						$(this).val('');
						alert('Quantity entered is greater than the max quantity(' + maxOrder + ') allowed, please fix')
					}
				} else {
					console.log("Quantity Received", receivedQty)
					InventoryReceived.calculateInventoryItemPrices(rowId);
					if (InventoryReceived.isDifferentUnit(rowId)) {
						console.log("Quantity Received", receivedQty)
						InventoryReceived.updateInventoryControlQuantity(rowId);
					} else {
						controlReceivedQtyEl.val(receivedQty)
                        quantityOrderedEl.val(receivedQty)
					}
				}
				InventoryReceived.calculateInventoryItemPrices(rowId);
			});

			$("#unit_"+ rowId ).on('change', function(e){
				$("#unit_id_" + rowId ).val($(this).val());
				$("#unit_code_" + rowId ).val($("#unit_"+ rowId + " option:selected").data('unit'));

				const isDifferent =  InventoryReceived.isDifferentUnit(rowId);
				if (isDifferent) {
					InventoryReceived.updateInventoryControlQuantity(rowId);
				} else {
					const quantity = parseFloat($("#quantity_received_"+ rowId ).val());
					$("#control_quantity_received_" + rowId).val( quantity )
				}
			});

			$("#vat_code_"+ rowId ).on('change', function(){
				InventoryReceived.calculateInventoryItemPrices(rowId);
			});

			$("#price_"+ rowId +"").on('blur', function(){

				InventoryReceived.calculateInventoryItemPrices(rowId);
			});
		},

        validateSupplier: function()
        {
          const supplierId = $("#id_supplier").val();

          if (supplierId === '' || supplierId === undefined) {
              alert("Please select the supplier");
              return false;
          }
          return true;
        },

        hasPreferredSupplier: function(preferredSupplierId)
        {
		    if (preferredSupplierId === null || preferredSupplierId === undefined) {
		        return false;
            }
		    return true;
        },

        notifyInventoryPreferredSupplier: function(preferredSupplier, preferredSupplierId)
		{
            const selectedSupplier = $("#id_supplier option:selected")
            if (selectedSupplier !== undefined) {
                const selectedSupplierId = parseInt(selectedSupplier.val())
                preferredSupplierId = parseInt(preferredSupplierId)
                if (preferredSupplierId !== selectedSupplierId) {
                    alert(`Your preferred supplier is ${preferredSupplier.name} which is different from the selected supplier`);
                }
            }
		},

		isDifferentUnit: function(rowId)
		{
			const unitId = parseInt($("#unit_"+ rowId +" option:selected").val());
			const saleUnitId = parseInt($("#sale_unit_id_"+ rowId).val());
			return unitId !== saleUnitId;
		},

		updateInventoryControlQuantity: function(rowId)
		{
            const quantity = parseFloat($("#quantity_received_"+ rowId ).val());
            if (quantity > 0) {
                try {
                        const unit = $("#unit_code_"+ rowId ).val();
                        const saleUnit =  $("#sale_unit_code_"+ rowId ).val();
                        const convert = require('convert-units');
                        const result = convert(quantity).from(unit).to(saleUnit)
                        $("#control_quantity_received_" + rowId).val( result )
                } catch (e) {
                    $("#control_quantity_received_" + rowId).val( quantity )
                }
            } else {
                $("#control_quantity_received_" + rowId).val(0)
            }
		},

		loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			var measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId === item.id) {
						html.push("<option data-unit='" + item.unit +"' value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option data-unit='" + item.unit +"' value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#unit_" + rowId).html(html.join('')).trigger("chosen:updated");

			});
		},

		loadSupplierReceivedItems: function(el)
		{
			var ajaxUrl = el.data('items-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id,
				date: $("#id_date").val()
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					InventoryReceived.displayPurchaseOrderInventoryItems(ajaxUrl);
                });
			});
		},

		loadSupplierPurchaseOrders: function(el)
		{
			const idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(el.data('purchase-orders-ajax-url'), {
				supplier_id: el.val()
			}, function(response){
				const html = [];
				html.push("<option value=''>--</option>");
				response['orders'].map(function(item){
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					$("#enter_totals").prop('disabled', false);
					const optionSelected = $("#id_purchase_order  option:selected");
					InventoryReceived.displayPurchaseOrderInventoryItems(optionSelected.data('ajax-url'));
                });
			});
		},

		loadSupplierInventoryItems: function(el)
		{
			var ajaxUrl = el.data('inventory-ajax-url');
			var supplier_id = el.val();
			var idReference = $("#id_inventory");
			$("#id_inventory_chosen").show();

			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplier_id
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");

				$.each(response, function(index, inventory_item){
					html.push("<option value='"+ inventory_item.id +"' " +
						"data-ajax-url='"+ inventory_item.received_items_url +"'>"+ inventory_item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_inventory  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					console.log("Update received items ---> ")
					InventoryReceived.loadSupplierInventoryReceivedItems(ajaxUrl);
                });
			});
		},

		loadSupplierInventoryReceivedItems: function(ajaxUrl)
		{
			var supplierId = $("#id_supplier_returned option:selected").val();
			var inventoryId = $("#id_inventory option:selected").val();
			console.log("Load received items for supplier and inventory item---> ", supplierId, inventoryId, ajaxUrl)
			// var supplierId = $("#id_supplier_returned option:selected").val();
			var idReference = $("#id_received_movement");
			$("#id_received_movement_chosen").show();
			idReference.html('');
			$.getJSON(ajaxUrl, {
				supplier_id: supplierId,
				inventory_id: inventoryId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				console.log(response)
				response.map(function(item){
				    console.log( item )
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					var optionSelected = $("#id_received_movement  option:selected");
					var ajaxUrl = optionSelected.data('ajax-url');
					InventoryReceived.displaySupplierInventoryReceivedItems(ajaxUrl, inventoryId);
                });
			});
		},

		displaySupplierInventoryReceivedItems: function(ajaxUrl, inventoryId)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, {
					inventory_id: inventoryId
				}, function(html){
				   $("#supplier_inventory_received_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						var inventoryItemId = $(this).data('item-id');
						InventoryReceived.calculateInventoryItemPrices(inventoryItemId)
				   });
				});
			} else {
				$("#supplier_inventory_received_items").html("");
			}
		},

		loadSupplierItems: function(el)
		{
			const supplierId = el.val();
			const idReference = $("#id_purchase_order");
			$("#id_purchase_order_chosen").show();

			idReference.html('');
			$.getJSON(el.data('items-ajax-url'), {
				supplier_id: supplierId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					html.push("<option value='"+ item.id +"' " +
						"data-ajax-url='"+ item.items_url +"'>"+ item.text +"</option>");
				});
				idReference.html(html.join('')).trigger("chosen:updated");

				idReference.on('change', function (e) {
					const optionSelected = $("#id_purchase_order  option:selected");
					InventoryReceived.displayPurchaseOrderInventoryItems(optionSelected.data('ajax-url'));
                });
			});
		},

		calculatePriceWithVat: function(sellingPrice, sellingPriceIncVat)
		{
			const percentage = $("#id_sale_vat_code option:selected").data('percentage');
			const vat = InventoryReceived.calculateVat(percentage, sellingPrice);
			const sellingPriceWithVat = parseFloat(sellingPrice) + vat;
			sellingPriceIncVat.val(accounting.formatMoney(sellingPriceWithVat));
		},

		calculateVat: function(percentage, sellingPrice)
		{
			let vat = 0;
			if (percentage !== undefined) {
				const percentageVal = parseFloat(percentage);
				const sellingPriceVal = parseFloat(sellingPrice);
				vat = (percentageVal / 100) * sellingPriceVal;
			}
			return vat;
		},

		displayPurchaseOrderInventoryItems: function(ajaxUrl)
		{
			if (ajaxUrl) {
				$.get(ajaxUrl, function(html){
				   $("#purchase_order_items").html(html);

				   $(".price-exc, .ordered, .received").on('blur', function(){
						InventoryReceived.calculateInventoryItemPrices($(this).data('item-id'))
				   });
				});
			} else {
				$("#purchase_order_items").html("");
			}
		},

		calculateInventoryItemPrices: function(inventoryItemId)
		{
			const price = $("#price_" + inventoryItemId);
			const ordered = $("#quantity_ordered_" + inventoryItemId);
			const received = $("#quantity_received_" + inventoryItemId);
			const short = $("#quantity_short_" + inventoryItemId);
			const vatPercentage = $("#vat_percentage_" + inventoryItemId);
			const backOrder = $("#back_order_" + inventoryItemId);

			let quantityOrdered = 0;
			let shortQuantity = 0;
			let quantityReceived = 0;
			let vatAmount = 0;
			let priceExcl = 0;

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}

			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}

			if (quantityOrdered > 0) {
				if (quantityReceived <= quantityOrdered) {
					shortQuantity =  quantityOrdered - quantityReceived
					shortQuantity = parseFloat(accounting.toFixed(shortQuantity, 2))
				} else {
					received.val(quantityOrdered);
					backOrder.prop('disabled', true);
					alert('Please enter valid received quantity')
					return false;
				}
			}

			if (short !== undefined  && short.length > 0) {
                short.val( shortQuantity );
            }

			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}

			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    const percentage = parseFloat(vatPercentage.val());
                    vatAmount = (percentage / 100) * priceExcl;

                }
            }
			// if (vat !== undefined && vat.length > 0) {
			// 	if (vat.val() !== '') {
			//
			// 		vatAmount += parseFloat(accounting.unformat(vat.val()));
			// 	}
			// }

			const totalExcl = priceExcl * quantityReceived;
			const priceIncl =  priceExcl + vatAmount;
			const totalInc = priceIncl * quantityReceived;
			const totalVatAmount = quantityReceived * vatAmount;

			if (backOrder !== undefined) {
				if (shortQuantity === 0) {
					backOrder.prop('disabled', true);
				} else {
					backOrder.prop('disabled', false);
				}
			}

			$("#vat_" + inventoryItemId).html( accounting.toFixed(totalVatAmount, 2) );
			$("#vat_amount_" + inventoryItemId).val(accounting.toFixed(totalVatAmount, 2));

			$("#price_incl_" + inventoryItemId).html( accounting.toFixed(priceIncl, 2) );
			$("#price_including_" + inventoryItemId).val(accounting.toFixed(priceIncl, 2));

			$("#total_price_exl_" + inventoryItemId).html( accounting.toFixed(totalExcl, 2) );
			$("#total_price_excluding_" + inventoryItemId).val(accounting.toFixed(totalExcl, 2));

			$("#total_price_incl_" + inventoryItemId).html( accounting.toFixed(totalInc, 2) );
			$("#total_price_including_" + inventoryItemId).val(accounting.toFixed(totalInc, 2));

			InventoryReceived.calculateTotals()
		},

		calculatePriceExcluding: function(totalPriceExcl, inventoryItemId)
		{
			const receivedEl = $("#quantity_received_" + inventoryItemId);
			let quantity = 0;

			if (receivedEl.val() !== '') {
				quantity = parseFloat(receivedEl.val());
			}

			if (quantity > 0) {
                const priceExcl = parseFloat(totalPriceExcl) / quantity;
				$("#price_" + inventoryItemId).val(priceExcl);
            }
			InventoryReceived.calculateInventoryItemPrices(inventoryItemId);
		},

		calculateTotals: function()
		{
			let totalOrdered = 0;
			let totalReceived = 0;
			let totalShort = 0;
			let priceExc = 0;
			let priceInc = 0;
			let totalPrice = 0;
			let totalTotalExc = 0;
			let totalTotalInc = 0;
			let totalVat = 0;

			$(".ordered").each(function(){
				totalOrdered += parseFloat($(this).val());
			});

			$(".received").each(function(){
				totalReceived += parseFloat($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseFloat($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".vat").each(function(){
				totalVat += parseFloat(accounting.unformat($(this).val()));
			});

			$("#total_ordered").html( accounting.toFixed(totalOrdered, 2) );
			$("#total_received").html( accounting.toFixed(totalReceived, 2) );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.toFixed(totalPrice, 2) );
			$("#price_excl").html( accounting.toFixed(priceExc, 2) );
			$("#price_incl").html( accounting.toFixed(priceInc, 2) );
			$("#total_price_excl").html( accounting.toFixed(totalTotalExc, 2) );
			$("#total_price_inc").html( accounting.toFixed(totalTotalInc, 2) );

			$("#id_total_amount").val( accounting.toFixed(totalTotalInc, 2) );

			$("#total_vat").html( accounting.toFixed(totalVat, 2) );
			$("#id_total_vat").val( accounting.toFixed(totalVat, 2) );
		}

	}
}(InventoryReceived || {}, jQuery))

