$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
			$(data).modal("show");

			$("#submit_adjustment").on('click', function(e){
				StockCount.saveAdjustments($(this));
				return false;
			});

			initChosenSelect();
			initDatePicker();

        });
    });

	$("#stock_count>tr>td:not(.actions)").on("click", function(){
		document.location.href = $(this).parent("tr").data("detail-url");;
		return false;
	});

	$("#generate_next").on('click', function(e){
		ajaxSubmitForm($(this), $("#stock_count_sheet_form"));
		return false;
	});

	$("#generate_stock_count_report").on('click', function(e){
		ajaxSubmitForm($(this), $("#stock_count_sheet_form"));
		return false;
	});

	$("#capture_count").on('click', function(e){
		ajaxSubmitForm($(this), $("#capture_count_form"));
		return false;
	});

	$("#select_stock_items").on('click', function(){
		ajaxSubmitForm($(this), $("#select_stock_count_form"));
		return false;
	});

	$(".stock-count-variance").each(function(){
		var hasPositiveAdjustment = StockCount.handleVarianceStockCount();
		if (hasPositiveAdjustment) {
			$("#adjust_using_average_price").show();
			$("#adjust_using_last_price").show();
		} else {
			$("#adjust_using_average_price").hide();
			$("#adjust_using_last_price").hide();
		}
	});

	$("input[name='count_type']").on('change', function(e){
		StockCount.showHideCountCaptureInput($(this))
	});
});

var StockCount = (function(stockCount, $){

	return {

		saveAdjustments: function(el)
		{
			var priceType = $("input[name='price_type']");
			console.log('Price type', priceType.length)
			var selectedPriceType;
			if (priceType.length > 0) {
				selectedPriceType = $("input[name='price_type']:checked").val();
				if (selectedPriceType === undefined) {
					alert('Please select the price type to be applied');
				} else {
					ajaxSubmitForm(el, $("#save_adjustment_form"));
				}
			} else {
				ajaxSubmitForm(el, $("#save_adjustment_form"));
			}
		},

		handleVarianceStockCount: function()
		{

		},

		showHideCountCaptureInput: function(el)
		{
			let countType = el.prop('id').substr(11);
			$(".how-many").hide();
			$("#" + countType).show();
			console.log('CountType ->', countType);
			if (countType === 'select_stock') {
				$("#generate_next").show();
				$("#generate_stock_count_report").hide();
				// StockCount.loadStockItems();
			}
		},

		loadStockItems() {
			console.log('--------------loadStockItems-----------');
			let selectedBranch = $("#id_branch").val();
			let queryName = $("#inventory_name").val();
			let inventoryContainer = $("#select_stock_items");
			inventoryContainer.html("Loading inventory items ...");
			let ajaxUrl = inventoryContainer.data('ajax-url');
			$.get(ajaxUrl, {
				branch_id: selectedBranch,
				q: queryName
			}, function(responseHtml){
				inventoryContainer.html(responseHtml);

				// $("#inventory_name").on('keyup', _.debounce(StockCount.loadStockItems(), 300, {
				//   'leading': true,
				//   'trailing': false
				// }));
			});
		}

	}
}(StockCount || {}, jQuery));


