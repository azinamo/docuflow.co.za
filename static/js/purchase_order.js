$(document).ready(function() {
	//Form Submit for IE Browser

	$(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#decline_invoice_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='decline_invoice_form']"));
            });

            $("#create_vat_report_submit").on('click', function(event){
               event.preventDefault();
               submitModalForm($("form[name='create_vat_report']"));
			});

			$("#add_invoice_comment_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='comment_invoice_form']"));
            });

			$("#add_state_role_submit").on('click', function (event) {
                submitModalForm($("form[name='state_role_form']"));
                return false;
            });

			$("#add_invoice_account_comment_submit").on('click', function (event) {
                submitModalForm($("form[name='comment_invoice_account_form']"));
                return false;
            });

			$("#send_email_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='email_supplier_form']"));
            });

			$("#park_invoice_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='park_invoice_form']"));
            });

			$("#add_invoice_image_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='add_invoice_image_form']"));
            });

			$("#change_invoice_status_submit").on('click', function(event){
				$("#text-reason").show();
				var element = $("#id_approval_comment");
				if (element.val() === '') {
					$(".has-error").remove()
					element.addClass('is-invalid');
					element.after('<div class="is-invalid-message has-error">Comment is required</div>');
					$(element.parent()).parent().addClass('text-danger');
					return false;
				} else {
					event.preventDefault();
					submitModalForm($("form[name='approve_invoice_decline_form']"));
				}
				return false;
			});

			$("#approve_invoice_submit").on('click', function(event){
				event.preventDefault();
				Invoice.approveInvoiceDecline($(this));
			});

			$("#reject_invoice_submit").on('click', function(event){
				event.preventDefault();
				Invoice.rejectInvoice($(this));
			});

			$("#distribute_invoice_accounts").on("click", function(){
				event.preventDefault();
				Invoice.distributeAccounts($(this));
			});

			$("#create_invoice_account_distribution_submit").on('click', function(event){
				submitModalForm($("#create_invoice_account_distribution"));
				return false;
			});

			$("#delete_invoice_account_distribution").on('click', function () {
				Invoice.send($(this))
            });

			$("#create_distribution_journal_submit").on('click', function (e) {
				submitModalForm($("#create_distribution_journals"));
				return false;
            });

			initDatePicker();

			initChosenSelect();

        });
    });

	$(".validate_number").on('keyup', function(){
		PurchaseOrder.validateNumber($(this));
		return false;
	});

	$(".calculate_net").on('blur', function(){
		PurchaseOrder.calculateRowNet($(this));
	});

	$(".calculate_vat").on('change', function(){
		const el = $(this)
		if (el.val() !== '') {
			PurchaseOrder.calculateRowVat(el);
		}
	});

	$(".inventory_item").on('change', function(){
		PurchaseOrder.handleRow($(this).data("row-id"));
	});

	$(".cancel-purchase-order").on('click', function(e){
		const action = $(this).data('action');
		const params = {};
		if (action && action !== '') {
			params['action'] = action
		}
		ajaxGet($(this), params);
		e.preventDefault();
	});

	$("#sign_purchase_order").on('click', function(e){
		e.preventDefault();
		PurchaseOrder.signPurchaseOrder($(this));
	});

	$("#resubmit_purchase_order").on('click', function(e){
		PurchaseOrder.signPurchaseOrder($(this));
		e.preventDefault();
	});

    const purchaseOrderComments = $("#purchase_order_comments");
    const purchaseOrderContainer = $("#purchase_order_detail_container");
    const purchaseOrderLines = $("#purchase_orders");

	if (purchaseOrderLines.length > 0) {
		PurchaseOrder.loadPurchaserOrderLines(purchaseOrderLines);
		PurchaseOrder.loadPurchaserOrderForm($("#purchase_order_lines"));
	}
	if (purchaseOrderComments.length > 0) {
		PurchaseOrder.loadComments(purchaseOrderComments);
	}

	if (purchaseOrderContainer.length > 0) {
		PurchaseOrder.loadPurchaseOrderImage(purchaseOrderContainer)
	}

	const invoiceFlowContainer = $("#invoice_flows");
	if (invoiceFlowContainer.length) {
		PurchaseOrder.loadInvoiceFlows(invoiceFlowContainer);
	}

	$("#create_purchaseorder").on("click", function(e){
		e.preventDefault()
		ajaxSubmitForm($(this), $("#purchase_order_form"));
	});

	$("#cancel_purchase_order").on("click", function(e){
		PurchaseOrder.cancelPurchaseOrder($(this));
		return false;
	});

	const inventoryItemsContainer = $("#purchase_order_items");
	if (inventoryItemsContainer.length > 0) {
		PurchaseOrder.loadPurchaseOrderItems(inventoryItemsContainer.data('ajax-url'));
	}

	$("#add_line").on('click', function(e){
		e.preventDefault();
		var ajaxUrl = inventoryItemsContainer.data('ajax-url');
		if (ajaxUrl === undefined) {
			ajaxUrl = $(this).data('ajax-url');
		}
		$(this).prop('disabled', true);
		PurchaseOrder.loadPurchaseOrderItems(ajaxUrl);
	});

	const poInvoiceBody = $("#purchaseorder_items_body");
	if (poInvoiceBody.length > 0) {
		PurchaseOrder.loadPurchaseOrderItems(poInvoiceBody, poInvoiceBody.data("ajax-url"));
	}

	const idSupplier = $("#id_supplier");
	idSupplier.on('change', function(e){
		e.preventDefault();
		return $.getJSON($("#id_supplier option:selected").data('detail-url'), {
			module: 'purchaseorders'
		}, function(response){
			$("#supplier_address_line_1").html(response.address_line_1)
			$("#supplier_address_line_2").html(response.address_line_2)
		});
	});

	const idBranch = $("#id_branch");
	idBranch.on('change', function(e){
		e.preventDefault();
		return $.getJSON($("#id_branch option:selected").data('detail-url'), {
			module: 'purchaseorders'
		}, function(response){
			$("#branch_address_line_1").html(response.address_line_1)
			$("#branch_address_line_2").html(response.address_line_2)
			$("#branch_town").html(response.town)
			$("#branch_province").html(response.province)
		});
	});

	$(".bulk-action").on('click', function(e){
		e.preventDefault();
		PurchaseOrder.bulkInvoicesAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		PurchaseOrder.singlePurchaseOrderAction($(this), $(this).data('purchase-order-id'));
	});

});

var PurchaseOrder = (function(purchaseOrder, $){

	return {
		loadInvoiceFlows: function(el)
		{
			$.get($(el).data('ajax-url'), function(responseHtml){
				el.html(responseHtml);
			});
		},

		deleteInvoiceFlow: function(el)
		{
			$.getJSON($(el).data('ajax-url'), function(responseJson){
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			});
		},

		deleteInvoiceFlowRole: function(el)
		{
			$.getJSON($(el).data('ajax-url'), function(responseJson){
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			});
		},

		bulkAction: function(el)
		{
			const purchaseOrders = getSelectedItems('purchase-order-action');
			const purchaseOrdersLen = purchaseOrders.length;
			if (purchaseOrdersLen === 0) {
				alert('Please select at lease one purchaser order');
			} else {
				const confirm = el.data('confirm');
				if (confirm === '1') {
					return confirm(el.data('confirm-text'))
				}
				$.post(el.data('ajax-url'), {
					purchase_order: purchaseOrders
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singlePurchaseOrderAction: function(el, purchaseOrders)
		{
			const purchaseOrdersLen = purchaseOrders.length;
			if (purchaseOrdersLen === 0) {
				alert('Please select at lease one purchase order');
			} else if (purchaseOrdersLen > 1) {
				alert('Please select one purchase order to action');
			} else {
				$.get(el.data('ajax-url'), {
					purchase_order_id: purchaseOrders
				}, function(response) {
					responseNotification(response);
				});
			}
		},

		loadPurchaseOrderItems: function(container, ajaxUrl)
		{
			let rowId = 0;
			const lastRow = $(".po-item-row").last();
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			PurchaseOrder.loadInvoiceItemRow(ajaxUrl, rowId, lastRow, {})
		},

		loadInvoiceItemRow: function(ajaxUrl, rowId, lastRow, params)
		{
			const container = $("#purchaseorder_items_placeholder");
			params = params || {}
			params['row_id'] = rowId
			$.get(ajaxUrl, params, function(html) {
				if (lastRow !== undefined && lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				const _lastRow = $(".po-item-row").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}

				$(".po-items").each(function(){
					PurchaseOrder.handleRow($(this).data("row-id"));
				});

				$(".line-type").on("change", function(e){
					PurchaseOrder.reloadPurchaseOrderItemRow($(this));
				});
			});
		},

		reloadPurchaseOrderItemRow: function(el){
			const rowId = el.data('row-id');
			let params = {'rows': 1, 'reload': 1, 'row_id': rowId, 'line_type': el.val()}
			$.get(el.data('ajax-url'), params, function(html) {
				$("#po_item_" + rowId).replaceWith(html);

				PurchaseOrder.handleRow(rowId);
			});
		},

		loadPoItemRow: function() {
			let rowId = 0;
			const lastRow = $(".purchase_order_row").last();
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(ajaxUrl, {
				row: rowId
			}, function(html){
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				rowId = parseInt(rowId) + 1;

				$(".purchase_order_row").each(function(){
					PurchaseOrder.handleRow($(this).data("row-id"));
				});

				$(".delete-row").on('click', function(){
					$("#row_" + $(this).data("row-id")).remove();
				});

				$("#add_line").prop('disabled', false);

			});
		},

		deleteRow: function(rowId)
		{
			$("#po_item_" + rowId).remove();

			calculateItemPrices(rowId);
		},

		handleRow: function(rowId, options = {})
		{
			const lineDiscount = $("#line_discount");
			const discountPercentageEl = $("#discount_percentage_" + rowId);
			if (lineDiscount.val() !== "" && discountPercentageEl.val() === "") {
				discountPercentageEl.val(lineDiscount.val());
			}
			$(".search-box").each(function(index, el){
				const searchBoxEl = $(el);
				const model = searchBoxEl.data('model');
				const modelSearchEl = $(".model-" + model + "-search-" + rowId);
				const searchUrl = modelSearchEl.data("ajax-url");
				const detailUrl = modelSearchEl.data("ajax-detail-url");
				modelSearchEl.autocomplete({
					minLength: 1,
					source: searchUrl,
					focus: function( event, ui ) {
						$( "#" + model + "_item_" + rowId ).val(ui.item.description);
						return false;
					},
					select: function( event, ui ) {
						$( "#"+ model + "_" + rowId ).val(ui.item.id);
						$( "#"+ model + "_item_" + rowId ).val(ui.item.code);

						if (model === 'vat_code') {
							PurchaseOrder.handleVatSelection(ui.item, rowId);
						} else if (model === 'inventory') {
							$( "#"+ model + "_item_description_" + rowId ).html(ui.item.description);
							PurchaseOrder.handleInventory(model, ui.item.id, detailUrl, rowId, options);
						} else if (model === 'account') {
							PurchaseOrder.handleAccountLine(model, ui.item.id, detailUrl, rowId);
						} else if (model === 'unit') {

						}
						return false;
					}
				})
				.autocomplete( "instance" )._renderItem = function( ul, item ) {
					return $( "<li>" )
						.append( "<div>" + item.code + "<br>" + item.description + "</div>" )
						.appendTo( ul );
				};
			});

			$("#inventory_item_" + rowId).on("blur", function(){
				PurchaseOrder.validateLine($(this), rowId);
			})
			$("#account_item_" + rowId).on("blur", function(){
				PurchaseOrder.validateLine($(this), rowId);
			})

			$("#invoiced_" + rowId).on("blur", function() {
				handleInventoryQuantity($(this), 2);
				PurchaseOrder.handleQuantity(rowId);
			});

			$("#quantity_" + rowId).on("blur", function() {
				handleInventoryQuantity($(this), 2);
				PurchaseOrder.handleOrderedQuantity(rowId);
			});

			$("#price_" + rowId).on("blur", function() {
				if (PurchaseOrder.isValidLine(rowId)) {
					calculateItemPrices(rowId);
				}
			});

			$("#vat_code_" + rowId).on("change", function(){
				const vatPercentageEl = $("#vat_percentage_"+ rowId);
				const selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
				const vatPercentage = selectedVatOption.data("percentage");
				if (vatPercentage !== undefined && vatPercentage !== "") {
					vatPercentageEl.val(vatPercentage);
				} else {
					vatPercentageEl.val(0);
				}
				if (PurchaseOrder.isValidLine(rowId)) {
					calculateItemPrices(rowId);
				}
			});

			$("#discount_percentage_"+ rowId +"").on("blur", function(){
				if (PurchaseOrder.isValidLine(rowId)) {
					calculateItemPrices(rowId);
				}
			});

			$("#delete_" + rowId).on("click", function(){
				const deleteAction = $(this).data("action") || "remove";
				if (deleteAction === "delete") {
					if (confirm("Are you sure you want to delete this row")) {
						PurchaseOrder.deleteRow(rowId);
					}
				} else {
					PurchaseOrder.deleteRow(rowId);
				}
			});

			if (PurchaseOrder.isValidLine(rowId)) {
				calculateItemPrices(rowId);
			}

			$("#type_" + rowId).on("change", function(e){
				e.preventDefault();
				PurchaseOrder.reloadPurchaseOrderItemRow($(this));
			});
		},

		isValidLine: function(rowId) {
			const inventoryEl = $("#inventory_" + rowId);
			const accountEl = $("#account_" + rowId);
			if (hasElement(inventoryEl) && inventoryEl.val() === '') {
				return false;
			}
			if (hasElement(accountEl) && accountEl.val() === '') {
				return false;
			}
			return true;
		},

		validateLine: function(el, rowId){
			const inventoryEl = $("#inventory_" + rowId);
			const accountEl = $("#account_" + rowId);
			if (hasElement(inventoryEl) && inventoryEl.val() === '') {
				alert('Please select a valid inventory item');
				el.val(0)
				return false;
			}
			if (hasElement(accountEl) && accountEl.val() === '') {
				alert('Please select a valid account item');
				el.val(0)
				return false;
			}
		},

		handleAccountLine: function(model, modelId, detailUrl, rowId, options) {
			const params = {};
			const customerEl = $("#id_customer");
			const pricingEl = $("#id_pricing");
			params["account_id"] = modelId
			params['customer_id'] = customerEl.val();

			// $.get(detailUrl, params, function(responseData){
			//
			//
			// }, "json");
		},

		handleInventory: function(model, modelId, detailUrl, rowId, options)
		{
			const params = {};
			const customerEl = $("#id_customer");
			const pricingEl = $("#id_pricing");
			if (model === undefined) {
				params['inventory_id'] = modelId
			} else {
				params[model + "_id"] = modelId
			}
			params['customer_id'] = customerEl.val();
			if (pricingEl !== undefined) {
				params['pricing_id'] = pricingEl.val();
			}

			$.get(detailUrl, params, function(responseData){
				const customerLineDiscount = $("#line_discount").val();
				const price = responseData.selling_price;
				const priceIncluding = responseData.selling_price_with_vat;
				const unit = responseData.unit.code;
				const binLocation = responseData.bin_location;
				const description = responseData.text;
				const vatId = responseData.vat_code.id;
				const saleVatId = responseData.sale_vat_code.id;
				const unitId = responseData.unit.id;
				const isDiscountable = responseData.is_discounted;
				const allowNegativeStock = responseData.allow_negative_stocks;
				const netWeight = responseData.weight;
				const grossWeight = responseData.gross_weight;
				const averagePrice = responseData.history ? responseData.history.average_price : 0;
				let availableStock = responseData.history ? responseData.history.in_stock : 0;

				const priceEl =  $("#price_" + rowId);
				const priceIncEl =  $("#price_including_" + rowId);

				priceEl.val(price);
				priceIncEl.val(priceIncluding);
				if (unit !== undefined && unit !== "") {
					$("#unit_" + rowId).val(unit);
				}
				if (unitId !== undefined && unitId !== "") {
					$("#unit_id_" + rowId).val(unitId);
				}

				if (binLocation !== undefined && binLocation !== "") {
					$("#bin_location_" + rowId).val(binLocation)
				}
				if (description !== undefined && description !== "") {
					const descriptionEl = $("#inventory_description_" + rowId);
					if (descriptionEl) {
						descriptionEl.html(description)
					}
					$("#description_" + rowId).val(description)
				}
				if (vatId !== undefined && vatId !== "") {
					$("#vat_code_" + rowId).val(vatId);
					$("#vat_percentage_" + rowId).val(responseData.sale_vat_code.percentage);
					$("#vat_code_item_" + rowId).val(responseData.sale_vat_code.code);
				}
				handleRowDiscount(rowId, isDiscountable, customerLineDiscount);
				availableStock = parseFloat(availableStock);
				if (isNaN(availableStock)) {
					availableStock = 0;
				}
				if (availableStock > 0) {
					$("#order_item_" + rowId).html("No");
				} else {
					$("#order_item_" + rowId).html("Yes");
				}
				if (netWeight !== undefined) {
					$("#net_weight_" + rowId).val(netWeight);
				}
				if (grossWeight !== undefined) {
					$("#gross_weight_" + rowId).val(grossWeight);
				}
				if (averagePrice !== undefined) {
					$("#average_price_" + rowId).val(averagePrice);
				}
				$("#available_stock_" + rowId).html(accounting.formatMoney(availableStock));
				$("#available_in_stock_" + rowId).html(availableStock);
				$("#in_stock_" + rowId).val(availableStock);
				console.log(options)
				if ('add_type' in options && ['estimate'].indexOf(options.add_type) > -1) {
					$("#allow_negative_stock_" + rowId).val('1');
				} else {
					$("#allow_negative_stock_" + rowId).val(allowNegativeStock);
				}

			}, "json");
		},

		handleOrderedQuantity: function(rowId, quantityEl)
		{
			console.log("----------handleOrderedQuantity------------", rowId, " has  element ", hasElement(quantityEl));
			quantityEl = quantityEl || $("#quantity_" + rowId);
			const invoicedEl =  $("#invoiced_" + rowId);
			const quantityOrderedEl = $("#quantity_ordered_" + rowId) || $("#ordered_" + rowId);
			const backOrderEl = $("#back_order_" + rowId);
			const invoicedQty  = getFieldValue(invoicedEl);
			const orderedQty  = getFieldValue(quantityOrderedEl);
			const quantity = parseFloat(quantityEl.val());
			const inStock = getFieldValue($("#in_stock_" + rowId));
			const allowNegativeStock = 1;
			console.log("<---- is orderable ---->")
			if (isNaN(quantity)) {
			   invoicedEl.val(0);
			} else {
				invoicedEl.val(quantity);
			}
			console.log("Has back order el", hasElement(backOrderEl), '-> ', backOrderEl)
			if (hasElement(backOrderEl)) {
			   console.log('Invoiced quantity => ', quantity, ' vs ordered quantity => ', orderedQty, ' ==> ', (quantity < orderedQty))
			   if (quantity < orderedQty) {
				  backOrderEl.prop("disabled", false).prop("checked", true);
			   } else {
				  backOrderEl.prop("disabled", true).prop("checked", false);
			   }
			}

			calculateItemPrices(rowId);
		},

        loadUnits: function(rowId, measureId, unitsUrl, unitId)
		{
			measureId = parseInt(measureId);
			$.getJSON(unitsUrl, {
				measure: measureId
			}, function(response){
				var html = [];
				html.push("<option value=''>--</option>");
				response.map(function(item){
					if (unitId === item.id) {
						html.push("<option value='"+ item.id +"' selected='selected'>"+ item.name +"</option>");
					} else {
						html.push("<option value='"+ item.id +"'>"+ item.name +"</option>");
					}
				});
				$("#" + rowId + "_unit").html(html.join('')).trigger("chosen:updated");

			});
		},

		displayUnitdisplayUnit: function(el)
		{
			const rowId = el.attr('id').split('_')[0];
			const inventoryItemSelected = $("#" + el.attr('id') + " option:selected");
			const unit = inventoryItemSelected.data('unit');
			if (unit !== '') {
				$("#" + rowId + "_unit").val(unit);
			}
		},

		signPurchaseOrder: function(el)
		{
			const ajaxUrl = el.data('ajax-url');
			const ajaxSaveUrl = el.data('save-ajax-url');
			if (ajaxSaveUrl !== undefined) {
				var form = $("#edit_invoice");
				form.ajaxSubmit({
					url:  ajaxSaveUrl,
					target: '#loader',   // target element(s) to be updated with server response
					beforeSubmit:  function(formData, jqForm, options) {
						var queryString = $.param(formData);
					},  // pre-submit callback
					error: function (data) {
						var element;
						// show error for each element
						if (data.responseJSON && 'errors' in data.responseJSON) {
							$.each(data.responseJSON.errors, function (key, value) {
								element.addClass('is-invalid');
								element.after('<div class="is-invalid-message">' + value[0] + '</div>');
							});
						}

						// flash error message
						flash('danger', 'Errors have occurred.');
					},
					success:       function(responseJson, statusText, xhr, $form)  {
						$.getJSON(ajaxUrl, {
							csrf_token: window.csrftoken
						}, function(response) {
							if ('alert' in response) {
								if ('url' in response){
									ajaxNotificationDialog(response.url)
								} else {
									alertErrors(response.text)
								}
							} else {
								responseNotification(response)
							}
						});
					},  // post-submit callback
					dataType: 'json',
					type: 'post'
				});
			} else {
				$.getJSON(ajaxUrl, {
					csrf_token: window.csrftoken
				}, function(response) {
					if ('alert' in response) {
						if ('url' in response){
							ajaxNotificationDialog(response.url)
						} else {
							alertErrors(response.text)
						}
					} else {
						responseNotification(response)
					}
				});
			}
		},

		loadPurchaseOrderImage: function(el)
		{
			$.get(el.data('ajax-url'), function(html){
				$("#purchase_order_detail_container").html(html);
			});
		},

		loadComments: function(el)
		{
			$.get(el.data('ajax-url'), function(html){
				el.html(html);
			});
		},

		loadPurchaserOrderLines: function(el) {
			const purchaserOrderId = el.data('invoice-id');
			$.get(el.data('ajax-url'), {
				invoice_id: purchaserOrderId,
			}, function(html){
				el.replaceWith(html);

				$(".purchase-order-line>td").on('click', function(e){
					const purchaserOrderId = $(this).parent().data('purchase-order-line-id');

					$("#add_purchase_order_line").attr('disabled', 'disabled');
					$("#update_purchase_order_line").removeAttr('disabled');
					PurchaseOrder.loadPurchaserOrderForm($(this).parent(), purchaserOrderId);
					return false;
				});

				$(".delete-purchase-order").on('click', function(){
					if (confirm('Are you sure you want to delete this purchase order line')) {
						PurchaseOrder.deletePurchaseOrderLine($(this));
					}
					return false;
				});
			});
		},

		loadPurchaserOrderForm: function(el, lineId)
		{
			if (el) {
                const ajaxUrl = el.data('ajax-url');
                const lineId = el.data('purchase-order-line-id');
                $.get(ajaxUrl, {
                    line_id: lineId
                }, function(html){
                   $("#purchase_order_lines").html(html);

                   const quantity = $("#id_quantity");
                   const unitValue = $("#id_order_value");
                   const vatValueEl = $("#id_vat_amount");
                   const poVatAmountEl = $("#po_vat_amount");
                   const netAmountEl = $("#net_amount");
                   const totalAmountEl = $("#total_amount");
                   const selectedVatCode = $("#id_vat_code option:selected");

                   quantity.on('blur', function(e){
                       const netAmount = PurchaseOrder.calculateNet(quantity.val(), unitValue.val());
                       const totalAmount = PurchaseOrder.calculateTotalAmount(quantity.val(), unitValue.val(), poVatAmountEl.val());
                       const vatAmount = PurchaseOrder.calculateVat(netAmount, selectedVatCode.data('percentage'));
                   	   netAmountEl.val(netAmount);
					   totalAmountEl.val(totalAmount);
					   poVatAmountEl.val(vatAmount)
				   });

                   unitValue.on('blur', function(e){
                       const netAmount = PurchaseOrder.calculateNet(quantity.val(), unitValue.val());
                   	   const totalAmount = PurchaseOrder.calculateTotalAmount(quantity.val(), unitValue.val(), vatValueEl.val());
                       const vatAmount = PurchaseOrder.calculateVat(netAmount, selectedVatCode.data('percentage'));
                   	   netAmountEl.val(netAmount);
					   totalAmountEl.val(totalAmount);
					   poVatAmountEl.val(vatAmount)
				   });

                   $("#id_vat_code").on('change', function(){
                   	   const ajaxUrl = $(this).data('calculate-line-amount');
                   	   const vatCode = $(this).val();
                   	   $.ajax({
						   url: ajaxUrl,
						   data: {
							   'vat_code': vatCode,
							   'unit_amount': unitValue.val(),
							   'quantity': quantity.val()
						   },
						   dataType: 'json'
					   }).done(function(responseJson){
						   if ('vat_amount' in responseJson) {
						   	  $("#vat_amount_text").text( responseJson.vat_amount );
						   	  vatValueEl.val( responseJson.vat_amount );
						   }
					   }).then(function(){
							const totalAmount = PurchaseOrder.calculateTotalAmount(quantity.val(), unitValue.val(), vatValueEl.val());
							totalAmountEl.val(totalAmount);
					   });
				   });


                    $("#update_purchase_order_line").on('click', function(e){
                    	e.stopImmediatePropagation();
                        submitForm($("#purchaser_order_form"));
                    })

                    $("#add_purchase_order_line").on("click", function(e){
                        e.stopImmediatePropagation();
                        PurchaseOrder.savePurchaseOrderLine($("#purchase_order_form"));
                        e.preventDefault()
                    });

                    $("#id_amount").on('blur', function(e){
                        PurchaseOrder.calculateAmount($(this));
                    });

                    $(".math-operation").on('blur', function(e){
                        PurchaseOrder.calculateAmount($(this));
                    });

                    $(".money").on('blur', function(e){
                        PurchaseOrder.makeMoney($(this));
                    });

                    $(".chosen-select").chosen();
                });
            }
		},

		savePurchaseOrderLine: function(form)
		{
			form = form || $("#purchase_order_form");
			// disable extra form submits
			form.addClass('submitted');
			var loader = $("#loader");
			loader.show();
			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					loader.hide();
					if (responseJson.error) {
						if (responseJson.hasOwnProperty('errors')) {
							highlightFormErrors(responseJson.errors)
						}
						alertErrors(responseJson.text)
					} else {
						form.find(":input:not('select')").each(function(){
							$(this).val('')
						})
						form.find(".chosen-select").val('').trigger("chosen:updated");
						responseNotification(responseJson);
						var purchaseOrderLines = $("#purchase_orders");
						if (purchaseOrderLines) {
							PurchaseOrder.loadPurchaserOrderForm($("#purchase_order_lines"))
							$("#update_invoice_account_changes").attr('disabled', 'disabled');
							$("#add_invoice_account_posting").removeAttr('disabled');
							PurchaseOrder.loadPurchaserOrderLines(purchaseOrderLines);
						}

					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		deletePurchaseOrderLine: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, {
			}, function(responseJson){
				responseNotification(responseJson);
			}, 'json');
		},

		validateNumber: function(el){
			var value = el.val();
			if (value !== '') {
				if (isNaN(value)) {
					el.val('');
				}
			}
		},

		calculateVatAmount: function(totalAmount, netAmount){
			var vatAmount = parseFloat(totalAmount) - parseFloat(netAmount)
			if (vatAmount > 0) {
				$("#id_vat_amount").val(parseFloat(vatAmount).toFixed(2))
			}
		},

		calculateRowVat: function(el) {
			var rowId = el.attr('id').split('_')[0];

			var netTotal = $("#" + rowId + "_net_total").val();
			var vatSelected = $("#" + el.attr('id') + " option:selected");
			var percentage = vatSelected.data('percentage');
			if (netTotal !== '' && percentage !== '') {
				var vatAmount = (parseFloat(percentage)/100) * parseFloat(netTotal);
				var vatAmountStr = accounting.formatMoney(vatAmount);
				$("#row_vat_amount_" + rowId).html(vatAmountStr);
				$("#" + rowId + "_vat_amount").val(vatAmount);
			}

			PurchaseOrder.calculateOrderValue(rowId);
		},

		calculateRowNet: function(el) {
			var rowId = el.attr('id').split('_')[0];

			var quantity = $("#" + rowId + "_quantity").val();
			var value = $("#" + rowId + "_value").val();
			console.log("Quantity entered is ", quantity)
			if (quantity !== '') {
				PurchaseOrder.validateMinOrderQuantity(rowId, quantity);
				PurchaseOrder.validateMaxOrderQuantity(rowId, quantity);

				if (value !== '') {
					var netValue = PurchaseOrder.calculateNet(quantity, value);
					var netText = accounting.formatMoney(netValue);

					$("#row_net_total_" + rowId).html(netText);
					$("#" + rowId + "_net_total").val(netValue);

					PurchaseOrder.calculateRowVat($("#" + rowId + "_vat_code"))
				}
			}

			PurchaseOrder.calculateOrderValue(rowId);
		},

		validateMinOrderQuantity: function(rowId, quantity)
		{
			var inventoryItem = $("#" + rowId + "_inventory_item  option:selected");
			console.log("Min order validation ", inventoryItem)
			if (inventoryItem.length > 0) {
				var minOrder = inventoryItem.data('min-order');
				console.log( minOrder )
				if (minOrder !== undefined) {
					var minOrder = parseInt(minOrder)
					var qty = parseInt(quantity)
					console.log(" min order ", minOrder, " and qty ", qty)
					if (qty < minOrder) {
						$("#" + rowId + "_quantity").val( minOrder )
						alert("You have entered quantity " + qty+ " which is below minimum order ("+ minOrder + "), " +
							"for this item. Please adjust");
						return false;
					}
				}
			}
		},

		validateMaxOrderQuantity: function(rowId, quantity)
		{
			var inventoryItem = $("#" + rowId + "_inventory_item  option:selected");
			console.log("Max order validation ", inventoryItem)
			if (inventoryItem.length > 0) {
				var maxOrder = inventoryItem.data('max-order');
				if (maxOrder !== undefined) {
					var maxOrder = parseInt(maxOrder);
					var qty = parseInt(quantity);
					console.log(" max order ", maxOrder, " and qty ", qty)
					if (qty > maxOrder) {
						$("#" + rowId + "_quantity").val( maxOrder )
						alert("You have entered quantity " + qty+ " which is above maximum order ("+ maxOrder + "), " +
							"for this item. Please adjust.");
						return false;
					}
				}
			}
		},


		notifyPreferredSupplier: function(el)
		{
			var rowId = el.attr('id').split('_')[0];
			var inventoryItem = $("#" + rowId + "_inventory_item option:selected");
			if (inventoryItem.length > 0) {
				var preferredSupplier = inventoryItem.data('preferred-supplier');
				var preferredSupplierId = inventoryItem.data('preferred-supplier-id');
				var selectedSupplier = $("#id_supplier option:selected")
				if (preferredSupplierId !== undefined && selectedSupplier !== undefined) {
					var selectedSupplierId = parseInt(selectedSupplier.val())
					preferredSupplierId = parseInt(preferredSupplierId)

					if (preferredSupplierId !== selectedSupplierId) {
						alert("Your preferred supplier is  " + preferredSupplier + " which is different from the selected supplier");
						return false;
					}
				}
			}
		},

		calculateVat: function(netAmount, vatPercentage)
		{
			if (netAmount !== '' && vatPercentage !== '') {
                try {
                    if (isNaN(vatPercentage)) {
                        throw new  Error('Vat not valid')
                    }
                    var totalVat = parseFloat(vatPercentage)/100 * parseFloat(netAmount);
                    return totalVat.toFixed(2);
                } catch (e) {
                    console.log('Net ', netAmount, ' and value ', vatPercentage, ' did not valid');
                }
            }
		},

		calculateNet: function(quantity, value)
		{
			if (quantity !== '' && value !== '') {
                try {
                    if (isNaN(quantity)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    if (isNaN(value)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    var totalNet = parseFloat(quantity) * parseFloat(value);
                    return totalNet.toFixed(2);
                } catch (e) {
                    console.log('Quantity ', quantity, ' and value ', value, ' did not valid');
                }
            }
		},

		calculateTotalAmount: function(quantity, value, vatAmount)
		{
			console.log('calculating total for quantity ', quantity, ' and value ', value, ' and vat amount', vatAmount)
			if (quantity !== '' && value !== '') {
                try {
                    if (isNaN(quantity)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    if (isNaN(value)) {
                        throw new  Error('Quantity is not a valid number')
                    }
                    var total = parseFloat(quantity) * parseFloat(value);

					if (vatAmount !== '') {
						total += parseFloat(vatAmount);
					}

                    return total.toFixed(2);
                } catch (e) {
                    console.log('Quantity ', quantity, ' and value ', value, ' and vat amount ', vatAmount,' did not valid.', e.message);
                }
            }
		},

		calculateOrderValue: function(rowId){
			var net = $("#" + rowId + "_net_total").val();
			var vat = $("#" + rowId + "_vat_amount").val();
			var total = 0;

			if (net !== '') {
				total += parseFloat(net);
			}
			if (vat !== '') {
				total += parseFloat(vat);
			}
			var totalText = accounting.formatMoney(total);
			$("#row_total_amount_" + rowId).html(totalText);
			$("#" + rowId + "_total").val(total);

			var po_total = PurchaseOrder.calculatePoTotal($(".total"));
			var po_vat = PurchaseOrder.calculatePoTotal($(".vat"));
			var po_net = PurchaseOrder.calculatePoTotal($(".net"));

			$("#total_net_value").html(po_net);
			$("#id_net_amount").val(accounting.toFixed(po_net, 2));

			$("#total_vat_value").html(po_vat);
			$("#id_vat_amount").val(accounting.toFixed(po_vat, 2));

			$("#total_order_value").html(po_total);
			$("#id_total_amount").val(accounting.toFixed(po_total, 2));
			$("#id_open_amount").val(accounting.toFixed(po_total, 2));
		},

		calculatePoTotal: function(el) {
			var total  = 0
			el.each(function(){
				if ($(this).val() !== '') {
					total += parseFloat($(this).val())
				}
			});
			return accounting.formatMoney(total);
		},

		addPurchaseOrderLine: function(el)
		{
			var self  = this;
			var ajaxUrl = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response);
				self.loadInvoiceHistory($("#invoice_comments"));
			});
		},

		savePurchaseOrderRequest: function(el)
		{
			var self = this;
			var flowAjaxUrl = $(el).data('ajax-url');
			var form = $("#logger_edit_invoice");
			var ajaxUrl = $("#logger_save_invoice_changes").data('ajax-url');
			form.ajaxSubmit({
				url: ajaxUrl,
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}
				},
				success:       function(responseJson, statusText, xhr, $form)
				{
					if (responseJson.error) {
						responseNotification(responseJson)
					} else {
						$.getJSON(flowAjaxUrl, {
							csrf_token: window.csrftoken
						}, function(response){
							if (response.error) {
								alertErrors(response.details)
								if(response.errors) {
									highlightFormErrors(response.errors)
								}
								//responseNotification(response)
							}
							if ('redirect' in response) {
								setTimeout(function(){
									document.location.href = response.redirect
								}, 1500)
							}
						});
					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		},

		validateInvoice: function(el)
		{
			var self 	  = this;
			var ajaxUrl   = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response)
			});
		},


		addToFlow: function(el)
		{
			var self 	 = this;
			var ajaxUrl  = $(el).data('ajax-url');
			var invoices = [];
			$('.invoice-check').each(function(index){
				if ($(this).is(":checked")) {
					invoices.push( $(this).val() )
				}
			});
			if (invoices.length > 0) {
				$.getJSON(ajaxUrl, {
					csrf_token: window.csrftoken,
					invoices: invoices.join(',')
				}, function(responseJson){
					responseNotification(responseJson)
				});
			} else {
				responseNotification({'error': true, 'text': 'Select invoices to add to flow'})
			}
		},

		cancelPurchaseOrder: function(el)
		{
			const ajaxUrl = el.data('ajax-url');
			$.getJSON(ajaxUrl, {}, function(response){
				responseNotification(response)
			})
		}

	}
}(PurchaseOrder || {}, jQuery))
