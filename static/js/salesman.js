$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			initDatePicker();

			initChosenSelect();

			const saveBtn = $("#save_salesman");
			saveBtn.on("click", function(e){
				console.log('Save salesman')
				e.preventDefault();
				ajaxSubmitForm(saveBtn, $("#salesman_form"));
			});

        });
    });

    $("#id_from_salesman").on("change", function(){
    	Salesman.loadCustomers($(this).val());
	});

    $("#transfer").on("click", function(e){
    	e.preventDefault();
    	ajaxSubmitForm($(this), $("#transfer_salesman_form"));
	});

});


var Salesman = (function(salesman, $){

	return {

		loadCustomers: function(salesmanId)
		{
			const container = $("#customers_list")
			$.get(container.data('ajax-url'), {
				salesman_id: salesmanId
			}, function(html){
				container.html(html);

				$("#all_customers").on("click", function(e){
					if ($(this).is(":checked")) {
						$(".customers").prop('checked', true);
					} else {
						$(".customers").prop('checked', false);
					}
				});
			});
		}

	}

}(Salesman || {}, jQuery))

