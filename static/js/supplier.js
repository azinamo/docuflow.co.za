$(function(){

	$("#id_supplier").on("change", function(e) {
		Supplier.displayDetail($("#supplier_detail"), $(this).val());
	});

	$( "#tabs" ).tabs({
        beforeActivate: function( event, ui ) {
            initChosenSelect();
            initDatePicker();
        }
    });

	const supplierHistoryEl = $("#supplier_history");
	if (supplierHistoryEl.length > 0) {
	    Supplier.loadHistory(supplierHistoryEl);
    }
});


var Supplier = (function(supplier, $){

	return {

	    loadHistory(el) {
            $.getJSON(el.data('ajax-url'), function(response){
                new Chart(document.getElementById("supplier_history"),{
                    "type": "line",
                    "data":{
                        "labels":response.periods,
                        "datasets":[{"label":"History","data":response.data,"fill":false,"borderColor":"rgb(75, 192, 192)","lineTension":0.1}]
                    },"options":{}
                });
             });
        },

        displayDetail: function(el, supplierId)
        {
            $.get(el.data('ajax-url'), {
            	supplier_id: supplierId
			}, function(html){
               el.html(html);

               $.getJSON(el.data('ajax-history-url'), {
                     supplier_id: supplierId
               }, function(response){

                    const config = {
                        type: 'line',
                        fill: false,
                        data: {
                            labels: response['periods'],
                            datasets: [{
                                label: response['previous_year']['label'],
                                backgroundColor: 'rgb(255, 99, 132)',
                                borderColor: 'rgb(255, 99, 132)',
                                fill: false,
                                data: response['previous_year']['data'],
                            },{
                                label: response['year']['label'],
                                backgroundColor: 'rgb(54, 162, 235)',
                                borderColor: 'rgb(54, 162, 235)',
                                fill: false,
                                data: response['year']['data'],
                                lineTension: 0.1
                            }]
                        },
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Supplier History'
                            }
                        }
                    };

                    const ctx = document.getElementById('supplier_history').getContext('2d');
                    new Chart(ctx, config);
                    const html = []
                    console.log(response['current'])
                    console.log(response['current']['periods'])
                    $(response['current']['periods']).each(function(i, p){
                        html.push("<tr>");
                            html.push("<td>" + p['period'] + "</td>")
                            html.push("<td>" + p['name'] + "</td>")
                            html.push("<td class='text-right'>" + accounting.formatMoney(p['vat']) + "</td>")
                            html.push("<td class='text-right'>" + accounting.formatMoney(p['net']) + "</td>")
                            html.push("<td class='text-right'>" + accounting.formatMoney(p['total']) + "</td>")
                        html.push("</tr>");
                    });
                    $("#history_body").html(html.join(' '));
                    const footerHtml = [];
                    footerHtml.push("<tr>");
                        footerHtml.push("<td></td>")
                        footerHtml.push("<td></td>")
                        footerHtml.push("<td class='text-right'>" + accounting.formatMoney(response['current']['totals']['vat']) + "</td>")
                        footerHtml.push("<td class='text-right'>" + accounting.formatMoney(response['current']['totals']['net']) + "</td>")
                        footerHtml.push("<td class='text-right'>" + accounting.formatMoney(response['current']['totals']['total']) + "</td>")
                    footerHtml.push("</tr>");
                    $("#history_footer").html(footerHtml.join(' '));
               });
            });
        },
	}
}(Supplier || {}, jQuery));

