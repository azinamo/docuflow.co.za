$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');
            let minDate = new Date()
			$("#create_month_close_submit").on('click', function (event) {
                event.preventDefault();
                const form = $($(this).parent()).parent()
                submitModalForm(form);
            });

			$(".history_datepicker").datepicker({
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "yy-mm-dd",
				maxDate         : -1,
				yearRange       : "1900:"+(minDate.getFullYear())
			});

			const dataMinDate = $("#id_start_date").data('min-date');
			if (dataMinDate) {
				date = new Date(dataMinDate);
				minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : minDate,
					yearRange       : "-30:+100"
				});
			} else {
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : +1,
					yearRange       : "-30:+100"
				});

			}

			$(".chosen-select").chosen();

        });
    });

    $(document).on("change", "#id_period", function(e){
        // console.log('--Period--');
        // console.log($(this).val());
        // $("#id_period").find("option").each(function(){
        //   console.log($(this).val());
        // });
    });

    $(".accounts").on('change', function(e){
        const rowId = $(this).attr('id').replace('account_', '');
        Journal.loadAccountVatCodes($(this), $(this).val(), rowId);

        if ($(this).find(':selected').data('account-type') === 1) {
            $("#distribution_" + rowId).prop('disabled', false)
        } else {
            $("#distribution_" + rowId).prop('disabled', true)
        }
    })

    $(".vat_codes").on('change', function(e){
        $("#description_" + $(this).attr('id').replace('vat_codes_', '')).html($(this).data('description'));
    })

    $(".credit_amount").on('blur', function(){
        if ($(this).val() !== '') {
            $("#debit_" + $(this).attr('id').replace('credit_', '')).prop('disabled', true);
            Journal.calculateTotalCredit();
        }
    })

    $(".debit_amount").on('blur', function(){
        const rowId = $(this).attr('id').replace('debit_', '');
        if ($(this).val() !== '') {
            const amount = parseFloat( $(this).val() );
            if (amount < 0) {
                $("#credit_" + rowId).val(amount * -1);
                $("#debit_" + rowId).prop('disabled', true);
                $(this).val('');
                Journal.calculateTotalCredit();
            } else {
                 $("#credit_" + rowId).prop('disabled', true);
                Journal.calculateTotalDebit();
            }
        }
    });

    $(".distributions").on('change', function(){
        if ($(this).val() === 'yes') {
           Journal.openDistributionModal($(this), $(this).attr('id').replace('distribution_', ''));
        }
    });

    $("#save_close_month").on('click', function(e){
        e.preventDefault();
        Period.saveMonthClose($(this));
    });

    const monthEndPendingDistributionsEl = $("#distribution_pending");
    if (monthEndPendingDistributionsEl.length > 0) {
        Period.runDistributionJournal(monthEndPendingDistributionsEl);
    }

});


var Period = (function(period, $){

	return {
        runDistributionJournal: function(el)
        {
            el.html("processing distributions ...")
            $.get(el.data('ajax-url'), function(responseJson){
               responseNotification(responseJson);
            });
        },

        saveMonthClose: function(el)
        {
            $.getJSON(el.data('ajax-url'), function(responseJson){
               responseNotification(responseJson);
            });
        }
	}

}(Period || {}, jQuery))

