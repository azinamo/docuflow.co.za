$(document).ready(function() {
	//Form Submit for IE Browser
	$("#send_access_credentials").on('click', function(e){
		var password = $("#id_new_password_1").val();
		User.sendAccessCredentials($(this), password);
		e.preventDefault();
	});

	$("#update_user_account").on("click", function(e){
		e.preventDefault();
		ajaxSubmitForm($(this), $("#edit_user_account"))
	});


	const temporaryProfileContainer = $("#temporary_profiles");
	if (temporaryProfileContainer.length) {
		User.getTemporaryProfiles(temporaryProfileContainer);
	}

});


var User = (function(user, $){

	return {

		getTemporaryProfiles: function(el){
			el.html("loading temporary profiles ...");
			const ajaxUrl = el.data('ajax-url');
			$.get(ajaxUrl, function(responseHtml){
				el.html(responseHtml);
			});
		},

		sendAccessCredentials: function(el, password)
		{
			if (password) {
				var ajaxUrl = el.data('ajax-url');
				$.getJSON(ajaxUrl, {
					password: password
				}, function(response){
					responseNotification(response)
				});
			} else {
				alert('Please enter valid password')
			}
		}

	}
}(User || {}, jQuery));

