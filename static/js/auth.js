$(document).ready(function() {

    $("#company_login").validate({
        errorPlacement: function(error, element) {
            if (element.parent().parent().attr("class") === "checker" || element.parent().parent().attr("class") == "choice" ) {
              error.appendTo( element.parent().parent().parent().parent().parent() );
            }
            else if (element.parent().parent().attr("class") === "checkbox" || element.parent().parent().attr("class") == "radio" ) {
              error.appendTo( element.parent().parent().parent() );
            }
            else {
              error.insertAfter(element);
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $(element).parent().addClass( errorClass );
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass( errorClass );
        },
        submitHandler: function (form) {
            const loginBtn = $("#login");
            const resultBox = $("#result");
            const formEl = $("#company_login");
            const ajaxUrl = formEl.data('ajax-url');
            loginBtn.html("Processing ...").addClass('loading');
            formEl.ajaxSubmit({
               url: ajaxUrl,
               dataType: 'json',
               success: function(response)
               {
                    loginBtn.html("Login");
                    if ('redirect_url' in response && response.redirect_url !== '') {
                        $("#result").addClass('text-success').html(response.text).addClass('text-success')
                        document.location.href = response.redirect_url
                    } else {
                        resultBox.html(response.text).addClass('text-danger')
                    }
               },
               error: function (response, statusText, xhr) {
                   console.log(statusText)
                   console.log(response)
                    loginBtn.html("Login");
                    if (response.responseJSON.hasOwnProperty('errors')) {
                        let messages = [];
                        $.each(response.responseJSON.errors, function(k, errors){
                            $.each(errors, function(i, error){
                                messages.push(error)
                            });
                        })
                        resultBox.html(messages.join('<br />')).addClass('text-danger')
                    } else {
                        resultBox.html(response.responseJSON.text).addClass('text-danger')
                    }
               },
            })
        }

    });

    $("#sign_up").on("click", function(e){
        e.preventDefault();
        Authentication.saveCompany($(this));
    });
        // $("#sign_up").validate({
        //     errorPlacement: function(error, element) {
        //         if (element.parent().parent().attr("class") == "checker" || element.parent().parent().attr("class") == "choice" ) {
        //           error.appendTo( element.parent().parent().parent().parent().parent() );
        //         }
        //         else if (element.parent().parent().attr("class") == "checkbox" || element.parent().parent().attr("class") == "radio" ) {
        //           error.appendTo( element.parent().parent().parent() );
        //         }
        //         else {
        //           error.insertAfter(element);
        //         }
        //     },
        //     highlight: function ( element, errorClass, validClass ) {
        //         $( element ).parent().addClass( errorClass );
        //     },
        //     unhighlight: function (element, errorClass, validClass) {
        //         $( element ).parent().removeClass( errorClass );
        //     },
        //     submitHandler: function (form) {
        //         var loginBtn = $("#login");
        //         loginBtn.html("Processing ...");
        //         $("#appbundle_reservation_submit").addClass('loading');;
        //         var ajaxUrl = $("#company_login").data('ajax-url');
        //         $.post(ajaxUrl, {
        //             username: $("#id_username").val(),
        //             password: $("#id_password").val()
        //         }, function(response, status, txt){
        //             loginBtn.html("Login");
        //             if ('redirect_url' in response && response.redirect_url !== '') {
        //                 $("#result").html(response.text).addClass('text-success')
        //                 document.location.href = response.redirect_url
        //             } else {
        //                 $("#result").html(response.text).addClass('text-danger')
        //             }
        //         }, 'json');
        //     }
        //
        // });

});


var Authentication = (function(authentication, $){

	return {

	    saveCompany: function(el)
        {
            $("#sign_up_form").LoadingOverlay("show", {'text': 'Processing ...'});

            ajaxSubmitForm(el, $("#sign_up_form"));

            //$("#sign_up_form").LoadingOverlay("hide");
        }

	}

}(Authentication || {}, jQuery));

