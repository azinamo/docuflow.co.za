$(document).ready(function() {

	$(document).on("click", "#save", function(e){
		ajaxSubmitForm($(this), $("#payroll_setup"));
	});

	const payrollSetupBody = $(".payroll-setup-body");
	if (payrollSetupBody.length > 0) {
		$(".payroll-setup-body").each(function(){
			Payroll.loadPayrollSetupItems($(this));
		})
	}

	$(document).on('click', '.add-payroll-setup-item', function(e){
		e.preventDefault();
		const setupEl = $(this);
		setupEl.prop('disabled', true).data('html', setupEl.text()).text('loading ...');
		let params = {'rows': 1}
		Payroll.loadPayrollSetupItems(setupEl, params);
	});

	Payroll.initAccountsAutocomplete()

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");

            Payroll.initAccountsAutocomplete();
        });
    });

	$(document).on("change", ".include-in-salary", function(){
		const rowId = $(this).data('row-id');
		const isDisabled = $(this).is(":checked");
		$("#statutory_account_code_" + rowId).prop('disabled', isDisabled);
	});

});


var Payroll = (function (payroll, $) {

	return {

		initAccountsAutocomplete() {
			$(".search-account-box").each(function(index, el){
				const searchBoxEl = $(el);
				const itemType = searchBoxEl.data('item-type');
				const rowId = searchBoxEl.data('row-id');
				const searchUrl = searchBoxEl.data("ajax-url");
				searchBoxEl.autocomplete({
					minLength: 1,
					source: searchUrl,
					focus: function(event, ui) {
						return false;
					},
					select: function(event, ui) {
						$( "#"+ itemType +"_account_code_" + rowId ).val(ui.item.code);
						$( "#"+ itemType +"_account_" + rowId ).val(ui.item.id);
						$( "#"+ itemType +"_account_description_" + rowId ).html(ui.item.name);
						return false;
					}
				})
				.autocomplete( "instance" )._renderItem = function( ul, item ) {
					return $( "<li>" )
						.append( "<div>" + item.label + "</div>" )
						.appendTo( ul );
				};
			});
		},

		loadPayrollSetupItems(el, params = {}) {
			const itemType = el.data('item-type')
			const lastRow = $(".setup-item-row-" + itemType).last();
			let rowId = 0;
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			Payroll.loadSetupItemRow(el, itemType, rowId, lastRow, params)
		},

		loadSetupItemRow(el, itemType, rowId, lastRow, params = {}) {
			const container = $("#" + itemType + "_payroll_setup_placeholder");
			params = params || {}
			params['row_id'] = rowId
			$.get(el.data('ajax-url'), params, function(html) {
				if (lastRow !== undefined && lastRow.length > 0) {
					lastRow.after(html)
					container.hide();
				} else {
					container.replaceWith(html);
				}

				Payroll.initAccountsAutocomplete()


				// const _lastRow = $(".setup-item-row-" + itemType).last();
				// if (_lastRow.length > 0) {
				// 	rowId = parseInt(_lastRow.data('row-id'));
				// }
				//
				// $(".sales-items").each(function(){
				// 	salesItem.handleRow($(this).data("row-id"));
				// });
				//
				// $(".line-type").on("change", function(e){
				// 	salesInvoice.reloadInvoiceItemRow($(this));
				// });

				el.prop('disabled', false).text(el.data('text'));
			});
		}
	}

}(Payroll || {}, jQuery));

