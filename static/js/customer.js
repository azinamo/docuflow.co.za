$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");

			var savePricingBtn = $("#save_pricing");
			var saveAddressBtn = $("#save_address");
			var saveBtn = $(".save-btn");

			saveAddressBtn.on("click", function(e){
				e.preventDefault();
				var addressForm = $("#address_form");
				addressForm.LoadingOverlay('show', {
					'text': 'Processing pricing'
				});
				ajaxSubmitForm(saveAddressBtn, addressForm);
			});

			savePricingBtn.on("click", function(){
				var pricingForm = $("#pricing_form");
				pricingForm.LoadingOverlay('show', {
					'text': 'Processing pricing'
				});
				ajaxSubmitForm(savePricingBtn, pricingForm);
			});

			saveBtn.on("click", function(e){
				console.log("id --> ", saveBtn.attr('id') )
				var submitForm = $("#" + saveBtn.attr('id') + "_form");
				var text = saveBtn.data('text');
				console.log(text);
				console.log("\r\n\n\n---------------\r\n\n");
				submitForm.LoadingOverlay('show', {
					'text': text
				});
				ajaxSubmitForm(saveBtn, submitForm);
			});

			$("#submit_delete").on("click", function(e){
				e.preventDefault();
				ajaxSubmitForm($(this), $("#delete_address_form"))
			});

			initDatePicker();
			initChosenSelect();
        });
    });

	$("#customers>tbody>tr>td:not(.actions)").on("click", function(){
		var detailUrl = $(this).parent("tr").data("detail-url");
		document.location.href = detailUrl;
		return false;
	});

	$("#id_customer").on("change", function(e) {
		Customer.displayDetail($("#customer_detail"), $(this).val());
	});

	$("#id_postal_country").on("change", function(e) {
		console.log('selected value ', $(this).val())
		const selectProvinceEl = $("#select_province");
		const enterProvinceEl = $("#enter_province");
		if ($(this).val() === 'ZA') {
			selectProvinceEl.show();
			enterProvinceEl.hide();
		} else {
			selectProvinceEl.hide();
			enterProvinceEl.show();
		}
	});

	$("#save_customer").on("click", function(e){
		ajaxSubmitForm($(this), $("#customer_form"));
		return false;
	});

	var paymentTerm = $("#id_payment_term");
	var statementDays = $("#statement_days");
	var invoiceDays = $("#invoice_days");
	var idDays = $("#id_days");
	var idStatementDays = $("#id_statement_days");
	paymentTerm.on("change", function(){
		var paymentTermVal = $(this).val();
		console.log("Value --> ", paymentTermVal, ' type --> ', typeof paymentTermVal);
		if (paymentTermVal === '1') {
			invoiceDays.removeClass('hide').addClass('show');
			statementDays.removeClass('show').addClass('hide');
			$("#id_credit_limit").prop('disabled', false);
		} else if (paymentTermVal === '3') {
			statementDays.removeClass('hide').addClass('show');
			invoiceDays.removeClass('show').addClass('hide');
			$("#id_credit_limit").prop('disabled', true);
		} else {
			invoiceDays.removeClass('show').addClass('hide');
			statementDays.removeClass('hide').addClass('show');
			$("#id_credit_limit").prop('disabled', false);
		}
	});
	if (idDays.is(":hidden")) {
		idStatementDays.val(idDays.val());
	}
	idStatementDays.on("change", function(){
		idDays.val($(this).val());
	});

	var customerCreditLimitEl = $("#id_credit_limit");
	customerCreditLimitEl.on("blur", function(){
		Customer.calculateAvailableCredit($(this));
	});

	var dfNumber = $("#id_df_number");
	dfNumber.on("blur", function(){
		Customer.getAndShowSuppliers($(this));
	});

	Customer.getAndShowSuppliers(dfNumber);

	$(".bulk-action").on('click', function(e){
		e.preventDefault();
		Customer.bulkAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		Customer.singleAction($(this));
	});

	$(".action").on('click', function(e){
		e.preventDefault();
		Customer.customerAction($(this));
	});

	$("#pdf").on("click", function(e){
		e.preventDefault()
		Customer.downloadPdf($(this));
	});
});


var Customer = (function(customer, $){

	return {

        displayDetail: function(el, customerId)
        {
            $.get(el.data('ajax-url'), {
            	customer_id: customerId
			}, function(html){
               el.html(html);

               $.getJSON(el.data('ajax-history-url'), {
                     customer_id: customerId
               }, function(response){

                    const config = {
                        type: 'line',
                        fill: false,
                        data: {
                            labels: response['periods'],
                            datasets: [{
                                label: response['previous_year']['label'],
                                backgroundColor: 'rgb(255, 99, 132)',
                                borderColor: 'rgb(255, 99, 132)',
                                fill: false,
                                data: response['previous_year']['data'],
                            },{
                                label: response['year']['label'],
                                backgroundColor: 'rgb(54, 162, 235)',
                                borderColor: 'rgb(54, 162, 235)',
                                fill: false,
                                data: response['year']['data'],
                                lineTension: 0.1
                            }]
                        },
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Customer History'
                            }
                        }
                    };

                    const ctx = document.getElementById('customer_history').getContext('2d');
                    new Chart(ctx, config);
                    const html = []
                    console.log(response['current'])
                    console.log(response['current']['periods'])
                    $(response['current']['periods']).each(function(i, p){
                        html.push("<tr>");
                            html.push("<td>" + p['period'] + "</td>")
                            html.push("<td>" + p['name'] + "</td>")
                            html.push("<td class='text-right'>" + accounting.formatMoney(p['vat']) + "</td>")
                            html.push("<td class='text-right'>" + accounting.formatMoney(p['net']) + "</td>")
                            html.push("<td class='text-right'>" + accounting.formatMoney(p['total']) + "</td>")
                        html.push("</tr>");
                    });
                    $("#history_body").html(html.join(' '));
                    const footerHtml = [];
                    footerHtml.push("<tr>");
                        footerHtml.push("<td></td>")
                        footerHtml.push("<td></td>")
                        footerHtml.push("<td class='text-right'>" + accounting.formatMoney(response['current']['totals']['vat']) + "</td>")
                        footerHtml.push("<td class='text-right'>" + accounting.formatMoney(response['current']['totals']['net']) + "</td>")
                        footerHtml.push("<td class='text-right'>" + accounting.formatMoney(response['current']['totals']['total']) + "</td>")
                    footerHtml.push("</tr>");
                    $("#history_footer").html(footerHtml.join(' '));
               });
            });
        },

		downloadPdf(el)
		{
			$.get(el.data('ajax-url'), function(response, status, xhr){
				// check for a filename
				var filename = "";
				var disposition = xhr.getResponseHeader('Content-Disposition');
				if (disposition && disposition.indexOf('attachment') !== -1) {
					var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
					var matches = filenameRegex.exec(disposition);
					if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
				}

				var type = xhr.getResponseHeader('Content-Type');
				var blob = new Blob([response], { type: type });

				if (typeof window.navigator.msSaveBlob !== 'undefined') {
					// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
					window.navigator.msSaveBlob(blob, filename);
				} else {
					var URL = window.URL || window.webkitURL;
					var downloadUrl = URL.createObjectURL(blob);

					if (filename) {
						// use HTML5 a[download] attribute to specify filename
						var a = document.createElement("a");
						// safari doesn't support this yet
						if (typeof a.download === 'undefined') {
							window.location = downloadUrl;
						} else {
							a.href = downloadUrl;
							a.download = filename;
							document.body.appendChild(a);
							a.click();
						}
					} else {
						window.location = downloadUrl;
					}
				}
				setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
			});
		},

		bulkAction: function(el)
		{
			const customers = Customer.getSelectedCustomers();
			const totalCustomers = customers.length;
			if (totalCustomers === 0) {
				alert('Please select at least one customer');
			} else {
				const ajaxUrl = el.data('ajax-url');
				const confirm = el.data('confirm');
				if (confirm === '1') {
					const confirmText = el.data('confirm-text')
					return confirm(confirmText)
				}
				$.post(ajaxUrl, {
					customers: customers
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singleAction: function(el)
		{
			const customers = Customer.getSelectedCustomers();
			const totalCustomers = customers.length;
			if (totalCustomers === 0) {
				alert('Please select at least one customer');
			} else if (totalCustomers > 1) {
				alert('Please select one customer to action');
			} else {
				const ajaxUrl = el.data('ajax-url');
				$.post(ajaxUrl, {
					customer_id: customers[0]
				}, function(response){
					responseNotification(response);
				});
			}
		},

		getSelectedCustomers: function()
		{
			let customers = []
			$(".customer-action").each(function(){
				if ($(this).is(":checked")) {
                    customers.push($(this).data('customer-id'));
                }
			});
			return customers;
		},

		customerAction: function(el)
		{
			const ajaxUrl = el.data('ajax-url');
			$.getJSON(ajaxUrl, function(response, status, xhr){
				responseNotification(response)
			});
		},

		getAndShowSuppliers: function(el)
		{
			console.log('--------- getAndShowSuppliers ----------')
			let dfNumber = el.val();
			let idSupplier = $("#id_supplier");
			idSupplier.empty();
			$("#id_supplier_chosen").empty();
			if (dfNumber !== '') {
				console.log('--------- We have df number ', dfNumber ,' ----------');
				$("#df_supplier").show();
				idSupplier.on("blur", function(){
					let supplierCode = $(this).val();
					if ( supplierCode !== '') {
						$("#search_loader").html('Searching ....');
						$("#supplier_response").html("")
						let supplierUrl = idSupplier.data('ajax-url');

						console.log('--------- Supplier url  ', supplierUrl ,' ----------', supplierCode);
						$.getJSON(supplierUrl, {
							df_number: dfNumber,
							term: supplierCode
						}, function(supplier) {
							if (supplier.data) {
								console.log(supplier.data.id)
								$("#id_supplier_id").val(supplier.data.id);
								$("#supplier_response").removeClass('text-error').addClass('text-success').html('Supplier found')
							} else {
								$("#supplier_response").html('Supplier not found').addClass('text-error').removeClass('text-success');
							}
							$("#search_loader").html('');
						});
					} else {
						$("#supplier_response").html('Enter valid supplier code').addClass('text-error').removeClass('text-success');
						$("#search_loader").html('');
					}
				});
			} else {
				$("#df_supplier").hide();
			}
		},

		calculateAvailableCredit: function (el) {
			console.log("----------calculateAvailableCredit-------------------");
			var creditCart = accounting.unformat(el.val());
			var totalInvoiced = accounting.unformat($("#total_invoiced").val());
			var availableCredit = accounting.unformat($("#available_credit").val());

			var available = creditCart - totalInvoiced;
			$("#available_credit").html( accounting.toFixed(available, 0));
			$("#id_available_credit").val( accounting.toFixed(available, 0))
		}
	}

}(Customer || {}, jQuery))

