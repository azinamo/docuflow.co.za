$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
		let self = $(this);
		Budget.destroyExistingModals(self).then(() => {
			Budget.openModal(self);
		});
        event.preventDefault();
    });

	$(".save-budget").on("click", function(e){
		e.preventDefault();
		ajaxSubmitForm($(this), $("#create_budget"));
	});

	$("#budget_type_individual").on("change", function(){
		console.log("budget_type_individual")
		if ($(this).is(":checked")) {
			$("#company_objects").css({'visibility': 'visible'});
		} else {
			$("#company_objects").css({'visibility': 'hidden'});
		}
		initChosenSelect()
	});

	$("#budget_type_all").on("change", function(){
		if ($(this).is(":checked")) {
			$("#company_objects").css({'visibility': 'hidden'});
		}
		initChosenSelect()
	});

	var accountBudgetContainer = $("#budget_accounts");
	if (accountBudgetContainer.length > 0) {
		Budget.loadBudgetAccounts(accountBudgetContainer, 1);
	}

	$(".previous-account").on("click", function(e){
		$(this).prop('disabled', true)
		e.preventDefault();
		var currentPage = parseInt($("#page").val());
		var prevPage = currentPage - 1;
		var totalPages = parseInt($("#total_pages").val());
		console.log("Prev click")
		console.log("Current page --> ", currentPage);
		console.log("Prev page --> ", prevPage);
		if (prevPage === 1) {
			$(".prev").hide();
		} else {
			if (prevPage === totalPages) {
				$(".next").hide();
			} else {
				$(".next").show();
			}
		}
		Budget.loadBudgetAccounts(accountBudgetContainer, prevPage)
		$(this).prop('disabled', false)
	});


	$(".next-account").on("click", function(e){
		e.preventDefault();
		var currentPage = parseInt($("#page").val());
		var nextPage = currentPage + 1;
		if (nextPage > 1) {
			$(".prev").show();
		} else {
			$(".prev").hide();
		}
		console.log("Current page is ", currentPage);
		console.log("Next page is ", nextPage);

		Budget.saveAccountBudget($(this), nextPage);
	});

	$("#update_account_budget").on('click', function(){
		Budget.saveAccountBudget($(this));
	});

});


var Budget = (function(budget, $){

	return {

		openModal(el) {
			$.get(el.data("modal"), function (data) {
				$(data).modal("show");
				var form = $(data).find("form");

				$("#submit_budget").on('click', function(e){
					e.preventDefault();
					ajaxSubmitForm($(this), $("#account_budget"));
				});

				$(".budget-amount").on("blur", function(e){
					let val = $(this).val();
					let periodId = $(this).data('period-id');
					if (!isNaN(val)) {
						Budget.calculateAccountTotal(periodId);
					} else {
						$(this).val('0');
					}
				});

				$("#period_totals").on('keyup', function(e){
					console.log('------ activateSplitBudgetBtn ---------');
					Budget.activateSplitBudgetBtn();
				});

				$("#split_total").on('click', function(e){
					e.preventDefault();
					console.log('------ splitPeriodBudget ---------');
					Budget.splitPeriodBudget();
				});

				initDatePicker();
				initChosenSelect()

			});
		},

		destroyExistingModals : async function(el){
			await $(".modal-ajax").each(function(){
				$(this).remove();
			});
		},

		splitPeriodBudget: function(el)
		{
			console.log('------ splitPeriodBudget ---------');
			let budgetAmounts = $(".budget-amount");
			let periodTotal = $("#period_totals");
			let periods = budgetAmounts.length;
			let totalAmount = parseFloat(periodTotal.val());
			let remainder = totalAmount%periods;
			console.log('Periods --> ', periodTotal);
			console.log('Total amount --> ', totalAmount);
			console.log('Remainder --> ', remainder);
			if (!isNaN(totalAmount)) {
				let accumulatedAmount = 0;
				let remainderAmount = parseInt(remainder);
				let periodAmount = ((totalAmount-remainderAmount)/periods).toFixed(2);
				let lastPeriodIndex = periods-1;
				let startAddIndex = lastPeriodIndex - remainderAmount;
				budgetAmounts.each(function(index, el) {
					let element = $(el);
					let periodId = element.data('period-id');
					console.log(periodId, index, startAddIndex, periodAmount, accumulatedAmount);
					if (index > startAddIndex) {
						let amount = parseFloat(periodAmount+1);
						accumulatedAmount += amount;
						element.val(amount);
					} else {
						accumulatedAmount += parseFloat(periodAmount);
						element.val(periodAmount);
					}
					Budget.addAccumulatedAmount(periodId, accumulatedAmount, totalAmount)
				});
			}
		},

		activateSplitBudgetBtn: function()
		{
			console.log('------ activateSplitBudgetBtn ---------');
			$("#split_total").prop('disabled', false);
		},

		saveAccountBudget: function(el, pageNum)
		{
			var accountBudgetForm = $("#account_budget");

			accountBudgetForm.LoadingOverlay('show', {
				'text': 'Updating ...'
			});
			// disable extra form submits
			accountBudgetForm.addClass('submitted');
			el.css('width', el.outerWidth());
			el.attr('data-original-html', el.html());
			el.html('<i class="fa fa-spinner fa-spin"></i>');
			el.prop('disabled', true);

			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			accountBudgetForm.ajaxSubmit({
				url: accountBudgetForm.attr('action'),
				target: '#loader',
				// pre-submit callback
				error: function (data) {
					// handle errors
					var error = {'error': true, 'text': data.statusText};
					responseNotification(error);
					el.prop('disabled', false).html(el.data('data-original-html'));
					accountBudgetForm.LoadingOverlay("hide");
				},
				success: function(responseJson, statusText, xhr, $form) {
					responseNotification(responseJson);


					el.css('width', el.outerWidth());
					el.html(el.data('original-html'));
					el.prop('disabled', false)
					accountBudgetForm.LoadingOverlay("hide");

					if (pageNum !== undefined && pageNum > 0) {
						Budget.loadBudgetAccounts($("#budget_accounts"), pageNum)
					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		},

		loadBudgetAccounts: function(container, page=0)
		{
			var ajaxUrl = container.data('ajax-url');
			container.LoadingOverlay('show', {
                text: 'Loading . . . '
            });
			$.get(ajaxUrl, {
				page: page
			}, function(html){
				container.html(html);

				$("#page").val(page);
				$("#page_num").html(page)

				container.LoadingOverlay('hide');


				$(".budget-amount").on("blur", function(e){
					let val = $(this).val();
					let periodId = $(this).data('period-id');
					if (!isNaN(val)) {
						Budget.calculateAccountTotal(periodId);
					} else {
						$(this).val('0');
					}
				});

				$("#period_totals").on('keyup', function(){
					console.log('------ activateSplitBudgetBtn ---------');
					Budget.activateSplitBudgetBtn();
				});

				$("#split_total").on('click', function(e){
					e.preventDefault();
					console.log('------ splitPeriodBudget ---------');
					Budget.splitPeriodBudget();
				});

			});
		},

		calculateAccountTotal: function(periodId)
		{
			let total = 0;
			let accumulatedAmount = 0;
			$(".budget-amount").each(function(index, el){
				let lineAmount = $(this).val();
				if (lineAmount !== '') {
					total += accounting.unformat(lineAmount);
				}
			});

			$(".budget-amount").each(function(index, el){
				let _periodId = $(this).data('period-id');
				let lineAmount = $(this).val();
				console.log('Line amount --> ', _periodId, lineAmount);

				accumulatedAmount += parseFloat(lineAmount);
				console.log('Acc amount --> ', _periodId, accumulatedAmount, total);
				Budget.addAccumulatedAmount(_periodId, accumulatedAmount, total);
			});
			$("#period_totals").val(total);
			$("#account_total").html(accounting.format(total));

		},

		addAccumulatedAmount: function(periodId, accumulatedAmount, totalAmount)
		{
			console.log('-----addAccumulatedAmount---');
			let accumulatedPercentage = accounting.toFixed(accumulatedAmount/totalAmount, 2);
			$("#accumulated_amount_" + periodId).html(accounting.format(accumulatedAmount));
			$("#accumulated_percentage_" + periodId).html(accumulatedPercentage*100);
		}
	}
}(Budget || {}, jQuery))

