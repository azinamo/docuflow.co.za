$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            // $("#approve_invoice_submit").on('click', function (event) {
            //     event.preventDefault();
            //     submitModalForm($("form[name='approve_invoice_form']"));
            // });

            $("#decline_invoice_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='decline_invoice_form']"));
            });

            $("#create_vat_report_submit").on('click', function(event){
               event.preventDefault();
               submitModalForm($("form[name='create_vat_report']"));
			});

			$("#add_invoice_comment_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='comment_invoice_form']"));
            });

			$("#add_state_role_submit").on('click', function (event) {
                submitModalForm($("form[name='state_role_form']"));
                return false;
            });

			$("#add_invoice_account_comment_submit").on('click', function (event) {
                submitModalForm($("form[name='comment_invoice_account_form']"));
                return false;
            });

			$("#send_email_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='email_supplier_form']"));
            });

			$("#park_invoice_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='park_invoice_form']"));
            });

			$("#add_invoice_image_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='add_invoice_image_form']"));
            });

			$("#change_invoice_status_submit").on('click', function(event){
				$("#text-reason").show();
				var element = $("#id_approval_comment");
				if (element.val() === '') {
					$(".has-error").remove()
					element.addClass('is-invalid');
					element.after('<div class="is-invalid-message has-error">Comment is required</div>');
					$(element.parent()).parent().addClass('text-danger');
					return false;
				} else {
					event.preventDefault();
					submitModalForm($("form[name='approve_invoice_decline_form']"));
				}
				return false;
			});

			$("#approve_invoice_submit").on('click', function(event){
				event.preventDefault();
				Invoice.approveInvoiceDecline($(this));
			});

			$("#reject_invoice_submit").on('click', function(event){
				event.preventDefault();
				Invoice.rejectInvoice($(this));
			});

			$($(data).find("form")).find(".modal-footer :submit")

			$("#distribute_invoice_accounts").on("click", function(){
				event.preventDefault();
				Invoice.distributeAccounts($(this));
			});

			$("#create_invoice_account_distribution_submit").on('click', function(event){
				submitModalForm($("#create_invoice_account_distribution"));
				return false;
			});

			$("#delete_invoice_account_distribution").on('click', function () {
				Invoice.send($(this))
            });
			initDatePicker();

			$("#create_distribution_journal_submit").on('click', function (e) {
				submitModalForm($("#create_distribution_journals"));
				return false;
            });

			initChosenSelect();

        });
    });

    $(".invoice-net-calculator").on('blur', function(e){
    	e.preventDefault();
    	Invoice.calculateInvoiceNet($(this));

	});

	$(".content-show").on('click', function(e){
	   e.preventDefault();
	   Invoice.showContent($(this));
	});

	$(".content-hide").on('click', function(e){
	   e.preventDefault();
	   Invoice.hideContent($(this));
	});

    var accountingPostingLine = $("#accounting_posting_line");

    var accountingPostings = $("#account_postings");
    var invoiceComments = $("#invoice_comments");
    var invoiceImage = $("#invoice_detail_container");
	if (accountingPostingLine.length > 0) {
	   Invoice.loadAccountPostingForm(accountingPostingLine);
	}
	if (accountingPostings.length > 0) {
		Invoice.loadAccountPostings(accountingPostings)
	}

	if (invoiceImage.length > 0) {
		Invoice.loadInvoiceImage(invoiceImage)
	}

	// $("#approve_invoice_submit").on('click', function(){
	// 	Invoice.saveInvoiceAccount();
	// })

	if (invoiceComments.length > 0) {
		Invoice.loadInvoiceHistory(invoiceComments);
	}

	$("#logger_save_invoice_changes").on('click', function(e){
		var form = $("#logger_edit_invoice");
		Invoice.saveInvoiceChange(this, form);
		e.preventDefault();
	});

	$("#save_invoice_changes").on('click', function(e){
		var form = $("#edit_invoice");
		Invoice.saveInvoiceChange(this, form);
		e.preventDefault();
	});

	$(".save_invoice").on('click', function(e){
		var form = $("#edit_invoice");
		Invoice.saveInvoiceChange(this, form);
		e.preventDefault();
	});

	$("#validate_invoice").on('click', function(e){
		Invoice.validateInvoice(this);
		e.preventDefault();
	});

	$("#sign_invoice").on('click', function(e){
		Invoice.signInvoice(this);
		e.preventDefault();
	});

	$("#add_cooment").on('click', function(e){
		Invoice.addInvoiceComment(this);
		e.preventDefault();
	});

	$("#add_invoices_to_flow").on('click', function(e){
		Invoice.addToFlow(this);
		e.preventDefault();
	});

	$("#add_to_flow").on('click', function(e){
	   e.preventDefault();
	   //addButtonSpinner($(this));
	   Invoice.addInvoiceToFlow(this);

		e.preventDefault();
	});

	$("#print_invoice").on('click', function(e){
		Invoice.printInvoice(this);
		e.preventDefault();
	});

	$("#email_supplier").on('click', function(e){
		Invoice.sendSupplerInvoiceEmail(this);
		e.preventDefault();
	});

	$("#park_invoice").on('click', function(e){
		Invoice.parkInvoice(this);
		e.preventDefault();
	});

	$("#unpark_invoice").on('click', function(e){
		Invoice.parkInvoice(this);
		e.preventDefault();
	});


	$(".delete_invoice_flow").on('click', function(e){
		if (confirm('Are you sure you want to delete this invoice flow')) {
			Invoice.deleteInvoiceFlow(this);
			e.preventDefault();
		}
	});

	$(".delete_invoice_flow_role").on('click', function(e){
		if (confirm('Are you sure you want to delete this invoice flow role')) {
			Invoice.deleteInvoiceFlowRole(this);
			e.preventDefault();
		}
	});

	$("form[id='certificate_form'] #id_supplier").on('change', function(e){
		var supplier = $("form[id='certificate_form'] #id_supplier option:selected").val();
		if (supplier !== '' || supplier !== undefined) {
            Invoice.loadSupplierData($(this), supplier);
		}
	});
	$("form[id='create_invoice'] #id_supplier").on('change', function(e){
		var supplier = $("form[id='create_invoice'] #id_supplier option:selected").val();
		if (supplier !== '' || supplier !== undefined) {
            Invoice.loadSupplierData($(this), supplier);
		}
	});
	$("form[id='logger_edit_invoice'] #id_supplier").on('change', function(e){
		var supplier = $("form[id='logger_edit_invoice'] #id_supplier option:selected").val();
		if (supplier !== '' || supplier !== undefined) {
            Invoice.loadSupplierData($(this), supplier);
		}
	});

	// $("#id_net_amount, #id_total_amount").on('blur', function(e){
	// 	var totalAmount = $("#id_total_amount").val();
	// 	var netAmount   = $("#id_net_amount").val();
	// 	if ( totalAmount !== '' && netAmount !== '') {
	// 	   Invoice.calculateVatAmount(totalAmount, netAmount);
	// 	}
	// })

	// $("#id_invoice_type").on('change', function(e){
	// 	$("#save_invoice_adjustments").show()
	// });


	$(".adjustable").on('blur change', function () {
		Invoice.handleAdjustedMoneyValues($(this));
    });

	$("#id_invoice_number").on('blur', function(){
		var supplier_id = $("#id_supplier").val();
		var invoice_number = $(this).val();

		if (invoice_number !== '' && supplier_id !== '') {
			Invoice.validateDocumentNumber($(this), supplier_id);
		}
	});

	$(".math-operation").on('blur', function(e){
		Invoice.calculateAmount($(this));
	});

	var formartMoney = debounce(function(el){
		Invoice.makeMoney(el);
	}, 650);

	// $(".money").on('blur', function(e){
	// 	Invoice.makeMoney($(this));
	// 	$(".money[readonly='readonly']").trigger('change')
	// });
    //
	// $(".money[readonly='readonly']").on('change', function(e){
	// 	console.log('Change');
	// 	console.log($(this));
	// 	Invoice.makeMoney($(this));
	// });


	$("#change_accounting_date").on('click', function(){
		event.preventDefault();
		var invoices = [];
		var modalUrl = $(this).data('ajax-url');
		$(".invoice-check").each(function(){
			if ($(this).is(":checked")) {
				invoices.push($(this).val())
			}
		});
		if (invoices.length > 0) {

			$.get(modalUrl, function (data) {
                $(data).modal('show');

                $("#accounting_date_datepicker").datepicker({
					onSelect:function (date) {
						$("#accounting_date").val(date)
                    }
				})
				$("#update_accounting_date").on('click', function(c){
					event.preventDefault();
					var ajaxUrl = $(this).data('ajax-url');
					$(this).html("Updating ...");

					$("#update_accounting_date_form").ajaxSubmit({
						url: ajaxUrl,
						target: '#loader',   // target element(s) to be updated with server response
						data: {
							'invoices': invoices
						},
						beforeSubmit:  function(formData, jqForm, options) {
							var queryString = $.param(formData);
						},  // pre-submit callback
						error: function (data) {
							// var element;
							// // show error for each element
							// if (data.responseJSON && 'errors' in data.responseJSON) {
							// 	$.each(data.responseJSON.errors, function (key, value) {
							// 		element.addClass('is-invalid');
							// 		element.after('<div class="is-invalid-message">' + value[0] + '</div>');
							// 	});
							// }
                            //
							// // flash error message
							// flash('danger', 'Errors have occurred.');
						},
						success:       function(responseJson, statusText, xhr, $form)  {
							responseNotification(responseJson);
						},  // post-submit callback
						dataType: 'json',
						type: 'post'
					});
				});

            })
		}
		return false;
	});


	var invoiceLinkedEL = $(".invoice-linked");
	console.log( invoiceLinkedEL )
	console.log( invoiceLinkedEL.length )
	invoiceLinkedEL.on("click", function(){
		Invoice.loadInvoiceLinkingPage($(this));
	});

	$("#invoice_payment").on("click", function(e){
		e.preventDefault();
		var el = $(this);
		var supplier_id = el.data('supplier-id');
		var invoice_id = el.data('invoice-id');
		Invoice.ajaxGet(el, {
			'parent_id': supplier_id,
			'invoice_id': invoice_id
		});
	});

	$("#pay_invoice").on("click", function(e){
		e.preventDefault();
		var el = $(this);
		var supplier_id = el.data('supplier-id');
		var invoice_id = el.data('invoice-id');
		Invoice.ajaxGet(el, {
			'parent_id': supplier_id,
			'invoice_id': invoice_id
		})
	});

	$("#send_email").on("click", function(e){
		Invoice.ajaxGet($(this));
	});

	var fetchEmails = $("#fetch_emails");
	fetchEmails.on('click', function(e){
	   var ajaxUrl = $(this).data('ajax-url');
	   e.preventDefault();
	   fetchEmails.html("Processing ...");
	   $.getJSON(ajaxUrl, function(response){
	   	   responseNotification(response);
		   fetchEmails.html("<i class='fa fa-refresh'></i> Update");

	   })
	});

	var createJournal = $(".create-journal");
	createJournal.on('click', function(e){
	   e.preventDefault();
	   Invoice.createJournal($(this));
	});



	$("#save_invoice").on('click', function(e){
		e.preventDefault();
		Invoice.saveInvoice();
	});

	$("#save_payment").on('click', function(e){
		e.preventDefault();
		Invoice.send($(this), {});
	});

	$("#upload_document").on("click", function(e){
		e.preventDefault();
		Invoice.savePayment($(this));
	});

	var pdfViewer = $("#pdf_viewer");
	if (pdfViewer.length > 0) {
		var pdf  = pdfViewer.data('pdf-url');
		PDFObject.embed(pdf, "#pdf_viewer");
	}

	$("#id_flow_proposal").on("change", function(e){
		Invoice.getFlowProposalUrl($(this));
	});

	$("#view_flow_proposal").on("click", function(e){
		Invoice.showWorkflowFlowProposal($(this));
	});

	$("#save_certificate").on('click', function(e){
		var form = $("#certificate_form");
		Invoice.saveDocument(this, form);
		e.preventDefault();
	});

	$("#update_certificate").on('click', function(e){
		var form = $("#certificate_form");
		Invoice.saveDocument(this, form);
		e.preventDefault();
	});

	$("#invoice_payment").on('click', function(e){
		e.preventDefault();
		Invoice.openPayments($(this));
	});

	$("#invoice_payment").on('click', function(e){
		e.preventDefault();
		Invoice.openPayments($(this));
	});

});


var Invoice = (function(invoice, $){

	return {
		_operators: [],
		_values: [],

		handleAdjustedMoneyValues: function(el)
		{
			console.log("------------ handleAdjustedMoneyValues ----------")
			var isAdjustment = false;
			var invoiceAdjustmentBtn = $("#save_invoice_adjustments");
			$(".adjustable").each(function(){
				var elId = $(this).attr('id');
				var currentValue = $(this).val();
				var originalEl = $("#" + elId + "_val");
				var originalValue = originalEl.val();
				if (originalEl !== undefined) {
					if (currentValue !== originalValue) {
						isAdjustment = true;
					}
				}
			});
			if (isAdjustment) {
				invoiceAdjustmentBtn.show();
			} else {
				invoiceAdjustmentBtn.hide();
			}
			var totalAmount = $("#id_total_amount").val();
			var netAmount = $("#id_net_amount").val();
			Invoice.calculateInvoiceNet($(this));
			//Invoice.calculateVatAmount(totalAmount, netAmount);
		},

		openPayments: function(el)
		{
			var paymentsUrl = el.data('ajax-url');
			if (paymentsUrl) {
				$.get(paymentsUrl, function(html){
					$("#invoice_pdf").html(html);
					$("#close_invoice_payment_link").on('click', function(e){
						Invoice.loadInvoiceImage($(this));
					});
				});
			}
		},

		showWorkflowFlowProposal: function(el, ajaxUrl)
		{
			var ajaxUrl = ajaxUrl || el.data('ajax-url');
			if (ajaxUrl) {
				$.get(ajaxUrl, function(html){
					$("#invoice_pdf").html(html);
					$("#close_invoice_image").on('click', function(e){
						Invoice.loadInvoiceImage($(this));
					});
				});
			}
		},

		getFlowProposalUrl: function(el)
		{
			var invoiceId = $("#view_flow_proposal").data('invoice-id')
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl && el.val()) {
				ajaxUrl = ajaxUrl + '?workflow_id=' +  el.val() + "&invoice_id=" + invoiceId;

				$("#view_flow_proposal").attr('data-ajax-url', ajaxUrl);
				$("#proposal_link").show();

				$("#view_flow_proposal").on("click", function(e){
					Invoice.showWorkflowFlowProposal($(this), ajaxUrl)
				});
			}
		},

		loadVatCodes: function(vatAmount)
		{
			var ajaxUrl = $("#id_vat_amount").data('vat-codes-url');
			console.log("ajax url --> ", ajaxUrl)
			if (ajaxUrl) {
				$("#id_vat_code_chosen").remove();
				var params = {'vat_amount': vatAmount};
				$.getJSON(ajaxUrl, params, function(vatCodes){
					var html = [];
					html.push("<select id='id_vat_code' name='vat_code' class='chosen-select'>");
					$.each(vatCodes, function(index, vatCode){
						if (vatCode['is_default']) {
							html.push("<option value='" + vatCode['id']+"' selected='selected'>"+ vatCode['label'] +"</option>");
						} else {
							html.push("<option value='" + vatCode['id']+"'>"+ vatCode['label'] +"</option>");
						}
					});
					html.push("<select id='id_vat_code' name='vat_code'>")
					$("#id_vat_code").replaceWith( html.join(' ') )

					$(".chosen-select").chosen().css({'width': '353px'})
					$("#id_vat_code_chosen").css({'width': '353px'})
				});
			}
		},

		savePayment: function(el)
		{
			var form = $("#payment_form");
			submitModalForm(form)
		},

		distributeAccounts: function(el)
		{
		   var accountIds = [];
		   $(".distribution-invoice-account").each(function(){
		   		if ($(this).is(":checked")) {
		   			accountIds.push($(this).val());
				}
		   });

		   var startDate = $("#id_start_date");
		   var nbMonth = $("#id_month");
		   if (accountIds.length === 0) {
			 alert("Please select the accounts")
		   } else if(!startDate.val()) {
			  alert("Please select start date")
		   } else if(!nbMonth.val()) {
			  alert("Please enter number of months")
		   } else {
			   var self = $("#" + el.attr('id'));
			   var container = $("#account_distribution");
			   container.html("Loading . . .");
			   var url = el.data('ajax-url');
			   $.get(url, {
				 months: nbMonth.val(),
				 start_date: startDate.val(),
				 account_ids: accountIds.join(',')
			   }, function(html){
				   container.html(html);
			   });
		   }
		},

		send: function(el, params)
		{
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
		},

		ajaxGet: function (el, params) {
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
        },

		saveDocument: function(el)
		{
			var form = $("#certificate_form");
			submitModalForm(form)
		},

		saveInvoice: function(el)
		{
			var form = $("#create_invoice");
			submitModalForm(form)
		},

		createJournal: function(el) {
		   var self = $("#" + el.attr('id'));
		   var text = el.text();
		   self.html("Processing ...");
		   var url = el.data('ajax-url');
		   $.get(url, {
			 period: $("#period").val()
		   }, function(response){
		   	   if (response.error) {
		   	   	  self.html(text);
			   }
			   responseNotification(response);
		   }, 'json');
		},

		loadInvoiceImage: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			var fileUrl = el.data('file-url');
			if (fileUrl) {
				 var pdf  = invoicePdf.data('pdf-url');
				 PDFObject.embed(pdf, "#invoice_pdf");
			} else if (ajaxUrl) {
				$.get(ajaxUrl, function(html){
				    var containerDiv;
					if ($("#purchase_order_detail_container").length > 0) {
						containerDiv = $("#purchase_order_detail_container");
					} else if ($("#purchase_orders").length > 0) {
						containerDiv = $("#purchase_orders");
					} else if($("#goods_received_container").length > 0) {
						containerDiv = $("#goods_received_container");
					} else if($("#invoice_detail_container").length > 0) {
						containerDiv = $("#invoice_detail_container");
					}
					containerDiv.html(html);
					var invoicePdf = $("#invoice_pdf");
					if (invoicePdf.length) {
						 var pdf  = invoicePdf.data('pdf-url');
						 PDFObject.embed(pdf, "#invoice_pdf");
					}
				});
			}
		},

		loadSupplierData: function(el, supplier)
		{
			var ajaxUrl = $("#supplier_detail_url").data('ajax-url');
			$.getJSON(ajaxUrl, {
				supplier : supplier,
			}, function(response){
				$("#id_supplier_name").val(response['name'])
				$("#id_supplier_number").val(response['code'])
                $("#id_vat_number").val(response['vat_number'])
                $("#id_supplier_account_ref").val(response['company_number'])
			});
		},

		payInvoices: function(el, invoices)
		{
			if (invoices.length > 0) {
				var ajaxUrl = el.data('ajax-url');
				$.getJSON(ajaxUrl, {
					invoices :invoices.join(','),
					payment_method_id: $("#payment_method_id").val()
				}, function(response){
					responseNotification(response)
				});
			}
		},

		addDiscount: function(el)
		{
			var invoiceId = el.val()
			var totalDiscountEl = $("#id_total_discount")
			var discountAmount = parseFloat($("#invoice_discount_" + invoiceId).val())
			var totalDiscount = 0
			if (totalDiscountEl.val() !== '') {
				totalDiscount = parseFloat(totalDiscountEl.val())
			}
			totalDiscount += discountAmount
			totalDiscountEl.val( totalDiscount.toFixed(2) )
			Invoice.calculateNetPayment()
		},

		removeDiscount: function(el)
		{
			var invoiceId = el.val()
			var totalDiscountEl = $("#id_total_discount")
			var discountAmount = parseFloat($("#invoice_discount_" + invoiceId).val())
			var totalDiscount  = 0
			if (totalDiscountEl.val() !== '') {
				totalDiscount = parseFloat(totalDiscountEl.val())
			}
			totalDiscount -= discountAmount
			totalDiscountEl.val( totalDiscount.toFixed(2) )
			Invoice.calculateNetPayment()
		},

		calculateNetPayment: function()
		{
			var selectedDiscount = $("#id_total_discount").val()
			var discountAllowed = $("#id_discount_disallowed").val()
			var totalDiscount = 0
			if (discountAllowed !== '') {
				totalDiscount += parseFloat(discountAllowed);
			}
			if (selectedDiscount !== '') {
				totalDiscount += parseFloat(selectedDiscount);
			}
			var total = $("#id_total_amount").val()
			var netAmount = parseFloat(total) - totalDiscount;
			$("#id_net_amount").val( netAmount.toFixed(2) )
		},

		allChecked: function(elClass, elId)
		{
			var countChecked = 0
			var countCheckboxes = 0
			elClass.each(function(){
				countCheckboxes++;
				if ($(this).is(":checked")) {
					countChecked++;
				}
			})
			//console.log(elClass, "chjecked --> ", countChecked, " count checkboxes --> ", countCheckboxes, elId)
			if (countCheckboxes > 0  && countChecked > 0) {
				if (countCheckboxes === countChecked) {
					//console.log("Make the element checked")
					elId.attr("checked", "checked")
                } else {
					elId.removeAttr("checked", "checked")
				}
			}
		},

		loadInvoiceLinkingPage: function(el)
		{
			var invoicePageContainer = $("#invoice_detail_container");
			invoicePageContainer.html("Loading ...");
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl !== undefined) {
				$.get(ajaxUrl, {

				}, function(html) {
					var poContainer = $("#purchase_order_detail_container");
					var goodsReceivedContainer = $("#goods_received_container");
					var purchaseOrdersContainer = $("#purchase_orders");
					if (poContainer.length > 0) {
						poContainer.replaceWith(html);
					} else if (goodsReceivedContainer.length > 0) {
						goodsReceivedContainer.replaceWith(html);
					} else if (purchaseOrdersContainer.length > 0) {
						purchaseOrdersContainer.replaceWith(html);
					} else if(invoicePageContainer.length > 0) {
						invoicePageContainer.replaceWith(html);
					}

					$("#linked_items_body>tr>td:not(.actions)").on('click', function(){

						var parentTr = $(this).parent('tr');
						var detailEl = $("#" + parentTr.data('detail-id'));
						var detailUrl = parentTr.data('detail-url');
						detailEl.html('loading ...');
						$.get(detailUrl, function(detailHtml){
							detailEl.html(detailHtml)
						});
						return false;
					});

					$(".purchase-order-line>td").nextUntil($('.not-editable'), 'td').on('click', function(e){
						var lineId = $(this).data('purchase-order-line-id');
						$("#add_purchase_order_line").attr('disabled', 'disabled');
						$("#update_purchase_order_line").removeAttr('disabled');
						Invoice.loadPurchaserOrderForm($(this), lineId);
						return false;
					});

					$(".save-invoice-linking").on('click', function(e){
						var linkedItems = [];
						$(".linked-item-check").each(function(){
							if ($(this).is(":checked")) {
								linkedItems.push($(this).val())
							}
						});
						var action = $(this).data('action');
						if (action === 'goods_received') {
							Invoice.saveGoodsReceived($(this));
						} else if (linkedItems.length > 0) {
							Invoice.saveInvoicePurchaseOrders($(this), linkedItems);
						}
					})
					$("#close_purchase_order_invoice_link").on('click', function(){
						Invoice.loadInvoiceImage($(this));
					});
				});
			}

		},

		loadPurchaseOrders: function(el){
			var ajaxUrl = el.data('ajax-url');
			$.get(ajaxUrl, {

			}, function(html) {

				if ($("#purchase_order_detail_container").length > 0) {
					$("#purchase_order_detail_container").replaceWith(html);
				} else if ($("#purchase_orders").length > 0) {
					$("#purchase_orders").replaceWith(html);
				} else if($("#invoice_detail_container").length > 0) {
					$("#invoice_detail_container").replaceWith(html);
				}

				$("#purchase_orders>tr>td:not(.actions)").on('click', function(){

					var purchaseDetails = $("#purchase_order_details");
					var detailUrl = $(this).parent('tr').data('detail-url');
					purchaseDetails.html('loading ...');
					$.get(detailUrl, function(po_html){
						purchaseDetails.html(po_html)
					});
					return false;
				});

				$(".purchase-order-line>td").nextUntil($('.not-editable'), 'td').on('click', function(e){
					var lineId = $(this).data('purchase-order-line-id');
					$("#add_purchase_order_line").attr('disabled', 'disabled');
					$("#update_purchase_order_line").removeAttr('disabled');
					Invoice.loadPurchaserOrderForm($(this), lineId);
					return false;
				});

				$("#save_purchase_order_invoice_link").on('click', function(e){
					var po_orders = [];
					$(".purchase-order-check").each(function(){
						if ($(this).is(":checked")) {
							po_orders.push($(this).val())
						}
					});
					if (po_orders.length > 0) {

						Invoice.saveInvoicePurchaseOrders($(this), po_orders);
					}
				})
				$("#close_purchase_order_invoice_link").on('click', function(){
					Invoice.loadInvoiceImage($(this));
				});

			});
		},

		saveGoodsReceived: function(el) {
			el.prop('disabled', true);
			var ajaxUrl = el.data('ajax-url');
			console.log( ajaxUrl )

			var goodsReceivedList = [];
			var goodsReturnedList = [];
			$(".received-link").each(function(){
				if ($(this).is(":checked")) {
					goodsReceivedList.push($(this).val())
				}
			});
			$(".returned-link").each(function(){
				if ($(this).is(":checked")) {
					goodsReturnedList.push($(this).val())
				}
			});

			$.getJSON(ajaxUrl, {
				goods_received: goodsReceivedList.join(','),
				goods_returned: goodsReturnedList.join(',')
			}, function(response){
				responseNotification(response);
				if (!response.error) {

					//Invoice.loadPurchaseOrders($("#link_invoice_to_purchase_order"))
					Invoice.loadAccountPostings($("#account_postings"));
				} else {
					el.prop('disabled', false);
				}
			});
		},

		saveInvoicePurchaseOrders: function(el, po_orders) {
			var ajaxUrl = el.data('ajax-url');
			console.log( ajaxUrl )
			$.getJSON(ajaxUrl, {
				purchase_orders: po_orders.join(',')
			}, function(response){
				responseNotification(response);
				if (!response.error) {
					Invoice.loadPurchaseOrders($("#link_invoice_to_purchase_order"))
				}
			});
		},

		validateDocumentNumber: function(el, supplier_id){
			var ajaxUrl = $("#validate_document_number").data('validate-document-number-url');
			$.get(ajaxUrl, {
				document_number: el.val(),
				supplier_id: supplier_id
			}, function(responseJson) {
				if (responseJson.error) {
					displayFormError(el, responseJson.text);
					el.addClass('text-danger').addClass('error')
				} else {
					responseNotification(responseJson);
					removeFormError(el)
					el.removeClass('text-danger').removeClass('error')
				}
			}, 'json');
		},

		makeMoney: function(el){
			var result = accounting.formatMoney($(el).val());
			var money = parseFloat($(el).val())
			console.log('Format mponey ', $(el).val(), result, money.toLocaleString())
			//el.val(result)
			//return false;
		},
		approveInvoiceDecline: function(el){
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, function(responseJson) {
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			}, 'json');
		},

		rejectInvoice: function(el){
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, function(responseJson) {
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			}, 'json');
		},

		amountCalculator: function(el)
		{
			// var value = el.val();
			// console.log(value);
			// var amountItems = value.split(/\+/);
			// console.log(" --------------- amountItems ---------------------- ");
			// console.log(amountItems);

			// if (isNaN(value)) {
			// 	this._operators.push(value);
			// } else {
			// 	this._values.push(value);
			// }
			// console.log('Operators');
			// console.log(this._operators);
			// console.log('values');
			// console.log(this._values);

		},

		calculateAmount: function(el)
		{
			//Todo implement the calculator in javascript, no need to go to the server
			var mathOperationElement = $("#math_operation");
			var ajaxUrl = $(this).data('ajax-url');
			if (mathOperationElement.length > 0) {
				ajaxUrl = mathOperationElement.data('ajax-url');
			}

			$.get(ajaxUrl, {
				amount: $(el).val(),
				csrf_token: $("input[name='csrfmiddlewaretoken']").val()
			}, function(responseJson){
				if (responseJson.error === false) {
					if ('amount' in responseJson) {
						$(el).val(responseJson.amount)
					}
				} else {
					responseNotification(responseJson);
				}
			}, 'json');
		},

		calculateVatAmount: function(totalAmount, netAmount)
		{
			var vatAmount = parseFloat(totalAmount) - parseFloat(netAmount)
			if (vatAmount > 0) {
				$("#id_vat_amount").val(parseFloat(vatAmount).toFixed(2))
			}
		},
		calculateNetAmount: function(totalAmount, vatAmount)
		{
			var netAmount = parseFloat(totalAmount) - parseFloat(vatAmount);
			console.log('Vat amount ', vatAmount, 'total ', parseFloat(totalAmount), ' net ', parseFloat(vatAmount))
			if (netAmount > 0) {
				$("#id_net_amount").val(parseFloat(netAmount).toFixed(2))
			}
		},

		calculateInvoiceNet: function(el)
		{
			console.log("---------------calculateInvoiceNet-------------------")
			var netAmountEl = $("#id_net_amount");
			var totalAmountEl = $("#id_total_amount");
			var vatAmountEl = $("#id_vat_amount");

			if (vatAmountEl !== undefined && totalAmountEl !== undefined) {
				var netAmount = 0;
				var totalAmount = totalAmountEl.val();
				var vatAmount = vatAmountEl.val();
				if (totalAmount !== "" && vatAmount !== "") {
					netAmount  = parseFloat(totalAmount) - parseFloat(vatAmount);
					console.log("Net amount --> ", netAmount);
					netAmountEl.val( accounting.toFixed(netAmount , 2));
				}

				console.log("Vat code on creating invoice");
				$("#id_vat_code option").each(function(){
					var percentage = $(this).data('percentage');
					if (vatAmount === "" || vatAmount === 0) {
						if (percentage > 0) {
							$(this).hide();
						} else {
							$(this).show();
						}
					} else {
						if (percentage === 0) {
							$(this).hide();
						} else {
							$(this).show();
						}
					}
				});
				var chosenResults = $("#id_vat_code_chosen").find('.chosen-results')

				chosenResults.each(function(){
					console.log( $(this).text() )
				})

				console.log( $("#id_vat_code_chosen") )
				console.log('......')
				console.log(  )
				console.log('____________......')
				console.log( $("#id_vat_code_chosen > .chosen-drops") )
				console.log( $("#id_vat_code_chosen.chosen-drop") )
				console.log( $("#id_vat_code_chosen.chosen-drop.chosen-results") )

				$("#id_vat_code_chosen>.active-results").each(function(){
					console.log($(this), $(this).text(), $(this).data('option-array-index'));
				})

				//Invoice.loadVatCodes(vatAmount);
			}
		},

		deleteInvoiceFlow: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, {
			}, function(responseJson){
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			}, 'json');
		},

		deleteInvoiceFlowRole: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			$.get(ajaxUrl, {
			}, function(responseJson){
				responseNotification(responseJson);
				if ('redirect' in responseJson) {
					document.location.href = responseJson.redirect
				}
			}, 'json');
		},

		openEditAccountPosting: function(el, accountId)
		{
			$("#add_invoice_account_posting").attr('disabled', 'disabled');
			$("#update_invoice_account_changes").removeAttr('disabled');
			Invoice.loadAccountPostingForm($(el), accountId);
		},

		loadAccountPostings: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			var invoiceId = el.data('invoice-id');
			el.html("Loading ....");
			$.get(ajaxUrl, {
				invoice_id: invoiceId,
				type: 'postings'
			}, function(html){
				el.replaceWith(html);

				$(".invoice-account-posting-line>td").nextUntil($('.not-editable'), 'td').on('click', function(e){
					var accountId = $(this).data('account-id');
					$("#add_invoice_account_posting").attr('disabled', 'disabled');
					$("#update_invoice_account_changes").removeAttr('disabled');
					Invoice.loadAccountPostingForm($(this), accountId);
					return false;
				});

				$(".delete-account-posting").on('click', function(){
					if (confirm('Are you sure you want to delete this account posting line')) {
						Invoice.deleteAccountPosting($(this));
					}
					return false;
				});
			});
		},

		loadAccountPostingForm: function(el, accountId)
		{
			var ajaxUrl = el.data('ajax-url');
			var invoiceId = el.data('invoice-id');
			$.get(ajaxUrl, {
				invoice_id: invoiceId,
				account_id: accountId
			}, function(html){
				$("#accounting_posting_line").html(html);
				$("#id_calculation_method").on('change', function(){
					var selectedValue = $(this).val()
					if (selectedValue == 'percentage') {
						$("#percentage").show();
						$("#amount").hide();
					} else {
						$("#percentage").hide();
						$("#amount").show();
					}
					return false;
				});

				$("#approve_invoice_submit").on('click', function(e){
					Invoice.saveInvoiceAccount();
					return false;
				});

				$("#edit_invoice_account_submit").on('click', function(){
					Invoice.updateInvoiceAccount();
					return false;
				})

				$("#update_invoice_account_changes").on('click', function(e){
					e.stopImmediatePropagation();
					Invoice.saveInvoiceAccount($("#invoice_account_form"));
					return false;
				});
				$("#add_invoice_account_posting").on("click", function(e){
					e.stopImmediatePropagation();
				 	Invoice.saveInvoiceAccount($("#invoice_account_form"));
				 	e.preventDefault()
				 });

				$("#id_amount").on('blur', function(e){
					Invoice.amountCalculator($(this));
				});

				$(".math-operation").on('blur', function(e){
						Invoice.calculateAmount($(this));
				});

				$(".money").on('blur', function(e){
					Invoice.makeMoney($(this));
				});

				$(".chosen-select").chosen();
			});
		},

		deleteAccountPosting: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			var accountId = el.data('account-id');
			$.get(ajaxUrl, {
				account_id: accountId
			}, function(responseJson){
				responseNotification(responseJson);
				var accountingPostings = $("#account_postings");
				if (accountingPostings) {
					Invoice.loadAccountPostings(accountingPostings)
				}
			}, 'json');
		},

		loadPurchaserOrderLines: function(el) {
			var ajaxUrl = el.data('ajax-url');
			var purchaserOrderId = el.data('invoice-id');
			$.get(ajaxUrl, {
				invoice_id: purchaserOrderId,
			}, function(html){
				el.replaceWith(html);

				$(".purchase-order-line>td").on('click', function(e){
					var purchaserOrderId = $(this).parent().data('purchase-order-line-id');
					console.log(purchaserOrderId);
					$("#add_purchase_order_line").attr('disabled', 'disabled');
					$("#update_purchase_order_line").removeAttr('disabled');
					Invoice.loadPurchaserOrderForm($(this).parent(), purchaserOrderId);
					return false;
				});

				$(".delete-purchase-order").on('click', function(){
					if (confirm('Are you sure you want to delete this purchase order line')) {
						Invoice.deletePurchaseOrderLine($(this));
					}
					return false;
				});
			});
		},

		loadPurchaserOrderForm: function(el, lineId)
		{
			if (el) {
                var ajaxUrl = el.data('ajax-url');
                var lineId = el.data('purchase-order-line-id');
                console.log( ajaxUrl )
				console.log( lineId )
                $.get(ajaxUrl, {
                    line_id: lineId
                }, function(html){
                   $("#purchase_order_lines").html(html);
                    $("#id_calculation_method").on('change', function(){
                        var selectedValue = $(this).val()
                        if (selectedValue == 'percentage') {
                            $("#percentage").show();
                            $("#amount").hide();
                        } else {
                            $("#percentage").hide();
                            $("#amount").show();
                        }
                        return false;
                    });

                    $("#approve_invoice_submit").on('click', function(e){
                        Invoice.saveInvoiceAccount();
                        return false;
                    });

                    $("#update_purchase_order_line").on('click', function(e){
                    	e.stopImmediatePropagation();
                        submitForm($("#purchaser_order_form"));
                    })

                    $("#update_invoice_account_changes").on('click', function(e){
                        e.stopImmediatePropagation();
                        Invoice.saveInvoiceAccount($("#purchase_order_form"));
                        return false;
                    });
                    $("#add_purchase_order_line").on("click", function(e){
                        e.stopImmediatePropagation();
                        Invoice.savePurchaseOrderLine($("#purchase_order_form"));
                        e.preventDefault()
                    });

                    $("#id_amount").on('blur', function(e){
                        Invoice.amountCalculator($(this));
                    });

                    $(".math-operation").on('blur', function(e){
                        Invoice.calculateAmount($(this));
                    });

                    $(".money").on('blur', function(e){
                        Invoice.makeMoney($(this));
                    });

                    $(".chosen-select").chosen();
                });
            }
		},

		savePurchaseOrderLine: function(form)
		{
			form = form || $("#purchase_order_form");
			// disable extra form submits
			form.addClass('submitted');
			var loader = $("#loader");
			loader.show();
			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					loader.hide();
					if (responseJson.error) {
						if (responseJson.hasOwnProperty('errors')) {
							highlightFormErrors(responseJson.errors)
						}
						alertErrors(responseJson.text)
					} else {
						form.find(":input:not('select')").each(function(){
							$(this).val('')
						})
						form.find(".chosen-select").val('').trigger("chosen:updated");
						responseNotification(responseJson);
						var purchaseOrderLines = $("#purchase_orders");
						if (purchaseOrderLines) {
							Invoice.loadPurchaserOrderForm($("#purchase_order_lines"))
							$("#update_invoice_account_changes").attr('disabled', 'disabled');
							$("#add_invoice_account_posting").removeAttr('disabled');
							Invoice.loadPurchaserOrderLines(purchaseOrderLines);
						}

					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		loadInvoiceHistory: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			var invoiceId = el.data('invoice-id');
			$.get(ajaxUrl, {
				invoice_id: invoiceId
			}, function(html){
				el.html(html);
			});
		},

        addAccount: function(el)
        {
        	var ajaxUrl			   = $(el).data('ajax-url');
        	var invoiceId 		   = $(el).data('invoice-id');
            var accountId          = $("#id_account");
            var percentageOfAmount = $("#id_percentage");
            var responseDiv 	   = $("#response");
            if (accountId.length > 0 && accountId.val() === '') {
                alert('Please select the account.');
                return false;
            } else if (percentageOfAmount.length > 0 && percentageOfAmount.val() === ''){
                alert('Please enter the percentage of the amount to assign the account.');
                return false;
            } else {
                var _counter = $("#counter");
                var accountDiv = $("#account");

                $.getJSON(ajaxUrl, {
                    invoice_id: invoiceId,
                    account_id: accountId.val(),
                    percentage_of_amount: percentageOfAmount.val()
                }, function(responseJson){
                    if (responseJson.error) {
                        responseDiv.html(responseJson.text);
                    } else {
                        responseDiv.html(responseJson.text);
                        var html = [];

                        $.each(responseJson['invoice_data'].invoice_accounts, function(index, account){

                            html.push("<tr>");
                            html.push("<td>");
                                html.push(account.account);
                            html.push("</td>");
                            html.push("<td>");
                                html.push(account.percentage_of_amount);
                            html.push("</td>");
                            html.push("<td>");
                                html.push(account.amount);
                            html.push("</td>");
                            html.push("<td></td>");
                            html.push("</tr>");
                        });
                        $("#accounts_table>tbody").empty().append( html.join(' ') )
                        $("#accounts_list").show();
                        $("#_percentage_of_total_amount").html( responseJson['invoice_data'].outstanding )
                        $("#id_account").val('')
                        $("#id_percentage").val('')
                    }

                });
            }
        },

		updateInvoiceAccount: function(form)
		{
			var form = $("#edit_invoice_account_form");
			// disable extra form submits
			form.addClass('submitted');
			var loader = $("#loader");
			loader.show();
			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					loader.hide();

					responseNotification(responseJson);
					var accountingPostings = $("#account_postings");
					if (accountingPostings) {
						Invoice.loadAccountPostings(accountingPostings)
					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		saveInvoiceAccount: function(form)
		{
			form = form || $("#create_invoice_account_form");
			// disable extra form submits
			form.addClass('submitted');
			var loader = $("#loader");
			loader.show();
			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					loader.hide();
					if (responseJson.error) {
						if (responseJson.hasOwnProperty('errors')) {
							highlightFormErrors(responseJson.errors)
						}
						alertErrors(responseJson.text)
					} else {
						form.find(":input:not('select')").each(function(){
							$(this).val('')
						})
						form.find(".chosen-select").val('').trigger("chosen:updated");
						responseNotification(responseJson);
						var accountingPostings = $("#account_postings");
						if (accountingPostings) {
							Invoice.loadAccountPostingForm($("#accounting_posting_line"))
							$("#update_invoice_account_changes").attr('disabled', 'disabled');
							$("#add_invoice_account_posting").removeAttr('disabled');
							Invoice.loadAccountPostings(accountingPostings);
						}

					}
					// var element;
					// // perform redirect
					// if (responseJson.hasOwnProperty('redirect')) {
					// 	$(location).attr('href', responseJson.redirect);
					// }
                    //
					// // flash success message
					// if (responseJson.hasOwnProperty('error')) {
					// 	flash(responseJson.error, responseJson.text);
					// }
                    //
					// if (responseJson.hasOwnProperty('errors')) {
					// 	$.each(responseJson.errors, function (key, value) {
					// 		var element = $("#id_" + key )
					// 		if (element.length > 0) {
					// 			element.addClass('is-invalid');
					// 			element.after('<div class="is-invalid-message has-error">' + value[0] + '</div>');
					// 		}
					// 	});
					// }
                    //
					// // dismiss modal
					// if (responseJson.hasOwnProperty('dismiss_modal')) {
					// 	form.closest('.modal').modal('toggle');
					// }
                    //
					// // reload page
					// if (responseJson.hasOwnProperty('reload_page')) {
					// 	location.reload();
					// }
                    //
					// // reload datatables
					// if (responseJson.hasOwnProperty('reload_datatables')) {
					// 	$($.fn.dataTable.tables()).DataTable().ajax.reload();
					// }
                    //
					// // reload sources
					// if (responseJson.hasOwnProperty('reload_sources')) {
					// 	$('[data-source]').each(function () {
					// 		$.get($(this).data('source'), function (data) {
					// 			$(this).html(data);
					// 		});
					// 	});
					// }
					// form.find(':submit').each(function () {
					// 	$(this).html( $(this).attr('data-original-html') );
                    //
					// });
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		saveInvoiceChange: function(el, form, notify)
		{
			var ajaxUrl = $(el).data('ajax-url');
			form.ajaxSubmit({
				url: ajaxUrl,
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					if (notify === undefined || notify === true) {
						responseNotification(responseJson)
                    }
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		saveAndSignInvoice: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			var ajaxSaveUrl = $("#save_invoice_changes").data('ajax-url');
			var form = $("#edit_invoice");
			form.ajaxSubmit({
				url:  ajaxSaveUrl,
				target: '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					$.getJSON(ajaxUrl, {
						csrf_token: window.csrftoken
					}, function(response) {
						if ('alert' in response) {
							if ('url' in response){
								ajaxNotificationDialog(response.url)
							} else {
								alertErrors(response.text)
							}
						} else {
							responseNotification(response)
						}
					});
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		},

		signInvoice: function(el)
		{
			var ajaxUrl = $(el).data('ajax-url');
			var ajaxSaveUrl = $(el).data('ajax-update-invoice-url');
			if (ajaxSaveUrl) {
				var form = $("#edit_invoice");
				form.ajaxSubmit({
					url:  ajaxSaveUrl,
					target: '#loader',   // target element(s) to be updated with server response
					beforeSubmit:  function(formData, jqForm, options) {
						var queryString = $.param(formData);
					},  // pre-submit callback
					error: function (data) {
						var element;
						// show error for each element
						if (data.responseJSON && 'errors' in data.responseJSON) {
							$.each(data.responseJSON.errors, function (key, value) {
								element.addClass('is-invalid');
								element.after('<div class="is-invalid-message">' + value[0] + '</div>');
							});
						}

						// flash error message
						flash('danger', 'Errors have occurred.');
					},
					success:       function(responseJson, statusText, xhr, $form)  {
						if (responseJson.error) {
							responseNotification(responseJson)
						} else {
							$.getJSON(ajaxUrl, {
								csrf_token: window.csrftoken
							}, function(response) {
								responseNotification(response)
							});
						}
					},  // post-submit callback
					dataType: 'json',
					type: 'post'
				});
			} else {
				$.getJSON(ajaxUrl, {
					csrf_token: window.csrftoken
				}, function(response) {
					responseNotification(response)
				});
			}
		},

		addInvoiceComment: function(el)
		{
			var self  = this;
			var ajaxUrl = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response)
				self.loadInvoiceHistory($("#invoice_comments"));
			});
		},

		printInvoice: function(el)
		{
			var self 	  = this;
			var ajaxUrl   = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response)
			});
		},

		sendSupplerInvoiceEmail: function(el){
			var self 	  = this;
			var ajaxUrl   = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response)
			});
		},

		parkInvoice: function(el){
			var self 	  = this;
			var ajaxUrl   = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response)
				if ('redirect' in response) {
					setTimeout(function(){
						document.location.href = response.redirect
					}, 1500)
				}
			});
		},

		addInvoiceToFlow: function(el)
		{

			var self = this;
			var flowAjaxUrl = $(el).data('ajax-url');
			var form = $("#logger_edit_invoice");
			var ajaxUrl = $("#logger_save_invoice_changes").data('ajax-url');
			form.ajaxSubmit({
				url: ajaxUrl,
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}
				},
				success:       function(responseJson, statusText, xhr, $form)
				{
					responseNotification(responseJson)
					if (responseJson.error) {
						//removeButtonSpinner($(el));
					} else {
						//removeButtonSpinner($(el));
						$.getJSON(flowAjaxUrl, {
							csrf_token: window.csrftoken
						}, function(response){
							responseNotification(response)
							if (response.error) {
								alertErrors(response.details)
								if(response.errors) {
									highlightFormErrors(response.errors)
								}
								//responseNotification(response)
							}
							if ('redirect' in response) {
								setTimeout(function(){
									document.location.href = response.redirect
								}, 1500)
							}
						});
					}
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
			});
		},

		validateInvoice: function(el)
		{
			var self 	  = this;
			var ajaxUrl   = $(el).data('ajax-url');
			$.getJSON(ajaxUrl, {
				csrf_token: window.csrftoken
			}, function(response){
				responseNotification(response)
			});
		},


		addToFlow: function(el)
		{
			var self 	 = this;
			var ajaxUrl  = $(el).data('ajax-url');
			var invoices = [];
			$('.invoice-check').each(function(index){
				if ($(this).is(":checked")) {
					invoices.push( $(this).val() )
				}
			});
			if (invoices.length > 0) {
				$.getJSON(ajaxUrl, {
					csrf_token: window.csrftoken,
					invoices: invoices.join(',')
				}, function(responseJson){
					responseNotification(responseJson)
				});
			} else {
				responseNotification({'error': true, 'text': 'Select invoices to add to flow'})
			}
		}

	}
}(Invoice || {}, jQuery))

