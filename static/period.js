$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');
            var minDate = new Date()
			$("#create_month_close_submit").on('click', function (event) {
                event.preventDefault();
                var form = $($(this).parent()).parent()
                submitModalForm(form);
            });

			$(".history_datepicker").datepicker({
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "yy-mm-dd",
				maxDate         : -1,
				yearRange       : "1900:"+(minDate.getFullYear())
			});


			var dataMinDate = $("#id_start_date").data('min-date');
			if (dataMinDate) {
				date = new Date(dataMinDate);
				minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : minDate,
					yearRange       : "-30:+100"
				});
			} else {
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : +1,
					yearRange       : "-30:+100"
				});

			}

			$(".chosen-select").chosen();

        });
    });


    $(".accounts").on('change', function(e){
        var rowId = $(this).attr('id').replace('account_', '');
        Journal.loadAccountVatCodes($(this), $(this).val(), rowId);

        var accountType = $(this).find(':selected').data('account-type')
        if (accountType === 1) {
            $("#distribution_" + rowId).prop('disabled', false)
        } else {
            $("#distribution_" + rowId).prop('disabled', true)
        }
    })

    $(".vat_codes").on('change', function(e){
        var rowId = $(this).attr('id').replace('vat_codes_', '');
        var description = $(this).data('description');
        $("#description_" + rowId).html(description);
    })

    $(".credit_amount").on('blur', function(){
        var rowId = $(this).attr('id').replace('credit_', '');
        if ($(this).val() !== '') {
            $("#debit_" + rowId).prop('disabled', true);
            Journal.calculateTotalCredit();
        }
    })

    $(".debit_amount").on('blur', function(){
        var rowId = $(this).attr('id').replace('debit_', '');
        if ($(this).val() !== '') {
            var amount = parseFloat( $(this).val() );
            if (amount < 0) {
                $("#credit_" + rowId).val(amount * -1);
                $("#debit_" + rowId).prop('disabled', true);
                $(this).val('');
                Journal.calculateTotalCredit();
            } else {
                 $("#credit_" + rowId).prop('disabled', true);
                Journal.calculateTotalDebit();
            }
        }
    });

    $(".distributions").on('change', function(){
        if ($(this).val() === 'yes') {
            var rowId = $(this).attr('id').replace('distribution_', '');
           Journal.openDistributionModal($(this), rowId);
        }
    });

    $("#save_close_month").on('click', function(e){
        e.preventDefault();
        Period.saveMonthClose($(this));
    });

});


var Period = (function(period, $){

	return {
        saveMonthClose: function(el)
        {
            var ajaxUrl = el.data('ajax-url');
            $.getJSON(ajaxUrl, function(responseJson){
               responseNotification(responseJson);
            });
        }
	}

}(Period || {}, jQuery))

