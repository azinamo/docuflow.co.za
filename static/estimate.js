$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");

            initDatePicker();
			initChosenSelect();
        });
    });

	$("#estimates>tbody>tr>td:not(.actions)").on("click", function(e){
		const detailUrl = $(this).parent("tr").data("detail-url");
		if (detailUrl !== undefined && detailUrl !== '') {
			document.location.href = detailUrl;
		}
		return false;
	});

	$(".save-estimate").on("click", function(e){
		e.preventDefault();
		ajaxSubmitForm($(this), $("#estimate_form"), {"next": $(this).data("action")});
	});


	var estimateItemsBody = $("#estimate_items_body");
	if (estimateItemsBody.length > 0) {
		Estimate.loadEstimateItems(estimateItemsBody, estimateItemsBody.data("ajax-url"));
	}

	var addEstimateLine = $("#add_estimate_line");
	addEstimateLine.on('click', function(){
		$(this).prop('disabled', true).data('html', addEstimateLine.text()).text('loading ...')
		Estimate.loadEstimateItems(addEstimateLine, addEstimateLine.data('ajax-url'));
	});

	$(".action-estimate").on('click', function (e) {
		e.preventDefault();
		Estimate.actionEstimate($(this));
    });

	$(".bulk-action").on('click', function(e){
		e.preventDefault();
		Estimate.bulkInvoicesAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		Estimate.singleInvoiceAction($(this));
	});

	var idCustomer = $("#id_customer");
	idCustomer.on('change', function(e){
		e.preventDefault();
		//Estimate.loadCustomerDetails(idCustomer);
	});
	
	$("#id_is_deposit_required").on("change", function (e) {
		var depositRequired = $("#deposit_required");
		var idDeposit = $("#id_deposit");
		if ($(this).is(":checked")) {
			depositRequired.show();
		} else {
			idDeposit.val("");
			depositRequired.hide();
		}
    });



});


var Estimate = (function(estimate, $){

	return {

	    loadCustomerDetails: function(el)
		{
			console.log('Load customer details');
			var ajaxUrl = $("#id_customer option:selected").data('detail-url');
			var vatNumber = $("#id_customer option:selected").data('detail-url');
			console.log( el );
			console.log( ajaxUrl );
			$.getJSON(ajaxUrl, {
				module: 'sales'
			}, function(response){
				console.log( response )
				console.log( response.is_default )
				if (response.is_default) {
					console.log("load customer form")
					Estimate.loadCashCustomerForm(response)
				} else {
					console.log("load customer details")
					Estimate.displayCustomerDetails(response)
				}
				initChosenSelect();
			});
		},

		displayCustomerDetails: function(response)
		{
			var postalAddressHtml = [];
			var deliveryAddressHtml = [];
			if (response.hasOwnProperty("postal")) {
				var postalAddress;
				console.log(typeof response.postal);
				if (typeof response.postal === "string") {
					postalAddress = JSON.parse(response.postal);
				} else {
					postalAddress = response.postal;
				}
				postalAddressHtml.push("<p class='mb-1'>"+postalAddress.address+"</p>");
				postalAddressHtml.push("<p class='mb-1'>"+postalAddress.city+"</p>");
				postalAddressHtml.push("<p class='mb-1'>"+postalAddress.province+"</p>");
				postalAddressHtml.push("<p class='mb-1'>"+postalAddress.country+"</p>");
			}
			if (response.hasOwnProperty("delivery")) {
				var deliveryAddress;
				console.log(typeof response.delivery)
				if (typeof response.delivery === "string") {
					deliveryAddress = JSON.parse(response.delivery);
				} else {
					deliveryAddress = response.delivery;
				}
				deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.address+"</p>");
				deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.city+"</p>");
				deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.province+"</p>");
				deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.country+"</p>");
			}
			if (response.hasOwnProperty("discount_terms")) {
				var discountTerms = response.discount_terms;
				if (discountTerms) {
					if (typeof response.discount_terms === "string") {
						discountTerms = JSON.parse(response.discount_terms);
					} else {
						discountTerms = response.discount_terms;
					}
					if (discountTerms.hasOwnProperty("line")) {
						if (discountTerms.line.discount && discountTerms.line.is_discounted)
						$("#line_discount").val(discountTerms.line.discount);
						$(".discount").each(function(){
							if ($(this).val() === "") {
								$(this).val(discountTerms.line.discount)
							}
						});
					}
					if (discountTerms.hasOwnProperty("invoice")) {
						if (discountTerms.invoice.discount && discountTerms.invoice.is_discounted) {
							$("#invoice_discount").val(discountTerms.invoice.discount);
						}
					}
				}
			}
			// if (response.credit_limit) {
			// 	postalAddressHtml.push("<div class='limits'>");
			// 	postalAddressHtml.push("<input type='hidden' name='credit_limit' id='credit_limit' value='" + accounting.unformat(response.credit_limit) +"'></div>");
			// 	postalAddressHtml.push("<span id='credit_limit_val'>" +accounting.formatMoney(response.credit_limit)+"</span>");
			// 	postalAddressHtml.push("<div>" +accounting.unformat(response.credit_limit)+"</div>");
			// 	postalAddressHtml.push("</div>")
			// } else {
			// 	console.log('Something wrong with limit')
			// }
			// if (response.available_credit || response.credit_limit) {
			// 	postalAddressHtml.push("<div class='limit'>");
			// 	postalAddressHtml.push("<div><span><strong>Available Credit</strong></span>" +accounting.formatMoney(response.available_credit)+"</div>");
			// 	postalAddressHtml.push("<input type='hidden' name='available_credit' id='available_credit' value='" + accounting.unformat(response.credit_limit) +"'></div>");
			// 	postalAddressHtml.push("</div>")
			// } else {
			// 	console.log('Something wrong with limit')
			// }
			if (postalAddressHtml.length > 0) {
				$("#customer_details").html(postalAddressHtml.join(" "))
			}
			if (deliveryAddressHtml.length > 0) {
				$("#shipping_to").html(deliveryAddressHtml.join(" "))
			}
			if (response.hasOwnProperty("vat_number")) {
				$("#id_vat_number").val(response.vat_number)
			}

			if (response.hasOwnProperty("pricings")) {
				var html = [];
				$.each(response.pricings, function(index, pricing){
					console.log( pricing )
					html.push("<option value='" + pricing.id + "'>"+ pricing.name+"</option>")
				})
				$("#id_pricing").html(html.join(' '));
			}
			$("#customer_full_name").html("");
			$(".cash").hide();
			$(".customer").show();
			$(".save-cash-invoice").hide();
			var saveBtn = $("#submit_invoice_and_continue");
			saveBtn.show();
		},

		loadCashCustomerForm: function(customerData)
		{
			$("#id_vat_number").val("");
			$("#customer_details").html(customerData['customer_form']);
			$("#shipping_to").html(customerData['delivery_form']);
			var saveCashBtn = $("#submit_cash_invoice_and_continue");
			saveCashBtn.show();
			$(".cash").show();
			$(".customer").hide();
			$(".save-invoice").hide();
		},

		bulkInvoicesAction: function(el)
		{
			var estimates = Estimate.getSelectedEstimates();
			var totalEstimates = estimates.length;
			if (totalEstimates === 0) {
				alert('Please select at least one estimate');
			} else {
				var ajaxUrl = el.data('ajax-url');
				var confirm = el.data('confirm');
				if (confirm === '1') {
					var confirmText = el.data('confirm-text')
					return confirm(confirmText)
				}
				console.log( estimates )
				console.log( ajaxUrl )
				$.post(ajaxUrl, {
					estimates: estimates
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singleInvoiceAction: function(el)
		{
			var estimates = Estimate.getSelectedEstimates();
			var totalEstimates = estimates.length;
			if (totalEstimates === 0) {
				alert('Please select at least one estimate');
			} else if (totalEstimates >1) {
				alert('Please select one estimate to action');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( estimates )
				console.log( ajaxUrl )
				$.post(ajaxUrl, {
					estimates: estimates
				}, function(response){
					responseNotification(response);
				});
			}
		},

		getSelectedEstimates: function()
		{
			var estimates = []
			$(".estimate-action").each(function(){
				if ($(this).is(":checked")) {
                    estimates.push($(this).data('estimate-id'));
                }
			});
			return estimates;
		},

		actionEstimate: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			$.getJSON(ajaxUrl, function(response){
				responseNotification(response)
			});
		},

		reloadEstimateRow: function(el)
		{
			var rowId = el.data('row-id');
			var ajaxUrl = el.data('ajax-url');
			var chosenWidth = $("#"+ rowId + "_estimate_item_chosen").width();
			$.get(ajaxUrl, {
				row_id:rowId,
				line_type: $("#type_"+ rowId +" option:selected").val()
			}, function(html) {
				$("#estimate_row_" + rowId).replaceWith(html);

				Estimate.handleRow(rowId);
				// $(".chosen-select").chosen();
				if (chosenWidth > 0) {
					$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
					$(".line-type").chosen({'width': '40px'});
				} else {
					$(".chosen-select").not('.line-type').chosen({'width': '250px'});
					$(".line-type").chosen({'width': '40px'});
				}
			});

		},

		handleRow: function(rowId)
		{
			$("#inventory_item_" + rowId).on('change', function(){
				var customerLineDiscount = $("#line_discount").val();
				console.log( "Row --> ", rowId, " and load fields")
				var selectedOption = $("#inventory_item_"+ rowId +" option:selected");
				var price = selectedOption.data('price');
				var priceIncluding = selectedOption.data('price-including');
				var unit = selectedOption.data('unit');
				var binLocation = selectedOption.data('bin-location');
				var description = selectedOption.data('description');
				var vatId = selectedOption.data('vat-id');
				var availableStock = selectedOption.data('stock');
				var unitId = selectedOption.data('unit-id');
				var isDiscountable = selectedOption.data('is-discountable');
				var priceEl =  $("#price_" + rowId);
				var priceIncEl =  $("#price_including_" + rowId);

				console.log( "price --> ", price, ' add to ', $("#price_" + rowId ));
				console.log( "price including--> ", priceIncluding, ' add to ', priceIncEl);
				console.log( "unit --> ", unit, ' add to ', $("#unit_" + rowId ));
				console.log( "is discountable --> ", isDiscountable, ' disable discount field ', typeof isDiscountable);
				priceEl.val( price );
				priceIncEl.val( priceIncluding );
				if (unit !== undefined && unit !== '') {
					$("#unit_" + rowId).val(unit);
				}
				if (unitId !== undefined && unitId !== '') {
					$("#unit_id_" + rowId).val(unitId);
				}

				if (binLocation !== undefined && binLocation !== '') {
					$("#bin_location_" + rowId).val(binLocation)
				}
				if (description !== undefined && description !== '') {
					$("#description_" + rowId).val(description)
				}
				if (vatId !== undefined && vatId !== '') {
					$("#vat_code_" + rowId).val(vatId).trigger("chosen:updated").trigger('change');
				} else {
					$("#vat_code_" + rowId).val('').trigger("chosen:updated").trigger('change');
				}
                Estimate.handleRowDiscount(rowId, isDiscountable, customerLineDiscount);
				availableStock = parseFloat(availableStock);
				if (availableStock > 0) {
					$("#order_item_" + rowId).html('No');
				} else {
					$("#order_item_" + rowId).html('Yes');
				}
			});

			$(".line-type").on('change', function(e){
				Estimate.reloadEstimateRow($(this));
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				Estimate.calculateInventoryItemPrices(rowId);
			});

			$("#price_"+ rowId +"").on('blur', function(){
				Estimate.calculateInventoryItemPrices(rowId);
			});

			$("#vat_code_"+ rowId).on('change', function(){
				var selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
				var vatPercentage = selectedVatOption.data('percentage');
				console.log('Vat code changes --> ', vatPercentage);
				if (vatPercentage !== undefined && vatPercentage !== '') {
					$("#vat_percentage_"+ rowId).val(vatPercentage);
				} else {
					$("#vat_percentage_"+ rowId).val(0);
				}
				Estimate.calculateInventoryItemPrices(rowId);
			});

			$("#discount_percentage_"+ rowId +"").on('blur', function(){
				Estimate.calculateInventoryItemPrices(rowId);
			});
		},

        handleRowDiscount: function(rowId, isDiscountable, lineDiscount)
        {
        	console.log($("#line_discount"));
			console.log($("#line_discount").val());
            console.log("Handling row discount, is discountable -> ", isDiscountable, typeof isDiscountable, lineDiscount );
            var discountPercentageEl = $("#discount_percentage_" + rowId);
            if (isDiscountable) {
                var discountVal = lineDiscount || 0;
                console.log("Now discount value is ", lineDiscount)
                discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
            } else {
                discountPercentageEl.val(0).prop('disabled', true).trigger('change');
                $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
                $("#discount_amount_" + rowId).val('0.00').trigger('change');
            }
        },

		loadEstimateItems: function(el, ajaxUrl)
		{
			var rowId = 0;
			var lastRow = $(".estimate_item_row").last();
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			Estimate.loadEstimateRow(el, ajaxUrl, rowId, lastRow)
		},

		loadEstimateRow: function(el, ajaxUrl, rowId, lastRow)
		{
			var chosenWidth = $("#"+ rowId + "_estimate_item_chosen").width();
			var container = $("#estimate_items_placeholder");
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow !== undefined && lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				var _lastRow = $(".estimate_item_row").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}
				Estimate.handleRow(rowId);

				initChosenSelect()
				if (chosenWidth > 0) {
					$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
					$(".line-type").chosen({'width': '40px'});
				} else {
					$(".chosen-select").not('.line-type').chosen({'width': '250px'});
					$(".line-type").chosen({'width': '40px'});
				}
				el.prop('disabled', false).text(el.data('html'));
			});
		},

		calculateInventoryItemPrices: function(rowId)
		{
			console.log("CALCULATING INVENTORY ROW ", rowId + " ---------------------------- ");
			var price = $("#price_" + rowId);
			var priceIncl = $("#price_including_" + rowId);
			var ordered = $("#quantity_ordered_" + rowId);
			var received = $("#quantity_" + rowId);
			var short = $("#quantity_short_" + rowId);
			var vat = $("#vat_amount_" + rowId);
			var vatPercentage = $("#vat_percentage_" + rowId);
			var discountPercentage = $("#discount_percentage_" + rowId);
			var discount = $("#discount_" + rowId);

			var quantityOrdered = 0;
			var shortQuantity = 0;
			var quantityReceived = 0;
			var vatAmount = 0;
			var priceExcl = 0;
			var discountAmount = 0;
			var totalVat = 0;
            var totalDiscount = 0;
            var priceIncludingAmount = 0;
            var totalInc = 0;
            var totalExcl = 0;
            var subTotal = 0

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}
			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}

			if (quantityOrdered > 0) {
				if (quantityReceived <= quantityOrdered) {
					shortQuantity =  quantityOrdered - quantityReceived
				} else {
					received.val(quantityOrdered);
					//backOrder.prop('disabled', true);
					alert('Please enter valid received quantity')
					return false;
				}
			}
			if (short !== undefined  && short.length > 0) {
                short.val( shortQuantity );
            }
			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}
			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentage/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                }
            }
            subTotal = priceExcl * quantityReceived;
			priceIncludingAmount = priceExcl + vatAmount;

			if (discountPercentage.length > 0) {
                if (discountPercentage.val() !== '' && priceExcl !== '') {
                    var discPercentage = parseFloat(discountPercentage.val());
                    console.log('Discount percentage --> ', discPercentage);
                    discountAmount = (discPercentage/100) * priceExcl;
                    console.log('Discount amount --> ', discountAmount);
                    discount.val(discountAmount)
                }
            }
            console.log("Price excluding, not discount ", priceExcl);

            console.log("Price including amount, not discount ", priceIncludingAmount);
            if (discountAmount > 0) {
			    priceExcl = priceExcl - discountAmount
            }

			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentage/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                    vat.val(vatAmount)
                }
            }

			// if (vat !== undefined && vat.length > 0) {
			// 	if (vat.val() !== '') {
			//
			// 		vatAmount += parseFloat(accounting.unformat(vat.val()));
			// 	}
			// }

			if (discountAmount !== '') {
			    totalDiscount = discountAmount * quantityReceived;
            }
            totalExcl = priceExcl * quantityReceived;
			totalInc = totalExcl;
            if (vatAmount !== '') {
            	console.log("Vat amount", vatAmount);
            	console.log("Price including + Vat amount", priceIncludingAmount, vatAmount);
			    totalVat = vatAmount * quantityReceived;
			    totalInc += totalVat;
            }
			//var totalInc = priceIncludingAmount * quantityReceived;
			console.log( " quantityReceived ", quantityReceived );
			console.log( " Price EXCL", priceExcl );
			console.log( " totalExcl ", totalExcl );
			console.log( " vatAmount ", vatAmount );
			console.log( " discountAmount ", discountAmount );
			console.log( " priceIncl ", priceIncludingAmount);
			console.log( " totalInc", totalInc );

			// if (backOrder !== undefined) {
			// 	if (shortQuantity === 0) {
			// 		backOrder.prop('disabled', true);
			// 	} else {
			// 		backOrder.prop('disabled', false);
			// 	}
			// }

			$("#vat_total_" + rowId).html( accounting.formatMoney(totalVat) );
			$("#total_vat_" + rowId).val( totalVat );

			$("#discount_amount_" + rowId).html( accounting.formatMoney(totalDiscount) );
			$("#total_discount_" + rowId).val(totalDiscount);

			$("#price_incl_" + rowId).html( accounting.formatMoney(priceIncl) );
			priceIncl.val( priceIncludingAmount );
			$("#sub_total_" + rowId).val( subTotal );


			$("#total_price_exl_" + rowId).html( accounting.formatMoney(totalExcl) );
			$("#total_price_excluding_" + rowId).val( totalExcl );

			$("#total_price_incl_" + rowId).html( accounting.formatMoney(totalInc) );
			$("#total_price_including_" + rowId).val( totalInc );

			Estimate.calculateTotals()
		},

		calculateTotals: function()
		{
			var totalOrdered = 0;
			var totalReceived = 0;
			var totalShort = 0;
			var priceExc = 0;
			var priceInc = 0;
			var totalPrice = 0;
			var totalTotalExc = 0;
			var totalTotalInc = 0;

			$(".ordered").each(function(){
				totalOrdered += parseInt($(this).val());
			});

			$(".received").each(function(){
				totalReceived += parseInt($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseInt($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});

			$("#total_ordered").html( totalOrdered );
			$("#total_received").html( totalReceived );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.formatMoney(totalPrice) );
			$("#price_excl").html( accounting.formatMoney(priceExc) );
			$("#price_incl").html( accounting.formatMoney(priceInc) );
			$("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
			$("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );

		}



	}
}(Estimate || {}, jQuery))

