import React from 'react';
class TableHeader extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            title: props.title,
            direction: null,
        }
    }

    handleClick = (e) => {
        let direction = (this.state.direction === null ? 'desc' : 'asc');
        this.props.handleSort(this.state.heading.accessor, direction)
    };

    _sortClass = (filterName) => {
        //return "fa fa-fw " + ((filterName == this.state.Data.sortColumnName) ? ("fa-sort-" + this.state.Data.sortOrder) : "fa-sort");
    };

    render() {
        return (
            <th ref={this.props.heading.accessor} data-column={this.props.heading.accessor} onClick={this.handleClick}>
                <span className="header-cell">
                    <span>{this.props.heading.title}</span>
                </span>
            </th>
        );
    };
}

export default TableHeader;