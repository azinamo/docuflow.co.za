import React from "react";
import Wrapper from "./App";

function Sidebar(props) {
        return (
            <div className="col-sm-3">
                <div className="sidebar-block">
                    Sidebar block
                </div>
                <div className="clearfix"></div>
                <div className="margin-top"></div>
            </div>
        );
};

export default Sidebar;
