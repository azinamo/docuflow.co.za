import React from  'react';
import Filters from './Filters';
import TableCell from './TableCell';
import TableHeader from "./TableHeader";
import TableRow from "./TableRow";


class DataTable extends React.Component {

    constructor(props)
    {
        super(props);
        this.state = {
            url: props.url,
            sortBy: null,
            descending: null,
            headings: props.headings,
            results: [],
            isLoading: false,
            placeholder: "Loading...",
            totalItems: 0,
            perPage :'',
            lastPage:'',
            currentPage:'',
            nextPageUrl:'',
            firstPageUrl:'',
            lastPageUrl:'',
            prevPageUrl:'',
            testType:'',
            sortColumn:'',
            sortOrder:'',
            loading:true,
            filters: props.filters
        };
        this.keyField = props.keyField || 'id';
        this.noData = props.noData || 'No records found';
        this.width = props.width || '100%';
    }

    componentWillReceiveProps()
    {
        console.log('DataTable---------- componentWillReceiveProps ----------------');
        console.log('filters');
        console.log(filters);
        console.log('prop filters');
        console.log(this.props.filters);
        if (filters !== this.props.filters) {
            console.log("Reload data again")
            // this.setState({filters: filters});
            this.loadData(this.state.url, filters);
        }
    }
//

    componentDidMount() {
        this.loadData(this.state.url)
    }

    loadData = (url) => {
        this.setState({isLoading: true});

        const _filters = this.objectToQueryString(this.state.filters);
        console.log("--- State filters ---");
        console.log(this.props.filters);
        console.log("--- filters ---");
        console.log(_filters);
        console.log( url+`?${_filters}` )
        fetch(url+`?${_filters}`)
            .then(response => {
                if (response.status !== 200) {
                    return this.setState({ placeholder: "Something went wrong" });
                }
                return response.json();
            })
            .then((data) => {
                this.setState({
                    results: data.results,
                    nextPageUrl: data.next,
                    prevPageUrl: data.previous,
                    totalItems: data.count,
                    isLoading: false
                })
            });
    };

    objectToQueryString = (params = {}) => {
        const keyValuePairs = [];
        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                let val = params[key];
                console.log(val);
                console.log(val.length);
                console.log( typeof val);
                if (val !== '' && val.length > 0) {
                    keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(val));
                }
            }
        }
        return keyValuePairs.join('&');
    };



    sortTable = (key, e) => {
        e.preventDefault();
        this.state.sortColumn = key;
        this.state.sortOrder = (this.state.sortOrder === '' || this.state.sortOrder === 'asc') ? 'desc' : 'asc';
        // let url = constant.APP_URL+'api/get-student?searchColumn=name&searchQuery='+this.state.searchQuery+'&test_type='+this.state.test_type
        //     +'&sortColumn='+this.state.sortColumn+'&sortOrder='+this.state.sortOrder;
        this.loadData(this.state.url);
    };


    handleSorting = (sortBy, direction) => {
        console.log("Sorting", sortBy, direction)
    };

    renderTableHeader = () => {
        let headers = this.state.headings;
        return headers.map((heading) => {
            return (<TableHeader heading={heading}
                                 key={heading.accessor}
                                 handleSort={this.handleSorting} />
                   )
        });
    };

    renderNoData = () => {
      return (
          <tr>
              <td colSpan={this.props.headings.length}>{this.noData}</td>
          </tr>
      )
    };

    openDetailPage = (e, row) => {
        if (row.hasOwnProperty('detail_url')) {
            var detailPageUrl = row['detail_url'];
            document.location.href = detailPageUrl;
        }
    };

    renderTableRows = () => {
        let headers = this.state.headings;
        let results = this.state.results;
        let tableRows = results.map((row, rowIndex) => {
            let tdsContent = headers.map((header, headerIndex) => {
                let content = row[header.accessor];
                let id = row[this.keyField];
                return (
                    <td key={id + ' - ' + headerIndex} data-id={id} data-row={rowIndex}  data-cell={headerIndex}>
                        {content}
                    </td>
                )
            });
            return (
                <tr key={rowIndex + '-row'} onClick={(e) => this.openDetailPage(e, row)}>
                    {tdsContent}
                </tr>
            )
        });
        return tableRows;
    };

    renderTableBody = () => {
        if (this.state.results && this.state.results.length > 0) {
            return this.renderTableRows();
        } else {
            return this.renderNoData()
        }
    };

    pagination = (target, e, i = null) => {
        e.preventDefault();
        this.setState({isLoading: true});

        switch (target) {
            case 'prev':
                if (this.state.prevPageUrl !== null) {
                    this.loadData(this.state.prevPageUrl);
                }
                break;
            case 'next':
                if (this.state.nextPageUrl !== null) {
                    this.loadData(this.state.nextPageUrl);
                }
                break;
        }
        this.setState({isLoading: false});
    };


    renderPagination = () => {
        console.log('Render pagination-->');
        let rows = [];
        for (let i = 1; i <= this.state.lastPage; i++) {
            rows.push(<li className="page-item" key={i}><a className="page-link" href="#" onClick={(e)=>this.pagination('btn-click',e,i)}>{i}</a></li>);
        }
        console.log(rows);
        return (
            <div>
                <div className="dataTables_paginate paging_simple_numbers" id="example23_paginate">
                    <ul className="pagination justify-content-end">
                        <li className="page-item">
                            <a className="page-link" href="#" onClick={(e)=>this.pagination('prev',e)}>Previous</a>
                        </li>
                        {rows}
                        <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('next',e)}>Next</a></li>
                    </ul>
                </div>
            </div>
        )
    };

    render() {
        let rows = [];
        for (let i = 1; i <= this.state.lastPage; i++) {
            rows.push(<li className="page-item" key={i}><a className="page-link" href="#" onClick={(e)=>this.pagination('btn-click',e,i)}>{i}</a></li>);
        }
        return (
            <div className="table-responsive">
                <div>
                    <table className="data-inner-table table">
                        <thead>
                        <tr>
                            {this.renderTableHeader()}
                        </tr>
                        </thead>
                        <tbody>
                        {!this.state.isLoading ?
                            this.renderTableBody()
                            :
                            <tr className='sweet-loading' style={{'textAlign': 'center'}}>
                                <td colSpan={this.state.headings.length}>Loading payments ...</td>
                            </tr>
                        }
                        </tbody>
                    </table>
                    <div className="dataTables_paginate paging_simple_numbers" id="example23_paginate">
                        <ul className="pagination justify-content-end">
                            <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('prev',e)}>Previous</a></li>
                            {rows}
                            <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('next',e)}>Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        );
    }
}
export default DataTable;