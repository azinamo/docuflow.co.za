import React from 'react';
import { PropTypes } from 'react'

export default class SupplierFilter extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            companySlug: props.companySlug,
            supplierState: false
        }
    }

    componentDidMount() {
        this.loadSuppliers()
    }

    handleChange = (event) => {
        this.props.handleFilterChange('supplier', event.target.value);
    };


    loadSuppliers = () => {

        axios.get('/suppliers/api/', {
            params: {'company_slug': this.state.companySlug}
        }).then((response) => {
            this.setState({ suppliers: response.results, isLoading: false })
        }).catch((error) => {
            if (error.status !== 200) {
                this.setState({ placeholder: "Something went wrong" });
            }
            console.log(error);
        });

        // fetch(`/suppliers/api/?company_slug=` + this.state.companySlug)
        //     .then(response => {
        //         if (response.status !== 200) {
        //             return this.setState({ placeholder: "Something went wrong" });
        //         }
        //         return response.json();
        //     })
        //     .then((data) => {
        //         this.setState({ suppliers: data.results, isLoading: false })
        //     });
    };


    render() {

        let {isLoading, suppliers} = this.state;
        if (isLoading || suppliers === undefined) {
            return <p>Loading....</p>
        } else {
            return (
                <div key={'suppliers-filter'}>
                    <label htmlFor={'supplier'} className="label label-control">Supplier</label>
                    <select name={'supplier'} id={'supplier'}
                            className="chosen-select filter filter-supplier form-control"
                            onChange={this.handleChange}>
                        <option key={'default'} value={""}>--select supplier-</option>
                        {suppliers.map((supplier) =>
                            <option key={'supplier-' + supplier.id}
                                    value={ supplier.id }>
                                {supplier.name}
                            </option>
                        )}
                    </select>
                </div>
            )
        }
    }
};