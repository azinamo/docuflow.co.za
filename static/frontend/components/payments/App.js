import React, {Component} from "react";
import ReactDOM from "react-dom";
import DataTable from "./DataTable";

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            payments: [],
            isLoading: false,
            url: props.url,
            placeholder: "Loading...",
            perPage :'',
            lastPage:'',
            currentPage:'',
            nextPageUrl:'',
            firstPageUrl:'',
            lastPageUrl:'',
            prevPageUrl:'',
            searchQuery:'',
            testType:'',
            sortColumn:'',
            sortOrder:'',
            loading:true
        };
    }

    componentDidMount() {
        console.log("STATE ---PROPS")
        console.log(this.state.url)
        this.loadData(this.state.url)
    }

    loadData = (url) => {
        this.setState({isLoading: true});
        fetch(url)
            .then(response => {
                if (response.status !== 200) {
                    return this.setState({ placeholder: "Something went wrong" });
                }
                return response.json();
            })
            .then((data) => {
                this.setState({
                    payments: data.results,
                    nextPageUrl: data.next,
                    prevPageUrl: data.previous,
                    isLoading: false
                })
            });
    };

    pagination = (target, e, i = null) => {
        e.preventDefault();
        this.setState({isLoading: true});

        let url;

        switch (target) {
            case 'prev':
                if (this.state.prevPageUrl !== null) {
                    this.loadData(this.state.prevPageUrl);
                }
                break;
            case 'next':
                if (this.state.nextPageUrl !== null) {
                    this.loadData(this.state.nextPageUrl);
                }
                break;
        }
        this.setState({isLoading: false});
    };

    keyPress = (e) => {
        if (e.keyCode === 13) {
            this.setState({
                isLoading: true
            });

            this.setState({
                searchQuery: e.target.value
            });

            this.loadData(this.state.url);

            this.setState({
                loading: false
            });
        }
    };

    sortTable = (key, e) => {
        e.preventDefault();
        this.state.sortColumn = key;
        this.state.sortOrder = (this.state.sortOrder === '' || this.state.sortOrder === 'asc') ? 'desc' : 'asc';
        // let url = constant.APP_URL+'api/get-student?searchColumn=name&searchQuery='+this.state.searchQuery+'&test_type='+this.state.test_type
        //     +'&sortColumn='+this.state.sortColumn+'&sortOrder='+this.state.sortOrder;
        this.loadData(this.state.url);
    };

    render() {

        // let table_row;
        //
        // if (this.state.payments !== null && this.state.payments.length > 0) {
        //     let tr;
        //     table_row = this.state.payments.map((row, index)=>{
        //         return (
        //             <tr key={index}>
        //                 <td>{row.payment_reference}</td>
        //                 <td>{row.payment_date}</td>
        //                 <td>{row.supplier}</td>
        //                 <td>{row.period}</td>
        //                 <td>{row.journal}</td>
        //                 <td>-</td>
        //             </tr>
        //         );
        //     });
        // } else {
        //     table_row = null
        // }

        let rows = [];
        for (let i = 1; i <= this.state.lastPage; i++) {
            rows.push(<li className="page-item" key={i}><a className="page-link" href="#" onClick={(e)=>this.pagination('btn-click',e,i)}>{i}</a></li>);
        }

        const headings = [
            {'title': 'Remmitance Number', 'accessor': 'payment_reference', 'index': 0},
            {'title': 'Date Paid', 'accessor': 'payment_date', 'index': 1},
            {'title': 'Supplier', 'accessor': 'supplier', 'index': 2},
            {'title': 'Total Amount', 'accessor': 'total_amount', 'index': 3},
            {'title': 'Period', 'accessor': 'period', 'index': 4},
            {'title': 'Updated', 'accessor': 'journal', 'index': 5},
            {'title': 'Actions', 'accessor': 'actions', 'index': 6},
        ];

        const {payments, isLoading} = this.state;

        return (
            <div className="page-wrapper">

                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-12 col-md-12">
                            <div className="card card-default">
                                <div className="card-body collapse show">
                                    <div className="d-flex no-block">
                                        <div className="ml-auto" style={{'display':'inherit'}}>
                                            <select className="custom-select"
                                                    onChange={(e)=>this.pagination('test_type',e)}
                                                    style={{'marginRight':'10px'}}>
                                                <option selected="">All</option>
                                                <option value="CET">CET</option>
                                                <option value="JEE">JEE</option>
                                                <option value="NEET">NEET</option>
                                            </select>

                                            <input type="text" name={"search"}
                                                   onKeyDown={(e)=>{this.keyPress(e)}} className="form-control"/>
                                        </div>
                                    </div>

                                    <div className="table-responsive">

                                        {!this.state.isLoading ?
                                            <div>
                                                <DataTable headings={headings}
                                                           rows={this.state.payments}
                                                />

                                                <div className="dataTables_paginate paging_simple_numbers" id="example23_paginate">
                                                    <ul className="pagination justify-content-end">
                                                        <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('prev',e)}>Previous</a></li>
                                                        {rows}
                                                        <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('next',e)}>Next</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            :
                                            <div className='sweet-loading' style={{'textAlign':'center'}}>
                                                <p>Loading ...</p>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
        // return (
        //     <div className="card-body collapse show">
        //         <div className="d-flex no-block">
        //             <h4 className="card-title">Student Report<br/>
        //                 <small className="text-muted">Student test report</small>
        //             </h4>
        //             <div className="ml-auto" style={{'display':'inherit'}}>
        //                 <select className="custom-select"
        //                         onChange={(e)=>this.pagination('test_type',e)}
        //                         style={{'marginRight':'10px'}}>
        //                     <option selected="">All</option>
        //                     <option value="CET">CET</option>
        //                     <option value="JEE">JEE</option>
        //                     <option value="NEET">NEET</option>
        //                 </select>
        //
        //                 <input type="text" name={"search"}
        //                        onKeyDown={(e)=>{this.keyPress(e)}} className="form-control"/>
        //             </div>
        //         </div>
        //         {isLoading ?
        //             <div className='sweet-loading' style={{'textAlign':'center'}}>
        //                 <p>Loading ...</p>
        //             </div>
        //             :
        //             <div>
        //                 <DataTable headings={headings} rows={payments} />
        //             </div>
        //         }
        //     </div>
        // );
    };
};

let rootEl = document.getElementById("app");
console.log( rootEl );
console.log( rootEl.dataset );
let url = rootEl.getAttribute('data-ajax-url');
console.log('Url is ', url, ' data set url ', rootEl.dataset.url );

ReactDOM.render(<App url={rootEl.dataset.url} />, rootEl);
