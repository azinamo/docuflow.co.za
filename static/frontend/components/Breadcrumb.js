import React from "react";

function Breadcrumb(props) {
        return (
            <div className = "breadcrumbs1_wrapper">
                <div className = "container">
                    <div className = "breadcrumbs1" >
                        <a href = "index.html" > Home </a><span>/
                </span>Pages
                        <span>/</span>Hotels
                    </div>
                </div>
            </div>
        );
};

export default Breadcrumb;