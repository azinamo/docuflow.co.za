import React, {Component} from "react";
import ReactDOM from "react-dom";
import DataTable from "./DataTable";
import SupplierFilter from "./payments/SupplierFilter";
import TableHeader from "./TableHeader";

import { debounce } from 'lodash'

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            url: props.url,
            companySlug: props.companySlug,
            searchQuery: '',
            filters: {},
            results: [],
            headings: [],
            isLoading: false,
            placeholder: "Loading...",
            totalItems: 0,
            perPage :'',
            lastPage:'',
            currentPage:'',
            nextPageUrl:'',
            firstPageUrl:'',
            lastPageUrl:'',
            prevPageUrl:'',
            testType:'',
            sortColumn:'',
            sortOrder:'',
        };
        this.timeout =  0;
    }

    componentDidMount() {
        this.loadData(this.state.url)
    }

    getPayments() {

    }

    loadData = () => {
        console.log('----jquery api------');
        this.setState({isLoading: true});
        let params = this.state.filters;
        //const _filters = this.objectToQueryString(this.state.filters);
        console.log("--- State filters ---");
        console.log(this.state.filters);
        console.log("--- filters ---");
        console.log( this.props.url );
        console.log( params );
        params['company_slug'] = this.state.companySlug
        // $.ajax({
        //     url: ,
        //     type: 'GET',
        //     dataType: 'json',
        //     data: params,
        //     success: function(data) {
        //         this.setState({
        //             results: data.results,
        //             nextPageUrl: data.next,
        //             prevPageUrl: data.previous,
        //             totalItems: data.count,
        //             isLoading: false
        //         })
        //     }.bind(this),
        //     error: function(response) {
        //         if (response.status !== 200) {
        //             return this.setState({ placeholder: "Something went wrong" });
        //         }
        //     }.bind(this)
        // });
        axios.get(this.props.url, {
            params: params
        }).then((response) => {
            console.log('Response')
            console.log(response)
            this.setState({
                results: response.data.results,
                nextPageUrl: response.data.next,
                prevPageUrl: response.data.previous,
                totalItems: response.data.count,
                isLoading: false
            })
        }).catch((error) => {
            if (error.status !== 200) {
                this.setState({ placeholder: "Something went wrong" });
            }
            console.log(error);
        });
    };

    objectToQueryString = (params = {}) => {
        const keyValuePairs = [];
        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                let val = params[key];
                console.log(val);
                console.log(val.length);
                console.log( typeof val);
                if (val !== '' && val.length > 0) {
                    keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(val));
                }
            }
        }
        return keyValuePairs.join('&');
    };


    raiseDoSearchWhenUserStoppedTyping = debounce(
    () => {
        this.loadData()
    }, 300);

    keyPress = (e) => {
        console.log(e.keyCode);
        if (e.keyCode !== 13) {
            let searchText =  e.target.value;

            console.log('Update the search query state');
            this.setState({'filters': {'q': searchText}}, () => {
                this.raiseDoSearchWhenUserStoppedTyping();
            });
        }
    };

    handleFilters = (filterKey, filterValue) => {
        console.log("Handle filters -- ", filterKey, filterValue);

        if (filterValue !== "") {
            let supplierFilter = {};
            supplierFilter[filterKey] = filterValue;
            console.log(supplierFilter)
            this.setState({filters: supplierFilter});
            this.loadData()
        }
        console.log("filters");
        console.log(this.state.filters);

    };



    renderTableHeader = () => {
        const headings = [
            {'title': 'Remmitance Number', 'accessor': 'payment_reference', 'index': 0},
            {'title': 'Date Paid', 'accessor': 'payment_date', 'index': 1},
            {'title': 'Supplier', 'accessor': 'supplier', 'index': 2},
            {'title': 'Total Amount', 'accessor': 'total_amount', 'index': 3},
            {'title': 'Period', 'accessor': 'period', 'index': 4},
            {'title': 'Updated', 'accessor': 'journal', 'index': 5},
            {'title': 'Actions', 'accessor': 'actions', 'index': 6},
        ];
        let headers = this.state.headings;
        return headings.map((heading) => {
            return (<TableHeader heading={heading}
                                 key={heading.accessor}
                                 handleSort={this.handleSorting} />
            )
        });
    };

    renderNoData = () => {
        const headings = [
            {'title': 'Remmitance Number', 'accessor': 'payment_reference', 'index': 0},
            {'title': 'Date Paid', 'accessor': 'payment_date', 'index': 1},
            {'title': 'Supplier', 'accessor': 'supplier', 'index': 2},
            {'title': 'Total Amount', 'accessor': 'total_amount', 'index': 3},
            {'title': 'Period', 'accessor': 'period', 'index': 4},
            {'title': 'Updated', 'accessor': 'journal', 'index': 5, 'status': true},
            {'title': 'Actions', 'accessor': 'actions', 'index': 6},
        ];
        return (
            <tr>
                <td colSpan={headings.length}>{this.noData}</td>
            </tr>
        )
    };

    openDetailPage = (e, row) => {
        if (row.hasOwnProperty('detail_url')) {
            const detailPageUrl = row['detail_url'];
            if (detailPageUrl !== "") {
                document.location.href = detailPageUrl;
            }
        }
    };

    renderTableRows = () => {
        const headings = [
            {'title': 'Remmitance Number', 'accessor': 'payment_reference', 'index': 0},
            {'title': 'Date Paid', 'accessor': 'payment_date', 'index': 1},
            {'title': 'Supplier', 'accessor': 'supplier', 'index': 2},
            {'title': 'Total Amount', 'accessor': 'total_amount', 'index': 3},
            {'title': 'Period', 'accessor': 'period', 'index': 4},
            {'title': 'Updated', 'accessor': 'journal', 'index': 5, status: true},
            {'title': 'Actions', 'accessor': 'actions', 'index': 6, 'actions': true},
        ];
        let headers = this.state.headings;
        let results = this.state.results;
        return results.map((row, rowIndex) => {
            let tdsContent = headings.map((header, headerIndex) => {
                let content = row[header.accessor];
                let id = row[this.keyField];
                return (
                    <td key={id + ' - ' + headerIndex} data-id={id} data-row={rowIndex}  data-cell={headerIndex}>
                        {header.status && content !==  '' ?
                            <span className={"ui-state ui-state-ok-text alert-success"}><i className="fa fa-close" /></span>
                            :
                            header.actions ?
                                <span>
                                    <a href="" data-toggle="tooltip" title="" className="btn-sm btn-primary" data-original-title="Edit"><i className="fa fa-pencil" /></a>
                                    <a href="" data-toggle="tooltip" title="" className="btn-sm btn-danger" data-original-title="Delete"><i className="fa fa-trash" /></a>
                                </span>
                                :
                            content
                        }
                    </td>
                )
            });
            return (
                <tr key={rowIndex + '-row'} onClick={(e) => this.openDetailPage(e, row)}>
                    {tdsContent}
                </tr>
            )
        });
    };

    renderTableBody = () => {
        if (this.state.results && this.state.results.length > 0) {
            return this.renderTableRows();
        } else {
            return this.renderNoData()
        }
    };

    pagination = (target, e, i = null) => {
        e.preventDefault();
        this.setState({isLoading: true});

        switch (target) {
            case 'prev':
                if (this.state.prevPageUrl !== null) {
                    this.loadData(this.state.prevPageUrl);
                }
                break;
            case 'next':
                if (this.state.nextPageUrl !== null) {
                    this.loadData(this.state.nextPageUrl);
                }
                break;
        }
        this.setState({isLoading: false});
    };


    renderPagination = () => {
        console.log('Render pagination-->');
        let rows = [];
        for (let i = 1; i <= this.state.lastPage; i++) {
            rows.push(<li className="page-item" key={i}><a className="page-link" href="#" onClick={(e)=>this.pagination('btn-click',e,i)}>{i}</a></li>);
        }
        console.log(rows);
        return (
            <div>
                <div className="dataTables_paginate paging_simple_numbers" id="example23_paginate">
                    <ul className="pagination justify-content-end">
                        <li className="page-item">
                            <a className="page-link" href="#" onClick={(e)=>this.pagination('prev',e)}>Previous</a>
                        </li>
                        {rows}
                        <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('next',e)}>Next</a></li>
                    </ul>
                </div>
            </div>
        )
    };

    render() {

        const headings = [
            {'title': 'Remmitance Number', 'accessor': 'payment_reference', 'index': 0},
            {'title': 'Date Paid', 'accessor': 'payment_date', 'index': 1},
            {'title': 'Supplier', 'accessor': 'supplier', 'index': 2},
            {'title': 'Total Amount', 'accessor': 'total_amount', 'index': 3},
            {'title': 'Period', 'accessor': 'period', 'index': 4},
            {'title': 'Updated', 'accessor': 'journal', 'index': 5},
            {'title': 'Actions', 'accessor': 'actions', 'index': 6},
        ];
        //this.setState({headings: headings});
        let rows = [];
        for (let i = 1; i <= this.state.lastPage; i++) {
            rows.push(<li className="page-item" key={i}><a className="page-link" href="#" onClick={(e)=>this.pagination('btn-click',e,i)}>{i}</a></li>);
        }

        return (
            <div className="panel panel-primary">
                <div className="panel-heading clearfix">
                    <div className="pull-left">
                        <SupplierFilter handleFilterChange={this.handleFilters} companySlug={this.state.companySlug} />
                    </div>
                    <div className="pull-right">
                        {this.state.isLoading ?
                            <span id="loading" className="text-right">Loading payments ...{this.state.isLoading}</span>
                            :
                            <span />
                        }
                        <input type="text"
                               placeholder={"search ..."}
                               name={"search"}
                               onKeyDown={(e)=>{this.keyPress(e)}}
                               className="filter search form-control form-control-sm" />
                    </div>

                </div>
                <div className="panel-body">

                    <div className="d-flex no-block">
                        <div className="ml-auto" style={{'display':'inherit'}}>
                        </div>
                    </div>
                    <div className="d-flex no-block">

                    </div>
                    <div className="table-responsive">
                        <div>
                            <table className="data-inner-table table">
                                <thead>
                                <tr>
                                    {this.renderTableHeader()}
                                </tr>
                                </thead>
                                <tbody>
                                {this.renderTableBody()}
                                </tbody>
                            </table>
                            <div className="dataTables_paginate paging_simple_numbers" id="example23_paginate">
                                <ul className="pagination justify-content-end">
                                    <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('prev',e)}>Previous</a></li>
                                    {rows}
                                    <li className="page-item"><a className="page-link" href="#" onClick={(e)=>this.pagination('next',e)}>Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
};

let rootEl = document.getElementById("app");
console.log( rootEl );
console.log( rootEl.dataset );
let url = rootEl.getAttribute('data-ajax-url');
let companu_slug = rootEl.getAttribute('data-company-slug');
console.log('Url is ', url, ' data set url ', rootEl.dataset.url );

ReactDOM.render(<App url={rootEl.dataset.url} companySlug={companu_slug} />, rootEl);
