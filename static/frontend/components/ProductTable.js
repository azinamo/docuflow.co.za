import React from 'react';
import TableHeader from './ProductTableHeader';
import TableRow from './ProductRow';

class ProductTable extends React.Component {

    render() {
        let productsArray = Object.keys(this.props.products).map((pid) => this.props.products[pid]);
        let rows = [];

        productsArray.forEach((product) => {
            rows.push(
                <TableRow product={product} key={product.id}></TableRow>
            )
        });

        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <TableHeader column="name"></TableHeader>
                            <TableHeader column="price"></TableHeader>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </table>
            </div>
        )
    };

}

export default ProductTable;