import React from "react";
import PropTypes from "prop-types";
import key from "weak-key";


export default class TableCell extends React.Component {

    constructor(props)
    {
        super(props);
        this.state = {
            column: props.column,
            row: props.row
        }
    }

    render() {
        return (<th>{this.state.row[this.state.column]}</th>);
    }
};

