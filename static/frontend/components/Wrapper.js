import React from "react";

function Wrapper(props) {
        return (
            <div id="content">
                <div className="container">
                    <div className="row">
                        {props.children}
                    </div>
                </div>
            </div>
        );
};

export default Wrapper;