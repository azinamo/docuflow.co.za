import React from 'react';
import TableCell from "./TableCell";

class TableRow extends React.Component {

    constructor(props)
    {
        super(props)
        this.state = {
            headers: props.headers,
            keyField: props.keyField,
            row: props.row,
            rowIndex: props.rowIndex
        }
    }

    renderTableCell = () => {
        return this.state.headers.map((header, headerIndex) => {
            return(<TableCell column={header.accessor}
                              title={header.title}
                              row={this.state.row} />
            )
        });
    };

    render() {
        let rowIndex = this.state.rowIndex;
        return (
            <tr>
                {this.renderTableCell}
            </tr>
        )
    }
}

export default TableRow;