$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
            $(data).modal("show");
			var form = $(data).find("form");
			initDatePicker();
			initChosenSelect()

        });
    });


	$(".save-delivery").on("click", function(e){
		e.preventDefault();
		var next = $(this).data("action");
		ajaxSubmitForm($(this), $("#delivery_form"), {"next": next});
	});


	var deliveryItemsBody = $("#delivery_items_body");
	if (deliveryItemsBody.length > 0) {
		var ajaxUrl = deliveryItemsBody.data("ajax-url");
		Delivery.loadDeliveryItems(deliveryItemsBody, ajaxUrl);
	}

	var deliveryEstimateItemsBody = $("#delivery_estimate_items_body");
	if (deliveryEstimateItemsBody.length > 0) {
		var ajaxUrl = deliveryEstimateItemsBody.data("ajax-url");
		Delivery.loadDeliveryItems(deliveryEstimateItemsBody, ajaxUrl, $("#delivery_estimate_items_placeholder"));
	}


	var addDeliveryLine = $("#add_delivery_line");
	addDeliveryLine.on('click', function(){
		$(this).prop('disabled', true).data('html', addDeliveryLine.text()).text('loading ...')
		var ajaxUrl = addDeliveryLine.data('ajax-url');
		Delivery.loadDeliveryItems(addDeliveryLine, ajaxUrl);
	});

	$(".action-delivery").on('click', function (e) {
		e.preventDefault();
		Delivery.actionDelivery($(this));
    });

	$(".bulk-action").on('click', function(e){
		e.preventDefault();
		Delivery.bulkInvoicesAction($(this));
	});

	$(".single-action").on('click', function(e){
		e.preventDefault();
		Delivery.singleInvoiceAction($(this));
	});

	$("#id_date").on('change', function(){
		var deliveryDate = $("#id_date").val()
		if (deliveryDate !== "") {
			deliveryDate = moment(deliveryDate);
		}
		var today = moment(moment().format('DD-MMM-YYYY')).add(1, 'h');
		var minTime = moment().format('HH:mm');
		var defaultTime = today.format('HH:mm');
		console.log( minTime );
		console.log( new Date(today.format('DD-MMM-YYYY HH:mm')) );
		if (today.isSame(deliveryDate)) {
			$('#id_delivery_time').timepicker({
				step: 5,
				useSelect: true,
				minTime: minTime,
				maxTime: '23:59',
				show2400: true,
				timeFormat: 'H:i'
			});
		}
	});

	$('#id_delivery_time').timepicker({
		step: 5,
		show2400: true,
		timeFormat: 'H:i',
		useSelect: true,
		showTimepicker: function(){

		}
	}).timepicker('getTime', new Date());

	var idCustomer = $("#id_customer");
	idCustomer.on('change', function(e){
		e.preventDefault();
		Delivery.loadCustomerDetails(idCustomer);
	});
});


var Delivery = (function(delivery, $){

	return {
	    loadCustomerDetails: function(el)
		{
			console.log('Load customer details');
			var ajaxUrl = $("#id_customer option:selected").data('detail-url');
			var vatNumber = $("#id_customer option:selected").data('detail-url');
			console.log( el );
			console.log( ajaxUrl );
			$.getJSON(ajaxUrl, function(response){
				var postalAddressHtml = [];
				var deliveryAddressHtml = [];
				console.log( response.postal )

				if (response.hasOwnProperty('postal')) {
					var postalAddress;
					console.log(typeof response.postal);
					if (typeof response.postal === 'string') {
						postalAddress = JSON.parse(response.postal);
					} else {
						postalAddress = response.postal;
					}
					postalAddressHtml.push("<p class='mb-1'>"+postalAddress.address+"</p>");
					postalAddressHtml.push("<p class='mb-1'>"+postalAddress.city+"</p>");
					postalAddressHtml.push("<p class='mb-1'>"+postalAddress.province+"</p>");
					postalAddressHtml.push("<p class='mb-1'>"+postalAddress.country+"</p>");
				}
				if (response.hasOwnProperty('delivery')) {
					var deliveryAddress;
					console.log(typeof response.delivery)
					if (typeof response.delivery === 'string') {
						deliveryAddress = JSON.parse(response.delivery);
					} else {
						deliveryAddress = response.delivery;
					}
					deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.address+"</p>");
					deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.city+"</p>");
					deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.province+"</p>");
					deliveryAddressHtml.push("<p class='mb-1'>"+deliveryAddress.country+"</p>");
				}
				if (response.hasOwnProperty('discount_terms')) {
					var discountTerms = response.discount_terms;
					if (typeof response.discount_terms === 'string') {
						discountTerms = JSON.parse(response.discount_terms);
					} else {
						discountTerms = response.discount_terms;
					}
					console.log(typeof discountTerms)
					console.log( discountTerms )
					if (discountTerms.hasOwnProperty('line')) {
						if (discountTerms.line.discount && discountTerms.line.is_discounted)
						$("#line_discount").val(discountTerms.line.discount);
						$(".discount").each(function(){
							if ($(this).val() === '') {
								$(this).val(discountTerms.line.discount)
							}
						});
					}
					if (discountTerms.hasOwnProperty('invoice')) {
						if (discountTerms.invoice.discount && discountTerms.invoice.is_discounted) {
                            $("#invoice_discount").val(discountTerms.invoice.discount);
                        }
					}
				}
				if (response.hasOwnProperty('delivery_method_id')) {
					var deliveryMethodEl = $("#id_delivery_method");
					if (deliveryMethodEl !== undefined) {
						console.log(response.delivery_method_id, typeof response.delivery_method_id );
						deliveryMethodEl.val(response.delivery_method_id);
					}

				}
				if (postalAddressHtml.length > 0) {
					$("#postal").html(postalAddressHtml.join(' '))
				}
				if (deliveryAddressHtml.length > 0) {
					$("#delivery").html(deliveryAddressHtml.join(' '))
				}

				if (response.hasOwnProperty('vat_number')) {
					$("#id_vat_number").val(response.vat_number)
				}

			});
		},

		bulkInvoicesAction: function(el)
		{
			var deliveries = Delivery.getSelectedDeliveries();
			var totalDeliveries = deliveries.length;
			if (totalDeliveries === 0) {
				alert('Please select at least one delivery');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( totalDeliveries )
				console.log( ajaxUrl )
				$.post(ajaxUrl, {
					deliveries: deliveries
				}, function(response){
					responseNotification(response);
				});
			}
		},

		singleInvoiceAction: function(el)
		{
			var deliveries = Delivery.getSelectedDeliveries();
			var totalDeliveries = deliveries.length;
			if (totalDeliveries === 0) {
				alert('Please select at least one delivery');
			} else if (totalDeliveries >1) {
				alert('Please select one delivery to action');
			} else {
				var ajaxUrl = el.data('ajax-url');
				console.log( deliveries )
				console.log( ajaxUrl )
				$.post(ajaxUrl, {
					deliveries: deliveries
				}, function(response){
					responseNotification(response);
				});
			}
		},

		getSelectedDeliveries: function()
		{
			var deliveries = []
			$(".delivery-action").each(function(){
				if ($(this).is(":checked")) {
                    deliveries.push($(this).data('delivery-id'));
                }
			});
			return deliveries;
		},

		actionDelivery: function(el)
		{
			console.log("Action deliveries")
			console.log(el)
			var ajaxUrl = el.data('ajax-url');
			console.log(ajaxUrl)
			$.getJSON(ajaxUrl, function(response){
				console.log(response)
				responseNotification(response)
			});
		},

		reloadDeliveryRow: function(el)
		{
			var rowId = el.data('row-id');
			var ajaxUrl = el.data('ajax-url');
			var chosenWidth = $("#"+ rowId + "_delivery_item_chosen").width();
			$.get(ajaxUrl, {
				row_id:rowId,
				line_type: $("#type_"+ rowId +" option:selected").val()
			}, function(html) {
				$("#delivery_row_" + rowId).replaceWith(html);

				Delivery.handleRow(rowId);
				// $(".chosen-select").chosen();
				if (chosenWidth > 0) {
					$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
					$(".line-type").chosen({'width': '40px'});
				} else {
					$(".chosen-select").not('.line-type').chosen({'width': '250px'});
					$(".line-type").chosen({'width': '40px'});
				}
			});

		},

		handleRow: function(rowId)
		{
			$("#inventory_item_" + rowId).on('change', function(){
				var customerLineDiscount = $("#line_discount").val();
				console.log( "Row --> ", rowId, " and load fields")
				var selectedOption = $("#inventory_item_"+ rowId +" option:selected");
				var price = selectedOption.data('price');
				var unit = selectedOption.data('unit');
				var binLocation = selectedOption.data('bin-location');
				var description = selectedOption.data('description');
				var vatId = selectedOption.data('vat-id');
				var availableStock = selectedOption.data('stock');
				var unitId = selectedOption.data('unit-id');
				var isDiscountable = selectedOption.data('is-discountable');
				var priceEl =  $("#price_" + rowId )
				console.log( "price --> ", price, ' add to ', $("#price_" + rowId ));
				console.log( "unit --> ", unit, ' add to ', $("#unit_" + rowId ));
				priceEl.val( price );
				if (unit !== undefined && unit !== '') {
					$("#unit_" + rowId).val(unit)
				}
				if (unitId !== undefined && unitId !== '') {
					$("#unit_id_" + rowId).val(unitId);
				}
				if (binLocation !== undefined && binLocation !== '') {
					$("#bin_location_" + rowId).val(binLocation)
				}
				if (description !== undefined && description !== '') {
					$("#description_" + rowId).val(description)
				}
				if (vatId !== undefined && vatId !== '') {
					$("#vat_code_" + rowId).val(vatId).trigger("chosen:updated").trigger('change');
				} else {
					$("#vat_code_" + rowId).val('').trigger("chosen:updated").trigger('change');
				}
				Delivery.handleRowDiscount(rowId, isDiscountable, customerLineDiscount);
				availableStock = parseFloat(availableStock);
				if (availableStock > 0 ) {
					$("#order_item_" + rowId).html('No');
				} else {
					$("#order_item_" + rowId).html('Yes');
				}

				$("#available_in_stock_" + rowId).html(availableStock);
				$("#in_stock_" + rowId).val(availableStock);
			});

			$(".line-type").on('change', function(e){
				Delivery.reloadEstimateRow($(this));
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				var quantity  = parseFloat($(this).val());
				var inStock = parseFloat($("#in_stock_" + rowId).val());
				var orderedQty = parseFloat($("#ordered_" + rowId).val());
				var allowNegativeStock = $("#inventory_item_" + rowId + " option:selected").data('allow-negative-stock');
				if (Delivery.isOrderable(quantity, inStock, allowNegativeStock)) {
					if (quantity < orderedQty) {
						$("#back_order_" + rowId).prop('disabled', false);
					} else {
						$("#back_order_" + rowId).prop('disabled', true).prop('checked', false);
					}

					Delivery.calculateInventoryItemPrices(rowId);
                } else {
					console.log("Does not allow negative stock")
					$("#quantity_" + rowId).val(inStock);
					$("#back_order_" + rowId).prop('disabled', false);
					Delivery.calculateInventoryItemPrices(rowId);
				}
			});

			$("#ordered_"+ rowId +"").on('blur', function(){
				var quantity  = parseFloat($(this).val());
				var inStock = parseFloat($("#in_stock_" + rowId).val());
				var allowNegativeStock = $("#inventory_item_" + rowId + " option:selected").data('allow-negative-stock');
				console.log("Quantity -> ", quantity, " in stock ", inStock, " allow negative --> ", allowNegativeStock)
				if (Delivery.isOrderable(quantity, inStock, allowNegativeStock)) {
					console.log( "We can order this even if it allows negative stock");
					console.log("DisAllow back ordering");
					//Delivery.calculateInventoryItemPrices(rowId);
					$("#back_order_" + rowId).prop('disabled', true).prop('checked', false);
					$("#quantity_" + rowId).val(quantity);
					Delivery.calculateInventoryItemPrices(rowId);
				} else {
					console.log("Allow back ordering");
					$("#quantity_" + rowId).val(inStock);
					$("#back_order_" + rowId).prop('disabled', false);
					//$("#ordered_" + rowId).val(inStock);
					Delivery.calculateInventoryItemPrices(rowId);
				}
			});

			$("#price_"+ rowId +"").on('blur', function(){
				Delivery.calculateInventoryItemPrices(rowId);
			});

			$("#vat_code_"+ rowId).on('change', function(){
				var selectedVatOption = $("#vat_code_"+ rowId +" option:selected");
				var vatPercentage = selectedVatOption.data('percentage');
				console.log('Vat code changes --> ', vatPercentage);
				if (vatPercentage !== undefined && vatPercentage !== '') {
					$("#vat_percentage_"+ rowId).val(vatPercentage);
				} else {
					$("#vat_percentage_"+ rowId).val(0);
				}
				Delivery.calculateInventoryItemPrices(rowId);
			});

			$("#discount_percentage_"+ rowId +"").on('blur', function(){
				Delivery.calculateInventoryItemPrices(rowId);
			});
		},

		isOrderable: function(quantity, inStock, allowNegativeStock)
		{
			if (quantity < inStock) {
				return true;
			}
			if (allowNegativeStock) {
				return true;
			}
			return false;
		},

        handleRowDiscount: function(rowId, isDiscountable, lineDiscount)
        {
        	console.log($("#line_discount"));
			console.log($("#line_discount").val());
            console.log("Handling row discount, is discountable -> ", isDiscountable, typeof isDiscountable, lineDiscount );
            var discountPercentageEl = $("#discount_percentage_" + rowId);
            if (isDiscountable) {
                var discountVal = lineDiscount || 0;
                console.log("Now discount value is ", lineDiscount)
                discountPercentageEl.val(discountVal).prop('disabled', false).trigger('change');
            } else {
                discountPercentageEl.val(0).prop('disabled', true).trigger('change');
                $("#total_discount_" + rowId).val('0.00').prop('disabled', false).trigger('change');
                $("#discount_amount_" + rowId).val('0.00').trigger('change');
            }
        },

		loadDeliveryItems: function(el, ajaxUrl, container)
		{
			var rowId = 0;
			var lastRow = $(".delivery_item_row").last();
			if (lastRow.length > 0) {
				console.log("Row was");
				console.log( lastRow.data('row-id') );
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			Delivery.loadDeliveryRow(el, ajaxUrl, rowId, lastRow, container)
		},

		loadDeliveryRow: function(el, ajaxUrl, rowId, lastRow, container)
		{
			var chosenWidth = $("#"+ rowId + "_delivery_item_chosen").width();
			container = container || $("#delivery_items_placeholder");

			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow !== undefined && lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				var _lastRow = $(".delivery_item_row").last();
				if (_lastRow.length > 0) {
					rowId = parseInt(_lastRow.data('row-id'));
				}
				console.log( "Row --> ", rowId)
				Delivery.handleRow(rowId);

				// $(".chosen-select").chosen();
				if (chosenWidth > 0) {
					$(".chosen-select").not('.line-type').chosen({'width': chosenWidth + "px"});
					$(".line-type").chosen({'width': '40px'});
				} else {
					$(".chosen-select").not('.line-type').chosen({'width': '250px'});
					$(".line-type").chosen({'width': '40px'});
				}
				el.prop('disabled', false).text(el.data('html'));
			});
		},

		calculateInventoryItemPrices: function(rowId)
		{
			console.log("CALCULATING INVENTORY ROW ", rowId + " ---------------------------- ");
			var price = $("#price_" + rowId);
			var priceIncl = $("#price_including_" + rowId);
			var ordered = $("#quantity_ordered_" + rowId);
			var received = $("#quantity_" + rowId);
			var short = $("#quantity_short_" + rowId);
			var vat = $("#vat_amount_" + rowId);
			var vatPercentage = $("#vat_percentage_" + rowId);
			var discountPercentage = $("#discount_percentage_" + rowId);
			var discount = $("#discount_" + rowId);
			var backOrder = $("#back_order_" + rowId);
			var inStock = $("#in_stock_" + rowId);

			var quantityOrdered = 0;
			var shortQuantity = 0;
			var quantityReceived = 0;
			var vatAmount = 0;
			var priceExcl = 0;
			var discountAmount = 0;
			var totalVat = 0;
            var totalDiscount = 0;
            var priceIncludingAmount = 0;
            var totalInc = 0;
            var totalExcl = 0;
			var subTotal = 0;

			if (ordered !== undefined && ordered.length > 0) {
				if (ordered.val() !== '') {
					quantityOrdered = parseFloat(ordered.val());
				}
			}
			if (received !== undefined && received.length > 0) {
				if (received.val() !== '') {
					quantityReceived = parseFloat(received.val());
				}
			}

			if (quantityOrdered > 0) {
				if (quantityReceived <= quantityOrdered) {
					shortQuantity =  quantityOrdered - quantityReceived
				} else {
					received.val(quantityOrdered);
					//backOrder.prop('disabled', true);
					alert('Please enter valid received quantity')
					return false;
				}
			}
			if (short !== undefined  && short.length > 0) {
                short.val( shortQuantity );
            }
			if (price !== undefined  && received.length > 0) {
				if (price.val() !== '') {
					priceExcl = parseFloat(accounting.unformat(price.val()));
				}
			}
			subTotal = priceExcl * quantityReceived;
			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentage = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentage/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                }
            }
			priceIncludingAmount = priceExcl + vatAmount;

			if (discountPercentage.length > 0) {
                if (discountPercentage.val() !== '' && priceExcl !== '') {
                    var discPercentage = parseFloat(discountPercentage.val());
                    console.log('Discount percentage --> ', discPercentage);
                    discountAmount = (discPercentage/100) * priceExcl;
                    console.log('Discount amount --> ', discountAmount);
                    discount.val(discountAmount)
                }
            }
            console.log("Price excluding, not discount ", priceExcl);

            console.log("Price including amount, not discount ", priceIncludingAmount);
            if (discountAmount > 0) {
			    priceExcl = priceExcl - discountAmount
            }

			if (vatPercentage.length > 0) {
                if (vatPercentage.val() !== '' && priceExcl !== '') {
                    var percentageVat = parseFloat(vatPercentage.val());
                    console.log('Vat percentage --> ', percentage);
                    vatAmount = (percentageVat/100) * priceExcl;
                    console.log('Vat amount --> ', vatAmount);
                    vat.val(vatAmount)
                }
            }

			// if (vat !== undefined && vat.length > 0) {
			// 	if (vat.val() !== '') {
			//
			// 		vatAmount += parseFloat(accounting.unformat(vat.val()));
			// 	}
			// }

			if (discountAmount !== '') {
			    totalDiscount = discountAmount * quantityReceived;
            }
            totalExcl = priceExcl * quantityReceived;
			totalInc = totalExcl;
            if (vatAmount !== '') {
            	console.log("Vat amount", vatAmount);
            	console.log("Price including + Vat amount", priceIncludingAmount, vatAmount);
			    totalVat = vatAmount * quantityReceived;
			    totalInc += totalVat;
            }
			//var totalInc = priceIncludingAmount * quantityReceived;
			console.log( " quantityReceived ", quantityReceived );
			console.log( " Price EXCL", priceExcl );
			console.log( " totalExcl ", totalExcl );
			console.log( " vatAmount ", vatAmount );
			console.log( " discountAmount ", discountAmount );
			console.log( " priceIncl ", priceIncludingAmount);
			console.log( " totalInc", totalInc );

			// if (backOrder !== undefined) {
			// 	if (shortQuantity === 0) {
			// 		backOrder.prop('disabled', true);
			// 	} else {
			// 		backOrder.prop('disabled', false);
			// 	}
			// }

			$("#vat_total_" + rowId).html( accounting.formatMoney(totalVat) );
			$("#total_vat_" + rowId).val( totalVat );

			$("#discount_amount_" + rowId).html( accounting.formatMoney(totalDiscount) );
			$("#total_discount_" + rowId).val(totalDiscount);

			$("#sub_total_" + rowId).val( subTotal );

			$("#price_incl_" + rowId).html( accounting.formatMoney(priceIncl) );
			priceIncl.val( priceIncludingAmount );

			$("#total_price_exl_" + rowId).html( accounting.formatMoney(totalExcl) );
			$("#total_price_excluding_" + rowId).val( totalExcl );

			$("#total_price_incl_" + rowId).html( accounting.formatMoney(totalInc) );
			$("#total_price_including_" + rowId).val( totalInc );

			Delivery.calculateTotals()
		},

		calculateTotals: function()
		{
			var totalOrdered = 0;
			var totalReceived = 0;
			var totalShort = 0;
			var priceExc = 0;
			var priceInc = 0;
			var totalPrice = 0;
			var totalTotalExc = 0;
			var totalTotalInc = 0;
			var totalVatAmount = 0;
			var totalDiscountAmount = 0;
			var subTotalAmount = 0;
			var netTotal = 0;
			var invoiceDiscountAmount = 0;

			$(".ordered").each(function(){
				totalOrdered += parseInt($(this).val());
			});

			$(".received, .quantity").each(function(){
				totalReceived += parseInt($(this).val());
			});
			$(".short").each(function(){
				totalShort += parseInt($(this).val());
			});
			$(".price").each(function(){
				totalPrice += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-exc").each(function(){
				priceExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".price-incl").each(function(){
				priceInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-excl").each(function(){
				totalTotalExc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-price-incl").each(function(){
				totalTotalInc += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-vat-amount").each(function(){
				totalVatAmount += parseFloat(accounting.unformat($(this).val()));
			});
			$(".total-discount").each(function(){
				totalDiscountAmount += parseFloat(accounting.unformat($(this).val()));
			});
			$(".sub-total").each(function(){
				subTotalAmount += parseFloat(accounting.unformat($(this).val()));
			});
			totalDiscountAmount = totalDiscountAmount * -1;
			netTotal = subTotalAmount + totalDiscountAmount;
			var invoiceDiscount = $("#invoice_discount").val();
			if (invoiceDiscount !== undefined && invoiceDiscount !== '') {
				$("#customer_discounts").show();
				if (netTotal !== '') {
					var totalInvoiceDiscount = ((parseFloat(invoiceDiscount)/100) * netTotal) * -1;
					var defaultVat = parseFloat($("#default_vat").val());
					var vatOnInvoiceDiscount = 0
					if (defaultVat !== undefined && defaultVat !== '') {
						vatOnInvoiceDiscount = 	(defaultVat/100) * totalInvoiceDiscount;
					}
					totalVatAmount += vatOnInvoiceDiscount;
					$("#customer_invoice_discounts").html(totalInvoiceDiscount);
					netTotal = netTotal + totalInvoiceDiscount;
				}
			} else {
				$("#customer_discounts").hide();
			}
			var totalOrderValue = netTotal + totalVatAmount;
			$("#sub_total").html(  accounting.formatMoney(subTotalAmount) );
			$("#net_total").html(  accounting.formatMoney(netTotal) );
			$("#total_ordered").html( totalOrdered );
			$("#total_received").html( totalReceived );
			$("#total_short").html( totalShort );
			$("#total_price").html( accounting.formatMoney(totalPrice) );
			$("#price_excl").html( accounting.formatMoney(priceExc) );
			$("#price_incl").html( accounting.formatMoney(priceInc) );
			$("#total_price_excl").html( accounting.formatMoney(totalTotalExc) );
			$("#total_price_inc").html( accounting.formatMoney(totalTotalInc) );
			$("#total_vat_amount").html( accounting.formatMoney(totalVatAmount) );
			$("#total_discount").html( accounting.formatMoney(totalDiscountAmount)  );
			$("#total_order_value").html( accounting.formatMoney(totalOrderValue) );
		}

	}
}(Delivery || {}, jQuery))

