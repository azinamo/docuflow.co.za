$(document).ready(function() {
	//Form Submit for IE Browser

	var supplier = $("#id_supplier");
	supplier.on('change', function(e){
		Payment.loadInvoices(supplier)
	});

	$(".math-operation").on('blur', function(e){
		Payment.calculateAmount($(this));
	});

	var formartMoney = debounce(function(el){
		Payment.makeMoney(el);
	}, 650);


	$("#pay_customer_invoices").on("click", function(e){
		e.preventDefault();
		Payment.paySelected($(this));
	});

	$("#make_payment").on('click', function(e){
		e.preventDefault();
		ajaxSubmitForm($(this), $("#sundry_payment"));
	});

	var totalAmount = $("#id_total_amount");
	totalAmount.on("blur", function(e){
		Payment.addPaymentLine(totalAmount)
	});


	$("#allocate_payment").on("click", function(e){
		e.preventDefault();
		ajaxSubmitForm($(this), $("#allocate_sundry_payment"));
	});

	$(".open-unpaid-supplier>td:not(.selecta)").on('click', function(e){
		Payment.openCustomerInvoices($(this).parent());
		return false;
	});

	$(".open-unpaid>td:not(.selecta)").on('click', function(e){
		Payment.openChildrenTable($(this).parent());
		return false;
	});



	$(".unpaid-check").on('change', function(e){
		e.preventDefault();
		Payment.selectParentInvoices($(this));
		Payment.allocatePayment($(this));
		//return false;
	});

	$("#pay_now").on("click", function(e){
		Payment.paySelected($(this), supplier);
	});

	$(".customer-invoice").on('change', function(e){
		Payment.allocatePayment($(this));
	});
});


var Payment = (function(payment, $){

	return {

		allocatePayment: function(el)
		{
			var parentId = el.data('customer-id');
			var amountSelected = Payment.totalInvoiceAmount(parentId);
			var availablePayments = Payment.customerAvailablePayments(parentId);
			if (amountSelected > availablePayments) {
				$(".pay-"+ parentId +"-payment-check").prop('disabled', false).prop('checked', true);
			} else {
				Payment.disableAvailablePayments();
			}
		},

		totalInvoiceAmount: function(supplierId)
		{
			var total = 0;
			$(".pay-" + supplierId + "-invoice-check").each(function(){
				var _this = $(this);
				if (_this.is(":checked")) {
					var invoiceId  = _this.val();
					var amount = $("#parent_" + supplierId + "_open_amount_" + invoiceId).val();
					total += parseFloat(amount)
				}
			});
			return total;
		},

		disableAvailablePayments: function(supplierId)
		{
			$(".pay-"+ supplierId +"-payment-check").prop('disabled', true).prop('checked', false);
		},

		customerAvailablePayments: function(supplierId)
		{
			var availablePayments = 0;
			$(".customer-"+ supplierId +"-payment-available").each(function(){
				availablePayments += parseFloat($(this).val()) * - 1;
			});
			return availablePayments;
		},

		getSelectedParent: function()
		{
			var parentId = $(".selected-tr").attr('id');
			return parentId;
		},

		getInvoicesToPay: function(parentId)
		{
			var supplierInvoices = [];
			var _invoicesClass = "pay-"+ parentId +"-invoice-check";
			$("." + _invoicesClass).each(function(){
				if ($(this).is(":checked")) {
					supplierInvoices.push($(this).val());
				}
			});
			return supplierInvoices;
		},

		getSelectedPayments: function(parentId)
		{
			var payments = [];
			var _paymentClass = "pay-"+ parentId +"-payment-check";
			$("." + _paymentClass).each(function(){
				console.log( " amount checked", $(this).is(":checked"), $(this).prop("checked"), $(this).checked )
				if ($(this).is(":checked")) {
					console.log( " supplier amount checked", $(this).is(":checked") )
					payments.push($(this).val());
				}
			});
			return payments;
		},

		paySelected: function(el)
		{
			var parentId = Payment.getSelectedParent();
			if (parentId) {
				var parentInvoices = Payment.getInvoicesToPay(parentId);
				var parentPayments = Payment.getSelectedPayments(parentId);
				if (parentInvoices.length > 0) {
					var ajaxUrl = el.data('ajax-url');
					// ajaxNotificationDialog(ajaxUrl, {
					// 	'parent_id': parentId,
					// 	'invoices': parentInvoices.join(','),
					// 	'payments': parentPayments.join(',')
					// });
					$.get(ajaxUrl, {'parent_id': parentId,
						'invoices': parentInvoices.join(','),
						'payments': parentPayments.join(',')
					}, function(response){
						responseNotification(response)
					});
				} else {
					alert("Please select the invoices to pay");
				}
			} else {
				alert("Please select the supplier to pay");
			}
		},

		selectParentInvoices: function(el)
		{
            var parentId = el.val();
            console.log( parentId );
			console.log( 'has it been selected or opened ', $(".parent-" + parentId + "-tr").hasClass('selected-tr') );
            if ( $(".parent-" + parentId + "-tr").hasClass('selected-tr')) {
				if (el.is(":checked")) {
					$(".pay-" + parentId +"-invoice-check").prop('checked', true);
				} else {
					$(".pay-" + parentId +"-invoice-check").prop('checked', false);
				}
			}
		},

		openChildrenTable: function (el)
        {
            var parentId = el.attr('id');
            if ($(".parent-" + parentId + "-tr").hasClass('selected-tr')) {
                $(".parent-" + parentId + "-row").toggle();
            } else {
				$(".unpaid-check").each(function(){
                   if (parentId !== $(this).attr('id')) {
                       $("#unpaid_" + $(this).val()).removeAttr('checked');
                       $(".parent-" + $(this).val() + "-row").hide();
                   }
                });

            	$(".parent").removeClass('selected-tr');
            	$(".parent-" + parentId + "-tr").addClass('selected-tr');
                $(".parent-" + parentId+ "-row").toggle();
            }
        },

		loadInvoices: function(el)
		{
			var supplierInvooceUrl = el.data('supplier-invoices-url');
			if (supplierInvooceUrl) {
				$.get(supplierInvooceUrl, {
					supplier_id: el.val()
				}, function(invoicesHtml){
					$("#customer_sundry_invoices_payment").replaceWith(invoicesHtml)

					$(".allocate-payment").on('blur', function(e){
						e.preventDefault();
						Payment.calculateInvoicePayment($(this));
						Payment.calculateAllocated();
					});

					$('.allocated').on('blur', function(e){
						e.preventDefault();
						var calculated = Payment.calculateNewOpen($(this));
						if (calculated) {
                            Payment.calculateAllocated();
                            Payment.calculatePaymentBalanceTotal();
						}
					});

				});
			}
		},


		calculateAllocated: function()
		{
			var updatePaymentBtn = $("#update_payment");
			var allocatedInvoiceAmount = Payment.calculateTotalAllocatedAmount();
			var allocatedPayment = Payment.calculatePaymentAllocatedAmount();
			var totalAllocated = allocatedPayment + allocatedInvoiceAmount;
			$("#allocated_amount").html(totalAllocated.toFixed(2));
			if (totalAllocated === 0) {
				updatePaymentBtn.prop('disabled', false);
			} else {
				updatePaymentBtn.prop('disabled', true);
			}
		},

		calculateNewOpen: function(el)
		{
			var paymentId = el.data('payment-id');
			var amountAllocated = parseFloat(el.val());
			var originalAmount = parseFloat($("#payment_amount_" + paymentId).val()) * -1;
			if (originalAmount <= amountAllocated) {
				var newOpen = originalAmount - amountAllocated;
				if (newOpen <= 0) {
					$("#balance_" + paymentId).val( newOpen.toFixed(2));
				} else {
					$("#balance_" + paymentId).val('');
					el.val('0.00');
				}
				return true;
			} else {
				$("#balance_" + paymentId).val('');
				el.val('0.00');
			}
			return false;
		},

		addPaymentLine: function(el)
		{
			$(".payment-line").remove();
			var paymentAmountUrl = el.data('payment-amount-url');
			$.get(paymentAmountUrl, {
				payment_amount: el.val()
			}, function(paymentAmountHtml){
				$("#supplier_sundry_invoices_payment>tbody").append(paymentAmountHtml);
				Payment.calculatePaymentBalance()
			});
		},

		calculatePaymentAmount: function(el)
		{
			var paymentId = el.data("payment-id");
			var allocatedAmount = parseFloat(el.val());
			var balance = $("#balance_" + paymentId);
			var payment = $("#payment_amount_" + paymentId);
			var paymentAmount = parseFloat(payment.val())
			var balanceAmount = 0;
			var openAmount = 0;
			if (allocatedAmount > paymentAmount) {
				balanceAmount = 0;
				payment.val(0);
			} else {
				balanceAmount = paymentAmount - allocatedAmount;
			}
			// if (balanceAmount > 0) {
			// 	balance.val( balanceAmount.toFixed(2) * -1);
			// }
			Payment.calculatePaymentBalanceTotal()
		},

		calculatePaymentBalanceTotal: function()
		{
			var totalOpen = 0;
			var openElements = $(".open-amount").filter(function(){
				return $(this).val() !== '';
			});
			openElements.map(function(){
				totalOpen += parseFloat($(this).val());
			});
			$("#total_new_open").html( totalOpen.toFixed(2) );
		},

		calculateInvoicePayment: function(el)
		{
			var invoiceId = el.data("invoice-id");
			var allocatedAmount = accounting.unformat(el.val());
			if (allocatedAmount > 0) {
				var openAmount = 0;
				var invoiceAmount = parseFloat($("#open_amount_" + invoiceId).val());
				console.log('invoiceAmount', invoiceAmount);
				console.log('allocatedAmount', allocatedAmount);
				console.log('Difference', (allocatedAmount > invoiceAmount))
				if (allocatedAmount > invoiceAmount) {
					openAmount = 0;
					el.val(invoiceAmount);
				} else {
					openAmount = parseFloat(invoiceAmount) - parseFloat(allocatedAmount);
				}
				$("#open_" + invoiceId).val( openAmount.toFixed(2) );
				Payment.calculatePaymentBalance()
				Payment.calculatePaymentBalanceTotal()
			}
		},

		calculateTotalPaymentAmount: function()
		{
			var paymentAmount = 0
			$(".payment-amount").each(function(){
				paymentAmount += parseFloat($(this).val());
			});
			return paymentAmount;
		},

		calculateTotalAllocatedAmount: function()
		{
			var allocationAmount = 0
			$(".allocate-payment").each(function(){
				if ($(this).val() !== '') {
					var amount = accounting.unformat($(this).val());
					allocationAmount += parseFloat(amount)
				}
			});
			return allocationAmount;
		},

		calculatePaymentAllocatedAmount: function()
		{
			var allocationAmount = 0
			$(".allocated").each(function(){
				if ($(this).val() !== '') {
					var amount = accounting.unformat($(this).val());
					allocationAmount += parseFloat(amount)
				}
			});
			return allocationAmount;
		},

		calculatePaymentBalance()
		{
			var paymentBalance = $("#balance");
			var paymentAmount = Payment.calculateTotalPaymentAmount();
			var balance = paymentBalance.val();
			var totalAllocation = Payment.calculateTotalAllocatedAmount();

			// if (totalAllocation > paymentAmount) {
			// 	Payment.clearAllocations();
			// } else {
				balance = paymentAmount - totalAllocation;
				$("#total_allocated").val(accounting.toFixed(paymentAmount, 2));

				var balanceAmount = balance;
				// paymentBalance.val( balanceAmount );
				balanceAmount  = balance * -1;
				$("#total_balance").html( accounting.formatMoney(balanceAmount) );
			//}

			// var paymentBtn = $("#make_payment");
			// if (balance == 0) {
			// 	paymentBtn.prop('disabled', false)
			// } else {
			// 	paymentBtn.prop('disabled', true)
			// }
		},

		clearAllocations()
		{
			$(".allocate-payment").each(function(){
				var invoiceId =  $(this).data('invoice-id');
				$(this).val('');
				$("#open_" + invoiceId).val('');
			});
			Payment.calculatePaymentBalance();
		},

		send: function(el, params)
		{
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
		},

		ajaxGet: function (el, params) {
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
        },

		addDiscount: function(el)
		{
			var invoiceId = el.val()
			var totalDiscountEl = $("#id_total_discount")
			var discountAmount = parseFloat($("#invoice_discount_" + invoiceId).val())
			var totalDiscount = 0
			if (totalDiscountEl.val() !== '') {
				totalDiscount = parseFloat(totalDiscountEl.val())
			}
			totalDiscount += discountAmount
			totalDiscountEl.val( totalDiscount.toFixed(2) )
			Payment.calculateNetPayment()
		},

		removeDiscount: function(el)
		{
			var invoiceId = el.val();
			var totalDiscountEl = $("#id_total_discount")
			var amount = $("#invoice_discount_" + invoiceId).val();
			if (amount !== '') {
                var discountAmount = parseFloat(amount);
                console.log('Discount amount ---', discountAmount)
				var totalDiscount  = 0;
                if (totalDiscountEl.val() !== '') {
                    totalDiscount = parseFloat(totalDiscountEl.val())
                }
                console.log('Initially total discount is ---', totalDiscount);
                totalDiscount -= discountAmount;
                console.log('Now removed ', discountAmount ,' total discount is ---', totalDiscount);
                totalDiscountEl.val( totalDiscount.toFixed(2) );
                Payment.calculateNetPayment()
            }
		},

		calculateNetPayment: function()
		{
			var selectedDiscount = $("#id_total_discount").val()
			var discountAllowed = $("#id_discount_disallowed").val()
			var totalDiscount = 0
			if (discountAllowed !== '') {
				totalDiscount += parseFloat(discountAllowed);
			}
			if (selectedDiscount !== '') {
				totalDiscount += parseFloat(selectedDiscount);
			}
			var total = $("#id_total_amount").val()
			var netAmount = parseFloat(total) - totalDiscount;
			$("#id_net_amount").val( netAmount.toFixed(2) )
		},

		allChecked: function(elClass, elId)
		{
			var countChecked = 0
			var countCheckboxes = 0
			elClass.each(function(){
				countCheckboxes++;
				if ($(this).is(":checked")) {
					countChecked++;
				}
			})
			//console.log(elClass, "chjecked --> ", countChecked, " count checkboxes --> ", countCheckboxes, elId)
			if (countCheckboxes > 0  && countChecked > 0) {
				if (countCheckboxes === countChecked) {
					//console.log("Make the element checked")
					elId.attr("checked", "checked")
                } else {
					elId.removeAttr("checked", "checked")
				}
			}
		}

	}
}(Payment || {}, jQuery));

