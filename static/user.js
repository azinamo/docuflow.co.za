$(document).ready(function() {
	//Form Submit for IE Browser
	$("#send_access_credentials").on('click', function(e){
		var password = $("#id_new_password_1").val();
		User.sendAccessCredentials($(this), password);
		e.preventDefault();
	});


});


var User = (function(user, $){

	return {

		sendAccessCredentials: function(el, password)
		{
			if (password) {
				var ajaxUrl = el.data('ajax-url');
				$.getJSON(ajaxUrl, {
					password: password
				}, function(response){
					responseNotification(response)
				});
			} else {
				alert('Please enter valid password')
			}
		}

	}
}(User || {}, jQuery));

