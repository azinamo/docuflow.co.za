$(document).ready(function() {
	//Form Submit for IE Browser
    $(".account-opening-balance").on('blur', function(e){
        var el = $(this);
        var amount = el.val();
        el.removeClass('text-danger')
        // if (isNaN(amount)) {
        //     el.focus().val('').addClass('text-danger')
        // } else {
        var amountStr = accounting.format(amount)
        el.val(amountStr)
        var accountId = el.attr('id').replace('opening_balance_', '')
        OpeningBalance.update(el, accountId, amount)
        //}
	});

    var copyAccountBalances = $("#copy_opening_balances");
    var text = copyAccountBalances.text()
        copyAccountBalances.on('click', function(e){
           var ajaxUrl = $(this).data('ajax-url');
           e.preventDefault();
           copyAccountBalances.html("Processing ...");
           $.getJSON(ajaxUrl, function(response){
               responseNotification(response)
               copyAccountBalances.html("<i class='fa fa-copy'></i> " + text);
           })
        });

});

var OpeningBalance = (function(openingBalance, $){

	return {

		update: function(el, accountId, amount)
		{
			var ajaxUrl = $(el).data('ajax-url');
			$.post(ajaxUrl, {
			    amount: amount
            }, function(responseJson, statusText, xhr, $form)  {
                responseNotification(responseJson);
                if (!responseJson.error) {
                    el.val(responseJson.amount);
                }
			}, 'json');
		}

	}
}(OpeningBalance || {}, jQuery))

