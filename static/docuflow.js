$( document ).ready(function() {
    if ( $(window).width() < 899 ){
        $( ".cross" ).hide();
        $( "#main_nav" ).hide();
        $( ".hamburger" ).click(function() {
            $( "#main_nav" ).slideToggle( "2000", function() {
                $( ".hamburger" ).hide();
                $( ".cross" ).show();
            });
        });
    } else{
        $( ".cross" ).hide();
        $( ".hamburger" ).hide();
        $( "#main_nav" ).show();
    }

    $( ".cross" ).click(function() {
        $( "#main_nav" ).slideToggle( "2000", function() {
        $( ".cross" ).hide();
        $( ".hamburger" ).show();
        });
    });

    //add materialize carousel
     document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.carousel');
        var instances = M.Carousel.init(elems, options);

        $('.carousel').carousel('fullWidth', true);
        $('.carousel').carousel('indicators', true);
        $('.carousel').carousel('dist', -80);

      });

        jQuery(window).resize(function(){
            if ( $(window).width() < 899 ){
                $( ".cross" ).hide();
                $( ".hamburger" ).show();
                $( "#main_nav" ).hide();
                $( ".hamburger" ).click(function() {
                    $( "#main_nav" ).css( "display", "block", function() {
                        $( ".hamburger" ).hide();
                        $( ".cross" ).show();
                    });
                });

            } else {
                $( ".cross" ).hide();
                $( ".hamburger" ).hide();
                $( "#main_nav" ).show();
            }
        });



   $('.carousel').carousel();



   // $("#submit").on('click', function(e){
   //      sendEnquiry();
   //      return false;
   // });

});

function sendEnquiry() {

   var name = $("#name");
   var surname = $("#surname");
   var company = $("#company");
   var email = $("#email");
   var phone_no = $("#phone_no");

   if (name.val() === '') {
      alert('Name is required')
   } else if (surname.val() === '') {
      alert('Surname is required')
   } else if (company.val() === '') {
      alert('Company name is required')
   } else if (email.val() === '') {
      alert('Email is required')
   } else if (phone_no.val() === '') {
      alert('Phone number is required')
   } else {
        $("#contact").ajaxSubmit({
            target: '#loader',   // target element(s) to be updated with server response
            beforeSubmit:  function(formData, jqForm, options) {
                var queryString = $.param(formData);
            },  // pre-submit callback
            error: function (data) {
                var element;
                // show error for each element
                if (data.responseJSON && 'errors' in data.responseJSON) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        element.addClass('is-invalid');
                        element.after('<div class="is-invalid-message">' + value[0] + '</div>');
                    });
                }

                // flash error message
                flash('danger', 'Errors have occurred.');
            },
            success:       function(responseJson, statusText, xhr, $form)  {
                var html = "<div class=\"alert alert-success\" role=\"alert\">\n" +
                    "  <p>"+ responseJson.text+ "</p>\n" +
                    "</div>"
                $("#contact").html(html);
            },  // post-submit callback
            dataType: 'json',
            type: 'post'
        });
   }
   return false;
}