$(document).ready(function() {
	//Form Submit for IE Browser

	var accountPostingLinesDiv = $("#account_posting_lines");
	if (accountPostingLinesDiv.length > 0) {
		AccountPosting.loadAccountPostingLines(accountPostingLinesDiv);
	}

	$("#save_account_posting").on('click', function(e){
		AccountPosting.save(this);
		e.preventDefault();
	});

	$("#update_account_posting").on('click', function(e){
		var form = $("#account_posting_detail");
		AccountPosting.update(this, form);
		e.preventDefault();
	});


	$(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			$("#save_account_posting_line_submit").on('click', function (event) {
                submitModalForm($("#account_posting_line_form"));
                return false
            });
			$("#edit_account_posting_line_submit").on('click', function (event) {
                submitModalForm($("form[name='edit_account_posting_line_form']"));
                return false
            });
			$(".chosen-select").chosen();
        });
    });
});

var AccountPosting = (function(accountPosting, $){

	return {

		save: function(form)
		{
			form = $("#approve_invoice_form") || form
			// disable extra form submits
			form.addClass('submitted');
			form.find(':submit').each(function () {
				$(this).css('width', $(this).outerWidth());
				$(this).attr('data-original-html', $(this).html());
				$(this).html('<i class="fa fa-spinner fa-spin"></i>');
			});

			// remove existing alert & invalid field info
			$('.alert-fixed').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-invalid-message').remove();
			form.ajaxSubmit({
				url: form.attr('action'),
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					var element;

					responseNotification(responseJson)

					// perform redirect

				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		update: function(el, form)
		{
			var ajaxUrl = $(el).data('ajax-url');
			form.ajaxSubmit({
				url: ajaxUrl,
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					responseNotification(responseJson)
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		},

		loadAccountPostingLines: function(el)
		{
			var ajaxUrl = el.data('ajax-url');
			var accountPostingId = el.data('invoice-id');
			$.get(ajaxUrl, {
				account_posting_id: accountPostingId,
				type: 'edit'
			}, function(html){
				el.html(html);
				$(".delete-account-posting").on('click', function(){
					if (confirm('Are you sure you want to delele this account posting')) {
						Invoice.deleteAccountPosting($(this));
					}
				});

				$(".delete_line").on('click', function(e){
					if (confirm('Are you sure you want to delete this line')) {
						AccountPosting.deleteAccountPostingLine($(this));
					}
					return false;
				});

			});
		},

		deleteAccountPostingLine: function(el) {
			var ajaxUrl = el.data('ajax-url');
			var accountPostingLineId = el.data('line-id');
			$.getJSON(ajaxUrl, {
				account_posting_line_id: accountPostingLineId
			}, function(response){
				AccountPosting.loadAccountPostingLines($("#account_posting_lines"))
			});
		}


	}
}(AccountPosting || {}, jQuery))

