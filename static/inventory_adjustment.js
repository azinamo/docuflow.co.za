$(document).ready(function() {

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');
			var form = $(data).find("form");
            var submitBtn = form.find("button[type='submit']");

			$("#id_new_quantity").on('blur', function(){
				InventoryAdjustment.calculateAdjustment($(this));
			});

            $("#submit_adjustment").on('click', function (event) {
            	var form = $("#stock_adjustment");
            	form.LoadingOverlay("show", {
            		'text': 'Processing ...'
				});
                ajaxSubmitForm($(this), form);
                return false
            });

            $("input[name='price_type']").on('change', function(){
            	InventoryAdjustment.adjustPrices($(this));
			});

            $("#own_price").on('blur', function(){
                InventoryAdjustment.adjustPrices($("#price_type_own_price"));
            });

            $("#submit_branch_movement").on('click', function(e){
            	var form = $("#move_branch_stock");
            	form.LoadingOverlay("show", {
            		'text': 'Processing ...'
				});
                ajaxSubmitForm($(this), form);
                return false
            });

			initChosenSelect();

        });
    });


	var inventoryItemsBody = $("#inventory_items_body");
	if (inventoryItemsBody.length > 0) {
		var ajaxUrl = inventoryItemsBody.data("ajax-url");
		InventoryAdjustment.loadInventoryItems(inventoryItemsBody, ajaxUrl);
	}


});


var InventoryAdjustment = (function(adjustment, $){

    return {
        loadInventoryItems: function(inventoryItemsBody, ajaxUrl)
		{
			$.get(ajaxUrl, function(html){
				inventoryItemsBody.html(html);
			})
		},

		adjustPrices: function(el)
		{
			console.log( el );
			var totalCost = 0;
			if (el.is(":checked")) {
			    var adjustment = parseFloat($("#id_adjustment").val());
			    var key = el.val();
			    console.log("calculate total price to adjust", el.val(), $("#" + key), key);
			    var selectedPrice =  $("#" + key);
                if (selectedPrice.length > 0 && selectedPrice.val() !== "") {
                    totalCost = parseFloat(selectedPrice.val()) * adjustment;
			        $("#id_price").val(selectedPrice.val());
                }
			}
			console.log(totalCost)
			$("#total_price").html(accounting.formatMoney(totalCost))
		},

		calculateAdjustment: function(el)
		{
		    console.log('Calculate adjustment');
			var onHand = $("#id_on_hand").val();
			var newQuantity = el.val();

			console.log('On hand -->', onHand, ' new quantity ', newQuantity);

			if (onHand !== "" && newQuantity !== "") {
				var adjustment = parseInt(newQuantity) - parseInt(onHand);
				console.log('Adjustment --> ', adjustment);
				if (adjustment > 0) {
				    $("#price_type_own_price").prop('checked', false);
				    $(".price-choice").show();
				    console.log('---Positive--- ');
                } else {
				    $(".price-choice").hide();
				    $(".price").hide();
				    $("#price_type_own_price").prop('checked', true);
				    console.log('---Negative--- ');
                }
				$("#id_adjustment").val( adjustment );
            }
		},

	}
}(InventoryAdjustment || {}, jQuery))

