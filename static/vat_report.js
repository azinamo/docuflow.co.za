$(function(){

	var saveVatReport = $("#save_vat_report");
	saveVatReport.on('click', function(e){
	    e.preventDefault();
        VatReport.saveReport($(this))
	});

});


var VatReport = (function(vat_report, $){

	return {

	    saveReport: function(el)
        {
            var ajaxUrl = el.data('ajax-url');
            console.log(ajaxUrl)
            el.html("Processing ...");
            $.getJSON(ajaxUrl, function(response){
               responseNotification(response)
               el.html("<i class='fa fa-file'></i> File");
            });
        }
	}

}(VatReport || {}, jQuery))