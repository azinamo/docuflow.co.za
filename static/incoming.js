$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

            $("#save_update").on('click', function (event) {
            	var formId = $("#create_incoming_invoice_update");
            	formId.LoadingOverlay("show", {
					text        : "Processing ...",
					progress    : true
				});
                event.preventDefault();
                submitModalForm($("form[name='create_incoming_invoice_update']"));
            });

            initDatePicker();

			initChosenSelect();

        });
    });

});


var IncomingUpdate = (function(incomingUpdate, $){

	return {

		createJournal: function(el) {
		   var self = $("#" + el.attr('id'));
		   var text = el.text();
		   self.html("Processing ...");
		   var url = el.data('ajax-url');
		   $.get(url, {
			 period: $("#period").val()
		   }, function(response){
		   	   if (response.error) {
		   	   	  self.html(text);
			   }
			   responseNotification(response);
		   }, 'json');
		},


	}
}(IncomingUpdate || {}, jQuery))

