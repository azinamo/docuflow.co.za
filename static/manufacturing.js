$(document).ready(function() {

    $(document).on("click", "[data-modal]", function (event) {
        event.preventDefault();
        $.get($(this).data("modal"), function (data) {
			initChosenSelect();
        });
    });

	$("#manufacturing>tr>td:not(.actions)").on("click", function(){
		var detailUrl = $(this).parent("tr").data("detail-url");
		document.location.href = detailUrl;
		return false;
	});


	$("#submit_manufacturing").on("click", function(e){
		var manufacturingForm = $("#manufacturing_form");
		manufacturingForm.LoadingOverlay('show', {'text': 'Processing ...'});

		ajaxSubmitForm($(this), $("#manufacturing_form"));
		return false;
	});

	var recipeItemsBody = $("#manufacturing_recipe_items");
	if (recipeItemsBody.length > 0) {
		Manufacturing.loadRecipeRow(recipeItemsBody);
	}

	var addRecipeLine = $("#add_inventory_recipe_line");
	addRecipeLine.on('click', function(){
		$(this).prop('disabled', true).data('html', addRecipeLine.text()).text('loading ...')
		Manufacturing.loadRecipeRow(addRecipeLine);
	});


	$(".remove-recipe-inventory-item").on("click", function (e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		Manufacturing.removeInventoryItem($(this));
    });
});

var Manufacturing = (function(manufacturing, $){

	return {

		removeInventoryItem: function(el)
		{
			var rowId = el.data("row-id");
			$("#row_" + rowId).remove();
		},

		loadRecipeRow: function(el)
		{
			var recipeRowClass = $(".recipe_item_row");
			var ajaxUrl = el.data('ajax-url');
			var container = $("#recipes__placeholder");
			var rowId = 0;
			var lastRow = recipeRowClass.last();
			if (lastRow.length > 0) {
				rowId = parseInt(lastRow.data('row-id')) + 1;
			}
			$.get(ajaxUrl, {
				row_id:rowId
			}, function(html) {
				if (lastRow.length > 0) {
					lastRow.after(html)
				} else {
					container.replaceWith(html);
				}
				recipeRowClass = $(".recipe_item_row");
				var _lastRow = Manufacturing.getLastRow(recipeRowClass, lastRow);

				recipeRowClass.each(function(){
					var itemRowId = $(this).data("row-id")
					Manufacturing.handleRow(itemRowId);
				});
				el.prop('disabled', false).text(el.data('html'));
			});
		},

		getLastRow: function(rowClass, lastRow)
		{
			var _lastRow = rowClass.last();
			if (_lastRow.length > 0) {
				return parseInt(_lastRow.data('row-id'));
			}
			return lastRow;
		},

		handleRow: function(rowId)
		{
			console.log("_----------------- handleRow --------------------");
			var chosenWidth = $("#"+ rowId + "_recipe_item_chosen").width();
			$("#"+ rowId +"_recipe_item").on('change', function(){
				Manufacturing.handleRecipe(rowId);
			});

			$("#quantity_"+ rowId +"").on('blur', function(){
				var selectedOption = $("#"+ rowId +"_recipe_item option:selected");
				Manufacturing.handleRecipe(rowId);
			});

			// $(".chosen-select").chosen();
			if (chosenWidth > 0) {
				$(".chosen-select").chosen({'width': chosenWidth + "px"});
			} else {
				$(".chosen-select").chosen({'width': '143px'});
			}
		},

		handleSelectedRecipe: function(el, rowId)
		{
			var selectedOption = $("#"+ rowId +"_recipe_item option:selected");
			var price = selectedOption.data('price');
			$("#price_" + rowId ).val( price );

			var recipteItemsUrl = selectedOption.data('ajax-recipe-inventory-url');
			Manufacturing.loadRecipeItems(el.val(), rowId, recipteItemsUrl);
		},

		loadRecipeItems: function(recipeId, rowId, recipeItemsUrl)
		{
			$.get(recipeItemsUrl, function(html){
				$("#recipe_details_" + rowId).html(html);
			});
		},

		handleRecipe: function(rowId)
		{
			var selectedOption = $("#"+ rowId +"_recipe_item option:selected");
			var recipeUrl = selectedOption.data('ajax-recipe-url');
			var costingUrl = selectedOption.data('ajax-cost-url');
			var recipeId = selectedOption.val();

			$.get(recipeUrl, function(recipeItems){
				var quantity = $("#quantity_"+ rowId +"").val();
				var params = Manufacturing.getBreakdown(quantity, recipeItems);
				params['quantity'] = quantity
				$.post(costingUrl, params, function(response){
					console.log(response);
				    if (response.error) {
						responseNotification(response);
					} else {
				    	Manufacturing.buildCostHtml(response, rowId, params);
					}
					//$("#recipe_details_" + rowId).html(responseHtml);
					// if (response.error) {
					// 	el.val('')
					// } else {
					// 	if (response.hasOwnProperty('unit_cost')) {
					// 		$("#unit_cost_" + rowId).html(response.unit_cost);
					// 	}
					// 	if (response.hasOwnProperty('total_cost')) {
					// 		$("#total_cost_" + rowId).html(accounting.format(response.total_cost));
					// 	}
					// }
				}, 'json');
			});
		},

		getBreakdown: function(quantity, recipeItems)
		{
			var recipeRecipeItems = {};
			recipeItems.map(function(recipeItem){
				var result = Manufacturing.convertQuantity(quantity, recipeItem.item_unit_code, recipeItem.stock_unit_code);
				recipeRecipeItems[recipeItem.id] = result;
			});
			return recipeRecipeItems;
		},

		validateInventoryQuantity: function(el, recipeItemId, rowId)
		{
			var ajaxUrl = el.data('ajax-url');
			var quantity = el.val();
			if (recipeItemId !== '' && quantity !== '') {
				var params = Manufacturing.convertAndCalculateQuantities(quantity, recipeItemId, rowId);

				params['recipe_id'] = recipeItemId;
				$.getJSON(ajaxUrl, params, function(response){
				    console.log(response);
				    responseNotification(response);
				    if (!response.error) {
						Manufacturing.buildCostHtml(response);
					}
					//$("#recipe_details_" + rowId).html(responseHtml);
					// if (response.error) {
					// 	el.val('')
					// } else {
					// 	if (response.hasOwnProperty('unit_cost')) {
					// 		$("#unit_cost_" + rowId).html(response.unit_cost);
					// 	}
					// 	if (response.hasOwnProperty('total_cost')) {
					// 		$("#total_cost_" + rowId).html(accounting.format(response.total_cost));
					// 	}
					// }
				});
			}
		},

        buildCostHtml: function(response, rowId, recipeItems)
        {
			console.log( "-------- buildCostHtml(" + rowId + ") --------- ");
			var html = [];
			html.push("<table class='table' id='recipe_inventory_items_"+ rowId +"'>");
			html.push("<thead>");
			html.push("<tr>");
				html.push("<th>Inventory</th>");
				html.push("<th class='text-right'>Quantity</th>");;
				html.push("<th>Unit</th>");
				html.push("<th>Control Unit</th>");
				html.push("<th class='text-right'>Quantity to be used</th>");
				html.push("<th class='text-right'>Unit Cost</th>");
				html.push("<th class='text-right'>Total Cost</th>");
			html.push("</tr>");
			html.push("</thead>");
			html.push("<tbody>");
			response['recipe_items'].map(function(recipeItem){
				html.push("<tr>")
					html.push("<td>" + recipeItem.inventory + " (" + recipeItem.received + ")</td>");
					html.push("<td class='text-right'>" + recipeItem.item_quantity + "</td>");
					html.push("<td>" + recipeItem.item_unit + "</td>");
					html.push("<td>" + recipeItem.sale_unit_code + "</td>");
					html.push("<td class='text-right'>" + recipeItem.quantity + "</td>");
					html.push("<td class='text-right'>" + recipeItem.unit_price + "</td>");
					html.push("<td class='text-right'>" + recipeItem.total_cost + "</td>");
				html.push("</tr>")
			});
			html.push("</tbody>");
			html.push("<tfoot>");
				html.push("<tr>");
					html.push("<th></th>");
					html.push("<th class='text-right'></th>");
					html.push("<th></th>");
					html.push("<th></th>");
					html.push("<th></th>");
					html.push("<th class='text-right'> " + response.unit_cost + "</th>");
					html.push("<th class='text-right'>" + accounting.formatMoney(response.total_cost) + "</th>");
				html.push("</tr>");
			html.push("</tfoot>");
			html.push("</table>");
			$.each(recipeItems, function(_recipeItemId, _recipeItem){
				if (_recipeItem.hasOwnProperty('rate')) {
					html.push("<input type='hidden' name='recipe_"+ _recipeItemId +"_inventory_item_"+ _recipeItemId +"_rate' value='"+_recipeItem.rate+"' />");
					html.push("<input type='hidden' name='recipe_"+ _recipeItemId +"_inventory_item_"+ _recipeItemId +"_quantity' value='"+_recipeItem.value+"' />");
				}
			});

			$("#recipe_details_" + rowId).html( html.join(' '));
        },

		convertAndCalculateQuantities: function(quantity, recipeId, rowId)
		{
			var manufactureQuantities = {};
			console.log("-------------------- convertAndCalculateQuantities --------------------" + quantity + " ---  " + recipeId + " ---- " + rowId)
			console.log("There are ", $(".recipe_qty_" + rowId + "_" + recipeId).length + " lines for recipes")
			$(".recipe_qty_" + rowId + "_" + recipeId).each(function(){
				var inventoryItemRowId =  $(this).data('row-id');
				var inventoryItemQty = parseFloat($(this).val());
				var totalInventoryQty = inventoryItemQty * quantity;
				var recipeInventoryItemUnit = $("#row_" + rowId + "_inventory_recipe_unit_" + inventoryItemRowId).val();
				var inventoryControlUnit = $("#row_" + rowId + "_inventory_recipe_sale_unit_" + inventoryItemRowId).val();

				var result = Manufacturing.convertQuantity(totalInventoryQty, recipeInventoryItemUnit, inventoryControlUnit);
				console.log("Result for ", rowId + " receipe id ", recipeId)
				console.log(result)
				console.log("Inventory item " + inventoryItemRowId + " requires " + inventoryItemQty + " = totalling to  " + totalInventoryQty + "(" + recipeInventoryItemUnit+ ") which is " + result['value'] + "(" + inventoryControlUnit + ")" );
				$("#row_" + rowId + "_quantity_used_" + inventoryItemRowId).html(accounting.toFixed(result['value'], 4) + " ");
				$("#row_" + rowId + "_inventory_recipe_qty_used_" + inventoryItemRowId).val(accounting.toFixed(result['value'], 4));
				$("#rate_" + rowId + "_" + inventoryItemRowId).val(accounting.toFixed(result['rate'], 4));
				manufactureQuantities["quantities_" + inventoryItemRowId] = result['value']
			});
			return manufactureQuantities;
		},

		convertQuantity: function(quantity, fromUnit, toUnit)
		{
			try {
				if (fromUnit !== toUnit) {
					var convert = require('convert-units');

					var rate = convert(1).from(fromUnit).to(toUnit);
					var result = convert(quantity).from(fromUnit).to(toUnit);
					console.log("\n");
					console.log( "Converted quantity " + quantity + " from " + fromUnit + " to " + toUnit + " to give " + result );
					console.log("\r\n\n");
					return {'rate': rate, 'value': result}
				} else {
					return {'rate': 1, 'value': quantity}
				}
			} catch (e) {
				return {'rate': 1, 'value': quantity}
			}
		}
	}
}(Manufacturing || {}, jQuery))

