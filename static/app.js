var docuflow = docuflow || {};

(function (ns) {

  ns.widget = {};

  ns.widget.invoiceTable = function invoiceTable(target) {
    var _table = target.parentNode.getElementsByClassName('dataTable')[0];
    window.addEventListener('load', function () {
      $(_table).DataTable({
        paging: false,
        search: false,
        info: false,
        searching: false,
        select: true
      });
    }, { passive: true });
  };

 ns.widget.setCookie = function setCookie(cookieName, cookieValue, expiryDays) {
    var d = new Date();
    d.setTime(d.getTime() + (expiryDays * 24 * 60 * 60 * 1000 ));
    var  expires = "expires=" + d.toUTCString();
    document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
  };

  ns.widget.getCookie = function setCookie(cookieName) {
      var name = cookieName + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' '){
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
      }
      return "";
  };

  ns.widget.launchLightbox = function launchLightbox(target, options) {
    //associate modal event on page
    options = {
      url: null,
      title:''
    };
    $('#createBooking').on('show.bs.modal', function (e) {
      //pull data from the event instigator
      options.url = e.relatedTarget.getAttribute('data-url');
      options.title = e.relatedTarget.getAttribute('data-modal-title');
      options.javascript = e.relatedTarget.getAttribute('data-javascript');

      //inject title
      e.currentTarget.querySelector('.modal-title').textContent = options.title;

      //inject content

      $.get(options.url, function (res) {

        e.currentTarget.querySelector('.modal-body .content').innerHTML = res;
        e.currentTarget.querySelector('.modal-body').classList.remove('ajaxLoading');

        if (options.javascript) {
            $.getScript(options.javascript, function() {

            });
        }
      });
    });
  };

  ns.widget.dataTable = function dataTable(targetImage) {

    var _dataTable, _actionBtn;

    window.addEventListener('load', function () {
      _dataTable = targetImage.parentNode.getElementsByClassName('dataTable')[0].children[1];
      _actionBtn = targetImage.parentNode.querySelector('[href="#additem"]');

      _actionBtn.addEventListener('click', function (e) {
        e.preventDefault();
        var _fieldList = e.target.parentNode.parentNode.getElementsByClassName('form-group');
        var _valueList = [];

        for (var i = 0; i < _fieldList.length; i++) {
          if (_fieldList[i].children[1].tagName === 'INPUT') {
            if (_fieldList[i].children[1].value !== '') {
              _valueList.push(_fieldList[i].children[1].value);
            }
          } else if (_fieldList[i].children[1].tagName === 'SELECT') {
            _valueList.push(_fieldList[i].children[1].selectedOptions[0].text);
          }
        }

        _valueList.push('<span class="btn btn-danger btn-xs material-icons" data-remove="true">close</span>');

        if ( (_fieldList.length + 1) === _valueList.length) {
          $(_dataTable).DataTable().row.add( _valueList ).draw();
        }
      }, { passive: false });

    }, { passive: true });

    ns.widget.dataTable.evaluateClick = function removeRow(e) {
      if (e.target.getAttribute('data-remove') !== null) {
        $(e.target.parentNode.parentNode.parentNode.parentNode).DataTable().row().remove(e.target.parentNode.parentNode._DT_RowIndex).draw();
      }
    };

    window.addEventListener('load', function () {
      $(_dataTable).DataTable({
        "paging":   false,
        "ordering": false,
        "info": false,
        "searching" : false
      });

    }, { passive: true });
  };

  ns.widget.ckeditor = function ckeditor(targetImage) {
    window.addEventListener('load', function () {
      CKEDITOR.replace(targetImage.parentNode.getElementsByClassName('ckeditor-container')[0], {
        customConfig:'/js/vendor/ckeditor/custom_config.js'
      });
    }, { passive: true });
  };


  ns.widget.initMap = function(mapId) {
      var elementId = document.getElementById( mapId + "_google_map" );
      var latElement = $("#" + mapId + "_address_latitude");
      var lngElement = $("#" + mapId + "_address_longitude");

      if ( elementId != undefined) {

          var zoomLevel = 6;
          if (latElement.val() && lngElement.val()) {
              var latLng = { lat: parseFloat(latElement.val()), lng: parseFloat(lngElement.val()) };
              zoomLevel = 15;
          } else {
              var latLng = { lat: -30.9214744, lng: 23.044 };
          }
          var map = new google.maps.Map(elementId, {
              zoom: zoomLevel,
              center: latLng,
              mapTypeControlOptions: {
                  mapTypeIds: []
              }, // here´s the array of controls
              disableDefaultUI: true, // a way to quickly hide all controls
              mapTypeControl: true,
              scaleControl: true,
              zoomControl: true,
              zoomControlOptions: {
                  style: google.maps.ZoomControlStyle.LARGE
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP
          });
          var marker = new google.maps.Marker({
              position: latLng,
              map: map,
              visible: true,
              shadow: null,
              zIndex: 999,
              draggable: true,
              animation: google.maps.Animation.DROP,
              title: 'Current Location!'
          });

          var input      = document.getElementById(mapId + "_address");

          marker.addListener("dragend", function(event) {
              var point = marker.getPosition();

              latElement.val( point.lat() );
              lngElement.val( point.lng() );
          });

          // Create the search box and link it to the UI element.

          if (input) {
              var options = { componentRestrictions: 'ZA' }

              var searchBox = new google.maps.places.SearchBox(input, options);
              map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

              //Bias the search box results towards current map's viewport
              map.addListener('bounds_changed', function(){
                  searchBox.setBounds(map.getBounds());
              });

              //Listen for the event fired when the user selects a prediction and retrieve more details for that place
              searchBox.addListener('places_changed', function() {
                  var places = searchBox.getPlaces();

                  if (places.length == 0) {
                      return;
                  }
                  //Clear out olf markers
                  marker.setMap(null);

                  //For each place get the name, icon and location
                  var bounds = new google.maps.LatLngBounds();
                  places.forEach(function(place) {
                      if (!place.geometry) {
                          //console.log('Returned place does not have a geometry');
                          return;
                      }
                      var icon = {
                          url: place.icon,
                          size: new google.maps.Size(71, 71),
                          origin: new google.maps.Point(0, 0),
                          anchor: new google.maps.Point(17, 34),
                          scaledSize: new google.maps.Size(25, 25)
                      };

                      //Create a marker for each place
                      var markerObject = new google.maps.Marker({
                          map: map,
                          animation: google.maps.Animation.DROP,
                          title: place.name,
                          position: place.geometry.location
                      });

                      var point = markerObject.getPosition();
                      latElement.val( point.lat() );
                      lngElement.val( point.lng() );

                      //markers.push(markerObject);

                      google.maps.event.trigger(markerObject, 'dragend', function(){
                          var point = markerObject.getPosition();
                          latElement.val( point.lat() );
                          lngElement.val( point.lng() );
                      });

                      console.log( place );
                      if (place.geometry.viewport) {
                          //Only geocodes have view port
                          bounds.union(place.geometry.viewport);
                      } else {
                          bounds.extend(place.geometry.location);
                      }
                  });
                  map.fitBounds(bounds);
              });
          }

          $("#" + mapId + "_google_map").css({'height' : '400px', 'width' : '100%'});
      }
  }

  /*
    takes two args
    type: the type of widget you want to initialise
    element - the image element calling the function
  */
  ns.widgetInit = function widgetInit(type,element) {
    ns.widget[type] && ns.widget[type](element);
  };


}(docuflow));