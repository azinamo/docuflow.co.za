$(document).ready(function() {

	window.Docuflow = window.Docuflow || {};
	var common = window.Docuflow.Common || {};
	var modal = window.Docuflow.Modal || {};
	var sales = window.Docuflow.Sales || {};
	var salesItem = window.Docuflow.SalesItem || {};

	$(".picked").on("blur", function(){
		PickingSlip.validateQuantity($(this));
	});

	$(".quantity").on("blur", function(e){
		PickingSlip.handleQty($(this));
	});

	$(".back-order").on("change", function(e){
		PickingSlip.calculateTotalForBackOrder();
	});

	$(".create-picking").on("change", function(e){
		PickingSlip.calculateTotalForBackOrder();
	});

	$("#save_picking_slip").on("click", function(){

		ajaxSubmitForm($(this), $("#picking_slip_form"));

		// $("#picking_slip_form").ajaxSubmit({
		// 	dataType: "json",
		// 	success: function(response){
		// 	    console.log(response);
		// 		responseNotification(response);
		// 	}
		// });
	});

	$("#save_delivery_note").on("click", function(){
		ajaxSubmitForm($(this), $("#delivery_note_form"));
	});

	$(".action-picking_slip").on('click', function (e) {
		console.log('Submit action ....');
		e.preventDefault();
		PickingSlip.actionPickingSlip($(this));
    });

	var pickingSlipItems = $(".sales-items");
	if (pickingSlipItems.length > 0) {
		pickingSlipItems.each(function(){
			var rowId = $(this).data('row-id');
			console.log("PICKING SLIP ROW IS --> ", rowId );
			if (rowId !== undefined) {
				salesItem.handleRow(rowId);
			}
		});
	}

	PickingSlip.calculateTotalQuantity();
	PickingSlip.calculateTotalNetWeight();
	PickingSlip.calculateTotalGrossWeight()

});


var PickingSlip = (function(pickingSlip, $){

	return {

        validateQuantity: function (el) {
            var rowId = el.data("row-id");
            var quantity = accounting.unformat($("#received_" + rowId).val());
            var picked = accounting.unformat(el.val());
            $("#quantity_" + rowId).val(picked);
			PickingSlip.validateQty(el, rowId, quantity, picked);
        },

		handleQty: function(el)
		{
			var rowId = el.data("row-id");
			var orderedQty = accounting.unformat($("#received_" + rowId).val());
			var availableQty = accounting.unformat($("#quantity_available_" + rowId).val());
			var quantity = accounting.unformat(el.val());
			console.log("Available quantity -->", availableQty);
			$("#quantity_" + rowId).val(quantity);
            var backOrderEl = $("#back_order_" + rowId);
            console.log("quantity received ", availableQty, " and picked is ", quantity);
            if (quantity > availableQty) {
                el.val(accounting.formatMoney(availableQty));
                backOrderEl.prop("disabled", true);
            } else if(quantity > orderedQty) {
            	el.val(accounting.formatMoney(availableQty));
                backOrderEl.prop("disabled", true);
			} else {
                if (quantity <= availableQty) {
                    backOrderEl.prop("checked", false).prop("disabled", false);
                }
            }
            quantity = accounting.unformat(el.val());
            var balance = orderedQty - quantity;
            if (balance > 0) {
				$("#balance_picking_" + rowId).html(accounting.formatMoney(balance));
				$("#picking_balance_" + rowId).val(accounting.toFixed(balance, 2));
				backOrderEl.prop("disabled", false);
			} else {
            	backOrderEl.prop("disabled", true);
			}

			PickingSlip.calculateTotalForPicking();
			PickingSlip.calculateTotalForBackOrder();
			PickingSlip.calculateTotalQuantity();
		},

		calculateTotalForPicking: function()
		{
			console.log("---------calculateTotalForPicking----------");
			var qty = 0;
			$(".quantity").each(function(){
				var rowId = $(this).data("row-id");
				var isPicking = $("#is_picked_" + rowId);

				var itemQty = $(this).val();

				console.log("Row -> ", rowId, ' is picked ', isPicking, 'is checked ', isPicking.is(":checked"), '--> quantity for picking -->', itemQty);
				if (isPicking.is(":checked") && itemQty !== "") {
					console.log("Add picking quantity --> ", itemQty);
					qty += parseFloat(itemQty)
				}
			});
			$("#total_send_for_picking").html(accounting.formatMoney(qty))
		},

		calculateTotalForBackOrder: function()
		{
			console.log("---------calculateTotalForBackOrder----------");
			var qty = 0;
			$(".quantity-balance").each(function(){
				var rowId = $(this).data("row-id");
				var isBackOrder = $("#back_order_" + rowId);

				var itemQty = $(this).val();

				console.log("Row -> ", rowId, ' is picked ', isBackOrder, 'is checked ', isBackOrder.is(":checked"), '--> balance -->', itemQty);

				if (isBackOrder.is(":checked") && itemQty !== "") {
					console.log("Add back order --> ", itemQty);
					qty += parseFloat(itemQty)
				}
			});
			$("#total_on_back_order").html(accounting.formatMoney(qty))
		},

		calculateTotalQuantity: function()
		{
			console.log("---------calculateTotalForBackOrder----------");
			var qty = 0;
			$(".quantity-ordered").each(function(){
				var itemQty = $(this).val();
				qty += parseFloat(itemQty)
			});
			$("#total_received").html(accounting.formatMoney(qty))
		},

		calculateTotalNetWeight: function()
		{
			console.log("---------calculateTotalForBackOrder----------");
			var qty = 0;
			$(".net-weight").each(function(){
				var itemQty = $(this).val();
				qty += parseFloat(itemQty)
			});
			$("#total_net_weight").html(accounting.formatMoney(qty))
		},

		calculateTotalGrossWeight: function()
		{
			console.log("---------calculateTotalForBackOrder----------");
			var qty = 0;
			$(".gross-weight").each(function(){
				var itemQty = $(this).val();
				qty += parseFloat(itemQty)
			});
			$("#total_gross_weight").html(accounting.formatMoney(qty))
		},

		actionPickingSlip: function(el)
		{
			console.log("Action picking slip")
			console.log(el)
			var ajaxUrl = el.data('ajax-url');
			console.log(ajaxUrl)
			$.getJSON(ajaxUrl, function(response){
				console.log(response)
				responseNotification(response)
			});
		},

    }

}(PickingSlip || {}, jQuery));

