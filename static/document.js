$(document).ready(function() {
	//Form Submit for IE Browser

    $(document).on('click', '[data-modal]', function (event) {
        event.preventDefault();
        $.get($(this).data('modal'), function (data) {
            $(data).modal('show');

			$("#add_invoice_comment_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='comment_invoice_form']"));
            });

			$("#add_invoice_account_comment_submit").on('click', function (event) {
                submitModalForm($("form[name='comment_invoice_account_form']"));
                return false;
            });

			$("#add_invoice_image_submit").on('click', function (event) {
                event.preventDefault();
                submitModalForm($("form[name='add_invoice_image_form']"));
            });
			var date = new Date();
			// datepicker on all date input with class datepicker
			$(".datepicker").datepicker({
				dateFormat: 'yy-mm-dd'
			});


			$(".history_datepicker").datepicker({
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "yy-mm-dd",
				maxDate         : -1,
				yearRange       : "1900:"+(date.getFullYear())
			});

			var minDate = new Date()
			var dataMinDate = $("#id_start_date").data('min-date');
			if (dataMinDate) {
				date = new Date(dataMinDate);
				minDate = new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01");
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : minDate,
					yearRange       : "-30:+100"
				});
			} else {
				$(".future_datepicker").datepicker({
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "yy-mm-dd",
					minDate         : +1,
					yearRange       : "-30:+100"
				});

			}
			$(".chosen-select").chosen();

        });
    });

	$("#save_certificate").on('click', function(e){
		var form = $("#create_certificate");
		ImportantDocument.saveDocument(this, form);
		e.preventDefault();
	});

	$("#save_invoice").on('click', function(e){
		e.preventDefault();
		Document.saveDocument()
	});

	$("#upload_document").on("click", function(e){
		e.preventDefault();
		Invoice.savePayment($(this))
	});

	if ($("#pdf_viewer").length > 0) {
		var pdf  = $(this).data('pdf-url');
		PDFObject.embed(pdf, "#pdf_viewer");
	}

});


var ImportantDocument = (function(importantDocument, $){

	return {

		send: function(el, params)
		{
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
		},

		ajaxGet: function (el, params) {
			var ajaxUrl = el.data('ajax-url');
			if (ajaxUrl) {
				$.getJSON(ajaxUrl, params, function(response){
					responseNotification(response)
				});
			}
        },

		saveInvoice: function(el)
		{
			var form = $("#create_invoice");
			submitModalForm(form)
		},

		saveDocument: function(el, form, notify)
		{
			var ajaxUrl = $(el).data('ajax-url');
			form.ajaxSubmit({
				url: ajaxUrl,
				target:        '#loader',   // target element(s) to be updated with server response
				beforeSubmit:  function(formData, jqForm, options) {
					var queryString = $.param(formData);
				},  // pre-submit callback
				error: function (data) {
					var element;
					// show error for each element
					if (data.responseJSON && 'errors' in data.responseJSON) {
						$.each(data.responseJSON.errors, function (key, value) {
							element.addClass('is-invalid');
							element.after('<div class="is-invalid-message">' + value[0] + '</div>');
						});
					}

					// flash error message
					flash('danger', 'Errors have occurred.');
				},
				success:       function(responseJson, statusText, xhr, $form)  {
					if (notify === undefined || notify === true) {
						responseNotification(responseJson)
                    }
				},  // post-submit callback
				dataType: 'json',
				type: 'post'
				// $.ajax options can be used here too, for example:
				//timeout:   3000
			});
		}

	}
}(Document || {}, jQuery))

