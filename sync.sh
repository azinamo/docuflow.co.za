cd /srv/docuflow
git add .
git commit -m 'Merge'
git checkout development
git pull origin development
source ../bin/activate
pip install -r requirements.txt
python manage.py migrate
supervisorctl restart dev_docuflow
deactivate