from collections import OrderedDict
from decimal import Decimal
from http import HTTPStatus

from django.urls import reverse

from docuflow.apps.company.models import VatCode
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.vat.services import generate_vat_reports, calculate_turnover_excl
from tests.base import AuthenticatedTestCase
from tests import factories


def serialize_vat_type(vat_type: VatCode, month_vat_type: JournalLine = None, is_output=False):
    total = 0
    if month_vat_type:
        if is_output:
            total = month_vat_type.credit * -1
        else:
            total = month_vat_type.debit
        total = Decimal(total).quantize(Decimal('0.01'))
    return {'id': vat_type.pk, 'label': vat_type, 'total': total}


def period_total(input_line: JournalLine = None, output: JournalLine = None, export: JournalLine = None):
    total = 0
    if input_line and input_line.debit:
        total += input_line.debit * -1
    if output and output.credit:
        total += output.credit
    if export and export.credit:
        total += export.credit
    return Decimal(total).quantize(Decimal('0.01'))


def calculate_excl(journal_lines):
    amount = 0
    for journal_line in journal_lines:
        if journal_line.vat_code and journal_line.vat_code.is_output:
            amount += calculate_turnover_excl(amount=journal_line.credit * -1, vat_code=journal_line.vat_code)
    return amount


def calculate_incl(journal_lines):
    amount = 0
    for journal_line in journal_lines:
        if journal_line.vat_code and journal_line.vat_code.is_output:
            amount += calculate_turnover_excl(amount=journal_line.credit * -1, vat_code=journal_line.vat_code) + Decimal(journal_line.credit * -1)
    return Decimal(str(amount)).quantize(Decimal('0.01'))


class TestVatReport(AuthenticatedTestCase):

    def test_generate_vat_report(self):
        vat_account = factories.AccountFactory(company=self.company, code='2410', name='Output 25%')
        input_vat_account = factories.AccountFactory(company=self.company, code='1470', name='Input 25%')
        export_vat_account = factories.AccountFactory(company=self.company, code='170', name='Export 25%')
        output_vat = factories.OutputVatCodeFactory(company=self.company, label='Output Vat', code=25, percentage=25,
                                                    account=vat_account)
        input_vat = factories.VatCodeFactory(
            company=self.company, label='Input Vat', code=25, percentage=25, account=input_vat_account
        )
        export_vat = factories.OutputVatCodeFactory(
            company=self.company, label='Export Vat', code=25, percentage=25, account=export_vat_account
        )

        jan_period = factories.PeriodFactory(period=1, period_year=self.year)
        jan_vat_report = factories.VatReportFactory(
            company=self.company, period=jan_period, created_by=self.profile, year=self.year,
            category_period=jan_period.period
        )
        jan_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jan_period, credit=1696174.50,
            accounting_date=jan_period.from_date, account=vat_account, debit=0, vat_code=output_vat,
            journal__journal_number=f'{jan_period.name}-OUTPUT-000{jan_period.period}', vat_report=jan_vat_report
        )
        jan_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jan_period, debit=709964.51,
            accounting_date=jan_period.to_date, account=input_vat_account, credit=0, vat_code=input_vat,
            journal__journal_number=f'{jan_period.name}-INPUT-000{jan_period.period}', vat_report=jan_vat_report
        )

        # February
        feb_period = factories.PeriodFactory(period=2, period_year=self.year)
        feb_vat_report = factories.VatReportFactory(
            company=self.company, period=feb_period, created_by=self.profile, year=self.year,
            category_period=feb_period.period
        )
        feb_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=feb_period, credit=3109728.85,
            accounting_date=feb_period.from_date, account=vat_account, debit=0, vat_code=output_vat,
            journal__journal_number=f'{feb_period.name}-OUTPUT-000{feb_period.period}', vat_report=feb_vat_report
        )
        feb_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=feb_period, debit=1535259.00,
            accounting_date=feb_period.to_date, account=input_vat_account, credit=0, vat_code=input_vat,
            journal__journal_number=f'{feb_period.name}-INPUT-000{feb_period.period}', vat_report=feb_vat_report
        )

        # March
        mar_period = factories.PeriodFactory(period=3, period_year=self.year)
        mar_vat_report = factories.VatReportFactory(
            company=self.company, period=mar_period, created_by=self.profile, year=self.year,
            category_period=mar_period.period
        )
        mar_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=mar_period, credit=2938421.00,
            accounting_date=mar_period.from_date, account=vat_account, debit=0, vat_code=output_vat,
            journal__journal_number=f'{mar_period.name}-OUTPUT-000{mar_period.period}', vat_report=mar_vat_report
        )
        mar_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=mar_period, debit=1212504.00,
            accounting_date=mar_period.to_date, account=input_vat_account, credit=0, vat_code=input_vat,
            journal__journal_number=f'{mar_period.name}-INPUT-000{mar_period.period}', vat_report=mar_vat_report
        )

        # April
        apr_period = factories.PeriodFactory(period=4, period_year=self.year)
        apr_vat_report = factories.VatReportFactory(
            company=self.company, period=apr_period, created_by=self.profile, year=self.year,
            category_period=apr_period.period
        )
        apr_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=apr_period, credit=1659710.00,
            debit=0, accounting_date=apr_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{apr_period.name}-OUTPUT-000{apr_period.period}', vat_report=apr_vat_report
        )
        apr_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=apr_period, debit=2966827.80,
            credit=0, accounting_date=apr_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{apr_period.name}-INPUT-000{apr_period.period}', vat_report=apr_vat_report
        )
        apr_export_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=apr_period, credit=3502.00, debit=0,
            accounting_date=apr_period.to_date, account=export_vat_account, vat_code=export_vat,
            journal__journal_number=f'{apr_period.name}-EXPORT-000{apr_period.period}', vat_report=apr_vat_report

        )

        # May
        may_period = factories.PeriodFactory(period=5, period_year=self.year)
        may_vat_report = factories.VatReportFactory(
            company=self.company, period=may_period, created_by=self.profile, year=self.year,
            category_period=may_period.period
        )
        may_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=may_period, credit=1815468.00,
            debit=0, accounting_date=may_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{may_period.name}-OUTPUT-000{may_period.period}', vat_report=may_vat_report
        )
        may_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=may_period, debit=473456.20,
            credit=0, accounting_date=may_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{may_period.name}-INPUT-000{may_period.period}', vat_report=may_vat_report
        )
        may_export_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=may_period, credit=3560.00,
            debit=0, accounting_date=may_period.to_date, account=export_vat_account, vat_code=export_vat,
            journal__journal_number=f'{may_period.name}-EXPORT-000{may_period.period}', vat_report=may_vat_report
        )

        # June
        jun_period = factories.PeriodFactory(period=6, period_year=self.year)
        jun_vat_report = factories.VatReportFactory(
            company=self.company, period=jun_period, created_by=self.profile, year=self.year,
            category_period=jun_period.period
        )
        jun_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jun_period, credit=1475982.95,
            debit=0, accounting_date=jun_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{jun_period.name}-OUTPUT-000{jun_period.period}', vat_report=jun_vat_report
        )
        jun_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jun_period, debit=1560558.00,
            credit=0, accounting_date=jun_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{jun_period.name}-INPUT-000{jun_period.period}', vat_report=jun_vat_report
        )

        # July
        jul_period = factories.PeriodFactory(period=7, period_year=self.year)
        jul_vat_report = factories.VatReportFactory(
            company=self.company, period=jul_period, created_by=self.profile, year=self.year,
            category_period=jul_period.period
        )
        jul_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jul_period, credit=1320663.85,
            debit=0, accounting_date=jul_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{jul_period.name}-OUTPUT-000{jul_period.period}', vat_report=jul_vat_report
        )
        jul_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jul_period, debit=495295.00,
            credit=0, accounting_date=jul_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{jul_period.name}-INPUT-000{jul_period.period}', vat_report=jul_vat_report
        )
        jul_export_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jul_period, credit=7142.00,
            debit=0, accounting_date=jul_period.to_date, account=export_vat_account, vat_code=export_vat,
            journal__journal_number=f'{jul_period.name}-EXPORT-000{jul_period.period}', vat_report=jul_vat_report
        )

        vat_types = VatCode.objects.filter(company=self.company, percentage__gt=0).order_by('id').all()
        vat_report = generate_vat_reports(company=self.company, vat_types=vat_types, year=self.year)

        jan = OrderedDict()
        jan[output_vat] = serialize_vat_type(vat_type=output_vat, month_vat_type=jan_output_journal, is_output=True)
        jan[input_vat] = serialize_vat_type(vat_type=input_vat, month_vat_type=jan_input_journal, is_output=False)
        jan[export_vat] = serialize_vat_type(vat_type=export_vat, month_vat_type=None, is_output=True)

        feb = OrderedDict()
        feb[output_vat] = serialize_vat_type(vat_type=output_vat, month_vat_type=feb_output_journal, is_output=True)
        feb[input_vat] = serialize_vat_type(vat_type=input_vat, month_vat_type=feb_input_journal, is_output=False)
        feb[export_vat] = serialize_vat_type(vat_type=export_vat, month_vat_type=None, is_output=True)

        mar = OrderedDict()
        mar[output_vat] = serialize_vat_type(vat_type=output_vat, month_vat_type=mar_output_journal, is_output=True)
        mar[input_vat] = serialize_vat_type(vat_type=input_vat, month_vat_type=mar_input_journal, is_output=False)
        mar[export_vat] = serialize_vat_type(vat_type=export_vat, month_vat_type=None, is_output=True)

        apr = OrderedDict()
        apr[output_vat] = serialize_vat_type(vat_type=output_vat, month_vat_type=apr_output_journal, is_output=True)
        apr[input_vat] = serialize_vat_type(vat_type=input_vat, month_vat_type=apr_input_journal, is_output=False)
        apr[export_vat] = serialize_vat_type(vat_type=export_vat, month_vat_type=apr_export_journal, is_output=True)

        may = OrderedDict()
        may[output_vat] = serialize_vat_type(vat_type=output_vat, month_vat_type=may_output_journal, is_output=True)
        may[input_vat] = serialize_vat_type(vat_type=input_vat, month_vat_type=may_input_journal, is_output=False)
        may[export_vat] = serialize_vat_type(vat_type=export_vat, month_vat_type=may_export_journal, is_output=True)

        jun = OrderedDict()
        jun[output_vat] = serialize_vat_type(vat_type=output_vat, month_vat_type=jun_output_journal, is_output=True)
        jun[input_vat] = serialize_vat_type(vat_type=input_vat, month_vat_type=jun_input_journal, is_output=False)
        jun[export_vat] = serialize_vat_type(vat_type=export_vat, month_vat_type=None, is_output=True)

        jul = OrderedDict()
        jul[output_vat] = serialize_vat_type(vat_type=output_vat, month_vat_type=jul_output_journal, is_output=True)
        jul[input_vat] = serialize_vat_type(vat_type=input_vat, month_vat_type=jul_input_journal, is_output=False)
        jul[export_vat] = serialize_vat_type(vat_type=export_vat, month_vat_type=jul_export_journal, is_output=True)

        jan_total = period_total(input_line=jan_input_journal, output=jan_output_journal, export=None),
        feb_total = period_total(input_line=feb_input_journal, output=feb_output_journal, export=None)

        reports = {
            jan_vat_report: {
                'report': jan_vat_report,
                'total': jan_total,
                'turnover_excl': calculate_excl([jan_input_journal, jan_output_journal]),
                'turnover_incl': calculate_incl([jan_input_journal, jan_output_journal]),
                'vat_types': jan
            },
            feb_vat_report: {
                'report': feb_vat_report,
                'total': feb_total,
                'turnover_excl': calculate_excl([feb_input_journal, feb_output_journal]),
                'turnover_incl': calculate_incl([feb_input_journal, feb_output_journal]),
                'vat_types': feb
            },
            mar_vat_report: {
                'report': mar_vat_report,
                'total': period_total(input_line=mar_input_journal, output=mar_output_journal, export=None),
                'turnover_excl': calculate_excl([mar_input_journal, mar_output_journal]),
                'turnover_incl': calculate_incl([mar_input_journal, mar_output_journal]),
                'vat_types': mar
            },
            apr_vat_report: {
                'report': apr_vat_report,
                'total': period_total(input_line=apr_input_journal, output=apr_output_journal, export=apr_export_journal),
                'turnover_excl': calculate_excl([apr_input_journal, apr_output_journal]),
                'turnover_incl': calculate_incl([apr_input_journal, apr_output_journal]),
                'vat_types': apr
            },
            may_vat_report: {
                'report': may_vat_report,
                'total': period_total(input_line=may_input_journal, output=may_output_journal, export=may_export_journal),
                'turnover_excl': calculate_excl([may_input_journal, may_output_journal]),
                'turnover_incl': calculate_incl([may_input_journal, may_output_journal]),
                'vat_types': may
            },
            jun_vat_report: {
                'report': jun_vat_report,
                'total': period_total(input_line=jun_input_journal, output=jun_output_journal, export=None),
                'turnover_excl': calculate_excl([jun_input_journal, jun_output_journal]),
                'turnover_incl': calculate_incl([jun_input_journal, jun_output_journal]),
                'vat_types': jun
            },
            jul_vat_report: {
                'report': jul_vat_report,
                'total': period_total(input_line=jul_input_journal, output=jul_output_journal, export=jul_export_journal),
                'turnover_excl': calculate_excl([jul_input_journal, jul_output_journal]),
                'turnover_incl': calculate_incl([jul_input_journal, jul_output_journal]),
                'vat_types': jul
            }
        }
        assert vat_report == reports
