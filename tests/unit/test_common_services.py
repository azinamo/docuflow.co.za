from decimal import Decimal

from django.test import TestCase

from docuflow.apps.common.services import get_credit_amount, get_debit_amount


def test_get_credit_amount():
    amount = 3000

    assert get_credit_amount(credit=amount, debit=0) == amount

    amount = -350
    assert get_credit_amount(credit=amount, debit=0) == 0


def test_get_debit_amount():
    amount = 3000

    assert get_debit_amount(credit=0, debit=amount) == amount

    amount = -350
    assert get_debit_amount(debit=amount, credit=0) == 0
