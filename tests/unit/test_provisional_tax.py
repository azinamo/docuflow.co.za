from decimal import Decimal

from django.test import TestCase

from docuflow.apps.provisionaltax.services import calculate_annualized_amount

from tests import utils


class ProvisionalTaxEstimateTestCase(TestCase):

    def test_calculate_annualized_amount(self):
        annualized_amount = calculate_annualized_amount(Decimal(1767800.63), 6)
        assert annualized_amount == utils.to_decimal('3535601.26')

        annualized_amount = calculate_annualized_amount(Decimal(3219172.94), 9)
        assert annualized_amount == utils.to_decimal('4292230.59')
