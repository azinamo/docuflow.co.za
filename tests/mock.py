from unittest import mock


# Accounts report import
account_import_data_response = mock.MagicMock(return_value={
    0: {
        1: '1020',
        2: 'Bank, PlusGiro',
        3: 'Balance Sheet',
        4: 'Cash Resources',
        5: '',
        6: '',
        7: 'Cash',
        8: 'Yes',
    },
    1: {
        1: '1025',
        2: 'Kundfordringer 6033',
        3: 'Balance Sheet',
        4: 'Current Assets',
        5: 'Customer',
        6: '',
        7: 'Cash Received',
        8: '',
    },
    2: {
        1: '2110',
        2: 'Leverantoss',
        3: 'Balance Sheet',
        4: 'Current Liabilities',
        5: 'Supplier',
        6: '',
        7: 'Cash Paid',
        8: '',
    },
    3: {
        1: '3081',
        2: 'Kassrabatter',
        3: 'Income Statement',
        4: 'Other Income',
        5: '',
        6: '',
        7: 'Cash Received',
        8: 'Yes',
    },
    4: {
        1: '5283',
        2: 'Foraldraens',
        3: 'Income Statement',
        4: 'Employment costs',
        5: '',
        6: '',
        7: 'Cash Paid',
        8: '',
    }
})


# Year Budget import response
year_budget_import_data_response = mock.MagicMock(return_value=[
    {
        0: '5010',
        1: 'Lön kollektivanställda / Employee Payroll',
        2: 22.612,
        3: 21.174,
        4: 23.235,
        5: 26.659,
        6: 37.748,
        7: 38.635,
        8: -29.108,
        9: 37.418,
        10: 45.711,
        11: 45.567,
        12: 53.004,
        13: 65.129,
        14: 387.784
    },
    {
        0: '5081',
        1: 'Lön över tid / Overtime',
        2: 6.498,
        3: 0.0,
        4: 7.322,
        5: 4.695,
        6: 13.376,
        7: 6.748,
        8: 0.0,
        9: 2.714,
        10: 0.0,
        11: 7.162,
        12: 10.822,
        13: 7.639,
        14: 66.976
    },
    {
        0: '5220',
        1: 'Löner tjänstemän / Mantis Managers',
        2: 40.489,
        3: 41.753,
        4: 41.366,
        5: 41.366,
        6: 41.366,
        7: 41.366,
        8: 41.366,
        9: 31.722,
        10: 41.366,
        11: 41.366,
        12: 41.366,
        13: 50.866,
        14: 495.76
    },
    {
        0: '6600',
        1: 'Reparation och underhåll / Repairs & Maint.',
        2: 277.0,
        3: 1.356,
        4: 14.164,
        5: 22.775,
        6: 14.212,
        7: 6.062,
        8: 12.358,
        9: -4.993,
        10: 6.352,
        11: '',
        12: 23.529,
        13: 24.327,
        14: 121.771
    },
])


mails_response = mock.MagicMock(return_value={
    0: {
        'Subject': '#41055',
        'From': 'docs@docuflow.co.za'
    }
})

mail_message = mock.MagicMock(return_value={
    'Subject': '#41055',
    'From': 'docs@docuflow.co.za'
})


mail_part_response = mock.MagicMock(return_value={
    0: {
        'filename': 'document.pdf',
        'data': 'XXXddd',
        'content_type': 'pdf',
        'checksum': '61e5dd7b76bc180be444823fbfbab3fd'
    },
    1: {
        'filename': 'document2.pdf',
        'data': 'doc2',
        'content_type': 'pdf',
        'checksum': '64e5dd7b76bc180be444823fbfbab3fd'
    }
})

save_file_response = mock.MagicMock(return_value={
    'filename': 'document.pdf',
    'file_path': 'XXXddd',
    'content_type': 'pdf',
    'checksum': '61e5dd7b76bc180be444823fbfbab3fd'
})

rename_file_response = mock.MagicMock(return_value=None)

fixed_assets_import_data = mock.MagicMock(return_value=(
    {
        0: {
            1: '',
            2: '',
            3: '',
            4: 'Assets',
            5: '',
            6: '',
            7: '',
            8: '',
            9: 'Opening Balances',
            10: '',
            11: 'Branch',
            12: 'Projekt',
            13: '',
            14: 'KST',
            15: '',
            16: '',
            17: '',
            18: ''
        },
        1: {
           1: 'Name',
           2: 'Category',
           3: 'Cost Price',
           4: 'Date Purchased',
           5: 'Own asset number',
           6: 'Supplier',
           7: 'Location',
           8: 'Qty',
           9: 'Opening Cost',
           10: 'Opening Acc. Depr',
           11: '',
           12: '',
           13: '',
           14: '',
           15: '',
           16: '',
           17: '',
           18: ''
        }
    },
    {
        4: {
            1: "GESAB Nussbaum 2,40 2-pelslyft",
            2: "Maskiner & Inventerier",
            3: 39250,
            4: "2011-05-01",
            5: "2",
            6: '',
            7: '',
            8: '',
            9: 39250,
            10: 39250,
            11: "Norrt\\u00e4lje",
            12: "701",
            13: 1,
            14: "100",
            15: 0.65,
            16: '',
            17: '',
            18: ''
        },
        5: {
            1: '',
            2: '',
            3: '',
            4: '',
            5: '',
            6: '',
            7: '',
            8: '',
            9: '',
            10: '',
            11: '',
            12: '',
            13: '',
            14: "7100",
            15: 0.15,
            16: '',
            17: '',
            18: ''
        },
        6: {
            1: '',
            2: '',
            3: '',
            4: '',
            5: '',
            6: '',
            7: '',
            8: '',
            9: '',
            10: '',
            11: '',
            12: '',
            13: '',
            14: "9800",
            15: 0.15,
            16: '',
            17: '',
            18: ''
        },
        7: {
            1: '',
            2: '',
            3: '',
            4: '',
            5: '',
            6: '',
            7: '',
            8: '',
            9: '',
            10: '',
            11: '',
            12: '',
            13: '',
            14: "4714",
            15: 0.05,
            16: '',
            17: '',
            18: ''
        },
    }
))
