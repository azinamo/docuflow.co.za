from datetime import date
from http import HTTPStatus
from unittest import mock

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

from docuflow.apps.budget.models import Budget, BudgetAccount, BudgetAccountPeriod
from docuflow.apps.budget.utils import ImportBudgetJob
from docuflow.apps.period.enums import BudgetType
from tests import factories
from tests.base import AuthenticatedTestCase
from tests.mock import year_budget_import_data_response


class BudgetViewTestCase(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.list_url = reverse('budget_index')

    def test_can_list_budgets(self):
        budgets = factories.BudgetFactory.create_batch(5, year=self.year)

        response = self.client.get(self.list_url)

        assert response.status_code == HTTPStatus.OK
        content = response.content.decode()
        for budget in budgets:
            assert str(budget.year) in content

    def test_create_year_budget(self):
        data = {
            'budget_type': BudgetType.WHOLE_COMPANY.value
        }

        response = self.ajax_post(reverse('create_year_budget', kwargs={'year_id': self.year.id}), data=data)

        assert response.status_code == HTTPStatus.OK
        budgets = Budget.objects.all()

        assert len(budgets) == 1
        assert budgets[0].year == self.year

    def test_delete_budget(self):
        pass

    @mock.patch('docuflow.apps.budget.utils.excell_upload_client.get_upload_data', year_budget_import_data_response)
    def test_import_year_budget_data_from_excell(self):
        periods = []
        for p in range(1, 13):
            from_date = date(month=p, year=self.year.year, day=1)
            to_date = date(month=p, year=self.year.year, day=20)
            periods.append(
                factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                        from_date=from_date, period_year=self.year, company=self.company)
            )

        file = SimpleUploadedFile('accounts_data.xlsx', b'any content')

        # Account to update
        account_1 = factories.AccountFactory(name='Account 1', code=5010, company=self.company)
        account_2 = factories.AccountFactory(name='Account 2', code=5081, company=self.company)
        account_3 = factories.AccountFactory(name='Account 3', code=5220, company=self.company)
        account_4 = factories.AccountFactory(name='Account 4', code=6600, company=self.company)

        import_budget = ImportBudgetJob(company=self.company, year=self.year, file=file)
        import_budget.execute()

        assert Budget.objects.count() == 1
        assert BudgetAccount.objects.count() == 4
        assert BudgetAccountPeriod.objects.count() == 48


