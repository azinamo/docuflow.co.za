from http import HTTPStatus
from unittest import mock

from django.core.management import call_command
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

from docuflow.apps.company.models import Account
from docuflow.apps.company.enums import AccountType, SubLedgerModule
from docuflow.apps.company.modules.account.utils import ImportAccountJob

from tests.base import AuthenticatedTestCase
from tests import factories
from tests.mock import account_import_data_response


def serialize_account_data(account: Account):
    return {
        'code': account.code,
        'name': account.name,
        'account_type': str(account.account_type),
        'report': str(account.report),
        'sub_ledger': '' if account.sub_ledger is None else str(account.sub_ledger),
        'vat_reporting': '' if account.vat_reporting is None else str(account.vat_reporting),
        'cash_flow': '' if account.cash_flow is None else str(account.cash_flow),
        'not_deductable': '' if account.not_deductable is False else 'Yes'
    }


class CompanyAccountTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(CompanyAccountTestCase, self).setUp()
        call_command('loaddata', 'fixtures/cash_flow_category.json', verbosity=1)

    def test_view_company_accounts(self):
        response = self.client.get(reverse('account_list'))

        assert response.status_code == HTTPStatus.OK

    def test_can_create_company_account(self):
        report_type = factories.ReportTypeFactory()
        cash_flow = factories.CashFlowFactory()

        data = {
            'name': 'Account Name',
            'code': '2412',
            'account_type': AccountType.INCOME_STATEMENT.value,
            'report': report_type.pk,
            'vat_reporting': '',
            'not_deductable': False,
            'sub_ledger': SubLedgerModule.SUPPLIER.value,
            'cash_flow': cash_flow.pk,
            'is_active': True
        }
        account_count = Account.objects.count()

        response = self.client.post(reverse('create_account'), data=data, follow=True)
        assert response.status_code == HTTPStatus.OK
        accounts = Account.objects.all().order_by('pk') # Overrider default ordering

        accounts_count = len(accounts)

        assert accounts_count == account_count + 1
        #
        account = accounts[accounts_count-1]

        assert data['name'] == account.name
        assert data['code'] == account.code
        assert data['account_type'] == account.account_type.value
        assert data['report'] == account.report.pk
        assert data['not_deductable'] == account.not_deductable
        assert data['sub_ledger'] == account.sub_ledger.value
        assert data['cash_flow'] == account.cash_flow.pk
        assert data['is_active'] == account.is_active

    def test_cannot_create_company_with_same_account_code(self):
        report_type = factories.ReportTypeFactory()
        cash_flow = factories.CashFlowFactory()
        factories.AccountFactory(code='2412', name='Bosc', company=self.company)

        data = {
            'name': 'Account Name',
            'code': '2412',
            'account_type': AccountType.INCOME_STATEMENT.value,
            'report': report_type.pk,
            'vat_reporting': '',
            'not_deductable': False,
            'sub_ledger': SubLedgerModule.SUPPLIER.value,
            'cash_flow': cash_flow.pk,
            'is_active': False
        }
        account_count = Account.objects.count()

        response = self.client.post(reverse('create_account'), data=data, follow=True)

        assert response.status_code == HTTPStatus.OK
        assert Account.objects.count() == account_count
        assert 'Account code already exists' in response.content.decode()

    def test_can_update_company_account(self):
        report_type = factories.ReportTypeFactory()
        cash_flow = factories.CashFlowFactory()
        account = factories.AccountFactory(code='2402', name='Bosc', company=self.company)

        data = {
            'name': 'Account Name',
            'code': '2412',
            'account_type': AccountType.INCOME_STATEMENT.value,
            'report': report_type.pk,
            'vat_reporting': '',
            'not_deductable': False,
            'sub_ledger': SubLedgerModule.SUPPLIER.value,
            'cash_flow': cash_flow.pk,
            'is_active': True
        }
        account_count = Account.objects.count()

        response = self.client.post(reverse('edit_account', args=(account.pk, )), data=data, follow=True)
        assert response.status_code == HTTPStatus.OK
        assert Account.objects.count() == account_count
        #
        account.refresh_from_db()

        assert data['name'] == account.name
        assert data['code'] == account.code
        assert data['account_type'] == account.account_type.value
        assert data['report'] == account.report.pk
        assert data['not_deductable'] == account.not_deductable
        assert data['sub_ledger'] == account.sub_ledger.value
        assert data['cash_flow'] == account.cash_flow.pk
        assert data['is_active'] == account.is_active

    def test_can_update_company_account_without_changing_code(self):
        report_type = factories.ReportTypeFactory()
        cash_flow = factories.CashFlowFactory()
        account = factories.AccountFactory(code='2412', name='Bosc', company=self.company)

        data = {
            'name': 'Account Name',
            'code': '2412',
            'account_type': AccountType.INCOME_STATEMENT.value,
            'report': report_type.pk,
            'vat_reporting': '',
            'not_deductable': False,
            'sub_ledger': SubLedgerModule.SUPPLIER.value,
            'cash_flow': cash_flow.pk,
            'is_active': True
        }
        account_count = Account.objects.count()

        response = self.client.post(reverse('edit_account', args=(account.pk, )), data=data, follow=True)
        assert response.status_code == HTTPStatus.OK
        assert Account.objects.count() == account_count
        #
        account.refresh_from_db()

        assert data['name'] == account.name
        assert data['code'] == account.code
        assert data['account_type'] == account.account_type.value
        assert data['report'] == account.report.pk
        assert data['not_deductable'] == account.not_deductable
        assert data['sub_ledger'] == account.sub_ledger.value
        assert data['cash_flow'] == account.cash_flow.pk
        assert data['is_active'] == account.is_active

    def test_can_delete_account(self):
        account = factories.AccountFactory(code='2412', name='Bosc', company=self.company)

        account_count = Account.objects.count()

        response = self.client.post(reverse('delete_account', args=(account.pk, )), follow=True)
        assert response.status_code == HTTPStatus.OK
        assert Account.objects.count() == account_count - 1
        # We soft deleting accounts
        assert Account.all_objects.count() == account_count

    def test_autocomplete_search_account(self):
        factories.AccountFactory(code='2412', name='Bosc', company=self.company)
        factories.AccountFactory(code='2401', name='Jos', company=self.company)
        factories.AccountFactory(code='1431', name='Mkaro', company=self.company)

        # Search full name
        response = self.ajax_get(reverse('autocomplete_accounts_search'), data={'term': 'Jos'})
        assert response.status_code == HTTPStatus.OK
        data = response.data
        assert len(data) == 1
        assert data[0]['code'] == '2401'
        assert data[0]['name'] == 'Jos'

        # Search partial name
        response = self.ajax_get(reverse('autocomplete_accounts_search'), data={'term': 'Mk'})
        assert response.status_code == HTTPStatus.OK
        data = response.data
        assert len(data) == 1
        assert data[0]['code'] == '1431'
        assert data[0]['name'] == 'Mkaro'

        # Search non-existent name
        response = self.ajax_get(reverse('autocomplete_accounts_search'), data={'term': 'Kobra'})
        assert response.status_code == 200
        data = response.data
        assert len(data) == 0

    @mock.patch('docuflow.apps.company.modules.account.utils.excell_upload_client.get_upload_data', account_import_data_response)
    def test_import_accounts_data_from_excell(self):
        file = SimpleUploadedFile('accounts_data.xlsx', b'any content')

        # Account to update
        updatable_account = factories.AccountFactory(name='To Be updated', code=5283)

        factories.ReportTypeFactory(name='Cost of Sales', slug='cost-of-sales')
        factories.ReportTypeFactory(name='Other Income', slug='other-income')
        factories.ReportTypeFactory(name='Employment costs', slug='employment-costs')
        factories.ReportTypeFactory(name='Fixed costs', slug='fixed-costs')
        factories.ReportTypeFactory(name='Semi Fixed costs', slug='semi-fixed-costs')
        factories.ReportTypeFactory(name='Other costs', slug='other-costs')
        factories.ReportTypeFactory(name='Depreciation costs', slug='depreciation-costs')
        factories.ReportTypeFactory(name='Financial Income', slug='financial-income')
        factories.ReportTypeFactory(name='Financial costs', slug='financial-costs')
        factories.ReportTypeFactory(name='Tax costs', slug='tax-costs')
        factories.ReportTypeFactory(name='Truck Income', slug='truck-income')
        factories.ReportTypeFactory(name='Other Manufacturing Costs', slug='other-manufacturing-costs')
        factories.ReportTypeFactory(name='Cash Resources', slug='cash-resources')
        factories.ReportTypeFactory(name='Current Liabilities', slug='current-liabilities')
        factories.ReportTypeFactory(name='Current Assets', slug='current-asset')

        import_accounts = ImportAccountJob(company=self.company, file=file)
        import_accounts.execute()

        # All existing accounts not in the list will be deactivated
        assert Account.objects.count() == 5

        data = account_import_data_response.return_value

        account = Account.objects.get(code=data[0][1])
        account_data = serialize_account_data(account=account)
        assert data[0][2] == account_data['name']
        assert data[0][3] == account_data['account_type']
        assert data[0][4] == account_data['report']
        assert data[0][5] == account_data['sub_ledger']
        assert data[0][6] == account_data['vat_reporting']
        assert data[0][7] == account_data['cash_flow']
        assert data[0][8] == account_data['not_deductable']

        updatable_account.refresh_from_db()
        account_data = serialize_account_data(account=updatable_account)
        assert data[4][1] == account_data['code']
        assert data[4][2] == account_data['name']
        assert data[4][3] == account_data['account_type']
        assert data[4][4] == account_data['report']
        assert data[4][5] == account_data['sub_ledger']
        assert data[4][6] == account_data['vat_reporting']
        assert data[4][7] == account_data['cash_flow']
        assert data[4][8] == account_data['not_deductable']
