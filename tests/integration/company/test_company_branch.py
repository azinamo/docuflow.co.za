from django.urls import reverse

from docuflow.apps.company.models import Branch

from tests.base import AuthenticatedTestCase
from tests import factories


def serialize_account_data(branch: Branch):
    return {
        'label': branch.label,
        'address_line_1': branch.address_line_1,
        'address_line_2': branch.address_line_2,
        'town': branch.town,
        'area_code': branch.area_code,
        'is_head_office': branch.is_head_office,
        'is_invoicable': branch.is_invoicable
    }


class CompanyBranchTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(CompanyBranchTestCase, self).setUp()

    def test_view_company_branches(self):
        response = self.client.get(reverse('branch_index'))

        assert response.status_code == 200

    def test_can_create_company_branch(self):
        data = {
            'label': 'Long Branch',
            'address_line_1': '99 Prime',
            'address_line_2': 'Lost Road',
            'town': 'Burma',
            'area_code': '9019',
            'is_head_office': False,
            'is_invoicable': False
        }
        branch_count = Branch.objects.count()

        response = self.client.post(reverse('create_branch'), data=data, follow=True)
        assert response.status_code == 200
        assert Branch.objects.count() == branch_count + 1
        #
        branch = Branch.objects.last()

        assert data['label'] == branch.label
        assert data['address_line_1'] == branch.address_line_1
        assert data['address_line_2'] == branch.address_line_2
        assert data['town'] == branch.town
        assert data['is_head_office'] == branch.is_head_office
        assert data['is_invoicable'] == branch.is_invoicable

    def test_create_head_office_branch(self):

        data = {
            'label': 'Long Branch',
            'address_line_1': '99 Prime',
            'address_line_2': 'Lost Road',
            'town': 'Burma',
            'area_code': '9019',
            'is_head_office': True,
            'is_invoicable': False
        }

        response = self.client.post(reverse('create_branch'), data=data, follow=True)

        assert response.status_code == 200
        branch = Branch.objects.last()
        assert data['label'] == branch.label
        assert data['address_line_1'] == branch.address_line_1
        assert data['address_line_2'] == branch.address_line_2
        assert data['town'] == branch.town
        assert data['is_head_office'] == branch.is_head_office
        assert data['is_invoicable'] == branch.is_invoicable

    def test_can_update_company_branch(self):
        branch = factories.BranchFactory(label='2402', town='Bosc', company=self.company)

        data = {
            'label': 'Long Branch',
            'address_line_1': '99 Prime',
            'address_line_2': 'Lost Road',
            'town': 'Burma',
            'area_code': '9019',
            'is_head_office': True,
            'is_invoicable': False
        }
        branch_count = Branch.objects.count()

        response = self.client.post(reverse('edit_branch', args=(branch.pk, )), data=data, follow=True)
        assert response.status_code == 200
        assert Branch.objects.count() == branch_count
        #
        branch.refresh_from_db()

        assert data['label'] == branch.label
        assert data['address_line_1'] == branch.address_line_1
        assert data['address_line_2'] == branch.address_line_2
        assert data['town'] == branch.town
        assert data['is_head_office'] == branch.is_head_office
        assert data['is_invoicable'] == branch.is_invoicable

    def test_can_delete_branch(self):
        branch = factories.BranchFactory(label='2412', town='Bosc', company=self.company)

        branch_count = Branch.objects.count()

        response = self.client.get(reverse('delete_branch', args=(branch.pk, )), follow=True)
        assert response.status_code == 200

        assert Branch.objects.count() == (branch_count - 1)
        # We soft deleting accounts
        assert Branch.all_objects.count() == branch_count
