from django.test import TestCase, Client
from django.urls import reverse
from django.core import mail

from docuflow.apps.company.enums import CompanyType
from docuflow.apps.company.models import Company
from tests.base import AuthenticatedTestCase
from tests import factories


class CompanyViewTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(CompanyViewTestCase, self).setUp()

    def test_can_manage_company(self):
        data = {
            'name': self.company.name,
            'address_line_1': 'Loth',
            'vat_number': '1244',
            'email_address': 'ane@gmail.com'
        }

        response = self.client.post(reverse('edit_company', kwargs={'pk': self.company.id}), data=data, follow=True)

        self.assertEqual(response.status_code, 200)
        # self.company.refresh_from_db()
        # assert self.company.address_line_1 == data['address_line_1']
        # assert self.company.vat_number == data['vat_number']
        # assert self.company.email_address == data['email_address']


class CompanyRegistrationTestCase(TestCase):

    def setUp(self) -> None:
        self.client = Client()
        self.list_url = reverse('my-companies')

    def test_can_register_company(self):
        plan = factories.SubscriptionFactory()
        self.registration_url = reverse('plan_signup', kwargs={'slug': plan.slug})
        data = {'name': 'Cavcon',
                'type': CompanyType.PRIVATE_COMPANY.value,
                'area_code': '21111',
                'address_line_1': '42 Broad Road',
                'address_line_2': 'Wynberg',
                'province': 'Western Cape',
                'vat_number': '11000000',
                'company_number': '1235555',
                'last_name': 'Last',
                'first_name': 'First',
                'email': 'info@gmail.com',
                'password1': 'password',
                'password2': 'password',
                'start_date': '2010-01-10'
                }
        assert Company.objects.count() == 0
        assert len(mail.outbox) == 0

        response = self.client.post(self.registration_url, data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        assert response.status_code == 200
        assert Company.objects.count() == 1
        assert len(mail.outbox) == 1
