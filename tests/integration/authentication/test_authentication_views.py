from django.urls import reverse
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes

from unittest import mock

from docuflow.apps.authentication.services import account_activation_token
from tests.base import BaseTestCase, AuthenticatedTestCase
from tests import factories


class TestAuthenticationView(BaseTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_can_view_login_page(self):
        company = factories.CompanyFactory()
        response = self.client.get(reverse('company_login', kwargs={'slug': company.slug}))
        assert response.status_code == 200
        content = response.content.decode()
        assert str(company) in content

    def test_ajax_login_view(self):
        company = factories.CompanyFactory()
        profile = factories.ProfileFactory(company=company, user=factories.UserFactory())
        data = {'username': profile.user.username, 'password': 'PASSWORD'}
        response = self.ajax_post(reverse('user_ajax_login', kwargs={'slug': company.slug}), data=data)
        assert response.status_code == 200

    def test_can_view_user_password_request(self):
        company = factories.CompanyFactory()
        profile = factories.ProfileFactory(company=company, user=factories.UserFactory())
        response = self.client.get(reverse('user_password_request', kwargs={'slug': company.slug}))
        assert response.status_code == 200

    def test_can_view_docuflow_login(self):
        response = self.client.get(reverse('docuflow_login'))
        assert response.status_code == 200

    def test_can_view_signup(self):
        response = self.client.get(reverse('signup'), follow=True)
        assert response.status_code == 200

    def test_can_view_account_activation_sent(self):
        response = self.client.get(reverse('account_activation_sent'))
        assert response.status_code == 200

    @mock.patch('django.contrib.auth.login', mock.MagicMock(return_value={}))
    def test_can_view_activation_page(self):
        profile = factories.ProfileFactory(company=factories.CompanyFactory(), user=factories.UserFactory())
        uid = urlsafe_base64_encode(force_bytes(str(profile.id), strings_only=True))
        token = account_activation_token.make_token(profile.user)

        response = self.client.get(reverse('activate', kwargs={'token': token, 'uidb64': uid}), follow=True)
        assert response.status_code == 200

    def test_can_view_user_login(self):
        response = self.client.get(reverse('user_login'))
        assert response.status_code == 200

    def test_can_redirect_to_invoices(self):
        response = self.client.get(reverse('company_redirect_invoices'), follow=True)
        assert response.status_code == 200


class AuthenticatedTestAuthenticationView(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_can_view_subscription_plan_detail(self):
        subscription = factories.SubscriptionFactory()
        response = self.client.get(reverse('subscription_plan_detail', kwargs={'slug': subscription.slug}))
        assert response.status_code == 200

    def test_company_redirect_login(self):
        response = self.client.get(reverse('company_redirect_login'), follow=True)
        assert response.status_code == 200

