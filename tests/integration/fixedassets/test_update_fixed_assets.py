from decimal import Decimal
from unittest import mock

from docuflow.apps.fixedasset.enums import FixedAssetStatus
from docuflow.apps.fixedasset.models import FixedAsset, Addition, OpeningBalance, Location, DepreciationSplit, Category
from docuflow.apps.fixedasset.utils import ImportFixedAssetJob
from tests.base import AuthenticatedTestCase
from tests.mock import fixed_assets_import_data
from tests import factories
from tests import utils


class UpdateFixedAssetsTest(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(UpdateFixedAssetsTest, self).setUp()

    def test_update_fixed_assets_due_for_depreciation(self):
        category = factories.FixedAssetCategoryFactory(name='Expenses on someone else\'s property')
