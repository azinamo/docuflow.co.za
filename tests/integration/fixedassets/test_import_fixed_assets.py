from decimal import Decimal
from unittest import mock

from docuflow.apps.fixedasset.enums import FixedAssetStatus
from docuflow.apps.fixedasset.models import FixedAsset, Addition, OpeningBalance, Location, DepreciationSplit, Category
from docuflow.apps.fixedasset.utils import ImportFixedAssetJob
from tests.base import AuthenticatedTestCase
from tests.mock import fixed_assets_import_data
from tests import factories
from tests import utils


class ImportFixedAssetsTest(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(ImportFixedAssetsTest, self).setUp()

    @mock.patch('docuflow.apps.fixedasset.client.fixed_asset_upload_client.get_upload_data', fixed_assets_import_data)
    def test_import_fixed_assets(self):
        factories.FixedAssetCategoryFactory(name='Maskiner & Inventerier', company=self.company)

        factories.BranchFactory(label='Norrt\\u00e4lje', company=self.company)

        kst = factories.CompanyObjectFactory(label='KST', company=self.company)
        projekt = factories.CompanyObjectFactory(label='Projekt', company=self.company)
        factories.ObjectItemFactory(code='701', company_object=projekt)
        factories.ObjectItemFactory(code='100', company_object=kst)
        factories.ObjectItemFactory(code='7100', company_object=kst)
        factories.ObjectItemFactory(code='9800', company_object=kst)
        factories.ObjectItemFactory(code='4714', company_object=kst)

        assert 0 == FixedAsset.objects.count()

        import_fixed_asset = ImportFixedAssetJob(company=self.company, year=self.year, file='file.xlsx')
        import_fixed_asset.execute()

        fixed_assets = FixedAsset.objects.all()
        assert len(fixed_assets) == 1

        fixed_assets_additions = Addition.objects.all()
        assert len(fixed_assets_additions) == 1

        fixed_assets_balances = OpeningBalance.objects.all()
        assert len(fixed_assets_balances) == 1

        fixed_assets_locations = Location.objects.all()
        assert len(fixed_assets_locations) == 1

        fixed_assets_splits = DepreciationSplit.objects.all()
        assert len(fixed_assets_splits) == 5
