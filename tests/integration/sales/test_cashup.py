from http import HTTPStatus

from django.urls import reverse
from django.utils import timezone

from docuflow.apps.sales.models import CashUp
from docuflow.apps.customer.models import AgeAnalysis
from docuflow.apps.journals.models import JournalLine
from tests.base import AuthenticatedTestCase
from tests import factories
from tests import utils


class TestCashUpView(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.list_url = reverse('cash_up_index')

    def test_can_view_cash_up_page(self):
        response = self.client.get(self.list_url)

        assert response.status_code == HTTPStatus.OK

    def test_create_cash_up_with_unallocated_payment(self):
        month = timezone.now().month
        tax_invoice_credit_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(name='Petty Cash', code='P00701', company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, period=month)
        customer = factories.CustomerFactory(company=self.company, name='African Brick Centre PE')
        payment_method = factories.PaymentMethodFactory(company=self.company, is_depositable=False, account=account)

        amount = 5116.84
        payment = factories.ReceiptFactory(amount=amount * -1, customer=customer, reference='Payment for EST00722',
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.ReceiptPaymentFactory(receipt=payment, payment_method=payment_method, amount=amount)

        invoice = factories.SalesTaxInvoiceFactory(total_amount=amount, customer=customer, vat_amount=667.42,
                                                   created_by=self.profile, period=period, branch=self.branch)
        receipt = factories.ReceiptFactory(amount=0, vat_amount=0, customer=customer, reference=str(invoice),
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.InvoiceReceiptFactory(amount=amount, receipt=receipt, invoice=invoice)
        factories.SundryReceiptFactory(parent=receipt, receipt=payment, amount=amount)

        data = {
            'date': period.to_date,
            'period': period.pk
        }

        assert CashUp.objects.count() == 0

        response = self.ajax_post(reverse('complete_cash_up'), data=data)

        assert response.status_code == HTTPStatus.OK
        assert response.data['text'] == 'Cash up saved successfully'

        cash_ups = CashUp.objects.all()
        assert len(cash_ups) == 1
        cash_up = cash_ups[0]

        payment.refresh_from_db()
        receipt.refresh_from_db()

        assert payment.cash_up == cash_up
        assert receipt.cash_up == cash_up

        age_analysis = AgeAnalysis.objects.first()
        assert age_analysis.current == utils.to_decimal(value=str(amount * -1))
        assert age_analysis.balance == utils.to_decimal(value=str(amount * -1))

        # Journal ledger
        journal_lines = JournalLine.objects.all()
        assert len(journal_lines) == 2
        customer_control_journal_line = JournalLine.objects.get(account=self.customer_account)
        payment_account = JournalLine.objects.get(account=account)
        assert customer_control_journal_line.credit == utils.to_decimal(value=str(amount))
        assert payment_account.debit == utils.to_decimal(value=str(amount))

    def test_create_cash_up_with_credit_note(self):
        month = timezone.now().month
        tax_invoice_credit_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(name='Petty Cash', code='P00701', company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, period=month)
        customer = factories.CustomerFactory(company=self.company, name='African Brick Centre PE')
        payment_method = factories.PaymentMethodFactory(company=self.company, is_depositable=False, account=account)

        amount = 5116.84
        payment = factories.ReceiptFactory(amount=amount * -1, customer=customer, reference='Payment for EST00722',
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.ReceiptPaymentFactory(receipt=payment, payment_method=payment_method, amount=amount)

        invoice = factories.SalesTaxInvoiceFactory(total_amount=amount, customer=customer, vat_amount=667.42,
                                                   created_by=self.profile, period=period, branch=self.branch)
        receipt = factories.ReceiptFactory(amount=0, vat_amount=0, customer=customer, reference=str(invoice),
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.InvoiceReceiptFactory(amount=amount, receipt=receipt, invoice=invoice)
        factories.SundryReceiptFactory(parent=receipt, receipt=payment, amount=amount)

        data = {
            'date': period.to_date,
            'period': period.pk
        }

        assert CashUp.objects.count() == 0

        response = self.ajax_post(reverse('complete_cash_up'), data=data)

        assert response.status_code == HTTPStatus.OK
        assert response.data['text'] == 'Cash up saved successfully'

        cash_ups = CashUp.objects.all()
        assert len(cash_ups) == 1
        cash_up = cash_ups[0]

        payment.refresh_from_db()
        receipt.refresh_from_db()

        # Age
        age_analysis = AgeAnalysis.objects.first()
        assert age_analysis.current == utils.to_decimal(value=str(amount * -1))
        assert age_analysis.balance == utils.to_decimal(value=str(amount * -1))

        assert payment.cash_up == cash_up
        assert receipt.cash_up == cash_up

        # Journal ledger
        journal_lines = JournalLine.objects.all()
        assert len(journal_lines) == 2
        customer_control_journal_line = JournalLine.objects.get(account=self.customer_account)
        payment_account = JournalLine.objects.get(account=account)
        assert customer_control_journal_line.credit == utils.to_decimal(value=str(amount))
        assert payment_account.debit == utils.to_decimal(value=str(amount))

    def test_can_create_cash_up_for_unallocated_payment(self):
        month = timezone.now().month
        tax_invoice_credit_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(name='Petty Cash', code='P00701', company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, period=month)
        customer = factories.CustomerFactory(company=self.company, name='African Brick Centre PE')
        payment_method = factories.PaymentMethodFactory(company=self.company, is_depositable=False, account=account)

        amount = 5116.84
        payment = factories.ReceiptFactory(amount=amount * -1, customer=customer, reference='Payment for EST00722',
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.ReceiptPaymentFactory(receipt=payment, payment_method=payment_method, amount=amount)

        invoice = factories.SalesTaxInvoiceFactory(total_amount=amount, customer=customer, vat_amount=667.42,
                                                   created_by=self.profile, period=period, branch=self.branch)
        receipt = factories.ReceiptFactory(amount=0, vat_amount=0, customer=customer, reference=str(invoice),
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.InvoiceReceiptFactory(amount=amount, receipt=receipt, invoice=invoice)
        factories.SundryReceiptFactory(parent=receipt, receipt=payment, amount=amount)

        data = {
            'date': period.to_date,
            'period': period.pk
        }

        assert CashUp.objects.count() == 0

        response = self.ajax_post(reverse('complete_cash_up'), data=data)

        assert response.status_code == 200
        assert response.data['text'] == 'Cash up saved successfully'

        cash_ups = CashUp.objects.all()
        assert len(cash_ups) == 1
        cash_up = cash_ups[0]

        payment.refresh_from_db()
        receipt.refresh_from_db()

        assert payment.cash_up == cash_up
        assert receipt.cash_up == cash_up

        # assert age_analysis.balance == 0
        # assert age_analysis.current == 0
        # Customer ledger
        # customer_ledger = CustomerLedger.objects.first()
        # assert CustomerLedger.objects.count() == 1
        # assert customer_ledger.object_id == invoice.id
        # assert customer_ledger.amount == amount
        # assert customer_ledger.status == LedgerStatus.PENDING

        # Journal ledger
        journal_lines = JournalLine.objects.all()
        assert len(journal_lines) == 2
        customer_control_journal_line = JournalLine.objects.get(account=self.customer_account)
        payment_account = JournalLine.objects.get(account=account)
        assert customer_control_journal_line.credit == utils.to_decimal(value=str(amount))
        assert payment_account.debit == utils.to_decimal(value=str(amount))

    def test_can_create_cash_up_with_depositable_payment_method(self):
        month = timezone.now().month
        tax_invoice_credit_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(name='Petty Cash', code='P00701', company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, period=month)
        customer = factories.CustomerFactory(company=self.company, name='African Brick Centre PE')
        payment_method = factories.PaymentMethodFactory(company=self.company, is_depositable=False, account=account)

        amount = 5116.84
        payment = factories.ReceiptFactory(amount=amount * -1, customer=customer, reference='Payment for EST00722',
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.ReceiptPaymentFactory(receipt=payment, payment_method=payment_method, amount=amount)

        invoice = factories.SalesTaxInvoiceFactory(total_amount=amount, customer=customer, vat_amount=667.42,
                                                   created_by=self.profile, period=period, branch=self.branch)
        receipt = factories.ReceiptFactory(amount=0, vat_amount=0, customer=customer, reference=str(invoice),
                                           created_by=self.profile, period=period, branch=self.branch)
        factories.InvoiceReceiptFactory(amount=amount, receipt=receipt, invoice=invoice)
        factories.SundryReceiptFactory(parent=receipt, receipt=payment, amount=amount)

        data = {
            'date': period.to_date,
            'period': period.pk
        }

        assert CashUp.objects.count() == 0

        response = self.ajax_post(reverse('complete_cash_up'), data=data)

        age_analysis = AgeAnalysis.objects.first()

        assert age_analysis.current == utils.to_decimal(value=str(amount * -1))
        assert age_analysis.balance == utils.to_decimal(value=str(amount * -1))

        assert response.status_code == HTTPStatus.OK
        assert response.data['text'] == 'Cash up saved successfully'

        cash_ups = CashUp.objects.all()
        assert len(cash_ups) == 1
        cash_up = cash_ups[0]

        payment.refresh_from_db()
        receipt.refresh_from_db()
        age_analysis.refresh_from_db()

        assert payment.cash_up == cash_up
        assert receipt.cash_up == cash_up

        # Journal ledger
        journal_lines = JournalLine.objects.all()
        assert len(journal_lines) == 2
        customer_control_journal_line = JournalLine.objects.get(account=self.customer_account)
        payment_account = JournalLine.objects.get(account=account)
        assert customer_control_journal_line.credit == utils.to_decimal(value=str(amount))
        assert payment_account.debit == utils.to_decimal(value=str(amount))
