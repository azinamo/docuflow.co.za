from unittest import mock

from docuflow.apps.inventory.factories import BranchInventoryFactory

invoice_lines = mock.MagicMock(return_value=[
    {
        # 'type_1': 'i',
        # 'inventory_1': inventory_1.id,
        # 'quantity_1': 3.00,
        # 'invoiced_1': 3,
        # 'unit_1': unit.id,
        # 'price_1': 3.15,
        # 'price_including_1': 3.6225,
        # 'total_price_excluding_1': 9.45,
        # 'sub_total_1': 9.45,
        # 'total_price_including_1': 10.87,
        # 'total_discount_1': 0,
        # 'vat_code_1': vat_code.id,
        # 'vat_percentage_1': vat_code.percentage,
        # 'total_vat_1': 1.42,
        # 'vat_amount_1': 1.42,
    }
])