from datetime import date

from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse

from docuflow.apps.sales.models import Receipt, InvoiceReceipt
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.customer.models import Ledger as CustomerLedger, AgeAnalysis
from tests.base import AuthenticatedTestCase
from tests import factories
from tests import utils


class TestInvoiceReceipts(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_can_view_receipt(self):
        response = self.ajax_get(reverse('receipts_index'))

        assert response.status_code == 200

    def test_view_receipt_list(self):
        period = factories.PeriodFactory(period_year=self.year, period=1)
        period2 = factories.PeriodFactory(period_year=self.year, period=2)
        period3 = factories.PeriodFactory(period_year=self.year, period=3)
        factories.ReceiptFactory(
            created_by=self.profile, branch=self.branch, period=period,
            customer=factories.CustomerFactory(company=self.company, number='0001')
        )
        factories.ReceiptFactory(
            created_by=self.profile, branch=self.branch, period=period2,
            customer=factories.CustomerFactory(company=self.company, number='0002')
        )
        factories.ReceiptFactory(
            created_by=self.profile, branch=self.branch, period=period3,
            customer=factories.CustomerFactory(company=self.company, number='0003')
        )

        response = self.api_call(reverse('sales-receipts-list'))

        assert response.status_code == 200
        assert response.data['count'] == 3

    def test_can_create_invoice_receipt(self):
        customer = factories.CustomerFactory(company=self.company, name='Buco', number='0009')
        account = factories.AccountFactory(company=self.company, name='Account 3', code='0003')
        payment_method = factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year)

        data = {
            'amount': 2000,
            'payment_method': payment_method.pk,
            'date': period.from_date,
            'customer': customer.pk,
            'reference': 'Month End Payment'
        }

        response = self.ajax_post(reverse('create_receipt'), data=data)

        assert response.status_code == 200
        receipts = Receipt.objects.all()
        assert receipts[0].customer.pk == customer.pk
        assert receipts[0].period.pk == period.pk
        assert receipts[0].payments.count() == 1

        supplier_ledger = CustomerLedger.objects.first()
        assert supplier_ledger.object_id == receipts[0].id
        assert supplier_ledger.credit == receipts[0].total_amount
        assert supplier_ledger.status == LedgerStatus.PENDING.value

        age_analysis = AgeAnalysis.objects.filter(period=period, customer=customer)
        assert age_analysis.count() == 0

    def test_can_pay_sales_invoices_with_ledger_balance(self):
        customer = factories.CustomerFactory(company=self.company, number='0009', name='Buco')
        payment_amount = 120
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        account = factories.AccountFactory(company=self.company)
        payment_account = factories.AccountFactory(name='Nedbank - Current', code='8400/000', company=self.company)
        payment_method = factories.PaymentMethodFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=factories.AccountFactory(company=self.company))
        ledger = factories.CustomerLedgerFactory(
            balance=3000, customer=customer, period=period, created_by=self.profile,
            journal_line=factories.JournalLineFactory(debit=3000, credit=0, account=account)
        )
        invoice_1 = factories.SalesTaxInvoiceFactory(period=period, total_amount=22304.26, customer=customer,
                                                     branch=self.branch)
        invoice_2 = factories.SalesTaxInvoiceFactory(period=period, total_amount=1760.79, customer=customer,
                                                     branch=self.branch)
        invoice_3 = factories.SalesTaxInvoiceFactory(period=period, total_amount=16082.16, customer=customer,
                                                     branch=self.branch)
        invoice_4 = factories.SalesTaxInvoiceFactory(period=period, total_amount=19078.16, customer=customer,
                                                     branch=self.branch)
        invoice_5 = factories.SalesTaxInvoiceFactory(period=period, total_amount=14187.09, customer=customer,
                                                     branch=self.branch)
        data = {
            'date': from_date,
            'amount': payment_amount,
            'customer': customer.pk,
            'payment_method': payment_method.pk,
            'reference': 'Reference',
            f'discount_{invoice_1.pk}': 1338.26,
            f'amount_{invoice_1.pk}': 22304.26,
            f'open_amount_{invoice_1.pk}': 22304.26,
            f'discount_{invoice_2.pk}': 105.65,
            f'amount_{invoice_2.pk}': 1760.79,
            f'open_amount_{invoice_2.pk}': 1760.79,
            f'discount_{invoice_3.pk}': 964.93,
            f'amount_{invoice_3.pk}': 16082.16,
            f'open_amount_{invoice_3.pk}': 16082.16,
            f'discount_{invoice_4.pk}': 1144.69,
            f'amount_{invoice_4.pk}': 19078.16,
            f'open_amount_{invoice_4.pk}': 19078.16,
            f'discount_{invoice_5.pk}': 851.23,
            f'amount_{invoice_5.pk}': 14187.09,
            f'pay_{invoice_5.pk}': invoice_5.pk,
            f'open_amount_{invoice_5.pk}': 0.00,
            f'is_discount_{ledger.pk}': 0.00,
            f'amount_{ledger.pk}': -3000.00,
            f'pay_ledger_{ledger.pk}': ledger.pk,
            f'open_amount_{ledger.pk}': 0.00,
            f'account_1': payment_account.pk,
            f'allow_negative_stock_1': 1,
            f'vat_code_1': vat_code.pk,
            f'vat_percentage_1': 15,
            f'total_vat_1': 0,
            f'vat_amount_1': 1443.53,
            f'payment_amount_1': 11067.09,
            f'change': 0.00,
            f'balance': 0.00
        }
        response = self.ajax_post(reverse('create_receipt'), data=data)
        assert response.status_code == 200
        ledger.refresh_from_db()
        assert ledger.balance == 0
        receipt = Receipt.objects.filter(branch__company=self.company).first()
        assert receipt.total_amount == payment_amount

        assert 1 == CustomerLedger.objects.filter(
            customer=customer, content_type=ContentType.objects.get_for_model(receipt), object_id=receipt.pk
        ).count()

        # Invoices updated
        invoice_1.refresh_from_db()
        invoice_2.refresh_from_db()
        invoice_5.refresh_from_db()
        assert invoice_1.open_amount == invoice_1.total_amount
        assert invoice_2.open_amount == invoice_2.total_amount
        assert invoice_5.open_amount == 0
        assert InvoiceReceipt.objects.filter(invoice=invoice_1, receipt=receipt).count() == 0
        assert InvoiceReceipt.objects.filter(invoice=invoice_2, receipt=receipt).count() == 0
        assert InvoiceReceipt.objects.filter(invoice=invoice_5, receipt=receipt).count() == 1

    def test_can_pay_sales_invoices_with_partial_ledger_balance(self):
        customer = factories.CustomerFactory(company=self.company, number='0009', name='Buco')
        payment_amount = 2020
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        account = factories.AccountFactory(company=self.company)
        payment_account = factories.AccountFactory(name='Nedbank - Current', code='8400/000', company=self.company)
        payment_method = factories.PaymentMethodFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=factories.AccountFactory(company=self.company))
        ledger = factories.CustomerLedgerFactory(
            balance=3000, customer=customer, period=period, created_by=self.profile,
            journal_line=factories.JournalLineFactory(debit=3000, credit=0, account=account)
        )
        invoice_1 = factories.SalesTaxInvoiceFactory(period=period, total_amount=22304.26, customer=customer,
                                                     branch=self.branch)
        invoice_2 = factories.SalesTaxInvoiceFactory(period=period, total_amount=1760.79, customer=customer,
                                                     branch=self.branch)
        invoice_3 = factories.SalesTaxInvoiceFactory(period=period, total_amount=16082.16, customer=customer,
                                                     branch=self.branch)
        invoice_4 = factories.SalesTaxInvoiceFactory(period=period, total_amount=19078.16, customer=customer,
                                                     branch=self.branch)
        invoice_5 = factories.SalesTaxInvoiceFactory(period=period, total_amount=14187.09, customer=customer,
                                                     branch=self.branch)
        ledger_amount = 2000.00
        data = {
            'date': from_date,
            'amount': payment_amount,
            'customer': customer.pk,
            'payment_method': payment_method.pk,
            'reference': 'Reference',
            f'discount_{invoice_1.pk}': 1338.26,
            f'amount_{invoice_1.pk}': 22304.26,
            f'open_amount_{invoice_1.pk}': 22304.26,
            f'discount_{invoice_2.pk}': 105.65,
            f'amount_{invoice_2.pk}': 1760.79,
            f'open_amount_{invoice_2.pk}': 1760.79,
            f'discount_{invoice_3.pk}': 964.93,
            f'amount_{invoice_3.pk}': 16082.16,
            f'open_amount_{invoice_3.pk}': 16082.16,
            f'discount_{invoice_4.pk}': 1144.69,
            f'amount_{invoice_4.pk}': 19078.16,
            f'open_amount_{invoice_4.pk}': 19078.16,
            f'discount_{invoice_5.pk}': 851.23,
            f'amount_{invoice_5.pk}': 14187.09,
            f'pay_{invoice_5.pk}': invoice_5.pk,
            f'open_amount_{invoice_5.pk}': 0.00,
            f'is_discount_{ledger.pk}': 0.00,
            f'amount_{ledger.pk}': ledger_amount * -1,
            f'pay_ledger_{ledger.pk}': ledger.pk,
            f'open_amount_{ledger.pk}': 0.00,
            f'account_1': payment_account.pk,
            f'allow_negative_stock_1': 1,
            f'vat_code_1': vat_code.pk,
            f'vat_percentage_1': 15,
            f'total_vat_1': 0,
            f'vat_amount_1': 1443.53,
            f'payment_amount_1': 11067.09,
            f'change': 0.00,
            f'balance': 0.00
        }
        response = self.ajax_post(reverse('create_receipt'), data=data)
        assert response.status_code == 200
        balance = ledger.balance - ledger_amount
        ledger.refresh_from_db()
        assert ledger.balance == balance
        receipt = Receipt.objects.filter(branch__company=self.company).first()
        assert receipt.total_amount == payment_amount

        assert 1 == CustomerLedger.objects.filter(
            customer=customer, content_type=ContentType.objects.get_for_model(receipt), object_id=receipt.pk
        ).count()

        # Invoices updated
        invoice_1.refresh_from_db()
        invoice_2.refresh_from_db()
        invoice_5.refresh_from_db()
        assert invoice_1.open_amount == invoice_1.total_amount
        assert invoice_2.open_amount == invoice_2.total_amount
        assert invoice_5.open_amount == 0
        assert InvoiceReceipt.objects.filter(invoice=invoice_1, receipt=receipt).count() == 0
        assert InvoiceReceipt.objects.filter(invoice=invoice_2, receipt=receipt).count() == 0
        assert InvoiceReceipt.objects.filter(invoice=invoice_5, receipt=receipt).count() == 1

    def test_can_create_unallocated_receipt(self):
        customer = factories.CustomerFactory(company=self.company, name='Buco', number='0009')
        account = factories.AccountFactory(company=self.company, name='Account 3', code='0003')
        payment_method = factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year)
        amount = 2000
        data = {
            'amount': amount,
            'balance': amount * -1,
            'payment_method': payment_method.pk,
            'date': period.from_date,
            'customer': customer.pk,
            'reference': 'Month End Payment'
        }

        response = self.ajax_post(reverse('create_receipt'), data=data)

        assert response.status_code == 200
        receipt = Receipt.objects.first()
        assert receipt.customer.pk == customer.pk
        assert receipt.period.pk == period.pk
        assert receipt.payments.count() == 1
        assert receipt.amount == utils.to_decimal(value=str(amount * -1))
        assert receipt.balance == utils.to_decimal(value=str(amount * -1))
        assert receipt.customer == customer
        assert receipt.branch == self.branch

        supplier_ledger = CustomerLedger.objects.first()
        assert supplier_ledger.object_id == receipt.id
        assert supplier_ledger.credit == receipt.total_amount
        assert supplier_ledger.status == LedgerStatus.PENDING.value

        age_analysis = AgeAnalysis.objects.filter(period=period, customer=customer)
        assert age_analysis.count() == 0

    def test_can_edit_invoice_payment(self):
        payment = factories.PaymentFactory(
            profile=self.profile, role=self.role, company=self.company,
            supplier=factories.SupplierFactory(company=self.company, code='0001'),
            account=factories.AccountFactory(company=self.company, name='Account 1', code='0001')
        )

        response = self.client.get(reverse('edit_payment', args=(payment.id, )))

        assert response.status_code == 200

    def test_can_update_invoice_payment(self):
        p = 4
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        april_period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                               from_date=from_date, period_year=self.year, company=self.company)

        p = 3
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=31)
        march_period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                               from_date=from_date, period_year=self.year, company=self.company)

        payment = factories.PaymentFactory(
            payment_date=april_period.from_date, period=april_period,
            profile=self.profile, role=self.role, company=self.company,
            payment_method=factories.PaymentMethodFactory(company=self.company, name='Bank'),
            supplier=factories.SupplierFactory(company=self.company, code='0001'),
            account=factories.AccountFactory(company=self.company, name='Account 1', code='0001')
        )
        data = {
            'period': march_period.pk,
            'payment_date': march_period.from_date,
            'account': payment.account.pk,
            'payment_method': payment.payment_method.pk,
            'supplier': payment.supplier.pk
        }

        response = self.client.post(reverse('edit_payment', args=(payment.id, )), data=data, follow=True)

        assert response.status_code == 200

        payment.refresh_from_db()
        assert payment.period.pk == march_period.pk
        assert payment.payment_date == march_period.from_date
        assert payment.account.pk == data['account']
        assert payment.payment_method.pk == data['payment_method']
        assert payment.supplier.pk == data['supplier']

    def test_delete_sales_receipt(self):
        """
        Test deleting the sales receipts
        """

    def test_allocated_receipt_payment(self):
        customer = factories.CustomerFactory(company=self.company, name='Länsförsäkringar', number='00010')
        factories.AccountFactory(company=self.company, name='Account 3', code='0003')
        payment_method = factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year)
        amount = -54000
        receipt = factories.ReceiptFactory(customer=customer, amount=amount, created_by=self.profile)
        age_analysis = factories.CustomerAgeAnalysisFactory(customer=customer, unallocated=amount * -1, current=0,
                                                            period=period)

        assert receipt.balance == utils.to_decimal(value=str(amount))

        data = {
            'date': period.from_date,
            'amount': amount,
            'customer': customer.pk,
            'payment_method': payment_method.pk,
            'reference': 'Reversal',
            f'receipt_allocate_{receipt.pk}': receipt.pk,
            f'receipt_balance_{receipt.pk}': amount,
            'change': amount * -1,
            'balance': 0.00,
        }

        response = self.ajax_post(reverse('create_receipt'), data=data)

        assert response.status_code == 200
        receipt.refresh_from_db()
        age_analysis.refresh_from_db()
        assert Receipt.objects.count() == 2
        assert receipt.balance == utils.to_decimal('0')

        assert age_analysis.balance == utils.to_decimal(value=str(amount * -1))
        assert age_analysis.unallocated == utils.to_decimal(value=str(amount * -1))

    def test_delete_allocated_receipt_payment(self):
        customer = factories.CustomerFactory(company=self.company, name='Länsförsäkringar', number='00010')
        factories.AccountFactory(company=self.company, name='Account 3', code='0003')
        factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year)

        amount = -54000
        receipt = factories.ReceiptFactory(customer=customer, amount=amount, created_by=self.profile, balance=0)
        age_analysis = factories.CustomerAgeAnalysisFactory(customer=customer, unallocated=amount * -1, current=0,
                                                            period=period)
        assert receipt.balance == utils.to_decimal(value=str(0))

        parent_receipt = factories.ReceiptFactory(customer=customer, amount=amount, created_by=self.profile, balance=0)
        factories.SundryReceiptFactory(parent=parent_receipt, receipt=receipt, amount=amount)

        response = self.api_call(url=reverse('delete_receipt', args=(parent_receipt.pk, )), method='delete')

        assert response.status_code == 200
        receipt.refresh_from_db()
        assert Receipt.objects.count() == 1
        assert receipt.balance == utils.to_decimal(value=str(amount))

        assert age_analysis.balance == utils.to_decimal(value=str(amount * -1))
        assert age_analysis.unallocated == utils.to_decimal(value=str(amount * -1))

    def test_create_cash_invoice_receipt(self):
        bank_transfer = factories.PaymentMethodFactory(company=self.company, name='Bank Transfer/EFT')
        cash = factories.PaymentMethodFactory(company=self.company, name='Cash')
        cheque = factories.PaymentMethodFactory(company=self.company, name='Cheque')
        credit_card = factories.PaymentMethodFactory(company=self.company, name='Credit Card')
        debit_order = factories.PaymentMethodFactory(company=self.company, name='Debit Order')

        data = {
            'amount': 1831.43,
            'date': '2021-08-05',
            'payment_method': [bank_transfer.pk, cash.pk, cheque.pk, credit_card.pk, debit_order.pk],
            f'amount_{bank_transfer.pk}': 1831.43,
            f'amount_{cash.pk}': '',
            f'amount_{cheque.pk}': '',
            f'amount_{credit_card.pk}': '',
            f'amount_{debit_order.pk}': '',
            '_balance': '',
            'change': '',
            'balance': 0.00
        }

        response = self.ajax_post(url=reverse('create_cash_invoice_receipt'), data=data)

        assert response.status_code == 200

        assert ('sales_invoice_receipt' in self.client.session) is True
        session_data = self.client.session['sales_invoice_receipt']
        assert session_data == {'change': 0, 'amount': 1831.43, 'balance': 0.0, 'date': '2021-08-05',
                                'payment_methods': [{'amount': 1831.43, 'payment_method': str(bank_transfer.pk)}],
                                'receipts': []
                                }

    def test_cannot_create_invoice_receipt_with_zero_total_for_invoices(self):
        bank_transfer = factories.PaymentMethodFactory(company=self.company, name='Bank Transfer/EFT')
        customer = factories.CustomerFactory(company=self.company, number='0003')

        period = factories.PeriodFactory(period_year=self.year, period=1)
        invoice = factories.SalesTaxInvoiceFactory(
            period=period, total_amount=465.75, customer=customer, branch=self.branch
        )

        credit_note = factories.CreditNoteInvoiceFactory(
            period=period, total_amount=-184, customer=customer, branch=self.branch
        )

        data = {
            'date': '2022-09-11',
            'amount': '0',
            'customer': f'{customer.pk}',
            'payment_method': f'{bank_transfer.pk}',
            'reference': 'Test',
            f'discount_{invoice.pk}': '0.00',
            f'amount_{invoice.pk}': '465.75',
            f'pay_{invoice.pk}': f'{invoice.pk}',
            f'open_amount_{invoice.pk}': '0.00',
            f'discount_{credit_note.pk}': '0.00',
            f'amount_{credit_note.pk}': '-184.00',
            f'pay_{credit_note.pk}': f'{credit_note.pk}',
            f'open_amount_{credit_note.pk}': '0.00',
            'change': '0.00',
            'balance': '0.00'
        }

        response = self.ajax_post(url=reverse('create_cash_invoice_receipt'), data=data)
        assert response.status_code == 200
        # assert ('sales_invoice_receipt' in self.client.session) is True
        # session_data = self.client.session['sales_invoice_receipt']
