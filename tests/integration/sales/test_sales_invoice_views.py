from decimal import Decimal
from http import HTTPStatus

from django.urls import reverse
from django.utils import timezone

from docuflow.apps.common.enums import Module
from docuflow.apps.customer.enums import LedgerStatus, PaymentTerm
from docuflow.apps.customer.models import Ledger as CustomerLedger, Customer
from docuflow.apps.inventory.enums import LedgerStatus as InventoryLedgerStatus
from docuflow.apps.inventory.models import Ledger as InventoryLedger
from docuflow.apps.invoice.models import Invoice as PurchaseInvoice
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.sales.enums import InvoiceItemType, InvoiceItemStatus
from docuflow.apps.sales.models import Invoice, InvoiceItem, InvoiceReceipt
from tests import factories
from tests import utils
from tests.base import AuthenticatedTestCase


class SalesInvoiceViewTestCase(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.list_url = reverse('sales_documents_index')

    def test_can_list_sales_invoices(self):
        period = factories.PeriodFactory(period_year=self.year)
        y = timezone.now().year - 1
        previous_year = factories.PeriodFactory(period_year=factories.YearFactory(year=y))

        factories.SalesTaxInvoiceFactory(branch=self.branch, period=period)
        factories.CreditNoteInvoiceFactory(branch=self.branch, period=period)
        factories.SalesTaxInvoiceFactory(branch=self.branch, period=previous_year)

        response = self.client.get(self.list_url)

        assert response.status_code == HTTPStatus.OK

    def test_generating_invoice_number_sequence(self):
        pass

    def test_generating_credit_note_number_sequence(self):
        period = factories.PeriodFactory()
        tax_invoice_type = factories.SalesTaxInvoiceTypeFactory(company=self.company)
        credit_invoice_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)

        first_invoice = factories.SalesTaxInvoiceFactory(
            branch=self.branch, period=period, invoice_type=tax_invoice_type
        )
        second_invoice = factories.SalesTaxInvoiceFactory(
            branch=self.branch, period=period, invoice_type=tax_invoice_type
        )
        first_credit_note = factories.CreditNoteInvoiceFactory(
            branch=self.branch, period=period, invoice_type=credit_invoice_type
        )
        second_credit_note = factories.CreditNoteInvoiceFactory(
            branch=self.branch, period=period, invoice_type=credit_invoice_type
        )
        third_invoice = factories.SalesTaxInvoiceFactory(
            branch=self.branch, period=period, invoice_type=tax_invoice_type
        )

    def test_create_sales_credit_invoice(self):
        tax_invoice_credit_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(company=self.company)
        credit_invoice_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        inventory_1 = factories.BranchInventoryFactory(branch=self.branch)
        inventory_2 = factories.BranchInventoryFactory(branch=self.branch)
        account = factories.AccountFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company))

        data = {
            'invoice_date': period.to_date,
            'invoice_type': credit_invoice_type.id,
            'period': period,
            'postal_country': 'ZA',
            'delivery_country': 'ZA',
            'customer': customer.id,
            'estimate_number': 'ES0001',
            'po_number': '1232',
            'vat_number': '4790262499',
            'total_amount': 14.92,
            'vat_amount': 1.95,
            'net_amount': 12.97,
            'sub_total': 12.97,
            'customer_discount': 0,
            'line_discount': -0.48,
            'is_pick_up': True,

            'type_1': 'i',
            'inventory_1': inventory_1.id,
            'quantity_1': 3.00,
            'invoiced_1': 3,
            'unit_1': unit.id,
            'price_1': 3.15,
            'price_including_1': 3.6225,
            'total_price_excluding_1': 9.45,
            'sub_total_1': 9.45,
            'total_price_including_1': 10.87,
            'total_discount_1': 0,
            'vat_code_1': vat_code.id,
            'vat_percentage_1': vat_code.percentage,
            'total_vat_1': 1.42,
            'vat_amount_1': 1.42,

            'type_2': 'i',
            'inventory_2': inventory_2.id,
            'quantity_2': 0,
            'invoiced_2': 3,
            'unit_2': unit.id,
            'price_2': 18.00,
            'price_including_2': 20.70,
            'total_price_excluding_2': 0.00,
            'sub_total_2': 0.00,
            'total_price_including_2': 0.00,
            'total_discount_2': 0,
            'vat_code_2': vat_code.id,
            'vat_percentage_2': vat_code.percentage,
            'total_vat_2': 0.00,
            'vat_amount_2': 0.00,

            'type_3': 'g',
            'account_3': account.id,
            'description_3': 'Testing',
            'quantity_3': 2.00,
            'invoiced_3': 2,
            'price_3': 2.00,
            'price_including_3': 2.3,
            'total_price_excluding_3': 3.52,
            'sub_total_3': 4.00,
            'total_price_including_3': 4.05,
            'total_discount_3': 0.48,
            'vat_code_3': vat_code.id,
            'vat_percentage_3': vat_code.percentage,
            'total_vat_3': 0.53,
            'vat_amount_3': 0.53,

            'type_4': 't',
            'item_description_4': 'Just the text'
        }
        assert Invoice.objects.count() == 0
        response = self.ajax_post(reverse('create_invoice', kwargs={'invoice_type': tax_invoice_credit_type.code}),
                                  data=data)

        assert response.status_code == HTTPStatus.OK

        invoices = Invoice.objects.all()

        assert len(invoices) == 1
        invoice = invoices[0]
        amount = Decimal(data['total_amount']).quantize(Decimal('.01')) * -1
        assert invoice.total_amount == amount
        assert invoice.estimate_number == data['estimate_number']
        assert invoice.po_number == data['po_number']
        assert invoice.vat_number == data['vat_number']
        assert invoice.invoice_items.count() == 4

        # Customer ledger
        customer_ledger = CustomerLedger.objects.first()
        assert CustomerLedger.objects.count() == 1
        assert customer_ledger.object_id == invoice.id
        assert customer_ledger.amount == amount
        assert customer_ledger.status == LedgerStatus.PENDING

        # Inventory ledger
        inventory_ledgers = InventoryLedger.objects.filter(inventory__branch=self.branch, status=InventoryLedgerStatus.PENDING)
        assert inventory_ledgers.count() == 2
        assert inventory_ledgers.filter(status=InventoryLedgerStatus.COMPLETE).count() == 0
        assert inventory_1.in_stock == 0
        assert inventory_2.in_stock == 0

        # Journal ledger
        assert JournalLine.objects.count() == 0

    def test_create_customer_credit_note_for_accounts(self):
        tax_invoice_credit_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(company=self.company)
        credit_invoice_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, code=10, percentage=25)
        unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company))

        data = {
            'invoice_date': period.to_date,
            'invoice_type': credit_invoice_type.id,
            'period': period,
            'postal_country': 'ZA',
            'delivery_country': 'ZA',
            'customer': customer.id,
            'estimate_number': 'ES0001',
            'po_number': 'TK00019',
            'vat_number': '4790262499',
            'is_pick_up': True,
            'line_discount': 0.00,
            'sub_total': 364318.40,
            'customer_discount': 0.00,
            'net_amount': 364318.40,
            'vat_amount': 91079.60,
            'total_amount': 455398.00,
            'action': 'continue',

            'type_1': 'g',
            'account_1': account.pk,
            'allow_negative_stock_1': 1,
            'description_1': 'Nissan Navara',
            'quantity_1': 1.00,
            'invoiced_1': 1,
            'price_1': 153368.80,
            'price_including_1': 191711,
            'total_price_excluding_1': 153368.80,
            'sub_total_1': 153368.80,
            'total_price_including_1': 191711.00,
            'discount_percentage_1': 0,
            'total_discount_1': 0,
            'vat_code_1': vat_code.pk,
            'vat_percentage_1': 25,
            'total_vat_1': 38342.2,
            'vat_amount_1': 38342.20,

            'type_2': 't',
            'item_description_2': 'Reg Number',

            'type_3': 't',
            'item_description_3': 'Chassis Number',

            'type_4': 'g',
            'account_4': account.pk,
            'allow_negative_stock_4': 1,
            'description_4': 'K`VDBill paym',
            'quantity_4': 1.00,
            'invoiced_4': 1,
            'price_4': 210949.60,
            'price_including_4': 263687,
            'total_price_excluding_4': 210949.60,
            'sub_total_4': 210949.60,
            'total_price_including_4': 263687.00,
            'discount_percentage_4': 0,
            'total_discount_4': 0,
            'vat_code_4': vat_code.pk,
            'vat_percentage_4': 25,
            'total_vat_4': 52737.4,
            'vat_amount_4': 52737.40,
        }

        assert Invoice.objects.count() == 0

        response = self.ajax_post(reverse('create_invoice', kwargs={'invoice_type': tax_invoice_credit_type.code}),
                                  data=data)

        assert response.status_code == HTTPStatus.OK

        invoices = Invoice.objects.all()

        assert len(invoices) == 1

        invoice = invoices[0]
        amount = utils.to_decimal(data['total_amount']) * -1
        assert invoice.total_amount == amount
        assert invoice.net_amount == utils.to_decimal(data['net_amount']) * -1
        assert invoice.vat_amount == utils.to_decimal(data['vat_amount']) * -1
        assert invoice.open_amount == amount
        assert invoice.estimate_number == data['estimate_number']
        assert invoice.po_number == data['po_number']
        assert invoice.vat_number == data['vat_number']
        assert invoice.invoice_items.count() == 4

        # Customer ledger
        customer_ledger = CustomerLedger.objects.first()
        assert CustomerLedger.objects.count() == 1
        assert customer_ledger.object_id == invoice.id
        assert customer_ledger.amount == amount
        assert customer_ledger.status == LedgerStatus.PENDING

        invoice_items = InvoiceItem.objects.all()
        assert len(invoice_items) == 4

        account_invoice_item = invoice_items[0]
        assert account_invoice_item.account == account
        assert account_invoice_item.description == data['description_1']
        assert account_invoice_item.quantity == utils.to_decimal(data['quantity_1']) * -1
        assert account_invoice_item.price == utils.to_decimal(data['price_1'])
        assert account_invoice_item.discount == utils.to_decimal(data['discount_percentage_1'])
        assert account_invoice_item.net_price == utils.to_decimal(data['sub_total_1']) * -1
        assert account_invoice_item.inventory_id is None
        assert account_invoice_item.invoice_id == invoice.pk
        assert account_invoice_item.unit_id is None
        assert account_invoice_item.customer_discount is None  # TODO - make amount default to 0
        assert account_invoice_item.total_amount == utils.to_decimal(data['total_price_including_1'])
        assert account_invoice_item.vat == vat_code.percentage
        assert account_invoice_item.vat_code_id == vat_code.pk
        assert account_invoice_item.status == InvoiceItemStatus.PENDING
        assert account_invoice_item.available_quantity == utils.to_decimal(data['quantity_1'])
        assert account_invoice_item.item_link is None
        assert account_invoice_item.item_type == InvoiceItemType.GENERAL
        assert account_invoice_item.vat_amount == utils.to_decimal(data['vat_amount_1']) * -1

        # Inventory ledger
        # inventory_ledgers = InventoryLedger.objects.filter(inventory__branch=self.branch, status=InventoryLedgerStatus.PENDING)
        # assert inventory_ledgers.count() == 2
        # assert inventory_ledgers.filter(status=InventoryLedgerStatus.COMPLETE).count() == 0
        # assert inventory_1.in_stock == 0
        # assert inventory_2.in_stock == 0

        # Journal ledger
        assert JournalLine.objects.count() == 0

    def test_can_create_cash_sales_tax_invoice(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(company=self.company)
        tax_invoice = factories.SalesTaxInvoiceTypeFactory(company=self.company)
        inventory_1 = factories.BranchInventoryFactory(branch=self.branch)
        inventory_2 = factories.BranchInventoryFactory(branch=self.branch)
        account = factories.AccountFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company))

        bank = factories.PaymentMethodFactory(name='Bank Transfer/EFT', company=self.company)

        data = {
            'invoice_date': period.to_date,
            'invoice_type': tax_invoice.id,
            'period': period,
            'postal_country': 'ZA',
            'delivery_country': 'ZA',
            'customer': customer.id,
            'estimate_number': 'ES0001',
            'po_number': '1232',
            'vat_number': '4790262499',
            'total_amount': 14.92,
            'vat_amount': 1.95,
            'net_amount': 12.97,
            'sub_total': 12.97,
            'customer_discount': 0,
            'line_discount': -0.48,
            'is_pick_up': True,

            'type_1': 'i',
            'inventory_1': inventory_1.id,
            'quantity_1': 3.00,
            'invoiced_1': 3,
            'unit_1': unit.id,
            'price_1': 3.15,
            'price_including_1': 3.6225,
            'total_price_excluding_1': 9.45,
            'sub_total_1': 9.45,
            'total_price_including_1': 10.87,
            'total_discount_1': 0,
            'vat_code_1': vat_code.id,
            'vat_percentage_1': vat_code.percentage,
            'total_vat_1': 1.42,
            'vat_amount_1': 1.42,

            'type_2': 'i',
            'inventory_2': inventory_2.id,
            'quantity_2': 0,
            'invoiced_2': 3,
            'unit_2': unit.id,
            'price_2': 18.00,
            'price_including_2': 20.70,
            'total_price_excluding_2': 0.00,
            'sub_total_2': 0.00,
            'total_price_including_2': 0.00,
            'total_discount_2': 0,
            'vat_code_2': vat_code.id,
            'vat_percentage_2': vat_code.percentage,
            'total_vat_2': 0.00,
            'vat_amount_2': 0.00,

            'type_3': 'g',
            'account_3': account.id,
            'description_3': 'Testing',
            'quantity_3': 2.00,
            'invoiced_3': 2,
            'price_3': 2.00,
            'price_including_3': 2.3,
            'total_price_excluding_3': 3.52,
            'sub_total_3': 4.00,
            'total_price_including_3': 4.05,
            'total_discount_3': 0.48,
            'vat_code_3': vat_code.id,
            'vat_percentage_3': vat_code.percentage,
            'total_vat_3': 0.53,
            'vat_amount_3': 0.53,

            'type_4': 't',
            'item_description_4': 'Just the text'
        }
        assert Invoice.objects.count() == 0

        session = self.client.session
        session['sales_invoice_receipt'] = {'change': 0, 'amount': 14.92, 'balance': 0.0,
                                            'date': period.to_date.strftime('%Y-%m-%d'),
                                            'payment_methods': [{'amount': 14.92,  'payment_method': bank.pk},
                                                                ],
                                            'receipts': []}
        session.save()

        response = self.ajax_post(reverse('create_invoice', kwargs={'invoice_type': tax_invoice.code}), data=data)

        assert response.status_code == HTTPStatus.OK

        invoices = Invoice.objects.all()

        assert len(invoices) == 1

        invoice = invoices[0]
        assert invoice.total_amount == Decimal(data['total_amount']).quantize(Decimal('.01'))
        assert invoice.estimate_number == data['estimate_number']
        assert invoice.po_number == data['po_number']
        assert invoice.vat_number == data['vat_number']
        assert invoice.invoice_items.count() == 4

    def test_can_create_cash_customer_tax_invoice(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CashCustomerFactory(company=self.company)
        tax_invoice = factories.SalesTaxInvoiceTypeFactory(company=self.company)
        inventory_1 = factories.BranchInventoryFactory(branch=self.branch)
        inventory_2 = factories.BranchInventoryFactory(branch=self.branch)
        account = factories.AccountFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company))

        bank = factories.PaymentMethodFactory(name='Bank Transfer/EFT', company=self.company)
        cash = factories.PaymentMethodFactory(name='Cash', company=self.company)
        cheque = factories.PaymentMethodFactory(name='Cheque', company=self.company)
        credit_card = factories.PaymentMethodFactory(name='Credit Card	', company=self.company)

        data = {
            'invoice_date': period.to_date,
            'invoice_type': tax_invoice.id,
            'period': period,
            'postal_country': 'ZA',
            'delivery_country': 'ZA',
            'customer': customer.id,
            'estimate_number': 'ES0001',
            'po_number': '1232',
            'vat_number': '4790262499',
            'total_amount': 14.92,
            'vat_amount': 1.95,
            'net_amount': 12.97,
            'sub_total': 12.97,
            'customer_discount': 0,
            'line_discount': -0.48,
            'is_pick_up': True,

            'type_1': 'i',
            'inventory_1': inventory_1.id,
            'quantity_1': 3.00,
            'invoiced_1': 3,
            'unit_1': unit.id,
            'price_1': 3.15,
            'price_including_1': 3.6225,
            'total_price_excluding_1': 9.45,
            'sub_total_1': 9.45,
            'total_price_including_1': 10.87,
            'total_discount_1': 0,
            'vat_code_1': vat_code.id,
            'vat_percentage_1': vat_code.percentage,
            'total_vat_1': 1.42,
            'vat_amount_1': 1.42,

            'type_2': 'i',
            'inventory_2': inventory_2.id,
            'quantity_2': 0,
            'invoiced_2': 3,
            'unit_2': unit.id,
            'price_2': 18.00,
            'price_including_2': 20.70,
            'total_price_excluding_2': 0.00,
            'sub_total_2': 0.00,
            'total_price_including_2': 0.00,
            'total_discount_2': 0,
            'vat_code_2': vat_code.id,
            'vat_percentage_2': vat_code.percentage,
            'total_vat_2': 0.00,
            'vat_amount_2': 0.00,

            'type_3': 'g',
            'account_3': account.id,
            'description_3': 'Testing',
            'quantity_3': 2.00,
            'invoiced_3': 2,
            'price_3': 2.00,
            'price_including_3': 2.3,
            'total_price_excluding_3': 3.52,
            'sub_total_3': 4.00,
            'total_price_including_3': 4.05,
            'total_discount_3': 0.48,
            'vat_code_3': vat_code.id,
            'vat_percentage_3': vat_code.percentage,
            'total_vat_3': 0.53,
            'vat_amount_3': 0.53,

            'type_4': 't',
            'item_description_4': 'Just the text'
        }
        session = self.client.session
        session['sales_invoice_receipt'] = {'change': 0, 'amount': 14.92, 'balance': 0.0,
                                            'date': period.to_date.strftime('%Y-%m-%d'),
                                            'payment_methods': [{'amount': 4.40,  'payment_method': bank.pk},
                                                                {'amount': 1.02, 'payment_method': cash.pk},
                                                                {'amount': 5.00, 'payment_method': cheque.pk},
                                                                {'amount': 4.50, 'payment_method': credit_card.pk}
                                                                ],
                                            'receipts': []}
        session.save()

        assert Invoice.objects.count() == 0

        response = self.ajax_post(reverse('create_invoice', kwargs={'invoice_type': tax_invoice.code}), data=data)

        assert response.status_code == 200

        invoices = Invoice.objects.all()
        customers = Customer.objects.all()

        assert len(invoices) == 1
        assert len(customers) == 2
        assert invoices[0].total_amount == Decimal(data['total_amount']).quantize(Decimal('.01'))
        assert invoices[0].estimate_number == data['estimate_number']
        assert invoices[0].po_number == data['po_number']
        assert invoices[0].vat_number == data['vat_number']
        assert invoices[0].invoice_items.count() == 4
        assert invoices[0].customer_id == customers[0].id

    def test_create_and_send_invoice_to_docuflow(self):
        pass

    def test_create_sales_tax_invoice_for_customer_with_credits(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(
            company=self.company, number='I0036', payment_term=PaymentTerm.INVOICE_DATE.value, credit_limit=50000,
            days=30, available_credit=50000
        )
        tax_invoice = factories.SalesTaxInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(company=self.company, code='3052', name='Transport')
        vat_code = factories.VatCodeFactory(company=self.company)

        data = {
            'invoice_date': period.to_date,
            'invoice_type': tax_invoice.id,
            'period': period,
            'postal_country': 'ZA',
            'delivery_country': 'ZA',
            'customer': customer.id,
            'estimate_number': 'ES0001',
            'po_number': '1232',
            'vat_number': '4790262499',
            'total_amount':  15600.00,
            'vat_amount': 3120.00,
            'net_amount': 12480.00,
            'sub_total': 12480.00,
            'customer_discount': 0,
            'line_discount': 0,
            'is_pick_up': True,

            'type_1': 'g',
            'account_1': account.id,
            'description_1': 'Nissan NV200',
            'quantity_1': 1.00,
            'invoiced_1': 1,
            'price_1': 12480.00,
            'price_including_1': 2.3,
            'total_price_excluding_1': 12480.00,
            'sub_total_1': 12480.00,
            'total_price_including_1': 15600.00,
            'total_discount_1': 0,
            'vat_code_1': vat_code.id,
            'vat_percentage_1': vat_code.percentage,
            'total_vat_1': 3120.00,
            'vat_amount_1': 3120.00,

            'type_2': 't',
            'item_description_2': 'Just the text'
        }

        response = self.ajax_post(reverse('create_invoice', kwargs={'invoice_type': tax_invoice.code}), data=data)

        assert response.status_code == HTTPStatus.OK

        invoices = Invoice.objects.all()

        assert len(invoices) == 1

        invoice = invoices[0]
        assert invoice.total_amount == Decimal(data['total_amount']).quantize(Decimal('.01'))
        assert invoice.estimate_number == data['estimate_number']
        assert invoice.po_number == data['po_number']
        assert invoice.vat_number == data['vat_number']
        assert invoice.invoice_items.count() == 2

        credit_limit = customer.credit_limit
        customer.refresh_from_db()
        available_credit = credit_limit - data['total_amount']
        assert customer.available_credit == available_credit
        assert customer.credit_limit == credit_limit

    def test_create_sales_tax_invoice_for_customer_without_enough_credits(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(
            company=self.company, number='I0036', payment_term=PaymentTerm.INVOICE_DATE.value, credit_limit=10000,
            days=30, available_credit=10000
        )
        tax_invoice = factories.SalesTaxInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(company=self.company, code='3052', name='Transport')
        vat_code = factories.VatCodeFactory(company=self.company)

        data = {
            'invoice_date': period.to_date,
            'invoice_type': tax_invoice.id,
            'period': period,
            'postal_country': 'ZA',
            'delivery_country': 'ZA',
            'customer': customer.id,
            'estimate_number': 'ES0001',
            'po_number': '1232',
            'vat_number': '4790262499',
            'total_amount':  15600.00,
            'vat_amount': 3120.00,
            'net_amount': 12480.00,
            'sub_total': 12480.00,
            'customer_discount': 0,
            'line_discount': 0,
            'is_pick_up': True,

            'type_1': 'g',
            'account_1': account.id,
            'description_1': 'Nissan NV200',
            'quantity_1': 1.00,
            'invoiced_1': 1,
            'price_1': 12480.00,
            'price_including_1': 2.3,
            'total_price_excluding_1': 12480.00,
            'sub_total_1': 12480.00,
            'total_price_including_1': 15600.00,
            'total_discount_1': 0,
            'vat_code_1': vat_code.id,
            'vat_percentage_1': vat_code.percentage,
            'total_vat_1': 3120.00,
            'vat_amount_1': 3120.00,

            'type_2': 't',
            'item_description_2': 'Just the text'
        }

        response = self.ajax_post(reverse('create_invoice', kwargs={'invoice_type': tax_invoice.code}), data=data)

        assert response.status_code == HTTPStatus.OK
        assert response.data['details'] == 'Not enough credit to invoice this, please contact accounts department' \
                                           ' or enter your 4 digit over ride key'
        assert ('override_url' in response.data) is True
        assert response.data['not_enough_credit'] is True

        invoices = Invoice.objects.all()

        assert len(invoices) == 0

        credit_limit = customer.credit_limit
        available_credit = customer.available_credit

        customer.refresh_from_db()
        assert customer.available_credit == available_credit
        assert customer.credit_limit == credit_limit

    def test_create_sales_tax_invoice_for_customer_with_updated_invoices(self):
        """
        TODO - Test for available credits, where there are customer invoices updated, to test this check in forms
        available_credit = invoice.customer.calculate_available (CustomerInvoiceForm line 682)
        """

    def test_credit_tax_invoice_with_a_receipt_linked(self):
        invoice_type = factories.SalesTaxInvoiceTypeFactory(company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(company=self.company)
        factories.SalesCreditInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(company=self.company, code='3052', name='Externt')
        vat_code = factories.VatCodeFactory(company=self.company, code=10, percentage=25)
        factories.VatCodeFactory(company=self.company, code=10, percentage=25, is_default=True, label='Output Vat',
                                 module=Module.SALES)

        invoice = factories.SalesTaxInvoiceFactory(
            invoice_type=invoice_type, total_amount=14816.66, net_amount=12884.05, vat_amount=1932.61,
            open_amount=14816.66, branch=self.branch, customer=customer, po_number='ZDM788', vat_number='Idalie',
            sub_total=12884.05
        )

        navara_item = factories.AccountItemFactory(
            description='Nissan Navara 2.3 DCI 4x4', quantity=1.00, price=12884.05, net_price=12884.05,
            invoice=invoice, total_amount=14816.66, vat=25.0000, vat_code=vat_code, account=account,
            vat_amount=1932.61
        )

        receipt = factories.ReceiptFactory(
            customer=customer, amount=14816.66, balance=0, branch=self.branch, created_by=self.profile
        )

        factories.InvoiceReceiptFactory(invoice=invoice, receipt=receipt, amount=14816.66)
        data = {
            'branch': self.branch.pk,
            'invoice_type': invoice_type.pk,
            'customer': customer.pk,
            'customer_name': str(customer),
            'postal_street': '158 Burman Road, Deal Party',
            'postal_city': 'Port Elizabeth',
            'postal_province': '',
            'postal_postal_code': 6000,
            'postal_country': 'ZA',
            'credit_limit': 0,
            'is_pick_up': 'on',
            'delivery_address': '',
            'address_id': '',
            'delivery_street': '',
            'delivery_city': '',
            'delivery_province': '',
            'delivery_postal_code': '',
            'estimate_number': '',
            'po_number': '',
            'vat_number': '4290292269',
            'period': period.pk,
            'invoice_date': period.from_date,
            'due_date': period.to_date,
            'salesman': '',
            'line_discount': 0.00,
            'sub_total': 12884.05,
            'customer_discount': 0.00,
            'net_amount': 12884.05,
            'vat_amount': 1932.61,
            'total_amount': 14816.66,
            'action': 'continue',

            'id_1': navara_item.pk,
            'type_1': 'g',
            'account_1': account.pk,
            'allow_negative_stock_1': 1,
            'description_1': 'Nissan Navara 2.3 DCI 4x4',
            'quantity_1': 1.00,
            'invoiced_1': 1,
            'price_1': 12884.05,
            'price_including_1': 14816.66,
            'total_price_excluding_1': 12884.05,
            'sub_total_1': 12884.05,
            'total_price_including_1': 14816.66,
            'discount_percentage_1': 0,
            'total_discount_1': 0,
            'vat_code_1': vat_code.pk,
            'vat_percentage_1': 15,
            'total_vat_1': 1932.61,
            'vat_amount_1': 1932.61
        }

        response = self.ajax_post(reverse('credit_invoice', kwargs={'pk': invoice.pk}), data=data)

        assert response.status_code == HTTPStatus.OK

        invoice.refresh_from_db()
        receipt.refresh_from_db()
        assert InvoiceReceipt.objects.count() == 0

        invoices = Invoice.objects.all()
        assert len(invoices) == 2
        assert invoices[0].linked_invoice == invoice
        assert receipt.balance == invoice.total_amount

    def test_send_invoice_to_docuflow(self):
        new_company = factories.CompanyFactory(area_code='41054', name='Df Company')
        factories.WorkflowFactory(company=new_company, is_default=True, created_by=self.profile.user)
        factories.PeriodFactory(company=new_company, opened=True, period=timezone.now().date().month,
                                period_year=factories.YearFactory(company=new_company, started=True))
        invoice_status = factories.StatusFactory()
        factories.TaxInvoiceTypeFactory(company=new_company)
        factories.VatCodeFactory(company=new_company, account=factories.AccountFactory(company=new_company))
        supplier = factories.SupplierFactory(company=new_company, vat_number='123456')
        factories.LoggerStatusFactory()
        expenditure_account = factories.AccountFactory(company=new_company, code='8231/001', name='Expenditure Control')
        supplier_account = factories.SupplierAccountFactory(company=new_company, code='9291/002', name='Supplier Control')
        customer_account = factories.CustomerAccountFactory(company=new_company, code='8001/103', name='Customer Control')
        vat_account = factories.VatAccountFactory(company=new_company, code='9505/001', name='DF Control - Input Vat')

        new_company.default_vat_account = vat_account
        new_company.default_expenditure_account = expenditure_account
        new_company.default_supplier_account = supplier_account
        new_company.default_customer_account = customer_account
        new_company.save()

        customer = factories.CustomerFactory(supplier=supplier, df_number=new_company.slug)
        tax_invoice = factories.SalesTaxInvoiceFactory(branch=self.branch, customer=customer)

        resp = self.api_call(reverse('send_to_docuflow', args=(tax_invoice.pk, )), method='post')

        assert resp.status_code == HTTPStatus.OK

        purchase_invoices = PurchaseInvoice.objects.all()

        assert len(purchase_invoices) == 1

        purchase_invoice = purchase_invoices[0]
        assert purchase_invoice.sales_invoice == tax_invoice
        assert purchase_invoice.status.slug == invoice_status.slug

    def test_account_edit_credit_note(self):
        credit_note = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(company=self.company)
        credit_invoice_type = factories.SalesCreditInvoiceTypeFactory(company=self.company)
        account = factories.AccountFactory(company=self.company, code='3052', name='Externt')
        vat_code = factories.VatCodeFactory(company=self.company, code=10, percentage=25)
        # unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company))

        invoice = factories.SalesTaxInvoiceFactory(
            invoice_type=credit_note, total_amount=-60981.00, net_amount=-48784.80, vat_amount=-12196.20,
            open_amount=-60981.00, branch=self.branch, customer=customer, po_number='ZDM788', vat_number='SE5565808432',
            sub_total=-48784.80
        )

        navara_item = factories.AccountItemFactory(
            description='Nissan Navara 2.3 DCI 4x4', quantity=-1.00, price=-159044.80, net_price=-159044.80,
            invoice=invoice, total_amount=-198806.00, vat=25.0000, vat_code=vat_code, account=account,
            vat_amount=-39761.20
        )
        kvd_bill_item = factories.AccountItemFactory(
            description='Betalning mottagen - KVDBil', quantity=-1.00, price=207829.60, net_price=207829.60,
            invoice=invoice, total_amount=259787.00, vat=25.0000, vat_code=vat_code, account=account,
            vat_amount=51957.40
        )
        reg_nr = factories.TextItemFactory(description='Reg nr.: ZDM788', invoice=invoice)
        chassis_nr = factories.TextItemFactory(description='Chassis nr.: VSKCTND23U0068416', invoice=invoice)

        bank = factories.PaymentMethodFactory(name='Bank Transfer/EFT', company=self.company)

        data = {
            'invoice_date': period.to_date,
            'invoice_type': credit_invoice_type.id,
            'period': period,
            'postal_country': 'ZA',
            'delivery_country': 'ZA',
            'customer': customer.id,
            'estimate_number': 'ES0001',
            'po_number': 'TK00019',
            'vat_number': '4790262499',
            'is_pick_up': True,
            'line_discount': 0.00,
            'sub_total': 48784.80,
            'customer_discount': 0.00,
            'net_amount': 48784.80,
            'vat_amount': 12196.20,
            'total_amount': 60981.00,
            'action': 'continue',

            'id_1': navara_item.pk,
            'account_1': account.pk,
            'allow_negative_stock_1': 1,
            'description_1': 'Nissan Navara 2.3 DCI 4x4',
            'quantity_1': -1.00,
            'invoiced_1': -1,
            'price_1': 159044.80,
            'price_including_1': 198806,
            'total_price_excluding_1': -159044.80,
            'sub_total_1': -159044.80,
            'total_price_including_1': -198806.00,
            'discount_percentage_1': 0,
            'total_discount_1': 0,
            'vat_code_1': vat_code.pk,
            'vat_percentage_1': 25,
            'total_vat_1': -39761.20,
            'vat_amount_1': -39761.20,

            'id_2': reg_nr.pk,
            'item_description_2': 'Reg Number',

            'id_3': chassis_nr.pk,
            'item_description_3': 'Chassis Number',

            'id_4': kvd_bill_item.pk,
            'account_4': account.pk,
            'allow_negative_stock_4': 1,
            'description_4': 'Betalning mottagen - KVDBil',
            'quantity_4': 1.00,
            'invoiced_4': 1,
            'price_4': 207829.60,
            'price_including_4': 259787,
            'total_price_excluding_4': 207829.60,
            'sub_total_4': 207829.60,
            'total_price_including_4': 259787.00,
            'discount_percentage_4': 0,
            'total_discount_4': 0,
            'vat_code_4': vat_code.pk,
            'vat_percentage_4': 25,
            'total_vat_4': 51957.40,
            'vat_amount_4': 51957.40,
        }

        session = self.client.session
        session['sales_invoice_receipt'] = {
            'change': 0, 'amount': 60981.00, 'balance': 0.0, 'date': period.to_date.strftime('%Y-%m-%d'),
            'payment_methods': [{'amount': 60981.00,  'payment_method': bank.pk}], 'receipts': []
        }
        session.save()

        response = self.ajax_post(reverse('edit_invoice', kwargs={'pk': invoice.pk}), data=data)
        assert response.status_code == HTTPStatus.OK

        invoice.refresh_from_db()
        amount = utils.to_decimal(data['total_amount'])

        assert invoice.total_amount == amount
        assert invoice.net_amount == utils.to_decimal(data['net_amount'])
        assert invoice.vat_amount == utils.to_decimal(data['vat_amount'])
        assert invoice.open_amount == utils.to_decimal('0.00')
        assert invoice.estimate_number == data['estimate_number']
        assert invoice.po_number == data['po_number']
        assert invoice.vat_number == data['vat_number']
        assert invoice.invoice_items.count() == 4

        # Customer ledger
        customer_ledgers = CustomerLedger.objects.all()

        assert len(customer_ledgers) == 2
        assert customer_ledgers[0].object_id == invoice.id
        assert customer_ledgers[0].amount == amount
        assert customer_ledgers[0].status == LedgerStatus.PENDING

        invoice_items = InvoiceItem.objects.all()
        assert len(invoice_items) == 4

        navara_item.refresh_from_db()
        assert navara_item.account == account
        assert navara_item.description == data['description_1']
        assert navara_item.quantity == utils.to_decimal(data['quantity_1'])
        assert navara_item.price == utils.to_decimal(data['price_1'])
        assert navara_item.discount == utils.to_decimal(data['discount_percentage_1'])
        assert navara_item.net_price == utils.to_decimal(data['sub_total_1'])
        assert navara_item.inventory_id is None
        assert navara_item.invoice_id == invoice.pk
        assert navara_item.unit_id is None
        assert navara_item.customer_discount == utils.to_decimal('0')
        assert navara_item.total_amount == utils.to_decimal(data['total_price_including_1'])
        assert navara_item.vat == vat_code.percentage
        assert navara_item.vat_code_id == vat_code.pk
        assert navara_item.status == InvoiceItemStatus.PENDING
        assert navara_item.available_quantity == utils.to_decimal(data['quantity_1'])
        assert navara_item.item_link is None
        assert navara_item.item_type == InvoiceItemType.GENERAL
        assert navara_item.vat_amount == utils.to_decimal(data['vat_amount_1'])

        kvd_bill_item.refresh_from_db()
        assert kvd_bill_item.account == account
        assert kvd_bill_item.description == data['description_4']
        assert kvd_bill_item.quantity == utils.to_decimal(data['quantity_4'])
        assert kvd_bill_item.price == utils.to_decimal(data['price_4'])
        assert kvd_bill_item.discount == utils.to_decimal(data['discount_percentage_4'])
        assert kvd_bill_item.net_price == utils.to_decimal(data['sub_total_4'])
        assert kvd_bill_item.inventory_id is None
        assert kvd_bill_item.invoice_id == invoice.pk
        assert kvd_bill_item.unit_id is None
        assert kvd_bill_item.customer_discount == utils.to_decimal('0')
        assert kvd_bill_item.total_amount == utils.to_decimal(data['total_price_including_4'])
        assert kvd_bill_item.vat == vat_code.percentage
        assert kvd_bill_item.vat_code_id == vat_code.pk
        assert kvd_bill_item.status == InvoiceItemStatus.PENDING
        assert kvd_bill_item.available_quantity == utils.to_decimal(data['quantity_4'])
        assert kvd_bill_item.item_link is None
        assert kvd_bill_item.item_type == InvoiceItemType.GENERAL
        assert kvd_bill_item.vat_amount == utils.to_decimal(data['vat_amount_4'])

        # TODO - Check for any journals or ledgers
        assert JournalLine.objects.count() == 0

    # def test_credit_tax_invoice(self):
    #     tax_invoice = TaxInvoiceFactory(branch=self.branch)
    #
    #     for i in range(0, 3):
    #         inventory_line = InventoryItemFactory(invoice=tax_invoice)
