from dateutil.relativedelta import relativedelta

from django.urls import reverse
from django.utils.timezone import now

from docuflow.apps.certificate.models import Certificate
from tests.base import AuthenticatedTestCase
from tests import factories


class ImportantDocumentTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(ImportantDocumentTestCase, self).setUp()

    def test_list_important_documents(self):
        factories.CertificateFactory(owner=self.profile, company=self.company)

        response = self.client.get(reverse('certificates'))
        assert response.status_code == 200

    def test_create_important_document(self):
        assert Certificate.objects.count() == 0
        document_type = factories.DocumentInvoiceTypeFactory(company=self.company)
        supplier = factories.SupplierFactory(company=self.company)
        employee = factories.EmployeeFactory(company=self.company)

        data = {
            'document_type': document_type.pk,
            'supplier': supplier.pk,
            'supplier_name': 'Supp-Name',
            'document_number': 1,
            'reference_1': 'Pfeze',
            'reference_2': 'Cora',
            'employee': employee.pk,
            'document_date': now().date(),
            'expiry_date': now().date() + relativedelta(months=5),
            'reminder': now().date() + relativedelta(days=30),
            'reminder_days': 30,
            'remind_role': self.role.pk,
            'comment': 'Text',
            'profiles': [self.profile.pk]
        }

        response = self.ajax_post(reverse('create_certificate'), data=data)
        assert response.status_code == 200

    def test_view_certificate_detail(self):
        certificate = factories.CertificateFactory(owner=self.profile, company=self.company)

        response = self.client.get(reverse('certificate_detail', kwargs={'pk': certificate.pk}))
        assert response.status_code == 200

    def test_certificate_show(self):
        certificate = factories.CertificateFactory(owner=self.profile, company=self.company)
        response = self.client.get(reverse('certificate_show', kwargs={'pk': certificate.pk}))
        assert response.status_code == 200

    def test_update_certificate(self):
        certificate = factories.CertificateFactory(owner=self.profile, company=self.company)

        document_type = factories.DocumentInvoiceTypeFactory(company=self.company)
        supplier = factories.SupplierFactory(company=self.company)
        employee = factories.EmployeeFactory(company=self.company)

        data = {
            'document_type': document_type.pk,
            'supplier': supplier.pk,
            'supplier_name': 'Supp-Name',
            'document_number': 1,
            'reference_1': 'Pfeze',
            'reference_2': 'Cora',
            'employee': employee.pk,
            'document_date': now().date(),
            'expiry_date': now().date() + relativedelta(months=5),
            'reminder': now().date() + relativedelta(days=30),
            'reminder_days': 30,
            'remind_role': self.role.pk,
            'comment': 'Text',
            'profiles': [self.profile.pk]
        }

        response = self.ajax_post(reverse('update_certificate', kwargs={'pk': certificate.pk}), data=data)
        assert response.status_code == 200

    def test_delete_certificate(self):
        certificate = factories.CertificateFactory(owner=self.profile, company=self.company)

        response = self.client.get(reverse('delete_certificate', kwargs={'pk': certificate.pk}), follow=True)

        assert response.status_code == 200
        assert Certificate.objects.count() == 0

    # def test_upload_certificate_document(self):
    #     data = {
    #         'image': 'image.pdf'
    #     }
    #
    #     response = self.ajax_post(reverse('upload_certificate_document'), data=data)
    #
    #     assert response.status_code == 200
