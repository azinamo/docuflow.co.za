from django.urls import reverse

from docuflow.apps.customer.models import Ledger as CustomerLedger, AgeAnalysis as CustomerAgeAnalysis
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.customer.services import age_customer_journal
from docuflow.apps.supplier.models import Ledger as SupplierLedger, AgeAnalysis as SupplierAgeAnalysis
from docuflow.apps.journals.models import Journal, JournalLine, JournalLineLog
from docuflow.apps.supplier.services import add_journal_to_age_analysis
from tests.base import AuthenticatedTestCase
from tests import factories


class TestJournalsView(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()

    def test_journals_view(self):
        response = self.client.get(reverse('journals_index'))

        assert response.status_code == 200

    def test_can_journals_api(self):
        factories.JournalFactory.create_batch(3, company=self.company)

        response = self.client.get(reverse('journals'))

        assert response.status_code == 200

    def test_create_general_journal(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        customer = factories.CustomerFactory(company=self.company)
        supplier = factories.SupplierFactory(company=self.company)
        account = factories.AccountFactory(company=self.company, name='Other')

        data = {
            # 'journal_number': 'JNL23063',
            'default_year': self.year.pk,
            'period': period.pk,
            'date': period.from_date,
            'journal_text': 'Just a text',
            'ledger_1': 0,
            'account_1': account.pk,
            'suffix_1': '',
            'debit_1': 3000,
            'credit_1': '',
            'ledger_2': 1,
            'supplier_2': supplier.pk,
            'debit_2': '',
            'credit_2': 1500,
            'ledger_3': 2,
            'customer_3': customer.pk,
            'debit_3': '',
            'credit_3': 1500,
            'total_debit': 3000.00,
            'total_credit': 3000.00,
            'difference': 0.00,
        }
        assert Journal.objects.count() == 0
        assert JournalLine.objects.count() == 0

        assert CustomerAgeAnalysis.objects.count() == 0
        assert CustomerLedger.objects.count() == 0

        assert SupplierAgeAnalysis.objects.count() == 0
        assert SupplierLedger.objects.count() == 0

        response = self.ajax_post(reverse('create_generate_journal'), data=data)

        self.assertEqual(response.status_code, 200)
        assert Journal.objects.count() == 1
        journal = Journal.objects.first()
        assert journal.journal_text == data['journal_text']
        # assert journal.journal_number == data['journal_number']
        assert journal.period.pk == data['period']
        assert journal.year.pk == data['default_year']

        assert JournalLine.objects.count() == 3
        customer_journal_line = JournalLine.objects.filter(account_id=self.customer_account.id).first()
        supplier_journal_line = JournalLine.objects.filter(account_id=self.supplier_account.id).first()

        # Customer
        customer_ledger = CustomerLedger.objects.first()
        assert CustomerLedger.objects.count() == 1
        assert customer_ledger.object_id == customer_journal_line.pk
        assert customer_ledger.customer_id == customer.pk
        assert customer_ledger.status == LedgerStatus.COMPLETE
        assert customer_ledger.journal_line == customer_journal_line
        assert customer_ledger.balance == customer_journal_line.credit

        assert CustomerAgeAnalysis.objects.count() == 1
        customer_age = CustomerAgeAnalysis.objects.first()
        assert customer_age.balance == customer_journal_line.credit
        assert customer_age.current == customer_journal_line.credit

        # Supplier
        supplier_ledger = SupplierLedger.objects.first()
        assert supplier_ledger.supplier_id == supplier.pk
        assert supplier_ledger.object_id == supplier_journal_line.pk
        assert supplier_ledger.status == SupplierLedger.COMPLETED
        assert supplier_ledger.journal_line == supplier_journal_line
        assert supplier_ledger.balance == supplier_journal_line.credit

        assert SupplierAgeAnalysis.objects.count() == 1
        supplier_age = SupplierAgeAnalysis.objects.first()
        assert supplier_age.balance == supplier_journal_line.credit
        assert supplier_age.current == supplier_journal_line.credit

    def test_delete_general_journal_lines(self):
        year = factories.YearFactory(company=self.company)
        period = factories.PeriodFactory(company=self.company, period_year=year)
        customer = factories.CustomerFactory(company=self.company)
        supplier = factories.SupplierFactory(company=self.company)

        journal = factories.JournalFactory(period=period, company=self.company)
        account_line = factories.JournalLineFactory(
            journal=journal, account=factories.AccountFactory(company=self.company, name='Other', code='0001'),
            credit=6000, debit=0
        )

        supplier_line = factories.SupplierJournalLineFactory(
            journal=journal, credit=0, debit=3000, content_object=supplier, account=factories.SupplierAccountFactory(
                company=self.company, name='Supplier', code='0002')
        )
        add_journal_to_age_analysis(journal_line=supplier_line)

        customer_line = factories.CustomerJournalLineFactory(
            journal=journal, credit=0, debit=3000, content_object=customer, account=factories.CustomerAccountFactory(
                company=self.company, name='Customer', code='0003')
        )
        age_customer_journal(journal_line=customer_line)

        balancing_account = factories.AccountFactory(company=self.company, name='Replacement', code='0004')

        assert SupplierAgeAnalysis.objects.count() == 1
        assert CustomerAgeAnalysis.objects.count() == 1
        data = {
            'period': journal.period.pk,
            'year': journal.period.period_year.pk,
            'journal_text': journal.journal_text,
            'journal_number': journal.journal_number,
            'date': journal.period.from_date,
            f'delete_{supplier_line.pk}': supplier_line.pk,
            f'delete_{customer_line.pk}': customer_line.pk,
            'ledger_4': 0,
            'account_4': balancing_account.pk,
            'suffix_4': '',
            'debit_4': 6000,
            'credit_4': '',
        }
        response = self.ajax_post(reverse('journal_lines', args=(journal.pk,)), data=data)
        assert response.status_code == 200

        assert JournalLineLog.objects.count() == 2

        # Customer age
        assert CustomerAgeAnalysis.objects.count() == 1
        customer_age = CustomerAgeAnalysis.objects.first()
        assert customer_age.balance == 0
        assert customer_age.current == 0

        # Supplier age
        assert SupplierAgeAnalysis.objects.count() == 1
        supplier_age = SupplierAgeAnalysis.objects.first()
        assert supplier_age.balance == 0
        assert supplier_age.current == 0
