from http import HTTPStatus

from django.urls import reverse

from docuflow.apps.journals.models import JournalLine, Journal
from docuflow.apps.vat.models import VatReport
from docuflow.apps.common.enums import Module
from tests.base import AuthenticatedTestCase
from tests import factories


class TestVatReportView(AuthenticatedTestCase):

    def test_vat_report_landing_page(self):
        vat_account = factories.AccountFactory(company=self.company, code='2410', name='Output 25%')
        input_vat_account = factories.AccountFactory(company=self.company, code='1470', name='Input 25%')
        export_vat_account = factories.AccountFactory(company=self.company, code='170', name='Export 25%')
        output_vat = factories.VatCodeFactory(company=self.company, label='Output Vat', code=25, percentage=25,
                                              account=vat_account)
        input_vat = factories.VatCodeFactory(company=self.company, label='Input Vat', code=25, percentage=25,
                                             account=input_vat_account)
        export_vat = factories.VatCodeFactory(company=self.company, label='Export Vat', code=25, percentage=25,
                                              account=export_vat_account)

        jan_period = factories.PeriodFactory(period=1, period_year=self.year)
        jan_vat_report = factories.VatReportFactory(
            company=self.company, period=jan_period, created_by=self.profile, year=self.year, category_period=jan_period.period)
        jan_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jan_period, credit=1696174.50,
            accounting_date=jan_period.from_date, account=vat_account, debit=0, vat_code=output_vat,
            journal__journal_number=f'{jan_period.name}-OUTPUT-000{jan_period.period}', vat_report=jan_vat_report
        )
        jan_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jan_period, debit=709964.51,
            accounting_date=jan_period.to_date, account=input_vat_account, credit=0, vat_code=input_vat,
            journal__journal_number=f'{jan_period.name}-INPUT-000{jan_period.period}', vat_report=jan_vat_report
        )

        # February
        feb_period = factories.PeriodFactory(period=2, period_year=self.year)
        feb_vat_report = factories.VatReportFactory(
            company=self.company, period=feb_period, created_by=self.profile, year=self.year,
            category_period=feb_period.period
        )
        feb_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=feb_period, credit=3109728.85,
            accounting_date=feb_period.from_date, account=vat_account, debit=0, vat_code=output_vat,
            journal__journal_number=f'{feb_period.name}-OUTPUT-000{feb_period.period}', vat_report=feb_vat_report
        )
        feb_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=feb_period, debit=1535259.00,
            accounting_date=feb_period.to_date, account=input_vat_account, credit=0, vat_code=input_vat,
            journal__journal_number=f'{feb_period.name}-INPUT-000{feb_period.period}', vat_report=feb_vat_report
        )

        # March
        mar_period = factories.PeriodFactory(period=3, period_year=self.year)
        mar_vat_report = factories.VatReportFactory(
            company=self.company, period=mar_period, created_by=self.profile, year=self.year,
            category_period=mar_period.period
        )
        mar_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=mar_period, credit=2938421.00,
            accounting_date=mar_period.from_date, account=vat_account, debit=0, vat_code=output_vat,
            journal__journal_number=f'{mar_period.name}-OUTPUT-000{mar_period.period}', vat_report=mar_vat_report
        )
        mar_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=mar_period, debit=1212504.00,
            accounting_date=mar_period.to_date, account=input_vat_account, credit=0, vat_code=input_vat,
            journal__journal_number=f'{mar_period.name}-INPUT-000{mar_period.period}', vat_report=mar_vat_report
        )

        # April
        apr_period = factories.PeriodFactory(period=4, period_year=self.year)
        apr_vat_report = factories.VatReportFactory(
            company=self.company, period=apr_period, created_by=self.profile, year=self.year,
            category_period=apr_period.period
        )
        apr_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=apr_period, credit=1659710.00,
            debit=0, accounting_date=apr_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{apr_period.name}-OUTPUT-000{apr_period.period}', vat_report=apr_vat_report
        )
        apr_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=apr_period, debit=2966827.80,
            credit=0, accounting_date=apr_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{apr_period.name}-INPUT-000{apr_period.period}', vat_report=apr_vat_report
        )
        apr_export_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=apr_period, credit=3502.00, debit=0,
            accounting_date=apr_period.to_date, account=export_vat_account, vat_code=export_vat,
            journal__journal_number=f'{apr_period.name}-EXPORT-000{apr_period.period}', vat_report=apr_vat_report

        )

        # May
        may_period = factories.PeriodFactory(period=5, period_year=self.year)
        may_vat_report = factories.VatReportFactory(
            company=self.company, period=may_period, created_by=self.profile, year=self.year,
            category_period=may_period.period
        )
        may_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=may_period, credit=1815468.00,
            debit=0, accounting_date=may_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{may_period.name}-OUTPUT-000{may_period.period}', vat_report=may_vat_report
        )
        may_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=may_period, debit=473456.20,
            credit=0, accounting_date=may_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{may_period.name}-INPUT-000{may_period.period}', vat_report=may_vat_report
        )
        may_export_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=may_period, credit=3560.00,
            debit=0, accounting_date=may_period.to_date, account=export_vat_account, vat_code=export_vat,
            journal__journal_number=f'{may_period.name}-EXPORT-000{may_period.period}', vat_report=may_vat_report
        )

        # June
        jun_period = factories.PeriodFactory(period=6, period_year=self.year)
        jun_vat_report = factories.VatReportFactory(
            company=self.company, period=jun_period, created_by=self.profile, year=self.year,
            category_period=jun_period.period
        )
        jun_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jun_period, credit=1475982.95,
            debit=0, accounting_date=jun_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{jun_period.name}-OUTPUT-000{jun_period.period}', vat_report=jun_vat_report
        )
        jun_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jun_period, debit=1560558.00,
            credit=0, accounting_date=jun_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{jun_period.name}-INPUT-000{jun_period.period}', vat_report=jun_vat_report
        )

        # July
        jul_period = factories.PeriodFactory(period=7, period_year=self.year)
        jul_vat_report = factories.VatReportFactory(
            company=self.company, period=jul_period, created_by=self.profile, year=self.year,
            category_period=jul_period.period
        )
        jul_output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jul_period, credit=1320663.85,
            debit=0, accounting_date=jul_period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{jul_period.name}-OUTPUT-000{jul_period.period}', vat_report=jul_vat_report
        )
        jul_input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jul_period, debit=495295.00,
            credit=0, accounting_date=jul_period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{jul_period.name}-INPUT-000{jul_period.period}', vat_report=jul_vat_report
        )
        jul_export_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=jul_period, credit=7142.00,
            debit=0, accounting_date=jul_period.to_date, account=export_vat_account, vat_code=export_vat,
            journal__journal_number=f'{jul_period.name}-EXPORT-000{jul_period.period}', vat_report=jul_vat_report
        )

        response = self.client.get(reverse('vat_report'))

        assert response.status_code == HTTPStatus.OK

        content = response.content.decode()

    def test_run_vat_report(self):

        jan_period = factories.PeriodFactory(period=1, period_year=self.year)

        response = self.client.get(reverse('run_vat_report', kwargs={'period': jan_period.pk}))

        assert response.status_code == HTTPStatus.OK

    def test_view_vat_report(self):
        vat_account = factories.AccountFactory(company=self.company, code='2410', name='Output 25%')
        input_vat_account = factories.AccountFactory(company=self.company, code='1470', name='Input 25%')
        export_vat_account = factories.AccountFactory(company=self.company, code='170', name='Export 25%')
        output_vat = factories.VatCodeFactory(company=self.company, label='Output Vat', code=25, percentage=25,
                                              account=vat_account)
        input_vat = factories.VatCodeFactory(company=self.company, label='Input Vat', code=25, percentage=25,
                                             account=input_vat_account)
        export_vat = factories.VatCodeFactory(company=self.company, label='Export Vat', code=25, percentage=25,
                                              account=export_vat_account)

        period = factories.PeriodFactory(period=6, period_year=self.year)
        vat_report = factories.VatReportFactory(
            company=self.company, period=period, created_by=self.profile, year=self.year,
            category_period=period.period
        )
        output_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=period, credit=1320663.85,
            debit=0, accounting_date=period.from_date, account=vat_account, vat_code=output_vat,
            journal__journal_number=f'{period.name}-OUTPUT-000{period.period}', vat_report=vat_report
        )
        input_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=period, debit=495295.00,
            credit=0, accounting_date=period.to_date, account=input_vat_account, vat_code=input_vat,
            journal__journal_number=f'{period.name}-INPUT-000{period.period}', vat_report=vat_report
        )
        export_journal = factories.JournalLineFactory(
            journal__company=self.company, journal__year=self.year, journal__period=period, credit=7142.00,
            debit=0, accounting_date=period.to_date, account=export_vat_account, vat_code=export_vat,
            journal__journal_number=f'{period.name}-EXPORT-000{period.period}', vat_report=vat_report
        )

        response = self.client.get(reverse('view_vat_report', kwargs={'report_id': vat_report.pk}))

        assert response.status_code == HTTPStatus.OK

        content = response.content.decode()

        assert (str(output_journal.journal) in content) is True

        assert (str(input_journal.journal) in content) is True

        assert (str(export_journal.journal) in content) is True

    def test_create_vat_report_modal(self):

        response = self.client.get(reverse('create_vat_report'))

        assert response.status_code == HTTPStatus.OK

        content = response.content.decode()

        assert ('Create Vat Report' in content) is True

    # def test_save_vat_report(self):
    #     vat_account = factories.AccountFactory(company=self.company, code='2410', name='Output 25%')
    #     input_vat_account = factories.AccountFactory(company=self.company, code='1470', name='Input 25%')
    #
    #     output_vat = factories.VatCodeFactory(company=self.company, label='Output Vat', code=25, percentage=25,
    #                                           account=vat_account)
    #     input_vat = factories.VatCodeFactory(company=self.company, label='Input Vat', code=25, percentage=25,
    #                                          account=input_vat_account)
    #     period = factories.PeriodFactory(period=1, period_year=self.year)
    #
    #     output_journal = factories.JournalLineFactory(
    #         journal__company=self.company, journal__year=self.year, journal__period=period, credit=1696174.50,
    #         accounting_date=period.from_date, account=vat_account, debit=0, vat_code=output_vat,
    #         journal__journal_number=f'{period.name}-OUTPUT-000{period.period}', journal__module=Module.PURCHASE.value,
    #         journal__date=period.from_date
    #     )
    #     input_journal = factories.JournalLineFactory(
    #         journal__company=self.company, journal__year=self.year, journal__period=period, debit=709964.51,
    #         accounting_date=period.to_date, account=input_vat_account, credit=0, vat_code=input_vat,
    #         journal__journal_number=f'{period.name}-INPUT-000{period.period}', journal__module=Module.PURCHASE.value,
    #         journal__date=period.from_date
    #     )
    #
    #     assert JournalLine.objects.count() == 2
    #     assert Journal.objects.count() == 2
    #     assert VatReport.objects.count() == 0
    #     assert input_journal.vat_report is None
    #     assert output_journal.vat_report is None
    #
    #     response = self.client.get(reverse('save_vat_report', kwargs={'period': period.pk}))
    #
    #     assert response.status_code == HTTPStatus.OK
    #
    #     vat_reports = VatReport.objects.all()
    #
    #     output_journal.refresh_from_db()
    #     input_journal.refresh_from_db()
    #
    #     assert len(vat_reports) == 1
    #     assert JournalLine.objects.count() == 5
    #     assert Journal.objects.count() == 3
    #
    #     assert input_journal.vat_report == vat_reports[0]
    #     assert output_journal.vat_report == vat_reports[0]
    #
    #     output_vat_lines = JournalLine.objects.filter(account=output_vat.account).all()
    #     assert len(output_vat_lines) == 2
    #     assert output_vat_lines[1].debit == output_journal.credit
    #
    #     input_vat_lines = JournalLine.objects.filter(account=input_vat.account).all()
    #     assert len(input_vat_lines) == 2
    #     assert input_vat_lines[1].credit == input_journal.debit
    #
    #     vat_amount = output_journal.credit - input_journal.debit
    #     input_vat_lines = JournalLine.objects.filter(account=self.vat_payable).all()
    #     assert input_vat_lines[0].credit == vat_amount
    #
    #     journals = Journal.objects.all()
    #     month_number = str(period.from_date.strftime('%m')).rjust(2, '0')
    #     assert journals[0].journal_text == f"Journals Vat Submissions {vat_reports[0].year}{month_number}"
