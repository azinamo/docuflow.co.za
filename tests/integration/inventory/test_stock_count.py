from django.urls import reverse
from django.utils.timezone import now
from decimal import Decimal

from docuflow.apps.inventory.models import StockCount
from docuflow.apps.inventory.enums import StockCountType, StockCountStatus
from tests import factories
from tests.base import AuthenticatedTestCase


def serialize_stock_count(stock_count: StockCount):
    return {
        'id': stock_count.id,
        'captured_by': stock_count.captured_by,
        'date': stock_count.date,
        'branch': stock_count.branch_id,
        'count_type': stock_count.count_type,
        'count': stock_count.count,
        'status': stock_count.status
    }


class TestStockCountViews(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_can_list_stock_count(self):
        stock_count_1 = factories.StockCountFactory(branch=self.branch)
        stock_count_2 = factories.StockCountFactory(branch=self.branch)
        stock_count_3 = factories.StockCountFactory(branch=self.branch)
        stock_count_4 = factories.StockCountFactory(branch=self.branch)

        stock_count_5 = factories.StockCountFactory(branch=factories.BranchFactory(company=factories.CompanyFactory()))

        response = self.client.get(reverse('stock_count_index'))

        assert response.status_code == 200
        assert str(stock_count_1) in response.content.decode()
        assert str(stock_count_2) in response.content.decode()
        assert str(stock_count_3) in response.content.decode()
        assert str(stock_count_4) in response.content.decode()
        assert str(stock_count_5) not in response.content.decode()

    def test_create_all_stock_count(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year)
        inventories = factories.BranchInventoryFactory.create_batch(30, branch=self.branch)
        data = {
            'date': now().date().replace(now().date().year, 1, 1),
            'period': period,
            'branch': self.branch.id,
            'captured_by': 'Mike',
            'count_type': StockCountType.ALL_STOCK.value
        }

        assert StockCount.objects.filter(branch=self.branch).count() == 0

        response = self.client.post(reverse('create_stock_count'), data=data, follow=True,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})

        stock_count = StockCount.objects.filter(branch=self.branch).first()

        assert response.status_code == 200
        assert StockCount.objects.filter(branch=self.branch).count() == 1
        assert stock_count.date == data['date']
        assert stock_count.captured_by == data['captured_by']
        assert stock_count.count_type == data['count_type']
        assert stock_count.inventory_items.count() == len(inventories)

    def test_create_random_stock_count(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year)
        inventories = factories.BranchInventoryFactory.create_batch(30, branch=self.branch)
        count = 20
        data = {
            'date': now().date().replace(now().date().year, 1, 1),
            'period': period,
            'branch': self.branch.id,
            'captured_by': 'Mike',
            'count_type': StockCountType.RANDOM.value,
            'count': count
        }

        assert StockCount.objects.filter(branch=self.branch).count() == 0

        response = self.client.post(reverse('create_stock_count'), data=data, follow=True,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})

        stock_count = StockCount.objects.filter(branch=self.branch).first()

        assert response.status_code == 200
        assert StockCount.objects.filter(branch=self.branch).count() == 1
        assert stock_count.date == data['date']
        assert stock_count.captured_by == data['captured_by']
        assert stock_count.count_type == data['count_type']
        assert stock_count.inventory_items.count() == count

    def test_capture_stock(self):
        stock_count = factories.StockCountFactory(branch=self.branch)
        inventories = factories.BranchInventoryFactory.create_batch(2, branch=self.branch)
        data = {}
        for inventory in inventories:
            stock_count_inventory = factories.InventoryStockCountFactory(stock_count=stock_count, inventory=inventory)
            assert stock_count_inventory.original_quantity == 0
            data[f'quantity_counted_{stock_count_inventory.id}'] = 10

        response = self.client.post(reverse('save_stock_capture_count', kwargs={'pk': stock_count.id}), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})

        stock_count.refresh_from_db()

        assert response.status_code == 200
        for stock_count_inventory in stock_count.inventory_items.all():
            assert stock_count_inventory.quantity == Decimal(10)

        assert stock_count.status == StockCountStatus.COUNTED
