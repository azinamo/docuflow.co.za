from http import HTTPStatus


from django.urls import reverse

from docuflow.apps.inventory.models import Inventory, BranchInventory
from docuflow.apps.inventory.enums import SkuType
from tests import factories
from tests.base import AuthenticatedTestCase


class TestInventoryViews(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_can_list_inventory(self):
        inventory_1 = factories.BranchInventoryFactory(branch=self.branch, inventory=factories.InventoryFactory(company=self.company))
        inventory_2 = factories.BranchInventoryFactory(branch=self.branch, inventory=factories.InventoryFactory(company=self.company))
        inventory_3 = factories.BranchInventoryFactory(branch=self.branch, inventory=factories.InventoryFactory(company=self.company))
        inventory_4 = factories.BranchInventoryFactory(branch=self.branch, inventory=factories.InventoryFactory(company=self.company))

        response = self.client.get(reverse('inventories-list'))

        assert response.status_code == HTTPStatus.OK

    def test_create_inventory(self):
        data = {
            'code': 'MM0101',
            'description': 'Description of inventory'
        }

        assert Inventory.objects.count() == 0
        assert BranchInventory.objects.count() == 0

        response = self.ajax_post(reverse('create_inventory_item'), data=data)

        assert response.status_code == HTTPStatus.OK

        inventories = Inventory.objects.all()

        assert len(inventories) == 1
        assert inventories[0].code == data['code']
        assert inventories[0].description == data['description']
        assert inventories[0].description == data['description']
        assert inventories[0].company == self.company

        branch_inventories = BranchInventory.all_inventory.all()
        assert len(branch_inventories) == 1
        assert branch_inventories[0].inventory == inventories[0]
        assert branch_inventories[0].branch == self.branch

    def test_create_inventory_and_copy_to_branches(self):
        head_office_branch = factories.BranchFactory(label='Head Office', company=self.company)
        admin_branch = factories.BranchFactory(label='Admin', company=self.company)

        data = {
            'code': 'MM0101',
            'description': 'Description of inventory',
            'is_copied': '1'
        }

        assert Inventory.objects.count() == 0
        assert BranchInventory.objects.count() == 0

        response = self.ajax_post(reverse('create_inventory_item'), data=data)

        assert response.status_code == HTTPStatus.OK

        inventories = Inventory.objects.all()

        assert len(inventories) == 1
        assert inventories[0].code == data['code']
        assert inventories[0].description == data['description']
        assert inventories[0].description == data['description']
        assert inventories[0].company == self.company

        branch_inventories = BranchInventory.all_inventory.all()
        assert len(branch_inventories) == 3
        assert branch_inventories[0].inventory == inventories[0]
        assert branch_inventories[0].branch == self.branch

        assert branch_inventories[1].inventory == inventories[0]
        assert branch_inventories[1].branch == admin_branch
        assert branch_inventories[1].parent_branch == self.branch

        assert branch_inventories[2].inventory == inventories[0]
        assert branch_inventories[2].branch == head_office_branch
        assert branch_inventories[2].parent_branch == self.branch

    def test_create_branch_inventory_from_inventory_validations(self):
        inventory = factories.InventoryFactory(company=self.company, code='MMS', description='Baking stuff')
        data = {
            'sku_type': '',
            'description': '',
            'bar_code': '',
            'unit': '',
            'supplier_code': '',
            'gl_account': '',
            'l': '',
            'b': '',
            'h': '',
            'cost_price': 0,
            'vat_code': '',
            'delivery_fee': 0,
            'environment_fee': '',
            'supplier': '',
            'sale_unit': '',
            'sales_account': '',
            'sale_quantity_in_package': '',
            'selling_price': 0,
            'sale_vat_code': '',
            'sale_delivery_fee': 0,
            'sale_environment_fee': '',
            'sold_as_type': '',
            'gross_weight': '',
            'weight': '',
            'service_cost': '',
            'service_account': '',
            'cost_of_sales_account': '',
            'stock_adjustment_account': '',
            'stock_unit': '',
            'price_type': 1,
            'critical_stock_level': '',
            'lowest_stock_level': '',
            'max_stock_level': '',
            'level_to_order_stock': '',
            'min_reorder': '',
            'max_reorder': '',
            'store_area': '',
            'rack': '',
            'bin_location': '',
            'group_level_item': [],
            'measure': ''
        }

        resp = self.ajax_post(reverse('create_branch_inventory', kwargs={'inventory_id': inventory.id}), data=data)

        assert resp.status_code == HTTPStatus.BAD_REQUEST

        assert resp.data['errors'] == {'sku_type': ['This field is required.', 'Sku type unit is required'],
                                       'sold_as_type': ['This field is required.'],
                                       'group_level_items': ['This field is required.'],
                                       'unit': ['Inventory unit is required', 'Unit is required'],
                                       'cost_price': ['Default cost is required'],
                                       }

    def test_create_sku_type_branch_inventory_from_inventory_validations(self):
        inventory = factories.InventoryFactory(company=self.company, code='MMS', description='Baking stuff')
        data = {
            'sku_type': SkuType.SKU.value,
            'description': '',
            'bar_code': '',
            'unit': '',
            'supplier_code': '',
            'gl_account': '',
            'l': '',
            'b': '',
            'h': '',
            'cost_price': 0,
            'vat_code': '',
            'delivery_fee': 0,
            'environment_fee': '',
            'supplier': '',
            'sale_unit': '',
            'sales_account': '',
            'sale_quantity_in_package': '',
            'selling_price': 0,
            'sale_vat_code': '',
            'sale_delivery_fee': 0,
            'sale_environment_fee': '',
            'sold_as_type': '',
            'gross_weight': '',
            'weight': '',
            'service_cost': '',
            'service_account': '',
            'cost_of_sales_account': '',
            'stock_adjustment_account': '',
            'stock_unit': '',
            'price_type': 1,
            'critical_stock_level': '',
            'lowest_stock_level': '',
            'max_stock_level': '',
            'level_to_order_stock': '',
            'min_reorder': '',
            'max_reorder': '',
            'store_area': '',
            'rack': '',
            'bin_location': '',
            'group_level_item': [],
            'measure': ''
        }

        resp = self.ajax_post(reverse('create_branch_inventory', kwargs={'inventory_id': inventory.id}), data=data)

        assert resp.status_code == HTTPStatus.BAD_REQUEST

        assert resp.data['errors'] == {'sold_as_type': ['This field is required.'],
                                       'group_level_items': ['This field is required.'],
                                       'unit': ['Inventory unit is required', 'Unit is required'],
                                       'cost_price': ['Default cost is required'],
                                       'sales_account': ['Sales account is required'],
                                       'cost_of_sales_account': ['Cost of sales account is required'],
                                       'gl_account': ['Inventory GL Account Number is required']
                                       }

    def test_create_branch_inventory_from_inventory(self):
        inventory = factories.InventoryFactory(company=self.company, code='MMS', description='Baking stuff')
        mix = factories.GroupLevelItemFactory(name='Tools')
        tools = factories.GroupLevelItemFactory(name='Tools')
        store_supply = factories.GroupLevelItemFactory(name='Store Supplies')

        unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company))
        sale_unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company, name='Each'),
                                          name='Each', unit='each')


        sales_account = factories.AccountFactory(code='1000/0002', name='Manufacturing', company=self.company)
        cost_of_sales_account = factories.AccountFactory(code='2001/0001', name='Cost of Prod', company=self.company)
        gl_account = factories.AccountFactory(code='2100/0003', name='Inv Adjustments', company=self.company)

        data = {
            'sku_type': SkuType.SKU.value,
            'description': 'Stuff',
            'bar_code': '',
            'unit': unit.pk,
            'supplier_code': '',
            'gl_account': gl_account.pk,
            'l': '',
            'b': '',
            'h': '',
            'cost_price': 0.20,
            'vat_code': '',
            'delivery_fee': 0,
            'environment_fee': '',
            'supplier': '',
            'sale_unit': sale_unit.pk,
            'sales_account': sales_account.pk,
            'sale_quantity_in_package': '',
            'selling_price': 0,
            'sale_vat_code': '',
            'sale_delivery_fee': 0,
            'sale_environment_fee': '',
            'sold_as_type': 2,
            'gross_weight': '',
            'weight': '',
            'service_cost': '',
            'service_account': '',
            'cost_of_sales_account': cost_of_sales_account.pk,
            'stock_adjustment_account': '',
            'stock_unit': sale_unit.pk,
            'price_type': 1,
            'critical_stock_level': '',
            'lowest_stock_level': '',
            'max_stock_level': '',
            'level_to_order_stock': '',
            'min_reorder': '',
            'max_reorder': '',
            'store_area': '',
            'rack': '',
            'bin_location': '',
            'group_level_items': [mix.pk, tools.pk, store_supply.pk],
            'measure': ''
        }

        assert BranchInventory.objects.count() == 0

        resp = self.ajax_post(reverse('create_branch_inventory', kwargs={'inventory_id': inventory.id}), data=data)

        assert resp.status_code == HTTPStatus.OK
        assert resp.data['text'] == 'Inventory item saved successfully'

        branch_inventories = BranchInventory.objects.all()

        assert len(branch_inventories) == 1
        assert branch_inventories[0].branch == self.branch
        assert branch_inventories[0].sales_account.pk == data['sales_account']
        assert branch_inventories[0].cost_of_sales_account.pk == data['cost_of_sales_account']
        assert branch_inventories[0].gl_account.pk == data['gl_account']
        assert list(branch_inventories[0].group_level_items.values_list('pk', flat=True)) == data['group_level_items']
