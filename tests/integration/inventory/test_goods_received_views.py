from decimal import Decimal, ROUND_HALF_EVEN

from django.urls import reverse, reverse_lazy
from django.utils.timezone import now

from docuflow.apps.inventory.models import Inventory, InventoryReceived, InventoryReceivedReturned, Received
from docuflow.apps.journals.models import JournalLine
from tests.base import AuthenticatedTestCase
from tests import factories


class TestGoodsReceivedViews(AuthenticatedTestCase):
    list_url = reverse_lazy('inventories_received-list')

    def setUp(self) -> None:
        super().setUp()

    def test_can_list_goods_received(self):
        period = factories.PeriodFactory(period_year=self.year)

        received_1 = factories.ReceivedFactory(branch=self.branch, period=period, profile=self.profile)
        received_2 = factories.ReceivedFactory(branch=self.branch, period=period, profile=self.profile)
        received_3 = factories.ReceivedFactory(branch=self.branch, period=period, profile=self.profile)
        received_4 = factories.ReceivedFactory(branch=self.branch, period=period, profile=self.profile)

        company = factories.CompanyFactory()
        received_5 = Received(branch=factories.BranchFactory(company=company),
                              period=factories.PeriodFactory(period_year=factories.YearFactory(company=company))
                              )

        response = self.client.get(self.list_url)

        assert response.status_code == 200

        assert Received.objects.filter(branch=self.branch).count() == 4

        assert str(received_1) in response.content.decode()
        assert str(received_2) in response.content.decode()
        assert str(received_3) in response.content.decode()
        assert str(received_4) in response.content.decode()

    def test_create_general_inventory_received(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year)
        supplier = factories.SupplierFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        unit = factories.UnitFactory(measure=factories.MeasureFactory(company=self.company))

        inventory_control = factories.InventoryControlAccountFactory(company=self.company)
        received_account = factories.GoodsReceivedAccountFactory(company=self.company)
        variance_account = factories.InventoryReceivedVariationAccountFactory(company=self.company)
        self.company.default_goods_received_account = received_account
        self.company.default_inventory_variance_account = variance_account
        self.company.save()

        inventory = factories.BranchInventoryFactory(branch=self.branch, gl_account=inventory_control)
        data = {
            'date': now().date().replace(now().date().year, 1, 1),
            'period': period.id,
            'branch': self.branch.id,
            'note': 'REC',
            'supplier': supplier.id,
            'inventory_item_0': inventory.id,
            'quantity_received_0': 10,
            'control_quantity_received_0': 10,
            'quantity_ordered_0': 10,
            'unit_unit_0': unit.id,
            'stock_unit_id_0': 1,
            'unit_id_0': unit.id,
            'price_0': 0.87,
            'price_including_0': 1,
            'vat_code_0': vat_code.id,
            'vat_percentage_0': vat_code.percentage,
            'vat_amount_0': 1.31,
            'total_price_excluding_0': 8.70,
            'total_price_including_0': 10.00,
            'total_vat': 1.31,
            'total_amount': 10
        }

        assert Received.objects.filter(branch=self.branch).count() == 0

        response = self.client.post(reverse('create_received'), data=data, follow=True,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        assert response.status_code == 200
        received = Received.objects.all()
        assert len(received) == 1
        assert received[0].supplier_id == data['supplier']
        assert received[0].branch_id == data['branch']
        assert received[0].period_id == data['period']

        inventory.refresh_from_db()
        assert inventory.in_stock == 10
        total_value = Decimal(0.87 * 10).quantize(Decimal('.01'), rounding=ROUND_HALF_EVEN)
        assert inventory.history.total_value == total_value

        gl_journal_line = JournalLine.objects.filter(account=inventory.gl_account).first()
        assert gl_journal_line.debit == total_value

        company_control_line = JournalLine.objects.filter(account=received_account).first()
        assert company_control_line.credit == total_value
        assert received[0].journal_id == company_control_line.journal_id

    def test_no_inventory_items_validation(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year)
        supplier = factories.SupplierFactory(company=self.company)
        variance_account = factories.AccountFactory(company=self.company)
        received_account = factories.AccountFactory(company=self.company)

        self.company.default_goods_received_account = received_account
        self.company.default_inventory_variance_account = variance_account
        self.company.save()

        data = {
            'date': now().date().replace(now().date().year, 1, 1),
            'period': period.id,
            'branch': self.branch.id,
            'note': 'REC',
            'supplier': supplier.id
        }
        assert Received.objects.filter(branch=self.branch).count() == 0

        response = self.client.post(reverse('create_received'), data=data, follow=True,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        assert response.status_code == 400