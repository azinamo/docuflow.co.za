from django.urls import reverse
from django.utils.timezone import now

from docuflow.apps.inventory.models import Manufacturing
from tests.base import AuthenticatedTestCase
from tests import factories
from tests.utils import format_datetime


def serialize_manufacturing(manufacturing: Manufacturing):
    return {
        'id': manufacturing.id,
        'description': manufacturing.description,
        'date': format_datetime(manufacturing.date),
        'created_at': format_datetime(manufacturing.created_at),
        'reference': str(manufacturing),
        'detail_url': f"/{manufacturing.branch.company.slug}{manufacturing.get_absolute_url()}"
    }


class TestManufacturingViews(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_manufacturing_index(self):
        response = self.client.get(reverse('manufacturing_index'))
        assert response.status_code == 200

    def test_can_list_manufacturing(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year)
        manufacturing_1 = factories.ManufacturingFactory(branch=self.branch, period=period, profile=self.profile)
        manufacturing_2 = factories.ManufacturingFactory(branch=self.branch, period=period, profile=self.profile)
        manufacturing_3 = factories.ManufacturingFactory(branch=self.branch, period=period, profile=self.profile)
        manufacturing_4 = factories.ManufacturingFactory(branch=self.branch, period=period, profile=self.profile)

        branch_2 = factories.BranchFactory(company=factories.CompanyFactory(name='New Company'))
        factories.ManufacturingFactory(branch=branch_2, period=period, profile=self.profile)

        response = self.api_call(url=reverse('inventories-manufacturing-list'), method='GET')

        assert response.status_code == 200

        assert response.data['results'] == [
            serialize_manufacturing(manufacturing_4),
            serialize_manufacturing(manufacturing_3),
            serialize_manufacturing(manufacturing_2),
            serialize_manufacturing(manufacturing_1),
        ]
        assert Manufacturing.objects.filter(branch=self.branch).count() == 4
        assert Manufacturing.objects.filter(branch=branch_2).count() == 1

    def test_create_manufacturing(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year)

        factories.InventoryControlAccountFactory(company=self.company)
        received_account = factories.GoodsReceivedAccountFactory(company=self.company)
        variance_account = factories.InventoryReceivedVariationAccountFactory(company=self.company)
        self.company.default_goods_received_account = received_account
        self.company.default_inventory_variance_account = variance_account
        self.company.save()

        inventories = factories.BranchInventoryFactory.create_batch(10, branch=self.branch)
        recipe = factories.RecipeFactory(inventory=inventories[0], profile=self.profile)
        recipe_2 = factories.RecipeFactory(inventory=inventories[1], profile=self.profile)

        data = {
            'date': now().date().replace(now().date().year, 1, 1),
            'period': period,
            'branch': self.branch.id,
            'description': 'Manufacturing of Stock',
            'profile': self.profile,
            'recipe_item_0': recipe.id,
            'quantity_0': 10,
            f'recipe_{recipe.id}_recipe_item_id': 287,
            f'recipe_{recipe.id}_item_total_8628': 75.40,
            f'recipe_{recipe.id}_item_each_price_8628': 1357.00,
            f'recipe_{recipe.id}_item_quantity_8628': 0.0556,
            f'recipe_{recipe.id}_item_total_8636': 26.80,
            f'recipe_{recipe.id}_item_each_price_8636': 208.00,
            f'recipe_{recipe.id}_item_quantity_8636': 0.1289,
            f'recipe_{recipe.id}_item_total_8630': 38.20,
            f'recipe_{recipe.id}_item_each_price_8630': 301.87,
            f'recipe_{recipe.id}_item_quantity_8630': 0.1267,
            f'recipe_{recipe.id}_item_total_8588': 71.30,
            f'recipe_{recipe.id}_item_each_price_8588': 18.95,
            f'recipe_{recipe.id}_item_quantity_8588': 3.7600,
            'recipe_item_1': recipe_2.id,
            'quantity_1': 10,
            f'recipe_{recipe_2.id}_recipe_item_id': 269,
            f'recipe_{recipe_2.id}_item_total_8630': 108.60,
            f'recipe_{recipe_2.id}_item_each_price_8630': 301.87,
            f'recipe_{recipe_2.id}_item_quantity_8630': 0.3597,
            f'recipe_{recipe_2.id}_item_total_8588': 268.90,
            f'recipe_{recipe_2.id}_item_each_price_8588': 18.95,
            f'recipe_{recipe_2.id}_item_quantity_8588': 14.1900,
            f'recipe_{recipe_2.id}_item_total_8628': 214.10,
            f'recipe_{recipe_2.id}_item_each_price_8628': 1357.00,
            f'recipe_{recipe_2.id}_item_quantity_8628': 0.1578,
            f'recipe_{recipe_2.id}_item_total_8636': 76.10,
            f'recipe_{recipe_2.id}_item_each_price_8636': 208.00,
            f'recipe_{recipe_2.id}_item_quantity_8636': 0.3660
        }

        assert Manufacturing.objects.filter(branch=self.branch).count() == 0

        response = self.ajax_post(reverse('create_manufacturing'), data=data)

        manufacturing = Manufacturing.objects.filter(branch=self.branch).first()
        assert response.status_code == 200
        assert Manufacturing.objects.filter(branch=self.branch).count() == 1
        assert manufacturing.date == data['date']
        assert str(manufacturing.period) == str(data['period'].period)
        assert manufacturing.description == data['description']
