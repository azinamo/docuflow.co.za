from django.urls import reverse
from django.utils.timezone import now

from docuflow.apps.invoice.models import Invoice
from docuflow.apps.purchaseorders.models import PurchaseOrder
from tests.base import AuthenticatedTestCase
from tests import factories
from tests import utils


class PurchaseOrderTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(PurchaseOrderTestCase, self).setUp()
        self.create_url = reverse('create_purchaseorder')

    def test_can_view_purchase_orders(self):
        response = self.client.get(path=reverse('purchaseorder_index'))

        assert response.status_code == 200

    def test_can_create_purchase_order(self):
        today = now().date()
        factories.PeriodFactory(period=today.month, period_year=self.year)
        factories.PurchaseOrderInvoiceTypeFactory(company=self.company)
        factories.StatusFactory(po_request=True)
        factories.StatusFactory(po_approved=True)
        factories.FlowStatusFactory()
        factories.FlowStatusFactory(name='Processed', slug='processed')
        factories.FlowStatusFactory(name='Processing', slug='processing')
        supplier = factories.SupplierFactory(company=self.company)
        account = factories.AccountFactory(name='Other PO', company=self.company, code='3400/0001')
        vat_code = factories.VatCodeFactory(
            company=self.company,
            account=factories.VatAccountFactory(company=self.company)
        )
        inventory = factories.BranchInventoryFactory(description='Diamond', branch=self.branch)

        data = {
            'supplier': supplier.pk,
            'branch': self.branch.pk,
            'memo_to_supplier': 'Uitenhedger comment',
            'type_1': 'i',
            'inventory_1': inventory.pk,
            'allow_negative_stock_1': 0,
            'average_price_1': 682.61,
            'in_stock_1': 1,
            'quantity_1': 10.00,
            'unit_1': 'Each',
            'unit_id_1': 1,
            'price_1': 1730.40,
            'price_including_1': 1989.96,
            'total_price_excluding_1': 17304.00,
            'sub_total_1': 17304.00,
            'total_price_including_1': 19899.60,
            'total_discount_1': 0.00,
            'vat_code_1': vat_code.pk,
            'vat_percentage_1': 15,
            'total_vat_1': 2595.60,
            'vat_amount_1': 2595.60,
            'type_2': 'g',
            'account_2': account.pk,
            'allow_negative_stock_2': 1,
            'description_2': 'Manage description',
            'quantity_2': 1.00,
            'unit_item_2': 'No',
            'unit_2': '',
            'price_2': 890,
            'price_including_2': 1023.5,
            'total_price_excluding_2': 881.10,
            'sub_total_2': 890.00,
            'total_price_including_2': 1013.27,
            'discount_percentage_2': 1,
            'total_discount_2': 8.90,
            'vat_code_2': vat_code.pk,
            'vat_percentage_2': 15,
            'total_vat_2': 132.17,
            'vat_amount_2': 132.17,
            'type_3': 't',
            'item_description_3': 'Just a text line',
            'type_4': 'i',
            'inventory_4': '',
            'allow_negative_stock_4': '',
            'average_price_4': '',
            'in_stock_4': '',
            'quantity_4': 0,
            'unit_4': '',
            'unit_id_4': '',
            'price_4': 0,
            'price_including_4': '',
            'total_price_excluding_4': '',
            'sub_total_4': '',
            'total_price_including_4': '',
            'discount_percentage_4': 0,
            'total_discount_4': '',
            'vat_code_4': vat_code.pk,
            'vat_percentage_4': 15,
            'total_vat_4': '',
            'vat_amount_4': '',
            'net_amount': '18185.10',
            'vat_amount': '2727.77',
            'total_amount': '20912.87'
        }
        assert PurchaseOrder.objects.count() == 0
        response = self.ajax_post(self.create_url, data=data)
        assert response.status_code == 200
        assert PurchaseOrder.objects.count() == 1
        assert Invoice.objects.count() == 1
        invoice_po = Invoice.objects.first()
        assert data['total_amount'] == utils.format_decimal(invoice_po.total_amount)
        assert invoice_po.due_date is not None

    def test_validations_errors(self):
        pass
        # data = {}
        # assert PurchaseOrder.objects.count() == 0
        # response = self.client.post(self.create_url, data=data)
        #
        # assert response.status_code == 400
        # assert PurchaseOrder.objects.count() == 0
        # assert Invoice.objects.count() == 0

    def test_can_approve_purchase_order(self):
        pass
        # today = now().date()
        # period = factories.PeriodFactory(period=today.month, period_year=self.year)
        # factories.PurchaseOrderInvoiceTypeFactory(company=self.company)
        # factories.StatusFactory(po_request=True)
        # factories.StatusFactory(po_approved=True)
        # factories.FlowStatusFactory()
        # factories.FlowStatusFactory(name='Processed', slug='processed')
        # factories.FlowStatusFactory(name='Processing', slug='processing')
        # supplier = factories.SupplierFactory(company=self.company)
        # account = factories.AccountFactory(name='Other PO', company=self.company, code='3400/0001')
        #
        # invoice = factories.InvoiceFactory(period=period, company=self.company)
        #
        # purchase_order = factories.PurchaseOrderFactory(invoice=invoice)
        #
        # assert PurchaseOrder.objects.count() == 0
        # response = self.client.post(reverse('approve_purchaseorder', args=(purchase_order.pk, )), data={})
        #
        # assert response.status_code == 200
        # assert PurchaseOrder.objects.count() == 1
        # assert Invoice.objects.count() == 1

    def test_can_decline_purchase_order(self):
        pass

    def test_can_order_purchase_order(self):
        pass

    def test_can_cancel_purchase_order(self):
        pass
