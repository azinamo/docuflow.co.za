from http import HTTPStatus
from datetime import date

from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.distribution.models import Distribution, DistributionAccount
from docuflow.apps.system.enums import AccountType
from tests.base import AuthenticatedTestCase
from tests import factories
from tests import utils


class TestDistributionView(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()

    def test_can_view_distributions(self):
        response = self.client.get(reverse('distributions_index'))

        assert response.status_code == HTTPStatus.OK

    def test_create_create_distribution_for_invoice_account(self):
        periods = []
        for p in range(1, 13):
            from_date = date(month=p, year=self.year.year, day=1)
            to_date = date(month=p, year=self.year.year, day=20)
            periods.append(
                factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                        from_date=from_date, period_year=self.year, company=self.company)
            )
        default_distribution_account = factories.AccountFactory(
            company=self.company, code='8200/0000', name='Default Control - Distributions'
        )
        self.company.default_distribution_account = default_distribution_account
        self.company.save()

        invoice = factories.InvoiceFactory(company=self.company, total_amount=22752.50, vat_amount=4550.50,
                                           period=periods[0])

        income_statement_report = factories.ReportTypeFactory(account_type=AccountType.INCOME_STATEMENT)

        account = factories.AccountFactory(company=self.company, name='Staff Uniforms', code='4360/000',
                                           report=income_statement_report)
        invoice_account = factories.InvoiceAccountFactory(invoice=invoice, amount=invoice.net_amount, account=account, profile=self.profile)

        data = {
            'starting_date': periods[0].to_date,
            'period': periods[0].pk,
            'nb_months': 5,
            'total_amount': invoice_account.amount,
            'account': invoice_account.account.pk,
            'suffix': '',
            'distribution_1': 1,
            'date_1': periods[1].to_date,
            'period_1': periods[1].pk,
            'accounts_1': [account.pk, default_distribution_account.pk],
            f'accounts_1_debit_{account.pk}': 3640.40,
            f'accounts_1_credit_{account.pk}': 0,
            f'accounts_1_debit_{default_distribution_account.pk}': 0,
            f'accounts_1_credit_{default_distribution_account.pk}': 3640.40,
            'distribution_2': 2,
            'date_2': periods[2].to_date,
            'period_2': periods[2].pk,
            'accounts_2': [account.pk, default_distribution_account.pk],
            f'accounts_2_debit_{account.pk}': 3640.40,
            f'accounts_2_credit_{account.pk}': 0,
            f'accounts_2_debit_{default_distribution_account.pk}': 0,
            f'accounts_2_credit_{default_distribution_account.pk}': 3640.40,
            'distribution_3': 3,
            'date_3': periods[3].to_date,
            'period_3': periods[3].pk,
            'accounts_3': [account.pk, default_distribution_account.pk],
            f'accounts_3_debit_{account.pk}': 3640.40,
            f'accounts_3_credit_{account.pk}': 0,
            f'accounts_3_debit_{default_distribution_account.pk}': 0,
            f'accounts_3_credit_{default_distribution_account.pk}': 3640.40,
            'distribution_4': 4,
            'date_4': periods[4].to_date,
            'period_4': periods[4].pk,
            'accounts_4': [account.pk, default_distribution_account.pk],
            f'accounts_4_debit_{account.pk}': 3640.40,
            f'accounts_4_credit_{account.pk}': 0,
            f'accounts_4_debit_{default_distribution_account.pk}': 0,
            f'accounts_4_credit_{default_distribution_account.pk}': 3640.40,
            'distribution_5': 5,
            'date_5': periods[5].to_date,
            'period_5': periods[5].pk,
            'accounts_5': [account.pk, default_distribution_account.pk],
            f'accounts_5_debit_{account.pk}': 3640.40,
            f'accounts_5_credit_{account.pk}': 0,
            f'accounts_5_debit_{default_distribution_account.pk}': 0,
            f'accounts_5_credit_{default_distribution_account.pk}': 3640.40
        }

        assert Distribution.objects.count() == 0
        assert DistributionAccount.objects.count() == 0

        content_type = ContentType.objects.get_for_model(invoice_account)

        response = self.ajax_post(reverse_lazy('create_distribution', kwargs={'content_type_id': content_type.pk,
                                                                              'object_id': invoice_account.pk}
                                               ), data=data)

        assert response.status_code == HTTPStatus.OK
        assert response.data['text'] == 'Distribution successfully saved'

        distributions = Distribution.objects.all()
        assert len(distributions) == 1
        assert distributions[0].nb_months == data['nb_months']
        assert distributions[0].starting_date == data['starting_date']
        assert distributions[0].total_amount == data['total_amount']
        assert distributions[0].content_type == content_type
        assert distributions[0].object_id == invoice_account.pk
        assert distributions[0].title == f"{invoice.supplier.name}-{str(invoice)}"
        assert distributions[0].is_credit is False

        distributions_accounts = DistributionAccount.objects.all()
        assert len(distributions_accounts) == 10
        assert distributions_accounts[1].account == account
        assert distributions_accounts[1].debit == utils.to_decimal(value=data[f'accounts_1_debit_{account.pk}'])
        assert distributions_accounts[1].credit == utils.to_decimal(value='0')
        assert distributions_accounts[1].distribution == distributions[0]
        assert distributions_accounts[1].period == periods[1]
        assert distributions_accounts[1].date == periods[1].to_date

        assert distributions_accounts[0].account == default_distribution_account
        assert distributions_accounts[0].credit == utils.to_decimal(value=data[f'accounts_1_debit_{account.pk}'])
        assert distributions_accounts[0].debit == utils.to_decimal(value='0')
        assert distributions_accounts[0].distribution == distributions[0]
        assert distributions_accounts[0].period == periods[1]
        assert distributions_accounts[0].date == periods[1].to_date

    # def test_amount_distribution(self):
    #     periods = []
    #     for p in range(1, 13):
    #         from_date = date(month=p, year=self.year.year, day=1)
    #         to_date = date(month=p, year=self.year.year, day=20)
    #         periods.append(
    #             factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
    #                                     from_date=from_date, period_year=self.year, company=self.company)
    #         )
    #     default_distribution_account = factories.AccountFactory(
    #         company=self.company, code='8200/0000', name='Default Control - Distributions'
    #     )
    #     self.company.default_distribution_account = default_distribution_account
    #     self.company.save()
    #
    #     invoice = factories.InvoiceFactory(company=self.company, total_amount=22752.50, vat_amount=4550.50,
    #                                        period=periods[0])
    #
    #     income_statement_report = factories.ReportTypeFactory(account_type=AccountType.INCOME_STATEMENT)
    #
    #     account = factories.AccountFactory(company=self.company, name='Staff Uniforms', code='4360/000',
    #                                        report=income_statement_report)
    #     invoice_account = factories.InvoiceAccountFactory(invoice=invoice, amount=invoice.net_amount, account=account, profile=self.profile)
    #
    #     data = {
    #         'nb_months': 6,
    #         'starting_date': '2021-08-03',
    #         'account_id': account.pk,
    #         'total_amount': 18202.00
    #     }
    #
    #     response = self.ajax_get(reverse_lazy('distribute_amount'), data=data)
    #
    #     assert response.status_code == HTTPStatus.OK
