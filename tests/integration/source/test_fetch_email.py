from unittest import mock

from django.core.management import call_command
from django.test import TestCase

from docuflow.apps.invoice.models import Invoice
from docuflow.apps.sources.utils import MailFetcher
from tests import factories
from tests.mock import mails_response


@mock.patch('docuflow.apps.sources.client.MailFetchClient.fetch_emails', mails_response)
# @mock.patch('docuflow.apps.sources.utils.MailFetcher.get_mail_message', mail_message)
# @mock.patch('docuflow.apps.sources.utils.MailFetcher.get_message_attachment', mail_part_response)
# @mock.patch('docuflow.apps.sources.utils.MailFetcher.save_file', save_file_response)
# @mock.patch('docuflow.apps.sources.utils.MailFetcher.rename_invoice_document', rename_file_response)
class MailFetchTests(TestCase):

    def setUp(self) -> None:
        self.company = factories.CompanyFactory(slug='41055')
        self.invoice_type = factories.TaxInvoiceTypeFactory(company=self.company)

    def test_mail_fetcher_job(self):
        factories.LoggerStatusFactory()
        mail_source = factories.IMAPEmailFactory(company=self.company)

        assert Invoice.objects.count() == 0

        breakpoint()
        job = MailFetcher()
        job.execute(source=mail_source)

        assert Invoice.objects.count() == 1

