from django.urls import reverse, reverse_lazy

from docuflow.apps.accounts.models import Profile
from tests.base import AuthenticatedTestCase
from tests import factories


class TestRegistrationViews(AuthenticatedTestCase):
    list_url = reverse_lazy('users-list')

    def setUp(self) -> None:
        super().setUp()

    def test_company_profile_registration(self):
        large_package = factories.SubscriptionFactory()

        data = {
            'first_name': 'Pat',
            'last_name': 'Ricia',
            'email': 'pat.ricia@gmail.com',
            'password1': 'munana123',
            'password2': 'munana123',
            'name': 'Munana',
            'type': '5',
            'company_number': '1239213089',
            'area_code': '890',
            'address_line_1': '42 The Lothians',
            'address_line_2': '99 Rosmead Avenue',
            'province': 'Western Cape',
            'vat_number': '3299324',
            'start_date': '2011-09-01'
        }

        response = self.ajax_post(url=reverse_lazy('plan_signup', kwargs={'slug': large_package.slug}), data=data)

        assert response.status_code == 200


