from datetime import date

from django.urls import reverse

from freezegun import freeze_time

from docuflow.apps.common.utils import format_money
from docuflow.apps.distribution.models import Distribution, DistributionAccount
from docuflow.apps.invoice.models import InvoiceAccount
from docuflow.apps.supplier import services as age_services
from tests.base import AuthenticatedTestCase
from tests import factories
from tests import utils


class InvoiceAccountTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(InvoiceAccountTestCase, self).setUp()

    def test_list_invoice_accounts(self):
        period = factories.PeriodFactory(period=3)
        net_account = factories.AccountFactory(company=self.company, code='9270', name='Provisions')

        invoice = factories.InvoiceFactory(period=period, company=self.company, total_amount=5000, vat_amount=500)

        net_invoice_account = factories.InvoiceAccountFactory(invoice=invoice, amount=invoice.net_amount,
                                                              account=net_account, profile=self.profile)
        vat_invoice_account = factories.InvoiceAccountFactory(invoice=invoice, amount=invoice.vat_amount,
                                                              account=self.vat_account, profile=self.profile,
                                                              is_vat=True)

        response = self.ajax_get(reverse('account_postings_view', kwargs={'invoice_id': invoice.pk}))

        assert response.status_code == 200
        content = response.data.decode()

        assert str(net_account) in content
        assert format_money(net_invoice_account.amount) in content
        assert str(self.vat_account) in content
        assert format_money(vat_invoice_account.amount) in content

    def test_create_invoice_account_for_net_amount(self):
        period = factories.PeriodFactory(period=3)
        net_account = factories.AccountFactory(company=self.company, code='9270', name='Provisions')
        self.role.accounts.set([net_account])

        invoice = factories.InvoiceFactory(period=period, company=self.company, total_amount=5000, vat_amount=500)

        assert InvoiceAccount.objects.count() == 0

        data = {
            'amount': invoice.net_amount,
            'account': net_account.pk,
            'comment': 'Comment'
        }

        response = self.ajax_post(reverse('create-invoice-account', kwargs={'invoice_id': invoice.pk}), data=data)

        assert response.status_code == 200

        invoice_account = InvoiceAccount.objects.first()

        assert invoice_account.amount == invoice.net_amount
        assert invoice_account.account == net_account
        assert invoice_account.is_vat is False
        assert invoice_account.is_holding_account is False
        assert invoice_account.vat_line is False
        assert invoice_account.is_posted is False
        assert invoice_account.account_posting_line is None
        assert invoice_account.invoice_id == invoice.pk
        assert invoice_account.profile_id == self.profile.pk
        assert invoice_account.deleted is None
        assert invoice_account.distribution_id is None
        assert invoice_account.goods_received_id is None
        assert invoice_account.goods_returned_id is None
        assert invoice_account.suffix == ''

    def test_create_vat_invoice_account(self):
        period = factories.PeriodFactory(period=3)
        self.role.accounts.set([self.vat_account])

        invoice = factories.InvoiceFactory(period=period, company=self.company, total_amount=5000, vat_amount=500)

        assert InvoiceAccount.objects.count() == 0

        data = {
            'amount': invoice.vat_amount,
            'account': self.vat_account.pk,
            'comment': 'Vat Line Comment',
            'vat_line': 1
        }

        response = self.ajax_post(reverse('create-invoice-account', kwargs={'invoice_id': invoice.pk}), data=data)

        assert response.status_code == 200

        invoice_account = InvoiceAccount.objects.first()

        assert invoice_account.amount == invoice.vat_amount
        assert invoice_account.account == self.vat_account
        assert invoice_account.is_vat is True
        assert invoice_account.is_holding_account is False
        assert invoice_account.vat_line is True
        assert invoice_account.is_posted is False
        assert invoice_account.account_posting_line is None
        assert invoice_account.invoice_id == invoice.pk
        assert invoice_account.profile_id == self.profile.pk
        assert invoice_account.deleted is None
        assert invoice_account.distribution_id is None
        assert invoice_account.goods_received_id is None
        assert invoice_account.goods_returned_id is None
        assert invoice_account.suffix == ''

    def test_edit_net_invoice_account(self):
        period = factories.PeriodFactory(period=3)
        net_account = factories.AccountFactory(company=self.company, code='9270', name='Provisions')
        self.role.accounts.set([net_account])
        logger_profile = factories.ProfileFactory(user=factories.UserFactory(first_name='Logger', last_name='Role'),
                                                  company=self.company)
        logger_profile.roles.set([self.logger])

        invoice = factories.InvoiceFactory(period=period, company=self.company, total_amount=5000, vat_amount=500)

        invoice_account = factories.InvoiceAccountFactory(invoice=invoice, amount=invoice.net_amount, role=self.logger,
                                                          account=self.expenditure_account, profile=logger_profile,
                                                          is_holding_account=True)

        assert InvoiceAccount.objects.count() == 1

        data = {
            'amount': invoice.net_amount,
            'account': net_account.pk,
            'comment': 'Net Comment',
            'suffix': 'IX-Suffix'
        }

        response = self.ajax_post(reverse('edit_invoice_account', kwargs={'pk': invoice_account.pk}), data=data)

        assert response.status_code == 200

        assert InvoiceAccount.objects.count() == 2

        invoice_account = InvoiceAccount.objects.last()

        assert invoice_account.amount == invoice.net_amount
        assert invoice_account.account == net_account
        assert invoice_account.is_vat is False
        assert invoice_account.is_holding_account is False
        assert invoice_account.vat_line is False
        assert invoice_account.account_posting_line is None
        assert invoice_account.invoice_id == invoice.pk
        assert invoice_account.profile_id == self.profile.pk
        assert invoice_account.role_id == self.role.pk
        assert invoice_account.deleted is None
        assert invoice_account.distribution_id is None
        assert invoice_account.goods_received_id is None
        assert invoice_account.goods_returned_id is None
        assert invoice_account.suffix == data['suffix']

    def test_delete_invoice_account_line(self):
        period = factories.PeriodFactory(period=3)
        net_account = factories.AccountFactory(company=self.company, code='9270', name='Provisions')
        self.role.accounts.set([net_account])

        invoice = factories.InvoiceFactory(period=period, company=self.company, total_amount=5000, vat_amount=500)

        invoice_account = factories.InvoiceAccountFactory(invoice=invoice, amount=invoice.net_amount, role=self.role,
                                                          account=self.expenditure_account, profile=self.profile)

        assert InvoiceAccount.objects.count() == 1

        response = self.ajax_get(reverse('delete_invoice_account_posting', kwargs={'pk': invoice_account.pk}))

        assert response.status_code == 200

        assert InvoiceAccount.objects.count() == 0

    def test_delete_invoice_account_line_with_distributions(self):
        period = factories.PeriodFactory(period=3)
        net_account = factories.AccountFactory(company=self.company, code='9270', name='Provisions')
        distribution_account = factories.AccountFactory(company=self.company, code='8200/000', name='Distribution Control')

        invoice = factories.InvoiceFactory(period=period, company=self.company, total_amount=5000, vat_amount=500)

        invoice_account = factories.InvoiceAccountFactory(invoice=invoice, amount=invoice.net_amount, role=self.role,
                                                          account=self.expenditure_account, profile=self.profile)

        profile = factories.ProfileFactory(company=self.company, user=factories.UserFactory(first_name='FN', last_name='LN'))
        distribution = factories.InvoiceAccountDistributionFactory(
            content_object=invoice_account, total_amount=invoice_account.amount, account=invoice_account.account,
            nb_months=1, profile=profile, role=self.role
        )

        factories.DistributionAccountFactory(
            distribution=distribution, account=net_account, debit=invoice_account.amount
        )
        factories.DistributionAccountFactory(
            distribution=distribution, account=distribution_account, credit=invoice_account.amount, debit=0
        )

        assert InvoiceAccount.objects.count() == 1
        assert Distribution.objects.count() == 1
        assert DistributionAccount.objects.count() == 2

        response = self.ajax_get(reverse('delete_invoice_account_posting', kwargs={'pk': invoice_account.pk}))

        assert response.status_code == 200

        assert InvoiceAccount.objects.count() == 0
        assert Distribution.objects.count() == 0
        assert DistributionAccount.objects.count() == 0
