from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from http import HTTPStatus

from django.urls import reverse

from docuflow.apps.invoice.models import Journal, InvoiceJournal
from docuflow.apps.journals.models import Journal as Ledger, JournalLine as LedgerLine
from docuflow.apps.supplier.models import AgeAnalysis

from tests.base import AuthenticatedTestCase
from tests import factories
from tests import utils


class TestIncomingJournal(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_can_list_incoming_journals(self):
        period = factories.PeriodFactory(period_year=self.year)
        incoming_journals = factories.IncomingInvoiceUpdateFactory.create_batch(
            3, created_by=self.profile, role=self.role, company=self.company, period=period
        )

        response = self.client.get(reverse('journal_for_incoming_invoice'))
        assert response.status_code == HTTPStatus.OK

        content = response.content.decode()
        assert (str(incoming_journals[0]) in content) is True
        assert (str(incoming_journals[1]) in content) is True
        assert (str(incoming_journals[2]) in content) is True

    def test_no_invoices_when_invoice_not_within_date_range(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Data')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        factories.InvoiceFactory(period=period, total_amount=1600.00, supplier=supplier, company=self.company,
                                 accounting_date=(from_date + relativedelta(months=+2)))
        factories.InvoiceFactory(period=period, total_amount=1200.00, supplier=supplier, company=self.company,
                                 accounting_date=(from_date + relativedelta(months=+1))
                                 )
        data = {
            'period': period.pk,
            'date': period.from_date + timedelta(days=5)
        }

        response = self.ajax_post(reverse('create_incoming_invoices'), data=data)
        assert response.status_code == 200
        assert Journal.objects.count() == 0
        assert response.data['text'] == 'No invoices found'

    def test_can_create_journal_for_invoices_in_flow(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=self.vat_account)
        invoice_1 = factories.InvoiceFactory(period=period, total_amount=1600.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=3),
                                             vat_amount=400)
        invoice_2 = factories.InvoiceFactory(period=period, total_amount=1200.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=10))
        invoice_3 = factories.InvoiceFactory(period=period, total_amount=5000.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=5))

        data = {
            'period': period.pk,
            'date': period.from_date + timedelta(days=5)
        }
        response = self.ajax_post(reverse('create_incoming_invoices'), data=data)
        assert response.status_code == 200

        assert Journal.objects.count() == 1
        assert InvoiceJournal.objects.count() == 2
        invoice_journals = InvoiceJournal.objects.all()

        assert invoice_journals[0].total_amount == invoice_1.total_amount
        assert invoice_journals[0].net_amount == invoice_1.net_amount
        assert invoice_journals[0].vat_amount == invoice_1.vat_amount

        assert invoice_journals[1].total_amount == invoice_3.total_amount
        assert invoice_journals[1].net_amount == invoice_3.net_amount
        assert invoice_journals[1].vat_amount == invoice_3.vat_amount

        assert Ledger.objects.count() == 1
        assert LedgerLine.objects.count() == 4

        # Supplier Control
        supplier_control_line = LedgerLine.objects.filter(account=self.company.default_supplier_account).first()
        assert supplier_control_line.credit == invoice_1.total_amount + invoice_3.total_amount

        # Vat control
        vat_control_line = LedgerLine.objects.filter(account=self.company.default_vat_account)
        assert vat_control_line[0].debit == invoice_1.vat_amount
        assert vat_control_line[1].debit == invoice_3.vat_amount

        # Invoices not yet posted
        expenditure_line = LedgerLine.objects.filter(account=self.company.default_expenditure_account).first()
        assert expenditure_line.debit == invoice_1.net_amount + invoice_3.net_amount

        assert AgeAnalysis.objects.count() == 1  # we only have 1 period
        age_analysis = AgeAnalysis.objects.filter(period=period, supplier=supplier).first()
        assert age_analysis.balance == invoice_1.total_amount + invoice_3.total_amount
        assert age_analysis.current == invoice_1.total_amount + invoice_3.total_amount

    def test_create_incoming_journal_for_invoices_adjusted(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=self.vat_account)
        invoice_1 = factories.InvoiceFactory(period=period, total_amount=1600.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=3),
                                             vat_amount=400)
        invoice_2 = factories.InvoiceFactory(period=period, total_amount=1200.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=10))
        invoice_3 = factories.InvoiceFactory(period=period, total_amount=5000.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=5))


        factories.IncomingInvoiceUpdateFactory(created_by=self.profile, role=self.role)

        data = {
            'period': period.pk,
            'date': period.from_date + timedelta(days=5)
        }
        response = self.ajax_post(reverse('create_incoming_invoices'), data=data)
        assert response.status_code == 200

        assert Journal.objects.count() == 2
        assert InvoiceJournal.objects.count() == 2
        invoice_journals = InvoiceJournal.objects.all()

        assert invoice_journals[0].total_amount == invoice_1.total_amount
        assert invoice_journals[0].net_amount == invoice_1.net_amount
        assert invoice_journals[0].vat_amount == invoice_1.vat_amount

        assert invoice_journals[1].total_amount == invoice_3.total_amount
        assert invoice_journals[1].net_amount == invoice_3.net_amount
        assert invoice_journals[1].vat_amount == invoice_3.vat_amount

        assert Ledger.objects.count() == 2
        assert LedgerLine.objects.count() == 4

        # Supplier Control
        supplier_control_line = LedgerLine.objects.filter(account=self.company.default_supplier_account).first()
        assert supplier_control_line.credit == invoice_1.total_amount + invoice_3.total_amount

        # Vat control
        vat_control_line = LedgerLine.objects.filter(account=self.company.default_vat_account)
        assert vat_control_line[0].debit == invoice_1.vat_amount
        assert vat_control_line[1].debit == invoice_3.vat_amount

        # Invoices not yet posted
        expenditure_line = LedgerLine.objects.filter(account=self.company.default_expenditure_account).first()
        assert expenditure_line.debit == invoice_1.net_amount + invoice_3.net_amount

        assert AgeAnalysis.objects.count() == 1  # we only have 1 period
        age_analysis = AgeAnalysis.objects.filter(period=period, supplier=supplier).first()
        assert age_analysis.balance == invoice_1.total_amount + invoice_3.total_amount
        assert age_analysis.current == invoice_1.total_amount + invoice_3.total_amount

    def test_create_journal_for_invoices_adjusted_with_different_amount(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=self.vat_account)
        invoice_1 = factories.InvoiceFactory(period=period, total_amount=1600.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=3),
                                             vat_amount=400)
        invoice_2 = factories.InvoiceFactory(period=period, total_amount=1200.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=10))
        invoice_3 = factories.InvoiceFactory(period=period, total_amount=5000.00, supplier=supplier, vat_code=vat_code,
                                             company=self.company, accounting_date=from_date + timedelta(days=5))

        data = {
            'period': period.pk,
            'date': period.from_date + timedelta(days=5)
        }
        response = self.ajax_post(reverse('create_incoming_invoices'), data=data)
        assert response.status_code == 200

        assert Journal.objects.count() == 1
        assert InvoiceJournal.objects.count() == 2
        invoice_journals = InvoiceJournal.objects.all()

        assert invoice_journals[0].total_amount == invoice_1.total_amount
        assert invoice_journals[0].net_amount == invoice_1.net_amount
        assert invoice_journals[0].vat_amount == invoice_1.vat_amount

        assert invoice_journals[1].total_amount == invoice_3.total_amount
        assert invoice_journals[1].net_amount == invoice_3.net_amount
        assert invoice_journals[1].vat_amount == invoice_3.vat_amount

        assert Ledger.objects.count() == 1
        assert LedgerLine.objects.count() == 4

        # Supplier Control
        supplier_control_line = LedgerLine.objects.filter(account=self.company.default_supplier_account).first()
        assert supplier_control_line.credit == invoice_1.total_amount + invoice_3.total_amount

        # Vat control
        vat_control_line = LedgerLine.objects.filter(account=self.company.default_vat_account)
        assert vat_control_line[0].debit == invoice_1.vat_amount
        assert vat_control_line[1].debit == invoice_3.vat_amount

        # Invoices not yet posted
        expenditure_line = LedgerLine.objects.filter(account=self.company.default_expenditure_account).first()
        assert expenditure_line.debit == invoice_1.net_amount + invoice_3.net_amount

        assert AgeAnalysis.objects.count() == 1  # we only have 1 period
        age_analysis = AgeAnalysis.objects.filter(period=period, supplier=supplier).first()
        assert age_analysis.balance == invoice_1.total_amount + invoice_3.total_amount
        assert age_analysis.current == invoice_1.total_amount + invoice_3.total_amount

    def test_update_invoice_with_adjusted_net_and_total_amount(self):
        supplier = factories.SupplierFactory(company=self.company, code='CHE001', name='Cheque Card Purchases')
        p = 8
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(
            period=p, to_date=to_date, from_date=from_date, period_year=self.year, company=self.company
        )
        adjustment_status = factories.StatusFactory(adjustment=True)
        vat_code = factories.VatCodeFactory(company=self.company, account=self.vat_account)
        invoice = factories.InvoiceFactory(
            period=period, total_amount=965.85, supplier=supplier, vat_code=vat_code, status=adjustment_status,
            company=self.company, accounting_date=from_date + timedelta(days=3), vat_amount=9.13
        )

        incoming_update = factories.IncomingInvoiceUpdateFactory(
            company=self.company, period=period, created_by=self.profile
        )
        invoice_journal = factories.InvoiceJournalFactory(
            invoice=invoice, journal=incoming_update, vat_amount=9.13, total_amount=956.72
        )
        data = {
            'period': period.pk,
            'date': period.from_date + timedelta(days=5)
        }
        response = self.ajax_post(reverse('create_incoming_invoices'), data=data)
        assert response.status_code == HTTPStatus.OK

        assert Journal.objects.count() == 2

        invoice_journals = InvoiceJournal.objects.all()

        assert len(invoice_journals) == 2

        assert invoice_journals[1].total_amount == utils.to_decimal(str(invoice.total_amount))
        assert invoice_journals[1].net_amount == utils.to_decimal(str(invoice.net_amount))
        assert invoice_journals[1].vat_amount == utils.to_decimal(str(invoice.vat_amount))

        assert Ledger.objects.count() == 2
        assert LedgerLine.objects.count() == 3

        # Supplier Control
        supplier_control_line = LedgerLine.objects.filter(account=self.company.default_supplier_account).first()
        amount = utils.to_decimal(invoice.total_amount - invoice_journal.total_amount)
        if amount > 0:
            assert supplier_control_line.credit == amount
        else:
            assert supplier_control_line.debit == amount

        # Vat control
        vat_control_line = LedgerLine.objects.filter(account=self.company.default_vat_account)
        assert vat_control_line[0].debit == 0

        # Invoices not yet posted
        expenditure_line = LedgerLine.objects.filter(account=self.company.default_expenditure_account).first()
        assert expenditure_line.debit == amount

        assert AgeAnalysis.objects.count() == 1  # we only have 1 period
        age_analysis = AgeAnalysis.objects.filter(period=period, supplier=supplier).first()
        assert age_analysis.balance == amount
        assert age_analysis.current == amount
