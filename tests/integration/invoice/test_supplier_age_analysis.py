from datetime import date

from freezegun import freeze_time

from docuflow.apps.common.utils import round_decimal
from docuflow.apps.supplier.models import AgeAnalysis
from docuflow.apps.supplier import services as age_services
from tests.base import AuthenticatedTestCase
from tests import factories


@freeze_time('2021-04-30')
class SupplierAgeAnalysisTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(SupplierAgeAnalysisTestCase, self).setUp()
        periods = []
        for p in range(1, 13):
            from_date = date(month=p, year=self.year.year, day=1)
            to_date = date(month=p, year=self.year.year, day=20)
            periods.append(
                factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                        from_date=from_date, period_year=self.year, company=self.company)
            )
        self.periods = periods

    def test_can_age_current_supplier_invoice(self):
        self.current_period = self.periods[0]

        invoice = factories.InvoiceFactory(period=self.current_period, company=self.company)

        age_services.add_invoice_to_age_analysis(invoice)

        assert AgeAnalysis.objects.count() == 12

        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=invoice.period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_2)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_3)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_4 = self.periods[3]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_4)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_5)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

        period_6 = self.periods[5]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_6)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

    def test_can_age_supplier_invoice_added_mid_year(self):
        current_period = self.periods[1]
        amount = 38284.98
        invoice = factories.InvoiceFactory(period=current_period, company=self.company, total_amount=amount)

        age_services.add_invoice_to_age_analysis(invoice)

        assert AgeAnalysis.objects.count() == 11

        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=invoice.period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[2]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_2)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[3]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_3)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_4 = self.periods[4]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_4)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[5]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_5)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

        period_6 = self.periods[6]
        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_6)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

    def test_can_full_pay_current_supplier_invoice_in_same_period(self):
        self.current_period = self.periods[0]

        invoice = factories.InvoiceFactory(period=self.current_period, company=self.company)

        age_services.add_invoice_to_age_analysis(invoice)

        assert AgeAnalysis.objects.count() == 12

        payment = factories.PaymentFactory(period=self.current_period, company=self.company, profile=self.profile,
                                           total_amount=invoice.total_amount)
        invoice_payment = factories.InvoicePaymentFactory(payment=payment, invoice=invoice)

        age_services.add_invoice_payment_to_age_analysis(invoice_payment=invoice_payment)

        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=invoice.period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        period_2_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_2)

        assert period_2_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.current == round_decimal(0)
        assert period_2_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.sixty_day == round_decimal(0)
        assert period_2_age_analysis.ninety_day == round_decimal(0)
        assert period_2_age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        period_3_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_3)
        assert period_3_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.current == round_decimal(0)
        assert period_3_age_analysis.thirty_day == round_decimal(0)
        assert period_3_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.ninety_day == round_decimal(0)
        assert period_3_age_analysis.one_twenty_day == round_decimal(0)

        period_4 = self.periods[3]
        period_4_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_4)
        assert period_4_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_4_age_analysis.current == round_decimal(0)
        assert period_4_age_analysis.thirty_day == round_decimal(0)
        assert period_4_age_analysis.sixty_day == round_decimal(0)
        assert period_4_age_analysis.ninety_day == round_decimal(invoice.total_amount)
        assert period_4_age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        period_5_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_5)
        assert period_5_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_5_age_analysis.current == round_decimal(0)
        assert period_5_age_analysis.thirty_day == round_decimal(0)
        assert period_5_age_analysis.sixty_day == round_decimal(0)
        assert period_5_age_analysis.ninety_day == round_decimal(0)
        assert period_5_age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

        period_6 = self.periods[5]
        period_6_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_6)
        assert period_6_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_6_age_analysis.current == round_decimal(0)
        assert period_6_age_analysis.thirty_day == round_decimal(0)
        assert period_6_age_analysis.sixty_day == round_decimal(0)
        assert period_6_age_analysis.ninety_day == round_decimal(0)
        assert period_6_age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

    def test_can_full_pay_supplier_invoice_in_future_period(self):
        self.current_period = self.periods[0]

        amount = 2048
        invoice = factories.InvoiceFactory(period=self.current_period, company=self.company, total_amount=amount)

        age_services.add_invoice_to_age_analysis(invoice)

        assert AgeAnalysis.objects.count() == 12

        payment_period = self.periods[3]
        payment = factories.PaymentFactory(period=payment_period, company=self.company, profile=self.profile,
                                           total_amount=amount, supplier=invoice.supplier)
        invoice_payment = factories.InvoicePaymentFactory(payment=payment, invoice=invoice, allocated=amount)

        age_services.add_invoice_payment_to_age_analysis(invoice_payment=invoice_payment)

        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=self.current_period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        period_2_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_2)

        assert period_2_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.current == round_decimal(0)
        assert period_2_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.sixty_day == round_decimal(0)
        assert period_2_age_analysis.ninety_day == round_decimal(0)
        assert period_2_age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        period_3_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_3)
        assert period_3_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.current == round_decimal(0)
        assert period_3_age_analysis.thirty_day == round_decimal(0)
        assert period_3_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.ninety_day == round_decimal(0)
        assert period_3_age_analysis.one_twenty_day == round_decimal(0)

         # App previous periods keeps their balances, but future periods will be cleared
        period_4_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=payment_period)
        assert period_4_age_analysis.balance == round_decimal(0)
        assert period_4_age_analysis.current == round_decimal(0)
        assert period_4_age_analysis.thirty_day == round_decimal(0)
        assert period_4_age_analysis.sixty_day == round_decimal(0)
        assert period_4_age_analysis.ninety_day == round_decimal(0)
        assert period_4_age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        period_5_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_5)
        assert period_5_age_analysis.balance == round_decimal(0)
        assert period_5_age_analysis.current == round_decimal(0)
        assert period_5_age_analysis.thirty_day == round_decimal(0)
        assert period_5_age_analysis.sixty_day == round_decimal(0)
        assert period_5_age_analysis.ninety_day == round_decimal(0)
        assert period_5_age_analysis.one_twenty_day == round_decimal(0)

        period_6 = self.periods[5]
        period_6_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_6)
        assert period_6_age_analysis.balance == round_decimal(0)
        assert period_6_age_analysis.current == round_decimal(0)
        assert period_6_age_analysis.thirty_day == round_decimal(0)
        assert period_6_age_analysis.sixty_day == round_decimal(0)
        assert period_6_age_analysis.ninety_day == round_decimal(0)
        assert period_6_age_analysis.one_twenty_day == round_decimal(0)

    def test_supplier_invoice_added_mid_year_paid_in_future_period(self):
        invoice_period = self.periods[1]
        amount = 38284.98
        invoice = factories.InvoiceFactory(period=invoice_period, company=self.company, total_amount=amount)

        age_services.add_invoice_to_age_analysis(invoice)

        # Only added to age on the second month, thus we have 11 periods its aged
        assert AgeAnalysis.objects.count() == 11

        payment_period = self.periods[3]

        april_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=payment_period)

        assert april_age_analysis.balance == round_decimal(invoice.total_amount)
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert april_age_analysis.thirty_day == round_decimal(0)

        payment = factories.PaymentFactory(period=payment_period, company=self.company, profile=self.profile,
                                           total_amount=amount, supplier=invoice.supplier)
        invoice_payment = factories.InvoicePaymentFactory(payment=payment, invoice=invoice, allocated=amount)

        age_services.add_invoice_payment_to_age_analysis(invoice_payment=invoice_payment)

        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=invoice_period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        march_period = self.periods[2]
        march_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=march_period)

        assert march_age_analysis.balance == round_decimal(invoice.total_amount)
        assert march_age_analysis.current == round_decimal(0)
        assert march_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert march_age_analysis.sixty_day == round_decimal(0)
        assert march_age_analysis.ninety_day == round_decimal(0)
        assert march_age_analysis.one_twenty_day == round_decimal(0)

        # App previous periods keeps their balances, but future periods will be cleared
        april_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=payment_period)

        assert april_age_analysis.balance == round_decimal(0)
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.thirty_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal(0)
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)

        may = self.periods[4]
        may_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=may)
        assert may_age_analysis.balance == round_decimal(0)
        assert may_age_analysis.current == round_decimal(0)
        assert may_age_analysis.thirty_day == round_decimal(0)
        assert may_age_analysis.sixty_day == round_decimal(0)
        assert may_age_analysis.ninety_day == round_decimal(0)
        assert may_age_analysis.one_twenty_day == round_decimal(0)

        june = self.periods[5]
        june_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=june)
        assert june_age_analysis.balance == round_decimal(0)
        assert june_age_analysis.current == round_decimal(0)
        assert june_age_analysis.thirty_day == round_decimal(0)
        assert june_age_analysis.sixty_day == round_decimal(0)
        assert june_age_analysis.ninety_day == round_decimal(0)
        assert june_age_analysis.one_twenty_day == round_decimal(0)

    def test_can_partial_pay_supplier_invoice_in_future_period(self):
        self.current_period = self.periods[0]

        amount = 2048
        invoice = factories.InvoiceFactory(period=self.current_period, company=self.company, total_amount=amount)

        age_services.add_invoice_to_age_analysis(invoice)

        assert AgeAnalysis.objects.count() == 12

        payment_period = self.periods[3]
        payment_amount = 2000
        payment = factories.PaymentFactory(period=payment_period, company=self.company, profile=self.profile,
                                           total_amount=payment_amount, supplier=invoice.supplier)
        invoice_payment = factories.InvoicePaymentFactory(payment=payment, invoice=invoice, allocated=payment_amount)

        invoice_balance = amount - payment_amount

        age_services.add_invoice_payment_to_age_analysis(invoice_payment=invoice_payment)

        age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=self.current_period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        period_2_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_2)

        assert period_2_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.current == round_decimal(0)
        assert period_2_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.sixty_day == round_decimal(0)
        assert period_2_age_analysis.ninety_day == round_decimal(0)
        assert period_2_age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        period_3_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_3)
        assert period_3_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.current == round_decimal(0)
        assert period_3_age_analysis.thirty_day == round_decimal(0)
        assert period_3_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.ninety_day == round_decimal(0)
        assert period_3_age_analysis.one_twenty_day == round_decimal(0)

        period_4_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=payment_period)
        assert period_4_age_analysis.balance == round_decimal(invoice_balance)
        assert period_4_age_analysis.current == round_decimal(0)
        assert period_4_age_analysis.thirty_day == round_decimal(0)
        assert period_4_age_analysis.sixty_day == round_decimal(0)
        assert period_4_age_analysis.ninety_day == round_decimal(invoice_balance)
        assert period_4_age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        period_5_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_5)
        assert period_5_age_analysis.balance == round_decimal(invoice_balance)
        assert period_5_age_analysis.current == round_decimal(0)
        assert period_5_age_analysis.thirty_day == round_decimal(0)
        assert period_5_age_analysis.sixty_day == round_decimal(0)
        assert period_5_age_analysis.ninety_day == round_decimal(0)
        assert period_5_age_analysis.one_twenty_day == round_decimal(invoice_balance)

        period_6 = self.periods[5]
        period_6_age_analysis = AgeAnalysis.objects.get(supplier=invoice.supplier, period=period_6)
        assert period_6_age_analysis.balance == round_decimal(invoice_balance)
        assert period_6_age_analysis.current == round_decimal(0)
        assert period_6_age_analysis.thirty_day == round_decimal(0)
        assert period_6_age_analysis.sixty_day == round_decimal(0)
        assert period_6_age_analysis.ninety_day == round_decimal(0)
        assert period_6_age_analysis.one_twenty_day == round_decimal(invoice_balance)

    def test_age_with_journal_payment(self):
        period = self.periods[1]
        amount = 38284.98
        supplier = factories.SupplierFactory(company=self.company)
        ledger = factories.SupplierLedgerFactory(
            period=period, supplier=supplier, created_by=self.profile,
            journal_line=factories.SupplierJournalLineFactory(
                credit=3000, content_object=supplier, debit=0,
                journal=factories.JournalFactory(period=period, company=self.company)
            )
        )
        age_services.add_journal_to_age_analysis(ledger.journal_line)
        # Only added to age on the second month, thus we have 11 periods its aged
        assert AgeAnalysis.objects.count() == 11

        payment_period = self.periods[3]

        april_age_analysis = AgeAnalysis.objects.get(supplier=ledger.supplier, period=payment_period)
        assert april_age_analysis.balance == round_decimal(ledger.amount) * -1
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal(ledger.amount) * -1
        assert april_age_analysis.thirty_day == round_decimal(0)

        payment = factories.PaymentFactory(period=payment_period, company=self.company, profile=self.profile,
                                           total_amount=amount, supplier=ledger.supplier)
        journal_payment = factories.SupplierLedgerPaymentFactory(payment=payment, ledger=ledger, amount=ledger.balance)

        age_services.add_journal_payment_to_age_analysis(ledger_payment=journal_payment)

        age_analysis = AgeAnalysis.objects.get(supplier=ledger.supplier, period=period)

        assert age_analysis.balance == round_decimal(ledger.amount) * -1
        assert age_analysis.current == round_decimal(ledger.amount) * -1
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        march_period = self.periods[2]
        march_age_analysis = AgeAnalysis.objects.get(supplier=ledger.supplier, period=march_period)

        assert march_age_analysis.balance == round_decimal(ledger.amount) * -1
        assert march_age_analysis.current == round_decimal(0)
        assert march_age_analysis.thirty_day == round_decimal(ledger.amount) * -1
        assert march_age_analysis.sixty_day == round_decimal(0)
        assert march_age_analysis.ninety_day == round_decimal(0)
        assert march_age_analysis.one_twenty_day == round_decimal(0)

        # App previous periods keeps their balances, but future periods will be cleared
        april_age_analysis = AgeAnalysis.objects.get(supplier=ledger.supplier, period=payment_period)

        assert april_age_analysis.balance == round_decimal('-6000.00')
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.thirty_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal('-6000.00')
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)

        may = self.periods[4]
        may_age_analysis = AgeAnalysis.objects.get(supplier=ledger.supplier, period=may)
        assert may_age_analysis.balance == round_decimal('-6000.00')
        assert may_age_analysis.current == round_decimal(0)
        assert may_age_analysis.thirty_day == round_decimal(0)
        assert may_age_analysis.sixty_day == round_decimal(0)
        assert may_age_analysis.ninety_day == round_decimal('-6000.00')
        assert may_age_analysis.one_twenty_day == round_decimal(0)

        june = self.periods[5]
        june_age_analysis = AgeAnalysis.objects.get(supplier=ledger.supplier, period=june)
        assert june_age_analysis.balance == round_decimal('-6000.00')
        assert june_age_analysis.current == round_decimal(0)
        assert june_age_analysis.thirty_day == round_decimal(0)
        assert june_age_analysis.sixty_day == round_decimal(0)
        assert june_age_analysis.ninety_day == round_decimal(0)
        assert june_age_analysis.one_twenty_day == round_decimal('-6000.00')

    def test_age_payment(self):
        period = self.periods[1]
        amount = 9000.00
        supplier = factories.SupplierFactory(company=self.company)
        payment = factories.PaymentFactory(period=period, company=self.company, profile=self.profile,
                                           total_amount=amount, supplier=supplier)

        age_services.add_payment_to_age_analysis(payment)
        # Only added to age on the second month, thus we have 11 periods its aged
        assert AgeAnalysis.objects.count() == 11

        unallocated_amount = round_decimal(amount) * -1

        current_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=period)
        assert current_age_analysis.balance == unallocated_amount
        assert current_age_analysis.unallocated == unallocated_amount
        assert current_age_analysis.current == round_decimal(0)
        assert current_age_analysis.one_twenty_day == round_decimal(0)
        assert current_age_analysis.ninety_day == round_decimal(0)
        assert current_age_analysis.sixty_day == round_decimal(0)
        assert current_age_analysis.thirty_day == round_decimal(0)

        march = self.periods[2]
        march_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=march)
        assert march_age_analysis.balance == unallocated_amount
        assert march_age_analysis.unallocated == unallocated_amount
        assert march_age_analysis.current == round_decimal(0)
        assert march_age_analysis.thirty_day == round_decimal(0)
        assert march_age_analysis.sixty_day == round_decimal(0)
        assert march_age_analysis.ninety_day == round_decimal(0)
        assert march_age_analysis.one_twenty_day == round_decimal(0)

        april = self.periods[3]
        april_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=april)
        assert april_age_analysis.balance == unallocated_amount
        assert april_age_analysis.unallocated == unallocated_amount
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.thirty_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal(0)
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)

        may = self.periods[4]
        may_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=may)
        assert may_age_analysis.balance == unallocated_amount
        assert may_age_analysis.unallocated == unallocated_amount
        assert may_age_analysis.current == round_decimal(0)
        assert may_age_analysis.thirty_day == round_decimal(0)
        assert may_age_analysis.sixty_day == round_decimal(0)
        assert may_age_analysis.ninety_day == round_decimal(0)
        assert may_age_analysis.one_twenty_day == round_decimal(0)

        june = self.periods[5]
        june_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=june)
        assert june_age_analysis.balance == unallocated_amount
        assert june_age_analysis.unallocated == unallocated_amount
        assert june_age_analysis.current == round_decimal(0)
        assert june_age_analysis.thirty_day == round_decimal(0)
        assert june_age_analysis.sixty_day == round_decimal(0)
        assert june_age_analysis.ninety_day == round_decimal(0)
        assert june_age_analysis.one_twenty_day == round_decimal(0)

        july = self.periods[5]
        july_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=july)
        assert july_age_analysis.balance == unallocated_amount
        assert july_age_analysis.unallocated == unallocated_amount
        assert july_age_analysis.current == round_decimal(0)
        assert july_age_analysis.thirty_day == round_decimal(0)
        assert july_age_analysis.sixty_day == round_decimal(0)
        assert july_age_analysis.ninety_day == round_decimal(0)
        assert july_age_analysis.one_twenty_day == round_decimal(0)

    def test_clear_unallocated_amount_with_payment(self):
        period = self.periods[1]
        amount = 9000.00
        supplier = factories.SupplierFactory(company=self.company)
        unallocated_payment = factories.PaymentFactory(period=period, company=self.company, profile=self.profile,
                                                       total_amount=amount, supplier=supplier)

        age_services.add_payment_to_age_analysis(payment=unallocated_payment)
        # Only added to age on the second month, thus we have 11 periods its aged
        assert AgeAnalysis.objects.count() == 11

        unallocated_amount = round_decimal(amount) * -1

        current_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=period)
        assert current_age_analysis.balance == unallocated_amount
        assert current_age_analysis.unallocated == unallocated_amount
        assert current_age_analysis.current == round_decimal(0)
        assert current_age_analysis.one_twenty_day == round_decimal(0)
        assert current_age_analysis.ninety_day == round_decimal(0)
        assert current_age_analysis.sixty_day == round_decimal(0)
        assert current_age_analysis.thirty_day == round_decimal(0)

        payment = factories.PaymentFactory(period=period, company=self.company, profile=self.profile,
                                           total_amount=0, supplier=supplier)

        child_payment = factories.ChildPaymentFactory(payment=unallocated_payment, parent=payment)

        age_services.clear_unallocated_on_age_analysis(child_payment=child_payment)

        current_age_analysis.refresh_from_db()
        assert current_age_analysis.balance == round_decimal(0)
        assert current_age_analysis.unallocated == round_decimal(0)
        assert current_age_analysis.current == round_decimal(0)
        assert current_age_analysis.one_twenty_day == round_decimal(0)
        assert current_age_analysis.ninety_day == round_decimal(0)
        assert current_age_analysis.sixty_day == round_decimal(0)
        assert current_age_analysis.thirty_day == round_decimal(0)

        march = self.periods[2]
        # TODO - Make one query and get from list
        march_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=march)
        assert march_age_analysis.balance == round_decimal(0)
        assert march_age_analysis.unallocated == round_decimal(0)
        assert march_age_analysis.current == round_decimal(0)
        assert march_age_analysis.thirty_day == round_decimal(0)
        assert march_age_analysis.sixty_day == round_decimal(0)
        assert march_age_analysis.ninety_day == round_decimal(0)
        assert march_age_analysis.one_twenty_day == round_decimal(0)

        april = self.periods[3]
        april_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=april)
        assert april_age_analysis.balance == round_decimal(0)
        assert april_age_analysis.unallocated == round_decimal(0)
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.thirty_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal(0)
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)

        may = self.periods[4]
        may_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=may)
        assert may_age_analysis.balance == round_decimal(0)
        assert may_age_analysis.unallocated == round_decimal(0)
        assert may_age_analysis.current == round_decimal(0)
        assert may_age_analysis.thirty_day == round_decimal(0)
        assert may_age_analysis.sixty_day == round_decimal(0)
        assert may_age_analysis.ninety_day == round_decimal(0)
        assert may_age_analysis.one_twenty_day == round_decimal(0)

        june = self.periods[5]
        june_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=june)
        assert june_age_analysis.balance == round_decimal(0)
        assert june_age_analysis.unallocated == round_decimal(0)
        assert june_age_analysis.current == round_decimal(0)
        assert june_age_analysis.thirty_day == round_decimal(0)
        assert june_age_analysis.sixty_day == round_decimal(0)
        assert june_age_analysis.ninety_day == round_decimal(0)
        assert june_age_analysis.one_twenty_day == round_decimal(0)

        july = self.periods[5]
        july_age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=july)
        assert july_age_analysis.balance == round_decimal(0)
        assert july_age_analysis.unallocated == round_decimal(0)
        assert july_age_analysis.current == round_decimal(0)
        assert july_age_analysis.thirty_day == round_decimal(0)
        assert july_age_analysis.sixty_day == round_decimal(0)
        assert july_age_analysis.ninety_day == round_decimal(0)
        assert july_age_analysis.one_twenty_day == round_decimal(0)

    def test_pay_multiple_invoice_in_different_periods(self):
        january_period = self.periods[0]
        march_period = self.periods[2]
        april_period = self.periods[3]

        supplier = factories.SupplierFactory(code='CEM001', name='Cemza (Pty) Ltd', company=self.company)
        jan_invoice = factories.InvoiceFactory(period=january_period, company=self.company, total_amount=1000.00,
                                               supplier=supplier)
        march_invoice = factories.InvoiceFactory(period=march_period, company=self.company, total_amount=40761.57,
                                                 supplier=supplier)
        april_invoice = factories.InvoiceFactory(period=april_period, company=self.company, total_amount=38233.48,
                                                 supplier=supplier)

        age_services.add_invoice_to_age_analysis(jan_invoice)
        age_services.add_invoice_to_age_analysis(march_invoice)
        age_services.add_invoice_to_age_analysis(april_invoice)

        assert AgeAnalysis.objects.count() == 12

        # January
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=january_period)
        assert age_analysis.balance == round_decimal(jan_invoice.open_amount)
        assert age_analysis.current == round_decimal(jan_invoice.open_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        # Feb
        feb_period = self.periods[1]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=feb_period)
        assert age_analysis.balance == round_decimal(jan_invoice.open_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(jan_invoice.open_amount)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # March
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=march_period)
        march_total = march_invoice.open_amount + jan_invoice.open_amount
        assert age_analysis.balance == round_decimal(march_total)
        assert age_analysis.current == round_decimal(march_invoice.open_amount)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(jan_invoice.open_amount)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # April
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=april_period)
        april_total = march_total + april_invoice.open_amount
        assert age_analysis.balance == round_decimal(april_total)
        assert age_analysis.current == round_decimal(april_invoice.open_amount)
        assert age_analysis.thirty_day == round_decimal(march_invoice.open_amount)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(jan_invoice.open_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # May
        may_period = self.periods[4]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=may_period)
        assert age_analysis.balance == round_decimal(april_total)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(april_invoice.open_amount)
        assert age_analysis.sixty_day == round_decimal(march_invoice.open_amount)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(jan_invoice.open_amount)

        # June
        june_period = self.periods[5]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=june_period)
        assert age_analysis.balance == round_decimal(april_total)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(april_invoice.open_amount)
        assert age_analysis.ninety_day == round_decimal(march_invoice.open_amount)
        assert age_analysis.one_twenty_day == round_decimal(jan_invoice.open_amount)

        # July
        july_period = self.periods[6]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=july_period)
        assert age_analysis.balance == round_decimal(april_total)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(april_invoice.open_amount)
        assert age_analysis.one_twenty_day == round_decimal(march_total)

        # August
        aug_period = self.periods[7]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=aug_period)
        assert age_analysis.balance == round_decimal(april_total)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(april_total)

        # Sept - Dec
        sep_period = self.periods[8]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=sep_period)
        assert age_analysis.balance == round_decimal(april_total)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(april_total)

        # Make Payment of all the invoices
        payment_period = april_period
        payment_amount = april_total
        payment = factories.PaymentFactory(period=payment_period, company=self.company, profile=self.profile,
                                           total_amount=payment_amount, supplier=supplier)
        jan_invoice_payment = factories.InvoicePaymentFactory(payment=payment, invoice=jan_invoice,
                                                              allocated=jan_invoice.open_amount)
        march_invoice_payment = factories.InvoicePaymentFactory(payment=payment, invoice=march_invoice,
                                                                allocated=march_invoice.open_amount)
        april_invoice_payment = factories.InvoicePaymentFactory(payment=payment, invoice=april_invoice,
                                                                allocated=april_invoice.open_amount)

        age_services.add_invoice_payment_to_age_analysis(invoice_payment=jan_invoice_payment)
        age_services.add_invoice_payment_to_age_analysis(invoice_payment=march_invoice_payment)
        age_services.add_invoice_payment_to_age_analysis(invoice_payment=april_invoice_payment)

        assert AgeAnalysis.objects.count() == 12

        # January
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=january_period)
        assert age_analysis.balance == round_decimal(jan_invoice.open_amount)
        assert age_analysis.current == round_decimal(jan_invoice.open_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        # Feb
        feb_period = self.periods[1]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=feb_period)
        assert age_analysis.balance == round_decimal(jan_invoice.open_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(jan_invoice.open_amount)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # March
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=march_period)
        march_total = march_invoice.open_amount + jan_invoice.open_amount
        assert age_analysis.balance == round_decimal(march_total)
        assert age_analysis.current == round_decimal(march_invoice.open_amount)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(jan_invoice.open_amount)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # April
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=april_period)
        april_total = march_total + april_invoice.open_amount
        assert age_analysis.balance == round_decimal(0)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # May
        may_period = self.periods[4]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=may_period)
        assert age_analysis.balance == round_decimal(0)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # June
        june_period = self.periods[5]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=june_period)
        assert age_analysis.balance == round_decimal(0)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # July
        july_period = self.periods[6]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=july_period)
        assert age_analysis.balance == round_decimal(0)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # August
        aug_period = self.periods[7]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=aug_period)
        assert age_analysis.balance == round_decimal(0)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        # Sept - Dec
        sep_period = self.periods[8]
        age_analysis = AgeAnalysis.objects.get(supplier=supplier, period=sep_period)
        assert age_analysis.balance == round_decimal(0)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)
