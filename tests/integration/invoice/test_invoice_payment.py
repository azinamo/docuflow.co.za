from datetime import date
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse

from docuflow.apps.invoice.models import Payment, InvoicePayment
from docuflow.apps.supplier.enums import LedgerStatus
from docuflow.apps.supplier.models import Ledger as SupplierLedger, AgeAnalysis, LedgerPayment
from docuflow.apps.supplier import services as supplier_services
from tests.base import AuthenticatedTestCase
from tests import factories
from tests import utils


def serialize_payment(payment: Payment):
    pass


class TestInvoicePayment(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_can_list_invoice_payments(self):
        payment_1 = factories.PaymentFactory(
            profile=self.profile, role=self.role,
            company=self.company, supplier=factories.SupplierFactory(company=self.company, code='0001'),
            account=factories.AccountFactory(company=self.company, name='Account 1', code='0001')
        )
        payment_2 = factories.PaymentFactory(
            profile=self.profile, role=self.role,
            company=self.company, supplier=factories.SupplierFactory(company=self.company, code='0002'),
            account=factories.AccountFactory(company=self.company, name='Account 2', code='0002')
        )
        payment_3 = factories.PaymentFactory(
            profile=self.profile, role=self.role,
            company=self.company, supplier=factories.SupplierFactory(company=self.company, code='0003'),
            account=factories.AccountFactory(company=self.company, name='Account 3', code='0003')
        )

        response = self.ajax_get(reverse('purchases-payments-list'))
        assert response.status_code == 200

    def test_can_create_invoice_payments(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001')
        account = factories.AccountFactory(company=self.company, name='Account 3', code='0003')
        payment_method = factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year, company=self.company)

        data = {
            'account': account.pk,
            'payment_method': payment_method.pk,
            'period': period.pk,
            'payment_date': period.from_date,
            'supplier': supplier.pk
        }

        response = self.ajax_post(reverse('create_supplier_payment', args=(supplier.pk, )), data=data)

        assert response.status_code == 200
        payment = Payment.objects.first()
        assert payment.supplier.pk == supplier.pk
        assert payment.account.pk == account.pk
        assert payment.period.pk == period.pk

        supplier_ledger = SupplierLedger.objects.first()
        assert supplier_ledger.object_id == payment.id
        assert supplier_ledger.credit == payment.total_amount
        assert supplier_ledger.status == LedgerStatus.PENDING.value

        age_analysis = AgeAnalysis.objects.filter(period=period, supplier=supplier)
        assert age_analysis.count() == 0

    def test_can_pay_with_ledger_balance(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        account = factories.AccountFactory(company=self.company)
        payment_account = factories.AccountFactory(name='Nedbank - Current', code='8400/000', company=self.company)
        payment_method = factories.PaymentMethodFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=factories.AccountFactory(company=self.company))
        ledger = factories.SupplierLedgerFactory(
            balance=3000, supplier=supplier, period=period, created_by=self.profile,
            journal_line=factories.JournalLineFactory(debit=3000, credit=0, account=account)
        )

        invoice_1 = factories.InvoiceFactory(period=period, total_amount=1685.00, supplier=supplier,
                                             company=self.company)
        invoice_2 = factories.InvoiceFactory(period=period, total_amount=1248.00, supplier=supplier,
                                             company=self.company)
        balance = ledger.balance - (invoice_1.total_amount + invoice_2.total_amount)
        data = {
            'supplier': supplier.pk,
            'invoices': (invoice_1.pk, invoice_2.pk),
            'payments': '',
            'ledgers': ledger.pk,
            'account': payment_account.pk,
            'period': period.pk,
            'payment_date': from_date,
            'payment_method': payment_method.pk,
            f'invoice_amount_{invoice_1.pk}': 1685.00,
            f'invoice_discount_{invoice_1.pk}': 25.28,
            f'invoice_amount_{invoice_2.pk}': 1248.00,
            f'invoice_discount_{invoice_2.pk}': 18.72,
            f'ledger_amount_{ledger.pk}': 2933.00,
            'total_amount': 0,
            'total_discount': 0,
            'discount_disallowed': 0,
            'vat_code': vat_code.pk,
            'net_amount': 0,
            'reference': 'Testing'
        }
        self.client.session['pay_supplier_invoices'] = [invoice_1.pk, invoice_2.pk]
        self.client.session['selected_payments'] = []
        self.client.session['selected_ledgers'] = [ledger.pk]

        response = self.ajax_post(reverse('create_supplier_payment', kwargs={'supplier_id': supplier.pk}), data=data)

        assert response.status_code == 200
        ledger.refresh_from_db()
        assert ledger.balance == balance
        payment = Payment.objects.filter(company=self.company).first()
        assert payment.total_amount == 0

        assert 1 == SupplierLedger.objects.filter(
            supplier=supplier, content_type=ContentType.objects.get_for_model(payment), object_id=payment.pk
        ).count()

        # Invoices updated
        invoice_1.refresh_from_db()
        invoice_2.refresh_from_db()
        assert invoice_2.open_amount == 0
        assert invoice_1.open_amount == 0
        assert InvoicePayment.objects.filter(invoice=invoice_1, payment=payment).count() == 1
        assert InvoicePayment.objects.filter(invoice=invoice_2, payment=payment).count() == 1

    def test_can_pay_invoice_with_less_ledger_balance(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        account = factories.AccountFactory(company=self.company)
        payment_account = factories.AccountFactory(name='Nedbank - Current', code='8400/000', company=self.company)
        payment_method = factories.PaymentMethodFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=factories.AccountFactory(company=self.company))
        ledger = factories.SupplierLedgerFactory(
            balance=5000, supplier=supplier, period=period, created_by=self.profile,
            journal_line=factories.JournalLineFactory(credit=5000, debit=0, account=account)
        )

        invoice_1 = factories.InvoiceFactory(period=period, total_amount=10000.00, supplier=supplier, company=self.company)
        data = {
            'supplier': supplier.pk,
            'invoices': invoice_1.pk,
            'payments': '',
            'ledgers': ledger.pk,
            'account': payment_account.pk,
            'period': period.pk,
            'payment_date': from_date,
            'payment_method': payment_method.pk,
            f'invoice_amount_{invoice_1.pk}': ledger.balance,
            f'invoice_discount_{invoice_1.pk}': 0,
            f'ledger_amount_{ledger.pk}': ledger.balance,
            'total_amount': ledger.balance,
            'total_discount': 0,
            'discount_disallowed': 0,
            'vat_code': vat_code.pk,
            'net_amount': ledger.balance,
            'reference': 'Testing'
        }
        self.client.session['pay_supplier_invoices'] = [invoice_1.pk]
        self.client.session['selected_payments'] = []
        self.client.session['selected_ledgers'] = [ledger.pk]

        response = self.ajax_post(reverse('create_supplier_payment', kwargs={'supplier_id': supplier.pk}), data=data)
        assert response.status_code == 200
        ledger.refresh_from_db()
        assert ledger.balance == 0
        payment = Payment.objects.filter(company=self.company).first()
        assert payment.total_amount == data['total_amount']
        assert payment.net_amount == data['net_amount']

        assert 1 == SupplierLedger.objects.filter(
            supplier=supplier, content_type=ContentType.objects.get_for_model(payment), object_id=payment.pk
        ).count()

        # Invoices updated
        ledger.refresh_from_db()
        invoice_1.refresh_from_db()
        assert invoice_1.open_amount == 5000
        assert InvoicePayment.objects.filter(invoice=invoice_1, payment=payment).count() == 1

    def test_create_note_payment(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        factories.AccountFactory(company=self.company)
        payment_account = factories.AccountFactory(name='Nedbank - Current', code='8400/000', company=self.company)
        payment_method = factories.PaymentMethodFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=factories.AccountFactory(company=self.company))
        invoice_type = factories.TaxInvoiceTypeFactory(credit_note=True)

        invoice = factories.InvoiceFactory(invoice_type=invoice_type, period=period, total_amount=-10000.00,
                                           supplier=supplier, company=self.company)
        data = {
            'supplier': supplier.pk,
            'invoices': invoice.pk,
            'payments': '',
            'ledgers': '',
            'account': payment_account.pk,
            'period': period.pk,
            'payment_date': from_date,
            'payment_method': payment_method.pk,
            f'invoice_amount_{invoice.pk}': invoice.open_amount,
            f'invoice_discount_{invoice.pk}': 0,
            'total_amount': invoice.open_amount,
            'total_discount': 0,
            'discount_disallowed': 0,
            'vat_code': vat_code.pk,
            'net_amount': invoice.open_amount,
            'reference': 'Testing'
        }
        self.client.session['pay_supplier_invoices'] = [invoice.pk]
        self.client.session['selected_payments'] = []
        self.client.session['selected_ledgers'] = []

        response = self.ajax_post(reverse('create_supplier_payment', kwargs={'supplier_id': supplier.pk}), data=data)
        assert response.status_code == 200
        payment = Payment.objects.filter(company=self.company).first()
        assert payment.total_amount == data['total_amount']
        assert payment.net_amount == data['net_amount']

        assert 1 == SupplierLedger.objects.filter(
            supplier=supplier, content_type=ContentType.objects.get_for_model(payment), object_id=payment.pk
        ).count()

        # Invoices updated
        invoice.refresh_from_db()
        assert invoice.open_amount == 0
        assert InvoicePayment.objects.filter(invoice=invoice, payment=payment).count() == 1

    def test_pay_less_on_supplier_credit_note_payment(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        account = factories.AccountFactory(company=self.company)
        payment_account = factories.AccountFactory(name='Nedbank - Current', code='8400/000', company=self.company)
        payment_method = factories.PaymentMethodFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=factories.AccountFactory(company=self.company))
        invoice_type = factories.TaxInvoiceTypeFactory(credit_note=True)

        invoice = factories.InvoiceFactory(invoice_type=invoice_type, period=period, total_amount=-10000.00,
                                           supplier=supplier, company=self.company)
        payment_amount = invoice.open_amount + 1000
        data = {
            'supplier': supplier.pk,
            'invoices': invoice.pk,
            'payments': '',
            'ledgers': '',
            'account': payment_account.pk,
            'period': period.pk,
            'payment_date': from_date,
            'payment_method': payment_method.pk,
            f'invoice_amount_{invoice.pk}': payment_amount,
            f'invoice_discount_{invoice.pk}': 0,
            'total_amount': payment_amount,
            'total_discount': 0,
            'discount_disallowed': 0,
            'vat_code': vat_code.pk,
            'net_amount': payment_amount,
            'reference': 'Testing'
        }

        self.client.session['pay_supplier_invoices'] = [invoice.pk]
        self.client.session['selected_payments'] = []
        self.client.session['selected_ledgers'] = []

        response = self.ajax_post(reverse('create_supplier_payment', kwargs={'supplier_id': supplier.pk}), data=data)
        assert response.status_code == 200
        payment = Payment.objects.filter(company=self.company).first()
        assert payment.total_amount == Decimal(payment_amount)
        assert payment.net_amount == Decimal(payment_amount)

        assert 1 == SupplierLedger.objects.filter(
            supplier=supplier, content_type=ContentType.objects.get_for_model(payment), object_id=payment.pk
        ).count()

        # Invoices updated
        invoice.refresh_from_db()
        assert invoice.open_amount == Decimal(-1000)
        assert InvoicePayment.objects.filter(invoice=invoice, payment=payment).count() == 1

    def test_pay_less_on_supplier_invoice(self):
        supplier = factories.SupplierFactory(company=self.company, code='0001', name='Da')
        p = 1
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date, from_date=from_date,
                                         period_year=self.year, company=self.company)
        account = factories.AccountFactory(company=self.company)
        payment_account = factories.AccountFactory(name='Nedbank - Current', code='8400/000', company=self.company)
        payment_method = factories.PaymentMethodFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company, account=factories.AccountFactory(company=self.company))
        invoice_type = factories.TaxInvoiceTypeFactory(company=self.company)

        invoice = factories.InvoiceFactory(invoice_type=invoice_type, period=period, total_amount=10000.00, supplier=supplier,
                                           company=self.company)
        payment_amount = invoice.open_amount - 1000

        data = {
            'supplier': supplier.pk,
            'invoices': invoice.pk,
            'payments': '',
            'ledgers': '',
            'account': payment_account.pk,
            'period': period.pk,
            'payment_date': from_date,
            'payment_method': payment_method.pk,
            f'invoice_amount_{invoice.pk}': payment_amount,
            f'invoice_discount_{invoice.pk}': 0,
            'total_amount': payment_amount,
            'total_discount': 0,
            'discount_disallowed': 0,
            'vat_code': vat_code.pk,
            'net_amount': payment_amount,
            'reference': 'Testing'
        }

        self.client.session['pay_supplier_invoices'] = [invoice.pk]
        self.client.session['selected_payments'] = []
        self.client.session['selected_ledgers'] = []

        response = self.ajax_post(reverse('create_supplier_payment', kwargs={'supplier_id': supplier.pk}), data=data)
        assert response.status_code == 200
        payment = Payment.objects.filter(company=self.company).first()
        assert payment.total_amount == Decimal(payment_amount)
        assert payment.net_amount == Decimal(payment_amount)

        supplier_ledgers = SupplierLedger.objects.filter(
            supplier=supplier, content_type=ContentType.objects.get_for_model(payment), object_id=payment.pk
        ).all()

        # ledger for payment
        assert len(supplier_ledgers) == 1
        assert supplier_ledgers[0].debit == payment_amount
        assert supplier_ledgers[0].status == LedgerStatus.PENDING

        # Invoices updated
        invoice.refresh_from_db()
        assert invoice.open_amount == utils.to_decimal(value='1000')
        invoice_payments = InvoicePayment.objects.filter(invoice=invoice, payment=payment).all()
        assert len(invoice_payments) == 1
        assert invoice_payments[0].allocated == payment_amount

    def test_can_edit_invoice_payment(self):
        payment = factories.PaymentFactory(
            profile=self.profile, role=self.role, company=self.company,
            supplier=factories.SupplierFactory(company=self.company, code='0001'),
            account=factories.AccountFactory(company=self.company, name='Account 1', code='0001')
        )

        response = self.client.get(reverse('edit_payment', args=(payment.id, )))

        assert response.status_code == 200

    def test_can_update_invoice_payment(self):
        p = 4
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=30)
        april_period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                               from_date=from_date, period_year=self.year, company=self.company)

        p = 3
        from_date = date(month=p, year=self.year.year, day=1)
        to_date = date(month=p, year=self.year.year, day=31)
        march_period = factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                               from_date=from_date, period_year=self.year, company=self.company)

        payment = factories.PaymentFactory(
            payment_date=april_period.from_date, period=april_period,
            profile=self.profile, role=self.role, company=self.company,
            payment_method=factories.PaymentMethodFactory(company=self.company, name='Bank'),
            supplier=factories.SupplierFactory(company=self.company, code='0001'),
            account=factories.AccountFactory(company=self.company, name='Account 1', code='0001')
        )
        data = {
            'period': march_period.pk,
            'payment_date': march_period.from_date,
            'account': payment.account.pk,
            'payment_method': payment.payment_method.pk,
            'supplier': payment.supplier.pk
        }

        response = self.client.post(reverse('edit_payment', args=(payment.id, )), data=data, follow=True)

        assert response.status_code == 200

        payment.refresh_from_db()
        assert payment.period.pk == march_period.pk
        assert payment.payment_date == march_period.from_date
        assert payment.account.pk == data['account']
        assert payment.payment_method.pk == data['payment_method']
        assert payment.supplier.pk == data['supplier']

    def test_delete_payment(self):
        payment_method = factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year, company=self.company)
        supplier = factories.SupplierFactory(company=self.company)

        posted_printed = factories.StatusFactory(posted_printed=True)
        final_signed = factories.StatusFactory(final_signed=True)
        partially_paid = factories.StatusFactory(partially_paid=True)
        paid = factories.StatusFactory(paid=True)
        paid_not_printed = factories.StatusFactory(paid_not_printed=True)
        paid_not_account_posted = factories.StatusFactory(paid_not_account_posted=True)
        invoice1 = factories.InvoiceFactory(
            company=self.company, status=paid_not_printed, supplier=supplier, period=period, total_amount=1797.45
        )
        invoice2 = factories.InvoiceFactory(
            company=self.company, status=paid_not_account_posted, supplier=supplier, period=period, total_amount=3292.28
        )
        invoice3 = factories.InvoiceFactory(
            company=self.company, status=partially_paid, supplier=supplier, period=period, total_amount=724.67
        )
        invoice4 = factories.InvoiceFactory(
            company=self.company,  status=paid, supplier=supplier, period=period, total_amount=64.15
        )
        invoice5 = factories.InvoiceFactory(
            company=self.company, status=paid_not_printed, supplier=supplier, period=period, total_amount=3785.78
        )
        payment = factories.PaymentFactory(supplier=supplier, company=self.company, period=period,
                                           payment_method=payment_method, profile=self.profile,
                                           invoices=[invoice1, invoice2, invoice3, invoice4, invoice5]
                                           )
        response = self.client.delete(reverse('delete_payment', args=(payment.pk, )), follow=True)

        assert response.status_code == 200

        invoice1.refresh_from_db()
        invoice2.refresh_from_db()
        invoice3.refresh_from_db()
        invoice4.refresh_from_db()
        invoice5.refresh_from_db()

        assert invoice1.status == posted_printed
        assert invoice2.status == final_signed
        assert invoice3.status == posted_printed
        assert invoice4.status == posted_printed
        assert invoice5.status == posted_printed

    def test_delete_payment_with_journal_payment(self):
        payment_method = factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year, company=self.company)
        supplier = factories.SupplierFactory(company=self.company)

        factories.StatusFactory(posted_printed=True)
        factories.StatusFactory(final_signed=True)
        factories.StatusFactory(partially_paid=True)
        factories.StatusFactory(paid=True)
        factories.StatusFactory(paid_not_account_posted=True)
        posted_printed = factories.StatusFactory(posted_printed=True)
        paid_not_printed = factories.StatusFactory(paid_not_printed=True)
        invoice_amount = 10000
        invoice = factories.InvoiceFactory(
            company=self.company, status=paid_not_printed, supplier=supplier, period=period,
            total_amount=invoice_amount, open_amount=0
        )
        payment = factories.PaymentFactory(
            supplier=supplier, company=self.company, period=period, payment_method=payment_method,
            profile=self.profile, invoices=[invoice]
        )
        ledger = factories.SupplierLedgerFactory(supplier=supplier, period=period, created_by=self.profile, balance=0)
        factories.SupplierLedgerPaymentFactory(payment=payment, ledger=ledger, amount=invoice_amount)
        response = self.client.delete(reverse('delete_payment', args=(payment.pk, )), follow=True)

        assert response.status_code == 200

        invoice.refresh_from_db()
        ledger.refresh_from_db()

        assert invoice.status == posted_printed
        assert LedgerPayment.objects.count() == 0
        assert SupplierLedger.objects.count() == 1
        assert ledger.balance == invoice_amount
        assert invoice.open_amount == invoice_amount
        assert invoice.status == posted_printed

    def test_payment_with_unallocated_payment_in_same_period(self):
        payment_method = factories.PaymentMethodFactory(company=self.company)
        period = factories.PeriodFactory(period_year=self.year, company=self.company)
        supplier = factories.SupplierFactory(company=self.company)

        factories.StatusFactory(posted_printed=True)
        factories.StatusFactory(final_signed=True)
        factories.StatusFactory(partially_paid=True)
        factories.StatusFactory(paid=True)
        factories.StatusFactory(paid_not_account_posted=True)
        posted_printed = factories.StatusFactory(posted_printed=True)
        paid_not_printed = factories.StatusFactory(paid_not_printed=True)
        invoice_amount = 10000
        invoice = factories.InvoiceFactory(
            company=self.company, status=paid_not_printed, supplier=supplier, period=period,
            total_amount=invoice_amount, open_amount=0
        )
        unallocated_payment = factories.PaymentFactory(
            supplier=supplier, company=self.company, period=period, payment_method=payment_method, profile=self.profile
        )
        # supplier_services.add
        payment = factories.PaymentFactory(
            supplier=supplier, company=self.company, period=period, payment_method=payment_method,
            profile=self.profile, invoices=[invoice]
        )
        ledger = factories.SupplierLedgerFactory(supplier=supplier, period=period, created_by=self.profile, balance=0)
        factories.SupplierLedgerPaymentFactory(payment=payment, ledger=ledger, amount=invoice_amount)
        response = self.client.delete(reverse('delete_payment', args=(payment.pk, )), follow=True)

        assert response.status_code == 200

        invoice.refresh_from_db()
        ledger.refresh_from_db()

        assert invoice.status == posted_printed
        assert LedgerPayment.objects.count() == 0
        assert SupplierLedger.objects.count() == 1
        assert ledger.balance == invoice_amount
        assert invoice.open_amount == invoice_amount
        assert invoice.status == posted_printed

    def test_pay_invoice_and_credit_note_in_one_transaction(self):
        periods = []
        for p in range(1, 13):
            from_date = date(month=p, year=self.year.year, day=1)
            to_date = date(month=p, year=self.year.year, day=20)
            periods.append(
                factories.PeriodFactory(period=p, name=from_date.strftime('%B'), to_date=to_date,
                                        from_date=from_date, period_year=self.year, company=self.company)
            )

        supplier = factories.SupplierFactory(company=self.company, code='SAF001', name='Safetymate Port')

        factories.StatusFactory(posted_printed=True)
        factories.StatusFactory(final_signed=True)
        factories.StatusFactory(partially_paid=True)
        factories.StatusFactory(paid=True)
        factories.StatusFactory(paid_not_account_posted=True)
        posted_printed = factories.StatusFactory(posted_printed=True)
        paid_not_printed = factories.StatusFactory(paid_not_printed=True)

        credit_note_type = factories.CreditInvoiceTypeFactory(company=self.company)

        invoice_amount = 411.20
        invoice = factories.InvoiceFactory(
            company=self.company, status=posted_printed, supplier=supplier, period=periods[5],
            total_amount=invoice_amount, accounting_date=periods[5].from_date, invoice_date=periods[5].from_date
        )
        invoice_2 = factories.InvoiceFactory(
            company=self.company, status=posted_printed, supplier=supplier, period=periods[6], total_amount=499.90,
            accounting_date=periods[6].from_date, invoice_date=periods[6].from_date
        )
        invoice_3 = factories.InvoiceFactory(
            company=self.company, status=posted_printed, supplier=supplier, period=periods[6], total_amount=179.40,
            accounting_date=periods[6].from_date, invoice_date=periods[6].from_date
        )

        credit_note_amount = -411.20
        credit_note = factories.InvoiceFactory(
            company=self.company, status=posted_printed, supplier=supplier, period=periods[6],
            invoice_type=credit_note_type, total_amount=credit_note_amount, accounting_date=periods[6].from_date,
            invoice_date=periods[6].from_date
        )

        june_age_analysis = factories.SupplierAgeAnalysisFactory(
            supplier=supplier, period=periods[5], balance=invoice_amount, current=invoice_amount,
            one_twenty_day=0, ninety_day=0, sixty_day=0, thirty_day=0, unallocated=0
        )
        july_age_analysis = factories.SupplierAgeAnalysisFactory(
            supplier=supplier, period=periods[6], balance=679.2, current=268.1,
            one_twenty_day=0, ninety_day=0, sixty_day=0, thirty_day=invoice_amount, unallocated=0
        )
        aug_age_analysis = factories.SupplierAgeAnalysisFactory(
            supplier=supplier, period=periods[7], balance=679.2, current=0,
            one_twenty_day=0, ninety_day=0, sixty_day=invoice_amount, thirty_day=268.1, unallocated=0
        )
        sept_age_analysis = factories.SupplierAgeAnalysisFactory(
            supplier=supplier, period=periods[8], balance=679.2, current=0,
            one_twenty_day=0, ninety_day=invoice_amount, sixty_day=268.1, thirty_day=0, unallocated=0
        )
        oct_age_analysis = factories.SupplierAgeAnalysisFactory(
            supplier=supplier, period=periods[9], balance=679.2, current=0,
            one_twenty_day=invoice_amount, ninety_day=268.1, sixty_day=0, thirty_day=0, unallocated=0
        )
        nov_age_analysis = factories.SupplierAgeAnalysisFactory(
            supplier=supplier, period=periods[10], balance=679.2, current=0,
            one_twenty_day=679.2, ninety_day=0, sixty_day=0, thirty_day=0, unallocated=0
        )

        # supplier ledgers
        ledger_1 = factories.SupplierLedgerFactory(
            supplier=supplier, period=periods[5], created_by=self.profile, balance=0, credit=invoice_amount, debit=0,
            content_object=invoice, object_id=invoice.pk)
        ledger_2 = factories.SupplierLedgerFactory(
            supplier=supplier, period=periods[6], created_by=self.profile, balance=0, credit=invoice_2.total_amount,
            debit=0, content_object=invoice_2, object_id=invoice_2.pk)
        ledger_3 = factories.SupplierLedgerFactory(
            supplier=supplier, period=periods[6], created_by=self.profile, balance=0, credit=invoice_3.total_amount,
            debit=0, content_object=invoice_3, object_id=invoice_3.pk)
        ledger_4 = factories.SupplierLedgerFactory(
            supplier=supplier, period=periods[6], created_by=self.profile, balance=0, credit=0,
            debit=credit_note.total_amount, content_object=credit_note, object_id=credit_note.pk)

        account = factories.AccountFactory(company=self.company, name='Nedbank Current', code='8400/0000')
        payment_method = factories.PaymentMethodFactory(company=self.company, name='Bank Transfer/EFT')
        vat_code = factories.VatCodeFactory(company=self.company)

        data = {
            f'invoice_amount_{credit_note.pk}': -411.20,
            f'invoice_discount_{credit_note.pk}': 0,
            f'invoice_amount_{invoice.pk}': 411.20,
            f'invoice_discount_{invoice.pk}': 0,
            'supplier': supplier.pk,
            'period': periods[7].pk,
            'payment_date': periods[7].to_date,
            'account': account.pk,
            'payment_method': payment_method.pk,
            'total_amount': 0.00,
            'total_discount': '',
            'discount_disallowed': '',
            'vat_code': vat_code.pk,
            'net_amount': 0.00,
            'reference': '',
            'attachment': '',
            'supplier_id': supplier.pk,
            'invoices': f'{invoice.pk}, {credit_note.pk}',
            'payments': '',
            'ledgers': ''
        }

        response = self.ajax_post(reverse('create_supplier_payment', args=(supplier.pk,)), data=data)

        assert response.status_code == 200
        invoice.refresh_from_db()
        credit_note.refresh_from_db()

        june_age_analysis.refresh_from_db()
        july_age_analysis.refresh_from_db()
        aug_age_analysis.refresh_from_db()
        sept_age_analysis.refresh_from_db()
        oct_age_analysis.refresh_from_db()
        nov_age_analysis.refresh_from_db()

        ledger_1.refresh_from_db()
        ledger_2.refresh_from_db()
        ledger_3.refresh_from_db()
        ledger_4.refresh_from_db()

        assert invoice.open_amount == utils.to_decimal(value=str(0))
        invoice_payments = InvoicePayment.objects.filter(invoice=invoice).all()
        assert len(invoice_payments) == 1
        assert invoice_payments[0].allocated == invoice.total_amount

        assert credit_note.open_amount == utils.to_decimal(value=str(0))
        credit_note_payments = InvoicePayment.objects.filter(invoice=credit_note).all()
        assert len(credit_note_payments) == 1
        assert credit_note_payments[0].allocated == credit_note.total_amount

        assert june_age_analysis.balance == invoice.total_amount
        assert june_age_analysis.current == invoice.total_amount
        assert june_age_analysis.thirty_day == 0
        assert june_age_analysis.sixty_day == 0
        assert june_age_analysis.ninety_day == 0
        assert june_age_analysis.one_twenty_day == 0
        assert june_age_analysis.unallocated == 0

        assert july_age_analysis.balance == utils.to_decimal(value='679.2')
        assert july_age_analysis.current == utils.to_decimal(value='268.1')
        assert july_age_analysis.thirty_day == invoice.total_amount
        assert july_age_analysis.sixty_day == 0
        assert july_age_analysis.ninety_day == 0
        assert july_age_analysis.one_twenty_day == 0
        assert july_age_analysis.unallocated == 0

        assert aug_age_analysis.balance == utils.to_decimal(value='679.2')
        assert aug_age_analysis.current == 0
        assert aug_age_analysis.thirty_day == utils.to_decimal(value='268.1')
        assert aug_age_analysis.sixty_day == utils.to_decimal(value='411.2')
        assert aug_age_analysis.ninety_day == 0
        assert aug_age_analysis.one_twenty_day == 0
        assert aug_age_analysis.unallocated == 0

        assert sept_age_analysis.balance == utils.to_decimal(value='679.2')
        assert sept_age_analysis.current == 0
        assert sept_age_analysis.thirty_day == 0
        assert sept_age_analysis.sixty_day == utils.to_decimal(value='268.1')
        assert sept_age_analysis.ninety_day == utils.to_decimal(value='411.2')
        assert sept_age_analysis.one_twenty_day == 0
        assert sept_age_analysis.unallocated == 0

        assert oct_age_analysis.balance == utils.to_decimal(value='679.2')
        assert oct_age_analysis.current == 0
        assert oct_age_analysis.thirty_day == 0
        assert oct_age_analysis.sixty_day == 0
        assert oct_age_analysis.ninety_day == utils.to_decimal(value='268.1')
        assert oct_age_analysis.one_twenty_day == utils.to_decimal(value='411.2')
        assert oct_age_analysis.unallocated == 0

        assert nov_age_analysis.balance == utils.to_decimal(value='679.2')
        assert nov_age_analysis.current == 0
        assert nov_age_analysis.thirty_day == 0
        assert nov_age_analysis.sixty_day == 0
        assert nov_age_analysis.ninety_day == 0
        assert nov_age_analysis.one_twenty_day == utils.to_decimal(value='679.2')
        assert nov_age_analysis.unallocated == 0

        assert ledger_1.credit == utils.to_decimal(value=invoice.total_amount)
        assert ledger_2.credit == utils.to_decimal(value=invoice_2.total_amount)
        assert ledger_3.credit == utils.to_decimal(value=invoice_3.total_amount)
        assert ledger_4.debit == utils.to_decimal(value=credit_note.total_amount)
