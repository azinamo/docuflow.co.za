﻿from dateutil.relativedelta import relativedelta
from datetime import timedelta

from django.core.management import call_command
from django.urls import reverse
from django.utils.timezone import now


from docuflow.apps.invoice.models import Invoice, InvoiceAccount, FlowRole, Flow
from docuflow.apps.invoice.enums import InvoiceStatus
from docuflow.apps.supplier.models import Ledger as SupplierLedger
from tests.base import AuthenticatedTestCase
from tests import utils, factories


class TestPurchaseInvoiceWithAccountPostingViews(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()
        call_command('loaddata', 'fixtures/invoice_flowstatus', verbosity=0)
        call_command('loaddata', 'fixtures/invoice_status', verbosity=0)

    def test_can_list_role_invoices(self):
        period = factories.PeriodFactory(period_year=self.year)
        workflow = factories.WorkflowFactory(
            company=self.company, created_by=self.profile.user, levels=[factories.WorkflowLevelFactory(roles=[self.accountant])]
        )

        invoices = factories.InvoiceFactory.create_batch(
            5, company=self.company, period=period, add_to_flow={
                'profile': self.profile, 'role': self.accountant
            }, workflow=workflow, status=factories.LoggerStatusFactory
        )

        response = self.client.get(reverse('invoices_list'))

        assert response.status_code == 200
        for i in invoices:
            assert str(i.invoice_number) in response.content.decode()

    def test_create_invoice_validations(self):
        pass

    def test_can_create_purchase_tax_invoice(self):
        period = factories.PeriodFactory(
            company=self.company, period_year=self.year, opened=True
        )
        workflow = factories.WorkflowFactory(
            company=self.company, created_by=self.profile.user, levels=[factories.WorkflowLevelFactory(roles=[self.accountant])]
        )
        supplier = factories.SupplierFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        tax_invoice = factories.TaxInvoiceTypeFactory(company=self.company, is_account_posting=True)
        data = {
            'supplier_name': 'Matter',
            'supplier_number': 'M00001',
            'vat_number': '12344',
            'due_date': f'{now().year}-02-28',
            'accounting_date': f'{now().year}-01-20',
            'invoice_date': f'{now().year}-02-20',
            'vat_amount': 5000,
            'net_amount': 10000,
            'sub_total': 15000,
            'total_amount': 15000,
            'invoice_type': tax_invoice.pk,
            'comments': 'New invoice',
            'invoice_number': 'PEL00012',
            'period': period.pk,
            'image': 'file.pdf',
            'vat_code': vat_code.pk,
            'supplier': supplier.pk,
            'workflow': workflow.pk
        }

        response = self.ajax_post(url=reverse('create_invoice'), data=data)

        assert response.status_code == 200
        assert Invoice.objects.count() == 1
        invoice = Invoice.objects.first()
        assert '1' == invoice.invoice_id
        assert data['invoice_number'] == invoice.invoice_number
        assert data['supplier_number'] == invoice.supplier_number
        assert data['supplier_name'] == invoice.supplier_name
        assert data['total_amount'] == invoice.total_amount
        assert data['sub_total'] == invoice.sub_total
        assert data['vat_amount'] == invoice.vat_amount
        assert data['invoice_date'] == utils.format_datetime(invoice.invoice_date)
        assert data['accounting_date'] == utils.format_datetime(invoice.accounting_date)
        assert data['due_date'] == utils.format_datetime(invoice.due_date)
        assert data['workflow'] == invoice.workflow.pk
        assert data['vat_number'] == invoice.vat_number
        assert data['invoice_number'] == invoice.invoice_number
        assert data['comments'] == invoice.comments
        assert data['invoice_type'] == invoice.invoice_type.pk
        assert data['period'] == invoice.period.pk
        assert data['vat_code'] == invoice.vat_code.pk
        assert data['supplier'] == invoice.supplier.pk
        assert data['vat_number'] == invoice.supplier.vat_number
        assert InvoiceStatus.AT_LOGGER.value == invoice.status.slug

    def test_can_create_invoice_from_mail(self):
        pass

    def test_can_create_tax_invoice_and_add_to_flow(self):

        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)

        workflow = factories.WorkflowFactory(
            company=self.company, created_by=self.profile.user, levels=[factories.WorkflowLevelFactory(roles=[self.accountant])]
        )

        supplier = factories.SupplierFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        tax_invoice = factories.TaxInvoiceTypeFactory(company=self.company, is_account_posting=True)

        data = {
            'supplier_name': 'Matter',
            'supplier_number': 'M00001',
            'vat_number': '12344',
            'due_date': f'{now().year}-02-28',
            'accounting_date': f'{now().year}-01-20',
            'invoice_date': f'{now().year}-02-20',
            'vat_amount': 5000,
            'net_amount': 10000,
            'sub_total': 15000,
            'total_amount': 15000,
            'invoice_type': tax_invoice.pk,
            'comments': 'New invoice',
            'invoice_number': 'PEL00012',
            'period': period.pk,
            'image': 'file.pdf',
            'vat_code': vat_code.pk,
            'supplier': supplier.pk,
            'workflow': workflow.pk,
            'add_to_flow': True,
            'is_protected': 1
        }

        response = self.ajax_post(url=reverse('create_invoice'), data=data)

        assert response.status_code == 200
        assert Invoice.objects.count() == 1
        invoice = Invoice.objects.first()
        assert InvoiceStatus.IN_FLOW.value == invoice.status.slug
        invoice_account = InvoiceAccount.objects.filter(invoice=invoice, is_vat=False).first()
        vat_invoice_account = InvoiceAccount.objects.filter(invoice=invoice, is_vat=True).first()

        assert InvoiceAccount.objects.filter(invoice=invoice).count() == 2
        assert Flow.objects.filter(invoice=invoice).count() == 1
        assert FlowRole.objects.count() == 1
        assert data['vat_amount'] == vat_invoice_account.amount
        assert data['net_amount'] == invoice_account.amount
        assert invoice.is_protected is True

        assert SupplierLedger.objects.count() == 1
        supplier_ledger = SupplierLedger.objects.first()
        assert supplier_ledger.balance == 0
        assert supplier_ledger.debit == 0
        assert supplier_ledger.credit == invoice.total_amount
        assert supplier_ledger.supplier == invoice.supplier

    def test_can_update_invoice_without_vat_in_flow(self):
        period = factories.PeriodFactory(period_year=self.year, opened=True)
        invoice = factories.InvoiceFactory(
            invoice_type=factories.TaxInvoiceTypeFactory(
                company=self.company, file=factories.PurchaseFileTypeFactory(company=self.company)
            ),
            supplier=factories.SupplierFactory(company=self.company, name='Cavcon Manufacturing', code='1976/003'),
            invoice_number='003382', company=self.company, period=period, status=factories.StatusFactory(),
            open_amount='23500.00', total_amount='23500.00', net_amount='23500.00', vat_amount=0,
            accounting_date=period.from_date
        )

        data = {
            'invoice_type': invoice.invoice_type.pk,
            'period': period.id,
            'supplier': invoice.supplier.pk,
            'total_amount': '13868.71',
            'net_amount': '13868.71',
            'accounting_date': invoice.accounting_date,
            'invoice_date': invoice.accounting_date,
            'supplier_name': invoice.supplier.name,
            'vat_amount': 0,
            'due_date': period.from_date + timedelta(days=50),
            'invoice_number': invoice.invoice_number
        }

        response = self.ajax_post(reverse('update_invoice', args=(invoice.pk, )), data=data)

        invoice.refresh_from_db()

        assert response.status_code == 200

        assert data['total_amount'] == utils.format_decimal(invoice.total_amount)
        assert data['total_amount'] == utils.format_decimal(invoice.open_amount)
        assert data['total_amount'] == utils.format_decimal(invoice.sub_total)
        assert data['net_amount'] == utils.format_decimal(invoice.net_amount)

    def test_can_sign_invoice(self):
        period = factories.PeriodFactory(period_year=self.year)
        workflow = factories.WorkflowFactory(
            company=self.company, created_by=self.profile.user, levels=[factories.WorkflowLevelFactory(roles=[self.accountant])]
        )

        invoice = factories.InvoiceFactory(
            company=self.company, period=period, workflow=workflow, status=factories.LoggerStatusFactory
        )

        response = self.ajax_get(url=reverse('sign_invoice_changes', kwargs={'pk': invoice.pk}), data={})

        assert response.status_code == 200

    def test_cannot_complete_sign_invoice_in_flow(self):
        pass

    def test_cannot_sign_invoice_with_default_accounts(self):
        pass

    def test_update_invoice_supplier_for_document_in_flow(self):

        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        workflow = factories.WorkflowFactory(
            company=self.company, created_by=self.profile.user, levels=[factories.WorkflowLevelFactory(roles=[self.accountant])]
        )
        supplier = factories.SupplierFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        tax_invoice = factories.TaxInvoiceTypeFactory(company=self.company, is_account_posting=True)
        data = {
            'supplier_name': 'Matter',
            'supplier_number': 'M00001',
            'vat_number': '12344',
            'due_date': f'{now().year}-02-28',
            'accounting_date': f'{now().year}-01-20',
            'invoice_date': f'{now().year}-02-20',
            'vat_amount': 5000,
            'net_amount': 10000,
            'sub_total': 15000,
            'total_amount': 15000,
            'invoice_type': tax_invoice.pk,
            'comments': 'New invoice',
            'invoice_number': 'PEL00012',
            'period': period.pk,
            'image': 'file.pdf',
            'vat_code': vat_code.pk,
            'supplier': supplier.pk,
            'workflow': workflow.pk,
            'add_to_flow': True
        }

        response = self.ajax_post(url=reverse('create_invoice'), data=data)
        assert response.status_code == 200
        assert Invoice.objects.count() == 1

        invoice = Invoice.objects.first()

        assert SupplierLedger.objects.count() == 1
        supplier_ledger = SupplierLedger.objects.first()
        assert supplier_ledger.balance == 0
        assert supplier_ledger.debit == 0
        assert supplier_ledger.credit == invoice.total_amount
        assert supplier_ledger.supplier == invoice.supplier

        new_supplier = factories.SupplierFactory(company=self.company, name='Wire', code='WES002')
        data = {
            'invoice_type': tax_invoice.pk,
            'period': period.id,
            'supplier': new_supplier.pk,
            'total_amount': invoice.total_amount,
            'net_amount': invoice.net_amount,
            'accounting_date': invoice.accounting_date,
            'invoice_date': invoice.accounting_date,
            'supplier_name': invoice.supplier.name,
            'vat_amount': invoice.vat_amount,
            'due_date': invoice.due_date,
            'invoice_number': invoice.invoice_number
        }

        response = self.ajax_post(reverse('update_invoice', args=(invoice.pk, )), data=data)
        assert response.status_code == 200
        invoice.refresh_from_db()

        assert invoice.supplier == new_supplier

        assert SupplierLedger.objects.count() == 1
        supplier_ledger = SupplierLedger.objects.first()
        assert supplier_ledger.balance == 0
        assert supplier_ledger.debit == 0
        assert supplier_ledger.credit == invoice.total_amount
        assert supplier_ledger.supplier == invoice.supplier

    def test_update_invoice_invoice_paid_while_in_flow(self):
        period = factories.PeriodFactory(period_year=self.year, period=6, opened=True)
        supplier = factories.SupplierFactory(name='Woodlands Diary', code='W001', company=self.company)
        workflow = factories.WorkflowFactory(
            company=self.company, created_by=self.profile.user,
            levels=[factories.WorkflowLevelFactory(roles=[self.accountant])]
        )

        invoice_amount = 900
        in_flow_not_printed = factories.StatusFactory()
        invoice_type = factories.TaxInvoiceTypeFactory(company=self.company)
        invoice = factories.InvoiceFactory(
            company=self.company, period=period, workflow=workflow, status=in_flow_not_printed, supplier=supplier,
            total_amount=invoice_amount, vat_amount=0, open_amount=0, invoice_type=invoice_type
        )
        factories.InvoiceAccountFactory(
            invoice=invoice, is_vat=False, amount=invoice_amount, is_posted=True, profile=self.profile
        )
        factories.InvoiceAccountFactory(
            invoice=invoice, is_vat=False, amount=invoice_amount, is_posted=False, profile=self.profile
        )

        payment = factories.PaymentFactory(company=self.company, supplier=supplier, total_amount=invoice_amount,
                                           profile=self.profile, period=period)
        factories.InvoicePaymentFactory(payment=payment, invoice=invoice, allocated=invoice_amount)
        invoice_ledger = factories.SupplierLedgerFactory(
            created_by=self.profile, supplier=supplier, object_id=invoice.id, credit=invoice_amount,
            content_object=invoice, debit=0
        )
        payment_ledger = factories.SupplierLedgerFactory(
            created_by=self.profile, period=period, supplier=supplier, object_id=payment.id, debit=invoice_amount,
            content_object=payment, credit=0
        )

        data = {
            'supplier': supplier.pk,
            'supplier_name': supplier.name,
            'supplier_number': '123ßßßßßßß',
            'vat_number': 4560266209,
            'supplier_account_ref': 'Intle',
            'invoice_type': invoice_type.pk,
            'invoice_number': 'INV01431346',
            'reference1': 1,
            'reference2': 2,
            'period': period.pk,
            'accounting_date': f'{self.year.year}-06-22',
            'invoice_date': f'{self.year.year}-06-21',
            'due_date': f'{self.year.year}-06-30',
            'total_amount': invoice_amount,
            'vat_amount': 0,
            'vat_code': '',
            'net_amount': invoice_amount
        }

        response = self.ajax_post(url=reverse('update_invoice', kwargs={'pk': invoice.pk}), data=data)
        assert response.status_code == 200

        invoice_ledger.refresh_from_db()

        assert invoice_ledger.credit == invoice_amount


class TestPurchaseInvoiceWithoutAccountPosting(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(TestPurchaseInvoiceWithoutAccountPosting, self).setUp()
        call_command('loaddata', 'fixtures/invoice_flowstatus', verbosity=0)
        call_command('loaddata', 'fixtures/invoice_status', verbosity=0)

    def test_can_create_invoice_and_add_to_flow(self):
        period = factories.PeriodFactory(company=self.company, period_year=self.year, opened=True)
        workflow = factories.WorkflowFactory(
            company=self.company, created_by=self.profile.user, levels=[factories.WorkflowLevelFactory(roles=[self.accountant])]
        )

        supplier = factories.SupplierFactory(company=self.company)
        vat_code = factories.VatCodeFactory(company=self.company)
        tax_invoice = factories.TaxInvoiceTypeFactory(company=self.company, is_account_posting=False)
        data = {
            'supplier_name': 'Matter',
            'supplier_number': 'M00001',
            'vat_number': '12344',
            'due_date': f'{now().year}-02-28',
            'accounting_date': f'{now().year}-01-20',
            'invoice_date': f'{now().year}-02-20',
            'vat_amount': 5000,
            'net_amount': 10000,
            'sub_total': 15000,
            'total_amount': 15000,
            'invoice_type': tax_invoice.pk,
            'comments': 'New invoice',
            'invoice_number': 'PEL00012',
            'period': period.pk,
            'image': 'file.pdf',
            'vat_code': vat_code.pk,
            'supplier': supplier.pk,
            'workflow': workflow.pk,
            'add_to_flow': True
        }

        response = self.ajax_post(url=reverse('create_invoice'), data=data)

        assert response.status_code == 200
        assert Invoice.objects.count() == 1
        invoice = Invoice.objects.first()
        assert InvoiceStatus.IN_FLOW_PRINTED.value == invoice.status.slug
        vat_invoice_account = InvoiceAccount.objects.filter(invoice=invoice, is_vat=True).first()
        # assert InvoiceAccount.objects.filter(invoice=invoice).count() == 1 TODO - Ensure correct invoice accounts
        #  are posted
        assert Flow.objects.filter(invoice=invoice).count() == 1
        assert FlowRole.objects.count() == 1
        assert data['vat_amount'] == vat_invoice_account.amount

    def test_sign_invoice_with_invalid_period_and_accounting_date(self):

        period = factories.PeriodFactory(period=5, period_year=self.year)
        invoice = factories.InvoiceFactory(company=self.company)
        data = {
            'period': period,
            'accounting_date': period.from_date + relativedelta(months=-2),
            'invoice_date': period.from_date + relativedelta(months=-2),
            'due_date': period.from_date + relativedelta(months=-1)
        }

        response = self.ajax_post(url=reverse('sign_invoice_changes', args=(invoice.pk, )), data=data)

    def test_change_status_on_decline_requested_invoice(self):

        period = factories.PeriodFactory(period=5, period_year=self.year)
        invoice = factories.InvoiceFactory(company=self.company, total_amount=0, status=factories.StatusFactory(
            decline_requested=True
        ))
        processed = factories.FlowStatusFactory(name='Processed', slug='processed')
        processing = factories.FlowStatusFactory(name='Processing', slug='processing')

        manager_flow = factories.FlowFactory(invoice=invoice, status=processed, is_signed=True, sign_count=1,
                                             is_current=False)
        owner_flow = factories.FlowFactory(invoice=invoice, status=processing, is_signed=False, sign_count=0,
                                           is_current=True)

        factories.FlowRoleFactory(flow=manager_flow, role=self.manager, is_signed=True, status=2)
        factories.FlowRoleFactory(flow=owner_flow, role=self.accountant, is_signed=False)

        response = self.ajax_post(url=reverse('change_invoice_decline_status', args=(invoice.pk, )), data={})

        assert response.status_code == 200
        invoice.refresh_from_db()
        owner_flow.refresh_from_db()
        assert owner_flow.is_current is False
        assert owner_flow.is_signed is True
        assert owner_flow.status == processed
        assert owner_flow.sign_count == 1

        assert Flow.objects.count() == 3
        assert FlowRole.objects.count() == 3

        assert Flow.objects.filter(is_current=True).count() == 1
        assert invoice.status.slug == InvoiceStatus.IN_FLOW.value

    def test_decline_requested_approval_invoice(self):
        period = factories.PeriodFactory(period=5, period_year=self.year)
        invoice = factories.InvoiceFactory(company=self.company, total_amount=0, status=factories.StatusFactory(
            decline_requested=True
        ))
        processed = factories.FlowStatusFactory(name='Processed', slug='processed')
        processing = factories.FlowStatusFactory(name='Processing', slug='processing')

        manager_flow = factories.FlowFactory(invoice=invoice, status=processed, is_signed=True, sign_count=1,
                                             is_current=False)
        owner_flow = factories.FlowFactory(invoice=invoice, status=processing, is_signed=False, sign_count=0,
                                           is_current=True)

        factories.FlowRoleFactory(flow=manager_flow, role=self.manager, is_signed=True, status=2)
        factories.FlowRoleFactory(flow=owner_flow, role=self.accountant, is_signed=False)

        response = self.ajax_post(url=reverse('approve_invoice_decline', args=(invoice.pk, )), data={
            'open_amount': 0, 'reason': 'Decline reason'})
        assert response.status_code == 200
        invoice.refresh_from_db()
        owner_flow.refresh_from_db()
        manager_flow.refresh_from_db()
        assert owner_flow.is_current is False
        assert owner_flow.is_signed is True
        assert owner_flow.status == processed
        assert owner_flow.sign_count == 1

        assert manager_flow.is_current is False
        assert manager_flow.is_signed is True
        assert manager_flow.status == processed
        assert manager_flow.sign_count == 1

        assert Flow.objects.count() == 2
        assert FlowRole.objects.count() == 2

        assert Flow.objects.filter(is_current=True).count() == 0
        assert invoice.status.slug == InvoiceStatus.DECLINE_PRINTED.value

    def test_update_invoice_from_another_docuflow_company_sale_s_module(self):
        amount = 10000

        # Sales invoice
        new_company = factories.CompanyFactory(area_code='08052', name='Sales Company')
        sales_year = factories.OpenYearFactory(company=new_company)
        sales_period = factories.PeriodFactory(period=5, period_year=sales_year)

        new_company_branch = factories.BranchFactory(label='Sales Branch', company=new_company)
        sales_invoice = factories.SalesTaxInvoiceFactory(total_amount=amount, branch=new_company_branch,
                                                         period=sales_period, invoice_date=sales_period.from_date)

        period = factories.PeriodFactory(period=5, period_year=self.year)
        supplier = factories.SupplierFactory(code='099', company=self.company)
        invoice_type = factories.TaxInvoiceTypeFactory(code='099', company=self.company)
        invoice = factories.InvoiceFactory(company=self.company, total_amount=amount, sales_invoice=sales_invoice,
                                           supplier=supplier, invoice_type=invoice_type)
        processed = factories.FlowStatusFactory(name='Processed', slug='processed')
        processing = factories.FlowStatusFactory(name='Processing', slug='processing')

        manager_flow = factories.FlowFactory(invoice=invoice, status=processed, is_signed=True, sign_count=1,
                                             is_current=False)
        owner_flow = factories.FlowFactory(invoice=invoice, status=processing, is_signed=False, sign_count=0,
                                           is_current=True)

        factories.FlowRoleFactory(flow=manager_flow, role=self.manager, is_signed=True, status=2)
        factories.FlowRoleFactory(flow=owner_flow, role=self.accountant, is_signed=False)

        data = {
            'supplier': invoice.supplier.pk,
            'supplier_name': invoice.supplier.name,
            'supplier_number': '123ßßßßßßß',
            'vat_number': 4560266209,
            'supplier_account_ref': 'Intle',
            'invoice_type': invoice.invoice_type.pk,
            'invoice_number': 'INV01431346',
            'reference1': 1,
            'reference2': 2,
            'period': period.pk,
            'accounting_date': period.from_date,
            'invoice_date': period.from_date,
            'due_date': '2021-06-30',
            'total_amount': amount,
            'vat_amount': 0,
            'vat_code': '',
            'net_amount': amount,
            'sales_invoice': sales_invoice.pk
        }
        response = self.ajax_post(url=reverse('update_invoice', args=(invoice.pk, )), data=data)

        assert response.status_code == 200
        invoice.refresh_from_db()
        assert invoice.sales_invoice == sales_invoice

    def test_role_cannot_sign_invoice_not_in_their_flow(self):
        """
        Ensures that a role can only sign invoices which the invoice flow's role is the current selected role, if not raise an exception
        """
