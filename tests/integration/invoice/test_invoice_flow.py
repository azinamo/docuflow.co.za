from http import HTTPStatus

from django.urls import reverse

from docuflow.apps.invoice.models import FlowRole, Flow
from tests import factories
from tests.base import AuthenticatedTestCase


class TestInvoiceFlow(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.invoice = factories.InvoiceFactory(company=self.company)

        factories.FlowStatusFactory(slug='processing', name='Processing')

    def test_can_list_invoice_flows(self):
        director_role = factories.RoleFactory(company=self.company, name='Director')
        accountant_role = factories.RoleFactory(company=self.company, name='Accountant')
        purchaser_role = factories.RoleFactory(company=self.company, name='Purchaser')
        sales_role = factories.RoleFactory(company=self.company, name='Sales')
        logger_role = factories.RoleFactory(company=self.company, name='Logger')

        flow = factories.FlowFactory(invoice=self.invoice)
        flow_1 = factories.FlowFactory(invoice=self.invoice)
        flow_2 = factories.FlowFactory(invoice=self.invoice)

        factories.FlowRoleFactory(flow=flow, role=director_role)
        factories.FlowRoleFactory(flow=flow_1, role=accountant_role)
        factories.FlowRoleFactory(flow=flow_2, role=purchaser_role)
        factories.FlowRoleFactory(flow=flow_2, role=sales_role)

        response = self.client.get(reverse('invoice_flows', kwargs={'invoice_id': self.invoice.pk}))
        assert response.status_code == HTTPStatus.OK

        content = response.content.decode()
        assert (str(director_role) in content) is True
        assert (str(accountant_role) in content) is True
        assert (str(purchaser_role) in content) is True
        assert (str(sales_role) in content) is True
        assert (str(logger_role) in content) is False

    def test_create_invoice_flow(self):
        """
        Test the creation of new flow level with a role
        """
        director_role = factories.RoleFactory(company=self.company, name='Director')
        accountant_role = factories.RoleFactory(company=self.company, name='Accountant')
        purchaser_role = factories.RoleFactory(company=self.company, name='Purchaser')
        sales_role = factories.RoleFactory(company=self.company, name='Sales')
        factories.RoleFactory(company=self.company, name='Logger')

        assert Flow.objects.count() == 0
        assert FlowRole.objects.count() == 0

        response = self.ajax_post(
            url=reverse('create_invoice_flow', kwargs={'invoice_id': self.invoice.pk, 'after': 0}),
            data={
                'roles': [director_role.pk, accountant_role.pk, sales_role.pk]
            }
        )

        assert response.status_code == HTTPStatus.OK

        flows = Flow.objects.all()
        flow_roles = FlowRole.objects.all()

        assert len(flows) == 1
        assert len(flow_roles) == 3
        assert FlowRole.objects.filter(role=purchaser_role).count() == 0

    def test_can_add_role_to_flow(self):
        """
        Test the addition of a role to an existing flow level
        """
        flow = factories.FlowFactory(invoice=self.invoice)
        flow_1 = factories.FlowFactory(invoice=self.invoice)

        director_role = factories.RoleFactory(company=self.company, name='Director')
        accountant_role = factories.RoleFactory(company=self.company, name='Accountant')
        purchaser_role = factories.RoleFactory(company=self.company, name='Purchaser')
        sales_role = factories.RoleFactory(company=self.company, name='Sales')
        factories.RoleFactory(company=self.company, name='Logger')

        factories.FlowRoleFactory(flow=flow, role=director_role)

        assert Flow.objects.count() == 2
        assert FlowRole.objects.count() == 1

        response = self.ajax_post(
            url=reverse('add_invoice_flow_role', kwargs={'invoice_id': self.invoice.pk, 'flow_id': flow.pk}),
            data={
                'roles': [accountant_role.pk]
            }
        )

        assert response.status_code == HTTPStatus.OK

        flows = Flow.objects.all()
        flow_roles = FlowRole.objects.filter(flow=flow).all()

        assert len(flows) == 2
        assert len(flow_roles) == 2
        assert FlowRole.objects.filter(role=purchaser_role).count() == 0
        assert FlowRole.objects.filter(role=sales_role).count() == 0

    def test_can_delete_flow_role(self):
        """
        Test the removal of a role from the flow level
        """
        flow = factories.FlowFactory(invoice=self.invoice)

        director_role = factories.RoleFactory(company=self.company, name='Director')
        accountant_role = factories.RoleFactory(company=self.company, name='Accountant')

        director_flow = factories.FlowRoleFactory(flow=flow, role=director_role)
        factories.FlowRoleFactory(flow=flow, role=accountant_role)

        assert Flow.objects.count() == 1
        assert FlowRole.objects.count() == 2

        response = self.ajax_delete(
            url=reverse('delete_invoice_flow_role', kwargs={'invoice_id': self.invoice.pk, 'flow_role_id': director_flow.pk})
        )

        assert response.status_code == HTTPStatus.OK

        flows = Flow.objects.all()
        flow_roles = FlowRole.objects.filter(flow=flow).all()

        assert len(flows) == 1
        assert len(flow_roles) == 1
        assert flow_roles[0].role == accountant_role

    def test_delete_flow_role_removes_flow(self):
        """
        Test the removal of a role from the flow level, when there is only 1 role, then the flow level will also be removed
        """
        flow = factories.FlowFactory(invoice=self.invoice)

        director_role = factories.RoleFactory(company=self.company, name='Director')

        director_flow = factories.FlowRoleFactory(flow=flow, role=director_role)

        assert Flow.objects.count() == 1
        assert FlowRole.objects.count() == 1

        response = self.ajax_delete(
            url=reverse('delete_invoice_flow_role', kwargs={'invoice_id': self.invoice.pk, 'flow_role_id': director_flow.pk})
        )

        assert response.status_code == HTTPStatus.OK

        flows = Flow.objects.all()
        flow_roles = FlowRole.objects.filter(flow=flow).all()

        assert len(flows) == 0
        assert len(flow_roles) == 0