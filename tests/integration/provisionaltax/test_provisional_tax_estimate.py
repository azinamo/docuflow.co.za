from http import HTTPStatus

from django.urls import reverse, reverse_lazy
from django.utils.timezone import now

from docuflow.apps.common.utils import format_money
from docuflow.apps.provisionaltax.models import Estimate
from docuflow.apps.provisionaltax.enums import ProvisionalTaxEstimateType

from tests.base import AuthenticatedTestCase
from tests import factories


class TestProvisionalTaxEstimateViews(AuthenticatedTestCase):
    list_url = reverse_lazy('provisional_tax_estimate_index')

    def setUp(self) -> None:
        super().setUp()

    def test_can_list_provisional_taxes(self):
        first = factories.FirstProvisionalTaxEstimateFactory(year=self.year, profile=self.profile)
        second = factories.SecondProvisionalTaxEstimateFactory(year=self.year, profile=self.profile)
        third = factories.ThirdProvisionalTaxEstimateFactory(year=self.year, profile=self.profile)

        response = self.client.get(self.list_url)

        assert response.status_code == HTTPStatus.OK

        assert Estimate.objects.filter(year=self.year).count() == 3

        assert format_money(first.total_amount) in response.content.decode()
        assert format_money(second.total_amount) in response.content.decode()
        assert format_money(third.total_amount) in response.content.decode()

    def test_create_first_provisional_tax(self):
        periods = []
        for i in range(12):
            periods.append(factories.PeriodFactory(period=i+1, period_year=self.year))
        period = periods[6:][0]

        data = {
            'year': self.year,
            'to_period': period.id,
            'due_date': now().date(),
            'tax_rate': 28,
            'turnover': 1767801,
            'annual_turnover': 3535601,
            'estimated_income': -152069,
            'annual_estimated_income': -162748,
            'income_deductable': -1919870,
            'income_non_deductable': 0,
            'estimated_taxable_income': -152069,
            'assesed_loss_previous_year': 1200,
            'year_tax': 19000,
            'period_tax': 9500,
            'rebates': 19000,
            'employee_tax': 2000,
            'foreign_tax': 3000,
            'tax_paid': 4000,
            'late_payment_penalty': 10000,
            'late_payment_interest': 3000,
            'total_amount': 13500,
            'profile': self.profile.id,
            'role': self.role.id
        }

        assert Estimate.objects.filter(year=self.year).count() == 0

        response = self.ajax_post(reverse('create_provisional_tax_estimate',
                                          kwargs={'tax_estimate_type': ProvisionalTaxEstimateType.FIRST.value}),
                                  data=data)
        assert response.status_code == HTTPStatus.OK

        estimates = Estimate.objects.all()
        assert len(estimates) == 1
        assert estimates[0].total_amount == data['total_amount']
        assert estimates[0].profile.id == data['profile']
        assert estimates[0].year_tax == data['year_tax']
        assert estimates[0].estimated_income == data['estimated_income']
        assert estimates[0].late_payment_interest == data['late_payment_interest']

    def test_create_second_provisional_tax(self):
        first = factories.FirstProvisionalTaxEstimateFactory(year=self.year, profile=self.profile)

        periods = []
        for i in range(12):
            periods.append(factories.PeriodFactory(period=i+1, period_year=self.year))
        period = periods[11:][0]

        data = {
            'year': self.year,
            'to_period': period.id,
            'due_date': now().date(),
            'tax_rate': 28,
            'turnover': 1767801,
            'annual_turnover': 3535601,
            'estimated_income': -152069,
            'annual_estimated_income': -162748,
            'income_deductable': -1919870,
            'income_non_deductable': 0,
            'estimated_taxable_income': -152069,
            'assesed_loss_previous_year': 1200,
            'year_tax': 19000,
            'period_tax': 9500,
            'rebates': 19000,
            'employee_tax': 2000,
            'foreign_tax': 3000,
            'tax_paid': first.total_amount,
            'late_payment_penalty': 10000,
            'late_payment_interest': 3000,
            'total_amount': 13500,
            'profile': self.profile.id,
            'role': self.role.id
        }

        assert Estimate.objects.filter(year=self.year).count() == 1

        response = self.ajax_post(reverse('create_provisional_tax_estimate',
                                          kwargs={'tax_estimate_type': ProvisionalTaxEstimateType.SECOND.value}),
                                  data=data)
        assert response.status_code == HTTPStatus.OK

        estimates = Estimate.objects.all()
        assert len(estimates) == 2
        assert estimates[1].total_amount == data['total_amount']
        assert estimates[1].profile.id == data['profile']
        assert estimates[1].year_tax == data['year_tax']
        assert estimates[1].estimated_income == data['estimated_income']
        assert estimates[1].late_payment_interest == data['late_payment_interest']

    def test_update_provisional_tax(self):
        period = factories.PeriodFactory(period_year=self.year)
        provisional_estimate = factories.FirstProvisionalTaxEstimateFactory(year=self.year, to_period=period, profile=self.profile)

        data = {
            'tax_paid': provisional_estimate.tax_paid,
            'to_period': provisional_estimate.to_period.id,
            'tax_rate': provisional_estimate.tax_rate,
            'due_date': provisional_estimate.due_date,
            'turnover': 1767801,
            'annual_turnover': 3535601,
            'estimated_income': -152069,
            'annual_estimated_income': -162748,
            'income_deductable': -1919870,
            'income_non_deductable': 0,
            'estimated_taxable_income': -152069,
            'assesed_loss_previous_year': 1200,
            'year_tax': 19000,
            'period_tax': 9500,
            'rebates': 19000,
            'employee_tax': 2000,
            'foreign_tax': 3000,
            'late_payment_penalty': 10000,
            'late_payment_interest': 3000,
            'total_amount': 13500
        }

        response = self.ajax_post(reverse('update_provisional_tax_estimate', args=(provisional_estimate.pk, )),
                                  data=data)

        provisional_estimate.refresh_from_db()

        assert response.status_code == HTTPStatus.OK
        assert provisional_estimate.total_amount == data['total_amount']
        assert provisional_estimate.year_tax == data['year_tax']
        assert provisional_estimate.estimated_income == data['estimated_income']
        assert provisional_estimate.late_payment_interest == data['late_payment_interest']
