from datetime import date

from docuflow.apps.common.utils import round_decimal
from docuflow.apps.customer.models import AgeAnalysis
from docuflow.apps.customer.services import age_customer_invoice, age_invoice_receipt
from tests.base import AuthenticatedTestCase
from tests import factories


class AgeAnalysisTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super(AgeAnalysisTestCase, self).setUp()
        periods = []
        for p in range(1, 13):
            from_date = date(month=p, year=self.year.year, day=1)
            to_date = date(month=p, year=self.year.year, day=20)
            periods.append(
                factories.PeriodFactory(
                    period=p,
                    name=from_date.strftime('%B'),
                    to_date=to_date,
                    from_date=from_date,
                    period_year=self.year,
                    company=self.company
                )
            )
        self.periods = periods

    def test_can_age_current_customer_invoice(self):
        self.current_period = self.periods[0]

        invoice = factories.SalesTaxInvoiceFactory(period=self.current_period, branch=self.branch)

        age_customer_invoice(invoice)

        assert AgeAnalysis.objects.count() == 12

        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=invoice.period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_2)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_3)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_4 = self.periods[3]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_4)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_5)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

        period_6 = self.periods[5]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_6)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

    def test_can_age_invoice_added_mid_year(self):
        current_period = self.periods[1]
        amount = 38284.98
        invoice = factories.SalesTaxInvoiceFactory(period=current_period, branch=self.branch,
                                                   total_amount=amount)

        age_customer_invoice(invoice)

        assert AgeAnalysis.objects.count() == 11

        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=invoice.period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[2]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_2)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[3]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_3)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_4 = self.periods[4]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_4)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[5]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_5)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

        period_6 = self.periods[6]
        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_6)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

    def test_can_full_pay_current_invoice_in_same_period(self):
        self.current_period = self.periods[0]

        invoice = factories.SalesTaxInvoiceFactory(period=self.current_period, branch=self.branch)

        age_customer_invoice(invoice)

        assert AgeAnalysis.objects.count() == 12

        receipt = factories.ReceiptFactory(period=self.current_period, branch=self.branch, created_by=self.profile,
                                           amount=invoice.total_amount)
        invoice_receipt = factories.InvoiceReceiptFactory(receipt=receipt, invoice=invoice)

        age_invoice_receipt(invoice_receipt=invoice_receipt)

        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=invoice.period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        period_2_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_2)

        assert period_2_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.current == round_decimal(0)
        assert period_2_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.sixty_day == round_decimal(0)
        assert period_2_age_analysis.ninety_day == round_decimal(0)
        assert period_2_age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        period_3_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_3)
        assert period_3_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.current == round_decimal(0)
        assert period_3_age_analysis.thirty_day == round_decimal(0)
        assert period_3_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.ninety_day == round_decimal(0)
        assert period_3_age_analysis.one_twenty_day == round_decimal(0)

        period_4 = self.periods[3]
        period_4_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_4)
        assert period_4_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_4_age_analysis.current == round_decimal(0)
        assert period_4_age_analysis.thirty_day == round_decimal(0)
        assert period_4_age_analysis.sixty_day == round_decimal(0)
        assert period_4_age_analysis.ninety_day == round_decimal(invoice.total_amount)
        assert period_4_age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        period_5_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_5)
        assert period_5_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_5_age_analysis.current == round_decimal(0)
        assert period_5_age_analysis.thirty_day == round_decimal(0)
        assert period_5_age_analysis.sixty_day == round_decimal(0)
        assert period_5_age_analysis.ninety_day == round_decimal(0)
        assert period_5_age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

        period_6 = self.periods[5]
        period_6_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_6)
        assert period_6_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_6_age_analysis.current == round_decimal(0)
        assert period_6_age_analysis.thirty_day == round_decimal(0)
        assert period_6_age_analysis.sixty_day == round_decimal(0)
        assert period_6_age_analysis.ninety_day == round_decimal(0)
        assert period_6_age_analysis.one_twenty_day == round_decimal(invoice.total_amount)

    def test_can_full_pay_invoice_in_future_period(self):
        self.current_period = self.periods[0]

        amount = 2048
        invoice = factories.SalesTaxInvoiceFactory(period=self.current_period, branch=self.branch, total_amount=amount)

        age_customer_invoice(invoice)

        assert AgeAnalysis.objects.count() == 12

        receipt_period = self.periods[3]
        receipt = factories.ReceiptFactory(period=receipt_period, branch=self.branch, created_by=self.profile,
                                           amount=amount, customer=invoice.customer)
        invoice_receipt = factories.InvoiceReceiptFactory(receipt=receipt, invoice=invoice, amount=amount)

        age_invoice_receipt(invoice_receipt=invoice_receipt)

        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=self.current_period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        period_2_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_2)

        assert period_2_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.current == round_decimal(0)
        assert period_2_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.sixty_day == round_decimal(0)
        assert period_2_age_analysis.ninety_day == round_decimal(0)
        assert period_2_age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        period_3_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_3)
        assert period_3_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.current == round_decimal(0)
        assert period_3_age_analysis.thirty_day == round_decimal(0)
        assert period_3_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.ninety_day == round_decimal(0)
        assert period_3_age_analysis.one_twenty_day == round_decimal(0)

         # App previous periods keeps their balances, but future periods will be cleared
        receipt_period = receipt_period
        period_4_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=receipt_period)
        assert period_4_age_analysis.balance == round_decimal(0)
        assert period_4_age_analysis.current == round_decimal(0)
        assert period_4_age_analysis.thirty_day == round_decimal(0)
        assert period_4_age_analysis.sixty_day == round_decimal(0)
        assert period_4_age_analysis.ninety_day == round_decimal(0)
        assert period_4_age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        period_5_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_5)
        assert period_5_age_analysis.balance == round_decimal(0)
        assert period_5_age_analysis.current == round_decimal(0)
        assert period_5_age_analysis.thirty_day == round_decimal(0)
        assert period_5_age_analysis.sixty_day == round_decimal(0)
        assert period_5_age_analysis.ninety_day == round_decimal(0)
        assert period_5_age_analysis.one_twenty_day == round_decimal(0)

        period_6 = self.periods[5]
        period_6_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_6)
        assert period_6_age_analysis.balance == round_decimal(0)
        assert period_6_age_analysis.current == round_decimal(0)
        assert period_6_age_analysis.thirty_day == round_decimal(0)
        assert period_6_age_analysis.sixty_day == round_decimal(0)
        assert period_6_age_analysis.ninety_day == round_decimal(0)
        assert period_6_age_analysis.one_twenty_day == round_decimal(0)

    def test_invoice_added_mid_year_paid_in_future_period(self):
        invoice_period = self.periods[1]
        amount = 38284.98
        invoice = factories.SalesTaxInvoiceFactory(period=invoice_period, branch=self.branch, total_amount=amount)

        age_customer_invoice(invoice)

        # Only added to age on the second month, thus we have 11 periods its aged
        assert AgeAnalysis.objects.count() == 11

        receipt_period = self.periods[3]

        april_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=receipt_period)

        assert april_age_analysis.balance == round_decimal(invoice.total_amount)
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert april_age_analysis.thirty_day == round_decimal(0)

        receipt = factories.ReceiptFactory(period=receipt_period, branch=self.branch, created_by=self.profile,
                                           amount=amount, customer=invoice.customer)
        invoice_receipt = factories.InvoiceReceiptFactory(receipt=receipt, invoice=invoice, amount=amount)

        age_invoice_receipt(invoice_receipt=invoice_receipt)

        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=invoice_period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        march_period = self.periods[2]
        march_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=march_period)

        assert march_age_analysis.balance == round_decimal(invoice.total_amount)
        assert march_age_analysis.current == round_decimal(0)
        assert march_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert march_age_analysis.sixty_day == round_decimal(0)
        assert march_age_analysis.ninety_day == round_decimal(0)
        assert march_age_analysis.one_twenty_day == round_decimal(0)

        # App previous periods keeps their balances, but future periods will be cleared
        receipt_period = receipt_period
        april_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=receipt_period)

        assert april_age_analysis.balance == round_decimal(0)
        assert april_age_analysis.current == round_decimal(0)
        assert april_age_analysis.thirty_day == round_decimal(0)
        assert april_age_analysis.sixty_day == round_decimal(0)
        assert april_age_analysis.ninety_day == round_decimal(0)
        assert april_age_analysis.one_twenty_day == round_decimal(0)

        may = self.periods[4]
        may_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=may)
        assert may_age_analysis.balance == round_decimal(0)
        assert may_age_analysis.current == round_decimal(0)
        assert may_age_analysis.thirty_day == round_decimal(0)
        assert may_age_analysis.sixty_day == round_decimal(0)
        assert may_age_analysis.ninety_day == round_decimal(0)
        assert may_age_analysis.one_twenty_day == round_decimal(0)

        june = self.periods[5]
        june_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=june)
        assert june_age_analysis.balance == round_decimal(0)
        assert june_age_analysis.current == round_decimal(0)
        assert june_age_analysis.thirty_day == round_decimal(0)
        assert june_age_analysis.sixty_day == round_decimal(0)
        assert june_age_analysis.ninety_day == round_decimal(0)
        assert june_age_analysis.one_twenty_day == round_decimal(0)

    def test_can_partial_pay_invoice_in_future_period(self):
        self.current_period = self.periods[0]

        amount = 2048
        invoice = factories.SalesTaxInvoiceFactory(period=self.current_period, branch=self.branch, total_amount=amount)

        age_customer_invoice(invoice)

        assert AgeAnalysis.objects.count() == 12

        receipt_period = self.periods[3]
        receipt_amount = 2000
        receipt = factories.ReceiptFactory(period=receipt_period, branch=self.branch, created_by=self.profile,
                                           amount=receipt_amount, customer=invoice.customer)
        invoice_receipt = factories.InvoiceReceiptFactory(receipt=receipt, invoice=invoice, amount=receipt_amount)

        invoice_balance = amount - receipt_amount

        age_invoice_receipt(invoice_receipt=invoice_receipt)

        age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=self.current_period)
        assert age_analysis.balance == round_decimal(invoice.total_amount)
        assert age_analysis.current == round_decimal(invoice.total_amount)
        assert age_analysis.one_twenty_day == round_decimal(0)
        assert age_analysis.ninety_day == round_decimal(0)
        assert age_analysis.sixty_day == round_decimal(0)
        assert age_analysis.thirty_day == round_decimal(0)

        period_2 = self.periods[1]
        period_2_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_2)

        assert period_2_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.current == round_decimal(0)
        assert period_2_age_analysis.thirty_day == round_decimal(invoice.total_amount)
        assert period_2_age_analysis.sixty_day == round_decimal(0)
        assert period_2_age_analysis.ninety_day == round_decimal(0)
        assert period_2_age_analysis.one_twenty_day == round_decimal(0)

        period_3 = self.periods[2]
        period_3_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_3)
        assert period_3_age_analysis.balance == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.current == round_decimal(0)
        assert period_3_age_analysis.thirty_day == round_decimal(0)
        assert period_3_age_analysis.sixty_day == round_decimal(invoice.total_amount)
        assert period_3_age_analysis.ninety_day == round_decimal(0)
        assert period_3_age_analysis.one_twenty_day == round_decimal(0)

         # App previous periods keeps their balances, but future periods will remain with the balance
        receipt_period = receipt_period
        period_4_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=receipt_period)
        assert period_4_age_analysis.balance == round_decimal(invoice_balance)
        assert period_4_age_analysis.current == round_decimal(0)
        assert period_4_age_analysis.thirty_day == round_decimal(0)
        assert period_4_age_analysis.sixty_day == round_decimal(0)
        assert period_4_age_analysis.ninety_day == round_decimal(invoice_balance)
        assert period_4_age_analysis.one_twenty_day == round_decimal(0)

        period_5 = self.periods[4]
        period_5_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_5)
        assert period_5_age_analysis.balance == round_decimal(invoice_balance)
        assert period_5_age_analysis.current == round_decimal(0)
        assert period_5_age_analysis.thirty_day == round_decimal(0)
        assert period_5_age_analysis.sixty_day == round_decimal(0)
        assert period_5_age_analysis.ninety_day == round_decimal(0)
        assert period_5_age_analysis.one_twenty_day == round_decimal(invoice_balance)

        period_6 = self.periods[5]
        period_6_age_analysis = AgeAnalysis.objects.get(customer=invoice.customer, period=period_6)
        assert period_6_age_analysis.balance == round_decimal(invoice_balance)
        assert period_6_age_analysis.current == round_decimal(0)
        assert period_6_age_analysis.thirty_day == round_decimal(0)
        assert period_6_age_analysis.sixty_day == round_decimal(0)
        assert period_6_age_analysis.ninety_day == round_decimal(0)
        assert period_6_age_analysis.one_twenty_day == round_decimal(invoice_balance)

    def test_unallocated_amount_on_customer_age(self):
        pass

    def test_payment_with_unallocated_amount(self):
        pass