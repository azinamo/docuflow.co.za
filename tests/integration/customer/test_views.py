from django.urls import reverse_lazy, reverse

from docuflow.apps.customer.models import Customer
from tests.base import AuthenticatedTestCase
from tests import factories


class CustomerViewTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.list_url = reverse_lazy('customer_index')

    def tearDown(self) -> None:
        # remove route files
        pass

    def test_can_list_customers(self):

        response = self.client.get(self.list_url)

        self.assertEqual(response.status_code, 200)

    def test_can_create_customer(self):
        data = {
            'name': 'Customer',
            'postal_country': 'ZA',
            'statement_days': 30
        }
        assert Customer.objects.count() == 0

        response = self.client.post(reverse('create_customer'), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})

        assert response.status_code == 200

        assert Customer.objects.filter(company=self.company).count() == 1
