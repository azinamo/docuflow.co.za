from django.test import TestCase
from django.urls import reverse
from django.core import mail


class HomepageViewTestCase(TestCase):

    def setUp(self) -> None:
        self.homepage_url = reverse('homepage')

    def test_can_view_homepage(self):
        response = self.client.get(self.homepage_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('pages/homepage.html')
        self.assertContains(response, 'Go paperless with docuflow')


class EnquiryViewTestCase(TestCase):

    def setUp(self) -> None:
        self.send_enquiry_url = reverse('enquiry')

    def test_can_send_enquiry(self):
        data = {'name': 'name', 'message': 'message', 'email': 'email@gamil.com', 'phone_number': 'phone_number',
                'company': 'company name'}
        self.assertEqual(len(mail.outbox), 0)
        response = self.client.post(reverse('enquiry'), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)

