from django.test import TestCase

from docuflow.apps.pages.forms import EnquiryForm


class EnquiryFormTest(TestCase):

    def test_enquiry_valid_data(self):
        form = EnquiryForm({
            'name': 'Name',
            'email': 'info@email.com',
            'company': 'Company',
            'message': 'Message'
        })
        self.assertTrue(form.is_valid())

    def test_enquiry_invalid_data(self):
        form = EnquiryForm({})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'name': ['This field is required.'],
            'email': ['This field is required.'],
            'company': ['This field is required.'],
            'message': ['This field is required.']
        })
