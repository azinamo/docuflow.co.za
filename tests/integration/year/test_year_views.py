from http import HTTPStatus

from django.urls import reverse

from docuflow.apps.company.enums import CompanyStatus
from docuflow.apps.period.models import Year
from docuflow.apps.period.enums import YearStatus
from tests.base import AuthenticatedTestCase
from tests import factories


class YearViewTestCase(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.list_url = reverse('years_index')

    def test_can_list_years(self):
        years = factories.YearFactory.create_batch(5, company=self.company)

        response = self.client.get(self.list_url)

        assert response.status_code == HTTPStatus.OK
        content = response.content.decode()
        for year in years:
            assert str(year) in content

    def test_create_new_year(self):
        data = {
            'year': 2021,
            'start_date': '2021-01-01',
            'end_date': '2021-12-31',
            'number_of_periods': 12,
            'journal_number': 'JNL1000-'
        }

        response = self.client.post(reverse('create_year'), data=data, follow=True)

        assert response.status_code == HTTPStatus.OK
        years = Year.objects.all()
        assert len(years) == 2
        assert years[1].year == data['year']
        assert years[1].start_date.strftime('%Y-%m-%d') == data['start_date']
        assert years[1].end_date.strftime('%Y-%m-%d') == data['end_date']
        assert years[1].number_of_periods == data['number_of_periods']
        assert years[1].journal_number == data['journal_number']

    def test_edit_year(self):
        year = factories.YearFactory(year=2020)

        data = {
            'year': 2020,
            'start_date': '2021-01-11',
            'end_date': '2021-12-11',
            'number_of_periods': 12,
            'journal_number': 'JNL1000-'
        }

        response = self.client.post(reverse('edit_year', args=(year.id, )), data=data, follow=True)

        assert response.status_code == HTTPStatus.OK
        year.refresh_from_db()
        assert Year.objects.count() == 2
        assert year.year == data['year']
        assert year.start_date.strftime('%Y-%m-%d') == data['start_date']
        assert year.end_date.strftime('%Y-%m-%d') == data['end_date']
        assert year.number_of_periods == data['number_of_periods']
        assert year.journal_number == data['journal_number']

    def test_delete_year(self):
        year = factories.YearFactory(year=2020)

        response = self.client.get(reverse('delete_year', args=(year.id, )), follow=True)

        assert response.status_code == HTTPStatus.OK
        assert Year.objects.count() == 1

    def test_year_closing_process(self):
        year = factories.YearFactory(year=2020, status=YearStatus.CLOSING, company=self.company)
        stages = ['shutting_login_screen', 'close_all_periods', 'ensure_last_period_is_open',
                  'change_status_to_closing', 'fixed_assets_closing_balances', 'general_ledger_closing_balances',
                  'inventory_closing_balances', 'customer_closing_balances', 'supplier_closing_balances',
                  'distribution_balances', 'goods_received_balances', 'invoices_not_year_posted', 'open_next_year',
                  'opening_login', 'done']
        for stage in stages:
            response = self.client.get(reverse('year_closing', args=(year.id, )), data={
                'stage': stage
            }, follow=True)
            assert response.status_code == HTTPStatus.OK

        year.refresh_from_db()
        self.company.refresh_from_db()

        assert year.status == YearStatus.FINAL
        assert year.is_active is False

        assert self.company.status == CompanyStatus.ACTIVE

    def test_year_opening_process(self):
        pass
