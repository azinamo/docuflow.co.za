import urllib

from django.urls import reverse

from tests.base import AuthenticatedTestCase
from tests import factories


class TestSuffixReport(AuthenticatedTestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_open_suffix_lines(self):
        journal_line = factories.JournalLineFactory(suffix='Insatt 02/01/2006')

        suffix_url = f"?{urllib.parse.urlencode({'suffix': journal_line.suffix})}"
        url = f"{reverse('open_suffix_lines', kwargs={'account_id': journal_line.account.id})}{suffix_url}"
        response = self.client.get(url)
        assert response.status_code == 200
