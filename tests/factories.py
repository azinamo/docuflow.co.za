import datetime
import factory
import faker
from calendar import monthrange
from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal

from django.core.management import call_command
from django.conf import settings
from django.db.models.signals import post_save
from django.utils.timezone import now

from docuflow.apps.accounts import models as accounts
from docuflow.apps.budget.models import Budget, BudgetAccount, BudgetAccountPeriod
from docuflow.apps.certificate import models as certificate_models
from docuflow.apps.common.enums import Module
from docuflow.apps.company import models as companies
from docuflow.apps.company import enums as company_enums
from docuflow.apps.customer import models as customers
from docuflow.apps.customer import enums as customer_enums
from docuflow.apps.distribution import models as distribution_models
from docuflow.apps.fixedasset import models as fixed_assets_models
from docuflow.apps.fixedasset import enums as fixed_assets_enums
from docuflow.apps.inventory import models as inventory
from docuflow.apps.invoice import models as purchase
from docuflow.apps.invoice import services as purchase_services
from docuflow.apps.invoice import enums as purchase_enums
from docuflow.apps.journals import models as journals
from docuflow.apps.sales import models as sales
from docuflow.apps.sales import enums as sales_enums
from docuflow.apps.sources.models import IMAPEmail
from docuflow.apps.supplier import models as suppliers
from docuflow.apps.payroll import models as payroll_models
from docuflow.apps.period import models as years
from docuflow.apps.period import enums as years_enums
from docuflow.apps.purchaseorders import models as purchaseorders
from docuflow.apps.provisionaltax import models as provisionaltaxes
from docuflow.apps.provisionaltax.enums import ProvisionalTaxEstimateType
from docuflow.apps.subscription import models as subscriptions
from docuflow.apps.system import models as system_models
from docuflow.apps.system import enums as system_enums
from docuflow.apps.workflow import models as workflow
from docuflow.apps.vat import models as vat_models

faker = faker.Factory.create()


# Subscription
class SubscriptionFactory(factory.DjangoModelFactory):
    name = 'ULTIMATE PACKAGE'
    price = 699
    description = 'ULTIMATE PACKAGE'
    slug = 'ultimate-package'

    class Meta:
        model = subscriptions.SubscriptionPlan
        django_get_or_create = ('name', 'price', 'slug')


# Accounts
@factory.django.mute_signals(post_save)
class UserFactory(factory.django.DjangoModelFactory):
    password = factory.PostGenerationMethodCall('set_password', 'PASSWORD')
    first_name = factory.lazy_attribute(lambda u: faker.first_name())
    last_name = factory.lazy_attribute(lambda u: faker.last_name())
    username = factory.LazyAttributeSequence(lambda u, c: f"{u.first_name}_{u.last_name}_{c}")
    email = factory.LazyAttribute(lambda u: u.username + '@docuflow.co.za')

    class Meta:
        model = accounts.User
        django_get_or_create = ('username', 'email', )

    # profile = factory.RelatedFactory('docuflow.apps.accounts.factories.ProfileFactory', 'user')


@factory.django.mute_signals(post_save)
class UserProfileFactory(factory.DjangoModelFactory):
    company = factory.SubFactory('tests.factories.CompanyFactory')
    user = factory.SubFactory(UserFactory)
    code = 'ERRO'

    class Meta:
        model = accounts.Profile
        django_get_or_create = ('company', 'user')

    @factory.post_generation
    def roles(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            self.roles.set(extracted)


class ProfileFactory(factory.DjangoModelFactory):
    company = factory.SubFactory('tests.factories.CompanyFactory')
    user = factory.SubFactory(UserFactory)
    code = factory.Sequence(lambda n: f'ERRO-{n}')

    class Meta:
        model = accounts.Profile
        django_get_or_create = ('company', 'user')


class PermissionGroupFactory(factory.DjangoModelFactory):
    is_active = True
    company = factory.SubFactory('tests.factories.CompanyFactory')
    label = 'Level 1'
    is_default = False

    class Meta:
        model = accounts.PermissionGroup


class RoleFactory(factory.DjangoModelFactory):
    company = factory.SubFactory('tests.factories.CompanyFactory')
    name = 'Administrator'
    label = factory.LazyAttribute(lambda o: o.name)

    class Meta:
        model = accounts.Role
        django_get_or_create = ('company', 'name')


class ManagerRoleFactory(RoleFactory):
    label = 'Manager'
    name = 'Manager'
    authorization_amount = 1000
    can_be_inserted_in_flow = True


class SupervisorRoleFactory(RoleFactory):
    label = 'Supervisor'
    name = 'Supervisor'
    authorization_amount = 1000
    can_be_inserted_in_flow = True


class ClerkFactory(RoleFactory):
    manager_role = factory.SubFactory(ManagerRoleFactory)
    supervisor_role = factory.SubFactory(SupervisorRoleFactory)

    label = 'Clerk'
    name = 'Clerk'
    authorization_amount = 1000
    can_be_inserted_in_flow = True


class ReportTypeFactory(factory.DjangoModelFactory):
    name = 'Turnover'
    account_type = system_enums.AccountType.BALANCE_SHEET
    slug = 'turnover'

    class Meta:
        model = system_models.ReportType
        django_get_or_create = ('slug', )


class CashFlowFactory(factory.DjangoModelFactory):
    name = 'Cash Received'
    is_active = True
    slug = 'cash-received'

    class Meta:
        model = system_models.CashFlowCategory
        django_get_or_create = ('slug', )


class SystemFixedAssetCategoryFactory(factory.DjangoModelFactory):
    name = 'Goodwill'

    class Meta:
        model = system_models.FixedAssetCategory
        django_get_or_create = ('name', )


class LoggerRoleFactory(RoleFactory):
    manager_role = factory.SubFactory(ManagerRoleFactory)
    supervisor_role = factory.SubFactory(SupervisorRoleFactory)

    label = 'Logger'
    name = 'Logger'
    authorization_amount = 1000
    can_be_inserted_in_flow = True


class AccountantRoleFactory(RoleFactory):
    company = factory.SubFactory('tests.factories.CompanyFactory')
    manager_role = factory.SubFactory(ManagerRoleFactory)
    supervisor_role = factory.SubFactory(SupervisorRoleFactory)

    label = 'Accountant'
    name = 'Accountant'
    authorization_amount = 1000
    can_be_inserted_in_flow = True


class UserOpenPageFactory(factory.DjangoModelFactory):
    profile = factory.SubFactory(ProfileFactory)

    class Meta:
        model = accounts.UserOpenPage
        django_get_or_create = ('profile', )

    page = 'https://docuflow.co.za/41054/invoices/10000/detail'


class CompanyFactory(factory.DjangoModelFactory):
    administrator = factory.SubFactory(UserFactory)
    subscription_plan = factory.SubFactory(SubscriptionFactory)
    # expenditure_account = factory.RelatedFactory('tests.factories.AccountFactory')
    # default_inventory_variance_account = factory.RelatedFactory('tests.factories.AccountFactory')
    # default_goods_received_account = factory.RelatedFactory('tests.factories.AccountFactory')
    area_code = '99999'
    name = factory.faker.Faker('name')

    class Meta:
        model = companies.Company
        django_get_or_create = ('name', )

    @factory.post_generation
    def status(self, create, extracted, **kwargs):
        call_command('loaddata', 'fixtures/company_status', verbosity=0)
        self.activate()
        return self


class AccountFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    name = 'Workmens Compensation'
    code = '4660/000'
    is_vat = False
    sub_ledger = None
    report = factory.SubFactory(ReportTypeFactory)
    cash_flow = factory.SubFactory(CashFlowFactory)
    is_active = True

    class Meta:
        model = companies.Account
        django_get_or_create = ('company', 'name', 'code')


class SupplierAccountFactory(AccountFactory):
    sub_ledger = company_enums.SubLedgerModule.SUPPLIER.value


class CustomerAccountFactory(AccountFactory):
    sub_ledger = company_enums.SubLedgerModule.CUSTOMER.value


class GoodsReceivedAccountFactory(AccountFactory):
    code = '7600/000'
    name = 'DF Control - Goods Received - Awaiting Invoice'


class InventoryControlAccountFactory(AccountFactory):
    code = '7800/001'
    name = 'DF Control - Inventory'
    sub_ledger = company_enums.SubLedgerModule.INVENTORY.value
    is_vat = False


class InventoryReceivedVariationAccountFactory(AccountFactory):
    is_vat = False
    code = '2200/001'
    name = 'Purch Var - Raw Materials'


class VatAccountFactory(AccountFactory):

    name = factory.Sequence(lambda n: f'Vat Account {n}')
    code = factory.Sequence(lambda n: f'000{n}/{n}')
    is_vat = True


class VatCodeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    account = factory.SubFactory(VatAccountFactory)
    label = f"Normal Input VAT"
    code = 15
    percentage = 15
    is_default = True
    module = Module.PURCHASE

    class Meta:
        model = companies.VatCode
        django_get_or_create = ('company', 'account')


class OutputVatCodeFactory(VatCodeFactory):
    label = f"Normal Output VAT"
    module = Module.SALES


class NonBankingDayFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)

    class Meta:
        model = companies.NonBankingDay
        django_get_or_create = ('company', )


class CompanyObjectFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    label = 'Projekt'
    slug = 'projekt'
    ordering = 1

    class Meta:
        model = companies.CompanyObject
        django_get_or_create = ('company', 'label')


class ObjectItemFactory(factory.DjangoModelFactory):
    company_object = factory.SubFactory(CompanyObjectFactory)
    code = '701'
    label = 'Sales'
    is_active = 1

    class Meta:
        model = companies.ObjectItem
        django_get_or_create = ('company_object', 'code')


class PaymentMethodFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    name = 'Bank Transfer/EFT'
    account = factory.SubFactory(AccountFactory)
    is_active = True
    is_depositable = False
    has_change = False
    journal_style = company_enums.JournalStyle.SEPARATE

    class Meta:
        model = companies.PaymentMethod
        django_get_or_create = ('company', 'name', )


class BranchFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    label = factory.lazy_attribute_sequence(lambda o, n: "Branch %d" % n)

    class Meta:
        model = companies.Branch
        django_get_or_create = ('label', )


class CurrencyFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)

    class Meta:
        model = companies.Currency
        django_get_or_create = ('company', )


class PurchaseFileTypeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    module = Module.PURCHASE.value

    class Meta:
        model = companies.FileType
        django_get_or_create = ('company', 'module', )

    name = 'Supplier Invoices'


class SalesFileTypeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    module = Module.SALES.value
    name = 'Customer Invoices'

    class Meta:
        model = companies.FileType
        django_get_or_create = ('company', 'module', )


class DocumentFileTypeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    module = Module.DOCUMENT.value
    name = 'Company Documents'

    class Meta:
        model = companies.FileType
        django_get_or_create = ('company', 'module',)


class MeasureFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)

    class Meta:
        model = companies.Measure
        django_get_or_create = ('company',)


class UnitFactory(factory.DjangoModelFactory):
    measure = factory.SubFactory(MeasureFactory)
    name = 'V'
    unit = 'cm'

    class Meta:
        model = companies.Unit
        django_get_or_create = ('measure', 'name', 'unit')


class DeliveryMethodFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)

    class Meta:
        model = companies.DeliveryMethod


# Years
class YearFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    year = factory.Sequence(lambda c: now().year + c)
    start_date = factory.LazyAttribute(lambda obj: date(year=now().year, month=1, day=1))
    end_date = factory.LazyAttribute(lambda obj: date(year=now().year, month=12, day=31))

    class Meta:
        model = years.Year
        django_get_or_create = ('company', 'year', )

    class Params:
        started = factory.Trait(
            status=years_enums.YearStatus.STARTED
        )


class OpenYearFactory(YearFactory):
    status = years_enums.YearStatus.OPEN


class PeriodFactory(factory.DjangoModelFactory):
    period_year = factory.SubFactory(YearFactory)
    company = factory.SubFactory(CompanyFactory)
    period = 1
    name = factory.LazyAttribute(lambda obj: obj.from_date.strftime('%B'))
    from_date = factory.LazyAttribute(lambda obj: date(year=obj.period_year.year, month=obj.period, day=1))
    to_date = factory.LazyAttribute(lambda obj: obj.from_date.replace(
        year=obj.from_date.year, month=obj.from_date.month, day=monthrange(obj.from_date.year, obj.from_date.month)[1]))

    class Meta:
        model = years.Period
        django_get_or_create = ('period_year', 'period', )

    class Params:
        opened = factory.Trait(
            status=years_enums.PeriodStatus.OPEN
        )


class ClosedPeriodFactory(PeriodFactory):
    status = years_enums.PeriodStatus.CLOSED


# Supplier
class SupplierFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)

    class Meta:
        model = suppliers.Supplier
        django_get_or_create = ('company', 'code')

    name = 'Supplier'
    code = '10001'


class SupplierAuthorizationFactory(factory.DjangoModelFactory):
    supplier = factory.SubFactory(SupplierFactory)

    class Meta:
        model = suppliers.SupplierAuthorization
        django_get_or_create = ('supplier', )

    amount = 10000


class RoleAuthorizationFactory(factory.DjangoModelFactory):
    supplier = factory.SubFactory(SupplierFactory)
    role = factory.SubFactory(AccountantRoleFactory)

    class Meta:
        model = suppliers.RoleAuthorization
        django_get_or_create = ('supplier', 'role')

    amount = 10000


class SupplierLedgerFactory(factory.DjangoModelFactory):
    supplier = factory.SubFactory(SupplierFactory)
    period = factory.SubFactory(PeriodFactory)
    journal_line = factory.SubFactory('tests.factories.JournalLineFactory')
    created_by = factory.SubFactory(ProfileFactory)
    content_object = factory.SubFactory('tests.factories.InvoiceFactory')
    object_id = 1
    text = 'Supplier payment with journal'
    debit = 3000.00
    credit = 0
    balance = 3000.00

    class Meta:
        model = suppliers.Ledger
        django_get_or_create = ('supplier', 'period', 'object_id')


class PaymentSupplierLedgerFactory(SupplierLedgerFactory):
    content_object = factory.SubFactory('tests.factories.PaymentFactory')


class SupplierLedgerPaymentFactory(factory.DjangoModelFactory):
    ledger = factory.SubFactory(SupplierLedgerFactory)
    payment = factory.SubFactory('tests.factories.PaymentFactory')
    amount = 3000.00

    class Meta:
        model = suppliers.LedgerPayment
        django_get_or_create = ('ledger', 'payment', )


class SupplierAgeAnalysisFactory(factory.DjangoModelFactory):
    supplier = factory.SubFactory(SupplierFactory)
    period = factory.SubFactory('tests.factories.PeriodFactory')
    balance = 1000
    one_twenty_day = 200
    ninety_day = 200
    sixty_day = 200
    thirty_day = 200
    current = 200
    unallocated = 0

    class Meta:
        model = suppliers.AgeAnalysis
        django_get_or_create = ('supplier', 'period', )


# Workflow
class WorkflowFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    created_by = factory.SubFactory(UserFactory)
    name = 'Default'
    description = 'Default Workflow'
    is_default = True

    class Meta:
        model = workflow.Workflow
        django_get_or_create = ('company', )

    @factory.post_generation
    def levels(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of roles were passed in, use them
            for level in extracted:
                self.levels.add(level)


class WorkflowLevelFactory(factory.DjangoModelFactory):
    workflow = factory.SubFactory(WorkflowFactory)

    class Meta:
        model = workflow.Level
        django_get_or_create = ('workflow', )

    sort_order = 1

    @factory.post_generation
    def roles(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of roles were passed in, use them
            for role in extracted:
                self.roles.add(role)


# Purchase
class StatusFactory(factory.DjangoModelFactory):
    name = 'In the flow not printed'
    slug = 'in-the-flow'
    code = '10'

    class Meta:
        model = purchase.Status
        django_get_or_create = ('slug', )

    class Params:
        posted_printed = factory.Trait(name='Account posting printed', slug='end-flow-printed', code=40)
        final_signed = factory.Trait(name='Final Signed', slug='final-signed', code=30)
        paid = factory.Trait(name='Paid', slug='paid', code=50)
        paid_printed = factory.Trait(name='Paid Printed', slug='paid-printed', code=70)
        paid_not_printed = factory.Trait(name='Paid Not Printed', slug='paid-not-printed', code=45)
        paid_not_account_posted = factory.Trait(name='Paid Not Account Posted', slug='paid-not-account-posted', code=46)
        pp_not_account_posted = factory.Trait(name='PP Not Account Posted', slug='pp-not-account-posted', code=47)
        partially_paid = factory.Trait(name='Partially Paid', slug='partially-paid', code=48)
        po_request = factory.Trait(name='Purchase Order Requested', slug='po-request', code=50)
        po_approved = factory.Trait(name='Purchase Order Approved', slug='po-approved', code=52)
        decline_requested = factory.Trait(name='Decline Requested', slug='decline-request-from-user', code=54)
        adjustment = factory.Trait(name='Adjustments', slug='adjustments', code=11)


class LoggerStatusFactory(StatusFactory):
    name = 'Arrived At Logger'
    slug = 'arrived-at-logger'
    code = '20'


class TaxInvoiceTypeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    file = factory.SubFactory(PurchaseFileTypeFactory)
    name = 'Tax Invoice'
    is_account_posting = True
    is_credit = False
    is_default = False
    number_start = 'INV0210001'
    code = 'tax-invoice'

    class Meta:
        model = purchase.InvoiceType
        django_get_or_create = ('company', 'file', )

    class Params:
        credit_note = factory.Trait(
            is_credit=True,
            number_start='CRN00001',
            name='Tax Credit Note',
            code='tax-credit-note'
        )


class CreditInvoiceTypeFactory(TaxInvoiceTypeFactory):
    name = 'Tax Credit Note'
    is_credit = True
    code = 'tax-credit-note'


class PurchaseOrderInvoiceTypeFactory(TaxInvoiceTypeFactory):
    name = 'Purchase Order'
    code = purchase_enums.InvoiceType.PURCHASE_ORDER.value


class SalesTaxInvoiceTypeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    file = factory.SubFactory(SalesFileTypeFactory)
    name = 'Tax Invoice'
    is_credit = False
    number_start = 'INV00'

    class Meta:
        model = purchase.InvoiceType
        django_get_or_create = ('company', 'file',)


class SalesCreditInvoiceTypeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    file = factory.SubFactory(SalesFileTypeFactory)
    name = 'Tax Credit Note'
    is_credit = True
    number_start = 'CRN'

    class Meta:
        model = purchase.InvoiceType
        django_get_or_create = ('company', 'file',)


class DocumentInvoiceTypeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    file = factory.SubFactory(DocumentFileTypeFactory)
    name = 'Important Document'
    is_credit = False
    number_start = 'CRN'

    class Meta:
        model = purchase.InvoiceType
        django_get_or_create = ('company', 'file',)


class InvoiceFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    invoice_type = factory.SubFactory(TaxInvoiceTypeFactory)
    status = factory.SubFactory(StatusFactory)
    supplier = factory.SubFactory(SupplierFactory)
    period = factory.SubFactory(PeriodFactory)
    workflow = factory.SubFactory(WorkflowFactory)
    total_amount = 1200
    vat_amount = 200
    invoice_date = now().date()
    accounting_date = now().date()
    invoice_id = factory.Sequence(lambda i: str(i))
    invoice_number = factory.Sequence(lambda i: f'INV000{i}')
    open_amount = factory.LazyAttribute(lambda obj: obj.total_amount)
    net_amount = factory.LazyAttribute(lambda obj: obj.total_amount - obj.vat_amount)
    due_date = factory.LazyAttribute(lambda obj: obj.accounting_date + relativedelta(months=2))

    class Meta:
        model = purchase.Invoice
        django_get_or_create = ('company', 'invoice_id')

    # class Params:
    #     sales = factory.Trait(
    #         sales_invoice=factory.SubFactory('tests.factories.SalesTaxInvoiceFactory')
    #     )

    @factory.post_generation
    def add_to_flow(self, create, extracted, **kwargs):
        if extracted:
            add_to_flow = purchase_services.AddToFlow(self, extracted['profile'], extracted['role'])
            add_to_flow.process_flow()


class InvoiceAccountFactory(factory.DjangoModelFactory):
    # account_posting_line = models.ForeignKey(, related_name='posting_line_invoices', null=True, blank=True, on_delete=models.DO_NOTHING)
    invoice = factory.SubFactory(InvoiceFactory)
    # account_posting_line = factory.SubFactory('docuflow.apps.company.factories.AccountPostingLine')
    account = factory.SubFactory(AccountFactory)
    vat_code = factory.SubFactory(VatCodeFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(LoggerRoleFactory)
    # invoice_account = models.ForeignKey('self', related_name="parent", null=True, blank=True, on_delete=models.CASCADE)
    comment = 'Invoice account'
    percentage = 10
    amount = 1000
    variance = 0
    vat_line = False
    reinvoice = False
    is_holding_account = False
    is_posted = False
    is_vat = False
    # goods_received = factory.SubFactory()
    # goods_returned = factory.SubFactory()
    # object_items = models.ManyToManyField(ObjectItem)
    # object_postings = models.ManyToManyField('accountposting.ObjectPosting')
    suffix = 'Suffix'

    class Meta:
        model = purchase.InvoiceAccount
        django_get_or_create = ('invoice', 'account', )


class FlowStatusFactory(factory.DjangoModelFactory):
    name = 'In Flow'
    slug = 'in-flow'

    class Meta:
        model = purchase.FlowStatus
        django_get_or_create = ('slug', )


class FlowFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory(InvoiceFactory)
    status = factory.SubFactory(FlowStatusFactory)
    is_current = False
    sign_count = 0
    is_signed = False
    order_number = factory.Sequence(lambda i: i)

    class Meta:
        model = purchase.Flow


class FlowRoleFactory(factory.DjangoModelFactory):
    flow = factory.SubFactory(FlowFactory)
    role = factory.SubFactory(LoggerRoleFactory)
    is_signed = False
    status = 1
    # signed_at = now().date()
    # signed_by = factory.SubFactory(ProfileFactory)
    # created_by = factory.SubFactory(ProfileFactory)
    # reminded_at = now().date() + relativedelta(months=10)

    class Meta:
        model = purchase.FlowRole
        django_get_or_create = ('flow', 'role')


class PurchaseUpdateFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    period = factory.SubFactory(PeriodFactory)
    ledger = factory.SubFactory('tests.factories.JournalFactory')
    role = factory.SubFactory(AccountantRoleFactory)
    created_by = factory.SubFactory(ProfileFactory)
    journal_id = factory.Sequence(lambda n:  f'{n + 1}')
    date = now().date()

    class Meta:
        model = purchase.Journal
        django_get_or_create = ('journal_type', 'company')


class IncomingInvoiceUpdateFactory(PurchaseUpdateFactory):
    journal_type = purchase_enums.JournalType.INCOMING.value
    report_unique_number = factory.LazyAttribute(lambda o: f'Incoming Invoice - Report {o.journal_id}')


class PostingJournalFactory(PurchaseUpdateFactory):
    journal_type = purchase_enums.JournalType.POSTING.value
    report_unique_number = factory.LazyAttribute(lambda o: f'Posting of Invoice {o.journal_id}')


class InvoiceJournalFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory(InvoiceFactory)
    journal = factory.SubFactory(PurchaseUpdateFactory)
    invoice_type = factory.SubFactory(TaxInvoiceTypeFactory)
    invoice_status = factory.SubFactory(StatusFactory)
    supplier = factory.SubFactory(SupplierFactory)
    total_amount = 1200
    vat_amount = 200
    invoice_date = now().date()
    accounting_date = now().date()
    invoice_number = factory.Sequence(lambda i: f'INV000{i}')
    net_amount = factory.LazyAttribute(lambda obj: obj.total_amount - obj.vat_amount)

    class Meta:
        model = purchase.InvoiceJournal
        django_get_or_create = ('invoice', 'journal', 'invoice_type', 'total_amount', 'vat_amount')


# Invoice Payments
class PaymentFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    supplier = factory.SubFactory(SupplierFactory)
    period = factory.SubFactory(PeriodFactory)
    payment_id = factory.Sequence(lambda pid: pid)
    reference = 'Payment Ref'
    role = factory.SubFactory(ManagerRoleFactory)
    profile = factory.SubFactory(ProfileFactory)
    payment_date = now().date()
    payment_method = factory.SubFactory(PaymentMethodFactory)
    account = factory.SubFactory(AccountFactory)
    discount_disallowed = 0
    total_amount = 1200
    total_discount = 0
    net_amount = 1000
    balance = factory.LazyAttribute(lambda obj: obj.total_amount)
    vat_code = factory.SubFactory(VatCodeFactory)

    class Meta:
        model = purchase.Payment
        django_get_or_create = ('company', 'supplier', 'account', )

    @factory.post_generation
    def invoices(self, create, extracted, **kwargs):
        if extracted:
            for i in extracted:
                invoice_payment = InvoicePaymentFactory(invoice=i, payment=self, allocated=i.total_amount)
                self.invoices.add(invoice_payment)


class InvoicePaymentFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory(InvoiceFactory)
    payment = factory.SubFactory(PaymentFactory)
    allocated_reference = 'Invoice Payment Reference'
    allocated = 1000
    discount = 0

    class Meta:
        model = purchase.InvoicePayment
        django_get_or_create = ('invoice', 'payment', )


class ChildPaymentFactory(factory.DjangoModelFactory):
    parent = factory.SubFactory(PaymentFactory)
    payment = factory.SubFactory(PaymentFactory)
    amount = 9000

    class Meta:
        model = purchase.ChildPayment
        django_get_or_create = ('parent', 'payment', )


# Customer
class DeliveryTermFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)

    class Meta:
        model = customers.DeliveryTerm


class CustomerFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    delivery_term = factory.SubFactory(DeliveryTermFactory)
    delivery_method = factory.SubFactory(DeliveryMethodFactory)
    supplier = factory.SubFactory(SupplierFactory)
    default_account = factory.SubFactory(AccountFactory)
    salesman = factory.SubFactory('tests.factories.SalesmanFactory')
    credit_limit = 10000000
    available_credit = 0
    name = 'Morgan'
    number = 'CUOOO1'
    vat_number = 'VAT0001'
    email = factory.LazyAttributeSequence(lambda o, c: f'{o.name.lower()}-{c}@docuflow.co.za')
    accepted_email_marketing = False
    phone_number = '07492378423'
    cell_number = '07492378423'
    accepted_sms_marketing = False
    contact_person = 'Mrog'
    account_person = 'Accountant'
    is_active = True
    payment_term = customer_enums.PaymentTerm.CASH_CUSTOMER.value
    days = 0
    is_debit_order_client = False
    document_layout = customer_enums.DocumentLayout.DEFAULT.value
    is_statement_discount = False
    statement_discount = 0
    is_line_discount = False
    line_discount = 0
    is_invoice_discount = False
    invoice_discount = 0
    discount_days = 15
    is_interest_charged = False
    interest = 2.5
    days_unpaid = 30
    printing_setup = '{}'
    statistics = '{}'
    is_price_on_delivery = False
    is_default = False
    external_company_number = 'REF'
    df_number = ''

    class Meta:
        model = customers.Customer
        django_get_or_create = ('company', 'number')


class CashCustomerFactory(CustomerFactory):

    name = 'Cash Customer'
    number = 'CU0002'
    payment_term = customer_enums.PaymentTerm.CASH_CUSTOMER.value
    is_default = True


class AddressFactory(factory.DjangoModelFactory):
    customer = factory.SubFactory(CustomerFactory)

    class Meta:
        model = customers.Address


class CustomerLedgerFactory(factory.DjangoModelFactory):
    customer = factory.SubFactory(CustomerFactory)
    period = factory.SubFactory(PeriodFactory)
    journal_line = factory.SubFactory('tests.factories.JournalLineFactory')
    text = 'Customer ledger'
    balance = 2000
    date = now().date()

    class Meta:
        model = customers.Ledger
        django_get_or_create = ('customer', )


class CustomerLedgerPayment(factory.DjangoModelFactory):
    ledger = factory.SubFactory(CustomerLedgerFactory)
    payment = factory.SubFactory('tests.factories.ReceiptFactory')
    amount = 2000

    class Meta:
        model = customers.LedgerPayment
        django_get_or_create = ('ledger', )


class PricingFactory(factory.DjangoModelFactory):
    customer = factory.SubFactory(CustomerFactory)

    class Meta:
        model = customers.Pricing


class QuantityDiscount(factory.DjangoModelFactory):
    pricing = factory.SubFactory(PricingFactory)
    inventory = factory.SubFactory('tests.factories.InventoryFactory')

    class Meta:
        model = customers.QuantityDiscount


class QuantityPricing(factory.DjangoModelFactory):
    pricing = factory.SubFactory(PricingFactory)
    inventory = factory.SubFactory('tests.factories.InventoryFactory')

    class Meta:
        model = customers.QuantityPricing


class CustomerAgeAnalysisFactory(factory.DjangoModelFactory):
    customer = factory.SubFactory(CustomerFactory)
    period = factory.SubFactory(PeriodFactory)
    unallocated = 0
    one_twenty_day = 0
    ninety_day = 0
    sixty_day = 0
    thirty_day = 0
    current = 1000
    balance = factory.LazyAttribute(lambda o: o.one_twenty_day + o.ninety_day + o.sixty_day + o.thirty_day + o.current + o.unallocated)
    data = {}

    class Meta:
        model = customers.AgeAnalysis


# Inventory
class GroupLevelFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    name = 'Raw/Finished Goods'
    slug = 'raw-finished-goods'

    class Meta:
        model = inventory.GroupLevel
        django_get_or_create = ('company', 'name')


class GroupLevelItemFactory(factory.DjangoModelFactory):
    group_level = factory.SubFactory(GroupLevelFactory)
    name = 'Mix'

    class Meta:
        model = inventory.GroupLevelItem
        django_get_or_create = ('group_level', 'name')


class InventoryFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    description = factory.Sequence(lambda n: "Base Oxygen - %s" % n)
    code = factory.Sequence(lambda n: "OXY - %s" % n)

    class Meta:
        model = inventory.Inventory
        django_get_or_create = ('company', )


class BranchInventoryFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)
    branch = factory.SubFactory(BranchFactory)
    parent_branch = factory.SubFactory(BranchFactory)
    sales_account = factory.SubFactory(AccountFactory)
    cost_of_sales_account = factory.SubFactory(AccountFactory)
    gl_account = factory.SubFactory(InventoryControlAccountFactory)
    stock_adjustment_account = factory.SubFactory(AccountFactory)
    vat_code = factory.SubFactory(VatCodeFactory)
    sale_vat_code = factory.SubFactory(VatCodeFactory)
    measure = factory.SubFactory(MeasureFactory)
    unit = factory.SubFactory(UnitFactory)
    stock_unit = factory.SubFactory(UnitFactory)
    supplier = factory.SubFactory(SupplierFactory)

    class Meta:
        model = inventory.BranchInventory
        django_get_or_create = ('inventory', 'branch', 'sales_account', 'cost_of_sales_account',
                                'stock_adjustment_account', 'vat_code', 'sale_vat_code', 'measure', 'unit',
                                'stock_unit', 'supplier')

    description = factory.Sequence(lambda n: "Oxygen %s" % n)
    is_blocked = False


class BranchRecipeInventoryFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)
    branch = factory.SubFactory(BranchFactory)
    parent_branch = factory.SubFactory(BranchFactory)
    sales_account = factory.SubFactory(AccountFactory)
    cost_of_sales_account = factory.SubFactory(AccountFactory)
    gl_account = factory.SubFactory(AccountFactory)
    stock_adjustment_account = factory.SubFactory(AccountFactory)
    vat_code = factory.SubFactory(VatCodeFactory)
    sale_vat_code = factory.SubFactory(VatCodeFactory)
    measure = factory.SubFactory(MeasureFactory)
    unit = factory.SubFactory(UnitFactory)
    stock_unit = factory.SubFactory(UnitFactory)
    supplier = factory.SubFactory(SupplierFactory)
    is_recipe = True

    class Meta:
        model = inventory.BranchInventory


class PurchaseHistoryFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(BranchInventoryFactory)
    supplier = factory.SubFactory(SupplierFactory)

    class Meta:
        model = inventory.PurchaseHistory


class RecipeFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(AccountantRoleFactory)

    class Meta:
        model = inventory.Recipe
        django_get_or_create = ('inventory', 'profile', 'role', )


class RecipeInventoryFactory(factory.DjangoModelFactory):
    recipe = factory.SubFactory(RecipeFactory)
    inventory = factory.SubFactory(BranchRecipeInventoryFactory)
    unit = factory.SubFactory(UnitFactory)
    quantity = 0.001

    class Meta:
        model = inventory.RecipeInventory
        django_get_or_create = ('recipe', 'inventory', 'unit')


class PurchaseOrderFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory('tests.factories.InvoiceFactory')

    class Meta:
        model = purchaseorders.PurchaseOrder
        django_get_or_create = ('invoice', )


class PurchaseOrderInventoryFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(BranchInventoryFactory)
    purchase_order = factory.SubFactory(PurchaseOrderFactory)
    invoice = factory.SubFactory(InvoiceFactory)
    unit = factory.SubFactory(UnitFactory)
    vat_code = factory.SubFactory(VatCodeFactory)

    class Meta:
        model = inventory.PurchaseOrderInventory


class TransactionFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory(BranchFactory)
    period = factory.SubFactory(PeriodFactory)
    supplier = factory.SubFactory(SupplierFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(AccountantRoleFactory)
    date = datetime.date(2020, 1, 1)

    class Meta:
        model = inventory.Transaction
        django_get_or_create = ('period', 'supplier', 'profile', 'role')


class ReceivedFactory(factory.DjangoModelFactory):
    # transaction = factory.SubFactory(TransactionFactory)
    period = factory.SubFactory(PeriodFactory)
    supplier = factory.SubFactory(SupplierFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(AccountantRoleFactory)
    date = datetime.date(2020, 1, 1)
    total_amount = 1200
    total_vat = 200
    note = 'Default Note'

    class Meta:
        model = inventory.Received
        django_get_or_create = ('role', 'profile', 'period', 'supplier')


class ReturnedFactory(factory.DjangoModelFactory):
    received_movement = factory.SubFactory(ReceivedFactory)
    invoice = factory.SubFactory('tests.factories.InvoiceFactory')

    class Meta:
        model = inventory.Returned


class StockAdjustmentFactory(factory.DjangoModelFactory):

    class Meta:
        model = inventory.StockAdjustment


# class OpeningBalanceFactory(factory.DjangoModelFactory):
#
#     class Meta:
#         model = models.OpeningBalance


class InventoryReceivedFactory(factory.DjangoModelFactory):
    received = factory.SubFactory(ReceivedFactory)
    inventory = factory.SubFactory(BranchInventoryFactory)
    invoice_item = factory.SubFactory('tests.factories.InvoiceItemFactory')
    unit = factory.SubFactory(UnitFactory)
    vat = factory.SubFactory(VatCodeFactory)

    class Meta:
        model = inventory.InventoryReceived


class InventoryReturnedFactory(factory.DjangoModelFactory):
    returned = factory.SubFactory(ReturnedFactory)
    inventory = factory.SubFactory(BranchInventoryFactory)
    invoice_item = factory.SubFactory('tests.factories.InvoiceItemFactory')
    unit = factory.SubFactory(UnitFactory)
    vat = factory.SubFactory(VatCodeFactory)

    class Meta:
        model = inventory.InventoryReturned


class InventoryReceivedReturnedFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)

    class Meta:
        model = inventory.InventoryReceivedReturned
        django_get_or_create = ('inventory', )

    quantity = 2
    price = 10
    balance = 10
    received_value = 12
    returned_value = 15


class StockCountFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory(BranchFactory)

    class Meta:
        model = inventory.StockCount
        django_get_or_create = ('branch', )

    date = factory.Faker('date')
    captured_by = factory.Faker('name')


class InventoryStockCountFactory(factory.DjangoModelFactory):
    stock_count = factory.SubFactory(StockCountFactory)
    inventory = factory.SubFactory(BranchInventoryFactory)

    class Meta:
        model = inventory.InventoryStockCount
        django_get_or_create = ('stock_count', 'inventory')

    quantity = 10
    original_quantity = 0


class InventoryLedgerFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(BranchInventoryFactory)
    period = factory.SubFactory(PeriodFactory)
    journal = factory.SubFactory('tests.factories.JournalFactory')
    created_by = factory.SubFactory(ProfileFactory)

    class Meta:
        model = inventory.Ledger


class ManufacturingFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory(BranchFactory)
    period = factory.SubFactory(PeriodFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(AccountantRoleFactory)
    date = now().date()
    description = 'Lintels Manufactured'

    class Meta:
        model = inventory.Manufacturing
        django_get_or_create = ('branch', 'period', 'profile', 'role')


class ManufacturingRecipeFactory(factory.DjangoModelFactory):
    manufacturing = factory.SubFactory(ManufacturingFactory)
    recipe = factory.SubFactory(ReceivedFactory)

    class Meta:
        model = inventory.ManufacturingRecipe


# Sales
class SalesTaxInvoiceFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory(BranchFactory)
    invoice_type = factory.SubFactory(SalesTaxInvoiceTypeFactory)
    period = factory.SubFactory(PeriodFactory)
    customer = factory.SubFactory(CustomerFactory)
    total_amount = 1024.40
    vat_amount = 124.40
    invoice_number = '452324232'
    po_number = 'PO01231231'
    invoice_date = now().date()
    accounting_date = now().date()
    invoice_id = factory.Sequence(lambda i: str(i))
    net_amount = factory.LazyAttribute(lambda obj: obj.total_amount - obj.vat_amount)
    open_amount = factory.LazyAttribute(lambda obj: obj.total_amount)
    due_date = factory.LazyAttribute(lambda obj: obj.accounting_date + relativedelta(months=2))

    class Meta:
        model = sales.Invoice
        django_get_or_create = ('branch', 'invoice_type', 'invoice_id', )

    class Params:
        paid = factory.Trait(
            payment_date=now().date() + relativedelta(months=1),
            open_amount=0
        )


class CreditNoteInvoiceFactory(SalesTaxInvoiceFactory):
    invoice_type = factory.SubFactory(SalesCreditInvoiceTypeFactory)
    total_amount = 1024.40 * -1
    vat_amount = 124.40 * -1
    invoice_number = '4-4232'
    po_number = 'PO-123'
    invoice_id = factory.Sequence(lambda i: str(i))


class InventoryItemFactory(factory.DjangoModelFactory):
    quantity = 10
    total_amount = 150
    price = 15

    class Meta:
        model = sales.InvoiceItem
        django_get_or_create = ('invoice', 'inventory')


class AccountItemFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory(SalesTaxInvoiceFactory)
    item_type = sales_enums.InvoiceItemType.GENERAL.value
    vat_code = factory.SubFactory(VatCodeFactory)
    account = factory.SubFactory(AccountFactory)
    discount = 0
    customer_discount = 0
    net_price = 150
    vat_amount = 10
    vat = 15
    status = sales_enums.InvoiceItemStatus.PENDING
    quantity = 10
    total_amount = 150
    price = 15
    description = 'Transport'
    available_quantity = factory.LazyAttribute(lambda o: o.quantity)

    class Meta:
        model = sales.InvoiceItem
        django_get_or_create = ('invoice', 'account', 'description')


class TextItemFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory(SalesTaxInvoiceFactory)
    description = 'Description of item'

    class Meta:
        model = sales.InvoiceItem
        django_get_or_create = ('invoice', 'description')


class CashUpFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory(BranchFactory)
    period = factory.SubFactory(PeriodFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(ManagerRoleFactory)

    class Meta:
        model = sales.CashUp
        django_get_or_create = ('branch', 'period', )

    total_amount = 1200
    date = now().date()


class ReceiptFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory(BranchFactory)
    period = factory.SubFactory(PeriodFactory)
    customer = factory.SubFactory(CustomerFactory)
    payment_method = factory.SubFactory(PaymentMethodFactory)
    created_by = factory.SubFactory(ProfileFactory)
    date = now().date()
    amount = 1500
    balance = factory.LazyAttribute(lambda o: o.amount)
    reference = 'Payment for Invoices'

    class Meta:
        model = sales.Receipt
        django_get_or_create = ('branch', 'period', 'customer')

    class Params:
        updated = factory.Trait(
            cash_up=factory.SubFactory(CashUpFactory)
        )


class InvoiceReceiptFactory(factory.DjangoModelFactory):
    receipt = factory.SubFactory(ReceiptFactory)
    invoice = factory.SubFactory(SalesTaxInvoiceFactory)
    amount = 1000
    discount = 0

    class Meta:
        model = sales.InvoiceReceipt
        django_get_or_create = ('receipt', 'invoice', 'amount')


class SundryReceiptFactory(factory.DjangoModelFactory):
    receipt = factory.SubFactory(ReceiptFactory)
    parent = factory.SubFactory(ReceiptFactory)
    amount = 1000

    class Meta:
        model = sales.SundryReceiptAllocation
        django_get_or_create = ('receipt', 'parent', )


class ReceiptPaymentFactory(factory.DjangoModelFactory):
    receipt = factory.SubFactory(ReceiptFactory)
    payment_method = factory.SubFactory(PaymentMethodFactory)
    amount = 1000

    class Meta:
        model = sales.ReceiptPayment
        django_get_or_create = ('receipt', 'payment_method', )


class SalesmanFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    code = 'VIC'
    name = 'Victor'

    class Meta:
        model = sales.Salesman
        django_get_or_create = ('company', 'code')


# Journals
class JournalFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    year = factory.SubFactory(YearFactory)
    period = factory.SubFactory(PeriodFactory)
    role = factory.SubFactory(AccountantRoleFactory)
    # created_by = factory.SubFactory(ProfileFactory)
    journal_text = 'Month end'
    journal_number = factory.Sequence(lambda c: f'JN0000{c + 1}')
    module = Module.SYSTEM

    class Meta:
        model = journals.Journal
        django_get_or_create = ('company', 'year', 'period', 'journal_number', )


class JournalLineFactory(factory.DjangoModelFactory):
    journal = factory.SubFactory(JournalFactory)
    account = factory.SubFactory(AccountFactory)
    suffix = 'FAX'
    debit = Decimal('1000')
    credit = Decimal(0)

    class Meta:
        model = journals.JournalLine
        django_get_or_create = ('journal', 'account')


class VatJournalLineFactory(JournalLineFactory):
    vat_code = factory.SubFactory(VatCodeFactory)

    class Params:
        processed = factory.Trait(
            vat_report=factory.SubFactory('tests.factories.VatReportFactory')
        )


class SupplierJournalLineFactory(JournalLineFactory):
    content_object = factory.SubFactory('tests.factories.SupplierFactory')


class CustomerJournalLineFactory(JournalLineFactory):
    content_object = factory.SubFactory('tests.factories.CustomerFactory')


# Provisional Tax Estimates
class FirstProvisionalTaxEstimateFactory(factory.DjangoModelFactory):
    year = factory.SubFactory(YearFactory)
    to_period = factory.SubFactory(PeriodFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(AccountantRoleFactory)

    class Meta:
        model = provisionaltaxes.Estimate
        django_get_or_create = ('year', 'to_period', 'profile', 'role', )

    due_date = now().date()
    turnover = 3693016
    tax_rate = 28
    estimated_income = -50672
    income_deductable = -3762911
    income_non_deductable = 0
    estimated_taxable_income = 0
    assesed_loss_previous_year = 0
    year_tax = 3000
    period_tax = 100
    rebates = 100
    employee_tax = 200
    foreign_tax = 3000
    tax_paid = 3000
    late_payment_penalty = 0
    late_payment_interest = 0
    total_amount = 1220
    tax_estimate_type = ProvisionalTaxEstimateType.FIRST.value


class SecondProvisionalTaxEstimateFactory(factory.DjangoModelFactory):
    year = factory.SubFactory(YearFactory)
    to_period = factory.SubFactory(PeriodFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(AccountantRoleFactory)

    class Meta:
        model = provisionaltaxes.Estimate
        django_get_or_create = ('year', 'to_period', 'profile', 'role')

    due_date = now().date()
    turnover = 3693016
    tax_rate = 28
    estimated_income = -50672
    income_deductable = -3762911
    income_non_deductable = 0
    estimated_taxable_income = 0
    assesed_loss_previous_year = 0
    year_tax = 3000
    period_tax = 100
    rebates = 100
    employee_tax = 200
    foreign_tax = 3000
    tax_paid = 3000
    late_payment_penalty = 0
    late_payment_interest = 0
    total_amount = 20000
    tax_estimate_type = ProvisionalTaxEstimateType.SECOND.value


class ThirdProvisionalTaxEstimateFactory(factory.DjangoModelFactory):
    year = factory.SubFactory(YearFactory)
    to_period = factory.SubFactory(PeriodFactory)
    profile = factory.SubFactory(ProfileFactory)
    role = factory.SubFactory(AccountantRoleFactory)

    class Meta:
        model = provisionaltaxes.Estimate
        django_get_or_create = ('year', 'to_period', 'profile', 'role')

    due_date = now().date()
    turnover = 3693016
    tax_rate = 28
    estimated_income = -50672
    income_deductable = -3762911
    income_non_deductable = 0
    estimated_taxable_income = 0
    assesed_loss_previous_year = 0
    year_tax = 3000
    period_tax = 100
    rebates = 100
    employee_tax = 200
    foreign_tax = 3000
    tax_paid = 3000
    late_payment_penalty = 0
    late_payment_interest = 0
    total_amount = 30000
    tax_estimate_type = ProvisionalTaxEstimateType.THIRD.value


# Distributions
class DistributionFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    title = 'Supplier Distr.'
    supplier = factory.SubFactory(SupplierFactory)
    distribution_id = factory.Sequence(lambda n: n + 1)
    nb_months = 3
    starting_date = now().date()
    account = factory.SubFactory(AccountFactory)
    is_credit = False
    total_amount = 12000
    role = factory.SubFactory(AccountantRoleFactory)
    profile = factory.SubFactory(ProfileFactory)

    class Meta:
        model = distribution_models.Distribution


class InvoiceAccountDistributionFactory(DistributionFactory):
    content_object = factory.SubFactory(InvoiceAccountFactory)


class JournalLineDistributionFactory(DistributionFactory):
    content_object = factory.SubFactory(JournalLineFactory)


class DistributionAccountFactory(factory.DjangoModelFactory):
    distribution = factory.SubFactory(InvoiceAccountFactory)
    account = factory.SubFactory(AccountFactory)
    debit = 12000
    credit = 0
    period = factory.SubFactory(PeriodFactory)
    date = factory.LazyAttribute(lambda obj: obj.period.from_date)
    ledger_line = factory.SubFactory(JournalLineFactory)

    class Meta:
        model = distribution_models.DistributionAccount


# Payroll
class EmployeeFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    employee_id = factory.Sequence(lambda n: n + 1)
    first_name = faker.first_name()
    second_name = 'Second'
    surname = faker.last_name()
    address_line_1 = 'Primrose'
    address_line_2 = 'Exeter Road'
    city = 'Paarl'
    province = 'North'
    postal_code = '7800'
    birthday = now().date() + relativedelta(years=-33)
    income_tax_number = 'TAX0293123'
    start_date = now().date() + relativedelta(years=-1, months=5)

    class Meta:
        model = payroll_models.Employee


# Important Documents/Certificate
class CertificateFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    document_type = factory.SubFactory(TaxInvoiceTypeFactory)
    supplier = factory.SubFactory(SupplierFactory)
    supplier_name = factory.LazyAttribute(lambda obj: obj.supplier.name)
    document_number = factory.LazyAttributeSequence(lambda obj, n: f'{obj.company.code}-000{n}')
    certificate_id = factory.Sequence(lambda n: n + 1)
    reference_1 = 'Ref 1'
    reference_2 = 'Ref 2'
    employee = factory.SubFactory(EmployeeFactory)
    document_date = now().date() + relativedelta(months=3)
    expiry_date = now().date() + relativedelta(months=19)
    reminder = True
    reminder_days = 30
    remind_role = factory.SubFactory(AccountantRoleFactory)
    comment = 'Comment'
    filename = 'reg-certificate.pdf'
    owner = factory.SubFactory(ProfileFactory)
    # profiles = models.ManyToManyField('accounts.Profile')

    class Meta:
        model = certificate_models.Certificate


class VatReportFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    year = factory.SubFactory(YearFactory)
    category_period = 1
    period = factory.SubFactory(PeriodFactory)
    ledger = factory.SubFactory(JournalFactory)
    report_number = factory.Sequence(lambda n: f'VAT-{n}')
    created_by = factory.SubFactory(ProfileFactory)

    class Meta:
        model = vat_models.VatReport
        django_get_or_create = ('company', 'year', 'period', 'category_period')


# Budget
class BudgetFactory(factory.DjangoModelFactory):
    year = factory.SubFactory(YearFactory)

    class Meta:
        model = Budget
        django_get_or_create = ('year', )


class BudgetAccountFactory(factory.DjangoModelFactory):
    budget = factory.SubFactory(BudgetFactory)
    account = factory.SubFactory(AccountFactory)

    class Meta:
        model = BudgetAccount
        django_get_or_create = ('budget', 'account')


class BudgetAccountPeriodFactory(factory.DjangoModelFactory):
    budget_account = factory.SubFactory(BudgetAccountFactory)
    period = factory.SubFactory(PeriodFactory)
    amount = 1000

    class Meta:
        model = BudgetAccountPeriod
        django_get_or_create = ('budget_account', 'period')


# Sources
class IMAPEmailFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    label = 'Docuflow'
    host = 'outlook.office365.com'
    ssl = True
    port = '993'
    username = 'senddoc'
    password = '1235'
    mailbox = 'INBOX'
    interval = 60
    uncompress = 'y'
    enabled = True

    class Meta:
        model = IMAPEmail
        django_get_or_create = ('host', 'username', 'password')


# Fixed Assets
class FixedAssetCategoryFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    category = factory.SubFactory(SystemFixedAssetCategoryFactory)
    name = 'Machines'
    account = factory.SubFactory(AccountFactory)
    depreciation_method = 1
    depreciation_account = factory.SubFactory(AccountFactory)
    accumulated_depreciation_account = factory.SubFactory(AccountFactory)
    depreciation_rate = 20
    is_active = 1

    class Meta:
        model = fixed_assets_models.Category
        django_get_or_create = ('company', 'category', 'name')


class FixedAssetFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    year = factory.SubFactory(OpenYearFactory)
    name = 'Kompressor'
    category = factory.SubFactory(FixedAssetCategoryFactory)
    cost_price = 120000
    net_book_value = 100000
    date_purchased = now().date() + relativedelta(years=-3)
    asset_number = 'kompressor-101'
    own_asset_number = 'kompressor-101'
    supplier = 'Mercedes'
    location = 'Harare'
    quantity = 1
    asset_account = factory.SubFactory(AccountFactory)
    depreciation_method = fixed_assets_enums.DepreciationMethod.STRAIGHT_LINE
    depreciation_account = factory.SubFactory(AccountFactory)
    depreciation_rate = 20
    accumulated_depreciation_account = factory.SubFactory(AccountFactory)

    class Meta:
        model = fixed_assets_models.FixedAsset
        django_get_or_create = ('company', 'own_asset_number')


class FixedAssetAdditionFactory(factory.DjangoModelFactory):
    name = 'Kompressor'
    addition_type = fixed_assets_enums.AdditionType.ADDITION
    fixed_asset = factory.SubFactory(FixedAssetFactory)
    date = now().date() + relativedelta(years=-3)
    amount = 120000

    class Meta:
        model = fixed_assets_models.FixedAsset
        django_get_or_create = ('fixed_asset', 'date')


class FixedAssetOpeningBalanceFactory(factory.DjangoModelFactory):
    year = factory.SubFactory(YearFactory)
    fixed_asset = factory.SubFactory(FixedAssetFactory)
    asset_value = 120000
    depreciation_value = 0

    class Meta:
        model = fixed_assets_models.OpeningBalance
        django_get_or_create = ('fixed_asset', 'year')


class FixedAssetLocationFactory(factory.DjangoModelFactory):
    fixed_asset = factory.SubFactory(FixedAssetFactory)
    branch = factory.SubFactory(BranchFactory)
    percentage = 20

    class Meta:
        model = fixed_assets_models.Location
        django_get_or_create = ('fixed_asset', 'branch')


class FixedAssetDepreciationSplitFactory(factory.DjangoModelFactory):
    fixed_asset = factory.SubFactory(FixedAssetFactory)
    object_item = factory.SubFactory(ObjectItemFactory)
    percentage = 20

    class Meta:
        model = fixed_assets_models.DepreciationSplit
        django_get_or_create = ('fixed_asset', 'object_item')


class FixedAssetUpdateFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    year = factory.SubFactory(YearFactory)
    period = factory.SubFactory(PeriodFactory)
    date = now().date() + relativedelta(years=-3)
    report_unique_number = 'FAUpdate-1'

    class Meta:
        model = fixed_assets_models.Update
        django_get_or_create = ('fixed_asset', 'year', 'period')


class FixedAssetUpdateAssetFactory(factory.DjangoModelFactory):
    fixed_asset = factory.SubFactory(FixedAssetFactory)
    asset_update = factory.SubFactory(FixedAssetUpdateFactory)
    period = factory.SubFactory(PeriodFactory)
    asset_value = 690
    depreciation_value = 690
    book_value = 1200

    class Meta:
        model = fixed_assets_models.FixedAssetUpdate
        django_get_or_create = ('fixed_asset', 'asset_update', 'period')


class DisposalFactory(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyFactory)
    period = factory.SubFactory(PeriodFactory)
    invoice_number = 'INV000123-2'
    selling_price = 20000
    vat_code = factory.SubFactory(VatCodeFactory)
    net_profit_loss = 20000
    disposal_date = now().date() + relativedelta(years=-1)
    role = factory.SubFactory(RoleFactory)

    class Meta:
        model = fixed_assets_models.Disposal
        django_get_or_create = ('company', 'period',)


class FixedAssetDisposalFactory(factory.DjangoModelFactory):
    fixed_asset = factory.SubFactory(FixedAssetFactory)
    disposal = factory.SubFactory(DisposalFactory)
    asset_value = 20000
    depreciation_value = 900
    book_value = 12000

    class Meta:
        model = fixed_assets_models.FixedAssetDisposal
        django_get_or_create = ('fixed_asset', 'disposal',)
