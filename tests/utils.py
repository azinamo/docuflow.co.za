from decimal import Decimal
from datetime import date, datetime
from typing import Union, Optional

from rest_framework.fields import empty, DateField, DateTimeField


def format_decimal(value, decimal_format='{0:.2f}'):
    return decimal_format.format(value)


def to_decimal(value: str):
    return Decimal(value).quantize(Decimal('.01'))


def format_datetime(dt: Union[date, datetime], output_format=empty) -> Optional[str]:
    """
    Formats a datetime value in the same way as django-rest-framework does.
    This accepts and returns None, too, for convenience.
    """
    if dt:
        field = DateTimeField if isinstance(dt, datetime) else DateField
        return field(format=output_format).to_representation(dt)


def to_date(date_str: str):
    try:
        if '-' in date_str:
            return datetime.strptime(date_str, '%Y-%m-%d')
        elif '/' in date_str:
            return datetime.strptime(date_str, '%Y/%m/%d')
        return datetime.strptime(date_str, '%Y%m%d')
    except ValueError:
        return None
