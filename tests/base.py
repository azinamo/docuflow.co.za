import os
import json
from collections import namedtuple

from django.urls import reverse
from django.conf import settings
from django.utils.http import urlencode
from django.test import TestCase, Client

from docuflow.apps.company.enums import SubLedgerModule
from tests import factories

ApiResponse = namedtuple('ApiResponse', 'status_code data headers response_raw')


class BaseTestCase(TestCase):

    def ajax_post(self, url, data, follow=True):
        res = self.client.post(path=url, data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}, follow=follow)
        try:
            response_data = json.loads(res.content.decode('utf8')) if res.content else None
        except ValueError:
            response_data = res.content
        return ApiResponse(res.status_code, response_data, res.headers, res)

    def ajax_get(self, url, data=None):
        res = self.client.get(path=url, data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        try:
            response_data = json.loads(res.content.decode('utf8')) if res.content else None
        except ValueError:
            response_data = res.content
        return ApiResponse(res.status_code, response_data, res.headers, res)

    def ajax_delete(self, url, data=None):
        res = self.client.delete(path=url, data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        try:
            response_data = json.loads(res.content.decode('utf8')) if res.content else None
        except ValueError:
            response_data = res.content
        return ApiResponse(res.status_code, response_data, res.headers, res)

    def api_call(self, url, data=None, params=None, method=None, user=None, **kw):
        kw.setdefault('content_type', 'application/json')
        method = method and method.lower()
        client = self.client
        if params:
            kw['QUERY_STRING'] = urlencode(params, doseq=True)
        if data is not None:
            client_method = getattr(client, method or 'post')
            data = json.dumps(data) if 'application/json' in kw['content_type'] and method != 'get' else data
            res = client_method(url, data=data, **kw)
        else:
            client_method = getattr(client, method or 'get')
            res = client_method(url, **kw)
        try:
            response_data = json.loads(res.content.decode('utf8')) if res.content else None
        except ValueError:
            response_data = res.content
        return ApiResponse(res.status_code, response_data, res.headers, res)


class AuthenticatedTestCase(BaseTestCase):

    def setUp(self) -> None:
        self.company = factories.CompanyFactory()
        self.expenditure_account = factories.AccountFactory(company=self.company, code='8230/000', name='Expenditure Control')
        self.supplier_account = factories.SupplierAccountFactory(company=self.company, code='9290/001', name='Supplier Control')
        self.customer_account = factories.AccountFactory(company=self.company, code='8000/003', name='Customer Control')
        self.vat_account = factories.AccountFactory(company=self.company, code='9505/000', name='DF Control - Input Vat')

        self.vat_receivable = factories.AccountFactory(company=self.company, code='1480', name='Vat Receivable')
        self.vat_payable = factories.AccountFactory(company=self.company, code='2480', name='Vat Payable')

        self.default_discount_allowed_account = factories.AccountFactory(company=self.company, code='3550',
                                                                         name='Discount allowed for cash')

        self.company.default_discount_allowed_account = self.default_discount_allowed_account
        self.company.default_vat_receivable_account = self.vat_receivable
        self.company.default_vat_payable_account = self.vat_payable
        self.company.default_vat_account = self.vat_account
        self.company.default_expenditure_account = self.expenditure_account
        self.company.default_supplier_account = self.supplier_account
        self.company.default_customer_account = self.customer_account
        self.company.save()

        user = factories.UserFactory()
        group = factories.PermissionGroupFactory()

        self.logger = factories.LoggerRoleFactory(company=self.company)
        self.accountant = factories.AccountantRoleFactory(company=self.company)
        self.manager = factories.ManagerRoleFactory(company=self.company)
        self.profile = factories.UserProfileFactory(
            company=self.company, user=user,
            roles=[self.logger, self.accountant, self.manager]
        )
        self.year = factories.OpenYearFactory(company=self.company)
        self.branch = factories.BranchFactory(company=self.company)
        self.client = Client()
        data = {'username': self.profile.user.username, 'password': 'PASSWORD'}
        self.client.post(reverse('user_ajax_login', kwargs={'slug': self.company.slug}), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        role = self.profile.roles.first()
        self.role = role
        session = self.client.session
        session['company_slug'] = self.company.slug
        session['company'] = self.company.id
        session['year'] = self.year.id
        session['branch'] = self.branch.id
        session['role'] = role.id if role else None
        session.save()

    def tearDown(self):
        route_path = os.path.join(settings.BASE_DIR, 'docuflow', 'routes', f'{self.company.slug}_urls.py')
        print(f"-----------tearDown----------{route_path} and exists {os.path.exists(path=route_path)}")
        if os.path.exists(path=route_path):
            os.unlink(route_path)

    # def get_token(self, user):
    #     payload = api_settings.JWT_PAYLOAD_HANDLER(user)
    #     token = api_settings.JWT_ENCODE_HANDLER(payload)
    #     return token
    #
    # def get_client(self, user=None):
    #     if user:
    #         token = self.get_token(user)
    #         client = self.client_class(HTTP_AUTHORIZATION=f'JWT {token}')
    #         client.user = user
    #         client.auth_token = token
    #         return client
    #     return self.client_class()
    #


class NonVatCompanyAuthenticatedTestCase(AuthenticatedTestCase):

    def setUp(self) -> None:
        self.company = factories.CompanyFactory(
            # default_goods_received_account=factories.GoodsReceivedAccountFactory(
            #     code='2700/000', name='Discount Received for Cash'),
            # default_inventory_variance_account=factories.InventoryReceivedVariationAccountFactory(
            #     code='2200/001', name='Purch Var - Raw Materials'),
            # expenditure_account=factories.AccountFactory(code='8200/000', name='Expenditure Control'),
        )
        self.expenditure_account = factories.AccountFactory(company=self.company, code='8200/000',
                                                            name='Expenditure Control')
        self.supplier_account = factories.AccountFactory(company=self.company, code='8200/001', name='Supplier Control')
        self.customer_account = factories.AccountFactory(company=self.company, code='8200/003', name='Customer Control')

        self.company.default_expenditure_account = self.expenditure_account
        self.company.default_supplier_account = self.supplier_account
        self.company.default_customer_account = self.customer_account
        self.company.save()

        user = factories.UserFactory()
        self.logger = factories.LoggerRoleFactory(company=self.company)
        self.accountant = factories.AccountantRoleFactory(company=self.company)
        self.manager = factories.ManagerRoleFactory(company=self.company)
        self.profile = factories.ProfileFactory(
            company=self.company, user=user,
            roles=[self.logger, self.accountant, self.manager]
        )
        self.year = factories.OpenYearFactory(company=self.company)
        self.branch = factories.BranchFactory(company=self.company)
        self.client = Client()
        data = {'username': self.profile.user.username, 'password': 'PASSWORD'}
        self.client.post(reverse('user_ajax_login', kwargs={'slug': self.company.slug}), data=data,
                         **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        role = self.profile.roles.first()
        self.role = role
        session = self.client.session
        session['company_slug'] = self.company.slug
        session['company'] = self.company.id
        session['year'] = self.year.id
        session['branch'] = self.branch.id
        session['role'] = role.id if role else None
        session.save()

    def tearDown(self):
        route_path = os.path.join(settings.BASE_DIR, 'docuflow', 'routes', f'{self.company.slug}_urls.py')
        print(f"-----------tearDown----------{route_path} and exists {os.path.exists(path=route_path)}")
        if os.path.exists(path=route_path):
            os.unlink(route_path)