#!/bin/bash
cd /prod/docuflow
git add .
git commit -am 'Merge'
git checkout master
git pull origin master
source ../bin/activate
pip install -r requirements.txt
python manage.py migrate
supervisorctl restart docuflow
deactivate
