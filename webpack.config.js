const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const BundleTracker = require('webpack-bundle-tracker');
// const VueLoaderPlugin = require('vue-loader/lib/plugin');
// const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');

module.exports = {
    context: __dirname,
    // entry: './assets/js/index.js',
    // output: {
    //     path: path.resolve('./assets/bundles/'),
    //     filename: 'app.js'
    // },
    entry: {
        payment: './build/components/Payment/List/index.js',
        journal: './build/components/Journal/List/index.js',
        invoice: './build/components/Invoice/List/index.js'
    },
    output: {
        path: path.resolve(__dirname, './static/bundles'),
        publicPath:  '.',
        filename: 'js/[name].min.js?[chunkhash]'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new ExtractTextPlugin({
            filename: 'css/style.min.css?[contenthash]'
        }),
        new AssetsPlugin({
            filename: 'assets.json',
            path: path.resolve(__dirname, './static'),
            fullPath: false
        }),
        //new BundleTracker({filename: './webpack-stats.json'}),
        //new VueLoaderPlugin(),
        // new webpack.ProvidePlugin({
        //       $: "jquery",
        //       jQuery: "jquery",
        //       jquery: "jquery",
        //       "window.jQuery": "jquery",
        //       Util: 'exports-loader?Util!bootstrap/js/dist/util'
        //     }),
    ],
    devServer: {
        contentBase: false,
        hot: true,
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    },
    module: {
        // loaders: [
        //     { test: /\.scss?$/, loader: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap'] },
        //     { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
        //     { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
        //     { loader: "imports-loader" }
        // ],

        rules:  [
            {
               test: /\.(scss)$/,
               use: [
                  {
                    // Interprets `@import` and `url()` like `import/require()` and will resolve them
                    loader: 'css-loader'
                  },
                  {
                    // Loader for webpack to process CSS with PostCSS
                    loader: 'postcss-loader',
                    options: {
                      plugins: function () {
                        return [
                          require('autoprefixer')
                        ];
                      }
                    }
                  },
                  {
                    // Loads a SASS/SCSS file and compiles it to CSS
                    loader: 'sass-loader'
                  }
               ]
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                  fallback: "vue-style-loader",
                  use: [
                      {
                        loader: 'css-loader',
                        options: {
                            minimize: 'production'
                        }
                      }
                  ]
                })
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            }
        ],
    }
    // resolve: {
    //     alias: {
    //         vue: 'vue/dist/vue.js',
    //         'jquery': path.join(__dirname, './node_modules/jquery/dist/jquery.min.js')
    //     }
    // },

};
