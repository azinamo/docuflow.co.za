#!/bin/bash

# Define the folder path
folder_path="/prod/docuflow/media/"

# Define the desired permissions (e.g., 755 for read/write/execute for owner and read/execute for group and others)
permissions="777"

# Check if the folder exists
if [ -d "$folder_path" ]; then
    # Apply permissions to the folder
    chmod -R "$permissions" "$folder_path"
    echo "Permissions set successfully for $folder_path"
else
    echo "Folder not found: $folder_path"
fi