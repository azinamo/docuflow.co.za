from django.conf.urls import url, include

urlpatterns = [
    url(r'^references/', include('docuflow.apps.references.urls')),
    url(r'^roles/', include('docuflow.apps.roles.urls')),
    url(r'^workflow/', include('docuflow.apps.workflow.urls')),
    url(r'^sources/', include('docuflow.apps.sources.urls')),
    url(r'^documents/', include('docuflow.apps.documents.urls')),
    url(r'^invoices/', include('docuflow.apps.invoice.urls')),
    url(r'^inventory/', include('docuflow.apps.inventory.urls')),
    url(r'^plans/', include('docuflow.apps.subscription.urls')),
    url(r'^system/', include('docuflow.apps.settings.urls')),
    url(r'^common/', include('docuflow.apps.common.urls')),
    url(r'^company/', include('docuflow.apps.company.urls')),
    url(r'^users/', include('docuflow.apps.accounts.urls')),
    url(r'^notifications/', include('docuflow.apps.notifications.urls')),
    url(r'^suppliers/', include('docuflow.apps.supplier.urls')),
    url(r'^accountposting/', include('docuflow.apps.accountposting.urls')),
    url(r'^years/', include('docuflow.apps.period.urls')),
    url(r'^journals/', include('docuflow.apps.journals.urls')),
    url(r'^intervention/', include('docuflow.apps.intervention.urls')),
    url(r'^vat/', include('docuflow.apps.vat.urls')),
    url(r'^sales/', include('docuflow.apps.sales.urls')),
    url(r'^fixedasset/', include('docuflow.apps.fixedasset.urls'))
]
