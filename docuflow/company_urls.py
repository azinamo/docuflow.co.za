"""docuflow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.static import serve

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('docuflow.apps.pages.urls')),
    url(r'^', include('docuflow.apps.authentication.urls')),
    url(r'^comment/', include('docuflow.apps.comment.urls')),
    url(r'^references/', include('docuflow.apps.references.urls')),
    url(r'^payment/', include('docuflow.apps.invoice.modules.payment.urls')),
    url(r'^roles/', include('docuflow.apps.roles.urls')),
    url(r'^workflow/', include('docuflow.apps.workflow.urls')),
    url(r'^sources/', include('docuflow.apps.sources.urls')),
    url(r'^documents/', include('docuflow.apps.documents.urls')),
    url(r'^invoices/', include('docuflow.apps.invoice.urls')),
    url(r'^inventory/', include('docuflow.apps.inventory.urls')),
    url(r'^plans/', include('docuflow.apps.subscription.urls')),
    url(r'^system/', include('docuflow.apps.system.urls')),
    url(r'^company/', include('docuflow.apps.company.urls')),
    url(r'^users/', include('docuflow.apps.accounts.urls')),
    url(r'^notifications/', include('docuflow.apps.notifications.urls')),
    url(r'^suppliers/', include('docuflow.apps.supplier.urls')),
    url(r'^budgets/', include('docuflow.apps.budget.urls')),
    url(r'^accountposting/', include('docuflow.apps.accountposting.urls')),
    url(r'^years/', include('docuflow.apps.period.urls')),
    url(r'^journals/', include('docuflow.apps.journals.urls')),
    url(r'^reports/', include('docuflow.apps.reports.urls')),
    url(r'^intervention/', include('docuflow.apps.intervention.urls')),
    url(r'^vat/', include('docuflow.apps.vat.urls')),
    url(r'^sales/', include('docuflow.apps.sales.urls')),
    url(r'^fixedasset/', include('docuflow.apps.fixedasset.urls')),
    url(r'^settings/', include('docuflow.apps.django_mfa.urls')),
]
urlpatterns += [
    url(r'^company_slug/references/', include('docuflow.apps.references.urls')),
    url(r'^company_slug/payment/', include('docuflow.apps.invoice.modules.payment.urls')),
    url(r'^company_slug/roles/', include('docuflow.apps.roles.urls')),
    url(r'^company_slug/workflow/', include('docuflow.apps.workflow.urls')),
    url(r'^company_slug/sources/', include('docuflow.apps.sources.urls')),
    url(r'^company_slug/invoices/', include('docuflow.apps.invoice.urls')),
    url(r'^company_slug/inventory/', include('docuflow.apps.inventory.urls')),
    url(r'^company_slug/customer/', include('docuflow.apps.customer.urls')),
    url(r'^company_slug/sales/', include('docuflow.apps.sales.urls')),
    url(r'^company_slug/important/documents/', include('docuflow.apps.certificate.urls')),
    url(r'^company_slug/subscription/', include('docuflow.apps.subscription.urls')),
    url(r'^company_slug/system/', include('docuflow.apps.system.urls')),
    url(r'^company_slug/budgets/', include('docuflow.apps.budget.urls')),
    url(r'^company_slug/common/', include('docuflow.apps.common.urls')),
    url(r'^company_slug/company/', include('docuflow.apps.company.urls')),
    url(r'^company_slug/users/', include('docuflow.apps.accounts.urls') ),
    url(r'^company_slug/notifications/', include('docuflow.apps.notifications.urls') ),
    url(r'^company_slug/suppliers/', include('docuflow.apps.supplier.urls')),
    url(r'^company_slug/accountposting/', include('docuflow.apps.accountposting.urls')),
    url(r'^company_slug/years/', include('docuflow.apps.period.urls')),
    url(r'^company_slug/journals/', include('docuflow.apps.journals.urls')),
    url(r'^company_slug/reports/', include('docuflow.apps.reports.urls')),
    url(r'^company_slug/intervention/', include('docuflow.apps.intervention.urls')),
    url(r'^company_slug/vat/', include('docuflow.apps.vat.urls')),
    url(r'^company_slug/fixedasset/', include('docuflow.apps.fixedasset.urls')),
    url(r'^company_slug/mfa/', include('docuflow.apps.django_mfa.urls')),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^tinymce/', include('tinymce.urls'))
]

if settings.DEV:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns


_media_url = settings.MEDIA_URL
if _media_url.startswith('/'):
    _media_url = _media_url[1:]
    urlpatterns += [url(r'^%s(?P<path>.*)$' % _media_url, serve, {'document_root': settings.MEDIA_ROOT})]
del(_media_url, serve)

admin.site.site_title = 'Docuflow Administration'
admin.site.site_header = 'Docuflow Administration'
