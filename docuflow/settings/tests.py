from docuflow.settings.base import *

# logging.disable(logging.CRITICAL)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
IS_TEST = True
DEBUG = False
DEV = True
TESTING = True
TEMPLATE_DEBUG = False
DEBUG_LOGGING = False
SECRET_KEY = 't0b(9*q5bmq(30c5r9o3ztt_q!#9qx(zpb39y+$t6&$-xffmfo'

# for sorl:
THUMBNAIL_DEBUG = False

INTERNAL_IPS = ['local.df.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    }
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'docuflow.apps.company.middleware.CompanyMiddleWare',
    # 'invoice.middleware.InvoiceViewerMiddleWare',
]

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
    },
    'select2': {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
    }
}

PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]
DEFAULT_FILE_STORAGE = 'inmemorystorage.InMemoryStorage'


# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
# TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG  # noqa F405
# TEMPLATES[0]["OPTIONS"]["loaders"] = [  # noqa F405
#     (
#         "django.template.loaders.cached.Loader",
#         [
#             "django.template.loaders.filesystem.Loader",
#             "django.template.loaders.app_directories.Loader",
#         ],
#     )
# ]


class DisableMigrations(dict):
    """ no migrations to run the tests fast """
    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return None


# DATABASES = DisableMigrations()

#
MIGRATION_MODULES = DisableMigrations()


# removing Django Debug Toolbar
INSTALLED_APPS = [app for app in INSTALLED_APPS if app not in ('debug_toolbar',)]
MIDDLEWARE_CLASSES = [
    cls for cls in MIDDLEWARE if cls not in ('debug_toolbar.middleware.DebugToolbarMiddleware',)
]

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
