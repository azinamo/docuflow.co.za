from docuflow.settings.base import * # noqa

ALLOWED_HOSTS = ['*']

INTERNAL_IPS = ['127.0.0.1', 'localhost', '91.92.136.230', 'local.docuflow.co.za', 'docuflow.co.za',
                'dev.docuflow.co.za']

DEBUG = True
DEV = True
SITE_ID = 1
IS_TEST = False

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient'
        },
        'KEY_PREFIX': 'df'
    },
    'select2': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

CACHE_TTL = 60 * 15

PO_FROM_EMAIL = 'PO-from-our-system@Docuflow.co.za'
DEFAULT_FROM_EMAIL = 'info@docuflow.co.za'
NO_REPLY_EMAIL = 'no-reply@docuflow.co.za'
WEB_ERRORS = 'admire@originate.co.za'
ADMIN_EMAIL = ('admire@originate.co.za', 'azinamo@gmail.com', )

EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"
SENDGRID_API_KEY = "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"

COUNTRY_CODE = 'ZA'

DATA_UPLOAD_MAX_NUMBER_FIELDS = None

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            # exact format is not important, this is the minimum information
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
    },
    # 'filters': {
    #     'require_debug_false': {
    #         '()': 'django.utils.log.RequireDebugFalse',
    #     },
    #     'require_debug_true': {
    #         '()': 'django.utils.log.RequireDebugTrue',
    #     },
    # },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console'
        },
        'mail_admins': {
            'level': 'ERROR',
            # 'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        # 'django.db.backends': {
        #     'level': 'DEBUG',
        # },
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'docuflow': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
    }
})

# Todo Django emailtools package for emails
