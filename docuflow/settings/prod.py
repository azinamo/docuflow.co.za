import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from docuflow.settings.base import *  # noqa

ALLOWED_HOSTS = ['docuflow.co.za', 'www.docuflow.co.za', 'mobile.12306.cn']

INTERNAL_IPS = ['docuflow.co.za', 'www.docuflow.co.za', 'mobile.12306.cn']

DEBUG = False
DEV = False
IS_TEST = False

SITE_ID = 1

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis:://127.0.0.1:6379',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient'
        },
        'KEY_PREFIX': 'production-df'
    },
    'select2': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

CACHE_TTL = 60 * 15
SELECT2_CACHE_BACKEND = 'select2'
CACHE_PREFIX = 'select2'
SELECT2_CACHE_PREFIX = 'docuflow_select2'

PO_FROM_EMAIL = 'PO-from-our-system@Docuflow.co.za'
DEFAULT_FROM_EMAIL = 'info@docuflow.co.za'
NO_REPLY_EMAIL = 'no-reply@docuflow.co.za'
WEB_ERRORS = 'admire@originate.co.za'
ADMIN_EMAIL = ['admire@originate.co.za', 'admire@gmail.com']

EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"
SENDGRID_API_KEY = "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"

COUNTRY_CODE = 'ZA'

DATA_UPLOAD_MAX_NUMBER_FIELDS = None

SECURE_SSL_HOST = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_HTTPONLY = True

sentry_sdk.init(
    dsn='https://5b64bb305a9d4e01bb37ae9555a79208@o214795.ingest.sentry.io/1353487',
    integrations=[DjangoIntegration()],
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production,
    traces_sample_rate=1.0,

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,

    # By default the SDK will try to use the SENTRY_RELEASE
    # environment variable, or infer a git commit
    # SHA as release, however you may want to set
    # something more human-readable.
    # release="myapp@1.0.0",
)

# https://lincolnloop.com/blog/django-logging-right-way/
# http://thegeorgeous.com/2015/02/27/Logging-into-multiple-files-in-Django.html
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            # exact format is not important, this is the minimum information
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console'
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'docuflow': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
    }
})
