from rest_framework import routers

from docuflow.apps.accounts.api.views import AccountsViewSet, RolesViewSet
from docuflow.apps.company.api.views import BranchViewSet, AccountViewSet
from docuflow.apps.customer.api.views import CustomerViewSet
from docuflow.apps.inventory.api.views import (InventoryViewSet, InventoryReceivedViewSet, InventoryReturnedViewSet,
                                               StockAdjustmentViewSet, RecipeViewSet, ManufacturingViewSet)
from docuflow.apps.invoice.api.views import InvoiceViewSet as PurchaseInvoiceViewSet, PaymentViewSet
from docuflow.apps.journals.api.views import JournalViewSet
from docuflow.apps.purchaseorders.api.views import PurchaseOrderViewSet
from docuflow.apps.sales.api.views import (CashUpViewSet, InvoiceViewSet, ReceiptViewSet, EstimateViewSet,
                                           DeliveryViewSet, PickingSlipViewSet, BackOrderViewSet)
from docuflow.apps.supplier.api.views import SupplierViewSet
from docuflow.apps.system.api.views import CashFlowViewSet, ReportTypeViewSet, PaymentMethodViewSet

router = routers.DefaultRouter()
router.register(r'users', AccountsViewSet, basename='users')
router.register(r'roles', RolesViewSet, basename='roles')
router.register(r'suppliers', SupplierViewSet, basename='suppliers')
router.register(r'company/accounts', AccountViewSet, basename='company_accounts')
router.register(r'customers', CustomerViewSet, basename='customers')
router.register(r'journals', JournalViewSet, basename='journals')
router.register(r'sales/invoices', InvoiceViewSet, basename='sales')
router.register(r'sales/estimates', EstimateViewSet, basename='sales-estimates')
router.register(r'sales/deliveries', DeliveryViewSet, basename='sales-deliveries')
router.register(r'sales/pickingslips', PickingSlipViewSet, basename='sales-pickingslips')
router.register(r'sales/backorders', BackOrderViewSet, basename='sales-backorders')
router.register(r'sales/receipts', ReceiptViewSet, basename='sales-receipts')
router.register(r'sales/cashup', CashUpViewSet, basename='sales-cashup')
router.register(r'purchases/payments', PaymentViewSet, basename='purchases-payments')
router.register(r'inventories/received', InventoryReceivedViewSet, basename='inventories_received')
router.register(r'inventories/returned', InventoryReturnedViewSet, basename='inventories_returned')
router.register(r'inventories/adjustments', StockAdjustmentViewSet, basename='inventories_adjustment')
router.register(r'inventories/recipes', RecipeViewSet, basename='inventories-recipes')
router.register(r'inventories/manufacturing', ManufacturingViewSet, basename='inventories-manufacturing')
router.register(r'inventories', InventoryViewSet, basename='inventories')
router.register(r'purchaseorders', PurchaseOrderViewSet, basename='purchaseorders')
router.register(r'purchases-invoices', PurchaseInvoiceViewSet, basename='purchases-invoices')
router.register(r'branch', BranchViewSet, basename='branches')
router.register(r'system/cashflows', CashFlowViewSet, basename='cashflow')
router.register(r'system/paymentoptions', PaymentMethodViewSet, basename='paymentmethods')
router.register(r'system/reporttypes', ReportTypeViewSet, basename='reporttypes')
