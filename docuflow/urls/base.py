"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path
from django.views.static import serve

from .api import router

admin.autodiscover()


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^', include('docuflow.apps.pages.urls')),
    re_path(r'^', include('docuflow.apps.authentication.urls')),
    re_path(r'^distribution/', include('docuflow.apps.distribution.urls')),
    re_path(r'^customer/', include('docuflow.apps.customer.urls')),
    re_path(r'^provisional/tax-estimate/', include('docuflow.apps.provisionaltax.urls')),
    re_path(r'^purchaseorders/', include('docuflow.apps.purchaseorders.urls')),
    re_path(r'^comment/', include('docuflow.apps.comment.urls')),
    re_path(r'^payment/', include('docuflow.apps.invoice.modules.payment.urls')),
    re_path(r'^payroll/', include('docuflow.apps.payroll.urls')),
    re_path(r'^roles/', include('docuflow.apps.roles.urls')),
    re_path(r'^workflow/', include('docuflow.apps.workflow.urls')),
    re_path(r'^sources/', include('docuflow.apps.sources.urls')),
    re_path(r'^important/documents/', include('docuflow.apps.certificate.urls')),
    re_path(r'^documents/', include('docuflow.apps.documents.urls')),
    re_path(r'^invoices/', include('docuflow.apps.invoice.urls')),
    re_path(r'^inventory/', include('docuflow.apps.inventory.urls')),
    re_path(r'^plans/', include('docuflow.apps.subscription.urls')),
    re_path(r'^system/', include('docuflow.apps.system.urls')),
    re_path(r'^common/', include('docuflow.apps.common.urls')),
    re_path(r'^company/', include('docuflow.apps.company.urls')),
    re_path(r'^users/', include('docuflow.apps.accounts.urls')),
    re_path(r'^suppliers/', include('docuflow.apps.supplier.urls')),
    re_path(r'^accountposting/', include('docuflow.apps.accountposting.urls')),
    re_path(r'^years/', include('docuflow.apps.period.urls')),
    re_path(r'^journals/', include('docuflow.apps.journals.urls')),
    re_path(r'^budgets/', include('docuflow.apps.budget.urls')),
    re_path(r'^reports/', include('docuflow.apps.reports.urls')),
    re_path(r'^intervention/', include('docuflow.apps.intervention.urls')),
    re_path(r'^vat/', include('docuflow.apps.vat.urls')),
    re_path(r'^sales/', include('docuflow.apps.sales.urls')),
    re_path(r'^fixedasset/', include('docuflow.apps.fixedasset.urls')),
    re_path(r'^mfa/', include('docuflow.apps.django_mfa.urls')),
    re_path(r'^select2/', include('django_select2.urls')),
    re_path(r'^api/v1/', include(router.urls)),

]

if settings.DEBUG and settings.MEDIA_ROOT:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)), ] + urlpatterns


_media_url = settings.MEDIA_URL
if _media_url.startswith('/'):
    _media_url = _media_url[1:]
    urlpatterns += [re_path('%s(?P<path>.*)$' % _media_url, serve, {'document_root': settings.MEDIA_ROOT})]
del(_media_url, serve)

admin.site.site_title = 'Docuflow Administration'
admin.site.site_header = 'Docuflow Administration'
