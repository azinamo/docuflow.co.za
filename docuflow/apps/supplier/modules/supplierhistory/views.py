from django.http.response import JsonResponse
from django.views.generic import View, TemplateView, FormView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.period.models import Year
from docuflow.apps.supplier.models import History
from .forms import SupplierSearchForm


class HistoryView(FormView):
    template_name = 'supplierhistory/index.html'
    form_class = SupplierSearchForm


class ViewHistoryView(TemplateView):
    template_name = 'supplierhistory/history.html'


class SupplierHistoryDataView(ProfileMixin, View):

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year-1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get(self, request, *args, **kwargs):
        year = self.get_year()
        previous_year = self.get_previous_year(year=year)

        data = {'year': {'label': '', 'data': []}, 'previous_year': {'label': '', 'data': []}, 'periods': [],
                'current': {'periods': [], 'totals': {'vat': 0, 'net': 0, 'total': 0}}}
        if year:
            data['year']['label'] = str(year)
        if previous_year:
            data['previous_year']['label'] = str(previous_year)

        histories = History.objects.filter(supplier_id=self.request.GET.get('supplier_id'), period__period_year__in=[year, previous_year])
        periods = {}
        for history in histories:
            vat = round(float(history.vat_amount))
            net = round(float(history.net_amount))
            total = round(float(history.total_amount))
            if history.period.period_year == year:
                data['year']['data'].append(total)
                data['current']['periods'].append({
                    'period': str(history.period), 'name': str(history.period.name),
                    'vat': vat, 'net': net,
                    'total': total
                })
                data['current']['totals']['vat'] += vat if vat else 0
                data['current']['totals']['net'] += net if net else 0
                data['current']['totals']['total'] += total if total else 0
            if history.period.period_year == previous_year:
                data['previous_year']['data'].append(total)
            periods[history.period.period] = history.period.name
        data['periods'] = [name for period, name in periods.items()]
        return JsonResponse(data)
