from django.urls import path

from . import views

urlpatterns = [
    path('', views.HistoryView.as_view(), name='supplier_history'),
    path('view', views.ViewHistoryView.as_view(), name='view_supplier_history'),
    path('data/', views.SupplierHistoryDataView.as_view(), name='supplier_history_data'),
]