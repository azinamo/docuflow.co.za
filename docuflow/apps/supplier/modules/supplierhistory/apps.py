from django.apps import AppConfig


class SupplierhistoryConfig(AppConfig):
    name = 'docuflow.apps.supplier.modules.supplierhistory'
