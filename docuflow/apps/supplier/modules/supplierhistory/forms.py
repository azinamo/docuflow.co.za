from django import forms

from django_select2.forms import HeavySelect2Widget


class SupplierSearchForm(forms.Form):
    supplier = forms.ChoiceField(widget=HeavySelect2Widget(data_view='search_suppliers_list', attrs={
        'data-view-url': '/supplier/history/view/'
    }))
