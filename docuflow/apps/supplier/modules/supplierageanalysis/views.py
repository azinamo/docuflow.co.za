import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.timezone import now
from django.views.generic import TemplateView, DetailView
from django.views.generic.edit import FormView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.invoice.models import Payment, Invoice
from docuflow.apps.supplier.models import AgeAnalysis, Ledger
from docuflow.apps.period.models import Period
from .forms import SupplierAgeAnalysisForm


logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class SupplierAgeAnalysisView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'supplierageanalysis/index.html'
    form_class = SupplierAgeAnalysisForm

    def get_initial(self):
        initial = super().get_initial()
        initial['to_date'] = now().date()
        initial['period'] = Period.objects.open(company=self.get_company(), year=self.get_year()).first()
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs


class GenerateAgeAnalysisView(ProfileMixin, TemplateView):
    template_name = 'supplierageanalysis/show.html'

    # noinspection PyMethodMayBeStatic
    def get_current_period(self, company, date):
        return Period.objects.filter(
            from_date__lte=date, to_date__gte=date, company=company
        ).order_by('-period').first()

    def get_template_names(self):
        return 'supplierageanalysis/history.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateAgeAnalysisView, self).get_context_data(**kwargs)

        form_class = SupplierAgeAnalysisForm(company=self.get_company(), year=self.get_year(), data=self.request.GET)
        if form_class.is_valid():
            report, totals = form_class.execute()
            context['report'] = report
            context['totals'] = totals
            context['company'] = self.get_company()
            context['year'] = self.get_year()
            context['report_date'] = form_class.cleaned_data['period'].to_date
        return context


class SupplierAgeAnalysisDetailView(DetailView):
    model = AgeAnalysis
    template_name = 'supplierageanalysis/detail.html'
    context_object_name = 'age_analysis'

    # noinspection PyMethodMayBeStatic
    def get_open_invoices(self, age_analysis: AgeAnalysis):
        return Invoice.objects.select_related(
            'status', 'invoice_type', 'company'
        ).prefetch_related('invoice_references').open(supplier=age_analysis.supplier).invoices().filter(
            period__period_year=age_analysis.period.period_year
        )

    # noinspection PyMethodMayBeStatic
    def get_open_payments(self, age_analysis: AgeAnalysis):
        return Payment.objects.open(supplier=age_analysis.supplier)

    # noinspection PyMethodMayBeStatic
    def get_open_ledgers(self, age_analysis: AgeAnalysis):
        return Ledger.objects.with_available_payments(
            company=age_analysis.supplier.company, supplier_id=age_analysis.supplier_id
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        age_analysis = self.get_object()
        invoices = self.get_open_invoices(age_analysis=age_analysis)
        payments = self.get_open_payments(age_analysis=age_analysis)
        ledgers = self.get_open_ledgers(age_analysis=age_analysis)
        context['invoices'] = invoices
        context['payments'] = payments
        context['ledgers'] = ledgers
        context['total'] = sum([sum(invoice.amount for invoice in invoices if invoice.amount),
                                sum(payment.amount for payment in payments if payment.amount),
                                sum(ledger.balance for ledger in ledgers if ledger.balance)
                                ])
        context['age_analysis'] = age_analysis
        return context
