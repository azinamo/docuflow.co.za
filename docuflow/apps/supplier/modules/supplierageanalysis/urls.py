from django.urls import path

from . import views

urlpatterns = [
    path('', views.SupplierAgeAnalysisView.as_view(), name='supplier_age_analysis'),
    path('generate/', views.GenerateAgeAnalysisView.as_view(), name='generate_supplier_age_analysis'),
    path('<int:pk>/detail/', views.SupplierAgeAnalysisDetailView.as_view(), name='supplier_age_analysis_detail'),
]
