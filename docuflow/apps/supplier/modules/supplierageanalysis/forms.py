import pprint

from django import forms
from django.db.models import Sum
from django.utils.timezone import now

from docuflow.apps.period.models import Period
from docuflow.apps.invoice.models import InvoicePayment
from docuflow.apps.supplier.models import Supplier

pp = pprint.PrettyPrinter(indent=4)


class SupplierAgeAnalysisForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super(SupplierAgeAnalysisForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].queryset = Supplier.objects.filter(company=self.company)
        self.fields['period'].queryset = Period.objects.filter(period_year=self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['period'].initial = self.get_current_period(company=self.company, date=now().date()).pk

    period = forms.ModelChoiceField(required=False, label='Period', queryset=None, widget=forms.Select(attrs={'class': 'chosen-select'}))
    supplier = forms.ModelChoiceField(required=False, label='Supplier', queryset=None, widget=forms.Select(attrs={'class': 'chosen-select'}))

    # noinspection PyMethodMayBeStatic
    def get_current_period(self, company, date):
        return Period.objects.filter(
            from_date__lte=date, to_date__gte=date, company=company
        ).order_by(
            '-period'
        ).first()

    def get_supplier_amount(self, supplier, key):
        if key in supplier:
            return supplier[key] if supplier[key] else 0
        return 0

    def slice_period(self, periods, start, offset=None):
        try:
            period_page = periods[start:offset]
            period = period_page[0]
            if not period:
                return None
        except IndexError:
            return None
        return period

    def get_invoice_payments(self, to_date):
        invoice_payments = InvoicePayment.objects.values(
            'invoice_id'
        ).annotate(
            total_paid=Sum('allocated')
        ).filter(
            payment__company=self.company,
            payment__payment_date__lte=to_date
        ).values('invoice_id', 'total_paid')

        return {payment['invoice_id']: payment['total_paid'] for payment in invoice_payments}

    def execute(self):
        period = self.cleaned_data['period']

        totals = {
            'payment_total': 0,
            'one_twenty_or_more_days': 0,
            'ninety_days': 0,
            'sixty_days': 0,
            'thirty_days': 0,
            'current': 0,
            'total': 0
        }
        supplier_age_balance = Supplier.objects.get_age_analysis(company=self.company, year=self.year, period=period,
                                                                 supplier=self.cleaned_data['supplier'])
        for supplier in supplier_age_balance:
            totals['payment_total'] += 0
            totals['one_twenty_or_more_days'] += supplier.one_twenty_day if supplier.one_twenty_day else 0
            totals['ninety_days'] += supplier.ninety_day if supplier.ninety_day else 0
            totals['sixty_days'] += supplier.sixty_day if supplier.sixty_day else 0
            totals['thirty_days'] += supplier.thirty_day if supplier.thirty_day else 0
            totals['current'] += supplier.current if supplier.current else 0
            totals['total'] += supplier.balance if supplier.balance else 0
        return supplier_age_balance, totals
