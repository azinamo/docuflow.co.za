from django.apps import AppConfig


class SupplierageanalysisConfig(AppConfig):
    name = 'docuflow.apps.supplier.modules.supplierageanalysis'
