from django.apps import AppConfig


class SupplierroleauthorizationConfig(AppConfig):
    name = 'docuflow.apps.supplier.modules.supplierroleauthorization'
