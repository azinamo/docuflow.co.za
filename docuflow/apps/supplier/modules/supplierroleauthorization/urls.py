from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('<int:authorization_id>/', views.IndexView.as_view(), name='supplier_role_authorization_index'),
    path('<int:authorization_id>/create/', csrf_exempt(views.CreateSupplierRoleAuthorizationView.as_view()),
         name='create_supplier_role_authorization'),
    path('<int:pk>/edit/', csrf_exempt(views.EditSupplierRoleAuthorizationView.as_view()),
         name='edit_supplier_role_authorization'),
    path('<int:pk>/delete/', views.DeleteSupplierRoleAuthorizationView.as_view(),
         name='delete_supplier_role_authorization'),
]
