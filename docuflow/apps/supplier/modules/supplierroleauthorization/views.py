import logging
import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.supplier.models import SupplierAuthorization, RoleAuthorization
from .forms import SupplierRoleAuthorizationForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'supplierroleauthorization/index.html'
    context_object_name = 'supplier_authorizations'

    def get_queryset(self):
        authorizations = RoleAuthorization.objects.select_related(
            'supplier'
        ).filter(
            supplier__company=self.get_company()
        ).order_by(
            'supplier__name',
            'created_at'
        )
        return authorizations

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context


class CreateSupplierRoleAuthorizationView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = RoleAuthorization
    template_name = 'supplierroleauthorization/create.html'
    success_message = 'Supplier successfully saved'

    def get_context_data(self, **kwargs):
        context = super(CreateSupplierRoleAuthorizationView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['supplier_authorization'] = self.get_supplier_authorization()
        return context

    def get_supplier_authorization(self):
        return SupplierAuthorization.objects.filter(pk=self.kwargs['authorization_id']).first()

    def get_success_url(self):
        return reverse('supplier-list')

    def get_form_kwargs(self):
        kwargs = super(CreateSupplierRoleAuthorizationView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_form_class(self):
        return SupplierRoleAuthorizationForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the supplier authorization, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateSupplierRoleAuthorizationView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                supplier_authorization = self.get_supplier_authorization()

                supplier_role_authorization = form.save(commit=False)
                supplier_role_authorization.supplier_authorization = supplier_authorization
                supplier_role_authorization.save()

                return JsonResponse({'error': False,
                                     'text': 'Supplier role authorization saved successfully',
                                     'reload': True
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occured, please try again',
                                     'details': 'Error saving the supplier role authorization {}'.format(exception.__str__()),
                                     'alert': True
                                     })
        return HttpResponse('Not allowed')


class EditSupplierRoleAuthorizationView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = RoleAuthorization
    template_name = 'supplierroleauthorization/edit.html'
    success_message = 'Supplier successfully saved'

    def get_context_data(self, **kwargs):
        context = super(EditSupplierRoleAuthorizationView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditSupplierRoleAuthorizationView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_form_class(self):
        return SupplierRoleAuthorizationForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the supplier role authorization, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditSupplierRoleAuthorizationView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                supplier_authorization = form.save(commit=False)
                supplier_authorization.save()

                return JsonResponse({'error': False,
                                     'text': 'Supplier role authorization saved successfully',
                                     'reload': True
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occured, please try again',
                                     'details': 'Error saving the supplier role authorization {}'.format(exception.__str__())
                                     })
        return HttpResponse('Not allowed')


class DeleteSupplierRoleAuthorizationView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, DeleteView):
    model = RoleAuthorization
    success_message = 'Supplier role authorization successfully saved'
    # success_url = reverse('supplier_role_authorization_index')
