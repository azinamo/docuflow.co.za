from django import forms
from django.db.models import Q
from django.contrib.auth.models import User

from docuflow.apps.supplier.models import RoleAuthorization


class SupplierRoleAuthorizationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(SupplierRoleAuthorizationForm, self).__init__(*args, **kwargs)
        self.fields['role'].queryset = self.fields['role'].queryset.filter(company=company)

    class Meta:
        model = RoleAuthorization
        fields = ('role', 'amount')
        widgets = {
            'role': forms.Select(attrs={'class': 'chosen-select'})
        }
