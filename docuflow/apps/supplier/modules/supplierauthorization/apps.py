from django.apps import AppConfig


class SupplierauthorizationConfig(AppConfig):
    name = 'docuflow.apps.supplier.modules.supplierauthorization'
