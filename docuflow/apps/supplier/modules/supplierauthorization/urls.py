from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='supplier_authorization_index'),
    path('create/', csrf_exempt(views.CreateSupplierAuthorizationView.as_view()), name='create_supplier_authorization'),
    path('<int:pk>/edit/', csrf_exempt(views.EditSupplierAuthorizationView.as_view()), name='edit_supplier_authorization'),
    path('<int:pk>/delete/', views.DeleteSupplierAuthorizationView.as_view(), name='delete_supplier_authorization')
]
