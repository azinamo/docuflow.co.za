from django import forms
from django.db.models import Q
from django.contrib.auth.models import User

from docuflow.apps.supplier.models import SupplierAuthorization


class SupplierAuthorizationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(SupplierAuthorizationForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].queryset = self.fields['supplier'].queryset.filter(company=company)

    class Meta:
        model = SupplierAuthorization
        fields = ('supplier', 'amount')
        widgets = {
            'template': forms.Select(attrs={'class': 'chosen-select'}),
            'default_account': forms.Select(attrs={'class': 'chosen-select'})
        }
