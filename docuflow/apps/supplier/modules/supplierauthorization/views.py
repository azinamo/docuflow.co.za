import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.supplier.models import SupplierAuthorization
from .forms import SupplierAuthorizationForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'supplierauthorization/index.html'
    context_object_name = 'supplier_authorizations'

    def get_queryset(self):
        authorizations = SupplierAuthorization.objects.select_related(
            'supplier'
        ).prefetch_related(
            'roles'
        ).filter(
            supplier__company=self.get_company()
        ).order_by(
            'supplier__name'
        )
        return authorizations

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context


class CreateSupplierAuthorizationView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = SupplierAuthorization
    template_name = 'supplierauthorization/create.html'
    success_message = 'Supplier authorization successfully saved'

    def get_context_data(self, **kwargs):
        context = super(CreateSupplierAuthorizationView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        return reverse('supplier_authorization_index')

    def get_form_kwargs(self):
        kwargs = super(CreateSupplierAuthorizationView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_form_class(self):
        return SupplierAuthorizationForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the supplier authorization, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateSupplierAuthorizationView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                supplier_authorization = form.save(commit=False)
                supplier_authorization.save()

                return JsonResponse({'error': False,
                                     'text': 'Supplier authorization saved successfully'
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': 'Error saving the supplier authorization {}'.format(exception.__str__())
                                     })
        return HttpResponse('Not allowed')


class EditSupplierAuthorizationView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = SupplierAuthorization
    template_name = 'supplierauthorization/edit.html'
    success_message = 'Supplier authorization successfully saved'

    def get_context_data(self, **kwargs):
        context = super(EditSupplierAuthorizationView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['supplier_authorization'] = self.get_supplier_authorization()
        return context

    def get_success_url(self):
        return reverse('supplier_authorization_index')

    def get_form_kwargs(self):
        kwargs = super(EditSupplierAuthorizationView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_supplier_authorization(self):
        return SupplierAuthorization.objects.filter(
            id=self.kwargs['pk']
        ).first()

    # def get_initial(self, **kwargs):
    #     initial = super(EditSupplierAuthorizationView, self).get_initial(**kwargs)
    #     supplier_authorization = self.get_supplier_authorization()
    #     initial['amount'] = supplier_authorization.amount
    #     return initial

    def get_form_class(self):
        return SupplierAuthorizationForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the supplier authorization, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditSupplierAuthorizationView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                supplier_authorization = form.save(commit=False)
                logger.info(supplier_authorization)
                logger.info(form.cleaned_data)
                supplier_authorization.save()

                return JsonResponse({'error': False,
                                     'text': 'Supplier authorization updated successfully',
                                     'reload': True
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': 'Error saving the supplier authorization {}'.format(exception.__str__())
                                     })
        return HttpResponse('Not allowed')


class DeleteSupplierAuthorizationView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, DeleteView):
    model = SupplierAuthorization
    success_message = 'Supplier successfully saved'
