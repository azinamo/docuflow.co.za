import logging

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.common.enums import Module
from docuflow.apps.period.models import Period
from docuflow.apps.supplier.models import Ledger

logger = logging.getLogger(__name__)


class CreateLedger(object):

    # noinspection PyMethodMayBeStatic
    def get_debit(self, amount):
        if amount > 0:
            return amount
        return 0

    # noinspection PyMethodMayBeStatic
    def get_credit(self, amount):
        if amount < 0:
            return amount * -1
        return 0


class CreatePaymentLedger(CreateLedger):

    def __init__(self, payment, status=Ledger.PENDING, ledger=None, date=None):
        self.payment = payment
        self.status = status
        self.ledger = ledger
        self.date = date

    def execute(self):
        content_type = self.get_content_type()
        supplier_ledger = self.get_ledger(content_type)
        if supplier_ledger:
            self.update_ledger(supplier_ledger)
        else:
            self.create(content_type)

    # noinspection PyMethodMayBeStatic
    def get_content_type(self):
        return ContentType.objects.get_for_model(self.payment)

    def get_ledger(self, content_type):
        return Ledger.objects.filter(
            supplier=self.payment.supplier, object_id=self.payment.id, content_type=content_type
        ).first()

    def create(self, content_type):
        amount = self.payment.total_amount
        debit = self.get_debit(amount)
        credit = self.get_credit(amount)

        created_by = self.payment.profile
        period = self.payment.period
        if not created_by:
            created_by = self.payment.company.profiles.first()
        if not period:
            period = Period.objects.get_by_date(self.payment.company, self.payment.created_at).first()

        ledger = Ledger.objects.create(
            supplier=self.payment.supplier,
            content_type=content_type,
            object_id=self.payment.id,
            credit=credit,
            debit=debit,
            created_by=created_by,
            period=period,
            date=self.payment.payment_date,
            module=Module.PAYMENT,
            text=str(self.payment),
            status=self.status
        )
        return ledger

    def update_ledger(self, supplier_ledger):
        supplier_ledger.status = self.status
        if self.ledger:
            supplier_ledger.journal = self.ledger
        supplier_ledger.save()


class CreateInvoiceLedger(CreateLedger):

    def __init__(self, invoice, status=Ledger.PENDING, ledger=None):
        self.invoice = invoice
        self.status = status
        self.ledger = ledger

    def execute(self):
        content_type = self.get_content_type()
        ledger = self.get_ledger(content_type=content_type)
        if ledger:
            self.update_ledger(ledger=ledger)
        else:
            self.create(content_type=content_type)

    def get_content_type(self):
        return ContentType.objects.get_for_model(self.invoice)

    def get_ledger(self, content_type):
        return Ledger.objects.filter(
                supplier=self.invoice.supplier,
                object_id=self.invoice.id,
                content_type=content_type
            ).first()

    def create(self, content_type):
        invoice_total = self.invoice.open_amount
        if self.invoice.is_credit_type and invoice_total > 0:
            invoice_total = invoice_total * -1

        amount = invoice_total * -1
        debit = self.get_debit(amount)
        credit = self.get_credit(amount)

        created_by = self.invoice.created_by
        period = self.invoice.period
        if not created_by:
            created_by = self.invoice.company.profiles.first()
        if not period:
            period = Period.objects.get_by_date(self.invoice.company, self.invoice.created_at)

        ledger = Ledger.objects.create(
            supplier=self.invoice.supplier,
            content_type=content_type,
            object_id=self.invoice.id,
            credit=credit,
            debit=debit,
            created_by=created_by,
            period=period,
            date=self.invoice.accounting_date,
            module=Module.PURCHASE,
            status=self.status,
            text=str(self.invoice)
        )
        return ledger

    def update_ledger(self, ledger):
        ledger.status = self.status
        ledger.journal = self.ledger
        ledger.save()
