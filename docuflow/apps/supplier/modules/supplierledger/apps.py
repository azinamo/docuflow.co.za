from django.apps import AppConfig


class SupplierledgerConfig(AppConfig):
    name = 'docuflow.apps.supplier.modules.supplierledger'
