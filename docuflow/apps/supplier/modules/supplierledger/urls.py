from django.urls import path

from . import views

urlpatterns = [
    path('', views.LedgerView.as_view(), name='supplier_ledger'),
    path('generate/', views.GenerateLedgerView.as_view(), name='generate_supplier_ledger')
]


