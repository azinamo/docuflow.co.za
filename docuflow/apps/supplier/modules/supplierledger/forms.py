from django import forms

from docuflow.apps.supplier.models import Ledger, Supplier, LedgerPayment


class SupplierLedgerForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super().__init__(*args, **kwargs)

        self.fields['from_supplier'].queryset = Supplier.objects.filter(company=self.company).order_by('code')
        self.fields['to_supplier'].queryset = Supplier.objects.filter(company=self.company).order_by('-code')

    from_date = forms.DateField()
    to_date = forms.DateField()
    from_supplier = forms.ModelChoiceField(queryset=None, label='Supplier', empty_label=None, to_field_name='code')
    to_supplier = forms.ModelChoiceField(queryset=None, label='Supplier', empty_label=None, to_field_name='code')


class LedgerPaymentForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(LedgerPaymentForm, self).__init__(*args, **kwargs)
        self.fields['ledger'].queryset = self.fields['ledger'].queryset.filter(supplier__company=self.company)
        self.fields['payment'].queryset = self.fields['payment'].queryset.filter(supplier__company=self.company)
    
    class Meta:
        model = LedgerPayment
        fields = ('ledger', 'payment', 'amount')

    def save(self, commit=True):
        ledger_payment = super(LedgerPaymentForm, self).save(commit=False)
        ledger_payment.save()

        supplier_ledger = Ledger.objects.get(id=ledger_payment.ledger_id)
        supplier_ledger.balance = supplier_ledger.balance - ledger_payment.amount
        supplier_ledger.save()

        return ledger_payment
