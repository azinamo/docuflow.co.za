import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import FormView, TemplateView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.supplier.models import Ledger, OpeningBalance
from .forms import SupplierLedgerForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class LedgerView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'supplierledger/index.html'
    form_class = SupplierLedgerForm

    def get_form_kwargs(self, **kwargs):
        kwargs = super(LedgerView, self).get_form_kwargs()
        company = self.get_company()
        kwargs['company'] = company
        return kwargs

    def get_initial(self):
        initial = super(LedgerView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        return initial

    def get_context_data(self, **kwargs):
        context = super(LedgerView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context


class GenerateLedgerView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'supplierledger/ledger.html'

    def get_opening_balances(self, company, year, cleaned_data):
        year_opening_balances = OpeningBalance.objects.filter(**self.get_opening_balance_filters(company, year, cleaned_data))
        opening_balances = {}
        for opening_balance in year_opening_balances:
            opening_balances[opening_balance.supplier] = {'lines': [],
                                                          'total_value': opening_balance.amount,
                                                          'balance': opening_balance.amount,
                                                          'incoming_balance': opening_balance.amount,
                                                          'initial_balance': opening_balance.amount,
                                                          'initial_debit': 0,
                                                          'initial_credit': 0,
                                                          'debit': 0,
                                                          'credit': 0,
                                                          'movement': 0,
                                                          'movement_debit': 0,
                                                          'movement_credit': 0,
                                                          }
        return opening_balances

    def get_opening_balance_filters(self, company, year, cleaned_data):
        lookup_expression = {'supplier__company': company, 'year': year}
        from_supplier = cleaned_data.get('from_supplier', None)
        to_supplier = cleaned_data.get('to_supplier', None)
        if from_supplier and to_supplier:
            if from_supplier == to_supplier:
                lookup_expression['supplier__code__iexact'] = from_supplier.code
            else:
                if from_supplier:
                    lookup_expression['supplier__code__gte'] = from_supplier.code
                if to_supplier:
                    lookup_expression['supplier__code__lte'] = to_supplier.code
        else:
            if from_supplier:
                lookup_expression['supplier__code__gte'] = from_supplier.code
            if to_supplier:
                lookup_expression['supplier__code__lte'] = to_supplier.code
        return lookup_expression

    def get_filter_options(self, company, year, cleaned_data):
        print(cleaned_data)
        lookup_expression = {'supplier__company': company}
        from_supplier = cleaned_data.get('from_supplier', None)
        to_supplier = cleaned_data.get('to_supplier', None)
        to_date = cleaned_data.get('to_date', None)
        if to_date:
            lookup_expression['date__lte'] = to_date
        if from_supplier and to_supplier:
            if from_supplier == to_supplier:
                lookup_expression['supplier__code__iexact'] = from_supplier.code
            else:
                if from_supplier:
                    lookup_expression['supplier__code__gte'] = from_supplier.code
                if to_supplier:
                    lookup_expression['supplier__code__lte'] = to_supplier.code
        else:
            if from_supplier:
                lookup_expression['supplier__code__gte'] = from_supplier.code
            if to_supplier:
                lookup_expression['supplier__code__lte'] = to_supplier.code
        # lookup_expression['date__lte'] = year.end_date
        lookup_expression['date__gte'] = year.start_date
        return lookup_expression

    def include_ledger_line(self, ledger, start_date):
        if not start_date:
            return True
        if ledger.date >= start_date:
            return True
        return False

    def get_supplier_ledger(self, company, year, cleaned_data):
        start_date = cleaned_data['from_date']
        filter_options = self.get_filter_options(company, year, cleaned_data)
        supplier_ledgers = self.get_opening_balances(company, year, cleaned_data)
        for ledger in Ledger.objects.get_ledgers(filter_options):
            line_amount = 0
            if ledger.amount:
                line_amount = ledger.amount
            debit_amount = line_amount if line_amount >= 0 else 0
            credit_amount = line_amount * -1 if line_amount < 0 else 0

            ledger_data = {'line': ledger, 'value': 0, 'initial_balance': 0, 'balance': 0,
                           'quantity_balance': 0, 'movement': 0, 'incoming_balance': 0,
                           'incoming_value': 0, 'credit_amount': credit_amount,
                           'debit_amount': debit_amount}

            if self.include_ledger_line(ledger, start_date):
                if ledger.supplier in supplier_ledgers:
                    balance = supplier_ledgers[ledger.supplier]['balance']
                    balance = ledger.amount + balance
                    ledger_data['value'] = ledger.amount
                    ledger_data['debit'] = ledger.debit
                    ledger_data['credit'] = ledger.credit
                    ledger_data['balance'] = balance
                    supplier_ledgers[ledger.supplier]['lines'].append(ledger_data)
                    supplier_ledgers[ledger.supplier]['total_value'] += ledger.amount
                    supplier_ledgers[ledger.supplier]['balance'] = balance
                    supplier_ledgers[ledger.supplier]['movement'] += line_amount
                    supplier_ledgers[ledger.supplier]['movement_debit'] += debit_amount
                    supplier_ledgers[ledger.supplier]['movement_credit'] += credit_amount
                    supplier_ledgers[ledger.supplier]['debit'] += debit_amount
                    supplier_ledgers[ledger.supplier]['credit'] += credit_amount
                else:
                    ledger_data['balance'] = ledger.amount
                    ledger_data['value'] = ledger.amount
                    ledger_data['debit'] = ledger.debit
                    ledger_data['credit'] = ledger.credit
                    supplier_ledgers[ledger.supplier] = {'lines': [ledger_data],
                                                         'total_value': ledger.amount,
                                                         'balance': ledger.amount,
                                                         'movement': line_amount,
                                                         'movement_debit': debit_amount,
                                                         'movement_credit': credit_amount,
                                                         'debit': debit_amount,
                                                         'credit': credit_amount
                                                         }
            else:
                if ledger.supplier in supplier_ledgers:
                    supplier_ledgers[ledger.supplier]['total_value'] += line_amount
                    supplier_ledgers[ledger.supplier]['balance'] += line_amount
                    supplier_ledgers[ledger.supplier]['initial_debit'] += debit_amount
                    supplier_ledgers[ledger.supplier]['initial_credit'] += credit_amount
                    supplier_ledgers[ledger.supplier]['initial_balance'] += line_amount
                    supplier_ledgers[ledger.supplier]['debit'] += debit_amount
                    supplier_ledgers[ledger.supplier]['credit'] += credit_amount
                else:
                    ledger_data['initial_balance'] = line_amount
                    ledger_data['incoming_value'] = line_amount
                    supplier_ledgers[ledger.supplier] = {'lines': [],
                                                         'total_value': line_amount,
                                                         'balance': line_amount,
                                                         'initial_debit': debit_amount,
                                                         'initial_credit': credit_amount,
                                                         'initial_balance': line_amount,
                                                         'debit': debit_amount,
                                                         'credit': credit_amount,
                                                         'movement': 0,
                                                         'movement_debit': 0,
                                                         'movement_credit': 0,
                                                         }
        return supplier_ledgers

    def get_context_data(self, **kwargs):
        context = super(GenerateLedgerView, self).get_context_data(**kwargs)
        company = self.get_company()
        form = SupplierLedgerForm(data=self.request.GET, company=company)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            year = self.get_year()
            context['year'] = year
            context['supplier_ledgers'] = self.get_supplier_ledger(company, year, form.cleaned_data)
            context['start_date'] = cleaned_data.get('from_date')
            context['end_date'] = cleaned_data.get('to_date')
            context['company'] = company
        return context
