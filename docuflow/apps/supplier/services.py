import logging
from decimal import Decimal

from django.db.models import Sum

from docuflow.apps.common.enums import DayAge
from docuflow.apps.company.enums import SubLedgerModule
from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Invoice, Payment, InvoicePayment, ChildPayment
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.journals.services import get_total_for_sub_ledger
from docuflow.apps.period.models import Period, Year
from .models import Ledger, AgeAnalysis, Supplier, LedgerPayment

logger = logging.getLogger(__name__)


class CreateSupplierLedger:

    def __init__(self, supplier, amount, content_type, object_id, profile, period, date, module, text=None,
                 status=Ledger.COMPLETED):
        logger.info("-----------------Create supplier ledger------------------------")
        self.supplier = supplier
        self.amount = amount
        self.content_type = content_type
        self.object_id = object_id
        self.status = status
        self.profile = profile
        self.period = period
        self.module = module
        self.date = date
        self.text = text

    def get_line_amount(self):
        logger.info("Get line amount for supplier ledger")
        debit = credit = 0
        if self.amount > 0:
            debit = self.amount
        else:
            credit = self.amount
        return debit, credit

    def execute(self):
        logger.info("Execute")
        ledger = None
        if self.content_type:
            debit, credit = self.get_line_amount()

            ledger = self.get_existing_ledger_line()
            if not ledger:
                ledger = self.create(credit, debit)
            else:
                ledger = self.update(ledger)
        return ledger

    def get_existing_ledger_line(self):
        logger.info("Check if ledger already exist")
        ledger = Ledger.objects.filter(
            supplier=self.supplier,
            object_id=self.object_id,
            content_type=self.content_type
        ).first()
        logger.info("Result --< {}".format(ledger))
        return ledger

    def update(self, ledger):
        logger.info("Update existing ledger {} to statys {}".format(ledger, self.status))
        ledger.status = self.status
        # ledger.journal = self.journal
        ledger.save()
        logger.info("Done updating ledger {} to statys {}".format(ledger, self.status))
        return ledger

    def create(self, credit, debit):
        logger.info("Create new ledger Cr({}) and Dr({})".format(credit, debit))
        ledger = Ledger.objects.create(
            supplier=self.supplier,
            content_type=self.content_type,
            object_id=self.object_id,
            credit=credit,
            debit=debit,
            created_by=self.profile,
            period=self.period,
            date=self.date,
            module=self.module,
            text=self.text,
            status=self.status
        )
        logger.info(f"Done creating ledger {ledger} to status {ledger.status}")
        return ledger


def add_invoice_to_age_analysis(invoice: Invoice):
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        supplier=invoice.supplier,
        period=invoice.period
    )
    amount = Decimal(invoice.total_amount).quantize(Decimal('0.01'))

    logger.info(f" age analysis {age_analysis} on supplier({age_analysis.supplier_id}) "
                f"in period {invoice.period_id} is amount = {amount}")
    age_analysis.current += amount if age_analysis.current else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    age_amount_from_period(age_analysis=age_analysis, period=age_analysis.period, amount=amount)


def add_to_supplier_age_analysis(supplier: Supplier, period: Period, amount):
    age_analysis, _ = AgeAnalysis.objects.get_or_create(
        supplier=supplier,
        period=period
    )
    age_analysis.current += amount if age_analysis.current else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    age_amount_from_period(age_analysis=age_analysis, period=age_analysis.period, amount=amount)


def age_past_invoice(payment: Payment, year: Year, amount: Decimal, start=0):
    periods = Period.objects.filter(period_year=year, period__gte=payment.period.period).order_by('period')
    ageing_invoice_payment_for_periods(periods=periods, start=start, supplier=payment.supplier, amount=amount)


def add_invoice_payment_to_age_analysis(invoice_payment: InvoicePayment, is_reverse=False):
    supplier = invoice_payment.payment.supplier
    period = invoice_payment.payment.period

    amount = invoice_payment.amount * -1
    if is_reverse:
        amount = amount * -1

    amount = Decimal(amount).quantize(Decimal('.01'))
    invoice_period = invoice_payment.invoice.period
    payment_year = invoice_payment.payment.period.period_year
    invoice_year = invoice_period.period_year
    if payment_year == invoice_year:
        if invoice_period.period < period.period:
            start = period.period - invoice_period.period
            age_past_invoice(payment=invoice_payment.payment, year=payment_year, amount=amount, start=start)
            age_upcoming_years(payment=invoice_payment.payment, year=payment_year, amount=amount,
                               invoice_year=invoice_year, invoice_period=invoice_period)
        else:
            age_analysis = add_to_current_age(supplier=supplier, period=period, amount=amount)
            age_for_next_periods(age_analysis=age_analysis, period=age_analysis.period, amount=amount)
    elif invoice_year.year < payment_year.year:
        start = (invoice_year.number_of_periods - invoice_period.period) + 1
        age_past_invoice(payment=invoice_payment.payment, year=payment_year, amount=amount, start=start)

        age_upcoming_years(payment=invoice_payment.payment, year=payment_year, amount=amount,
                           invoice_year=invoice_year, invoice_period=invoice_period)


def add_journal_payment_to_age_analysis(ledger_payment: LedgerPayment, is_reverse=False):
    supplier = ledger_payment.payment.supplier
    period = ledger_payment.payment.period

    amount = ledger_payment.amount * -1  # Journal payment is saved as a positive amount, but when we
    # doing payments we are removing it, so multiply by -1
    if is_reverse:
        amount = amount * -1
    amount = Decimal(amount)
    ledger_period = ledger_payment.ledger.period
    payment_year = ledger_payment.payment.period.period_year
    ledger_year = ledger_payment.ledger.period.period_year
    if payment_year == ledger_year:
        if ledger_period.period < period.period:
            start = period.period - ledger_period.period
            age_past_invoice(payment=ledger_payment.payment, year=payment_year, amount=amount, start=start)
            age_upcoming_years(payment=ledger_payment.payment, year=payment_year, amount=amount,
                               invoice_year=ledger_year,
                               invoice_period=ledger_period)
        else:
            age_analysis = add_to_current_age(supplier=supplier, period=period, amount=amount)
            age_for_next_periods(age_analysis=age_analysis, period=age_analysis.period, amount=amount)
    elif ledger_year.year < payment_year.year:
        start = (ledger_year.number_of_periods - ledger_period.period) + 1
        age_past_invoice(payment=ledger_payment.payment, year=payment_year, amount=amount, start=start)

        age_upcoming_years(payment=ledger_payment.payment, year=payment_year, amount=amount, invoice_year=ledger_year,
                           invoice_period=ledger_period)


def clear_unallocated_on_age_analysis(child_payment, is_reverse=False):
    supplier = child_payment.payment.supplier
    period = child_payment.parent.period

    ages = AgeAnalysis.objects.filter(
        period__period__gte=period.period, period__period_year=period.period_year, supplier=supplier
    )
    for age in ages:
        age.unallocated += child_payment.amount
        age.balance += child_payment.amount
        age.save()


def age_upcoming_years(payment: Payment, year: Year, invoice_year: Year, invoice_period: Period, amount: Decimal):
    years = Year.objects.agable().filter(year__gt=year.year, company=year.company).order_by('-year')
    for c, year in enumerate(years):
        # logger.info(f"{c} -> Ageing year {year}")
        start = 3
        if c == 0:
            start = invoice_year.number_of_periods - invoice_period.period
        periods = Period.objects.filter(period_year=year).order_by('period')
        ageing_invoice_payment_for_periods(periods=periods, start=start, supplier=payment.supplier, amount=amount)


def ageing_invoice_payment_for_periods(periods, start, amount, supplier):
    for counter, period in enumerate(periods, start=1):
        if start == 0:
            # Age in current period
            age_for_period(period=period, amount=amount, supplier=supplier, period_days=DayAge.CURRENT)
        elif start == 1:
            # Age in 30
            age_for_period(period=period, amount=amount, supplier=supplier, period_days=DayAge.THIRTY_DAY)
        elif start == 2:
            # Age in 60
            age_for_period(period=period, amount=amount, supplier=supplier, period_days=DayAge.SIXTY_DAY)
        elif start == 3:
            # Age in 90
            age_for_period(period=period, amount=amount, supplier=supplier, period_days=DayAge.NINETY_DAY)
        elif start >= 4:
            # Age in 120
            age_for_period(period=period, amount=amount, supplier=supplier, period_days=DayAge.ONE_TWENTY_DAY)
        start += 1


def age_for_period(supplier: Supplier, period: Period, amount: Decimal, period_days: DayAge):
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        supplier=supplier,
        period=period
    )
    if period_days == DayAge.CURRENT:  # Current
        age_analysis.current += amount if age_analysis.current else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.THIRTY_DAY:
        age_analysis.thirty_day += amount if age_analysis.thirty_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.SIXTY_DAY:
        age_analysis.sixty_day += amount if age_analysis.sixty_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.NINETY_DAY:
        age_analysis.ninety_day += amount if age_analysis.ninety_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.ONE_TWENTY_DAY:
        age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    return age_analysis


def add_to_current_age(supplier: Supplier, period: Period, amount):
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        supplier=supplier,
        period=period
    )
    age_analysis.current += amount if age_analysis.current else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    return age_analysis


def age_for_next_periods(age_analysis: AgeAnalysis, period: Period, amount):
    periods = Period.objects.filter(period__gt=period.period, period_year=period.period_year).order_by('period')
    period_count = periods.count()

    age_over_periods(age_analysis=age_analysis, periods=periods, amount=amount)

    age_for_next_years(age_analysis=age_analysis, previous_year=period.period_year, amount=amount,
                       period_count=period_count)


def age_for_next_years(age_analysis, previous_year, amount, period_count):
    for counter, year in enumerate(
            Year.objects.agable().filter(year__gt=previous_year.year, company=previous_year.company).order_by('-year')):
        periods = Period.objects.filter(period_year=year).order_by('period')
        # logger.info(f"{year}:: Start at {counter} using age {age_analysis}(Period {age_analysis.period} and customer {age_analysis.supplier.name})")
        # logger.info(periods)
        if counter == 0:
            age_over_periods(age_analysis=age_analysis, periods=periods, amount=amount, start=period_count)
        else:
            age_over_periods(age_analysis=age_analysis, periods=periods, amount=amount, start=0, is_last=True)


def age_over_periods(age_analysis, periods, amount, start=0, is_last=False):
    for counter, _period in enumerate(periods, start=start):
        period_age_analysis, is_new = AgeAnalysis.objects.get_or_create(
            supplier=age_analysis.supplier,
            period=_period
        )
        if is_last:
            period_age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
            period_age_analysis.balance += amount if age_analysis.balance else amount
        else:
            if counter == 0:
                period_age_analysis.thirty_day += amount if age_analysis.thirty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter == 1:
                period_age_analysis.sixty_day += amount if age_analysis.sixty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter == 2:
                period_age_analysis.ninety_day += amount if age_analysis.ninety_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter >= 3:
                period_age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
        period_age_analysis.save()


def add_payment_to_age_analysis(payment: Payment, is_reverse=False):
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        supplier=payment.supplier,
        period=payment.period
    )
    amount = payment.total_amount if is_reverse else payment.total_amount * -1

    age_analysis.unallocated += amount if age_analysis.unallocated else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    age_unallocated(age_analysis=age_analysis, period=payment.period, amount=amount)


def age_unallocated(age_analysis: AgeAnalysis, period: Period, amount: Decimal):
    periods = Period.objects.filter(period__gt=period.period, period_year=period.period_year).order_by('period')

    age_unallocated_over_periods(age_analysis=age_analysis, periods=periods, amount=amount)

    age_year_unallocated(age_analysis=age_analysis, year=period.period_year, amount=amount)


def age_unallocated_over_periods(age_analysis, periods, amount):
    for counter, period in enumerate(periods):
        period_age_analysis, is_new = AgeAnalysis.objects.get_or_create(
            supplier=age_analysis.supplier,
            period=period
        )
        period_age_analysis.unallocated += amount if age_analysis.unallocated else amount
        period_age_analysis.balance += amount if age_analysis.balance else amount
        period_age_analysis.save()


def age_year_unallocated(age_analysis, year, amount):
    for year in Year.objects.agable().filter(year__gt=year.year, company=year.company):
        periods = Period.objects.filter(period_year=year).order_by('period')
        logger.info(periods)

        age_unallocated_over_periods(age_analysis=age_analysis, periods=periods, amount=amount)


def add_journal_to_age_analysis(journal_line: JournalLine, is_reverse=False):
    age_analysis, _ = AgeAnalysis.objects.get_or_create(
        supplier_id=journal_line.object_id,
        period=journal_line.journal.period
    )
    amount = journal_line.amount

    age_analysis.current += amount if age_analysis.current else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    age_amount_from_period(age_analysis=age_analysis, period=age_analysis.period, amount=amount)


def age_amount_from_period(age_analysis: AgeAnalysis, period: Period, amount):
    periods = Period.objects.filter(period__gt=period.period, period_year=period.period_year).order_by('period')

    start = 0

    age_for_periods(age_analysis=age_analysis, periods=periods, amount=amount, start=start)

    age_amount_from_year(age_analysis=age_analysis, previous_year=age_analysis.period.period_year, amount=amount,
                         period_count=periods.count())


def age_amount_from_year(age_analysis: AgeAnalysis, previous_year: Year, amount, period_count=0):
    for counter, year in enumerate(
            Year.objects.agable().filter(year__gt=previous_year.year, company=previous_year.company).order_by('-year')):
        periods = Period.objects.filter(period_year=year).order_by('period')
        if counter == 0:
            age_for_periods(age_analysis=age_analysis, periods=periods, amount=amount, start=period_count)
        else:
            age_for_periods(age_analysis=age_analysis, periods=periods, amount=amount, start=0, is_last=True)


def age_for_periods(age_analysis, periods, amount, start=0, is_last=False):
    for counter, _period in enumerate(periods, start=start):
        period_age_analysis, is_new = AgeAnalysis.objects.get_or_create(
            supplier=age_analysis.supplier,
            period=_period
        )
        if is_last:
            period_age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
            period_age_analysis.balance += amount if age_analysis.balance else amount
        else:
            if counter == 0:
                period_age_analysis.thirty_day += amount if age_analysis.thirty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter == 1:
                period_age_analysis.sixty_day += amount if age_analysis.sixty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter == 2:
                period_age_analysis.ninety_day += amount if age_analysis.ninety_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter >= 3:
                period_age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
        period_age_analysis.save()


def age_supplier_from_year(age_analysis: AgeAnalysis, supplier: Supplier, period: Period):
    next_periods = Period.objects.filter(period__gt=period.period, period_year=period.period_year).order_by('period')
    for counter, next_period in enumerate(next_periods):
        period_age_analysis, is_new = AgeAnalysis.objects.get_or_create(
            supplier=supplier,
            period=next_period
        )
        if counter == 0:
            period_age_analysis.thirty_day += age_analysis.current or Decimal(0)
            period_age_analysis.sixty_day += age_analysis.thirty_day or Decimal(0)
            period_age_analysis.ninety_day += age_analysis.sixty_day or Decimal(0)
            period_age_analysis.one_twenty_day += (age_analysis.ninety_day or Decimal(0)) + (
                    age_analysis.one_twenty_day or Decimal(0))
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        if counter == 1:
            period_age_analysis.sixty_day += (age_analysis.thirty_day or Decimal(0)) + (
                    age_analysis.current or Decimal(0))
            period_age_analysis.ninety_day += age_analysis.sixty_day or Decimal(0)
            period_age_analysis.one_twenty_day += age_analysis.ninety_day or Decimal(0)
            period_age_analysis.one_twenty_day += age_analysis.one_twenty_day or Decimal(0)
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        if counter == 2:
            period_age_analysis.ninety_day += (age_analysis.sixty_day or Decimal(0)) + (
                    age_analysis.thirty_day or Decimal(0)) + (age_analysis.current or Decimal(0))
            period_age_analysis.one_twenty_day += (age_analysis.ninety_day or Decimal(0)) + (
                    age_analysis.one_twenty_day or Decimal(0))
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        if counter >= 3:
            period_age_analysis.one_twenty_day += (age_analysis.one_twenty_day or Decimal(0))
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        period_age_analysis.unallocated += age_analysis.unallocated
        period_age_analysis.balance += age_analysis.unallocated
        period_age_analysis.save()


def add_payment_allocation_age_analysis(child_payment: ChildPayment):
    period = child_payment.payment.period
    supplier = child_payment.payment.supplier
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        supplier=supplier,
        period=period
    )
    amount = child_payment.amount

    age_analysis.unallocated += amount if age_analysis.unallocated else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    age_unallocated(age_analysis=age_analysis, period=period, amount=amount)


def is_supplier_balancing(company: Company, year: Year, period: Period = None, end_date=None, ) -> bool:
    end_date = end_date or year.end_date
    if period and period.to_date:
        end_date = period.to_date
    account_total = get_total_for_sub_ledger(
        year=year,
        company=company,
        sub_ledger=SubLedgerModule.SUPPLIER,
        start_date=year.start_date,
        end_date=end_date
    )
    account_total = abs(account_total) if account_total else 0

    period = period or year.last_period_open
    age_analysis = AgeAnalysis.objects.filter(period=period).aggregate(total=Sum('balance'))
    age_total = abs(age_analysis['total']) if age_analysis and age_analysis['total'] else 0

    logger.info(f"Journals total {account_total} vs age total {age_total}, Difference => {age_total - account_total} "
                f" Balancing => {age_total == account_total} ")
    return age_total == account_total
