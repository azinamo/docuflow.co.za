import calendar
import csv
import json
import logging
import pprint
from datetime import datetime
from decimal import Decimal

import pytz
from dateutil import parser
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q, Sum
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse_lazy, reverse
from django.utils.timezone import now
from django.views.generic import ListView, TemplateView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from openpyxl import load_workbook

from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.enums import SubLedgerModule
from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Payment, Status, Invoice
from docuflow.apps.invoice.utils import completed_statuses, processing_statuses
from docuflow.apps.journals.services import get_total_for_sub_ledger
from docuflow.apps.period.models import Year, Period
from docuflow.apps.supplier.models import Supplier, Domain, OpeningBalance, AgeAnalysis
from .api.serializers import SupplierSerializer
from .forms import SupplierForm, SupplierDomainForm, ImportForm
from .report import SupplierBalance

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class SuppliersView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'supplier/index.html'

    def get_context_data(self, **kwargs):
        context = super(SuppliersView, self).get_context_data(**kwargs)
        context['ajax_url'] = reverse('supplier-list') + '?format=datatables'
        return context


class CreateSupplierView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = Supplier
    form_class = SupplierForm
    template_name = 'supplier/create.html'
    success_message = 'Supplier successfully saved'
    success_url = reverse_lazy('supplier-list')

    def get_context_data(self, **kwargs):
        context = super(CreateSupplierView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateSupplierView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class AddSupplierView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = Supplier
    form_class = SupplierForm
    template_name = 'supplier/add.html'
    success_message = 'Supplier successfully saved'

    def get_context_data(self, **kwargs):
        context = super(AddSupplierView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(AddSupplierView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Supplier could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(AddSupplierView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save(commit=False)
            return JsonResponse({
                'error': False,
                'text': 'Supplier successfully saved',
                'reload': True
            })
        return HttpResponse('Not allowed')


class EditSupplierView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = Supplier
    form_class = SupplierForm
    template_name = 'supplier/edit.html'
    success_message = 'Supplier successfully updated'
    success_url = reverse_lazy('supplier-list')

    def get_queryset(self):
        return Supplier.all.with_history(year=self.get_year()).filter(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(EditSupplierView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditSupplierView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class DeleteSupplierView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Supplier
    template_name = 'supplier/confirm_delete.html'
    success_message = 'Supplier successfully deleted'
    success_url = reverse_lazy('supplier-list')


class ImportSuppliersView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, FormView):
    template_name = 'supplier/import.html'
    form_class = ImportForm
    success_url = reverse_lazy('supplier-list')

    def get_supplier(self, company, code, name):
        return Supplier.objects.filter(company=company, code=code, name=name).first()

    def get_suppliers(self, company):
        return Supplier.objects.filter(company=company).all()

    def supplier_exist(self, suppliers, sup):
        for supplier in suppliers:
            if supplier.code == sup['code'] and supplier.name == sup['name']:
                return supplier
        return None

    def deactive_suppliers(self, suppliers):
        for supplier in suppliers:
            supplier.is_active = False
            supplier.save()

    def get_imported_data(self, csv_file):
        wb = load_workbook(csv_file, data_only=True)
        counter = 0
        data = []
        if 'Sheet1' in wb:
            ws = wb['Sheet1']
            for row in ws.rows:
                if len(row) > 1:
                    c = 0
                    data_dict = {}
                    for cell in row:
                        if c == 0:
                            data_dict['code'] = cell.value
                        if c == 1:
                            data_dict['name'] = cell.value
                        c += 1
                    data.append(data_dict)
                    counter += 1

        return {'supplier_data': data, 'total': counter }

    def get_csv_import_data(self, csv_file):
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        counter = 0
        data = []

        for line in lines:
            if counter > 0:
                fields = line.split(";")
                data_dict = {}

                if len(fields) > 1 and (fields[0] and fields[1]):
                    data_dict['code'] = fields[0]
                    data_dict['name'] = fields[1]
                    data.append(data_dict)
            counter += 1
        return {'supplier_data': data, 'total': counter}

    def import_suppliers(self, company, suppliers, suppliers_data):
        counter = 0
        imported = 0
        import_result = []
        for sup in suppliers_data['supplier_data']:
            try:
                supplier = self.get_supplier(company, sup['code'], sup['name'])
                if supplier:
                    sup['is_new'] = False
                    supplier.is_active = True
                    supplier.save()
                elif sup['name'] and sup['code']:
                        sup['is_new'] = True
                        supplier = Supplier.objects.create(
                            code=sup['code'],
                            name=sup['name'],
                            company=company
                        )
                        if supplier:
                            sup['saved'] = True
                            imported += 1
                import_result.append(sup)
            except Exception as err:
                pass
                # we need to log the error as they occured and or probably display results of the upload
                # print('Exception saving {}'.format(err))
            counter += 1
        return imported

    def form_valid(self, form):
        try:
            csv_file = self.request.FILES["csv_file"]
            if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx')):
                messages.error(self.request, 'File is not .csv, .xlsx or xls type')
                return HttpResponseRedirect(reverse("import-accounts"))

            if csv_file.multiple_chunks():
                messages.error(self.request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 1000),))
                return HttpResponseRedirect(reverse("import-accounts"))

            supplier_data = []
            if csv_file.name.endswith('.csv'):
                supplier_data = self.get_csv_import_data(csv_file)
            elif csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx'):
                supplier_data = self.get_imported_data(csv_file)
            # loop over the lines and save them to the database. If there is an error store as string and display

            company = self.get_company()
            suppliers = self.get_suppliers(company)
            if suppliers:
                self.deactive_suppliers(suppliers)

            imported = self.import_suppliers(company, suppliers, supplier_data)

            messages.success(self.request, f'{imported} supplier successfully imported.')
        except Exception as exception:
            logger.exception(exception)
            messages.error(self.request, "Unable to upload file")
        return super(ImportSuppliersView, self).form_valid(form)


class SampleSuppliersFileView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=suppliers.csv'

        writer = csv.writer(response)
        writer.writerow(['Supplier Code', 'Name'])
        writer.writerow(['CODE0001', 'Supplier Name'])

        return response


class SupplierDomainView(LoginRequiredMixin, ListView):
    model = Domain
    template_name = 'domain/index.html'
    context_object_name = 'domains'

    def get_context_data(self, **kwargs):
        context = super(SupplierDomainView, self).get_context_data(**kwargs)
        context['supplier'] = Supplier.objects.get(pk=self.kwargs['supplier_id'])
        return context

    def get_queryset(self):
        supplier = Supplier.objects.get(pk=self.kwargs['supplier_id'])
        return Domain.objects.filter(supplier=supplier)


class CreateSupplierDomainView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    model = Domain
    template_name = 'domain/create.html'
    success_message = 'Supplier domain successfully created.'

    def get_success_url(self):
        return reverse('create-supplier-domains', args=(self.kwargs['supplier_id'],))

    def get_form_class(self):
        return SupplierDomainForm

    def form_valid(self, form):
        domain = form.save(commit=False)
        domain.supplier = Supplier.objects.get(pk=self.kwargs['supplier_id'])
        domain.save()
        return HttpResponseRedirect(reverse('supplier-domain-index', args=(self.kwargs['supplier_id'], )))


class EditSupplierDomainView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Domain
    template_name = 'domain/edit.html'
    success_message = 'Supplier domain successfully updated.'

    def get_context_data(self, **kwargs):
        context = super(EditSupplierDomainView, self).get_context_data(**kwargs)
        context['supplier_id'] = self.kwargs['supplier_id']
        return context

    def get_success_url(self):
        return reverse('supplier-domain-index', args=(self.kwargs['supplier_id'], ))

    def get_form_class(self):
        return SupplierDomainForm


class DeleteSupplierDomainView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Domain
    template_name = 'domain/confirm_delete.html'
    success_message = 'Supplier domain succesfully detelete'

    def get_success_url(self):
        return reverse('supplier-domain-index', args=(self.kwargs['supplier_id'], ))


class SupplierBalanceOptionMixin(object):

    # noinspection PyUnresolvedReferences
    def get_invoice_statuses(self):
        completed = completed_statuses() + ['partially-paid']
        processing = processing_statuses()
        invoice_statuses = []
        has_status = self.request.GET.getlist('status', None)
        if has_status:
            statuses = self.request.GET.getlist('status')
            for status in statuses:
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        else:
            invoice_statuses = completed
        return invoice_statuses

    # noinspection PyUnresolvedReferences
    def get_filter_options(self, company):
        invoice_filter = {'company': company}
        payment_filter = {'company': company}

        today = datetime.now().date()
        is_historical = False
        posted_date = self.request.GET.get('date')
        if posted_date:
            requested_date = datetime.strptime(posted_date, '%d-%b-%Y').date()
            if today != requested_date:
                is_historical = True

        if 'supplier' in self.request.GET and self.request.GET['supplier']:
            invoice_filter['supplier_id'] = self.request.GET['supplier']
            payment_filter['supplier_id'] = self.request.GET['supplier']
        if is_historical:
            invoice_filter['accounting_date__lte'] = requested_date
            invoice_filter['payment_date__gte'] = requested_date
            payment_filter['payment_date__lte'] = requested_date
            date_filter = requested_date
        else:
            today = datetime.now().date()
            date_filter = today

        invoice_filter['invoice_type__is_account_posting'] = True
        return {'invoice': invoice_filter,
                'payment': payment_filter,
                'date': date_filter,
                'is_historical': is_historical
                }


class SupplierBalanceView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, SupplierBalanceOptionMixin, TemplateView):
    template_name = 'supplier/balance/index.html'
    success_message = 'Supplier domain successfully created.'

    def get_suppliers(self, company, filter_options):
        suppliers = []
        invoice_filter = filter_options.get('invoice', {})
        supplier_id = int(invoice_filter.get('supplier_id', 0))
        for supplier in Supplier.objects.filter(company=company,
                                                is_active=True, is_blocked=False).all():
            _supplier = {'name': supplier.name, 'id': supplier.id, 'selected': False}
            if supplier_id == supplier.id:
                _supplier['selected'] = True
            suppliers.append(_supplier)
        return suppliers

    def get_statuses(self):
        sts = ['final-signed', 'in-the-flow', 'end-flow-printed', 'paid', 'printed-status-still-in-the-flow']
        return Status.objects.filter(slug__in=sts).all()

    def display_statuses(self, filter_options):
        completed = completed_statuses()
        processing = processing_statuses()
        selected_statuses = filter_options.get('invoice', {})
        statuses_list = []
        for _completed_status in completed:
            if 'status__slug__in' in selected_statuses and _completed_status in selected_statuses['status__slug__in']:
                statuses_list.append('completed')

        for _processing_status in processing:
            if 'status__slug__in' in selected_statuses and _processing_status in selected_statuses['status__slug__in']:
                statuses_list.append('processing')
        return statuses_list

    def make_query_string(self, statuses, filter_options):
        completed_status = 'completed' in statuses
        processing_status = 'processing' in statuses
        invoice_filter = filter_options.get('invoice', {})
        is_historical = filter_options.get('is_historical', False)

        options_query_str = ''
        if completed_status and processing_status:
            options_query_str = 'status=completed&status=processing'
        elif completed_status:
            options_query_str = 'status=completed'
        elif processing_status:
            options_query_str = 'status=processing'

        # filter_date = invoice_filter.get('accounting_date__lte', None)
        filter_supplier = invoice_filter.get('supplier_id', None)
        if is_historical:
            if options_query_str:
                options_query_str += '&date={}'.format(self.request.GET['date'])
            else:
                options_query_str += 'date={}'.format(self.request.GET['date'])

        if filter_supplier:
            if options_query_str:
                options_query_str += '&supplier_id={}'.format(filter_supplier)
            else:
                options_query_str += 'supplier_id={}'.format(filter_supplier)

        query_str = '?{}&redirect={}'.format(options_query_str, reverse_lazy('supplier_balance'))

        return query_str

    def get_context_data(self, **kwargs):
        context = super(SupplierBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()

        # statuses = self.get_statuses()
        filter_options = self.get_filter_options(company)
        selected_statuses = self.display_statuses(filter_options)
        query_string = self.make_query_string(selected_statuses, filter_options)

        report = SupplierBalance(company, self.get_year(), filter_options)
        report.process_report()

        context['suppliers'] = self.get_suppliers(company, filter_options)
        context['statuses'] = selected_statuses
        context['date'] = filter_options.get('date', datetime.now().date())
        context['report'] = report
        context['company'] = company
        context['report_date'] = filter_options.get('date', datetime.now().date())
        context['query_string'] = query_string
        context['year'] = self.get_year()
        context['is_historical'] = filter_options.get('is_historical', False)
        return context


class SupplierBalanceReportView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, TemplateView):
    template_name = 'supplier/balance/balance.html'
    success_message = 'Supplier domain successfully created.'

    def get_month_day_range(self, date):
        first_day = date.replace(day=1)
        last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
        return first_day, last_day

    def get_invoices(self, company):
        statuses = ['final-signed', 'end-flow-printed', 'paid', 'printed-status-still-in-the-flow']
        invoices = Invoice.objects.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency').filter(
            company=company, status__slug__in=statuses, period__period_year__is_active=True
        )

        today = now()
        current_month_range = self.get_month_day_range(today)

        thirty_days = datetime(today.year, today.month -1, 1, tzinfo=pytz.UTC)
        thirty_day_range = self.get_month_day_range(thirty_days)

        sixty_days = datetime(today.year, today.month - 2, 1, tzinfo=pytz.UTC)
        sixty_day_range = self.get_month_day_range(sixty_days)

        ninety_days = datetime(today.year, today.month - 3, 1, tzinfo=pytz.UTC)
        ninety_day_range = self.get_month_day_range(ninety_days)

        one_twenty_or_more_days = datetime(today.year, today.month - 4, 1, tzinfo=pytz.UTC)
        one_twenty_or_more_range = self.get_month_day_range(one_twenty_or_more_days)

        day_ranges = {
            'one_twenty_or_more_days': {
                'from': one_twenty_or_more_range[0],
                'to': one_twenty_or_more_range[1],
                'totals': {},
                'total': 0
            },
            'ninety_days': {
                'from': ninety_day_range[0],
                'to': ninety_day_range[1],
                'totals': {},
                'total': 0
            },
            'sixty_days': {
                'from': sixty_day_range[0],
                'to': sixty_day_range[1],
                'totals': {},
                'total': 0
            },
            'thirty_days': {
                'from': thirty_day_range[0],
                'to': thirty_day_range[1],
                'totals': {},
                'total': 0
            },
            'current': {
                'from': current_month_range[0],
                'to': current_month_range[1],
                'totals': {},
                'total': 0
            }
        }
        supplier_invoices = {}
        for invoice in invoices:
            if invoice.supplier:
                if invoice.supplier_id in supplier_invoices:
                    supplier_invoices[invoice.supplier_id]['invoices'].append(invoice)
                else:
                    supplier_invoices[invoice.supplier_id] = {'invoices': [invoice],
                                                              'supplier': invoice.supplier,
                                                              'day_ranges': day_ranges,
                                                              'total': 0
                                                              }

        suppliers_report = self.get_supplier_day_range_totals(supplier_invoices, day_ranges)
        return suppliers_report

    def get_supplier_day_ranges(self, supplier_invoices, day_ranges):
        supplier = {'one_twenty_or_more_days': 0,
                    'ninety_days': 0,
                    'sixty_days': 0,
                    'thirty_days': 0,
                    'current': 0,
                    'day_ranges': 0,
                    'total': 0
                    }
        for invoice in supplier_invoices:
            invoice_total = invoice.total_amount
            supplier['name'] = invoice.supplier.name
            supplier['code'] = invoice.supplier.code
            supplier['id'] = invoice.supplier.id
            if invoice.is_credit_type:
                invoice_total = invoice_total * -1
            supplier['total'] += invoice_total
            if invoice.created_at < day_ranges['one_twenty_or_more_days']['from']:
                supplier['one_twenty_or_more_days'] += invoice_total
            elif day_ranges['ninety_days']['from'] <= invoice.created_at <= day_ranges['ninety_days']['to']:
                supplier['ninety_days'] += invoice_total
            elif day_ranges['sixty_days']['from'] <= invoice.created_at <= day_ranges['sixty_days']['to']:
                supplier['sixty_days'] += invoice_total
            elif day_ranges['thirty_days']['from'] <= invoice.created_at <= day_ranges['thirty_days']['to']:
                supplier['thirty_days'] += invoice_total
            elif day_ranges['current']['from'] <= invoice.created_at <= day_ranges['current']['to']:
                supplier['current'] += invoice_total
        return supplier

    def get_supplier_day_range_totals(self, supplier_invoices, day_ranges):
        report = {'day_ranges': day_ranges, 'suppliers': {}, 'total': 0}
        for supplier_id, supplier_invoice in supplier_invoices.items():
            supplier_day_range_totals = self.get_supplier_day_ranges(supplier_invoice['invoices'], day_ranges)
            report['suppliers'][supplier_id] = supplier_day_range_totals
            report['day_ranges']['one_twenty_or_more_days']['total'] += \
                supplier_day_range_totals['one_twenty_or_more_days']
            report['day_ranges']['ninety_days']['total'] += supplier_day_range_totals['ninety_days']
            report['day_ranges']['sixty_days']['total'] += supplier_day_range_totals['sixty_days']
            report['day_ranges']['thirty_days']['total'] += supplier_day_range_totals['thirty_days']
            report['day_ranges']['current']['total'] += supplier_day_range_totals['current']
            report['total'] += supplier_day_range_totals['total']
        return report

    def prepare_invoice(self, invoice):
        if invoice.is_credit_type:
            amount = invoice.total_amount * -1
        else:
            amount = invoice.total_amount

        inv = {'invoice_type': invoice.invoice_type,
               'unique_number': invoice,
               'id': invoice.id,
               'invoice_id': invoice.invoice_id,
               'invoice_date': invoice.invoice_date,
               'due_date': invoice.due_date,
               'amount': amount
               }

        return inv

    def get_supplier_invoices(self, invoices):
        supplier_invoices = {}
        for invoice in invoices.queryset.all():
            if invoice.supplier_id in supplier_invoices:
                _invoice = self.prepare_invoice(invoice)
                supplier_invoices[invoice.supplier_id]['total'] += _invoice['amount']
                supplier_invoices[invoice.supplier_id]['invoices'].append(_invoice)
            else:
                _invoice = self.prepare_invoice(invoice)
                supplier_invoices[invoice.supplier_id] = {'supplier': invoice.supplier,
                                                          'invoices': [_invoice],
                                                          'total': _invoice['amount']
                                                          }

        return supplier_invoices

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_context_data(self, **kwargs):
        context = super(SupplierBalanceReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        report = self.get_invoices(company)

        context['report'] = report
        context['company'] = company
        context['report_date'] = datetime.now
        return context

    def form_valid(self, form):
        domain = form.save(commit=False)
        domain.supplier = Supplier.objects.get(pk=self.kwargs['supplier_id'])
        domain.save()
        return HttpResponseRedirect(reverse('supplier-domain-index', args=(self.kwargs['supplier_id'],)))


class SupplierBalanceDetailReportView(LoginRequiredMixin, SuccessMessageMixin, SupplierBalanceOptionMixin, TemplateView):
    template_name = 'supplier/balance_detail.html'

    def get_supplier(self):
        return Supplier.objects.get(pk=self.kwargs['pk'])

    def get_available_payments(self, filter_options=None):
        return Payment.objects.select_related(
            'company', 'supplier', 'period'
        ).prefetch_related(
            'invoices'
        ).filter(
            **filter_options
        )

    def get_supplier_invoices(self, supplier, company):
        supplier_invoices = {'total_vat': 0, 'total_net': 0, 'total': 0, 'invoices': [], 'payments': []}
        filter_options = self.get_filter_options(company)
        date_filter = None

        invoice_filter = filter_options.get('invoice', {})
        payment_filter = filter_options.get('payment', {})
        is_historical = filter_options.get('is_historical', False)
        report_date = filter_options.get('date', datetime.now().date())

        payment_filter['supplier_id'] = supplier.id
        invoice_filter['supplier_id'] = supplier.id

        payments = self.get_available_payments(payment_filter)

        if 'date' in invoice_filter:
            date_filter = invoice_filter.pop('date')
        invoice_filter['period__isnull'] = False
        invoice_filter['period__period_year__isnull'] = False

        invoices = Invoice.objects.select_related(
            'company', 'status', 'supplier', 'invoice_type'
        ).prefetch_related(
            'invoice_references'
        ).filter(
            **invoice_filter
        ).exclude(open_amount=0)

        if date_filter:
            invoices = invoices.filter(date_filter)
        invoice_payment_filter = {'payment__payment_date__lte': report_date}
        for invoice in invoices:
            if is_historical:
                invoice_total = invoice.calculate_open_amount(invoice_payment_filter)
            else:
                invoice_total = invoice.open_amount

            supplier_invoices['total'] = supplier_invoices['total'] + invoice_total

            invoice_payment_filter = {'payment__payment_date__lte': filter_options['date']}
            logger.info(f" {is_historical} -> Invoice {str(invoice)} - ({invoice.id}) has open amount of {invoice_total}")
            supplier_invoices['invoices'].append({
                'invoice': invoice,
                'open_amount': invoice_total,
                'url': reverse('invoice_detail', kwargs={'pk': invoice.id})
            })
        for payment in payments:
            available_amount = payment.display_available_amount
            if available_amount != 0:
                supplier_invoices['total'] = supplier_invoices['total'] + payment.display_available_amount

                supplier_invoices['payments'].append({
                    'payment': payment
                })

        return supplier_invoices

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_redirect_url(self, supplier):
        redirect_url = self.request.GET.get('redirect', None)
        if redirect_url:
            self.request.session['supplier_invoices_{}'.format(supplier.id)] = redirect_url
        else:
            redirect_url = self.request.session.get('supplier_invoices_{}'.format(supplier.id), None)

        date = self.request.GET.get('date', None)
        if date:
            if redirect_url:
                redirect_url += '?date={}'.format(self.request.GET['date'])
            else:
                redirect_url += '?date={}'.format(self.request.GET['date'])
        statuses = self.request.GET.getlist('status', None)
        if statuses:
            if redirect_url:
                if "?" not in redirect_url:
                    status_str = '?status='
                else:
                    status_str = '&status='
                for s in statuses:
                    status_str = status_str + s
                redirect_url += status_str
            else:
                status_str = ''
                for s in statuses:
                    status_str = status_str + s
                redirect_url += '&status=' + status_str

        return redirect_url

    def get_context_data(self, **kwargs):
        context = super(SupplierBalanceDetailReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        supplier = self.get_supplier()
        report = self.get_supplier_invoices(supplier, company)

        context['redirect_url'] = self.get_redirect_url(supplier)
        context['report'] = report
        context['company'] = company
        context['supplier'] = supplier
        if 'date' in self.request.GET:
            parsed_date = parser.parse(self.request.GET['date'])
            context['report_date'] = parsed_date.date()
        else:
            context['report_date'] = datetime.now().date()
        # context['supplier_invoices'] = self.get_supplier_invoices(invoices)
        context['invoice_redirect_url'] = reverse('supplier_invoices', kwargs={'pk': supplier.id})
        return context


class SupplierDetailView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get_supplier(self):
        return Supplier.objects.get(pk=self.request.GET['supplier'])

    def get(self, request, *args, **kwargs):
        supplier = self.get_supplier()
        return JsonResponse({'supplier': str(supplier),
                             'name': supplier.name,
                             'code': supplier.code,
                             'vat_number': supplier.vat_number,
                             'company_number': supplier.company_number,
                             'is_document_protected': supplier.is_document_protected
                             })


class SuppliersListView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get_supplier(self):
        return Supplier.objects.value().filter()

    def get(self, request, *args, **kwargs):
        supplier = self.get_supplier()
        return JsonResponse({'supplier': str(supplier), 'name': supplier.name, 'code': supplier.code,
                             'vat_number': supplier.vat_number, 'company_number': supplier.company_number})


class SearchSuppliersView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        suppliers = []
        term = self.request.GET.get('term')
        if term:
            suppliers = Supplier.objects.search(self.get_company(), term).values('code', 'name', 'id')[0:50]
        suppliers_list = []
        for supplier in suppliers:
            suppliers_list.append({
                'code': supplier['code'],
                'name': supplier['name'],
                'text': f"{supplier['code']} - {supplier['name']}",
                'label': f"{supplier['code']} - {supplier['name']}",
                'id': supplier['id']
            })
        return JsonResponse({'err': 'nil', 'results': suppliers_list})


class SearchSuppliersByDfNumberView(View):

    def get(self, request, *args, **kwargs):
        df_number = self.request.GET.get('df_number')
        term = self.request.GET.get('term', '')
        filters = {}
        if df_number:
            filters['company__slug'] = df_number
        if term:
            filters['code'] = term

        supplier = Supplier.objects.filter(**filters).first()
        data = None
        if supplier:
            data = SupplierSerializer(instance=supplier).data
        return JsonResponse({'data': data})


class OpeningBalanceView(ProfileMixin, TemplateView):
    template_name = 'supplier/opening_balances.html'

    def get_opening_balances(self, company, year):
        opening_balances = OpeningBalance.objects.filter(year=year, company=company)
        supplier_balances = {}
        for opening_balance in opening_balances:
            supplier_balances[opening_balance.supplier_id] = opening_balance
        return supplier_balances

    def get_suppliers(self, company, year):
        balance_sheet_accounts = Supplier.objects.filter(company=company)
        opening_balances = self.get_opening_balances(company, year)
        suppliers = {}
        total = 0
        for supplier in balance_sheet_accounts:
            suppliers[supplier.id] = {
                'id': supplier.id,
                'code': supplier.code,
                'name': supplier.name,
                'opening_balance': 0
            }
            if supplier.id in opening_balances:
                suppliers[supplier.id]['opening_balance'] = opening_balances[supplier.id].amount
                total += opening_balances[supplier.id].amount
        return suppliers, total

    def get_context_data(self, **kwargs):
        context = super(OpeningBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        suppliers = self.get_suppliers(company, year)

        context['suppliers'] = suppliers[0]
        context['total'] = suppliers[1]
        context['year'] = year
        return context


class UpdateOpeningBalanceView(ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            obj = OpeningBalance.objects.filter(
                supplier_id=self.kwargs['supplier_id'],
                year=self.get_year()
            ).first()
            amount = Decimal(self.request.POST.get('amount', 0))
            if obj:
                obj.amount = amount
                obj.save()
            else:
                obj = OpeningBalance.objects.create(
                    supplier_id=self.kwargs['supplier_id'],
                    year=self.get_year(),
                    company=self.get_company(),
                    amount=amount
                )
            return JsonResponse({
                'text': 'Opening balance updated',
                'error': False,
                'amount': obj.amount
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Opening balance could not be updated',
                'error': True
            })


class AutocompleteSuppliersSearchView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        q = Q()
        q.add(Q(company=self.get_company()), Q.AND)
        term = self.request.GET.get('term')
        if term:
            q.add(Q(name__istartswith=term) | Q(code__istartswith=term), Q.AND)
        suppliers = []
        for supplier in Supplier.objects.filter(q):
            suppliers.append({
                'code': supplier.code,
                'name': supplier.name,
                'text': f"{supplier.code} - {supplier.name}",
                'description': supplier.name,
                'label': str(supplier),
                'id': supplier.id
            })
        return HttpResponse(json.dumps(suppliers))


class BalanceSheetVsAgeAnalysisView(ProfileMixin, TemplateView):
    template_name = 'supplier/balance_sheet_vs_age_analysis.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        company = self.get_company()

        year = Year.objects.started().get(company=company)
        period = Period.objects.open(company=company, year=year).first()

        sub_ledger_total = get_total_for_sub_ledger(
            year=year,
            company=company,
            sub_ledger=SubLedgerModule.SUPPLIER,
            start_date=year.start_date,
            end_date=year.end_date
        )
        account_total = abs(sub_ledger_total) if sub_ledger_total else 0

        age_analysis = AgeAnalysis.objects.filter(period=period).aggregate(total=Sum('balance'))
        age_total = abs(age_analysis['total']) if age_analysis and age_analysis['total'] else 0

        context['account_total'] = sub_ledger_total
        context['age_total'] = age_total
        context['is_balancing'] = age_total == account_total
        return context
