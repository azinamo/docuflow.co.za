from django.contrib import admin

from docuflow.apps.common.admin import YearFilter
from . import models


@admin.register(models.Supplier)
class SupplierAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'is_blocked', 'vat_number', 'company', 'email_address')
    list_filter = ('company', 'is_blocked')
    ordering = ('code', )
    search_fields = ('code', 'name')

    def has_add_permission(self, request):
        return False


@admin.register(models.Ledger)
class LedgerAdmin(admin.ModelAdmin):
    list_display = ('supplier', 'text', 'date', 'debit', 'credit', 'balance', 'status', 'module', )
    list_filter = ('supplier', 'status', 'journal', )
    fieldsets = [('Basic Information', {'fields': ['text', 'date', 'debit', 'credit', 'balance', 'status',
                                                   'supplier', 'period',
                                                   'created_by', 'module', 'content_type', 'object_id']}), ]
    list_select_related = ('supplier', )
    search_fields = ('text', )

    # def ledger_status(self, obj):
    #     if obj.status == enums.LedgerStatus.PENDING:
    #         return f'<div style="width:100%%; height:100%%; background-color:gray;">{obj.status}</div>'
    #     if obj.status == enums.LedgerStatus.COMPLETED:
    #         return f'<div style="width:100%%; height:100%%; background-color:green;">{obj.status}</div>'
    #     return obj.status
    # ledger_status.allow_tags = True

    def has_add_permission(self, request):
        return False


@admin.register(models.AgeAnalysis)
class SupplierAgeAnalysisAdmin(admin.ModelAdmin):
    list_display = ('supplier', 'period', 'unallocated', 'one_twenty_day', 'ninety_day', 'sixty_day', 'thirty_day',
                    'current', 'balance')
    list_filter = (YearFilter, 'supplier')
    ordering = ('period', '-balance')
    search_fields = ('supplier__name', 'balance')
    list_select_related = ('period', 'supplier')

    def has_add_permission(self, request):
        return False
