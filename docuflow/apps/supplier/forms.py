from django import forms

from .models import Supplier, Domain


class SupplierForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(SupplierForm, self).__init__(*args, **kwargs)
        self.fields['account_posting_proposal'].queryset = self.fields['account_posting_proposal'].queryset.filter(
            company=self.company)
        self.fields['flow_proposal'].queryset = self.fields['flow_proposal'].queryset.filter(company=self.company)
        self.fields['currency'].queryset = self.fields['currency'].queryset.filter(company=self.company)
        self.fields['default_account'].queryset = self.fields['default_account'].queryset.filter(company=self.company)
        self.fields['default_account'].empty_label = None
        if not self.instance.pk and self.company.default_supplier_account:
            self.fields['default_account'].initial = self.company.default_supplier_account

    class Meta:
        model = Supplier
        fields = ('code', 'name', 'is_blocked', 'is_classified', 'is_active', 'bee_scorecard_code', 'is_vat_registered',
                  'vat_number', 'vat_method', 'email_address', 'payment_terms', 'payment_days', 'discount_available',
                  'discount', 'discount_terms', 'discount_days', 'company_number', 'bank', 'account_number', 'branch',
                  'currency', 'address_line_1', 'address_line_2', 'postal_code', 'phone', 'flow_proposal',
                  'account_posting_proposal', 'default_account', 'is_document_protected'
                  )
        widgets = {
            'default_account': forms.Select(attrs={'class': 'chosen-select'}),
            'is_document_protected': forms.CheckboxInput()
        }

    def clean(self):
        cleaned_data = super(SupplierForm, self).clean()
        if cleaned_data['is_vat_registered'] is True:
            # raise forms.ValidationError('Default expenses account is required')
            if cleaned_data['vat_number'] is None:
                self.add_error('vat_number', 'Vat number is required')
            if cleaned_data['vat_method'] is None:
                self.add_error('vat_method', 'Vat method is required')

            if cleaned_data['discount_available'] is True:
                # raise forms.ValidationError('Default expenses account is required')
                if cleaned_data['discount'] is None:
                    self.add_error('discount', 'Discount is required')
                elif cleaned_data['discount'] > 99:
                    self.add_error('discount', 'Discount Cannot be more than 99')

                if cleaned_data['discount_terms'] is None:
                    self.add_error('discount_terms', 'Discount terms is required')

                if cleaned_data['discount_days'] is None:
                    self.add_error('discount_days', 'Discount days is required')
                elif cleaned_data['discount_days'] > 99:
                    self.add_error('discount_days', 'Discount days cannot be more than 99')

        payment_days = cleaned_data.get('payment_days')
        if payment_days and payment_days > 99:
            self.add_error('payment_days', 'Payment days Cannot be more than 99')
        if not self.instance.pk:
            code = cleaned_data.get('code')
            if code and Supplier.objects.filter(company=self.company, code=code).exists():
                self.add_error('code', 'Supplier code already exists')
        return cleaned_data

    def save(self, commit=True):
        supplier = super().save(commit=False)
        supplier.company = self.company
        supplier.is_active = True
        supplier.save()
        return supplier


class SupplierDomainForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'is_active')
        model = Domain


class ImportForm(forms.Form):
    csv_file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)')


class SupplierTemplateForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(SupplierTemplateForm, self).__init__(*args, **kwargs)
        self.fields['template'].queryset = self.fields['template'].queryset.filter(company=company)
    template = forms.ModelChoiceField(required=False, label='Select existing template', queryset=None)
    docfile = forms.FileField(required=False, label='Sample File/Invoice')
    label = forms.CharField(required=True, label='Template Name')
