import calendar
import logging
import pprint
from collections import OrderedDict

from docuflow.apps.invoice.models import Invoice, Payment
from docuflow.apps.period.models import Period

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class SupplierBalance(object):

    def __init__(self, company, year, filter_options, is_historical=False):
        self.company = company
        self.year = year
        self.day_ranges = OrderedDict()
        self.suppliers = {}
        self.is_historical = filter_options.get('is_historical', is_historical)
        self.report_date = filter_options.get('date')
        self.filter_options = filter_options
        self.total = 0

        self.day_ranges = {
            'one_twenty_or_more_days': {
                'totals': {},
                'total': 0,
                'key': 'one_twenty_or_more_days'
            },
            'ninety_days': {
                'totals': {},
                'total': 0,
                'key': 'ninety_days'
            },
            'sixty_days': {
                'totals': {},
                'total': 0,
                'key': 'sixty_days'
            },
            'thirty_days': {
                'totals': {},
                'total': 0,
                'key': 'thirty_days'
            },
            'current': {
                'totals': {},
                'total': 0,
                'key': 'current'
            },
            'unallocated': {
                'total': 0,
                'key': 'unallocated'
            }
        }

    def get_month_day_range(self, date):
        first_day = date.replace(day=1)
        last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
        return first_day, last_day

    def init_period(self, periods, key, index):
        try:
            return {
                'period': periods[index]['counter'],
                'year': periods[index]['year'],
                'to': periods[index],
                'totals': {},
                'total': 0,
                'key': key
            }
        except IndexError:
            return {
                'period': None,
                'year': None,
                'to': None,
                'totals': {},
                'total': 0,
                'key': key
            }

    def get_day_ranges(self):
        period = self.get_period()
        periods = self.get_periods(period)
        day_ranges = OrderedDict()
        if periods:
            day_ranges = {
                'one_twenty_or_more_days': self.init_period(periods, 'one_twenty_or_more_days', 4),
                'ninety_days': self.init_period(periods, 'ninety_days', 3),
                'sixty_days': self.init_period(periods, 'sixty_days', 2),
                'thirty_days': self.init_period(periods, 'thirty_days', 1),
                'current': self.init_period(periods, 'current', 0),
                'unallocated': {
                    'total': 0,
                    'key': 'unallocated'
                }
            }
        return day_ranges

    def get_invoices(self):
        filter_dict = self.filter_options.get('invoice', {})
        date_filter = None
        if 'date' in filter_dict:
            filter_dict.pop('date')
        filter_dict['period__isnull'] = False
        filter_dict['period__period_year__isnull'] = False
        invoices = Invoice.objects.select_related(
            'invoice_type', 'period', 'company', 'status', 'supplier', 'currency', 'period__period_year'
        ).prefetch_related(
            'payments'
        ).filter(
            **filter_dict
        ).exclude(open_amount=0).order_by(
            'supplier__code'
        )
        if date_filter:
            invoices = invoices.filter(date_filter)
        return invoices

    def get_available_payments(self):
        filter_dict = self.filter_options.get('payment', {})
        return Payment.objects.select_related(
            'company', 'supplier', 'period', 'period__period_year'
        ).prefetch_related(
            'invoices', 'children', 'children__parent', 'parent'
        ).filter(**filter_dict).exclude(balance=0)

    def get_invoice_data(self, invoice, invoice_payment_filter):
        if self.is_historical:
            open_amount = invoice.calculate_open_amount(invoice_payment_filter)
        else:
            open_amount = invoice.open_amount
        if invoice.is_credit_type and open_amount > 0:
            open_amount = open_amount * -1
        invoice_data = {'amount': open_amount,
                        'period': invoice.period.period,
                        'supplier': invoice.supplier,
                        'invoice_is_credit_type': invoice.is_credit_type,
                        'year': int(invoice.period.period_year.year)}
        return invoice_data

    def get_supplier_invoices(self, invoices, payments=None):
        supplier_invoices = {}
        payments = payments if payments else []
        invoice_payment_filter = {'payment__payment_date__lte': self.report_date}
        for invoice in invoices:
            if invoice.supplier:
                invoice_data = self.get_invoice_data(invoice, invoice_payment_filter)
                open_amount = invoice_data.get('amount', 0)
                logger.info(f"{self.is_historical} - Invoice {str(invoice)} - ({invoice.id}) has open amount of {open_amount}")
                if invoice.supplier in supplier_invoices:
                    supplier_invoices[invoice.supplier]['total'] += open_amount
                    supplier_invoices[invoice.supplier]['total_discount'] += invoice.supplier_discount
                    supplier_invoices[invoice.supplier]['invoices'].append(invoice_data)
                else:
                    supplier_invoices[invoice.supplier] = {'invoices': [invoice_data],
                                                           'total': open_amount,
                                                           'total_discount': invoice.supplier_discount,
                                                           'payments': [],
                                                           'total_payment': 0,
                                                           'total_payment_discount': 0
                                                           }
        for payment in payments:
            if payment.available_amount > 0:
                if payment.supplier in supplier_invoices:
                    supplier_invoices[payment.supplier]['payments'].append(payment)
                    if payment.available_amount:
                        supplier_invoices[payment.supplier]['total_payment'] += payment.display_available_amount
                    if payment.total_discount:
                        supplier_invoices[payment.supplier]['total_payment_discount'] += payment.total_discount
                else:
                    supplier_invoices[payment.supplier] = {
                                                           'invoices': [],
                                                           'total': 0,
                                                           'total_discount': 0,
                                                           'payments': [payment],
                                                           'total_payment': payment.display_available_amount,
                                                           'total_payment_discount': payment.total_discount
                                                           }
        return supplier_invoices

    def process_report(self):
        payments = self.get_available_payments()
        invoices = self.get_invoices()

        supplier_invoices = self.get_supplier_invoices(invoices, payments)
        self.get_supplier_day_range_totals(supplier_invoices)

    def get_period(self):
        if self.is_historical:
            heighest_period = Period.objects.filter(from_date__lte=self.report_date,
                                                    to_date__gte=self.report_date,
                                                    company=self.company
                                                    ).order_by('-period').first()
            return heighest_period.period
        else:
            try:
                heighest_period = Period.objects.filter(company=self.company,
                                                        is_closed=False,
                                                        period_year=self.year
                                                        ).order_by('-period').first()
                return heighest_period.period
            except Exception:
                heighest_period = Period.objects.filter(company=self.company,
                                                        period_year=self.year
                                                        ).order_by('-period').first()
                return heighest_period.period

    def get_periods(self, period):
        periods_list = []
        max_periods = 5
        heighest_period = period + 1
        if period:
            c = 0
            for p in reversed(range(1, heighest_period)):
                if c < max_periods:
                    periods_list.append({'counter': p, 'year': int(self.year.year)})
                c += 1
            periods_count = len(periods_list)
            if periods_count == max_periods:
                return periods_list
            else:
                outstanding = max_periods - periods_count
                previous_year_periods = self.get_previous_year_periods(outstanding)
                return periods_list + previous_year_periods
        return periods_list

    def get_previous_year_periods(self, counter):
        periods = Period.objects.filter(company=self.company, period_year__year=int(self.year.year) - 1
                                        ).order_by('-period')[0:counter]
        period_list = []
        for p in periods:
            period_list.append({'counter': p.period, 'year': int(p.period_year.year)})
        return period_list

    def get_supplier_day_ranges(self, invoices, total_unallocated, day_ranges):
        supplier = {'one_twenty_or_more_days': 0,
                    'ninety_days': 0,
                    'sixty_days': 0,
                    'thirty_days': 0,
                    'current': 0,
                    'day_ranges': 0,
                    'total_unallocated': total_unallocated,
                    'total': total_unallocated
                    }
        for invoice in invoices:
            invoice_supplier = invoice.get('supplier', None)
            invoice_period = invoice.get('period', None)
            if invoice_supplier and invoice_period:
                invoice_total = invoice.get('amount', 0)
                invoice_year = invoice.get('year', self.year.year)
                invoice_is_credit_type = invoice.get('is_credit_type', False)
                supplier['name'] = invoice_supplier.name
                supplier['code'] = invoice_supplier.code
                supplier['id'] = invoice_supplier.id
                if invoice_is_credit_type:
                    invoice_total = invoice_total * -1
                supplier['total'] += invoice_total
                if invoice_period == day_ranges['current']['period']:
                    supplier['current'] += invoice_total
                elif invoice_period == day_ranges['ninety_days']['period']:
                    supplier['ninety_days'] += invoice_total
                elif invoice_period == day_ranges['sixty_days']['period']:
                    supplier['sixty_days'] += invoice_total
                elif invoice_period == day_ranges['thirty_days']['period']:
                    supplier['thirty_days'] += invoice_total
                elif invoice_period <= day_ranges['one_twenty_or_more_days']['period'] or invoice_year < day_ranges['one_twenty_or_more_days']['year']:
                    supplier['one_twenty_or_more_days'] += invoice_total
        return supplier

    def get_supplier_day_range_totals(self, supplier_invoices):
        day_ranges = self.get_day_ranges()
        for supplier, supplier_invoice in supplier_invoices.items():
            invoices = supplier_invoice.get('invoices', [])
            total_unallocated = supplier_invoice.get('total_payment', 0)
            supplier_day_range_totals = self.get_supplier_day_ranges(invoices, total_unallocated, day_ranges)
            self.day_ranges['unallocated']['total'] += supplier_day_range_totals.get('total_unallocated', 0)
            self.suppliers[supplier] = supplier_day_range_totals
            self.day_ranges['one_twenty_or_more_days']['total'] += supplier_day_range_totals['one_twenty_or_more_days']
            self.day_ranges['ninety_days']['total'] += supplier_day_range_totals['ninety_days']
            self.day_ranges['sixty_days']['total'] += supplier_day_range_totals['sixty_days']
            self.day_ranges['thirty_days']['total'] += supplier_day_range_totals['thirty_days']
            self.day_ranges['current']['total'] += supplier_day_range_totals['current']

            self.total += supplier_day_range_totals['total']