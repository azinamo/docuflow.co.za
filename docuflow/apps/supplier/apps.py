from django.apps import AppConfig


class SupplierConfig(AppConfig):
    name = 'docuflow.apps.supplier'
