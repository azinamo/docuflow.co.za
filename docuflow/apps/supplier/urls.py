from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.SuppliersView.as_view(), name='supplier-list'),
    path('create/', views.CreateSupplierView.as_view(), name='create_supplier'),
    path('add/', views.AddSupplierView.as_view(), name='add_supplier'),
    path('edit/<int:pk>/', views.EditSupplierView.as_view(), name='edit_supplier'),
    path('delete/<int:pk>/', views.DeleteSupplierView.as_view(), name='delete_supplier'),
    path('import/', views.ImportSuppliersView.as_view(), name='import_suppliers'),
    path('sample/file/', views.SampleSuppliersFileView.as_view(), name='sample_suppliers_import_file'),
    path('supplier/detail/', views.SupplierDetailView.as_view(), name='supplier_detail'),
    path('supplier/list/', views.SuppliersListView.as_view(), name='suppliers_list'),
    path('opening/balances/', views.OpeningBalanceView.as_view(), name='supplier_opening_balances'),
    path('update/<int:supplier_id>/opening/balances/', csrf_exempt(views.UpdateOpeningBalanceView.as_view()),
         name='update_year_supplier_balance'),
    path('copy/<int:year_id>/previous_year/opening/balances/', views.UpdateOpeningBalanceView.as_view(),
         name='copy_supplier_previous_year_balances'),
    path('<int:supplier_id>/domains/', views.SupplierDomainView.as_view(), name='supplier-domain-index'),
    path('<int:supplier_id>/domains/create/', views.CreateSupplierDomainView.as_view(),
         name='create-supplier-domain'),
    path('<int:supplier_id>/domains/edit/<int:pk>/', views.EditSupplierDomainView.as_view(),
         name='edit-supplier-domain'),
    path('<int:supplier_id>/domains/delete/<int:pk>/', views.DeleteSupplierDomainView.as_view(),
         name='delete-supplier-domain'),
    path('balance/', views.SupplierBalanceView.as_view(), name='supplier_balance'),
    path('balance/report/', views.SupplierBalanceReportView.as_view(), name='supplier_balance_report'),
    path('balance/<int:pk>/detail/report/', views.SupplierBalanceDetailReportView.as_view(),
         name='supplier_invoices'),
    path('search/', views.SearchSuppliersView.as_view(), name='search_suppliers_list'),
    path('autocomplete/search/', views.AutocompleteSuppliersSearchView.as_view(), name='autocomplete_suppliers_search'),
    path('dfnumber/', views.SearchSuppliersByDfNumberView.as_view(), name='suppliers_by_df_number'),
    path('balance-sheet/vs/age-analysis', views.BalanceSheetVsAgeAnalysisView.as_view(),
         name='supplier_balance_sheet_vs_age_analysis'),
]

urlpatterns += [
    path('ledger/', include('docuflow.apps.supplier.modules.supplierledger.urls')),
    path('authorization/', include('docuflow.apps.supplier.modules.supplierauthorization.urls')),
    path('role/authorization/', include('docuflow.apps.supplier.modules.supplierroleauthorization.urls')),
    path('history/', include('docuflow.apps.supplier.modules.supplierhistory.urls')),
    path('ageanalysis/', include('docuflow.apps.supplier.modules.supplierageanalysis.urls')),
]
