from django_filters import rest_framework as filters

from .models import Supplier


class SupplierFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')
    code = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Supplier
        fields = ['company__slug', 'name', 'code']
