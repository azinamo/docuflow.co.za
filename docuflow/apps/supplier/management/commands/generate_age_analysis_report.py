import logging
import pprint

from django.core.management import BaseCommand
from django.db import transaction
from django.db.models import Q

from docuflow.apps.supplier.models import AgeAnalysis

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "generate age analysis report"

    def handle(self, *args, **options):
        with transaction.atomic():
            for age_analysis in AgeAnalysis.objects.filter(
                balance=0, period__period_year__year=2022
            ).exclude(supplier__company__slug='41054').filter(
                ~Q(current=0) | ~Q(thirty_day=0) | ~Q(sixty_day=0) | ~Q(ninety_day=0) | ~Q(one_twenty_day=0)
            ).order_by('id'):
                balance = age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
                if age_analysis.current != 0:
                    self.stdout.write(self.style.WARNING(f"*** Current => {age_analysis.current} Age has an error here {age_analysis.id} for supplier {age_analysis.supplier_id}"))
                if age_analysis.thirty_day != 0:
                    self.stdout.write(self.style.WARNING(f"*** 30 => {age_analysis.thirty_day} Age has an error here {age_analysis.id} for supplier {age_analysis.supplier_id}"))
                if age_analysis.sixty_day != 0:
                    self.stdout.write(self.style.WARNING(f"*** 60 => {age_analysis.sixty_day} Age has an error here {age_analysis.id} for supplier {age_analysis.supplier_id}"))
                if age_analysis.ninety_day != 0:
                    self.stdout.write(self.style.WARNING(f"*** 90 => {age_analysis.ninety_day} Age has an error here {age_analysis.id} for supplier {age_analysis.supplier_id}"))
                if age_analysis.one_twenty_day != 0:
                    self.stdout.write(self.style.WARNING(f"*** 120 => {age_analysis.one_twenty_day} Age has an error here {age_analysis.id} for supplier {age_analysis.supplier_id}"))
                # if balance != total:
                #     self.stdout.write(self.style.WARNING(f"*** Age has an error here {age_analysis.id}"))
                # else:
                #     self.stdout.write(self.style.ERROR(f"Age has an error here {age_analysis.id}"))