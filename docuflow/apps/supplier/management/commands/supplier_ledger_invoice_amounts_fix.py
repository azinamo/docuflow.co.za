from django.core.management import BaseCommand
from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.invoice.models import Invoice
from docuflow.apps.supplier.models import Ledger


class Command(BaseCommand):
    help = "Pull email from the sources"

    def handle(self, *args, **options):
        ledgers = Ledger.objects.filter(content_type=ContentType.objects.get_for_model(Invoice, for_concrete_model=False),
                                        credit=0, debit=0, created_at__year=2021, supplier_id=560)
        for ledger in ledgers:
            try:
                invoice = Invoice.objects.get(pk=ledger.object_id)
                if invoice.is_credit_type:
                    ledger.debit = invoice.total_amount
                else:
                    ledger.credit = invoice.total_amount
                ledger.save()
            except Invoice.DoesNotExist:
                pass
            self.stdout.write(self.style.ERROR(f"Invoice {ledger.object_id} is missing for supplier {ledger.supplier} "))
        self.stdout.write(self.style.SUCCESS("Done"))


