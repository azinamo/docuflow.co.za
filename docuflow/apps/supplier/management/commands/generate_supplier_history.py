from django.core.management import BaseCommand
from django.db import transaction

from docuflow.apps.company.models import Company
from docuflow.apps.supplier.models import History, Supplier
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.period.models import Period


class Command(BaseCommand):
    help = "Generate year history"

    def handle(self, *args, **options):
        with transaction.atomic():
            History.objects.update(
                net_amount=0,
                vat_amount=0,
                total_amount=0
            )
            for company in Company.objects.all():
                suppliers = Supplier.objects.filter(company=company).all()
                periods = Period.objects.filter(period_year__year__in=[2020, 2019]).all()
                invoices = Invoice.objects.select_related(
                    'supplier', 'currency'
                ).filter(
                    period__in=[period for period in periods],
                    supplier__in=[supplier for supplier in suppliers]
                ).all()

                for supplier in suppliers:
                    for period in periods:
                        history, is_new = History.objects.get_or_create(
                            period=period,
                            supplier=supplier,
                            defaults={'total_amount': 0, 'vat_amount': 0, 'net_amount': 0}
                        )
                        for invoice in invoices:
                            if invoice.supplier == supplier and invoice.period == period:
                                history.net_amount += invoice.net_amount
                                history.vat_amount += invoice.vat_amount
                                history.total_amount += invoice.total_amount
                                history.save()
                        self.stdout.write(self.style.SUCCESS(f"Created history for period {period} -> {supplier}"))
