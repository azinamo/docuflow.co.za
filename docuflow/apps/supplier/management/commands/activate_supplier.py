from django.core.management import BaseCommand, CommandError
from django.core.mail import send_mail
from django.db import transaction

from docuflow.apps.supplier.models import Supplier
from docuflow.apps.company.models import Company, Account
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.accounts.models import PermissionGroup, Role

class Command(BaseCommand):
    help = "Pull email from the sources"

    def create_default_roles(self, company, profile):
        role_permissions = [
            {'role': 'Administrator', 'group': 'Level 10', 'is_default': False},
            {'role': None, 'group': 'Level 6', 'is_default': False},
            {'role': None, 'group': 'Level 8', 'is_default': False},
            {'role': None, 'group': 'Level 2', 'is_default': False},
            {'role': 'Logger', 'group': 'Logger', 'is_default': True}
        ]

        for role_permission in role_permissions:
            if role_permission['role']:
                role = Role.objects.create(
                    company=company,
                    label=role_permission['role'],
                    name=self.create_name(role_permission['role'])
                )
                profile.roles.add(role)

                permission_group = PermissionGroup.objects.create(
                    company=company,
                    label=role_permission['group'],
                    is_default=role_permission['is_default'],
                    name="{}-{}-{}".format(company.id, company.name, role_permission['group'])
                )
                if permission_group:
                    role.permission_groups.add(permission_group)
            else:
                PermissionGroup.objects.create(
                    company=company,
                    label=role_permission['group'],
                    is_default=role_permission['is_default'],
                    name="{}-{}-{}".format(company.id, company.name, role_permission['group'])
                )

    def create_default_invoice_types(self, company):
        invoice_types = [{'name': 'Tax Invoice', 'account_posting': True, 'is_credit': False, 'is_default': True},
                         {'name': 'Credit Note', 'account_posting': True, 'is_credit': True, 'is_default': False},
                         {'name': 'Statement', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Purchase Order', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Quote', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Invoice', 'account_posting': True, 'is_credit': False, 'is_default': False},
                        ]
        for invoice_type in invoice_types:
            InvoiceType.objects.create(
                name=invoice_type['name'],
                is_default=invoice_type['is_default'],
                is_account_posting=invoice_type['account_posting'],
                is_credit=invoice_type['is_credit'],
                company=company
            )

    def handle(self, *args, **options):
        try:
            with transaction.atomic():
                suppliers = Supplier.objects.filter().all()
                for supplier in suppliers:
                    supplier.is_active = True
                    supplier.save()

                accounts = Account.objects.filter().all()
                for account in accounts:
                    account.is_active = True
                    account.save()

                companies = Company.objects.filter().all()
                for company in companies:

                    if not company.roles.exists():
                        self.create_default_roles(company)

                    if not company.document_types.exists():
                        self.create_default_invoice_types(company)
                    else:
                        has_tax_invoice = False
                        for invoice_type in company.document_types.all():
                            if invoice_type.name == 'Tax Invoice':
                                invoice_type.is_default = True
                                has_tax_invoice = True
                            invoice_type.save()
                        if not has_tax_invoice:
                            InvoiceType.objects.create(
                                name='',
                                company=company,
                                is_default=True
                            )
        except Exception as err:
            self.stdout.write(self.style.SUCCESS("Exception occurred, {}".format(err.__str__())))
        self.stdout.write(self.style.SUCCESS("Done activating documents"))


