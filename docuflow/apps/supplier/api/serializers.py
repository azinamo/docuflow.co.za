from django.urls import reverse_lazy
from rest_framework import serializers

from docuflow.apps.supplier.models import Supplier


class SupplierSerializer(serializers.ModelSerializer):
    default_account = serializers.SerializerMethodField()
    reference = serializers.SerializerMethodField()
    detail_url = serializers.SerializerMethodField()

    class Meta:
        model = Supplier
        fields = ('id', 'code', 'name', 'is_blocked', 'is_active', 'default_account', 'reference', 'detail_url',
                  'address_line_1', 'address_line_2', 'phone')
        datatables_always_serialize = ('id', 'detail_url')

    def get_reference(self, instance):
        return str(instance)

    def get_detail_url(self, instance):
        return reverse_lazy('edit_supplier', args=(instance.pk,))

    def get_default_account(self, instance):
        return str(instance.default_account) if instance.default_account else ''
