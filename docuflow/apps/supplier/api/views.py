from rest_framework import viewsets

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.supplier.models import Supplier

from .serializers import SupplierSerializer


class SupplierViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = SupplierSerializer

    def get_queryset(self):
        q = Supplier.all.select_related(
            'company', 'currency', 'flow_proposal', 'account_posting_proposal', 'default_account'
        )
        return q.filter(company=self.get_company()).order_by('code')
