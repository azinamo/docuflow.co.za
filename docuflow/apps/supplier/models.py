import logging
from decimal import Decimal

from django.contrib.contenttypes.fields import GenericForeignKey, ContentType
from django.db import models
from django.db.models import F, Q, Sum
from django.db.models.functions import Coalesce
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from enumfields.fields import EnumIntegerField, EnumField
from safedelete.models import SafeDeleteModel, SOFT_DELETE

from docuflow.apps.accountposting.models import AccountPosting
from docuflow.apps.accountposting.models import AccountPosting
from docuflow.apps.common.behaviours import Agable
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Company, Currency, Account
from docuflow.apps.company.enums import SubLedgerModule
from docuflow.apps.invoice.models import Invoice, Status, Payment, InvoicePayment
from docuflow.apps.period.models import Period
from docuflow.apps.workflow.models import Workflow
from . import enums

logger = logging.getLogger(__name__)


class SupplierManager(models.Manager):

    def get_queryset(self):
        return super(SupplierManager, self).get_queryset().filter(is_active=True, is_blocked=False, deleted__isnull=True)

    def search(self, company, term):
        return self.filter((Q(name__istartswith=term) | Q(code__istartswith=term)), company=company)

    def slice_period(self, periods, start, offset=None):
        try:
            period_page = periods[start:offset]
            period = period_page[0]
            if not period:
                return None
            return period
        except IndexError:
            return None

    def get_age_balances(self, company, supplier=None, year=None, period=None, status=None, to_date=None):
        supplier_filters = {'company': company}
        if supplier:
            supplier_filters['id'] = supplier.id

        periods = Period.objects.get_ranges(year, period)
        thirty_day_period = self.slice_period(periods, 1, 2)
        sixty_day_period = self.slice_period(periods, 2, 3)
        ninety_day_period = self.slice_period(periods, 3, 4)
        one_twenty_day_period = self.slice_period(periods, 4)

        current_invoice = Invoice.objects.get_sub_query_for_supplier_age(company=company, period=period)
        thirty_day = Invoice.objects.get_sub_query_for_supplier_age(company=company, period=thirty_day_period)
        sixty_day_invoice = Invoice.objects.get_sub_query_for_supplier_age(company=company, period=sixty_day_period)
        ninety_day_invoice = Invoice.objects.get_sub_query_for_supplier_age(company=company, period=ninety_day_period)
        one_twenty_day_invoice = Invoice.objects.get_sub_query_for_supplier_age(company=company, period=one_twenty_day_period, is_last=True)
        unallocated_payment = Payment.objects.for_supplier_age(company=company)
        return self.annotate(
            current=models.Subquery(current_invoice[:1]),
            thirty_day=models.Subquery(thirty_day[:1]),
            sixty_day=models.Subquery(sixty_day_invoice[:1]),
            ninety_day=models.Subquery(ninety_day_invoice[:1]),
            one_twenty_day=models.Subquery(one_twenty_day_invoice[:1]),
            payment_total=models.Subquery(unallocated_payment[:1]),
            unallocated=Coalesce(F('payment_total'), 0) * -1,
            invoice_open_balance=Coalesce(F('current'), 0) + Coalesce(F('thirty_day'), 0) + Coalesce(F('sixty_day'), 0) + Coalesce(F('ninety_day'), 0) + Coalesce(F('one_twenty_day'), 0),
            balance=F('invoice_open_balance') + F('unallocated'),
        ).filter(
            **supplier_filters
        ).values(
            'id', 'name', 'code', 'current', 'thirty_day', 'sixty_day', 'ninety_day', 'one_twenty_day', 'balance',
            'unallocated'
        ).exclude(balance=0).order_by('name')

    def get_historical_age_balances(self, company, supplier=None, year=None, period=None):
        supplier_filters = {'company': company}
        if supplier:
            supplier_filters['id'] = supplier.id

        periods = Period.objects.get_ranges(year, period)
        thirty_day_period = self.slice_period(periods, 1, 2)
        sixty_day_period = self.slice_period(periods, 2, 3)
        ninety_day_period = self.slice_period(periods, 3, 4)
        one_twenty_day_period = self.slice_period(periods, 4)
        current_invoice = Invoice.objects.get_total_for_supplier_age(company=company, period=period, year=year)
        thirty_day = Invoice.objects.get_total_for_supplier_age(company=company, period=thirty_day_period, year=year)
        sixty_day_invoice = Invoice.objects.get_total_for_supplier_age(company=company, period=sixty_day_period, year=year)
        ninety_day_invoice = Invoice.objects.get_total_for_supplier_age(company=company, period=ninety_day_period, year=year)
        one_twenty_day_invoice = Invoice.objects.get_total_for_supplier_age(company=company, period=one_twenty_day_period, year=year, is_last=True)
        return self.annotate(
            incoming_balance=models.Subquery(
                OpeningBalance.objects.filter(
                    year=year, supplier_id=models.OuterRef('id')
                ).values_list('amount')[:1]
            ),
            invoice_current=models.Subquery(current_invoice[:1]),
            current=Coalesce(F('incoming_balance'), 0) * -1 + Coalesce(F('invoice_current'), 0),
            thirty_day=models.Subquery(thirty_day[:1]),
            sixty_day=models.Subquery(sixty_day_invoice[:1]),
            ninety_day=models.Subquery(ninety_day_invoice[:1]),
            one_twenty_day=models.Subquery(one_twenty_day_invoice[:1]),
            invoice_open_balance=F('current') + Coalesce(F('thirty_day'), Decimal('0')) + Coalesce(F('sixty_day'), Decimal('0')) + Coalesce(F('ninety_day'), Decimal('0')) + Coalesce(F('one_twenty_day'), Decimal('0')),
            payment_total=models.Subquery(
                Payment.objects.values(
                    'supplier'
                ).filter(
                    supplier_id=models.OuterRef('id'), period__period_year=year,
                    period__to_date__lte=period.to_date
                ).annotate(
                    total=Sum('total_amount')
                ).order_by().values_list('total')[:1]
            ),
            balance=Coalesce(F('payment_total'), Decimal('0')) - F('invoice_open_balance'),
        ).filter(
            **supplier_filters
        ).values(
            'id', 'name', 'code', 'current', 'thirty_day', 'sixty_day', 'ninety_day', 'one_twenty_day', 'payment_total', 'balance'
        ).exclude(balance=0).order_by('name')

    def get_age_analysis_query(self, period, year, is_last=False):
        age_filter = {'period__period_year': year, 'supplier_id': models.OuterRef('id')}
        if is_last:
            age_filter['period__from_date__lte'] = period.to_date
        else:
            age_filter['period'] = period
        return AgeAnalysis.objects.values(
            'supplier_id'
        ).annotate(
            total=Coalesce(F('balance'), Decimal('0'))
        ).filter(
            **age_filter
        ).order_by().values_list('total')[:1]

    def get_age_analysis(self, company, supplier=None, year=None, period=None):
        supplier_filters = {'company': company}
        if supplier:
            supplier_filters['id'] = supplier.id

        age_analysis = AgeAnalysis.objects.select_related(
            'period', 'supplier'
        ).filter(period=period).exclude(balance=0)
        if supplier:
            age_analysis = age_analysis.filter(supplier=supplier)
        return age_analysis.order_by('supplier__name')

    def with_year_balance(self, year):
        return self.filter(
            ledgers__period__period_year=year
        ).annotate(
            total=Sum(Coalesce(F('ledgers__debit'), Decimal('0')), output_field=models.DecimalField())
        )


class AllSupplierManager(models.Manager):

    def get_queryset(self):
        return super(AllSupplierManager, self).get_queryset()

    def with_history(self, year):
        return self.prefetch_related(
            models.Prefetch(
                'periods', queryset=History.objects.select_related('period').filter(period__period_year=year)
            )
        ).annotate(
            total=Sum('periods__total_amount'),
            total_net=Sum('periods__net_amount'),
            total_vat=Sum('periods__vat_amount')
        )


class Supplier(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    PAYMENT_TERMS = (
        ('invoice_date', 'From invoice date'),
        ('statement_date', 'Statement date'),
    )
    DISCOUNT_TERMS = (
        ('invoice_date', 'From invoice date'),
        ('statement_date', 'Month end statement date'),
    )
    VAT_METHODS = (
        ('calculate', 'Calculate'),
        ('find_on_invoice', 'Find on invoice')
    )
    company = models.ForeignKey(Company, blank=True, null=True, related_name="suppliers", on_delete=models.CASCADE)
    code = models.CharField(max_length=255, db_index=True)
    name = models.CharField(max_length=255, db_index=True)
    supplier_id = models.PositiveIntegerField(null=True)
    is_active = models.BooleanField(default=True)
    is_blocked = models.BooleanField(default=False)
    is_classified = models.BooleanField(default=False)
    bee_scorecard_code = models.CharField(max_length=255, null=True, blank=True, verbose_name='BEE Scorecard Code')
    is_vat_registered = models.BooleanField(default=False)
    vat_number = models.CharField(max_length=255, blank=True, null=True)
    vat_method = EnumField(enums.VatMethod, max_length=25, blank=True, null=True)
    payment_terms = EnumField(enums.PaymentTerm, max_length=25, blank=True, null=True)
    payment_days = models.IntegerField(blank=True, null=True)
    discount_available = models.BooleanField(default=False)
    discount = models.DecimalField(blank=True, null=True, decimal_places=4, max_digits=15)
    discount_terms = EnumField(enums.DiscountTerm, max_length=25, blank=True, null=True)
    discount_days = models.IntegerField(blank=True, null=True)
    company_number = models.CharField(max_length=255, blank=True, null=True, verbose_name='Supplier Acc Ref')
    bank = models.CharField(max_length=50, blank=True, null=True)
    account_number = models.CharField(max_length=200, blank=True, null=True, verbose_name='Bank Account Number')
    branch = models.CharField(max_length=100, blank=True, null=True)
    currency = models.ForeignKey(Currency, related_name='supplier_currency', blank=True, null=True, on_delete=models.DO_NOTHING)
    address_line_1 = models.CharField(max_length=255, blank=True)
    address_line_2 = models.CharField(max_length=255, blank=True)
    province = models.CharField(max_length=255, blank=True, null=True, verbose_name='Province')
    phone = models.CharField(max_length=100, blank=True, null=True)
    email_address = models.EmailField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=200, blank=True, null=True, verbose_name='Postal Code')
    accounting_from_date = models.DateField(auto_now=False, null=True, blank=True)
    accounting_to_date = models.DateField(auto_now=False, null=True, blank=True)
    accounting_new_date = models.DateField(auto_now=False, null=True, blank=True)
    flow_proposal = models.ForeignKey(Workflow, blank=True, null=True, related_name="flows", on_delete=models.DO_NOTHING)
    account_posting_proposal = models.ForeignKey(AccountPosting, blank=True, null=True, related_name="posting_proposals", on_delete=models.DO_NOTHING)
    default_account = models.ForeignKey('company.Account', null=True, verbose_name='Control Account',
                                        related_name="supplier_default_accounts", on_delete=models.DO_NOTHING,
                                        limit_choices_to=models.Q(sub_ledger=SubLedgerModule.SUPPLIER))
    is_document_protected = models.BooleanField(default=False, verbose_name='Documents always protected')

    objects = SupplierManager()
    all = AllSupplierManager()

    def save(self, *args, **kwargs):
        if not self.pk or not self.supplier_id:
            supplier_counts = Supplier.objects.filter(company=self.company).count()
            self.supplier_id = supplier_counts + 1
        return super(Supplier, self).save(*args, **kwargs)

    def __str__(self):
        return f"{self.code} - {self.name}"

    def get_absolute_url(self):
        return reverse('edit-supplier', kwargs={'pk': self.pk})

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Suppliers'


class SupplierAuthorization(models.Model):

    supplier = models.OneToOneField(Supplier, related_name='authorization_limit', on_delete=models.CASCADE)
    amount = models.DecimalField(verbose_name='Maximum Period Limit', decimal_places=2, max_digits=15)


class RoleAuthorization(models.Model):

    role = models.ForeignKey('accounts.Role', related_name='roles', on_delete=models.CASCADE)
    supplier_authorization = models.ForeignKey(SupplierAuthorization, related_name='role_authorizations', on_delete=models.CASCADE)
    amount = models.DecimalField(verbose_name='Maximum order amount', decimal_places=2, max_digits=15)


class Domain(models.Model):
    supplier = models.ForeignKey(Supplier, blank=False, null=False, related_name="domains", on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Supplier Domains'


class LedgerQuerySet(models.QuerySet):

    # noinspection PyMethodMayBeStatic
    def get_debit(self, amount):
        if amount > 0:
            return amount
        return 0

    # noinspection PyMethodMayBeStatic
    def get_credit(self, amount):
        if amount < 0:
            return amount * -1
        return 0

    # noinspection PyMethodMayBeStatic
    def create_from_invoice(self, invoice, profile=None, status=None, general_ledger=None):
        if invoice.has_account_posting and not invoice.is_purchase_order:
            if not status:
                status = enums.LedgerStatus.PENDING
            status = enums.LedgerStatus.COMPLETED if status == 'completed' else enums.LedgerStatus.PENDING

            defaults = {
                'period': invoice.period,
                'balance': 0,
                'supplier_id': invoice.supplier.id,
                'created_by': profile if profile else invoice.created_by,
                'module': Module.PURCHASE.value,
                'date': invoice.accounting_date,
                'text': str(invoice),
                'status': status,
                'journal': general_ledger
            }
            if not invoice.payments.exists():
                # If the invoice is paid while in flow, the open amount is set to 0, so if the invoice is updated,
                # this will set the ledger to 0, to avoid that, we check if there was a payment done
                defaults['debit'] = invoice.open_amount if invoice.is_credit_type else 0
                defaults['credit'] = 0 if invoice.is_credit_type else invoice.open_amount

            ledger, _, = Ledger.objects.update_or_create(
                object_id=invoice.id,
                content_type=ContentType.objects.get_for_model(invoice),
                defaults=defaults
            )
            return ledger
        return None

    def with_available_amount(self):
        return self.annotate(
            available_amount=F('balance') * -1,
            total_amount=F('debit') + F('credit')
        )

    def with_available_payments(self, company, supplier_id):
        query = self.with_available_amount().filter(
            supplier__company=company, module=Module.JOURNAL, journal_line__isnull=False
        ).exclude(
            balance=0
        )
        if supplier_id:
            query = query.filter(supplier_id=supplier_id)
        return query

    # noinspection PyMethodMayBeStatic
    def get_ledgers(self, lookup_expression):
        return Ledger.objects.select_related(
            'period', 'supplier'
        ).filter(**lookup_expression).exclude(
            debit=Decimal(0), credit=Decimal(0)
        ).order_by(
            'supplier__code', 'supplier__name', 'date'
        )


class Ledger(models.Model):
    PENDING = 0
    COMPLETED = 1

    STATUSES = (
        (PENDING, 'Pending'),
        (COMPLETED, 'Complete')
    )

    supplier = models.ForeignKey(Supplier, blank=False, null=False, related_name="ledgers", on_delete=models.DO_NOTHING)
    content_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.CASCADE, related_name='supplier_ledgers')
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    journal = models.ForeignKey('journals.Journal', blank=True, null=True, on_delete=models.CASCADE)
    journal_line = models.OneToOneField('journals.JournalLine', related_name='supplier_ledger', null=True, blank=True, on_delete=models.CASCADE)
    ledger_id = models.CharField(max_length=255, null=True, blank=True)
    text = models.CharField(max_length=255, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    period = models.ForeignKey('period.Period', related_name="supplier_ledgers", on_delete=models.DO_NOTHING)
    debit = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    credit = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    balance = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    module = EnumIntegerField(Module,  default=Module.PURCHASE)
    created_by = models.ForeignKey('accounts.Profile', related_name="supplier_ledgers", on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now_add=True)
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=PENDING)

    objects = LedgerQuerySet.as_manager()

    def __str__(self):
        return f"SP{self.ledger_id}"

    class Meta:
        verbose_name_plural = 'Supplier Ledger'

    @property
    def module_name(self):
        return self.module.label

    @property
    def amount(self):
        total = 0
        if self.debit:
            total += self.debit
        if self.credit:
            total += self.credit * -1
        return total

    @property
    def is_pending(self):
        return self.status == Ledger.PENDING

    @property
    def is_completed(self):
        return self.status == Ledger.COMPLETED

    @property
    def available(self):
        return self.balance * -1


class LedgerPayment(models.Model):
    ledger = models.ForeignKey(Ledger, related_name='payments', on_delete=models.CASCADE)
    payment = models.ForeignKey('invoice.Payment', related_name='supplier_ledgers', on_delete=models.CASCADE)
    amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)

    @property
    def display_amount(self):
        return self.amount * -1


class OpeningBalance(models.Model):
    company = models.ForeignKey(Company, related_name='supplier_opening_balances', on_delete=models.CASCADE)
    supplier = models.ForeignKey(Supplier, related_name='opening_balances', on_delete=models.CASCADE)
    year = models.ForeignKey('period.Year', related_name='supplier_opening_balances', on_delete=models.CASCADE)
    amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')

    class Meta:
        ordering = ['-supplier__name']
        verbose_name = _('Opening Balance')
        verbose_name_plural = _('Opening Balances')

    def __str__(self):
        return f"Balance {self.supplier}-{self.year}"


class AgeAnalysis(Agable):
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('period', 'supplier', )

    def __str__(self):
        return f"{self.supplier} in period {self.period} -> {self.balance}"


class History(models.Model):
    supplier = models.ForeignKey(Supplier, related_name='periods', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', related_name='supplier_history', on_delete=models.PROTECT)
    net_amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    vat_amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    total_amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)

    class Meta:
        ordering = ('period__period', )
        unique_together = ('period', 'supplier')

    def add_invoice(self, invoice: Invoice):
        self.net_amount += invoice.net_amount if invoice.net_amount else 0
        self.vat_amount += invoice.vat_amount if invoice.vat_amount else 0
        self.total_amount += invoice.total_amount if invoice.total_amount else 0
        self.save()

