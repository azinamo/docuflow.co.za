# Generated by Django 3.1 on 2020-11-27 18:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('period', '0005_auto_20201124_2016'),
        ('supplier', '0009_auto_20201123_0721'),
    ]

    operations = [

        migrations.RenameField(
            model_name='supplier',
            old_name='bank_name',
            new_name='account_number',
        ),
        migrations.RenameField(
            model_name='supplier',
            old_name='vat_code',
            new_name='postal_code',
        ),
        migrations.AddField(
            model_name='supplier',
            name='province',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Province'),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='address_line_1',
            field=models.CharField(blank=True, default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='supplier',
            name='address_line_2',
            field=models.CharField(blank=True, default='', max_length=255),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='History',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('net_amount', models.DecimalField(decimal_places=2, default=0, max_digits=12)),
                ('vat_amount', models.DecimalField(decimal_places=2, default=0, max_digits=12)),
                ('total_amount', models.DecimalField(decimal_places=2, default=0, max_digits=12)),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='supplier_history', to='period.period')),
                ('supplier', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='supplier.supplier')),
            ],
        ),

    ]
