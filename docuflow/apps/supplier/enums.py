from enumfields import IntEnum, Enum


class SupplierStatus(IntEnum):
    ACTIVE = 1
    BLOCKED = 2
    CLASSIFIED = 3


class PaymentTerm(Enum):
    INVOICE_DATE = 'invoice_date'
    STATEMENT_DATE = 'statement_date'

    class Labels:
        INVOICE_DATE = 'From invoice date'
        STATEMENT_DATE = 'Statement date'


class DiscountTerm(Enum):
    INVOICE_DATE = 'invoice_date'
    STATEMENT_DATE = 'statement_date'

    class Labels:
        INVOICE_DATE = 'From invoice date'
        STATEMENT_DATE = 'Month end statement date'


class VatMethod(Enum):
    CALCULATE = 'calculate'
    FIND_ON_INVOICE = 'find_on_invoice'

    class Labels:
        CALCULATE = 'Calculate'
        FIND_ON_INVOICE = 'Find on invoice'


class LedgerStatus(IntEnum):
    PENDING = 0
    COMPLETED = 1
