import logging
import pprint
from datetime import timedelta
from decimal import Decimal

import xlwt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import F, Sum
from django.db.models.functions import Coalesce
from django.http.response import HttpResponse, JsonResponse
from django.utils.timezone import now
from django.views.generic import FormView, TemplateView, View
from openpyxl import Workbook

from docuflow.apps.budget.models import BudgetAccount, BudgetAccountPeriod
from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.enums import AccountType, SubLedgerModule
from docuflow.apps.company.models import Account, CompanyObject, Branch, Company
from docuflow.apps.inventory.models import BranchInventory, GroupLevel
from docuflow.apps.journals.models import JournalLine, OpeningBalance
from docuflow.apps.journals.services import (
    get_total_for_account, get_total_for_sub_ledger, get_total_for_vat_type, get_total_report_type
)
from docuflow.apps.period.models import Year, Period
from docuflow.apps.supplier.models import AgeAnalysis
from docuflow.apps.system.models import ReportType
from .balancesheet import BalanceSheet
from .forms import (
    IncomeStatementReportForm, BalanceSheetForm, PeriodIncomeStatementReportSearchForm, ForecastingReportForm,
    CashFlowForm, GeneralLedgerForm, InventoryValuationForm, SuffixReportForm
)
from .incomestatement import IncomeStatement, PeriodIncomeStatement
from .inventoryvaluation import InventoryValuation

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger("journal_error_log")


class ReportMixin:

    # noinspection PyUnresolvedReferences
    def get_company_objects(self):
        return CompanyObject.objects.prefetch_related(
            'object_items'
        ).filter(company_id=self.request.session['company'])

    def get_report_title(self, object_items):
        title = 'Whole Company'
        if len(object_items) > 0:
            objects = []
            company_objects = self.get_company_objects()
            for company_object in company_objects:
                for object_item in company_object.object_items.all():
                    if object_item.id in object_items:
                        objects.append(object_item.label)
            title = ",".join(objects)
        return title

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self,  current_year):
        y = int(current_year.year) - 1
        return Year.objects.filter(year=y, company=current_year.company).first()

    # noinspection PyUnresolvedReferences
    def can_include_cents(self):
        include_cents = self.request.GET.get('include_cents', False)
        if include_cents == 'on':
            return True
        return include_cents

    def can_include_zero_accounts(self):
        include_zero_accounts = self.request.GET.get('include_zero_accounts', False)
        if include_zero_accounts == 'on' or include_zero_accounts == '1':
            return True
        return include_zero_accounts

    # noinspection PyUnresolvedReferences
    def get_filter_options(self, company, year, account_type):
        filter_options = {'journal__company': company, 'journal__year': year,
                          'account__report__account_type': account_type,
                          'account__company': company}
        start_date = end_date = None

        if 'from_date' in self.request.GET and self.request.GET['from_date']:
            start_date = now().strptime(self.request.GET['from_date'], '%Y-%m-%d').date()
        if 'to_date' in self.request.GET and self.request.GET['to_date']:
            end_date = now().strptime(self.request.GET['to_date'], '%Y-%m-%d').date()
            filter_options['journal__date__lte'] = end_date

        object_items = self.request.GET.getlist('object_items')
        if object_items:
            items_list = [int(object_item) for object_item in object_items if not object_item.startswith('all', 0, 3)]
            if len(items_list):
                filter_options['object_items__id__in'] = [object_id for object_id in items_list]
        return filter_options, start_date, end_date


class IncomeStatementView(LoginRequiredMixin, ProfileMixin, ReportMixin, FormView):
    template_name = 'reports/incomestatement/index.html'
    form_class = IncomeStatementReportForm

    def get_initial(self):
        initial = super(IncomeStatementView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['company'] = self.get_company()
        kwargs['previous_year'] = self.get_previous_year(self.get_year())
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(IncomeStatementView, self).get_context_data(**kwargs)
        objects = self.get_company_objects()
        context['company_objects'] = objects.order_by('label')
        context['year'] = self.get_year()
        context['objects_count'] = len(objects)
        return context


class GenerateIncomeStatementView(LoginRequiredMixin, ProfileMixin, ReportMixin, TemplateView):

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'reports/incomestatement/print.html'
        return 'reports/incomestatement/show.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateIncomeStatementView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()

        income_statement_form = IncomeStatementReportForm(data=self.request.GET, company=company, year=year,
                                                          previous_year=self.get_previous_year(year))
        if income_statement_form.is_valid():
            columns = self.request.GET.getlist('columns', ['period', 'accumulated', 'previous_year', 'budget',
                                                           'budget_variance'])

            income_statement = income_statement_form.execute(columns)
            context['income_statement'] = income_statement
            context['year'] = year
            context['start_date'] = income_statement.start_date
            context['end_date'] = income_statement.end_date
            context['print_date'] = now().date()
            context['company'] = company
            context['previous_year_title'] = income_statement.previous_year_title
            context['budget_title'] = income_statement.budget_title
            context['report_title'] = self.get_report_title([])
            context['columns'] = columns
            context['column_count'] = len(columns)
            context['include_cents'] = income_statement.include_cents
        else:
            logger.info(income_statement_form.errors)
            context['errors'] = income_statement_form.errors
        return context


class DownloadIncomeStatementView(LoginRequiredMixin, ProfileMixin, ReportMixin, View):

    def get(self, request, *args, **kwargs):
        company = self.get_company()
        year = self.get_year()

        balance_sheet_form = BalanceSheetForm(data=self.request.GET, company=company, year=year,
                                              previous_year=self.get_previous_year(year))
        if balance_sheet_form.is_valid():
            balance_sheet = balance_sheet_form.execute()

            show_movement = year.start_date != balance_sheet.start_date

            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename=""'

            wb = Workbook()
            filename = 'income_statement_sheet.xls'
            ws = wb.active
            ws.title = 'Balance sheet report'

            # Sheet header, first row
            row_count = 1
            font_style = xlwt.XFStyle()
            font_style.font.bold = True
            ws.cell(column=0, row=row_count, value=str(company))

            row_count += 1
            ws.cell(column=0, row=row_count, value='Balance sheet report')

            row_count += 1
            ws.cell(column=1, row=row_count, value='Preliminary')

            row_count += 1
            ws.cell(column=1, row=row_count, value='Year')
            ws.cell(column=2, row=row_count, value=year.start_date.strftime('%d-%M-%Y'))
            ws.cell(column=3, row=row_count, value=year.end_date.strftime('%d-%M-%Y'))

            row_count += 1
            ws.cell(column=1, row=row_count, value='Balance for')
            ws.cell(column=2, row=row_count, value=self.get_report_title([]))

            row_count += 1
            ws.write(row_count, 0, '', font_style)
            ws.write(row_count, 1, )
            ws.write(row_count, 2, )

            ws.cell(column=1, row=row_count, value='Period dates(s)')
            ws.cell(column=2, row=row_count, value=balance_sheet.start_date.strftime('%d-%M-%Y'))
            ws.cell(column=3, row=row_count, value=balance_sheet.end_date.strftime('%d-%M-%Y'))

            row_count += 1
            ws.cell(column=1, row=row_count, value='Preliminary')

            row_count += 1
            row_count += 1
            ws.cell(column=1, row=row_count, value='')
            ws.cell(column=2, row=row_count, value='')
            ws.cell(column=3, row=row_count, value='Ingoing Balance')
            ws.cell(column=4, row=row_count, value='Period')
            ws.cell(column=5, row=row_count, value='Outgoing Balance')

            # Sheet body, remaining rows
            row_count += 1
            ws.cell(column=1, row=row_count, value='ASSET')
            row_count += 1
            for report_type, report_accounts in balance_sheet.assets.items():
                ws.cell(column=1, row=row_count, value=report_type.name)
                ws.cell(column=2, row=row_count, value='')
                ws.cell(column=3, row=row_count, value='')
                ws.cell(column=4, row=row_count, value='')
                ws.cell(column=5, row=row_count, value='')
                row_count += 1
                for account, account_line in report_accounts['accounts'].items():
                    ws.cell(column=1, row=row_count, value=account.name)
                    ws.cell(column=2, row=row_count, value=account.code)
                    ws.cell(column=3, row=row_count, value=account_line.get('incoming', 0))
                    ws.cell(column=4, row=row_count, value=account_line.get('period_amount', 0))
                    ws.cell(column=5, row=row_count, value=account_line.get('outgoing', 0))
                    row_count += 1

                ws.cell(column=1, row=row_count, value=f'Subtotal {report_type.name}')
                ws.cell(column=2, row=row_count, value='')
                ws.cell(column=3, row=row_count, value=report_accounts['incoming'])
                ws.cell(column=4, row=row_count, value=report_accounts['period_amount'])
                ws.cell(column=5, row=row_count, value=report_accounts['outgoing'])

                row_count += 1
            # ws.write(row_count, 0, 'Total Assets', font_style)
            # ws.write(row_count, 1, '')
            # ws.write(row_count, 2, balance_sheet.total_incoming_asset, font_style)
            # ws.write(row_count, 3, balance_sheet.total_period_asset, font_style)
            # ws.write(row_count, 4, balance_sheet.total_outgoing_asset, font_style)
            # row_count += 1
            # row_count += 1
            # ws.write(row_count, 0, 'LIABILITIES AND EQUITY', font_style)
            # row_count += 1
            # for report_type, liabilities_accounts in balance_sheet.liabilities.items():
            #     ws.write(row_count, 0, report_type.name)
            #     ws.write(row_count, 1, '')
            #     ws.write(row_count, 2, '')
            #     ws.write(row_count, 3, '')
            #     ws.write(row_count, 4, '')
            #     row_count += 1
            #     for account, account_line in liabilities_accounts['accounts'].items():
            #         ws.write(row_count, 0, account.name)
            #         ws.write(row_count, 1, account.code)
            #         ws.write(row_count, 2, account_line.get('incoming', 0))
            #         ws.write(row_count, 3, account_line.get('period_amount', 0))
            #         ws.write(row_count, 4, account_line.get('outgoing', 0))
            #         row_count += 1
            #     ws.write(row_count, 0, f'Subtotal {report_type.name}')
            #     ws.write(row_count, 1, '')
            #     ws.write(row_count, 2, liabilities_accounts['incoming'])
            #     ws.write(row_count, 3, liabilities_accounts['period_amount'])
            #     ws.write(row_count, 4, liabilities_accounts['outgoing'])
            #     row_count += 1
            # ws.write(row_count, 0, 'Total Liabilities and Own Capital', font_style)
            # ws.write(row_count, 1, '')
            # ws.write(row_count, 2, balance_sheet.total_incoming_liabilities, font_style)
            # ws.write(row_count, 3, balance_sheet.total_period_liabilities, font_style)
            # ws.write(row_count, 4, balance_sheet.total_outgoing_liabilities, font_style)
            # row_count += 1
            # ws.write(row_count, 0, 'Total', font_style)
            # ws.write(row_count, 1, balance_sheet.total_incoming, font_style)
            # ws.write(row_count, 2, '')
            # ws.write(row_count, 3, balance_sheet.total_period, font_style)
            # ws.write(row_count, 4, balance_sheet.total_outgoing, font_style)
            wb.save(response)
            return response
        else:
            return HttpResponse('Could not generate the balance sheet report')


class BalanceSheetView(LoginRequiredMixin, ProfileMixin, ReportMixin, FormView):
    template_name = 'reports/balancesheet/balance_sheet.html'
    form_class = BalanceSheetForm

    def get_initial(self):
        initial = super(BalanceSheetView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        initial['year'] = year
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['company'] = self.get_company()
        kwargs['previous_year'] = self.get_previous_year(self.get_year())
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(BalanceSheetView, self).get_context_data(**kwargs)
        objects = self.get_company_objects()
        context['company_objects'] = objects.order_by('label')
        context['year'] = self.get_year()
        context['objects_count'] = len(objects)
        return context


class GenerateBalanceSheetView(LoginRequiredMixin, ProfileMixin, ReportMixin, TemplateView):
    template_name = 'reports/balancesheet/show.html'

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'reports/balancesheet/print.html'
        return 'reports/balancesheet/show.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateBalanceSheetView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()

        balance_sheet_form = BalanceSheetForm(
            data=self.request.GET,
            company=company,
            year=year,
            previous_year=self.get_previous_year(current_year=year)
        )
        if balance_sheet_form.is_valid():
            balance_sheet = balance_sheet_form.execute()

            context['income_statement'] = balance_sheet.income_statement
            context['balance_sheet'] = balance_sheet
            context['year'] = year
            context['show_movement'] = year.start_date != balance_sheet.start_date
            context['start_date'] = balance_sheet.start_date
            context['end_date'] = balance_sheet.end_date
            context['print_date'] = now().date()
            context['company'] = company
            context['include_cents'] = balance_sheet.include_cents
            context['report_title'] = self.get_report_title([])
        else:
            context['errors'] = balance_sheet_form.errors
        return context


class DownloadBalanceSheetView(LoginRequiredMixin, ProfileMixin, ReportMixin, View):

    def get(self, request, *args, **kwargs):
        company = self.get_company()
        year = self.get_year()

        balance_sheet_form = BalanceSheetForm(data=self.request.GET, company=company, year=year,
                                              previous_year=self.get_previous_year(year))
        if balance_sheet_form.is_valid():
            balance_sheet = balance_sheet_form.execute()

            show_movement = year.start_date != balance_sheet.start_date

            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename="balance_sheet.xls"'

            wb = xlwt.Workbook(encoding='utf-8')
            ws = wb.add_sheet('Balance Sheet')

            # Sheet header, first row
            row_count = 1
            font_style = xlwt.XFStyle()
            font_style.font.bold = True

            row = ws.row(0)
            row.write(0, str(company), font_style)

            row_count += 1
            ws.write(row_count, 0, 'Balance sheet report', font_style)

            row_count += 1
            ws.write(row_count, 0, 'Preliminary', font_style)

            row_count += 1
            ws.write(row_count, 0, 'Year', font_style)
            ws.write(row_count, 1, year.start_date.strftime('%d-%M-%Y'), font_style)
            ws.write(row_count, 2, year.end_date.strftime('%d-%M-%Y'), font_style)

            row_count += 1
            ws.write(row_count, 0, 'Balance for', font_style)
            ws.write(row_count, 1, self.get_report_title([]))

            row_count += 1
            ws.write(row_count, 0, 'Period dates(s)', font_style)
            ws.write(row_count, 1, balance_sheet.start_date.strftime('%d-%M-%Y'))
            ws.write(row_count, 2, balance_sheet.end_date.strftime('%d-%M-%Y'))

            row_count += 1
            ws.write(row_count, 0, 'Preliminary', font_style)

            header_row = ws.row(1)
            header_row.write(0, '', font_style)
            header_row.write(1, '', font_style)
            header_row.write(2, 'Ingoing Balance', font_style)
            header_row.write(3, 'Period', font_style)
            header_row.write(4, 'Outgoing Balance', font_style)
            # Sheet body, remaining rows
            row_count += 1
            ws.write(row_count, 0, 'ASSETS', font_style)
            row_count += 1
            for report_type, report_accounts in balance_sheet.assets.items():
                ws.write(row_count, 0, report_type.name)
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                row_count += 1
                for account, account_line in report_accounts['accounts'].items():
                    ws.write(row_count, 0, account.name)
                    ws.write(row_count, 1, account.code)
                    ws.write(row_count, 2, account_line.get('incoming', 0))
                    ws.write(row_count, 3, account_line.get('period_amount', 0))
                    ws.write(row_count, 4, account_line.get('outgoing', 0))
                    row_count += 1
                ws.write(row_count, 0, f'Subtotal {report_type.name}')
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, report_accounts['incoming'])
                ws.write(row_count, 3, report_accounts['period_amount'])
                ws.write(row_count, 4, report_accounts['outgoing'])
                row_count += 1
            ws.write(row_count, 0, 'Total Assets', font_style)
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, balance_sheet.total_incoming_asset, font_style)
            ws.write(row_count, 3, balance_sheet.total_period_asset, font_style)
            ws.write(row_count, 4, balance_sheet.total_outgoing_asset, font_style)
            row_count += 1
            row_count += 1
            ws.write(row_count, 0, 'LIABILITIES AND EQUITY', font_style)
            row_count += 1
            for report_type, liabilities_accounts in balance_sheet.liabilities.items():
                ws.write(row_count, 0, report_type.name)
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                row_count += 1
                for account, account_line in liabilities_accounts['accounts'].items():
                    ws.write(row_count, 0, account.name)
                    ws.write(row_count, 1, account.code)
                    ws.write(row_count, 2, account_line.get('incoming', 0))
                    ws.write(row_count, 3, account_line.get('period_amount', 0))
                    ws.write(row_count, 4, account_line.get('outgoing', 0))
                    row_count += 1
                ws.write(row_count, 0, f'Subtotal {report_type.name}')
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, liabilities_accounts['incoming'])
                ws.write(row_count, 3, liabilities_accounts['period_amount'])
                ws.write(row_count, 4, liabilities_accounts['outgoing'])
                row_count += 1
            ws.write(row_count, 0, 'Total Liabilities and Own Capital', font_style)
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, balance_sheet.total_incoming_liabilities, font_style)
            ws.write(row_count, 3, balance_sheet.total_period_liabilities, font_style)
            ws.write(row_count, 4, balance_sheet.total_outgoing_liabilities, font_style)
            row_count += 1
            ws.write(row_count, 0, 'Total', font_style)
            ws.write(row_count, 1, balance_sheet.total_incoming, font_style)
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, balance_sheet.total_period, font_style)
            ws.write(row_count, 4, balance_sheet.total_outgoing, font_style)
            wb.save(response)
            return response
        else:
            return HttpResponse('Could not generate the balance sheet report')


class PeriodIncomeStatementView(LoginRequiredMixin, ProfileMixin, ReportMixin, FormView):
    template_name = 'reports/periodincomestatement/index.html'
    form_class = PeriodIncomeStatementReportSearchForm

    def get_initial(self):
        initial = super(PeriodIncomeStatementView, self).get_initial()
        return initial

    def get_periods(self, company, year):
        return Period.objects.filter(
            company=company,
            period_year=year
        )

    def get_context_data(self, **kwargs):
        context = super(PeriodIncomeStatementView, self).get_context_data(**kwargs)
        year = self.get_year()
        company = self.get_company()
        periods = self.get_periods(company, year)
        objects = self.get_company_objects()
        context['from_periods'] = periods.order_by('period')
        context['to_periods'] = periods.order_by('-period')
        context['company_objects'] = objects.order_by('label')
        context['year'] = year
        context['objects_count'] = len(objects)
        return context


class GeneratePeriodIncomeStatementView(LoginRequiredMixin, ProfileMixin, ReportMixin, TemplateView):

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'reports/periodincomestatement/print.html'
        return 'reports/periodincomestatement/show.html'

    def get_filter_options(self, company, year, account_type):
        filter_options = {'journal__company': company, 'journal__year': year,
                          'account__report__account_type': account_type,
                          'account__company': company
                          }

        from_period = self.request.GET.get('from_period', 1)
        to_period = self.request.GET.get('to_period', 12)

        if from_period:
            filter_options['journal__period__period__gte'] = from_period
        if to_period:
            filter_options['journal__period__period__lte'] = to_period

        object_items = self.request.GET.getlist('object_items')
        if object_items:
            items_list = [int(object_item) for object_item in object_items if not object_item.startswith('all', 0, 3)]
            if len(items_list):
                filter_options['object_items__id__in'] = [object_id for object_id in items_list]
        return filter_options, from_period, to_period

    def get_columns(self):
        # columns = ['period', 'accumulated', 'previous_year', 'budget', 'budget_variance']
        return self.request.GET.getlist('columns')

    def get_context_data(self, **kwargs):
        context = super(GeneratePeriodIncomeStatementView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        columns = self.get_columns()
        filter_options, from_period, to_period = self.get_filter_options(company, year, AccountType.INCOME_STATEMENT.value)
        include_cents = self.can_include_cents()

        previous_year_title = self.request.GET.get('previous_year_title', 'Whole')
        budget_title = self.request.GET.get('budget_title', 'Whole')

        options = {'include_cents': include_cents, 'previous_year_title': previous_year_title,
                   'budget_title': budget_title}
        income_statement = PeriodIncomeStatement(year, from_period, to_period, filter_options, **options)
        results = income_statement.execute()

        period_from = Period.objects.filter(period=from_period, period_year=year).first()
        period_to = Period.objects.filter(period=to_period, period_year=year).first()

        report_title = self.get_report_title(filter_options.get('object_items__id__in', []))
        periods = income_statement.get_period_range()
        context['income_statement'] = income_statement
        context['results'] = results
        context['periods'] = periods
        context['year'] = year
        context['print_date'] = now().date()
        context['company'] = company
        context['previous_year_title'] = previous_year_title
        context['budget_title'] = budget_title
        context['report_title'] = report_title
        context['columns'] = columns
        context['from_period'] = period_from
        context['to_period'] = period_to
        context['end_date'] = period_to.to_date + timedelta(days=1)
        context['include_cents'] = include_cents
        context['column_count'] = len(periods) + len(columns)
        return context


class TrialBalanceView(LoginRequiredMixin, ProfileMixin, ReportMixin, FormView):
    template_name = 'reports/trialbalance/index.html'
    form_class = BalanceSheetForm

    def get_initial(self):
        initial = super(TrialBalanceView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['company'] = self.get_company()
        kwargs['previous_year'] = self.get_previous_year(self.get_year())
        return kwargs

    def get_accounts(self, company):
        return Account.objects.filter(company=company).exclude(code='').exclude(name='')

    def get_context_data(self, **kwargs):
        context = super(TrialBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = self.get_company_objects()
        context['company_objects'] = objects.order_by('label')
        context['year'] = self.get_year()
        context['objects_count'] = len(objects)
        return context


class GenerateTrialBalanceView(LoginRequiredMixin, ProfileMixin, ReportMixin, TemplateView):

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'reports/trialbalance/print.html'
        return 'reports/trialbalance/show.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateTrialBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        previous_year = self.get_previous_year(year)
        include_cents = self.can_include_cents()
        include_zero_accounts = self.can_include_zero_accounts()
        income_filter_options, start_date, end_date = self.get_filter_options(company, year, AccountType.INCOME_STATEMENT.value)

        options = {'include_cents': include_cents, 'previous_year_title': 'Whole',
                   'current_year_filter': 'balance_sheet', 'include_zero_accounts': include_zero_accounts}

        income_statement = IncomeStatement(
            company=company,
            year=year,
            start_date=start_date,
            end_date=end_date,
            previous_year=previous_year,
            **options
        )
        income_statement.execute()

        balance_sheet_filter_options, start_date, end_date = self.get_filter_options(
            company, year, AccountType.BALANCE_SHEET.value
        )
        balance_sheet = BalanceSheet(
            company=company,
            year=year,
            start_date=start_date,
            end_date=end_date,
            income_statement=income_statement,
            **options
        )
        balance_sheet.execute()

        context['income_statement'] = income_statement
        context['balance_sheet'] = balance_sheet
        context['year'] = year
        context['start_date'] = start_date
        context['end_date'] = end_date
        context['show_movement'] = start_date != year.start_date
        context['print_date'] = now().date()
        context['company'] = company
        context['include_cents'] = include_cents
        context['include_zero_accounts'] = include_zero_accounts
        context['report_title'] = self.get_report_title(balance_sheet_filter_options.get('object_items__id__in', []))
        return context


class DownloadTrialBalanceView(LoginRequiredMixin, ProfileMixin, ReportMixin, View):

    def get(self, request, *args, **kwargs):
        company = self.get_company()
        year = self.get_year()
        previous_year = self.get_previous_year(year)
        include_cents = self.can_include_cents()
        income_filter_options, start_date, end_date = self.get_filter_options(company, year, AccountType.INCOME_STATEMENT.value)
        options = {'include_cents': include_cents, 'previous_year_title': 'Whole', 'current_year_filter': 'balance_sheet'}

        income_statement = IncomeStatement(company=company, year=year, start_date=start_date, end_date=end_date,
                                           previous_year=previous_year, **options)
        income_statement.execute()

        balance_sheet_filter_options, start_date, end_date = self.get_filter_options(company, year, AccountType.BALANCE_SHEET.value)
        balance_sheet = BalanceSheet(company=company, year=year, start_date=start_date, end_date=end_date,
                                     income_statement=income_statement, **options)
        balance_sheet.execute()

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="trial_balance.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Trial Balance')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        font_style.alignment = True

        row = ws.row(0)
        row.write(0, str(company), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Trial Balance Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Year', font_style)
        ws.write(row_count, 1, year.start_date.strftime('%d-%M-%Y'), font_style)
        ws.write(row_count, 2, year.end_date.strftime('%d-%M-%Y'), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Trial Balance for', font_style)
        ws.write(row_count, 1, self.get_report_title(balance_sheet_filter_options.get('object_items__id__in', [])))

        row_count += 1
        ws.write(row_count, 0, 'Period dates(s)', font_style)
        ws.write(row_count, 1, start_date.strftime('%d-%M-%Y'))
        ws.write(row_count, 2, end_date.strftime('%d-%M-%Y'))

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        header_row = ws.row(1)
        header_row.write(0, '', font_style)
        header_row.write(1, '', font_style)
        header_row.write(2, 'Ingoing Balance', font_style)
        header_row.write(3, 'Period', font_style)
        header_row.write(4, 'Outgoing Balance', font_style)

        # Sheet body, remaining rows
        row_count += 1
        ws.write(row_count, 0, 'ASSETS', font_style)
        row_count += 1
        for report_type, report_accounts in balance_sheet.assets.items():
            for account, account_line in report_accounts['accounts'].items():
                ws.write(row_count, 0, account.name)
                ws.write(row_count, 1, account.code)
                ws.write(row_count, 2, account_line.get('incoming', 0))
                ws.write(row_count, 3, account_line.get('period_amount', 0))
                ws.write(row_count, 4, account_line.get('outgoing', 0))
                row_count += 1
        ws.write(row_count, 0, 'Total Assets', font_style)
        ws.write(row_count, 1, '')
        ws.write(row_count, 2, balance_sheet.total_incoming_asset, font_style)
        ws.write(row_count, 3, balance_sheet.total_period_asset, font_style)
        ws.write(row_count, 4, balance_sheet.total_outgoing_asset, font_style)
        row_count += 1
        row_count += 1
        ws.write(row_count, 0, 'LIABILITIES AND EQUITY', font_style)
        row_count += 1
        for report_type, liabilities_accounts in balance_sheet.liabilities.items():
            for account, account_line in liabilities_accounts['accounts'].items():
                ws.write(row_count, 0, account.name)
                ws.write(row_count, 1, account.code)
                ws.write(row_count, 2, account_line.get('incoming', 0))
                ws.write(row_count, 3, account_line.get('period_amount', 0))
                ws.write(row_count, 4, account_line.get('outgoing', 0))
                row_count += 1
            row_count += 1
        ws.write(row_count, 0, 'Total Liabilities and Own Capital', font_style)
        ws.write(row_count, 1, '')
        ws.write(row_count, 2, balance_sheet.total_incoming_liabilities, font_style)
        ws.write(row_count, 3, balance_sheet.total_period_liabilities, font_style)
        ws.write(row_count, 4, balance_sheet.total_outgoing_liabilities, font_style)
        row_count += 1
        for lines_type, lines in income_statement.lines.items():
            for report_type, report_lines in lines['report_type_lines'].items():
                for account, income_account_line in report_lines['accounts'].items():
                    row_count += 1
                    ws.write(row_count, 0, account[0])
                    ws.write(row_count, 1, account[1])
                    ws.write(row_count, 2, 0)
                    ws.write(row_count, 3, income_account_line.get('period_amount', 0))
                    ws.write(row_count, 4, income_account_line.get('outgoing_amount', 0))
            if lines_type == 'tax':
                row_count += 1
                ws.write(row_count, 0, 'Total Income Statement', font_style)
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, 0)
                ws.write(row_count, 3, lines.get('total', 0), font_style)
                ws.write(row_count, 4, lines.get('current_year_net_result', 0), font_style)
        row_count += 1
        ws.write(row_count, 0, 'Total', font_style)
        ws.write(row_count, 1, balance_sheet.total_incoming, font_style)
        ws.write(row_count, 2, '')
        ws.write(row_count, 3, balance_sheet.total_period, font_style)
        ws.write(row_count, 4, balance_sheet.total_outgoing, font_style)
        wb.save(response)
        return response


class ForecastingReportView(LoginRequiredMixin, ProfileMixin, ReportMixin, FormView):
    template_name = 'reports/forecasting/index.html'
    form_class = ForecastingReportForm

    def get_context_data(self, **kwargs):
        context = super(ForecastingReportView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context


class GenerateForecastingReportView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'reports/forecasting/print.html'
        return 'reports/forecasting/show.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateForecastingReportView, self).get_context_data(**kwargs)
        form = ForecastingReportForm(data=self.request.GET)
        if form.is_valid():
            year = self.get_year()
            context['report'] = form.execute(year, self.request)
            context['year'] = year
        return context


class CashFlowView(LoginRequiredMixin, ProfileMixin, ReportMixin, FormView):
    template_name = 'reports/cashflow/index.html'
    form_class = CashFlowForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['previous_year'] = self.get_previous_year(self.get_year())
        return kwargs

    def get_accounts(self, company):
        return Account.objects.filter(
            company=company
        ).exclude(code='').exclude(name='')

    def get_context_data(self, **kwargs):
        context = super(CashFlowView, self).get_context_data(**kwargs)
        objects = self.get_company_objects()
        context['company_objects'] = objects.order_by('label')
        context['year'] = self.get_year()
        context['objects_count'] = len(objects)
        return context


class GenerateCashFlowView(LoginRequiredMixin, ProfileMixin, ReportMixin, TemplateView):
    template_name = 'reports/cashflow/show.html'

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'reports/cashflow/print.html'
        return 'reports/cashflow/show.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateCashFlowView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        include_cents = self.can_include_cents()
        cash_flow_form = CashFlowForm(data=self.request.GET, company=company, year=year, previous_year=self.get_previous_year(year))
        if cash_flow_form.is_valid():
            cashflow_statement = cash_flow_form.execute()
            year = cashflow_statement.year
            context['cashflow_statement'] = cashflow_statement.lines
        else:
            context['errors'] = cash_flow_form.errors
        context['show_movement'] = True
        context['start_date'] = year.start_date
        context['end_date'] = year.end_date
        context['print_date'] = now().date()
        context['company'] = company
        context['include_cents'] = include_cents
        context['year'] = year
        return context


class GeneralLedgerView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'reports/ledger/index.html'
    form_class = GeneralLedgerForm

    def get_initial(self):
        initial = super(GeneralLedgerView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_company_objects(self, company):
        return CompanyObject.objects.prefetch_related('object_items').filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(GeneralLedgerView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = self.get_company_objects(company)
        context['company_objects'] = objects.order_by('label')
        context['year'] = self.get_year()
        context['counter'] = len(objects)
        return context


class GenerateGeneralLedgerView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def get_template_names(self):
        is_print = self.request.GET.get('print', 'show')
        if is_print == '1':
            return 'reports/ledger/print.html'
        elif self.request.GET.get('break_by') == 'separate_period':
            return 'reports/ledger/periods.html'
        elif self.request.GET.get('break_by') == 'by_objects':
            return 'reports/ledger/objects.html'
        elif self.request.GET.get('break_by') == 'by_suffix':
            return 'reports/ledger/suffix.html'
        return 'reports/ledger/show.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateGeneralLedgerView, self).get_context_data(**kwargs)
        form = GeneralLedgerForm(data=self.request.GET, year=self.get_year())
        if form.is_valid():
            context['general_ledger'] = form.execute()
        else:
            context['error'] = 'Ledger could not be generated, please try again'
        context['is_print'] = self.request.GET.get('print') == '1'
        context['company'] = self.get_company()
        return context


class DownloadGeneralLedgerView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, **kwargs):
        company = self.get_company()
        year = self.get_year()
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = f'attachment; filename="general_ledger_{str(company.slug)}_{str(year)}.xls"'

        form = GeneralLedgerForm(data=self.request.GET, year=year)
        if form.is_valid():
            general_ledger = form.execute()
            if form.cleaned_data.get('break_by') == 'separate_period':
                return self.format_as_separate_periods(company, general_ledger, response)
            elif form.cleaned_data.get('break_by') == 'by_objects':
                return self.grouped_by_objects(company, general_ledger, response)
            elif form.cleaned_data.get('break_by') == 'by_suffix':
                return self.grouped_by_suffix(company, general_ledger, response)
            else:
                return self.default_layout(company, general_ledger, response)
        else:
            return HttpResponse('Ledger could not be generated, please try again')

    # noinspection PyMethodMayBeStatic
    def default_layout(self, company, general_ledger, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('General Ledger')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(company), font_style)

        row_count += 1
        ws.write(row_count, 0, 'General Ledger Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Year', font_style)
        ws.write(row_count, 1, general_ledger.year.start_date.strftime('%Y-%m-%d'), font_style)
        ws.write(row_count, 2, general_ledger.year.end_date.strftime('%Y-%m-%d'), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Period dates(s)', font_style)
        ws.write(row_count, 1, general_ledger.start_date.strftime('%Y-%m-%d'))
        ws.write(row_count, 2, general_ledger.end_date.strftime('%Y-%m-%d'))

        row_count += 2
        ws.write(row_count, 0, 'Account', font_style)
        ws.write(row_count, 1, 'Name', font_style)
        ws.write(row_count, 2, '', font_style)
        ws.write(row_count, 3, '', font_style)
        ws.write(row_count, 4, '', font_style)
        ws.write(row_count, 5, '', font_style)
        ws.write(row_count, 6, '', font_style)
        ws.write(row_count, 7, 'Ingoing Balance', font_style)
        ws.write(row_count, 8, 'Period', font_style)
        ws.write(row_count, 9, 'Outgoing Balance', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Period', font_style)
        ws.write(row_count, 1, 'Date', font_style)
        ws.write(row_count, 2, 'Module', font_style)
        ws.write(row_count, 3, 'JNL/Nr', font_style)
        ws.write(row_count, 4, 'Customer/Supplier', font_style)
        ws.write(row_count, 5, 'Journal Text', font_style)
        ws.write(row_count, 6, 'Text', font_style)
        ws.write(row_count, 7, 'Debit', font_style)
        ws.write(row_count, 8, 'Credit', font_style)
        ws.write(row_count, 9, 'Balance', font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        row_count += 1
        for account, account_lines in general_ledger.journal_lines.items():
            row_count += 1
            ws.write(row_count, 0, account[1])
            ws.write(row_count, 1, account[2])

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Incoming Balance', font_style)
            ws.write(row_count, 7, '')
            ws.write(row_count, 8, '')
            ws.write(row_count, 9, account_lines.get('incoming_balance', 0), font_style)

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Initial balance')
            ws.write(row_count, 7, account_lines.get('initial_debit', 0))
            ws.write(row_count, 8, account_lines.get('initial_credit', 0))
            ws.write(row_count, 9, account_lines.get('initial_balance', 0))

            for line_id, journal_line in account_lines.get('lines', {}).items():
                row_count += 1
                ws.write(row_count, 0, journal_line['line']['journal__period__period'])
                ws.write(row_count, 1, journal_line['line']['accounting_date'].strftime('%Y-%m-%d'))
                ws.write(row_count, 2, journal_line['module'])
                ws.write(row_count, 3, journal_line['line']['journal__journal_number'])
                ws.write(row_count, 4, journal_line['line']['ledger_type_reference'])
                ws.write(row_count, 5, journal_line['line']['journal__journal_text'])
                ws.write(row_count, 6, journal_line['line']['text'])
                ws.write(row_count, 7, journal_line['debit'])
                ws.write(row_count, 8, journal_line['credit'])
                ws.write(row_count, 9, journal_line['balance'])

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Movement for period', font_style)
            ws.write(row_count, 7, account_lines.get('movement_debit', 0), font_style)
            ws.write(row_count, 8, account_lines.get('movement_credit', 0), font_style)
            ws.write(row_count, 9, '')

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Balance Carry Forward', font_style)
            ws.write(row_count, 7, account_lines.get('account_debit', 0), font_style)
            ws.write(row_count, 8, account_lines.get('account_credit', 0), font_style)
            ws.write(row_count, 9, account_lines.get('outstanding', 0))

            row_count += 1
            ws.write(row_count, 0, '')
        wb.save(response)
        return response

    def format_as_separate_periods(self, company, general_ledger, response):
        pass

    def grouped_by_objects(self, company, general_ledger, response):
        pass

    # noinspection PyMethodMayBeStatic
    def grouped_by_suffix(self, company, general_ledger, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('General Ledger')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(company), font_style)

        row_count += 1
        ws.write(row_count, 0, 'General Ledger Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Year', font_style)
        ws.write(row_count, 1, general_ledger.year.start_date.strftime('%Y-%m-%d'), font_style)
        ws.write(row_count, 2, general_ledger.year.end_date.strftime('%Y-%m-%d'), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Period dates(s)', font_style)
        ws.write(row_count, 1, general_ledger.start_date.strftime('%Y-%m-%d'))
        ws.write(row_count, 2, general_ledger.end_date.strftime('%Y-%m-%d'))

        row_count += 2
        ws.write(row_count, 0, 'Suffix', font_style)
        ws.write(row_count, 1, 'Total', font_style)

        # Sheet body, remaining rows

        row_count += 1
        for suffix, amount in general_ledger.journal_lines.items():
            row_count += 1
            suffix = suffix if suffix else '(blank)'
            ws.write(row_count, 0, suffix)
            ws.write(row_count, 1, amount)

        row_count += 1
        ws.write(row_count, 0, 'Total')
        ws.write(row_count, 1, general_ledger.total)
        wb.save(response)
        return response


class SuffixReportView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'reports/suffix/index.html'
    form_class = SuffixReportForm

    def get_initial(self):
        initial = super(SuffixReportView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_company_objects(self, company):
        return CompanyObject.objects.prefetch_related('object_items').filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(SuffixReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = self.get_company_objects(company)
        context['company_objects'] = objects.order_by('label')
        context['year'] = self.get_year()
        context['counter'] = len(objects)
        return context


class GenerateSuffixReportView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def get_template_names(self):
        is_print = self.request.GET.get('print', 'show')
        if is_print == '1':
            return 'reports/suffix/print.html'
        return 'reports/suffix/show.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateSuffixReportView, self).get_context_data(**kwargs)
        form = SuffixReportForm(data=self.request.GET, year=self.get_year())
        if form.is_valid():
            context['report'] = form.execute()
        else:
            context['error'] = 'Report could not be generated, please try again'
        context['is_print'] = self.request.GET.get('print') == '1'
        context['company'] = self.get_company()
        return context


class DownloadSuffixLedgerView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, **kwargs):
        company = self.get_company()
        year = self.get_year()
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = f'attachment; filename="general_ledger_{str(company.slug)}_{str(year)}.xls"'

        form = GeneralLedgerForm(data=self.request.GET, year=year)
        if form.is_valid():
            general_ledger = form.execute()
            if form.cleaned_data.get('break_by') == 'separate_period':
                return self.format_as_separate_periods(company, general_ledger, response)
            elif form.cleaned_data.get('break_by') == 'by_objects':
                return self.grouped_by_objects(company, general_ledger, response)
            elif form.cleaned_data.get('break_by') == 'by_suffix':
                return self.grouped_by_suffix(company, general_ledger, response)
            else:
                return self.default_layout(company, general_ledger, response)
        else:
            return HttpResponse('Ledger could not be generated, please try again')

    # noinspection PyMethodMayBeStatic
    def default_layout(self, company, general_ledger, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('General Ledger')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(company), font_style)

        row_count += 1
        ws.write(row_count, 0, 'General Ledger Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Year', font_style)
        ws.write(row_count, 1, general_ledger.year.start_date.strftime('%Y-%m-%d'), font_style)
        ws.write(row_count, 2, general_ledger.year.end_date.strftime('%Y-%m-%d'), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Period dates(s)', font_style)
        ws.write(row_count, 1, general_ledger.start_date.strftime('%Y-%m-%d'))
        ws.write(row_count, 2, general_ledger.end_date.strftime('%Y-%m-%d'))

        row_count += 2
        ws.write(row_count, 0, 'Account', font_style)
        ws.write(row_count, 1, 'Name', font_style)
        ws.write(row_count, 2, '', font_style)
        ws.write(row_count, 3, '', font_style)
        ws.write(row_count, 4, '', font_style)
        ws.write(row_count, 5, '', font_style)
        ws.write(row_count, 6, '', font_style)
        ws.write(row_count, 7, 'Ingoing Balance', font_style)
        ws.write(row_count, 8, 'Period', font_style)
        ws.write(row_count, 9, 'Outgoing Balance', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Period', font_style)
        ws.write(row_count, 1, 'Date', font_style)
        ws.write(row_count, 2, 'Module', font_style)
        ws.write(row_count, 3, 'JNL/Nr', font_style)
        ws.write(row_count, 4, 'Customer/Supplier', font_style)
        ws.write(row_count, 5, 'Journal Text', font_style)
        ws.write(row_count, 6, 'Text', font_style)
        ws.write(row_count, 7, 'Debit', font_style)
        ws.write(row_count, 8, 'Credit', font_style)
        ws.write(row_count, 9, 'Balance', font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        row_count += 1
        for account, account_lines in general_ledger.journal_lines.items():
            row_count += 1
            ws.write(row_count, 0, account[1])
            ws.write(row_count, 1, account[2])

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Incoming Balance', font_style)
            ws.write(row_count, 7, '')
            ws.write(row_count, 8, '')
            ws.write(row_count, 9, account_lines.get('incoming_balance', 0), font_style)

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Initial balance')
            ws.write(row_count, 7, account_lines.get('initial_debit', 0))
            ws.write(row_count, 8, account_lines.get('initial_credit', 0))
            ws.write(row_count, 9, account_lines.get('initial_balance', 0))

            for line_id, journal_line in account_lines.get('lines', {}).items():
                row_count += 1
                ws.write(row_count, 0, journal_line['line']['journal__period__period'])
                ws.write(row_count, 1, journal_line['line']['accounting_date'].strftime('%Y-%m-%d'))
                ws.write(row_count, 2, journal_line['module'])
                ws.write(row_count, 3, journal_line['line']['journal__journal_number'])
                ws.write(row_count, 4, journal_line['line']['ledger_type_reference'])
                ws.write(row_count, 5, journal_line['line']['journal__journal_text'])
                ws.write(row_count, 6, journal_line['line']['text'])
                ws.write(row_count, 7, journal_line['debit'])
                ws.write(row_count, 8, journal_line['credit'])
                ws.write(row_count, 9, journal_line['balance'])

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Movement for period', font_style)
            ws.write(row_count, 7, account_lines.get('movement_debit', 0), font_style)
            ws.write(row_count, 8, account_lines.get('movement_credit', 0), font_style)
            ws.write(row_count, 9, '')

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, 'Balance Carry Forward', font_style)
            ws.write(row_count, 7, account_lines.get('account_debit', 0), font_style)
            ws.write(row_count, 8, account_lines.get('account_credit', 0), font_style)
            ws.write(row_count, 9, account_lines.get('outstanding', 0))

            row_count += 1
            ws.write(row_count, 0, '')
        wb.save(response)
        return response

    def format_as_separate_periods(self, company, general_ledger, response):
        pass

    def grouped_by_objects(self, company, general_ledger, response):
        pass

    # noinspection PyMethodMayBeStatic
    def grouped_by_suffix(self, company, general_ledger, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('General Ledger')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(company), font_style)

        row_count += 1
        ws.write(row_count, 0, 'General Ledger Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Year', font_style)
        ws.write(row_count, 1, general_ledger.year.start_date.strftime('%Y-%m-%d'), font_style)
        ws.write(row_count, 2, general_ledger.year.end_date.strftime('%Y-%m-%d'), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Period dates(s)', font_style)
        ws.write(row_count, 1, general_ledger.start_date.strftime('%Y-%m-%d'))
        ws.write(row_count, 2, general_ledger.end_date.strftime('%Y-%m-%d'))

        row_count += 2
        ws.write(row_count, 0, 'Suffix', font_style)
        ws.write(row_count, 1, 'Total', font_style)

        # Sheet body, remaining rows

        row_count += 1
        for suffix, amount in general_ledger.journal_lines.items():
            row_count += 1
            suffix = suffix if suffix else '(blank)'
            ws.write(row_count, 0, suffix)
            ws.write(row_count, 1, amount)

        row_count += 1
        ws.write(row_count, 0, 'Total')
        ws.write(row_count, 1, general_ledger.total)
        wb.save(response)
        return response


class InventoryValuationView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'reports/valuation/index.html'
    form_class = InventoryValuationForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_inventories(self, branch):
        return BranchInventory.objects.select_related('inventory').prefetch_related(
            'group_level_items', 'group_level_items__group_level'
        ).filter(branch=branch)

    def get_group_levels(self, branch):
        return GroupLevel.objects.eligible_parents(branch.company)

    def get_context_data(self, **kwargs):
        context = super(InventoryValuationView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        context['branch'] = branch
        context['group_levels'] = self.get_group_levels(branch)
        context['from_inventories'] = self.get_inventories(branch).order_by('inventory__code')
        context['to_inventories'] = self.get_inventories(branch).order_by('-inventory__code')
        return context


class GenerateInventoryValuationView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'reports/valuation/no_breaking.html'

    def get_template_names(self):
        # group_levels = self.report_group_levels()
        break_by = self.request.GET.get('break_by')
        if break_by == 'no_breaking':
            return 'reports/valuation/no_breaking.html'
        else:
            if self.request.GET.get('print') == '1':
                return f'reports/valuation/print_{break_by}.html'
            return f'reports/valuation/break_by_{break_by}.html'

    def get_selected_branch(self):
        branch_id = self.request.GET.get('branch_id', None)
        if branch_id == 'all':
            return None
        if branch_id:
            return Branch.objects.filter(id=branch_id).first()
        return self.get_branch()

    def get_context_data(self, **kwargs):
        context = super(GenerateInventoryValuationView, self).get_context_data(**kwargs)
        form = InventoryValuationForm(data=self.request.GET, year=self.get_year(), company=self.get_company(),
                                      branch=self.get_branch())
        if form.is_valid():
            context['report'] = form.execute()
        else:
            context['errors'] = form.errors  # 'Valuation report could not be generated, please try again'
        return context


class DownloadInventoryValuationView(LoginRequiredMixin, ProfileMixin, View):

    def get_selected_branch(self):
        branch_id = self.request.GET.get('branch_id', None)
        if branch_id == 'all':
            return None
        if branch_id:
            return Branch.objects.filter(id=branch_id).first()
        return self.get_branch()

    def get(self, request, **kwargs):
        branch = self.get_selected_branch()
        year = self.get_year()
        report = InventoryValuation(
            year=year,
            company=self.get_company(),
            branch=branch,
            from_inventory=self.request.GET.get('from_inventory'),
            to_inventory=self.request.GET.get('to_inventory'),
            break_by=self.request.GET.get('break_by')
        )
        valuation_report = report.execute()

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = f'attachment; filename="inventory_valuation_{str(branch)}_{str(year)}.xls"'

        break_by = self.request.GET.get('break_by')
        if break_by == 'no_breaking':
            return self.download_with_no_breaking(branch, valuation_report, response)
        elif break_by == 'group_levels':
            return self.download_break_by_levels(branch, valuation_report, response)
        elif break_by == 'children_totals':
            return self.download_break_by_children_totals(branch, valuation_report, response)
        elif break_by == 'parent_totals':
            return self.download_break_by_parent_totals(branch, valuation_report, response)
        elif break_by == 'parent_no_totals':
            return self.download_break_by_parent_no_totals(branch, valuation_report, response)
        elif break_by == 'parent_and_children_totals':
            return self.download_break_by_parent_and_children_totals(branch, valuation_report, response)

    def download_with_no_breaking(self, branch, valuation_report, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Inventory Valuation')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(branch), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Inventory Valuation Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 2
        ws.write(row_count, 0, 'SKU', font_style)
        ws.write(row_count, 1, 'Name', font_style)
        ws.write(row_count, 2, 'Date', font_style)
        ws.write(row_count, 3, 'Quantity', font_style)
        ws.write(row_count, 4, 'Valuation', font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        row_count += 1
        for inventory, breakdown in valuation_report['inventories'].items():
            row_count += 1
            ws.write(row_count, 0, str(inventory))
            ws.write(row_count, 1, str(inventory.description))
            ws.write(row_count, 2, now().strftime('%d-%M-%Y'))
            ws.write(row_count, 3, inventory.history.in_stock)
            ws.write(row_count, 4, inventory.history.total_value)

        row_count += 1
        ws.write(row_count, 0, '')
        ws.write(row_count, 1, '')
        ws.write(row_count, 2, 'Totals')
        ws.write(row_count, 3, valuation_report.get('total_quantity', 0))
        ws.write(row_count, 4, valuation_report.get('total', 0))

        wb.save(response)
        return response

    def download_break_by_levels(self, branch, valuation_report, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Inventory Valuation')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(branch), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Inventory Valuation Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 2
        ws.write(row_count, 0, 'SKU', font_style)
        ws.write(row_count, 1, 'SKU', font_style)
        ws.write(row_count, 2, 'SKU', font_style)
        ws.write(row_count, 3, 'Name', font_style)
        ws.write(row_count, 4, 'Date', font_style)
        ws.write(row_count, 5, 'Quantity', font_style)
        ws.write(row_count, 6, 'Valuation', font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        row_count += 1
        for level, level_report in valuation_report['levels'].items():
            row_count += 1
            ws.write(row_count, 0, str(level))
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, '')
            ws.write(row_count, 6, '')

            for level_item, level_item_report in level_report['level_items'].items():
                row_count += 1
                ws.write(row_count, 0, '')
                ws.write(row_count, 1, str(level_item))
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                ws.write(row_count, 5, '')
                ws.write(row_count, 6, '')

                for inventory, ledgers in level_item_report['inventory_items'].items():
                    row_count += 1
                    ws.write(row_count, 0, '')
                    ws.write(row_count, 1, '')
                    ws.write(row_count, 2, str(inventory))
                    ws.write(row_count, 3, str(inventory.description))
                    ws.write(row_count, 4, now().strftime('%d-%M-%Y'))
                    ws.write(row_count, 5, inventory.history.in_stock)
                    ws.write(row_count, 6, inventory.history.total_value)
                row_count += 1
                ws.write(row_count, 0, '')
                ws.write(row_count, 1, f'{str(level_item)} Totals')
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                ws.write(row_count, 5, level_item_report.get('total_quantity', 0))
                ws.write(row_count, 6, level_item_report.get('total', 0))
            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, f'{str(level)} Totals')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, level_report.get('total_quantity', 0))
            ws.write(row_count, 6, level_report.get('total', 0))
        row_count += 1
        ws.write(row_count, 0, '')
        ws.write(row_count, 1, '')
        ws.write(row_count, 2, 'Totals')
        ws.write(row_count, 3, valuation_report.get('total_quantity', 0))
        ws.write(row_count, 4, valuation_report.get('total', 0))

        wb.save(response)
        return response

    def download_break_by_parent_and_children_totals(self, branch, valuation_report, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Inventory Valuation')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(branch), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Inventory Valuation Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 2
        ws.write(row_count, 0, 'Group Level', font_style)
        ws.write(row_count, 1, '', font_style)
        ws.write(row_count, 2, 'Quantity', font_style)
        ws.write(row_count, 3, 'Valuation', font_style)

        # Sheet body, remaining rows
        row_count += 1
        for level, level_report in valuation_report['levels'].items():
            row_count += 1
            ws.write(row_count, 0, str(level))
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            for level_item, level_item_report in level_report['level_items'].items():
                row_count += 1
                ws.write(row_count, 0, '')
                ws.write(row_count, 1, str(level_item))
                ws.write(row_count, 2, level_item_report.get('total_quantity', 0))
                ws.write(row_count, 3, level_item_report.get('total', 0))
        row_count += 1
        ws.write(row_count, 0, '')
        ws.write(row_count, 1, 'Totals')
        ws.write(row_count, 2, valuation_report.get('total_quantity', 0))
        ws.write(row_count, 3, valuation_report.get('total', 0))

        wb.save(response)
        return response

    def download_break_by_parent_no_totals(self, branch, valuation_report, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Inventory Valuation')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(branch), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Inventory Valuation Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 2
        ws.write(row_count, 0, 'Group Level', font_style)
        ws.write(row_count, 1, 'Name', font_style)
        ws.write(row_count, 2, 'Quantity', font_style)
        ws.write(row_count, 3, 'Valuation', font_style)

        # Sheet body, remaining rows
        row_count += 1
        for level, level_report in valuation_report['levels'].items():
            row_count += 1
            ws.write(row_count, 0, str(level))
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            for level_item, level_item_report in level_report['level_items'].items():
                row_count += 1
                ws.write(row_count, 0, '')
                ws.write(row_count, 1, str(level_item))
                ws.write(row_count, 2, level_item_report.get('total_quantity', 0))
                ws.write(row_count, 3, level_item_report.get('total', 0))
        row_count += 1
        ws.write(row_count, 0, '')
        ws.write(row_count, 1, 'Totals')
        ws.write(row_count, 2, valuation_report.get('total_quantity', 0))
        ws.write(row_count, 3, valuation_report.get('total', 0))

        wb.save(response)
        return response

    def download_break_by_parent_totals(self, branch, valuation_report, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Inventory Valuation')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(branch), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Inventory Valuation Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 2
        ws.write(row_count, 0, 'Group Level', font_style)
        ws.write(row_count, 1, 'Valuation', font_style)

        # Sheet body, remaining rows
        row_count += 1
        for level, level_report in valuation_report['levels'].items():
            row_count += 1
            ws.write(row_count, 0, str(level))
            ws.write(row_count, 1, level_report.get('total', 0))
        row_count += 1
        ws.write(row_count, 0, 'Totals')
        ws.write(row_count, 1, valuation_report.get('total', 0))

        wb.save(response)
        return response

    def download_break_by_children_totals(self, branch, valuation_report, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Inventory Valuation')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(branch), font_style)

        row_count += 1
        ws.write(row_count, 0, 'Inventory Valuation Report', font_style)

        row_count += 1
        ws.write(row_count, 0, 'Preliminary', font_style)

        row_count += 2
        ws.write(row_count, 0, 'Name', font_style)
        ws.write(row_count, 1, 'Quantity', font_style)
        ws.write(row_count, 2, 'Valuation', font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        row_count += 1
        for level, level_report in valuation_report['levels'].items():
            for level_item, level_item_report in level_report['level_items'].items():
                row_count += 1
                ws.write(row_count, 0, str(level_item))
                ws.write(row_count, 1, level_item_report.get('total_quantity', 0))
                ws.write(row_count, 2, level_item_report.get('total', 0))
        row_count += 1
        ws.write(row_count, 0, 'Totals')
        ws.write(row_count, 1, valuation_report.get('total_quantity', 0))
        ws.write(row_count, 2, valuation_report.get('total', 0))

        wb.save(response)
        return response


class AgeVsLedgerView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'reports/age_vs_ledger.html'

    def get_ledger_lines(self, company: Company, year: Year):
        journal_lines = JournalLine.objects.annotate(
            period_total=Coalesce(F('debit'), 0) + Coalesce(F('credit'), 0) * -1
        ).filter(
            account__sub_ledger=SubLedgerModule.SUPPLIER, journal__company=company,
            journal__year=year, journal__date__gte=year.start_date, journal__date__lte=year.end_date
        ).order_by('journal__date').all()
        opening_balances = OpeningBalance.objects.get_for_sub_ledger(
            year=year, company=company, sub_ledger=SubLedgerModule.SUPPLIER
        ).all()
        opening_total = sum(opening_balance.amount for opening_balance in opening_balances if opening_balance.amount)
        journal_total = 0
        lines = {}
        for journal_line in journal_lines:
            lines[journal_line.id] = {
                'text': journal_line.text if journal_line.text else journal_line.journal.journal_text,
                'id': journal_line.id,
                'date': journal_line.accounting_date.strftime('%d %m %Y'),
                'amount': str(journal_line.period_total),
                'miss_match': False
            }
            journal_total += journal_line.period_total if journal_line.period_total else 0
        # with open('balancing.json', 'w') as fp:
        #     json.dump(lines, fp)

        missing_data = {}
        balancing_ids = []
        balancing = {}
        # with open('balancing.json') as file:
        #     balancing = json.load(file)
        #     for b_id, row in balancing.items():
        #         if row['id'] not in lines:
        #             missing_data[b_id] = row
        #         balancing_ids.append(int(b_id))

        for line_id, line in lines.items():
            if line_id not in balancing_ids:
                lines[line_id]['miss_match'] = True

        ledger_total = opening_total + journal_total
        lines = [line for amt, line in lines.items()]
        balancing_total = opening_total + sum(Decimal(bdata['amount']) for b_amount, bdata in balancing.items())

        return ledger_total, opening_balances, lines, balancing, balancing_total

    def get_age_lines(self, company: Company, year: Year):
        period = year.last_period_open
        age_analysis = AgeAnalysis.objects.filter(period=period).order_by('balance').all()
        total = sum(age.balance for age in age_analysis if age.balance)
        return total, age_analysis

    def get_context_data(self, **kwargs):
        context = super(AgeVsLedgerView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        age_total, age_lines = self.get_age_lines(company=company, year=year)
        ledger_total, opening_balances, ledger_lines, balancing_data, balancing_total = self.get_ledger_lines(company=company, year=year)

        context['balancing_data'] = balancing_data
        context['balancing_total'] = balancing_total
        context['ledger_lines'] = ledger_lines
        context['opening_balances'] = opening_balances
        context['ledger_total'] = ledger_total
        context['age_lines'] = age_lines
        context['age_total'] = age_total
        return context


class CurrentAssetsView(ProfileMixin, TemplateView):
    template_name = 'reports/current_assets.html'

    # noinspection PyMethodMayBeStatic
    def get_customer_control(self, company: Company, year: Year):
        return get_total_for_account(
            year=year,
            company=company,
            account=company.default_customer_account,
            start_date=year.start_date,
            end_date=year.end_date
        )

    # noinspection PyMethodMayBeStatic
    def get_input_vat(self, company: Company, year: Year):
        return get_total_for_vat_type(
            year=year,
            company=company,
            module=Module.PURCHASE,
            start_date=year.start_date,
            end_date=year.end_date
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        company = self.get_company()

        context.update({
            'customer_control': self.get_customer_control(company=company, year=year),
            'pre_paid_expenses': 0,
            'input_vat': self.get_input_vat(company=company, year=year)
        })
        return context


class CashReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'reports/cash_report.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        report_type = ReportType.objects.get(slug='cash-resources')
        year = self.get_year()
        cash_resources = get_total_report_type(
            year=year,
            company=self.get_company(),
            report_type=report_type,
            start_date=year.start_date,
            end_date=year.end_date
        )

        context.update({'amount': cash_resources,})
        return context


class CurrentLiabilitiesView(ProfileMixin, TemplateView):
    template_name = 'reports/current_liabilities.html'

    # noinspection PyMethodMayBeStatic
    def get_df_suppliers_control_total(self, company: Company, year: Year):
        return get_total_for_sub_ledger(
            year=year,
            company=company,
            sub_ledger=SubLedgerModule.SUPPLIER,
            start_date=year.start_date,
            end_date=year.end_date
        )

    # noinspection PyMethodMayBeStatic
    def get_df_invoice_not_yet_posted_total(self, company: Company, year: Year):
        return get_total_for_account(
            year=year,
            company=company,
            account=company.default_expenditure_account,
            start_date=year.start_date,
            end_date=year.end_date
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        company = self.get_company()

        context.update({
            'provision_for_leave': 0,
            'df_control_suppliers': self.get_df_suppliers_control_total(company=company, year=year),
            'vat_return_submitted': 0,
            'df_control_invoices_not_yet_posted': self.get_df_invoice_not_yet_posted_total(company=company, year=year)
        })
        return context


class TurnoverVsBudgetSummary(ProfileMixin, TemplateView):
    template_name = 'reports/turnover_vs_budget.html'

    # noinspection PyMethodMayBeStatic
    def get_turnover_total(self, company: Company, year: Year, report_type: ReportType):
        return get_total_report_type(
            year=year,
            company=company,
            report_type=report_type,
            start_date=year.start_date,
            end_date=year.end_date
        )

    # noinspection PyMethodMayBeStatic
    def get_budget_total(self, company: Company, year: Year, report_type: ReportType):
        budget = BudgetAccount.objects.values('account__report').filter(
            budget__year=year, account__company=year.company, account__report=report_type
        ).aggregate(total=Sum('period_amounts__amount'))
        amount = Decimal('0')
        if budget and budget['total']:
            amount = budget['total']
        return amount

    def get_context_data(self, **kwargs):
        context = super(TurnoverVsBudgetSummary, self).get_context_data(**kwargs)
        year = self.get_year()
        company = self.get_company()
        report_type = ReportType.objects.get(slug='turnover')

        turnover = self.get_turnover_total(year=year, company=company, report_type=report_type)
        budget = self.get_budget_total(year=year, company=company, report_type=report_type)

        achievement = Decimal('0')
        if (turnover and budget) and budget != 0:
            achievement = (turnover / budget) * 100

        context.update({
            'turnover': turnover,
            'budget': budget,
            'achievement': achievement,
        })
        return context


class MonthlyTurnoverDataView(ProfileMixin, View):

    def get_monthly_turnover(self, year, report_type, periods):

        data = []

        journal_lines = JournalLine.objects.select_related('journal', 'journal__period').filter(
            account__report=report_type, journal__year=year
        ).order_by('journal__period__period')

        turnover_data = {}
        for journal_line in journal_lines:
            total = round(float(journal_line.amount)) if journal_line.amount else 0
            period = journal_line.journal.period
            period_id = str(period.id)
            if period_id in turnover_data:
                turnover_data[period_id] += total
            else:
                turnover_data[period_id] = total

        for period in periods:
            amount = turnover_data.get(str(period.id), 0)
            data.append(amount * -1)
        return data

    def get_monthly_budget(self, year, report_type, periods):

        data = []

        budget_lines = BudgetAccountPeriod.objects.select_related(
            'budget_account', 'budget_account__account', 'period'
        ).filter(
            budget_account__account__report=report_type, budget_account__budget__year=year
        ).order_by('period__period')

        budget_data = {}
        for budget_line in budget_lines:
            total = round(float(budget_line.amount)) if budget_line.amount else 0
            period = budget_line.period
            period_id = str(period.id)
            if period_id in budget_data:
                budget_data[period_id] += total
            else:
                budget_data[period_id] = total

        for period in periods:
            amount = budget_data.get(str(period.id), 0)
            data.append(amount * -1)
        return data

    def get(self, request, *args, **kwargs):
        report_type = ReportType.objects.get(slug='turnover')
        year = self.get_year()
        periods = Period.objects.filter(period_year=year).order_by('period').all()

        turnover_report = self.get_monthly_turnover(year=year, report_type=report_type, periods=periods)
        budget_report = self.get_monthly_budget(year=year, report_type=report_type, periods=periods)

        return JsonResponse({
            'turnover': turnover_report,
            'budget': budget_report,
            'periods': [period.name for period in periods]
        }, safe=False)
