import logging
from collections import defaultdict
from decimal import Decimal

from django import forms

from docuflow.apps.company.models import Account, Branch
from docuflow.apps.inventory.models import Inventory
from docuflow.apps.invoice.models import Invoice, Payment
from docuflow.apps.journals.models import OpeningBalance
from docuflow.apps.period.models import Period
from docuflow.apps.reports.cashflowstatement import CashFlowStatement
from docuflow.apps.reports.generalledger import GeneralLedger, SuffixLedger
from .balancesheet import BalanceSheet
from .incomestatement import IncomeStatement
from .inventoryvaluation import InventoryValuation

logger = logging.getLogger(__name__)


class BaseReportForm(forms.Form):
    by_objects = forms.BooleanField(required=False, label='Break by objects')
    separate_periods = forms.BooleanField(required=False, label=' Each period separate')
    show_detail = forms.BooleanField(required=False, label='Show Details of edited line')
    show_detail_module = forms.BooleanField(required=False, label=' Show Details of module journals')
    is_potrait = forms.BooleanField(required=False, label='Potrait')
    is_landscape = forms.BooleanField(required=False, label='Landscape')


class BalanceSheetForm(BaseReportForm):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.previous_year = kwargs.pop('previous_year')
        self.company = kwargs.pop('company')
        super(BalanceSheetForm, self).__init__(*args, **kwargs)

    from_date = forms.DateField(required=False, label='From Date')
    to_date = forms.DateField(required=False, label='To Date')
    include_cents = forms.BooleanField(required=False)

    def execute(self):
        options = {'previous_year_title': 'accumulated', 'current_year_filter': 'balance_sheet'}
        from_date = self.cleaned_data['from_date']
        to_date = self.cleaned_data['to_date']
        include_cents = self.cleaned_data['include_cents']

        income_statement = IncomeStatement(
            year=self.year,
            company=self.company,
            start_date=from_date,
            end_date=to_date,
            previous_year=self.previous_year,
            include_cents=include_cents,
            **options
        )
        income_statement.execute()

        balance_sheet = BalanceSheet(
            company=self.company,
            year=self.year,
            start_date=from_date,
            end_date=to_date,
            income_statement=income_statement,
            include_cents=include_cents,
            **options
        )
        balance_sheet.execute()

        return balance_sheet


class IncomeStatementReportForm(BaseReportForm):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.previous_year = kwargs.pop('previous_year')
        self.company = kwargs.pop('company')
        super(IncomeStatementReportForm, self).__init__(*args, **kwargs)

    from_date = forms.DateField(required=False, label='From Date')
    to_date = forms.DateField(required=False, label='To Date')
    include_cents = forms.BooleanField(required=False)
    object_item = forms.CharField(required=False)
    columns = forms.ChoiceField(choices=(
        ('period', 'Period'), ('previous_year', 'Previous Year'), ('budget', 'Budget'),
        ('accumulated', 'Accumulated'), ('budget_variance', 'Variance to Budget')
    ), required=False)
    previous_year_title = forms.ChoiceField(choices=(
        ('Whole', 'Whole'), ('Accumulated', 'Accumulated'), ('Period', 'Period')
    ), required=False)
    budget_title = forms.ChoiceField(choices=(
        ('Whole', 'Whole'), ('Accumulated', 'Accumulated'), ('Period', 'Period')
    ), required=False)

    def execute(self, columns):
        previous_year_title = self.cleaned_data.get('previous_year_title', 'Whole')
        budget_title = self.cleaned_data.get('budget_title', 'Whole')
        include_previous_year = False
        if 'previous_year' in columns:
            include_previous_year = True
        options = {'previous_year_title': previous_year_title,
                   'budget_title': budget_title}
        income_statement = IncomeStatement(year=self.year, company=self.company, start_date=self.cleaned_data['from_date'],
                                           end_date=self.cleaned_data['to_date'], previous_year=self.previous_year,
                                           include_cents=self.cleaned_data['include_cents'], include_previous_year=include_previous_year,
                                           **options)
        income_statement.execute()

        return income_statement


class PeriodIncomeStatementReportSearchForm(BaseReportForm):

    from_period = forms.ChoiceField(required=False, label='From Date')
    to_period = forms.DateField(required=False, label='To Date')


class ForecastingReportForm(forms.Form):

    is_processing = forms.BooleanField(required=False, label='Processing')
    is_completed = forms.BooleanField(required=False, label='Completed')

    def get_invoices(self, year):
        open_invoices = Invoice.objects.day_forecasting(
            year=year,
            is_completed=self.cleaned_data['is_completed'],
            is_processing=self.cleaned_data['is_processing']
        )

        invoices_total = sum(invoice['total_open'] for invoice in open_invoices)

        return {'invoices_total': invoices_total, 'invoices': open_invoices}

    def get_payments(self, year):
        payments = Payment.objects.due_for_payment_by_supplier(
            company=year.company,
            end_date=year.end_date
        )

        supplier_payments = defaultdict(Decimal)
        total_payments = 0

        for payment in payments:
            supplier = f"{payment['supplier__code']} - {payment['supplier__name']}"
            supplier_payments[supplier] += payment['unpaid_amount']
            total_payments += payment['unpaid_amount']

        return {'payments_total': total_payments, 'suppliers': dict(supplier_payments)}

    def execute(self, year, request):
        report = dict()
        report.update(**self.get_invoices(year))
        report.update(**self.get_payments(year))
        return report


class CashFlowForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.previous_year = kwargs.pop('previous_year')
        super(CashFlowForm, self).__init__(*args, **kwargs)
        self.fields['from_period'].queryset = Period.objects.filter(company=self.company, period_year=self.year).order_by('period')
        self.fields['from_period'].empty_label = None
        self.fields['to_period'].queryset = Period.objects.filter(company=self.company, period_year=self.year).order_by('-period')
        self.fields['to_period'].empty_label = None

    from_period = forms.ModelChoiceField(required=False, queryset=None)
    to_period = forms.ModelChoiceField(required=False, queryset=None)
    include_cents = forms.BooleanField(required=False)
    objects = forms.ChoiceField(required=False)

    def clean(self):
        if Account.objects.filter(cash_flow__isnull=True, is_active=True, company=self.company).exists():
            raise forms.ValidationError('Cash flow cannot be generated as not all accounts have been categorised for '
                                        'the cash flow')

    def execute(self):
        cashflow_statement = CashFlowStatement(
            year=self.year,
            from_period=self.cleaned_data['from_period'],
            to_period=self.cleaned_data['to_period'],
            previous_year=self.previous_year,
            include_cents=self.cleaned_data['include_cents']
        )
        cashflow_statement.execute()
        return cashflow_statement


class GeneralLedgerForm(forms.Form):

    NONE = 'none'
    BY_OBJECTS = 'by_objects'
    BY_SUFFIX = 'by_suffix'
    SEPARATE_PERIOD = 'separate_period'

    BREAKING_CHOICES = (
        (NONE, 'None'),
        (BY_OBJECTS, 'Break by objects'),
        (BY_SUFFIX, 'Suffix'),
        (SEPARATE_PERIOD, 'Each period separate')
    )

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        super().__init__(*args, **kwargs)

        self.fields['from_account'].queryset = Account.objects.filter(company=self.year.company).exclude(
            code='', name='').order_by('code')
        self.fields['to_account'].queryset = Account.objects.filter(company=self.year.company).exclude(
            code='', name='').order_by('-code')

    from_date = forms.DateField(required=False, label='From Date')
    to_date = forms.DateField(required=False, label='To Date')
    break_by = forms.ChoiceField(widget=forms.RadioSelect(), required=False, choices=BREAKING_CHOICES)
    show_detail = forms.BooleanField(required=False, label='Show Details of edited line')
    show_detail_module = forms.BooleanField(required=False, label=' Show Details of module journals')
    is_potrait = forms.BooleanField(required=False, label='Potrait')
    is_landscape = forms.BooleanField(required=False, label='Landscape')
    from_account = forms.ModelChoiceField(queryset=None, label='From Account', empty_label=None, to_field_name='code')
    to_account = forms.ModelChoiceField(queryset=None, label='To Account', empty_label=None, to_field_name='code')

    def get_filter_options(self, request):
        filter_options = {'journal__company': self.year.company, 'journal__year': self.year}
        from_account = self.cleaned_data.get('from_account', None)
        to_account = self.cleaned_data.get('to_account', None)
        to_date = self.cleaned_data.get('to_date', None)

        if from_account:
            filter_options['account__code__gte'] = from_account
        if to_account:
            filter_options['account__code__lte'] = to_account

        if to_date:
            filter_options['accounting_date__lte'] = to_date

        object_items = request.GET.getlist('object_items')
        if object_items:
            items_list = [int(object_item) for object_item in object_items if not object_item.startswith('all', 0, 3)]
            if len(items_list):
                filter_options['object_items__id__in'] = [object_id for object_id in items_list]

        return filter_options

    def get_opening_balances(self):
        return OpeningBalance.objects.ledger_lines(
            year=self.year,
            from_account=self.cleaned_data['from_account'],
            to_account=self.cleaned_data['to_account']
        )

    def execute(self):
        opening_balances = self.get_opening_balances()

        return self.get_ledger(opening_balances=opening_balances)

    def get_ledger(self, opening_balances):
        logger.info(self.cleaned_data['break_by'])
        if self.cleaned_data.get('break_by') == self.BY_SUFFIX:
            general_ledger = SuffixLedger(
                year=self.year,
                start_date=self.cleaned_data['from_date'],
                end_date=self.cleaned_data['to_date'],
                account=self.cleaned_data['from_account']
            )
            general_ledger.execute()
        elif self.cleaned_data.get('break_by') == self.BY_OBJECTS:
            general_ledger = GeneralLedger(
                year=self.year,
                start_date=self.cleaned_data['from_date'],
                end_date=self.cleaned_data['to_date'],
                from_account=self.cleaned_data['from_account'],
                to_account=self.cleaned_data['to_account'],
                object_items=self.data.getlist('object_items')
            )
            general_ledger.execute(opening_balances)
        elif self.cleaned_data.get('break_by') == self.SEPARATE_PERIOD:
            general_ledger = GeneralLedger(
                year=self.year,
                start_date=self.cleaned_data['from_date'],
                end_date=self.cleaned_data['to_date'],
                from_account=self.cleaned_data['from_account'],
                to_account=self.cleaned_data['to_account'],
                object_items=self.data.getlist('object_items')
            )
            general_ledger.execute(opening_balances)
        else:
            general_ledger = GeneralLedger(
                year=self.year,
                start_date=self.cleaned_data['from_date'],
                end_date=self.cleaned_data['to_date'],
                from_account=self.cleaned_data['from_account'],
                to_account=self.cleaned_data['to_account'],
                object_items=self.data.getlist('object_items'))
            general_ledger.execute(opening_balances)
        return general_ledger


class SuffixReportForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        super().__init__(*args, **kwargs)

        self.fields['account'].queryset = Account.objects.filter(company=self.year.company).exclude(
            code='', name='').order_by('code')

    from_date = forms.DateField(required=False, label='From Date')
    to_date = forms.DateField(required=False, label='To Date')
    is_potrait = forms.BooleanField(required=False, label='Potrait')
    is_landscape = forms.BooleanField(required=False, label='Landscape')
    account = forms.ModelChoiceField(queryset=None, label='Account', empty_label=None)

    def execute(self):
        report = SuffixLedger(year=self.year, start_date=self.cleaned_data['from_date'],
                              end_date=self.cleaned_data['to_date'], account=self.cleaned_data['account'])
        report.execute()

        return report


class InventoryValuationForm(forms.Form):

    NONE = 'none'
    BY_OBJECTS = 'by_objects'
    BY_SUFFIX = 'by_suffix'
    SEPARATE_PERIOD = 'separate_period'

    BREAKING_CHOICES = (
        (NONE, 'None'),
        (BY_OBJECTS, 'Break by objects'),
        (BY_SUFFIX, 'Suffix'),
        (SEPARATE_PERIOD, 'Each period separate')
    )

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.company = kwargs.pop('company')
        self.branch = kwargs.pop('branch')
        super().__init__(*args, **kwargs)

        self.fields['from_inventory'].queryset = Inventory.objects.filter(
            branch_inventory_items__branch=self.branch
        ).order_by('code')
        self.fields['to_inventory'].queryset = Inventory.objects.filter(
            branch_inventory_items__branch=self.branch
        ).order_by('-code')
        self.fields['branch'].queryset = Branch.objects.filter(company=self.company)
        if self.branch:
            self.fields['branch'].initial = self.branch
        self.fields['from_inventory'].empty_label = None
        self.fields['to_inventory'].empty_label = None

    from_inventory = forms.ModelChoiceField(queryset=None, label='From Account', to_field_name='code', required=False,
                                            widget=forms.Select(attrs={'class': 'form-control chosen-select'}))
    to_inventory = forms.ModelChoiceField(queryset=None, label='To Account', to_field_name='code', required=False,
                                          widget=forms.Select(attrs={'class': 'form-control chosen-select'}))
    branch = forms.ModelChoiceField(queryset=None, empty_label='All',
                                    widget=forms.Select(attrs={'class': 'form-control chosen-select'}))
    show_each_branch = forms.ChoiceField(widget=forms.RadioSelect(), required=False, choices=BREAKING_CHOICES)
    include_zeros = forms.BooleanField(required=False)
    break_by_parent_totals = forms.BooleanField(required=False)
    break_by_children_totals = forms.BooleanField(required=False)
    break_by_parent_no_totals = forms.BooleanField(required=False)
    break_by_parent_and_children_totals = forms.BooleanField(required=False)
    no_breaking = forms.BooleanField(required=False)
    break_by = forms.CharField(required=False)

    def execute(self):
        valuation_report = InventoryValuation(
            year=self.year, company=self.company, branch=self.cleaned_data['branch'],
            from_inventory=self.cleaned_data['from_inventory'], to_inventory=self.cleaned_data['to_inventory'],
            break_by=self.cleaned_data['break_by'], include_zeros=self.cleaned_data['include_zeros']
        )
        return valuation_report.execute()
