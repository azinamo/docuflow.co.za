from typing import List

def get_amount(amount, include_cents=False):
    if amount:
        amt = float(amount)
        if not include_cents:
            return round(amt)
        else:
            return amt
    return 0


def include_line(include_zero_accounts: bool, amounts: List) -> bool:
    if include_zero_accounts:
        return True
    if all([-1 < get_amount(amt) < 1 for amt in amounts]):
        return False
    return True
