import datetime
import logging
import pprint
from collections import OrderedDict
from decimal import Decimal

from django.db.models import Q, Sum, Case, When, DecimalField, F, OuterRef, Func, Subquery
from django.db.models.functions import Coalesce

from docuflow.apps.fixedasset.models import FixedAssetDisposal, Addition
from docuflow.apps.fixedasset.enums import AdditionType
from docuflow.apps.journals.models import JournalLine, OpeningBalance
from docuflow.apps.system.models import CashFlowCategory

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class BaseCashFlowStatement:

    def __init__(self, include_cents):
        self.include_cents = include_cents
        self.lines = OrderedDict({
            'operating_activities': {
                'cash': {
                    'cash_receipts': {'slug': 'cash-received', 'title': 'Cash receipts from customers', 'amount': 0, 'prev_year': 0, 'multiplier': -1},
                    'cash_paid': {'slug': 'cash-paid', 'title': 'Cash paid to suppliers and employees', 'amount': 0, 'prev_year': 0, 'multiplier': -1},
                    'total': {'title': 'Cash generated from operations', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                },
                'movements': {
                    'decrease_in_inventory': {
                        'slug': 'inventory-movement',
                        'title': 'Decrease/(Increase) in Inventory',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'decrease_in_prepayments': {
                        'slug': 'prepayment-movement',
                        'title': 'Decrease/(Increase) in Prepayments',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'decrease_in_accounts_receivable': {
                        'slug': 'account-receivable-movement',
                        'title': 'Decrease/(Increase) in Accounts Receivable',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'decrease_in_accounts_payable': {
                        'slug': 'account-payable-movement',
                        'title': '(Decrease)/Increase in Accounts Payable',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'movement_in_working_capital': {
                        'slug': 'movement-in-working-capital',
                        'title': 'Movement in working capital', 'amount': 0,
                        'prev_year': 0, 'multiplier': -1
                    },
                },
                'dividends': {
                    'dividend_received': {
                        'slug': 'dividend',
                        'title': 'Dividends Received',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'interest_received': {
                        'slug': 'interest-received',
                        'title': 'Interest Received',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'interest_paid': {
                        'slug': 'interest-paid',
                        'title': 'Interest Paid',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'tax_paid': {
                        'slug': 'tax',
                        'title': 'Tax Paid',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                },
                'total': {'amount': 0, 'title': 'Net cash flow from operating activities', 'prev_year': 0, 'multiplier': 1}
            },
            'investing_activities': {
                'cash': {
                    'acquisition': {
                        'slug': 'acquisition',
                        'title': 'Acquisition of property, plant and equipment',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'disposals': {
                        'slug': 'disposals',
                        'title': 'Disposal of property, plan and equipment',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'long_term_investment_debit': {
                        'slug': 'long_term_investment_debit',
                        'title': 'Long Term investment made',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'long_term_investment_credit': {
                        'slug': 'long_term_investment_credit',
                        'title': 'Proceeds from Long Term Investments',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': -1
                    },
                    'total': {
                        'slug': 'total_investing_activities',
                        'title': 'Net Cash Flow from investing activities',
                        'amount': 0,
                        'prev_year': 0,
                        'multiplier': 1
                    },
                }
            },
            'financing_activities': {
                'cash': {
                    'additional_share_proceeds': {'slug': 'additional_share', 'title': 'Proceeds from additional share issue',
                                                  'amount': 0, 'prev_year': 0, 'multiplier': -1},
                    'long_term_loans': {'slug': 'long-term-liabilities', 'title': 'Increase / Decrease in Long Term Loans',
                                        'amount': 0, 'prev_year': 0, 'multiplier': -1},
                    'retained_income': {'slug': 'retained-income', 'title': 'Dividends Paid', 'amount': 0, 'prev_year': 0, 'multiplier': -1},
                    'total': {'slug': 'total', 'title': 'Net Cash Flow from financing activities', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                },
            },
            'increase_decrease': {
                'cash': {
                    'increase_decrease': {'slug': 'increase_decrease', 'title': 'NET INCREASE / DECREASE IN CASH', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                    'period_start': {'slug': 'period_start', 'title': 'Cash at the beginning of the period', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                    'total': {'slug': 'total', 'title': 'Cash at end of the period', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                },
                'actual_cash': {
                    'actual_cash_at_period_end': {'slug': 'period_end_actual_cash', 'title': 'Actual Cash at the end of the period', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                    'difference': {'slug': 'difference', 'title': 'Difference', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                }
            },
            'cash': {
                'cash': {
                    'cash': {'slug': 'cash', 'title': 'Cash', 'amount': 0, 'prev_year': 0, 'multiplier': 1},
                }
            }
        })

    def get_amount(self, amount):
        return amount if amount else 0

    def get_journal_amount_case(self, q_filter):
        return Case(When(q_filter, then=Sum('debit') + Sum(F('credit') * -1)), output_field=DecimalField())


class CashFlowStatement(BaseCashFlowStatement):

    def __init__(self, year, from_period, to_period, previous_year, include_cents=False):
        self.from_period = from_period
        self.to_period = to_period
        self.previous_year = previous_year
        self.include_cents = include_cents
        self.year = year
        super(CashFlowStatement, self).__init__(self.include_cents)

    def get_period_year(self, previous_year):
        period_year = self.year
        if previous_year:
            period_year = previous_year
        return period_year

    def get_previous_year_dates(self):
        if self.previous_year:
            return self.previous_year.start_date, self.previous_year.end_date
        else:
            today = datetime.date.today()
            from_date = datetime.date(year=today.year - 1, month=1, day=1).today()
            to_date = datetime.date(year=today.year - 1, month=12, day=31).today()
            return from_date, to_date

    def get_year_period_filter(self, start_date, end_date):
        q_period_filter = Q()
        q_period_filter.add(Q(accounts__company=self.year.company), Q.AND)
        q_period_filter.add(Q(accounts__account_journals__accounting_date__gte=start_date), Q.AND)
        q_period_filter.add(Q(accounts__account_journals__accounting_date__lte=end_date), Q.AND)
        return q_period_filter

    def total_cash_flow(self, cash_flow_slug, field, start_date, end_date, year):
        return JournalLine.objects.aggregate(
            total=Sum(field, filter=Q(
                journal__year=year,
                journal__company=self.year.company,
                accounting_date__gte=start_date,
                accounting_date__lte=end_date,
                account__cash_flow__slug=cash_flow_slug
            )))

    def total_by_cash_flow_category(self, slug, start_date, end_date):
        return JournalLine.objects.aggregate(
            total=Sum(
                Coalesce(F('credit'), Decimal('0')) + Coalesce(F('debit'), Decimal('0')),
                filter=Q(
                    journal__company=self.year.company,
                    accounting_date__gte=start_date,
                    accounting_date__lte=end_date,
                    account__cash_flow__slug=slug
                )
            )
        )

    def account_total(self, start_date, end_date):
        accounts_query = JournalLine.objects.filter(account__cash_flow_id=OuterRef('id')).order_by()
        accounts_query = accounts_query.filter(journal__company=self.year.company)
        accounts_query = accounts_query.filter(accounting_date__gte=start_date)
        accounts_query = accounts_query.filter(accounting_date__lte=end_date)
        return accounts_query.values_list(Func(Coalesce(F('debit'), Decimal('0')) - Coalesce(F('credit'), Decimal('0')), function='SUM'))

    def get_cash_flow(self):
        current_year_mvt = self.account_total(start_date=self.from_period.from_date, end_date=self.to_period.to_date)
        previous_year_mvt = self.account_total(start_date=self.get_previous_year_dates()[0], end_date=self.get_previous_year_dates()[1])

        return CashFlowCategory.objects.values(
            'slug'
        ).annotate(
            total_movement=Subquery(current_year_mvt[:1]),
            previous_year_movement=Subquery(previous_year_mvt[:1]),
        ).values(
            'name', 'total_movement', 'previous_year_movement', 'slug'
        )

    def execute(self):
        cash_flows = self.get_cash_flow()
        for key, lines in self.lines.items():
            for k, l in lines.items():
                if k != 'total':
                    for i, v in l.items():
                        if 'slug' in v:
                            for c in cash_flows:
                                if c['slug'] == v['slug']:
                                    current_year = c['total_movement'] * v['multiplier'] if c['total_movement'] else 0
                                    previous_year = c['previous_year_movement'] * v['multiplier'] if c['previous_year_movement'] else 0
                                    self.lines[key][k][i]['amount'] = current_year
                                    self.lines[key][k][i]['prev_year'] = previous_year
        self.add_acquisitions()
        self.add_disposal_and_depreciations()
        self.add_debit_long_term_investments()
        self.add_credit_long_term_investments()
        self.calculate_totals()
        self.cash_at_beginning_of_period()
        self.add_incoming_balances()
        self.add_cash_end_of_period_balances()
        self.add_actual_cash()
        self.add_difference()

    def add_acquisitions(self):
        fixed_asset_additions = Addition.objects.filter(
            fixed_asset__deleted__isnull=True, addition_type=AdditionType.ADDITION, fixed_asset__company=self.year.company
        ).aggregate(
            year_total_additions=Sum('amount', filter=Q(fixed_asset__company=self.year.company, date__range=[self.from_period.from_date, self.to_period.to_date])),
            prev_year_total_additions=Sum('amount', filter=Q(fixed_asset__company=self.year.company, date__range=[self.get_previous_year_dates()[0], self.get_previous_year_dates()[1]]))
        )
        key = 'investing_activities'
        k = 'cash'
        i = 'acquisition'
        self.lines[key][k][i]['amount'] = fixed_asset_additions['year_total_additions'] * -1 if fixed_asset_additions['year_total_additions'] else 0
        self.lines[key][k][i]['prev_year'] = fixed_asset_additions['prev_year_total_additions'] * -1 if fixed_asset_additions['prev_year_total_additions'] else 0

    def add_disposal_and_depreciations(self):
        fixed_asset_disposals = FixedAssetDisposal.objects.filter(
            fixed_asset__company=self.year.company
        ).aggregate(
            year_disposals=Sum(Coalesce(F('depreciation_value'), Decimal('0')) - Coalesce(F('asset_value'), Decimal('0')), filter=Q(disposal__disposal_date__range=[self.from_period.from_date, self.to_period.to_date])),
            prev_year_disposals=Sum(Coalesce(F('depreciation_value'), Decimal('0')) - Coalesce(F('asset_value'), Decimal('0')), filter=Q(disposal__disposal_date__range=[self.get_previous_year_dates()[0], self.get_previous_year_dates()[1]])),
        )

        proceeds_on_assets = self.total_by_cash_flow_category('proceeds-on-assets', self.from_period.from_date, self.to_period.to_date)
        previous_proceeds_on_assets = self.total_by_cash_flow_category('proceeds-on-assets', self.get_previous_year_dates()[0], self.get_previous_year_dates()[1])

        proceeds_on_assets_total = proceeds_on_assets['total'] if proceeds_on_assets['total'] else 0
        prev_proceeds_on_assets_total = previous_proceeds_on_assets['total'] if previous_proceeds_on_assets['total'] else 0

        this_year_disposals = fixed_asset_disposals['year_disposals'] if fixed_asset_disposals['year_disposals'] else 0
        pre_year_disposals = fixed_asset_disposals['prev_year_disposals'] if fixed_asset_disposals['prev_year_disposals'] else 0

        key = 'investing_activities'
        k = 'cash'
        i = 'disposals'
        self.lines[key][k][i]['amount'] = proceeds_on_assets_total + this_year_disposals
        self.lines[key][k][i]['prev_year'] = prev_proceeds_on_assets_total + pre_year_disposals

    def add_debit_long_term_investments(self):
        long_term_investment = self.total_cash_flow(cash_flow_slug='long-term-investments', field='debit', start_date=self.from_period.from_date, end_date=self.to_period.to_date, year=self.year)
        prev_year_long_term_investment = self.total_cash_flow(cash_flow_slug='long-term-investments', field='debit', start_date=self.get_previous_year_dates()[0], end_date=self.get_previous_year_dates()[1], year=self.previous_year)
        logger.info("_______ long_term_investment -_________-")
        logger.info(long_term_investment)

        logger.info("_______ prev_year_long_term_investment _________-")
        logger.info(prev_year_long_term_investment)
        key = 'investing_activities'
        k = 'cash'
        i = 'long_term_investment_debit'
        self.lines[key][k][i]['amount'] = long_term_investment['total'] * -1 if long_term_investment['total'] else 0
        self.lines[key][k][i]['prev_year'] = prev_year_long_term_investment['total'] * -1 if prev_year_long_term_investment['total'] else 0

    def add_credit_long_term_investments(self):
        long_term_investment = self.total_cash_flow(cash_flow_slug='long-term-investments', field='credit', start_date=self.from_period.from_date, end_date=self.to_period.to_date, year=self.year)
        prev_year_long_term_investment = self.total_cash_flow(cash_flow_slug='long-term-investments', field='credit', start_date=self.get_previous_year_dates()[0], end_date=self.get_previous_year_dates()[1], year=self.previous_year)

        proceeds_on_long_term = self.total_by_cash_flow_category('proceeds-on-long-term-investments', self.from_period.from_date, self.to_period.to_date)
        previous_proceeds_on_long_term = self.total_by_cash_flow_category('proceeds-on-long-term-investments', self.get_previous_year_dates()[0], self.get_previous_year_dates()[1])

        key = 'investing_activities'
        k = 'cash'
        i = 'long_term_investment_credit'
        proceeds_on_long_term_total = proceeds_on_long_term['total'] if proceeds_on_long_term['total'] else 0
        previous_proceeds_on_long_term_total = previous_proceeds_on_long_term['total'] if previous_proceeds_on_long_term['total'] else 0

        long_term_investment_total = long_term_investment['total'] if long_term_investment['total'] else 0
        prev_year_long_term_investment_total = prev_year_long_term_investment['total'] if prev_year_long_term_investment['total'] else 0

        self.lines[key][k][i]['amount'] = proceeds_on_long_term_total - long_term_investment_total
        self.lines[key][k][i]['prev_year'] = previous_proceeds_on_long_term_total - prev_year_long_term_investment_total

    def cash_at_beginning_of_period(self):
        operating_total = self.lines['operating_activities']['total']['amount']
        prev_year_operating_total = self.lines['operating_activities']['total']['prev_year']
        investing_total = self.lines['investing_activities']['cash']['total']['amount']
        prev_year_investing_total = self.lines['investing_activities']['cash']['total']['prev_year']
        financing_total = self.lines['financing_activities']['cash']['total']['amount']
        prev_year_financing_total = self.lines['financing_activities']['cash']['total']['prev_year']

        key = 'increase_decrease'
        k = 'cash'
        i = 'increase_decrease'
        self.lines[key][k][i]['amount'] = operating_total + investing_total + financing_total
        self.lines[key][k][i]['prev_year'] = prev_year_operating_total + prev_year_investing_total + prev_year_financing_total

    def add_incoming_balances(self):
        incoming_balances = OpeningBalance.objects.aggregate(
            year_balances=Sum('amount', filter=Q(year__start_date__gte=self.from_period.from_date,
                                                 year__end_date__lte=self.to_period.to_date,
                                                 account__cash_flow__slug='cash',
                                                 company=self.year.company
                                                 )),
            prev_year_balances=Sum('amount', filter=Q(year__start_date__gte=self.get_previous_year_dates()[0],
                                                      year__end_date__lte=self.get_previous_year_dates()[1],
                                                      account__cash_flow__slug='cash',
                                                      company=self.year.company
                                                      ))
        )
        key = 'increase_decrease'
        k = 'cash'
        i = 'period_start'
        self.lines[key][k][i]['amount'] = incoming_balances['year_balances'] if incoming_balances['year_balances'] else 0
        self.lines[key][k][i]['prev_year'] = incoming_balances['prev_year_balances'] if incoming_balances['prev_year_balances'] else 0

    def add_cash_end_of_period_balances(self):
        cash_increase_decrease_total = self.lines['increase_decrease']['cash']['increase_decrease']['amount']
        prev_year_cash_increase_decrease_total = self.lines['increase_decrease']['cash']['increase_decrease']['prev_year']
        incoming_total = self.lines['increase_decrease']['cash']['period_start']['amount']
        prev_incoming_total = self.lines['increase_decrease']['cash']['period_start']['prev_year']

        key = 'increase_decrease'
        k = 'cash'
        i = 'total'
        self.lines[key][k][i]['amount'] = cash_increase_decrease_total + incoming_total
        self.lines[key][k][i]['prev_year'] = prev_year_cash_increase_decrease_total + prev_incoming_total

    def add_actual_cash(self):
        cash_mvt = self.lines['cash']['cash']['cash']['amount']
        prev_year_cash_mvt = self.lines['cash']['cash']['cash']['prev_year']
        incoming_total = self.lines['increase_decrease']['cash']['period_start']['amount']
        prev_incoming_total = self.lines['increase_decrease']['cash']['period_start']['prev_year']

        key = 'increase_decrease'
        k = 'actual_cash'
        i = 'actual_cash_at_period_end'
        self.lines[key][k][i]['amount'] = cash_mvt + incoming_total
        self.lines[key][k][i]['prev_year'] = prev_year_cash_mvt + prev_incoming_total

    def add_difference(self):
        period_cash = self.lines['increase_decrease']['cash']['total']['amount']
        prev_period_cash = self.lines['increase_decrease']['cash']['total']['prev_year']

        cash_mvt = self.lines['increase_decrease']['actual_cash']['actual_cash_at_period_end']['amount']
        prev_year_cash_mvt = self.lines['increase_decrease']['actual_cash']['actual_cash_at_period_end']['prev_year']

        key = 'increase_decrease'
        k = 'actual_cash'
        i = 'difference'
        self.lines[key][k][i]['amount'] = period_cash - cash_mvt
        self.lines[key][k][i]['prev_year'] = prev_period_cash - prev_year_cash_mvt

    def calculate_totals(self):
        for key, lines in self.lines.items():
            key_year_total = 0
            key_prev_year_total = 0
            for k, l in lines.items():
                k_year_total = 0
                k_prev_year_total = 0
                if k == 'total':
                    pass
                else:
                    for i, v in l.items():
                        k_year_total += v['amount'] if v['amount'] else 0
                        k_prev_year_total += v['prev_year'] if v['prev_year'] else 0

                        key_year_total += v['amount'] if v['amount'] else 0
                        key_prev_year_total += v['prev_year'] if v['prev_year'] else 0
                if 'total' in l:
                    self.lines[key][k]['total']['amount'] = k_year_total
                    self.lines[key][k]['total']['prev_year'] = k_prev_year_total

            if 'total' in lines:
                self.lines[key]['total']['amount'] = key_year_total
                self.lines[key]['total']['prev_year'] = key_prev_year_total
