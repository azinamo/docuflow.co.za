import logging
import pprint
from datetime import timedelta

from django import template
from django.utils.timezone import now

from docuflow.apps.invoice.models import Invoice, Payment
from docuflow.apps.sales.enums import InvoiceType
from docuflow.apps.sales.models import Invoice as SalesInvoice, Receipt

register = template.Library()

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


@register.inclusion_tag('reports/update_summary.html')
def updates_summary(company, year, start_date, end_date):
    end_date = end_date - timedelta(days=1)
    incoming = Invoice.objects.pending_incoming(company, start_date=start_date, end_date=end_date, year=year)
    posting = Invoice.objects.pending_posting(company, start_date=start_date, end_date=end_date, year=year)
    payments = Payment.objects.pending_updates(company, year=year, start_date=start_date, end_date=end_date)
    sales_tax_invoice = SalesInvoice.objects.pending_updates(company, invoice_type=InvoiceType.TAX_INVOICE.value,
                                                             start_date=start_date, end_date=end_date, year=year)
    sales_tax_credit_note = SalesInvoice.objects.pending_updates(company, invoice_type=InvoiceType.CREDIT_INVOICE.value,
                                                                 start_date=start_date, end_date=end_date, year=year)
    not_yet_posted = Invoice.objects.not_yet_posted(company=company, year=year)
    cash_ups = Receipt.objects.pending_cash_up(company=company, year=year)

    return {'open_batches': not_yet_posted.count(),
            'incoming': incoming.count() if company.run_incoming_document_journal else 0,
            'posting': posting.count(), 'payments': payments.count(), 'print_date': now().date(),
            'has_incoming_updates': company.run_incoming_document_journal, 'sales_tax_invoice': sales_tax_invoice.count(),
            'sales_tax_credit_note': sales_tax_credit_note.count(), 'cash_ups': cash_ups.count()
            }
