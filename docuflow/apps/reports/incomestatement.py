import logging
import pprint
from collections import OrderedDict
from datetime import datetime
from decimal import Decimal

from django.db.models import Q, Sum, Case, When, DecimalField, F
from django.db.models.functions import Coalesce

from docuflow.apps.budget.models import BudgetAccount
from docuflow.apps.company.enums import AccountType
from docuflow.apps.journals.models import JournalLine, OpeningBalance
from docuflow.apps.reports.utils import get_amount, include_line

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class IncomeStatementGroup(object):
    period_amount = 0
    current_year_amount = 0
    previous_year_amount = 0
    budget_amount = 0
    outgoing_amount = 0
    incoming_amount = 0
    items = {}
    periods = {}

    def __int__(self, group_name, items=None):
        if not items:
            items = {}
        self.group_name = 0
        self.items = 0
        self.total = 0

    def __add__(self, other):
        return IncomeStatementGroup

    def calculate_totals(self):
        pass


class GrossProfit(IncomeStatementGroup):

    profit_percentage = 0
    current_year_profit_percentage = 0
    previous_year_profit_percentage = 0

    def __repr__(self):
        return 'gross_profit'

    def balance_sheet_type(self):
        return 'gross_profit'


class OtherCost(IncomeStatementGroup):
    pass


class Income(IncomeStatementGroup):
    pass


class Expense(IncomeStatementGroup):
    pass


class DepreciationCost(IncomeStatementGroup):
    pass


class FinancialCost(IncomeStatementGroup):
    pass


class Tax(IncomeStatementGroup):

    def __str__(self):
        return 'Results before Income Tax'


class AccountLine(object):

    def __init__(self, account, incoming, period_amount, outgoing=0, movement=0, current_year_amount=0, previous_year_amount=0, budget_amount=0):
        self.account = account
        self.incoming = incoming
        self.period_amount = period_amount
        self.outgoing = outgoing
        self.movement = movement
        self.current_year_amount = current_year_amount
        self.previous_year_amount = previous_year_amount
        self.budget_amount = budget_amount


class BaseIncomeStatement:

    def __init__(self, include_cents):
        self.include_cents = include_cents
        self.lines = OrderedDict({
            'gross_profit': {'period_amount': 0, 'current_year_amount': 0, 'previous_year_amount': 0,
                             'profit_percentage': 0, 'current_year_profit_percentage': 0,
                             'previous_year_profit_percentage': 0,
                             'budget_amount': 0, 'outgoing': 0, 'incoming': 0, 'report_type_lines': {},
                             'periods': {}, 'budget_profit_percentage': 0, 'outgoing_amount': 0,
                             'budget_variance': 0
                             },
            'other_costs': {'period_amount': 0, 'current_year_amount': 0, 'previous_year_amount': 0,
                            'profit_percentage': 0, 'current_year_profit_percentage': 0, 'outgoing_amount': 0,
                            'budget_net_profit_percentage': 0, 'previous_year_profit_percentage': 0,
                            'budget_amount': 0, 'outgoing': 0, 'incoming': 0, 'report_type_lines': {}, 'periods': {},
                            'budget_variance': 0
                            },
            'income': {'period_amount': 0, 'total_income': 0, 'current_year_amount': 0, 'previous_year_amount': 0,
                       'budget_amount': 0, 'outgoing': 0, 'incoming': 0, 'report_type_lines': {},
                       'current_year_total_income': 0, 'previous_year_total_income': 0, 'outgoing_amount': 0,
                       'periods': {}, 'budget_variance': 0
                       },
            'expenses': {'period_amount': 0, 'current_year_amount': 0, 'previous_year_amount': 0, 'total': 0,
                         'budget_amount': 0, 'outgoing': 0, 'incoming': 0, 'report_type_lines': {},
                         'current_year_expenses': 0, 'previous_year_expenses': 0, 'periods': {}, 'budget_variance': 0},
            'depreciation_costs': {'period_amount': 0, 'current_year_amount': 0, 'previous_year_amount': 0, 'total': 0,
                                   'budget_amount': 0, 'outgoing': 0, 'incoming': 0, 'report_type_lines': {},
                                   'current_year_depreciation_costs': 0, 'previous_year_depreciation_costs': 0,
                                   'periods': {}, 'outgoing_amount': 0, 'budget_variance': 0},
            'financial_costs': {'period_amount': 0, 'current_year_amount': 0, 'previous_year_amount': 0, 'total': 0,
                                'budget_amount': 0, 'outgoing': 0, 'incoming': 0, 'report_type_lines': {},
                                'current_year_financial_costs': 0, 'previous_year_financial_costs': 0, 'periods': {},
                                'budget_variance': 0},
            'tax': {'period_amount': 0, 'current_year_amount': 0, 'previous_year_amount': 0, 'total': 0,
                    'budget_amount': 0, 'outgoing': 0, 'incoming': 0, 'report_type_lines': {},
                    'current_year_net_result': 0, 'outgoing_total': 0, 'previous_year_net_result': 0, 'periods': {},
                    'budget_variance': 0
                    }
        })

    # noinspection PyMethodMayBeStatic
    def get_amount(self, amount):
        return amount if amount else 0

    # noinspection PyMethodMayBeStatic
    def get_query_filtered_case(self, q_filter):
        return Case(When(q_filter, then=Sum(Coalesce(F('debit'), Decimal('0'))) + Sum(Coalesce(F('credit'), Decimal('0')) * -1)), output_field=DecimalField())

    # noinspection PyMethodMayBeStatic
    def is_turnover(self, slug):
        return slug == 'turnover'

    # noinspection PyMethodMayBeStatic
    def is_pre_gross_profit(self, slug):
        return slug in ['turnover', 'cost-of-sales']

    # noinspection PyMethodMayBeStatic
    def other_costs(self, slug):
        return slug in ['other-manufacturing-costs']

    # noinspection PyMethodMayBeStatic
    def is_post_gross_profit(self, slug):
        return slug in ['truck-income', 'other-income']

    # noinspection PyMethodMayBeStatic
    def is_expense(self, slug):
        return slug in ['employment-costs', 'sales-costs', 'fixed-costs', 'semi-fixed-costs', 'other-costs',
                        'occupation-costs', 'rental-costs']

    # noinspection PyMethodMayBeStatic
    def is_depreciation_costs(self, slug):
        return slug in ['depreciation-costs']

    # noinspection PyMethodMayBeStatic
    def is_financial_costs(self, slug):
        return slug in ['financial-income', 'financial-costs']

    # noinspection PyMethodMayBeStatic
    def is_tax(self, slug):
        return slug in ['tax-and-other-book-closing-transaction', 'tax-costs']

    def get_statement_key(self, report_type):
        if self.is_pre_gross_profit(report_type):
            return 'gross_profit'
        elif self.is_post_gross_profit(report_type):
            return 'income'
        elif self.is_expense(report_type):
            return 'expenses'
        elif self.is_depreciation_costs(report_type):
            return 'depreciation_costs'
        elif self.is_financial_costs(report_type):
            return 'financial_costs'
        elif self.is_tax(report_type):
            return 'tax'
        elif self.other_costs(report_type):
            return 'other_costs'
        else:
            return 'other_costs'

    # noinspection PyMethodMayBeStatic
    def calculate_percentage(self, amount, divisor):
        if divisor != 0:
            return (amount / divisor) * 100
        return 0

    def calculate_totals(self, period_turnover_total, current_year_turnover_total=0, previous_turnover_total=0,
                         budget_turnover_total=0):
        period_gross_profit = self.lines['gross_profit']['period_amount']
        current_year_gross_profit = self.lines['gross_profit']['current_year_amount']
        previous_year_gross_profit = self.lines['gross_profit']['previous_year_amount']
        budget_gross_profit = self.lines['gross_profit']['budget_amount']

        self.lines['gross_profit']['profit_percentage'] = self.calculate_percentage(period_gross_profit, period_turnover_total)
        self.lines['gross_profit']['current_year_profit_percentage'] = self.calculate_percentage(current_year_gross_profit, current_year_turnover_total)
        self.lines['gross_profit']['previous_year_profit_percentage'] = self.calculate_percentage(previous_year_gross_profit, previous_turnover_total)
        self.lines['gross_profit']['budget_gross_profit_percentage'] = self.calculate_percentage(budget_gross_profit, budget_turnover_total)

        period_total_other_costs = self.lines['other_costs']['period_amount']
        current_year_total_other_costs = self.lines['other_costs']['current_year_amount']
        previous_year_total_other_costs = self.lines['other_costs']['previous_year_amount']
        budget_other_costs = self.lines['other_costs']['budget_amount']

        period_net_gross_profit = period_gross_profit + period_total_other_costs
        current_year_net_gross_profit = current_year_gross_profit + current_year_total_other_costs
        previous_year_net_gross_profit = previous_year_gross_profit + previous_year_total_other_costs
        budget_net_gross_profit = budget_gross_profit + budget_other_costs

        self.lines['other_costs']['total'] = period_net_gross_profit
        self.lines['other_costs']['current_year_total'] = current_year_net_gross_profit
        self.lines['other_costs']['previous_year_total'] = previous_year_net_gross_profit
        self.lines['other_costs']['budget_total'] = budget_net_gross_profit

        self.lines['other_costs']['profit_percentage'] = self.calculate_percentage(period_net_gross_profit, period_turnover_total)
        self.lines['other_costs']['current_year_profit_percentage'] = self.calculate_percentage(current_year_net_gross_profit, current_year_turnover_total)
        self.lines['other_costs']['previous_year_profit_percentage'] = self.calculate_percentage(previous_year_net_gross_profit, previous_turnover_total)
        self.lines['other_costs']['budget_net_profit_percentage'] = self.calculate_percentage(budget_net_gross_profit, budget_turnover_total)

        period_total_income = self.lines['income']['period_amount']
        current_year_total_income = self.lines['income']['current_year_amount']
        previous_year_total_income = self.lines['income']['previous_year_amount']
        budget_total_income = self.lines['income']['budget_amount']

        expenses_total = self.lines['expenses']['period_amount']
        current_year_total_expenses = self.lines['expenses']['current_year_amount']
        previous_year_total_expenses = self.lines['expenses']['previous_year_amount']
        budget_expenses_total = self.lines['expenses']['budget_amount']

        depreciation_costs = self.lines['depreciation_costs']['period_amount']
        current_year_total_depreciation_costs = self.lines['depreciation_costs']['current_year_amount']
        previous_year_total_depreciation_costs = self.lines['depreciation_costs']['previous_year_amount']
        budget_depreciation_costs = self.lines['depreciation_costs']['budget_amount']

        period_financial_costs = self.lines['financial_costs']['period_amount']
        current_year_total_financial_costs = self.lines['financial_costs']['current_year_amount']
        previous_year_total_financial_costs = self.lines['financial_costs']['previous_year_amount']
        budget_financial_costs = self.lines['financial_costs']['budget_amount']

        period_tax_amount = self.lines['tax']['period_amount']
        current_year_total_tax_amount = self.lines['tax']['current_year_amount']
        previous_year_total_tax_amount = self.lines['tax']['previous_year_amount']
        budget_tax_amount = self.lines['tax']['budget_amount']

        # logger.info("-------------------------------CALCULATIONS-------------------------------------")
        if period_net_gross_profit:
            period_total_income += period_net_gross_profit

        if budget_gross_profit:
            budget_total_income += budget_net_gross_profit

        if current_year_net_gross_profit:
            current_year_total_income += current_year_net_gross_profit

        if previous_year_net_gross_profit:
            previous_year_total_income += previous_year_net_gross_profit

        budget_before_depreciation = budget_total_income
        if budget_expenses_total:
            budget_before_depreciation += budget_expenses_total

        period_total_before_depreciation = period_total_income
        if expenses_total:
            period_total_before_depreciation += expenses_total

        current_year_before_depreciation = current_year_total_income
        if current_year_total_expenses:
            current_year_before_depreciation += current_year_total_expenses
        # logger.info("{} total before depreciation {}".format(label, period_total_before_depreciation))

        previous_year_before_depreciation = previous_year_total_income
        if previous_year_total_expenses:
            previous_year_before_depreciation += previous_year_total_expenses

        period_total_after_depreciation = period_total_before_depreciation
        if depreciation_costs:
            period_total_after_depreciation += depreciation_costs

        current_year_total_after_depreciation = current_year_before_depreciation
        if current_year_total_depreciation_costs:
            current_year_total_after_depreciation += current_year_total_depreciation_costs
        # logger.info("{} total after depreciation {}".format(label, period_total_after_depreciation))

        previous_year_total_after_depreciation = previous_year_before_depreciation
        if previous_year_total_depreciation_costs:
            previous_year_total_after_depreciation += previous_year_total_depreciation_costs

        budget_after_depreciation = budget_before_depreciation
        if budget_depreciation_costs:
            budget_after_depreciation += budget_depreciation_costs

        period_financial_costs += period_total_after_depreciation
        budget_financial_costs += budget_after_depreciation
        current_year_total_financial_costs += current_year_total_after_depreciation
        # logger.info("{} total  financial costs {}".format(label, period_financial_costs))

        previous_year_total_financial_costs += previous_year_total_after_depreciation

        period_net_result = period_financial_costs
        if period_tax_amount:
            period_net_result += period_tax_amount

        budget_net_result = budget_financial_costs
        if budget_tax_amount:
            budget_net_result += budget_tax_amount

        current_year_net_result = current_year_total_financial_costs
        if current_year_total_tax_amount:
            current_year_net_result += current_year_total_tax_amount
        # logger.info("{} total net result {}".format(label, period_net_result))

        previous_year_net_result = previous_year_total_financial_costs
        if previous_year_total_tax_amount:
            previous_year_net_result += previous_year_total_tax_amount

        self.lines['income']['total_income'] = period_total_income
        self.lines['income']['current_year_total_income'] = current_year_total_income
        self.lines['income']['previous_year_total_income'] = previous_year_total_income
        self.lines['income']['budget_total'] = budget_total_income

        self.lines['expenses']['total'] = period_total_before_depreciation
        self.lines['expenses']['current_year_expenses'] = current_year_before_depreciation
        self.lines['expenses']['previous_year_expenses'] = previous_year_before_depreciation
        self.lines['expenses']['budget_total'] = budget_before_depreciation

        self.lines['depreciation_costs']['total'] = period_total_after_depreciation
        self.lines['depreciation_costs']['current_year_depreciation_costs'] = current_year_total_after_depreciation
        self.lines['depreciation_costs']['previous_year_depreciation_costs'] = previous_year_total_after_depreciation
        self.lines['depreciation_costs']['budget_total'] = budget_after_depreciation

        self.lines['financial_costs']['total'] = period_financial_costs
        self.lines['financial_costs']['current_year_financial_costs'] = current_year_total_financial_costs
        self.lines['financial_costs']['previous_year_financial_costs'] = previous_year_total_financial_costs
        self.lines['financial_costs']['budget_total'] = budget_financial_costs

        self.lines['tax']['total'] = period_net_result
        self.lines['tax']['current_year_net_result'] = current_year_net_result
        self.lines['tax']['previous_year_net_result'] = previous_year_net_result
        self.lines['tax']['outgoing_total'] = (period_net_result - current_year_net_result)
        self.lines['tax']['budget_total'] = budget_net_result

    def calculate_budget_variance(self):
        for group_name, group_lines in self.lines.items():
            for report_type, report_types in group_lines['report_type_lines'].items():
                for account_key, account_lines in report_types['accounts'].items():
                    period_amount = account_lines.get('period_amount', 0)
                    budget_amount = account_lines.get('budget_amount', 0)
                    budget_variance = 0 if budget_amount == 0 else (period_amount/budget_amount) * 100
                    account_lines['budget_variance'] = budget_variance
                report_period_amount = report_types.get('period_amount', 0)
                report_budget_amount = report_types.get('budget_amount', 0)
                report_budget_variance = 0 if report_budget_amount == 0 else (report_period_amount / report_budget_amount) * 100
                report_types['budget_variance'] = report_budget_variance
            group_total = group_lines.get('total', 0)
            group_budget_total = group_lines.get('budget_total', 0)
            group_budget_variance = 0 if group_budget_total == 0 else (group_total / group_budget_total) * 100
            group_lines['budget_variance'] = group_budget_variance


class IncomeStatement(BaseIncomeStatement):

    def __init__(self, year, company, start_date, end_date, previous_year, include_cents=False,
                 include_previous_year=False, include_zero_accounts=True, **kwargs):
        self.start_date = start_date
        self.end_date = end_date
        self.company = company
        self.year = year
        self.include_cents = include_cents
        self.include_zero_accounts = include_zero_accounts
        self.previous_year = self.get_period_year(previous_year)
        self.include_previous_year = include_previous_year
        self.previous_year_title = kwargs.get('previous_year_title', 'Whole').lower()
        self.budget_title = kwargs.get('budget_title', 'Whole').lower()
        self.current_year_filter = kwargs.get('current_year_filter', 'normal').lower()
        super(IncomeStatement, self).__init__(self.include_cents)

    def get_period_year(self, previous_year):
        period_year = self.year
        if previous_year:
            period_year = previous_year
        return period_year

    def get_last_year_date(self, day, month):
        start_date_str = f"{day}-{month}-{self.previous_year.year}"
        return datetime.strptime(start_date_str, '%d-%m-%Y').date()

    def get_previous_year_filter(self):
        if self.previous_year_title == 'period':
            start_date = self.get_last_year_date(self.start_date.day, self.start_date.month)
            end_date = self.get_last_year_date(self.end_date.day, self.end_date.month)

            return self.get_year_period_filter(start_date, end_date)
        elif self.previous_year_title == 'whole':
            return Case(When(journal__year=self.previous_year, then=Sum(Coalesce(F('debit'), Decimal('0'))) + Sum(Coalesce(F('credit'), Decimal('0')) * -1)), output_field=DecimalField())
        elif self.previous_year_title == 'accumulated':
            end_date = self.get_last_year_date(self.start_date.day, self.start_date.month)
            return self.get_year_accumulated_filter(self.previous_year, end_date)

    def get_year_period_filter(self, start_date, end_date):

        q_period_filter = Q()
        q_period_filter.add(Q(journal__date__gte=start_date), Q.AND)
        q_period_filter.add(Q(journal__date__lte=end_date), Q.AND)

        return self.get_query_filtered_case(q_period_filter)

    def get_year_accumulated_filter(self, year, journal_date, is_for_balance_sheet=False):

        q_accumulated_filter = Q()
        q_accumulated_filter.add(Q(journal__year=year), Q.AND)
        if is_for_balance_sheet:
            q_accumulated_filter.add(Q(journal__date__lt=journal_date), Q.AND)
        else:
            q_accumulated_filter.add(Q(journal__date__lte=journal_date), Q.AND)
        return self.get_query_filtered_case(q_accumulated_filter)

    def get_budget_lines(self):
        b_filter = Q()
        b_filter.add(Q(period_amounts__period__from_date__gte=self.start_date), Q.AND)
        b_filter.add(Q(period_amounts__period__to_date__lte=self.end_date), Q.AND)

        budget_lines = BudgetAccount.objects.annotate(
            budget_amount=Case(When(b_filter, then=Sum('period_amounts__amount')), output_field=DecimalField())
        ).select_related(
            'account'
        ).filter(
            account__account_type=AccountType.INCOME_STATEMENT.value, budget__year=self.year,
            account__company=self.company
        ).order_by(
            'account__code'
        ).values('budget_amount', 'account_id', 'account__name', 'account__code', 'account__id',
                 'account__report__name', 'account__report__slug')
        budget_turnover_total = 0
        for line in budget_lines:
            line_key = (line['account__report__slug'], line['account__report__name'])
            account_key = (line['account__name'], line['account__code'])
            budget_amount = self.get_amount(line['budget_amount'])

            account_line = {'account': line['account__name'],
                            'account_code': line['account__code'],
                            'incoming': 0,
                            'period_amount': 0,
                            'outgoing': 0,
                            'outgoing_amount': 0,
                            'movement': 0,
                            'budget_amount': budget_amount,
                            'current_year_amount': 0,
                            'previous_year_amount': 0,
                            'budget_variance': 0
                            }
            if budget_amount != 0:
                statement_key = self.get_statement_key(line['account__report__slug'])
                if self.is_pre_gross_profit(line['account__report__slug']):
                    if self.is_turnover(line['account__report__slug']):
                        budget_turnover_total += budget_amount if budget_amount else 0
                if statement_key and line_key and account_key:
                    self.add_income_statement_line(statement_key, line_key, account_key, account_line, 0, 0, 0, 0, budget_amount)
        return budget_turnover_total

    def get_journal_lines(self):
        this_year_period_filter = self.get_year_period_filter(self.start_date, self.end_date)
        if self.current_year_filter and self.current_year_filter == 'balance_sheet':
            this_year_accumulated_filter = self.get_year_accumulated_filter(
                year=self.year, journal_date=self.start_date, is_for_balance_sheet=True)
        else:
            this_year_accumulated_filter = self.get_year_accumulated_filter(self.year, self.end_date)

        previous_year_filter = self.get_previous_year_filter()

        filters = {'journal__company': self.company, 'deleted__isnull': True, 'account__company': self.company,
                   'account__report__account_type': AccountType.INCOME_STATEMENT.value,
                   }
        if self.include_previous_year:
            filters['journal__year_id__in'] = [self.year.id, self.previous_year.id]
        else:
            filters['journal__year'] = self.year
        return JournalLine.objects.annotate(
            total_incoming=this_year_accumulated_filter,
            period_total=this_year_period_filter,
            previous_year_total=previous_year_filter,
            period_budget=this_year_period_filter,
        ).select_related(
            'account', 'journal', 'journal__period', 'vat_code', 'supplier', 'invoice', 'invoice__invoice_type', 'journal__year'
        ).prefetch_related(
            'object_items'
        ).filter(
            **filters
        ).order_by(
            'account__code'
        ).values(
            'account_id', 'account__name', 'account__code', 'account__id', 'total_incoming', 'period_total',
            'account__report__name', 'account__report__slug', 'previous_year_total'
        )

    def add_income_statement_line(self, line_type, report_type, account_key, account_line, incoming_amount,
                                  period_amount, current_year_amount, previous_year_amount, budget_amount):
        if report_type in self.lines[line_type]['report_type_lines']:
            if account_key in self.lines[line_type]['report_type_lines'][report_type]['accounts']:
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['period_amount'] += period_amount if period_amount else 0
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['current_year_amount'] += current_year_amount if current_year_amount else 0
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['previous_year_amount'] += previous_year_amount if previous_year_amount else 0
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['budget_amount'] += budget_amount if budget_amount else 0
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key][
                    'outgoing_amount'] += account_line.get('outgoing_amount', 0)

            else:
                account_line['incoming'] = incoming_amount
                account_line['outgoing_amount'] = account_line.get('outgoing_amount', 0)
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key] = account_line
        else:
            self.lines[line_type]['report_type_lines'][report_type] = {
                'incoming': incoming_amount,
                'period_amount': 0,
                'current_year_amount': 0,
                'previous_year_amount': 0,
                'budget_amount': 0,
                'outgoing_amount': account_line.get('outgoing_amount', 0),
                'accounts': {account_key: account_line}
            }

        current_year_total = self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['current_year_amount']
        current_year_budget = self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key][
            'budget_amount']
        budget_variance = 0 if current_year_budget == 0 else (current_year_total / current_year_budget)
        self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['budget_variance'] = budget_variance

        self.lines[line_type]['report_type_lines'][report_type]['incoming'] += incoming_amount if incoming_amount else 0
        self.lines[line_type]['report_type_lines'][report_type]['current_year_amount'] += current_year_amount if current_year_amount else 0
        self.lines[line_type]['report_type_lines'][report_type]['budget_amount'] += budget_amount if budget_amount else 0
        self.lines[line_type]['report_type_lines'][report_type]['previous_year_amount'] += previous_year_amount if previous_year_amount else 0
        self.lines[line_type]['report_type_lines'][report_type]['period_amount'] += period_amount if period_amount else 0

        self.lines[line_type]['incoming'] += incoming_amount
        self.lines[line_type]['current_year_amount'] += current_year_amount
        self.lines[line_type]['budget_amount'] += budget_amount
        self.lines[line_type]['previous_year_amount'] += previous_year_amount
        self.lines[line_type]['period_amount'] += period_amount

    def get_opening_balances(self):
        opening_balances = OpeningBalance.objects.select_related(
            'account'
        ).filter(
            company=self.company, year=self.year, account__report__account_type=AccountType.INCOME_STATEMENT.value
        ).exclude(
            amount__exact=0
        ).order_by(
            'account__code'
        )
        for opening_balance in opening_balances:
            line_key = (opening_balance.account.report.slug, opening_balance.account.report.name)
            account_key = (opening_balance.account.name, opening_balance.account.code)
            amount = self.get_amount(opening_balance.amount)
            account_line = {'account': opening_balance.account.name,
                            'account_code': opening_balance.account.code,
                            'incoming': amount,
                            'period_amount': 0,
                            'outgoing_amount': 0,
                            'movement': 0,
                            'budget_amount': 0,
                            'current_year_amount': 0,
                            'previous_year_amount': 0,
                            'budget_variance': 0
                            }
            group_key = self.get_statement_key(opening_balance.account.report.slug)
            if group_key and line_key and account_key:
                self.add_income_statement_line(group_key, line_key, account_key, account_line, amount, 0, 0, 0, 0)

    def execute(self):
        lines = self.get_journal_lines()
        self.get_opening_balances()
        period_turnover_total = 0
        current_year_turnover_total = 0
        previous_turnover_total = 0
        budget_total = 0
        for journal_line in lines:

            line_key = (journal_line['account__report__slug'], journal_line['account__report__name'])
            account_key = (journal_line['account__name'], journal_line['account__code'])
            period_total = self.get_amount(journal_line['period_total'])
            total_incoming = self.get_amount(journal_line['total_incoming'])
            previous_year_amount = self.get_amount(journal_line['previous_year_total'])
            budget = 0

            account_line = {'account': journal_line['account__name'],
                            'account_code': journal_line['account__code'],
                            'incoming': total_incoming,
                            'period_amount': period_total,
                            'outgoing_amount': total_incoming + period_total,
                            'budget_amount': budget,
                            'budget_variance': 0,
                            'current_year_amount': total_incoming,
                            'previous_year_amount': previous_year_amount
                            }
            # line_total = total_incoming + period_total + previous_year_amount
            group_key = self.get_statement_key(journal_line['account__report__slug'])
            if self.is_pre_gross_profit(journal_line['account__report__slug']):
                if self.is_turnover(journal_line['account__report__slug']):
                    period_turnover_total += period_total if period_total else 0
                    current_year_turnover_total += total_incoming if total_incoming else 0
                    previous_turnover_total += previous_year_amount if previous_year_amount else 0
            if group_key and line_key and account_key:
                self.add_income_statement_line(group_key, line_key, account_key, account_line, 0, period_total,
                                               total_incoming, previous_year_amount, budget)

        budget_turnover_total = self.get_budget_lines()
        self.calculate_totals(
            period_turnover_total, current_year_turnover_total, previous_turnover_total, budget_turnover_total
        )
        self.calculate_budget_variance()
        self.prepare_lines()

    def prepare_lines(self):
        income_lines = {}
        for line_type, lines in self.lines.items():
            income_lines[line_type] = {
                'report_type_lines': {},
                'period_amount': lines['previous_year_amount'],
                'current_year_amount': lines['current_year_amount'],
                'previous_year_amount': lines['previous_year_amount'],
                'profit_percentage': lines.get('profit_percentage'),
                'current_year_profit_percentage': lines.get('current_year_profit_percentage'),
                'previous_year_profit_percentage': lines.get('previous_year_profit_percentage'),
                'budget_amount': lines['budget_amount'],
                'outgoing': lines['outgoing'],
                'incoming': lines['incoming'],
                'periods': lines['periods'],
                'budget_profit_percentage': lines.get('budget_profit_percentage'),
                'outgoing_amount': lines.get('outgoing_amount'),
                'budget_variance': lines.get('budget_variance'),
                'budget_gross_profit_percentage': lines.get('budget_gross_profit_percentage'),
                'total': lines.get('total'),
                'current_year_net_result': lines.get('current_year_net_result'),
                'previous_year_net_result': lines.get('previous_year_net_result'),
            }
            for report_type, report_lines in lines['report_type_lines'].items():
                income_lines[line_type]['report_type_lines'][report_type] = {
                    'accounts': {},
                    'incoming': report_lines['incoming'],
                    'period_amount': report_lines['previous_year_amount'],
                    'current_year_amount': report_lines['current_year_amount'],
                    'previous_year_amount': report_lines['previous_year_amount'],
                    'budget_amount': report_lines['budget_amount'],
                    'outgoing_amount': report_lines['outgoing_amount'],
                    'budget_variance': report_lines['budget_variance'],
                }
                for account, income_account_line in report_lines['accounts'].items():
                    line_amounts = [
                        income_account_line['current_year_amount'], income_account_line['period_amount'],
                        income_account_line['outgoing_amount']
                    ]
                    if include_line(include_zero_accounts=self.include_zero_accounts, amounts=line_amounts):
                        income_lines[line_type]['report_type_lines'][report_type]['accounts'][account] = income_account_line

        self.lines = income_lines



class PeriodIncomeStatement(BaseIncomeStatement):

    def __init__(self, year, from_period, to_period, filter_options, **kwargs):
        self.year = year
        self.from_period = from_period
        self.to_period = to_period
        self.filter_options = filter_options
        self.include_cents = kwargs.get('include_cents', False)
        self.kwargs = kwargs
        self.periods = self.get_period_range()
        super(PeriodIncomeStatement, self).__init__(self.include_cents)

    def get_period_range(self):
        from_period = int(self.from_period)
        to_period = int(self.to_period) + 1
        return range(from_period, to_period)

    def get_budget_lines(self):
        budget_lines = BudgetAccount.objects.get_income_statement_totals(
            self.year, period=self.kwargs.get('budget_title'), from_period=self.from_period, to_period=self.to_period
        )

        budget_turnover_total = 0
        for line in budget_lines:
            line_key = (line['account__report__slug'], line['account__report__name'])
            account_key = (line['account__name'], line['account__code'])
            period_amount = 0
            budget_amount = self.get_amount(line['budget_amount'])

            account_line = {'account': line['account__name'],
                            'account_code': line['account__code'],
                            'period_amount': period_amount,
                            'outgoing': 0,
                            'budget_amount': budget_amount,
                            'budget_variance': 0,
                            'periods': {},
                            'current_year_amount': period_amount,
                            'previous_year_amount': period_amount
                            }
            if budget_amount != 0:
                statement_key = self.get_statement_key(line['account__report__slug'])
                if self.is_pre_gross_profit(line['account__report__slug']):
                    if self.is_turnover(line['account__report__slug']):
                        budget_turnover_total += budget_amount
                if statement_key and line_key and account_key:
                    self.add_income_statement_line(statement_key, line_key, account_key, None, account_line,
                                                   period_amount, budget_amount)
        return budget_turnover_total

    def get_journal_lines(self):
        q_filter = Q()
        q_filter.add(Q(journal__period__period__gte=self.from_period), Q.AND)
        q_filter.add(Q(journal__period__period__lte=self.to_period), Q.AND)

        lines = JournalLine.objects.annotate(
            period_amount=Case(When(q_filter, then=Sum(Coalesce(F('debit'), 0)) + Sum(Coalesce(F('credit'), 0) * -1)), output_field=DecimalField())
        ).select_related(
            'account', 'journal', 'journal__period', 'vat_code', 'supplier', 'invoice', 'invoice__invoice_type', 'journal__year'
        ).prefetch_related(
            'object_items'
        ).filter(
            **self.filter_options
        ).order_by(
            'account__code', 'journal__date', '-accounting_date', 'journal__period__period',
        ).order_by(
            'account__code'
        ).values(
            'account_id', 'account__name', 'account__code', 'account__id', 'period_amount',
            'account__report__name', 'account__report__slug', 'journal__period__period'
        )
        return lines

    def add_income_statement_line(self, line_type, report_type, account_key, line_period, account_line, period_amount,
                                  budget_amount):

        if report_type in self.lines[line_type]['report_type_lines']:
            if account_key in self.lines[line_type]['report_type_lines'][report_type]['accounts']:
                for period in self.periods:
                    _amount = period_amount if line_period == period else 0
                    if period in self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['periods']:
                        self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['periods'][period]['amount'] += _amount
                    else:
                        self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['periods'][period] = {'amount': _amount}
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['period_amount'] += period_amount
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['budget_amount'] += budget_amount
            else:
                account_line['periods'] = {}
                for period in self.periods:
                    if line_period == period:
                        account_line['periods'][period] = {'amount': period_amount}
                    else:
                        account_line['periods'][period] = {'amount': 0}
                self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key] = account_line
        else:
            account_line['periods'] = {}
            for period in self.periods:
                if line_period == period:
                    if period in account_line['periods']:
                        account_line['periods'][period]['amount'] += period_amount
                    else:
                        account_line['periods'][period] = {'amount': period_amount}
                else:
                    account_line['periods'][period] = {'amount': 0}
            self.lines[line_type]['report_type_lines'][report_type] = {
                'period_amount': 0,
                'current_year_amount': 0,
                'previous_year_amount': 0,
                'budget_amount': 0,
                'budget_variance': 0,
                'outgoing': 0,
                'periods': {},
                'accounts': {account_key: account_line}
            }

        # budget_amount = self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key][
        #     'budget_amount']
        budget_variance = 0 if budget_amount == 0 else (period_amount/budget_amount)
        self.lines[line_type]['report_type_lines'][report_type]['accounts'][account_key]['budget_variance'] = budget_variance
        self.lines[line_type]['report_type_lines'][report_type]['budget_amount'] += budget_amount
        self.lines[line_type]['report_type_lines'][report_type]['budget_variance'] += budget_variance
        self.lines[line_type]['report_type_lines'][report_type]['period_amount'] += period_amount
        for period in self.periods:
            amount = period_amount if line_period == period else 0
            # budget_amount = budget_amount if line_period == period else 0

            if period in self.lines[line_type]['report_type_lines'][report_type]['periods']:
                self.lines[line_type]['report_type_lines'][report_type]['periods'][period]['amount'] += amount
            else:
                self.lines[line_type]['report_type_lines'][report_type]['periods'][period] = {'amount': amount}

            if period in self.lines[line_type]['periods']:
                self.lines[line_type]['periods'][period]['budget_amount'] += budget_amount
                self.lines[line_type]['periods'][period]['period_amount'] += amount
            else:
                self.lines[line_type]['periods'][period] = {'budget_amount': budget_amount, 'period_amount': amount}

        self.lines[line_type]['budget_amount'] += budget_amount
        self.lines[line_type]['period_amount'] += period_amount

    def execute(self):
        budget_turnover_total = self.get_budget_lines()
        lines = self.get_journal_lines()
        periods_turnovers = {}
        for line in lines:
            # logger.info(line)
            # logger.info("{} --> {}({}) -> Period({})={}".format(
            #     line['account__report__name'], line['account__code'], line['account__name'],
            #     line['journal__period__period'], line['period_amount']
            # ))
            line_key = (line['account__report__slug'], line['account__report__name'])
            account_key = (line['account__name'], line['account__code'])
            period_key = line['journal__period__period']
            period_amount = self.get_amount(line['period_amount'])
            budget_amount = 0

            account_line = {'account': line['account__name'],
                            'account_code': line['account__code'],
                            'period_amount': period_amount,
                            'outgoing': 0,
                            'budget_amount': budget_amount,
                            'budget_variance': 0,
                            'periods': {},
                            'current_year_amount': period_amount,
                            'previous_year_amount': period_amount
                            }
            statement_key = self.get_statement_key(line['account__report__slug'])
            if self.is_pre_gross_profit(line['account__report__slug']):
                if self.is_turnover(line['account__report__slug']):
                    if period_key in periods_turnovers:
                        periods_turnovers[period_key] += period_amount
                    else:
                        periods_turnovers[period_key] = period_amount
            if statement_key and line_key and account_key:
                self.add_income_statement_line(statement_key, line_key, account_key, period_key, account_line,
                                               period_amount, budget_amount)

        period_turnover_total = self.calculate_periods_totals(periods_turnovers)
        self.calculate_totals(period_turnover_total, 0, 0, budget_turnover_total)
        self.calculate_budget_variance()

    def calculate_periods_totals(self, periods_turnovers):
        period_turnover_total = 0
        for period in self.periods:
            label = f'Period {period}'
            period_gross = self.lines['gross_profit']['periods'].get(period, None)
            period_gross_profit = period_gross['period_amount'] if period_gross else 0
            budget_gross_profit = period_gross['budget_amount'] if period_gross else 0
            period_turnover = periods_turnovers.get(period, 0)
            period_turnover_total += period_turnover

            if period_turnover != 0:
                profit_percentage = self.calculate_percentage(amount=period_gross_profit, divisor=period_turnover)
                self.lines['gross_profit']['periods'][period]['profit_percentage'] = profit_percentage

            other_costs = self.lines['other_costs']['periods'].get(period, None)
            period_other_cost = other_costs['period_amount'] if other_costs else 0

            # logger.info(f"{label} other costs {period_other_cost}")

            period_net_gross_profit = period_gross_profit + period_other_cost
            # logger.info(f"{label} net gross profit {period_net_gross_profit}")
            net_profit_percentage = 0
            self.lines['other_costs']['periods'][period] = {'total': period_net_gross_profit, 'profit_percentage': 0}
            if period_turnover != 0:
                net_profit_percentage = self.calculate_percentage(amount=period_net_gross_profit, divisor=period_turnover)
                self.lines['other_costs']['periods'][period]['profit_percentage'] = net_profit_percentage
            # logger.info(f"{label} net gross profit percentage {net_profit_percentage}")

            period_income = self.lines['income']['periods'].get(period, None)
            period_total_income = period_income['period_amount'] if period_income else 0
            # budget_total_income = period_income['budget_amount'] if period_income else 0

            # logger.info(f"{label} total income {period_total_income}")

            period_expenses = self.lines['expenses']['periods'].get(period, None)
            expenses_total = period_expenses['period_amount'] if period_expenses else 0
            # budget_expenses_total = period_expenses['budget_amount'] if period_expenses else 0

            # logger.info(f"{label} total expenses {expenses_total}")

            period_depreciation = self.lines['depreciation_costs']['periods'].get(period, None)
            depreciation_costs = period_depreciation['period_amount'] if period_depreciation else 0
            # budget_depreciation_costs = period_depreciation['budget_amount'] if period_depreciation else 0

            # logger.info(f"{label} total depreciation_costs {depreciation_costs}")

            period_financials = self.lines['financial_costs']['periods'].get(period, None)
            period_financial_costs = period_financials['period_amount'] if period_financials else 0
            # budget_financial_costs = period_financials['budget_amount'] if period_financials else 0

            period_tax = self.lines['tax']['periods'].get(period, None)
            period_tax_amount = period_tax['period_amount'] if period_tax else 0
            # budget_tax_amount = period_tax['budget_amount'] if period_tax else 0

            # logger.info(f"{label} total tax costs {period_tax_amount}")

            # logger.info("-------------------------------CALCULATIONS-------------------------------------")
            # if period_other_cost:
            #     period_net_gross_profit += period_other_cost

            if period_net_gross_profit:
                period_total_income += period_net_gross_profit

            # if budget_net_gross_profit:
            #     budget_total_income += budget_net_gross_profit

            period_total_before_depreciation = period_total_income
            if expenses_total:
                period_total_before_depreciation += expenses_total

            # budget_total_before_depreciation = budget_total_income
            # if budget_expenses_total:
            #     budget_total_before_depreciation += budget_expenses_total

            period_total_after_depreciation = period_total_before_depreciation
            if depreciation_costs:
                period_total_after_depreciation += depreciation_costs

            # budget_total_after_depreciation = budget_total_before_depreciation
            # if budget_depreciation_costs:
            #     budget_total_after_depreciation += budget_depreciation_costs

            period_financial_costs += period_total_after_depreciation
            # budget_financial_costs += budget_total_after_depreciation

            period_net_result = period_financial_costs
            if period_tax_amount:
                period_net_result += period_tax_amount

            # budget_net_result = budget_financial_costs
            # if budget_tax_amount:
            #     budget_net_result += budget_tax_amount

            self.lines['income']['periods'][period] = {'total': period_total_income}

            self.lines['expenses']['periods'][period] = {'total': period_total_before_depreciation}

            self.lines['depreciation_costs']['periods'][period] = {'total': period_total_after_depreciation}

            self.lines['financial_costs']['periods'][period] = {'total': period_financial_costs}

            self.lines['tax']['periods'][period] = {'total': period_net_result}
        return period_turnover_total
