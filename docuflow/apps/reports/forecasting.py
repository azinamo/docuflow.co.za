from datetime import time, date

from docuflow.apps.invoice.models import Invoice


class ForecastingReport(object):

    def __init__(self, year):
        self.year = year
        self.total = 0
        self.day_invoices = {}

    def get_invoices(self, filter_options):
        return Invoice.objects.select_related(
            'invoice_type', 'company', 'period', 'period__period_year'
        ).filter(
            **filter_options
        ).order_by(
            '-due_date'
        ).exclude(deleted__isnull=False)

    @staticmethod
    def is_due(invoice_due_date):
        if isinstance(invoice_due_date, date):
            if invoice_due_date < date.today():
                return True
        return False

    @staticmethod
    def sort_report(forecasting_report):
        ordered_dates = sorted(forecasting_report)
        sorted_report = []
        for d in ordered_dates:
            if d in forecasting_report:
                sorted_report.append(forecasting_report[d])
        return sorted_report

    def process_report(self, filter_options):
        invoices = self.get_invoices(filter_options)
        monthly_invoices = {}
        for invoice in invoices:
            if invoice.due_date:
                invoice_total = invoice.open_amount

                timestamp = time.mktime(invoice.due_date.timetuple())

                self.total += invoice_total
                if timestamp in monthly_invoices:
                    monthly_invoices[timestamp]['total'] += invoice_total
                    monthly_invoices[timestamp]['invoices'].append(invoice)
                else:
                    is_due = ForecastingReport.is_due(invoice.due_date)
                    monthly_invoices[timestamp] = {'total': invoice_total,
                                                   'due_date': invoice.due_date,
                                                   'year': invoice.due_date.year,
                                                   'month': invoice.due_date.month,
                                                   'day': invoice.due_date.day,
                                                   'is_due': is_due,
                                                   'invoices': [invoice]
                                                   }
        self.day_invoices = ForecastingReport.sort_report(monthly_invoices)
