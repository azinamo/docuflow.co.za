import logging
import pprint
from collections import OrderedDict, defaultdict
from decimal import Decimal


from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import CompanyObject, ObjectItem
from docuflow.apps.journals.models import JournalLine, OpeningBalance


pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class GeneralLedger(object):

    def __init__(self, year, start_date, end_date, from_account, to_account, object_items):
        self.year = year
        self.start_date = start_date
        self.end_date = end_date
        self.from_account = from_account
        self.to_account = to_account
        self.object_items = object_items
        self.journal_lines = {}
        self.linked_objects = {}
        self.linked_models = {}
        self.linked_models_count = 0

    def get_company_object_items(self):
        object_items = ObjectItem.objects.select_related(
            'company_object'
        ).filter(
            company_object__company=self.year.company
        ).all()
        company_object_items = {}
        for object_item in object_items:
            if object_item.company_object_id in company_object_items:
                company_object_items[object_item.company_object_id].append(object_item)
            else:
                company_object_items[object_item.company_object_id] = [object_item]
        return company_object_items

    def get_company_objects(self):
        return CompanyObject.objects.prefetch_related(
            'object_items'
        ).filter(company=self.year.company)

    def get_company_linked_models(self):
        linked_models = {}
        company_objects = self.get_company_objects()

        for company_object in company_objects:
            linked_models[company_object.id] = {}
            linked_models[company_object.id]['id'] = company_object.id
            linked_models[company_object.id]['label'] = company_object.label
            linked_models[company_object.id]['class'] = company_object
            linked_models[company_object.id]['object_items'] = []
            self.linked_models_count += 1
            for object_item in company_object.object_items.all():
                linked_models[company_object.id]['object_items'].append(object_item)
        return linked_models

    def line_amount(self, debit, credit):
        total = 0
        if debit:
            total = debit
        if credit:
            total -= credit
        return total

    def get_object_links(self, objects, line, is_append=False, links=None):
        if not links:
            links = {}
        if is_append and links:
            for k, linked_object in links.items():
                if line['object_items__company_object_id'] == k:
                    links[k]['label'] = line['object_items__label']
        else:
            links = OrderedDict()
            for k, linked_object in objects.items():
                links[linked_object['id']] = {'value': None, 'label': '', 'object_items': ''}
                if line['object_items__company_object_id'] == linked_object['id']:
                    links[linked_object['id']]['label'] = line['object_items__label']
        return links

    def include_in_display(self, accounting_date, start_date):
        if not start_date:
            return True
        if accounting_date >= start_date:
            return True
        return False

    def get_lines_by_accounts(self, lines, opening_balances, linked_objects):
        accounts_lines = OrderedDict()

        for opening_balance in opening_balances:
            account_key = (opening_balance.account_id, opening_balance.account.code, opening_balance.account.name)
            opening_balance_amount = opening_balance.amount if opening_balance.amount else Decimal(0)
            accounts_lines[account_key] = {
                'total_credit': 0,
                'total_debit': 0,
                'outstanding': opening_balance_amount,
                'initial_debit': 0,
                'initial_credit': 0,
                'account_debit': 0,
                'account_credit': 0,
                'movement_debit': 0,
                'movement_credit': 0,
                'incoming_balance': opening_balance_amount,
                'balance': opening_balance_amount,
                'initial_balance': opening_balance_amount,
                'account_balance': opening_balance_amount,
                'periods': OrderedDict(),
                'lines': {}
            }

        return accounts_lines

    def execute(self, opening_balances):
        journal_lines = JournalLine.objects.get_ledger_lines(
            year=self.year,
            start_date=self.start_date,
            end_date=self.end_date,
            from_account=self.from_account,
            to_account=self.to_account,
            object_items=self.object_items
        )

        linked_objects = self.get_company_linked_models()
        self.linked_models = linked_objects

        accounts_lines = OrderedDict()

        for opening_balance in opening_balances:
            account_key = (opening_balance.account_id, opening_balance.account.code, opening_balance.account.name)
            opening_balance_amount = opening_balance.amount if opening_balance.amount else Decimal(0)
            accounts_lines[account_key] = {
                'total_credit': 0,
                'total_debit': 0,
                'outstanding': opening_balance_amount,
                'initial_debit': 0,
                'initial_credit': 0,
                'account_debit': 0,
                'account_credit': 0,
                'movement_debit': 0,
                'movement_credit': 0,
                'incoming_balance': opening_balance_amount,
                'balance': opening_balance_amount,
                'initial_balance': opening_balance_amount,
                'account_balance': opening_balance_amount,
                'periods': OrderedDict(),
                'lines': {}
            }

        for line in journal_lines:
            credit_amount = line.get('credit', 0)
            debit_amount = line.get('debit', 0)
            debit_amount = debit_amount if debit_amount else 0
            credit_amount = credit_amount if credit_amount else 0
            account_id = line.get('account_id')
            line_amount = line.get('line_amount', 0)
            account_key = (account_id, line['account__code'], line['account__name'])
            accounting_date = line['journal__date']

            if self.include_in_display(accounting_date, self.start_date):
                if account_key in accounts_lines:

                    line_exists = False

                    links = {}
                    if line['id'] in accounts_lines[account_key]['lines']:
                        line_exists = True
                        links = accounts_lines[account_key]['lines'][line['id']]['object_links']

                    object_links = self.get_object_links(linked_objects, line, line_exists, links)

                    if not line_exists:
                        line_balance = line_amount + accounts_lines[account_key]['account_balance']

                        journal_line = {
                            'credit': credit_amount,
                            'debit': debit_amount,
                            'line_amount': line_amount,
                            'outstanding': 0,
                            'initial_debit': 0,
                            'initial_credit': 0,
                            'balance': line_balance,
                            'initial_balance': 0,
                            'periods': OrderedDict(),
                            'line': line,
                            'object_links': object_links,
                            'module': Module(line['journal__module']) if line['journal__module'] else ''
                        }

                        accounts_lines[account_key]['lines'][line['id']] = journal_line
                        accounts_lines[account_key]['account_debit'] += debit_amount
                        accounts_lines[account_key]['account_credit'] += credit_amount
                        accounts_lines[account_key]['account_balance'] = line_balance
                        accounts_lines[account_key]['movement_debit'] += debit_amount
                        accounts_lines[account_key]['movement_credit'] += credit_amount
                        accounts_lines[account_key]['outstanding'] += line_amount
                    else:
                        accounts_lines[account_key]['lines'][line['id']]['object_links'] = object_links
                else:
                    object_links = self.get_object_links(linked_objects, line)

                    journal_line = {
                        'credit': credit_amount,
                        'debit': debit_amount,
                        'line_amount': line_amount,
                        'outstanding': 0,
                        'initial_debit': 0,
                        'initial_credit': 0,
                        'incoming_balance': 0,
                        'balance': line_amount,
                        'initial_balance': 0,
                        'periods': OrderedDict(),
                        'line': line,
                        'object_links': object_links,
                        'module': Module(line['journal__module']) if line['journal__module'] else ''
                    }
                    accounts_lines[account_key] = {
                        'lines': {line['id']: journal_line},
                        'account_credit': credit_amount,
                        'account_debit': debit_amount,
                        'account_balance': line_amount,
                        'outstanding': line_amount,
                        'movement_debit': debit_amount,
                        'movement_credit': credit_amount
                    }
            else:
                if account_key in accounts_lines:
                    accounts_lines[account_key]['total_credit'] = 0
                    accounts_lines[account_key]['total_debit'] = 0
                    accounts_lines[account_key]['initial_debit'] += debit_amount
                    accounts_lines[account_key]['initial_credit'] += credit_amount
                    accounts_lines[account_key]['account_balance'] += line_amount
                    accounts_lines[account_key]['outstanding'] += line_amount
                    accounts_lines[account_key]['initial_balance'] += line_amount
                    accounts_lines[account_key]['balance'] += line_amount
                    accounts_lines[account_key]['account_debit'] += debit_amount
                    accounts_lines[account_key]['account_credit'] += credit_amount
                else:
                    accounts_lines[account_key] = {
                        'total_credit': 0,
                        'total_debit': 0,
                        'outstanding': line_amount,
                        'initial_debit': debit_amount,
                        'initial_credit': credit_amount,
                        'balance': line_amount,
                        'initial_balance': line_amount,
                        'account_balance': line_amount,
                        'account_debit': debit_amount,
                        'account_credit': credit_amount,
                        'movement_debit': 0,
                        'movement_credit': 0,
                        'periods': OrderedDict(),
                        'lines': {}
                    }

        self.journal_lines = accounts_lines

        # account_lines = self.get_lines_by_accounts(journal_lines, opening_balances, linked_objects)

    # noinspection PyMethodMayBeStatic
    def get_lines_grouped_by_periods(self, journal_account_lines):
        logger.info("-------- Group by periods ------")
        account_period_lines = OrderedDict()
        for account_key, account_lines in journal_account_lines.items():
            period_lines = {}
            if account_key:
                account_period_lines[account_key] = OrderedDict()
                for line_key, line in account_lines['lines'].items():

                    line_period = line['line']['journal__period__period']
                    period_number = line['line']['journal__period__period']
                    credit_amount = line['line']['credit']
                    debit_amount = line['line']['debit']
                    line_amount = debit_amount - credit_amount

                    if line_period in period_lines:
                        # line_balance = line_amount + period_lines[line_period]['account_balance']
                        line_balance = line_amount
                        period_line = {
                            'credit': credit_amount,
                            'debit': debit_amount,
                            'line_amount': line_amount,
                            'outstanding': 0,
                            'initial_debit': 0,
                            'initial_credit': 0,
                            'balance': line_balance,
                            'initial_balance': 0,
                            'periods': OrderedDict(),
                            'line': line['line']
                        }
                        period_lines[line_period].append(period_line)
                    else:
                        period_line = {
                            'credit': credit_amount,
                            'debit': debit_amount,
                            'line_amount': line_amount,
                            'outstanding': 0,
                            'initial_debit': 0,
                            'initial_credit': 0,
                            'balance': line_amount,
                            'initial_balance': 0,
                            'periods': OrderedDict(),
                            'line': line['line']
                        }
                        period_lines[line_period] = [period_line]

                account_period_lines[account_key]['account'] = account_key
                account_period_lines[account_key]['account_credit'] = account_lines['account_credit']
                account_period_lines[account_key]['account_debit'] = account_lines['account_debit']
                account_period_lines[account_key]['account_balance'] = account_lines['account_balance']
                account_period_lines[account_key]['outstanding'] = account_lines['outstanding']
                account_period_lines[account_key]['period_lines'] = period_lines
        return account_period_lines

    def sort_account_lines(self, account_lines):
        sorted_lines = OrderedDict()
        for account, account_lines in account_lines.items():
            for period, period_lines in account_lines['periods'].items():
                time_sorted = sorted(period_lines)
                time_lines = []
                for t in time_sorted:
                    if t in period_lines:
                        time_lines.append(period_lines[t])

                if account in sorted_lines:
                    if period in sorted_lines[account]['periods']:
                        sorted_lines[account]['periods'][period] = time_lines
                    else:
                        sorted_lines[account]['periods'][period] = time_lines
                else:
                    sorted_lines[account] = OrderedDict()
                    sorted_lines[account] = {'periods': {period: time_lines},
                                             'total_credit': account_lines['total_credit'],
                                             'total_debit': account_lines['total_debit'],
                                             'outstanding': account_lines['outstanding'],
                                             'initial_debit': account_lines['initial_debit'],
                                             'initial_credit': account_lines['initial_credit'],
                                             'incoming_balance': account_lines['incoming_balance'],
                                             'balance': account_lines['balance'],
                                             'initial_balance': account_lines['initial_balance'],
                                             }
        return sorted_lines


class LedgerByAccounts(GeneralLedger):

    def __init__(self, company):
        self.company = company
        super(LedgerByAccounts, self).__init__(self.company)

    def get_lines_by_accounts(self, lines, opening_balances, linked_objects, start_date):

        accounts_lines = OrderedDict()

        for opening_balance in opening_balances:
            account_key = (opening_balance.account_id, opening_balance.account.code, opening_balance.account.name)
            accounts_lines[account_key] = {
                'total_credit': 0,
                'total_debit': 0,
                'outstanding': opening_balance.amount,
                'initial_debit': 0,
                'initial_credit': 0,
                'incoming_balance': opening_balance.amount,
                'balance': opening_balance.amount,
                'initial_balance': 0,
                'periods': OrderedDict(),
                'lines': {}
            }

        for line in lines:
            credit_amount = line.get('credit', 0)
            debit_amount = line.get('debit', 0)
            account_id = line.get('account_id')
            line_amount = self.line_amount(debit_amount, credit_amount)
            account_key = (account_id, line['account__code'], line['account__name'])

            if self.include_in_display(line['journal__date'], start_date):
                if account_key in accounts_lines:
                    line_exists = False

                    links = {}
                    if line['id'] in accounts_lines[account_key]['lines']:
                        line_exists = True
                        links = accounts_lines[account_key]['lines'][line['id']]['object_links']

                    object_links = self.get_object_links(linked_objects, line, line_exists, links)

                    if not line_exists:
                        line_balance = line_amount + accounts_lines[account_key]['account_balance']

                        journal_line = {
                            'credit': credit_amount,
                            'debit': debit_amount,
                            'line_amount': line_amount,
                            'outstanding': 0,
                            'initial_debit': 0,
                            'initial_credit': 0,
                            'balance': line_balance,
                            'initial_balance': 0,
                            'periods': OrderedDict(),
                            'line': line,
                            'object_links': object_links
                        }
                        account_credit = credit_amount
                        account_debit = debit_amount
                        account_balance = line_balance

                        accounts_lines[account_key]['lines'][line['id']] = journal_line
                        accounts_lines[account_key]['account_debit'] = account_debit
                        accounts_lines[account_key]['account_credit'] = account_credit
                        accounts_lines[account_key]['account_balance'] = account_balance
                        accounts_lines[account_key]['outstanding'] += line_amount
                    else:
                        accounts_lines[account_key]['lines'][line['id']]['object_links'] = object_links
                else:

                    object_links = self.get_object_links(linked_objects, line)

                    journal_line = {
                        'credit': credit_amount,
                        'debit': debit_amount,
                        'line_amount': line_amount,
                        'outstanding': 0,
                        'initial_debit': 0,
                        'initial_credit': 0,
                        'incoming_balance': 0,
                        'balance': line_amount,
                        'initial_balance': 0,
                        'periods': OrderedDict(),
                        'line': line,
                        'object_links': object_links
                    }
                    accounts_lines[account_key] = {
                        'lines': {line['id']: journal_line},
                        'account_credit': credit_amount,
                        'account_debit': debit_amount,
                        'account_balance': line_amount,
                        'outstanding': line_amount
                    }
            else:
                if account_key in accounts_lines:
                    accounts_lines[account_key]['total_credit'] = 0
                    accounts_lines[account_key]['total_debit'] = 0
                    accounts_lines[account_key]['initial_debit'] = 0
                    accounts_lines[account_key]['initial_credit'] = 0
                    accounts_lines[account_key]['account_balance'] += line_amount
                    accounts_lines[account_key]['outstanding'] += line_amount
                    accounts_lines[account_key]['incoming_balance'] += line_amount
                    accounts_lines[account_key]['balance'] += line_amount
                else:
                    accounts_lines[account_key] = {
                        'total_credit': 0,
                        'total_debit': 0,
                        'outstanding': line_amount,
                        'initial_debit': 0,
                        'initial_credit': 0,
                        'incoming_balance': line_amount,
                        'balance': line_amount,
                        'initial_balance': 0,
                        'account_balance': line_amount,
                        'account_debit': debit_amount,
                        'account_credit': credit_amount,
                        'periods': OrderedDict(),
                        'lines': {}
                    }
        return accounts_lines

    def execute(self):
        pass


class LedgerByObjects(GeneralLedger):

    def __init__(self, journal):
        self.journal = journal
        self.journal_lines = {}

    def get_lines_by_accounts(self, journal_lines, opening_balances, company_object):
        accounts_lines = []
        for journal_line in journal_lines:

            line = LedgerLine(journal_line)
            line_amount = line.debit - line.credit
            for journal_line_object_item in journal_line.object_items.all():
                if journal_line_object_item.company_object.id == company_object.id:

                    if journal_line.account_number in journal_lines['accounts']:
                        line['balance'] = line_amount + journal_lines['accounts'][journal_line.account.code]['balance']
                        accounts_lines['accounts'][journal_line.account_number]['lines'].append(line)
                        accounts_lines['accounts'][journal_line.account_number]['total_credit'] += line.credit
                        accounts_lines['accounts'][journal_line.account_number]['total_debit'] += line.debit
                        accounts_lines['accounts'][journal_line.account_number]['outstanding'] += line_amount
                        accounts_lines['accounts'][journal_line.account_number]['balance'] += line_amount
                    else:
                        # line['balance'] = line_amount
                        incoming_balance = 0
                        if journal_line.account.id in opening_balances:
                            incoming_balance = opening_balances[journal_line.account.id].amount
                        line_amount += incoming_balance
                        line['balance'] = line_amount
                        accounts_lines['accounts'][journal_line.account_number] = {
                            'account': journal_line.account,
                            'total_credit': line.credit,
                            'total_debit': line.debit,
                            'outstanding': line_amount,
                            'initial_debit': 0,
                            'initial_credit': 0,
                            'incoming_balance': incoming_balance,
                            'balance': line_amount,
                            'initial_balance': 0
                        }
                        accounts_lines['accounts'][journal_line.account_number]['lines'] = [line]

            accounts_lines['difference'] = journal_lines['debit_sum'] - journal_lines['credit_sum']
        return accounts_lines

    def process_report(self):
        journal_lines = {'accounts': OrderedDict(), 'credit_sum': 0, 'debit_sum': 0, 'difference': 0}

        opening_balances = self.get_account_opening_balances()

        linked_objects = self.get_company_linked_models()

        objects_lines = []
        for company_object in linked_objects:
            objects_lines[company_object.id] = {'accounts': {}, 'total': 0, 'object': company_object}
            objects_lines[company_object.id]['accounts'] = self.get_lines_by_accounts(journal_lines, opening_balances,
                                                                                      company_object)
        return objects_lines


class SuffixLedger(object):

    def __init__(self, year, start_date, end_date, account):
        self.year = year
        self.start_date = start_date
        self.end_date = end_date
        self.account = account
        self.journal_lines = []
        self.opening_balance = None
        self.total = Decimal(0)

    def get_opening_balances(self):
        self.opening_balance = OpeningBalance.objects.ledger_lines(year=self.year, account=self.account).first()

    def execute(self):
        self.get_opening_balances()
        journal_lines = JournalLine.objects.get_ledger_suffix_lines(year=self.year, start_date=self.start_date,
                                                                    end_date=self.end_date, account=self.account,
                                                                    with_balance=False)
        suffix_lines = defaultdict(Decimal)
        self.total += self.opening_balance.amount if self.opening_balance else 0
        for journal_line in journal_lines:
            line_amount = journal_line['line_amount'] if journal_line['line_amount'] else Decimal(0)
            suffix_lines[journal_line['suffix']] += line_amount
            self.total += line_amount

        self.journal_lines = {k: v for k, v in suffix_lines.items() if v != 0}
