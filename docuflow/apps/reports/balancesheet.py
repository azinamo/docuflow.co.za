import logging
import pprint
from collections import namedtuple

from docuflow.apps.company.enums import AccountType
from docuflow.apps.journals.models import JournalLine, OpeningBalance
from docuflow.apps.reports.utils import include_line, get_amount
from .incomestatement import IncomeStatement


logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


AccountLine = namedtuple('AccountLine', ['id', 'name', 'code'])
ReportType = namedtuple('ReportType', ['name', 'slug'])


class BalanceSheet:

    def __init__(self, company, year, start_date, end_date, income_statement: IncomeStatement, include_cents=False, include_zero_accounts=False, **kwargs):
        self.start_date = start_date
        self.end_date = end_date
        self.income_statement = income_statement
        self.include_cents = include_cents
        self.include_zero_accounts = include_zero_accounts
        self.company = company
        self.year = year

        self.total_outgoing = 0
        self.total_incoming = 0
        self.total_movement = 0
        self.total_period = 0
        self.total_assets = 0
        self.total_period_asset = 0
        self.total_incoming_asset = 0
        self.total_outgoing_asset = 0
        self.total_movement_asset = 0
        self.total_liabilities = 0
        self.assets = {}

        self.total_period_liabilities = 0
        self.total_incoming_liabilities = 0
        self.total_outgoing_liabilities = 0
        self.total_movement_liabilities = 0
        self.liabilities = {}

    def get_balance_sheet_structure(self):
        balance_sheet = {
            'assets': {
                'label': 'Assets',
                'types': {
                    'fixed-assets': {},
                    'turnover-assets': {},
                    'current-assets': {},
                    'occupation-costs': {}
                }
            },
            'liabilities_and_own_capital': {
                'label': 'LIABILITIES AND EQUITY',
                'types': {
                    'liabilities': {},
                    'long-term-liabilities': {},
                    'short-term-liabilities': {},
                    'own-capital': {},
                    'tax-costs': {},
                }
            }
        }
        return balance_sheet

    def is_liability(self, slug):
        return slug in ['long-term-liabilities', 'current-liabilities', 'equity']

    def is_asset(self, slug):
        return slug in ['fixed-assets', 'current-asset', 'inventory-assets', 'cash']

    def add_liability_account_line(
            self,
            line_key,
            account_key,
            account_line,
            incoming_amount=0,
            period_amount=0,
            movement_amount=0
    ):
        outgoing_amount = incoming_amount + period_amount + movement_amount
        if line_key in self.liabilities:
            if account_key in self.liabilities[line_key]['accounts']:
                self.liabilities[line_key]['accounts'][account_key]['incoming'] += incoming_amount
                self.liabilities[line_key]['accounts'][account_key]['period_amount'] += period_amount
                self.liabilities[line_key]['accounts'][account_key]['movement_amount'] += movement_amount
                self.liabilities[line_key]['accounts'][account_key]['outgoing'] += outgoing_amount
            else:
                account_line['outgoing'] = outgoing_amount
                account_line['incoming'] = incoming_amount
                self.liabilities[line_key]['accounts'][account_key] = account_line
        else:
            account_line['outgoing'] = outgoing_amount
            self.liabilities[line_key] = {
                'incoming': 0,
                'period_amount': 0,
                'movement_amount': 0,
                'outgoing': 0,
                'accounts': {account_key: account_line}
            }
        self.liabilities[line_key]['incoming'] += incoming_amount
        self.liabilities[line_key]['movement_amount'] += movement_amount
        self.liabilities[line_key]['period_amount'] += period_amount
        self.liabilities[line_key]['outgoing'] += outgoing_amount
        self.total_liabilities += outgoing_amount

        self.total_period_liabilities += period_amount
        self.total_movement_liabilities += movement_amount
        self.total_incoming_liabilities += incoming_amount
        self.total_outgoing_liabilities += outgoing_amount

        self.total_incoming += incoming_amount
        self.total_movement += movement_amount
        self.total_outgoing += outgoing_amount
        self.total_period += period_amount

    def add_asset_account_line(self, line_key, account_key, account_line, incoming_amount=0, period_amount=0,
                               movement_amount=0):
        outgoing_amount = incoming_amount + period_amount + movement_amount

        if line_key in self.assets:
            if account_key in self.assets[line_key]['accounts']:
                self.assets[line_key]['accounts'][account_key]['incoming'] += incoming_amount
                self.assets[line_key]['accounts'][account_key]['period_amount'] += period_amount
                self.assets[line_key]['accounts'][account_key]['movement_amount'] += movement_amount
                self.assets[line_key]['accounts'][account_key]['outgoing'] += outgoing_amount
            else:
                account_line['outgoing'] = outgoing_amount
                account_line['incoming'] = incoming_amount
                self.assets[line_key]['accounts'][account_key] = account_line
        else:
            account_line['outgoing'] = outgoing_amount
            account_line['incoming'] = incoming_amount
            self.assets[line_key] = {
                'incoming': 0,
                'period_amount': 0,
                'movement_amount': 0,
                'outgoing': 0,
                'accounts': {account_key: account_line}
            }

        self.assets[line_key]['incoming'] += incoming_amount
        self.assets[line_key]['movement_amount'] += movement_amount
        self.assets[line_key]['period_amount'] += period_amount
        self.assets[line_key]['outgoing'] += outgoing_amount

        self.total_period_asset += period_amount
        self.total_movement_asset += movement_amount
        self.total_incoming_asset += incoming_amount
        self.total_outgoing_asset += outgoing_amount

        self.total_incoming += incoming_amount
        self.total_movement += movement_amount
        self.total_outgoing += outgoing_amount
        self.total_period += period_amount

    def get_opening_balances(self):
        opening_balances = OpeningBalance.objects.select_related(
            'account', 'account__report'
        ).filter(
            company=self.company, year=self.year, account__report__account_type=AccountType.BALANCE_SHEET.value
        ).order_by(
            'account__code'
        )
        if not self.include_zero_accounts:
            opening_balances = opening_balances.exclude(amount__exact=0)

        _ledger_lines = {'line_key': '', 'account_key': ''}
        for opening_balance in opening_balances:
            account_key = AccountLine(
                opening_balance.account.id, opening_balance.account.name, opening_balance.account.code
            )
            line_key = ReportType(opening_balance.account.report.name, opening_balance.account.report.slug)
            amount = get_amount(amount=opening_balance.amount, include_cents=self.include_cents)

            account_line = {'account': opening_balance.account.name,
                            'account_code': opening_balance.account.code,
                            'incoming': amount,
                            'period_amount': 0,
                            'outgoing': 0,
                            'movement_amount': 0
                            }

            if self.is_liability(opening_balance.account.report.slug):
                self.add_liability_account_line(
                    line_key=line_key,
                    account_key=account_key,
                    account_line=account_line,
                    incoming_amount=amount,
                    period_amount=0,
                    movement_amount=0
                )
            else:
                self.add_asset_account_line(
                    line_key=line_key,
                    account_key=account_key,
                    account_line=account_line,
                    incoming_amount=amount,
                    period_amount=0,
                    movement_amount=0
                )

    def execute(self):
        lines = JournalLine.objects.balance_report(
            company=self.company,
            year=self.year,
            start_date=self.start_date,
            end_date=self.end_date,
            account_type=AccountType.BALANCE_SHEET.value
        )
        self.get_opening_balances()
        for journal_line in lines:
            account_key = AccountLine(
                journal_line['account_id'], journal_line['account__name'], journal_line['account__code']
            )
            line_key = ReportType(journal_line['account__report__name'], journal_line['account__report__slug'])
            period_total = get_amount(amount=journal_line['period_total'], include_cents=self.include_cents)
            movement_amount = get_amount(amount=journal_line['total_incoming'], include_cents=self.include_cents)

            account_line = {'account': journal_line['account__name'],
                            'account_code': journal_line['account__code'],
                            'movement_amount': movement_amount,
                            'period_amount': period_total,
                            'outgoing': 0
                            }

            if self.is_liability(journal_line['account__report__slug']):
                self.add_liability_account_line(
                    line_key=line_key,
                    account_key=account_key,
                    account_line=account_line,
                    incoming_amount=0,
                    period_amount=period_total,
                    movement_amount=movement_amount
                )
            else:
                self.add_asset_account_line(
                    line_key=line_key,
                    account_key=account_key,
                    account_line=account_line,
                    incoming_amount=0,
                    period_amount=period_total,
                    movement_amount=movement_amount
                )

        self.balance_totals()
        self.add_equity_from_income_statement()
        self.liabilities = self.clean_report_accounts_lines(report_type_accounts=self.liabilities)
        self.assets = self.clean_report_accounts_lines(report_type_accounts=self.assets)

    def clean_report_accounts_lines(self, report_type_accounts):
        report_accounts = {}
        for line_key, accounts_line in report_type_accounts.items():
            for account, account_line in accounts_line['accounts'].items():
                amounts = [
                    account_line['incoming'], account_line['movement_amount'], account_line['period_amount'],
                    account_line['outgoing']
                ]
                if include_line(include_zero_accounts=self.include_zero_accounts, amounts=amounts):
                    if line_key in report_accounts:
                        report_accounts[line_key]['accounts'][account] = account_line
                    else:
                        report_accounts[line_key] =  {
                            'accounts': {
                                account: account_line
                            }
                        }
        return report_accounts

    def add_equity_from_income_statement(self):
        line_key = ReportType('Equity', 'equity')
        period_amount = self.income_statement.lines['tax']['total']
        movement_amount = self.income_statement.lines['tax']['current_year_net_result']

        account_key = AccountLine('-1', 'Current Year Profit/(Loss)', '-')
        period_total = get_amount(include_cents=self.include_cents, amount=period_amount)
        movement_amount = get_amount(include_cents=self.include_cents, amount=movement_amount)
        account_line = {'account': 'Current Year Profit/(Loss)',
                        'account_code': '-',
                        'movement_amount': movement_amount,
                        'period_amount': period_total,
                        'outgoing': 0,
                        'incoming': 0
                        }
        self.add_liability_account_line(
            line_key=line_key,
            account_key=account_key,
            account_line=account_line,
            incoming_amount=0,
            period_amount=period_total,
            movement_amount=movement_amount
        )

    def balance_totals(self):
        if not self.include_cents:
            if self.total_incoming != 0:
                default_bank_account = self.company.default_bank_account
                if default_bank_account:
                    line_key = ReportType(default_bank_account.report.name, default_bank_account.report.slug)

                    account_key = AccountLine(
                        default_bank_account.id, default_bank_account.name, default_bank_account.code
                    )

                    amount = get_amount(amount=self.total_incoming, include_cents=self.include_cents) * -1

                    account_line = {'account': default_bank_account.name,
                                    'account_code': default_bank_account.code,
                                    'incoming': amount,
                                    'period_amount': 0,
                                    'outgoing': 0,
                                    'movement_amount': 0
                                    }

                    self.add_asset_account_line(
                        line_key=line_key,
                        account_key=account_key,
                        account_line=account_line,
                        incoming_amount=amount,
                        period_amount=0,
                        movement_amount=0
                    )
