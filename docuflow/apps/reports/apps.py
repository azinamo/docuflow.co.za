from django.apps import AppConfig


class ReportsConfig(AppConfig):
    name = 'docuflow.apps.reports'
