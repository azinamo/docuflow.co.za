from django.urls import path

from .views import *

# security_patterns = ([
#     path('security/', security_settings, name="security_settings"),
#     path('configure/', configure_mfa, name="configure_mfa"),
#     path('enable/', enable_mfa, name="enable_mfa"),
#     path('verify/token/', verify_otp, name="verify_otp"),
#     path('disable/', disable_mfa, name="disable_mfa"),
# ], 'mfa')

urlpatterns = [
    path('security/', security_settings, name="security_settings"),
    path('configure/', configure_mfa, name="configure_mfa"),
    path('enable/', enable_mfa, name="enable_mfa"),
    path('verify/token/', verify_otp, name="verify_otp"),
    path('disable/', disable_mfa, name="disable_mfa"),
]
