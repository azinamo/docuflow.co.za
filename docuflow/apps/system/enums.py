from enumfields import IntEnum, Enum


class AccountType(IntEnum):
    INCOME_STATEMENT = 1
    BALANCE_SHEET = 2

    @classmethod
    def get_value_from_label(cls, label):
        try:
            return next(acc_type.value for acc_type in cls if acc_type.label == label)
        except StopIteration:
            return None


class CashFlow(Enum):
    CASH_RECEIVED = 'cash-received'
    CASH_PAID = 'cash-paid'
    DIVIDENT = 'dividend'
    INTEREST_RECEIVED = 'interest-received'
    INTEREST_PAID = 'interest-paid'
    TAX = 'tax'
    LONG_TERM_INVESTMENT = 'long-term-investments'
    EQUITY = 'equity'
    LONG_TERM_LIABILITY = 'long-term-liabilities'
    CASH = 'cash'
