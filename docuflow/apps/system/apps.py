from django.apps import AppConfig


class SystemConfig(AppConfig):
    name = 'docuflow.apps.system'
