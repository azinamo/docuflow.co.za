import json

from annoying.functions import get_object_or_None
from django.views.generic import ListView, TemplateView, View
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse

from .models import VersionLog, InvoiceFieldValue
from .forms import InvoiceFieldValueForm


# Create your views here.
class SettingsView(TemplateView):
    template_name = 'settings/index.html'


class VersionLogView(LoginRequiredMixin, ListView):
    model = VersionLog
    template_name = 'versionlog/index.html'
    context_object_name = 'version_logs'


class UpdateFieldValuesView(FormView):
    template_name = 'fieldvalues/update.html'
    model = InvoiceFieldValue
    form_class = InvoiceFieldValueForm

    def get_object(self):
        return get_object_or_None(InvoiceFieldValue, field=self.kwargs['field_id'])

    def get_context_data(self, **kwargs):

        context = super(UpdateFieldValuesView, self).get_context_data(**kwargs)
        context['invoice_field'] = self.get_object()
        print(context['invoice_field'])
        return context

    def form_invalid(self, form):
        return HttpResponse(
            json.dumps({'error': False,
                        'text': 'Error updating the field values',
                        'errors': form.errors
                        })
        )

    def form_valid(self, form):

        values = form.cleaned_data['values']
        print(values)
        invoice_field = self.get_object()

        if invoice_field:
            invoice_field.values = values
            invoice_field.save()

            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Field values successfully saved.',
                            })
            )
        else:
            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Field not found',
                            })
            )


class SaveTemplateFieldOptionValues(View):

    def post(self, request, *args, **kwargs):
        values = self.request.POST['values']
        invoice_field = get_object_or_None(InvoiceFieldValue, field=self.kwargs['field_id'])

        if invoice_field:
            invoice_field.values = values
            invoice_field.save()

            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Field values successfully saved.',
                            })
            )
        else:
            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Field not found',
                            })
            )
