from django import forms

from .models import InvoiceFieldValue


class InvoiceFieldValueForm(forms.ModelForm):
    class Meta:
        model = InvoiceFieldValue
        exclude = ['field', 'regex', 'label']
