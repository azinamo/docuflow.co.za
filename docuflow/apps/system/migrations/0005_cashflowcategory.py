# Generated by Django 3.1 on 2020-10-04 02:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0004_paymentmethod'),
    ]

    operations = [
        migrations.CreateModel(
            name='CashFlowCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('name', models.CharField(max_length=255)),
                ('is_active', models.BooleanField(default=True)),
                ('slug', models.CharField(editable=False, max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Cash Flows',
                'ordering': ['name'],
            },
        ),
    ]
