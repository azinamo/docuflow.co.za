# Generated by Django 3.1 on 2020-12-05 19:38

from django.db import migrations, models


def add_default_currencies(apps, schema_editor):
    Currency = apps.get_model('system.Currency')
    CompanyCurrency = apps.get_model('company.Currency')

    for company_currency in CompanyCurrency.objects.all():
        Currency.objects.get_or_create(
            code=company_currency.code,
            defaults={
                'name': company_currency.name,
                'symbol': company_currency.symbol
            }
        )


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0009_currency'),
    ]

    operations = [
        migrations.RunPython(add_default_currencies, reverse_code=migrations.RunPython.noop),
    ]
