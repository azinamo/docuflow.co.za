# Generated by Django 3.1 on 2020-12-05 19:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0008_vatcode'),
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('code', models.CharField(max_length=10)),
                ('symbol', models.CharField(max_length=5)),
            ],
            options={
                'verbose_name_plural': 'Currencies',
                'ordering': ['name'],
            },
        ),
    ]
