# Generated by Django 3.1 on 2020-09-15 01:20

from django.db import migrations, models
import docuflow.apps.system.enums
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReportType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('name', models.CharField(max_length=255)),
                ('account_type', enumfields.fields.EnumIntegerField(enum=docuflow.apps.system.enums.AccountType)),
                ('slug', models.CharField(editable=False, max_length=255)),
                ('sorting', models.PositiveSmallIntegerField(null=True)),
            ],
            options={
                'verbose_name_plural': 'Report Types',
                'ordering': ('-account_type', 'sorting'),
            },
        ),
        migrations.RemoveField(
            model_name='permissionoption',
            name='permission_type',
        ),
        migrations.DeleteModel(
            name='MenuItem',
        ),
        migrations.DeleteModel(
            name='PermissionOption',
        ),
        migrations.DeleteModel(
            name='PermissionType',
        ),
    ]
