from rest_framework import viewsets

from docuflow.apps.system.models import CashFlowCategory, ReportType, PaymentMethod
from . import serializers


class CashFlowViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CashFlowCategorySerializer
    queryset = CashFlowCategory.objects.all()


class ReportTypeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ReportTypeSerializer
    queryset = ReportType.objects.all()


class PaymentMethodViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.PaymentMethodSerializer
    queryset = PaymentMethod.objects.all()
