from rest_framework import serializers

from docuflow.apps.system.models import ReportType, CashFlowCategory, PaymentMethod


class ReportTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportType
        fields = ('id', 'name', 'account_type', 'slug')


class CashFlowCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = CashFlowCategory
        fields = ('id', 'name', 'is_active')


class PaymentMethodSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentMethod
        fields = ('id', 'name', 'has_change', 'is_active')
