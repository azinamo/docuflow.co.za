from django.conf import settings
from django.db import models
from django.template.defaultfilters import slugify

from enumfields import EnumIntegerField
from django_countries.fields import CountryField
from safedelete.models import SafeDeleteModel, SOFT_DELETE

from docuflow.apps.common.enums import Module
from . import enums


class VersionLog(models.Model):
    label = models.CharField(max_length=255)
    version = models.CharField(max_length=255)
    major = models.CharField(max_length=255)
    minor = models.CharField(max_length=255)
    revision = models.PositiveIntegerField()
    valid_from = models.DateTimeField()

    def __str__(self):
        return f"{self.label}"

    class Meta:
        verbose_name_plural = 'Version Log'


class SystemPermission(models.Model):
    class Meta:
        managed = False  # No database table creation or deletion operations will be performed for this model
        permissions = (
            ('system_settings', 'Manage system settings'),
            ('run_reports', 'Can run reports'),
            ('view_invoice', 'Can view list of invoices'),
            ('add_to_flow', 'Can add invoice to flow'),
            ('edit_invoice', 'Can edit the invoice contents'),
            ('change_dates', 'Change Dates'),
            ('can_change_vat', 'Can change VAT'),
            ('change_due_date', 'Change due date'),
            ('account_posting', 'Account Posting'),
            ('can_add_email_source', 'Add Email Source'),
            ('can_manage_user_profiles', 'Manage User Accounts'),
            ('can_manage_company', 'Manage Company Details'),
        )


class InvoiceFieldValue(models.Model):
    label = models.CharField(max_length=255, verbose_name='Label', blank=False, null=False)
    field = models.CharField(max_length=255, blank=False, null=False)
    values = models.TextField(blank=True, null=True)
    regex = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.field}"

    class Meta:
        verbose_name_plural = 'Field Values'


class ReportType(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(max_length=255)
    account_type = EnumIntegerField(enums.AccountType)
    slug = models.CharField(max_length=255, editable=False)
    sorting = models.PositiveSmallIntegerField(null=True)

    class Meta:
        verbose_name_plural = 'Report Types'
        ordering = ('-account_type', 'sorting')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        if not self.sorting:
            self.sorting = ReportType.objects.filter().count() + 1
        return super(ReportType, self).save(*args, **kwargs)

    @property
    def account_type_name(self):
        return str(self.account_type)

    @property
    def is_asset(self):
        return self.slug in ['fixed-assets', 'current-asset', 'inventory-assets', 'cash']

    @property
    def is_liability(self):
        return self.slug in ['long-term-liabilities', 'current-liabilities', 'equity']


class PaymentMethod(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(max_length=255, verbose_name='Name')
    is_active = models.BooleanField(default=True)
    has_change = models.BooleanField(default=False, verbose_name='Has Change')

    class Meta:
        verbose_name_plural = 'Payment Methods'
        ordering = ['name']

    def __str__(self):
        return self.name

    def is_eft(self):
        return self.name.lower() == 'eft'


class CashFlowCategory(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    slug = models.CharField(max_length=255, editable=False)

    class Meta:
        verbose_name_plural = 'Cash Flows'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super(CashFlowCategory, self).save(*args, **kwargs)


class CountryManager(models.Manager):

    # noinspection PyMethodMayBeStatic
    def get_tax_rate(self, country_code=None):
        try:
            if country_code:
                country = Country.objects.get(country=country_code)
            else:
                country = Country.objects.get(country=settings.DEFAULT_COUNTRY)
            return country.tax_rate
        except Country.DoesNotExist:
            return 0


class Country(models.Model):
    country = CountryField(multiple=False)
    tax_rate = models.DecimalField(default=0, max_digits=7, decimal_places=2)

    objects = CountryManager()

    def __str__(self):
        return str(self.country)

    class Meta:
        verbose_name_plural = 'Country Settings'
        verbose_name = 'Country Settings'


class VatCode(models.Model):
    name = models.CharField(max_length=255)
    country = CountryField(multiple=False)
    percentage = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    code = models.CharField(max_length=255)
    module = EnumIntegerField(Module)

    def __str__(self):
        return str(self.code)

    class Meta:
        verbose_name_plural = 'Vat Code'
        verbose_name = 'Vat Code'


class Currency(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=10)
    symbol = models.CharField(max_length=5)

    class Meta:
        verbose_name_plural = 'Currencies'
        ordering = ['name']

    def __str__(self):
        return f"{self.code} ({self.name})"


class FixedAssetCategory(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Fixed Asset Categories'
        ordering = ['name']

    def __str__(self):
        return f"{self.name}"
