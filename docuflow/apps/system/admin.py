from django.contrib import admin
from django.db.models import Count

from . import models


@admin.register(models.Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'symbol',)

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    list_display = ('name', 'has_change', 'is_active',)

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.ReportType)
class ReportTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'account_type', 'accounts_linked')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.annotate(
            total_accounts=Count('accounts')
        )

    def accounts_linked(self, obj):
        return obj.total_accounts

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.CashFlowCategory)
class CashFlowCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('country', 'tax_rate',)

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.FixedAssetCategory)
class FixedAssetCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )

    def has_delete_permission(self, request, obj=None):
        return False
