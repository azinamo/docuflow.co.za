from django.urls import path

from . import views

urlpatterns = [
    path(r'', views.SettingsView.as_view(), name='settings'),
    path('version/log/', views.VersionLogView.as_view(), name='version_logs'),
    path('field/<str:field_id>/values/', views.UpdateFieldValuesView.as_view(), name='update_field_values'),
]
