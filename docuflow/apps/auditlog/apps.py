from django.apps import AppConfig


class AuditlogConfig(AppConfig):
    name = 'docuflow.apps.auditlog'
    verbose_name = "Audit log"
