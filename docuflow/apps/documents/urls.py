from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='documents-index'),
    path('list/', views.DocumentsListView.as_view(), name='documents-list'),
    path('upload/', views.PushView.as_view(), name='documents-push'),
    path('content/<int:pk>/', views.DocumentContentView.as_view(), name='documents-content'),
    path('ocr-document/<int:pk>/', views.OcrDocumentContentView.as_view(), name='ocr-document'),
    path('document-template/<int:pk>/', views.DocumentTemplate.as_view(), name='document-template'),
    path('scan-documents/', views.OcrDocumentsView.as_view(), name='scan-documents'),
    path('<int:pk>/restore/', views.DocumentRestoreView.as_view(), name='document-restore'),
    path('<int:pk>/delete/', views.DocumentDeleteView.as_view(), name='document-delete'),
]
