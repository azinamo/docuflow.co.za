from django.apps import AppConfig


class DocumentsConfig(AppConfig):
    name = 'docuflow.apps.documents'
