import logging

from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.views.generic import TemplateView, DetailView, FormView, ListView, View, DeleteView

from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Invoice
from .forms import UploadForm, DocumentTemplateForm
from .models import Document

logger = logging.getLogger(__name__)


class PDFTextExtractionNotAllowed(Exception):
    pass


class DocumentTemplate(LoginRequiredMixin, FormView):
    model = Document
    template_name = "documents/template.html"

    success_message = 'Template successfully updated.'

    def get_form_kwargs(self):
        kwargs = super(DocumentTemplate, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_context_data(self, **kwargs):
        context = super(DocumentTemplate, self).get_context_data(**kwargs)

        context['document'] = Document.objects.get(pk=self.kwargs['pk'])
        return context

    def get_success_url(self):
        return reverse('documents-index')

    def get_form_class(self):
        return DocumentTemplateForm

    def form_valid(self, form):
        # document = Document.objects.get(pk=self.kwargs['pk'])
        # template = Template.objects.get(pk=self.request.POST['template'])
        # if template:
        #     document.template = template
        #     document.save()

        return HttpResponseRedirect(reverse('documents-index'))


class IndexView(LoginRequiredMixin, ListView):
    model = Document
    template_name = "documents/index.html"
    context_object_name = 'documents'

    def  get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        context['DOCUMENT_URL'] = settings.DOCUMENT_IMAGE_URL
        return context

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_queryset(self):
        company = self.get_company()
        return Document.objects.filter(company=company, is_completed__isnull=True)
    # def get_context_data(self, **kwargs):
    #     print(kwargs)
    #     print(self.request.GET)
    #     print(self.request.POST)
    #     return TemplateView.get_context_data(self, **kwargs)


# Create your views here.
class DocumentsListView(LoginRequiredMixin, ListView):
    model = Document
    template_name = "documents/documents.html"
    context_object_name = 'documents'

    def get_context_data(self, **kwargs):
        context = super(DocumentsListView, self).get_context_data(**kwargs)
        context['DOCUMENT_URL'] = settings.DOCUMENT_IMAGE_URL
        return context

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_queryset(self):
        company = self.get_company()
        if self.request.GET['in_trash']:
            return Document.objects.deleted_only().filter(company=company, is_completed__isnull=True)
        else:
            return Document.objects.filter(company=company)


class DocumentDeleteView(LoginRequiredMixin, DeleteView):
    model = Document
    success_message = 'Document successfully deleted'

    def get(self, request, *args, **kwargs):
        messages.success(self.request, 'Document successfully deleted.')
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('documents-index')


class DocumentRestoreView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        document = Document.objects.all_with_deleted().get(pk=self.kwargs['pk'])
        if document:
            try:
                document.undelete()
                messages.success(self.request, 'Document successfully restored.')
            except Exception as exception:
                print(f"Error occured {str(exception)}")
                messages.error(self.request, exception)
                redirect('documents-index')
        else:
            print('Document not found and its pk {} '.format(self.kwargs['id']))
            messages.error(self.request, 'Invoice not found')

        return HttpResponseRedirect(reverse('documents-index'))


class FetchView(LoginRequiredMixin, DetailView):

    model = Document

    def render_to_response(self, context, **response_kwargs):
        """
        Override the default to return the unencrypted image/PDF as raw data.
        """

        content_types = {
            Document.TYPE_PDF: "application/pdf",
            Document.TYPE_PNG: "image/png",
            Document.TYPE_JPG: "image/jpeg",
            Document.TYPE_GIF: "image/gif",
            Document.TYPE_TIF: "image/tiff",
        }

        if self.kwargs["kind"] == "thumb":
            return HttpResponse(
                GnuPG.decrypted(self.object.thumbnail_file),
                content_type=content_types[Document.TYPE_PNG]
            )

        response = HttpResponse(
            GnuPG.decrypted(self.object.source_file),
            content_type=content_types[self.object.file_type]
        )
        response["Content-Disposition"] = 'attachment; filename="{}"'.format(
            self.object.file_name)

        return response


class PushView(LoginRequiredMixin, FormView):
    """
    A crude REST-ish API for creating documents.
    """
    form_class = UploadForm
    template_name = 'documents/upload.html'


class DocumentContentView(LoginRequiredMixin, TemplateView):
    template_name = 'documents/show.html'

    def get_context_data(self, **kwargs):
        context = super(DocumentContentView, self).get_context_data(**kwargs)

        context['document'] = Document.objects.get(pk=self.kwargs['pk'])
        return context


class OcrDocumentsView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        company = self.request.session['company']
        documents = Document.objects.filter(company=company, is_completed__isnull=True)

        if documents:
            counter = 0
            for document in documents:
                if document.template and document.template.filename:
                    result = document.extract_invoice_data()
                    if result:
                        counter = counter + 1
                        invoice = get_object_or_None(Invoice, document=document)
                        if not invoice:
                            invoice = Invoice.objects.create(
                                supplier_name=None,
                                invoice_date=result['date'],
                                company=document.company,
                                invoice_type_id=None,
                                document=self,
                                accounting_date=timezone.now(),
                            )
                            invoice.scan_update(result)
                            # messages.success(self.request, 'Invoice created with details.')
                        else:
                            invoice.scan_update(result)

            messages.success(self.request, '{} documents updated'.format(counter))
            return HttpResponseRedirect(reverse_lazy('documents-index'))
        else:
            messages.error(self.request, '0 documents found')
        return HttpResponseRedirect(reverse_lazy('documents-index'))


class OcrDocumentContentView(LoginRequiredMixin, View):
    template_name = 'documents/ocr_document.html'

    def get(self, request, *args, **kwargs):
        document = Document.objects.get(pk=self.kwargs['pk'])

        if document and document.template:
            invoice = document.extract_invoice_data()
            if invoice:
                return HttpResponseRedirect(reverse_lazy('invoice-edit-details', kwargs={'pk': invoice.id}))
            else:
                return HttpResponseRedirect(reverse_lazy('documents-index'))
        else:
            messages.error(self.request, 'Error occurred. Ensure you linked the document to the right invoice')

        return HttpResponseRedirect(reverse_lazy('documents-content', kwargs={'pk': self.kwargs['pk']}))
