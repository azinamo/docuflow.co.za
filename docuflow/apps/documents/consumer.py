import logging
import os
import uuid

from django.conf import settings
from .parsers import ParseError, RasterisedDocumentParser

from .signals import (
    document_consumer_declaration,
    document_consumption_finished,
    document_consumption_started
)


class ConsumerError(Exception):
    pass


class PDFTextExtractionNotAllowed(Exception):
    pass


class Consumer(object):
    """
    Loop over every file found in CONSUMPTION_DIR and:
      1. Convert it to a greyscale pnm
      2. Use tesseract on the pnm
      3. Encrypt and store the document in the MEDIA_ROOT
      4. Store the OCR'd text in the database
      5. Delete the document and image(s)
    """

    SCRATCH = settings.SCRATCH_DIR
    CONSUME = settings.CONSUMPTION_DIR
    CONSUME_DIR = settings.CONSUMPTION_DECRYPTED_DIR

    def __init__(self, Document):

        self.logger = logging.getLogger(__name__)
        self.logging_group = None
        self.document = Document

        try:
            os.makedirs(self.SCRATCH)
        except FileExistsError:
            pass

        self.stats = {}
        self._ignore = []

        if not self.CONSUME:
            raise ConsumerError(
                "The CONSUMPTION_DIR settings variable does not appear to be "
                "set."
            )

        if not os.path.exists(self.CONSUME):
            raise ConsumerError(
                "Consumption directory {} does not exist".format(self.CONSUME))

        self.parsers = []
        for response in document_consumer_declaration.send(self):
            self.parsers.append(response[1])

        self.parsers.append(RasterisedDocumentParser(self.CONSUME))

        if not self.parsers:
            raise ConsumerError(
                "No parsers could be found, not even the default.  "
                "This is a problem."
            )

    def log(self, level, message):
        getattr(self.logger, level)(message, extra={
            "group": self.logging_group
        })

    def consume_document(self):

        doc = os.path.join(self.CONSUME_DIR, self.document)
        print('Check the doc %s ' % doc)

        # if not re.match(FileInfo.REGEXES["title"], doc):
        #     continue

        # if not self._is_ready(doc):
        #     continue

        parser_class = self._get_parser_class(doc)

        parser_class = RasterisedDocumentParser(doc)
        print('Parser class is  %s ' % parser_class)

        self.logging_group = uuid.uuid4()
        print('Logging Group is  %s ' % self.logging_group)

        self.log("info", "Consuming {}".format(doc))
        print('info  %s ' % "Consuming {}".format(doc))

        print('Consumption started to send signal')
        document_consumption_started.send(
            sender=self.__class__,
            filename=doc,
            logging_group=self.logging_group
        )

        parsed_document = parser_class(doc)
        print('Get text of document %s' % doc )
        # thumbnail = parsed_document.get_thumbnail()
        document = None
        try:
            self._store(parser_class.get_text(), doc, '')
        except ParseError as e:
            self._ignore.append(doc)
            self.log("error", "PARSE FAILURE for {}: {}".format(doc, e))
            parser_class.cleanup()

        else:

            parser_class.cleanup()
            self._cleanup_doc(doc)

            self.log(
                "info",
                "Document {} consumption finished".format(document)
            )

            document_consumption_finished.send(
                sender=self.__class__,
                document=document,
                logging_group=self.logging_group
            )

    def consume(self):

        for doc in os.listdir(self.CONSUME):

            doc = os.path.join(self.CONSUME, doc)
            print('Check the doc %s ' % doc)

            if not os.path.isfile(doc):
                continue

            # if not re.match(FileInfo.REGEXES["title"], doc):
            #     continue

            if doc in self._ignore:
                continue

            # if not self._is_ready(doc):
            #     continue

            print('Check if duplicate file %s ' % doc)
            if self._is_duplicate(doc, self.source.company):
                self.log(
                    "info",
                    "Skipping {} as it appears to be a duplicate".format(doc)
                )
                self._ignore.append(doc)
                continue

            parser_class = self._get_parser_class(doc)

            parser_class = RasterisedDocumentParser(doc)
            print('Parser class is  %s ' % parser_class)

            if not parser_class:
                self.log(
                    "error", "No parsers could be found for {}".format(doc))
                self._ignore.append(doc)
                continue

            self.logging_group = uuid.uuid4()
            print('Logging Group is  %s ' % self.logging_group)

            self.log("info", "Consuming {}".format(doc))
            print('info  %s ' % "Consuming {}".format(doc))

            print('Consumption started to send signal')
            document_consumption_started.send(
                sender=self.__class__,
                filename=doc,
                logging_group=self.logging_group
            )

            parser_class(doc)
            print('Get text of document %s' % doc )
            # thumbnail = parsed_document.get_thumbnail()
            document = None
            try:
                self._store(parser_class.get_text(), doc, '')
            except ParseError as e:
                self._ignore.append(doc)
                self.log("error", "PARSE FAILURE for {}: {}".format(doc, e))
                parser_class.cleanup()

                continue

            else:

                parser_class.cleanup()
                self._cleanup_doc(doc)

                self.log(
                    "info",
                    "Document {} consumption finished".format(document)
                )

                document_consumption_finished.send(
                    sender=self.__class__,
                    document=document,
                    logging_group=self.logging_group
                )

    def _get_parser_class(self, doc):
        """
        Determine the appropriate parser class based on the file
        """
        options = []

        for parser in self.parsers:
            result = parser(doc)

            if result:
                options.append(result)

        self.log("info", "Parsers available: {}".format(", ".join([str(o["parser"].__name__) for o in options])))

        if not options:
            return None

        # Return the parser with the highest weight.
        return sorted(
            options, key=lambda _: _["weight"], reverse=True)[0]["parser"]

    def _store(self, text, doc, thumbnail):
        document = None
        # file_info = FileInfo.from_path(doc)
        #
        # stats = os.stat(doc)
        #
        # print('Real DEAL -- Saving record to database');
        # self.log("debug", "Saving record to database")
        #
        # created = file_info.created or timezone.make_aware(
        #             datetime.datetime.fromtimestamp(stats.st_mtime))
        #
        # with open(doc, "rb") as f:
        #     document = Document.objects.create(
        #         correspondent=file_info.correspondent,
        #         title=file_info.title,
        #         content=text,
        #         file_type=file_info.extension,
        #         checksum=hashlib.md5(f.read() + bytes(self.source.company.id)).hexdigest(),
        #         created=created,
        #         modified=created,
        #         company=self.source.company
        #     )
        #
        # relevant_tags = set(list(Tag.match_all(text)) + list(file_info.tags))
        # if relevant_tags:
        #     tag_names = ", ".join([t.slug for t in relevant_tags])
        #     self.log("debug", "Tagging with {}".format(tag_names))
        #     document.tags.add(*relevant_tags)
        #
        # # Encrypt and store the actual document
        # with open(doc, "rb") as unencrypted:
        #     with open(document.source_path, "wb") as encrypted:
        #         self.log("debug", "Encrypting the document")
        #         encrypted.write(GnuPG.encrypted(unencrypted))
        #
        # # Encrypt and store the thumbnail
        # # with open(thumbnail, "rb") as unencrypted:
        # #     with open(document.thumbnail_path, "wb") as encrypted:
        # #         self.log("debug", "Encrypting the thumbnail")
        # #         encrypted.write(GnuPG.encrypted(unencrypted))
        # #
        # # self.log("info", "Completed")
        #
        return document

    def _cleanup_doc(self, doc):
        self.log("debug", "Deleting document {}".format(doc))
        # os.unlink(doc)

    def _is_ready(self, doc):
        """
        Detect whether `doc` is ready to consume or if it's still being written
        to by the uploader.
        """

        t = os.stat(doc).st_mtime
        if self.stats.get(doc) == t:
            del(self.stats[doc])
            return True

        self.stats[doc] = t

        return False
