from django.utils.timezone import now

from .models import Profile, Role, TemporaryProfile


def get_user_profile(user):
    return Profile.objects.prefetch_related('roles').select_related('company').filter(user=user).first()


def get_permissions_list(role_id):
    role = Role.objects.prefetch_related('permission_groups', 'permission_groups__permissions').get(pk=role_id)
    permissions = []
    if role:
        for permission_group in role.permission_groups.all():
            for permission in permission_group.permissions.all():
                permissions.append(permission.codename)
    return permissions


def get_valid_temporary_profiles(profile_id):
    return TemporaryProfile.objects.select_related(
        'profile', 'profile__user'
    ).filter(
        profile_id=profile_id, start_date__lte=now().date(), expiry_date__gte=now().date()
    )
