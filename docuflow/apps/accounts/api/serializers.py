from rest_framework import serializers

from django.urls import reverse

from django.contrib.auth.models import User
from docuflow.apps.accounts.models import Profile, Role, PermissionGroup


class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = PermissionGroup
        fields = ('id', 'label', 'is_active')


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'name', 'first_name', 'last_name', 'email', 'is_active', 'last_login')

    def get_name(self, instance):
        return f"{instance.first_name} {instance.last_name}"


class SupervisorRoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = ('name', 'label', 'id')


class ManagerRoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = ('name', 'label', 'id')


class RoleSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, source='permission_groups')
    manager_role = ManagerRoleSerializer()
    supervisor_role = SupervisorRoleSerializer()
    detail_url = serializers.SerializerMethodField()
    delete_url = serializers.SerializerMethodField()
    permissions_url = serializers.SerializerMethodField()

    class Meta:
        model = Role
        fields = ('name', 'label', 'id', 'authorization_amount', 'can_be_inserted_in_flow', 'groups', 'supervisor_role',
                  'manager_role', 'detail_url', 'delete_url', 'permissions_url')
        datatables_always_serialize = ('id', 'detail_url', 'delete_url', 'permissions_url')

    def get_detail_url(self, instance):
        return reverse('edit_role', args=(instance.pk, ))

    def get_delete_url(self, instance):
        return reverse('delete_role', args=(instance.pk, ))

    def get_permissions_url(self, instance):
        return reverse('role_permission_detail', args=(instance.pk, ))


class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    roles = RoleSerializer(many=True)
    detail_url = serializers.SerializerMethodField()
    delete_url = serializers.SerializerMethodField()
    change_password_url = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ('user', 'valid_to', 'number_of_records', 'email_confirmed', 'code', 'branch', 'override_code',
                  'override_limit', 'roles', 'id', 'detail_url', 'delete_url', 'change_password_url')
        datatables_always_serialize = ('id', 'detail_url', 'change_password_url', 'delete_url')

    def get_reference(self, instance):
        return str(instance)

    def get_detail_url(self, instance):
        return reverse('edit_user_account', args=(instance.user_id, ))

    def get_delete_url(self, instance):
        return reverse('user_account_delete', args=(instance.user_id, ))

    def get_change_password_url(self, instance):
        return reverse('user_account_change_password', args=(instance.user_id, ))


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Profile
        fields = ('user', 'valid_to', 'number_of_records', 'email_confirmed')
