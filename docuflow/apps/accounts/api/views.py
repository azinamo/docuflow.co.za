from rest_framework import viewsets

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.accounts.models import Profile, Role
from .serializers import UserProfileSerializer, RoleSerializer


class AccountsViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = UserProfileSerializer

    def get_queryset(self):
        return Profile.objects.select_related(
            'user'
        ).prefetch_related('roles').filter(company=self.get_company())


class RolesViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = RoleSerializer

    def get_queryset(self):
        return Role.objects.prefetch_related(
            'permission_groups'
        ).select_related(
            'manager_role', 'supervisor_role'
        ).filter(
            company=self.get_company()
        )

