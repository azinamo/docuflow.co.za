# Generated by Django 3.1 on 2020-09-03 07:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_auto_20200811_1848'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='expiry_date',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='from_date',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='temporary_user',
        ),
        migrations.CreateModel(
            name='TemporaryProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateField()),
                ('expiry_date', models.DateField(verbose_name='Expiry Date')),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='profiles', to='accounts.profile')),
                ('temporary', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='temporary_profiles', to='accounts.profile')),
            ],
        ),
    ]
