from django.urls import path

from . import views

urlpatterns = [
    path('', views.TemporaryUserView.as_view(), name='temporary_profile_index'),
    path('edit/<int:pk>/', views.EditTemporaryUserView.as_view(), name='temporary_profile_edit'),
    path('delete/<int:pk>/', views.DeleteTemporaryUserView.as_view(), name='temporary_profile_delete'),
]
