from django import forms

from docuflow.apps.accounts.models import TemporaryProfile


class TemporaryProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TemporaryProfileForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.has_started:
            self.fields['start_date'] = forms.DateField(required=False, widget=forms.HiddenInput(), label='')

    class Meta:
        model = TemporaryProfile
        fields = ('start_date', 'expiry_date')

        widgets = {
            'start_date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'expiry_date': forms.TextInput(attrs={'class': 'form-control datepicker'})
        }

    def clean(self):
        if not self.cleaned_data['start_date']:
            self.add_error('start_date', 'From date is required')
        if not self.cleaned_data['expiry_date']:
            self.add_error('expiry_date', 'Expiry date is required')
        print(self.cleaned_data)
        if self.cleaned_data['start_date'] > self.cleaned_data['expiry_date']:
            self.add_error('start_date', 'Start date cannot be after expiry date')
            self.add_error('expiry_date', 'Expiry date cannot be before start date')

        return self.cleaned_data
