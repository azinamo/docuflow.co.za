import logging

from django.contrib import messages
from django.views.generic import ListView, View
from django.views.generic.edit import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect

from annoying.functions import get_object_or_None

from docuflow.apps.accounts.models import TemporaryProfile
from .forms import TemporaryProfileForm

logger = logging.getLogger(__name__)


# Create your views here.
class TemporaryUserView(LoginRequiredMixin, ListView):
    model = TemporaryProfile
    template_name = 'temporaryuser/index.html'
    context_object_name = 'profiles'

    def get_context_data(self, **kwargs):
        context = super(TemporaryUserView, self).get_context_data(**kwargs)
        context['redirect'] = self.request.GET.get('redirect')
        return context

    def get_queryset(self):
        return TemporaryProfile.objects.filter(
            temporary_id=self.kwargs['profile_id']
        )


class EditTemporaryUserView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = TemporaryProfile
    form_class = TemporaryProfileForm
    template_name = 'temporaryuser/edit.html'
    success_message = 'Temporary profile successfully updated'

    def get_success_url(self):
        if self.request.GET.get('redirect'):
            return self.request.GET.get('redirect')
        return reverse('temporary_profile_edit', kwargs=self.kwargs)


class DeleteTemporaryUserView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get(self, request, *args, **kwargs):
        profile = get_object_or_None(TemporaryProfile, id=self.kwargs['pk'])
        if profile:
            profile.delete()
            messages.success(self.request, 'Temporary profile deleted successfully')

        if self.request.GET.get('redirect'):
            return HttpResponseRedirect(self.request.GET.get('redirect'))
        return HttpResponseRedirect(reverse_lazy('temporary_profile_index', kwargs={'profile_id': self.kwargs['profile_id']}))
