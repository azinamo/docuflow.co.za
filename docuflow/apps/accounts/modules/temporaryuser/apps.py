from django.apps import AppConfig


class TemporaryuserConfig(AppConfig):
    name = 'docuflow.apps.accounts.modules.temporaryuser'
