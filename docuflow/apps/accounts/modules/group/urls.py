from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.PermissionGroupsView.as_view(), name='permission_group_index'),
    path('configure/', views.ConfigSystemAccessLevelsView.as_view(), name='system_access_levels'),
    path('system/access/levels/', views.PermissionGroupSystemAccessLevelsView.as_view(),
         name='group_permission_system_access_settings'),
    path('create', views.CreatePermissionGroupView.as_view(), name='create_permission_group'),
    path('edit/<int:pk>/', views.EditPermissionGroupView.as_view(), name='edit_permission_group'),
    path('delete/<int:pk>/', views.DeletePermissionGroupView.as_view(),
         name='delete_permission_group'),
    path('<int:pk>/group/members/', views.PermissionGroupMembersView.as_view(), name='permission_group_members'),
    path('save/permissions/', views.SaveGroupPermissionsView.as_view(), name='save_permission_group_permissions'),
    path('changes/', views.PermissionGroupChangesView.as_view(), name='permission_group_changes'),
    path('permission/search/', views.PermissionSearchView.as_view(), name='permission_search'),
    path('master/<int:master_company_id>/permission_group/', views.PermissionGroupsView.as_view(),
         name='master_permission_group_index'),
    path('master/<int:master_company_id>/permission_group/create', views.CreatePermissionGroupView.as_view(),
         name='create_master_permission_group'),
    path('master/<int:master_company_id>/permission_group/edit/<int:pk>/', views.EditPermissionGroupView.as_view(),
         name='edit_master_permission_group'),
    path('master/<int:master_company_id>/permission_group/delete/<int:pk>/', views.DeletePermissionGroupView.as_view(),
         name='delete_master_permission_group'),
    path('master/<int:master_company_id>/system/access/levels/', views.ConfigSystemAccessLevelsView.as_view(),
         name='master_system_access_levels'),
    path('master/<int:master_company_id>/save/permission_group/permissions/',
         csrf_exempt(views.SaveGroupPermissionsView.as_view()),
         name='save_master_permission_group_permissions'),
]
