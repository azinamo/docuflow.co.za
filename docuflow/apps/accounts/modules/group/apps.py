from django.apps import AppConfig


class GroupConfig(AppConfig):
    name = 'docuflow.apps.accounts.modules.group'
