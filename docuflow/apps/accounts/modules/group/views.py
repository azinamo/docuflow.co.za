import itertools

from django.shortcuts import redirect
from django.views.generic import ListView, TemplateView, FormView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django_filters.views import FilterView
from django.contrib.auth.models import Permission
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.http import JsonResponse
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.core.cache import cache

from docuflow.apps.accounts import services
from docuflow.apps.accounts.enums import DocuflowPermission
from docuflow.apps.accounts.forms import PermissionGroupForm
from docuflow.apps.accounts.models import Role, PermissionGroup
from docuflow.apps.accounts.filters import RoleFilter
from docuflow.apps.auditlog.models import LogEntry
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax_post
from docuflow.apps.company.models import Company

from .utils import get_group_permissions_cache_key, get_role_menu_cache_key


class PermissionGroupsView(LoginRequiredMixin, ListView):
    model = PermissionGroup
    context_object_name = 'groups'

    def get_template_names(self):
        if 'master_company_id' in self.kwargs:
            return 'group/master_index.html'
        return 'group/index.html'

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_context_data(self, **kwargs):
        context = super(PermissionGroupsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_queryset(self):
        company = self.get_company()
        return PermissionGroup.objects.filter(company=company)

    # def get_queryset(self):
    #     profile = self.request.user.profile
    #     return UserGroup.objects.filter(company=profile.company)


class CreatePermissionGroupView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    template_name = 'group/create.html'
    success_message = 'Permission group successfully created'
    success_url = reverse_lazy('system_access_levels')
    model = PermissionGroup
    form_class = PermissionGroupForm

    def get_context_data(self, **kwargs):
        context = super(CreatePermissionGroupView, self).get_context_data(**kwargs)
        context['left_list'] = self.left_list()
        context['right_list'] = []
        return context

    def left_list(self):
        results = []
        all_permissions = itertools.groupby(Permission.objects.filter())
        for namespace, permissions in all_permissions:
            permission_options = [(permission.pk, permission.name) for permission in permissions]
            results.append( (namespace, permission_options))
        return results

    def form_valid(self, form):
        try:
            user_group = form.save(commit=False)
            company = self.get_company()
            user_group.company = company
            user_group.name = f"{company.id}-{company.name}-{self.request.POST['label']}"
            user_group.is_active = True
            user_group.save()
            form.save_m2m()

            role_permissions = [int(p) for p in self.request.POST.getlist('role_permissions')]

            for permission_id in role_permissions:
                permission = Permission.objects.get(pk=permission_id)
                if permission:
                    user_group.permissions.add(permission)

            return HttpResponseRedirect(self.success_url())
        except Exception as e:
            messages.error(self.request, 'Permission group could not be save, please try again {}'.format(e.__str__()))
            return HttpResponseRedirect(reverse('create_permission_group'))


class EditPermissionGroupView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = "group/edit.html"
    model = PermissionGroup
    success_message = 'Permission group successfully updated'
    success_url = reverse_lazy('permission_group_index')

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def left_list(self):
        results = []
        all_permissions = itertools.groupby(Permission.objects.filter())
        for namespace, permissions in all_permissions:
            permission_options = [(permission.pk, permission.name) for permission in permissions]
            results.append(
               (namespace, permission_options)
            )
        return results

    def right_list(self):
        results = []
        for namespace, permissions in itertools.groupby(self.get_object().permissions.all()):
            permission_options = [
                (permission.pk, permission) for permission in permissions
            ]
            results.append(
                (namespace, permission_options)
            )
        return results

    def get_context_data(self, **kwargs):
        context = super(EditPermissionGroupView, self).get_context_data(**kwargs)
        context['left_list'] = self.left_list()
        context['right_list'] = self.right_list()
        # f or permission in Permission.objects.all():
        return context

    def get_form_class(self):
        return PermissionGroupForm

    def form_invalid(self, form):
        messages.error(self.request, 'Permission group could not be saved, please fix the errors.')
        return super(EditPermissionGroupView, self).form_invalid(form)

    def form_valid(self, form):
        try:
            permission_group = form.save(commit=False)
            company = self.get_company()
            permission_group.company = company
            permission_group.name = "{}-{}-{}".format(company.id, company.name, self.request.POST['label'])
            permission_group.is_active = True
            permission_group.save()
            # form.save_m2m()

            assigned_permissions = self.request.POST.getlist('unassigned_permissions')
            role_permissions = [int(p) for p in self.request.POST.getlist('role_permissions')]

            permission_group.permissions.clear()

            for permission_id in role_permissions:
                permission = Permission.objects.get(pk=permission_id)
                if permission:
                    permission_group.permissions.add(permission)

            messages.success(self.request, 'Permission group saved successfully')
            return HttpResponseRedirect(reverse('permission_group_index'))
        except Exception as e:
            messages.error(self.request, 'Permission group could not be save, please try again {}.'.format(e))
            return HttpResponseRedirect(reverse('edit_permission_group', kwargs={'pk': self.kwargs['pk']}))


class ConfigSystemAccessLevelsView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, FormView):
    template_name = 'group/configure.html'
    form_class = PermissionGroupForm
    success_url = reverse_lazy('role_permission_detail')
    success_message = 'Role permissions successfully.'

    def get_groups(self, company):
        return PermissionGroup.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(ConfigSystemAccessLevelsView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['groups'] = self.get_groups(company)
        context['company'] = company
        return context


class PermissionGroupSystemAccessLevelsView(LoginRequiredMixin, TemplateView):
    template_name = 'group/detail.html'

    def get_group(self):
        return PermissionGroup.objects.prefetch_related('permissions').get(pk=self.request.GET['group_id'])

    def get_context_data(self, **kwargs):
        context = super(PermissionGroupSystemAccessLevelsView, self).get_context_data(**kwargs)
        group = self.get_group()
        context['group'] = group
        context['menu_items'] = services.get_menu_items()
        context['invoice_fields'] = services.get_invoice_fields()
        context['group_permissions'] = list(group.permissions.values_list('codename', flat=True))
        return context


class SaveGroupPermissionsView(LoginRequiredMixin, View):

    # noinspection PyMethodMayBeStatic
    def has_opening_balance_access(self, permissions):
        if (
                DocuflowPermission.CAN_MANAGE_ACCOUNTANT_ACCOUNTS_OPENING_BALANCE.value in permissions
                or DocuflowPermission.CAN_MANAGE_ACCOUNTANT_SUPPLIER_OPENING_BALANCE.value in permissions
                or DocuflowPermission.CAN_MANAGE_ACCOUNTANT_CUSTOMER_OPENING_BALANCE.value in permissions
        ):
            return True
        return False

    def post(self, request, *args, **kwargs):
        if is_ajax_post(request=self.request):
            group = PermissionGroup.objects.get(pk=self.request.POST['group_id'])
            group.permissions.clear()
            code_names = []
            for field, val in self.request.POST.items():
                if val == 'yes':
                    code_names.append(field)
            if self.has_opening_balance_access(permissions=code_names):
                code_names.append(DocuflowPermission.CAN_VIEW_ACCOUNTANT_OPENING_BALANCE.value)
            permissions = Permission.objects.filter(codename__in=code_names)

            group.permissions.set(permissions)

            cache_key = get_group_permissions_cache_key(group=group)
            cache.set(cache_key, list(group.permissions.values_list('codename', flat=True)))

            for role in Role.objects.filter(permission_groups__in=[group]).all():
                cache.delete(get_role_menu_cache_key(role=role))

            return JsonResponse({'error': False,
                                 'text': 'Permission group permissions successfully updated',
                                 'reload': True
                                 })
        return redirect(reverse('system_access_levels'))


class PermissionGroupChangesView(LoginRequiredMixin, SuccessMessageMixin, TemplateView):
    template_name = 'group/changes.html'

    def get_context_data(self, **kwargs):
        context = super(PermissionGroupChangesView, self).get_context_data(**kwargs)
        context['log_entries'] = LogEntry.objects.get_for_model(PermissionGroup)
        return context


class DeletePermissionGroupView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    template_name = 'group/confirm_delete.html'
    model = PermissionGroup
    success_message = 'Permission group successfully deleted'
    success_url = reverse_lazy('permission_group_index')


class PermissionGroupMembersView(LoginRequiredMixin, TemplateView):
    template_name = 'group/members.html'

    # def get(self, request, *args, **kwargs):


class PermissionSearchView(LoginRequiredMixin, FilterView):
    model = Role
    filterset_class = RoleFilter
    template_name = 'permission/search.html'
    context_object_name = 'roles'

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_queryset(self):
        company = self.get_company()
        queryset = Role.objects.filter(company=company)
        if 'role' in self.request.GET and self.request.GET['role'] != '':
            queryset = queryset.filter(pk=int(self.request.GET['role']))
        if 'name' in self.request.GET and self.request.GET['name'] != '':
            queryset = queryset.filter(label__startswith=self.request.GET['name'])
        if 'group' in self.request.GET and self.request.GET['group'] != '':
            queryset = queryset.filter(pk=self.request.GET['group'])

        return queryset
