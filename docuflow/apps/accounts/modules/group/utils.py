from docuflow.apps.accounts.models import Role, PermissionGroup


def get_group_permissions_cache_key(group: PermissionGroup) -> str:
    return f'group_permissions_{group.id}'


def get_role_cache_key(role: Role) -> str:
    return f'role_permissions_groups_{role.id}'


def get_role_menu_cache_key(role: Role) -> str:
    return f'role_{role.id}_menu_items'

