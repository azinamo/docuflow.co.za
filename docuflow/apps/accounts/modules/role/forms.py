from django import forms

from docuflow.apps.accounts.models import Profile, Role, PermissionGroup


# Create your forms here.
class RoleForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(RoleForm, self).__init__(*args, **kwargs)
        self.fields['manager_role'].queryset = Role.objects.filter(company=company)
        self.fields['supervisor_role'].queryset = Role.objects.filter(company=company)
        self.fields['label'] = forms.CharField(label='Name')
        # self.fields['supervisor_role']['label'] = 'Purchase Order Role'
        # self.fields['manager_role']['label'] = 'Escalation Role'
        self.fields['reminder_to_role_days'] = forms.IntegerField(
            label='Escalate document after X days', required=False)
        self.fields['send_reminder_after_inbox'] = forms.IntegerField( label='Send a reminder after  X days in inbox',
                                                                       required=False)
        self.fields['send_reminder_before_due_date_days'] = forms.IntegerField(label='Send reminder X days before '
                                                                                     'due date', required=False)

    class Meta:
        model = Role
        fields = ('name', 'label', 'authorization_amount', 'can_be_inserted_in_flow', 'manager_role', 'supervisor_role',
                  'reminder_to_role_days', 'send_reminder_after_inbox', 'send_reminder_before_due_date_days',
                  'direct_reminder', 'permission_groups')


class RoleAuthorizationForm(forms.Form):
    REGISTER_CHOICES = (
        ('', '--please select--'),
        ('account', 'Accounts'),
        ('department', 'Department'),
        ('branch', 'Branch'),
        ('area', 'Area'),
    )
    register = forms.ChoiceField(choices=REGISTER_CHOICES, label='Register', required=False)


class RoleOverviewForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(RoleOverviewForm, self).__init__(*args, **kwargs)
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    account = forms.ChoiceField(required=False, label='Account', choices=(('*', '*'),))
    show_details = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_company_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_account_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_object_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_item_type_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_expense_type_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    active = forms.ChoiceField(required=False, label='Active', choices=(('*', '*'),))
    number_of_lines = forms.CharField(required=False)