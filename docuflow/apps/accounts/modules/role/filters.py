import django_filters
from docuflow.apps.accounts.models import Role, PermissionGroup
from docuflow.apps.company.models import Company


def roles(request):
    if request is None:
        return Role.objects.none()
    if 'company' in request.session:
        return Role.objects.filter(company=request.session['company'])
    return Role.objects.none()


def permission_groups(request):
    if request is None:
        return PermissionGroup.objects.none()
    if 'company' in request.session:
        return PermissionGroup.objects.filter(company=request.session['company'])
    return Role.objects.none()


class RoleFilter(django_filters.FilterSet):
    label = django_filters.CharFilter(lookup_expr='icontains')
    name = django_filters.CharFilter(lookup_expr='icontains')
    permission_groups = django_filters.ModelChoiceFilter(queryset=permission_groups)

    class Meta:
        model = Role
        fields = ['label', 'name', 'permission_groups']

