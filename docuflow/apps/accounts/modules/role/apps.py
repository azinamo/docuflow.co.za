from django.apps import AppConfig


class RoleConfig(AppConfig):
    name = 'docuflow.apps.accounts.modules.role'
