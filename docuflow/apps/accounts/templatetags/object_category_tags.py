from django import template

from docuflow.apps.company.models import Company

register = template.Library()


@register.inclusion_tag('account_postingline/company_object_categories.html')
def company_object_categories(company_id):
    company = Company.objects.get(pk=company_id)
    object_categories = []
    accounts = []
    if company:
        object_categories = company.generic_category.all()
        accounts = company.company_accounts.all()

    return {'object_categories': object_categories, 'accounts': accounts}


@register.simple_tag(name='line_model_value')
def account_posting_line_link_model_value(linked_models, line):
    for linked_model in linked_models:
        if line.id in linked_model['values']:
            return linked_model['values'][line.id]
    return None
