import logging
from django import template
from django.core.cache import cache

from annoying.functions import get_object_or_None

from docuflow.apps.accounts.models import Role, Profile
from docuflow.apps.accounts.selectors import get_valid_temporary_profiles
from docuflow.apps.company.models import Branch
from docuflow.apps.period.models import Year
from docuflow.apps.period.services import get_current_year
from docuflow.apps.common import utils

register = template.Library()
logger = logging.getLogger(__name__)


@register.inclusion_tag('roles/profile_roles.html')
def profile_roles(profile_id, user_id, company_id, role_id):
    profile = get_user_profile(company_id=company_id, user_id=user_id, default_profile_id=profile_id)
    cache_key = utils.get_profile_roles_cache_key(profile_id=profile_id, company_id=company_id)
    roles = cache.get(cache_key)
    if not roles:
        roles = get_profile_roles(profile)
        cache.set(cache_key, roles)
    return {'profile_roles': roles, 'profile_id': profile_id, 'role_id': role_id, 'user_id': user_id,
            'company_id': company_id}


@register.inclusion_tag('temporaryuser/nav/profiles.html')
def temporary_profiles(profile_id, user_id, company_id=None):
    profile = get_user_profile(company_id=company_id, user_id=user_id, default_profile_id=profile_id)
    temp_profiles = get_valid_temporary_profiles(profile_id)
    # TODO - Ensure the temporary profile is set when we add or remove them on the management page
    cache_key = f'temporary_profiles_{profile.pk}'

    profiles = cache.get(cache_key)
    if not profiles:
        profiles = [profile]
        for temporary_profile in temp_profiles:
            profiles.append(temporary_profile.temporary)
        cache.set(cache_key, profiles)
    return {'profiles': profiles, 'user_id': user_id, 'the_profile': profile, 'company_id': company_id}


@register.inclusion_tag('branch/profile_branches.html')
def profile_branches(profile_id, company_id):
    if company_id:
        branches = Branch.objects.filter(company_id=company_id)
    else:
        branches = cache.get(utils.get_profile_branch_cache_key(profile_id=profile_id, company_id=company_id))
        if not branches:
            profile = Profile.objects.get(pk=profile_id)
            branches = []
            if profile:
                branches = get_profile_branches(profile, profile.branch)
            cache.set(f'profile_{profile_id}_branches', branches)
    return {'profile_branches': branches, 'profile_id': profile_id, 'company_id': company_id}


@register.inclusion_tag('years/profile_years.html')
def profile_years(profile_id, company_id, year_id=None):
    years_key = f"profile__{profile_id}__years"
    years = cache.get(years_key)
    if not years:
        years = Year.objects.filter(company_id=company_id).all()
        cache.set(years_key, years)
    if not year_id:
        if len(years) > 0:
            open_years = [year.id for year in years if year.is_active]
            if len(open_years) > 0:
                year_id = open_years[0]
            else:
                year_id = years[0].id
        else:
            year_id = None
    return {'profile_years': years, 'default_year_id': year_id}


@register.simple_tag(name='current_role')
def current_role(role_id, profile_id, company_id):
    cache_key = utils.get_profile_role_cache_key(
        profile_id=profile_id, company_id=company_id
    )
    role = cache.get(cache_key)
    if not role:
        role = Role.objects.select_related(
            'company', 'manager_role', 'supervisor_role'
        ).prefetch_related('permission_groups').get(pk=role_id)
        cache.set(cache_key, role)
    return role


@register.simple_tag(name='current_profile')
def current_profile(company_id, user_id, default_profile_id):
    return get_user_profile(company_id, user_id, default_profile_id)


@register.simple_tag(name='current_branch')
def current_branch(profile_id, company_id, branch_id=None):
    cache_key = utils.get_profile_branch_cache_key(profile_id=profile_id, company_id=company_id)
    branch = cache.get(cache_key)
    if not branch:
        if branch_id and type(branch_id) == int:
            branch = get_object_or_None(Branch, pk=branch_id)
        elif profile_id and type(profile_id) == int:
            profile = Profile.objects.select_related('branch').get(pk=profile_id)
            if profile.branch:
                branch = profile.branch
            else:
                branches = Branch.objects.filter(company=profile.company)
                for branch in branches:
                    if branch.is_default:
                        return branch
                branch = branches.first()
        elif company_id and type(company_id) == int:
            branches = Branch.objects.filter(company_id=company_id)
            for branch in branches:
                if branch.is_default:
                    return branch
            branch = branches.first()
        cache.set(cache_key, branch)
    return branch if branch else '-'


@register.simple_tag(name='role_can')
def role_can(perm):
    return False


@register.simple_tag(name='current_year')
def current_year(session):
    # TODO - Year should be per user logged in
    return get_current_year(profile_id=session['profile'], company_id=session['company'])


def get_user_profile(company_id, user_id, default_profile_id):
    cache_key = utils.get_profile_cache_key(user_id=user_id, company_id=company_id)
    profile = cache.get(cache_key)
    if not profile:
        profile_id = None
        if default_profile_id:
            profile_id = default_profile_id
        elif isinstance(profile, int):
            profile_id = profile
        try:
            profile = Profile.objects.select_related(
                'role', 'branch', 'year', 'user', 'company'
            ).prefetch_related(
                'roles', 'branches',
            ).get(pk=profile_id)
        except Profile.DoesNotExist:
            profile = None
        cache.set(cache_key, profile)
    return profile


def get_temporary_profile_roles(profile):
    roles = []
    for temporary_profile in profile.temporary_profiles.all():
        for temporary_profile_role in temporary_profile.roles.all():
            roles.append(temporary_profile_role)
    return roles


def get_profile_branches(profile, default_branch):
    branches = []
    for branch in profile.branches.filter(is_invoicable=True):
        branches.append(branch)
    if default_branch and default_branch not in branches:
        branches.append(default_branch)
    return branches


def get_temporary_profile_branches(profile):
    branches = []
    for temporary_profile in profile.temporary_profiles.all():
        for temporary_profile_branch in temporary_profile.branches.all():
            branches.append(temporary_profile_branch)
    return branches


def get_profile_roles(profile):
    roles = []
    for role in profile.roles.all():
        roles.append(role)
    return roles
