import pprint
import logging

from django import template
from django.core.cache import cache

from docuflow.apps.accounts.models import Role
from docuflow.apps.accounts import services
from docuflow.apps.common import utils

register = template.Library()

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


@register.inclusion_tag('roles/menu.html')
def display_role_menu(role_id, profile_id, company_id):
    role = None
    if not role_id or role_id == '':
        menu_items = services.get_menu_items(role=None)
    else:
        try:
            cache_key = utils.get_profile_role_cache_key(
                profile_id=profile_id, company_id=company_id
            )
            role = cache.get(cache_key)
            if not role:
                role = Role.objects.select_related(
                    'company', 'manager_role', 'supervisor_role'
                ).prefetch_related('permission_groups').get(pk=role_id)
                cache.set(cache_key, role)

            menu_items = services.get_menu_items(role=role)
        except Role.DoesNotExist:
            menu_items = services.get_menu_items(role=None)
    return {'menu_items': menu_items, 'user': role}
