import pprint
from django import template
from django.utils.translation import gettext as _

from docuflow.apps.accounts.permission_options import options

register = template.Library()
pp = pprint.PrettyPrinter(indent=4)


@register.inclusion_tag('group/group_permissions.html')
def permission_group_permissions(permission_group):

    assigned_group_permissions = []
    for permission in permission_group.permissions.all():
        assigned_group_permissions.append(permission.codename)

    group_permissions = [
            {
                'id': 'notice', 'label': 'Notice Board',
                'children': [
                    {'id': 'board', 'label': 'Notice Board', 'options': options['d']},
                ]
            },
            {
                'id': 'my_profile', 'label': 'My Profile',
                'children': [
                    {'id': 'module', 'label': 'My Profile',
                     'options': options['a']},
                    {'id': 'temporary_users', 'label': 'Specify Temporary Users for your roles',
                     'options': options['a']},
                    {'id': 'username', 'label': 'Username', 'options': options['d']},
                    {'id': 'user_code', 'label': 'User code', 'options': options['d']},
                    {'id': 'first_name', 'label': 'First name', 'options': options['d']},
                    {'id': 'last_name', 'label': 'Last name', 'options': options['d']},
                    {'id': 'email', 'label': 'Email address', 'options': options['d']},
                    {'id': 'is_active', 'label': 'Active', 'options': options['d']},
                    {'id': 'valid_to', 'label': 'Valid to', 'options': options['d']},
                    {'id': 'number_of_records', 'label': 'Display number of invoices',
                     'options': options['d']},
                    {'id': 'password', 'label': 'Password', 'options': options['d']},
                    {'id': 'temporary_users', 'label': 'Temporary users', 'options': options['d']},
                    {'id': 'from_date', 'label': 'From date', 'options': options['d']},
                    {'id': 'expiry_date', 'label': 'Expiry Date', 'options': options['d']},
                ]
            },
            {
                'label': 'Sales',
                'id': 'sales',
                'icon': 'fa-sort-amount-asc fw',
                'url': '#',
                'children': [
                    {
                        'id': 'sales_module', 'label': 'Sales Module', 'options': options['d']
                    },
                    {
                        'label': _('Estimates'), 'id': 'estimates', 'icon': 'fa-user fw', 'options': options['d']
                    },
                    {
                        'label': _('Back order'), 'id': 'back_order', 'icon': 'fa-user fw','options': options['d']
                    },
                    {
                        'label': _('Picking'), 'id': 'picking', 'icon': 'fa-user fw', 'options': options['d']
                    },
                    {
                        'label': _('Deliveries'), 'id': 'deliveries', 'icon': 'fa-user fw', 'options': options['d']
                    },
                    {
                        'label': _('Documents'), 'id': 'documents', 'icon': 'fa-user fw', 'options': options['d']
                    },
                    {
                        'label': _('Cash Up'), 'id': 'cash_up', 'icon': 'fa-user fw', 'options': options['d'],
                    },
                    {
                        'label': _('Customer Receipts'), 'id': 'receipts', 'icon': 'fa-user fw', 'options': options['d']
                    },
                    {
                        'label': _('Customer Ledger'), 'id': 'customer_ledger', 'icon': 'fa-user fw', 'options': options['d'],
                    }
                ]
            },
            {
                'label': 'Purchases', 'id': 'purchases', 'icon': 'fa-tags fw', 'url': '', 'permissions': ['all'],
                'children': [
                    {
                        'id': 'purchase_module', 'label': 'Purchases Module', 'options': options['d']
                    },
                    {
                        'label': 'My Documents', 'id': 'manage_invoices', 'icon': 'fa-user fw', 'permissions': ['yes'],
                        'options': options['d']
                    },
                    {
                        'label': 'Document Logs', 'id': 'manage_log', 'icon': 'fa-user fw', 'permissions': ['yes'],
                        'options': options['d']
                    },
                    {
                        'label': 'General PO Request', 'id': 'purchase_order_request', 'icon': 'fa-puzzle-piece fw',
                        'permissions': ['yes'], 'options': options['d'],
                    }
                ]
            },
            {
                'label': 'Inventory Perpetual FIFO',
                'id': 'inventory',
                'icon': 'fa-indent fw',
                'url': '',
                'permissions': ['all'],
                'children': [
                    {
                        'id': 'inventory_module',
                        'label': 'Inventory Perpetual FIFO module',
                        'options': options['d']
                    },
                    {
                        'label': _('Inventory Register'),
                        'id': 'inventory_register',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Recipes'),
                        'id': 'create_inventory_recipe',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Purchase orders'),
                        'id': 'inventory_purchase_orders',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Goods Received'),
                        'id': 'goods_received',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Goods Returned'),
                        'id': 'goods_returned',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Manufacturing'),
                        'id': 'manufacturing',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('View Inventory Item'),
                        'id': 'view_inventory_item',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Adjust Inventory Levels'),
                        'id': 'adjust_inventory_levels',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Move Inventory between Branches'),
                        'id': 'move_inventory_between_branches',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Campaigns'),
                        'id': 'campaigns',
                        'icon': 'fa-user fw',
                        'url': '#',
                        'permissions': ['all'],
                        'options': options['d'],
                    },

                    {
                        'label': _('Inventory Order Suggestion'),
                        'id': 'inventory_order_suggestion',
                        'icon': 'fa-user fw',
                        'url': '#',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Group Levels'),
                        'id': 'group_level',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Stock Count'),
                        'id': 'group_level',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': _('Ledger'),
                        'id': 'inventory_ledger',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },

                ]
            },
            {
                'label': 'Accountant',
                'id': 'accountant',
                'icon': 'fa-money fw',
                'url': '',
                'permissions': ['all'],
                'children': [
                    {
                        'id': 'accountant_module',
                        'label': 'Accountant module',
                        'options': options['d']
                    },
                    {
                        'label': 'Capture', 'id': 'capture', 'icon': 'fa-user fw', 'permissions': ['all'],
                        'options': options['d'],
                        'children': [
                            {
                                'label': 'General Journals', 'id': 'general_journals', 'icon': 'fa-user fw',
                                'url': '#', 'permissions': ['all'], 'options': options['d']
                            },
                            {
                                'label': 'Journal Templates', 'id': 'journal_templates', 'icon': 'fa-user fw',
                                'url': '#', 'permissions': ['all'], 'options': options['d']
                            },
                            {
                                'label': 'Customer Payments', 'id': 'customer_payments',
                                'icon': 'fa-user fw', 'url': '#', 'permissions': ['all'], 'options': options['d']
                            },
                            {
                                'label': 'Suppliers Payments', 'id': 'pay_suppliers', 'icon': 'fa-user fw',
                                'permissions': ['all'], 'options': options['d'],
                            },
                            {
                                'label': 'Vat Submission', 'id': 'vat_submissions', 'icon': 'fa-user fw', 'url': '#',
                                'permissions': ['all'], 'options': options['d']
                            },
                            {
                                'label': 'Budgets', 'id': 'budgets', 'icon': 'fa-user fw', 'url': '#',
                                'permissions': ['all'], 'options': options['d']
                            },
                            {
                                'label': 'Opening Balances', 'id': 'opening_balances', 'icon': 'fa-grav fw',
                                'permissions': ['all'], 'options': options['d'],
                                'children': [
                                    {
                                        'label': _('Accounts'),
                                        'id': 'accounts',
                                        'permissions': ['yes'],
                                        'options': options['d']
                                    },
                                    {
                                        'label': _('Supplier'),
                                        'id': 'supplier',
                                        'permissions': ['yes'],
                                        'options': options['d']
                                    },
                                    {
                                        'label': _('Customer'),
                                        'id': 'customer',
                                        'permissions': ['yes'],
                                        'options': options['d']
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        'label': 'Updates Sales Module', 'id': 'updates_sales_module', 'icon': 'fa-user fw', 'url': '#',
                        'permissions': ['all'], 'options': options['d'],
                        'children': [
                            {
                                'label': 'Invoices', 'id': 'invoices', 'icon': 'fa-user fw', 'url': '#',
                                'permissions': ['all'], 'options': options['d']
                            },
                            {
                                'label': 'Customer Payments', 'id': 'customer_payments', 'icon': 'fa-user fw',
                                'url': '#', 'permissions': ['all'], 'options': options['d']
                            }
                        ]
                    },
                    {
                        'label': 'Updates Purchases Module', 'id': 'update_purchases_module', 'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'], 'options': options['d'],
                        'children': [
                            {
                                'label': 'Incoming Invoices', 'id': 'incoming', 'icon': 'fa-user fw',
                                'permissions': ['all'], 'options': options['d']
                            },
                            {
                                'label': 'Account Posting', 'id': 'posting', 'icon': 'fa-user fw', 'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Pay Suppliers', 'id': 'payment', 'icon': 'fa-user fw', 'permissions': ['all'],
                                'options': options['d']
                            }
                        ]
                    },
                    {
                        'label': 'Updates Inventory Module', 'id': 'updates_inventory_module', 'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'], 'options': options['d'],
                        'children': [
                            {
                                'label': 'Goods Received Notes Journal',
                                'id': 'goods_received',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Goods Returned Notes Journal',
                                'id': 'goods_returned',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                        ]
                    },
                    {
                        'label': 'Reports',
                        'id': 'reports',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                        'children': [
                            {
                                'label': 'Customer Age Analysis',
                                'id': 'customer_age_analysis',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Customer Statements',
                                'id': 'customer_statements',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Supplier Age Analysis',
                                'id': 'supplier_age_analysis',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Distribution Balances',
                                'id': 'distribution_balance',
                                'icon': 'fa-grav fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Invoiced Not Yet Posted Control Account',
                                'id': 'invoices_not_yet_posted',
                                'icon': 'fa-grav fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Invoice Flow Status',
                                'id': 'invoice_flow_status',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Inventory Valuation',
                                'id': 'inventory_valuation',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Inventory Ledger',
                                'id': 'inventory_ledger',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Account Listing',
                                'id': 'account_listing',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Budgets',
                                'id': 'budgets',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Provisional Tax Estimate',
                                'id': 'provisional_tax_estimate',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                        ]
                    },
                    {
                        'label': 'Month End Close',
                        'id': 'pay_suppliers',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': 'Month End Reports',
                        'id': 'pay_suppliers',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                        'children': [
                            {
                                'label': 'General Ledger',
                                'id': 'general_ledger',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Balance Sheet',
                                'id': 'balance_sheet',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Income Statement',
                                'id': 'income_statement',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Income Statement by Period',
                                'id': 'income_statement_by_period',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Cash Flow Statement',
                                'id': 'cash_flow_statement',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                        ]
                    },
                ]
            },
            {
                'label': 'Payroll',
                'id': 'payroll',
                'icon': 'fa-list-alt fw',
                'url': '',
                'permissions': ['all'],
                'children': [
                    {
                        'id': 'payroll_module',
                        'label': 'Payroll module',
                        'options': options['d']
                    },
                    {
                        'label': 'Weekly Payroll',
                        'id': 'manage_invoices',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': 'Monthly Payroll',
                        'id': 'monthly_payroll',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': 'Update',
                        'id': 'update',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                        'children': [
                            {
                                'label': 'Weekly Pay Journal',
                                'id': 'manage_invoices',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Monthly Pay Journal',
                                'id': 'manage_invoices',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                        ]
                    },
                    {
                        'label': 'Print',
                        'id': 'purchase_order_request',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                        'children': [
                            {
                                'label': 'Individual Payslip',
                                'id': 'manage_invoices',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'All Payslips',
                                'id': 'manage_invoices',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'EMP201 Submission',
                                'id': 'manage_invoices',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                            {
                                'label': 'Workman’s Compensation',
                                'id': 'manage_invoices',
                                'icon': 'fa-user fw',
                                'permissions': ['all'],
                                'options': options['d']
                            },
                        ]
                    },
                    {
                        'label': 'Employee Register',
                        'id': 'purchase_order_request',
                        'icon': 'fa-puzzle-piece fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                    {
                        'label': 'Provision Register',
                        'id': 'pay_suppliers',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d'],
                    },
                ]
            },
            {
                'label': 'Important Documents',
                'id': 'certificates',
                'icon': 'fa-drivers-license fw',
                'url': '#',
                'permissions': ['all'],
                'children': [
                    {
                        'id': 'certificate_module',
                        'label': 'Certificate module',
                        'options': options['d']
                    },
                    {'id': 'create', 'label': 'Certificates', 'options': options['d']},
                ]
            },
            {
                'label': 'Fixed Asset Register',
                'id': 'fixed_asset',
                'icon': 'fa-registered fw',
                'url': '',
                'permissions': ['yes'],
                'children': [
                    {
                        'id': 'module',
                        'label': 'Fixed Asset Register Module',
                        'options': options['d']
                    },
                    {
                        'label': _('Asset Category'),
                        'id': 'category',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Assets'),
                        'id': 'register',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Opening Balances'),
                        'id': 'opening_balances',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Disposals'),
                        'id': 'disposals',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Update'),
                        'id': 'updates',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                    {
                        'label': _('Depreciation Adjustments'),
                        'id': 'depreciation_adjustments',
                        'icon': 'fa-user fw',
                        'permissions': ['all'],
                        'options': options['d']
                    },
                ]
            },
            {
                'id': 'reports', 'label': 'Reports',
                'children': [
                    {'id': 'module', 'label': _('Reports Module'), 'options': options['d']},
                    {'id': 'documents', 'label': _('Show All Documents'), 'options': options['d']},
                    {'id': 'run_bottlenecks_report', 'label': _('Run bottlenecks report'), 'options': options['d']},
                    {'id': 'run_requisition_flow_status', 'label': _('Run requisitions flow status report'),
                     'options': options['d']},
                    {'id': 'invoice_processing_days', 'label': _('Invoice Processing days'), 'options': options['d']},
                    {'id': 'role_overview', 'label': _('Run role overview report'), 'options': options['d']},
                    {
                        'id': 'invoice', 'label': 'Invoice',
                        'children': [
                            {'id': 'run_captured', 'label': 'Run captured invoices report', 'options': options['d']},
                            {'id': 'run_show', 'label': 'Run show invoices report', 'options': options['d']},
                            {'id': 'run_account_balances', 'label': 'Run account balances report',
                             'options': options['d']},
                            {'id': 'run_bottlenecks', 'label': 'Run bottlenecks report', 'options': options['d']},
                            {'id': 'run_processing_days', 'label': 'Run invoice processing days report',
                             'options': options['d']},
                            {'id': 'run_payment_forecast', 'label': 'Run payment forecast report',
                             'options': options['d']},
                            {'id': 'run_flow_status', 'label': 'Run invoice flow status report',
                             'options': options['d']},
                            {'id': 'run_supplier_balance', 'label': 'Run supplier balance report',
                             'options': options['d']},
                            {'id': 'change_cash_flow', 'label': 'Change cash flow for an invoice in reports',
                             'options': options['a']},
                            {'id': 'invoice_change_due_date',
                             'label': 'Change the due date and payment date for an invoice in reports',
                             'options': options['b']},
                            {'id': 'invoice_view_with_classification',
                             'label': 'View invoices with classification in reports', 'options': options['d']},
                            {'id': 'invoice_limited_access_to_records', 'label': 'Limited access to records in invoice',
                             'options': options['g']},
                        ]
                    },
                    {
                        'id': 'administration', 'label': _('Administration'),
                        'children': [
                            {'id': 'run_documents_per_month', 'label': _('Run documents per month report'),
                             'options': options['d']}
                        ]
                    }
                ]
            },
            {
                'id': 'administration', 'label': 'Administration',
                'children': [
                    {'id': 'administration_module', 'label': 'Administration module', 'options': options['d']},
                    {
                        'id': 'general', 'label': 'General Information',
                        'children': [
                            {'id': 'users', 'label': 'Administration of users', 'options': options['a']},
                            {'id': 'authorization_amount_for_posting',
                             'label': 'Administration of authorization amount for a posting',
                             'options': options['a']},
                            {'id': 'favourites', 'label': 'Administration of favourites(Company, Account, Object)',
                             'options': options['a']},
                            {'id': 'permissions_to_access',
                             'label': 'Administration of permissions to access Companies, Accounts, Objects '
                                      '(Requires that the role is at least Role Administator)',
                             'options': options['d']},
                            {'id': 'permissions_groups_and_rules_for_functions',
                             'label': 'Administration of permission settings for permission groups and rules for '
                                      'functions', 'options': options['b']},
                            {'id': 'flow_proposals', 'label': 'Administration of flow proposals',
                             'options': options['a']},
                            {'id': 'user_defined_groups', 'label': 'Administration of user defined groups',
                             'options': options['a']},
                            {'id': 'account_posting_proposals', 'label': 'Administration of account posting proposals',
                             'options': options['a']},
                            {'id': 'roles',
                             'label': 'Administration of roles (Requires that the role be Role Administrator)',
                             'options': options['a']},
                            {'id': 'temporary_users',
                             'label': 'Administration of temporary users(Requires that the role be Role Administrator)',
                             'options': options['a']},
                            {'id': 'role_administrator', 'label': 'Role Administrator', 'options': options['d']},
                        ]
                     },
                     {
                        'id': 'administration_invoice', 'label': 'Invoice',
                        'children': [
                            {'id': 'change_accounting_date', 'label': 'Change accounting date', 'options': options['d']},
                            {'id': 'series', 'label': 'Administration of invoice series', 'options': options['a']},
                            {'id': 'references', 'label': 'Administration of reference', 'options': options['a']},
                            {'id': 'reference_matches', 'label': 'Administration of reference matches',
                             'options': options['a']},
                            {'id': 'rule_based_flow_auxillary_roles', 'label': 'Manage rule-based flow auxillary roles',
                             'options': options['a']},
                            {'id': 'payment_terms', 'label': 'Administration of payment terms', 'options': options['a']},
                            {'id': 'alias_for_units', 'label': 'Manage aliases for units', 'options': options['a']}
                        ]
                     },
                     {
                        'id': 'register', 'label': 'Register',
                        'children': [
                            {'id': 'accounts', 'label': 'Administration of accounts', 'options': options['j']},
                            {'id': 'posting_relationships', 'label': 'Administration of posting relationships',
                             'options': options['j']},
                            {'id': 'companies_and_addresses', 'label': 'Administration of companies and addresses',
                             'options': options['j']},
                            {'id': 'currencies', 'label': 'Administration of currencies', 'options': options['a']},
                            {'id': 'manage_non_banking_days', 'label': 'Manage of non-banking days',
                             'options': options['a']},
                            {'id': 'objects', 'label': 'Administration of objects', 'options': options['j']},
                            {'id': 'accounting_periods', 'label': 'Administration of accounting periods',
                             'options': options['a']},
                            {'id': 'supplier', 'label': 'Administration of suppliers', 'options': options['a']},
                            {'id': 'vat_codes_types', 'label': 'Administration of vat codes and vat types',
                             'options': options['a']
                             },
                        ]
                     }
                ]
            },
            {
                'id': 'system', 'label': 'System',
                'children': [
                    {'id': 'module', 'label': 'System Module', 'options': options['j'] }
                ]
            },
            {
                'id': 'invoice', 'label': 'Documents Settings',
                'children': [
                    {'id': 'process_supplier_invoices', 'label': 'Invoice Product - Process Supplier Invoices',
                     'options': options['a']},
                    {'id': 'header', 'label': 'Manage Invoice Header', 'options': options['b']},
                    {'id': 'process_expenditure_allocations', 'label': 'Process an invoice\'s expenditure allocations',
                     'options': options['c']},
                    {'id': 'process_vat_posting', 'label': 'Process an invoice\'s vat posting',
                     'options': options['c']},
                    {'id': 'sign_account_postings', 'label': 'Sign account postings for an invoice',
                     'options': options['d']},
                    {'id': 'sign_variation_authorization_amount',
                     'label': 'Sign an invoice with a variable authorization amount',
                     'options': options['d']},
                    {'id': 'specify_re_invoicing_for_invoice_posting',
                     'label': 'Specify re - invoicing for an invoice\'s posting',
                     'options': options['c']},
                    {'id': 'distribute_account_posting',
                     'label': 'Distribute an invoice\'s account posting over period',
                     'options': options['d']},
                    {'id': 'specify_asset_details',
                     'label': 'Specify asset details in an invoice\'s account posting',
                     'options': options['a']},
                    {'id': 'classification',
                     'label': 'Classification of invoices',
                     'options': options['e']},
                    {'id': 'specify_part_payment',
                     'label': 'Specify part - payment for an invoice',
                     'options': options['a']},
                    {'id': 'manage_flow',
                     'label': 'Manage an invoice\'s flow',
                     'options': options['l']},
                    {'id': 'fast_sign_in_inbox',
                     'label': 'Fast-sign in inbox for my invoice',
                     'options': options['d']},
                    {'id': 'manage_log',
                     'label': 'Manage the invoice log',
                     'options': options['a']},
                    {'id': 'edit_supplier',
                     'label': 'Edit supplier for an invoice',
                     'options': options['d']},
                    {'id': 'edit_currency',
                     'label': 'Edit currency and amount for an invoice', 'options': options['a']},
                    {'id': 'change_accounting_date',
                     'label': 'Change accounting date for invoice', 'options': options['b']},
                    {'id': 'change_due_date',
                     'label': 'Change due date for invoice', 'options': options['b']},
                    {'id': 'change_account_posting_external_order',
                     'label': 'Change the account posting for external order', 'options': options['d']},
                    {'id': 'change_comments', 'label': 'Edit comments', 'options': options['d']}
                ]
            },
            {
                'id': 'invoice_fields', 'label': 'Invoice Fields',
                'children': [
                    {'id': 'company', 'label': 'Company', 'options': options['d']},
                    {'id': 'supplier', 'label': 'Supplier', 'options': options['d']},
                    {'id': 'supplier_name', 'label': 'Supplier Name', 'options': options['d']},
                    {'id': 'supplier_number', 'label': 'Supplier Number', 'options': options['d']},
                    {'id': 'supplier_account_ref', 'label': 'Supplier Account Ref', 'options': options['d']},
                    {'id': 'document_type', 'label': 'Document Types', 'options': options['d']},
                    {'id': 'link_purchase_order', 'label': 'Link Purchase Order', 'options': options['d']},
                    {'id': 'link_reciept', 'label': 'Link Receipt', 'options': options['d']},
                    {'id': 'document_number', 'label': 'Document Number', 'options': options['d']},
                    {'id': 'vat_number', 'label': 'Vat Number', 'options': options['d']},
                    {'id': 'reference_1', 'label': 'Reference 1', 'options': options['d']},
                    {'id': 'reference_2', 'label': 'Reference 2', 'options': options['d']},
                    {'id': 'flow_proposal', 'label': 'Flow Proposal', 'options': options['d']},
                    {'id': 'account_posting_proposal', 'label': 'Account Posting Proposal', 'options': options['d']},
                    {'id': 'accounting_date', 'label': 'Accounting Date', 'options': options['d']},
                    {'id': 'document_date', 'label': 'Document Date', 'options': options['d']},
                    {'id': 'due_date', 'label': 'Due Date', 'options': options['d']},
                    {'id': 'date_paid', 'label': 'Date Paid', 'options': options['d']},
                    {'id': 'currency', 'label': 'Currency', 'options': options['d']},
                    {'id': 'total_amount', 'label': 'Total Amount', 'options': options['d']},
                    {'id': 'vat_amount', 'label': 'Vat Amount', 'options': options['d']},
                    {'id': 'vat_type', 'label': 'Vat Type', 'options': options['d']},
                    {'id': 'net_amount', 'label': 'Net Amount', 'options': options['d']},
                    {'id': 'comments', 'label': 'Comments', 'options': options['d']},
                    {'id': 'attachment', 'label': 'Attachment', 'options': options['d']}
                ]
            }
        ]
    # permission_items = []
    # for group_permission in group_permissions:
    #     if group_permission.get('children'):
    #         children = group_permission.pop('children')
    #         group_permission['children'] = get_child_permission([group_permission], children)
    #
    #         group_permission['permission_name'] = group_permission['id']
    #         permission_items.append(group_permission)
    #     else:
    #         group_permission['permission_name'] = group_permission['id']
    #         permission_items.append(group_permission)
    # pp.pprint(permission_items)
    return {'permission_group_permissions': group_permissions, 'assigned_group_permissions': assigned_group_permissions}


def get_child_permission(parent_items, permission_items):
    for i, permission_item in enumerate(permission_items):
        if permission_item.get('children'):
            children = permission_item.pop('children')
            parent_items.append(permission_item)
            permission_item['children'] = get_child_permission(parent_items, children)
            permission_item['permission_name'] = create_name(parent_items, permission_item['id'])
        else:
            pp.pprint(parent_items)
            pp.pprint(permission_item)
            it = parent_items.append(permission_item)
            print(f"Parent items {type(parent_items)} and item is {type(permission_item)}")
            print(parent_items)
            permission_item['permission_name'] = create_name(parent_items)
        permission_items[i] = permission_item
    return permission_items


@register.inclusion_tag('group/group_permissions.html')
def permission_children(permission_group):
    return {'group_permission': permission_group}


@register.inclusion_tag('group/permission_select.html')
def permission_select(menu_item, assigned_permission):
    permission = str(menu_item.get('permission'))
    permission_options = []

    for option in menu_item.get('options', []):
        option['selected'] = ''
        option['name'] = permission
        if option['value'] == 'yes' and permission in assigned_permission:
            option['selected'] = 'selected'

        permission_options.append(option)
    return {'permission': permission,
            'menu_item': menu_item,
            'permission_options': permission_options,
            'assigned_group_permissions': assigned_permission
            }


def create_name(group_permission, child_permission, permission):
    name = get_id(group_permission)

    group_name = get_id(child_permission)
    if group_name:
        name = f"{name}_{group_name}" if name else group_name

    child_name = get_id(permission)
    if child_name:
        name = f"{name}_{child_name}" if name else child_name
    return name


def get_id(permission):
    if permission and permission.get('id') != '':
        return permission['id']
    return ''


#
# def create_name(parent_items, prefix=''):
#     name = ''
#     for parent_item in parent_items:
#         item_name = get_id(parent_item)
#         if item_name:
#             name = f"{name}_{item_name}" if name else item_name
#     return f"{prefix}_{name}" if prefix else name
#
#
# def get_id(permission):
#     if permission and permission.get('id') != '':
#         return permission['id']
#     return ''
