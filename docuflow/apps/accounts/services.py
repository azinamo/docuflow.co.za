from typing import List, Any, Dict

from django.core.cache import cache
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from docuflow.apps.accounts.models import Role
from docuflow.apps.accounts.modules.group.utils import get_group_permissions_cache_key, get_role_cache_key, get_role_menu_cache_key
from . import enums


def get_menu_items(role: Role = None) -> list:
    cache_key = f'menu_items_' if role is None else f"{get_role_menu_cache_key(role=role)}"
    role_menu_items = cache.get(cache_key)
    if role_menu_items:
        return role_menu_items

    dashboard_text = role.company.name if role else 'Dashboard'

    purchase_update_children = [
        {
            'label': _('Incoming Invoices'),
            'id': 'incoming',
            'icon': 'fa-user fw',
            'url': reverse('journal_for_incoming_invoice'),
            'permission': enums.DocuflowPermission.CAN_MANAGE_INCOMING_INVOICE.value,
            'options': get_permission_options('d')
        },
        {
            'label': _('Account Posting'),
            'id': 'posting',
            'icon': 'fa-user fw',
            'url': reverse('journal_for_posting_invoice'),
            'permission': enums.DocuflowPermission.CAN_MANAGE_POSTING_INVOICE.value,
            'options': get_permission_options('d')
        },
        {
            'label': _('Supplier payments'),
            'id': 'payment',
            'icon': 'fa-user fw',
            'url': reverse('journal_for_payment_invoice'),
            'permission': enums.DocuflowPermission.CAN_MANAGE_PAYMENT_UPDATE.value,
            'options': get_permission_options('d')
        }]

    menu_items = [
        {
            'label': dashboard_text,
            'id': 'dashboard',
            'icon': 'fa-dashboard fw',
            'url': reverse('dashboard'),
            'permission': '*'
        },
        {
            'label': _('Notice Board'),
            'id': 'notice_board',
            'icon': 'fa-bell-o fw',
            'url': reverse('notice_board'),
            'permission': enums.DocuflowPermission.CAN_VIEW_NOTICE_BOARD.value
        },
        {
            'label': _('Profile'),
            'id': 'my_profile',
            'icon': 'fa-user fw',
            'url': reverse('current_user_edit'),
            'permission': enums.DocuflowPermission.CAN_MANAGE_PROFILE.value,
            'options': get_permission_options('d')
        },
        {
            'label': _('Sales'),
            'id': 'sales',
            'icon': 'fa-sort-amount-asc fw',
            'url': '#',
            'permission': enums.DocuflowPermission.CAN_VIEW_SALES_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('Estimates'),
                    'id': 'estimates',
                    'icon': 'fa-user fw',
                    'url': reverse('estimates_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_ESTIMATES.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Back order'),
                    'id': 'back_order',
                    'icon': 'fa-user fw',
                    'url': reverse('sales_back_order_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_BACK_ORDER.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Picking'),
                    'id': 'picking',
                    'icon': 'fa-user fw',
                    'url': reverse('picking_slips'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_PICKING_SLIP.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Deliveries'),
                    'id': 'deliveries',
                    'icon': 'fa-user fw',
                    'url': reverse('sales_delivery_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_DELIVERY.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Documents'),
                    'id': 'documents',
                    'icon': 'fa-user fw',
                    'url': reverse('sales_documents_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_SALES_INVOICES.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Cash Up'),
                    'id': 'cash_up',
                    'icon': 'fa-user fw',
                    'url': reverse('cash_up_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_CASH_UP.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Customer Receipts'),
                    'id': 'receipts',
                    'icon': 'fa-user fw',
                    'url': reverse('receipts_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_RECEIPT.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Customer Ledger'),
                    'id': 'customer_ledger',
                    'icon': 'fa-user fw',
                    'url': reverse('customer_ledger'),
                    'permission': enums.DocuflowPermission.CAN_VIEW_CUSTOMER_LEDGER.value,
                    'options': get_permission_options('d'),
                }
            ]
        },
        {
            'label': _('Purchases'),
            'id': 'purchases',
            'icon': 'fa-tags fw',
            'url': '',
            'permission': enums.DocuflowPermission.CAN_VIEW_PURCHASES_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('My Documents'),
                    'id': 'manage_invoices',
                    'icon': 'fa-user fw',
                    'url': reverse('all-invoices'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_PURCHASE_INVOICE.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Document Logs'),
                    'id': 'manage_log',
                    'icon': 'fa-user fw',
                    'url': reverse('invoice_log'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_DOCUMENT_LOGS.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('General PO Request'),
                    'id': 'purchase_order_request',
                    'icon': 'fa-puzzle-piece fw',
                    'url': reverse('purchaseorder_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_PURCHASE_ORDER.value,
                    'options': get_permission_options('d'),
                }
            ]
        },
        {
            'label': _('Inventory Perpetual FIFO'),
            'id': 'inventory',
            'icon': 'fa-indent fw',
            'url': '#',
            'permission': enums.DocuflowPermission.CAN_VIEW_INVENTORY_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('Inventory Register'),
                    'id': 'inventory_register',
                    'icon': 'fa-user fw',
                    'url': reverse('inventory_index'),
                    'permission': enums.DocuflowPermission.CAN_VIEW_INVENTORY.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Recipes'),
                    'id': 'create_inventory_recipe',
                    'icon': 'fa-user fw',
                    'url': reverse('recipe_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_RECIPE.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Purchase orders'),
                    'id': 'inventory_purchase_orders',
                    'icon': 'fa-user fw',
                    'url': reverse('purchaseorder_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_PURCHASE_ORDER.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Goods Received'),
                    'id': 'goods_received',
                    'icon': 'fa-user fw',
                    'url': reverse('inventory_received_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_GOODS_RECEIVED.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Goods Returned'),
                    'id': 'goods_returned',
                    'icon': 'fa-puzzle-piece fw',
                    'url': reverse('inventory_returned_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_GOODS_RETURNED.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Manufacturing'),
                    'id': 'manufacturing',
                    'icon': 'fa-puzzle-piece fw',
                    'url': reverse('manufacturing_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_INVENTORY_MANUFACTURING.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('View Inventory Item'),
                    'id': 'view_inventory_item',
                    'icon': 'fa-puzzle-piece fw',
                    'url': reverse('view_inventory_item'),
                    'permission': enums.DocuflowPermission.CAN_VIEW_INVENTORY.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Adjust Inventory Levels'),
                    'id': 'adjust_inventory_levels',
                    'icon': 'fa-puzzle-piece fw',
                    'url': reverse('stock_adjustment'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_INVENTORY_ADJUSTMENT.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Move Inventory between Branches'),
                    'id': 'move_inventory_between_branches',
                    'icon': 'fa-user fw',
                    'url': reverse('move_branch_stock'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_INVENTORY_MOVEMENT.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Campaigns'),
                    'id': 'campaigns',
                    'icon': 'fa-user fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_MANAGE_INVENTORY_CAMPAIGN.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Inventory Order Suggestion'),
                    'id': 'inventory_order_suggestion',
                    'icon': 'fa-user fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_VIEW_INVENTORY_ORDER_SUGGESTION.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Group Levels'),
                    'id': 'group_level',
                    'icon': 'fa-user fw',
                    'url': reverse('group_level_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_GROUP_LEVEL.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Stock Count'),
                    'id': 'group_level',
                    'icon': 'fa-user fw',
                    'url': reverse('stock_count_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_STOCK_COUNT.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Ledger'),
                    'id': 'inventory_ledger',
                    'icon': 'fa-user fw',
                    'url': reverse('inventory_ledger'),
                    'permission': enums.DocuflowPermission.CAN_VIEW_INVENTORY_LEDGER.value,
                    'options': get_permission_options('d'),
                },

            ]
        },
        {
            'label': _('Accountant'),
            'id': 'accountant',
            'icon': 'fa-money fw',
            'url': '',
            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('Capture'),
                    'id': 'capture',
                    'icon': 'fa-user fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_CAPTURE.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('Journals'),
                            'id': 'general_journals',
                            'icon': 'fa-grav fw',
                            'url': reverse('journals_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_JOURNAL.value,
                            'options': get_permission_options('d')
                        },
                        # {
                        #     'label': _('Distribution Journals'),
                        #     'id': 'create_distribution_journal',
                        #     'icon': 'fa-user fw',
                        #     'url': reverse('create_distribution_journal'),
                        #     'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_DISTRIBUTION_JOURNAL.value,
                        #     'options': get_permission_options('d')
                        # },
                        {
                            'label': _('Journal Templates'),
                            'id': 'journal_templates',
                            'icon': 'fa-user fw',
                            'url': '#',
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_JOURNAL_TEMPLATE.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Customer Receipts'),
                            'id': 'customer_payments',
                            'icon': 'fa-user fw',
                            'url': '#',
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_CUSTOMER_RECEIPT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Suppliers Payments'),
                            'id': 'pay_suppliers',
                            'icon': 'fa-user fw',
                            'url': reverse('unpaid_invoice_by_supplier'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_SUPPLIER_PAYMENT.value,
                            'options': get_permission_options('d'),
                        },
                        {
                            'label': _('Supplier Deposit Payment'),
                            'id': 'sundry_payment',
                            'icon': 'fa-payment fw',
                            'url': reverse('sundry_payment'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_SUNDRY_PAYMENT.value,
                            'options': get_permission_options('d'),
                        },
                        {
                            'label': _('Allocate Sundry Payments'),
                            'id': 'allocate_sundry_payment',
                            'icon': 'fa-payment fw',
                            'url': reverse('allocate_sundry_payment'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_ALLOCATE_SUNDRY_PAYMENT.value,
                            'options': get_permission_options('d'),
                        },
                        {
                            'label': _('Vat Submission'),
                            'id': 'vat_submissions',
                            'icon': 'fa-user fw',
                            'url': reverse('vat_report'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_VAT_SUBMISSION.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Budgets'),
                            'id': 'budgets',
                            'icon': 'fa-user fw',
                            'url': reverse('budget_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_BUDGET.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Opening Balance'),
                            'id': 'opening_balances',
                            'icon': 'fa-user fw',
                            'url': reverse('opening_balances'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_OPENING_BALANCE.value,
                            'children': [
                                {
                                    'label': _('Accounts'),
                                    'id': 'accounts',
                                    'icon': 'fa-grav fw',
                                    'url': reverse('opening_balances'),
                                    'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_ACCOUNTS_OPENING_BALANCE.value,
                                    'options': get_permission_options('d')
                                },
                                {
                                    'label': _('Supplier'),
                                    'id': 'supplier',
                                    'icon': 'fa-grav fw',
                                    'url': reverse('supplier_opening_balances'),
                                    'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_SUPPLIER_OPENING_BALANCE.value,
                                    'options': get_permission_options('d')
                                },
                                {
                                    'label': _('Customer'),
                                    'id': 'customer',
                                    'icon': 'fa-grav fw',
                                    'url': reverse('customer_opening_balances'),
                                    'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_CUSTOMER_OPENING_BALANCE.value,
                                    'options': get_permission_options('d')
                                }
                            ]
                        },
                    ]
                },
                {
                    'label': _('Updates Sales Module'),
                    'id': 'updates_sales_module',
                    'icon': 'fa-user fw',
                    'url': reverse('sales_updates_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_UPDATE_SALES.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Updates Purchases Module'),
                    'id': 'update_purchases_module',
                    'icon': 'fa-puzzle-piece fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_UPDATE_PURCHASE_MODULE.value,
                    'options': get_permission_options('d'),
                    'children': []
                },
                {
                    'label': _('Reports'),
                    'id': 'accountant_reports',
                    'icon': 'fa-puzzle-piece fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_REPORTS.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('Customer Age Analysis'),
                            'id': 'customer_age_analysis',
                            'icon': 'fa-user fw',
                            'url': reverse('customer_age_analysis'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_CUSTOMER_AGE_ANALYSIS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Customer Statements'),
                            'id': 'customer_statements',
                            'icon': 'fa-user fw',
                            'url': reverse('customer_statements'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_CUSTOMER_STATEMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Supplier Age Analysis'),
                            'id': 'supplier_age_analysis',
                            'icon': 'fa-user fw',
                            'url': reverse('supplier_age_analysis'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_SUPPLIER_AGE_ANALYSIS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Payment Forecasting'),
                            'id': 'payment_forecasting',
                            'icon': 'fa-grav fw',
                            'url': reverse('forecasting_report'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_PAYMENT_FORECASTING.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Distribution Balances'),
                            'id': 'distribution_balance',
                            'icon': 'fa-grav fw',
                            'url': reverse('distribution_balance'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_DISTRIBUTION_BALANCE.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Distributions'),
                            'id': 'distributions_index',
                            'icon': 'fa-grav fw',
                            'url': reverse('distributions_index'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_DISTRIBUTIONS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Goods Received - Awaiting Invoice'),
                            'id': 'goods_received_not_invoiced',
                            'icon': 'fa-grav fw',
                            'url': reverse('goods_received_not_invoiced'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_GOODS_RECEIVED_AWAITING_INVOICE.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Invoiced Not Yet Posted'),
                            'id': 'invoices_not_yet_posted',
                            'icon': 'fa-grav fw',
                            'url': reverse('invoices_not_yet_posted'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_INVOICE_NOT_POSTED.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Invoice Flow Status'),
                            'id': 'invoice_flow_status',
                            'icon': 'fa-user fw',
                            'url': reverse('invoice_flow_status'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_INVOICE_FLOW_STATUS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Inventory Valuation'),
                            'id': 'inventory_valuation',
                            'icon': 'fa-user fw',
                            'url': reverse('inventory_valuation'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_INVENTORY_VALUATION.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Inventory Ledger'),
                            'id': 'inventory_ledger',
                            'icon': 'fa-user fw',
                            'url': reverse('inventory_ledger'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_INVENTORY_LEDGER.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Provisional Tax Estimate'),
                            'id': 'provisional_tax_estimate',
                            'icon': 'fa-user fw',
                            'url': reverse('provisional_tax_estimate_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_PROVISIONAL_TAX_ESTIMATE.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Payments'),
                            'id': 'payments',
                            'icon': 'fa-grav fw',
                            'url': reverse('payment_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_PAYMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Cash Receipts'),
                            'id': 'payments',
                            'icon': 'fa-grav fw',
                            'url': reverse('receipts_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_CASH_RECEIPTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Supplier Ledger'),
                            'id': 'supplier_ledger',
                            'icon': 'fa-grav fw',
                            'url': reverse('supplier_ledger'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_SUPPLIER_LEGER.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('History'),
                            'id': 'history',
                            'icon': 'fa-user fw',
                            'url': reverse('supplier_history'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_HISTORY.value,
                            'children': [
                                {
                                    'label': _('Supplier'),
                                    'id': 'supplier_history',
                                    'icon': 'fa-grav fw',
                                    'url': reverse('supplier_history'),
                                    'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_SUPPLIER_HISTORY.value,
                                    'options': get_permission_options('d')
                                },
                                {
                                    'label': _('Customer'),
                                    'id': 'customer_history',
                                    'icon': 'fa-grav fw',
                                    'url': reverse('customer_history'),
                                    'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_CUSTOMER_HISTORY.value,
                                    'options': get_permission_options('d')
                                }
                            ]
                        },
                    ]
                },
                {
                    'label': _('Month End Close'),
                    'id': 'pay_suppliers',
                    'icon': 'fa-user fw',
                    'url': reverse('month_close'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_ACCOUNTANT_MONTH_END_CLOSE.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Month End Reports'),
                    'id': 'pay_suppliers',
                    'icon': 'fa-user fw',
                    'url': reverse('unpaid_invoice_by_supplier'),
                    'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_MONTH_END_REPORTS.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('General Ledger'),
                            'id': 'general_ledger',
                            'icon': 'fa-user fw',
                            'url': reverse('general_ledger'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_GENERAL_LEDGER_REPORT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Suffix'),
                            'id': 'suffix_report',
                            'icon': 'fa-user fw',
                            'url': reverse('suffix_report'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_SUFFIX_REPORT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Balance Sheet'),
                            'id': 'balance_sheet',
                            'icon': 'fa-user fw',
                            'url': reverse('balance_sheet'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_BALANCE_SHEET_REPORT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Income Statement'),
                            'id': 'income_statement',
                            'icon': 'fa-user fw',
                            'url': reverse('income_statement'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_INCOME_STATEMENT_REPORT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Income Statement by Period'),
                            'id': 'income_statement_by_period',
                            'icon': 'fa-user fw',
                            'url': reverse('period_income_statement'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_INCOME_STATEMENT_BY_PERIOD_REPORT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Cash Flow Statement'),
                            'id': 'cash_flow_statement',
                            'icon': 'fa-user fw',
                            'url': reverse('cash_flow_statement'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_CASH_STATEMENT_REPORT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Trial Balance'),
                            'id': 'trial_balance',
                            'icon': 'fa-user fw',
                            'url': reverse('trial_balance'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_ACCOUNTANT_TRIAL_BALANCE_REPORT.value,
                            'options': get_permission_options('d')
                        },
                    ]
                },
            ]
        },
        {
            'label': _('Payroll'),
            'id': 'payroll',
            'icon': 'fa-list-alt fw',
            'url': '',
            'permission': enums.DocuflowPermission.CAN_VIEW_PAYROLL_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('Setup Payroll'),
                    'id': 'setup_payroll',
                    'icon': 'fa-user fw',
                    'url': reverse('payroll_setup'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_PAYROLL_SETUP.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Statutory Setup'),
                    'id': 'statutory_setup',
                    'icon': 'fa-user fw',
                    'url': reverse('statutory_setup'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_PAYROLL_STATUTORY_SETUP.value,
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Weekly Payroll'),
                    'id': 'weekly_payroll',
                    'icon': 'fa-user fw',
                    'url': reverse('all-invoices'),
                    'permission': 'can_manage_payroll_weekly',
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Monthly Payroll'),
                    'id': 'monthly_payroll',
                    'icon': 'fa-user fw',
                    'url': reverse('invoice_log'),
                    'permission': 'can_manage_payroll_monthly',
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Update'),
                    'id': 'update',
                    'icon': 'fa-puzzle-piece fw',
                    'url': '#',
                    'permission': 'can_manage_payroll_update',
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('Weekly Pay Journal'),
                            'id': 'weekly_pay_journal',
                            'icon': 'fa-user fw',
                            'url': reverse('all-invoices'),
                            'permission': 'can_manage_payroll_pay_journal',
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Monthly Pay Journal'),
                            'id': 'monthly_pay_journal',
                            'icon': 'fa-user fw',
                            'url': reverse('all-invoices'),
                            'permission': 'can_manage_payroll_pay_journal',
                            'options': get_permission_options('d')
                        },
                    ]
                },
                {
                    'label': _('Print'),
                    'id': 'print',
                    'icon': 'fa-puzzle-piece fw',
                    'url': '#',
                    'permission': 'can_manage_payroll_pay_journal',
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('Individual Payslip'),
                            'id': 'individual_payslips',
                            'icon': 'fa-user fw',
                            'url': reverse('all-invoices'),
                            'permission': 'can_manage_payroll_pay_journal',
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('All Payslips'),
                            'id': 'all_payslips',
                            'icon': 'fa-user fw',
                            'url': reverse('all-invoices'),
                            'permission': 'can_manage_payroll_pay_journal',
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('EMP201 Submission'),
                            'id': 'emp_submission',
                            'icon': 'fa-user fw',
                            'url': reverse('all-invoices'),
                            'permission': 'can_manage_payroll_pay_journal',
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Workman’s Compensation'),
                            'id': 'workman_compensation',
                            'icon': 'fa-user fw',
                            'url': reverse('all-invoices'),
                            'permission': 'can_manage_payroll_pay_journal',
                            'options': get_permission_options('d')
                        },
                    ]
                },
                {
                    'label': _('Employee Register'),
                    'id': 'employee_register',
                    'icon': 'fa-puzzle-piece fw',
                    'url': reverse('create_purchaseorder'),
                    'permission': 'can_manage_payroll_pay_journal',
                    'options': get_permission_options('d'),
                },
                {
                    'label': _('Provision Register'),
                    'id': 'pay_suppliers',
                    'icon': 'fa-user fw',
                    'url': '#',
                    'permission': 'can_manage_payroll_pay_journal',
                    'options': get_permission_options('d'),
                },
            ]
        },
        {
            'label': _('Important Documents'),
            'id': 'certificates',
            'icon': 'fa-folder fw',
            'url': reverse('certificates'),
            'permission': enums.DocuflowPermission.CAN_VIEW_IMPORTANT_DOCUMENTS_MODULE.value,
            'options': get_permission_options('d'),
        },
        {
            'label': _('Fixed Asset Register'),
            'id': 'fixed_asset',
            'icon': 'fa-registered fw',
            'url': '#',
            'permission': enums.DocuflowPermission.CAN_VIEW_FIXED_ASSET_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('Register'),
                    'id': 'register',
                    'icon': 'fa-user fw',
                    'url': reverse('asset_register_index'),
                    'permission': enums.DocuflowPermission.CAN_VIEW_FIXED_ASSET_REGISTER.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Asset Category'),
                    'id': 'category',
                    'icon': 'fa-user fw',
                    'url': reverse('asset_category_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_FIXED_ASSET_CATEGORY.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Assets'),
                    'id': 'register',
                    'icon': 'fa-user fw',
                    'url': reverse('fixed_asset_index'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_FIXED_ASSETS.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Opening Balances'),
                    'id': 'opening_balances',
                    'icon': 'fa-user fw',
                    'url': reverse('fixed_asset_opening_balances'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_FIXED_ASSETS_OPENING_BALANCE.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Disposals'),
                    'id': 'disposals',
                    'icon': 'fa-user fw',
                    'url': reverse('fixed_asset_disposal'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_FIXED_ASSETS_DISPOSALS.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Update'),
                    'id': 'updates',
                    'icon': 'fa-user fw',
                    'url': reverse('fixed_asset_update'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_FIXED_ASSETS_UPDATES.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Depreciation Adjustments'),
                    'id': 'depreciation_adjustments',
                    'icon': 'fa-user fw',
                    'url': reverse('depreciation_adjustments'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_FIXED_ASSETS_DEPRECIATION_ADJUSTMENT.value,
                    'options': get_permission_options('d')
                },
            ]
        },
        {
            'label': _('Reports'),
            'id': 'reports',
            'icon': 'fa-bar-chart fw',
            'url': '#',
            'options': get_permission_options('d'),
            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_MODULE.value,
            'children': [
                {
                    'label': _('Show All Documents'),
                    'id': 'documents',
                    'icon': 'fa-grav fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_VIEW_DOCUMENTS_REPORTS.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('At Logger'),
                            'id': 'type', 'icon': 'fa-user fw',
                            'url': f"{reverse('show_invoice_report')}?status=arrived-at-logger",
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_LOGGER_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('In the flow'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': f"{reverse('show_invoice_report')}?status=in-the-flow",
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_IN_FLOW_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Completed'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': f"{reverse('show_invoice_report')}?status=completed",
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_COMPLETED_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Paid'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': f"{reverse('show_invoice_report')}?status=paid",
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_PAID_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Rejected'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': f"{reverse('show_invoice_report')}?status=rejected",
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_REJECTED_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('PO Approved'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': f"{reverse('show_invoice_report')}?status=po-approved",
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_PO_APPROVED_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('All Records'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': reverse('show_invoice_report'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_ALL_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Final Signed'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': f"{reverse('show_invoice_report')}?status=final-signed",
                            'permission': enums.DocuflowPermission.CAN_VIEW_REPORTS_FINAL_SIGNED_DOCUMENTS.value,
                            'options': get_permission_options('d')
                        },
                    ]
                },
                {
                    'label': _('Files'),
                    'id': 'files_balances',
                    'icon': 'fa-grav fw',
                    'url': reverse('show_balance_index'),
                    'permission': ['yes'],
                    'options': get_permission_options('d')
                },
                {
                    'label': _('Balances'), 'id': 'balances', 'icon': 'fa-grav fw', 'url': reverse('show_balance_index'),
                    'permission': ['balances_yes'], 'options': get_permission_options('d')
                },
                {
                    'label': _('Bottlenecks'), 'id': 'bottlenecks', 'icon': 'fa-grav fw',
                    'url': reverse('show_bottleneck_index'), 'permission': ['bottlenecks_yes'], 'options': get_permission_options('d')
                },
                {
                    'label': _('Invoice Processing days'), 'id': 'invoice_processing_days', 'icon': 'fa-grav fw',
                    'url': reverse('invoice_processing_days_report'), 'permission': ['yes'], 'options': get_permission_options('d')
                },
                {
                    'label': _('Roles Overview'), 'id': 'role_overview', 'icon': 'fa-grav fw',
                    'url': reverse('roles_overview_report'), 'permission': ['yes'], 'options': get_permission_options('d')
                },

            ]
        },
        {
            'label': _('Administration'),
            'id': 'administration',
            'icon': 'fa-database fw',
            'url': '#',
            'permission': enums.DocuflowPermission.CAN_VIEW_ADMINISTRATION_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('General Information'),
                    'id': 'general',
                    'icon': 'fa-users fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_VIEW_ADMINISTRATION_GENERAL_INFORMATION.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('User Accounts'), 'id': 'user_account', 'icon': 'fa-user fw',
                            'url': reverse('user_accounts_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_USER_ACCOUNTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Authorization Amount'), 'id': 'authorization_amount', 'icon': 'fa-user fw',
                            'url': reverse('supplier_authorization_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_SUPPLIER_AUTHORIZATION_AMOUNT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Permissions'), 'id': 'permission', 'icon': 'fa-user fw',
                            'url': reverse('roles_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_PERMISSIONS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('System Access Levels'), 'id': 'system_access_levels', 'icon': 'fa-user fw',
                            'url': reverse('system_access_levels'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_ACCESS_LEVELS.value,
                            'options': get_permission_options('d')
                        },

                        {
                            'label': _('Flow Proposal'), 'id': 'flow_proposal', 'icon': 'fa-user fw',
                            'url': reverse('workflow_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_FLOW_PROPOSAL.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Intervention'), 'id': 'rule_based_flow', 'icon': 'fa-user fw',
                            'url': reverse('intervention_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_FLOW_INTERVENTION.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Account posting proposal'), 'id': 'account_posting_proposal',
                            'icon': 'fa-user fw', 'url': reverse('account_posting_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_ACCOUNT_POSTING_PROPOSAL.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Objects for expenses split'), 'id': 'posting_object_index',
                            'icon': 'fa-user fw',
                            'url': reverse('posting_object_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_OBJECT_EXPENSES_SPLIT.value,
                            'options': get_permission_options('d')
                        },

                        {
                            'label': _('Temporary users'), 'id': 'temporary_users', 'icon': 'fa-user fw', 'url': '',
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_TEMPORARY_USERS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Role administrators'), 'id': 'role_administrators', 'icon': 'fa-user fw',
                            'url': reverse('system_access_levels'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_ROLE_ADMINISTRATORS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Roles'), 'id': 'roles', 'icon': 'fa-user fw', 'url': reverse('roles_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_ROLES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Setup'), 'id': 'setup', 'icon': 'fa-user fw', 'url': '',
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_SETUP.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Change password'), 'id': 'change_password', 'icon': 'fa-user fw',
                            'url': reverse('current-user-password-change'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_CHANGE_PASSWORD.value,
                            'options': get_permission_options('d')
                        },
                    ]
                },
                {
                    'label': _('Documents'),
                    'id': 'invoice',
                    'icon': 'fa-users fw',
                    'url': '#',
                    'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_DOCUMENTS.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('Files'), 'id': 'files', 'icon': 'fa-user fw', 'url': reverse('file_type_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_DOCUMENT_FILES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Document types'), 'id': 'type', 'icon': 'fa-user fw',
                            'url': reverse('invoice_types'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_DOCUMENT_TYPES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('References matching'), 'id': 'reference_matching', 'icon': 'fa-user fw',
                            'url': '#',
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_DOCUMENT_REFERENCE_MATCHING.value,
                            'options': get_permission_options('d')
                        }
                    ]
                },
                {
                    'label': _('Register'), 'id': 'register', 'icon': 'fa-users fw', 'url': '',
                    'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('Accounts'), 'id': 'accounts', 'icon': 'fa-user fw', 'url': reverse('account_list'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_ACCOUNTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Objects'), 'id': 'object', 'icon': 'fa-user fw',
                            'url': reverse('company_objects_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_OBJECTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Default Accounts'), 'id': 'default_accounts', 'icon': 'fa-user fw',
                            'url': reverse('default_accounts'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_DEFAULT_ACCOUNTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Suppliers'), 'id': 'suppliers', 'icon': 'fa-user fw',
                            'url': reverse('supplier-list'),
                            'options': get_permission_options('d'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_SUPPLIERS.value
                        },
                        {
                            'label': _('Inventory'), 'id': 'account', 'icon': 'fa-user fw',
                            'url': reverse('import_inventory'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_INVENTORIES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Years'), 'id': 'years', 'icon': 'fa-user fw', 'url': reverse('years_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_YEARS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Periods'), 'id': 'periods', 'icon': 'fa-user fw', 'url': reverse('periods_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_PERIODS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Email Source'), 'id': 'manage_email_source', 'icon': 'fa-user fw',
                            'url': reverse('imap-sources-index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_EMAIL_SOURCE.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Vat Codes'), 'id': 'vat_codes', 'icon': 'fa-user fw',
                            'url': reverse('vat_code_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_VAT_CODES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Manage Company'), 'id': 'company', 'icon': 'fa-user fw',
                            'url': reverse('manage_company'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_MANAGE_COMPANY.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Bank Account'), 'id': 'banks', 'icon': 'fa-user fw',
                            'url': reverse('bank_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_BANK_ACCOUNT.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Branches'), 'id': 'branches', 'icon': 'fa-user fw',
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_BRANCH.value,
                            'url': reverse('branch_index'),
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Payment Options'), 'id': 'payment_options', 'icon': 'fa-user fw',
                            'url': reverse('payment_method_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_PAYMENT_OPTIONS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Non banking days'), 'id': 'non_banking_days', 'icon': 'fa-user fw',
                            'url': reverse('nonbankingdays-list'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_NON_BANKING_DAYS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Blacklisted suppliers'), 'id': 'blacklisted_suppliers', 'icon': 'fa-user fw',
                            'url': reverse('supplier-list'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_BLACKLISTED_SUPPLIERS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Measures'), 'id': 'units', 'icon': 'fa-user fw',
                            'url': reverse('measure_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_MEASURES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Units'), 'id': 'units', 'icon': 'fa-user fw',
                            'url': reverse('unit_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_UNITS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Customer'), 'id': 'customers', 'icon': 'fa-user fw',
                            'url': reverse('customer_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_CUSTOMERS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Salesman'), 'id': 'salesman', 'icon': 'fa-user fw',
                            'url': reverse('salesman_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_SALESMAN.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Delivery Method'), 'id': 'delivery_methods', 'icon': 'fa-user fw',
                            'url': reverse('delivery_method_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_PAYMENT_METHODS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Delivery Terms'), 'id': 'delivery_methods', 'icon': 'fa-user fw',
                            'url': reverse('delivery_term_index'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_ADMINISTRATION_REGISTER_DELIVERY_TERMS.value,
                            'options': get_permission_options('d')
                        },

                    ]
                }
            ]
        },
        {
            'label': _('Settings'),
            'id': 'settings',
            'icon': 'fa-gears fw',
            'url': '',
            'permission': enums.DocuflowPermission.CAN_VIEW_SETTINGS_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('Security'), 'id': 'security_settings', 'icon': 'fa-lock fw',
                    'url': reverse('security_settings'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_SECURITY_SETTINGS.value,
                    'options': get_permission_options('d')
                },
                {
                    'label': _('All'), 'id': 'notification_settings', 'icon': 'fa-user fw',
                    'url': reverse('notification-settings'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_NOTIFICATION_SETTINGS.value,
                    'options': get_permission_options('d')
                },
            ]
        },
        {
            'label': _('System'),
            'id': 'system',
            'icon': 'fa-gear fw',
            'url': '',
            'permission': enums.DocuflowPermission.CAN_VIEW_SYSTEM_MODULE.value,
            'options': get_permission_options('d'),
            'children': [
                {
                    'label': _('Notification settings'), 'id': 'notification_settings', 'icon': 'fa-user fw',
                    'url': reverse('notification-settings'),
                    'permission': enums.DocuflowPermission.CAN_MANAGE_SYSTEM_NOTIFICATIONS_SETTINGS.value,
                    'options': get_permission_options('d')
                },

                {
                    'label': _('System Logs'), 'id': 'logs', 'icon': 'fa-list', 'url': '',
                    'permission': enums.DocuflowPermission.CAN_VIEW_SYSTEM_LOGS.value,
                    'options': get_permission_options('d'),
                    'children': [
                        {
                            'label': _('Logins'), 'id': 'logins', 'icon': 'fa-user fw', 'url': reverse('login_logs'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_SYSTEM_LOGIN_LOGS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Contracts'), 'id': 'contracts', 'icon': 'fa-user fw', 'url': '#',
                            'permission': enums.DocuflowPermission.CAN_VIEW_SYSTEM_CONTRACTS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Invoice changes'), 'id': 'invoice_changes', 'icon': 'fa-user fw',
                            'url': reverse('invoice_changes'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_SYSTEM_INVOICE_CHANGES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Invoice flow changes'), 'id': 'invoice_flow_changes', 'icon': 'fa-user fw',
                            'url': reverse('invoice_flow_log'),
                            'permission': enums.DocuflowPermission.CAN_VIEW_SYSTEM_INVOICE_FLOW_CHANGES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Invoice integration ERP'), 'id': 'invoice_integration_erp', 'icon': 'fa-user fw',
                            'url': '#',
                            'permission': enums.DocuflowPermission.CAN_MANAGE_SYSTEM_INVOICE_ERP_INTEGRATION.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Role permissions'), 'id': 'role_permissions', 'icon': 'fa-user fw',
                            'url': reverse('permission_group_changes'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_SYSTEM_ROLE_PERMISSION.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Roles'), 'id': 'roles', 'icon': 'fa-user fw', 'url': reverse('role_changes'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_SYSTEM_ROLES.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Users'), 'id': 'users', 'icon': 'fa-user fw', 'url': reverse('user_changes'),
                            'permission': enums.DocuflowPermission.CAN_MANAGE_SYSTEM_USERS.value,
                            'options': get_permission_options('d')
                        },
                        {
                            'label': _('Version Log'), 'id': 'version_log', 'icon': 'fa-user fw',
                            'url': '',
                            'permission': enums.DocuflowPermission.CAN_VIEW_SYSTEM_VERSION_LOGS.value,
                            'options': get_permission_options('d')
                        },
                    ]
                }
            ]
        }
    ]
    role_permissions = []
    if not role:
        for counter, menu_item in enumerate(menu_items):
            if menu_item['id'] == 'purchases':
                menu_items[counter]['children'].append({
                    'label': _('Distribute'),
                    'id': 'distribute',
                    'icon': 'fa-user fw',
                    'permission': enums.DocuflowPermission.CAN_DISTRIBUTE.value,
                    'options': get_permission_options('d')
                })
    if role:
        role_permissions = get_role_permissions(role=role)
        if len(role_permissions) == 0:
            return []
        if not role.company.run_incoming_document_journal:
            purchase_update_children.pop(0)

        menu_items = get_role_menu_items(menu_items=menu_items, role_permissions=role_permissions)
    menu_items = add_accountant_purchases_reports(menu_items, purchase_update_children, role_permissions)

    cache.set(cache_key, menu_items)
    return menu_items


def add_accountant_purchases_reports(menu_items, purchase_update_children, permissions):
    purchase_menu_items = []
    for child_item in purchase_update_children:
        if has_access(menu_item=child_item, permissions=permissions):
            purchase_menu_items.append(child_item)

    update_children = purchase_menu_items if permissions else purchase_update_children
    if update_children:
        for counter, menu_item in enumerate(menu_items):
            if menu_item['id'] == 'accountant':
                for c, child_menu in enumerate(menu_item['children']):
                    if child_menu['id'] == 'update_purchases_module':
                        menu_items[counter]['children'][c]['children'] = update_children
    return menu_items


def get_invoice_fields(role: Role = None) -> list:

    invoice_fields = [
            {
                'id': 'company',
                'label': 'Company',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_COMPANY.value
            },
            {
                'id': 'supplier',
                'label': 'Supplier',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_SUPPLIER.value
            },
            {
                'id': 'supplier_name',
                'label': 'Supplier Name',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_SUPPLIER_NAME.value
            },
            {
                'id': 'supplier_number',
                'label': 'Supplier Number',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_SUPPLIER_NUMBER.value
            },
            {
                'id': 'supplier_account_ref',
                'label': 'Supplier Account Ref',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_SUPPLIER_ACCOUNT_REF.value
            },
            {
                'id': 'document_type',
                'label': 'Document Types',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_DOCUMENT_TYPE.value
            },
            {
                'id': 'link_purchase_order',
                'label': 'Link Purchase Order',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_LINKING_PURCHASE_ORDER.value
            },
            {
                'id': 'link_receipt',
                'label': 'Link Receipt',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_LINK_RECEIPT.value
            },
            {
                'id': 'document_number',
                'label': 'Document Number',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_DOCUMENT_NUMBER.value
            },
            {
                'id': 'vat_number',
                'label': 'Vat Number',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_VAT_NUMBER.value
            },
            {
                'id': 'reference_1',
                'label': 'Reference 1',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_REFERENCE_NUMBER.value
            },
            {
                'id': 'reference_2',
                'label': 'Reference 2',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_REFERENCE_NUMBER.value
            },
            {
                'id': 'flow_proposal',
                'label': 'Flow Proposal',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_FLOW_PROPOSAL.value
            },
            {
                'id': 'account_posting_proposal',
                'label': 'Account Posting Proposal',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_ACCOUNT_POSTING_PROPOSAL.value
            },
            {
                'id': 'accounting_date',
                'label': 'Accounting Date',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_ACCOUNTING_DATE.value
            },
            {
                'id': 'document_date',
                'label': 'Document Date',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_DOCUMENT_DATE.value
            },
            {
                'id': 'due_date',
                'label': 'Due Date',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_DUE_DATE.value
             },
            {
                'id': 'date_paid',
                'label': 'Date Paid',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_DATE_PAID.value
            },
            {
                'id': 'currency',
                'label': 'Currency',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_CURRENCY.value
            },
            {
                'id': 'total_amount',
                'label': 'Total Amount',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_TOTAL_AMOUNT.value
            },
            {
                'id': 'vat_amount',
                'label': 'Vat Amount',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_VAT_AMOUNT.value
            },
            {
                'id': 'vat_type',
                'label': 'Vat Type',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_VAT_TYPE.value
            },
            {
                'id': 'net_amount',
                'label': 'Net Amount',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_NET_AMOUNT.value
            },
            {
                'id': 'comments',
                'label': 'Comments',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_COMMENT.value
            },
            {
                'id': 'attachment',
                'label': 'Attachment',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_ATTACHMENT.value
            },
            {
                'id': 'manage_invoice_flows',
                'label': 'Manage Invoice Flows',
                'options': get_permission_options('d'),
                'permission': enums.DocuflowPermission.CAN_MANAGE_INVOICE_FLOW.value
            }
        ]
    if role:
        role_permissions = get_role_permissions(role=role)
        invoice_fields = get_role_menu_items(menu_items=invoice_fields, role_permissions=role_permissions)
    return invoice_fields


def get_role_permissions(role: Role):
    groups = get_role_groups(role)
    role_permissions = []

    for group in groups:
        group_permissions = get_group_permissions(group=group)
        for permission in group_permissions:
            role_permissions.append(permission)

    if role.slug == 'administrator':
        role_permissions.append(enums.DocuflowPermission.CAN_VIEW_ADMINISTRATION_MODULE.value)
    return role_permissions


def get_role_groups(role):
    cache_key = get_role_cache_key(role=role)
    permission_groups = cache.get(cache_key)
    if not permission_groups:
        permission_groups = role.permission_groups.all()
        cache.set(cache_key, permission_groups)
    return permission_groups


def get_group_permissions(group):
    cache_key = get_group_permissions_cache_key(group=group)
    permissions = cache.get(cache_key)
    if not permissions:
        permissions = group.permissions.values_list('codename', flat=True)
        cache.set(cache_key, permissions)
    return permissions


def get_role_menu_items(menu_items: List[Any], role_permissions: List[str]) -> List:
    # TODO - Test and handle multi-level menu item properly, Test with accountant module - no
    role_menu_items = []
    for menu_item in menu_items:
        _menu = {}
        if has_access(menu_item=menu_item, permissions=role_permissions):
            if 'children' in menu_item:
                children = []
                _menu = get_menu_item(menu_item=menu_item, permissions=role_permissions)
                menu_children = menu_item.get('children', [])
                children = add_children(children, menu_children, role_permissions)
                # for menu_child in menu_children:
                #     child_data = get_menu_item(menu_item=menu_child, permissions=role_permissions)
                #     if child_data:
                #         children.append(child_data)
                if len(children):
                    _menu['children'] = children
                if _menu:
                    role_menu_items.append(_menu)
            else:
                role_menu_items.append(menu_item)
    return role_menu_items


def get_menu_item(menu_item: Dict, permissions=List[str]):
    is_accessible = False
    children = []
    _menu = {}
    if 'children' in menu_item:
        menu_children = menu_item.pop('children')
        children = add_children(children, menu_children, permissions)

    if has_access(menu_item=menu_item, permissions=permissions):
        is_accessible = True
    if is_accessible:
        _menu = menu_item
        if children:
            _menu['children'] = children
    return _menu


def add_children(children, menu_children, permissions):
    for menu_child in menu_children:
        _child = get_menu_item(menu_item=menu_child, permissions=permissions)
        if _child:
            children.append(_child)
    return children


def has_access(menu_item: Dict, permissions: List[str]) -> bool:
    if menu_item['permission'] == '*':
        return True
    else:
        if isinstance(menu_item['permission'], enums.DocuflowPermission):
            return menu_item['permission'].value in permissions
        else:
            return menu_item['permission'] in permissions


def get_menu_options(menu_items):
    options = []
    for menu_item in menu_items:
        if 'children' in menu_item and len(menu_item['children']) > 0:
            child_options = []
            for child_menu in menu_item['children']:
                if 'children' in child_menu and len(child_menu['children']) > 0:
                    grand_child_options = []
                    for grand_child_menu_item in child_menu['children']:
                        if len(grand_child_menu_item) > 0:
                            grand_child_options.append((grand_child_menu_item['id'], grand_child_menu_item['title']))
                    if len(grand_child_options) > 0:
                        options.append((child_menu['title'], tuple(grand_child_options)))
                    else:
                        child_options.append((child_menu['id'], child_menu['title']))
                else:
                    child_options.append((child_menu['id'], child_menu['title']))
            options.append((menu_item['title'], tuple(child_options)))
        else:
            options.append((menu_item['id'], menu_item['title']))
    return options


def get_menu_item_url(year, menu_item_id):
    for menu_item in get_menu_items(year=year):
        if 'children' in menu_item:
            for child_menu in menu_item['children']:
                if 'children' in child_menu:
                    for grand_child_menu in child_menu['children']:
                        if menu_item_id == grand_child_menu['id'] and 'url' in grand_child_menu:
                            return grand_child_menu['url']
                elif menu_item_id == child_menu['id']:
                    return child_menu['url']
        elif menu_item_id == menu_item['id']:
            return menu_item['url']


def get_permission_options(option: str):
    options = {
        'a': [
            {'value': enums.PermissionOption.NO_ACCESS.value, 'label': 'No Access'},
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Add and edit'},
            {'value': enums.PermissionOption.CAN_DELETE.value, 'label': 'Delete'}
        ],
        'b': [
            {'value': enums.PermissionOption.NO_ACCESS.value, 'label': 'No Access'},
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View'},
            {'value': enums.PermissionOption.CAN_EDIT.value, 'label': 'Edit'},
        ],
        'c': [
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Edit & Delete Unsigned Data'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Edit & Delete Signed Data'}
        ],
        'd': [
            {'value': enums.PermissionOption.CAN_NO.value, 'label': 'No'},
            {'value': enums.PermissionOption.CAN_YES.value, 'label': 'Yes'},
        ],
        'e': [
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Not Permitted to Edit'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'May Edit confidentiality classification'},
        ],
        'f': [
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View'},
            {'value': enums.PermissionOption.CAN_ADD.value, 'label': 'Add'},
            {'value': enums.PermissionOption.CAN_DELETE.value, 'label': 'Delete'}
        ],
        'g': [
            {'value': 'view_user_part_of_flow', 'label': 'Only view record where user is part of flow'},
            # {'value': 'view_user_not_part_of_flow', 'label': 'View records where user is not part of flow'},
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View all records'}
        ],
        'h': [
            {'value': enums.PermissionOption.NO_ACCESS.value, 'label': 'No Access'},
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Add and edit'},
        ],
        'i': [
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Manage Roles for which role is role administrator'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Manage all roles'}
        ],
        'j': [
            {'value': enums.PermissionOption.NO_ACCESS.value, 'label': 'No Access'},
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Add and edit'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Delete and allocate permissions'}
        ],
        'k': [
            {'value': enums.PermissionOption.CAN_VIEW.value, 'label': 'View'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Add and Delete'},
        ],
        'l': [
            {'value': enums.PermissionOption.CAN_ADD.value, 'label': 'Add'},
            {'value': enums.PermissionOption.CAN_MANAGE.value, 'label': 'Add and Delete'},
        ]
    }
    return options.get(option)
