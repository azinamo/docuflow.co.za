from django.core.management.base import BaseCommand

from docuflow.apps.accounts.models import Profile


class Command(BaseCommand):
    help = "Migrate roles some company data"

    def handle(self, *args, **kwargs):
        profiles = Profile.objects.all()
        for profile in profiles:
            for profile_role in profile.profile_roles.all():
                profile.roles.add(profile_role.role)
