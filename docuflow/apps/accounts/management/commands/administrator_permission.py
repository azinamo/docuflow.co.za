from django.core.management.base import BaseCommand

from docuflow.apps.company.models import Company
from django.contrib.auth.models import Permission, User


class Command(BaseCommand):
    help = "Dump some company data"

    def handle(self, *args, **kwargs):
        companies = Company.objects.all()
        for company in companies:
            administrator = company.administrator
            if administrator:
                _user = User.objects.get(pk=administrator.id)
                for permission in Permission.objects.all():
                    print(permission)
                    _user.user_permissions.add(permission)

                return None
