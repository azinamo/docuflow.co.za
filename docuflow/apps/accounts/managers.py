from django.db import models


class PermissionGroupManager(models.Manager):
    """
    Lets do querysets limited to companies that we currently
    enrolled profiles
    """
    def get_query_set(self):
        return super(PermissionGroupManager, self).get_queryset().filter(company__isnull=False).distinct()
