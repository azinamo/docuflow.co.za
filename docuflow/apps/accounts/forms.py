from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.template.loader import get_template
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext as __

from docuflow.apps.company.models import Branch
from .enums import NumberOfRecords
from .models import Profile, Role, PermissionGroup, TemporaryProfile
from .tokens import account_activation_token


class UserForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.request = kwargs.pop('request')
        super(UserForm, self).__init__(*args, **kwargs)

    valid_to = forms.DateField(required=False, widget=forms.DateInput(attrs={'class': 'datepicker'}))
    number_of_records = forms.ChoiceField(choices=Profile.NUMBER_OF_INVOICES, required=False)

    code = forms.CharField(required=False, label='User Signing Code')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'is_active', 'valid_to', 'number_of_records', 'code')

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_unusable_password()
        user.save()
        user.profile.parent = self.request.user
        user.profile.company = self.company
        user.profile.code = self.cleaned_data['code']
        user.profile.save()
        self.save_m2m()

        subject = 'New user account created'
        template = get_template('mailer/account_activation_email.html')
        message = template.render({
            'username': user.username,
            'company': self.company,
            'domain': self.request.get_host(),
            'uid': urlsafe_base64_encode(force_bytes(user.profile.pk)),
            'token': account_activation_token.make_token(user),
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
        })
        from_addr = 'no-reply@docuflow.co.za'
        recipient_list = (user.email, )
        send_mail(subject, message, from_addr, recipient_list, html_message=message)
        return user


class UserAccountForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        super(UserAccountForm, self).__init__(*args, **kwargs)
        self.fields['temporary_user'].queryset = Profile.objects.filter(company=self.company).exclude(
            id=self.profile.pk)
        self.initial['number_of_records'] = self.profile.number_of_records
        if self.profile:
            self.initial['number_of_records'] = self.profile.number_of_records
            self.initial['valid_to'] = self.profile.valid_to
            self.initial['code'] = self.profile.code
            self.initial['override_limit'] = round(self.profile.override_limit, 2) if self.profile.override_limit else 0
            self.initial['can_view_protected_document'] = self.profile.can_view_protected_document

    valid_to = forms.DateField(required=False, widget=forms.DateInput(attrs={'class': 'datepicker'}))
    number_of_records = forms.ChoiceField(choices=Profile.NUMBER_OF_INVOICES, required=False)
    temporary_user = forms.ModelChoiceField(queryset=None, required=False)
    start_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'class': 'datepicker'}),
                                 label='From Date')
    expiry_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'class': 'datepicker'}))
    code = forms.CharField(required=True, label='User Signing Code')
    branches = forms.ModelMultipleChoiceField(required=False, queryset=Branch.objects.all())
    new_password_1 = forms.CharField(required=False, label=__('New Password'), widget=forms.PasswordInput())
    new_password_2 = forms.CharField(required=False, label=__('Confirm Password'), widget=forms.PasswordInput())
    override_limit = forms.DecimalField(label='Limit', required=False, decimal_places=2)
    can_view_protected_document = forms.BooleanField(widget=forms.CheckboxInput(), required=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'code', 'new_password_1', 'new_password_2',
                  'override_limit', 'temporary_user', 'start_date', 'expiry_date', 'is_active')

    def clean(self):
        if self.cleaned_data['temporary_user']:
            if not self.cleaned_data['start_date']:
                self.add_error('start_date', 'From date is required')
            if not self.cleaned_data['expiry_date']:
                self.add_error('expiry_date', 'Expiry date is required')

        return self.cleaned_data

    def save(self, commit=True):
        user = super(UserAccountForm, self).save(commit=True)

        self.profile.valid_to = self.cleaned_data['valid_to']
        self.profile.number_of_records = self.cleaned_data['number_of_records']
        self.profile.code = self.cleaned_data['code']
        self.profile.override_limit = self.cleaned_data['override_limit']
        self.profile.can_view_protected_document = self.cleaned_data.get('can_view_protected_document')
        branches = self.request.POST.getlist('branches', [])
        self.profile.save()

        if self.cleaned_data['temporary_user']:
            TemporaryProfile.objects.create(
                temporary=self.profile,
                profile=self.cleaned_data['temporary_user'],
                start_date=self.cleaned_data['start_date'],
                expiry_date=self.cleaned_data['expiry_date']
            )

        profile_cache_key = f"user_{self.request.session['user']}_profile_{self.request.session['company']}"
        cache.set(profile_cache_key, self.profile)
        self.profile.branches.set(branches)
        return user


class ProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        super(ProfileForm, self).__init__(*args, **kwargs)

        if self.profile:
            self.fields['branch'].queryset = Branch.objects.filter(company=self.company)
            self.fields['role'].queryset = Role.objects.filter(company=self.company)
            self.fields['temporary_user'].queryset = Profile.objects.filter(company=self.company).exclude(id=self.profile.id)
            self.initial['number_of_records'] = self.profile.number_of_records
            if self.profile.override_code:
                self.initial['override_code'] = self.profile.override_code
            else:
                self.initial['override_code'] = None
            self.initial['code'] = self.profile.code
            self.initial['branch'] = self.profile.branch
            self.initial['role'] = self.profile.role

    number_of_records = forms.ChoiceField(choices=NumberOfRecords.get_options(), required=False,
                                                   label=__('Display number of records'))
    temporary_user = forms.ModelChoiceField(queryset=None, required=False)
    branch = forms.ModelChoiceField(queryset=None, required=False, label='Default Branch')
    role = forms.ModelChoiceField(queryset=None, required=False, label='Default Role')
    start_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'class': 'datepicker'}),
                                 label=__('From Date'))
    expiry_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'class': 'datepicker'}))
    code = forms.CharField(required=True, label=__('User Signing Code'))
    new_password_1 = forms.CharField(required=False, label=__('New Password'), widget=forms.PasswordInput())
    new_password_2 = forms.CharField(required=False, label=__('Confirm Password'), widget=forms.PasswordInput())
    change_password = forms.BooleanField(required=False, label=__('Reset password on login'), widget=forms.CheckboxInput())
    override_code = forms.CharField(label='Enter your 4 digit code to use to override', required=False)
    override_limit = forms.DecimalField(label='Limit', required=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'code', 'new_password_1', 'new_password_2', 'start_date',
                  'expiry_date', 'temporary_user', 'change_password', 'override_code', 'override_limit', 'branch', 'role')

    def clean(self):
        change_password = self.cleaned_data.get('change_password')
        if change_password:
            password_1 = self.cleaned_data['new_password_1']
            password_2 = self.cleaned_data['new_password_2']

            if password_1 != password_2:
                raise ValidationError('Passwords do no match.')

        if self.cleaned_data['temporary_user']:
            if not self.cleaned_data['start_date']:
                self.add_error('start_date', 'From date is required')
            if not self.cleaned_data['expiry_date']:
                self.add_error('expiry_date', 'Expiry date is required')

        return self.cleaned_data

    def save(self, commit=True):
        super(ProfileForm, self).save(commit=True)

        self.profile.number_of_records = self.cleaned_data['number_of_records']
        self.profile.code = self.cleaned_data['code']
        self.profile.override_code = self.cleaned_data['override_code']
        self.profile.branch = self.cleaned_data['branch']
        self.profile.role = self.cleaned_data['role']
        self.profile.save()

        if self.cleaned_data['temporary_user']:
            TemporaryProfile.objects.create(
                temporary=self.profile,
                profile=self.cleaned_data['temporary_user'],
                start_date=self.cleaned_data['start_date'],
                expiry_date=self.cleaned_data['expiry_date']
            )

        _key = f"year_{self.request.session['company']}_"
        cache.set(_key, None)
        cache.set(f'profile_{self.profile.id}_branches', None)

        cache_key = f"user_{self.request.session['user']}_profile_{self.request.session['company']}"
        cache.set(cache_key, self.profile)


class UserProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['roles'].queryset = Role.objects.filter(company=company)

    first_name = forms.CharField(max_length=200, required=False, help_text='Optional')
    last_name = forms.CharField(max_length=100, required=False, help_text='Optional')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address')
    roles = forms.MultipleChoiceField(required=False, help_text='Optional')

    class Meta:
        model = Profile
        fields = ('valid_to', 'number_of_records')


class PasswordForm(forms.Form):
    new_password_1 = forms.CharField(label=__('New Password'), widget=forms.PasswordInput())
    new_password_2 = forms.CharField(label=__('Confirm Password'), widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(PasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        password_1 = self.cleaned_data['new_password_1']
        password_2 = self.cleaned_data['new_password_2']

        if password_1 != password_2:
            raise ValidationError('Passwords do no match.')
        else:
            if self.user:
                validate_password(password_2, self.user)

        return self.cleaned_data


class PermissionGroupForm(forms.ModelForm):
    pass

    class Meta:
        model = PermissionGroup
        fields = ('label', 'is_active')


class RoleForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(RoleForm, self).__init__(*args, **kwargs)
        self.fields['manager_role'].queryset = Role.objects.filter(company=company)
        self.fields['supervisor_role'].queryset = Role.objects.filter(company=company)
        self.fields['label'] = forms.CharField(label='Name')
        self.fields['reminder_to_role_days'] = forms.IntegerField(
            label='Send a email to role supervisor after X days in inbox', required=False)
        self.fields['send_reminder_after_inbox'] = forms.IntegerField( label='Send a reminder after  X days in inbox',
                                                                       required=False)
        self.fields['send_reminder_before_due_date_days'] = forms.IntegerField(label='Send reminder X days before '
                                                                                     'due date', required=False)

    class Meta:
        model = Role
        fields = ('name', 'label', 'authorization_amount', 'can_be_inserted_in_flow', 'manager_role', 'supervisor_role',
                  'reminder_to_role_days', 'send_reminder_after_inbox', 'send_reminder_before_due_date_days',
                  'direct_reminder', 'permission_groups')


class RoleAuthorizationForm(forms.Form):
    REGISTER_CHOICES = (
        ('', '--please select--'),
        ('account', 'Accounts'),
        ('department', 'Department'),
        ('branch', 'Branch'),
        ('area', 'Area')
    )
    register = forms.ChoiceField(choices=REGISTER_CHOICES, label='Register', required=False)


class RoleOverviewForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(RoleOverviewForm, self).__init__(*args, **kwargs)
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    account = forms.ChoiceField(required=False, label='Account', choices=(('*', '*'),))
    show_details = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_company_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_account_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_object_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_item_type_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    show_expense_type_permission = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    active = forms.ChoiceField(required=False, label='Active', choices=(('*', '*'),))
    number_of_lines = forms.CharField(required=False)


class PermissionGroupAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PermissionGroupAdminForm, self).__init__(*args, **kwargs)

    class Meta:
        model = PermissionGroup
        fields = ('name', 'is_active', 'label', 'permissions', 'is_default')
        widgets = {
            'permissions': FilteredSelectMultiple(is_stacked=False, verbose_name='Permissions')
        }


class AdminProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AdminProfileForm, self).__init__(*args, **kwargs)
        self.user: User = getattr(self.instance, 'user', None)
        if self.user:
            self.fields['username'].initial = self.user.username
            self.fields['first_name'].initial = self.user.first_name
            self.fields['last_name'].initial = self.user.last_name
            self.fields['email'].initial = self.user.email
            self.fields['is_active'].initial = self.user.is_active
        if self.instance and self.instance.pk:
            self.fields['roles'].queryset = self.fields['roles'].queryset.filter(company=self.instance.company)

    username = forms.CharField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    is_active = forms.BooleanField(required=True)

    class Meta:
        model = Profile
        fields = ('company', 'username', 'first_name', 'last_name', 'code', 'roles', 'email', 'is_active')
        widgets = {
            'permissions': FilteredSelectMultiple(is_stacked=False, verbose_name='Permissions')
        }

    def save(self, commit=True):
        user, _ = User.objects.update_or_create(
            username=self.cleaned_data['username'],
            defaults={
                'first_name': self.cleaned_data['first_name'],
                'last_name': self.cleaned_data['last_name'],
                'email': self.cleaned_data['email'],
                'is_active': self.cleaned_data['is_active']
            }
        )
        profile = super(AdminProfileForm, self).save(commit=commit)
        profile.user = user
        profile.save()
        # return profile
        return profile


# # Define the signal handler function
# @receiver(post_save, sender=Profile)
# def my_model_post_save(sender, instance, **kwargs):
#     # Do something after MyModel is saved
#     pass
#
#
# post_save.disconnect(my_model_post_save, sender=Profile)
