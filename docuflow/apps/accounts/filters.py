import django_filters
from .models import Profile, Role, PermissionGroup


def roles(request):
    if request is None:
        return Role.objects.none()
    return Role.objects.filter(company=request.session['company'])


class UserFilter(django_filters.FilterSet):
    user__first_name = django_filters.CharFilter(lookup_expr='icontains')
    user__last_name = django_filters.CharFilter(lookup_expr='icontains')
    user__email = django_filters.CharFilter(lookup_expr='icontains')
    user__username = django_filters.CharFilter(lookup_expr='icontains')
    roles = django_filters.ModelChoiceFilter(queryset=roles)

    class Meta:
        model = Profile
        fields = ['user__first_name', 'user__last_name', 'user__email', 'user__username', 'roles']


def permission_groups(request):
    if request is None:
        return PermissionGroup.objects.none()
    return PermissionGroup.objects.filter(company=request.session['company'])


class RoleFilter(django_filters.FilterSet):
    label = django_filters.CharFilter(lookup_expr='icontains')
    name = django_filters.CharFilter(lookup_expr='icontains')
    permission_groups = django_filters.ModelChoiceFilter(queryset=permission_groups)

    class Meta:
        model = Role
        fields = ['label', 'name', 'permission_groups']


class AccountPostingFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Role
        fields = ['name']
