from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'docuflow.apps.accounts'
