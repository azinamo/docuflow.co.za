import json

from annoying.decorators import ajax_request
from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.core.cache import cache
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, FormView, View
from django.views.generic.edit import CreateView, UpdateView
from django_filters.views import FilterView

from docuflow.apps.auditlog.models import LogEntry
from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.common import utils
from docuflow.apps.company.models import Company, Account, Branch
from docuflow.apps.period.models import Year
from . import selectors
from .filters import UserFilter
from .forms import PasswordForm, UserForm, ProfileForm, UserAccountForm
from .models import Profile, Role, UserLoginLog


class IndexView(LoginRequiredMixin, ProfileMixin, DataTableMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'accounts/index.html'
    # permission_required = ('administration_general_users_add_and_edit', 'administration_general_users_view')
    permission_required = ()

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context


class AccountSearchView(LoginRequiredMixin, FilterView):
    model = Profile
    filterset_class = UserFilter
    template_name = 'accounts/search.html'
    context_object_name = 'accounts'

    def get_queryset(self):
        company = self.request.session['company']
        return Profile.objects.filter(company=company)


class CreateUserAccountView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    form_class = UserForm
    model = User
    template_name = 'accounts/create.html'
    success_url = reverse_lazy('user_accounts_index')
    success_message = 'New user profile created'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'User account successfully created.')
        return HttpResponseRedirect(reverse_lazy('user_accounts_index'))


class EditProfileView(LoginRequiredMixin, FormView):
    form_class = ProfileForm
    template_name = 'accounts/edit_profile.html'
    success_url = reverse_lazy('user_accounts_index')

    def get_profile(self):
        return Profile.objects.get(pk=self.request.session['profile'])

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(EditProfileView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def role_permisssions(self, role_id):
        role = Role.objects.prefetch_related('permission_groups').filter(pk=role_id).first()
        permissions = []
        if role:
            for permission_group in role.permission_groups.all():
                for permission in permission_group.permissions.all():
                    permissions.append(permission.codename)
        return permissions

    def get_role_permissions(self):
        role_id = self.request.session['role']
        if 'permissions' in self.request.session:
            if role_id in self.request.session['permissions']:
                return self.request.session['permissions'][role_id]
            else:
                permissions = self.role_permisssions(role_id)
                self.request.session['permissions'][role_id] = permissions
                return permissions
        else:
            permissions = self.role_permisssions(role_id)
            self.request.session['permissions'] = {role_id: permissions}
            return permissions

    def get_context_data(self, **kwargs):
        context = super(EditProfileView, self).get_context_data(**kwargs)
        permissions = self.get_role_permissions()
        profile = self.get_profile()
        company = self.get_company()
        context['permissions'] = permissions
        context['company'] = company
        context['profile'] = profile
        context['account'] = profile.user
        context['can_edit'] = False
        context['branches'] = Branch.objects.filter(company=company)
        context['profile_branches'] = [branch.id for branch in profile.branches.all()]
        return context

    def form_invalid(self, form):
        messages.error(self.request, 'User profile could not be saved, please fix the errors.')
        return super(EditProfileView, self).form_invalid(form)

    def form_valid(self, form):
        profile = self.get_profile()
        user = profile.user
        is_active = user.is_active
        user.username = form.cleaned_data['username']
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.email = form.cleaned_data['email']
        if form.cleaned_data['new_password_1'] and form.cleaned_data['new_password_2']:
            user.set_password(form.cleaned_data['new_password_1'])
        user.is_active = is_active
        user.save()

        profile = user.profile
        profile.valid_to = form.cleaned_data['valid_to']
        profile.number_of_records = form.cleaned_data['number_of_records']
        profile.temporary_user = form.cleaned_data['temporary_user']
        profile.from_date = form.cleaned_data['from_date']
        profile.expiry_date = form.cleaned_data['expiry_date']
        profile.code = form.cleaned_data['code']
        profile.year = form.cleaned_data['year']
        profile.override_limit = form.cleaned_data['override_limit']
        profile.override_code = form.cleaned_data['override_code']
        profile.branch = form.cleaned_data['branch']
        branches = self.request.POST.getlist('branches', [])
        profile.branches.clear()
        if len(branches) > 0:
            for branch in branches:
                profile.branches.add(branch)
        profile.save()

        if profile.year:
            self.request.session['profile_year_id'] = profile.year.id
        if profile.branch:
            self.request.session['branch_id'] = profile.branch.id

        cache.set(f'profile_{profile.id}_branches', None)

        messages.success(self.request, 'User profile successfully saved')
        return HttpResponseRedirect(reverse_lazy('current_user_edit'))


class CurrentUserEditView(LoginRequiredMixin, ProfileMixin, UpdateView):
    form_class = ProfileForm
    model = User
    template_name = 'accounts/edit.html'
    success_url = reverse_lazy('current_user_edit')

    def get_object(self, queryset=None):
        return User.objects.get(pk=self.request.user.pk)

    def get_profile(self):
        return Profile.objects.get(pk=self.request.session['profile'])

    def get_form_kwargs(self):
        kwargs = super(CurrentUserEditView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CurrentUserEditView, self).get_context_data(**kwargs)
        company = self.get_company()
        profile = self.get_profile()
        context['permissions'] = selectors.get_permissions_list(self.request.session['role'])
        context['company'] = company
        context['profile'] = profile
        context['account'] = profile.user
        context['can_edit'] = False
        context['branches'] = Branch.objects.filter(company=company)
        context['profile_branches'] = profile.branches.values_list('pk', flat=True)
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'User profile could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CurrentUserEditView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            form.save()
            return JsonResponse({
                'text': 'User profile updated successfully ',
                'error': False,
                'redirect': reverse('current_user_edit')
            })
        return HttpResponse('Not allowed')


class UserAccountEditView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    form_class = UserAccountForm
    model = User
    template_name = 'accounts/edit_account.html'
    message = "User account updated successfully"
    context_object_name = 'user'

    def get_success_url(self):
        return reverse_lazy('edit_user_account', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        context = super(UserAccountEditView, self).get_context_data(**kwargs)
        user = self.get_object()
        profile = user.profile
        permissions = self.get_role_permissions()

        company = profile.company
        context['permissions'] = permissions
        context['profile'] = profile
        context['company'] = company
        context['account'] = user
        context['can_edit'] = True
        context['branches'] = Branch.objects.filter(company=company)
        context['profile_branches'] = [branch.id for branch in profile.branches.all()]
        context['can_manage_profile'] = 'can_manage_profile' in permissions
        return context

    def get_form_kwargs(self):
        kwargs = super(UserAccountEditView, self).get_form_kwargs()
        profile = self.get_profile()
        kwargs['profile'] = profile
        kwargs['company'] = profile.company
        kwargs['request'] = self.request
        return kwargs

    def get_profile(self):
        return Profile.objects.select_related('user', 'company').get(user_id=self.kwargs['pk'])

    def get_role_permissions(self):
        role_id = self.request.session['role']
        permissions = []
        if role_id:
            if 'permissions' in self.request.session:
                if role_id in self.request.session['permissions']:
                    return self.request.session['permissions'][role_id]
                else:
                    permissions = self.role_permisssions(role_id)
                    self.request.session['permissions'][role_id] = permissions
                    return permissions
            else:
                permissions = self.role_permisssions(role_id)
                self.request.session['permissions'] = {role_id: permissions}
                return permissions
        return permissions

    def role_permisssions(self, role_id):
        role = Role.objects.prefetch_related('permission_groups').get(pk=role_id)
        permissions = []
        if role:
            for permission_group in role.permission_groups.all():
                for permission in permission_group.permissions.all():
                    permissions.append(permission.codename)
        return permissions

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'User profile could not be saved, please fix the errors.',
                'errors': form.errors,
            }, status=400)
        return super(UserAccountEditView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            form.save()
            return JsonResponse({
                'text': 'User profile updated successfully ',
                'error': False,
                'redirect': reverse('user_accounts_index')
            })
        return HttpResponse('Not allowed')


class UserChangesView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/changes.html'

    def get_context_data(self, **kwargs):
        context = super(UserChangesView, self).get_context_data(**kwargs)
        context['log_entries'] = LogEntry.objects.get_for_model(Profile)
        return context


class LoginLogView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/login_logs.html'

    def get_context_data(self, **kwargs):
        context = super(LoginLogView, self).get_context_data(**kwargs)
        context['log_entries'] = UserLoginLog.objects.select_related('profile', 'profile__user').filter()
        return context


class SendAccessCredentialsView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        profile = get_object_or_None(Profile, pk=self.kwargs['pk'])
        if not profile:
            return HttpResponse(json.dumps({
                'text': 'Profile not found, try again',
                'error': True
            }))
        if 'password' in self.request.GET:
            subject = 'Activate Your Docuflow Account'
            email_data = dict({'username': profile.user.username,
                               'company': profile.company,
                               'domain': self.request.get_host(),
                               'uid': '',
                               'token': '',
                               'email': profile.user.email,
                               'first_name': profile.user.first_name,
                               'last_name': profile.user.last_name
                               })

            email_data['password'] = self.request.GET['password']

            message = render_to_string('mailer/account_activation_email.html', email_data)
            profile.user.email_user(subject, message, from_email='info@docuflow.co.za', html_message=message)
            return HttpResponse(json.dumps({
                'text': 'Email successfully send',
                'error': False
            }))


class UserAccountDeleteView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        profile = Profile.objects.filter(user_id=self.kwargs['pk']).first()
        if profile:
            # user = profile.user
            profile.delete()

            # user.delete()
            messages.success(request, 'User account deleted successfully')
        else:
            messages.success(request, 'User account not found')
        return redirect(reverse_lazy('user_accounts_index'))


class AccountProfileView(LoginRequiredMixin, TemplateView):
    template_name = "accounts/show.html"

    def get_context_data(self, **kwargs):
        context = super(AccountProfileView, self).get_context_data(**kwargs)
        user = self.request.user
        context['profile'] = Profile.objects.get(user=user)
        return context


class ChangeUserAccountPaswordView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    form_class = PasswordForm
    model = Profile
    template_name = 'accounts/change_password.html'
    success_message = "User account password successfully updated"
    success_url = reverse_lazy('user_accounts_index')

    def form_valid(self, form):
        user = User.objects.get(pk=self.kwargs['pk'])
        user.set_password(self.request.POST['new_password_1'])
        user.save()
        return HttpResponseRedirect(reverse_lazy('user_accounts_index'))


class CurrentUserPasswordChangeView(LoginRequiredMixin, FormView):
    template_name = 'accounts/change_password.html'
    form_class = PasswordForm
    mode = get_user_model()


class ChangeCurrentUserBranchView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            try:
                branch = Branch.objects.get(pk=self.kwargs['pk'])
            except Branch.DoesNotExist:
                return JsonResponse({
                    'error': True,
                    'text': 'Branch not found',
                })

            self.request.session['branch'] = branch.id
            cache_key = utils.get_profile_branch_cache_key(
                profile_id=self.request.session['profile'], company_id=self.request.session['company']
            )
            cache.set(cache_key, branch)
            return JsonResponse({
                'error': False,
                'text': 'Branch successfully updated',
                'reload': True
            })
        return redirect(reverse('all-invoices'))


class ChangeCurrentUserYearView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            try:
                year = Year.objects.get(pk=self.kwargs['pk'])
            except Year.DoesNotExist:
                return JsonResponse({
                    'error': False,
                    'text': 'Year does not exist',
                    'reload': True
                })

            cache_key = utils.get_profile_year_cache_key(
                profile_id=self.request.session['profile'], company_id=self.request.session['company']
            )
            cache.set(cache_key, year)

            self.request.session['year'] = year.id

            return JsonResponse({
                'error': False,
                'text': 'Year successfully updated',
                'reload': True
            })
        return redirect(reverse('all-invoices'))


class ValidateOverrideKey(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            try:
                override_key = self.request.GET.get('override_key')
                if not override_key:
                    raise ValueError('Please enter a valid override key')
                if len(override_key) > 4:
                    raise ValueError('Please enter a valid override key')

                profile = Profile.objects.filter(id=self.request.session['profile']).first()
                if not profile:
                    return JsonResponse({'error': True, 'text': 'Profile not found'})
                # amount = self.request.GET['override_limit']
                if profile.override_code == override_key:
                    return JsonResponse({'error': False,
                                         'text': 'Override key is valid',
                                         })
                else:
                    return JsonResponse({'error': True,
                                         'text': 'Override key is not valid',
                                         })
            except ValueError as ex:
                return JsonResponse({'error': True, 'text': ex.__str__() })
        return HttpResponse('Not allowed')


def generate_codename(label):
    name_list = (label).upper().split(' ')
    counter = 0
    codename = ''
    for str in name_list:
        if counter < 2:
            codename += "{} ".format(str[0:3])
        counter = counter + 1
    return codename


@ajax_request
def get_object_permissions(request):

    company = Company.objects.get(pk=request.session['company'])
    role = Role.objects.get(pk=request.GET['role_id'])
    object_category = None

    if object_category.slug == 'accounts':
        role_accounts = role.accounts.all()
        account_ids = [role_account.id for role_account in role_accounts]
        left = [{'id': a.id, 'name': "{}-{}".format(a.code, a.name)} for a in Account.objects.filter(company=company) if len(a.name) > 0 and not a.id in account_ids]
        right = [{'id': ac.id, 'name': "{}-{}".format(ac.code, ac.name)} for ac in role_accounts]
        left_list = left
        right_list = right
    else:
        category_object_items = []

        role_object_items = role.object_items.filter(category=object_category).all()
        role_object_item_ids = [category_object_item.id for category_object_item in role_object_items]

        left = [{'id': d.id, 'name': d.label} for d in category_object_items if len(d.label) > 0 and not d.id in role_object_item_ids]

        right = [{'id': dpt.id, 'name': dpt.label} for dpt in role_object_items]
        left_list = left
        right_list = right

    ajax_response = {'right_list' : right_list, 'left_list': left_list, 'error': False}

    return ajax_response


class GrantProfileAccessView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            current_profile = self.request.session.get('profile')
            if current_profile:
                cache.set(f'profile_{current_profile}_roles', [])

            profile = Profile.objects.prefetch_related('roles').get(pk=self.kwargs['profile_id'])
            if profile:
                cache_key = f"user_{self.request.session['user']}_profile_{self.request.session['company']}"
                cache.set(cache_key, profile)
                profile_role = profile.roles.first()
                if profile_role:
                    self.request.session['role'] = profile_role.id
                    self.request.session['role_id'] = profile_role.id
                redirect_url = self.request.GET.get('next')
                if not redirect_url:
                    redirect_url = reverse_lazy('all-invoices')

                return JsonResponse({
                    'error': False,
                    'text': 'Profile successfully updated',
                    'redirect': redirect_url
                })
        return HttpResponse('Not allowed')
