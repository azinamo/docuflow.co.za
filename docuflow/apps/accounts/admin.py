from django.contrib import admin

from . import models
from .forms import PermissionGroupAdminForm, AdminProfileForm

#
# @admin.register(models.Profile)
# class PermissionGroupAdmin(admin.ModelAdmin):
#     list_display = ('label', 'company', 'is_active', 'is_default')
#     list_filter = ('company', )
#     search_fields = ('label', )


@admin.register(models.PermissionGroup)
class PermissionGroupAdmin(admin.ModelAdmin):
    form = PermissionGroupAdminForm
    list_display = ('label', 'company', 'is_active', 'is_default')
    list_filter = ('company', )
    search_fields = ('label', )
    list_select_related = ('company', )


@admin.register(models.Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ('label', 'company', 'company_code')
    list_filter = ('company',)
    search_fields = ('label',)

    # noinspection PyMethodMayBeStatic
    def company_code(self, role: models.Role):
        return role.company.slug


@admin.register(models.Profile)
class UserProfileAdmin(admin.ModelAdmin):
    form = AdminProfileForm
    list_display = ('__str__', 'company', 'username')
    list_filter = ('company',)
    search_fields = ('user__first_name', 'user__last_name', 'company__name', 'company__slug', 'user__email')
    list_select_related = ('company', 'user')

    # noinspection PyMethodMayBeStatic
    def username(self, profile: models.Profile):
        return profile.user.username

