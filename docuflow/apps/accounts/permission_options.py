options = {
    'a': [
        {'value': 'no_access', 'label': 'No Access'},
        {'value': 'view', 'label': 'View'},
        {'value': 'add_and_edit', 'label': 'Add and edit'},
        {'value': 'delete', 'label': 'Delete'}
    ],
    'b': [
        {'value': 'no_access', 'label': 'No Access'},
        {'value': 'view', 'label': 'View'},
        {'value': 'edit', 'label': 'Edit'},
    ],
    'c': [
        {'value': 'view', 'label': 'View'},
        {'value': 'edit_and_delete_unsigned_data', 'label': 'Edit & Delete Unsigned Data'},
        {'value': 'edit_and_delete_signed_data', 'label': 'Edit & Delete Signed Data'}
    ],
    'd': [
        {'value': 'no', 'label': 'No'},
        {'value': 'yes', 'label': 'Yes'},
    ],
    'e': [
        {'value': 'not_permitted_to_edit', 'label': 'Not Permitted to Edit'},
        {'value': 'may_edit', 'label': 'May Edit confidentiality classification'},
    ],
    'f': [
        {'value': 'view', 'label': 'View'},
        {'value': 'add', 'label': 'Add'},
        {'value': 'delete', 'label': 'Delete'}
    ],
    'g': [
        {'value': 'view_user_part_of_flow', 'label': 'Only view record where user is part of flow'},
        # {'value': 'view_user_not_part_of_flow', 'label': 'View records where user is not part of flow'},
        {'value': 'view_all', 'label': 'View all records'}
    ],
    'h': [
        {'value': 'no_access', 'label': 'No Access'},
        {'value': 'view', 'label': 'View'},
        {'value': 'add_and_edit', 'label': 'Add and edit'},
    ],
    'i': [
        {'value': 'manage_roles_for_which_role_is_role_administrator',
         'label': 'Manage Roles for which role is role administrator'},
        {'value': 'manage_all_roles', 'label': 'Manage all roles'}
    ],
    'j': [
        {'value': 'no_access', 'label': 'No Access'},
        {'value': 'view', 'label': 'View'},
        {'value': 'add_and_edit', 'label': 'Add and edit'},
        {'value': 'delete_and_allocate_permissions', 'label': 'Delete and allocate permissions'}
    ],
    'k': [
        {'value': 'view', 'label': 'View'},
        {'value': 'add_and_delete', 'label': 'Add and Delete'},
    ],
    'l': [
        {'value': 'add', 'label': 'Add'},
        {'value': 'add_and_delete', 'label': 'Add and Delete'},
    ],
}