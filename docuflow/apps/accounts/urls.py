from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='user_accounts_index'),
    path('search/', views.AccountSearchView.as_view(), name='user_accounts_search'),
    path('create/', views.CreateUserAccountView.as_view(), name='create-user-account'),
    path('profile/', views.CurrentUserEditView.as_view(), name='current_user_edit'),
    path('edit/<int:pk>/', views.UserAccountEditView.as_view(), name='edit_user_account'),
    path('changes/', views.UserChangesView.as_view(), name='user_changes'),
    path('logs/login/', views.LoginLogView.as_view(), name='login_logs'),
    path('send/access/credentials/<int:pk>/', views.SendAccessCredentialsView.as_view(), name='send_access_credentials'),
    path('change/current/year/<int:pk>', views.ChangeCurrentUserYearView.as_view(), name='change_year'),
    path('change/current/branch/<int:pk>', views.ChangeCurrentUserBranchView.as_view(), name='change_branch'),
    path('delete/<int:pk>/', views.UserAccountDeleteView.as_view(), name='user_account_delete'),
    path('change/password/<int:pk>/', views.ChangeUserAccountPaswordView.as_view(), name='user_account_change_password'),
    path('password/change/', views.CurrentUserPasswordChangeView.as_view(), name='current-user-password-change'),
    path('validate/override/key/', views.ValidateOverrideKey.as_view(), name='validate_override_key'),
    path('grant/profile/<int:profile_id>/access/', views.GrantProfileAccessView.as_view(), name='grant_profile_access'),
    path('<int:profile_id>/temporary/', include('docuflow.apps.accounts.modules.temporaryuser.urls')),
    path('groups/', include('docuflow.apps.accounts.modules.group.urls')),
    path('roles/', include('docuflow.apps.accounts.modules.role.urls')),
]



