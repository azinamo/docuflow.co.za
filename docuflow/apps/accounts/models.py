from django.utils.timezone import datetime
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User, Group
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.template.defaultfilters import slugify

from safedelete.models import SafeDeleteModel, SOFT_DELETE
from enumfields.fields import EnumIntegerField

from docuflow.apps.auditlog.registry import auditlog
from docuflow.apps.company.models import Company, Account, ObjectItem
from .managers import PermissionGroupManager
from .enums import DocuflowPermission, NumberOfRecords


# https://github.com/macropin/django-registration/blob/master/registration/models.py
class PermissionGroup(Group):
    is_active = models.BooleanField(blank=True, default=False)
    company = models.ForeignKey(Company, related_name='permission_groups', blank=True, null=True, on_delete=models.CASCADE)
    label = models.CharField(blank=True, null=True, max_length=200)
    is_default = models.BooleanField(default=False)
    # Two managers for this model, the first is default
    # So all companies will appear in admin
    # The second is only invoked when we call
    # Company.has_profiles().all()

    objects = models.Manager()
    has_profiles = PermissionGroupManager()

    class Meta:
        verbose_name_plural = 'Permission Groups'
        ordering = ['label']
        unique_together = ('company', 'label')
        permissions = DocuflowPermission.get_choices()

    def __str__(self):
        return self.label


class Profile(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    NUMBER_OF_INVOICES = (
        (10, 10),
        (20, 20),
        (30, 30),
        (40, 40)
    )
    company = models.ForeignKey(Company, null=True, related_name='profiles', on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    valid_to = models.DateField(null=True, blank=True)
    number_of_records = EnumIntegerField(NumberOfRecords, default=NumberOfRecords.TEN, verbose_name='Number of records per page')
    year = models.ForeignKey('period.Year', related_name='year_profiles', null=True, blank=True, on_delete=models.DO_NOTHING)
    email_confirmed = models.BooleanField(default=False)
    code = models.CharField(max_length=255, blank=True)
    roles = models.ManyToManyField('Role')
    branch = models.ForeignKey('company.Branch', null=True, blank=True, verbose_name='Default Branch', related_name='+', on_delete=models.SET_NULL)
    role = models.ForeignKey('Role', null=True, blank=True, verbose_name='Default Role', related_name='+', on_delete=models.SET_NULL)
    branches = models.ManyToManyField('company.Branch', verbose_name='Access to')
    override_limit = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    override_code = models.CharField(max_length=5, blank=True)
    can_view_protected_document = models.BooleanField(default=False, verbose_name='Can view protected documents')

    class QS(models.QuerySet):

        def active(self):
            return self.filter(user__is_active=True)

    objects = QS.as_manager()

    class Meta:
        verbose_name_plural = 'Profiles'

    def __str__(self):
        return f"{self.user.first_name} {self.user.last_name}"

    def save(self, *args, **kwargs):
        if not self.code and self.company:
            first = self.user.first_name[0:2]
            last = self.user.last_name[0:2]
            self.code = f"{self.company.company_code}{first}{last}"
        return super(Profile, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('profiles')


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class TemporaryProfile(models.Model):
    profile = models.ForeignKey(Profile, related_name='profiles', on_delete=models.DO_NOTHING)
    temporary = models.ForeignKey(Profile, related_name='temporary_profiles', on_delete=models.DO_NOTHING)
    start_date = models.DateField()
    expiry_date = models.DateField(verbose_name='Expiry Date')

    @property
    def is_valid(self):
        return self.start_date > datetime.now().date() < self.expiry_date

    @property
    def has_started(self):
        return self.start_date < datetime.now().date()

    @property
    def has_expired(self):
        return self.expiry_date < datetime.now().date()


class RoleManager(models.Manager):

    def get_queryset(self):
        return super(RoleManager, self).get_queryset().filter(deleted__isnull=True)

    def get_permissions_list(self, role_id):
        role = self.prefetch_related('permission_groups', 'permission_groups__permissions').get(pk=role_id)
        permissions = []
        if role:
            for permission_group in role.permission_groups.all():
                for permission in permission_group.permissions.all():
                    permissions.append(permission.codename)
        return permissions


class RoleQueryset(models.QuerySet):

    def administrator(self):
        return self.filter(slug='administrator')


class Role(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(max_length=64, verbose_name='Name', blank=True)
    label = models.CharField(max_length=64, verbose_name='Role')
    company = models.ForeignKey(Company, related_name='company_roles', on_delete=models.CASCADE)
    permission_groups = models.ManyToManyField(PermissionGroup, related_name='permission_groups', verbose_name='System Access Levels')
    authorization_amount = models.CharField(blank=True, max_length=200)
    can_be_inserted_in_flow = models.BooleanField(null=True)
    manager_role = models.ForeignKey('self', related_name='manager_roles', verbose_name='Invoice Flow Escalation', blank=True, null=True, on_delete=models.DO_NOTHING)
    supervisor_role = models.ForeignKey('self', related_name='supervisor_roles', verbose_name='Purchase Order', blank=True, null=True, on_delete=models.DO_NOTHING)
    reminder_to_role_days = models.IntegerField(blank=True, null=True)
    invoice_account_to_role_days = models.IntegerField(blank=True, null=True)
    send_reminder_after_inbox = models.IntegerField(blank=True, null=True)
    send_reminder_before_due_date_days = models.IntegerField(blank=True, null=True)
    direct_reminder = models.BooleanField(null=True)
    accounts = models.ManyToManyField(Account)
    object_items = models.ManyToManyField(ObjectItem)
    slug = models.CharField(max_length=255, blank=True)

    objects = RoleManager.from_queryset(RoleQueryset)()

    class Meta:
        verbose_name = 'Role'
        ordering = ('label', )
        unique_together = ('company', 'label')

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        self.slug = slugify(self.label)
        return super(Role, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('permissions-roles')

    def natural_key(self):
        return self.label

    def create_codename(self):
        name_list = self.label.upper().split(' ')
        counter = 0
        codename = ''
        for _str in name_list:
            if counter < 2:
                codename += f"{_str[0:3]} "
            counter = counter + 1
        return codename

    def get_profiles(self):
        role_profiles = []
        for profile in self.profile_set.all():
            role_profiles.append(f"{profile.user.first_name} {profile.user.last_name}")
        return ', '.join(role_profiles)


class UserLoginLog(models.Model):
    profile = models.ForeignKey(Profile, related_name='login_logs', on_delete=models.CASCADE)
    remote_addr = models.GenericIPAddressField(blank=True, null=True, verbose_name="remote address")
    timestamp = models.DateTimeField(auto_now_add=True, verbose_name="timestamp")
    message = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name_plural = 'Login Logs'

    def __str__(self):
        return f"{self.profile} logged in at {self.timestamp}"


class UserOpenPage(models.Model):
    profile = models.ForeignKey(Profile, related_name='open_pages', on_delete=models.CASCADE)
    page = models.TextField()
    agent = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


auditlog.register(PermissionGroup)
auditlog.register(Role)
auditlog.register(Profile)
