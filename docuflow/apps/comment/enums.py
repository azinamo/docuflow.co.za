from enumfields import IntEnum


class CommentType(IntEnum):
    COMMENT = 1
    ACTION = 2


class CommentAction(IntEnum):
    CREATED = 0
    UPDATED = 1
    DELETED = 2
