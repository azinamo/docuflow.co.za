import os
import logging

from django.conf import settings
from django.views.generic import ListView, View, TemplateView
from django.views.generic.edit import UpdateView, CreateView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.accounts.models import Role
from docuflow.apps.common.utils import create_dir, clean_filename
from .models import Comment
from .forms import CommentForm


logger = logging.getLogger(__name__)


class CommentsView(LoginRequiredMixin, ListView):
    model = Comment
    template_name = 'comment/index.html'
    context_object_name = 'comments'

    def get_queryset(self):
        return Comment.objects.get_by_content_object(self.kwargs['object_id'], self.kwargs['content_type'])

    def get_context_data(self, **kwargs):
        context = super(CommentsView, self).get_context_data(**kwargs)
        context['upload_url'] = settings.MEDIA_URL
        can_edit_invoice = self.request.session.get('can_edit_invoice', [])
        can_edit = True if self.kwargs['object_id'] in can_edit_invoice else False
        context['can_edit'] = can_edit
        return context


class CreateCommentView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = "comment/create.html"
    model = Comment
    form_class = CommentForm

    def get_form_kwargs(self):
        kwargs = super(CreateCommentView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateCommentView, self).get_context_data(**kwargs)
        context['object_id'] = self.kwargs['object_id']
        context['content_type'] = self.kwargs['content_type']
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True, 'text': 'Comment could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateCommentView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            comment = form.save(commit=False)
            comment.profile = self.get_profile()
            comment.role_id = self.request.session['role']
            comment.object_id = self.kwargs['object_id']
            comment.content_type_id = self.kwargs['content_type']
            comment.save()
            return JsonResponse({'error': False,
                                 'text': 'Comment successfully saved.',
                                 'reload': True
                                 })
        return HttpResponse("Not Allowed")


class EditCommentView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = "comment/edit.html"
    model = Comment

    def get_form_class(self):
        return CommentForm

    def get_form_kwargs(self):
        kwargs = super(EditCommentView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditCommentView, self).get_context_data(**kwargs)
        context['comment'] = self.get_object()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Comment could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditCommentView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            return JsonResponse({'error': False,
                                 'text': 'Comment successfully updated.',
                                 'reload': True
                                 })
        return HttpResponse("Not Allowed")


class DeleteCommentView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        comment = Comment.objects.get(pk=self.kwargs['pk'])
        comment.delete()
        messages.success(self.request, 'Comment deleted successfully')
        return HttpResponseRedirect('')


class CommentAttachmentView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = "comment/view_attachment.html"

    def get_comment(self):
        return Comment.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(CommentAttachmentView, self).get_context_data(**kwargs)
        comment = self.get_comment()
        context['comment'] = comment
        context['upload_url'] = settings.MEDIA_URL
        return context


class AddCommentsView(LoginRequiredMixin, TemplateView):
    template_name = 'comment/add_comment.html'

    def get_context_data(self, **kwargs):
        context = super(AddCommentsView, self).get_context_data(**kwargs)
        context['upload_url'] = settings.MEDIA_URL
        context['content_type'] = self.kwargs['content_type']
        context['tmp_comments'] = self.request.session.get(f"comments_{self.kwargs['content_type']}")
        return context


class AddCommentView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "comment/add.html"
    form_class = CommentForm

    def get_form_kwargs(self):
        kwargs = super(AddCommentView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddCommentView, self).get_context_data(**kwargs)
        context['content_type'] = self.kwargs['content_type']
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True, 'text': 'Comment could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(AddCommentView, self).form_invalid(form)

    def handle_file_upload(self, company, f):
        uploads = f'{str(company.slug)}/uploads'
        uploads_dir = os.path.join(settings.MEDIA_ROOT, str(company.slug), 'uploads')
        uploads_dir = create_dir(uploads_dir)

        filename = clean_filename(str(f))
        path = os.path.join(uploads_dir, filename)

        with open(path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

        return f'{uploads}/{filename}'

    def form_valid(self, form):
        if self.request.is_ajax():
            company = self.get_company()
            role = Role.objects.get(pk=self.request.session['role'])

            uploaded_file = None
            session_key = f"comments_{self.kwargs['content_type']}"
            comments = self.request.session.get(session_key, [])
            if form.cleaned_data['attachment']:
                uploaded_file = self.handle_file_upload(company, form.cleaned_data['attachment'])
                del form.cleaned_data['attachment']

            comment_data = {k: v for k, v in form.cleaned_data.items()}
            comment_data['attachment'] = uploaded_file
            comment_data['profile_id'] = self.request.session['profile']
            comment_data['profile_name'] = str(self.get_profile())
            comment_data['role_id'] = self.request.session['role']
            comment_data['role_name'] = str(role)
            comment_data['content_type_id'] = self.kwargs['content_type']
            comments.append(comment_data)

            self.request.session[session_key] = comments

            return JsonResponse({'error': False,
                                 'text': 'Comment successfully added.'
                                 })
        return HttpResponse("Not Allowed")
