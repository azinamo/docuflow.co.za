import os

from django.contrib.contenttypes.fields import ContentType
from django.conf import settings

from docuflow.apps.accounts.models import Profile, Role
from .models import Comment


def create_comments(request, content_type: ContentType, object_id: int, profile: Profile, role: Role):
    session_key = f'comments_{content_type.id}'
    comments = request.session.get(session_key)
    if comments:
        for comment in comments:
            cmt = Comment.objects.create_comment(
                object_id=object_id,
                content_type=content_type,
                comment=comment.get('comment'),
                **{'profile': profile, 'role': role},
            )
            attachment = comment.get('attachment')
            if cmt and attachment:
                cmt.attachment.name = attachment
                cmt.save()
    if session_key in request.session:
        del request.session[f'comments_{content_type.id}']


def clear_session_comments(request, content_type: ContentType):
    session_key = f'comments_{content_type.id}'
    comments = request.session.get(session_key)
    if comments:
        for comment in comments:
            attachment = comment.get('attachment')
            if attachment:
                path = os.path.join(settings.MEDIA_ROOT, attachment)
                if os.path.isfile(path):
                    os.unlink(path)
    if session_key in request.session:
        del request.session[f'comments_{content_type.id}']
