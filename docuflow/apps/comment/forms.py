from django import forms

from .models import Comment


class CommentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Comment
        fields = ('comment', 'attachment', )

    def clean(self):
        cleaned_data = super(CommentForm, self).clean()
        attachment = cleaned_data.get('attachment', None)
        comment = cleaned_data.get('comment')

        if not (attachment or comment):
            self.add_error('comment', 'Please enter the comment or add an attachment')

        return cleaned_data
