from django import template
from django.conf import settings

from docuflow.apps.comment.models import Comment

register = template.Library()


@register.inclusion_tag('comment/index.html')
def comments(object_id, content_type_id):
    object_comments = Comment.objects.get_by_content_object(object_id, content_type_id)
    return {'object_id': object_id,
            'content_type_id': content_type_id,
            'comments': object_comments,
            'upload_url': settings.MEDIA_URL
            }


@register.inclusion_tag('comment/add.html')
def add_comments(content_type_id):
    return {'content_type_id': content_type_id,
            'upload_url': settings.MEDIA_URL
            }
