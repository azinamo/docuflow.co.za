from django.urls import path

from . import views

urlpatterns = [
    path('<int:content_type>/<int:object_id>/', views.CommentsView.as_view(), name='comment_index'),
    path('<int:content_type>/<int:object_id>/create/', views.CreateCommentView.as_view(), name='create_comment'),
    path('attachment/<int:pk>/', views.CommentAttachmentView.as_view(), name='comment_attachment'),
    path('<int:pk>/edit/', views.EditCommentView.as_view(), name='edit_comment'),
    path('<int:pk>/delete/', views.DeleteCommentView.as_view(), name='delete_comment'),
    path('<int:content_type>/', views.AddCommentsView.as_view(), name='add_comments'),
    path('<int:content_type>/add/', views.AddCommentView.as_view(), name='add_comment'),
]
