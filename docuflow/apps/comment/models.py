from django.contrib.contenttypes.fields import GenericForeignKey, ContentType
from django.db.models import JSONField
from django.db import models

from enumfields import EnumIntegerField

from . import enums


# Create your models here.
class CommentManager(models.Manager):

    def create_comment(self, object_id, content_type, comment, profile=None, role=None, attachment=None):
        return self.create(
            object_id=object_id,
            content_type=content_type,
            comment=comment,
            comment_type=enums.CommentType.COMMENT,
            profile=profile,
            role=role,
            attachment=attachment
        )

    def get_by_content_object(self, object_id, content_type_id):
        return self.select_related(
            'profile', 'role'
        ).filter(
            object_id=object_id, content_type_id=content_type_id
        )


class Comment(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    comment = models.TextField()
    comment_type = EnumIntegerField(enums.CommentType, default=enums.CommentType.ACTION)
    action = EnumIntegerField(enums.CommentAction, default=enums.CommentAction.CREATED)
    profile = models.ForeignKey('accounts.Profile', related_name='my_comments', on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', related_name='my_comments', on_delete=models.DO_NOTHING)
    attachment = models.FileField(upload_to='uploads', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    data = JSONField(null=True)

    objects = CommentManager()

    class Meta:
        ordering = ['-created_at']

    @property
    def link_url(self):
        if self.data and hasattr('link_url', self.data):
            return self.data['link_url']
        return ''

    @property
    def is_linked(self):
        if self.data and hasattr('is_linked', self.data):
            return self.data['is_linked']
        return ''
