from django.apps import AppConfig


class CommentConfig(AppConfig):
    name = 'docuflow.apps.comment'
