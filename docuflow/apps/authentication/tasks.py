from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from celery import shared_task


@shared_task
def send_signup_email(user_data, subject, message):
    # user.email_user(subject, message)

    html_content = render_to_string('mailer/account_activation_email.html', {'user': user_data})
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, 'info@docuflow.co.za', [user_data.email])
    msg.attach_alternative(html_content, 'text/html')
    msg.send()

    return '{} email successfully send '.format(user_data.username)

