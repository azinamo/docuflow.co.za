from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('logout/', views.LogoutView.as_view(), name='user_logout'),
    path('logout/', views.LoginView.as_view(), name='login'),
    path('<str:slug>/login/', views.CompanyLoginView.as_view(), name='company_login'),
    path('ajax/<str:slug>/login/', csrf_exempt(views.AjaxLoginView.as_view()), name='user_ajax_login'),
    path('plan/<str:slug>/', views.UpdateProfileView.as_view(), name='subscription_plan_detail'),
    path('<str:slug>/password/request/', views.UserPasswordRequest.as_view(), name='user_password_request'),
    path('company/login/', views.CompanyLoginRedirectView.as_view(), name='company_redirect_login'),
    path('company/invoices/', views.CompanyInvoiceRedirectView.as_view(), name='company_redirect_invoices'),
    path('docuflow/', views.DocuflowLoginView.as_view(), name='docuflow_login'),
    path('signup/', views.RegistrationView.as_view(), name='signup'),
    path('account_activation_sent/', views.AccountActivationSentView.as_view(), name='account_activation_sent'),
    path('activate/(<str:uidb64>/(<str:token>/', views.ActiveView.as_view(), name='activate'),
    path('login/', views.DocuflowLoginView.as_view(), name='user_login')
]
