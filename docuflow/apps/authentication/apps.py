from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'docuflow.apps.authentication'
