import logging

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import AuthenticationForm
from django.conf import settings
from django.core.cache import cache
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy, reverse, set_urlconf
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_str
from django.views.generic.edit import FormView, View
from django.views.generic import TemplateView, RedirectView

from annoying.functions import get_object_or_None

from docuflow.apps.common import utils
from docuflow.apps.period.enums import YearStatus
from docuflow.apps.subscription.models import SubscriptionPlan
from docuflow.apps.accounts.models import Profile, UserLoginLog
from docuflow.apps.company.models import Company, Branch
from docuflow.apps.period.models import Year
from docuflow.apps.company.utils import set_company_url_conf
from docuflow.apps.company.services import get_company_from_path
from docuflow.apps.accounts.tokens import account_activation_token
from .forms import ProfilePlanForm, RequestPasswordForm
from .services import CreateDefaultRoles
from .exceptions import RoleNotFoundExcecption

logger = logging.getLogger(__name__)


class RegistrationView(View):
    template_name = "home/signup.html"

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('plans_pricing'))


class CompanyLoginRedirectView(RedirectView):

    def get(self, request, *args, **kwargs):
        redirect_url = reverse('user_login')
        company = getattr(self.request, 'company', self.request.session.get('company_slug', None))
        if company:
            redirect_url = reverse('company_login', kwargs={'slug': company})
        logout(self.request)
        return HttpResponseRedirect(redirect_url)


class CompanyInvoiceRedirectView(RedirectView):

    def get(self, request, *args, **kwargs):
        redirect_url = reverse('user_login')
        company = getattr(self.request, 'company', self.request.session.get('company_slug', None))
        if company:
            redirect_url = reverse('all-invoices', kwargs={'slug': company})
        else:
            logout(self.request)
        return HttpResponseRedirect(redirect_url)


class AjaxLoginView(FormView):
    form_class = AuthenticationForm

    def get_company(self):
        return Company.objects.filter(slug=self.kwargs['slug']).first()

    def login_log(self, profile):
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ipaddress = x_forwarded_for.split(',')[-1].strip()
        else:
            ipaddress = self.request.META.get('REMOTE_ADDR')

        UserLoginLog.objects.create(profile=profile, message='Login successful', remote_addr=ipaddress)

    # noinspection PyMethodMayBeStatic
    def create_roles(self, profile):
        default_roles = CreateDefaultRoles(profile)
        roles = default_roles.execute()
        return roles

    def _get_role(self, profile, roles):
        role = None
        if len(roles) > 0:
            role = roles[0]
        if not role:
            logger.info("Default roles not found, create new ones for this company")
            roles = self.create_roles(profile)
            return self._get_role(profile, roles)
        return role

    def get_default_role(self, profile, roles):
        if profile.role:
            return profile.role
        else:
            try:
                return self._get_role(profile, roles)
            except Exception as exception:
                logger.exception(exception)
                logger.info("Error getting default role for profile {} -> {}".format(profile, exception.__str__()))
                raise RoleNotFoundExcecption('No roles found for this company, please contact administrator/webmaster')

    # noinspection PyMethodMayBeStatic
    def get_default_profile_year(self, company, profile):
        year = Year.objects.filter(company=company, is_active=True, status=YearStatus.OPEN).first()
        if not year:
            year = Year.objects.active(company=company).first()
            if year:
                profile.year = year
                profile.save()
                return year
            else:
                year = Year.objects.open(company=company).first()
                if year:
                    profile.year = year
                    profile.save()
                    return year
        else:
            profile.year = year
            profile.save()
            return year

    # noinspection PyMethodMayBeStatic
    def get_default_branch(self, company, profile):
        if profile.branch:
            return profile.branch
        else:
            branch = Branch.objects.filter(company=company, is_default=True).first()
            if branch:
                return branch
            else:
                branch = Branch.objects.filter(company=company).first()
                if branch:
                    return branch

    # noinspection PyMethodMayBeStatic
    def get_user_profile(self, user):
        return Profile.objects.prefetch_related(
            'roles', 'branches'
        ).select_related(
            'company', 'role', 'branch', 'user', 'year'
        ).get(
            user=user
        )

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred trying to login, please try again',
                                 'errors': form.errors
                                 }, status=400)
        return super(AjaxLoginView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            response = {'redirect_url': '', 'error': False, 'text': '', 'details': ''}
            status = 200
            try:
                user = form.get_user()
                logger.info(f"Authenticated user is {user}")
                if user is not None:
                    company = self.get_company()
                    if company:
                        self.request.session['company'] = company.id
                        self.request.session['company_slug'] = company.slug
                        set_company_url_conf(company.slug)
                    if company.is_active:
                        logger.info(f"User status {user.is_active}")
                        if user.is_active:
                            profile = self.get_user_profile(user)
                            self.request.session['user'] = user.id
                            if profile and company.id == profile.company_id:
                                roles = profile.roles.all()
                                role = self.get_default_role(profile=profile, roles=roles)
                                # self.check_permissions(company, profile)
                                # if not company:
                                self.request.session['company'] = company.id
                                self.request.session['company_slug'] = company.slug

                                branch = self.get_default_branch(company, profile)
                                year = self.get_default_profile_year(company, profile)

                                self.request.session['profile'] = profile.id
                                if roles:
                                    roles_cache_key = utils.get_profile_roles_cache_key(
                                        profile_id=profile.id, company_id=company.id
                                    )
                                    cache.set(roles_cache_key, roles)

                                if role:
                                    self.request.session['role'] = role.id
                                    role_cache_key = utils.get_profile_role_cache_key(
                                        profile_id=profile.id, company_id=company.id
                                    )
                                    cache.set(role_cache_key, role)
                                else:
                                    self.request.session['role'] = None

                                if branch:
                                    self.request.session['branch'] = branch.id
                                    branch_cache_key = utils.get_profile_branch_cache_key(
                                        profile_id=profile.id, company_id=company.id
                                    )
                                    cache.set(branch_cache_key, branch)
                                else:
                                    self.request.session['branch'] = None

                                if year:
                                    self.request.session['year'] = year.id
                                    year_cache_key = utils.get_profile_year_cache_key(
                                        profile_id=profile.id, company_id=company.id
                                    )
                                    cache.set(year_cache_key, year)
                                else:
                                    self.request.session['year'] = None

                                # year_cache_key = f"{user.id}_year_{company.id}"
                                # cache.set(year_cache_key, year)

                                profile_cache_key = utils.get_profile_cache_key(
                                    user_id=user.id,
                                    company_id=company.id
                                )
                                cache.set(profile_cache_key, profile)

                                years_cache_key = utils.get_profile_years_cache_key(session=self.request.session)
                                cache.set(years_cache_key, Year.objects.filter(company=self.get_company()).all())

                                self.login_log(profile)

                                company_slug = getattr(self.request, 'company', self.request.session.get('company_slug', None))

                                if company_slug:
                                    login(self.request, user)

                                urlconf = getattr(self.request, 'urlconf', None)
                                if not urlconf:
                                    urls = set_company_url_conf(str(company_slug), True)
                                    set_urlconf(urls)
                                    setattr(self.request, 'urlconf', urls)
                                urlconf = getattr(self.request, 'urlconf', None)

                                if urlconf:
                                    logger.info("Url for company is set now")
                                    response['redirect_url'] = reverse_lazy('all-invoices')
                                    response['text'] = 'Login successful'
                                else:
                                    response['error'] = True
                                    response['text'] = 'Login to company failed, please try again'
                            else:
                                response['text'] = 'Company and profile not matching'
                                response['error'] = True
                                status = 400
                        else:
                            response['text'] = 'User is not yet active, contact you company administrator to activate'
                            status = 400
                            response['error'] = True
                    else:
                        response['text'] = 'Company is not yet active, please contact docuflow administrator to activate'
                        status = 400
                        response['error'] = True
                else:
                    response['text'] = 'User credentials not valid, please try again'
                    status = 400
                    response['error'] = True
            except RoleNotFoundExcecption as exception:
                logger.exception(exception)
                response['text'] = str(exception)
                response['details'] = str(exception)
                status = 500
            return JsonResponse(response, status=status)
        return super(AjaxLoginView, self).form_valid(form)


class CompanyLoginView(FormView):
    template_name = "home/login.html"
    form_class = AuthenticationForm
    success_url = reverse_lazy('all-invoices')

    def get(self, request, **kwargs):
        is_authenticated = request.user.is_authenticated
        if is_authenticated:
            return HttpResponseRedirect(reverse_lazy('all-invoices'))
        """Handle GET requests: instantiate a blank version of the form."""
        return super(CompanyLoginView, self).get(request)

    def get_company(self):
        company_slug = getattr(self.kwargs, 'company_id', None)
        if not company_slug:
            company_slug = getattr(self.kwargs, 'slug', None)
        if not company_slug:
            _, company_slug = get_company_from_path(self.request.path_info)
        if not company_slug:
            raise ObjectDoesNotExist('Company {} does not exist'.format(self.kwargs['slug']))
        return get_object_or_None(Company, slug=company_slug)

    def get_context_data(self, **kwargs):
        context = super(CompanyLoginView, self).get_context_data(**kwargs)
        company = self.get_company()

        urls = set_company_url_conf(company.slug)
        urlconf = getattr(self.request, 'urlconf', None)
        if not urlconf:
            logger.info(f"Url configuration for {company} --> {urlconf} not found, set it up to {urls}")
            setattr(self.request, 'urlconf', urls)

        if company:
            self.request.session['company'] = company.id
            self.request.session['company_slug'] = company.slug
        context['company'] = company
        return context

    def form_invalid(self, form):
        messages.error(self.request, 'Error logging in, please try again {}'.format(form.errors))
        slug = None
        if 'company_id' in self.kwargs:
            slug = self.kwargs['company_id']
        if 'slug' in self.kwargs:
            slug = self.kwargs['slug']
        if slug:
            return HttpResponseRedirect(reverse('company_login', kwargs={'slug': slug}))
        return HttpResponseRedirect(reverse('user_login'))

    def get_current_year(self, company):
        return Year.objects.filter(company=company, is_open=True).order_by('-year').first()

    def form_valid(self, form):
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password')
        try:
            user = authenticate(username=username, password=raw_password)
            if user is not None:
                company = self.get_company()
                if company:
                    self.request.session['company'] = company.id
                    self.request.session['company_slug'] = company.slug

                if user.is_active:
                    profile = Profile.objects.get(user=user)
                    self.request.session['user'] = user.id
                    if profile and company.id == profile.company_id:
                        role_id = None
                        roles = profile.roles.all()
                        if roles:
                            role_id = roles[0].id

                        self.request.session['role'] = role_id
                        self.request.session['profile'] = profile.id

                        if not company:
                            self.request.session['company'] = profile.company.id
                            self.request.session['company_slug'] = profile.company.slug

                        year = self.get_current_year(company)
                        if year:
                            self.request.session['year_id'] = year.id

                        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
                        if x_forwarded_for:
                            ipaddress = x_forwarded_for.split(',')[-1].strip()
                        else:
                            ipaddress = self.request.META.get('REMOTE_ADDR')

                        UserLoginLog.objects.create(
                            profile=profile,
                            message='Login successful',
                            remote_addr=ipaddress
                        )
                        if self.request.session['company']:
                            logger.info('Company not in session')
                            login(self.request, user)

                        return redirect(reverse_lazy('all-invoices'))
                    else:
                        messages.error(self.request, 'Company and profile not matching')
                        return redirect(reverse_lazy('company_login', kwargs={'slug': self.kwargs['company_id']}))
                else:
                    messages.error(self.request, 'User not active')
                    return redirect(reverse_lazy('company_login', kwargs={'slug': self.kwargs['company_id']}))
            else:
                messages.error(self.request, 'User not found')
                return redirect(reverse_lazy('company_login', kwargs={'slug': self.kwargs['company_id']}))
        except Exception as exception:
            messages.error(self.request, 'Unexpected error occurred')
        return super(CompanyLoginView, self).form_valid(self)


class DocuflowLoginView(FormView):
    template_name = "home/docuflow_login.html"
    form_class = AuthenticationForm
    success_url = reverse_lazy('all_companies')

    def get_context_data(self, **kwargs):
        logout(self.request)
        context = super(DocuflowLoginView, self).get_context_data(**kwargs)
        return context

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('docuflow_login'))

    def form_valid(self, form):
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password')
        try:
            user = authenticate(username=username, password=raw_password)
            if user and user.is_staff:
                profile = Profile.objects.get(user=user)
                self.request.session['user'] = user.id
                self.request.session['profile'] = profile.id

                x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
                if x_forwarded_for:
                    ipaddress = x_forwarded_for.split(',')[-1].strip()
                else:
                    ipaddress = self.request.META.get('REMOTE_ADDR')

                UserLoginLog.objects.create(
                    profile=profile,
                    message='Login successful',
                    remote_addr=ipaddress
                )
                login(self.request, user)
                return redirect(reverse_lazy('all_companies'))
            else:
                messages.error(self.request, 'User not found')
                return redirect(reverse_lazy('user_login'))
        except Exception as exception:
            messages.error(self.request, 'Unexpected error occurred')
        return super(DocuflowLoginView, self).form_valid(self)


class ModuleView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        slug = self.kwargs.get('slug', None)
        if slug:
            return HttpResponseRedirect(reverse_lazy('company_login', kwargs={'slug': slug}))
        else:
            return HttpResponseRedirect(reverse_lazy('homepage'))


class LogoutView(LoginRequiredMixin, View):
    template_name = 'documents/ocr_document.html'

    def get(self, request, *args, **kwargs):
        # TODO - Clear all user cache
        user = request.user
        cache.set(f'profile_{user.id}_branches', [])
        cache.set(f'profile_{user.id}_departments', [])
        cache.set(f'profile_{user.id}_categories', [])
        cache.set(f'profile_{user.id}_accesses_all', [])
        cache.set(f'profile_{user.id}_dealerships', [])
        cache.set(f'profile_{user.id}_dealership', None)
        cache.set(f'profile_{user.id}_branch', None)
        cache.set(f'profile_{user.id}_department', None)

        logout(self.request)
        company = self.request.session.get('company_slug')
        if company:
            return HttpResponseRedirect(reverse_lazy('company_login', kwargs={'slug': company}))
        else:
            return HttpResponseRedirect(reverse_lazy('user_login'))


class LoginView(View):

    def get(self, request, *args, **kwargs):
        logout(self.request)
        company = self.request.session.get('company_slug')
        if company:
            return HttpResponseRedirect(reverse_lazy('company_login', kwargs={'slug': company}))
        else:
            return HttpResponseRedirect(reverse_lazy('homepage'))


class CompanyUserLogoutView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        redirect_url = reverse('user_login')
        company = getattr(self.request, 'company', self.request.session.get('company_slug', None))
        if company:
            redirect_url = reverse('company_login', kwargs={'slug': company})
        logger.info(f"Company {company}")
        logout(self.request)
        return HttpResponseRedirect(redirect_url)


class UpdateProfileView(FormView):
    template_name = "home/plan.html"
    form_class = ProfilePlanForm

    def get_subscription_plan(self):
        subscription_plan = SubscriptionPlan.objects.get(slug=self.kwargs['slug'])
        return subscription_plan

    def get_user_profile(self):
        profile = Profile.objects.get(user=self.request.session['user'])
        return profile

    def get_form_kwargs(self):
        kwargs = super(UpdateProfileView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(UpdateProfileView, self).get_context_data(**kwargs)
        context['subscription_plan'] = self.get_subscription_plan()
        context['profile'] = self.get_user_profile()
        return context

    def form_valid(self, form):
        try:
            with transaction.atomic():
                user = self.request.session['user']

                subscription_plan = SubscriptionPlan.objects.get(slug=self.kwargs['slug'])

                profile = Profile.objects.get(user=user)
                user = User.objects.get(pk=user)
                company = profile.company
                company.subscription_plan = subscription_plan
                company.save()

                subject = 'Activate Your Docuflow Account'
                email_data = {
                    'username': user.username,
                    'company': company,
                    'domain': self.request.get_host(),
                    'uid': urlsafe_base64_encode(force_bytes(profile.pk)),
                    'token': account_activation_token.make_token(user),
                    'email': user.email,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                }
                message = render_to_string('mailer/account_activation_email.html', email_data)
                user.email_user(subject, message, from_email=settings.DEFAULT_FROM_EMAIL, html_message=message)
                # send_signup_email.delay(email_data, subject, message)
            return redirect(reverse('company_login', kwargs={'slug': company.slug}))
        except Exception as exception:
            print('Error {} at '.format(exception))
            messages.error(self.request, "Unexpected error occurred {}".format(exception.__str__()))
            return redirect(reverse('subscription_plan_detail', kwargs={'slug': self.kwargs['slug']}))


class UserPasswordRequest(FormView):
    template_name = 'home/request_password.html'
    form_class = RequestPasswordForm

    def get_company(self):
        return get_object_or_None(Company, slug=self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        context = super(UserPasswordRequest, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context

    def form_valid(self, form):
        try:
            profile = Profile.objects.filter(
                user__email=form.cleaned_data['email_address'], company__slug=self.kwargs['slug']
            ).first()
            if profile:
                user = profile.user
                company = profile.company
                subject = 'Activate Your Docuflow Account'
                email_data = {
                    'username': user.username,
                    'company': company,
                    'domain': self.request.get_host(),
                    'uid': urlsafe_base64_encode(force_bytes(profile.pk)),
                    'token': account_activation_token.make_token(user),
                    'email': user.email,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                }
                message = render_to_string('mailer/account_activation_email.html', email_data)
                user.email_user(subject, message, from_email='info@docuflow.co.za', html_message=message)
                messages.success(self.request, 'Email with instructions send to you email address.')
            else:
                messages.error(self.request, 'User with entered email not found')
            return redirect(reverse('user_password_request', kwargs={'slug': self.kwargs['slug']}))
        except Exception as exception:
            messages.error(self.request, "Unexpected error occurred {}".format(exception.__str__()))
            return redirect(reverse('user_password_request', kwargs={'slug': self.kwargs['slug']}))


class AccountActivationSentView(TemplateView):
    template_name = 'home/index.html'


class ActiveView(View):

    def get(self, request, *args, **kwargs):
        try:
            uid = force_str(urlsafe_base64_decode(self.kwargs['uidb64']))
            user = User.objects.get(pk=uid)
        except User.DoesNotExist as e:
            user = None

        if user is not None and account_activation_token.check_token(user, self.kwargs['token']):
            user.is_active = True
            user.profile.email_confirmed = True
            user.save()
        return redirect('homepage')
