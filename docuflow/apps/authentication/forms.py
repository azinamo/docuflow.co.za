import logging

from django import forms
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from docuflow.apps.common.utils import cache_profile
from docuflow.apps.accounts.selectors import get_user_profile
from docuflow.apps.company.selectors import get_company_by_slug
from . import services

logger = logging.getLogger(__name__)


# Create your forms here.
class UserForm(UserCreationForm):
    first_name = forms.CharField(max_length=200, required=True, help_text='Required')
    last_name = forms.CharField(max_length=100, required=True, help_text='Required')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1')


class ProfilePlanForm(forms.Form):
    subscription_plan_id = forms.HiddenInput()
    user_id = forms.HiddenInput()


class RequestPasswordForm(forms.Form):
    email_address = forms.EmailField(required=True, label='Email Address')


class CompanyUserAuthenticationForm(AuthenticationForm):
    user = None
    profile = None
    company = None

    def __init__(self, *args, **kwargs):
        self.company_slug = kwargs.pop('company_slug')
        self.url_conf = kwargs.pop('url_conf')
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        if not self.company_slug:
            raise ValidationError('Company not found')

        self.user = authenticate(username=cleaned_data['username'], password=cleaned_data['password'])
        if not self.user:
            raise ValidationError('User credentials not valid, please try again')

        self.profile = get_user_profile(self.user)
        if not self.profile:
            raise ValidationError('User profile not found, please try again')

        self.company = get_company_by_slug(self.company_slug)
        if not self.company:
            raise ValidationError('Company not found, please ensure your company is registered by Docuflow')

        if not self.company.is_active:
            raise ValidationError('Company is not yet active, please contact administrator to activate.')

        if not self.user.is_active:
            raise ValidationError('Profile not yet active, please contact company administrator to activate.')

        logger.info(f"User status {self.user.is_active}")

        if not self.company.id == self.profile.company_id:
            raise ValidationError('Company and profile not matching')

        if not self.url_conf:
            logger.info('Url settings not found')
            raise ValidationError('Login to company failed, please try again')

        return cleaned_data

    def execute(self, request):
        logger.info(f"Login ")

        login(request, self.user)

        role_id = services.get_default_role(self.profile, self.profile.roles.all())
        # self.check_permissions(company, profile)
        branch = services.get_default_branch(self.company, self.profile)
        year = services.get_default_profile_year(self.company, self.profile)

        request.session['profile'] = self.profile.id
        if role_id:
            logger.info(f"Setting role role to {role_id} ")
            request.session['role'] = role_id
        else:
            request.session['role_id'] = None
        if branch:
            logger.info(f"Setting branch to {branch} ")
            request.session['branch_id'] = branch.id
        else:
            request.session['branch_id'] = None
        if year:
            logger.info(f"Setting year to {year} ")
            request.session['year'] = year.id
        else:
            request.session['year'] = None

        logger.info(f"Setting company in session to {self.company.id}  ")
        logger.info(f"Setting company slug in session to {self.company.slug}  ")
        request.session['company'] = self.company.id
        request.session['company_slug'] = self.company.slug

        urlconf = getattr(request, 'urlconf')
        company_urls = services.set_company_url_conf(self.company.slug, False)
        if not urlconf:
            setattr(request, 'urlconf', company_urls)

        cache_profile(self.profile.company.id, profile=self.profile, branch=branch, year=year)

        services.login_log(self.profile, request)
        return self.user
