import logging
import pprint

from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.exceptions import ValidationError
from django.db import transaction
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from docuflow.apps.accounts.enums import DocuflowPermission
from docuflow.apps.accounts.models import UserLoginLog, Role, PermissionGroup
from docuflow.apps.accounts.tokens import account_activation_token
from docuflow.apps.company.models import Branch
from docuflow.apps.company.utils import set_company_url_conf
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.period.models import Year
from docuflow.apps.period.selectors import get_open_year


pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class RegistrationException(Exception):
    pass


class Registration(object):

    def __init__(self, company, plan, user):
        logger.info('---------START REGISTRATION----------')
        self.company = company
        self.plan = plan
        self.user = user

    def assign_permissions(self):
        admin_role = get_object_or_None(Role, company=self.company, name='admin')
        if not admin_role:
            Role.objects.create(
             label='Administrator',
             name='admin',
             company=self.company
            )

        for permission in Permission.objects.all():
            self.user.user_permissions.add(permission)

    def create_name(self, label):
        name_list = label.upper().split(' ')
        counter = 0
        codename = ''
        for name in name_list:
            if counter < 2:
                codename += f"{name[0:3]} "
            counter = counter + 1
        return codename

    def create_default_roles(self, profile):
        logger.info('Create default roles and assign to profile -> {}'.format(profile))
        role_permissions = [
            {'role': 'Administrator', 'group': 'Level 10', 'is_default': False},
            {'role': None, 'group': 'Level 6', 'is_default': False},
            {'role': None, 'group': 'Level 8', 'is_default': False},
            {'role': None, 'group': 'Level 2', 'is_default': False},
            {'role': 'Logger', 'group': 'Logger', 'is_default': True}
        ]

        choices = DocuflowPermission.get_choices()
        for role_permission in role_permissions:
            if role_permission['role']:
                role = Role.objects.create(
                    company=self.company,
                    label=role_permission['role'],
                    name=self.create_name(role_permission['role'])
                )
                profile.roles.add(role)

                permission_group = PermissionGroup.objects.create(
                    company=self.company,
                    label=role_permission['group'],
                    is_default=role_permission['is_default'],
                    name=f"{self.company.id}-{self.company.name}-{role_permission['group']}"
                )
                if permission_group:
                    role.permission_groups.add(permission_group)

                perms = [pp.pk for pp in Permission.objects.filter(codename__in=[c[0] for c in choices])]
                permission_group.permissions.set(perms)
            else:
                PermissionGroup.objects.create(
                    company=self.company,
                    label=role_permission['group'],
                    is_default=role_permission['is_default'],
                    name=f"{self.company.id}-{self.company.name}-{role_permission['group']}"
                )

    def create_default_invoice_types(self):
        invoice_types = [{'name': 'Tax Invoice', 'account_posting': True, 'is_credit': False, 'is_default': True},
                         {'name': 'Credit Note', 'account_posting': True, 'is_credit': True, 'is_default': False},
                         {'name': 'Statement', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Purchase Order', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Quote', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Invoice', 'account_posting': True, 'is_credit': False, 'is_default': False},
                        ]
        for invoice_type in invoice_types:
            InvoiceType.objects.create(
                name=invoice_type['name'],
                is_default=invoice_type['is_default'],
                is_account_posting=invoice_type['account_posting'],
                is_credit=invoice_type['is_credit'],
                company=self.company
            )

    def add_urls(self, slug):
        set_company_url_conf(slug)

    def execute(self, host):
        try:
            with transaction.atomic():

                profile = self.user.profile
                profile.company = self.company
                profile.save()
                logger.info('Profile created -> {}'.format(profile))

                self.company.administrator = self.user
                self.company.subscription_plan = self.plan
                self.company.save()

                self.company.activate()
                logger.info('Company Admin assigned created -> {}'.format(profile))

                self.create_default_roles(self.user.profile)

                self.create_default_invoice_types()

                self.add_urls(self.company.slug)

                subject = 'Activate Your Docuflow Account'
                email_data = {
                    'username': self.user.username,
                    'company': self.company,
                    'domain': host,
                    'uid': urlsafe_base64_encode(force_bytes(str(self.user.profile.id), strings_only=True)),
                    'token': account_activation_token.make_token(self.user),
                    'email': self.user.email,
                    'first_name': self.user.first_name,
                    'last_name': self.user.last_name,
                }
                message = render_to_string('mailer/account_activation_email.html', email_data)
                self.user.email_user(subject, message, from_email=settings.DEFAULT_FROM_EMAIL, html_message=message)
                # send_signup_email.delay(email_data, subject, message)

        except RegistrationException as exception:
            pass
            # raise RegistrationException('An unexpected error occured registrating the company, please try again.')


class CreateDefaultRoles:

    def __init__(self, profile):
        self.profile = profile

    def get_default_roles(self):
        roles_with_permission_groups = [
            {'role': 'Administrator', 'group': 'Level 10', 'is_default': False, 'is_admin': True},
            {'role': None, 'group': 'Level 6', 'is_default': False, 'is_admin': False},
            {'role': None, 'group': 'Level 8', 'is_default': False, 'is_admin': False},
            {'role': None, 'group': 'Level 2', 'is_default': False, 'is_admin': False},
            {'role': 'Logger', 'group': 'Logger', 'is_default': True, 'is_admin': False}
        ]
        return roles_with_permission_groups

    def execute(self):
        roles_with_permission_groups = self.get_default_roles()
        roles = []

        for role_permission in roles_with_permission_groups:
            default_role = role_permission.get('role', None)
            if default_role:
                role = Role.objects.create(
                    company=self.profile.company,
                    label=default_role,
                    name=self.create_name(default_role)
                )
                roles.append(role)
                self.profile.roles.add(role)

                self.create_role_permission_group(role, role_permission.get('group', None),
                                                  role_permission.get('is_default', False),
                                                  role_permission.get('is_admin', False))
            else:
                self.create_role_permission_group(None, role_permission.get('group', None),
                                                  role_permission.get('is_default', False),
                                                  role_permission.get('is_admin', False)
                                                  )
        return roles

    def create_role_permission_group(self, role, role_group, is_default, is_admin):
        permission_group = PermissionGroup.objects.create(
            company=self.profile.company,
            label=role_group,
            is_default=is_default,
            name=f"{self.profile.company.id}-{self.profile.company.name}-{role_group}"
        )
        if role and permission_group:
            role.permission_groups.add(permission_group)
            if is_admin:
                self.assgin_permissions(permission_group)
        return permission_group

    def create_name(self, label):
        name_list = label.upper().split(' ')
        counter = 0
        codename = ''
        for _str in name_list:
            if counter < 2:
                codename += f" {_str[0:3]}"
            counter = counter + 1
        return codename

    def assgin_permissions(self, permission_group):
        for permission in Permission.objects.all():
            permission_group.permissions.add(permission)


class AssignAdminPermissions:

    def __init__(self, company, profile):
        self.company = company
        self.profile = profile

    def is_admin(self):
        return self.company.administrator == self.profile

    def execute(self):
        if self.is_admin():
            try:
                for permission in Permission.objects.all():
                    self.profile.user.user_permissions.add(permission)
            except Exception as e:
                pass


def get_default_role(profile, roles):
    try:
        return get_role_id(profile, roles)
    except Exception as exception:
        raise ValidationError('No roles found for this company, please contact administrator/webmaster')


def get_role_id(profile, roles):
    role = None
    if len(roles) > 0:
        role = roles[0]
    if not role:
        roles = create_roles(profile)
        return get_role_id(profile, roles)
    return role.id


def create_roles(profile):
    logger.info("Default roles not found, create new ones for this company")
    default_roles = CreateDefaultRoles(profile)
    roles = default_roles.execute()
    return roles


def get_default_profile_year(company, profile):
    year = get_open_year(company)
    if not year:
        year = Year.objects.open(company=company).first()
        if year:
            profile.year = year
            profile.save()
            return year
        else:
            year = Year.objects.open(company=company).first()
            if year:
                profile.year = year
                profile.save()
                return year
    else:
        profile.year = year
        profile.save()
        return year


def get_default_branch(company, profile):
    if profile.branch:
        return profile.branch
    else:
        branch = Branch.objects.filter(company=company, is_default=True).first()
        if branch:
            return branch
        else:
            branch = Branch.objects.filter(company=company).first()
            if branch:
                return branch


def login_log(profile, request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ipaddress = x_forwarded_for.split(',')[-1].strip()
    else:
        ipaddress = request.META.get('REMOTE_ADDR')

    UserLoginLog.objects.create(
        profile=profile, message='Login successful', remote_addr=ipaddress
    )
