from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.VatView.as_view(), name='vat_report'),
    path('create/', csrf_exempt(views.CreateVatReportView.as_view()), name='create_vat_report'),
    path('<int:pk>/edit/', views.EditVatReportView.as_view(), name='edit_vat_report'),
    path('run/<int:period>/report/', views.RunVatReportView.as_view(), name='run_vat_report'),
    path('<int:report_id>/detail/', views.ReportView.as_view(), name='view_vat_report'),
    path('save/<int:period>/report/', views.SaveVatReportView.as_view(), name='save_vat_report'),
    path('summary', views.VatSummaryView.as_view(), name='vat_summary'),
]
