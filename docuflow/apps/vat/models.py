import logging

from django.db import models

from docuflow.apps.company.models import Company, Account, VatCode, ObjectItem, CompanyObject, PaymentMethod, Currency
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.period.models import Period, Year

logger = logging.getLogger(__name__)


class VatReport(models.Model):
    company = models.ForeignKey(Company, related_name="vat_reports", on_delete=models.CASCADE)
    year = models.ForeignKey(Year, related_name='year_vat_reports', on_delete=models.DO_NOTHING)
    category_period = models.PositiveIntegerField(null=True, blank=True)
    period = models.ForeignKey(Period, null=True, blank=True, related_name='vat_reports', on_delete=models.DO_NOTHING)
    ledger = models.ForeignKey('journals.Journal', null=True, blank=True, related_name='journal_vat_reports', on_delete=models.DO_NOTHING)
    report_number = models.CharField(max_length=255, null=True, blank=True)
    created_by = models.ForeignKey('accounts.Profile', related_name='created_vat_reports', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.pk or not self.report_number:
            self.report_number = self.get_month_repr()
        return super(VatReport, self).save(*args, **kwargs)

    def get_month_repr(self):
        return self.period.to_date.strftime('%Y%m')

    def get_category_period(self, year, period):
        months = self.get_months()
        for m, name in months.items():
            if m == period:
                return self.get_month_repr(year, m, name)
        return ''

    def __str__(self):
        if self.period:
            return f"{self.period.to_date.strftime('%Y%m')}"
        return f"{self.report_number}"

    class Meta:
        ordering = ['created_at']
        verbose_name_plural = 'Vat Reports'
