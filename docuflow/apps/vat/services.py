import logging
from collections import OrderedDict
from decimal import Decimal
from typing import List

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Company, VatCode
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.invoice.reports import Payment
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.period.models import Year
from docuflow.apps.vat.models import VatReport
from .exceptions import NoJournalAccountException, VatException

logger = logging.getLogger(__name__)


class VatReportMixin(object):

    def get_journal_lines(self):
        raise NotImplementedError('Not implemented')

    def is_valid_for_vat_report(self, journal_line: JournalLine, vat_amount: Decimal):
        raise NotImplementedError('Not implemented')

    # noinspection PyMethodMayBeStatic
    def get_vat_amount(self, journal_line):
        amount = 0
        if journal_line.debit:
            amount = journal_line.debit
        elif journal_line.credit:
            amount = journal_line.credit * -1

        if journal_line.is_reversal:
            amount = amount * -1
        return amount

    # noinspection PyMethodMayBeStatic
    def get_payments(self, payment_type, lines):
        return {
            (payment.id, payment_type): payment
            for payment in Payment.objects.select_related('supplier', 'company', 'period', 'period__period_year').filter(
                pk__in=[j.object_id for j in lines if j.content_type == payment_type])
        }

    def execute(self):
        journal_lines = self.get_journal_lines()
        payment_type = ContentType.objects.get_for_model(Payment, for_concrete_model=False)
        payments = self.get_payments(lines=journal_lines, payment_type=payment_type)

        vat_codes_lines = {}
        for journal_line in journal_lines:
            total_amount = journal_line.including_vat if journal_line.including_vat else Decimal(0)
            vat_amount = self.get_vat_amount(journal_line=journal_line)
            net_amount = journal_line.excluding_vat if journal_line.excluding_vat else Decimal(0)

            if not self.is_valid_for_vat_report(journal_line=journal_line, vat_amount=vat_amount):
                continue

            setattr(journal_line, 'payment', payments.get((journal_line.object_id, journal_line.content_type), None))

            if journal_line.vat_code in vat_codes_lines:
                vat_codes_lines[journal_line.vat_code]['total'] += total_amount
                vat_codes_lines[journal_line.vat_code]['total_vat'] += vat_amount
                vat_codes_lines[journal_line.vat_code]['total_net'] += net_amount
                vat_codes_lines[journal_line.vat_code]['lines'].append(journal_line)
            else:
                vat_codes_lines[journal_line.vat_code] = {
                    'vat_code': journal_line.vat_code,
                    'total': total_amount,
                    'total_vat': vat_amount,
                    'total_net': net_amount,
                    'lines': [journal_line]
                }
        return vat_codes_lines


class VatReportGenerator(VatReportMixin):

    last_date = None

    def __init__(self, company: Company, vat_report: VatReport):
        self.vat_report = vat_report
        self.company = company

    def get_vat_accounts(self):
        return [vat_code.account.id for vat_code in self.company.company_vat_codes.all() if vat_code.account]

    def get_journal_lines(self):
        return JournalLine.objects.select_related(
            'invoice', 'account', 'journal__company', 'vat_code', 'journal', 'journal__year', 'journal__period',
            'supplier', 'invoice', 'invoice__supplier', 'invoice__company', 'invoice__invoice_type', 'vat_report',
            'sales_invoice', 'sales_invoice__customer', 'sales_invoice__invoice_type',
            'sales_invoice__customer__company', 'content_type'
        ).prefetch_related('object_items').filter(
            journal__company=self.company, vat_report=self.vat_report, vat_code__percentage__gt=0
        )

    def is_valid_for_vat_report(self, journal_line: JournalLine, vat_amount: Decimal):
        if vat_amount == 0:
            return False
        return True


class PeriodVatReport(VatReportMixin):

    last_date = None

    def __init__(self, company, period):
        self.period = period
        self.company = company
        self.last_date = self.get_last_date()

    def get_vat_accounts(self):
        # TODO - move this to the queryset and filter for account in queryset
        return [vat_code.account.id for vat_code in self.company.company_vat_codes.all() if vat_code.account]

    def get_journal_lines(self):
        return JournalLine.objects.get_vat_lines(company=self.company, vat_code_account_ids=self.get_vat_accounts())

    def is_valid_for_vat_report(self, journal_line: JournalLine, vat_amount: Decimal):
        if vat_amount == 0:
            return False
        return journal_line.accounting_date <= self.last_date

    def get_last_date(self):
        return self.period.to_date


class CreateVatReport:

    def __init__(self, company, period, year, profile, role):
        logger.info('-----------Start --- CreateVatReport-------------')
        self.company = company
        self.period = period
        self.year = year
        self.profile = profile
        self.role = role

    def execute(self):
        logger.info('-----------EXECUTE-------------')
        period_vat_report = PeriodVatReport(self.company, self.period)
        journal_vat_lines = period_vat_report.execute()

        logger.info('-----------LINES-------------')
        logger.info(journal_vat_lines)

        last_date = period_vat_report.last_date

        vat_report = self.create(last_date)
        if vat_report:
            self.update_journal_lines(vat_report, journal_vat_lines)
            self.create_ledger(last_date, vat_report, journal_vat_lines)

    def create(self, last_date):
        logger.info('-----------Create vat report-------------')
        vat_report = VatReport()
        vat_report.company = self.company
        vat_report.category_period = last_date.strftime('%m')
        vat_report.period = self.period
        vat_report.year = self.year
        vat_report.created_by = self.profile
        vat_report.save()
        logger.info('-----------Done {}-------------'.format(vat_report))
        return vat_report

    def get_vat_receivable_account(self):
        vat_receivable_account = self.company.default_vat_receivable_account
        if not vat_receivable_account:
            raise NoJournalAccountException('Company default vat receivable account not found')
        return vat_receivable_account

    def get_vat_payable_account(self):
        vat_payable_account = self.company.default_vat_payable_account
        if not vat_payable_account:
            raise NoJournalAccountException('Company default vat payable account not found')
        return vat_payable_account

    def update_journal_lines(self, vat_report, journal_vat_lines):
        logger.info('-----------update_journal_lines-------------')
        journal_lines = []
        for vat_code, vat_line in journal_vat_lines.items():
            for journal_line in vat_line['lines']:
                journal_line.vat_report = vat_report
                journal_lines.append(journal_line)
        JournalLine.objects.bulk_update(journal_lines, {'vat_report'})

    def get_ledger_lines(self, journal_vat_lines):
        logger.info('-----------get_ledger_lines-------------')
        total_credit = 0
        total_debit = 0
        ledger_lines = []
        content_type = ContentType.objects.get_for_model(JournalLine, for_concrete_model=False)
        logger.info(f'-----------Content type {content_type}-------------')
        for vat_code, journal_vat_line in journal_vat_lines.items():
            if not vat_code:
                raise VatException(f'Vat code {vat_code} not found')

            logger.info(f"{vat_code} Total vat amount is {journal_vat_line['total_vat']} ")
            debit = 0
            credit = 0
            if journal_vat_line['total_vat'] > 0:
                credit = journal_vat_line['total_vat']
                total_credit += credit
            else:
                debit = journal_vat_line['total_vat'] * -1
                total_debit += debit

            logger.info(f"And its debit {debit} or credit {credit}")
            ledger_lines.append({
                'account': vat_code.account,
                'credit': credit,
                'debit': debit,
                'text': '',
                'object_items': []
            })

        difference = total_credit - total_debit
        if difference > 0:
            ledger_lines.append({
                'account': self.get_vat_receivable_account(),
                'credit': 0,
                'debit': difference,
                'text': '',
                'object_items': []
            })

        else:
            ledger_lines.append({
                'account': self.get_vat_payable_account(),
                'credit': (difference * -1),
                'debit': 0,
                'text': '',
                'object_items': []
            })
        logger.info(f'-----------Ledger lines {ledger_lines}-------------')
        return ledger_lines

    def create_ledger(self, last_date, vat_report, journal_vat_lines):
        logger.info(f'-----------create_ledger {last_date}-------------')

        ledger_lines = self.get_ledger_lines(journal_vat_lines)

        month_number = str(last_date.strftime('%m')).rjust(2, '0')

        ledger = CreateLedger(
            company=vat_report.company,
            year=vat_report.period.period_year,
            period=vat_report.period,
            profile=vat_report.created_by,
            role=self.role,
            text=f'Journals Vat Submissions {vat_report.year}{month_number}',
            journal_lines=ledger_lines,
            date=last_date,
            module=Module.SYSTEM
        )
        ledger = ledger.execute()
        if ledger:
            vat_report.ledger = ledger
            vat_report.save()
        logger.info('-----------Done creating ledger {}-------------'.format(ledger))
        return ledger


def generate_vat_reports(company: Company, vat_types: List[VatCode], year: Year):
    # TODO - Use annotations to calculate totals for the vat report
    reports = VatReport.objects.prefetch_related(
        'vat_lines', 'vat_lines__vat_code', 'vat_lines__vat_code__account', 'vat_lines__journal',
        'vat_lines__journal__period', 'vat_lines__journal__period__period_year', 'vat_lines__invoice',
        'vat_lines__invoice__invoice_type', 'vat_lines__invoice__supplier', 'vat_lines__content_type',
        'vat_lines__object_items', 'vat_lines__journal__year', 'vat_lines__account'
    ).select_related('year', 'period', 'ledger').filter(company=company, year=year)
    period_reports = {}
    for report in reports:
        period_reports[report] = {'report': report, 'total': 0, 'vat_types': OrderedDict(), 'turnover_incl': 0,
                                  'turnover_excl': 0}
        for vat_type in vat_types:
            period_reports[report]['vat_types'][vat_type] = {
                'label': vat_type, 'id': vat_type.id, 'total': 0
            }
        total_turnover_excl = 0
        total_turnover_incl = 0
        for line in report.vat_lines.all():
            if line.vat_code in period_reports[report]['vat_types']:
                amount = line.debit if line.debit else line.credit * -1
                turnover_exc = 0
                turnover_incl = 0
                if line.vat_code.is_output:
                    turnover_exc = calculate_turnover_excl(amount=amount, vat_code=line.vat_code)
                    turnover_incl = turnover_exc + amount

                total_turnover_excl += turnover_exc
                total_turnover_incl += turnover_incl
                period_reports[report]['total'] -= amount
                period_reports[report]['vat_types'][line.vat_code]['total'] += amount
        period_reports[report]['turnover_excl'] = total_turnover_excl
        period_reports[report]['turnover_incl'] = total_turnover_incl
    return period_reports


def calculate_turnover_excl(amount, vat_code: VatCode):
    return Decimal(Decimal(amount) / Decimal(vat_code.percentage / 100)).quantize(Decimal('0.01'))


def calculate_turnover_incl(amount: Decimal, vat_code: VatCode):
    return Decimal((vat_code.percentage / 100) * amount).quantize(Decimal('0.01'))


def get_category_periods(company, year, executed_periods=None):
    executed_periods = executed_periods or []
    company_periods = company.get_vat_periods(year)
    category_periods = []

    for p in company_periods:
        if p[0] not in executed_periods:
            category_periods.append(p)

    return category_periods


def get_executed_periods(company, year):
    reports = VatReport.objects.filter(company=company, year=year)
    periods = []
    for report in reports:
        if report.period:
            periods.append(report.period.id)
    return periods
