import logging
import pprint
from calendar import monthrange

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse, reverse_lazy
from django.utils.timezone import now
from django.utils.translation import gettext as __
from django.views.generic import TemplateView, View
from django.views.generic.edit import UpdateView, FormView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import VatCode
from docuflow.apps.period.models import Period
from docuflow.apps.vat.models import VatReport
from .exceptions import VatException
from .forms import VatReportForm
from .services import PeriodVatReport, CreateVatReport, VatReportGenerator
from .services import generate_vat_reports, get_executed_periods, get_category_periods
from ..journals.services import get_total_for_vat_type

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class VatView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'vat/index.html'

    # noinspection PyMethodMayBeStatic
    def get_vat_types(self, company):
        return VatCode.objects.filter(company=company, percentage__gt=0).order_by('id').all()

    def get_context_data(self, **kwargs):
        context = super(VatView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        vat_types = self.get_vat_types(company)
        context['year'] = year
        context['company'] = company
        context['vat_reports'] = generate_vat_reports(company=company, vat_types=vat_types, year=year)
        context['vat_types'] = vat_types
        return context


class CreateVatReportView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = VatReportForm
    template_name = 'vat/create.html'
    success_url = reverse_lazy('supplier-list')
    success_message = __('Vat report successfully saved')

    def get_context_data(self, **kwargs):
        context = super(CreateVatReportView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateVatReportView, self).get_form_kwargs()
        company = self.get_company()
        year = self.get_year()
        periods = get_executed_periods(company=company, year=year)
        choices = get_category_periods(company=company, year=year, executed_periods=periods)
        kwargs['periods'] = choices
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Vat report could not be created, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateVatReportView, self).form_invalid(form)

    def form_valid(self, form):
        period_id = int(self.request.POST['period'])
        if not period_id:
            return JsonResponse({'text': 'Select a valid period', 'error': True})
        return JsonResponse({
            'text': 'Redirecting ...',
            'error': False,
            'redirect': reverse('run_vat_report', kwargs={'period': period_id})
        })


class EditVatReportView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = VatReport
    form_class = VatReportForm
    template_name = 'supplier/edit.html'
    success_url = reverse_lazy('supplier-list')
    success_message = __('Vat report successfully updated')

    def get_context_data(self, **kwargs):
        context = super(EditVatReportView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditVatReportView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class RunVatReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'vat/run_vat_report.html'

    # noinspection PyMethodMayBeStatic
    def get_period_last_date(self, period_id):
        try:
            period = Period.objects.get(pk=period_id)
            return period.to_date
        except Period.DoesNotExist:
            return None

    def get_period(self):
        return Period.objects.filter(id=self.kwargs['period']).first()

    def get_context_data(self, **kwargs):
        context = super(RunVatReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        period = self.get_period()

        period_vat_report = PeriodVatReport(company=company, period=period)
        journal_lines = period_vat_report.execute()

        context['category_period'] = period_vat_report.last_date
        context['month'] = period.id
        context['company'] = company
        context['journal_lines'] = journal_lines
        context['journal_lines_count'] = len(journal_lines)
        return context


class SaveVatReportView(LoginRequiredMixin, ProfileMixin, View):

    def get_category_last_date(self, month, year):
        start_date = now().date()
        last_date = start_date.replace(year=year.year, month=month, day=monthrange(year.year, month)[1])
        return last_date

    def get_period_last_date(self, period):
        return period.to_date

    def get(self, request, *args, **kwargs):
        period_id = int(self.kwargs['period'])
        if not period_id:
            return JsonResponse({'text': 'Period is required', 'error': True})
        try:
            with transaction.atomic():
                period = Period.objects.get(pk=period_id)

                create_vat_report = CreateVatReport(
                    company=self.get_company(),
                    period=period,
                    year=self.get_year(),
                    profile=self.get_profile(),
                    role=self.get_role()
                )
                create_vat_report.execute()

                return JsonResponse({
                    'text': 'Vat report successfully filed',
                    'error': False,
                    'reload': True
                })
        except VatException as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': str(exception),
                'details': str(exception),
                'error': True
            })


class ReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    model = VatReport
    template_name = 'vat/detail.html'
    success_message = __('Vat report successfully updated')

    def get_template_names(self):
        if self.request.GET.get('print') == '1':
            return 'vat/print.html'
        return 'vat/detail.html'

    def get_vat_report(self):
        return VatReport.objects.get(pk=self.kwargs['report_id'])

    def get_context_data(self, **kwargs):
        context = super(ReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        vat_report = self.get_vat_report()
        period_vat_report = VatReportGenerator(company=company, vat_report=vat_report)
        journal_lines = period_vat_report.execute()

        context['vat_report'] = vat_report
        context['company'] = company
        context['journal_lines'] = journal_lines
        return context


class VatSummaryView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'vat/summary.html'

    # noinspection PyMethodMayBeStatic
    def get_vat_types(self, company):
        return VatCode.objects.filter(company=company, percentage__gt=0).order_by('id').all()

    def get_balance_sheet_report(self, company, year):
        return get_total_for_vat_type(
            company=company,
            year=year,
            start_date=year.start_date,
            end_date=year.end_date
        )

    def get_context_data(self, **kwargs):
        context = super(VatSummaryView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        vat_types = self.get_vat_types(company=company)
        vat_reports = generate_vat_reports(company=company, vat_types=vat_types, year=year)
        balance_sheet_total = self.get_balance_sheet_report(company=company, year=year)
        vat_total = 0
        context['year'] = year
        context['company'] = company
        context['vat_reports'] = vat_reports
        context['vat_types'] = vat_types
        context['balance_sheet_total'] = balance_sheet_total
        context['is_balancing'] = vat_total == balance_sheet_total
        return context
