from django.apps import AppConfig


class VatConfig(AppConfig):
    name = 'docuflow.apps.vat'
