class VatException(Exception):
    pass


class NoJournalAccountException(VatException):
    pass