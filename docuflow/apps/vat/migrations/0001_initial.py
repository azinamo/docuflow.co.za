# Generated by Django 2.0 on 2020-03-28 16:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('journals', '0001_initial'),
        ('period', '0001_initial'),
        ('accounts', '0002_auto_20200328_1821'),
        ('company', '0002_auto_20200328_1821'),
    ]

    operations = [
        migrations.CreateModel(
            name='VatReport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category_period', models.PositiveIntegerField(blank=True, null=True)),
                ('report_number', models.CharField(blank=True, max_length=255, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('company', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vat_reports', to='company.Company')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='created_vat_reports', to='accounts.Profile')),
                ('ledger', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='journal_vat_reports', to='journals.Journal')),
                ('period', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='vat_reports', to='period.Period')),
                ('year', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='year_vat_reports', to='period.Year')),
            ],
            options={
                'verbose_name_plural': 'Vat Reports',
                'ordering': ['created_at'],
            },
        ),
    ]
