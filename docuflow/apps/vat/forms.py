from django import forms


class VatReportForm(forms.Form):

    def __init__(self, *args, **kwargs):
        periods = []
        if 'periods' in kwargs:
            periods = kwargs.pop('periods')
        super(VatReportForm, self).__init__(*args, **kwargs)
        self.fields['period'].choices = periods

    period = forms.ChoiceField(required=True, label='Period', choices={})

