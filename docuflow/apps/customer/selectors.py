from docuflow.apps.customer.models import Customer


def get_customers_with_balances(company_id, include_default=False):
    return Customer.objects.with_balance().select_related(
        'company', 'salesman', 'supplier', 'delivery_method', 'delivery_term', 'default_account'
    ).prefetch_related(
        'pricings', 'addresses'
    ).filter(
        company_id=company_id, is_default=include_default
    )

