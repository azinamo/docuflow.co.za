# Generated by Django 2.0 on 2020-03-28 16:21

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.query_utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounts', '0002_auto_20200328_1821'),
        ('inventory', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('sales', '0001_initial'),
        ('company', '0002_auto_20200328_1821'),
        ('journals', '0001_initial'),
        ('period', '0001_initial'),
        ('customer', '0002_auto_20200328_1821'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledgerpayment',
            name='payment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='customer_ledgers', to='sales.Receipt'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='content_type_customers', to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='created_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='created_customer_ledgers', to='accounts.Profile'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ledgers', to='customer.Customer'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='journal_line',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customer_ledger', to='journals.JournalLine'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='period',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='period_customer_ledgers', to='period.Period'),
        ),
        migrations.AddField(
            model_name='fixedpricing',
            name='inventory',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fixed_prices', to='inventory.BranchInventory'),
        ),
        migrations.AddField(
            model_name='fixedpricing',
            name='pricing',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fixed_prices', to='customer.Pricing'),
        ),
        migrations.AddField(
            model_name='deliveryterm',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='delivery_terms', to='company.Company'),
        ),
        migrations.AddField(
            model_name='customer',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customers', to='company.Company'),
        ),
        migrations.AddField(
            model_name='customer',
            name='default_account',
            field=models.ForeignKey(blank=True, limit_choices_to=django.db.models.query_utils.Q(sub_ledger=2), null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='customer_default_accounts', to='company.Account'),
        ),
        migrations.AddField(
            model_name='customer',
            name='delivery_method',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customer_delivery_methods', to='company.DeliveryMethod'),
        ),
        migrations.AddField(
            model_name='customer',
            name='delivery_term',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customer_delivery_terms', to='customer.DeliveryTerm'),
        ),
        migrations.AddField(
            model_name='customer',
            name='salesman',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='customers', to='sales.Salesman'),
        ),
    ]
