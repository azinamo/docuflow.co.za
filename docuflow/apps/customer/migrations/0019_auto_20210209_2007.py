# Generated by Django 3.1 on 2021-02-09 18:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0018_customer_external_company_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='interest',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True),
        ),
    ]
