# Generated by Django 3.1 on 2020-11-18 08:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('period', '0002_auto_20200422_1713'),
        ('customer', '0012_auto_20200609_1544'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgeAnalysis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unallocated', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('one_twenty_day', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('ninety_day', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('sixty_day', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('thirty_day', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('current', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('balance', models.DecimalField(decimal_places=2, default=0, max_digits=15)),
                ('data', models.JSONField(blank=True, null=True)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='age_analysis', to='customer.customer')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='customer_age_analysis', to='period.period')),
            ],
        ),
    ]
