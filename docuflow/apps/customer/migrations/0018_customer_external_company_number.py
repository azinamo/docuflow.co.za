# Generated by Django 3.1 on 2021-02-09 17:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0017_history_cost_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='external_company_number',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
