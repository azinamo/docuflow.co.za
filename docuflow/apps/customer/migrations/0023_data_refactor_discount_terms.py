# Generated by Django 3.1.2 on 2021-10-21 14:26

from django.db import migrations


def refactoring_customer_discount_terms(apps, schema_editor):
    Customer = apps.get_model('customer.Customer')

    for customer in Customer.objects.all():
        if customer.discount_terms:
            invoice_discount = customer.discount_terms.get('invoice')
            if invoice_discount:
                is_invoice_discounted = invoice_discount.get('is_discounted', False)
                invoice_discount_percentage = float(str(invoice_discount.get('discount', '0')).replace('%', ''))
                customer.is_invoice_discount = is_invoice_discounted
                if is_invoice_discounted and invoice_discount_percentage:
                    customer.invoice_discount = invoice_discount_percentage

            line_discount = customer.discount_terms.get('line')
            if line_discount:
                is_line_discounted = line_discount.get('is_discounted', False)
                line_discount_percentage = float(str(line_discount.get('discount', '0')).replace('%', ''))
                customer.is_line_discount = is_line_discounted
                if is_line_discounted and line_discount_percentage:
                    customer.line_discount = line_discount_percentage

            statement_discount = customer.discount_terms.get('statement')
            if statement_discount:
                is_statement_discounted = statement_discount.get('is_discounted', False)
                statement_discount_percentage = float(str(statement_discount.get('discount', '0')).replace('%', ''))
                customer.is_statement_discount = is_statement_discounted
                if is_statement_discounted and statement_discount_percentage:
                    customer.statement_discount = statement_discount_percentage
            customer.save()


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0022_auto_20220217_1325'),
    ]

    operations = [
        migrations.RunPython(refactoring_customer_discount_terms, migrations.RunPython.noop)
    ]
