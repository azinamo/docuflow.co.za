from enumfields import IntEnum


class PaymentTerm(IntEnum):
    INVOICE_DATE = 1
    STATEMENT_DATE = 2
    CASH_CUSTOMER = 3

    class Labels:
        INVOICE_DATE = 'From Invoice Date'
        STATEMENT_DATE = 'From Statement Date'
        CASH_CUSTOMER = 'Cash Customer'


class DocumentLayout(IntEnum):
    DEFAULT = 0


class AddressType(IntEnum):
    DELIVERY = 0
    POSTAL = 2


class LedgerStatus(IntEnum):
    PENDING = 0
    COMPLETE = 1
