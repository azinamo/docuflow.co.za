from rest_framework import serializers

from django.urls import reverse_lazy
from django_countries.fields import Country

from docuflow.apps.customer.enums import AddressType
from docuflow.apps.customer.models import Customer, Address


class AddressSerializer(serializers.ModelSerializer):
    is_delivery = serializers.SerializerMethodField()
    country_name = serializers.SerializerMethodField()

    class Meta:
        model = Address
        fields = ('id', 'address_type', 'address_line_1', 'address_line_2', 'address_line_3', 'city', 'province',
                  'postal_code', 'country', 'is_delivery', 'country_name')

    # noinspection PyMethodMayBeStatic
    def get_is_delivery(self, instance):
        return instance.address_type == AddressType.DELIVERY

    # noinspection PyMethodMayBeStatic
    def get_country_name(self, instance):
        return Country(code=instance.country).name


class CustomerSummarySerializer(serializers.ModelSerializer):
    detail_url = serializers.SerializerMethodField()

    class Meta:
        model = Customer
        fields = ('id', 'name', 'number', 'vat_number', 'email', 'phone_number', 'contact_person', 'credit_limit',
                  'df_number', 'detail_url')

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse_lazy('edit_customer', args=(instance.pk, ))


class CustomerSerializer(serializers.ModelSerializer):
    addresses = AddressSerializer(many=True)
    pricings = serializers.StringRelatedField(many=True)
    available_amount = serializers.SerializerMethodField()
    term_of_payment = serializers.SerializerMethodField()
    detail_url = serializers.SerializerMethodField()

    class Meta:
        model = Customer
        fields = ('id', 'name', 'number', 'vat_number', 'email', 'phone_number', 'contact_person', 'credit_limit',
                  'available_amount', 'available_credit', 'payment_term', 'days', 'discount_days', 'delivery_term',
                  'delivery_method', 'salesman', 'addresses', 'pricings', 'is_default', 'df_number', 'term_of_payment',
                  'detail_url', 'is_statement_discount', 'statement_discount', 'is_line_discount', 'line_discount',
                  'is_invoice_discount', 'invoice_discount', 'is_active')
        datatables_always_serialize = ('id', 'detail_url', 'is_active')

    # noinspection PyMethodMayBeStatic
    def get_term_of_payment(self, instance):
        return instance.payment_term.label if instance.payment_term else ''

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse_lazy('edit_customer', args=(instance.pk, ))

    # noinspection PyMethodMayBeStatic
    def get_available_amount(self, instance):
        amount = getattr(instance, 'available_amount', None)
        if amount:
            return amount
        return None

