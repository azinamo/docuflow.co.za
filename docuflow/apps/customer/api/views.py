from rest_framework import viewsets

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.customer.models import Customer
from .serializers import CustomerSerializer


class CustomerViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = CustomerSerializer

    def get_queryset(self):
        return Customer.objects.with_balance().select_related(
            'company', 'salesman', 'supplier', 'delivery_method', 'delivery_term', 'default_account'
        ).prefetch_related(
            'pricings', 'addresses'
        ).filter(
            company_id=self.get_company().id, is_default=False
        )
