from django.contrib import admin

from docuflow.apps.common.admin import YearFilter
from . import models


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('number', 'name', 'is_active', 'vat_number', 'company', 'email')
    list_filter = ('company', 'is_active')
    ordering = ('number', )
    search_fields = ('number', 'name')
    list_select_related = ('company', )

    def has_add_permission(self, request):
        return False


@admin.register(models.AgeAnalysis)
class CustomerAgeAnalysisAdmin(admin.ModelAdmin):
    list_display = ('customer', 'period', 'unallocated', 'one_twenty_day', 'ninety_day', 'sixty_day', 'thirty_day',
                    'current', 'balance')
    readonly_fields = ('customer', 'period')
    fields = ('customer', 'period', 'unallocated', 'one_twenty_day', 'ninety_day', 'sixty_day', 'thirty_day',
              'current', 'balance')
    list_filter = (YearFilter, 'customer__name', )
    ordering = ('period', '-balance')
    search_fields = ('customer__name', 'number')
    list_select_related = ('period', 'customer', 'customer__company')
    date_hierarchy = 'period__from_date'

    def has_add_permission(self, request):
        return False


@admin.register(models.Ledger)
class CustomerLedgerAdmin(admin.ModelAdmin):
    list_display = ('customer', 'period', 'date', 'quantity', 'amount', 'balance', 'module', 'text',
                    'journal_number', 'status')
    fieldsets = [('Basic Information', {'fields': ['quantity', 'amount', 'balance', 'text', 'journal_number',
                                                   'description', 'status']})
                 ]
    list_filter = ('customer__name', )
    ordering = ('period', '-balance')
    search_fields = ('customer__name', 'number')
    list_select_related = ('period', 'customer', 'customer__company')
    date_hierarchy = 'date'

    def has_add_permission(self, request):
        return False
