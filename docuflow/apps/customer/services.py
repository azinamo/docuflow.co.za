import logging
from decimal import Decimal

from django.db.models import Sum

from docuflow.apps.common.enums import DayAge
from docuflow.apps.company.models import Company
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.journals.services import get_total_for_account
from docuflow.apps.sales.models import Invoice, InvoiceReceipt, Receipt, SundryReceiptAllocation
from docuflow.apps.period.models import Period, Year
from .models import Customer, Ledger, AgeAnalysis

logger = logging.getLogger(__name__)


class CustomerCreditCalculator(object):

    def __init__(self, customer):
        self.customer = customer

    def ledger_total(self):
        updated_invoices = Ledger.objects.filter(customer=self.customer)
        total = sum([ledger.value * ledger.quantity for ledger in updated_invoices if ledger.value and ledger.quantity])
        return total

    def not_updated_total(self):
        total = Invoice.objects.aggregate(total=Sum('net_price')).filter(customer=self.customer)
        return total

    def execute(self):
        ledger_total = self.ledger_total()
        not_updated_total = self.not_updated_total()
        available = self.customer.credit_limit - ledger_total - not_updated_total

        self.customer.available_credit = available
        self.customer.save()


def age_customer_invoice(invoice: Invoice):

    if invoice.is_credit and invoice.linked_invoice:
        amount = invoice.total_amount
        invoice = invoice.linked_invoice
        age_analysis = add_to_current_age(customer=invoice.customer, period=invoice.period, amount=amount)
    else:
        amount = invoice.total_amount
        age_analysis = add_to_current_age(customer=invoice.customer, period=invoice.period, amount=invoice.total_amount)
    age_for_next_periods(age_analysis=age_analysis, period=age_analysis.period, amount=amount)


def age_past_invoice(invoice_receipt: InvoiceReceipt, year: Year, amount: Decimal, start=0):
    periods = Period.objects.filter(period_year=year, period__gte=invoice_receipt.receipt.period.period).order_by('period')
    ageing_invoice_receipt_for_periods(periods=periods, start=start, customer=invoice_receipt.receipt.customer,
                                       amount=amount)


def age_invoice_receipt(invoice_receipt: InvoiceReceipt, is_reverse=False):
    customer = invoice_receipt.receipt.customer
    period = invoice_receipt.receipt.period
    amount = (invoice_receipt.amount + invoice_receipt.discount) * -1
    if is_reverse:
        amount = amount * -1
    amount = Decimal(amount)

    invoice_period = invoice_receipt.invoice.period
    receipt_year = invoice_receipt.receipt.period.period_year
    invoice_year = invoice_period.period_year
    if receipt_year == invoice_year:
        if invoice_period.period < period.period:
            start = period.period - invoice_period.period
            logger.info(f"Invoice period {invoice_period.period} is less than receipt period {period.period}")
            age_past_invoice(invoice_receipt=invoice_receipt, year=receipt_year, amount=amount, start=start)
            age_upcoming_years(invoice_receipt=invoice_receipt, year=receipt_year, amount=amount,
                               invoice_year=invoice_year, invoice_period=invoice_period)
        else:
            age_analysis = add_to_current_age(customer=customer, period=period, amount=amount)

            age_for_next_periods(age_analysis=age_analysis, period=age_analysis.period, amount=amount)
    elif invoice_year.year < receipt_year.year:
        start = (invoice_year.number_of_periods - invoice_period.period) + 1
        age_past_invoice(invoice_receipt=invoice_receipt, year=receipt_year, amount=amount, start=start)

        age_upcoming_years(invoice_receipt=invoice_receipt, year=receipt_year, amount=amount,
                           invoice_year=invoice_year, invoice_period=invoice_period)


def age_upcoming_years(invoice_receipt: InvoiceReceipt, year: Year, invoice_year: Year, invoice_period: Period,
                       amount: Decimal):
    years = Year.objects.agable().filter(year__gt=year.year, company=year.company).order_by('-year')
    for c, year in enumerate(years):
        start = 3
        if c == 0:
            start = invoice_year.number_of_periods - invoice_period.period
        periods = Period.objects.filter(period_year=year).order_by('period')
        ageing_invoice_receipt_for_periods(periods=periods, start=start, customer=invoice_receipt.receipt.customer,
                                           amount=amount)


def ageing_invoice_receipt_for_periods(periods, start, amount, customer):

    for counter, period in enumerate(periods, start=1):
        if start == 0:
            # Age in current period
            age_for_period(period=period, amount=amount, customer=customer, period_days=DayAge.CURRENT)
        elif start == 1:
            # Age in 30
            age_for_period(period=period, amount=amount, customer=customer, period_days=DayAge.THIRTY_DAY)
        elif start == 2:
            # Age in 60
            age_for_period(period=period, amount=amount, customer=customer, period_days=DayAge.SIXTY_DAY)
        elif start == 3:
            # Age in 90
            age_for_period(period=period, amount=amount, customer=customer, period_days=DayAge.NINETY_DAY)
        elif start >= 4:
            # Age in 120
            age_for_period(period=period, amount=amount, customer=customer, period_days=DayAge.ONE_TWENTY_DAY)
        start += 1


def age_receipt(receipt: Receipt):
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        customer=receipt.customer,
        period=receipt.period
    )
    amount = receipt.balance

    age_unallocated(age_analysis, receipt.customer, receipt.period, amount)


def age_receipt_allocation(sundry_receipt: SundryReceiptAllocation):
    customer = sundry_receipt.receipt.customer
    period = sundry_receipt.parent.period
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        customer=customer,
        period=period
    )
    amount = sundry_receipt.amount

    age_unallocated(age_analysis, customer, period, amount)


def age_unallocated(age_analysis: AgeAnalysis, customer: Customer, period: Period, amount):
    age_analysis.unallocated += amount if age_analysis.unallocated else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    periods = Period.objects.filter(period__gt=period.period, period_year=period.period_year).order_by('period')
    for counter, period in enumerate(periods):
        period_age_analysis, is_new = AgeAnalysis.objects.get_or_create(
            customer=customer,
            period=period
        )
        period_age_analysis.unallocated += amount if age_analysis.unallocated else amount
        period_age_analysis.balance += amount if age_analysis.balance else amount
        period_age_analysis.save()


def age_customer_journal(journal_line: JournalLine, is_reverse=False):

    age_analysis = add_to_current_age(
        customer=journal_line.content_object,
        period=journal_line.journal.period,
        amount=journal_line.amount
    )

    age_for_next_periods(age_analysis=age_analysis, period=age_analysis.period, amount=journal_line.amount)


def age_for_next_periods(age_analysis: AgeAnalysis, period: Period, amount):
    periods = Period.objects.filter(period__gt=period.period, period_year=period.period_year).order_by('period')
    period_count = periods.count()

    age_over_periods(age_analysis=age_analysis, periods=periods, amount=amount)

    age_for_next_years(age_analysis=age_analysis, previous_year=period.period_year, amount=amount, period_count=period_count)


def age_for_next_years(age_analysis, previous_year, amount, period_count):
    for counter, year in enumerate(Year.objects.agable().filter(year__gt=previous_year.year, company=previous_year.company).order_by('-year')):
        periods = Period.objects.filter(period_year=year).order_by('period')
        if counter == 0:
            age_over_periods(age_analysis=age_analysis, periods=periods, amount=amount, start=period_count)
        else:
            age_over_periods(age_analysis=age_analysis, periods=periods, amount=amount, start=0, is_last=True)


def age_over_periods(age_analysis, periods, amount, start=0, is_last=False):
    for counter, _period in enumerate(periods, start=start):
        period_age_analysis, is_new = AgeAnalysis.objects.get_or_create(
            customer=age_analysis.customer,
            period=_period
        )
        if is_last:
            period_age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
            period_age_analysis.balance += amount if age_analysis.balance else amount
        else:
            if counter == 0:
                period_age_analysis.thirty_day += amount if age_analysis.thirty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter == 1:
                period_age_analysis.sixty_day += amount if age_analysis.sixty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter == 2:
                period_age_analysis.ninety_day += amount if age_analysis.ninety_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
            if counter >= 3:
                period_age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
                period_age_analysis.balance += amount if age_analysis.balance else amount
        period_age_analysis.save()


def age_customer_from_period(age_analysis: AgeAnalysis, customer: Customer, period: Period):
    next_periods = Period.objects.filter(period__gt=period.period, period_year=period.period_year).order_by('period')
    for counter, next_period in enumerate(next_periods):
        period_age_analysis, is_new = AgeAnalysis.objects.get_or_create(
            customer=customer,
            period=next_period
        )
        if counter == 0:
            period_age_analysis.thirty_day += age_analysis.current or Decimal(0)
            period_age_analysis.sixty_day += age_analysis.thirty_day or Decimal(0)
            period_age_analysis.ninety_day += age_analysis.sixty_day or Decimal(0)
            period_age_analysis.one_twenty_day += (age_analysis.ninety_day or Decimal(0)) + (
                        age_analysis.one_twenty_day or Decimal(0))
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        if counter == 1:
            period_age_analysis.sixty_day += (age_analysis.thirty_day or Decimal(0)) + (
                        age_analysis.current or Decimal(0))
            period_age_analysis.ninety_day += age_analysis.sixty_day or Decimal(0)
            period_age_analysis.one_twenty_day += age_analysis.ninety_day or Decimal(0)
            period_age_analysis.one_twenty_day += age_analysis.one_twenty_day or Decimal(0)
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        if counter == 2:
            period_age_analysis.ninety_day += (age_analysis.sixty_day or Decimal(0)) + (
                        age_analysis.thirty_day or Decimal(0)) + (age_analysis.current or Decimal(0))
            period_age_analysis.one_twenty_day += (age_analysis.ninety_day or Decimal(0)) + (
                        age_analysis.one_twenty_day or Decimal(0))
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        if counter >= 3:
            period_age_analysis.one_twenty_day += (age_analysis.one_twenty_day or Decimal(0))
            period_age_analysis.balance += age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.one_twenty_day
        period_age_analysis.unallocated += age_analysis.unallocated
        period_age_analysis.balance += age_analysis.unallocated
        period_age_analysis.save()


def add_to_current_age(customer: Customer, period: Period, amount):
    logger.info(f"Period --> {period.id} and Customer --> {customer.id} \r\n\n")
    age_analysis, is_new = AgeAnalysis.objects.get_or_create(
        customer=customer,
        period=period
    )
    age_analysis.current += amount if age_analysis.current else amount
    age_analysis.balance += amount if age_analysis.balance else amount
    age_analysis.save()

    return age_analysis


def age_for_period(customer: Customer, period: Period, amount: Decimal, period_days: DayAge):
    age_analysis, _ = AgeAnalysis.objects.get_or_create(
        customer=customer,
        period=period
    )
    if period_days == DayAge.CURRENT:  # Current
        age_analysis.current += amount if age_analysis.current else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.THIRTY_DAY:
        age_analysis.thirty_day += amount if age_analysis.thirty_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.SIXTY_DAY:
        age_analysis.sixty_day += amount if age_analysis.sixty_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.NINETY_DAY:
        age_analysis.ninety_day += amount if age_analysis.ninety_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    elif period_days == DayAge.ONE_TWENTY_DAY:
        age_analysis.one_twenty_day += amount if age_analysis.one_twenty_day else amount
        age_analysis.balance += amount if age_analysis.balance else amount
        age_analysis.save()
    return age_analysis


def age_linked_credit_note(credit_note: Invoice):
    invoice = credit_note.linked_invoice

    age_customer_invoice(invoice=invoice)


def is_customer_balancing(company: Company, year: Year, period: Period = None, end_date=None) -> bool:
    end_date = end_date or year.end_date
    if period and period.to_date:
        end_date = period.to_date
    account_total = get_total_for_account(
        year=year,
        company=company,
        account=company.default_customer_account,
        start_date=year.start_date,
        end_date=end_date
    )
    account_total = abs(account_total) if account_total else 0

    period = period or year.last_period_open
    age_analysis = AgeAnalysis.objects.filter(period=period).aggregate(total=Sum('balance'))
    age_total = abs(age_analysis['total']) if age_analysis and age_analysis['total'] else 0

    logger.info(f"Variation found CUSTOMER AGE TOTAL = {age_total} vs BALANCE SHEET TOTAL= {account_total}. "
                f"DIFF = {age_total - account_total}")

    return age_total == account_total
