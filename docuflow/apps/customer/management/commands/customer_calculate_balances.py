from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django.conf import settings
from docuflow.apps.customer.models import Customer
from docuflow.apps.company.models import Company
from docuflow.apps.sales.models import Invoice


class Command(BaseCommand):
    help = "Dump some company data"

    def handle(self, *args, **kwargs):
        companies = Company.objects.all()
        for company in companies:
            try:
                customers = Customer.objects.filter(company=company, is_default=False, pk=354).all()
                for customer in customers:
                    print("Looking for invoice for {}".format(customer))
                    invoices = Invoice.objects.filter(customer=customer)

                    print("Found {}".format(invoices.count()))
                    invoices_total = 0
                    if invoices:
                        invoices_total = sum([invoice.invoice_total for invoice in invoices if invoice.total_amount])

                    credit_limit = 0
                    if customer.credit_limit:
                        credit_limit = float(customer.credit_limit)
                    available = credit_limit - float(invoices_total)
                    customer.available_credit = available
                    customer.save()

                    print("Customer {} has and not updated total = {} and credit limit {}, available is {}({})".format(
                        customer.name, invoices_total, customer.credit_limit, customer.available_credit, available
                    ))

                    #calculator = CustomerCreditCalculator(customer)
                    #calculator.execute()
            except Exception as exception:
                print("Exception occurred {}".format(exception.__str__()))
