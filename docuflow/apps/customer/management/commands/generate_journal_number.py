from django.core.management.base import BaseCommand
from docuflow.apps.customer.models import Ledger
from docuflow.apps.company.models import Company


class Command(BaseCommand):
    help = "Dump some company data"

    def handle(self, *args, **kwargs):
        companies = Company.objects.all()
        for company in companies:
            for ledger in Ledger.objects.filter(customer__company=company, period__period_year__year=2022):
                if ledger and ledger.content_type:
                    content_object = ledger.content_object
                    if ledger.content_type.model == 'invoice':
                        journal_number = ledger.journal_number
                        if content_object.po_number:
                            journal_number = content_object.po_number
                        elif content_object.estimate:
                            journal_number = str(content_object.estimate)
                        elif content_object.delivery:
                            journal_number = str(content_object.delivery)
                        elif content_object.picking_slip:
                            journal_number = str(content_object.picking_slip)
                        elif content_object.estimate_number:
                            journal_number = str(content_object.estimate_number)
                        elif not isinstance(ledger.journal_number, int):
                            if ledger.content_type.model == 'invoice' and content_object.po_number:
                                journal_number = content_object.reference
                        ledger.journal_number = journal_number
                        content_object.invoice_number = journal_number
                        content_object.save()
                    elif ledger.content_type.model == 'receipt' and content_object.reference:
                        ledger.journal_number = content_object.reference
                    elif ledger.content_type.model == 'journalline' and content_object.text:
                        ledger.journal_number = content_object.text
                    ledger.save()

