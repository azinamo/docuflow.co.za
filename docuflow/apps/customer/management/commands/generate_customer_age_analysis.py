import logging
import pprint

from django.core.management import BaseCommand
from django.db import transaction
from django.db.models import Q

from docuflow.apps.customer.models import AgeAnalysis

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "generate age analysis"

    def add_arguments(self, parser):
        parser.add_argument('-c', '--customer', type=str, help='Indicates the customer to be calculated')

    def handle(self, *args, **options):
        with transaction.atomic():
            for age_analysis in AgeAnalysis.objects.filter(
                period__period_year__year=2022
            ).filter(customer__company__slug='41054').filter(
                Q(current__lt=0) | Q(thirty_day__lt=0) | Q(sixty_day__lt=0) | Q(ninety_day__lt=0) | Q(one_twenty_day__lt=0)
            ).order_by('id').distinct():
                total = age_analysis.unallocated + age_analysis.current + age_analysis.thirty_day + age_analysis.sixty_day + age_analysis.ninety_day + age_analysis.ninety_day
                print(f"Company {age_analysis.customer.company.slug}"
                      f" Found {age_analysis.id} for year {age_analysis.period.period_year} on "
                      f"customer {age_analysis.customer.name}({age_analysis.customer_id}) "
                      f" -> has some incorrect periods "
                      f" Current = {age_analysis.current}, 30 Days = {age_analysis.thirty_day}, "
                      f" 60 Days = {age_analysis.sixty_day}, 90 Days = {age_analysis.ninety_day}, "
                      f"120 Days = {age_analysis.one_twenty_day}; "
                      f"Balance -> {total}")
        self.stdout.write(self.style.SUCCESS("Done generating the age analysis"))
