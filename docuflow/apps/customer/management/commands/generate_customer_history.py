from django.core.management import BaseCommand
from django.db import transaction

from docuflow.apps.customer.models import History, Customer
from docuflow.apps.sales.models import Invoice
from docuflow.apps.period.models import Period


class Command(BaseCommand):
    help = "Generate year history"

    def handle(self, *args, **options):
        with transaction.atomic():
            History.objects.update(
                net_amount=0,
                vat_amount=0,
                total_amount=0
            )

            for period in Period.objects.filter(period_year__year__in=[2020, 2019]):
                for customer in Customer.objects.filter(company=period.period_year.company):
                    history, is_new = History.objects.get_or_create(
                        period=period,
                        customer=customer,
                        defaults={'total_amount': 0, 'vat_amount': 0, 'net_amount': 0}
                    )
                    for invoice in Invoice.objects.filter(period=period, customer=customer):
                        history.net_amount += invoice.net_amount
                        history.vat_amount += invoice.vat_amount
                        history.total_amount += invoice.total_amount
                        history.save()
                    self.stdout.write(self.style.SUCCESS(f"Created history for period {period} -> {customer}"))


