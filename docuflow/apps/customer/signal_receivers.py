from django.dispatch import receiver
from django.db.models.signals import post_save

from docuflow.apps.customer.models import Ledger, AgeAnalysis, OpeningBalance
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.period.models import Year


@receiver(post_save, sender=Ledger, dispatch_uid='customer_ledger')
def handle_customer_ledger(sender, instance, created, **kwargs):
    if instance.status == LedgerStatus.COMPLETE:
        year = instance.period.period_year

        if year.is_closing:
            last_period = year.last_period
            # Copy closing year Closing balance for customer to started year Opening balances for customer
            period_age, _ = AgeAnalysis.objects.get_or_create(period=last_period, customer=instance.customer)
            started_year = Year.objects.started().filter(company=year.company).first()
            opening_balance, _ = OpeningBalance.objects.get_or_create(
                customer=period_age.customer,
                year=started_year
            )
            opening_balance.amount = period_age.balance
            opening_balance.save()
