from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.CustomersView.as_view(), name='customer_index'),
    path('create/', views.CreateCustomerView.as_view(), name='create_customer'),
    path('add/', views.AddCustomerView.as_view(), name='add_customer'),
    path('<int:pk>/edit/', views.EditCustomerView.as_view(), name='edit_customer'),
    path('<int:pk>/delete/', views.DeleteCustomerView.as_view(), name='delete_customer'),
    path('<int:pk>/details/', views.CustomerDetailsView.as_view(), name='customer_details'),
    path('update/vat/number/', csrf_exempt(views.UpdateCustomerVatView.as_view()), name='update_customer_vat_number'),
    path('list/', csrf_exempt(views.CustomersListView.as_view()), name='customers_list'),
    path('search/', views.SearchCustomersView.as_view(), name='search_customers_list'),
    path('autocomplete/search/', views.AutocompleteSearchCustomersView.as_view(), name='autocomplete_search_customers'),
    path('age/analysis/', views.CustomerAgeAnalysisView.as_view(), name='customer_age_balance'),
    path('ageanalysis/', views.AgeAnalysisView.as_view(), name='customer_age_analysis'),
    path('generate/ageanalysis/', views.GenerateAgeAnalysisView.as_view(), name='generate_customer_age_analysis'),
    path('download/ageanalysis/', views.DownloadAgeAnalysisView.as_view(), name='download_customer_age_analysis'),
    path('ageanalysis/<int:pk>/detail/', views.AgeAnalysisDetailView.as_view(), name='age_analysis_detail'),
    path('opening/balances/', views.OpeningBalanceView.as_view(), name='customer_opening_balances'),
    path('update/<int:customer_id>/opening/balances/', csrf_exempt(views.UpdateOpeningBalanceView.as_view()),
         name='update_year_customer_balance'),
    path('copy/<int:year_id>/previous_year/opening/balances/', views.UpdateOpeningBalanceView.as_view(), 
         name='copy_customer_previous_year_balances'),
    path('action/<str:action>/', views.MultiCustomerActionView.as_view(), name='multiple_customer_action'),
    path('import/', views.ImportView.as_view(), name='import_customers'),
    path('generate/pdfs', csrf_exempt(views.GeneratePdfsView.as_view()), name='generate_pdfs'),
    path('statements/', views.StatementsView.as_view(), name='customer_statements'),
    path('generate/statements/', csrf_exempt(views.GenerateStatementsView.as_view()), name='generate_customer_statements'),
    path('statement/<int:customer_id>/', views.StatementView.as_view(), name='generate_customer_statement'),
    path('pdf/statement/<int:customer_id>/', csrf_exempt(views.StatementPdfView.as_view()), name='statement_pdf'),
    path('email/statement/<int:customer_id>/', csrf_exempt(views.EmailStatementView.as_view()), name='email_statement'),
    path('print/statement/<int:customer_id>/', csrf_exempt(views.PrintStatementView.as_view()), name='print_statement'),
    path('show/statements/<str:view_type>/', csrf_exempt(views.ShowStatementsView.as_view()), name='show_statements'),
    path('email/statements/', csrf_exempt(views.EmailStatementsView.as_view()), name='email_customer_statements'),
    path('statement/<str:signature>/', csrf_exempt(views.ViewStatementsView.as_view()), name='view_statement'),
    path('screen/statements/', csrf_exempt(views.ScreenStatementsView.as_view()), name='screen_customer_statements'),
    path('<int:pk>/detail/', views.CustomerDetailView.as_view(), name='customer_detail'),
    path('balance-sheet/vs/age-analysis', views.BalanceSheetVsAgeAnalysisView.as_view(),
         name='balance_sheet_vs_age_analysis'),
]

urlpatterns += [
    path('history/', include('docuflow.apps.customer.modules.customerhistory.urls')),
    path('deliveryterm/', include('docuflow.apps.customer.modules.deliveryterm.urls')),
    path('ledger/', include('docuflow.apps.customer.modules.customerledger.urls')),
    path('pricing/', include('docuflow.apps.customer.modules.pricing.urls')),
    path('fixedpricing/', include('docuflow.apps.customer.modules.fixedpricing.urls')),
    path('address/', include('docuflow.apps.customer.modules.address.urls')),
]
