import logging
import pprint
import json
import os
import ssl
import xlwt
from decimal import Decimal
from datetime import datetime
from dateutil import parser
from django.db.models import Sum
from openpyxl import load_workbook
from zipfile import ZipFile

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.conf import settings
from django.core.validators import ValidationError
from django.core import signing
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.urls import reverse, reverse_lazy
from django.utils.timezone import now
from django.template.loader import render_to_string
from django.views.generic import TemplateView, View, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django_countries import countries
from django_weasyprint import WeasyTemplateResponseMixin
from weasyprint import HTML

from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.common.utils import datatable_pages_menu, is_ajax, is_ajax_post
from docuflow.apps.common.enums import SaProvince, Module
from docuflow.apps.company.models import Company, DeliveryMethod, Account
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.sales.models import Salesman, Invoice, Receipt
from docuflow.apps.period.models import Period, Year
from .api.serializers import CustomerSerializer
from .models import Customer, Address, Pricing, DeliveryTerm, Ledger, OpeningBalance, AgeAnalysis
from .forms import CustomerForm, ImportForm, StatementForm, CustomerStatementForm, AgeAnalysisForm
from .emails import StatementEmail
from .enums import AddressType, PaymentTerm, DocumentLayout
from ..journals.services import get_total_for_account

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class CustomerListJson(LoginRequiredMixin, View):
    model = Customer
    columns = ['id', 'code', 'name']
    order_columns = ['id', 'code', 'name']
    max_display_length = 30

    def render_column(self, row, column):
        # We want to render as a custom column
        return super(CustomerListJson, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(name__startswith=search)
        return qs


class CustomersView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'customer/index.html'


class CreateCustomerView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = Customer
    template_name = 'customer/create.html'
    success_message = 'Customer successfully saved'
    form_class = CustomerForm

    def get_context_data(self, **kwargs):
        context = super(CreateCustomerView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['document_types'] = InvoiceType.objects.filter(company=company, file__module=Module.SALES)
        return context

    def get_success_url(self):
        if self.request.GET.get('redirect'):
            return self.request.GET.get('redirect')
        else:
            return reverse('customer_index')

    def get_form_kwargs(self):
        kwargs = super(CreateCustomerView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the customer, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True,
                                 }, status=400)
        return super(CreateCustomerView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save(commit=False)

            return JsonResponse({
                'error': False,
                'text': 'Customer saved successfully',
                'redirect': self.get_success_url()
            })
        return super(CreateCustomerView, self).form_valid(form)


class AddCustomerView(CreateCustomerView):
    template_name = 'customer/add.html'

    def form_valid(self, form):
        if is_ajax(request=self.request):
            with transaction.atomic():
                form.save(commit=False)

            return JsonResponse({
                'error': False,
                'text': 'Customer saved successfully',
                'reload': True
            })


class EditCustomerView(LoginRequiredMixin,  ProfileMixin, SuccessMessageMixin, UpdateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'customer/edit.html'
    success_message = 'Customer successfully updated'
    success_url = reverse_lazy('customer_index')

    def get_queryset(self):
        return Customer.objects.with_balance().select_related(
            'salesman', 'supplier', 'delivery_method', 'delivery_term', 'default_account'
        ).prefetch_related(
            'addresses', 'pricings'
        )

    def get_initial(self):
        initial = super().get_initial()
        customer_address = Address.objects.filter(customer=self.get_object())
        for customer_address in customer_address:
            if customer_address.address_type == AddressType.POSTAL:
                initial['postal_province'] = customer_address.province
                initial['postal_country'] = customer_address.country
            if customer_address.address_type == AddressType.DELIVERY:
                initial['delivery_province'] = customer_address.province
                initial['delivery_country'] = customer_address.country
        return initial

    def get_context_data(self, **kwargs):
        context = super(EditCustomerView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['pricings'] = self.get_object().pricings.all
        context['delivery_addresses'] = self.get_object().addresses.filter(address_type=AddressType.DELIVERY)
        context['document_types'] = InvoiceType.objects.filter(company=company, file__module=Module.SALES)
        return context

    def get_form_kwargs(self):
        kwargs = super(EditCustomerView, self).get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the customer, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(EditCustomerView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                customer = form.save(commit=False)

            return JsonResponse({
                'error': False,
                'text': 'Customer saved successfully',
                'redirect': reverse('edit_customer', args=(customer.pk, ))
            })
        return super(EditCustomerView, self).form_valid(form)


class DeleteCustomerView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Customer
    template_name = 'customer/confirm_delete.html'
    success_message = 'Customer successfully deleted'
    success_url = reverse_lazy('customer_index')


class CustomerDetailsView(LoginRequiredMixin, View):

    def get_address(self):
        return Address.objects.filter(customer_id=self.kwargs['pk'])

    def get_pricing(self):
        return Pricing.objects.filter(customer_id=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        customer = Customer.objects.prefetch_related('periods').with_balance().filter(pk=self.kwargs['pk']).first()
        delivery_addresses = self.get_address().filter(address_type=AddressType.DELIVERY)
        postal = self.get_address().filter(address_type=AddressType.POSTAL).first()
        customer['delivery'] = {}
        customer['postal'] = {}
        if delivery_addresses.count() > 0:
            delivery_address = delivery_addresses.first()
            customer['delivery']['address'] = delivery_address.address_line_1
            customer['delivery']['city'] = delivery_address.city
            customer['delivery']['country'] = delivery_address.country.name
            customer['delivery']['province'] = delivery_address.province_name
            customer['delivery']['postal_code'] = delivery_address.postal_code

        if postal:
            customer['postal']['address'] = postal.address_line_1
            customer['postal']['city'] = postal.city
            customer['postal']['country'] = postal.country.name
            customer['postal']['province'] = postal.province_name
            customer['postal']['postal_code'] = postal.postal_code

        if 'module' in request.GET and request.GET['module'] == 'sales':
            customer['customer_form'] = render_to_string('sales/customer.html', {'customer': customer,
                                                                                 'countries': dict(countries),
                                                                                 'provinces': SaProvince.choices()
                                                                                 })
            customer['delivery_form'] = render_to_string('sales/shipping_to.html', {'customer': customer,
                                                                                    'countries': dict(countries),
                                                                                    'provinces': SaProvince.choices()
                                                                                    })
        pricings = self.get_pricing()
        if pricings:
            customer['pricings'] = []
            for pricing in pricings:
                customer['pricings'].append({'id': pricing.id, 'name': pricing.name})
        return JsonResponse(customer)


class UpdateCustomerVatView(LoginRequiredMixin, View):

    def get_customer(self):
        return Customer.objects.filter(id=self.request.POST.get('customer_id')).first()

    def post(self, request, *args, **kwargs):
        try:
            customer = self.get_customer()
            if customer:
                customer.vat_number = self.request.POST.get('vat_number')
                customer.save()
            return JsonResponse({'text': 'Vat number updated', 'error': False})
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({'text': 'Unexpected error occurred updating vat number',
                                 'error': True,
                                 'detail': str(exception)
                                 })


class CustomersListView(LoginRequiredMixin, ProfileMixin, View):

    def get_customers(self):
        return Customer.objects.select_related(
            'company'
        ).filter(company=self.get_company()).order_by('name').values()

    def get(self, **kwargs):
        customers = self.get_customers()
        return JsonResponse(customers)


class AgeAnalysisView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'customer/ageanalysis/index.html'
    form_class = AgeAnalysisForm

    def get_initial(self):
        initial = super().get_initial()
        initial['period'] = Period.objects.open(company=self.get_company(), year=self.get_year()).first()
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AgeAnalysisView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['year'] = self.get_year()
        return context


class GenerateAgeAnalysisView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'customer/ageanalysis/report.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateAgeAnalysisView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['year'] = self.get_year()
        context['report_date'] = now().date()
        form_class = AgeAnalysisForm(data=self.request.GET, company=company, year=self.get_year())
        if form_class.is_valid():
            age_analysis, totals = form_class.execute()
            context['age_analysis'] = age_analysis
            context['period'] = form_class.cleaned_data['period']
            context['totals'] = totals
        else:
            logger.info(form_class.errors)
            context['error'] = form_class.errors
        return context


class DownloadAgeAnalysisView(LoginRequiredMixin, ProfileMixin, View):
    template_name = 'customer/ageanalysis/report.html'

    def get(self, request, *args, **kwargs):
        company = self.get_company()

        form_class = AgeAnalysisForm(data=self.request.GET, company=company, year=self.get_year())
        if form_class.is_valid():
            age_analysis, totals = form_class.execute()

            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename="customer_age_analysis.xls"'

            wb = xlwt.Workbook(encoding='utf-8')
            ws = wb.add_sheet('Age Analysis')

            # Sheet header, first row
            row_count = 1
            font_style = xlwt.XFStyle()
            font_style.font.bold = True

            row = ws.row(0)
            row.write(0, str(company), font_style)

            row_count += 1
            ws.write(row_count, 0, 'Age Analysis', font_style)

            row_count += 1
            ws.write(row_count, 0, 'Preliminary', font_style)

            row_count += 1
            ws.write(row_count, 0, 'Year', font_style)
            ws.write(row_count, 1, self.get_year().start_date.strftime('%d-%M-%Y'), font_style)
            ws.write(row_count, 2, self.get_year().end_date.strftime('%d-%M-%Y'), font_style)

            row_count += 1
            ws.write(row_count, 0, 'Report date(s)', font_style)
            ws.write(row_count, 1, now().date().strftime('%d-%M-%Y'))

            header_row = ws.row(1)
            header_row.write(0, 'Customer Code', font_style)
            header_row.write(1, 'Customer Name', font_style)
            header_row.write(2, 'Unallocated', font_style)
            header_row.write(3, '120+ Days', font_style)
            header_row.write(5, '90 Days', font_style)
            header_row.write(6, '60 Days', font_style)
            header_row.write(7, '30 Days', font_style)
            header_row.write(8, 'Total', font_style)
            # Sheet body, remaining rows
            row_count += 1
            for customer_age in age_analysis:
                ws.write(row_count, 0, customer_age.customer.number)
                ws.write(row_count, 1, customer_age.customer.name)
                ws.write(row_count, 2, customer_age.unallocated)
                ws.write(row_count, 3, customer_age.one_twenty_day)
                ws.write(row_count, 4, customer_age.ninety_day)
                ws.write(row_count, 5, customer_age.sixty_day)
                ws.write(row_count, 6, customer_age.thirty_day)
                ws.write(row_count, 7, customer_age.current)
                ws.write(row_count, 8, customer_age.balance)
                row_count += 1
            row_count += 1
            ws.write(row_count, 0, 'Totals', font_style)
            ws.write(row_count, 1, '', font_style)
            ws.write(row_count, 2, totals['unallocated_total'], font_style)
            ws.write(row_count, 3, totals['one_twenty_or_more_days'], font_style)
            ws.write(row_count, 4, totals['ninety_days'], font_style)
            ws.write(row_count, 5, totals['sixty_days'], font_style)
            ws.write(row_count, 6, totals['thirty_days'], font_style)
            ws.write(row_count, 7, totals['current'], font_style)
            ws.write(row_count, 8, totals['total'], font_style)
            wb.save(response)
            return response
        else:
            return HttpResponse('Could not generate the balance sheet report')


class CustomerAgeAnalysisView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'customer/age_analysis.html'

    def get_current_period(self, company, date):
        return Period.objects.filter(
            from_date__lte=date, to_date__gte=date, company=company
        ).order_by(
            '-period'
        ).first()

    def get_customer_amount(self, customer, key):
        if key in customer:
            return customer[key] if customer[key] else 0
        return 0

    def get_context_data(self, **kwargs):
        context = super(CustomerAgeAnalysisView, self).get_context_data(**kwargs)
        company = self.get_company()
        date = datetime.now().date()
        current_period = self.get_current_period(company, date)

        customer_age_balance = Customer.objects.get_age_balances(year=self.get_year(), current_period=current_period)
        totals = {
            'receipt_total': 0,
            'one_twenty_or_more_days': 0,
            'ninety_days': 0,
            'sixty_days': 0,
            'thirty_days': 0,
            'current': 0,
            'total': 0
        }
        for customer in customer_age_balance:
            totals['receipt_total'] += self.get_customer_amount(customer, 'receipt_total')
            totals['one_twenty_or_more_days'] += self.get_customer_amount(customer, 'one_twenty_day_invoice')
            totals['ninety_days'] += self.get_customer_amount(customer, 'ninety_day_invoice')
            totals['sixty_days'] += self.get_customer_amount(customer, 'sixty_day_invoice')
            totals['thirty_days'] += self.get_customer_amount(customer, 'thirty_day_invoice')
            totals['current'] += self.get_customer_amount(customer, 'current_invoice_total')
            totals['total'] += self.get_customer_amount(customer, 'balance')
        context['report'] = customer_age_balance
        context['totals'] = totals
        context['company'] = company
        context['year'] = self.get_year()
        return context


class AgeAnalysisDetailView(LoginRequiredMixin, ProfileMixin, DetailView):
    template_name = 'customer/age_analysis_invoices.html'
    model = AgeAnalysis

    def get_customer(self):
        return Customer.objects.filter(id=self.kwargs['pk']).first()

    def get_context_data(self, **kwargs):
        context = super(AgeAnalysisDetailView, self).get_context_data(**kwargs)
        age_analysis = self.get_object()
        company = self.get_company()
        customer = age_analysis.customer
        filter_options = {'customer_id': self.kwargs['pk']} # self.get_filter_options(company)
        # selected_statuses = self.display_statuses(filter_options)
        # query_string = self.make_query_string(selected_statuses, filter_options)
        invoices = Invoice.objects.select_related('invoice_type').not_paid().filter(customer=age_analysis.customer)
        receipts = Receipt.objects.open().filter(period=age_analysis.period, customer=age_analysis.customer)
        receipts_total = sum([receipt.open_amount for receipt in receipts])
        invoices_total = sum([invoice.open_amount for invoice in invoices])
        context['company'] = company
        context['year'] = self.get_year()
        context['customer'] = customer
        context['invoices'] = invoices
        context['receipts'] = receipts
        context['total'] = receipts_total + invoices_total
        if 'date' in self.request.GET:
            parsed_date = parser.parse(self.request.GET['date'])
            context['report_date'] = parsed_date.date()
        else:
            context['report_date'] = datetime.now().date()
        return context


class ImportView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, FormView):
    template_name = 'customer/import.html'
    form_class = ImportForm

    def get_customer(self, company, name):
        return Customer.objects.filter(company=company, number=name).first()

    def get_company_customers(self, company):
        return company.customers.all()

    def deactivate_customer(self, customers):
        for customer in customers:
            customer.is_active = False
            customer.save()

    def get_or_create_delivery_terms(self, company, term):
        delivery_term = DeliveryTerm.objects.filter(name__icontains=term, company=company).first()
        if delivery_term:
            return delivery_term
        else:
            return DeliveryTerm.objects.create(name=term, company=company)

    def get_or_create_delivery_method(self, company, method):
        delivery_method = DeliveryMethod.objects.filter(name__icontains=method, company=company).first()
        if delivery_method:
            return delivery_method
        else:
            return DeliveryMethod.objects.create(name=method, company=company)

    def get_or_create_salesman(self, company, code):
        salesman = Salesman.objects.filter(code__icontains=code, company=company).first()
        if salesman:
            return salesman
        else:
            return Salesman.objects.create(code=code, name=code, company=company)

    def get_default_account(self, company, method):
        account = Account.objects.filter(code__icontains=method, company=company).first()
        if account:
            return account
        else:
            return Account.objects.create(code=method, company=company)

    # noinspection PyMethodMayBeStatic
    def get_by_province_name(self, province_name):
        province = str(province_name).lower()
        province_id = SaProvince.EASTERN_CAPE.value
        if province == 'western cape':
            return SaProvince.WESTERN_CAPE.value
        elif province == 'eastern cape':
            return SaProvince.EASTERN_CAPE.value
        elif province == 'gauteng':
            return SaProvince.GAUTENG.value
        elif province == 'mpumalanga':
            return SaProvince.MPUMALANGA.value
        elif province == 'free state':
            return SaProvince.FREE_STATE.value
        return province_id

    def import_customers(self, company, account_data):
        is_updated = 0
        is_new = 0
        total = 0
        customer_accounts = []
        for acc in account_data['customers']:
            try:
                customer = self.get_customer(company, acc['number'])
                if customer:
                    customer.is_active = True
                    customer.company = company
                    customer.save()
                    is_updated += 1
                elif acc['number'] and acc['name']:
                    postal_address = acc.pop('postal')
                    delivery_address = acc.pop('delivery')
                    acc['company_id'] = company.id
                    customer = Customer.objects.create(**acc)
                    if customer:
                        is_new += 1
                        if len(postal_address) > 0:
                            customer.save_address(postal_address, AddressType.POSTAL)
                        if len(delivery_address) > 0:
                            customer.save_address(delivery_address, AddressType.DELIVERY)

            except Exception as err:
                logger.exception(err)
                messages.error(self.request, err.__str__())
                # we need to log the error as they occurred and or probaby display results of the upload
            total += 1
        return {'new': is_new, 'updated': is_updated, 'total': total}

    def get_imported_data(self, csv_file, company):
        wb = load_workbook(csv_file, data_only=True)
        counter = 0
        data = []
        if 'Sheet1' in wb:
            ws = wb['Sheet1']
            for row in ws.rows:
                if len(row) > 1 and counter > 0:
                    c = 0
                    data_dict = {'number': '', 'is_active': True, 'name': '', 'vat_number': '',
                                 'postal': {'street': '', 'city': '', 'province': '', 'postal_code': '', 'country': ''},
                                 'delivery': {'street': '', 'city': '', 'province': '', 'postal_code': '', 'country': ''},
                                 'phone_number': '', 'cell_number': '', 'accepted_sms_marketing': False,
                                 'email': '', 'accepted_email_marketing': False, 'contact_person': '',
                                 'account_person': '', 'credit_limit': '', 'salesman_id': '',
                                 'payment_term': '', 'days': '', 'is_debit_order_client': False, 'delivery_term_id': '',
                                 'delivery_method_id': '', 'document_layout': '', 'default_account_id': '',
                                 'discount_terms': {
                                     'statement': {'is_discounted': False, 'discount': 0},
                                     'line': {'is_discounted': False, 'discount': 0},
                                     'invoice': {'is_discounted': False, 'discount': 0},
                                 }, }
                    for cell in row:
                        if c == 0:
                            data_dict['number'] = cell.value
                        if c == 1:
                            data_dict['is_active'] = True if cell.value == 'Yes' else False
                        if c == 2:
                            data_dict['name'] = cell.value
                        if c == 3:
                            data_dict['vat_number'] = cell.value
                        if c == 4:
                            data_dict['postal']['street'] = cell.value
                        if c == 5:
                            data_dict['postal']['city'] = cell.value
                        if c == 6:
                            province_id = self.get_by_province_name(cell.value)
                            data_dict['postal']['province'] = province_id
                        if c == 7:
                            data_dict['postal']['postal_code'] = cell.value
                        if c == 8:
                            data_dict['postal']['country'] = 'ZA'
                        if c == 9:
                            data_dict['delivery']['street'] = cell.value
                        if c == 10:
                            data_dict['delivery']['city'] = cell.value
                        if c == 11:
                            province = str(cell.value).lower()
                            data_dict['delivery']['province'] = province
                        if c == 12:
                            data_dict['delivery']['postal_code'] = cell.value
                        if c == 13:
                            data_dict['delivery']['country'] = 'ZA'
                        if c == 14:
                            data_dict['phone_number'] = cell.value
                        if c == 15:
                            data_dict['cell_number'] = cell.value
                        if c == 16:
                            data_dict['accepted_sms_marketing'] = True if cell.value == 'Yes' else False
                        if c == 17:
                            data_dict['email'] = cell.value
                        if c == 18:
                            data_dict['accepted_email_marketing'] = True if cell.value == 'Yes' else False
                        if c == 19:
                            data_dict['contact_person'] = cell.value
                        if c == 20:
                            data_dict['account_person'] = cell.value
                        if c == 21:
                            data_dict['credit_limit'] = cell.value
                        if c == 22:
                            if cell.value:
                                salesman = self.get_or_create_salesman(company, cell.value)
                                data_dict['salesman_id'] = salesman.id if salesman else None
                        if c == 23:
                            terms = PaymentTerm.STATEMENT_DATE
                            if cell.value == 'Cash Customer':
                                terms = PaymentTerm.CASH_CUSTOMER
                            data_dict['payment_term'] = terms
                        if c == 24:
                            days = 0
                            if cell.value:
                                try:
                                    days = int(cell.value)
                                except:
                                    pass
                            data_dict['days'] = days
                        if c == 25:
                            data_dict['is_debit_order_client'] = True if cell.value == 'Yes' else False
                        if c == 26:
                            if cell.value:
                                delivery_term = self.get_or_create_delivery_terms(company, cell.value)
                                data_dict['delivery_term_id'] = delivery_term.id if delivery_term else None
                        if c == 27:
                            if cell.value:
                                delivery_method = self.get_or_create_delivery_method(company, cell.value)
                                data_dict['delivery_method_id'] = delivery_method.id if delivery_method else None
                        if c == 28:
                            data_dict['document_layout'] = DocumentLayout.DEFAULT if cell.value == 'Default' else DocumentLayout.DEFAULT
                        if c == 29:
                            if cell.value:
                                default_account = self.get_default_account(company, cell.value)
                                data_dict['default_account_id'] = default_account.id if default_account else None
                        if c == 30:
                            data_dict['discount_terms']['statement']['is_discounted'] = True if cell.value == 'Yes' else False
                        if c == 31:
                            statement_discount = 0
                            try:
                                if cell.value and cell.value != '':
                                    statement_discount = float(cell.value)
                            except:
                                pass
                            data_dict['discount_terms']['statement']['discount'] = statement_discount
                        if c == 32:
                            data_dict['discount_terms']['line']['is_discounted'] = True if cell.value == 'Yes' else False
                        if c == 33:
                            line_discount = 0
                            try:
                                if cell.value and cell.value != '':
                                    line_discount = float(cell.value)
                            except:
                                pass
                            data_dict['discount_terms']['line']['discount'] = line_discount
                        if c == 34:
                            data_dict['discount_terms']['invoice']['is_discounted'] = True if cell.value == 'Yes' else False
                        if c == 35:
                            invoice_discount = 0
                            try:
                                if cell.value and cell.value != '':
                                    invoice_discount = float(cell.value)
                            except:
                                pass
                            data_dict['discount_terms']['invoice']['discount'] = invoice_discount
                        if c == 36:
                            data_dict['discount_days'] = cell.value if isinstance(cell.value, int) else 0
                        if c == 37:
                            data_dict['is_interest_charged'] = True if cell.value == 'Yes' else False
                        if c == 38:
                            interest = 0
                            try:
                                if cell.value and cell.value != '':
                                    interest = float(cell.value)
                            except:
                                pass
                            data_dict['interest'] = interest
                        if c == 39:

                            data_dict['days_unpaid'] = cell.value if isinstance(cell.value, int) else 0
                        if c == 40:
                            data_dict['df_number'] = cell.value
                        if c == 41:
                            data_dict['is_price_on_delivery'] = True if cell.value == 'Yes' else False
                        c += 1
                    data.append(data_dict)
                counter += 1
        return {'customers': data, 'total': counter}

    def get_csv_import_data(self, csv_file, company):
        pass

    def form_valid(self, form):
        try:
            company = self.get_company()
            csv_file = self.request.FILES["csv_file"]
            if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx')):
                messages.error(self.request, 'File is not .csv, .xlsx or xls type')
                return HttpResponseRedirect(reverse("import_customers"))

            if csv_file.multiple_chunks():
                messages.error(self.request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 1000),))
                return HttpResponseRedirect(reverse("import_customers"))

            customers_data = []
            if csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx'):
                customers_data = self.get_imported_data(csv_file, company)
            # loop over the lines and save them to the database. If there is an error store as string and display
            customers = self.get_company_customers(company)
            if customers:
                self.deactivate_customer(customers)

            import_result = self.import_customers(company, customers_data)

            messages.success(self.request, '{} new customers successfully imported.'.format(import_result['new']))
        except Exception as exception:
            messages.error(self.request, "Unable to upload file {}".format(exception))
        return super(ImportView, self).form_valid(form)

    def get_success_url(self):
        return reverse('customer_index')


class StatementsView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'statement/index.html'
    form_class = StatementForm

    def get_customers(self, company):
        return Customer.objects.filter(company=company)

    def get_periods(self, company, year):
        return Period.objects.filter(company=company, period_year=year)

    def get_form_kwargs(self):
        kwargs = super(StatementsView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(StatementsView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['year'] = year
        return context


class GenerateStatementsView(ProfileMixin, TemplateView):
    template_name = 'statement/statements.html'

    def get_filtered_balances(self, balances, cleaned_data):
        filtered_customers = {}
        for customer, balance_line in balances.items():
            #
            balance = balance_line.get('balance', 0)
            if cleaned_data.get('customer_emails') == 'with':
                if not customer.email or customer.email is None:
                    continue

            if cleaned_data.get('customer_emails') == 'without':
                if customer.email:
                    continue

            if cleaned_data.get('movement_and_outstanding_balance'):
                if balance == 0 or balance is None:
                    continue

            if cleaned_data.get('movement_and_no_outstanding_balance'):
                if balance or balance != 0:
                    continue

            if cleaned_data.get('only_outstanding_balance'):
                if not balance or balance == 0:
                    continue

            if cleaned_data.get('dormant_for_period'):
                pass

            filtered_customers[customer] = {'balance': balance}
        return filtered_customers

    def get_context_data(self, **kwargs):
        context = super(GenerateStatementsView, self).get_context_data(**kwargs)
        year = self.get_year()
        context['year'] = year
        form = StatementForm(self.request.GET, company=self.get_company(), year=year)
        customers = []
        if form.is_valid():
            customers = form.execute()

        context['customers'] = customers
        context['from_period'] = self.request.GET.get('from_period')
        context['to_period'] = self.request.GET.get('to_period')
        return context


class BaseStatementMixin(ProfileMixin):

    def get_history_period(self, periods, start, offset=None):
        try:
            period_page = periods[start:offset]
            period = period_page[0]
            if not period:
                return None
        except IndexError:
            return None
        return period

    def get_periods_ranges(self, year, period_to, count=5):
        return Period.objects.filter(
            company=year.company, id__lte=period_to.id
        ).exclude(
            period_year__isnull=True
        ).order_by(
            'period_year', '-period'
        )[0:count]

    def get_customer(self, customer_id):
        return Customer.objects.prefetch_related('sales_invoice_related').get(pk=customer_id)

    def get_opening_balance(self, year, customer_id):
        opening_balance = OpeningBalance.objects.filter(year=self.get_year(), customer_id=customer_id).first()
        if opening_balance and opening_balance.amount:
             return opening_balance.amount
        return Decimal(0)

    def customer_statement(self, year, customer_id, from_period, to_period):
        balance = Ledger.objects.customer_opening_balance(customer_id, year, from_period)
        opening_balance = self.get_opening_balance(year=year, customer_id=customer_id)
        opening_movement = (balance['total'] if balance['total'] else Decimal(0)) + opening_balance

        ledgers = Ledger.objects.filter(
            customer_id=customer_id, period__period__gte=from_period, period__period__lte=to_period,
            period__period_year=year
        )
        closing_movement = opening_movement
        statement = {'balance': 0, 'credit': 0, 'debit': closing_movement, 'lines': {},
                     'opening_movement': opening_movement, 'closing_movement': closing_movement,
                     'opening_balance': opening_balance, 'first_page_lines': {}, 'other_pages': {}}
        balance = closing_movement
        limit = 23
        counter = 0
        chunk = 0
        for ledger in ledgers:
            balance += ledger.debit - ledger.credit
            line_balance = {'balance': balance, 'link_url': ''}
            if ledger.content_type and ledger.content_type.name == 'invoice':
                signature = signing.dumps({'document_id': ledger.object_id})
                line_balance['link_url'] = reverse('invoice_document', kwargs={'signature': signature})
            else:
                signature = signing.dumps({'document_id': ledger.object_id})
                line_balance['link_url'] = reverse('receipt_document', kwargs={'signature': signature})

            statement['debit'] += ledger.debit
            statement['credit'] += ledger.credit
            statement['balance'] = balance
            statement['lines'][ledger] = line_balance
            if counter == limit:
                chunk += 1
                counter = 0
                limit = -1
            if limit > 0:
                statement['first_page_lines'][ledger] = line_balance
            else:
                statement['other_pages'][ledger] = line_balance
            counter += 1
        return statement

    def get_statement_date(self, year, to_period):
        period = Period.objects.filter(period=to_period, period_year=year).first()
        return period.to_date


class StatementView(BaseStatementMixin, TemplateView):
    template_name = 'statement/statement.html'

    def get_statement_form(self):
        data = {k: v for k, v in self.request.GET.items()}
        data['customer'] = self.kwargs['customer_id']
        return CustomerStatementForm(data=data, initial={'customer': self.kwargs['customer_id']}, company=self.get_company())

    # PyUnresolvedReferences
    def get_context_data(self, **kwargs):
        context = super(StatementView, self).get_context_data(**kwargs)
        if not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
            ssl._create_default_https_context = ssl._create_unverified_context

        form = self.get_statement_form()
        if form.is_valid():
            customer = form.cleaned_data['customer']
            from_period = form.cleaned_data['from_period']
            to_period = form.cleaned_data['to_period']
            year = self.get_year()
            statement = self.customer_statement(year, customer.id, from_period.period, to_period.period)
            customer_age_balance = Customer.objects.get_age_balances(
                customer=customer, year=year, current_period=to_period
            ).first()
            context['current_total'] = customer_age_balance.get('current_total', 0)
            context['thirty_days_total'] = customer_age_balance.get('thirty_day_total', 0)
            context['sixty_days_total'] = customer_age_balance.get('sixty_day_total', 0)
            context['ninety_days_total'] = customer_age_balance.get('ninety_day_total', 0)
            context['one_twenty_days_total'] = customer_age_balance.get('one_twenty_day_total', 0)
            context['year'] = year
            context['company'] = year.company
            context['statement_date'] = to_period.to_date
            context['customer'] = customer
            context['statement'] = statement
            query_string = f"?to_period={to_period.id}&from_period={from_period.id}"
            pdf_url = reverse('statement_pdf', kwargs={'customer_id': customer.id})
            email_url = reverse('email_statement', kwargs={'customer_id': customer.id})
            print_url = reverse('print_statement', kwargs={'customer_id': customer.id})
            context['pdf_url'] = f"{pdf_url}{query_string}"
            context['email_url'] = f"{email_url}{query_string}"
            context['print_url'] = f"{print_url}{query_string}&print=1"
            context['account_details'] = customer.company.bank_details.first() if customer.company.bank_details else None
        else:
            context['errors'] = form.errors
        return context


# TODO generate the pdf in background and provide link to the
class GeneratePdfsView(BaseStatementMixin, View):

    def generate_statement(self, customer, from_period, to_period):
        context = {}
        year = self.get_year()
        statement = self.customer_statement(year, customer.id, from_period.period, to_period.period)
        customer_age_balance = Customer.objects.get_age_balances(customer=customer, year=year, current_period=to_period).first()
        context['current_total'] = customer_age_balance.get('current_total', 0)
        context['thirty_days_total'] = customer_age_balance.get('thirty_day_total', 0)
        context['sixty_days_total'] = customer_age_balance.get('sixty_day_total', 0)
        context['ninety_days_total'] = customer_age_balance.get('ninety_day_total', 0)
        context['one_twenty_days_total'] = customer_age_balance.get('one_twenty_day_total', 0)
        context['year'] = year
        context['company'] = year.company
        context['statement_date'] = to_period.to_date
        context['customer'] = customer
        context['statement'] = statement
        query_string = f"?to_period={to_period.id}&from_period={from_period.id}"
        pdf_url = reverse('statement_pdf', kwargs={'customer_id': customer.id})
        email_url = reverse('email_statement', kwargs={'customer_id': customer.id})
        print_url = reverse('print_statement', kwargs={'customer_id': customer.id})
        context['pdf_url'] = f"{pdf_url}{query_string}"
        context['email_url'] = f"{email_url}{query_string}"
        context['print_url'] = f"{print_url}{query_string}&print=1"
        return context

    def generate_pdf(self, customer, from_period, to_period, dir_name):

        context = self.generate_statement(customer, from_period, to_period)
        html_string = render_to_string('statement/pdf.html', context)

        # Using Rendered HTML to generate PDF
        pdf_doc = HTML(string=html_string, base_url=self.request.get_host())
        pdf = pdf_doc.write_pdf(stylesheets=[
            settings.STATIC_IMAGES_ROOT + 'css/font-awesome.min.css',
            settings.STATIC_IMAGES_ROOT + 'css/pdf.css',
        ])

        filename = os.path.join(dir_name,  f"{customer.number.lower()}_statement.pdf")
        if os.path.exists(dir_name):
            with open(filename, 'wb') as f:
                f.write(pdf)
        return filename

    def get(self, request, *args, **kwargs):
        if not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
            ssl._create_default_https_context = ssl._create_unverified_context

        customer_ids = self.request.GET.getlist('customers[]')
        from_period = Period.objects.get(id=int(self.request.GET.get('from_period')))
        to_period = Period.objects.get(id=int(self.request.GET.get('to_period')))
        customers = Customer.objects.filter(id__in=[int(customer_id) for customer_id in customer_ids])
        files = []
        _, dir_name = Company.create_company_dir('statements')
        for customer in customers:
            pdf_file = self.generate_pdf(customer, from_period, to_period, dir_name)
            files.append(pdf_file)
        zip_location = os.path.join(dir_name, 'customer_statements.zip')
        if len(files) > 0:
            with ZipFile(zip_location, 'w') as zip:
                for file in files:
                    zip.write(file, os.path.basename(file))

        zip_url = settings.MEDIA_URL + 'statements/tmp/customer_statements.zip'
        return JsonResponse({
            'text': 'Pdfs generated successfully... download will start in a second ',
            'error': False,
            'files': files,
            'download_url': zip_url
        })


class StatementPdfView(WeasyTemplateResponseMixin, StatementView):
    template_name = 'statement/pdf.html'
    pdf_stylesheets = [
        settings.STATIC_IMAGES_ROOT + 'css/font-awesome.min.css',
        settings.STATIC_IMAGES_ROOT + 'css/pdf.css',
    ]

    pdf_attachment = True

    def get_pdf_filename(self):
        customer = self.get_customer(self.kwargs.get('customer_id'))

        return f"{customer.number.lower()}_statement.pdf"


class EmailStatementView(BaseStatementMixin, View):

    def get(self, request, *args, **kwargs):
        if not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
            ssl._create_default_https_context = ssl._create_unverified_context

        year = self.get_year()
        customer = self.get_customer(self.kwargs.get('customer_id'))
        from_period = self.request.GET.get('from_period')
        to_period = self.request.GET.get('to_period')
        period_to = Period.objects.get(pk=to_period)
        customer_age_balance = Customer.objects.get_age_balances(customer=customer, year=year,
                                                                 current_period=period_to).first()
        data = {'customer': customer.id, 'year_id': year.id, 'from_period': from_period, 'to_period': to_period}
        email_statement = StatementEmail(customer, customer_age_balance, data, self.request.get_host())
        email_statement.send()
        return JsonResponse({
            'text': 'Statement email successfully send',
            'error': False
        })


class EmailStatementsView(BaseStatementMixin, View):

    def get(self, request, *args, **kwargs):
        customer_ids = self.request.GET.getlist('customers[]')
        from_period = Period.objects.get(id=int(self.request.GET.get('from_period')))
        to_period = Period.objects.get(id=int(self.request.GET.get('to_period')))
        customers = Customer.objects.filter(id__in=[int(customer_id) for customer_id in customer_ids])
        files = []
        year = self.get_year()
        for customer in customers:
            statement = self.customer_statement(year, customer.id, from_period.period, to_period.period)
            data = {'customer': customer.id, 'year_id': year.id, 'from_period': from_period.id, 'to_period': to_period.id}

            email_statement = StatementEmail(customer, statement, data, self.request.get_host())
            email_statement.send()
        return JsonResponse({
            'text': 'Emails successfully send',
            'error': False,
            'files': files
        })


class MultiCustomerActionView(View):

    def get(self, *args, **kwargs):
        customers = self.request.GET.getlist('customers[]')
        if customers:
            redirect_url = ''
            action = 'print'
            if action == 'print':
                redirect_url = reverse('print_statements')
            redirect_url = f"{redirect_url}?customers={','.join(customers)}"
            return JsonResponse({
                'text': 'Redirecting....',
                'redirect': redirect_url,
                'error': False
            })
        else:
            return JsonResponse({
                'error': True,
                'text': 'No customers selcted'
            })


class PrintStatementView(WeasyTemplateResponseMixin, StatementView):
    template_name = 'statement/print.html'


class ShowStatementsView(BaseStatementMixin, TemplateView):

    def get_template_names(self):
        if self.kwargs['view_type'] == 'print':
            return 'statements/print.html'
        return 'statements/detail.html'

    def get_customer_statements(self):
        customer_ids = self.request.GET.getlist('customer[]')
        from_period = Period.objects.get(id=int(self.request.GET.get('from_period')))
        to_period = Period.objects.get(id=int(self.request.GET.get('to_period')))
        customers = Customer.objects.filter(id__in=[int(customer_id) for customer_id in customer_ids])
        year = self.get_year()
        statements = {}
        for customer in customers:
            context = {}
            statement = self.customer_statement(year, customer.id, from_period.period, to_period.period)
            customer_age_balance = Customer.objects.get_age_balances(customer=customer, year=year, current_period=to_period).first()

            context['current_total'] = customer_age_balance.get('current_total', 0)
            context['thirty_days_total'] = customer_age_balance.get('thirty_day_total', 0)
            context['sixty_days_total'] = customer_age_balance.get('sixty_day_total', 0)
            context['ninety_days_total'] = customer_age_balance.get('ninety_day_total', 0)
            context['one_twenty_days_total'] = customer_age_balance.get('one_twenty_day_total', 0)
            context['year'] = year
            context['company'] = year.company
            context['statement_date'] = to_period.to_date
            context['customer'] = customer
            context['statement'] = statement
            context['is_print'] = self.kwargs['view_type'] == 'print'
            context['account_details'] = year.company.bank_details.first() if year.company.bank_details else None
            statements[customer] = context
        return statements

    def get_context_data(self, **kwargs):
        context = super(ShowStatementsView, self).get_context_data(**kwargs)
        context['statements'] = self.get_customer_statements()
        return context


class ViewStatementsView(BaseStatementMixin, TemplateView):
    template_name = 'statement/view.html'

    def get_statement_form(self, data):
        data.pop('year_id')
        data['customer'] = data['customer']
        return CustomerStatementForm(data=data, company=self.get_company())

    def get_extracted_year(self, extracted_data):
        try:
            return Year.objects.get(id=extracted_data['year_id'])
        except Year.DoesNotExist:
            raise ValidationError('Year not found')

    def get_context_data(self, **kwargs):
        context = super(ViewStatementsView, self).get_context_data(**kwargs)
        extracted_data = signing.loads(self.kwargs.get('signature'))
        year = self.get_extracted_year(extracted_data)
        form = self.get_statement_form(extracted_data)

        if form.is_valid():
            customer = form.cleaned_data['customer']
            from_period = form.cleaned_data['from_period']
            to_period = form.cleaned_data['to_period']

            statement = self.customer_statement(year, customer.id, from_period.period, to_period.period)
            customer_age_balance = Customer.objects.get_age_balances(customer=customer, year=year, current_period=to_period).first()

            context['current_total'] = customer_age_balance.get('current_total', 0)
            context['thirty_days_total'] = customer_age_balance.get('thirty_day_total', 0)
            context['sixty_days_total'] = customer_age_balance.get('sixty_day_total', 0)
            context['ninety_days_total'] = customer_age_balance.get('ninety_day_total', 0)
            context['one_twenty_days_total'] = customer_age_balance.get('one_twenty_day_total', 0)
            context['year'] = year
            context['company'] = year.company
            context['statement_date'] = to_period.to_date
            context['customer'] = customer
            context['statement'] = statement
            context['is_public'] = True
            context['account_details'] = year.company.bank_details.first() if year.company.bank_details else None
            query_string = f"?to_period={to_period.id}&from_period={from_period.id}"
            pdf_url = reverse('statement_pdf', kwargs={'customer_id': customer.id})
            print_url = reverse('print_statement', kwargs={'customer_id': customer.id})
            context['pdf_url'] = f"{pdf_url}{query_string}"
            context['print_url'] = f"{print_url}{query_string}&print=1"
        else:
            context['errors'] = form.errors
        return context


class ScreenStatementsView(BaseStatementMixin, TemplateView):
    template_name = 'statement/screen.html'

    def get_context_data(self, **kwargs):
        context = super(ScreenStatementsView, self).get_context_data(**kwargs)
        customer_ids = self.request.GET.getlist('customers[]')
        from_period = Period.objects.get(id=int(self.request.GET.get('from_period')))
        to_period = Period.objects.get(id=int(self.request.GET.get('to_period')))
        customers = Customer.objects.filter(id__in=[int(customer_id) for customer_id in customer_ids])
        statements = {}
        year = self.get_year()
        for customer in customers:
            statement = self.customer_statement(year, customer.id, from_period.period, to_period.period)
            data = {'customer': customer.id, 'year_id': year.id, 'from_period': from_period.id,
                    'to_period': to_period.id}
            customer_age_balance = Customer.objects.get_age_balances(customer=customer, year=year, current_period=to_period).first()
            customer_statement = {}
            customer_statement['current_total'] = customer_age_balance.get('current_total', 0)
            customer_statement['thirty_days_total'] = customer_age_balance.get('thirty_day_total', 0)
            customer_statement['sixty_days_total'] = customer_age_balance.get('sixty_day_total', 0)
            customer_statement['ninety_days_total'] = customer_age_balance.get('ninety_day_total', 0)
            customer_statement['one_twenty_days_total'] = customer_age_balance.get('one_twenty_day_total', 0)
            customer_statement['year'] = year
            customer_statement['company'] = year.company
            customer_statement['statement_date'] = to_period.to_date
            customer_statement['customer'] = customer
            customer_statement['statement'] = statement
            statements[customer] = customer_statement
        context['statements'] = statements
        return context


class OpeningBalanceView(ProfileMixin, TemplateView):
    template_name = 'customer/opening_balances.html'

    def get_opening_balances(self, company, year):
        opening_balances = OpeningBalance.objects.filter(year=year)
        customer_balances = {}
        for opening_balance in opening_balances:
            customer_balances[opening_balance.customer_id] = opening_balance
        return customer_balances

    def get_customers(self, company, year):
        customers = Customer.objects.filter(company=company)
        opening_balances = self.get_opening_balances(company, year)
        customer_list = {}
        total = 0
        for customer in customers:
            customer_list[customer.id] = {
                'id': customer.id,
                'code': customer.number,
                'name': customer.name,
                'opening_balance': 0
            }
            if customer.id in opening_balances:
                customer_list[customer.id]['opening_balance'] = opening_balances[customer.id].amount
                total += opening_balances[customer.id].amount
        return customer_list, total

    def get_context_data(self, **kwargs):
        context = super(OpeningBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        customers = self.get_customers(company, year)

        context['customers'] = customers[0]
        context['total'] = customers[1]
        context['year'] = year
        return context


class UpdateOpeningBalanceView(ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        obj = OpeningBalance.objects.filter(customer_id=self.kwargs['customer_id'], year=self.get_year()).first()
        amount = Decimal(self.request.POST.get('amount', 0))
        if obj:
            obj.amount = amount
            obj.save()
        else:
            obj = OpeningBalance.objects.create(
                customer_id=self.kwargs['customer_id'],
                year=self.get_year(),
                amount=amount
            )
        return JsonResponse({
            'text': 'Opening balance updated',
            'error': False,
            'amount': obj.amount
        })


class SearchCustomersView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        customers = []
        if 'term' in self.request.GET:
            term = self.request.GET['term']
            customers = Customer.objects.search(self.get_company(), term).values('number', 'name', 'id')[0:50]
        customers_list = []
        for customer in customers:
            customers_list.append({
                'code': customer['number'],
                'name': customer['name'],
                'text': f"{customer['number']} - {customer['name']}",
                'label': f"CUS{customer['number']}",
                'id': customer['id']
            })
        return JsonResponse({'err': 'nil', 'results': customers_list})


class AutocompleteSearchCustomersView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        customers = []
        term = self.request.GET.get('term')
        if term:
            customers = Customer.objects.search(self.get_company(), term).values('number', 'name', 'id')[0:50]
        customers_list = []
        for customer in customers:
            customers_list.append({
                'code': customer['number'],
                'name': customer['name'],
                'text': f"{customer['number']} - {customer['name']}",
                'label': f"CUS{customer['number']}",
                'id': customer['id'],
                'description': customer['name']
            })
        return HttpResponse(json.dumps(customers_list))


class CustomerDetailView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        customer = Customer.objects.with_balance().get(pk=self.kwargs['pk'])
        data = CustomerSerializer(instance=customer).data
        return JsonResponse(data)


class BalanceSheetVsAgeAnalysisView(ProfileMixin, TemplateView):
    template_name = 'customer/balance_sheet_vs_age_analysis.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        company = self.get_company()
        
        running_year = Year.objects.started().get(company=company)
        period = Period.objects.open(company=company, year=running_year).first()

        account_total = get_total_for_account(
            year=running_year,
            company=company,
            account=company.default_customer_account,
            start_date=running_year.start_date,
            end_date=running_year.end_date
        )
        age_analysis = AgeAnalysis.objects.filter(period=period).aggregate(total=Sum('balance'))
        age_total = abs(age_analysis['total']) if age_analysis and age_analysis['total'] else 0

        context['account_total'] = account_total
        context['age_total'] = age_total
        context['is_balancing'] = age_total == account_total
        return context



