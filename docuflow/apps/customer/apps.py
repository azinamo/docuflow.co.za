from django.apps import AppConfig


class CustomerConfig(AppConfig):
    name = 'docuflow.apps.customer'

    def ready(self):
        import docuflow.apps.customer.signal_receivers # noqa

