from rest_framework import generics

from docuflow.apps.customer.api.serializers import CustomerSerializer
from .models import Customer


class DetailView(generics.RetrieveAPIView):
    serializer_class = CustomerSerializer

    def get_queryset(self):
        return Customer.objects.with_balance().select_related(
            'company', 'salesman', 'supplier', 'delivery_method', 'delivery_term', 'default_account'
        ).prefetch_related(
            'pricings', 'addresses'
        )
