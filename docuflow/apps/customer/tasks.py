# invoice.py


def log_request(sender, env, **kwargs):
    method = env['REQUEST_METHOD']


def calculate_available_credit(available_credit, debit=0, credit=0):
    available_credit = available_credit or 0
    if debit:
        available_credit -= debit
    if credit:
        available_credit += credit
    return available_credit
