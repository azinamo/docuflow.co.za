from django.forms import ModelChoiceField


class CustomerModelChoiceField(ModelChoiceField):

    def label_from_instance(self, obj):
        return f"{obj.name}"
