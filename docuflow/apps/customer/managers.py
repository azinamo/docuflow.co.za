from django.contrib.contenttypes.fields import ContentType
from django.db import models

from .enums import LedgerStatus, PaymentTerm


# Create your models here.
class LedgerManager(models.Manager):

    def create_ledger(self, customer, amount, profile, period, date, **extra_fields):

        ledger = {'customer': customer, 'amount': amount, 'profile': profile, 'period': period,
                  'date': date}
        ledger = self.model(**ledger, **extra_fields)
        ledger.save(using=self._db)
        return ledger

    def create_customer_ledger(self, invoice,  profile, is_pending=False):
        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        if content_type and invoice.customer:
            status = 1
            if is_pending:
                status = 0
            ledger = self.filter(
                customer=invoice.customer,
                content_type=content_type,
                object_id=invoice.id
            ).first()
            if ledger:
                ledger.status = 1
                ledger.save()
            else:
                ledger = self.create(
                    customer=invoice.customer,
                    content_type=content_type,
                    object_id=invoice.id,
                    amount=invoice.total_amount,
                    created_by=profile,
                    period=invoice.period,
                    date=invoice.invoice_date,
                    text=str(invoice),
                    status=status
                )
            return ledger


class CustomerQuerySet(models.QuerySet):

    def get_balance_filter(self, year=None, period_from=None, period_to=None):
        q_filter = models.Q()
        if year:
            q_filter = q_filter.add(models.Q(period__period_year=year), models.Q.AND)
        if period_from:
            q_filter = q_filter.add(models.Q(period__period__gte=period_from), models.Q.AND)
        if period_to:
            q_filter = q_filter.add(models.Q(period__period__lte=period_to), models.Q.AND)
        return q_filter

    def search(self, company, term):
        return self.filter(company=company).filter(
            models.Q(name__istartswith=term) | models.Q(number__istartswith=term)
        ).exclude(is_default=True)

    def invoicable(self):
        return self.filter(is_default=False)

    def exclude_cash(self):
        return self.exclude(payment_term=PaymentTerm.CASH_CUSTOMER).exclude(is_default=True)

    def balances_report(self, filters, period_filters):
        balance_filter = models.Q()
        balance_filter.add(models.Q(ledgers__period__period__lte=period_filters['to_period']), models.Q.AND)
        balance_filter.add(models.Q(ledgers__period__period__gte=period_filters['from_period']), models.Q.AND)
        return self.annotate(
            total_balance=models.Case(
                models.When(balance_filter, then=models.Sum('ledgers__amount')), output_field=models.DecimalField()
            )
        ).filter(
            **filters
        ).values(
            'customer__name', 'customer__number', 'total_balance'
        ).order_by(
            'customer__name'
        )

    def create_from_invoice(self, invoice, profile, status='complete'):
        is_pending = False if status == 'complete' else True
        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        if content_type and invoice.customer:
            status = LedgerStatus.COMPLETE
            if is_pending:
                status = LedgerStatus.PENDING
            customer_ledger = self.objects.filter(
                customer=invoice.customer,
                content_type=content_type,
                object_id=invoice.id
            ).first()
            if customer_ledger:
                customer_ledger.status = LedgerStatus.COMPLETE
                customer_ledger.save()
            else:
                customer_ledger = self.objects.create(
                    customer=invoice.customer,
                    content_type=content_type,
                    object_id=invoice.id,
                    amount=invoice.total_amount,
                    created_by=profile,
                    period=invoice.period,
                    date=invoice.invoice_date,
                    text=str(invoice),
                    status=status
                )
            return customer_ledger

