from datetime import datetime
import logging

from django.conf import settings
from django.urls import reverse
from django.core import signing

from emailtools import HTMLEmail


logger = logging.getLogger(__name__)


class StatementEmail(HTMLEmail):
    from_email = settings.NO_REPLY_EMAIL
    subject = 'Customer statement'
    template_name = 'statement/email.html'

    def get_to(self):
        logger.info(f"Send email to {[self.args[0].email]}")
        return [self.args[0].email]

    def generate_signature(self):
        return signing.dumps(self.args[2])

    def get_context_data(self, **kwargs):
        kwargs = super(StatementEmail, self).get_context_data(**kwargs)
        customer = self.args[0]
        customer_age_balance = self.args[1]
        kwargs['customer'] = customer
        # kwargs['statement'] = self.args[1]
        signature = self.generate_signature()
        url_path = reverse('view_statement', kwargs={'signature': signature})
        kwargs['action_url'] = f"https://{self.args[3]}{url_path}"
        kwargs['date'] = datetime.now().date()
        kwargs['invoice_id'] = '1'
        kwargs['total'] = 0
        kwargs['due_date'] = datetime.now().date()
        kwargs['name'] = self.args[0].name
        kwargs['purchase_date'] = datetime.now().date()
        kwargs['company'] = customer.company
        kwargs['account_details'] = customer.company.bank_details.first() if customer.company.bank_details else None
        kwargs['current_total'] = customer_age_balance.get('current_total', 0)
        kwargs['thirty_days_total'] = customer_age_balance.get('thirty_day_total', 0)
        kwargs['sixty_days_total'] = customer_age_balance.get('sixty_day_total', 0)
        kwargs['ninety_days_total'] = customer_age_balance.get('ninety_day_total', 0)
        kwargs['one_twenty_days_total'] = customer_age_balance.get('one_twenty_day_total', 0)
        return kwargs


statement_email = StatementEmail.as_callable()
