import calendar
from collections import OrderedDict

from .models import *
from docuflow.apps.sales.models import Invoice, Receipt
from docuflow.apps.period.models import Period

pp = pprint.PrettyPrinter(indent=4)


class DayRange(object):

    def __init__(self, name, period):
        self.total = 0
        self.name = name
        self.totals = {}
        self.period = period


class AgeAnalysis(object):

    def __init__(self, company, year, date, is_historical):
        self.company = company
        self.year = year
        self.date = date
        self.is_historical = is_historical
        self.day_ranges = self.get_day_ranges()
        self.invoice_items = []

    def initialize_day_ranges(self):
        day_ranges = {
            'one_twenty_or_more_days': DayRange('one_twenty_or_more_days', None),
            'ninety_days': DayRange('ninety_days', None),
            'sixty_days': DayRange('sixty_days', None),
            'thirty_days': DayRange('thirty_days', None),
            'current': DayRange('current', None),
            'unallocated': DayRange('unallocated', None),
        }
        return day_ranges

    def get_month_day_range(self, date):
        first_day = date.replace(day=1)
        last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
        return first_day, last_day

    def get_period(self):
        if self.is_historical:
            highest_period = Period.objects.filter(
                from_date__lte=self.date, to_date__gte=self.date, company=self.company
            ).order_by(
                '-period'
            ).first()
            return highest_period.period
        else:
            try:
                highest_period = Period.objects.filter(
                    company=self.company, is_closed=False, period_year=self.year
                ).order_by(
                    '-period'
                ).first()
                return highest_period.period
            except Exception:
                highest_period = Period.objects.filter(
                    company=self.company, period_year=self.year
                ).order_by(
                    '-period'
                ).first()
                return highest_period.period

    def get_periods(self, period):
        periods_list = []
        max_periods = 5
        highest_period = period + 1
        if period:
            c = 0
            for p in reversed(range(1, highest_period)):
                if c < max_periods:
                    periods_list.append({'counter': p, 'year': int(self.year.year)})
                c += 1
            periods_count = len(periods_list)
            if periods_count == max_periods:
                return periods_list
            else:
                outstanding = max_periods - periods_count
                previous_year_periods = self.get_previous_year_periods(outstanding)
                return periods_list + previous_year_periods
        return periods_list

    def get_previous_year_periods(self, counter):
        periods = Period.objects.filter(
            company=self.company,
            period_year__year=int(self.year.year) - 1
        ).order_by(
            '-period'
        )[0:counter]
        period_list = []
        for p in periods:
            period_list.append({'counter': p.period, 'year': int(p.period_year.year)})
        return period_list

    def get_day_ranges(self):

        period = self.get_period()
        periods = self.get_periods(period)
        day_ranges = OrderedDict()
        if periods:
            day_ranges = {
                'one_twenty_or_more_days': {
                    'period': periods[4]['counter'],
                    'year': periods[4]['year'],
                    'to': periods[4],
                    'totals': {},
                    'total': 0,
                    'key': 'one_twenty_or_more_days'
                },
                'ninety_days': {
                    'period': periods[3]['counter'],
                    'year': periods[3]['year'],
                    'to': periods[3],
                    'totals': {},
                    'total': 0,
                    'key': 'ninety_days'
                },
                'sixty_days': {
                    'period': periods[2]['counter'],
                    'year': periods[2]['year'],
                    'to': periods[2],
                    'totals': {},
                    'total': 0,
                    'key': 'sixty_days'
                },
                'thirty_days': {
                    'period': periods[1]['counter'],
                    'year': periods[1]['year'],
                    'to': periods[1],
                    'totals': {},
                    'total': 0,
                    'key': 'thirty_days'
                },
                'current': {
                    'period': periods[0]['counter'],
                    'year': periods[0]['year'],
                    'to': periods[0],
                    'totals': {},
                    'total': 0,
                    'key': 'current'
                },
                'unallocated': {
                    'total': 0,
                    'key': 'unallocated'
                }
            }
        return day_ranges

    def execute(self):
        periods = {'one_twenty_or_more_days': 0,
                   'ninety_days': 0,
                   'sixty_days': 0,
                   'thirty_days': 0,
                   'current': 0,
                   'day_ranges': 0,
                   'total_unallocated': 0,
                   'total': 0
                   }

        for item in self.invoice_items:
            periods['item'] = item
            if item.period == self.day_ranges['current']['period']:
                periods['current'] += item.amount
            elif item.period == self.day_ranges['ninety_days']['period']:
                periods['ninety_days'] += item.amount
            elif item.period == self.day_ranges['sixty_days']['period']:
                periods['sixty_days'] += item.amount
            elif item.period == self.day_ranges['thirty_days']['period']:
                periods['thirty_days'] += item.amount
            elif item.period <= self.day_ranges['one_twenty_or_more_days']['period']:
                periods['one_twenty_or_more_days'] += item.amount
        return periods


class CustomerAgeAnalysis(object):

    def __init__(self, company, year, filter_options, is_historical=False):
        self.company = company
        self.year = year
        self.day_ranges = OrderedDict()
        self.customers = {}
        self.is_historical = filter_options.get('is_historical', is_historical)
        self.report_date = filter_options.get('date')
        self.filter_options = filter_options
        self.total = 0
        self.day_ranges = {
            'one_twenty_or_more_days': {
                'totals': {},
                'total': 0,
                'key': 'one_twenty_or_more_days'
            },
            'ninety_days': {
                'totals': {},
                'total': 0,
                'key': 'ninety_days'
            },
            'sixty_days': {
                'totals': {},
                'total': 0,
                'key': 'sixty_days'
            },
            'thirty_days': {
                'totals': {},
                'total': 0,
                'key': 'thirty_days'
            },
            'current': {
                'totals': {},
                'total': 0,
                'key': 'current'
            },
            'unallocated': {
                'total': 0,
                'key': 'unallocated'
            }
        }

    def get_day_ranges(self):
        age_analysis = AgeAnalysis(self.company, self.year, self.report_date, self.is_historical)
        return age_analysis.get_day_ranges()

    def get_invoices(self):
        filter_dict = self.filter_options
        date_filter = None
        if 'date' in filter_dict:
            filter_dict.pop('date')
        invoices = Invoice.objects.select_related(
            'invoice_type', 'period', 'branch__company', 'customer'
        ).prefetch_related(
            'payments'
        ).filter(
            **filter_dict
        ).filter(
            branch__company=self.company
        ).order_by(
            'customer__name'
        )
        if date_filter:
            invoices = invoices.filter(date_filter)
        # invoice_payment_filter = {'payments__payment__payment_date__lte': self.filter_options['date']}
        # invoices = invoices.annotate(total_paid=Sum('payments__allocated'))
        return invoices.all()

    def get_available_payments(self):
        filter_dict = self.filter_options.get('payment', {})

        payments = Receipt.objects.select_related(
            'branch__company', 'customer', 'period',
        ).prefetch_related(
            'invoices'
        ).filter(**filter_dict).filter(
            branch__company=self.company
        )

        return payments

    def get_invoice_data(self, invoice, invoice_payment_filter):
        print(f"Invoice {invoice} for customer {invoice.customer_id}")
        print()
        if self.is_historical:
            open_amount = invoice.calculate_open_amount(invoice_payment_filter)
        else:
            open_amount = invoice.open_amount
        if invoice.is_credit and open_amount > 0:
            open_amount = open_amount * -1
        invoice_data = {'amount': open_amount,
                        'period': invoice.period.period,
                        'customer': invoice.customer,
                        'invoice_is_credit_type': invoice.is_credit,
                        'year': int(invoice.period.period_year.year),
                        'invoice': invoice
                        }
        return invoice_data

    def get_customer_invoices(self, invoices, payments=None):
        if not payments:
            payments = []
        customers_invoices = {}
        invoice_payment_filter = {'payment__payment_date__lte': self.report_date}
        for invoice in invoices:
            if invoice.customer:
                invoice_data = self.get_invoice_data(invoice, invoice_payment_filter)
                open_amount = invoice_data.get('amount', 0)
                discount = 0
                if invoice.customer_discount:
                    discount = invoice.customer_discount
                if invoice.customer in customers_invoices:
                    customers_invoices[invoice.customer]['total'] += open_amount
                    customers_invoices[invoice.customer]['total_discount'] += discount
                    customers_invoices[invoice.customer]['invoices'].append(invoice_data)
                else:
                    customers_invoices[invoice.customer] = {'invoices': [invoice_data],
                                                            'total': open_amount,
                                                            'total_discount': discount,
                                                            'payments': [],
                                                            'total_payment': 0,
                                                            'total_payment_discount': 0
                                                            }
        for payment in payments:
            if payment.balance != 0 and payment.balance is not None:
                if payment.customer in customers_invoices:
                    if payment.balance:
                        customers_invoices[payment.customer]['payments'].append(payment)
                        customers_invoices[payment.customer]['total_payment'] += payment.balance
                        customers_invoices[payment.customer]['total'] += payment.balance
                else:
                    customers_invoices[payment.customer] = {
                                                           'invoices': [],
                                                           'total': payment.balance,
                                                           'total_discount': 0,
                                                           'payments': [payment],
                                                           'total_payment': payment.balance,
                                                           'total_payment_discount': 0}
        return customers_invoices

    def get_customer_data(self):
        payments = self.get_available_payments()
        invoices = self.get_invoices()

        customers_invoices = self.get_customer_invoices(invoices, payments)

        return customers_invoices

    def execute(self):
        customers_invoices = self.get_customer_data()
        self.get_customer_day_range_totals(customers_invoices)

    def get_customer_day_ranges(self, invoices, total_unallocated, day_ranges):
        customer = {'one_twenty_or_more_days': 0,
                    'ninety_days': 0,
                    'sixty_days': 0,
                    'thirty_days': 0,
                    'current': 0,
                    'day_ranges': 0,
                    'total_unallocated': total_unallocated,
                    'total': total_unallocated
                    }
        for invoice in invoices:
            invoice_customer = invoice.get('customer', None)
            invoice_period = invoice.get('period', None)
            if invoice_customer and invoice_period:
                invoice_total = invoice.get('amount', 0)
                invoice_year = invoice.get('year', self.year.year)
                invoice_is_credit_type = invoice.get('is_credit_type', False)
                customer['name'] = invoice_customer.name
                customer['code'] = invoice_customer.number
                customer['id'] = invoice_customer.id
                if invoice_is_credit_type:
                    invoice_total = invoice_total * -1
                customer['total'] += invoice_total

                if invoice_period == day_ranges['current']['period']:
                    customer['current'] += invoice_total
                elif invoice_period == day_ranges['ninety_days']['period']:
                    customer['ninety_days'] += invoice_total
                elif invoice_period == day_ranges['sixty_days']['period']:
                    customer['sixty_days'] += invoice_total
                elif invoice_period == day_ranges['thirty_days']['period']:
                    customer['thirty_days'] += invoice_total
                elif invoice_period <= day_ranges['one_twenty_or_more_days']['period']:
                    customer['one_twenty_or_more_days'] += invoice_total
        return customer

    def get_customer_day_range_totals(self, customers_invoices):
        day_ranges = self.get_day_ranges()
        pp.pprint(day_ranges)
        for customer, customer_invoice in customers_invoices.items():
            invoices = customer_invoice.get('invoices', [])
            total_unallocated = customer_invoice.get('total_payment', 0)
            customer_day_range_totals = self.get_customer_day_ranges(invoices, total_unallocated, day_ranges)
            self.day_ranges['unallocated']['total'] += customer_day_range_totals.get('total_unallocated', 0)
            self.customers[customer] = customer_day_range_totals
            self.day_ranges['one_twenty_or_more_days']['total'] += customer_day_range_totals['one_twenty_or_more_days']
            self.day_ranges['ninety_days']['total'] += customer_day_range_totals['ninety_days']
            self.day_ranges['sixty_days']['total'] += customer_day_range_totals['sixty_days']
            self.day_ranges['thirty_days']['total'] += customer_day_range_totals['thirty_days']
            self.day_ranges['current']['total'] += customer_day_range_totals['current']

            self.total += customer_day_range_totals['total']