import logging
import pprint

from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, JsonResponse

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.customer.models import Customer, Address


from .forms import AddressForm


logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=4)


class AddressView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Address
    template_name = 'address/index.html'
    context_object_name = 'addresses'

    def get_queryset(self):
        return Address.objects.select_related('customer').filter(customer_id=self.kwargs['customer_id'])

    def get_context_data(self, **kwargs):
        context = super(AddressView, self).get_context_data(**kwargs)
        context['customer'] = Customer.objects.get(pk=self.kwargs['customer_id'])
        return context


class CreateAddressView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = Address
    template_name = 'address/create.html'
    success_message = 'Address successfully saved'
    form_class = AddressForm

    def get_customer(self):
        return Customer.objects.get(id=self.kwargs['customer_id'])

    def get_context_data(self, **kwargs):
        context = super(CreateAddressView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['customer'] = self.get_customer()
        return context

    def get_success_url(self):
        return reverse('address_index')

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the address.',
                                 'errors': form.errors
                                 })
        return super(CreateAddressView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                address = form.save(commit=False)
                address.customer = self.get_customer()
                address.is_active = True
                address.save()

                return JsonResponse({'error': False,
                                     'text': 'Address saved successfully',
                                     'reload': True
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': f"Error saving the address {str(exception)}"
                                     })
        return HttpResponse('Not allowed')


class EditAddressView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = Address
    template_name = 'address/edit.html'
    success_message = 'Address successfully updated'
    form_class = AddressForm
    context_object_name = 'address'

    def get_initial(self):
        initial = super().get_initial()
        initial['customer'] = self.get_object().customer
        return initial

    def get_context_data(self, **kwargs):
        context = super(EditAddressView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditAddressView, self).get_form_kwargs()
        return kwargs

    def get_success_url(self):
        return reverse('pricing_index')

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the address, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditAddressView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                form.save()

                return JsonResponse({'error': False,
                                     'text': 'Address updated successfully',
                                     'reload': True
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': f"Error saving the pricing {str(exception)}"
                                     })
        return HttpResponse('Not allowed')


class DeleteAddressView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Address
    template_name = 'address/confirm_delete.html'
    success_message = 'Address successfully deleted'

    def get_success_url(self):
        return reverse_lazy('address_index', kwargs={'customer_id': self.get_object().customer_id})

    def delete(self, request, *args, **kwargs):
        super().delete(request, *args, **kwargs)
        return JsonResponse({
            'error': False,
            'text': 'Address deleted successfully',
            'reload': True
        })


