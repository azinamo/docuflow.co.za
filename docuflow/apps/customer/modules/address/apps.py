from django.apps import AppConfig


class AddressConfig(AppConfig):
    name = 'docuflow.apps.customer.modules.address'
