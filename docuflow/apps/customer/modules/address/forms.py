from django import forms

from docuflow.apps.customer.models import Address
from docuflow.apps.customer.enums import AddressType


class BaseAddressForm(forms.ModelForm):

    class Meta:
        model = Address
        fields = ('address_line_1', 'address_line_2', 'address_line_3', 'city', 'province', 'postal_code', 'country',
                  'customer')

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data['address_line_1']:
            self.add_error('address_line_1', 'Street Address is required')


class AddressForm(forms.ModelForm):

    class Meta:
        model = Address
        fields = ('address_line_1', 'address_line_2', 'address_line_3', 'city', 'province', 'postal_code', 'country')

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data['address_line_1']:
            self.add_error('address_line_1', 'Street Address is required')


class PostalAddressForm(BaseAddressForm):

    def save(self, commit=True):
        address = super().save(commit=False)
        address.address_type = AddressType.POSTAL
        address.save()
        return address


class DeliveryAddressForm(BaseAddressForm):

    def save(self, commit=True):
        address = super().save(commit=False)
        address.address_type = AddressType.DELIVERY
        address.save()
        return address

