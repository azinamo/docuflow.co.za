from django.urls import path

from . import views

urlpatterns = [
    path('<int:customer_id>/', views.AddressView.as_view(), name='address_index'),
    path('<int:customer_id>/create/', views.CreateAddressView.as_view(), name='create_address'),
    path('edit/<int:pk>/', views.EditAddressView.as_view(), name='edit_address'),
    path('delete/<int:pk>/', views.DeleteAddressView.as_view(), name='delete_address'),
]
