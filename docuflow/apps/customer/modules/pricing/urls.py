from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('<int:customer_id>/', views.PricingView.as_view(), name='pricing_index'),
    path('<int:customer_id>/create/', views.CreatePricingView.as_view(), name='create_pricing'),
    path('edit/<pk>/', csrf_exempt(views.EditPricingView.as_view()), name='edit_pricing'),
    path('delete/<int:pk>/', views.DeletePricingView.as_view(), name='delete_pricing'),
]
