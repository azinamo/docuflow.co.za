from django.apps import AppConfig


class PricingConfig(AppConfig):
    name = 'docuflow.apps.customer.modules.pricing'
