from django import forms

from docuflow.apps.customer.models import Pricing, FixedPricing, QuantityDiscount, QuantityPricing


class PricingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PricingForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Pricing
        fields = ('name',)


class PriceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PriceForm, self).__init__(*args, **kwargs)

    class Meta:
        model = FixedPricing
        fields = ('price', 'price_including', )


class QuantityDiscountForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(QuantityDiscountForm, self).__init__(*args, **kwargs)

    class Meta:
        model = QuantityDiscount
        fields = ('quantity', 'discount', )


class QuantityPriceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(QuantityPriceForm, self).__init__(*args, **kwargs)

    class Meta:
        model = QuantityPricing
        fields = ('quantity', 'price', )
