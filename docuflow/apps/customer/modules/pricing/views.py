import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import JsonResponse
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, DeleteView, FormView
from django.urls import reverse, reverse_lazy

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax_post, is_ajax
from docuflow.apps.customer.models import Customer, Pricing

from .forms import PricingForm


logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=4)


class DeliveryTermListJson(View):
    model = Pricing
    columns = ['id', 'code', 'name']
    order_columns = ['id', 'code', 'name']

    max_display_length = 30

    def render_column(self, row, column):
        # We want to render as a custom column
        return super(DeliveryTermListJson, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(name__startswith=search)
        return qs


class PricingView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Pricing
    template_name = 'pricing/index.html'
    context_object_name = 'pricings'

    def get_customer(self):
        return Customer.objects.get(pk=self.kwargs['customer_id'])

    def get_queryset(self):
        return Pricing.objects.select_related('customer').filter(customer=self.get_customer()).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(PricingView, self).get_context_data(**kwargs)
        context['customer'] = self.get_customer()
        return context


class CreatePricingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = Pricing
    template_name = 'pricing/fixed_prices/create.html'
    success_message = 'Pricing successfully saved'
    form_class = PricingForm
    success_url = reverse_lazy('pricing_index')

    def get_customer(self):
        return Customer.objects.filter(id=self.kwargs['customer_id']).first()

    def get_context_data(self, **kwargs):
        context = super(CreatePricingView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['customer'] = self.get_customer()
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the supplier authorization, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(CreatePricingView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            pricing = form.save(commit=False)
            customer = self.get_customer()
            pricing.customer = customer
            pricing.is_active = True
            pricing.save()

            return JsonResponse({'error': False,
                                 'text': 'Pricing saved successfully',
                                 'reload': True
                                 })
        return super(CreatePricingView, self).form_valid(form)


class EditPricingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, FormView):
    model = Pricing
    template_name = 'pricing/edit.html'
    success_message = 'Pricing successfully updated'
    form_class = PricingForm
    success_url = reverse_lazy('pricing_index')

    def get_context_data(self, **kwargs):
        context = super(EditPricingView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the supplier authorization, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditPricingView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            pricing = form.save(commit=False)
            customer = self.get_customer()
            pricing.customer = customer
            pricing.save()

            return JsonResponse({'error': False,
                                 'text': 'Pricing updated successfully',
                                 'reload': True
                                 })
        return super(EditPricingView, self).form_valid(form)


class DeletePricingView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Pricing
    template_name = 'pricing/confirm_delete.html'
    success_message = 'Pricing successfully deleted'
    success_url = reverse_lazy('pricing_index')
