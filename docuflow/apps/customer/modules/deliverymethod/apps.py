from django.apps import AppConfig


class DeliverymethodConfig(AppConfig):
    name = 'docuflow.apps.customer.modules.deliverymethod'
