import csv
import logging
from calendar import monthrange
from datetime import datetime
from datetime import timedelta
from dateutil import parser
from django.utils import timezone
import pytz
import calendar
import pprint

from django.contrib import messages
from django.views.generic import ListView, TemplateView, View
from django_filters.views import FilterView
# from search_views.views import SearchListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.db.models import Q

from annoying.functions import get_object_or_None

from docuflow.apps.company.models import DeliveryMethod
from docuflow.apps.accounts.models import Profile
from docuflow.apps.period.models import Year, Period
from docuflow.apps.company.models import Company
from .forms import DeliveryMethodForm
from docuflow.apps.common.mixins import CompanyMixin, ProfileMixin

logger = logging.getLogger(__name__)
# Create your views here.

pp = pprint.PrettyPrinter(indent=4)


class DeliveryMethodListJson(View):
    model = DeliveryMethod
    columns = ['id', 'code', 'name']
    order_columns = ['id', 'code', 'name']

    max_display_length = 30

    def render_column(self, row, column):
        # We want to render as a custom column
        return super(DeliveryMethodListJson, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(name__startswith=search)

        return qs


class DeliveryMethodView(ProfileMixin, ListView):
    model = DeliveryMethod
    template_name = 'deliverymethod/index.html'
    context_object_name = 'delivery_methods'

    def get_queryset(self):
        return DeliveryMethod.objects.select_related('company').filter(company=self.get_company()).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(DeliveryMethodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = DeliveryMethod
    template_name = 'deliverymethod/create.html'
    success_message = 'Delivery method successfully saved'

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryMethodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        return reverse('delivery_method_index')

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryMethodView, self).get_form_kwargs()
        return kwargs

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_class(self):
        return DeliveryMethodForm

    def form_valid(self, form):
        try:
            customer = form.save(commit=False)
            company = self.get_company()
            customer.company = company
            customer.save()
            messages.success(self.request, 'Delivery method saved successfully')
            if 'redirect' in self.request.GET:
                return HttpResponseRedirect(self.request.GET['redirect'])
        except Exception as exception:
            messages.error(self.request, 'Unable to save delivery method;  {}.'.format(exception))
            return super(CreateDeliveryMethodView, self).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = DeliveryMethod
    template_name = 'deliverymethod/edit.html'
    success_message = 'Delivery method successfully updated'

    def get_context_data(self, **kwargs):
        context = super(EditDeliveryMethodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(EditDeliveryMethodView, self).get_form_kwargs()
        return kwargs

    def get_success_url(self):
        return reverse('delivery_method_index')

    def get_form_class(self):
        return DeliveryMethodForm


class DeleteDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = DeliveryMethod
    template_name = 'deliverymethod/confirm_delete.html'
    success_message = 'Delivery Method successfully deleted'

    def get_success_url(self):
        return reverse('delivery_method_index')

