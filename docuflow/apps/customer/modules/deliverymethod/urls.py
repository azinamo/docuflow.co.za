from django.conf.urls import url

from . import views
urlpatterns = [
    url(r'^$', views.DeliveryMethodView.as_view(), name='delivery_method_index'),
    url(r'^create/$', views.CreateDeliveryMethodView.as_view(), name='create_delivery_method'),
    url(r'^edit/(?P<pk>\d+)/$', views.EditDeliveryMethodView.as_view(), name='edit_delivery_method'),
    url(r'^delete/(?P<pk>\d+)/$', views.DeleteDeliveryMethodView.as_view(), name='delete_delivery_method'),
]
