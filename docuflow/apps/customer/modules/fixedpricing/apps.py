from django.apps import AppConfig


class FixedpricingConfig(AppConfig):
    name = 'docuflow.apps.customer.modules.fixedpricing'
