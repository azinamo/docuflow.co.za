import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.customer.models import Pricing, FixedPricing
from .forms import FixedPricingForm, ImportForm
from .services import ImportFixedPricing

logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=4)


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    model = FixedPricing
    template_name = 'fixedpricing/index.html'
    context_object_name = 'fixed_prices'

    def get_pricing(self):
        return Pricing.objects.get(
            pk=self.kwargs['pricing_id']
        )

    def get_queryset(self):
        return FixedPricing.objects.select_related(
            'pricing',
            'inventory'
        ).filter(
            pricing_id=self.kwargs['pricing_id']
        ).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['pricing'] = self.get_pricing()
        return context


class CreateFixedPricingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = FixedPricing
    template_name = 'fixedpricing/create.html'
    success_message = 'Fixed pricing successfully saved'

    def get_prcing(self):
        return Pricing.objects.filter(
            id=self.kwargs['pricing_id']
        ).first()

    def get_context_data(self, **kwargs):
        context = super(CreateFixedPricingView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['pricing'] = self.get_prcing()
        return context

    def get_success_url(self):
        return reverse('fixed_pricing_index')

    def get_form_kwargs(self):
        kwargs = super(CreateFixedPricingView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_form_class(self):
        return FixedPricingForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the fixed pricing, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(CreateFixedPricingView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                fixed_pricing = form.save(commit=False)
                pricing = self.get_prcing()
                fixed_pricing.pricing = pricing
                fixed_pricing.save()

                return JsonResponse({'error': False,
                                     'text': 'Fixed pricing saved successfully',
                                     'reload': True
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': 'Error saving the fixed pricing {}'.format(exception.__str__())
                                     })
        return HttpResponse('Not allowed')


class ImportFixedPricingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, FormView):
    template_name = 'fixedpricing/import.html'
    success_message = 'Fixed pricing successfully saved'

    def get_pricing(self):
        return Pricing.objects.filter(
            id=self.kwargs['pricing_id']
        ).first()

    def get_context_data(self, **kwargs):
        context = super(ImportFixedPricingView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['pricing'] = self.get_pricing()
        return context

    def get_success_url(self):
        return reverse('fixed_pricing_index')

    def get_form_class(self):
        return ImportForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred importing fixed pricings, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(ImportFixedPricingView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                csv_file = form.cleaned_data["csv_file"]
                logger.info("{} is xlsx -- {}".format(csv_file.name, csv_file.name.endswith('.xlsx')))
                if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx')):
                    return JsonResponse({'error': True,
                                         'text': 'File is not .csv, .xlsx or xls type',
                                         'details': 'Error importing the file'
                                         })
                if csv_file.multiple_chunks():
                    file_size = csv_file.size / (1000 * 1000)
                    return JsonResponse({'error': True,
                                         'text': "Uploaded file is too big ({} MB)." .format(file_size),
                                         'details': 'Error importing the file'
                                         })

                import_fixed_pricing = ImportFixedPricing(self.get_branch(), self.get_pricing(), csv_file)
                import_fixed_pricing.execute()

                return JsonResponse({'error': False,
                                     'text': 'Fixed Pricing updated successfully'
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': 'Error saving the pricing {}'.format(exception.__str__())
                                     })
        return HttpResponse('Not allowed')


class EditFixedPricingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = FixedPricing
    template_name = 'fixedpricing/edit.html'
    success_message = 'Fixed pricing successfully updated'

    def get_fixed_pricing(self):
        return FixedPricing.objects.filter(
            id=self.kwargs['pk']
        ).first()

    def get_context_data(self, **kwargs):
        context = super(EditFixedPricingView, self).get_context_data(**kwargs)
        context['fixed_pricing'] = self.get_fixed_pricing()
        return context

    def get_initial(self):
        initial = super(EditFixedPricingView, self).get_initial()
        fixed_pricing = self.get_fixed_pricing()
        if fixed_pricing:
            initial['inventory'] = fixed_pricing.inventory
        return initial

    def get_form_kwargs(self):
        kwargs = super(EditFixedPricingView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_success_url(self):
        return reverse('pricing_index')

    def get_form_class(self):
        return FixedPricingForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred updating the fixed pricing, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditFixedPricingView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                fixed_pricing = form.save(commit=False)
                fixed_pricing.save()

                return JsonResponse({'error': False,
                                     'text': 'Fixed pricing updated successfully',
                                     'reload': True
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': 'Error saving the fixed pricing {}'.format(exception.__str__())
                                     })
        return HttpResponse('Not allowed')


class DeleteFixedPricingView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = FixedPricing
    template_name = 'fixedpricing/confirm_delete.html'
    success_message = 'Pricing successfully deleted'

    def get_success_url(self):
        return reverse('fixed_pricing_index')
