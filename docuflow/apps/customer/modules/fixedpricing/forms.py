from django import forms

from docuflow.apps.customer.models import FixedPricing


class FixedPricingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(FixedPricingForm, self).__init__(*args, **kwargs)
        self.fields['inventory'].queryset = self.fields['inventory'].queryset.filter(
            branch__company=company
        )

    class Meta:
        model = FixedPricing
        fields = ('inventory', 'price', 'price_including', )


class ImportForm(forms.Form):

    csv_file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)')