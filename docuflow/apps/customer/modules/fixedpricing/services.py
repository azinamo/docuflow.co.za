import logging
from decimal import Decimal

from django.db.models import Q

from openpyxl import load_workbook

from docuflow.apps.customer.models import FixedPricing, Pricing, Customer
from docuflow.apps.inventory.models import BranchInventory


logger = logging.getLogger(__name__)


class ImportFixedPricing:

    def __init__(self, branch, pricing, csv_file):
        self.csv_file = csv_file
        self.pricing = pricing
        self.branch = branch

    def get_csv_import_data(self):
        return []

    def get_branch_inventory(self, branch, inventory_code=None, description=None):
        inventory_code = inventory_code.strip() if inventory_code else None
        description = description.strip() if description else None
        q_object = Q()
        q_object.add(Q(branch=branch), Q.AND)
        if description is not None:
            logger.info("Search by description {}".format(description))
            q_object.add(Q(description__icontains=description), Q.AND)
        if inventory_code is not None:
            logger.info("Search by  inventory_code {}".format(inventory_code))
            q_object.add(Q(inventory__code__icontains=inventory_code), Q.AND)

        inventory = BranchInventory.objects.filter(q_object).first()
        if inventory:
            return inventory
        return None

    def process_excel(self):
        wb = load_workbook(self.csv_file, data_only=True)
        counter = 0
        inventory_data = []
        sheet = wb.active

        rows = sheet.rows

        for row in rows:
            c = 0
            line_data = {'inventory': None, 'price': '', 'price_including': ''}
            inventory = None
            for cell in row:
                if c == 0:
                    inventory = self.get_branch_inventory(self.branch, cell.value)
                    line_data['inventory'] = inventory
                if c == 1 and not inventory:
                    line_data['inventory'] = self.get_branch_inventory(self.branch, None, cell.value)
                if c == 2:
                    line_data['price'] = 0 if cell.value == '' else cell.value
                if c == 3:
                    line_data['price_including'] = 0 if cell.value == '' else cell.value
                c = c + 1
                logger.info(cell)
            inventory_data.append(line_data)
            counter += 1
        return {'inventory': inventory_data, 'total': counter}

    def prepare_data(self, inventory_data):
        logger.info("Prepare import data")
        logger.info(inventory_data['inventory'])
        lines = []
        pricing_lines = inventory_data.get('inventory', [])
        for line_data in pricing_lines:

            logger.info(line_data)
            price = line_data.get('price', 0)
            including = line_data.get('price_including', 0)
            inventory = line_data.get('inventory', None)
            logger.info("type os value {} ---> {} for inventory {}".format(price, type(price), inventory))
            if type(price) == str:
                continue

            if price:
                price = Decimal(float(price))
            else:
                price = 0

            if including:
                including = Decimal(float(including))
            else:
                including = 0

            if price and inventory:
                fixed_pricing = FixedPricing.objects.filter(
                    pricing=self.pricing,
                    inventory=inventory
                ).first()
                if fixed_pricing:
                    logger.info("Fixed pricing already exists {} =-> {}".format(self.pricing, inventory))
                    fixed_pricing.price = price
                    fixed_pricing.price_including = including
                    fixed_pricing.inventory = inventory
                    fixed_pricing.pricing = self.pricing
                    fixed_pricing.save()
                else:
                    logger.info("New ---< Fixed pricing does not exists {} =-> {}".format(self.pricing, inventory))
                    fixed_pricing = FixedPricing()
                    fixed_pricing.price = price
                    fixed_pricing.price_including = including
                    fixed_pricing.inventory = inventory
                    fixed_pricing.pricing = self.pricing
                    fixed_pricing.save()
                    # fixed_pricing = FixedPricing(**{'price': price,
                    #                                 'price_including': including,
                    #                                 'inventory': inventory,
                    #                                 'pricing': self.pricing
                    #                                 })
                lines.append(fixed_pricing)
        logger.info(lines)
        return lines

    def execute(self):
        inventory_pricing_data = []
        if self.csv_file.name.endswith('.csv'):
            inventory_pricing_data = self.get_csv_import_data()
        elif self.csv_file.name.endswith('.xls') or self.csv_file.name.endswith('.xlsx'):
            inventory_pricing_data = self.process_excel()

        lines = self.prepare_data(inventory_pricing_data)

        # if len(lines) > 0:
        #     FixedPricing.objects.bulk_create(lines)
