from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('<int:pricing_id>/', views.IndexView.as_view(), name='fixed_pricing_index'),
    path('<int:pricing_id>/create/', csrf_exempt(views.CreateFixedPricingView.as_view()), name='create_fixed_pricing'),
    path('<int:pricing_id>/import/', csrf_exempt(views.ImportFixedPricingView.as_view()), name='import_fixed_pricing'),
    path('edit/<int:pk>/', csrf_exempt(views.EditFixedPricingView.as_view()), name='edit_fixed_pricing'),
    path('delete/<int:pk>/', views.DeleteFixedPricingView.as_view(), name='delete_fixed_pricing'),
]
