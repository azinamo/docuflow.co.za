from django.apps import AppConfig


class DeliverytermConfig(AppConfig):
    name = 'docuflow.apps.customer.modules.deliveryterm'
