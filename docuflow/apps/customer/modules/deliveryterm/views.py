import logging
import pprint

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.customer.models import DeliveryTerm
from .forms import DeliveryTermForm

logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=4)


class DeliveryTermListJson(View):
    model = DeliveryTerm
    columns = ['id', 'code', 'name']
    order_columns = ['id', 'code', 'name']

    max_display_length = 30

    def render_column(self, row, column):
        # We want to render as a custom column
        return super(DeliveryTermListJson, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(name__startswith=search)

        return qs


class DeliveryTermView(LoginRequiredMixin, ProfileMixin, ListView):
    model = DeliveryTerm
    template_name = 'deliveryterm/index.html'
    context_object_name = 'delivery_terms'

    def get_queryset(self):
        return DeliveryTerm.objects.select_related('company').filter(company=self.get_company()).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(DeliveryTermView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateDeliveryTermView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = DeliveryTerm
    template_name = 'deliveryterm/create.html'
    success_message = 'Delivery Term successfully saved'

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryTermView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        return reverse('delivery_term_index')

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryTermView, self).get_form_kwargs()
        return kwargs

    def get_form_class(self):
        return DeliveryTermForm

    def form_valid(self, form):
        try:
            customer = form.save(commit=False)
            company = self.get_company()
            customer.company = company
            customer.save()
            messages.success(self.request, 'Delivery term saved successfully')
            if 'redirect' in self.request.GET:
                return HttpResponseRedirect(self.request.GET['redirect'])
        except Exception as exception:
            messages.error(self.request, 'Unable to save delivery term;  {}.'.format(exception))
            return super(CreateDeliveryTermView, self).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditDeliveryTermView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = DeliveryTerm
    template_name = 'deliveryterm/edit.html'
    success_message = 'Delivery term successfully updated'

    def get_context_data(self, **kwargs):
        context = super(EditDeliveryTermView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditDeliveryTermView, self).get_form_kwargs()
        return kwargs

    def get_success_url(self):
        return reverse('delivery_term_index')

    def get_form_class(self):
        return DeliveryTermForm


class DeleteDeliveryTermView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = DeliveryTerm
    template_name = 'deliveryterm/confirm_delete.html'
    success_message = 'Delivery term successfully deleted'

    def get_success_url(self):
        return reverse('delivery_term_index')
