from django.urls import path

from . import views

urlpatterns = [
    path('', views.DeliveryTermView.as_view(), name='delivery_term_index'),
    path('create/', views.CreateDeliveryTermView.as_view(), name='create_delivery_term'),
    path('edit/<int:pk>/', views.EditDeliveryTermView.as_view(), name='edit_delivery_term'),
    path('delete/<int:pk>/', views.DeleteDeliveryTermView.as_view(), name='delete_delivery_term'),
]
