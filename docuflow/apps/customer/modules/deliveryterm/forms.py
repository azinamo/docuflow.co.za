from django import forms

from docuflow.apps.customer.models import DeliveryTerm


class DeliveryTermForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DeliveryTermForm, self).__init__(*args, **kwargs)

    class Meta:
        model = DeliveryTerm
        fields = ('name',)

