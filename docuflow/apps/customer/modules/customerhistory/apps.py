from django.apps import AppConfig


class CustomerhistoryConfig(AppConfig):
    name = 'docuflow.apps.customer.modules.customerhistory'
