from django import forms

from django_select2.forms import HeavySelect2Widget


class CustomerSearchForm(forms.Form):
    customer = forms.ChoiceField(widget=HeavySelect2Widget(data_view='search_customers_list', attrs={
        'data-view-url': 'view/customer/history/'
    }))
