from django.urls import path

from . import views

urlpatterns = [
    path('', views.HistoryView.as_view(), name='customer_history'),
    path('view', views.ViewHistoryView.as_view(), name='view_customer_history'),
    path('history/data/', views.CustomerHistoryDataView.as_view(), name='customer_history_data'),
]
