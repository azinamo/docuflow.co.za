import logging
import pprint
from datetime import datetime


from django.views.generic import ListView, FormView, View, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from docuflow.apps.customer.models import Customer, Ledger, OpeningBalance
from docuflow.apps.sales.models import Invoice
from docuflow.apps.inventory.exceptions import *
from docuflow.apps.common.mixins import CompanyMixin, ProfileMixin, CompanyLoginRequiredMixin
from .forms import LedgerSearchForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


# Create your views here.
class CustomerLedgerView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'customerledger/index.html'
    form_class = LedgerSearchForm

    # noinspection PyMethodMayBeStatic
    def get_customers(self, company):
        return Customer.objects.filter(
            company=company
        )

    def get_initial(self):
        initial = super(CustomerLedgerView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        return initial

    def get_context_data(self, **kwargs):
        context = super(CustomerLedgerView, self).get_context_data(**kwargs)
        company = self.get_company()
        customers = self.get_customers(company)
        context['year'] = self.get_year()
        context['from_customers'] = customers.order_by('number')
        context['to_customers'] = customers.order_by('-number')
        return context


class GenerateLedgerView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'customerledger/ledger.html'

    def get_opening_balances(self, filter_options):
        opening_balances = {}
        customer_balances = OpeningBalance.objects.select_related(
            'customer', 'customer__company'
        ).filter(year=self.get_year(), customer__company=self.get_company())
        if filter_options.get('customer__number'):
            customer_balances = customer_balances.filter(customer__number=filter_options['customer__number'])
        if filter_options.get('customer__number__gte'):
            customer_balances = customer_balances.filter(customer__number__gte=filter_options['customer__number__gte'])
        if filter_options.get('customer__number__lte'):
            customer_balances = customer_balances.filter(customer__number__lte=filter_options['customer__number__lte'])
        for opening_balance in customer_balances:
            opening_balances[opening_balance.customer] = {
                'lines': [],
                'total_value': opening_balance.amount,
                'balance': opening_balance.amount,
                'incoming_balance': opening_balance.amount,
                'incoming_value': 0,
                'initial_debit': 0,
                'initial_credit': 0,
                'initial_balance': 0,
                'debit': 0,
                'credit': 0,
                'movement': 0,
                'movement_debit': 0,
                'movement_credit': 0,
            }
        return opening_balances

    def get_filter_options(self, company):
        filter_options = {'customer__company': company}
        start_date = end_date = None

        from_date = self.request.GET.get('from_date', None)
        to_date = self.request.GET.get('to_date', None)
        from_period = self.request.GET.get('from_period', None)
        to_period = self.request.GET.get('to_period', None)
        from_customer = self.request.GET.get('from_customer', None)
        to_customer = self.request.GET.get('to_customer', None)
        if from_date:
            start_date = datetime.strptime(from_date, '%Y-%m-%d').date()
        if to_date:
            end_date = datetime.strptime(to_date, '%Y-%m-%d').date()
            filter_options['date__lte'] = end_date
        if from_period:
            filter_options['period__gte'] = from_period
        if to_period:
            filter_options['period__lt'] = to_period
        if from_customer == to_customer:
            filter_options['customer__number'] = from_customer
        else:
            if from_customer:
                filter_options['customer__number__gte'] = from_customer
            if to_customer:
                filter_options['customer__number__lte'] = to_customer

        return filter_options, start_date, end_date

    def get_ledgers(self, filter_options):
        return Ledger.objects.select_related(
            'period', 'customer', 'customer__company'
        ).prefetch_related(
            'content_object'
        ).filter(
            **filter_options
        ).filter(
            period__period_year=self.get_year()
        ).order_by(
            'date', 'created_at'
        )

    # noinspection PyMethodMayBeStatic
    def include_ledger_line(self, ledger, start_date):
        if not start_date:
            return True
        if ledger.date >= start_date:
            return True
        return False

    def get_customer_ledger(self, filter_options, start_date):
        customer_ledgers = self.get_opening_balances(filter_options=filter_options)
        ledgers = self.get_ledgers(filter_options)
        for ledger in ledgers:
            line_amount = 0
            if ledger.amount:
                line_amount = ledger.amount
            debit_amount = line_amount if line_amount >= 0 else 0
            credit_amount = line_amount * -1 if line_amount < 0 else 0

            ledger_data = {'line': ledger, 'value': 0, 'initial_balance': 0, 'balance': 0,
                           'quantity_balance': 0, 'movement': 0, 'incoming_balance': 0,
                           'incoming_value': 0, 'credit_amount': credit_amount,
                           'debit_amount': debit_amount}

            if self.include_ledger_line(ledger, start_date):
                if ledger.customer in customer_ledgers:
                    balance = customer_ledgers[ledger.customer]['balance']
                    balance = line_amount + balance
                    ledger_data['value'] = line_amount
                    ledger_data['balance'] = balance

                    customer_ledgers[ledger.customer]['lines'].append(ledger_data)
                    customer_ledgers[ledger.customer]['total_value'] += line_amount
                    customer_ledgers[ledger.customer]['balance'] = balance
                    customer_ledgers[ledger.customer]['movement'] += line_amount
                    customer_ledgers[ledger.customer]['movement_debit'] += debit_amount
                    customer_ledgers[ledger.customer]['movement_credit'] += credit_amount
                    customer_ledgers[ledger.customer]['debit'] += debit_amount
                    customer_ledgers[ledger.customer]['credit'] += credit_amount
                else:
                    ledger_data['balance'] = line_amount
                    ledger_data['value'] = line_amount
                    customer_ledgers[ledger.customer] = {'lines': [ledger_data],
                                                         'total_value': line_amount,
                                                         'balance': line_amount,
                                                         'movement': line_amount,
                                                         'movement_debit': debit_amount,
                                                         'movement_credit': credit_amount,
                                                         'debit': debit_amount,
                                                         'credit': credit_amount
                                                         }
            else:
                if ledger.customer in customer_ledgers:
                    customer_ledgers[ledger.customer]['incoming_value'] += line_amount
                    customer_ledgers[ledger.customer]['total_value'] += line_amount
                    customer_ledgers[ledger.customer]['balance'] += line_amount
                    customer_ledgers[ledger.customer]['initial_debit'] += debit_amount
                    customer_ledgers[ledger.customer]['initial_credit'] += credit_amount
                    customer_ledgers[ledger.customer]['initial_balance'] += line_amount
                    customer_ledgers[ledger.customer]['debit'] += debit_amount
                    customer_ledgers[ledger.customer]['credit'] += credit_amount
                else:
                    ledger_data['initial_balance'] = line_amount
                    ledger_data['incoming_value'] = line_amount
                    customer_ledgers[ledger.customer] = {'lines': [],
                                                         'total_value': line_amount,
                                                         'balance': line_amount,
                                                         'incoming_value': line_amount,
                                                         'initial_debit': debit_amount,
                                                         'initial_credit': credit_amount,
                                                         'initial_balance': line_amount,
                                                         'debit': debit_amount,
                                                         'credit': credit_amount,
                                                         'movement': 0,
                                                         'movement_debit': 0,
                                                         'movement_credit': 0,
                                                         }
        return customer_ledgers

    def get_context_data(self, **kwargs):
        context = super(GenerateLedgerView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        company = self.get_company()
        filter_options, start_date, end_date = self.get_filter_options(company)
        context['customer_ledgers'] = self.get_customer_ledger(filter_options, start_date)
        context['start_date'] = start_date
        context['end_date'] = end_date
        context['company'] = company
        return context
