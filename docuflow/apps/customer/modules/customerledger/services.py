from docuflow.apps.sales.models import Invoice


class CustomerLedger(object):

    def __init__(self, customer):
        self.customer = customer
        self.lines = []

    def get_customer_invoices(self):
        invoices = Invoice.objects.filter(customer=self.customer)