from django.apps import AppConfig


class CustomerledgerConfig(AppConfig):
    name = 'docuflow.apps.customer.modules.customerledger'
