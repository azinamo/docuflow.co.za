from django import forms


class LedgerSearchForm(forms.Form):
    from_date = forms.DateField(required=True, label='From Date')
    to_date = forms.DateField(required=True, label='To Date')
    is_potrait = forms.BooleanField(required=False, label='Potrait')
    is_landscape = forms.BooleanField(required=False, label='Landscape')
    by_periods = forms.BooleanField(required=False, label='Break by period')
