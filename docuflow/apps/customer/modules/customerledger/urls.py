from django.urls import path

from . import views

urlpatterns = [
    path('', views.CustomerLedgerView.as_view(), name='customer_ledger'),
    path('generate/', views.GenerateLedgerView.as_view(), name='generate_customer_ledger'),
]
