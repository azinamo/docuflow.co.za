from decimal import Decimal

from django import forms
from django.conf import settings
from django.db.models import Q, Subquery, DecimalField
from django.db.models.functions import Coalesce
from django_countries.fields import CountryField

from docuflow.apps.common.enums import SaProvince
from docuflow.apps.period.models import Period
from .enums import AddressType
from .fields import CustomerModelChoiceField
from .models import Customer, Ledger


class CustomerForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.request = kwargs.pop('request')
        super(CustomerForm, self).__init__(*args, **kwargs)
        self.fields['delivery_method'].queryset = self.fields['delivery_method'].queryset.filter(company=self.company)
        self.fields['delivery_term'].queryset = self.fields['delivery_term'].queryset.filter(company=self.company)
        self.fields['default_account'].queryset = self.fields['default_account'].queryset.filter(company=self.company)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.company)
        self.fields['default_account'].empty_label = None
        if self.instance and self.instance.postal_address:
            if not self.instance.postal_address.is_south_africa:
                self.fields['country_province'].initial = self.instance.postal_address.province

    df_number = forms.CharField(required=False)
    postal_province = forms.ChoiceField(required=False, choices=SaProvince.choices())
    postal_country = CountryField(default=settings.COUNTRY_CODE).formfield()
    country_province = forms.CharField(required=False)

    statement_days = forms.ChoiceField(choices=((30, '30'), (60, '60'), (90, '90')), label='Days')

    class Meta:
        model = Customer
        fields = '__all__'

        widgets = {
            'delivery_term': forms.Select(attrs={'class': 'chosen-select'}),
            'delivery_method': forms.Select(attrs={'class': 'chosen-select'}),
            'default_account': forms.Select(attrs={'class': 'chosen-select'}),
            'available_credit': forms.HiddenInput()
        }

    def save(self, commit=True):
        customer = super(CustomerForm, self).save(commit=False)
        customer.company = self.company

        discount_terms = {}
        postal_address = {}
        delivery_address = {}
        printing_setup = {}
        for k, v in self.request.POST.items():
            if v != '':
                if k[0:7] == 'postal_':
                    postal_key = k[7:]
                    postal_address[postal_key] = v
                if k[0:9] == 'delivery_':
                    delivery_key = k[9:]
                    delivery_address[delivery_key] = v
                if k[0:15] == 'discount_price_':
                    discount_key = k[15:]
                    is_discount = self.request.POST.get(f"is_discount_{discount_key}", False)
                    if is_discount == 'on':
                        discount_terms[discount_key] = v
                if k[0:15] == 'discount_price_':
                    discount_key = k[15:]
                    is_discount = self.request.POST.get(f"is_discount_{discount_key}", False)
                    if is_discount == 'on':
                        discount_terms[discount_key] = {'discount': v, 'is_discounted': True}
                    else:
                        discount_terms[discount_key] = {'discount': 0, 'is_discounted': False}
                if k[0:14] == 'print_records_':
                    print_key = k[14:]
                    customer_print = self.request.POST.get(f"print_customers_{print_key}", 'print')
                    copy = self.request.POST.get(f"copy_{print_key}", 0)
                    printing_setup[print_key] = {'customer': customer_print, 'copy': copy, 'our_records': v}

        customer.discount_terms = discount_terms
        customer.printing_setup = printing_setup
        customer.save()

        country_province = self.cleaned_data['country_province']
        if country_province:
            postal_address['province'] = country_province

        customer.save_address(delivery_address, AddressType.DELIVERY)
        customer.save_address(postal_address, AddressType.POSTAL)
        return customer


class ImportForm(forms.Form):
    csv_file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)')

    def clean(self):
        cleaned_data = self.cleaned_data
        csv_file = cleaned_data["csv_file"]
        file_types = ['.xls', '.csv', 'xlsx']
        if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx')):
            self.add_error('csv_file', f"File is not in {','.join(file_types)} type")

        if csv_file.multiple_chunks():
            size = csv_file.size / (1000 * 1000)
            self.add_error('csv_file', "Uploaded file is too big (%.2f MB)." % size)


class StatementForm(forms.Form):

    WITH_EMAILS = 'with'
    WITHOUT_EMAILS = 'without'
    ALL_CUSTOMERS = 'all'

    EMAIL_CHOICES = (
        (WITH_EMAILS, 'With Emails'),
        (WITHOUT_EMAILS, 'Without Emails'),
        (ALL_CUSTOMERS, 'All')
    )

    def __init__(self, *args, **kwargs):
        if 'company' in kwargs:
            self.company = kwargs.pop('company')
        if 'year' in kwargs:
            self.year = kwargs.pop('year')
        super(StatementForm, self).__init__(*args, **kwargs)
        self.fields['from_customer'].queryset = Customer.objects.filter(company=self.company).order_by('number')
        self.fields['to_customer'].queryset = Customer.objects.filter(company=self.company).order_by('-number')
        self.fields['to_period'].queryset = Period.objects.filter(period_year=self.year).order_by('-period')
        self.fields['from_period'].queryset = Period.objects.filter(period_year=self.year).order_by('period')

    movement_and_outstanding_balance = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    movement_and_no_outstanding_balance = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    only_outstanding_balance = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    dormant_for_period = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    customer_emails = forms.ChoiceField(required=False, choices=EMAIL_CHOICES, widget=forms.RadioSelect())
    from_customer = CustomerModelChoiceField(queryset=None, empty_label=None)
    to_customer = CustomerModelChoiceField(queryset=None, empty_label=None)
    to_period = forms.ModelChoiceField(queryset=None, empty_label=None)
    from_period = forms.ModelChoiceField(queryset=None, empty_label=None)

    def execute(self):
        current_filter = Q()
        q = Q()
        q.add(Q(company=self.company), Q.AND)
        if self.cleaned_data['to_period']:
            current_filter.add(Q(date__lte=self.cleaned_data['to_period'].to_date), Q.AND)
        if self.cleaned_data['from_period']:
            current_filter.add(Q(date__gte=self.cleaned_data['from_period'].from_date), Q.AND)

        if self.cleaned_data['from_customer']:
            q.add(Q(name__gte=self.cleaned_data['from_customer'].name), Q.AND)
        if self.cleaned_data['to_customer']:
            q.add(Q(name__lte=self.cleaned_data['to_customer'].name), Q.AND)

        if self.cleaned_data['customer_emails'] == self.WITH_EMAILS:
            q.add(~(Q(email='') | Q(email='None') | Q(email__isnull=True)), Q.AND)
        if self.cleaned_data['customer_emails'] == self.WITHOUT_EMAILS:
            q.add(Q(email=''), Q.AND)

        if self.cleaned_data['movement_and_outstanding_balance']:
            q.add(~Q(movement_total=0), Q.AND)
            q.add(Q(balance__gt=0), Q.AND)
        if self.cleaned_data['movement_and_no_outstanding_balance']:
            q.add(~Q(movement_total=0), Q.AND)
            q.add(Q(balance=0), Q.AND)
        if self.cleaned_data['only_outstanding_balance']:
            q.add(Q(balance__gt=0), Q.AND)
        if self.cleaned_data['dormant_for_period']:
             pass
        # customer_filter.add(movement_filter, models.Q.AND)
        ledger_query = Ledger.objects.customer_balance(end_date=self.cleaned_data['to_period'].to_date, year=self.year)
        ledger_movement_query = Ledger.objects.customer_balance(
            start_date=self.cleaned_data['from_period'].from_date, end_date=self.cleaned_data['to_period'].to_date
        )
        return Customer.objects.annotate(
            balance=Coalesce(Subquery(ledger_query[:1], output_field=DecimalField()), Decimal('0')),
            movement_total=Coalesce(Subquery(ledger_movement_query[:1], output_field=DecimalField()), Decimal('0'))
        ).filter(
            q
        ).values(
            'id', 'name', 'number', 'email', 'balance', 'movement_total'
        ).order_by('name')


class CustomerStatementForm(forms.Form):

    def __init__(self, *args, **kwargs):
        if 'company' in kwargs:
            self.company = kwargs.pop('company')
        if 'year' in kwargs:
            self.year = kwargs.pop('year')
        super(CustomerStatementForm, self).__init__(*args, **kwargs)
        self.fields['customer'].queryset = Customer.objects.filter(company=self.company)
        self.fields['to_period'].queryset = Period.objects.filter(period_year__company=self.company)
        self.fields['from_period'].queryset = Period.objects.filter(period_year__company=self.company)

    movement_outstanding_balance = forms.BooleanField(required=False, label='Movement and Outstanding balance',
                                                      widget=forms.CheckboxInput())
    movement_no_outstanding_balance = forms.BooleanField(required=False, label='Movement and No Outstanding balance',
                                                         widget=forms.CheckboxInput())
    only_outstanding_balance = forms.BooleanField(required=False, label='Only Outstanding balance',
                                                  widget=forms.CheckboxInput())
    dormant_for_period = forms.BooleanField(required=False, label='Dormant for period', widget=forms.CheckboxInput())
    with_emails = forms.BooleanField(required=False, label='Customers with emails', widget=forms.CheckboxInput())
    without_emails = forms.BooleanField(required=False, label='Customers without emails', widget=forms.CheckboxInput())
    customer = CustomerModelChoiceField(queryset=None, empty_label=None)
    to_period = forms.ModelChoiceField(queryset=None, empty_label=None)
    from_period = forms.ModelChoiceField(queryset=None, empty_label=None)


class AgeAnalysisForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super(AgeAnalysisForm, self).__init__(*args, **kwargs)
        self.fields['customer'].queryset = Customer.objects.filter(company=self.company)
        self.fields['period'].queryset = Period.objects.filter(period_year=self.year).order_by('-period')
        self.fields['period'].empty_label = None

    period = forms.ModelChoiceField(required=False, label='Period', queryset=None, widget=forms.Select(attrs={'class': 'chosen-select'}))
    customer = CustomerModelChoiceField(required=False, label='Customer', queryset=None, widget=forms.Select(attrs={'class': 'chosen-select'}))

    def get_current_period(self, company, date):
        return Period.objects.filter(
            from_date__lte=date, to_date__gte=date, company=company
        ).order_by(
            '-period'
        ).first()

    def execute(self):
        period = self.cleaned_data['period']

        totals = {
            'unallocated_total': 0,
            'one_twenty_or_more_days': 0,
            'ninety_days': 0,
            'sixty_days': 0,
            'thirty_days': 0,
            'current': 0,
            'total': 0
        }
        customer_age_analysis = Customer.objects.get_age_analysis(period=period, customer=self.cleaned_data['customer'])
        for customer_age in customer_age_analysis:
            totals['unallocated_total'] += customer_age.unallocated if customer_age.unallocated else 0
            totals['one_twenty_or_more_days'] += customer_age.one_twenty_day if customer_age.one_twenty_day else 0
            totals['ninety_days'] += customer_age.ninety_day if customer_age.ninety_day else 0
            totals['sixty_days'] += customer_age.sixty_day if customer_age.sixty_day else 0
            totals['thirty_days'] += customer_age.thirty_day if customer_age.thirty_day else 0
            totals['current'] += customer_age.current if customer_age.current else 0
            totals['total'] += customer_age.balance if customer_age.balance else 0
        return customer_age_analysis, totals
