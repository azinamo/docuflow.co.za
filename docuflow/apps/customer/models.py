import logging
import pprint
from calendar import monthrange
from datetime import timedelta, datetime
from decimal import Decimal

from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q, F, Subquery
from django.db.models.functions import Coalesce
from django_countries.fields import CountryField
from enumfields import EnumIntegerField
from jsonfield import JSONField
from safedelete.models import SafeDeleteModel, SOFT_DELETE

from docuflow.apps.common.behaviours import Timestampable, Agable
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Account
from docuflow.apps.company.enums import SubLedgerModule
from docuflow.apps.period.models import Period
from docuflow.apps.sales.models import Receipt, Invoice
from . import enums
from . import managers
from .enums import PaymentTerm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class DeliveryTerm(models.Model):
    company = models.ForeignKey('company.Company', related_name='delivery_terms', null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


class CustomerManager(models.Manager):

    def with_year_balance(self, year):
        return self.filter(
            ledgers__period__period_year=year
        ).annotate(
            total=Coalesce(models.Sum('ledgers__amount', output_field=models.DecimalField()), Decimal('0')),
        )

    def with_balance(self):
        return self.annotate(
            balance_amount=Coalesce(models.Sum('ledgers__amount', output_field=models.DecimalField()), Decimal('0')),
            available_amount=Coalesce(models.F('credit_limit'), Decimal('0')) - models.F('balance_amount')
        )

    def create_cash_customer(self, company, name, **kwargs):
        return self.create(name=name, company=company, payment_term=PaymentTerm.CASH_CUSTOMER, is_active=True, **kwargs)

    def slice_period(self, periods, start, offset=None):
        try:
            period_page = periods[start:offset]
            period = period_page[0]
            if not period:
                return None
        except IndexError:
            return None
        return period

    def get_age_balances(self, customer=None, year=None, current_period=None):
        q = Q()
        if year:
            q.add(Q(company=year.company), Q.AND)
        elif current_period:
            q.add(Q(company=current_period.period_year.company), Q.AND)
        if customer:
            q.add(Q(id=customer.id), Q.AND)
        else:
            q.add(~Q(balance=0), Q.AND)

        periods = Period.objects.get_ranges(year, current_period)
        thirty_day_period = self.slice_period(periods, 1, 2)
        sixty_day_period = self.slice_period(periods, 2, 3)
        ninety_day_period = self.slice_period(periods, 3, 4)
        one_twenty_day_period = self.slice_period(periods, 4)

        current_receipt = Receipt.objects.customer_open_total(current_period)
        thirty_day_receipt = Receipt.objects.customer_open_total(thirty_day_period)
        sixty_day_receipt = Receipt.objects.customer_open_total(sixty_day_period)
        ninety_day_receipt = Receipt.objects.customer_open_total(ninety_day_period)
        one_twenty_day_receipt = Receipt.objects.customer_open_total(one_twenty_day_period, True)

        current_invoice = Invoice.objects.customer_open_total(current_period)
        thirty_day_invoice = Invoice.objects.customer_open_total(thirty_day_period)
        sixty_day_invoice = Invoice.objects.customer_open_total(sixty_day_period)
        ninety_day_invoice = Invoice.objects.customer_open_total(ninety_day_period)
        one_twenty_day_invoice = Invoice.objects.customer_open_total(one_twenty_day_period, True)
        return self.annotate(
            current_receipt_total=Subquery(current_receipt[:1]),
            current_invoice_total=Subquery(current_invoice[:1]),
            thirty_day_receipt=Subquery(thirty_day_receipt[:1]),
            thirty_day_invoice=Subquery(thirty_day_invoice[:1]),
            sixty_day_receipt=Subquery(sixty_day_receipt[:1]),
            sixty_day_invoice=Subquery(sixty_day_invoice[:1]),
            ninety_day_receipt=Subquery(ninety_day_receipt[:1]),
            ninety_day_invoice=Subquery(ninety_day_invoice[:1]),
            one_twenty_day_receipt=Subquery(one_twenty_day_receipt[:1]),
            one_twenty_day_invoice=Subquery(one_twenty_day_invoice[:1]),
            current_total=Coalesce(F('current_invoice_total'), Decimal('0')) + Coalesce(F('current_receipt_total'), Decimal('0')),
            thirty_day_total=Coalesce(F('thirty_day_invoice'), Decimal('0')) + Coalesce(F('thirty_day_receipt'), Decimal('0')),
            sixty_day_total=Coalesce(F('sixty_day_invoice'), Decimal('0')) + Coalesce(F('sixty_day_receipt'), Decimal('0')),
            ninety_day_total=Coalesce(F('ninety_day_invoice'), Decimal('0')) + Coalesce(F('ninety_day_receipt'), Decimal('0')),
            one_twenty_day_total=Coalesce(F('one_twenty_day_invoice'), Decimal('0')) + Coalesce(F('one_twenty_day_receipt'), Decimal('0')),
            receipt_total=Coalesce(F('current_receipt_total'), Decimal('0')) + Coalesce(F('thirty_day_receipt'), Decimal('0')) +
                          Coalesce(F('sixty_day_receipt'), Decimal('0')) + Coalesce(F('ninety_day_receipt'), Decimal('0')) +
                          Coalesce(F('one_twenty_day_receipt'), Decimal('0')),
            balance=F('current_total') + F('thirty_day_total') + F('sixty_day_total') + F('ninety_day_total') + F('one_twenty_day_total')
        ).filter(
            q
        ).values(
            'id', 'name', 'number', 'balance',
            'current_invoice_total', 'thirty_day_invoice', 'sixty_day_invoice', 'ninety_day_invoice',
            'one_twenty_day_invoice', 'current_total', 'thirty_day_total', 'sixty_day_total', 'ninety_day_total',
            'one_twenty_day_total', 'receipt_total'
        ).order_by('name')

    def get_age_analysis(self, customer=None, period=None):
        age_analysis_qs = AgeAnalysis.objects.select_related(
            'period', 'customer'
        ).filter(period=period).exclude(balance=0)
        if customer:
            age_analysis_qs = age_analysis_qs.filter(customer=customer)
        return age_analysis_qs.order_by('customer__name')


class Customer(SafeDeleteModel, Timestampable):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey('company.Company', related_name='customers', null=True, blank=True, on_delete=models.CASCADE)
    number = models.CharField(max_length=255, blank=True, verbose_name='Customer Number', db_index=True)
    customer_id = models.PositiveIntegerField(null=True, blank=True)
    name = models.CharField(max_length=255, default='', db_index=True)
    vat_number = models.CharField(max_length=255, blank=True, default='')
    email = models.EmailField(null=True, blank=True)
    accepted_email_marketing = models.BooleanField(default=False, verbose_name='Customer accept Email marketing')
    phone_number = models.CharField(max_length=255, blank=True, default='')
    cell_number = models.CharField(max_length=255, blank=True, default='')
    accepted_sms_marketing = models.BooleanField(default=False, verbose_name='Customer accept SMS marketing')
    contact_person = models.CharField(max_length=255, blank=True, default='')
    account_person = models.CharField(max_length=255, blank=True, default='')
    credit_limit = models.DecimalField(max_digits=12, decimal_places=0, blank=True, null=True)
    available_credit = models.DecimalField(max_digits=12, decimal_places=0, null=True, blank=True,
                                           verbose_name='Credit Available')
    is_active = models.BooleanField(default=False)
    payment_term = EnumIntegerField(enums.PaymentTerm, null=True, blank=True)
    days = models.PositiveIntegerField(null=True, blank=True)
    is_debit_order_client = models.BooleanField(default=False, verbose_name='Debit Order Client')
    delivery_term = models.ForeignKey(DeliveryTerm, related_name='customer_delivery_terms', null=True, blank=True, on_delete=models.CASCADE)
    delivery_method = models.ForeignKey('company.DeliveryMethod', related_name='customer_delivery_methods', null=True, blank=True, on_delete=models.CASCADE)
    document_layout = EnumIntegerField(enums.DocumentLayout, verbose_name='Specific Document Layout', null=True, blank=True)
    is_statement_discount = models.BooleanField(default=False)
    statement_discount = models.DecimalField(max_digits=5, decimal_places=2, default=0, blank=True, null=True)
    is_line_discount = models.BooleanField(default=False)
    line_discount = models.DecimalField(max_digits=5, decimal_places=2, default=0, blank=True, null=True)
    is_invoice_discount = models.BooleanField(default=False)
    invoice_discount = models.DecimalField(max_digits=5, decimal_places=2, default=0, blank=True, null=True)
    discount_days = models.PositiveIntegerField(null=True, blank=True)
    is_interest_charged = models.BooleanField(default=False, verbose_name='Interest charged')
    interest = models.DecimalField(decimal_places=2, max_digits=4, null=True, blank=True)
    days_unpaid = models.PositiveIntegerField(null=True, blank=True)
    printing_setup = JSONField(blank=True, null=True, verbose_name='Printing Setup')
    statistics = JSONField(blank=True, null=True, verbose_name='Statistics')
    is_price_on_delivery = models.BooleanField(default=False, verbose_name='Show Price on delivery')
    is_default = models.BooleanField(default=False, verbose_name='Default Customer')
    external_company_number = models.CharField(max_length=50, blank=True)
    df_number = models.CharField(max_length=50, blank=True, default='')
    supplier = models.ForeignKey('supplier.Supplier', blank=True, null=True, related_name="customers", on_delete=models.DO_NOTHING)
    journals = GenericRelation('journals.Journal', related_query_name='customer')
    default_account = models.ForeignKey(
        'company.Account', blank=True, null=True, related_name="customer_default_accounts", on_delete=models.DO_NOTHING,
        limit_choices_to=models.Q(sub_ledger=SubLedgerModule.CUSTOMER))
    salesman = models.ForeignKey('sales.Salesman', blank=True, null=True, related_name="customers", on_delete=models.DO_NOTHING)

    objects = CustomerManager.from_queryset(managers.CustomerQuerySet)()

    class Meta:
        ordering = ('number', 'name', )

    def __str__(self):
        return f"{self.company.slug}{self.number}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.number:
            filter_options = {'company_id': self.company_id}
            number = self.generate_customer_number(1, filter_options)
            pre = self.name[0:1] if self.name else ''
            self.number = f"{pre.upper()}{str(number).rjust(4, '0')}"
        return super(Customer, self).save(*args, **kwargs)

    def generate_customer_number(self, counter, filter_options):
        try:
            customer_count = Customer.objects.filter(**filter_options).count()
            return customer_count + 1
        except:
            return counter

    def save_address(self, address, address_type):
        address_line_1 = address.get('street', None)
        if len(address) > 0 and address_line_1:
            address_object = Address.objects.filter(address_type=address_type, customer=self).first()
            if not address_object:
                address_object = Address()
                address_object.customer = self
            city = address.get('city', '')
            province = address.get('province', '')
            address_object.address_type = address_type
            address_object.postal_code = address.get('postal_code', '')
            address_object.country = address.get('country', None)
            address_object.province = province if province is not None else ''
            address_object.city = city if city is not None else ''
            address_object.address_line_1 = address_line_1
            address_object.save()

    def max_discount_date(self):
        return datetime.now().date() + timedelta(self.days)

    def calculate_due_date(self, invoice_date):
        if self.payment_term == enums.PaymentTerm.INVOICE_DATE:
            if self.days:
                due_date = invoice_date + timedelta(self.days)
                return due_date.strftime('%Y-%m-%d')
        elif self.payment_term == enums.PaymentTerm.STATEMENT_DATE:
            if self.days:
                due_date = invoice_date + timedelta(self.days)
                month_range = monthrange(int(due_date.strftime('%Y')), int(due_date.strftime('%m')))
                due_date = due_date + timedelta(month_range[1] - int(due_date.strftime('%d')))
                return due_date.strftime('%Y-%m-%d')
        return invoice_date

    def update_customer_credit(self):
        invoice_amount = self.sales_invoice_related.aggregate(total_invoiced=models.Sum('total_amount'))
        available_credit = Decimal(0)
        if self.available_credit:
            available_credit = self.available_credit
        if invoice_amount:
            available_credit -= Decimal(invoice_amount)
        self.available_credit = available_credit
        self.save()

    @property
    def is_cash(self):
        return self.payment_term == enums.PaymentTerm.CASH_CUSTOMER or self.is_default

    @property
    def customer_discount(self):
        if self.is_invoice_discount:
            return self.invoice_discount or 0
        return 0
        # if self.discount_terms:
        #     invoice_discount = self.discount_terms.get('invoice')
        #     if invoice_discount:
        #         is_discounted = invoice_discount.get('is_discounted', False)
        #         discount_percentage = float(invoice_discount.get('discount', False))
        #         if is_discounted and discount_percentage:
        #             return discount_percentage
        # return 0

    @property
    def customer_line_discount(self):
        if self.is_line_discount:
            return self.line_discount or 0
        # if self.discount_terms:
        #     line_discount = self.discount_terms.get('line')
        #     if line_discount:
        #         is_discounted = line_discount.get('is_discounted', False)
        #         discount_percentage = float(line_discount.get('discount', False))
        #         if is_discounted and discount_percentage:
        #             return discount_percentage
        # return 0

    @property
    def customer_statement_discount(self):
        # TODO - Breakdown the discount into separate fields
        if self.is_statement_discount:
            return self.statement_discount or 0
        # if self.discount_terms:
        #     statement_discount = self.discount_terms.get('statement')
        #     if statement_discount:
        #         is_discounted = statement_discount.get('is_discounted', False)
        #         discount_percentage = Decimal(statement_discount.get('discount', 0))
        #         if is_discounted and discount_percentage:
        #             return discount_percentage
        # return 0

    @property
    def delivery_address(self):
        return self.addresses.filter(address_type=enums.AddressType.DELIVERY).first()

    @property
    def postal_address(self):
        return self.addresses.filter(address_type=enums.AddressType.POSTAL).first()

    @property
    def is_statement_days(self):
        return self.payment_term == enums.PaymentTerm.STATEMENT_DATE

    @property
    def total_invoiced(self):
        ledgers_total = Ledger.objects.filter(
            status=enums.LedgerStatus.COMPLETE, customer=self
        ).aggregate(total_amount=models.Sum('amount'))
        total_amount = 0
        if ledgers_total and ledgers_total['total_amount']:
            total_amount = ledgers_total['total_amount']
        return Decimal(total_amount)

    @property
    def calculate_available(self):
        credit_limit = 0
        if self.credit_limit:
            credit_limit = self.credit_limit
        return credit_limit - self.total_invoiced

    @property
    def payment_term_name(self):
        return self.payment_term


class Address(models.Model):

    address_type = EnumIntegerField(enums.AddressType, default=enums.AddressType.DELIVERY)
    customer = models.ForeignKey(Customer, related_name='addresses', on_delete=models.CASCADE)
    address_line_1 = models.CharField(max_length=255, verbose_name='Street')
    address_line_2 = models.CharField(max_length=255, blank=True, verbose_name='Suburb', default='')
    address_line_3 = models.CharField(max_length=255, blank=True, default='')
    city = models.CharField(max_length=255, blank=True, default='')
    province = models.CharField(max_length=255, blank=True, default='')
    postal_code = models.CharField(max_length=255, blank=True, default='')
    country = CountryField()
    latitude = models.DecimalField(default=False, decimal_places=10, max_digits=15, blank=True, null=True)
    longitude = models.DecimalField(default=False, decimal_places=10, max_digits=15, blank=True, null=True)
    is_default = models.BooleanField(default=False)

    @property
    def province_name(self):
        return self.province

    @property
    def address_type_name(self):
        return str(self.address_type)

    @property
    def is_south_africa(self):
        return self.country == 'ZA'


class LedgerQuerySet(models.QuerySet):

    def paid(self):
        return self.filter(status=5)

    def open(self):
        return self.exclude(models.Q(balance=0) | models.Q(balance__isnull=True))

    def for_customer(self, customer_id):
        return self.filter(customer_id=customer_id)

    def customer_opening_balance(self, customer_id, year, period):
        return self.for_customer(customer_id).filter(
            period__period__lt=period, period__period_year=year
        ).aggregate(
            total=models.Sum('amount')
        )

    def with_available_balance(self):
        return self.annotate(payment_amount=F('balance') * -1)

    def with_available_payments(self, journal_filters):
        return self.with_available_balance().filter(
            **journal_filters
        ).filter(journal_line__isnull=True).exclude(payment_amount=0)

    def totals(self, **kwargs):
        q = models.Q()
        filters = {}
        if kwargs.get('customer_emails') == 'with':
            q.add(~(models.Q(customer__email='') | models.Q(customer__email='None') | models.Q(customer__email__isnull=True)), q.AND)
        if kwargs.get('customer_emails') == 'without':
            q.add(models.Q(customer__email=''), q.AND)

        movement_filter = models.Q()
        if kwargs.get('movement_and_outstanding_balance'):
            movement_filter.add(models.Q(total_balance__gt=0), q.OR)
        if kwargs.get('movement_and_no_outstanding_balance'):
            movement_filter.add(~(models.Q(total_balance__gt=0) | models.Q(total_balance__lt=0)), q.OR)
        if kwargs.get('only_outstanding_balance'):
            movement_filter.add(models.Q(total_balance__gt=0), q.OR)
        if kwargs.get('dormant_for_period'):
            pass
        q.add(movement_filter, q.AND)
        return self.select_related('customer').aggregate(
            total_balance=models.Sum('amount')
        ).filter(
            **filters
        ).filter(q)

    def balances_report(self, **kwargs):
        q = models.Q()
        filters = {}
        if kwargs.get('to_period'):
            q.add(models.Q(date__lte=kwargs.get('to_period').to_date), models.Q.AND)
        if kwargs.get('from_period'):
            q.add(models.Q(date__gte=kwargs.get('from_period').from_date), models.Q.AND)

        return self.select_related('customer').annotate(
            total_balance=models.Sum('amount')
        ).filter(
            **filters
        ).filter(q).exclude(total_balance=0).values(
            'customer_id', 'customer__name', 'total_balance', 'customer__email', 'customer__number'
        ).order_by('customer__name')

    def create_customer_ledger(self, invoice,  profile, is_pending=False):
        content_type = ContentType.objects.get_for_model(invoice)
        if content_type and invoice.customer:
            status = 0 if is_pending else 1
            ledger = self.filter(customer=invoice.customer, content_type=content_type, object_id=invoice.id).first()
            if ledger:
                ledger.status = 1
                ledger.save()
            else:
                ledger = self.create(
                    customer=invoice.customer,
                    content_type=content_type,
                    object_id=invoice.id,
                    amount=invoice.total_amount,
                    created_by=profile,
                    period=invoice.period,
                    date=invoice.invoice_date,
                    text=str(invoice),
                    status=status
                )
            return ledger

    def create_payment(self, ledger_id, receipt, amount):
        ledger = self.filter(pk=ledger_id).first()
        if ledger:
            logger.info(f"New Ledger Open amount {ledger.balance - amount}")
            ledger.balance = ledger.balance - amount
            ledger.save()

            return LedgerPayment.objects.create(
                ledger=ledger,
                payment=receipt,
                amount=amount
            )
        return None

    # noinspection PyMethodMayBeStatic
    def create_from_receipt(self, receipt, period, amount, date, profile, module=Module.SALES, status=enums.LedgerStatus.PENDING):
        content_type = ContentType.objects.filter(app_label='sales', model='receipt').first()
        if content_type and receipt.customer:
            return Ledger.objects.create(
                customer=receipt.customer,
                period=period,
                quantity=1,
                amount=amount,
                module=module,
                created_by=profile,
                date=date,
                status=status,
                text=str(receipt),
                content_type=content_type,
                object_id=receipt.id,
                description='Receipt',
                journal_number=receipt.reference
            )

    # noinspection PyMethodMayBeStatic
    def create_from_invoice(self, invoice, profile, status=enums.LedgerStatus.PENDING):
        content_type = ContentType.objects.get_for_model(invoice)
        journal_number = str(invoice.estimate) if invoice.estimate else ''
        if invoice.po_number:
            journal_number = invoice.po_number
        if not journal_number and invoice.delivery:
            journal_number = str(invoice.delivery) if invoice.delivery else ''
        if not journal_number and invoice.picking_slip:
            journal_number = str(invoice.picking_slip) if invoice.picking_slip else ''

        customer_ledger, _ = Ledger.objects.update_or_create(
            customer=invoice.customer,
            content_type=content_type,
            object_id=invoice.id,
            defaults={
                'amount': invoice.total_amount,
                'created_by': profile,
                'period': invoice.period,
                'date': invoice.invoice_date,
                'text': str(invoice),
                'status': status,
                'description': str(invoice.invoice_type),
                'journal_number': journal_number
            }
        )
        return customer_ledger

    def mark_invoice_as_completed(self, invoice, profile):
        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        customer_ledger = Ledger.objects.filter(
            customer=invoice.customer,
            content_type=content_type,
            object_id=invoice.id
        ).first()
        if customer_ledger:
            customer_ledger.status = enums.LedgerStatus.COMPLETE
            customer_ledger.save()
        else:
            self.create_from_invoice(invoice, profile, enums.LedgerStatus.COMPLETE)

    def mark_receipt_as_completed(self, receipt):
        content_type = ContentType.objects.filter(app_label='sales', model='receipt').first()
        customer_ledger = Ledger.objects.filter(
            customer=receipt.customer,
            content_type=content_type,
            object_id=receipt.id
        ).first()
        if customer_ledger:
            customer_ledger.status = enums.LedgerStatus.COMPLETE
            customer_ledger.save()

    def customer_balance(self, customer=None, start_date=None, end_date=None, year=None):
        query = self.filter(customer_id=models.OuterRef('id')).order_by()
        if customer:
            query = query.filter(customer=customer)
        if start_date and end_date:
            query = query.filter(date__gte=start_date, date__lte=end_date)
        elif start_date:
            query = query.filter(date__gte=start_date)
        elif end_date:
            query = query.filter(date__lte=end_date)
        return query.values_list(models.Func('amount', function='SUM'))


class Ledger(models.Model):
    # limit = models.Q(app_label='sales', model='movement') | \
    #          models.Q(app_label='inventory', model='inventorymovement') | \
    #          models.Q(app_label='inventory', model='inventorystockadjustment') | \
    #          models.Q(app_label='inventory', model='stockadjustment')

    customer = models.ForeignKey(Customer, related_name='ledgers', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', related_name='period_customer_ledgers', null=True, blank=True,
                               on_delete=models.DO_NOTHING)
    quantity = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    amount = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Value')
    balance = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    module = EnumIntegerField(Module, default=Module.SALES)
    content_type = models.ForeignKey(ContentType, null=True, blank=True, related_name='content_type_customers', on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    ledger_id = models.CharField(max_length=255, blank=True, default='')
    text = models.CharField(max_length=255, blank=True, default='')
    description = models.CharField(max_length=255, blank=True)
    journal_number = models.CharField(max_length=255, blank=True, default='')
    created_by = models.ForeignKey('accounts.Profile', related_name='created_customer_ledgers', on_delete=models.DO_NOTHING)
    journal_line = models.OneToOneField('journals.JournalLine', related_name='customer_ledger', null=True, blank=True,
                                        on_delete=models.CASCADE)
    date = models.DateField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = EnumIntegerField(enums.LedgerStatus, default=enums.LedgerStatus.COMPLETE)

    objects = LedgerQuerySet.as_manager()

    class Meta:
        unique_together = ('customer', 'content_type', 'object_id')
        ordering = ('date', )
        index_together = [
            ['customer', 'date']
        ]

    def __str__(self):
        if hasattr(self, 'journal'):
            return str(self.journal)
        return f"CL{self.ledger_id}"

    @property
    def module_name(self):
        return str(self.module)

    @property
    def is_pending(self):
        return self.status == enums.LedgerStatus.PENDING

    @staticmethod
    def generate_journal_number(company, year, counter=1):
        journal_number = counter
        result = Ledger.objects.filter(journal_number=journal_number, customer__company=company, period__period_year=year)
        if result:
            counter = counter + 1
            return Ledger.generate_journal_number(company, year, counter)
        else:
            return journal_number

    @property
    def linked_object(self):
        if self.content_type and self.object_id:
            return self.content_type.get_object_for_this_type(pk=self.object_id)
        return None

    @property
    def available_amount(self):
        return self.balance * -1 if self.balance else 0

    @property
    def debit(self):
        if self.amount and self.amount > 0:
            return self.amount
        return 0

    @property
    def credit(self):
        if self.amount and self.amount < 0:
            return self.amount * -1
        return 0

    @property
    def document_type(self):
        if not self.description:
            if self.content_type:
                if self.content_type.model == 'invoice':
                    return str(self.content_object.invoice_type)
                elif self.content_type.model == 'receipt':
                    return 'Receipt'
            else:
                return 'Tax Invoice'
        return self.description

    @classmethod
    def new(cls, receipt: Receipt, receipt_payment, profile):
        return cls.objects.create(
            customer=receipt.customer,
            amount=receipt_payment.amount * -1,
            text=str(receipt),
            date=receipt.date,
            period=receipt.period,
            created_by=profile,
            status=enums.LedgerStatus.PENDING,
            journal_number=str(receipt_payment.account),
            object_id=receipt_payment.id,
            content_type=ContentType.objects.get_for_model(receipt_payment)
        )

    @classmethod
    def create_from_invoice(cls, invoice, profile, status=enums.LedgerStatus.PENDING):
        content_type = ContentType.objects.get_for_model(invoice)
        journal_number = str(invoice.estimate) if invoice.estimate else ''
        if invoice.po_number:
            journal_number = invoice.po_number
        if not journal_number and invoice.delivery:
            journal_number = str(invoice.delivery) if invoice.delivery else ''
        if not journal_number and invoice.picking_slip:
            journal_number = str(invoice.picking_slip) if invoice.picking_slip else ''

        ledger, _ = cls.objects.update_or_create(
            customer=invoice.customer,
            content_type=content_type,
            object_id=invoice.id,
            defaults={
                'amount': invoice.total_amount,
                'created_by': profile,
                'period': invoice.period,
                'date': invoice.invoice_date,
                'text': str(invoice),
                'status': status,
                'description': str(invoice.invoice_type),
                'journal_number': journal_number
            }
        )
        return ledger


class LedgerPayment(models.Model):
    payment = models.ForeignKey('sales.Receipt', related_name='customer_ledgers', on_delete=models.CASCADE)
    ledger = models.ForeignKey(Ledger, related_name='payments', on_delete=models.CASCADE)
    amount = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)

    @property
    def amount_paid(self):
        return self.amount * -1 if self.amount else 0


class Pricing(models.Model):
    customer = models.ForeignKey(Customer, related_name='pricings', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class FixedPricing(models.Model):
    pricing = models.ForeignKey(Pricing, related_name='fixed_prices', on_delete=models.CASCADE)
    inventory = models.ForeignKey('inventory.BranchInventory', related_name='fixed_prices', on_delete=models.CASCADE)
    price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Price Exc')
    price_including = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Price Inc')

    class Meta:
        unique_together = ('inventory', 'pricing')


class QuantityDiscount(models.Model):
    pricing = models.ForeignKey(Pricing, related_name='quantity_discounts', on_delete=models.CASCADE)
    inventory = models.ForeignKey('inventory.Inventory', related_name='customer_quantity_discounts', on_delete=models.CASCADE)
    quantity = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    discount = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)


class QuantityPricing(models.Model):
    pricing = models.ForeignKey(Pricing, related_name='quantity_prices', on_delete=models.CASCADE)
    inventory = models.OneToOneField('inventory.Inventory', related_name='customer_quantity_prices', on_delete=models.CASCADE)
    quantity = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)


class OpeningBalance(models.Model):
    customer = models.ForeignKey(Customer, related_name='opening_balances', on_delete=models.CASCADE)
    year = models.ForeignKey('period.Year', related_name='customer_opening_balances', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)


class AgeAnalysis(Agable):
    customer = models.ForeignKey(Customer, related_name='age_analysis', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('period', 'customer')


class History(models.Model):
    customer = models.ForeignKey(Customer, related_name='periods', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', related_name='customer_history', on_delete=models.PROTECT)
    cost_price = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    net_amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    vat_amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    total_amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)

    class Meta:
        ordering = ('period__period', )

    def add_invoice(self, invoice: Invoice):
        self.net_amount += invoice.net_amount
        self.vat_amount += invoice.vat_amount
        self.total_amount += invoice.total_amount
        self.save()

    def gross_profit(self):
        return self.net_amount - self.cost_price
