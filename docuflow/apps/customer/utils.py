import logging
from decimal import Decimal
from datetime import date

from django.urls import reverse_lazy

from .models import Customer
from docuflow.apps.common.widgets import DataAttributesSelect

logger = logging.getLogger(__name__)


def customer_select_data(company):
    customer_data = {'data-detail-url': {'': ''},
                     'data-payment-term': {'': ''},
                     'data-payment-days': {'': ''}
                     }

    customers = Customer.objects.filter(company=company).all().order_by('-is_default')
    for c in customers:
        customer_data['data-detail-url'][c.id] = reverse_lazy('customer_details', kwargs={'pk': c.id})
        customer_data['data-payment-term'][c.id] = c.payment_term
        customer_data['data-payment-days'][c.id] = c.days
    return DataAttributesSelect(choices=[('', '-----------')] + [(c.id, str(c)) for c in customers], data=customer_data)


def calculate_statement_discount(customer: Customer, amount, due_date, receipt_date):
    if isinstance(receipt_date, date) and isinstance(due_date, date):
        if customer.statement_discount:
            if due_date >= receipt_date:
                return amount * (customer.statement_discount / 100)
    return Decimal(0)
