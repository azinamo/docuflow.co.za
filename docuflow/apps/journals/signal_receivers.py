from django.dispatch import receiver
from django.db.models.signals import post_save

from docuflow.apps.journals.models import JournalLine
from docuflow.apps.journals.services import year_closing_transaction


@receiver(post_save, sender=JournalLine, dispatch_uid='year_closing_journal')
def year_closing(sender, instance, created, **kwargs):
    if created:
        year_closing_transaction(journal_line=instance)
