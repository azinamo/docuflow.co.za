from django.db import models


class JournalManager(models.Manager):

    use_in_migrations = True

    def _create_journal(self, company, year, period, date, module, journal_text, **extra_fields):
        if not company:
            raise ValueError('Company field is required')

        journal = self.model(company=company, year=year, period=period, date=date, module=module,
                             journal_text=journal_text, **extra_fields)

        journal.save(using=self._db)
        return journal

    def create_user(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_journal(email=email, password=password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_superuser=True')

        return self._create_journal(email=email, password=password, **extra_fields)

    def balance_sheet_lines(self, start_date, end_date, **filter_options):
        period_filter = models.Q()
        period_filter.add(models.Q(journal__date__gte=start_date), models.Q.AND)
        period_filter.add(models.Q(journal__date__lte=end_date), models.Q.AND)

        incoming_filter = models.Q()
        incoming_filter.add(models.Q(journal__date__lte=start_date), models.Q.AND)
        return self.annotate(
            total_incoming=models.Case(
                models.When(incoming_filter, then=models.Sum('debit') + models.Sum(models.F('credit') * -1)),
                output_field=models.DecimalField()
            ),
            period_total=models.Case(
                models.When(period_filter, then=models.Sum('debit') + models.Sum(models.F('credit') * -1)),
                output_field=models.DecimalField()
            )
        ).select_related(
            'account', 'journal', 'journal__period', 'vat_code', 'supplier', 'invoice', 'invoice__invoice_type', 'journal__year'
        ).prefetch_related(
            'object_items'
        ).filter(
            **filter_options
        ).order_by(
            '-account__report_type__account_type', 'account__report_type__sorting', 'account__code', 'journal__date', '-date', 'journal__period__period',
        ).values(
            'account_id', 'account__name', 'account__code', 'account__id', 'total_incoming', 'period_total',
            'account__report__name', 'account__report__slug'
        )
