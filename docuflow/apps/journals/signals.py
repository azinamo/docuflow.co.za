import django.dispatch

year_closing_transaction = django.dispatch.Signal(providing_args=['account', 'credit', 'debit'])