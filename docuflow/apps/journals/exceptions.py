class JournalException(Exception):
    pass


class InvalidPeriodAccountingDateException(Exception):
    pass


class UnavailableWorkflow(Exception):
    """
    To be raised if the Invoice Workflow does not exist
    Invoice
    """


class InvalidInvoiceTotal(Exception):
    """
    To be raised if the Invoice totals does not balance
    """


class InvoiceRequestedDecline(Exception):
    """
    To be raised if the Invoice has been set to DECLINE(status 90)
    """


class InvalidDistributionAmount(Exception):
    """
    To be raised if the Invoice amount entered exceeds the totals for the invoice
    """


class DuplicateInvoiceNumber(Exception):
    """c
    To be raised if there is duplicate invoice numbers
    """


class AccountPostingNotSetException(Exception):
    pass


class InvoiceJournalNotAvailable(JournalException):
    pass


class DefaultAccountNotPosted(JournalException):
    pass


class JournalNotBalancingException(JournalException):
    pass


class InvoiceHasVariance(JournalException):
    pass


class AccountingDateClosed(JournalException):
    pass


class ContentTypeNotFound(JournalException):
    pass