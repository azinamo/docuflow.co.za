import logging
import pprint

from django.db.models import Q, Sum, Case, When, DecimalField, F
from .models import JournalLine, OpeningBalance


logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class BalanceSheet:

    def __init__(self, filter_options, start_date, end_date, include_cents=False):
        self.start_date = start_date
        self.end_date = end_date
        self.filter_options = filter_options
        self.include_cents = include_cents

        self.total_outgoing = 0
        self.total_incoming = 0
        self.total_movement = 0
        self.total_period = 0
        self.total_assets = 0
        self.total_period_asset = 0
        self.total_incoming_asset = 0
        self.total_outgoing_asset = 0
        self.total_movement_asset = 0
        self.total_liabilities = 0
        self.assets = {}

        self.total_period_liabilities = 0
        self.total_incoming_liabilities = 0
        self.total_outgoing_liabilities = 0
        self.total_movement_liabilities = 0
        self.liabilities = {}

    def get_amount(self, amount):
        if amount:
            amt = float(amount)
            if not self.include_cents:
                return round(amt)
            else:
                return amt
        return 0

    def get_balance_sheet_structure(self):
        balance_sheet = {
            'assets': {
                'label': 'Assets',
                'types': {
                    'fixed-assets': {},
                    'turnover-assets': {},
                    'current-assets': {}
                }
            },
            'liabilities_and_own_capital': {
                'label': 'LIABILITIES AND OWN CAPITAL',
                'types': {
                    'liabilities': {},
                    'long-term-liabilities': {},
                    'short-term-liabilities': {},
                    'own-capital': {},
                }
            }
        }
        return balance_sheet

    def get_journal_lines(self):
        logger.info("Ledger lines")
        q_period_filter = Q()
        q_period_filter.add(Q(journal__date__gte=self.start_date), Q.AND)
        q_period_filter.add(Q(journal__date__lte=self.end_date), Q.AND)
        lines = JournalLine.objects.annotate(
            total_incoming=Case(
                When(journal__date__lte=self.start_date, then=Sum('debit') + Sum(F('credit') * -1)), output_field=DecimalField()
            ),
            period_total=Case(
                When(q_period_filter, then=Sum('debit') + Sum(F('credit') * -1)), output_field=DecimalField()
            )
        ).select_related(
            'account',
            'journal',
            'journal__period',
            'vat_code',
            'supplier',
            'invoice',
            'invoice__invoice_type',
            'journal__year'
        ).prefetch_related(
            'object_items'
        ).filter(
            **self.filter_options
        ).order_by(
            '-account__report_type__account_type',
            'account__report_type__sorting',
            'account__code',
            'journal__date',
            '-date',
            'journal__period__period',
        ).values(
            'account_id', 'account__name', 'account__code', 'account__id', 'total_incoming', 'period_total',
            'account__report__name', 'account__report__slug'
        )
        logger.info(lines)
        return lines

    def is_liability(self, slug):
        return slug in ['long-term-liabilities', 'current-liabilities', 'equity']

    def is_asset(self, slug):
        return slug in ['fixed-assets', 'current-asset', 'inventory-assets', 'cash']

    def add_liability_account_line(self, line_key, account_key, account_line, incoming_amount=0, period_amount=0,
                                   movement_amount=0):
        outgoing_amount = incoming_amount + period_amount + movement_amount
        if line_key in self.liabilities:
            if account_key in self.liabilities[line_key]['accounts']:
                self.liabilities[line_key]['accounts'][account_key]['period_amount'] += period_amount
                self.liabilities[line_key]['accounts'][account_key]['movement_amount'] += movement_amount
                self.liabilities[line_key]['accounts'][account_key]['outgoing'] += outgoing_amount
            else:
                account_line['outgoing'] = outgoing_amount
                self.liabilities[line_key]['accounts'][account_key] = account_line
        else:
            account_line['outgoing'] = outgoing_amount
            self.liabilities[line_key] = {
                'incoming': incoming_amount,
                'period_amount': 0,
                'movement_amount': 0,
                'outgoing': 0,
                'accounts': {account_key: account_line}
            }
        self.liabilities[line_key]['incoming'] += incoming_amount
        self.liabilities[line_key]['movement_amount'] += movement_amount
        self.liabilities[line_key]['period_amount'] += period_amount
        self.liabilities[line_key]['outgoing'] += outgoing_amount
        self.total_liabilities += outgoing_amount

        self.total_period_liabilities += period_amount
        self.total_movement_liabilities += movement_amount
        self.total_incoming_liabilities += incoming_amount
        self.total_outgoing_liabilities += outgoing_amount

        self.total_incoming += incoming_amount
        self.total_movement += movement_amount
        self.total_outgoing += outgoing_amount
        self.total_period += period_amount

    def add_asset_account_line(self, line_key, account_key, account_line, incoming_amount=0, period_amount=0,
                               movement_amount=0):
        outgoing_amount = incoming_amount + period_amount + movement_amount

        if line_key in self.assets:
            if account_key in self.assets[line_key]['accounts']:
                self.assets[line_key]['accounts'][account_key]['period_amount'] += period_amount
                self.assets[line_key]['accounts'][account_key]['movement_amount'] += movement_amount
                self.assets[line_key]['accounts'][account_key]['outgoing'] += outgoing_amount
            else:
                account_line['outgoing'] = outgoing_amount
                self.assets[line_key]['accounts'][account_key] = account_line
        else:
            account_line['outgoing'] = outgoing_amount
            self.assets[line_key] = {
                'incoming': 0,
                'period_amount': 0,
                'movement_amount': 0,
                'outgoing': 0,
                'accounts': {account_key: account_line}
            }

        self.assets[line_key]['incoming'] += incoming_amount
        self.assets[line_key]['movement_amount'] += movement_amount
        self.assets[line_key]['period_amount'] += period_amount
        self.assets[line_key]['outgoing'] += outgoing_amount

        self.total_period_asset += period_amount
        self.total_movement_asset += movement_amount
        self.total_incoming_asset += incoming_amount
        self.total_outgoing_asset += outgoing_amount

        self.total_incoming += incoming_amount
        self.total_movement += movement_amount
        self.total_outgoing += outgoing_amount
        self.total_period += period_amount

    def get_opening_balances(self):
        opening_balances = OpeningBalance.objects.select_related(
            'account',
            'account__report'
        ).filter(
            company=self.filter_options['journal__company'],
            year=self.filter_options['journal__year'],
            account__report__account_type=self.filter_options['account__report__account_type']
        ).exclude(
            amount__exact=0
        ).order_by(
            'account__code'
        )

        for opening_balance in opening_balances:
            line_key = (opening_balance.account.report.slug, opening_balance.account.report.name)
            account_key = (opening_balance.account.name, opening_balance.account.code)
            amount = self.get_amount(opening_balance.amount)

            account_line = {'account': opening_balance.account.name,
                            'account_code': opening_balance.account.code,
                            'incoming': amount,
                            'period_amount': 0,
                            'outgoing': 0,
                            'movement_amount': 0
                            }

            if self.is_liability(opening_balance.account.report.slug):
                self.add_liability_account_line(line_key, account_key, account_line, amount, 0, 0)
            else:
                self.add_asset_account_line(line_key, account_key, account_line, amount, 0, 0)

    def execute(self):
        lines = self.get_journal_lines()
        self.get_opening_balances()
        for journal_line in lines:
            line_key = (journal_line['account__report__slug'], journal_line['account__report__name'])
            logger.info(line_key)
            account_key = (journal_line['account__name'], journal_line['account__code'])
            period_total = self.get_amount(journal_line['period_total'])
            movement_amount = self.get_amount(journal_line['total_incoming'])

            account_line = {'account': journal_line['account__name'],
                            'account_code': journal_line['account__code'],
                            'movement_amount': movement_amount,
                            'period_amount': period_total,
                            'outgoing': 0
                            }

            if self.is_liability(journal_line['account__report__slug']):
                self.add_liability_account_line(line_key, account_key, account_line, 0, period_total, movement_amount)
            else:
                self.add_asset_account_line(line_key, account_key, account_line, 0, period_total, movement_amount)

        logger.info(self.liabilities)
        logger.info(self.assets)


class BalanceSheetLiabilities:
    long_term = {}
    turnover_assets = {}
    own_capital = {}

    def __init__(self, company, year):
        self.company = company

    def execute(self):
        pass
