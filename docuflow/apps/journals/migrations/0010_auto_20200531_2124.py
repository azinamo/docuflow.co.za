# Generated by Django 2.0 on 2020-05-31 19:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('journals', '0009_auto_20200529_0637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
