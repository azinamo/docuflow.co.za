# Generated by Django 2.0 on 2020-06-03 09:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('journals', '0012_auto_20200602_0427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='date',
            field=models.DateField(blank=True, db_index=True, null=True),
        ),
    ]
