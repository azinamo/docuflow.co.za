from enumfields import IntEnum

from django.urls import reverse_lazy
from django.utils.translation import gettext as __


class LedgerType(IntEnum):
    GENERAL = 0
    SUPPLIER = 1
    CUSTOMER = 2

    class Labels:
        GENERAL = __('G')
        SUPPLIER = __('S')
        CUSTOMER = __('C')

    @classmethod
    def get_ajax_url(cls, ledger_type):
        if ledger_type == cls.GENERAL:
            return reverse_lazy('ledger_accounts_list')
        elif ledger_type == cls.SUPPLIER:
            return reverse_lazy('suppliers_list')
        elif ledger_type == cls.CUSTOMER:
            return reverse_lazy('customers_list')
