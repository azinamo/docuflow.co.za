import logging
import re
from collections import OrderedDict
from datetime import datetime, timedelta
from decimal import Decimal

from django.contrib.contenttypes.fields import GenericForeignKey, ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q, F, Case, When, Sum, DecimalField
from django.db.models.functions import Coalesce
from django.utils.translation import gettext_lazy as _
from enumfields.fields import EnumIntegerField
from safedelete.managers import SafeDeleteManager
from safedelete.models import SafeDeleteModel, SOFT_DELETE

from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.common.behaviours import Loggable, Distributable
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Company, Account, VatCode, ObjectItem, CompanyObject, PaymentMethod, Currency
from docuflow.apps.distribution.models import Distribution
from docuflow.apps.period.models import Period, Year
from docuflow.apps.system.enums import AccountType
from .enums import LedgerType

logger = logging.getLogger(__name__)


class Journal(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    GENERAL = 0
    SUPPLIER = 1
    CUSTOMER = 2
    LEDGERS = (
        (GENERAL, 'G'),
        (SUPPLIER, 'S'),
        (CUSTOMER, 'C'),
    )

    company = models.ForeignKey(Company, related_name='company_journals', on_delete=models.CASCADE)
    year = models.ForeignKey(Year, related_name='year_journals', on_delete=models.DO_NOTHING)
    period = models.ForeignKey(Period, related_name='period_journals', on_delete=models.DO_NOTHING, db_index=True)
    journal_id = models.PositiveIntegerField(null=True)
    journal_number = models.CharField(max_length=255)
    journal_text = models.CharField(max_length=255)
    module = EnumIntegerField(Module)
    role = models.ForeignKey(Role, related_name="ledgers", blank=True, null=True, on_delete=models.DO_NOTHING)
    comment = models.TextField(blank=True)
    attachment = models.FileField(upload_to='uploads', null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    created_by = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name='ledgers', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Journals'
        ordering = ['-created_at']
        indexes = [
            models.Index(fields=['company', 'date'])
        ]

    def __str__(self):
        return self.journal_number

    def save(self, *args, **kwargs):
        if not self.date:
            self.date = datetime.now()

        if not self.pk or not self.journal_id:
            journal_count = Journal.objects.filter(company=self.company).count()
            self.journal_id = journal_count + 1
        if not self.journal_number:
            filter_options = {'company': self.company, 'year': self.year}
            journal_str = Journal.get_journal_str(filter_options)
            self.journal_number = self.generate_journal_number(filter_options, 1, journal_str)
        return super(Journal, self).save(*args, **kwargs)

    @staticmethod
    def get_journal_str(filter_options):
        journal_str = ''
        year = filter_options['year']
        if year and year.journal_number:
            journal_str = year.journal_number
        last_journal = Journal.objects.filter(**filter_options).first()
        if last_journal:
            logger.info(f"Last journal number is {last_journal.journal_number}")
            journal_str = last_journal.journal_number
        return journal_str

    def is_balanced(self):
        total_debit = self.calculate_total_credit()
        total_credit = self.calculate_total_debit()
        difference = total_debit - total_credit
        if total_credit != total_debit:
            return False
        return True

    def calculate_total_credit(self):
        return sum(line.credit for line in self.lines.all() if line.credit)

    def calculate_total_debit(self):
        return sum(line.debit for line in self.lines.all() if line.debit)

    # noinspection PyMethodMayBeStatic
    def get_journal_number_counter(self, journal_str):
        digits = re.findall(r'\d+', journal_str)
        match_len = len(digits)
        if match_len > 0:
            return digits[match_len-1]
        return 1

    def get_constant_part_start(self, journal_str, counting_part):
        counting_part_len = len(str(counting_part))
        if counting_part_len:
            counting_part_len = counting_part_len * -1
        constant_part = journal_str[:counting_part_len]
        return constant_part

    def generate_journal_number(self, filter_options, counter=1, journal_str=''):
        if journal_str:
            try:
                counting_part = self.get_journal_number_counter(journal_str)
                constant_part = self.get_constant_part_start(journal_str, counting_part)
                _next_number = int(counting_part) + 1
                _counter_str = str(_next_number).rjust(len(counting_part), '0')
                journal_number = f"{constant_part}{_counter_str}"
                return journal_number
            except ValueError:
                return str(counter).rjust(5, '0')
        else:
            count = Journal.objects.filter(**filter_options).count()
            if count > 0:
                counter = count + 1
            return str(counter).rjust(5, '0')

    @property
    def module_name(self):
        return self.module.label

    @property
    def journal_sum(self):
        return 0

    @property
    def is_invoice_journal(self):
        return self.module == Module.JOURNAL

    @property
    def view_journal(self):
        if self.module in [Module.PURCHASE, Module.SYSTEM, Module.PAYMENT, Module.FIXED_ASSET]:
            return True
        if not self.created_by:
            return True
        return (self.period and self.period.is_closed_period) or self.is_invoice_journal

    @property
    def is_general(self):
        return self.module in [Module.ACCOUNTING, Module.JOURNAL]

    @property
    def is_open(self):
        return self.period and self.period.is_open

    @property
    def is_open_general(self):
        if self.period:
            if self.period.is_open and self.is_general:
                return True
            return False
        return False


class JournalLineManager(SafeDeleteManager):

    def get_vat_lines(self, company, vat_code_account_ids):
        return self.select_related(
            'invoice', 'invoice__invoice_type', 'invoice__company', 'account', 'journal__company', 'vat_code',
            'journal', 'journal__year', 'journal__period', 'supplier', 'sales_invoice', 'sales_invoice__customer',
            'sales_invoice__invoice_type', 'sales_invoice__customer__company', 'invoice__supplier'
        ).prefetch_related(
            'object_items', 'content_object', 'invoice__invoice_accounts'
        ).filter(
            journal__company=company, vat_report__isnull=True, account_id__in=vat_code_account_ids,
            journal__deleted__isnull=True
        ).exclude(
            journal__module__in=[Module.SYSTEM.value, Module.DOCUMENT.value]
        )

    def get_ledger_suffix_lines(self, year, start_date=None, end_date=None, account=None, object_items=None, with_balance=True):
        if not object_items:
            object_items = []

        q = self.annotate(
            debit_amount=Coalesce(F('debit'), 0, output_field=models.DecimalField()),
            credit_amount=Coalesce(F('credit'), 0, output_field=models.DecimalField()) * -1,
            line_amount=Sum(F('debit_amount') + F('credit_amount'))
        ).filter(journal__year=year, account=account)

        if start_date:
            q = q.filter(journal__date__gte=start_date)

        if end_date:
            q = q.filter(journal__date__lte=end_date)

        if not with_balance:
            q = q.filter(Q(line_amount__gt=0) | Q(line_amount__lt=0))

        if object_items:
            items_list = [int(object_item) for object_item in object_items if not object_item.startswith('all', 0, 3)]
            if len(items_list):
                q = q.filter(object_items__id__in=[object_id for object_id in items_list])

        return q.order_by('suffix').values('suffix', 'line_amount')

    def get_ledger_lines(self, year, start_date=None, end_date=None, from_account=None, to_account=None, object_items=None):
        if not object_items:
            object_items = []

        filters = {'journal__year': year}
        if from_account:
            filters['account__code__gte'] = from_account.code
        if to_account:
            filters['account__code__lte'] = to_account.code
        if end_date:
            filters['journal__date__lte'] = end_date

        if object_items:
            items_list = [int(object_item) for object_item in object_items if not object_item.startswith('all', 0, 3)]
            if len(items_list):
                filters['object_items__id__in'] = [object_id for object_id in items_list]

        return self.annotate(
            debit_amount=Coalesce(F('debit'), 0, output_field=models.DecimalField()),
            credit_amount=Coalesce(F('credit'), 0, output_field=models.DecimalField()) * -1,
            line_amount=F('debit_amount') + F('credit_amount')
        ).select_related(
            'account', 'journal', 'journal__period', 'vat_code', 'supplier', 'invoice', 'invoice__invoice_type',
            'journal__year'
        ).prefetch_related(
            'object_items'
        ).filter(
            **filters
        ).order_by(
            '-account__code', '-account__name', 'journal__date'
        ).values(
            'credit', 'debit', 'debit_amount', 'credit_amount', 'line_amount',
            'text', 'account_id', 'account__name', 'account__code', 'account__id', 'journal__period__period',
            'journal__date', 'journal__journal_text', 'journal__journal_number', 'vat_code__label', 'vat_report_id',
            'object_items__company_object_id', 'object_items__label', 'created_at', 'id', 'vat_code_id', 'invoice_id',
            'accounting_date', 'ledger_type_reference', 'journal__module', 'suffix'
        )

    # noinspection PyMethodMayBeStatic
    def get_period_amount_filter(self, start_date, end_date):
        period_filter = Q()
        period_filter.add(Q(journal__date__gte=start_date), Q.AND)
        period_filter.add(Q(journal__date__lte=end_date), Q.AND)
        return period_filter

    # noinspection PyMethodMayBeStatic
    def get_incoming_amount_filter(self, start_date):
        start_date = start_date - timedelta(days=1)
        incoming_filter = Q()
        incoming_filter.add(Q(journal__date__lte=start_date), Q.AND)
        return incoming_filter

    def balance_report(self, company, year, start_date, end_date, account_type):
        period_filter = self.get_period_amount_filter(start_date, end_date)
        incoming_filter = self.get_incoming_amount_filter(start_date)
        return self.values(
            'account'
        ).annotate(
            total_incoming=Case(
                When(incoming_filter, then=Coalesce(Sum('debit'), 0) + Coalesce(Sum(F('credit') * -1), 0)),
                output_field=models.DecimalField()
            ),
            period_total=Case(
                When(period_filter, then=Coalesce(Sum('debit'), 0) + Coalesce(Sum(F('credit') * -1), 0)),
                output_field=models.DecimalField()
            )
        ).filter(
            journal__company=company, journal__year=year, account__report__account_type=account_type,
            deleted__isnull=True, account__company=company
        ).order_by(
            '-account__report__account_type', 'account__report__sorting', 'account__code', 'journal__date',
            '-accounting_date', 'journal__period__period',
        ).values(
            'id', 'account_id', 'account__name', 'account__code', 'account__id', 'total_incoming', 'period_total',
            'account__report__name', 'account__report__slug'
        )

    # noinspection PyMethodMayBeStatic
    def get_query_filtered_case(self, q_filter):
        return Case(When(q_filter, then=Sum(Coalesce(F('debit'), 0)) + Sum(Coalesce(F('credit'), 0) * -1)),
                    output_field=DecimalField())

    def get_year_period_filter(self, start_date, end_date):
        q_period_filter = Q()
        q_period_filter.add(Q(journal__date__gte=start_date), Q.AND)
        q_period_filter.add(Q(journal__date__lte=end_date), Q.AND)

        return self.get_query_filtered_case(q_period_filter)

    def total_deductable_expenses(self, company, is_deducatble, year, to_date):
        # is non deductable, not ticked
        this_year_period_filter = self.get_year_period_filter(year.start_date, to_date)

        return JournalLine.objects.values(
            'account__report__slug'
        ).annotate(
            period_total=this_year_period_filter
        ).filter(
            journal__company=company, journal__year=year, account__report__account_type=AccountType.INCOME_STATEMENT.value,
            account__company=company, journal__date__lte=to_date, account__not_deductable=is_deducatble
        ).exclude(
            account__report__slug='turnover'
        ).aggregate(total=Sum('period_total'))

    def get_for_report_type(self, year, company, report_type, account_type, start_date, end_date):
        this_year_period_filter = self.get_year_period_filter(start_date, end_date)

        return JournalLine.objects.values(
            'account__report__slug'
        ).annotate(
            period_total=this_year_period_filter
        ).filter(
            journal__company=company, journal__year=year, account__report__account_type=account_type,
            journal__date__lte=end_date, account__report__slug=report_type
        ).aggregate(total=Sum('period_total'))

    def get_for_sub_ledger(self, year, company, sub_ledger, start_date, end_date):
        this_year_period_filter = self.get_year_period_filter(start_date, end_date)

        return JournalLine.objects.values(
            'account__sub_ledger'
        ).annotate(
            period_total=this_year_period_filter
        ).filter(
            journal__company=company, journal__year=year, journal__date__lte=end_date,
            account__sub_ledger=sub_ledger
        )

    def get_for_account(self, year, company, account, start_date=None, end_date=None):
        this_year_period_filter = self.get_year_period_filter(start_date, end_date)

        return JournalLine.objects.values(
            'account'
        ).annotate(
            period_total=this_year_period_filter
        ).filter(
            journal__company=company, journal__year=year, journal__date__lte=end_date,
            account=account
        ).aggregate(total=Sum('period_total'))

    def for_vat_type(self, year, company, start_date=None, end_date=None):
        this_year_period_filter = self.get_year_period_filter(start_date, end_date)

        return self.annotate(
            period_total=this_year_period_filter
        ).filter(
            journal__company=company, journal__year=year, journal__date__lte=end_date,
            account__in=VatCode.objects.filter(company=company).values_list('account_id', flat=True)
        ).aggregate(total=Sum('period_total'))

    def for_cash_flow(self, year, company, cash_flow, start_date=None, end_date=None):
        this_year_period_filter = self.get_year_period_filter(start_date, end_date)

        return self.annotate(
            period_total=this_year_period_filter
        ).filter(
            journal__company=company, journal__year=year, journal__date__lte=end_date, account__cash_flow=cash_flow
        ).aggregate(total=Sum('period_total'))

    def for_report_type(self, year, company, report_type, start_date=None, end_date=None):
        this_year_period_filter = self.get_year_period_filter(start_date, end_date)

        return self.annotate(
            period_total=this_year_period_filter
        ).filter(
            journal__company=company, journal__year=year, journal__date__lte=end_date, account__report=report_type
        ).aggregate(total=Sum('period_total'))


class JournalLine(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    GENERAL = 0
    SUPPLIER = 1
    CUSTOMER = 2

    LEDGERS = (
        (GENERAL, 'G'),
        (SUPPLIER, 'S'),
        (CUSTOMER, 'C'),
    )

    journal = models.ForeignKey(Journal, related_name='lines', on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name='account_journals', on_delete=models.PROTECT)
    content_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.PROTECT)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    vat_code = models.ForeignKey(
        VatCode, null=True, blank=True, db_index=True, related_name='vat_type_journals', on_delete=models.SET_NULL
    )
    invoice = models.ForeignKey(
        'invoice.Invoice', null=True, blank=True, related_name='journal_ledger_invoice', on_delete=models.SET_NULL
    )
    sales_invoice = models.ForeignKey(
        'sales.Invoice', null=True, blank=True, related_name='journal_lines', on_delete=models.SET_NULL
    )
    accounting_date = models.DateField(null=True, blank=True, db_index=True)
    supplier = models.ForeignKey(
        'supplier.Supplier', null=True, blank=True, related_name='supplier_ledger_journals', on_delete=models.SET_NULL
    )
    debit = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    credit = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    amended_by = models.CharField(max_length=255, default='', blank=True)
    text = models.TextField(default='', blank=True)
    is_vat = models.BooleanField(default=False)
    is_reversal = models.BooleanField(default=False)
    vat_report = models.ForeignKey('vat.VatReport', blank=True, null=True, related_name='vat_lines', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    ledger_type = EnumIntegerField(LedgerType, verbose_name="Ledger", default=LedgerType.GENERAL)
    ledger_type_reference = models.CharField(max_length=255, blank=True)
    object_items = models.ManyToManyField(ObjectItem)
    suffix = models.CharField(max_length=255, blank=True)
    branch = models.ForeignKey(
        'company.Branch', related_name='journal_lines', null=True, blank=True, on_delete=models.SET_NULL
    )
    distribution = models.OneToOneField(
        'distribution.Distribution', related_name='journal_line', null=True, blank=True, on_delete=models.SET_NULL
    )

    objects = JournalLineManager()

    class Meta:
        indexes = [
            models.Index(fields=['accounting_date', 'journal', 'account'])
        ]

    def __str__(self):
        return str(self.journal.journal_number)

    def full_clean(self, **kwargs):
        super().full_clean(**kwargs)
        if not self.accounting_date:
            raise ValidationError('Accounting date is required')

    def get_object_links(self, linked_objects):
        object_links = OrderedDict()
        for k, linked_object in linked_objects.items():
            object_links[linked_object['id']] = {
                'value': None,
                'label': linked_object['label'],
                'object_items': linked_object['object_items']
            }
        for journal_line_object_item in self.object_items.all():
            if journal_line_object_item.company_object.id in linked_objects:
                object_links[journal_line_object_item.company_object.id]['value'] = journal_line_object_item.label
        return object_links

    def get_journal_text(self):
        if self.text:
            return self.text
        elif self.journal.journal_text:
            return self.journal.journal_text
        return ''

    def current_balance(self, balance):
        if balance:
            return self.amount + balance
        return self.amount + balance

    def get_invoice(self):
        if self.invoice:
            return self.invoice
        elif self.sales_invoice:
            return self.sales_invoice
        return None

    @property
    def total_amount(self):
        total_amount = 0
        invoice = self.get_invoice()
        if invoice:
            if hasattr(invoice, 'customer'):
                total_amount = invoice.total_amount * -1
            else:
                total_amount = invoice.total_amount
        if self.is_reversal:
            total_amount = total_amount * -1
        return total_amount

    @property
    def vat_amount(self):
        invoice = self.get_invoice()
        if invoice:
            if hasattr(invoice, 'customer'):
                vat_amount = invoice.vat_amount * -1
            else:
                if invoice.vat_amount == 0 and self.vat_code:
                    vat_amount = self.amount
                else:
                    vat_amount = invoice.vat_amount
        else:
            vat_amount = self.line_vat_amount
        if self.is_reversal:
            vat_amount = vat_amount * -1
        return vat_amount

    @property
    def excluding_vat(self):
        if self.vat_code and self.vat_code.percentage:
            return (self.amount * 100) / self.vat_code.percentage
        return 0

    @property
    def including_vat(self):
        return self.excluding_vat + self.amount

    @property
    def line_vat_amount(self):
        if self.debit:
            if self.is_reversal:
                return self.debit * -1
            return self.debit
        elif self.credit:
            if self.is_reversal:
                return self.credit * -1
            return self.credit * -1

    @property
    def net_amount(self):
        net_amount = 0
        invoice = self.get_invoice()
        if invoice:
            if hasattr(invoice, 'customer'):
                net_amount = invoice.net_amount * -1
            else:
                net_amount = invoice.net_amount
        if self.is_reversal:
            net_amount = net_amount * -1
        return net_amount

    @property
    def debit_amount(self):
        total = Decimal(0)
        if self.debit and self.debit > 0:
            total += self.debit
        if self.credit and self.credit < 0:
            total += self.credit * -1
        return total

    @property
    def credit_amount(self):
        total = Decimal(0)
        if self.credit and self.credit > 0:
            total += self.credit
        if self.debit and self.debit < 0:
            total += self.debit * -1
        return total

    @property
    def line_reference(self):
        return f"journalline{self.id}"

    @property
    def account_number(self):
        return self.account.code if self.account else 'N/A'

    @property
    def journal_account(self):
        return self.account if self.account else 'N/A'

    @property
    def account_name(self):
        return self.account.name if self.account else 'N/A'

    @property
    def journal_text(self):
        if self.text:
            return self.text
        elif self.journal.journal_text:
            return self.journal.journal_text
        return ''

    @property
    def journal_date(self):
        if self.invoice:
            return self.invoice.accounting_date
        elif self.journal.date:
            return self.journal.date
        else:
            return self.created_at.date()

    @property
    def supplier_name(self):
        if self.supplier:
            return self.supplier
        if self.invoice and self.invoice.supplier:
            return self.invoice.supplier
        return ''

    @property
    def amount(self):
        total = Decimal(0)
        if self.debit_amount:
            total = self.debit_amount
        if self.credit_amount:
            total -= self.credit_amount
        return total

    @property
    def is_supplier_line(self):
        return self.content_type and self.content_type.model == 'supplier'

    @property
    def is_customer_line(self):
        return self.content_type and self.content_type.model == 'customer'

    @property
    def is_credit(self):
        return self.credit and self.credit > 0

    @property
    def journal_distribution(self):
        return Distribution.objects.filter(content_type=ContentType.objects.get_for_model(self), object_id=self.pk).first()


class OpeningBalanceManager(models.Manager):

    def ledger_lines(self, year, from_account=None, to_account=None, account=None):
        filters = {'year': year}
        if from_account:
            filters['account__code__gte'] = from_account.code
        if to_account:
            filters['account__code__lte'] = to_account.code
        if account:
            filters['account'] = account
        return self.select_related(
            'account'
        ).filter(**filters).order_by('account__code', 'account__name').only(
            'account__code', 'account__name', 'account__id', 'amount'
        )

    def get_for_report_type(self, year, company, report_type, account_type):
        return self.filter(
            company=company, year=year, account__report__account_type=account_type,
            account__report__slug=report_type
        ).aggregate(total=Sum('amount'))

    def get_for_sub_ledger(self, year, company, sub_ledger):
        return self.filter(
            company=company, year=year, account__sub_ledger=sub_ledger
        )


class OpeningBalance(models.Model):
    company = models.ForeignKey(Company, related_name='opening_balances', on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name='opening_balances', on_delete=models.CASCADE)
    year = models.ForeignKey(Year, related_name='opening_balances', on_delete=models.CASCADE)
    amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')

    objects = OpeningBalanceManager()

    def __str__(self):
        return f"Balance {self.account}-{self.year}"

    class Meta:
        ordering = ['-account__code']
        verbose_name = _('Opening Balance')
        verbose_name_plural = _('Opening Balances')
        unique_together = ('account', 'year')


class JournalLineLog(Loggable):

    journal_line = models.ForeignKey(JournalLine, null=True, blank=True, related_name='logs', on_delete=models.DO_NOTHING)

    def __str__(self):
        return f"{self.comment} by {self.profile}"

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Logs')
        verbose_name_plural = _('Logs')