from rest_framework import viewsets
from django.db.models import Count

from docuflow.apps.journals.filters import JournalFilter

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.journals.api.serializers import LedgerSerializer
from docuflow.apps.journals.models import Journal


class JournalViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = LedgerSerializer

    def get_queryset(self):
        return Journal.objects.filter(
            company=self.get_company(), year=self.get_year()
        ).annotate(
            has_been_edited=Count('lines__logs')
        ).select_related(
            'period', 'created_by', 'created_by__user'
        ).order_by(
            '-created_at'
        )
