from django.urls import reverse

from rest_framework import serializers

from docuflow.apps.journals.models import Journal


class LedgerSerializer(serializers.ModelSerializer):
    is_open_general = serializers.SerializerMethodField()
    is_locked = serializers.SerializerMethodField()
    module = serializers.SerializerMethodField()
    detail_url = serializers.SerializerMethodField()
    period_number = serializers.SerializerMethodField()
    creator = serializers.SerializerMethodField()
    created_date = serializers.SerializerMethodField()
    has_been_edited = serializers.SerializerMethodField()

    class Meta:
        fields = ('id', 'journal_number', 'journal_text', 'module', 'role', 'created_at', 'date',
                  'is_open', 'is_locked', 'detail_url', 'is_open_general', 'period_number', 'comment',
                  'creator', 'created_date', 'has_been_edited')
        model = Journal
        datatables_always_serialize = ('id', 'detail_url', 'is_locked', 'is_open', 'is_open_general', 'has_been_edited')

    def get_period_number(self, instance):
        return instance.period.period if instance.period else ''

    def get_is_locked(self, instance):
        if instance.period:
            return instance.period.is_locked
        return False

    def get_has_been_edited(self, instance):
        return instance.has_been_edited

    def get_is_open_general(self, instance):
        return instance.is_open_general

    def get_module(self, instance):
        return instance.module_name

    def get_creator(self, instance):
        return str(instance.created_by) if instance.created_by else ''

    def get_created_date(self, instance):
        return instance.created_at.strftime('%Y-%m-%d') if instance.created_at else ''

    def get_detail_url(self, instance):
        if instance.is_open:
            return reverse('journal_lines', kwargs={'pk': instance.id})
        return reverse('journal_detail', args=(instance.pk,))
