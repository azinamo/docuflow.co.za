import pprint

from django.utils.timezone import now
from django import forms

from docuflow.apps.distribution.models import Distribution, DistributionAccount as JournalDistribution


pp = pprint.PrettyPrinter(indent=4)


class AccountDistributeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(AccountDistributeForm, self).__init__(*args, **kwargs)
        self.fields['starting_date'].initial = now().date()
        self.fields['total_amount'].initial = self.request.GET.get('amount')
        self.fields['nb_months'].label = 'How many months'
        self.fields['total_amount'].label = None

    class Meta:
        model = Distribution
        fields = ('starting_date', 'nb_months', 'total_amount')

    def save(self, commit=True):
        distribution = super().save(commit=False)
        distribution.role = self.role
        distribution.profile = self.profile

        distributions = {}
        for field, value in self.request.POST.items():
            if field.startswith('distribution', 0, 12):
                row_id = value
                accounts = self.request.POST.getlist(f'accounts_{row_id}')
                data = {
                    'date': self.request.POST.get(f'date_{row_id}'),
                    'period': self.request.POST.get(f'period_{row_id}'),
                    'debit': self.request.POST.get(f'debit_{row_id}'),
                    'credit': self.request.POST.get(f'credit_{row_id}'),
                    'accounts': {}
                }
                for account_id in accounts:
                    data['accounts'][account_id] = {
                        'debit': self.request.POST.get(f'accounts_{row_id}_debit_{account_id}'),
                        'credit': self.request.POST.get(f'accounts_{row_id}_credit_{account_id}')
                    }
                distributions[row_id] = data
        journal_lines_distributions = {}
        if 'distributions' in self.request.session:
            journal_lines_distributions = self.request.session['distributions']
        print("Before: Current distributions")
        pp.pprint(journal_lines_distributions)
        journal_lines_distributions[self.request.POST.get('row_id')] = {
            'lines': distributions,
            'distribution': {
                'total_amount': float(distribution.total_amount),
                'starting_date': str(distribution.starting_date),
                'nb_months': distribution.nb_months
            }
        }
        print("After: Current distributions")
        pp.pprint(journal_lines_distributions)
        self.request.session['distributions'] = journal_lines_distributions
        return distribution


class JournalAccountDistributionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.journal_line = kwargs.pop('journal_line')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(JournalAccountDistributionForm, self).__init__(*args, **kwargs)
        self.fields['starting_date'].initial = self.journal_line.accounting_date
        self.fields['total_amount'].initial = self.journal_line.amount
        self.fields['nb_months'].label = 'How many months'
        self.fields['total_amount'].label = None

    class Meta:
        model = Distribution
        fields = ('starting_date', 'nb_months', 'total_amount')
        widgets = {
            'starting_date': forms.TextInput(attrs={'class': 'future_datepicker'}),
            'total_amount': forms.HiddenInput()
        }

    def save(self, commit=True):
        distribution = super(JournalAccountDistributionForm, self).save(commit=False)
        distribution.journal_line = self.journal_line
        distribution.profile = self.profile
        distribution.role = self.role
        distribution.company = self.journal_line.journal.company
        distribution.save()

        self.create_distribution_accounts(distribution)

        return distribution

    def create_distribution_accounts(self, distribution):
        if distribution.distribution_accounts.exists():
            for distribution_line in distribution.distribution_accounts.all():
                distribution_line.delete()

        for field, value in self.request.POST.items():
            if field.startswith("distribution", 0, 12):
                accounts = self.request.POST.getlist(f'accounts_{value}')
                for account_id in accounts:
                    debit = self.request.POST.get(f'accounts_{value}_debit_{account_id}')
                    credit = self.request.POST.get(f'accounts_{value}_credit_{account_id}')
                    data = {
                        'date': self.request.POST.get(f'date_{value}'),
                        'period': self.request.POST.get(f'period_{value}'),
                        'account': account_id,
                        'debit': round(float(debit), 2),
                        'credit': round(float(credit), 2),
                        'distribution': distribution
                    }
                    journal_distribution_form = JournalDistributionForm(data=data)
                    if journal_distribution_form.is_valid():
                        journal_distribution_form.save()
                    else:
                        print(journal_distribution_form.errors)


class JournalDistributionForm(forms.ModelForm):

    class Meta:
        model = JournalDistribution
        fields = ('account', 'debit', 'credit', 'period', 'distribution', 'date')

