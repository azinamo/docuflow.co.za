from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.DistributionReportView.as_view(), name='journal_distributions_index'),
    url(r'^account/create$', views.CreateAccountDistributeView.as_view(), name='create_journal_account_distribution'),
    url(r'^(?P<journal_line_id>\d+)/create/$', views.CreateDistributionView.as_view(),
        name='create_journal_distribution'),
    url(r'^accounts/(?P<journal_line_id>\d+)/(?P<account_id>\d+)/distribute/$', views.DistributionDetailView.as_view(),
        name='journal_distribution_detail'),
    url(r'^edit/(?P<pk>\d+)/$', views.EditDistributionView.as_view(), name='edit_journal_distribution'),
    url(r'^(?P<pk>\d+)/generate/$', views.GenerateDistributionView.as_view(),
        name='generate_journal_line_distribution'),
    url(r'^line/generate/$', views.GenerateLineDistributionView.as_view(),
        name='generate_line_distribution'),
    url(r'^account/(?P<pk>\d+)/delete$', views.DeleteJournalDistributionView.as_view(),
        name='delete_journal_distribution'),
    url(r'^(?P<pk>\d+)/balances/$', views.DistributionBalanceBreakdownView.as_view(),
        name='show_journal_distribution_balance_breakdown'),
    url(r'^(?P<pk>\d+)/journal/detail/$', views.DistributionJournalView.as_view(),
        name='distribution_journal_detail'),
    url(r'^journal/create/$', views.CreateJournalForDistributionView.as_view(),
        name='journal_create_journal_for_distribution'),
    url(r'^journal/$', views.JournalForDistributionInvoiceView.as_view(),
        name='journal_journal_for_distribution'),
    url(r'^balances/$', views.DistributionBalanceView.as_view(),
        name='journal_distribution_balance'),
    url(r'^capture/journals/$', views.CaptureDistributionJournalView.as_view(),
        name='journal_capture_distribution_journal'),
]
