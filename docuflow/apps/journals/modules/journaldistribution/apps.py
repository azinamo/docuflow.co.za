from django.apps import AppConfig


class JournaldistributionConfig(AppConfig):
    name = 'docuflow.apps.journals.modules.journaldistribution'
