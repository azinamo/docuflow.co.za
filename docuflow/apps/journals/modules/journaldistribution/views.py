import collections
import logging
import pprint
from decimal import Decimal

from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.db import transaction
from django.urls import reverse
from django.views.generic import ListView, FormView, View, TemplateView, UpdateView, CreateView
from django.utils.timezone import datetime

from annoying.functions import get_object_or_None

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Company, ObjectItem, Account
from docuflow.apps.period.models import Period, Year
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.distribution.models import Distribution, DistributionAccount
from docuflow.apps.invoice.reports import DistributionJournal, DistributionBalance
from docuflow.apps.invoice.services import distribute_invoice_accounts
from docuflow.apps.invoice.models import Journal, InvoiceJournal, InvoiceAccount, Invoice
from . import services
from . import forms

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


# Create your views here.
class DistributionReportView(ProfileMixin, LoginRequiredMixin, ListView):
    template_name = 'journaldistribution/index.html'
    models = Distribution
    context_object_name = 'distributions'

    def get_queryset(self):
        return Distribution.objects.filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(DistributionReportView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context


class CreateAccountDistributeView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'journaldistribution/create_for_account.html'
    form_class = forms.AccountDistributeForm

    def get_account(self):
        return Account.objects.get(pk=self.request.GET.get('account_id'))

    def get_form_kwargs(self):
        kwargs = super(CreateAccountDistributeView, self).get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateAccountDistributeView, self).get_context_data(**kwargs)
        context['account'] = self.get_account()
        context['amount'] = Decimal(self.request.GET.get('amount'))
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Account distribution could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateAccountDistributeView, self).form_invalid(form)

    def create_distribution_accounts(self, distribution, invoice):
        invoice_accounts = JournalLine.objects.filter(distribution=distribution)
        months = int(distribution.nb_months)
        start_date = datetime.strptime(distribution.starting_date.strftime('%Y%m%d'), '%Y%m%d')

        periods = self.get_periods(distribution.company)
        accounts_distribution = distribute_invoice_accounts(distribution.company, invoice_accounts, months, start_date,
                                                            periods, invoice)

        for k, account_distribution in accounts_distribution['distributions'].items():
            DistributionAccount.objects.create(
                distribution=distribution,
                account=account_distribution['distribution_account'],
                debit=account_distribution['debit'],
                credit=account_distribution['credit'],
                distribution_date=account_distribution['day'],
                period=account_distribution['period']
            )
            for account_distribution_account in account_distribution['accounts']:
                DistributionAccount.objects.create(
                    distribution=distribution,
                    account=account_distribution_account['account'],
                    credit=account_distribution_account['credit'],
                    debit=account_distribution_account['debit'],
                    distribution_date=account_distribution['day'],
                    period=account_distribution['period'],
                    invoice_account_id=account_distribution_account['invoice_account_id']
                )

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                with transaction.atomic():
                    form.save()
                return JsonResponse({'error': False,
                                     'text': 'Distribution successfully saved'
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'Unexpected error occurred ',
                                     'details': 'Error occurred {}'.format(exception)
                                     })
        return HttpResponse('Not allowed')


class CreateDistributionView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = 'journaldistribution/create.html'
    form_class = forms.JournalAccountDistributionForm
    model = Distribution

    def get_journal_line(self):
        return JournalLine.objects.get(pk=self.kwargs['journal_line_id'])

    def get_form_kwargs(self):
        kwargs = super(CreateDistributionView, self).get_form_kwargs()
        kwargs['journal_line'] = self.get_journal_line()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateDistributionView, self).get_context_data(**kwargs)
        context['journal_line'] = self.get_journal_line()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Distribution could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateDistributionView, self).form_invalid(form)

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def form_valid(self, form):
        if self.request.is_ajax():
            with transaction.atomic():
                form.save()
            return JsonResponse({'error': False,
                                 'text': 'Distribution successfully saved',
                                 'reload': True
                                 })
        return HttpResponse('Not allowed')


class EditDistributionView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = 'journaldistribution/edit.html'
    form_class = forms.JournalAccountDistributionForm
    model = Distribution

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def get_initial(self):
        initial = super(EditDistributionView, self).get_initial()
        return initial

    def get_form_kwargs(self):
        kwargs = super(EditDistributionView, self).get_form_kwargs()
        kwargs['journal_line'] = self.get_object().journal_line
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditDistributionView, self).get_context_data(**kwargs)
        distribution = self.get_object()
        periods = self.get_periods(distribution.company)
        accounts_distributions = services.distribute_journal_line(distribution.company, distribution.nb_months, distribution.starting_date, periods, distribution.journal_line)
        context['months'] = distribution.nb_months
        context['start_date'] = distribution.starting_date
        context['distributions'] = accounts_distributions['distributions']
        context['total_distribution'] = accounts_distributions['total_distribution']
        return context

    # def create_distribution_accounts(self, distribution, invoice):
    #     invoice_accounts = InvoiceAccount.objects.filter(distribution=distribution)
    #     months = int(distribution.nb_months)
    #     start_date = datetime.strptime(distribution.starting_date.strftime('%Y%m%d'), '%Y%m%d')
    #
    #     periods = self.get_periods(distribution.company)
    #     accounts_distribution = distribute_invoice_accounts(distribution.company, invoice_accounts, months, start_date,
    #                                                         periods, invoice)
    #
    #     for k, account_distribution in accounts_distribution['distributions'].items():
    #         DistributionAccount.objects.create(
    #             distribution=distribution,
    #             account=account_distribution['distribution_account'],
    #             credit=account_distribution['total'],
    #             distribution_date=account_distribution['day'],
    #             period=account_distribution['period']
    #         )
    #         for account_distribution_account in account_distribution['accounts']:
    #             DistributionAccount.objects.create(
    #                 distribution=distribution,
    #                 account=account_distribution_account['account'],
    #                 debit=account_distribution_account['amount'],
    #                 distribution_date=account_distribution['day'],
    #                 period=account_distribution['period']
    #             )

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Distribution could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditDistributionView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            with transaction.atomic():
                form.save()
            return JsonResponse({'error': False,
                                 'text': 'Distribution successfully updated',
                                 'reload': True
                                 })


class DeleteJournalDistributionView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            distribution = Distribution.objects.get(pk=self.kwargs['pk'])
            with transaction.atomic():
                journal_line = distribution.journal_line
                for distribution_account in distribution.distribution_accounts.all():
                    distribution_account.delete()

                journal_line.distribution = None
                journal_line.save()

                if distribution.distribution_accounts.count() == 0:
                    distribution.delete()

                response = {'text': 'Distribution deleted successfully',
                            'error': False,
                            'reload': True
                            }
        except Distribution.DoesNotExist:
            response = {'text': 'Distribution not found', 'error': True}
        return JsonResponse(response)


class DistributionBalanceBreakdownView(LoginRequiredMixin, TemplateView):
    template_name = 'journaldistribution/breakdown.html'

    def get_filter_options(self, company):
        options = {'company': company}
        invoice_statuses = []
        completed = completed_statuses()
        processing = processing_statuses()
        if 'status' in self.request.GET and self.request.GET['status']:
            for status in self.request.GET.getlist('status'):
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        else:
            invoice_statuses = completed
        if len(invoice_statuses) > 0:
            options['invoice__status__slug__in'] = invoice_statuses
        return options

    def get_statuses(self):
        sts = ['final-signed', 'in-the-flow', 'end-flow-printed', 'paid', 'printed-status-still-in-the-flow']
        return Status.objects.filter(slug__in=sts).all()

    @staticmethod
    def calculate_total_credit(invoice_accounts):
        total = 0
        for acc in invoice_accounts:
            total += acc['amount']
        return total

    @staticmethod
    def get_distribution_invoice_accounts(invoice_accounts, nb_months, is_last_month):
        accounts = []
        prev_months_counter = (nb_months-1)
        for invoice_account in invoice_accounts:
            if is_last_month:
                _accounts_total = round((invoice_account.amount/nb_months), 2) * prev_months_counter
                amount = round((invoice_account.amount - _accounts_total), 2)
            else:
                amount = invoice_account.amount/nb_months
            accounts.append({'amount': round(amount, 2),
                             'account': invoice_account.account.code,
                             'account_id': invoice_account.account.id,
                             'invoice_account_id': invoice_account.id
                             })
        return accounts

    def get_invoice_accounts(self, invoice_accounts, months, start_date):
        company = self.get_company()
        default_distribution_account = company.default_distribution_account
        current_month = start_date.month
        current_year = start_date.year
        day = start_date.day
        month_counter = 0
        accounts_distribution = {'distributions': {}, 'total_distribution': 0}
        counter = 1
        total_distribution = 0
        for month in range(months):
            _day = day
            _month = current_month + month_counter
            last_day = start_date.replace(year=current_year, month=_month, day=monthrange(current_year, _month)[1])
            if day > last_day.day:
                _day = last_day.day
            start_at = datetime.replace(start_date, current_year, _month, _day)
            is_last = (counter == months)

            distribution_invoice_accounts = self.get_distribution_invoice_accounts(invoice_accounts, months, is_last)
            total_credit = self.calculate_total_credit(distribution_invoice_accounts)
            total_distribution += total_credit
            accounts_distribution['distributions'][counter] = {'day': start_at,
                                                               'accounts': distribution_invoice_accounts,
                                                               'period': month + 1,
                                                               'total': total_credit,
                                                               'distribution_account': default_distribution_account
                                                               }
            if _month == 12:
                current_month = 1
                month_counter = 0
                current_year = current_year + 1
            else:
                month_counter += 1
            counter += 1
        accounts_distribution['total_distribution'] = total_distribution
        return accounts_distribution

    def get_company(self):
        return Company.objects.select_related(
            'default_distribution_account'
        ).get(
            pk=self.request.session['company']
        )

    def get_distribution(self, filter_options):
        return Distribution.objects.select_related('company').prefetch_related('accounts',
                                                                               'accounts__account',
                                                                               'accounts__period',
                                                                               'accounts__ledger_line'
                                                                               ).get(pk=self.kwargs['pk'])

    def get_accounts(self, distribution):
        return InvoiceAccount.objects.filter(distribution=distribution)

    def get_distribution_accounts(self, distribution, linked_models):
        distr_acts = {}
        total_credit = 0
        total_debit = 0
        accounts = distribution.accounts.select_related(
            'period',
            'period__period_year'
        ).filter(
            ledger_line__isnull=True
        ).order_by(
            'period__period'
        ).all()
        for distribution_account in accounts:
            if distribution_account.period and distribution_account.period.period_year:
                period_id = distribution_account.period.id
                year_id = distribution_account.period.period_year.id
                distr_account = self.get_distribution_account(distribution_account, linked_models)
                account = distr_account['account']
                if year_id in distr_acts:
                    if period_id in distr_acts[year_id]['periods']:
                        if account.distribution_date in distr_acts[year_id]['periods'][period_id]['accounts']:
                            distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date].append(distr_account)
                            total_credit += account.credit
                            total_debit += account.debit
                        else:
                            distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date] = [distr_account]
                            total_credit += account.credit
                            total_debit += account.debit
                    else:
                        distr_acts[year_id]['periods'][period_id] = {
                            'accounts': collections.OrderedDict(), 'period': account.period}
                        distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date] = [distr_account]
                        total_credit += account.credit
                        total_debit += account.debit
                else:
                    distr_acts[year_id] = {'year': account.period.period_year, 'periods': collections.OrderedDict()}
                    distr_acts[year_id]['periods'][period_id] = {
                        'accounts': collections.OrderedDict(),
                        'period': account.period
                    }
                    distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date] = [distr_account]
                    total_credit += account.credit
                    total_debit += account.debit
        return {'distributions':  distr_acts, 'total_credit': total_credit, 'total_debit': total_debit}

    def get_distribution_account(self, distribution_account, linked_models):
        distr_acc = {'account': distribution_account, 'object_items': {}}
        for model_id, linked_model in linked_models.items():
            distr_acc['object_items'][model_id] = ''

        if distribution_account.invoice_account:
            invoice_account = distribution_account.invoice_account
            for object_item in invoice_account.object_items.all():
                distr_acc['object_items'][object_item.company_object_id] = object_item.__str__();

        return distr_acc

    @staticmethod
    def get_company_linked_modes(company):
        linked_models = {}
        count = 0
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            if count < 2:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
                    company_object_id=company_object.id)
            count += 1
        return linked_models

    def get_context_data(self, **kwargs):
        context = super(DistributionBalanceBreakdownView, self).get_context_data(**kwargs)
        company = self.get_company()
        options = self.get_filter_options(company)

        linked_models = self.get_company_linked_modes(company)

        distribution = self.get_distribution(options)
        distributions = self.get_distribution_accounts(distribution, linked_models)
        context['linked_models'] = linked_models
        context['distribution'] = distribution
        context['distributions'] = distributions['distributions']
        context['total_debit'] = distributions['total_debit']
        context['total_credit'] = distributions['total_credit']
        return context


class DistributionDetailView(LoginRequiredMixin, TemplateView):
    template_name = 'journaldistribution/detail.html'

    @staticmethod
    def calculate_total_credit(invoice_accounts):
        total = 0
        for acc in invoice_accounts:
            total += acc['amount']
            yield total

    @staticmethod
    def get_distribution_invoice_accounts(invoice_accounts, nb_months, is_last_month):
        accounts = []
        prev_months_counter = (nb_months-1)
        for invoice_account in invoice_accounts:
            if is_last_month:
                _accounts_total = round((invoice_account.amount/nb_months), 2) * prev_months_counter
                amount = round((invoice_account.amount - _accounts_total), 2)
            else:
                amount = invoice_account.amount/nb_months
            accounts.append({'amount': round(amount, 2),
                             'account': invoice_account.account.code,
                             'account_id': invoice_account.account.id,
                             'invoice_account_id': invoice_account.id
                             })
            yield accounts

    def get_invoice_accounts(self, invoice_accounts, months, start_date):
        company = self.get_company()
        default_distribution_account = company.default_distribution_account
        current_month = start_date.month
        current_year = start_date.year
        day = start_date.day
        month_counter = 0
        accounts_distribution = {'distributions': {}, 'total_distribution': 0}
        counter = 1
        total_distribution = 0
        for month in range(months):
            _day = day
            _month = current_month + month_counter
            last_day = start_date.replace(year=current_year, month=_month, day=monthrange(current_year, _month)[1])
            if day > last_day.day:
                _day = last_day.day
            start_at = datetime.replace(start_date, current_year, _month, _day)
            is_last = (counter == months)

            distribution_invoice_accounts = self.get_distribution_invoice_accounts(invoice_accounts, months, is_last)
            total_credit = self.calculate_total_credit(distribution_invoice_accounts)
            total_distribution += total_credit
            accounts_distribution['distributions'][counter] = {'day': start_at,
                                                               'accounts': distribution_invoice_accounts,
                                                               'period': month + 1,
                                                               'total': total_credit,
                                                               'distribution_account': default_distribution_account
                                                               }
            if _month == 12:
                current_month = 1
                month_counter = 0
                current_year = current_year + 1
            else:
                month_counter += 1
            counter += 1
        accounts_distribution['total_distribution'] = total_distribution
        return accounts_distribution

    def get_company(self):
        return Company.objects.select_related(
            'default_distribution_account'
        ).get(
            pk=self.request.session['company']
        )

    def get_distribution(self):
        return Distribution.objects.select_related(
            'company'
        ).prefetch_related(
            'accounts',
            'accounts__account',
            'accounts__period',
            'accounts__ledger_line'
        ).get(pk=self.kwargs['pk'])

    def get_accounts(self, distribution):
        return InvoiceAccount.objects.filter(distribution=distribution)

    def get_distribution_accounts(self, distribution, linked_models):
        distr_acts = {}
        total_credit = 0
        total_debit = 0
        accounts = DistributionAccount.objects.select_related(
            'distribution',
            'period',
            'period__period_year',
            'ledger_line',
            'ledger_line__journal',
            'invoice_account',
            'account'
        ).order_by(
            'period__period'
        ).filter(
            distribution=distribution
        )
        for distribution_account in accounts:
            if distribution_account.period and distribution_account.period.period_year:
                period_id = distribution_account.period.id
                year_id = distribution_account.period.period_year.id
                distr_account = self.get_distribution_account(distribution_account, linked_models)
                account = distr_account['account']
                if year_id in distr_acts:
                    if period_id in distr_acts[year_id]['periods']:
                        if account.distribution_date in distr_acts[year_id]['periods'][period_id]['accounts']:
                            distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date].append(distr_account)
                            total_credit += account.credit
                            total_debit += account.debit
                        else:
                            distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date] = [distr_account]
                            total_credit += account.credit
                            total_debit += account.debit
                    else:
                        distr_acts[year_id]['periods'][period_id] = {
                            'accounts': collections.OrderedDict(), 'period': account.period}
                        distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date] = [distr_account]
                        total_credit += account.credit
                        total_debit += account.debit
                else:
                    distr_acts[year_id] = {'year': account.period.period_year, 'periods': collections.OrderedDict()}
                    distr_acts[year_id]['periods'][period_id] = {
                        'accounts': collections.OrderedDict(),
                        'period': account.period
                    }
                    distr_acts[year_id]['periods'][period_id]['accounts'][account.distribution_date] = [distr_account]
                    total_credit += account.credit
                    total_debit += account.debit
        return {'distributions':  distr_acts, 'total_credit': total_credit, 'total_debit': total_debit}

    def get_distribution_account(self, distribution_account, linked_models):
        distr_acc = {'account': distribution_account, 'object_items': {}}
        for model_id, linked_model in linked_models.items():
            distr_acc['object_items'][model_id] = ''

        if distribution_account.invoice_account:
            invoice_account = distribution_account.invoice_account
            for object_item in invoice_account.object_items.all():
                distr_acc['object_items'][object_item.company_object_id] = object_item.__str__();

        return distr_acc

    @staticmethod
    def get_company_linked_modes(company):
        linked_models = {}
        count = 0
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            if count < 2:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
                    company_object_id=company_object.id)
            count += 1
        return linked_models

    def get_context_data(self, **kwargs):
        context = super(DistributionDetailView, self).get_context_data(**kwargs)
        distribution = self.get_distribution()

        linked_models = self.get_company_linked_modes(distribution.company)
        distributions = self.get_distribution_accounts(distribution, linked_models)

        context['distribution'] = distribution
        context['distributions'] = distributions['distributions']
        context['total_debit'] = distributions['total_debit']
        context['total_credit'] = distributions['total_credit']
        context['linked_models'] = linked_models
        return context


class GenerateDistributionView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name ='journaldistribution/distribution_accounts.html'

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def get_journal_line(self):
        return JournalLine.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(GenerateDistributionView, self).get_context_data(**kwargs)
        form_kwargs = {
            'request': self.request,
            'profile': self.get_profile(),
            'role': self.get_role()
        }
        account_line_form = forms.AccountDistributeForm(data=self.request.GET, **form_kwargs)
        if account_line_form.is_valid():
            months = account_line_form.cleaned_data['nb_months']
            start_date = account_line_form.cleaned_data['starting_date']
            company = self.get_company()

            periods = self.get_periods(company)
            accounts_distribution = services.distribute_journal_line(company, months, start_date, periods, self.get_journal_line())

            context['months'] = months
            context['start_date'] = start_date
            context['distributions'] = accounts_distribution['distributions']
            context['total_distribution'] = accounts_distribution['total_distribution']
        else:
            print(account_line_form.errors)
        return context


class GenerateLineDistributionView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name ='journaldistribution/distribution_accounts.html'

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def get_account(self):
        return Account.objects.get(pk=self.request.GET.get('account_id'))

    def get_context_data(self, **kwargs):
        context = super(GenerateLineDistributionView, self).get_context_data(**kwargs)
        form_kwargs = {
            'request': self.request,
            'profile': self.get_profile(),
            'role': self.get_role()
        }
        account_line_form = forms.AccountDistributeForm(data=self.request.GET, **form_kwargs)
        if account_line_form.is_valid():
            months = account_line_form.cleaned_data['nb_months']
            start_date = account_line_form.cleaned_data['starting_date']
            company = self.get_company()
            amount = account_line_form.cleaned_data['total_amount']
            is_credit = True if self.request.GET.get('credit_amount') else False

            periods = self.get_periods(company)
            accounts_distribution = services.distribute_line(company, months, start_date, periods, self.get_account(),
                                                             amount, is_credit)

            context['months'] = months
            context['start_date'] = start_date
            context['distributions'] = accounts_distribution['distributions']
            context['total_distribution'] = accounts_distribution['total_distribution']
        else:
            print(account_line_form.errors)
        return context


class DistributionJournalView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'journaldistribution/invoice_journal.html'

    def get_journal_invoice_accounts(self, journal, journal_invoices):
        invoice_accounts = InvoiceAccount.objects.filter(
            invoice_id__in=[journal.invoice_id for journal in journal_invoices],
            is_holding_account=True
        ).all()

        return invoice_accounts

    def get_journal(self):
        return Journal.objects.get(pk=self.kwargs['pk'])

    def get_role_profiles(self, journal):
        role_profiles = []
        if journal.role:
            for profile in journal.role.profile_set.all():
                role_profiles.append("{} {}".format(profile.user.first_name, profile.user.last_name))
        return ', '.join(role_profiles)

    def process_report(self, journal):
        journal_invoices = InvoiceJournal.objects.filter(journal=journal)
        return journal_invoices

    def get_context_data(self, **kwargs):
        context = super(DistributionJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        journal = self.get_journal()

        context['company'] = company
        context['journal'] = journal

        supplier_account = None
        expenditure_account = None
        vat_account = None
        discount_received_account = None

        report = DistributionJournal(journal)
        report.process_report()

        if company.default_vat_account:
            vat_account = company.default_vat_account.code
        if company.default_expenditure_account:
            expenditure_account = company.default_expenditure_account.code
        if company.default_supplier_account:
            supplier_account = company.default_supplier_account.code
        if company.default_discount_received_account:
            discount_received_account = company.default_discount_received_account.code

        context['supplier_account'] = supplier_account
        context['expenditure_account '] = expenditure_account
        context['credit_account'] = supplier_account
        context['vat_account'] = vat_account
        context['discount_received_account'] = discount_received_account

        role_profiles = self.get_role_profiles(journal)

        context['role_profiles'] = role_profiles
        context['report'] = report
        return context


class CreateJournalForDistributionView(LoginRequiredMixin, ProfileMixin, View):
    def generate_unique_number(self, journal_key, company, counter=1):
        profile = Profile.objects.get(pk=self.request.session['profile'])
        counter_str = str(counter).rjust(5, '0')
        company_code = (company.code[0:3]).upper()
        user_code = (profile.user.first_name[0:2]).upper()
        unique_number = "{}-{}-{}-{}".format(company_code, user_code, journal_key, counter_str)

        result = get_object_or_None(Journal, report_unique_number=unique_number)
        if result:
            counter = counter + 1
            return self.generate_unique_number(journal_key, company, counter)
        else:
            return unique_number

    def get_invoices(self, company, period):
        invoices = Invoice.objects.select_related('status',
                                                  'invoice_type',
                                                  'supplier',
                                                  'company').filter(status__slug__in=['paid',
                                                                                      'printed-status-still-in-the-flow'
                                                                                      ],
                                                                    period=period,
                                                                    company=company).exclude(distribution_journal=True)
        return invoices

    def create_journal(self, company, role):
        unique_number = self.generate_unique_number('JDI', company, 1)
        journal = Journal.objects.create(
            report_unique_number=unique_number,
            journal_type=Journal.DISTRIBUTION,
            created_by=self.request.user.profile,
            company=company,
            role=role
        )
        return journal

    def get(self, request, *args, **kwargs):
        if 'period' in self.request.GET and self.request.GET['period']:
            try:
                to_invoice_status = Status.objects.get(slug='paid-printed')
                period = self.request.GET['period']
                company = self.get_company()
                if to_invoice_status:
                    invoices = self.get_invoices(company, period)
                    if invoices:
                        role = Role.objects.get(pk=self.request.session['role'])
                        journal = self.create_journal(company, role)
                        for invoice in invoices:
                            invoice.distribution_journal = True
                            invoice.save()

                            InvoiceJournal.objects.create(
                                invoice=invoice,
                                journal=journal,
                                invoice_status=invoice.status,
                                vat_amount=invoice.vat_amount,
                                net_amount=invoice.net_amount,
                                total_amount=invoice.total_amount,
                                invoice_type=invoice.invoice_type,
                                supplier=invoice.supplier,
                                vat_number=invoice.vat_number,
                                accounting_date=invoice.accounting_date,
                                invoice_date=invoice.invoice_date,
                                supplier_number=invoice.supplier_number
                            )
                        return JsonResponse({'error': False,
                                             'text': 'Journal for distribution successfully created',
                                             'redirect': reverse('journal_for_distribution_invoice')
                                             })
                    else:
                        return JsonResponse({'error': True,
                                             'text': 'No invoices to process',
                                             'redirect': reverse('journal_for_distribution_invoice')
                                             })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'Error occurred updating the invoices',
                                     'details': "Exception {}".format(exception)
                                     })
        else:
            return JsonResponse({'error': True,
                                 'text': 'Please select the period'
                                 })


class JournalForDistributionInvoiceView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'journaldistribution/journal.html'

    def get_periods(self, company):
        return Period.objects.filter(company=company, is_closed=False)

    def get_context_data(self, **kwargs):
        context = super(JournalForDistributionInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['journals'] = self.get_journals(company)
        context['periods'] = self.get_periods(company)
        return context

    def get_journals(self, company):
        return Journal.objects.filter(journal_type=Journal.DISTRIBUTION, company=company,
                                      period__period_year__is_active=True)


class CaptureDistributionJournalView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, TemplateView):
    template_name = 'journaldistribution/capture.html'

    def get_statuses(self):
        statuses = ['final-signed', 'in-the-flow', 'end-flow-printed', 'paid', 'printed-status-still-in-the-flow',
                    'paid-printed']
        return Status.objects.filter(slug__in=statuses).all()

    def display_statuses(self, statuses, selected_statuses):
        completed = ['end-flow-printed', 'paid', 'paid-printed']
        processing = processing_statuses()

        statuses_list = []
        for _completed_status in completed:
            if 'invoice__status__slug__in' in selected_statuses and _completed_status in selected_statuses['invoice__status__slug__in']:
                statuses_list.append('completed')

        for _processing_status in processing:
            if 'invoice__status__slug__in' in selected_statuses and _processing_status in selected_statuses['invoice__status__slug__in']:
                statuses_list.append('processing')
        return statuses_list

    def get_filter_options(self, company):
        options = {'company': company}
        invoice_statuses = []
        completed = ['end-flow-printed', 'paid', 'paid-printed']
        processing = processing_statuses()
        if 'status' in self.request.GET and self.request.GET['status']:
            for status in self.request.GET.getlist('status'):
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        else:
            invoice_statuses = completed
        if len(invoice_statuses) > 0:
            options['invoice__status__slug__in'] = invoice_statuses
        return options

    def get_context_data(self, **kwargs):
        context = super(CaptureDistributionJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()

        statuses = self.get_statuses()
        options = self.get_filter_options(company)

        report = DistributionBalance(company)
        report.process_report(options)

        context['options'] = options
        context['report'] = report
        context['company'] = company
        context['report_date'] = datetime.now
        context['year'] = year
        context['statuses'] = self.display_statuses(statuses, options)
        return context


class DistributionBalanceView(LoginRequiredMixin, SuccessMessageMixin, TemplateView):
    template_name = 'journaldistribution/balance.html'

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_statuses(self):
        sts = ['final-signed', 'in-the-flow', 'end-flow-printed', 'paid', 'printed-status-still-in-the-flow',
               'paid-printed']
        return Status.objects.filter(slug__in=sts).all()

    def display_statuses(self, statuses, selected_statuses):
        completed = ['final-signed', 'end-flow-printed', 'paid', 'paid-printed']
        processing = processing_statuses()

        statuses_list = []
        for _completed_status in completed:
            if 'invoice__status__slug__in' in selected_statuses:
                if _completed_status in selected_statuses['invoice__status__slug__in']:
                    statuses_list.append('completed')

        for _processing_status in processing:
            if 'invoice__status__slug__in' in selected_statuses:
                if _processing_status in selected_statuses['invoice__status__slug__in']:
                    statuses_list.append('processing')
        statuses_list = set(statuses_list)
        return statuses_list

    def get_filter_options(self, company):
        options = {'company': company}
        invoice_statuses = []
        completed = ['final-signed', 'end-flow-printed', 'paid', 'paid-printed']
        processing = processing_statuses()
        if 'status' in self.request.GET and self.request.GET['status']:
            for status in self.request.GET.getlist('status'):
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        #else:
            #invoice_statuses = completed
        if len(invoice_statuses) > 0:
            options['invoice__status__slug__in'] = invoice_statuses
        return options

    def get_year(self, company):
        return Year.objects.filter(company=company, is_open=True, is_active=True).first()

    def get_context_data(self, **kwargs):
        context = super(DistributionBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year(company)

        statuses = self.get_statuses()
        options = self.get_filter_options(company)

        report = DistributionBalance(company)
        report.process_report(options)

        context['options'] = options
        context['report'] = report
        context['company'] = company
        context['report_date'] = datetime.now
        context['year'] = year
        context['statuses'] = self.display_statuses(statuses, options)
        return context
