from django.apps import AppConfig


class JournalsConfig(AppConfig):
    name = 'docuflow.apps.journals'

    def ready(self):
        import docuflow.apps.journals.signal_receivers  # noqa
