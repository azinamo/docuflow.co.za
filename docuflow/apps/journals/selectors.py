from datetime import timedelta

from django.db.models import F, Q, Sum, Case, When, DecimalField, Count

from .models import Journal, JournalLine, OpeningBalance
from docuflow.apps.company.models import Company, Account
from docuflow.apps.period.models import Year
from docuflow.apps.company.enums import ReportType


def get_year_journals(company, year):
    return Journal.objects.filter(
        company=company, year=year
    ).annotate(
        has_been_edited=Count('lines__logs')
    ).select_related(
        'period', 'created_by', 'created_by__user'
    ).order_by(
        '-created_at'
    )


def get_balance_sheet_report_type_total_opening(year: Year, report_type: str):
    return OpeningBalance.objects.values(
        'account__report__slug'
    ).annotate(
        total=Sum('amount')
    ).filter(
        company=year.company,
        year=year,
        account__report__account_type=ReportType.BALANCE_SHEET,
        account__report__slug=report_type
    ).exclude(
        amount__exact=0
    ).values(
        'account__report__slug', 'total'
    ).order_by().aggregate(report_type_total=Sum('total'))


def get_report_lines(year: Year, report_type: str, account: Account):
    return JournalLine.objects.select_related(
        'account', 'account__report', 'journal', 'journal__year', 'journal__company'
    ).filter(
        accounting_date__range=[year.start_date, year.end_date], journal__company=year.company,
        journal__year=year, account__report__account_type=ReportType.BALANCE_SHEET,
        account__report__slug=report_type
    ).order_by(
        '-account__report__account_type', 'account__report__sorting', 'account__code', 'journal__date',
        '-accounting_date', 'journal__period__period',
    )


def get_period_amount_filter(start_date, end_date):
    q_filter = Q()
    q_filter.add(Q(journal__date__gte=start_date), Q.AND)
    q_filter.add(Q(journal__date__lte=end_date), Q.AND)
    return q_filter


def get_incoming_amount_filter(start_date):
    start_date = start_date - timedelta(days=1)
    q_filter = Q()
    q_filter.add(Q(journal__date__lte=start_date), Q.AND)
    return q_filter


def balance_sheet_report(company: Company, year: Year, start_date, end_date, account: Account = None):
    period_filter = get_period_amount_filter(start_date, end_date)
    incoming_filter = get_incoming_amount_filter(start_date)
    return JournalLine.objects.values(
        'account__report__slug', 'account__code'
    ).annotate(
        total_incoming=Case(
            When(incoming_filter, then=Sum('debit') + Sum(F('credit') * -1)), output_field=DecimalField()
        ),
        period_total=Case(
            When(period_filter, then=Sum('debit') + Sum(F('credit') * -1)), output_field=DecimalField()
        )
    ).filter(
        journal__company=company, journal__year=year, account__report__account_type=ReportType.BALANCE_SHEET,
        journal__date__lte=end_date, deleted__isnull=True
    ).values(
        'id', 'account_id', 'account__name', 'account__code', 'account__id', 'total_incoming', 'period_total',
        'account__report__name', 'account__report__slug'
    )
