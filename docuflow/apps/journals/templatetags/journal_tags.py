
from django import template

register = template.Library()

from docuflow.apps.journals.utils import get_ledger_types


@register.inclusion_tag('journals/cell/journal_type.html')
def journal_type_cell(row_id, cell, tabs_start, module_url, ledger_type, value=None):
    tab_index = int(row_id) + int(cell) + int(tabs_start)
    return {'row': row_id, 'start': cell, 'module_url': module_url, 'tab_index': tab_index,
            'ledger_type': ledger_type, 'ledgers': get_ledger_types(), 'value': value}


@register.inclusion_tag('journals/cell/supplier.html')
def supplier_cell(row_id, cell, tabs_start):
    tab_index = int(row_id) + int(cell) + int(tabs_start)
    return {'row': row_id, 'tab_index': tab_index}


@register.inclusion_tag('journals/cell/customer.html')
def customer_cell(row_id, cell, tabs_start):
    tab_index = int(row_id) + int(cell) + int(tabs_start)
    return {'row': row_id, 'tab_index': tab_index}


@register.inclusion_tag('journals/cell/account.html')
def account_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row': row_id, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('journals/cell/credit.html')
def credit_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row': row_id, 'tabs_start': tabs_start, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('journals/cell/debit.html')
def debit_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row': row_id, 'tabs_start': tabs_start, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('journals/cell/vat_code.html')
def vat_code_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row': row_id, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('journals/cell/suffix.html')
def suffix_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row': row_id, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('journals/cell/text.html')
def text_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row': row_id, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('journals/cell/object_items.html')
def objects_items_cells(row_id, cell, tabs_start, objects, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row': row_id, 'tab_index': tab_index, 'value': value, 'objects': objects}



