import logging
from datetime import datetime
import pprint

from django.db.models import Q, Sum, Case, When, DecimalField, F

from .models import JournalLine, OpeningBalance

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class IncomeStatement:

    def __init__(self, year, filter_options, start_date, end_date, previous_year, **kwargs):
        logger.info("Kwargs")
        logger.info(kwargs)
        self.start_date = start_date
        self.end_date = end_date
        self.filter_options = filter_options
        self.include_cents = kwargs.get('include_cents', False)
        self.previous_year = previous_year
        self.year = year
        self.previous_year_title = kwargs.get('previous_year_title', 'Whole').lower()

    def get_amount(self, amount):
        if amount:
            amt = float(amount)
            if not self.include_cents:
                return round(amt)
            else:
                return amt
        return 0

    def get_previous_year_filter(self):
        if self.previous_year_title == 'period':
            start_date_str = "{}-{}-{}".format(self.start_date.day, self.start_date.month, self.previous_year.year)
            end_date_str = "{}-{}-{}".format(self.end_date.day, self.end_date.month, self.previous_year.year)
            start_date = datetime.strptime(start_date_str, '%d-%m-%Y').date()
            end_date = datetime.strptime(end_date_str, '%d-%m-%Y').date()
            logger.info("Last year --> {} Start date {} from {}".format(self.previous_year, start_date, start_date_str))
            logger.info("End year --> {} End date {} from {}".format(self.previous_year, end_date, end_date_str))
            return self.get_year_period_filter(start_date, end_date)
        elif self.previous_year_title == 'whole':
            return Case(When(journal__year=self.previous_year, then=Sum('debit') + Sum(F('credit') * -1)),
                        output_field=DecimalField()
                        )
        elif self.previous_year_title == 'accumulated':
            end_date_str = "{}-{}-{}".format(self.start_date.day, self.start_date.month, self.previous_year.year)
            end_date = datetime.strptime(end_date_str, '%d-%m-%Y').date()
            logger.info("End year --> {} End date {} from {}".format(self.previous_year, end_date, end_date_str))

            return self.get_year_accumulated_filter(self.previous_year, end_date)

    def get_year_period_filter(self, start_date, end_date):
        q_period_filter = Q()
        q_period_filter.add(Q(journal__date__gte=start_date), Q.AND)
        q_period_filter.add(Q(journal__date__lte=end_date), Q.AND)

        return self.get_query_filtered_case(q_period_filter)

    def get_year_accumulated_filter(self, year, end_date):
        q_accumulated_filter = Q()
        q_accumulated_filter.add(Q(journal__year=year), Q.AND)
        q_accumulated_filter.add(Q(journal__date__lte=end_date), Q.AND)

        return self.get_query_filtered_case(q_accumulated_filter)

    def get_query_filtered_case(self, q_filter):
        return Case(When(q_filter, then=Sum('debit') + Sum(F('credit') * -1)), output_field=DecimalField())

    def get_journal_lines(self):

        this_year_period_filter = self.get_year_period_filter(self.start_date, self.end_date)

        this_year_accumulated_filter = self.get_year_accumulated_filter(self.year, self.end_date)

        previous_year_filter = self.get_previous_year_filter()

        lines = JournalLine.objects.annotate(
            total_incoming=this_year_accumulated_filter,
            period_total=this_year_period_filter,
            previous_year_total=previous_year_filter
        ).select_related(
            'account', 'journal', 'journal__period', 'vat_code', 'supplier', 'invoice',
            'invoice__invoice_type', 'journal__year'
        ).prefetch_related(
            'object_items'
        ).filter(
            **self.filter_options
        ).order_by(
            'account__code',
            'journal__date',
            '-date',
            'journal__period__period',
        ).values(
            'account_id', 'account__name', 'account__code', 'account__id', 'total_incoming', 'period_total',
            'account__report__name', 'account__report__id', 'previous_year_total'
        )
        logger.info(lines)
        return lines

    def get_opening_balances(self):
        opening_balances = OpeningBalance.objects.select_related(
            'account'
        ).filter(
            company=self.filter_options['journal__company'],
            year=self.year,
            account__report__account_type=self.filter_options['account__report__account_type']
        ).exclude(
            amount__exact=0
        ).order_by(
            'account__code'
        )
        balance_sheet_lines = {}
        for opening_balance in opening_balances:
            line_key = (opening_balance.account.report_id, opening_balance.account.report.name)
            account_key = (opening_balance.account.name, opening_balance.account.code)
            amount = self.get_amount(opening_balance.amount)
            account_line = {'account': opening_balance.account.name,
                            'account_code': opening_balance.account.code,
                            'ingoing': amount,
                            'period_total': 0,
                            'outgoing': amount,
                            'movement': 0,
                            'budget': 0,
                            'current_year': 0,
                            'previous_year': 0,
                            'budget_variance': 0
                            }
            balance_sheet_lines[line_key] = {
                'ingoing': amount,
                'period_total': 0,
                'movement': 0,
                'outgoing': amount,
                'budget': 0,
                'current_year': amount,
                'previous_year': 0,
                'account_lines': {account_key: account_line}
            }
        return balance_sheet_lines

    def execute(self):
        lines = self.get_journal_lines()
        balance_sheet_lines = self.get_opening_balances()
        for journal_line in lines:
            logger.info("{} --> {}({}) -> Current Year={}, Dr: {}, last year {}".format(
                journal_line['account__report__name'], journal_line['account__code'], journal_line['account__name'],
                journal_line['total_incoming'], journal_line['period_total'], journal_line['previous_year_total']
                                                                               ))
            line_key = (journal_line['account__report__id'], journal_line['account__report__name'])
            account_key = (journal_line['account__name'], journal_line['account__code'])
            period_total = self.get_amount(journal_line['period_total'])
            total_incoming = self.get_amount(journal_line['total_incoming'])
            previous_year_amount = self.get_amount(journal_line['previous_year_total'])

            if line_key in balance_sheet_lines:
                outgoing = total_incoming + period_total
                balance_sheet_lines[line_key]['ingoing'] += total_incoming
                balance_sheet_lines[line_key]['period_total'] += period_total
                balance_sheet_lines[line_key]['outgoing'] += outgoing
                balance_sheet_lines[line_key]['previous_year'] += previous_year_amount
                balance_sheet_lines[line_key]['current_year'] += total_incoming

                if account_key in balance_sheet_lines[line_key]['account_lines']:
                    balance_sheet_lines[line_key]['account_lines'][account_key]['ingoing'] += total_incoming
                    balance_sheet_lines[line_key]['account_lines'][account_key]['period_total'] += period_total
                    balance_sheet_lines[line_key]['account_lines'][account_key]['outgoing'] += outgoing
                    balance_sheet_lines[line_key]['account_lines'][account_key]['previous_year'] += previous_year_amount
                    balance_sheet_lines[line_key]['account_lines'][account_key]['current_year'] += total_incoming
                else:
                    account_line = {'account': journal_line['account__name'],
                                    'account_code': journal_line['account__code'],
                                    'ingoing': total_incoming,
                                    'period_total': period_total,
                                    'outgoing': outgoing,
                                    'budget': 0,
                                    'budget_variance': 0,
                                    'current_year': total_incoming,
                                    'previous_year': previous_year_amount
                                    }
                    balance_sheet_lines[line_key]['account_lines'][account_key] = account_line
            else:
                outgoing = total_incoming + period_total
                account_line = {'account': journal_line['account__name'],
                                'account_code': journal_line['account__code'],
                                'ingoing': total_incoming,
                                'current_year': total_incoming,
                                'budget': 0,
                                'budget_variance': 0,
                                'period_total': period_total,
                                'outgoing': outgoing,
                                'previous_year': previous_year_amount
                                }
                balance_sheet_lines[line_key] = {
                    'ingoing': total_incoming,
                    'period_total': period_total,
                    'current_year': total_incoming,
                    'outgoing': outgoing,
                    'budget': 0,
                    'previous_year': previous_year_amount,
                    'account_lines': {account_key: account_line}
                }
        logger.info(balance_sheet_lines)
        return balance_sheet_lines


# class BalanceSheetAsset:
#
#     def __init__(self, journal_line):
#         self.journal_line = journal_line
#         self.amount = self.journal_line.amount
#         self.movement_balance = self.journal_line.amount
#         self.period_balance = self.journal_line.amount
#         self.outgoing_balance = self.journal_line.amount
#
#     def __add__(self, other):
#         if other.amount:
#             self.outgoing_balance = float(self.amount) + float(other.amount)
#         return self.amount

    # @property
    # def outgoing_balance(self):
    #     if other.amount:
    #         self.amount = float(self.amount) + float(other.amount)
    #     return self.amount
    #

class BalanceSheetLiabilities:
    long_term = {}
    turnover_assets = {}
    own_capital = {}

    def __init__(self, company, year):
        self.company = company

    def execute(self):
        pass
