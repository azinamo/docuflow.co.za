import logging
import pprint

from django.urls import reverse
from django.db.models import Sum

from docuflow.apps.distribution.models import Distribution
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.sales.models import Invoice as SalesInvoice
from docuflow.apps.period.models import Period


pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class AccountLedger(object):
    def _init__(self, account):
        self.account = account
        self.incoming_balance = self.get_incoming_balance()
        self.credit = 0
        self.debit = 0
        self.balance = 0
        self.lines = {}

    def process_account_ledger(self):
        pass

    def get_incoming_balance(self):
        self.debit += self.incoming_balance
        return 0

    def add_credit(self, v):
        self.credit += v

    def add_debit(self, v):
        self.debit += v

    def calculate_balance(self):
        self.balance += self.debit - self.credit


class SalesLine(object):

    def __init__(self, journal_line, linked_models, reverse=True):
        self.reverse = reverse
        self.journal_line = journal_line
        self.invoice = self.get_sales_invoice()
        self.account_code = journal_line.account_number
        self.year = journal_line.journal.year
        self.line_id = journal_line.id
        self.period = journal_line.journal.period
        self.created_at = journal_line.journal.created_at
        self.date = self.get_journal_line_date()
        self.invoice_date = self.get_invoice_date()
        self.journal_number = journal_line.journal.journal_number
        self.journal_text = journal_line.journal.journal_text
        self.text = journal_line.text
        self.amount = self.get_amount()
        self.debit = self.get_debit_amount()
        self.net_amount = self.get_net_amount()
        self.vat_amount = self.get_vat_amount()
        self.total_amount = self.get_total_amount()
        self.credit = self.get_credit_amount()
        self.vat_type = journal_line.vat_code
        self.object_links = self.get_object_links(linked_models)
        self.module_name = journal_line.journal.module_name
        self.account_name = journal_line.account_name
        self.supplier = self.get_supplier()
        self.vat_report = journal_line.vat_report
        self.balance = 0
        self.view_url = self.view_url()
        self.is_reverse = journal_line.is_reversal
        self.reference = self.get_reference()
        self.document_type = self.get_document_type()

    def __str__(self):
        return "Sales Journal line "

    def get_reference(self):
        return str(self.invoice)

    def get_document_type(self):
        return self.invoice.invoice_type

    def get_sales_invoice(self):
        return SalesInvoice.objects.select_related(
            'customer',
            'invoice_type'
        ).filter(id=self.journal_line.object_id).first()

    def view_url(self):
        if self.journal_line.content_type.model == 'invoice':
            return reverse('view_sales_invoice', kwargs={'pk': self.journal_line.object_id})
        return ''

    def get_object_links(self, linked_models):
        object_links = {}
        return object_links

    def is_reversal(self):
        if self.reverse is False:
            return False

        return self.journal_line.is_reversal

    def set_balance(self, value):
        self.balance = value

    def get_supplier(self):
        if self.invoice.customer:
            return self.invoice.customer
        elif self.invoice.customer_name:
            return self.invoice.customer_name

    def get_journal_line_date(self):
        if self.invoice:
            return self.invoice.created_at.date()
        elif self.journal_line.journal.date:
            return self.journal_line.journal.date
        else:
            return self.journal_line.created_at.date()

    def get_invoice_date(self):
        if self.invoice:
            return self.invoice.invoice_date
        elif self.journal_line.journal.date:
            return self.journal_line.journal.date
        else:
            return self.journal_line.created_at.date()

    def get_amount(self):
        if self.journal_line.debit:
            return self.journal_line.debit
        elif self.journal_line.credit:
            return self.journal_line.credit * -1

    def get_total_amount(self):
        total_amount = 0
        if self.invoice:
            total_amount = self.invoice.total_amount
            if self.invoice.is_credit:
                total_amount = total_amount * -1
        if self.is_reversal():
            total_amount = total_amount * -1
        return total_amount

    def get_net_amount(self):
        net_amount = 0
        if self.invoice:
            net_amount = self.invoice.net_amount
            if self.invoice.is_credit:
                net_amount = net_amount * -1
        if self.is_reversal():
            net_amount = net_amount * -1
        return net_amount

    def get_vat_amount(self):
        if self.invoice:
            vat_amount = self.invoice.vat_amount
            if self.invoice.is_credit:
                vat_amount = self.invoice.vat_amount * -1
        else:
            vat_amount = self.get_line_vat_amount()
        if self.is_reversal():
            vat_amount = vat_amount * -1
        return vat_amount

    def get_line_vat_amount(self):
        if self.journal_line.debit:
            if self.is_reversal():
                return self.journal_line.debit * -1
            return self.journal_line.debit
        elif self.journal_line.credit:
            if self.is_reversal():
                return self.journal_line.credit * -1
            return self.journal_line.credit * -1

    def get_credit_amount(self):
        if self.journal_line.debit < 0:
            credit_amount = self.journal_line.debit * -1
        elif self.journal_line.credit < 0:
            credit_amount = self.journal_line.credit * -1
        else:
            credit_amount = self.journal_line.credit
        if self.is_reversal():
            credit_amount = credit_amount * -1
        return credit_amount

    def get_debit_amount(self):
        debit_amount = 0
        if self.journal_line.debit >= 0:
            debit_amount = self.journal_line.debit
        if self.is_reversal():
            debit_amount = debit_amount * -1
        return debit_amount


class LedgerLine(object):
    def __init__(self, journal_line, linked_models, reverse=True):
        self.reverse = reverse
        self.invoice = journal_line.get_invoice()
        self.journal_line = journal_line
        self.account_code = journal_line.account_number
        self.year = journal_line.journal.year
        self.line_id = journal_line.id
        self.period = journal_line.journal.period
        self.created_at = journal_line.journal.created_at
        self.date = self.get_journal_line_date()
        self.invoice_date = self.get_invoice_date()
        self.journal_number = journal_line.journal.journal_number
        self.journal_text = journal_line.journal.journal_text
        self.text = journal_line.text
        self.amount = self.get_amount()
        self.debit = self.get_debit_amount()
        self.net_amount = self.get_net_amount()
        self.vat_amount = self.get_vat_amount()
        self.total_amount = self.get_total_amount()
        self.credit = self.get_credit_amount()
        self.vat_type = journal_line.vat_code
        self.object_links = self.get_object_links(linked_models)
        self.module_name = journal_line.journal.module_name
        self.account_name = journal_line.account_name
        self.supplier = self.get_supplier()
        self.vat_report = journal_line.vat_report
        self.balance = 0
        self.view_url = self.view_url()
        self.is_reverse = journal_line.is_reversal
        self.reference = self.get_reference()
        self.document_type = self.get_document_type()

    def __str__(self):
        if self.invoice:
            return str(self.invoice)
        return str(self.journal_line)

    def get_reference(self):
        if self.invoice:
            if hasattr(self.invoice, 'invoice_number'):
                return self.invoice.invoice_number
            else:
                return str(self.invoice)
        return 'Payment'

    def get_document_type(self):
        if self.invoice:
            return self.invoice.invoice_type
        return ''

    def view_url(self):
        if self.journal_line.invoice:
            return reverse('invoice_detail', kwargs={'pk': self.journal_line.invoice.id})
        elif self.journal_line.content_type:
            if self.journal_line.content_type.model == 'invoice':
                return reverse('view_sales_invoice', kwargs={'pk': self.journal_line.object_id})
        return ''

    def get_object_links(self, linked_models):
        object_links = {}
        for key, linked_model in linked_models.items():
            object_links[key] = {'value': None, 'code': None, 'object_item': None}

        for link in self.journal_line.object_items.all():
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.label, 'code': link.code, 'object_item': link}

        return object_links

    def is_reversal(self):
        if self.reverse is False:
            return False
        return self.journal_line.is_reversal

    def set_balance(self, value):
        self.balance = value

    def get_supplier(self):
        if self.journal_line.supplier:
            return self.journal_line.supplier.name
        elif hasattr(self.invoice, 'customer'):
            return self.invoice.customer.name
        elif self.journal_line.invoice and self.journal_line.invoice.supplier:
            return self.journal_line.invoice.supplier.name

    def get_journal_line_date(self):
        if self.journal_line.invoice:
            return self.journal_line.invoice.accounting_date
        elif self.journal_line.journal.date:
            return self.journal_line.journal.date
        else:
            return self.journal_line.created_at.date()

    def get_invoice_date(self):
        if self.invoice:
            return self.invoice.invoice_date
        elif self.journal_line.journal.date:
            return self.journal_line.journal.date
        else:
            return self.journal_line.created_at.date()

    def get_amount(self):
        if self.journal_line.debit:
            return self.journal_line.debit
        elif self.journal_line.credit:
            return self.journal_line.credit * -1

    def get_total_amount(self):
        total_amount = 0
        if self.invoice:
            total_amount = self.invoice.total_amount
        if self.is_reversal():
            total_amount = total_amount * -1
        return total_amount

    def get_net_amount(self):
        net_amount = 0
        if self.invoice:
            if self.journal_line.credit:
                net_amount = self.invoice.net_amount
            else:
                net_amount = self.invoice.net_amount
        if self.is_reversal():
            net_amount = net_amount * -1
        return net_amount

    def get_vat_amount(self):
        if self.invoice:
            vat_amount = self.get_line_vat_amount()
        else:
            vat_amount = self.get_line_vat_amount()
        if self.is_reversal():
            vat_amount = vat_amount * -1
        return vat_amount

    def get_line_vat_amount(self):
        if self.journal_line.debit:
            if self.is_reversal():
                return self.journal_line.debit * -1
            return self.journal_line.debit
        elif self.journal_line.credit:
            if self.is_reversal():
                return self.journal_line.credit * -1
            return self.journal_line.credit * -1

    def get_credit_amount(self):
        if self.journal_line.debit and self.journal_line.debit < 0:
            credit_amount = self.journal_line.debit * -1
        elif self.journal_line.credit and self.journal_line.credit < 0:
            credit_amount = self.journal_line.credit * -1
        else:
            credit_amount = self.journal_line.credit if self.journal_line.credit else 0
        if self.is_reversal():
            credit_amount = credit_amount * -1
        return credit_amount

    def get_debit_amount(self):
        debit_amount = 0
        if self.journal_line.debit and self.journal_line.debit >= 0:
            debit_amount = self.journal_line.debit
        if self.is_reversal():
            debit_amount = debit_amount * -1
        return debit_amount


class DistributionBalance(object):

    def __init__(self, company):
        self.company = company
        self.day_ranges = {}
        self.distributions = {}
        self.total = 0

    def get_distributions(self, filter_options):
        return Distribution.objects.with_pending_accounts(filter_options)

    def process_report(self, options):
        distributions = self.get_distributions(options)

        self.get_distribution_day_range_totals(distributions)

    def get_period(self):
        try:
            heighest_period = Period.objects.filter(company=self.company, is_closed=False).order_by('-period').first()
            return heighest_period.period
        except Exception:
            return None

    def get_distribution_day_range_totals(self, distributions):

        for distribution in distributions:
            totals = {'total': 0, 'name': distribution, 'code': '', 'id': distribution.id}
            for distribution_account in distribution.accounts.all():
                totals['total'] += distribution_account.credit
            self.distributions[distribution.id] = totals
            self.total += totals['total']

