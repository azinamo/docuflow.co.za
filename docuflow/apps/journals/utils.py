from django.urls import reverse_lazy

from .enums import LedgerType


def get_ledger_types():
    return [
        {'name': str(LedgerType.GENERAL), 'value': LedgerType.GENERAL.value, 'is_default': True,
         'data-ajax-url': reverse_lazy('ledger_accounts_list')},
        {'name': str(LedgerType.SUPPLIER), 'value': LedgerType.SUPPLIER.value, 'is_default': False,
         'data-ajax-url': reverse_lazy('suppliers_list')},
        {'name': str(LedgerType.CUSTOMER), 'value': LedgerType.CUSTOMER.value, 'is_default': False,
         'data-ajax-url': reverse_lazy('customers_list')}
    ]
