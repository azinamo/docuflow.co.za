import logging
import pprint
from collections import defaultdict
from decimal import Decimal, InvalidOperation
from openpyxl import load_workbook

import xlrd
from annoying.functions import get_object_or_None
from django import forms
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache

from docuflow.apps.comment.services import create_comments
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Account, VatCode
from docuflow.apps.company.enums import SubLedgerModule
from docuflow.apps.customer.models import Ledger as CustomerLedger
from docuflow.apps.customer.services import age_customer_journal, is_customer_balancing
from docuflow.apps.distribution.forms import DistributionAccountForm
from docuflow.apps.distribution.models import Distribution, DistributionAccount
from docuflow.apps.fixedasset.models import Addition, FixedAsset, OpeningBalance as FixedAssetOpeningBalance, Category
from docuflow.apps.fixedasset.enums import FixedAssetStatus, AdditionType
from docuflow.apps.period.models import Period
from docuflow.apps.supplier.models import Ledger as SupplierLedger
from docuflow.apps.supplier.services import add_journal_to_age_analysis, is_supplier_balancing
from docuflow.apps.system.models import CashFlowCategory
from .enums import LedgerType
from .exceptions import JournalNotBalancingException, JournalException
from .models import Journal, JournalLine, JournalLineLog
from .services import GeneralLedger as JournalLedger

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class ManageJournalLinesMixin(object):

    # noinspection PyUnresolvedReferences
    def create_journal_lines(self, journal: Journal):
        cache_key = f'{self.company.pk}_{self.year.id}_journal_distributions_{self.profile.pk}_{self.role.pk}'
        distributions = cache.get(cache_key, {})

        default_distribution_account = None
        if distributions and len(distributions) > 0:
            default_distribution_account = self.company.default_distribution_account
            if not default_distribution_account:
                raise forms.ValidationError('Default distribution account is not set')

        for field, value in self.request.POST.items():
            if field.startswith('account_', 0, 8):
                row_counter = field[8:]
                journal_line = self.handle_account_line(
                    journal=journal,
                    row_counter=row_counter,
                    distributions=distributions,
                    default_distribution_account=default_distribution_account
                )
                if journal_line:
                    self.total_debit += journal_line.debit if journal_line.debit else 0
                    self.total_credit += journal_line.credit if journal_line.credit else 0
            elif field.startswith('supplier_', 0, 9):
                row_counter = field[9:]
                journal_line = self.handle_supplier_line(journal=journal, row_counter=row_counter)
                if journal_line:
                    self.total_debit += journal_line.debit if journal_line.debit else 0
                    self.total_credit += journal_line.credit if journal_line.credit else 0
            elif field.startswith('customer_', 0, 9):
                row_counter = field[9:]
                journal_line = self.handle_customer_line(journal=journal, row_counter=row_counter)
                if journal_line:
                    self.total_debit += journal_line.debit if journal_line.debit else 0
                    self.total_credit += journal_line.credit if journal_line.credit else 0

        difference = int(round(self.total_debit, 2) - round(self.total_credit, 2))
        logger.info(f"Different is {self.total_debit} - {self.total_credit} => {difference}, from instance {self.instance}")
        if not self.instance:
            if self.total_credit == 0 or self.total_debit == 0:
                raise forms.ValidationError('No general ledger lines were found')
        if difference != 0:
            raise JournalNotBalancingException('Journals did not balance this transaction could  not be saved')

        cache.set(cache_key, {})

    # noinspection PyUnresolvedReferences
    def handle_account_line(self, journal: Journal, row_counter, distributions, default_distribution_account):
        ledger_type = int(float(self.request.POST.get(f"ledger_{row_counter}", 0)))
        content_type = JournalLedger.get_ledger_content_type(ledger_type)
        object_id = self.request.POST.get(f'account_{row_counter}', 0)
        account_id = object_id
        if account_id and object_id:
            account_data = {
                'journal': journal,
                'account': account_id,
                'object_id': object_id,
                'content_type': content_type,
                'vat_code': self.request.POST.get(f'vat_code_{row_counter}', None),
                'debit': self.request.POST.get(f'debit_{row_counter}', 0),
                'credit': self.request.POST.get(f'credit_{row_counter}', 0),
                'ledger_type': ledger_type,
                'accounting_date': journal.date,
                'suffix': self.request.POST.get(f"suffix_{row_counter}", ''),
                'object_items': self.request.POST.getlist(f"object_item_{row_counter}", None),
                'text': self.request.POST.get(f"text_{row_counter}", None)
            }

            journal_line_form = JournalLineForm(data=account_data, company=self.company)
            if journal_line_form.is_valid():
                journal_line = journal_line_form.save()
                if distributions:
                    line_distributions = distributions.get(row_counter)
                    if line_distributions:
                        distribution = self.create_ledger_line_distributions(line_distributions, journal_line)
                        journal_line.distribution = distribution
                        journal_line.account = default_distribution_account
                        journal_line.save()

                asset_category = Category.objects.filter(
                    company=journal.company, account_id=journal_line.account
                ).first()

                asset_addition = getattr(journal_line, 'asset_addition', None)
                fixed_asset = getattr(journal_line, 'fixed_asset', None)
                is_linked = asset_addition or fixed_asset

                if asset_category and not is_linked:
                    assets_data = self.request.session.get('journal_fixed_assets', [])
                    if not assets_data:
                        raise JournalException(
                            f'Account {journal_line.account} is linked to asset category, but fixed asset data was found.')

                    self.create_journal_line_assets(journal_line=journal_line, rows=assets_data)

                return journal_line
            else:
                logger.info(journal_line_form.errors)
        return None

    def create_journal_line_assets(self, journal_line, rows):
        assets = []
        for row in rows:
            row_data = row['fixed_asset_data']
            option_type = row_data['asset_option']
            if option_type in [AdditionType.REVALUATION.value, AdditionType.ADDITION.value, AdditionType.DISPOSAL.value]:
                addition = Addition.objects.create(
                    date=row_data['date'],
                    amount=row_data['value'],
                    fixed_asset_id=row_data['fixed_asset'],
                    name=row_data['name'],
                    journal_line=journal_line,
                    addition_type=AdditionType(option_type)
                )
                assets.append(addition)
            else:
                fixed_asset = FixedAsset.objects.create(
                    company=journal_line.journal.company,
                    quantity=row_data['quantity'],
                    date_purchased=row_data['date'],
                    cost_price=row_data['value'],
                    fixed_asset_id=row_data['fixed_asset'],
                    name=row_data['name'],
                    journal_line=journal_line,
                    category=row_data['category'],
                    location=row_data['location'],
                    asset_number=row_data['asset_number'] or str(journal_line.journal),
                    depreciation_method=row_data['depreciation_method'],
                    depreciation_account=row_data['depreciation_account'],
                    depreciation_rate=row_data['depreciation_rate'],
                    accumulated_depreciation_account=row_data['accumulated_depreciation_account'],
                    asset_account=row_data['asset_account'],
                    status=FixedAssetStatus.ACTIVE
                )
                FixedAssetOpeningBalance.objects.get_or_create(
                    year=journal_line.journal.year,
                    fixed_asset_id=row_data['fixed_asset'],
                    defaults={
                      'asset_value': 0,
                      'depreciation_value': 0
                    }
                )
                Addition.objects.update_or_create(
                    date=row_data['date'],
                    fixed_asset=row_data['fixed_asset'],
                    defaults={
                        'name': row_data['name'],
                        'amount': row_data['value'],
                        'journal_line_id': journal_line.id
                    }
                )
                assets.append(fixed_asset)

    # noinspection PyUnresolvedReferences
    def handle_supplier_line(self, journal: Journal, row_counter):
        logger.info("Handle supplier line")
        ledger_type = int(float(self.request.POST.get(f"ledger_{row_counter}", 0)))
        default_account = JournalLedger.get_default_supplier_account(journal.company)
        object_id = self.request.POST.get(f'supplier_{row_counter}', 0)
        account_id = default_account.pk
        content_type = JournalLedger.get_ledger_content_type(ledger_type)
        if account_id and object_id:
            data = {
                'journal': journal,
                'account': account_id,
                'object_id': object_id,
                'content_type': content_type,
                'vat_code': self.request.POST.get(f'vat_code_{row_counter}', None),
                'debit': self.request.POST.get(f'debit_{row_counter}', 0),
                'credit': self.request.POST.get(f'credit_{row_counter}', 0),
                'ledger_type': ledger_type,
                'accounting_date': journal.date,
                'suffix': self.request.POST.get(f"suffix_{row_counter}", ''),
                'object_items': self.request.POST.getlist(f"object_item_{row_counter}", None),
                'text': self.request.POST.getlist(f"text_{row_counter}", None)
            }
            journal_line_form = SupplierJournalLineForm(data=data, company=self.company)
            if journal_line_form.is_valid():
                journal_line = journal_line_form.save()
                return journal_line
            else:
                logger.info(journal_line_form.errors)
        return None

    # noinspection PyUnresolvedReferences
    def handle_customer_line(self, journal: Journal, row_counter):
        logger.info("Handle customer line")
        ledger_type = int(float(self.request.POST.get(f"ledger_{row_counter}", 0)))
        content_type = JournalLedger.get_ledger_content_type(ledger_type)
        default_account = JournalLedger.get_default_customer_account(journal.company)
        object_id = self.request.POST.get(f'customer_{row_counter}', 0)
        account_id = default_account.pk
        if account_id and object_id:
            data = {
                'journal': journal,
                'account': account_id,
                'object_id': object_id,
                'content_type': content_type,
                'vat_code': self.request.POST.get(f'vat_code_{row_counter}', None),
                'debit': self.request.POST.get(f'debit_{row_counter}', 0),
                'credit': self.request.POST.get(f'credit_{row_counter}', 0),
                'ledger_type': ledger_type,
                'accounting_date': journal.date,
                'suffix': self.request.POST.get(f"suffix_{row_counter}", ''),
                'object_items': self.request.POST.getlist(f"object_item_{row_counter}", None),
                'text': self.request.POST.getlist(f"text_{row_counter}", None)
            }
            journal_line_form = CustomerJournalLineForm(data=data, company=self.company)
            if journal_line_form.is_valid():
                journal_line = journal_line_form.save()
                return journal_line
            else:
                logger.info(journal_line_form.errors)
        return None

    # noinspection PyUnresolvedReferences
    def create_ledger_line_distributions(self, line_distributions, journal_line):
        distribution = Distribution.objects.create(
            **line_distributions['distribution'],
            account=journal_line.account,
            is_credit=True if journal_line.is_credit else False,
            profile=self.profile,
            role=self.role,
            company=self.company,
            content_type=ContentType.objects.get_for_model(journal_line),
            object_id=journal_line.id,
            title=f"{journal_line.journal.journal_text}-{str(journal_line.journal)}"
        )
        if distribution:
            for k, line_distribution in line_distributions['lines'].items():
                for account_id, account_line in line_distribution['accounts'].items():
                    data = {
                        'credit': round(Decimal(account_line['credit']), 2),
                        'debit': round(Decimal(account_line['debit']), 2),
                        'account': int(account_id),
                        'period': int(line_distribution['period']),
                        'distribution': distribution,
                        'date': line_distribution['date']
                    }
                    journal_distribution_form = DistributionAccountForm(data=data, company=self.company)
                    if journal_distribution_form.is_valid():
                        journal_distribution_form.save()
                    else:
                        logger.info(journal_distribution_form.errors)
                        raise JournalException('Error occurred saving distribution line')
            return distribution

    # noinspection PyMethodMayBeStatic
    def delete_from_customer_ledger(self, journal_line):
        customer_ledger = CustomerLedger.objects.filter(
            module=Module.JOURNAL.value, content_type=ContentType.objects.get_for_model(journal_line),
            object_id=journal_line.id
        ).first()
        if customer_ledger:
            customer_ledger.delete()
        # handle supplier age analysis
        age_customer_journal(journal_line=journal_line, is_reverse=True)

    # noinspection PyMethodMayBeStatic
    def delete_from_supplier_ledger(self, journal_line):
        logger.info("Delete supplier ledger")
        supplier_ledger = SupplierLedger.objects.filter(
            content_type=ContentType.objects.get_for_model(journal_line), object_id=journal_line.id,
            journal=journal_line.journal
        ).first()
        if supplier_ledger:
            supplier_ledger.delete()

        # handle supplier age analysis
        add_journal_to_age_analysis(journal_line=journal_line, is_reverse=True)


class JournalForm(ManageJournalLinesMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.total_credit = 0
        self.total_debit = 0
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(JournalForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.total_credit = self.instance.calculate_total_credit()
            self.total_debit = self.instance.calculate_total_debit()
            if self.instance.year.pk == self.year.pk:
                self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
            else:
                self.fields['period'].queryset = Period.objects.filter(pk=self.instance.period_id)
        else:
            self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['year'].queryset = self.fields['year'].queryset.filter(company=self.company)

    class Meta:
        model = Journal
        fields = ('period', 'year', 'date', 'journal_text', 'journal_number')

    def clean(self):
        cleaned_data = super().clean()
        date = self.cleaned_data.get('date')
        period = self.cleaned_data.get('period')

        if self.cleaned_data['year'] and self.cleaned_data['year'].is_closing:
            self.add_error('year', 'Year is closing and editing is not allowed.')

        if not period:
            self.add_error('period', 'Valid period is required')

        if not date:
            self.add_error('date', 'Valid date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and date do not match')
                self.add_error('period', 'Period and date do not match')
        return cleaned_data

    def save(self, commit=True):
        journal = super().save(commit=False)
        journal.company = self.company
        journal.role = self.role
        journal.created_by = self.profile
        if not journal.module:
            journal.module = Module.JOURNAL
        journal.save()

        content_type = ContentType.objects.get_for_model(journal)
        create_comments(self.request, content_type, journal.id, self.profile, self.role)

        self.process_deleted_items()

        self.create_journal_lines(journal)

        if not is_supplier_balancing(company=self.company, year=journal.period.period_year, period=journal.period,
                                     end_date=journal.date):
            raise JournalException('A variation on supplier age and balance sheet found')

        # if not is_customer_balancing(company=self.company, year=journal.period.period_year, period=journal.period,
        #                              end_date=journal.date):
        #     raise JournalException('A variation on customer age and balance sheet found')

        return journal

    def process_deleted_items(self):
        lines = [int(val) for field, val in self.request.POST.items() if field.startswith('delete', 0, 6) and val]
        for journal_line in JournalLine.objects.filter(id__in=lines):
            JournalLineLog.objects.create(
                journal_line_id=journal_line.id,
                comment='Line deleted',
                profile=self.profile,
                role=self.role
            )

            if journal_line.is_supplier_line:
                self.delete_from_supplier_ledger(journal_line)
            if journal_line.is_customer_line:
                self.delete_from_customer_ledger(journal_line)
            if journal_line.debit:
                self.total_debit -= journal_line.debit
            if journal_line.credit:
                self.total_credit -= journal_line.credit
            journal_line.delete()


class JournalLineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(JournalLineForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.company)
        self.fields['journal'].queryset = self.fields['journal'].queryset.filter(company=self.company)
        self.fields['object_items'].required = False

    class Meta:
        model = JournalLine
        fields = ('debit', 'credit', 'text', 'account', 'accounting_date', 'journal', 'suffix',
                  'vat_code', 'object_id', 'content_type', 'ledger_type', 'object_items')

    def save(self, commit=True):
        journal_line = super().save(commit=False)
        journal_line.save()

        self.save_m2m()

        if journal_line.content_type:
            if journal_line.content_type.model == 'supplier':
                supplier_ledger = JournalLedger.add_to_supplier_ledger(journal_line.object_id, journal_line)
                if supplier_ledger:
                    journal_line.text = str(supplier_ledger.supplier)
                    journal_line.save()
            elif journal_line.content_type.model == 'customer':
                customer_ledger = JournalLedger.add_to_customer_ledger(journal_line.object_id, journal_line)
                if customer_ledger:
                    journal_line.text = str(customer_ledger.customer)
                    journal_line.save()

        return journal_line


class SupplierJournalLineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(SupplierJournalLineForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.company)
        self.fields['journal'].queryset = self.fields['journal'].queryset.filter(company=self.company)
        self.fields['object_items'].required = False

    class Meta:
        model = JournalLine
        fields = ('debit', 'credit', 'text', 'account', 'accounting_date', 'journal', 'suffix',
                  'vat_code', 'object_id', 'content_type', 'ledger_type', 'object_items')

    def save(self, commit=True):
        journal_line = super().save(commit=False)
        journal_line.save()

        self.save_m2m()

        supplier_ledger = JournalLedger.add_to_supplier_ledger(journal_line.object_id, journal_line)
        if supplier_ledger:
            journal_line.text = str(supplier_ledger.supplier)
            journal_line.save()

        add_journal_to_age_analysis(journal_line=journal_line)

        return journal_line


class CustomerJournalLineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(CustomerJournalLineForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.company)
        self.fields['journal'].queryset = self.fields['journal'].queryset.filter(company=self.company)
        self.fields['object_items'].required = False

    class Meta:
        model = JournalLine
        fields = ('debit', 'credit', 'text', 'account', 'accounting_date', 'journal', 'suffix',
                  'vat_code', 'object_id', 'content_type', 'ledger_type', 'object_items')

    def save(self, commit=True):
        journal_line = super().save(commit=False)
        journal_line.save()

        self.save_m2m()

        customer_ledger = JournalLedger.add_to_customer_ledger(journal_line.object_id, journal_line)
        if customer_ledger:
            journal_line.text = str(customer_ledger.customer)
            journal_line.save()

        age_customer_journal(journal_line=journal_line)

        return journal_line


class CreateGenerateJournalForm(ManageJournalLinesMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.total_credit = 0
        self.total_debit = 0
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        super(CreateGenerateJournalForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(company=self.company, year=self.year).order_by('-period')
        self.fields['period'].empty_label = None

    journal_number = forms.CharField(required=False, label='Journal Number')
    default_year = forms.CharField(required=False, label='Year')

    class Meta:
        model = Journal
        fields = ('journal_text', 'period', 'date', 'journal_number', 'default_year')
        widgets = {
                    'date': forms.DateInput(attrs={'class': 'datepicker'})
                   }

    def clean(self):
        cleaned_data = super(CreateGenerateJournalForm, self).clean()
        journal_date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not journal_date:
            self.add_error('date', 'Date is required')

        if period and journal_date:
            if not period.is_valid(journal_date):
                self.add_error('date', 'Period and journal date do not match')

        if not self.instance.pk:
            if Journal.objects.filter(year=self.year, company=self.company, journal_number=cleaned_data['journal_number']).exists():
                self.add_error('journal_number', f'Journal with number {cleaned_data["journal_number"]} already exists.')

        return cleaned_data

    def save(self, commit=True):
        journal = super().save(commit=False)
        journal.module = Module.ACCOUNTING
        journal.company = self.company
        journal.year = self.year
        journal.created_by = self.profile
        journal.save()

        self.create_journal_lines(journal=journal)

        create_comments(
            request=self.request,
            content_type=ContentType.objects.get_for_model(journal),
            object_id=journal.id,
            profile=self.profile,
            role=self.role
        )
        if 'journal_fixed_asset_additions' in self.request.session:
            del self.request.session['journal_fixed_asset_additions']
        if 'journal_fixed_assets' in self.request.session:
            del self.request.session['journal_fixed_assets']

        return journal


class JournalDistributeForm(forms.Form):

    month = forms.CharField(required=True, label='How many months', widget=forms.TextInput())
    start_date = forms.DateField(required=True, label='Starting Date',
                                 widget=forms.TextInput(attrs={'class': 'datepicker', 'readonly': 'readonly'}))


class DistributeJournalForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(DistributeJournalForm, self).__init__(*args, **kwargs)
        self.fields['periods'].queryset = Period.objects.open(self.company, self.year)

    periods = forms.ModelMultipleChoiceField(required=True, label='Periods', queryset=None)

    def execute(self):
        for period in self.cleaned_data['periods']:
            accounts = DistributionAccount.objects.pending_update(period)
            if accounts:
                journal = Journal.objects.create(**{
                    'company': self.company,
                    'year': period.period_year,
                    'module': Module.JOURNAL.value,
                    'period': period,
                    'created_by': self.profile,
                    'role': self.role,
                    'date': period.to_date,
                    'journal_text': 'Distribution of Invoices - Report',
                })
                for distribution_account in accounts:
                    line = JournalLine.objects.create(
                        journal=journal,
                        account=distribution_account.account,
                        credit=distribution_account.credit,
                        debit=distribution_account.debit,
                        accounting_date=period.to_date,
                        content_type=ContentType.objects.get_for_model(distribution_account),
                        object_id=distribution_account.id
                    )
                    if line:
                        distribution_account.ledger_line = line
                        distribution_account.save()


class JournalCommentForm(forms.Form):
    comment = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))
    attachment = forms.FileField(required=False, label='Attachment')


class JournalLineAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(JournalLineAdminForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.company)
        self.fields['journal'].queryset = self.fields['journal'].queryset.filter(company=self.company)
        self.fields['object_items'].required = False

    class Meta:
        model = JournalLine
        fields = ('debit', 'credit', 'text', 'account', 'accounting_date', 'journal', 'suffix',
                  'vat_code', 'object_id', 'content_type', 'ledger_type', 'object_items')

    def save(self, commit=True):
        journal_line = super().save(commit=False)
        journal_line.save()

        self.save_m2m()

        customer_ledger = JournalLedger.add_to_customer_ledger(journal_line.object_id, journal_line)
        if customer_ledger:
            journal_line.text = str(customer_ledger.customer)
            journal_line.save()

        return journal_line


class CreateJournalForm(ManageJournalLinesMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.total_credit = 0
        self.total_debit = 0
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(CreateJournalForm, self).__init__(*args, **kwargs)

        self.fields['period'].queryset = Period.objects.for_year(year=self.year).order_by('-period')
        self.fields['year'].queryset = self.fields['year'].queryset.filter(company=self.company)

    class Meta:
        model = Journal
        fields = ('period', 'year', 'date', 'journal_text', 'journal_number')

    def clean(self):
        cleaned_data = super().clean()
        date = self.cleaned_data.get('date')
        period = self.cleaned_data.get('period')

        if not period:
            self.add_error('period', 'Valid period is required')

        if not date:
            self.add_error('date', 'Valid date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and date do not match')
                self.add_error('period', 'Period and date do not match')
        return cleaned_data

    def save(self, commit=True):
        journal = super().save(commit=False)
        journal.company = self.company
        journal.role = self.role
        journal.created_by = self.profile
        journal.module = Module.JOURNAL
        journal.save()

        return journal


class ImportForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        super(ImportForm, self).__init__(*args, **kwargs)

    csv_file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)', required=True)

    def clean(self):
        csv_file = self.cleaned_data['csv_file']
        if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith(
                '.xlsx')):
            self.add_error('csv_file', 'File is not .csv, .xlsx or xls type')

        if csv_file.multiple_chunks():
            self.add_error('csv_file', "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 1000),))

    def execute(self):
        logger.info("Start importing")
        csv_file = self.cleaned_data['csv_file']
        journals_data = []
        if csv_file.name.endswith('.csv'):
            journals_data = self.get_csv_data(csv_file)
        elif csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx'):
            journals_data = self.get_excell_data(csv_file)

        import_result = self.save_journals(journals_data)
        return import_result

    def save_journals(self, data):
        is_updated = 0
        is_new = 0
        total = 0
        for journal_text, journal_lines in data['data'].items():
            journal_data = {
                'journal_text': f"{journal_text}-{journal_lines[0]['text']}" if journal_lines[0]['text'] else journal_text,
                'date': journal_lines[0]['date'],
                'period': journal_lines[0]['period'].id,
                'year': self.year,
                'journal_number': journal_text
            }
            journal_form = CreateJournalForm(data=journal_data, role=self.role, profile=self.profile,
                                             company=self.company, year=self.year)
            if journal_form.is_valid():
                journal = journal_form.save()
                for journal_line in journal_lines:
                    journal_line_data = {
                        'journal': journal.id,
                        'account': journal_line['account'].id,
                        'accounting_date': journal_line['date'],
                        'debit': journal_line['debit'],
                        'credit': journal_line['credit'],
                        'text': journal_line['text'],
                        'ledger_type': LedgerType.GENERAL.value
                    }
                    journal_line_form = JournalLineForm(data=journal_line_data, company=self.company)
                    if journal_line_form.is_valid():
                        journal_line_form.save()
                    else:
                        logger.info(journal_line_form.errors)
                        raise JournalException(f'Errors saving journal {journal_line_form.errors}')

                is_new += 1
            else:
                logger.info(journal_form.errors)
                raise JournalException(f'Errors saving journal {journal_form.errors}')
            total += 1
        return {'new': is_new, 'updated': is_updated, 'total': total}

    # noinspection PyMethodMayBeStatic
    def get_cell_value(self, value, is_money=False):
        if isinstance(value, str):
            value = value.strip().strip('\n')
        if is_money:
            if not value or value == '':
                return 0
            try:
                return Decimal(value).quantize(Decimal('.01'))
            except InvalidOperation as err:
                logger.exception(err)
                logger.debug(f'Amount could not be converted: {value}')
                return Decimal('0').quantize(Decimal('.01'))
        return value

    # noinspection PyMethodMayBeStatic
    def get_excell_data(self, file):
        counter = 0
        data = defaultdict(list)
        errors = []
        book = load_workbook(filename=file)
        sh = book.active
        for row in sh.iter_rows():
            data_dict = {'period': '', 'date': '', 'journal_number': '', 'account': None, 'debit': None, 'credit': '',
                         'text': ''}
            error_dict = {'period': '', 'date': '', 'journal_number': '', 'account': None, 'debit': None, 'credit': '',
                          'text': ''}
            has_error = False
            if counter > 0:
                for c, cell in enumerate(row, start=0):
                    if c == 0:
                        period = get_object_or_None(Period, period=int(cell.value), period_year=self.year)
                        if period:
                            data_dict['period'] = period
                        else:
                            has_error = True
                            error_dict['period'] = int(cell.value)
                    if c == 1:
                        data_dict['date'] = self.get_cell_value(cell.value)
                    if c == 2:
                        data_dict['journal_number'] = self.get_cell_value(cell.value)
                    if c == 3:
                        account = get_object_or_None(Account, code=self.get_cell_value(cell.value), company=self.company)
                        if account:
                            data_dict['account'] = account
                        else:
                            has_error = True
                            error_dict['account'] = self.get_cell_value(cell.value)
                    if c == 4:
                        data_dict['debit'] = self.get_cell_value(value=cell.value, is_money=True)
                    if c == 5:
                        data_dict['credit'] = self.get_cell_value(value=cell.value, is_money=True)
                    if c == 6:
                        data_dict['text'] = self.get_cell_value(cell.value)
                    c += 1
            counter += 1
            if has_error:
                error_dict['date'] = data_dict['date']
                error_dict['text'] = data_dict['text']
                error_dict['credit'] = data_dict['credit']
                error_dict['debit'] = data_dict['debit']
                error_dict['journal_number'] = data_dict['journal_number']
                errors.append(error_dict)
            else:
                if all([data_dict['account'], data_dict['date'], data_dict['period'], (data_dict['credit'] or data_dict['debit'])]):
                    data[data_dict['journal_number']].append(data_dict)
        return {'data': data, 'total': counter, 'errors': errors}

    # noinspection PyMethodMayBeStatic
    def get_csv_data(self, csv_file):
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        counter = 0
        data = []
        for i, line in enumerate(lines):
            if counter > 0:
                fields = line.split(",")
                if len(fields) > 1 and (fields[0] and fields[1]):
                    data_dict = {'name': '', 'code': '', 'account_type': 1, 'report': None, 'sub_ledger': None,
                                 'vat_reporting': None, 'cash_flow': None, 'not_deductable': False}

                    data_dict['code'] = self.get_cell_value(fields[0])
                    data_dict['name'] = self.get_cell_value(fields[1])
                    if fields[2]:
                        period = get_object_or_None(Period, period=self.get_cell_value(fields[3]), period_year=self.year)
                        data_dict['period'] = period
                    if fields[3]:
                        account = get_object_or_None(Account, code=self.get_cell_value(fields[3]), company=self.company)
                        data_dict['account'] = account
                    if fields[4]:
                        sub_ledger = SubLedgerModule.get_value_from_label(self.get_cell_value(fields[4]))
                        data_dict['sub_ledger'] = sub_ledger
                    if fields[5]:
                        vat_report = get_object_or_None(VatCode, label=self.get_cell_value(fields[5]),
                                                        company=self.company)
                        if vat_report:
                            data_dict['vat_reporting'] = vat_report
                    if fields[6]:
                        cash_flow = get_object_or_None(CashFlowCategory, name=self.get_cell_value(fields[6]))
                        if cash_flow:
                            data_dict['cash_flow'] = cash_flow
                    if fields[7]:
                        cell_value = self.get_cell_value(fields[7])
                        data_dict['not_deductable'] = True if cell_value == 'Yes' else False
                    data.append(data_dict)
            counter += 1
        pp.pprint(data)
        return {'account_data': data, 'total': counter}