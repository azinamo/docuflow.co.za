import logging
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.db.models import Sum

from docuflow.apps.common.enums import Module
from docuflow.apps.company.enums import SubLedgerModule
from docuflow.apps.company.models import ObjectItem, Company, Account, VatCode
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.customer.models import Ledger as CustomerLedger
from docuflow.apps.invoice.exceptions import InvalidDefaultAccount
from docuflow.apps.period.models import Year
from docuflow.apps.sales.models import Invoice as CustomerInvoice
from docuflow.apps.supplier.models import Ledger as SupplierLedger
from docuflow.apps.system.enums import AccountType
from .exceptions import JournalNotBalancingException
from .models import JournalLine, Journal, JournalLineLog, OpeningBalance

logger = logging.getLogger(__name__)


def round_up(amount):
    if not isinstance(amount, Decimal):
        amount = Decimal(amount)
    return Decimal(amount.quantize(Decimal('.01')))


class CreateLedger:

    def __init__(self, company, year, period, profile, role, text, journal_lines, date=None, module=Module.PURCHASE):
        logger.info('------------CreateLedger -----------------')
        self.company = company
        self.year = year
        self.period = period
        self.profile = profile
        self.role = role
        self.text = text
        self.journal_lines = journal_lines
        self.date = date
        self.module = module

    def get_journal_type(self):
        if not self.module:
            return Module.PURCHASE
        return self.module

    def create(self):
        journal = Journal.objects.create(
            company=self.company,
            module=self.get_journal_type(),
            year=self.year,
            period=self.period,
            created_by=self.profile,
            role=self.role,
            journal_text=self.text
        )
        if self.date:
            journal.date = self.date
        journal.save()
        logger.info(f"Journal {journal} created ")
        return journal

    def execute(self):
        logger.info('------------EXECUTE -----------------')
        journal = self.create()
        if journal:
            total_debit = Decimal(0)
            total_credit = Decimal(0)
            for line in self.journal_lines:
                journal_line = CreateAccountJournalLine(
                    ledger=journal,
                    account=line.get('account', None),
                    debit=line.get('debit', 0),
                    credit=line.get('credit', 0),
                    text=line.get('text', None),
                    vat_code=line.get('vat_code', None),
                    invoice=line.get('invoice', None),
                    sales_invoice=line.get('sales_invoice', None),
                    object_items=line.get('object_items'),
                    content_type=line.get('content_type'),
                    object_id=line.get('object_id'),
                    suffix=line.get('suffix'),
                    accounting_date=line.get('accounting_date'),
                    ledger_type_reference=line.get('ledger_type_reference'),
                    branch=line.get('branch')
                )
                journal_line.execute()
                if journal_line:
                    if journal_line.debit:
                        total_debit += journal_line.debit
                    if journal_line.credit:
                        total_credit += journal_line.credit
            _total_credit = round_up(total_credit)
            _total_debit = round_up(total_debit)
            difference = _total_credit - _total_debit
            logger.warning(f"Credit -> {_total_credit} vs Debit {_total_debit} -> difference {difference}")
            if difference != 0:
                raise JournalNotBalancingException('Journals did not balance this transaction could not be saved')
        return journal


class UpdateLedger:

    def __init__(self, ledger, text, journal_lines, date=None, module=Module.PURCHASE):
        self.ledger = ledger
        self.text = text
        self.journal_lines = journal_lines
        self.date = date
        self.module = module

    def get_journal_type(self):
        if not self.module:
            return Module.PURCHASE
        return self.module

    def execute(self):
        total_debit = Decimal(0)
        total_credit = Decimal(0)
        for line in self.journal_lines:
            journal_line = CreateAccountJournalLine(self.ledger, line.get('account', None), **line)
            journal_line.execute()
            if journal_line:
                if journal_line.debit:
                    total_debit += journal_line.debit
                if journal_line.credit:
                    total_credit += journal_line.credit
        _total_credit = round_up(total_credit)
        _total_debit = round_up(total_debit)
        difference = _total_credit - _total_debit
        _difference = int(difference)

        logger.warning(f"Total credit --> {_total_credit} vs total debit {_total_debit} -> difference {difference} (Int vat = {_difference})")
        if _difference != 0:
            raise JournalNotBalancingException('Journals did not balance this transaction could  not be saved')


class CreateLedgerLine:

    def __init__(self, ledger):
        self.ledger = ledger

    def get_credit(self, credit, debit):
        credit_amount = 0
        if credit != 0:
            if credit < 0:
                credit_amount = 0
            else:
                credit_amount = credit

        if debit < 0:
            credit_amount += debit * -1
        return credit_amount

    def get_debit(self, debit, credit):
        amount = 0
        if debit != 0:
            if debit < 0:
                amount = 0
            else:
                amount = debit

        if credit < 0:
            amount += credit * -1
        return amount

    def execute(self):
        raise NotImplementedError('Method not implemented')


class CreateAccountJournalLine(CreateLedgerLine):

    def __init__(self,
                 ledger, account, debit=0, credit=0, text='', vat_code=None, invoice=None, object_items=None,
                 content_type=None, object_id=None, accounting_date=None, ledger_type_reference=None, suffix=None,
                 sales_invoice=None, branch=None):
        self.ledger = ledger
        super(CreateAccountJournalLine, self).__init__(ledger)
        self.account = account
        self.debit = self.get_debit(debit, credit)
        self.credit = self.get_credit(credit, debit)
        self.vat_code = vat_code
        self.invoice = invoice
        self.sales_invoice = sales_invoice
        self.text = text
        self.object_items = object_items if object_items else []
        self.content_type = content_type
        self.object_id = object_id
        self.accounting_date = accounting_date
        self.ledger_type_reference = ledger_type_reference
        self.suffix = suffix
        self.branch = branch

    def execute(self):
        if self.credit != 0 or self.debit != 0:
            """
            You cannot show a debit and a credit on one line, must only display EITHER a debit or credit but NEVER both
            """
            debit_amount = self.debit
            credit_amount = self.credit
            if self.debit and self.credit:
                debit_amount = self.debit - self.credit if self.credit < self.debit else 0
                credit_amount = self.credit - self.debit if self.debit < self.credit else 0

            ledger_line = JournalLine.objects.create(
                journal=self.ledger,
                account=self.account,
                debit=debit_amount,
                credit=credit_amount
            )
            if ledger_line:
                if self.ledger_type_reference:
                    ledger_line.ledger_type_reference = self.ledger_type_reference
                if self.vat_code:
                    ledger_line.vat_code = self.vat_code
                if self.invoice:
                    ledger_line.invoice = self.invoice
                    ledger_line.text = str(self.invoice)
                    if not ledger_line.vat_code and self.invoice.vat_code:
                        ledger_line.vat_code = self.invoice.vat_code
                if self.sales_invoice:
                    ledger_line.sales_invoice = self.sales_invoice
                if self.text:
                    ledger_line.text = str(self.text)
                if self.accounting_date:
                    ledger_line.accounting_date = self.accounting_date
                else:
                    ledger_line.accounting_date = self.ledger.date
                if self.content_type and self.object_id:
                    ledger_line.content_type = self.content_type
                    ledger_line.object_id = self.object_id
                if self.suffix:
                    ledger_line.suffix = self.suffix
                if self.branch:
                    ledger_line.branch = self.branch
                ledger_line.save()

                if self.object_items and len(self.object_items) > 0:
                    for object_item in self.object_items:
                        ledger_line.object_items.add(object_item)

                if ledger_line.content_type and ledger_line.content_type.name == 'invoice':
                    invoice = ledger_line.content_object
                    if isinstance(invoice, CustomerInvoice):
                        invoice.journal_line = ledger_line
                        invoice.save()

            return ledger_line
        return None


class CreateSystemLedgerLine(CreateLedgerLine):

    def __init__(self, ledger, supplier, debit=0, credit=0, vat_code=None, content_type=None, object_id=None):
        self.ledger = ledger
        super(CreateSystemLedgerLine, self).__init__()
        self.supplier = supplier
        self.debit = self.get_debit(debit, credit)
        self.credit = self.get_credit(credit, debit)
        self.vat_code = vat_code
        self.content_type = content_type
        self.object_id = object_id

    def execute(self):
        if self.credit != 0 or self.debit != 0:
            ledger_line = JournalLine.objects.create(
                journal=self.ledger,
                supplier=self.supplier,
            )
            if ledger_line:
                ledger_line.credit = self.credit
                ledger_line.debit = self.debit
                if self.vat_code:
                    ledger_line.vat_code = self.vat_code
                if self.content_type and self.object_id:
                    ledger_line.content_type = self.content_type
                    ledger_line.object_id = self.object_id
                ledger_line.save()
            return ledger_line
        return None


class GeneralLedger:

    # noinspection PyMethodMayBeStatic
    def get_float_value_from_post(self, posted_str):
        if posted_str:
            return Decimal(posted_str)
        return 0

    # noinspection PyMethodMayBeStatic
    def get_int_value_from_post(self, posted_str):
        if posted_str:
            return int(float(posted_str))
        return 0

    @classmethod
    def get_ledger_content_type(cls, ledger_type):
        if JournalLine.GENERAL == ledger_type:
            return ContentType.objects.filter(app_label='company', model='account').first()
        elif JournalLine.SUPPLIER == ledger_type:
            return ContentType.objects.filter(app_label='supplier', model='supplier').first()
        elif JournalLine.CUSTOMER == ledger_type:
            return ContentType.objects.filter(app_label='customer', model='customer').first()

    @classmethod
    def get_default_supplier_account(cls, company):
        if not company.default_supplier_account:
            raise InvalidDefaultAccount('Default supplier account is not setup, please ensure its setup before you can '
                                        'run this journal')
        return company.default_supplier_account

    @classmethod
    def get_default_customer_account(cls, company):
        if not company.default_customer_account:
            raise InvalidDefaultAccount('Default customer account is not setup, please ensure its setup before you can '
                                        'run this journal')
        return company.default_customer_account

    @classmethod
    def add_to_customer_ledger(cls, customer_id, journal_line):
        return CustomerLedger.objects.create(
            customer_id=customer_id,
            period=journal_line.journal.period,
            amount=journal_line.amount,
            balance=journal_line.credit if journal_line.is_credit else journal_line.debit * -1,
            text=journal_line.journal.journal_text,
            module=Module.JOURNAL,
            content_type=ContentType.objects.get_for_model(journal_line),
            object_id=journal_line.id,
            date=journal_line.journal.date,
            created_by=journal_line.journal.created_by,
            journal_line=journal_line,
            status=LedgerStatus.COMPLETE
        )

    # noinspection PyMethodMayBeStatic
    def delete_from_customer_ledger(self, journal_line):
        customer_ledger = CustomerLedger.objects.filter(
            module=Module.JOURNAL, content_type=ContentType.objects.get_for_model(journal_line),
            object_id=journal_line.id
        ).first()
        return customer_ledger.delete()

    @classmethod
    def add_to_supplier_ledger(cls, supplier_id, journal_line: JournalLine):
        ledger = SupplierLedger.objects.create(
            content_type=ContentType.objects.get_for_model(journal_line),
            object_id=journal_line.id,
            supplier_id=supplier_id,
            period=journal_line.journal.period,
            debit=journal_line.debit,
            credit=journal_line.credit,
            balance=journal_line.credit if journal_line.is_credit else journal_line.debit * -1,
            text=journal_line.journal.journal_text,
            module=Module.JOURNAL,
            date=journal_line.journal.date,
            created_by=journal_line.journal.created_by,
            journal=journal_line.journal,
            journal_line=journal_line,
            status=SupplierLedger.COMPLETED
        )
        return ledger

    # noinspection PyMethodMayBeStatic
    def delete_from_supplier_ledger(self, journal_line):
        supplier_ledger = SupplierLedger.objects.filter(
            content_type=ContentType.objects.get_for_model(journal_line), object_id=journal_line.id,
            journal=journal_line.journal
        ).first()
        return supplier_ledger.delete()


class CreateGeneralLedger(GeneralLedger):

    def __init__(self, company, profile, year, journal, post_request):
        self.company = company
        self.profile = profile
        self.year = year
        self.journal = journal
        self.post_request = post_request

    def execute(self):
        self.save_journal_lines()

    def save_journal_lines(self):
        total_credit = self.journal.calculate_total_credit()
        total_debit = self.journal.calculate_total_credit()
        logger.info(f"Save journal lines {self.journal}, Dr = {total_debit}, Cr = {total_credit}")
        for field in self.post_request:
            if field.startswith('account_', 0, 8):
                row_counter = field[8:]
                logger.info(f"row key --> {row_counter}")

                ledger_type = int(float(self.post_request.get(f"ledger_{row_counter}", 0)))
                logger.info(f"Ledger type --> {ledger_type}")
                content_type = self.get_ledger_content_type(ledger_type)
                default_account = None
                is_customer_line = False
                is_supplier_line = False
                if content_type:
                    if content_type.model == 'supplier':
                        is_supplier_line = True
                        default_account = self.get_default_supplier_account(self.journal.company)
                    elif content_type.model == 'customer':
                        is_customer_line = True
                        default_account = self.get_default_customer_account(self.journal.company)

                logger.info(f"Content type --> {content_type}")
                account_id = self.post_request.get(f"account_{row_counter}")
                if default_account:
                    account_id = default_account.id
                if account_id:
                    object_id = self.post_request.get('account_{}'.format(row_counter), 0)
                    credit_str = self.post_request.get('credit_{}'.format(row_counter), 0)
                    debit_str = self.post_request.get('debit_{}'.format(row_counter), 0)
                    vat_code_str = self.post_request.get('vat_code_{}'.format(row_counter), None)
                    debit = credit = 0
                    vat_code = None
                    logger.info(f"Credit {credit_str}, Debit {debit_str} and vat code --> {vat_code_str}")
                    if debit_str:
                        debit = self.get_float_value_from_post(debit_str)
                    if credit_str:
                        credit = self.get_float_value_from_post(credit_str)
                    if vat_code_str:
                        vat_code = self.get_int_value_from_post(vat_code_str)
                    account_id = self.get_int_value_from_post(account_id)
                    distribution = self.post_request.get(f"distribution_{row_counter}", False)
                    object_items = self.post_request.getlist(f"object_item_{row_counter}", None)
                    suffix = self.post_request.get(f"suffix_{row_counter}", '')

                    journal_line = JournalLine.objects.create(
                        journal=self.journal,
                        account_id=account_id,
                        object_id=object_id,
                        content_type=content_type,
                        vat_code_id=None if vat_code == 0 else vat_code,
                        debit=debit,
                        credit=credit,
                        ledger_type=ledger_type,
                        accounting_date=self.journal.date,
                        suffix=suffix
                    )
                    total_debit += float(journal_line.debit)
                    total_credit += float(journal_line.credit)

                    if journal_line:
                        for object_item_id in object_items:
                            if object_item_id:
                                object_item = ObjectItem.objects.get(pk=object_item_id)
                                journal_line.object_items.add(object_item)

                    if is_customer_line:
                        logger.info(f"Update customer {object_id} ledger {journal_line}")
                        customer_ledger = self.add_to_customer_ledger(object_id, journal_line)
                        if customer_ledger:
                            journal_line.text = str(customer_ledger.customer)
                            journal_line.save()

                    if is_supplier_line:
                        logger.info(f"Update supplier {object_id} ledger {journal_line}")
                        supplier_ledger = self.add_to_supplier_ledger(object_id, journal_line)
                        if supplier_ledger:
                            journal_line.text = str(supplier_ledger.supplier)
                            journal_line.save()
                    journal_line.save()
        difference = int(round(total_debit, 2) - round(total_credit, 2))
        logger.info(f"Total credit = {total_credit}, total debit {total_debit}, Difference = {difference}")
        if difference != 0:
            raise JournalNotBalancingException('Journals did not balance this transaction could  not be saved')


class UpdateLedgerLines(GeneralLedger):

    def __init__(self, ledger, profile, role, post_request):
        self.ledger = ledger
        self.profile = profile
        self.role = role
        self.post_request = post_request
        self.total_credit = self.ledger.calculate_total_credit()
        self.total_debit = self.ledger.calculate_total_debit()

    def execute(self):
        pass

    def process_deleted_items(self):
        logger.info("process deleted items")
        lines = [int(val) for field, val in self.post_request.items() if field.startswith('delete', 0, 6) and val]

        for line_id in lines:
            journal_line = JournalLine.objects.filter(id=line_id).first()
            logger.info(f"Lines to delete {journal_line}")
            logger.info(lines)
            JournalLineLog.objects.create(
                journal_line_id=line_id,
                comment='Line deleted',
                profile=self.profile,
                role=self.role
            )
            if journal_line.is_supplier_line:
                logger.info("Process supplier line deleted")
                self.delete_from_supplier_ledger(journal_line)
            if journal_line.is_customer_line:
                logger.info("Process customer line deleted")
                self.delete_from_customer_ledger(journal_line)
            if journal_line.debit:
                self.total_debit -= journal_line.debit
            if journal_line.credit:
                self.total_credit -= journal_line.credit
            journal_line.delete()

    def save_journal_lines(self):
        logger.info(" ---- save_journal_lines ---- ")
        logger.info(f"Current total debit {self.total_debit} and current credit is {self.total_credit} -->")
        for field, value in self.post_request.items():
            if field.startswith('account_', 0, 8):
                row_counter = field[8:]
                logger.info(f"row key --> {row_counter}")

                ledger_type = int(float(self.post_request.get('ledger_{}'.format(row_counter), 0)))
                content_type = self.get_ledger_content_type(ledger_type)
                default_account = None
                is_customer_line = False
                is_supplier_line = False
                if content_type:
                    if content_type.model == 'supplier':
                        is_supplier_line = True
                        default_account = self.get_default_supplier_account(self.ledger.company)
                    elif content_type.model == 'customer':
                        is_customer_line = True
                        default_account = self.get_default_customer_account(self.ledger.company)
                account = self.post_request.get('account_{}'.format(row_counter))
                account_id = None
                object_id = None
                if account:
                    account_id = float(account)
                    object_id = account_id
                if default_account:
                    account_id = default_account.id
                if account_id:
                    credit_str = self.post_request.get('credit_{}'.format(row_counter), 0)
                    debit_str = self.post_request.get('debit_{}'.format(row_counter), 0)
                    vat_code_str = self.post_request.get('vat_code_{}'.format(row_counter), None)
                    suffix = self.post_request.get('suffix_{}'.format(row_counter), '')
                    debit = credit = 0
                    vat_code = None
                    if debit_str:
                        debit = self.get_float_value_from_post(debit_str)
                    if credit_str:
                        credit = self.get_float_value_from_post(credit_str)
                    if vat_code_str:
                        vat_code = self.get_int_value_from_post(vat_code_str)
                    distribution = self.post_request.get('distribution_{}'.format(row_counter), False)
                    object_items = self.post_request.getlist('object_item_{}'.format(row_counter), None)
                    text = self.post_request.get('text_{}'.format(row_counter))
                    line = {'debit': debit,
                            'credit': credit,
                            'account_id': account_id,
                            'text': text,
                            'vat_code_id': None if vat_code == 0 else vat_code,
                            'journal': self.ledger,
                            'ledger_type': ledger_type,
                            'object_id': object_id,
                            'content_type': content_type,
                            'accounting_date': self.ledger.date,
                            'suffix': suffix
                            }

                    journal_line = JournalLine.objects.create(**line)

                    self.total_debit += debit
                    self.total_credit += credit

                    if journal_line:
                        for object_item_id in object_items:
                            if object_item_id:
                                object_item = ObjectItem.objects.get(pk=object_item_id)
                                journal_line.object_items.add(object_item)

                    if is_customer_line:
                        ledger = self.add_to_customer_ledger(object_id, journal_line)
                        if ledger:
                            journal_line.text = str(ledger.customer)
                            journal_line.customer_id = object_id

                    if is_supplier_line:
                        ledger = self.add_to_supplier_ledger(object_id, journal_line)
                        if ledger:
                            journal_line.text = str(ledger.supplier)
                            journal_line.supplier_ledger_id = object_id
                    journal_line.save()

                    JournalLineLog.objects.create(
                        journal_line=journal_line,
                        comment='Line created',
                        profile=self.profile,
                        role=self.role
                    )

        logger.info(f"Total credit = {self.total_credit}, total debit {self.total_debit}")
        difference = int(round(self.total_debit, 2) - round(self.total_credit, 2))
        if difference != 0:
            raise JournalNotBalancingException('Journals did not balance this transaction could  not be saved')


class UpdateGeneralJournal(GeneralLedger):

    def __init__(self, journal, profile, role, post_request):
        self.journal = journal
        self.profile = profile
        self.role = role
        self.post_request = post_request

    def delete_journal_lines(self, lines, edited_lines):
        for line in lines:
            if line.id not in edited_lines:
                line.delete()

    def execute(self):
        lines = [line for line in self.journal.lines.all()]
        edited_journals_lines = []
        for field in self.post_request:
            if field[0:8] == 'account_' and self.post_request.get(field) != '':
                counter = field[7:]
                journal_line_id = self.post_request.get("journal_line_id{}".format(counter), None)
                account_id = self.post_request.get("account{}".format(counter), None)
                distribution_str = "distribution{}".format(counter)
                vat_code_str = self.post_request.get("vat_code{}".format(counter), None)
                object_items_key = "object_item{}".format(counter)
                debit_str = self.post_request.get("debit{}".format(counter), 0)
                credit_str = self.post_request.get("credit{}".format(counter), 0)
                debit = credit = 0
                vat_code = None
                object_items = []
                if object_items_key in self.post_request:
                    object_items = self.post_request.getlist(object_items_key)

                if credit_str:
                    credit = float(credit_str)
                if debit_str:
                    debit = float(debit_str)
                if vat_code_str:
                    vat_code = int(vat_code_str)

                if journal_line_id:
                    journal_line = JournalLine.objects.get(pk=int(journal_line_id))
                    edited_journals_lines.append(journal_line.id)
                    journal_line.account_id = int(account_id)
                    if vat_code:
                        journal_line.vat_code_id = int(vat_code)
                    journal_line.debit = debit
                    journal_line.credit = credit
                    journal_line.save()

                    journal_line.object_items.clear()
                    if journal_line:
                        for object_item_id in object_items:
                            if object_item_id:
                                object_item = ObjectItem.objects.get(pk=int(object_item_id))
                                journal_line.object_items.add(object_item)

                else:
                    journal_line = JournalLine.objects.create(
                        journal=self.journal,
                        account_id=int(account_id),
                        vat_code_id=vat_code,
                        debit=debit,
                        credit=credit,
                        accounting_date=self.journal.date
                    )
                    if journal_line:
                        for object_item_id in object_items:
                            if object_item_id:
                                object_item = ObjectItem.objects.get(pk=object_item_id)
                                journal_line.object_items.add(object_item)

        self.delete_journal_lines(lines, edited_journals_lines)


def get_balance_sheet_total_for_account_type(year: Year, company: Company, report_type: str, start_date, end_date):
    journal_total = JournalLine.objects.get_for_report_type(
        year=year,
        company=company,
        report_type=report_type,
        account_type=AccountType.BALANCE_SHEET.value,
        start_date=start_date,
        end_date=end_date
    )
    logger.info(f"Journal lines total {journal_total}")
    opening_total = OpeningBalance.objects.get_for_report_type(
        year=year,
        company=company,
        report_type=report_type,
        account_type=AccountType.BALANCE_SHEET.value
    )
    logger.info(f"Opening balance total {opening_total}")
    account_total = journal_total['total'] if journal_total and journal_total['total'] else 0
    account_total += opening_total['total'] if opening_total and opening_total['total'] else 0

    return account_total


def get_total_for_sub_ledger(company: Company, year: Year, sub_ledger: SubLedgerModule, start_date, end_date):
    journal_total = JournalLine.objects.get_for_sub_ledger(
        year=year, company=company, sub_ledger=sub_ledger, start_date=start_date, end_date=end_date
    ).aggregate(total=Sum('period_total'))

    opening_total = OpeningBalance.objects.get_for_sub_ledger(
        year=year, company=company, sub_ledger=sub_ledger
    ).aggregate(total=Sum('amount'))

    account_total = journal_total['total'] if journal_total and journal_total['total'] else 0
    account_total += opening_total['total'] if opening_total and opening_total['total'] else 0

    return account_total


def year_closing_transaction(journal_line: JournalLine):
    journal = journal_line.journal
    year = journal.year
    started_year = Year.objects.started().filter(company=year.company).first()
    if year.is_closing and started_year:
        journal_line.refresh_from_db()
        logger.info(f"Year {year} closing, move balances to next year")
        default_prior_year_profit = journal.company.default_previous_year_account
        if not default_prior_year_profit:
            raise ValueError('Default prior year profit account, not found')
        logger.info(f"Next Year {started_year}")
        if journal_line.account.is_income_statement:
            amount = journal_line.credit * -1 if journal_line.credit else journal_line.debit
            logger.info(f"Account {journal_line.account} is income statement account add amount of {amount} to {default_prior_year_profit} ")
            opening_balance, is_new = OpeningBalance.objects.get_or_create(
                account=default_prior_year_profit,
                year=started_year,
                company=journal.company
            )
            opening_balance.amount = opening_balance.amount + amount if opening_balance.amount else amount
            opening_balance.save()
        elif journal_line.account.is_balance_sheet:
            amount = journal_line.credit * -1 if journal_line.credit else journal_line.debit
            logger.info(f"Account {journal_line.account} is balance sheet statement account add amount of {amount}  to {journal_line.account} ")
            opening_balance, is_new = OpeningBalance.objects.get_or_create(
                account=journal_line.account,
                year=started_year,
                company=journal.company
            )
            opening_balance.amount = opening_balance.amount + amount if opening_balance.amount else amount
            opening_balance.save()


def get_total_for_account(company: Company, year: Year, account: Account, start_date, end_date):
    journal_total = JournalLine.objects.get_for_account(
        year=year, company=company, account=account, start_date=start_date, end_date=end_date
    )
    try:
        opening_balance = OpeningBalance.objects.get(year=year, company=company, account=account)
        total = opening_balance.amount if opening_balance and opening_balance.amount else 0
    except OpeningBalance.DoesNotExist:
        total = 0
    total += journal_total['total'] if journal_total and journal_total['total'] else 0

    return total


def get_total_for_vat_type(company: Company, year: Year, start_date, end_date):
    journal_total = JournalLine.objects.for_vat_type(
        year=year, company=company, start_date=start_date, end_date=end_date
    )
    try:
        opening_balance = OpeningBalance.objects.filter(
            year=year,
            company=company,
            account__in=VatCode.objects.filter(company=company).values_list('account_id', flat=True)
        ).aggregate(total=Sum('amount'))
        total = opening_balance['total'] if opening_balance and opening_balance['total'] else 0
    except OpeningBalance.DoesNotExist:
        total = 0
    total += journal_total['total'] if journal_total and journal_total['total'] else 0

    return total


def get_total_report_type(company: Company, year: Year, report_type, start_date, end_date):
    journal_total = JournalLine.objects.for_report_type(
        year=year, company=company, report_type=report_type, start_date=start_date, end_date=end_date
    )
    try:
        opening_balance = OpeningBalance.objects.filter(
            year=year, company=company, account__report=report_type
        ).aggregate(total=Sum('amount'))
        total = opening_balance['total'] if opening_balance and opening_balance['total'] else 0
    except OpeningBalance.DoesNotExist:
        total = 0
    total += journal_total['total'] if journal_total and journal_total['total'] else 0

    return total
