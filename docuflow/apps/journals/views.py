import json
import logging
import pprint
from decimal import Decimal

import xlwt
from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.db.models import Sum, F, Prefetch
from django.forms import ValidationError
from django.http import HttpResponse, JsonResponse
from django.urls import reverse, reverse_lazy
from django.utils.timezone import now
from django.views.generic import DetailView, FormView, View, TemplateView
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.common import utils
from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import CompanyMixin, ProfileMixin, AddRowMixin, DataTableMixin
from docuflow.apps.company.models import VatCode, Account
from docuflow.apps.distribution.models import DistributionAccount, Distribution
from docuflow.apps.fixedasset.models import Category
from docuflow.apps.journals.exceptions import JournalException, JournalNotBalancingException
from docuflow.apps.payroll.models import Employee
from docuflow.apps.period.models import Period, Year
from docuflow.apps.supplier.models import Supplier
from . import selectors
from .enums import LedgerType
from .forms import JournalForm, CreateGenerateJournalForm, ImportForm
from .models import Journal, JournalLine, JournalLineLog, OpeningBalance
from .services import UpdateLedgerLines, UpdateGeneralJournal
from .utils import get_ledger_types

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger("journal_logs")


class JournalsView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'journals/index.html'


class CreateJournalView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = 'journals/create.html'
    form_class = CreateGenerateJournalForm
    model = Journal
    def get_initial(self):
        initial = super(CreateJournalView, self).get_initial()
        initial['date'] = now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateJournalView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = self.get_company_object_for_module(company, Module.JOURNAL.value)
        year = self.get_year()
        filter_options = {'year': year, 'company': company}
        journal_str = Journal.get_journal_str(filter_options)
        ledger = Journal()
        ledger_number = ledger.generate_journal_number(filter_options, 1, journal_str)
        context['company'] = company
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['objects'] = objects
        context['journal_number'] = ledger_number
        context['year'] = year
        context['content_type'] = ContentType.objects.get_for_model(ledger)
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Journal could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateJournalView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    form.save()
                    if 'journal_fixed_asset_additions' in self.request.session:
                        del self.request.session['journal_fixed_asset_additions']
                    if 'journal_fixed_assets' in self.request.session:
                        del self.request.session['journal_fixed_assets']

                    return JsonResponse({
                        'text': 'Journal saved successfully',
                        'error': False,
                        'redirect': reverse('journals_index')
                    })
            except (JournalException, ValidationError, JournalNotBalancingException) as exception:
                logger.exception(exception)
                return JsonResponse({
                    'text': str(exception),
                    'modal': True,
                    'error': True,
                    'details': str(exception)
                })
        return HttpResponse("Not allowed")


class ImportJournalView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, FormView):
    form_class = ImportForm
    template_name = 'journals/import.html'
    success_url = reverse_lazy('journals_index')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def form_valid(self, form):
        try:
            with transaction.atomic():
                import_result = form.execute()
                messages.success(self.request, f"{import_result['new']} new accounts successfully imported.")
        except JournalException as e:
            logger.exception(e)
            messages.error(self.request, str(e))
        return super(ImportJournalView, self).form_valid(form)


class JournalLinesMixin(object):

    def base_object_links(self, linked_objects):
        object_links = {}
        for k, linked_object in linked_objects.items():
            object_links[k] = {
                'value': None,
                'label': linked_object['label'],
                'object_items': linked_object['object_items']
            }
        return object_links

    def get_object_links(self, journal_line, linked_models):
        object_links = {}
        for key, linked_model in linked_models.items():
            object_links[key] = {'value': None, 'code': None}

        for link in journal_line.object_items.all():
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.label, 'code': link.code, 'id': link.id}

        return object_links

    def get_lines(self, journal, linked_objects, distributions=None):
        object_links = self.base_object_links(linked_objects)
        distributions = distributions if distributions else {}
        ledger_lines = {'lines': [], 'credit_sum': 0, 'debit_sum': 0, 'difference': 0}
        for ledger_line in journal.lines.all():
            object_items = {}

            for company_object_id, object_link in object_links.items():
                object_items[company_object_id] = {
                    'value': None,
                    'label': object_link['label'],
                    'object_items': object_link['object_items']
                }
                for journal_line_object_item in ledger_line.object_items.all():
                    if journal_line_object_item.company_object_id == company_object_id:
                        object_items[company_object_id]['value'] = journal_line_object_item.code

            ledger_lines['credit_sum'] += ledger_line.credit_amount
            ledger_lines['debit_sum'] += ledger_line.debit_amount

            ledger_lines['lines'].append({
                'line': ledger_line,
                'object_links': object_items,
                'distribution': distributions.get(ledger_line.pk)
            })
        ledger_lines['difference'] = ledger_lines['debit_sum'] - ledger_lines['credit_sum']
        return ledger_lines

    def get_journal_lines_distributions(self, journal_lines):
        distributions = Distribution.objects.filter(
            content_type=ContentType.objects.get_for_model(JournalLine),
            object_id__in=[line.id for line in journal_lines]
        )
        journals_distributions = {}
        for distribution in distributions:
            journals_distributions[distribution.object_id] = distribution
        return journals_distributions


class JournalDetailView(LoginRequiredMixin, ProfileMixin, JournalLinesMixin, DetailView):
    model = Journal
    template_name = 'journals/detail.html'

    def get_queryset(self):
        lines_qs = JournalLine.objects.select_related(
            'account', 'vat_code', 'branch'
        ).prefetch_related(
            'object_items'
        )
        return super().get_queryset().select_related(
            'year', 'period', 'company'
        ).prefetch_related(
            Prefetch('lines', queryset=lines_qs)
        ).filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        company_objects = self.get_company_linked_models(self.get_company())
        journals_distributions = self.get_journal_lines_distributions(journal_lines=self.object.lines.all())
        journal_lines = self.get_lines(
            journal=self.object,
            linked_objects=company_objects,
            distributions=journals_distributions
        )
        context['company'] = self.get_company()
        context['objects'] = company_objects
        context['journal_lines'] = journal_lines
        context['content_type'] = ContentType.objects.get_for_model(JournalLine, False)
        context['ledger_content_type'] = ContentType.objects.get_for_model(Journal, False)
        return context


class EditJournalView(LoginRequiredMixin, ProfileMixin, JournalLinesMixin, UpdateView):
    template_name = 'journals/edit.html'
    form_class = JournalForm
    model = Journal

    def get_queryset(self):
        lines_qs = JournalLine.objects.select_related(
            'account', 'vat_code', 'branch'
        ).prefetch_related(
            'object_items'
        )
        return super().get_queryset().select_related(
            'year', 'period', 'company'
        ).prefetch_related(
            Prefetch('lines', queryset=lines_qs)
        ).filter(company=self.get_company())

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['year'] = self.object.year
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        company_objects = self.get_company_linked_models(company)
        journals_distributions = self.get_journal_lines_distributions(journal_lines=self.object.lines.all())
        journal_lines = self.get_lines(journal=self.object, linked_objects=company_objects, distributions=journals_distributions)
        context['company'] = company
        context['objects'] = company_objects
        context['journal_lines'] = journal_lines
        context['content_type'] = ContentType.objects.get_for_model(JournalLine, False)
        context['ledger_content_type'] = ContentType.objects.get_for_model(Journal, False)
        context['can_edit'] = not self.get_year().is_closing
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred updating journal, please fix the errors.',
                'errors': form.errors,
                'alert': True
            }, status=400)
        return super(EditJournalView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                try:
                    form.save()

                    update_ledger = UpdateLedgerLines(self.get_object(), self.get_profile(), self.get_role(), self.request.POST)
                    update_ledger.execute()

                    return JsonResponse({
                        'text': 'Document saved successfully',
                        'error': False,
                        'reload': True
                    })
                except JournalException as exception:
                    return JsonResponse({
                        'text': str(exception),
                        'error': True
                    })


class LedgerAccountsView(View, CompanyMixin):

    def get_accounts(self):
        accounts = Account.objects.filter(company=self.get_company()).values('id', 'name', 'code')
        return accounts

    def get_suppliers(self):
        suppliers = Supplier.objects.filter(company=self.get_company()).values('id', 'name', 'code')
        return suppliers

    def get_customers(self):
        employees = Employee.objects.filter(company=self.get_company()).values('id', 'name', 'code')
        return employees

    def get(self, request, *args, **kwargs):
        accounts_list = {}
        ledger = int(self.request.GET.get('ledger', 0))
        if ledger == LedgerType.SUPPLIER.value:
            accounts_list = self.get_suppliers()
        elif ledger == LedgerType.GENERAL.value:
            accounts_list = self.get_accounts()
        elif ledger == LedgerType.CUSTOMER.value:
            accounts_list = self.get_customers()

        return HttpResponse(json.dumps(accounts_list))


class ValidatePeriodView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        period = None
        journal_date = self.request.GET['journal_date']
        if self.request.GET['period']:
            period = Period.objects.get(pk=int(self.request.GET['period']))
        if not period:
            return JsonResponse({'text': 'Please select the period', 'error': True})
        if not journal_date:
            return JsonResponse({'text': 'Please select the journal date', 'error': True})

        journal_date = now().strptime(journal_date, '%Y-%m-%d')

        if not period.is_valid(journal_date.date()):
            return JsonResponse({'text': 'Period and journal date do not match', 'error': True, 'alert': True})
        else:
            return JsonResponse({'text': 'Period and journal valid', 'error': False})


class EditGenerateJournalView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = 'journals/edit_general.html'
    form_class = CreateGenerateJournalForm
    model = Journal

    def get_form_kwargs(self):
        kwargs = super(EditGenerateJournalView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.object.year
        return kwargs

    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            'lines', 'lines__account', 'lines__vat_code'
        )

    def get_journal_lines(self, journal, linked_objects):
        journal_lines = []
        total_credit = 0
        total_debit = 0
        for journal_line in journal.lines.order_by('id').all():
            object_links = dict()

            vat_code_id = None
            if journal_line.vat_code_id:
                vat_code_id = journal_line.vat_code_id

            for linked_object_id, linked_object in linked_objects.items():
                object_links[linked_object_id] = {'selected': None, 'label': linked_object['label'],
                                                  'object_items': linked_object['object_items']}

            for journal_line_object_item in journal_line.object_items.all():
                if journal_line_object_item.company_object.id in linked_objects:
                    object_links[journal_line_object_item.company_object.id]['selected'] = journal_line_object_item.id

            if journal_line.account:
                journal_lines.append({
                    'line': journal_line,
                    'reference': "journal_{}".format(journal_line.id),
                    'object_links': object_links
                })
                total_credit += journal_line.credit_amount
                total_debit += journal_line.debit_amount
        difference = total_debit - total_credit
        return {'journal_lines': journal_lines, 'credit': total_credit, 'debit': total_debit, 'difference': difference}

    def get_last_journal_comment(self, journal):
        return None

    def get_context_data(self, **kwargs):
        context = super(EditGenerateJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = self.get_company_linked_models(company)
        year = self.get_year()
        periods = Period.objects.filter(company=company, period_year=year)
        journal = self.object
        lines = self.get_journal_lines(journal, objects)
        ledgers = get_ledger_types()
        comment = self.get_last_journal_comment(journal)

        context['journal_lines'] = lines['journal_lines']
        context['total_credit'] = lines['credit']
        context['total_debit'] = lines['debit']
        context['difference'] = lines['difference']
        context['lines'] = range(journal.lines.count(), 15)
        context['company'] = company
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['accounts'] = Account.objects.filter(company=company)
        context['objects'] = objects
        context['ledgers'] = ledgers
        context['comment'] = comment
        context['year'] = year
        context['periods'] = periods
        context['open_periods'] = [period for period in periods if period.is_closed_period is False]

        context['journal'] = journal
        period_is_open = False
        if journal.period:
            period_is_open = not journal.period.is_closed_period
        context['period_is_open'] = period_is_open
        return context

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                journal = form.save()

                update_journal = UpdateGeneralJournal(journal, self.get_profile(), self.get_role(), form, self.request.POST)
                update_journal.execute()

                return JsonResponse({
                    'text': 'Journal saved successfully',
                    'error': False,
                    'reload': True
                })


class DeleteJournalView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            journal = Journal.objects.get(pk=self.kwargs['pk'])
            journal.delete()
            return JsonResponse({'text': 'Journal deleted successfully', 'error': False, 'reload': True})
        except Journal.DoesNotExist as exception:
            return JsonResponse({
                'text': 'Journal not found',
                'details': str(exception),
                'error': True
            })


class DeleteJournalLineView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            journal_line = JournalLine.objects.get(pk=self.kwargs['pk'])
            journal_line.delete()
            return JsonResponse({
                'text': 'Journal line deleted successfully',
                'error': False,
                'reload': True
            })
        except JournalLine.DoesNotExist as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Journal line does nto exist',
                'details': str(exception),
                'error': True
            })


class AddJournalLineView(LoginRequiredMixin, ProfileMixin, AddRowMixin, TemplateView):
    template_name = 'journals/add_line.html'

    def get_module_url(self):
        if self.request.GET.get('ledger_type'):
            return reverse('add_journal_line') + "?ledger_type=" + self.request.GET.get('invoice_type')
        return reverse('add_journal_line')

    def get_context_data(self, **kwargs):
        context = super(AddJournalLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = self.get_company_object_for_module(company, Module.JOURNAL.value)
        year = self.get_year()
        ledger_type = int(self.request.GET.get('line_type', 0))
        is_reload = True if self.request.GET.get('reload', 0) == '1' else False
        row_id = self.get_row_start()
        row_count = self.get_rows()
        tabs_start = self.get_tabs_start(6, row_id, is_reload)
        ledgers = get_ledger_types()
        col_count = 8 + len(objects)
        journal_rows = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['company'] = company
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['ledgers'] = ledgers
        context['objects'] = objects
        context['year'] = year
        context['start'] = 0
        context['row_start'] = row_id - 1
        context['rows'] = journal_rows
        context['cols'] = col_count
        context['module_url'] = self.get_module_url()
        context['ledger_type'] = ledger_type
        is_edit = self.request.GET.get('edit', False) == '1'
        context['is_edit'] = is_edit
        context['is_reload'] = is_reload
        context['content_type'] = ContentType.objects.get_for_model(JournalLine, False)
        return context


class CopyYearBalanceView(ProfileMixin, View):

    def get_account_opening_balances(self, year: Year):
        return OpeningBalance.objects.select_related(
            'account'
        ).filter(company=year.company, year=year).exclude(amount__exact=0).order_by('account__code')

    def get_journal_lines(self, company, year, filter_options):
        accounts = {}

        for opening_balance in self.get_account_opening_balances(year=year):
            accounts[opening_balance.account] = opening_balance.amount

        for journal_line in JournalLine.objects.select_related('account').filter(**filter_options, account__isnull=False):
            debit = journal_line.debit or 0
            credit = journal_line.credit or 0

            line_amount = debit - credit
            if journal_line.account in accounts:
                accounts[journal_line.account] += line_amount
            else:
                accounts[journal_line.account] = line_amount
        return accounts

    def get_previous_year(self, current_year):
        try:
            y = int(current_year.year) - 1
            return Year.objects.get(year=y, company=current_year.company)
        except Year.DoesNotExist as e:
            logger.debug('Previous year does not exist')
            return None

    def get(self, request, *args, **kwargs):
        year = Year.objects.get(pk=self.kwargs['pk'])

        previous_year = self.get_previous_year(current_year=year)
        if not previous_year:
            return JsonResponse({'text': 'Previous year not found', 'error': True})

        filter_options = dict()
        company = self.get_company()
        filter_options['journal__company'] = company
        filter_options['journal__year'] = previous_year
        journal_lines = self.get_journal_lines(company=company, year=previous_year, filter_options=filter_options)

        OpeningBalance.objects.filter(year=year).update(amount=0)

        opening_balances_to_create = []
        opening_balances_to_update = []
        accounts_balances = {}
        for opening_balance in OpeningBalance.objects.select_related('account').filter(year=year, company=company):
            accounts_balances[opening_balance.account] = opening_balance

        for account, balance in journal_lines.items():
            account_opening_balance = accounts_balances.get(account)
            if account_opening_balance:
                amount = account_opening_balance.amount + balance
                setattr(account_opening_balance, 'amount', amount)
                opening_balances_to_update.append(account_opening_balance)
            else:
                opening_balances_to_create.append(
                    OpeningBalance(
                        account=account,
                        amount=balance,
                        year=year,
                        company=company
                    )
                )
        if opening_balances_to_create:
            OpeningBalance.objects.bulk_create(opening_balances_to_create)

        if opening_balances_to_update:
            OpeningBalance.objects.bulk_update(opening_balances_to_update, {'amount'})

        return JsonResponse({
            'text': 'Opening balances updated successfully', 'error': False,
            'reload': True
        })


class SetupOpeningBalanceView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'opening_balances/index.html'

    def get_opening_balances(self, company, year):
        opening_balances = OpeningBalance.objects.filter(year=year, company=company)
        account_balances = {}
        for opening_balance in opening_balances:
            account_balances[opening_balance.account_id] = opening_balance
        return account_balances

    def get_accounts(self, company, year):
        # TODO - Refactoring the query to use annotations for totals
        balance_sheet_accounts = Account.objects.balance_sheet().filter(company=company)
        opening_balances = self.get_opening_balances(company, year)
        accounts = {}
        total = 0
        for account in balance_sheet_accounts:
            accounts[account.id] = {'id': account.id, 'code': account.code, 'name': account.name, 'opening_balance': 0}
            if account.id in opening_balances:
                accounts[account.id]['opening_balance'] = opening_balances[account.id].amount
                total += opening_balances[account.id].amount
        return accounts, total

    def get_context_data(self, **kwargs):
        context = super(SetupOpeningBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        accounts = self.get_accounts(company, year)

        context['accounts'] = accounts[0]
        context['total'] = accounts[1]
        context['year'] = year
        return context


class UpdateOpeningBalanceView(LoginRequiredMixin, ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            company = self.get_company()
            year = self.get_year()
            account_id = int(self.kwargs['account_id'])
            posted_amount = self.request.POST.get('amount', None)
            if not account_id:
                ctx = {'text': 'Account not found', 'error': True}
            elif not posted_amount:
                ctx = {'text': 'No amount submitted', 'error': True}
            else:
                amount = Decimal(posted_amount)
                opening_balance = get_object_or_None(OpeningBalance, company=company, account_id=account_id, year=year)
                if not opening_balance:
                    opening_balance = OpeningBalance.objects.create(
                        account_id=account_id,
                        amount=amount,
                        year=year,
                        company=company
                    )
                else:
                    opening_balance.amount = amount
                    opening_balance.save()
                ctx = {'text': 'Opening balances updated successfully',
                       'error': False,
                       'amount': opening_balance.amount,
                       'reload': False
                       }
        except Exception as e:
            logger.exception(e)
            ctx = {'text': 'Unexpected error occurred, please try again',
                   'details': str(e),
                   'error': True
                   }

        return JsonResponse(ctx)


class CheckFixedAssetView(LoginRequiredMixin, ProfileMixin, View):

    def post(self, request, **kwargs):
        company = self.get_company()
        year = self.get_year()
        request = self.request
        profile = self.get_profile()
        role = self.get_role()
        journal = None
        if self.request.GET.get('id'):
            try:
                journal = Journal.objects.select_related('year').get(pk=self.request.GET.get('id'))
                year = journal.year
            except Journal.DoesNotExist:
                pass

        journal_form = CreateGenerateJournalForm(
            instance=journal,
            data=self.request.POST,
            company=company,
            year=year,
            profile=profile,
            role=role,
            request=self.request
        )
        if journal_form.is_valid():
            accounts = [int(v) for k, v in request.POST.items() if k[0:7] == 'account' and v != '']

            asset_category = Category.objects.filter(company_id=company, account_id__in=accounts).first()

            if asset_category and asset_category.account:
                return JsonResponse({
                    'text': 'Asset account found',
                    'error': False,
                    'account': str(asset_category.account),
                    'account_id': asset_category.account.id,
                    'create_fixed_asset_url': reverse('create_journal_asset', kwargs={
                        'journal_account_id': asset_category.account.id
                    })
                })
            return JsonResponse({'text': 'No asset account found', 'error': False})
        else:
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving journal, please fix the errors.',
                'errors': journal_form.errors,
                'alert': True
            }, status=400)


class ValidateDeleteLine(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, **kwargs):
        try:
            journal_line = JournalLine.objects.get(pk=self.kwargs['pk'])
            if DistributionAccount.objects.updated().for_model(content_object=journal_line).exists():
                return JsonResponse({
                    'text': 'Journal line cannot be delete. Distributions linked this line already started',
                    'error':  True
                })
            return JsonResponse({
                'text': 'Journal line can be deleted',
                'error': False
            })
        except JournalLine.DoesNotExist:
            return JsonResponse({
                'text': 'Journal line does not exist',
                'error':  True
            })


class LedgerLogView(TemplateView):
    template_name = 'journals/tracking.html'

    def get_context_data(self, **kwargs):
        context = super(LedgerLogView, self).get_context_data(**kwargs)
        logs = JournalLineLog.objects.filter(journal_line__journal__id=self.kwargs['pk'])
        context['logs'] = logs
        return context


class MonthlyReportMixin:

    def can_include_cents(self):
        include_cents = self.request.GET.get('include_cents', False)
        if include_cents == 'on':
            return True
        return include_cents

    def get_filter_options(self, company, year, account_type):
        filter_options = {'journal__company': company, 'journal__year': year,
                          'account__report__account_type': account_type}
        start_date = end_date = None

        if 'from_date' in self.request.GET and self.request.GET['from_date']:
            start_date = now().strptime(self.request.GET['from_date'], '%Y-%m-%d').date()
        if 'to_date' in self.request.GET and self.request.GET['to_date']:
            end_date = now().strptime(self.request.GET['to_date'], '%Y-%m-%d').date()
            filter_options['journal__date__lte'] = end_date

        object_items = self.request.GET.getlist('object_items')
        if object_items:
            items_list = [int(object_item) for object_item in object_items if not object_item.startswith('all', 0, 3)]
            if len(items_list):
                filter_options['object_items__id__in'] = [object_id for object_id in items_list]
        return filter_options, start_date, end_date


class LedgerAccountLinesView(TemplateView):
    template_name = 'journals/report_type_lines.html'

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['year_id'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        account = Account.objects.get(pk=942)
        opening_balance = selectors.get_balance_sheet_report_type_total_opening(year, self.kwargs['slug'])['report_type_total']
        opening_balance_amount = opening_balance['report_type_total'] if opening_balance else 0
        ledger_lines = selectors.get_report_lines(year, self.kwargs['slug'], account)

        balance_sheet_report = selectors.balance_sheet_report(year.company, year, year.start_date, year.end_date, account)
        journal_lines = {}
        missing_in_journal_lines = {}
        for ledger_line in ledger_lines:
            journal_lines[ledger_line.id] = ledger_line
        for line in balance_sheet_report:
            if line['id'] not in journal_lines:
                missing_in_journal_lines[line['id']] = line

        context['lines'] = ledger_lines
        context['year'] = year
        credit = ledger_lines.aggregate(total=Sum(F('credit')))
        debit = ledger_lines.aggregate(total=Sum(F('debit')))
        movement = debit['total'] - credit['total']
        context['total_credit'] = credit['total']
        context['total_debit'] = debit['total']
        context['year_total'] = movement
        context['opening'] = opening_balance
        context['report_type_balance'] =  opening_balance_amount + movement
        return context


class SuffixLinesView(ProfileMixin, TemplateView):
    template_name = 'journals/suffix_lines.html'

    def get_suffix_lines(self):
        qs = JournalLine.objects.filter(journal__company=self.get_company(), journal__year=self.get_year(),
                                        account_id=self.kwargs['account_id'])
        suffix = self.request.GET.get('suffix')
        if suffix == 'blank':
            return qs.filter(suffix__exact='')
        else:
            return qs.filter(suffix__iexact=suffix)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lines = self.get_suffix_lines()
        context['lines'] = lines
        context['row_id'] = self.request.GET.get('row_id')
        return context


class SearchSuffixView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        lines = JournalLine.objects.filter(
            suffix__icontains=self.request.GET.get('term'), journal__company=self.get_company(),
            journal__year=self.get_year()
        ).values('id', 'suffix').distinct('suffix')
        return JsonResponse(list(lines), safe=False)


class SearchAccountSuffixView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        lines = JournalLine.objects.filter(
            suffix__icontains=self.request.GET.get('term'), journal__company=self.get_company(),
            journal__year=self.get_year(), account_id=int(self.request.GET.get('account_id'))
        ).values('id', 'suffix').distinct('suffix')
        return JsonResponse(list(lines), safe=False)


class UpdateSuffixesView(ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            for field, value in self.request.POST.items():
                if field.startswith('suffix', 0, 6):
                    line_id = int(field[7:])
                    try:
                        journal_line = JournalLine.objects.get(pk=line_id)
                        journal_line.suffix = value
                        journal_line.save()
                    except JournalLine.DoesNotExist:
                        pass
        return JsonResponse({
            'text': 'Suffixes updated successfully',
            'error': False
        })


class ExportJournalView(LoginRequiredMixin, ProfileMixin, JournalLinesMixin, View):

    def get(self, request, *args, **kwargs):
        journal = Journal.objects.prefetch_related('lines').get(pk=self.kwargs['pk'])
        company = self.get_company()
        company_objects = self.get_company_linked_models(company)
        journals_distributions = self.get_journal_lines_distributions(journal_lines=journal.lines.all())
        journal_lines = self.get_lines(journal=journal, linked_objects=company_objects, distributions=journals_distributions)

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = f'attachment; filename="journal_{journal.journal_number}.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(f'Journal {journal}')

        # Sheet header, first row
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        header_row = ws.row(0)
        header_row.write(0, 'Text', font_style)
        header_row.write(1, 'Konto', font_style)
        counter = 2
        for k, company_object in company_objects.items():
            header_row.write(counter, company_object['label'], font_style)
            counter += 1
        header_row.write(counter, 'Debit', font_style)
        counter += 1
        header_row.write(counter, 'Credit', font_style)
        # Sheet body, remaining rows
        row_count = 1
        for journal_line in journal_lines['lines']:
            debit = journal_line['line'].debit_amount if journal_line['line'].debit_amount > 0 else ''
            credit = journal_line['line'].credit_amount if journal_line['line'].credit_amount > 0 else ''

            ws.write(row_count, 0, journal_line['line'].journal.journal_text)
            ws.write(row_count, 1, journal_line['line'].account.code)
            line_counter = 2
            for key, object_item in journal_line['object_links'].items():
                ws.write(row_count, line_counter, object_item['value'])
                line_counter += 1
            ws.write(row_count, line_counter, debit)
            line_counter += 1
            ws.write(row_count, line_counter, credit)
            row_count += 1
        wb.save(response)
        return response


class ExportJournalsView(LoginRequiredMixin, ProfileMixin, JournalLinesMixin, View):

    def get(self, request, *args, **kwargs):
        company = self.get_company()
        company_objects = self.get_company_linked_models(company)
        year = self.get_year()

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = f'attachment; filename="journals_{company.slug}_{year.year}.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(f'Journals {company.slug}')
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        # Sheet header, first row
        header_row = ws.row(0)
        header_row.write(0, 'Period', font_style)
        header_row.write(1, 'Accounting Date', font_style)
        header_row.write(2, 'Journal Text', font_style)
        header_row.write(3, 'Text', font_style)
        header_row.write(4, 'Konto', font_style)
        counter = 5
        for k, company_object in company_objects.items():
            header_row.write(counter, company_object['label'], font_style)
            counter += 1
        header_row.write(counter, 'Debit', font_style)
        counter += 1
        header_row.write(counter, 'Credit', font_style)

        row_count = 1
        for journal in Journal.objects.filter(company=company, year=year):
            journal_lines = self.get_lines(journal=journal, linked_objects=company_objects)

            # Sheet body, remaining rows
            for journal_line in journal_lines['lines']:
                debit = journal_line['line'].debit_amount if journal_line['line'].debit_amount > 0 else ''
                credit = journal_line['line'].credit_amount if journal_line['line'].credit_amount > 0 else ''

                ws.write(row_count, 0, journal_line['line'].journal.period.period)
                ws.write(row_count, 1, journal_line['line'].accounting_date.strftime('%d-%m-%Y'))
                ws.write(row_count, 2, journal_line['line'].journal.journal_number)
                ws.write(row_count, 3, journal_line['line'].journal.journal_text)
                ws.write(row_count, 4, journal_line['line'].account.code)
                line_counter = 5
                for key, object_item in journal_line['object_links'].items():
                    ws.write(row_count, line_counter, object_item['value'])
                    line_counter += 1
                ws.write(row_count, line_counter, debit)
                line_counter += 1
                ws.write(row_count, line_counter, credit)
                row_count += 1
        wb.save(response)
        return response
