from django.urls import reverse
from django.core.management.base import BaseCommand
from docuflow.apps.company.models import Company
from docuflow.apps.journals.models import Journal, JournalLine
from django.db.models import Q


class Command(BaseCommand):
    help = "Clear the company invoices, to restart the imports"

    def handle(self, *args, **kwargs):
        try:
            slugs = ['41054']
            if len(slugs) > 0:
                companies = Company.objects.filter(slug__in=slugs)
            else:
                companies = Company.objects.filter()
            for company in companies:
                print("----------GET JOURNALS FOR  {} -----------".format(company))
                journals = Journal.objects.prefetch_related(
                    'lines'
                ).filter(
                    Q(company=company) | Q(company_id__isnull=True)
                ).filter(period_id=363)
                print("\t\t----------FOUND {} journals ------------".format(len(journals)))

                for journal in journals:
                    total_debit = 0
                    total_credit = 0
                    for line in journal.lines.all():
                        if line.credit and line.credit < 0:
                            total_credit += line.credit * -1
                        else:
                            total_credit += line.credit
                        total_debit += line.debit
                    if total_credit != total_debit:
                        print(f"Journal {journal} ({journal.period}), total debit {total_debit} and credit {total_credit}, did not match. Different = {total_debit-total_credit}, link {reverse('journal_lines', kwargs={'pk': journal.id})}")
            print("_-------------------------------------------------------------------------------______")
        except Exception as exception:
            print("Exception occurred {} ".format(exception))
