from django.core.management.base import BaseCommand
from docuflow.apps.journals.models import JournalLine


class Command(BaseCommand):
    help = "Fix accounting principle "

    def handle(self, *args, **kwargs):
        for journal_line in JournalLine.objects.filter(credit__gt=0, debit__gt=0):
            print(f"Journal line for {journal_line.journal}({journal_line.account}), Credit = {journal_line.credit} vs Debit = {journal_line.debit}")

            debit_amount = journal_line.debit - journal_line.credit if journal_line.credit < journal_line.debit else 0
            credit_amount = journal_line.credit - journal_line.debit if journal_line.debit < journal_line.credit else 0

            journal_line.debit = debit_amount
            journal_line.credit = credit_amount
            journal_line.save()

            journal_line.refresh_from_db()
            print(f"New amount  Credit =  {journal_line.credit} vs  Debit = {journal_line.debit} \r\n")
