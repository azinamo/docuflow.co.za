from django.core.management.base import BaseCommand
from docuflow.apps.journals.models import Journal, JournalLine
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.invoice.models import Invoice as SalesInvoice


class Command(BaseCommand):
    help = "Clear the company journals, to restart the imports"

    def handle(self, *args, **kwargs):
        try:
            counter = 0
            journal_lines = JournalLine.objects.filter(accounting_date__isnull=True)
            for journal_line in journal_lines:
                if not journal_line.accounting_date:
                    journal_line.accounting_date = journal_line.journal.date
                    counter += 1
                journal_line.save()
            print(f"Successfully update {counter}")
        except Exception as exception:
            print(f"Exception occurred {str(exception)} ")
