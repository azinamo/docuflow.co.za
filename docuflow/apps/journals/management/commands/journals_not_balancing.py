from django.core.management.base import BaseCommand
from docuflow.apps.company.models import Company
from docuflow.apps.journals.models import Journal


class Command(BaseCommand):
    help = "Clear the company invoices, to restart the imports"

    def add_arguments(self, parser):
        parser.add_argument('-c', '--company_slug', type=str, help='Indicates the company to check')

    def handle(self, *args, **kwargs):
        slugs = [kwargs.get('company_slug', None)]
        qs = Journal.objects.filter()
        if slugs:
            qs = qs.filter(company__slug__in=slugs)
        for journal in qs:
            total_debit = journal.calculate_total_debit()
            total_credit = journal.calculate_total_credit()
            if total_debit != total_credit:
                print(f"Journal {journal}({journal.period.period_year}) - {journal.id} not balancing "
                      f"and diff is {total_debit - total_credit} ")
