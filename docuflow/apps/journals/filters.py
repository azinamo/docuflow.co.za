from .models import Journal

from django_filters import rest_framework as filters


class JournalFilter(filters.FilterSet):
    class Meta:
        model = Journal
        fields = ['company__slug']
