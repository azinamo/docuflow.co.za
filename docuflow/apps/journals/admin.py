from django.contrib import admin

from . import models
from .forms import JournalLineAdminForm


# Register your models here.
class JournalLineAdmin(admin.TabularInline):
    model = models.JournalLine
    list_select_related = ('account', 'vat_code', 'journal', 'vat_report')
    list_display = ('account', 'debit', 'credit', 'text', 'suffix')
    fieldsets = [('Basic Information', {'fields': ['account', 'vat_code', 'text', 'debit', 'credit', 'suffix']})
                 ]
    extra = 0


@admin.register(models.Journal)
class JournalAdmin(admin.ModelAdmin):
    fieldsets = [('Basic Information', {'fields': ['journal_number', 'period', 'journal_text', 'date']})
                 ]
    date_hierarchy = 'date'
    list_display = ('__str__', 'journal_text', 'module', 'created_by')
    list_filter = ('company', 'year')
    inlines = [JournalLineAdmin, ]
    list_select_related = ('company', 'created_by')
    list_per_page = 50
    search_fields = ('journal_number', 'journal_text', )

    def has_add_permission(self, request):
        return False
