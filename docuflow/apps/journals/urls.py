from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.JournalsView.as_view(), name='journals_index'),
    path('create/', views.CreateJournalView.as_view(), name='create_generate_journal'),
    path('import/', views.ImportJournalView.as_view(), name='import_generate_journal'),
    path('export/', views.ExportJournalsView.as_view(), name='export_journals'),
    path('<int:pk>/detail/', views.JournalDetailView.as_view(), name='journal_detail'),
    path('<int:pk>/edit/', views.EditJournalView.as_view(), name='journal_lines'),
    path('<int:pk>/export/', views.ExportJournalView.as_view(), name='journal_export'),
    path('general/<int:pk>/edit/', views.EditGenerateJournalView.as_view(), name='edit_generate_journal'),
    path('<int:pk>/delete/', views.DeleteJournalView.as_view(), name='delete_journal'),
    path('line/add/', views.AddJournalLineView.as_view(), name='add_journal_line'),
    path('line/<int:pk>/delete/', views.DeleteJournalLineView.as_view(), name='delete_journal_line'),
    path('copy/year/<int:pk>/balances/', views.CopyYearBalanceView.as_view(), name='copy_previous_year_balances'),
    path('open/balances/', views.SetupOpeningBalanceView.as_view(), name='opening_balances'),
    path('update/opening/<int:account_id>/balances/', csrf_exempt(views.UpdateOpeningBalanceView.as_view()),
         name='update_year_account_balance'),
    path('validate/period/', views.ValidatePeriodView.as_view(), name='validate_journal_period'),
    path('check/fixed/asset/', views.CheckFixedAssetView.as_view(), name='check_fixed_asset_accounts'),
    path('validate/delete/<int:pk>/journalline/', views.ValidateDeleteLine.as_view(), name='validate_delete_journal_line'),
    path('ledger/accounts/list/', views.LedgerAccountsView.as_view(), name='ledger_accounts_list'),
    path('open/<int:account_id>/suffix/lines/', views.SuffixLinesView.as_view(), name='open_suffix_lines'),
    path('search/suffix/', views.SearchSuffixView.as_view(), name='search_suffix'),
    path('search/account/suffix/', views.SearchAccountSuffixView.as_view(), name='search_account_suffix'),
    path('update/suffixes/', views.UpdateSuffixesView.as_view(), name='update_suffixes'),
    path('<int:pk>/logs/', views.LedgerLogView.as_view(), name='journal_logs'),
    path('<int:year_id>/<str:slug>/lines/', views.LedgerAccountLinesView.as_view(),
         name='report_type_lines'),
]
