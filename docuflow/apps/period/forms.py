from django import forms
from django.core.cache import cache
from django.forms import ModelChoiceField

from .models import Period, Year
from .enums import PeriodStatus, YearStatus
from . import services


class PeriodModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return f"{obj.period}-{obj.name}"


class YearForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        super(YearForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Year
        fields = ('year', 'start_date', 'end_date', 'number_of_periods', 'journal_number')
        widgets = {
            'start_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'end_date': forms.DateInput(attrs={'class': 'datepicker'}),
        }

    def clean(self):
        cleaned_data = super(YearForm, self).clean()
        if self.company and not self.instance.pk:
            if Year.objects.filter(year=cleaned_data['year'], company=self.company).exists():
                self.add_error('year', f"{cleaned_data['year']} Year already exists for this company")

    def save(self, commit=True):
        year = super(YearForm, self).save(commit=False)
        year.company = self.company
        year.status = YearStatus.NOT_STARTED
        year.save()

        if year.is_open:
            year.mark_as_open()
        year.save()
        services.process_year_periods(year=year)
        if not self.instance.pk:
            # TODO - Ensure if we change number of periods, we remove or add extra periods
            year_cache_key = f"profile__{self.profile.id}__years"
            cache.set(year_cache_key, Year.objects.filter(company=self.company).all())
        return year


class PeriodForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PeriodForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['is_closed'].initial = self.instance.status == PeriodStatus.CLOSED

    is_closed = forms.BooleanField(required=False, label='Closed')

    class Meta:
        model = Period
        fields = ('period_year', 'period', 'name', 'from_date', 'to_date')
        widgets = {
            'from_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'to_date': forms.DateInput(attrs={'class': 'datepicker'})
        }

    def save(self, commit=True):
        period = super(PeriodForm, self).save(commit=commit)
        if self.cleaned_data['is_closed']:
            period.mark_as_closed()
        else:
            period.mark_as_opened()


class MonthCloseForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(MonthCloseForm, self).__init__(*args, **kwargs)

        self.fields['period'].queryset = Period.objects.pending_locking(company=self.company)

    period = PeriodModelChoiceField(required=True, label='Period', queryset=None)


class StartYearForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.previous_year = kwargs.pop('previous_year')
        super(StartYearForm, self).__init__(*args, **kwargs)
        self.fields['open_year'].label = f'Open the year {self.year}'
        self.fields['close_previous'].label = f'Change {self.previous_year} into "closing status"'

    open_year = forms.BooleanField(required=True)
    make_year_default = forms.BooleanField(required=True, label='Will make this year the default when logging in')
    close_previous = forms.BooleanField(required=True)
    cannot_be_undone = forms.BooleanField(required=True, label='I understand this process cannot be undone')

    def execute(self):
        pass


class ClosingYearForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.previous_year = kwargs.pop('previous_year')
        super(ClosingYearForm, self).__init__(*args, **kwargs)
        self.fields['close_year'].label = f'It will close the year({self.year}) and cannot be opened again'

    check_reporting = forms.BooleanField(required=True, label='It will check that all required reporting is done for'
                                                              ' the year')
    close_journals = forms.BooleanField(required=True, label=' It will do the closing journals as required to close of'
                                                             ' ledgers')
    close_year = forms.BooleanField(required=True)
    cannot_be_undone = forms.BooleanField(required=True, label='I understand this process cannot be undone')

    def execute(self):
        pass
