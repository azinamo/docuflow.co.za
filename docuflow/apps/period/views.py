import calendar
import datetime
import logging

from django.core.cache import cache
from django.utils.translation import gettext as __
from django.contrib import messages
from django.views.generic import ListView, TemplateView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from django.db import transaction, models

from annoying.functions import get_object_or_None

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Invoice, Payment
from docuflow.apps.vat.models import VatReport
from docuflow.apps.fixedasset.models import Update
from docuflow.apps.accounts.models import Profile
from docuflow.apps.invoice.enums import InvoiceStatus
from docuflow.apps.distribution.models import DistributionAccount
from docuflow.apps.sales.models import Invoice as SalesInvoice, Receipt
from .forms import PeriodForm, YearForm, MonthCloseForm, StartYearForm, ClosingYearForm
from .exceptions import PeriodException
from .enums import PeriodStatus, YearStatus
from .models import Period, Year
from . import services

logger = logging.getLogger(__name__)


class PeriodsView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Period
    template_name = 'period/index.html'
    context_object_name = 'periods'

    def get_queryset(self):
        return Period.objects.for_year(year=self.get_year()).select_related('period_year').order_by(
            '-period_year__year', 'period')

    def get_context_data(self, **kwargs):
        context = super(PeriodsView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['year'] = self.get_year()
        return context


class CreatePeriodView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = Period
    form_class = PeriodForm
    template_name = 'period/create.html'
    success_message = __('Period days successfully saved')
    success_url = reverse_lazy('periods_index')

    def get_context_data(self, **kwargs):
        context = super(CreatePeriodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def form_valid(self, form):
        try:
            period = form.save(commit=False)
            period.company = self.get_company()
            period.save()
        except Exception as err:
            messages.error(self.request, __('Unexpected error occurred, please try again.'))
            return super(CreatePeriodView, self).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditPeriodView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = Period
    form_class = PeriodForm
    template_name = 'period/edit.html'
    success_message = __('Period successfully updated')
    success_url = reverse_lazy('periods_index')

    def get_context_data(self, **kwargs):
        context = super(EditPeriodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def form_valid(self, form):
        try:
            with transaction.atomic():
                form.save()

            messages.success(self.request, __('Period updated successfully'))
        except PeriodException as period_exception:
            messages.error(self.request, str(period_exception))
        return HttpResponseRedirect(reverse('periods_index'))


class DeletePeriodView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Period
    template_name = 'period/confirm_delete.html'
    success_message = __('Period successfully deleted')

    def get(self, request, *args, **kwargs):
        try:
            period = Period.objects.get(pk=self.kwargs['pk'])
            period.delete()
            messages.success(self.request, __('Period deleted successfully'))
        except Exception as exception:
            messages.error(self.request, __('Unexpected error occurred'))
        return HttpResponseRedirect(reverse('periods_index'))


class YearsView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Year
    template_name = 'years/index.html'
    context_object_name = 'years'

    def get_queryset(self):
        return Year.objects.prefetch_related(
            models.Prefetch('year_periods', Period.objects.opened()),
            models.Prefetch('year_periods', Period.objects.opened(), to_attr='periods'),
        ).filter(company=self.get_company()).order_by('-year')

    def get_context_data(self, **kwargs):
        context = super(YearsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateYearView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = Year
    form_class = YearForm
    template_name = 'years/create.html'
    success_message = __('Year days successfully saved')
    success_url = reverse_lazy('years_index')

    def get_context_data(self, **kwargs):
        context = super(CreateYearView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateYearView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def form_valid(self, form):
        with transaction.atomic():
            year = form.save(commit=False)

        messages.success(self.request, __('Year saved successfully'))
        return HttpResponseRedirect(reverse('year_create_opening_balances_index', kwargs={'pk': year.id}))


class YearOpeningBalancesView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'years/opening_balances.html'

    def get_year(self):
        return Year.objects.filter(pk=self.kwargs['pk']).get()

    def get_context_data(self, **kwargs):
        context = super(YearOpeningBalancesView, self).get_context_data(**kwargs)
        year = self.get_year()
        context['company'] = self.get_company()
        context['year'] = year
        return context


class ChangeYearStatusView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        to_status = self.kwargs['to']
        year = Year.objects.get(pk=self.kwargs['pk'])
        if to_status == 'final':
            count_finalized_previous_year = Year.objects.filter(year__lt=year.year, status=YearStatus.FINAL, company=year.company).count()
            count_previous_years = Year.objects.filter(year__lt=year.year, company=year.company).count()
            if count_finalized_previous_year == count_previous_years:
                return JsonResponse({
                    'text': 'Redirecting ...',
                    'redirect': reverse('finalize_year', args=(year.id, )),
                    'error': False
                })
            else:
                return JsonResponse({
                    'text': f'Previous years are not yet finalized. You need to finalize them first before finalizing {year}',
                    'warning': True
                })
        elif to_status == 'start':
            return JsonResponse({
                'text': 'Year updated successfully',
                'redirect': reverse('start_year', args=(year.id,)),
                'error': False
            })


class FinalizeYear(ProfileMixin, FormView):
    template_name = 'years/finalize/finalize.html'
    form_class = ClosingYearForm

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year - 1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        year = self.get_year()
        kwargs['year'] = year
        kwargs['previous_year'] = self.get_previous_year(year=year)
        return kwargs

    def get_success_url(self):
        year = self.get_year()
        if year and services.has_open_batches(year):
            return reverse('finalize_year_open_batches', args=(year.id, ))
        else:
            return reverse('finalize_vat_reports', args=(year.id,))


class FinalizeYearVatReportsView(TemplateView):
    template_name = 'years/finalize/vat_reports.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = Year.objects.get(pk=self.kwargs['pk'])
        periods = Period.objects.annotate(
            has_vat_reports=models.Count('vat_reports')
        ).filter(period_year=year).order_by('period')
        context['periods'] = periods
        context['year'] = year
        return context


class FinalizeYearCompleteView(LoginRequiredMixin, TemplateView):
    template_name = 'years/finalize/complete.html'

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year - 1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        previous_year = self.get_previous_year(year)
        context['year'] = year
        context['previous_year'] = previous_year
        context['next_year'] = year.year + 1
        return context


class FinalizeYearClosingProcessView(ProfileMixin, View):
    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year - 1, company=year.company)
        except Year.DoesNotExist:
            return None

    # noinspection PyMethodMayBeStatic
    def get_next_year(self, year):
        try:
            return Year.objects.get(year=year.year + 1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get(self, request, *args, **kwargs):
        stage = self.request.GET.get('stage')
        year = Year.objects.get(pk=self.kwargs['pk'])
        previous_year = self.get_previous_year(year=year)

        company = self.get_company()
        message = 'Processed'
        title = ''
        is_alert = False
        if stage == 'shutting_login_screen':
            company.mark_as_suspended()
        elif stage == 'close_all_periods':
            services.close_year_periods(year=year)
        elif stage == 'ensure_last_period_is_open':
            services.ensure_last_period_is_open(year=year)
        elif stage == 'change_status_to_closing':
            year.mark_as_closing()
        elif stage == 'fixed_assets_closing_balances':
            pass
            # services.fixed_assets_closing_balances(previous_year=year, year=next_year)
        elif stage == 'general_ledger_closing_balances':
            pass
            # services.general_ledger_closing_balances(previous_year=year, year=next_year)
        elif stage == 'inventory_closing_balances':
            pass
            # services.inventory_closing_balances(year=year)
        elif stage == 'customer_closing_balances':
            pass
            # services.customer_closing_balances(year=year)
        elif stage == 'supplier_closing_balances':
            pass
            # services.supplier_closing_balances(year=year)
        elif stage == 'distribution_balances':
            pass
            # services.distribution_balances(year=year)
        elif stage == 'goods_received_balances':
            services.goods_received_balances(year=year)
        elif stage == 'invoices_not_year_posted':
            services.invoices_not_year_posted(year=year)
        elif stage == 'open_next_year':
            year.mark_as_final()
            next_year = self.get_next_year(year=year)
            if next_year:
                next_year.mark_as_open()
        elif stage == 'opening_login':
            services.opening_login(company=year.company)
        elif stage == 'done':
            message = f'Your year end is now complete and the current year is now up to date.'
            title = 'Congratulations!'
            is_alert = False

        return JsonResponse({
            'text': f'{message}',
            'title': f'{title}',
            'error': False,
            'alert': is_alert,
            'home': reverse('all-invoices')
        })


class StartYearView(FormView):
    template_name = 'years/starting/start.html'
    form_class = StartYearForm

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year - 1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        year = self.get_year()
        kwargs['year'] = year
        kwargs['previous_year'] = self.get_previous_year(year=year)
        return kwargs

    def get_success_url(self):
        year = self.get_year()
        previous_year = self.get_previous_year(year)
        if previous_year and services.has_open_batches(previous_year):
            return reverse('show_open_batches', args=(year.id, ))
        else:
            self.request.session['year_stared_requested'] = 'start'
            return reverse('logout_company_users', args=(year.id,))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        context['year'] = year
        context['previous_year'] = self.get_previous_year(year=year)
        return context

    def form_valid(self, form):
        form.execute()
        return super().form_valid(form)


class OpenBatchesView(TemplateView):
    template_name = 'years/starting/open_batches.html'

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year - 1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        previous_year = self.get_previous_year(year)
        context['periods'] = services.get_open_batches_by_period(previous_year)
        context['year'] = year
        context['previous_year'] = previous_year
        return context


class FinalizeYearOpenBatchesView(OpenBatchesView):
    template_name = 'years/finalize/open_batches.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        context['periods'] = services.get_open_batches_by_period(year=year)
        context['year'] = year
        return context


class LogoutCompanyUsersView(TemplateView):
    template_name = 'years/starting/logout_company_users.html'

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        context['year'] = year
        return context


class StartingYearView(TemplateView):
    template_name = 'years/starting/starting_year.html'

    def get(self, request, *args, **kwargs):
        if 'year_stared_requested' not in self.request.session:
            return HttpResponseRedirect(reverse('years_index'))
        return super(StartingYearView, self).get(request, *args, **kwargs)

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year - 1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        previous_year = self.get_previous_year(year)
        context['year'] = year
        context['previous_year'] = previous_year
        context['next_year'] = year.year + 1
        return context


class YearStartProcessingView(ProfileMixin, View):

    # noinspection PyMethodMayBeStatic
    def get_previous_year(self, year):
        try:
            return Year.objects.get(year=year.year - 1, company=year.company)
        except Year.DoesNotExist:
            return None

    def get(self, request, *args, **kwargs):
        stage = self.request.GET.get('stage')
        year = Year.objects.get(pk=self.kwargs['pk'])
        previous_year = self.get_previous_year(year=year)
        company = Company.objects.get(pk=self.request.session['company'])
        message = 'Processed'
        title = ''
        is_alert = False
        if stage == 'shutting_login_screen':
            company.mark_as_suspended()
        elif stage == 'close_all_periods':
            services.close_year_periods(year=previous_year)
        elif stage == 'ensure_last_period_is_open':
            services.ensure_last_period_is_open(year=previous_year)
        elif stage == 'change_status_to_closing':
            if previous_year:
                previous_year.mark_as_closing()
        elif stage == 'fixed_assets_closing_balances':
            if previous_year:
                services.fixed_assets_closing_balances(previous_year=previous_year, year=year)
        elif stage == 'general_ledger_closing_balances':
            if previous_year:
                services.general_ledger_closing_balances(company=company, previous_year=previous_year, year=year)
        elif stage == 'inventory_closing_balances':
            services.inventory_closing_balances(company=company, year=year)
        elif stage == 'customer_closing_balances':
            if previous_year:
                services.customer_closing_balances(previous_year=previous_year, year=year)
        elif stage == 'supplier_closing_balances':
            if previous_year:
                services.supplier_closing_balances(previous_year=previous_year, year=year)
        elif stage == 'distribution_balances':
            if previous_year:
                services.distribution_balances(year=previous_year)
        elif stage == 'goods_received_balances':
            if previous_year:
                services.goods_received_balances(year=previous_year)
        elif stage == 'invoices_not_year_posted':
            if previous_year:
                services.invoices_not_year_posted(year=previous_year)
        elif stage == 'creating_next_year':
            services.creating_next_year(year=year)

            cache.set(f"profile__{self.get_profile().id}__years", Year.objects.filter(company=self.get_company()).all())
        elif stage == 'opening_year':

            year.mark_as_open()
            year.is_active = True
            year.save()
            cache.set(f"{self.request.session['user']}_year_{year.company_id}", year)
        elif stage == 'opening_first_period':
            services.opening_first_period(year=year)
        elif stage == 'opening_login':
            services.opening_login(company=year.company)
        elif stage == 'done':
            title = f'Congratulations'
            message = f'The current year is now {year}!'
            if previous_year:
                message = f'{message} You can still process items in {previous_year} to complete that year by changing' \
                          f' the year on the top right. Do not forget to Close {previous_year} once you have prepared' \
                          f' your Financial Statements that will be submitted to the Tax Office. {previous_year} should' \
                          f' be changed to final status within 6 months after the year end date and submitted to the' \
                          f' tax office. If you have not changed the status to Final on {previous_year}, then the' \
                          f' Finance Responsible Role will get reminders everytime they login',
            is_alert = True
            if 'year_stared_requested' not in self.request.session:
                del self.request.session['year_stared_requested']

        return JsonResponse({
            'text': message,
            'title': f'{title}',
            'error': False,
            'alert': is_alert,
            'home': reverse('all-invoices')
        })


class UpdateDefaultYearView(LoginRequiredMixin, ProfileMixin, View):

    def get_year(self):
        year = Year.objects.get(pk=self.kwargs['pk'])
        return year

    def update_cached_year(self, year):
        _key = f"year_{self.request.session['company']}_"
        cache.set(_key, year)

    def post(self, request, *args, **kwargs):
        try:
            year = self.get_year()
            if year.open():
                company = self.get_company()
                for company_year in company.company_years.all():
                    company_year.is_active = False
                    company_year.save()

                year.is_active = True
                year.save()

                profile = get_object_or_None(Profile, pk=self.request.session['profile'])
                profile.year = year
                profile.save()

                self.update_cached_year(year)

                self.request.session['profile_year_id'] = year.id

                return JsonResponse({
                    'text': 'Year updated successfully',
                    'error': False
                    })
            else:
                return JsonResponse({
                    'text': 'Please open the year before making it default year',
                    'error': True
                    })
        except Exception as exception:
            return JsonResponse({
                    'text': 'Unexpected error occurred, please try again',
                    'error': True,
                    'details': str(exception)
                })


class EditYearView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = Year
    template_name = 'years/edit.html'
    success_message = __('Year successfully updated')
    form_class = YearForm
    success_url = reverse_lazy('years_index')

    def get_form_kwargs(self):
        kwargs = super(EditYearView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditYearView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def form_valid(self, form):
        try:
            form.save(commit=False)

            messages.success(self.request, __('Year updated successfully'))
        except PeriodException as period_exception:
            messages.error(self.request, str(period_exception))
        except Exception as exception:
            logger.exception(exception)
            messages.error(self.request, __('Unexpected error occurred '))
        return HttpResponseRedirect(reverse('years_index'))


class DeleteYearView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Year
    template_name = 'years/confirm_delete.html'
    success_message = __('Year successfully deleted')

    def get(self, request, *args, **kwargs):
        year = Year.objects.get(pk=self.kwargs['pk'])
        year.delete()
        messages.success(self.request, __('Year deleted successfully'))
        return HttpResponseRedirect(reverse('years_index'))


class MonthCloseView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'month_close/index.html'

    def get_context_data(self, **kwargs):
        context = super(MonthCloseView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = Year.objects.filter(is_active=True, company=company).get()
        context['company'] = company
        context['year'] = year
        return context


class CreateMonthCloseView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'month_close/create.html'
    form_class = MonthCloseForm

    def get_form_kwargs(self):
        kwargs = super(CreateMonthCloseView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error creating month close report, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateMonthCloseView, self).form_invalid(form)

    def is_previous_locked(self, period):
        previous_period = period.period-1
        period = get_object_or_None(Period, period=previous_period, company=period.company,
                                    period_year=period.period_year)
        if period and not period.is_locked:
            return False
        return True

    def form_valid(self, form):
        period = form.cleaned_data['period']
        if period:
            return JsonResponse({
                'text': 'Redirecting ...',
                'error': False,
                'redirect': reverse('view_month_close_journals', kwargs={'period': period.id})
            })
        else:
            return JsonResponse({'text': 'Select a valid period', 'error': True})


class ShowMonthCloseView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'month_close/show.html'

    def get_period(self):
        return Period.objects.get(pk=self.kwargs['period'])

    def get_pending_account_postings(self, period):
        return Invoice.objects.pending_posting(
            company=self.get_company(),
            end_date=period.to_date,
            statuses=InvoiceStatus.pending_posting_statuses()
        ).filter(period__period_year=period.period_year).all()

    def get_pending_distributions(self, period):
        return DistributionAccount.objects.month_close_pending(period=period)

    def get_pending_incoming_updates(self, period):
        return Invoice.objects.pending_incoming(
            company=self.get_company(),
            end_date=period.to_date
        ).filter(period__period_year=period.period_year).all()

    def get_pending_payments(self, period):
        return Payment.objects.pending_updates(
            end_date=period.to_date,
            company=self.get_company()
        ).filter(period__period_year=period.period_year).all()

    def get_pending_sales_updates(self, period):
        return SalesInvoice.objects.pending_updates(
            company=self.get_company(),
            end_date=period.to_date
        ).filter(period__period_year=period.period_year).all()

    def get_pending_cashup_updates(self, period):
        return Receipt.objects.pending_cash_up(
            company=self.get_company(),
            end_date=period.to_date
        ).filter(period__period_year=period.period_year).all()

    def is_pending_vat_updates(self, period):
        periods_available = Period.objects.filter(
            company=self.get_company(),
        ).exclude(
            pk__in=VatReport.objects.filter(company=self.get_company(), year=period.period_year).values_list('pk', flat=True)
        ).values_list('pk', flat=True)
        return period.id not in periods_available

    def get_period_last_date(self, year, period):
        month = int(period.period)
        start_date = datetime.datetime.now().date()
        last_date = start_date.replace(year=year.year, month=month, day=calendar.monthrange(year.year, month)[1])
        return last_date

    def can_submit_in_period(self, period, year):
        vat_periods = period.company.get_vat_periods(year=year)
        for vat_period in vat_periods:
            if vat_period[0] == period.id:
                return True
        return False

    def unsubmited_report_lines(self, period, year):
        if self.can_submit_in_period(period, year):
            if not VatReport.objects.filter(company=self.get_company(), period=period, year=year).exists():
                return [period]
        return []

    def outstanding_assets_depreciation(self, year, period):
        if year.year_fixed_assets.count() > 0:
            fixed_asset_journal = Update.objects.filter(period=period)
            if fixed_asset_journal:
                return []
            else:
                return [1]
        return []

    def get_context_data(self, **kwargs):
        context = super(ShowMonthCloseView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = Year.objects.filter(is_active=True, company=company).get()
        period = self.get_period()

        sales_updates = self.get_pending_sales_updates(period=period)
        cashup_updates = self.get_pending_cashup_updates(period=period)
        is_pending_vat_updates = self.is_pending_vat_updates(period=period)
        account_postings = self.get_pending_account_postings(period)
        distributions = self.get_pending_distributions(period)
        incoming_updates = self.get_pending_incoming_updates(period=period)
        payments = self.get_pending_payments(period)
        vat_lines = self.unsubmited_report_lines(period, year)
        depreciation_journal = self.outstanding_assets_depreciation(year, period)

        sales_updates_count = len(sales_updates)
        cashup_updates_count = len(cashup_updates)
        incoming_updates_count = len(incoming_updates)
        payments_count = len(payments)
        distributions_count = len(distributions)
        account_postings_count = len(account_postings)
        vat_lines_count = len(vat_lines)
        depreciation_journal_count = len(depreciation_journal)

        has_pending_updates = (vat_lines_count + account_postings_count + distributions_count + payments_count +
                               incoming_updates_count + depreciation_journal_count + sales_updates_count + cashup_updates_count)

        context['has_in_flow_invoices'] = incoming_updates_count

        context['sales_updates_count'] = sales_updates_count
        context['sales_updates'] = sales_updates

        context['cashup_updates_count'] = cashup_updates_count
        context['cashup_updates'] = cashup_updates

        context['incoming_updates_count'] = incoming_updates_count
        context['incoming_updates'] = incoming_updates

        context['distributions_count'] = distributions_count
        context['distributions'] = distributions

        context['has_pending_updates'] = has_pending_updates

        context['account_postings_count'] = account_postings_count
        context['account_postings'] = account_postings

        context['vat_lines_count'] = vat_lines_count
        context['vat_reports'] = []

        context['is_pending_vat_updates'] = is_pending_vat_updates

        context['payments_count'] = payments_count
        context['payments'] = payments

        context['depreciation_count'] = depreciation_journal_count
        context['depreciations'] = depreciation_journal

        context['company'] = company
        context['year'] = year
        context['period'] = period
        return context


class SaveMonthCloseView(LoginRequiredMixin, ProfileMixin, View):

    def lock_months(self, period):
        for period in Period.objects.pending_locking(company=self.get_company(), period=period):
            period.status = PeriodStatus.LOCKED
            period.is_closed = True
            period.save()

    def is_previous_locked(self, period):
        previous_period = period.period-1
        period = get_object_or_None(Period, period=previous_period, company=period.company,
                                    period_year=period.period_year)
        if period and not period.is_locked:
            return False
        return True

    def get(self, request, *args, **kwargs):
        period = Period.objects.get(pk=self.kwargs['period_id'])
        self.lock_months(period=period)
        return JsonResponse({
            'text': 'Month closed successfully',
            'error': False,
            'redirect': reverse('month_close')
        })
