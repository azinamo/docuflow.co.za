from enumfields import IntEnum


class PeriodStatus(IntEnum):
    CLOSED = 0
    OPEN = 1
    LOCKED = 2


class YearStatus(IntEnum):
    CLOSING = 0  # Status of the prev year, when you have just opened a new year
    OPEN = 1  # Status of the year when the previous year has been Final status
    LOCKED = 2
    NOT_STARTED = 3  # We need this status as we know we have to have the year open with copied periods so that distributions can work….
    STARTED = 4  # So if you have started a year and the previous year is still open (closing see below) then the system knows it has to copy into both years?
    FINAL = 5  # This is when you finally close the year and cannot open it


class BudgetType(IntEnum):
    WHOLE_COMPANY = 0
    INDIVIDUAL_OBJECTS = 1




