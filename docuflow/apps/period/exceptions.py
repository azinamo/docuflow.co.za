class PeriodException(Exception):
    pass


class HasFinalSignedDocuments(PeriodException):
    pass


class HasOpenPeriods(PeriodException):
    pass
