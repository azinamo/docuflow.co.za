from rest_framework import serializers

from .models import Period, Year


class YearSerializer(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = '__all__'


class PeriodSerializer(serializers.ModelSerializer):

    period_year = YearSerializer()

    class Meta:
        model = Period
        fields = '__all__'

