from django.contrib import admin

from . import models


# Register your models here.
@admin.register(models.Year)
class YearAdmin(admin.ModelAdmin):
    list_display = ('year', 'start_date', 'end_date', 'status', 'number_of_periods', 'budget_type')
    list_select_related = ('company', )
    list_filter = ('company', 'year')
    search_fields = ('year', )


@admin.register(models.Period)
class PeriodAdmin(admin.ModelAdmin):
    list_display = ('period_year', 'period', 'name', 'from_date', 'to_date', 'status')
    list_select_related = ('period_year', )
    list_filter = ('period_year', )
    search_fields = ('period', )
