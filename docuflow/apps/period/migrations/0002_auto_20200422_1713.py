# Generated by Django 2.2 on 2020-04-22 15:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('period', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='year',
            name='journal_number',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Journal Number Starts From'),
            preserve_default=False,
        ),
    ]
