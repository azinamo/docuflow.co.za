from django.urls import path

from . import views

urlpatterns = [
    path('', views.YearsView.as_view(), name='years_index'),
    path('create/', views.CreateYearView.as_view(), name='create_year'),
    path('edit/<int:pk>/', views.EditYearView.as_view(), name='edit_year'),
    path('delete/<int:pk>/', views.DeleteYearView.as_view(), name='delete_year'),
    path('opening/balances/<int:pk>/', views.YearOpeningBalancesView.as_view(),
         name='year_create_opening_balances_index'),
    path('change/<int:pk>/<str:to>/status/', views.ChangeYearStatusView.as_view(), name='change_year_status'),
    path('start/<int:pk>/', views.StartYearView.as_view(), name='start_year'),
    path('open/<int:pk>/batches', views.OpenBatchesView.as_view(), name='show_open_batches'),
    path('logout/<int:pk>/users', views.LogoutCompanyUsersView.as_view(), name='logout_company_users'),
    path('starting/<int:pk>/', views.StartingYearView.as_view(), name='start_next_year'),
    path('processing/<int:pk>/start/', views.YearStartProcessingView.as_view(), name='year_starting'),
    path('finalize/<int:pk>/', views.FinalizeYear.as_view(), name='finalize_year'),
    path('finalize/<int:pk>/open_batches/', views.FinalizeYearOpenBatchesView.as_view(), name='finalize_year_open_batches'),
    path('finalize/<int:pk>/vat/', views.FinalizeYearVatReportsView.as_view(), name='finalize_vat_reports'),
    path('finalize/<int:pk>/complete/', views.FinalizeYearCompleteView.as_view(), name='complete_finalize'),
    path('closing/<int:pk>/process/', views.FinalizeYearClosingProcessView.as_view(), name='year_closing'),
    path('default/<int:pk>/', views.UpdateDefaultYearView.as_view(), name='update_default_year'),
    path('periods/', views.PeriodsView.as_view(), name='periods_index'),
    path('periods/create/', views.CreatePeriodView.as_view(), name='create_period'),
    path('periods/edit/<int:pk>/', views.EditPeriodView.as_view(), name='edit_period'),
    path('periods/delete/<int:pk>/', views.DeletePeriodView.as_view(), name='delete_period'),
    path('month/close/', views.MonthCloseView.as_view(), name='month_close'),
    path('create/month/close/', views.CreateMonthCloseView.as_view(), name='create_month_close'),
    path('period/<int:period>/month/close/', views.ShowMonthCloseView.as_view(), name='view_month_close_journals'),
    path('save/<int:period_id>/month/close/', views.SaveMonthCloseView.as_view(), name='save_month_close'),
]
