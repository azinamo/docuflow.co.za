import django.dispatch

period_close = django.dispatch.Signal(providing_args=['period'])
period_open = django.dispatch.Signal(providing_args=['period'])
