import calendar
import logging
import pprint
from datetime import timedelta, date
from decimal import Decimal
from typing import List

from django.core.cache import cache
from django.utils import timezone

from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Company, Account, Branch
from docuflow.apps.customer.models import (Customer, OpeningBalance as CustomerOpeningBalance,
                                           AgeAnalysis as CustomerAgeAnalysis)
from docuflow.apps.fixedasset.models import OpeningBalance as FixedAssetOpeningBalance
from docuflow.apps.fixedasset.services import get_assets_balances
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.models import BranchInventory, InventoryReceived, Received
from docuflow.apps.invoice.models import Invoice, Payment
from docuflow.apps.journals.models import OpeningBalance, JournalLine, Journal
from docuflow.apps.reports.incomestatement import IncomeStatement
from docuflow.apps.sales.enums import InvoiceType
from docuflow.apps.sales.models import Invoice as SalesInvoice, Receipt
from docuflow.apps.supplier.models import (Supplier, OpeningBalance as SupplierOpeningBalance,
                                           AgeAnalysis as SupplierAgeAnalysis)
from .enums import YearStatus, PeriodStatus
from .models import Period, Year
from ..common.utils import get_profile_year_cache_key

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


def get_month_day_range(day):
    first_day = day.replace(day=1)
    last_day = day.replace(day=calendar.monthrange(day.year, day.month)[1])
    return first_day, last_day


def process_year_periods(year: Year) -> List[Period]:
    start = year.start_date
    month_counter = 0
    nb_periods = year.number_of_periods + 1
    year_counter = 0
    periods = []

    for period_count in range(1, nb_periods):
        if period_count == 1:
            start_date = start
            month_range = get_month_day_range(day=start)
            month_counter = start.month
            year_counter = start.year
        else:
            if month_counter == 13:
                year_counter += 1
                month_counter = 1

            start = start.replace(month=month_counter, year=year_counter, day=1)
            month_range = get_month_day_range(day=start)
            start_date = month_range[0]
        end_date = month_range[1]
        if start_date and end_date:
            period, _ = Period.objects.update_or_create(
                period=period_count,
                period_year=year,
                company=year.company,
                defaults={
                    'from_date': start_date,
                    'to_date': end_date,
                    'name': start_date.strftime('%B'),
                    'is_closed': False,
                    'status': PeriodStatus.CLOSED,
                }
            )
            periods.append(period)
        # year.end_date = end_date
        # year.save()
        month_counter += 1
    return periods


def start_year(year: Year):
    pass


def close_period(sender, period: Period, **kwargs):
    logger.info(f"Sender --> {sender}")
    logger.info(f"period {period}")
    logger.info(f"kwargs {kwargs}")


def has_open_batches(year: Year):
    start_date = year.start_date
    end_date = year.end_date - timedelta(days=1)
    company = year.company
    if company.run_incoming_document_journal:
        incoming = Invoice.objects.pending_incoming(company, start_date=start_date, end_date=end_date)
    else:
        incoming = Invoice.objects.none()
    posting = Invoice.objects.pending_posting(company, start_date=start_date, end_date=end_date)
    payments = Payment.objects.pending_updates(company, year=year, start_date=start_date, end_date=end_date)
    sales_tax_invoice = SalesInvoice.objects.pending_updates(company, invoice_type=InvoiceType.TAX_INVOICE.value,
                                                             start_date=start_date, end_date=end_date)
    sales_tax_credit_note = SalesInvoice.objects.pending_updates(company, invoice_type=InvoiceType.CREDIT_INVOICE.value,
                                                                 start_date=start_date, end_date=end_date)
    cash_ups = Receipt.objects.pending_cash_up(company=company)

    return any([incoming.exists(), posting.exists(), payments.exists(), sales_tax_invoice.exists(),
                sales_tax_credit_note.exists(), cash_ups.exists()])


def get_open_batches_by_period(year: Year):
    start_date = year.start_date
    end_date = year.end_date - timedelta(days=1)
    company = year.company
    incoming = Invoice.objects.pending_incoming(company, start_date=start_date, end_date=end_date).all()
    posting = Invoice.objects.pending_posting(company, start_date=start_date, end_date=end_date).all()
    payments = Payment.objects.pending_updates(company, year=year, start_date=start_date, end_date=end_date).all()
    sales_tax_invoice = SalesInvoice.objects.pending_updates(company, invoice_type=InvoiceType.TAX_INVOICE.value,
                                                             start_date=start_date, end_date=end_date).all()
    sales_tax_credit_note = SalesInvoice.objects.pending_updates(company, invoice_type=InvoiceType.CREDIT_INVOICE.value,
                                                                 start_date=start_date, end_date=end_date).all()
    cash_ups = Receipt.objects.pending_cash_up(company=company).all()

    periods = Period.objects.filter(period_year=year).order_by('period')

    period_pending_items = {}
    for period in periods:
        period_pending_items[period] = {
            'incoming': count_pending(period, incoming),
            'posting': count_pending(period, posting),
            'payments': count_pending(period, payments),
            'sales_tax_invoice': count_pending(period, sales_tax_invoice),
            'sales_tax_credit_note': count_pending(period, sales_tax_credit_note),
            'cash_ups': count_pending(period, cash_ups)
        }
    return period_pending_items


def count_pending(period, items):
    period_items = 0
    for item in items:
        period_items += 1 if item.period == period else 0
    return period_items


def group_period(periods, items):
    period_pending_items = []
    for period in periods:
        period_items = 0
        for item in items:
            period_items += 1 if item.period == period else 0
        period_pending_items[period] = period_items
    return period_pending_items


# def shut_down_login_screen(company: Company):
#


def close_year_periods(year: Year):
    for counter, period in enumerate(Period.objects.filter(period_year=year).order_by('period'), start=1):
        if counter < year.number_of_periods:
            period.mark_as_closed()


def ensure_last_period_is_open(year: Year):
    last_period = Period.objects.filter(period_year=year).order_by('period').last()
    if last_period:
        last_period.mark_as_opened()


def fixed_assets_closing_balances(previous_year: Year, year: Year):
    fixed_assets = get_assets_balances(company=year.company, year=previous_year)
    for asset_id, asset in fixed_assets.items():
        fixed_asset = asset['asset']
        if fixed_asset.fixed_asset_disposal.count() == 0:
            asset_value = asset['closing_balance']
            depreciation_value = asset['closing_depreciation']
            if asset_value > 0:
                FixedAssetOpeningBalance.objects.update_or_create(
                    year=year,
                    fixed_asset=fixed_asset,
                    defaults={
                        'asset_value': asset_value,
                        'depreciation_value': depreciation_value
                    }
                )


def general_ledger_closing_balances(company: Company, previous_year: Year, year: Year):
    close_year_profit_journals(company=company, year=previous_year)

    for account in Account.objects.balance_sheet().filter(company=previous_year.company):
        balance = calculate_account_balance(year=previous_year, account=account)

        OpeningBalance.objects.update_or_create(
            year=year,
            company=year.company,
            account=account,
            defaults={
                'amount': balance
            }
        )
    copy_profit_loss_opening_balance(company=company, previous_year=previous_year, year=year)


def calculate_account_balance(account: Account, year: Year):
    logger.info(f"calculate_account_balance for account {account}")
    account_balance = JournalLine.objects.get_for_account(year=year, company=year.company, account=account,
                                                          start_date=year.start_date, end_date=year.end_date)
    opening_balance = account.opening_balances.filter(year=year).first()
    return (opening_balance.amount if opening_balance else 0) + (account_balance['total'] if account_balance['total'] else 0)


def copy_profit_loss_opening_balance(company: Company, previous_year: Year, year: Year):
    default_previous_year_account = company.default_previous_year_account

    profit = get_profit(company=company, year=previous_year)
    logger.info(f"Add profit of {profit} to {default_previous_year_account} to year {year}")
    if profit and default_previous_year_account:
        OpeningBalance.objects.update_or_create(
            year=year,
            company=year.company,
            account=default_previous_year_account,
            defaults={
                'amount': profit
            }
        )


def get_profit(company, year):
    previous_year = Year.objects.filter(year=year.year - 1, company=company).first()

    income_statement = IncomeStatement(company=company, start_date=year.start_date, end_date=year.end_date, year=year,
                                       previous_year=previous_year, include_previous_year=False)
    income_statement.execute()
    return income_statement.lines['tax']['total']


def close_year_profit_journals(company: Company, year: Year):
    logger.info(f'---close_year_profit_journals--- company {company}')
    logger.info(f'---default_previous_year_account---  {company.default_previous_year_account}')
    logger.info(f'---default_retained_income_account---  {company.default_retained_income_account}')
    default_previous_year_account = company.default_previous_year_account
    default_retained_income_account = company.default_retained_income_account

    profit_balance = 0
    if default_previous_year_account:
        profit_balance = calculate_account_balance(account=default_previous_year_account, year=year)
    logger.info(f"balance for account {default_previous_year_account} is {profit_balance}")
    if profit_balance != 0:
        period = year.last_period
        journal = Journal.objects.create(
            company=year.company,
            year=year,
            period=period,
            date=timezone.now().date(),
            journal_text='Year closing',
            module=Module.SYSTEM.value
        )
        if journal:
            JournalLine.objects.create(
                journal=journal,
                accounting_date=timezone.now().date(),
                credit=profit_balance * -1 if profit_balance < 0 else 0,
                debit=profit_balance if profit_balance > 0 else 0,
                account=default_retained_income_account
            )
            JournalLine.objects.create(
                journal=journal,
                accounting_date=timezone.now().date(),
                debit=profit_balance * -1 if profit_balance < 0 else 0,
                credit=profit_balance if profit_balance > 0 else 0,
                account=default_previous_year_account
            )


def inventory_closing_balances(company: Company, year: Year):
    first_period = year.first_period
    for branch in Branch.objects.filter(company=company):
        transaction, is_new = Received.all.get_or_create(
            movement_type=MovementType.OPENING_BALANCE,
            branch=branch,
            period=first_period,
            defaults={
                'date': timezone.now().date(),
                'note': 'Year Closing'
            }
        )
        for inventory in BranchInventory.objects.filter(branch=branch):
            InventoryReceived.objects.get_or_create(
                received=transaction,
                inventory=inventory,
                defaults={
                    'price_excluding': inventory.average_price,
                    'quantity': inventory.in_stock
                }
            )


def customer_closing_balances(previous_year: Year, year: Year):
    last_period = previous_year.last_period
    for customer_age in Customer.objects.get_age_analysis(period=last_period):
        CustomerOpeningBalance.objects.get_or_create(
            customer=customer_age.customer,
            year=year,
            defaults={
                'amount': customer_age.balance
            }
        )
        for counter, next_period in enumerate(Period.objects.filter(period_year=year).order_by('period')):
            period_age, is_new = CustomerAgeAnalysis.objects.get_or_create(
                customer=customer_age.customer,
                period=next_period
            )
            if counter == 0:
                period_age.thirty_day += customer_age.current or Decimal(0)
                period_age.sixty_day += customer_age.thirty_day or Decimal(0)
                period_age.ninety_day += customer_age.sixty_day or Decimal(0)
                period_age.one_twenty_day += (customer_age.ninety_day or Decimal(0)) + (
                        customer_age.one_twenty_day or Decimal(0))
                period_age.balance += customer_age.current + customer_age.thirty_day + customer_age.sixty_day + customer_age.ninety_day + customer_age.one_twenty_day
            if counter == 1:
                period_age.sixty_day += customer_age.current or Decimal(0)
                period_age.ninety_day += (customer_age.thirty_day or Decimal(0))
                period_age.one_twenty_day += (customer_age.sixty_day or Decimal(0)) + (customer_age.ninety_day or Decimal(0))
                period_age.one_twenty_day += customer_age.one_twenty_day or Decimal(0)
                period_age.balance += customer_age.current + customer_age.thirty_day + customer_age.sixty_day + customer_age.ninety_day + customer_age.one_twenty_day
            if counter == 2:
                period_age.ninety_day += (customer_age.current or Decimal(0))
                period_age.one_twenty_day += (customer_age.thirty_day or Decimal(0)) + (
                        customer_age.sixty_day or Decimal(0)) + (customer_age.ninety_day or Decimal(0)) + (
                        customer_age.one_twenty_day or Decimal(0))
                period_age.balance += customer_age.current + customer_age.thirty_day + customer_age.sixty_day + customer_age.ninety_day + customer_age.one_twenty_day
            if counter >= 3:
                period_age.one_twenty_day += (customer_age.one_twenty_day or Decimal(0)) + customer_age.current + customer_age.thirty_day + customer_age.sixty_day + customer_age.ninety_day + customer_age.one_twenty_day
                period_age.balance += customer_age.current + customer_age.thirty_day + customer_age.sixty_day + customer_age.ninety_day + customer_age.one_twenty_day
            period_age.unallocated += customer_age.unallocated
            period_age.balance += customer_age.unallocated
            period_age.save()


def supplier_closing_balances(previous_year: Year, year: Year):
    last_period = previous_year.last_period
    for supplier_age in Supplier.objects.get_age_analysis(period=last_period, company=previous_year.company):
        SupplierOpeningBalance.objects.update_or_create(
            company=year.company,
            supplier=supplier_age.supplier,
            year=year,
            defaults={
                'amount': supplier_age.balance * -1 if supplier_age.balance else 0
            }
        )
        for counter, next_period in enumerate(Period.objects.filter(period_year=year).order_by('period')):
            period_age_analysis, is_new = SupplierAgeAnalysis.objects.get_or_create(
                supplier=supplier_age.supplier,
                period=next_period
            )
            if counter == 0:
                period_age_analysis.thirty_day += supplier_age.current or Decimal(0)
                period_age_analysis.sixty_day += supplier_age.thirty_day or Decimal(0)
                period_age_analysis.ninety_day += supplier_age.sixty_day or Decimal(0)
                period_age_analysis.one_twenty_day += (supplier_age.ninety_day or Decimal(0)) + (
                        supplier_age.one_twenty_day or Decimal(0))
                period_age_analysis.balance += supplier_age.current + supplier_age.thirty_day + supplier_age.sixty_day + supplier_age.ninety_day + supplier_age.one_twenty_day
            if counter == 1:
                period_age_analysis.sixty_day += supplier_age.current or Decimal(0)
                period_age_analysis.ninety_day += supplier_age.thirty_day or Decimal(0)
                period_age_analysis.one_twenty_day += (supplier_age.sixty_day or Decimal(0)) + (supplier_age.ninety_day or Decimal(0))
                period_age_analysis.one_twenty_day += supplier_age.one_twenty_day or Decimal(0)
                period_age_analysis.balance += supplier_age.current + supplier_age.thirty_day + supplier_age.sixty_day + supplier_age.ninety_day + supplier_age.one_twenty_day
            if counter == 2:
                period_age_analysis.ninety_day += (supplier_age.current or Decimal(0))
                period_age_analysis.one_twenty_day += (supplier_age.thirty_day or Decimal(0)) + (
                        supplier_age.sixty_day or Decimal(0)) + (supplier_age.ninety_day or Decimal(0)) + (
                        supplier_age.one_twenty_day or Decimal(0))
                period_age_analysis.balance += supplier_age.current + supplier_age.thirty_day + supplier_age.sixty_day + supplier_age.ninety_day + supplier_age.one_twenty_day
            if counter >= 3:
                period_age_analysis.one_twenty_day += (supplier_age.one_twenty_day or Decimal(0)) + supplier_age.current + supplier_age.thirty_day + supplier_age.sixty_day + supplier_age.ninety_day + supplier_age.one_twenty_day
                period_age_analysis.balance += supplier_age.current + supplier_age.thirty_day + supplier_age.sixty_day + supplier_age.ninety_day + supplier_age.one_twenty_day
            period_age_analysis.unallocated += supplier_age.unallocated
            period_age_analysis.balance += supplier_age.unallocated
            period_age_analysis.save()


def distribution_balances(year: Year):
    pass


def goods_received_balances(year: Year):
    pass


def invoices_not_year_posted(year: Year):
    pass


def calculate_end_date(start_date, number_of_periods):
    end_month = start_date.month + (number_of_periods - 1)
    year_number = start_date.year
    if end_month > 12:
        year_number = year_number + 1
        end_month = start_date.month - 1
    end_date = start_date.replace(year=year_number, month=end_month, day=calendar.monthrange(year=year_number, month=end_month)[1])
    return end_date


def creating_next_year(year: Year):
    number_of_periods = 12
    if year:
        year_number = year.year + 1
        start_date = date(year=year.start_date.year + 1, day=year.start_date.day, month=year.start_date.month)
        number_of_periods = year.number_of_periods
    else:
        year_number = timezone.now().date().year + 1
        start_date = date(year=year_number, month=1, day=1)

    end_date = calculate_end_date(
        start_date=start_date,
        number_of_periods=number_of_periods
    )

    next_year, is_new = Year.objects.update_or_create(
        company=year.company,
        year=year_number,
        defaults={
            'is_active': False,
            'start_date': start_date,
            'end_date': end_date,
            'number_of_periods': number_of_periods,
            'status': YearStatus.NOT_STARTED
        }
    )
    if next_year:
        process_year_periods(year=next_year)
    return next_year


def opening_first_period(year: Year):
    period = Period.objects.filter(period_year=year).order_by('period').first()
    if period:
        period.mark_as_opened()


def opening_login(company: Company):
    company.mark_as_active()


def get_current_year(profile_id, company_id, year_id=None):
    cache_key = get_profile_year_cache_key(profile_id=profile_id, company_id=company_id)
    if year_id and year_id != -1:
        year = Year.objects.get(pk=year_id)
        cache.set(cache_key, year)
        return year
    elif company_id and isinstance(company_id, int):
        year = cache.get(cache_key)
        if not year:
            year = Year.objects.filter(company_id=company_id, is_active=True).first()
            cache.set(cache_key, year)
        return year
    else:
        return None
