from .models import Year
from . import enums


def get_open_year(company):
    return Year.objects.open(company=company).filter(is_active=True).first()
