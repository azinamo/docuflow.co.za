import datetime

from django.db import models
from django.db.models import Count, Q
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as __
from django.conf import settings
from dateutil.relativedelta import  relativedelta


from enumfields import EnumIntegerField
from safedelete.models import SafeDeleteModel, HARD_DELETE_NOCASCADE

from docuflow.apps.company.models import Company
from .exceptions import HasOpenPeriods, HasFinalSignedDocuments
from .signals import period_close, period_open
from .enums import PeriodStatus, YearStatus, BudgetType


class YearManager(models.Manager):

    def get_queryset(self):
        return super(YearManager, self).get_queryset().filter(deleted__isnull=True)

    def active(self, company, year=None):
        filter_options = {'company': company, 'is_active': True}
        if year:
            filter_options['year'] = year
        return self.filter(**filter_options)

    def closed(self, company, year=None):
        filter_options = {'company': company, 'status': YearStatus.CLOSING.value}
        if year:
            filter_options['year'] = year
        return self.filter(**filter_options)

    def started(self):
        return self.filter(Q(status=YearStatus.STARTED.value) | Q(status=YearStatus.OPEN.value))

    def agable(self):
        return self.exclude(Q(status=YearStatus.FINAL.value) | Q(status=YearStatus.NOT_STARTED.value))

    def open(self, company, year=None):
        filter_options = {'company': company, 'status': YearStatus.OPEN.value}
        if year:
            filter_options['year'] = year
        return self.filter(**filter_options)

    def locked(self, company, year=None):
        filter_options = {'company': company, 'status': YearStatus.LOCKED.value}
        if year:
            filter_options['year'] = year
        return self.filter(**filter_options)


class Year(SafeDeleteModel):
    _safedelete_policy = HARD_DELETE_NOCASCADE

    company = models.ForeignKey(Company, related_name='company_years', on_delete=models.CASCADE)
    year = models.PositiveIntegerField(verbose_name='Year')
    start_date = models.DateField(null=True, blank=True, verbose_name='Period Start Date')
    end_date = models.DateField(null=True, blank=True, verbose_name='End Date')
    status = EnumIntegerField(YearStatus, default=YearStatus.NOT_STARTED)
    is_open = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    journal_number = models.CharField(max_length=255, blank=True, verbose_name='Journal Number Starts From')
    number_of_periods = models.PositiveIntegerField(default=12)
    budget_type = EnumIntegerField(BudgetType, default=BudgetType.WHOLE_COMPANY)

    objects = YearManager()

    class Meta:
        unique_together = ('year', 'company')
        ordering = ['-year']
        verbose_name = 'Year'
        verbose_name_plural = 'Years'

    def __str__(self):
        return f"{self.year}"

    def has_open_periods(self):
        return self.year_periods.filter(models.Q(is_closed=False) | models.Q(status=YearStatus.OPEN)).count()

    def mark_as_closing(self):
        self.status = YearStatus.CLOSING
        self.is_active = False
        self.save()

    def mark_as_closed(self):
        self.status = YearStatus.CLOSING
        periods = self.has_open_periods()
        if periods:
            raise HasOpenPeriods(__('Year cannot be closed. There are still some periods open.'))
        self.save()

    def mark_as_open(self):
        self.status = YearStatus.STARTED
        self.save()

    def mark_as_final(self):
        self.status = YearStatus.FINAL
        self.save()

    def open(self):
        return self.status == YearStatus.OPEN

    def closed(self):
        return self.status == YearStatus.CLOSING

    def locked(self):
        return self.status == YearStatus.LOCKED

    def budget(self):
        if self.has_budget:
            return self.budgets.all()[0]
        return None

    @cached_property
    def has_budget(self):
        return self.budgets.count() > 0 if self.budgets else False

    @property
    def type_name(self):
        return self.budget_type

    @property
    def year_end_at(self):
        if not self.end_date:
            for period in self.year_periods.order_by('-to_date').all():
                return period.to_date
            return None
        else:
            return self.end_date

    @property
    def last_period_open(self):
        return self.year_periods.filter(status=PeriodStatus.OPEN).order_by('-period').first()

    @property
    def is_individual_objects(self):
        return self.budget_type == BudgetType.INDIVIDUAL_OBJECTS

    @property
    def is_final(self):
        return self.status == YearStatus.FINAL

    @property
    def is_not_started(self):
        return self.status == YearStatus.NOT_STARTED

    @property
    def is_closing(self):
        return self.status == YearStatus.CLOSING

    @property
    def is_running(self):
        return self.status in [YearStatus.OPEN, YearStatus.STARTED]

    @property
    def can_manage(self):
        return self.status in [YearStatus.OPEN, YearStatus.STARTED, YearStatus.CLOSING]

    @property
    def first_period(self):
        return Period.objects.filter(period_year=self, period=1).first()

    @property
    def second_last_period(self):
        return Period.objects.filter(period_year=self, period=self.number_of_periods - 1).first()

    @property
    def last_period(self):
        return Period.objects.filter(period_year=self, period=self.number_of_periods).first()

    @property
    def can_be_started(self):
        if self.is_not_started and not Year.objects.filter(company=self.company, status=YearStatus.CLOSING).exists():
            if settings.DEBUG:  # Can test opening even if the year has not started
                return True
            return timezone.now().date() >= (self.start_date + timezone.timedelta(days=-1))
        return False


class PeriodManager(models.Manager):
    def active_year(self):
        return self.filter(period_year__is_active=True)

    def for_year(self, year):
        return self.filter(period_year=year)

    def opened(self):
        return self.filter(status=PeriodStatus.OPEN)

    def company_year(self, company, year):
        return self.filter(period_year__company=company, period_year=year)

    def closed(self, company, year):
        return self.filter(status=PeriodStatus.CLOSED, period_year__company=company, period_year=year)

    def open(self, company, year):
        return self.filter(status=PeriodStatus.OPEN, period_year__company=company, period_year=year)

    def get_by_date(self, company, period_date):
        return self.filter(from_date__lte=period_date, to_date__gte=period_date, period_year__company=company)

    def open_for_depreciation_update(self, year, company):
        return self.annotate(
            assets_updates_count=Count('assets_updates')
        ).filter(
            status=PeriodStatus.OPEN, period_year__company=company, period_year=year
        )

    def with_no_distributions(self, company, year):
        return self.annotate(
            distribution_count=models.Case(
                models.When(distributions__ledger_line__isnull=False, then=Count('distributions')),
                default=0,
                output_field=models.IntegerField()
            )
        ).filter(
            is_closed=False, period_year__is_open=True, period_year__company=company, period_year=year,
            distribution_count=0
        )

    def get_ranges(self, year, period_to, count=5):
        return Period.objects.select_related('period_year').filter(
            period_year__company=year.company, id__lte=period_to.id
        ).exclude(
            period_year__isnull=True
        ).order_by(
            'period_year', '-period'
        )[0:count]

    def pending_locking(self, company, period=None):
        from_date = timezone.now().date() - relativedelta(months=1)
        if period:
            from_date = period.to_date

        return self.filter(
            company=company, period_year__is_active=True, from_date__lt=from_date
        ).exclude(status=PeriodStatus.LOCKED).order_by('period')


class Period(models.Model):
    period_year = models.ForeignKey(Year, related_name='year_periods', on_delete=models.CASCADE)
    period = models.PositiveIntegerField(null=False, blank=False, verbose_name='Period Number')
    name = models.CharField(max_length=255)
    from_date = models.DateField(default=None, null=False)
    to_date = models.DateField(default=None, null=False)
    company = models.ForeignKey(Company, default=None, null=False, related_name='company_periods', on_delete=models.CASCADE)
    status = EnumIntegerField(PeriodStatus, default=PeriodStatus.OPEN)
    is_closed = models.BooleanField(default=False)

    objects = PeriodManager()

    def __repr__(self):
        return f"{self.period_year} - {self.period}"

    def __str__(self):
        return f"{self.period}"

    class Meta:
        ordering = ['-period']
        verbose_name_plural = 'Periods'

    def is_valid(self, date):
        return self.from_date <= date <= self.to_date

    def has_final_signed_invoices(self):
        return self.invoices.filter(period=self, status__slug='final-signed').exists()

    def mark_as_closed(self):
        invoices = self.has_final_signed_invoices()
        if invoices:
            raise HasFinalSignedDocuments(__('There are final signed documents in this period, please fix'))
        self.status = PeriodStatus.CLOSED
        self.save()
        period_close.send(sender=self.__class__, period=self)

    def mark_as_opened(self):
        self.status = PeriodStatus.OPEN
        self.save()
        period_close.send(sender=self.__class__, period=self)

    @property
    def is_locked(self):
        return self.status == PeriodStatus.LOCKED

    @property
    def is_open(self):
        return self.status == PeriodStatus.OPEN

    @property
    def is_closed_period(self):
        return self.status == PeriodStatus.CLOSED

