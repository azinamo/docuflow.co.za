import django_filters
from .models import Period, Year
from docuflow.apps.company.models import Company


def years(request):
    if request is None:
        return Year.objects.none()
    return Year.objects.filter(company=request.session['company'])


class PeriodFilter(django_filters.FilterSet):
    period_year = django_filters.ModelChoiceFilter(queryset=years)

    class Meta:
        model = Period
        fields = ['name', 'is_closed', 'period_year']

