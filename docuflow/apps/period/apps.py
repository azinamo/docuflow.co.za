from django.apps import AppConfig

# from .services import close_period
# from .signals import period_open, period_close


class PeriodConfig(AppConfig):
    name = 'docuflow.apps.period'

    # def ready(self):
    #     period_close.connect(close_period)
