import logging

from django.urls import reverse
from django.db import migrations, models

logger = logging.getLogger(__name__)


def move_purchase_orders(app, schema_editor):
    PurchaseOrder = app.get_model('purchaseorders', 'PurchaseOrder')
    PurchaseOrderItem = app.get_model('purchaseorders', 'PurchaseOrderItem')

    InvoicePurchaseOrder = app.get_model('invoice', 'Invoice')
    InvoicePurchaseOrderItem = app.get_model('invoice', 'PurchaseOrder')
    Comment = app.get_model('invoice', 'Comment')

    Unit = app.get_model('company', 'Unit')

    POInventory = app.get_model('inventory', 'PurchaseOrder')
    POInventoryItem = app.get_model('inventory', 'PurchaseOrderInventory')
    Received = app.get_model('inventory', 'Received')

    for invoice_po in InvoicePurchaseOrder.objects.filter(invoice_type__code='purchase-order'):
        po = PurchaseOrder.objects.create(
            invoice_id=invoice_po.id,
            purchase_order_id=invoice_po.invoice_id
        )
        for pi_item in InvoicePurchaseOrderItem.objects.filter(invoice=invoice_po):
            measure_unit = Unit.objects.filter(unit__icontains=pi_item.unit.lower()).first()
            if measure_unit:
                unit_id = measure_unit.id
            else:
                unit_id = 1
            total = 0
            if pi_item.quantity and pi_item.order_value:
                total = pi_item.order_value * pi_item.quantity
            PurchaseOrderItem.objects.create(
                vat_code_id=pi_item.vat_code_id,
                vat_amount=pi_item.vat_amount,
                total_amount=total,
                unit_id=unit_id,
                quantity=pi_item.quantity,
                description=pi_item.description,
                purchase_order=po
            )
        request_comment = Comment.objects.filter(status__slug='po-request', invoice_id=invoice_po.id).first()
        approved_comment = Comment.objects.filter(status__slug='po-approved', invoice_id=invoice_po.id).first()
        ordered_comment = Comment.objects.filter(status__slug='po-ordered', invoice_id=invoice_po.id).first()
        if request_comment:
            add_tracking(app, invoice_po, po, request_comment, 'request', 'Purchase Order requested')
        if approved_comment:
            add_tracking(app, invoice_po, po, approved_comment, 'approved', 'Purchase Order approved')
        if ordered_comment:
            add_tracking(app, invoice_po, po, ordered_comment, 'ordered', 'Purchase Order ordered')

    for po_inventory in POInventory.objects.all():
        inventory_received = Received.objects.filter(purchase_order_id=po_inventory.id).first()

        defaults = {
            'memo_to_supplier': po_inventory.memo_to_supplier,
            'purchase_order_id': po_inventory.invoice.invoice_id
        }

        _po, is_new = PurchaseOrder.objects.get_or_create(
            invoice_id=po_inventory.invoice_id,
            defaults=defaults
        )
        if inventory_received:
            logger.info(f"Old purchase order id for {str(inventory_received)}  is {inventory_received.purchase_order.id}")
            _po.received_id = inventory_received.id
            _po.save()
            logger.info(f"New Purchase order {_po.id} linked to received {inventory_received.id}")
            logger.info(defaults)
        po_items = POInventoryItem.objects.filter(purchase_order=po_inventory)
        for po_item in po_items.all():
            total = 0
            if po_item.quantity and po_item.order_value:
                total = po_item.order_value * po_item.quantity
            PurchaseOrderItem.objects.create(
                vat_code_id=po_item.vat_code_id,
                vat_amount=po_item.vat_amount,
                total_amount=total,
                unit=po_item.unit,
                quantity=po_item.quantity,
                unit_price=po_item.order_value,
                status=po_item.status,
                inventory=po_item.inventory,
                purchase_order=_po
            )

        request_comment = Comment.objects.filter(status__slug='po-request', invoice_id=po_inventory.invoice_id).first()
        approved_comment = Comment.objects.filter(status__slug='po-approved', invoice_id=po_inventory.invoice_id).first()
        ordered_comment = Comment.objects.filter(status__slug='po-ordered', invoice_id=po_inventory.invoice_id).first()
        if request_comment:
            add_tracking(app, po_inventory.invoice, _po, request_comment, 'request', 'Purchase Order requested')
        if approved_comment:
            add_tracking(app, po_inventory.invoice, _po, approved_comment, 'approved', 'Purchase Order approved')
        if ordered_comment:
            add_tracking(app, po_inventory.invoice, _po, ordered_comment, 'ordered', 'Purchase Order ordered')

        if inventory_received:
            inventory_received.purchase_order = None
            inventory_received.save()


def add_tracking(app, invoice, po, comment, typ, text):
    Tracking = app.get_model('purchaseorders', 'Tracking')
    Tracking.objects.create(
        purchase_order=po,
        tracking_type=typ,
        comment=text,
        profile=comment.participant.profile,
        role=comment.role,
        created_at=comment.created_at,
        data={
            'link_url': reverse('invoice_detail', args=(invoice.id,))
        }
    )


class Migration(migrations.Migration):
    dependencies = [
        ('purchaseorders', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(move_purchase_orders, reverse_code=migrations.RunPython.noop)
    ]