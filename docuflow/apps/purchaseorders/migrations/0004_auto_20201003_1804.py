# Generated by Django 3.1 on 2020-10-03 16:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('purchaseorders', '0003_auto_20201003_1635'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaseorderitem',
            name='discount_amount',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=12),
        ),
        migrations.AddField(
            model_name='purchaseorderitem',
            name='net_amount',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=12),
        ),
    ]
