# Generated by Django 3.1 on 2020-10-02 01:56

from django.db import migrations, models
import django.db.models.deletion
import docuflow.apps.purchaseorders.enums
import enumfields.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounts', '0007_auto_20200904_1821'),
        ('company', '0018_auto_20200917_1507'),
        ('invoice', '0016_data_distribution_linking'),
        ('inventory', '0028_auto_20200815_0352'),
    ]

    operations = [
        migrations.CreateModel(
            name='PurchaseOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('purchase_order_id', models.CharField(max_length=255)),
                ('memo_to_supplier', models.TextField(blank=True)),
                ('invoice', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='purchaseorder', to='invoice.invoice')),
                ('parent', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='backorder', to='purchaseorders.purchaseorder')),
                ('received', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='po', to='inventory.received')),
            ],
        ),
        migrations.CreateModel(
            name='Tracking',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateField(auto_created=True)),
                ('tracking_type', models.CharField(max_length=255)),
                ('comment', models.TextField()),
                ('data', models.JSONField(blank=True)),
                ('profile', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='accounts.profile')),
                ('purchase_order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='trackings', to='purchaseorders.purchaseorder')),
                ('role', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='accounts.role')),
            ],
        ),
        migrations.CreateModel(
            name='PurchaseOrderItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=255)),
                ('quantity', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=12)),
                ('unit_price', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=12)),
                ('vat_percentage', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=5)),
                ('vat_amount', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=12)),
                ('total_amount', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=12)),
                ('status', enumfields.fields.EnumIntegerField(default=1, enum=docuflow.apps.purchaseorders.enums.PurchaseOrderItemStatus)),
                ('account', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='purchaseorders', to='company.account')),
                ('inventory', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='purchaseorders', to='inventory.branchinventory')),
                ('purchase_order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='purchaseorder_items', to='purchaseorders.purchaseorder')),
                ('unit', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='company.unit')),
                ('vat_code', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='company.vatcode')),
            ],
        ),
    ]
