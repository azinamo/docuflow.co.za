import logging
import pprint

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.db import transaction
from django.http import JsonResponse
from django.template.loader import get_template
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, View, TemplateView, DetailView
from django.views.generic.edit import UpdateView

from docuflow.apps.common.mixins import ProfileMixin, AddRowMixin, DataTableMixin
from docuflow.apps.common.utils import generate_tabbing_matrix, is_ajax_post, is_ajax
from docuflow.apps.company.models import VatCode, Unit, ObjectItem
from docuflow.apps.invoice import services as invoice_services
from docuflow.apps.invoice.models import Invoice, InvoiceType, Status
from docuflow.apps.purchaseorders.models import PurchaseOrder
from . import services
from .forms import PurchaseOrderForm, DeclineForm

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class PurchaseOrderView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'purchaseorders/index.html'


class CreatePurchaseOrderView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'purchaseorders/create.html'
    success_message = 'Purchase order successfully saved'
    form_class = PurchaseOrderForm
    success_url = reverse_lazy('purchaseorder_index')

    def get_context_data(self, **kwargs):
        context = super(CreatePurchaseOrderView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['invoice_type'] = InvoiceType.objects.purchase_order(self.get_company()).first()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreatePurchaseOrderView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if is_ajax_post(self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the purchase order, please fix the errors.',
                                 'errors': form.errors,
                                 }, status=400)
        return super(CreatePurchaseOrderView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            form.save()
            return JsonResponse({
                'text': 'Purchase order saved successfully.',
                'error': False,
                'redirect': reverse('purchaseorder_index')
            })


class PurchaseOrderDetailView(ProfileMixin, DetailView):
    model = PurchaseOrder
    template_name = 'purchaseorders/detail.html'
    context_object_name = 'purchase_order'

    def get_template_names(self):
        if self.request.GET.get('print') == '1':
            return 'purchaseorders/print.html'
        return 'purchaseorders/detail.html'

    def get_queryset(self):
        return PurchaseOrder.objects.select_related(
            'invoice', 'invoice__period', 'invoice__company', 'invoice__invoice_type',
            'invoice__supplier', 'invoice__branch'
        ).prefetch_related(
            'purchaseorder_items', 'purchaseorder_items__inventory', 'purchaseorder_items__inventory__inventory',
            'purchaseorder_items__vat_code', 'purchaseorder_items__account', 'purchaseorder_items__unit',
            'trackings', 'trackings__profile', 'trackings__role'
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_print'] = self.request.GET.get('print') == '1'
        return context


class PurchaseOrderTemplateView(PurchaseOrderDetailView):

    def get_template_names(self):
        return 'purchaseorders/template.html'


class AddPurchaseOrderItemRowView(ProfileMixin, AddRowMixin, TemplateView):
    template_name = 'purchaseorders/po_items.html'

    def get_vat_code(self):
        return VatCode.objects.input().filter(company=self.get_company(), is_default=True).first()

    def get_units(self):
        return Unit.objects.filter(measure__company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(AddPurchaseOrderItemRowView, self).get_context_data(**kwargs)
        row_id = self.get_row_start()
        is_reload = True if self.request.GET.get('reload', 0) == '1' else False
        vat_code = self.get_vat_code()
        col_count = 9
        row_count = self.get_rows()
        tabs_start = self.get_tabs_start(4, row_id, is_reload)
        context['units'] = self.get_units()
        context['is_deletable'] = int(row_id) > 0
        context['row_id'] = row_id
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['default_vat_code'] = vat_code
        context['module_url'] = reverse('add_purchaseorder_row')
        context['tabs_start'] = tabs_start
        context['start'] = 0
        context['rows'] = generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['is_reload'] = is_reload
        context['row_start'] = row_id
        context['is_copy'] = self.request.GET.get('is_copy')
        return context


class ApprovePurchaseOrderView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        if is_ajax(request):
            try:
                purchase_order = PurchaseOrder.objects.select_related('invoice').get(invoice_id=self.kwargs['invoice_id'])
                with transaction.atomic():

                    next_invoice_id = services.approve(purchase_order, self.get_profile(), self.get_role())
                    requested_redirect_url = self.request.GET.get('redirect_url')
                    if requested_redirect_url and requested_redirect_url != '':
                        redirect_url = requested_redirect_url
                    else:
                        redirect_url = reverse('all-invoices')
                        if next_invoice_id:
                            redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                    return JsonResponse({
                        'error': False,
                        'text': 'Purchase order successfully approved.',
                        'redirect': redirect_url
                    })
            except PurchaseOrder.DoesNotExist:
                return JsonResponse({
                    'error': False,
                    'text': 'Purchase order not found',
                }, status=404)


class OrderPurchaseOrderView(LoginRequiredMixin, ProfileMixin, View):

    def get_purchase_oder(self):
        return PurchaseOrder.objects.select_related('invoice').get(invoice_id=self.kwargs['invoice_id'])

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            with transaction.atomic():
                purchase_order = self.get_purchase_oder()

                next_invoice_id = services.order(purchase_order, self.get_profile(), self.get_role())
                requested_redirect_url = self.request.GET.get('redirect_url')
                if requested_redirect_url and requested_redirect_url != '':
                    redirect_url = requested_redirect_url
                else:
                    redirect_url = reverse('all-invoices')
                    if next_invoice_id:
                        redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                return JsonResponse({
                    'error': False,
                    'text': 'Purchase order successfully ordered.',
                    'redirect': redirect_url
                })


class ResubmitPurchaseOrderView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            purchase_order = PurchaseOrder.objects.get(pk=self.kwargs['pk'])
            services.resubmit(purchase_order=purchase_order, profile=self.get_profile(), role=self.get_role())
            return JsonResponse({'text': 'Purchase order successfully resubmitted',
                                 'error': False,
                                 'redirect': reverse('all-invoices')
                                 })
        except PurchaseOrder.DoesNotExist:
            return JsonResponse({'text': 'Purchase order not found',
                                 'error': True
                                 })


class DeclineView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "purchaseorders/decline.html"
    form_class = DeclineForm

    def get_purchase_order(self):
        return PurchaseOrder.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(DeclineView, self).get_context_data(**kwargs)
        purchase_order = self.get_purchase_order()
        context['purchase_order'] = purchase_order
        return context

    def form_invalid(self, form):
        if is_ajax_post(self.request):
            return JsonResponse({'error': True,
                                 'text': 'Purchase order could not be declined, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(DeclineView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            purchase_order = self.get_purchase_order()

            next_invoice_id = services.decline(purchase_order, self.get_profile(), self.get_role(), form.cleaned_data['comment'])
            requested_redirect_url = self.request.GET.get('redirect_url')
            if requested_redirect_url and requested_redirect_url != '':
                redirect_url = requested_redirect_url
            else:
                redirect_url = reverse('all-invoices')
                if next_invoice_id:
                    redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
            return JsonResponse({'error': False,
                                 'text': 'Purchase order successfully declined.',
                                 'redirect': redirect_url
                                 })


class EditPurchaseOrderLineView(ProfileMixin, UpdateView):
    template_name = 'purchaseorders/edit.html'
    form_class = PurchaseOrderForm

    def get_object(self, queryset=None):
        return PurchaseOrder.objects.get(pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super(EditPurchaseOrderLineView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditPurchaseOrderLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        purchaser_order = self.get_object()
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['purchase_order'] = purchaser_order
        return context

    def form_valid(self, form):
        form.save()
        return JsonResponse({'text': 'Line updated successfully',
                             'error': False,
                             'reload': True
                             })


class CreatePurchaseOrderLineView(ProfileMixin, FormView):
    template_name = 'purchaseorders/create_line.html'
    form_class = PurchaseOrderForm

    def get_form_kwargs(self):
        kwargs = super(CreatePurchaseOrderLineView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(CreatePurchaseOrderLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['invoice'] = self.get_invoice()
        return context

    def form_valid(self, form):
        invoice = self.get_invoice()
        purchase_order_line = form.save(commit=False)
        purchase_order_line.invoice = invoice
        purchase_order_line.save()
        return JsonResponse({'text': 'Purchase order line saved successfully',
                             'error': False
                             })


class SearchGoodsReceivedPurchaseOrderView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        purchase_orders = []
        orders = PurchaseOrder.objects.in_order().for_branch(branch=self.get_branch()).filter(
            invoice__supplier_id=self.request.GET['supplier_id']
        )
        for po_order in orders:
            items_url = reverse('receive_purchase_order_items', kwargs={'purchase_order_id': po_order.id})
            purchase_orders.append({'text': str(po_order), 'id': po_order.id,
                                    'invoice_id': po_order.id, 'items_url': items_url
                                    })

        return JsonResponse({'orders': purchase_orders})


class SinglePurchaseOrderActionView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            purchase_order = PurchaseOrder.objects.get(pk=self.request.GET.get('purchase_order_id'))
            action = self.kwargs['action']
            if action == 'print':
                return JsonResponse({
                    'text': 'Redirecting ...',
                    'error': False,
                    'redirect': reverse('purchaseorder_detail', kwargs={'pk': purchase_order.id}) + "?print=1"
                })
            elif action == 'email':
                services.send_email(purchase_order, self.get_profile(), self.get_role())
                return JsonResponse({
                    'text': 'Purchase order email send successfully ...',
                    'error': False,
                })
            else:
                return JsonResponse({
                    'text': 'Action does not exist yet, please try again later',
                    'error': False,
                    'reload': True
                })
        except Invoice.DoesNotExist as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Invoice not found',
                'error': True,
                'details': str(exception)
            })


class AddPurchaseOrderRowView(ProfileMixin, TemplateView):
    template_name = 'purchaseorders/row.html'

    def get_context_data(self, **kwargs):
        context = super(AddPurchaseOrderRowView, self).get_context_data(**kwargs)
        context['row'] = int(self.request.GET['row']) + 1
        company = self.get_company()
        context['vat_codes'] = VatCode.objects.filter(company=company)
        return context


class PurchaseOrderRowsView(LoginRequiredMixin, TemplateView):
    template_name = 'purchaseorders/rows.html'

    def get_invoice(self):
        return Invoice.objects.prefetch_related('purchase_orders').get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(PurchaseOrderRowsView, self).get_context_data(**kwargs)

        invoice = self.get_invoice()
        po = services.PurchaseOrderReport(invoice=invoice)
        po.process_purchase_orders()

        context['po'] = po
        return context


class FilePurchaseOrderView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def sign_invoice(self, invoice):
        sign = services.SignInvoice(invoice=invoice, profile=self.get_profile(), role=self.get_role(), intervene=False)
        sign.process_signing()

        return invoice.get_role_next_invoice(role=self.get_role())

    def get(self, request, *args, **kwargs):
        if is_ajax(request):
            with transaction.atomic():
                invoice = self.get_invoice()

                next_invoice_id = self.sign_invoice(invoice)

                redirect_url = reverse('all-invoices')
                if next_invoice_id:
                    redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                return JsonResponse({
                    'error': False,
                    'text': 'Purchase order successfully filed.',
                    'redirect': redirect_url
                })


class EmailPurchaseOrderView(LoginRequiredMixin, ProfileMixin, View):

    def file_invoice(self, invoice, profile):
        sign = services.SignInvoice(invoice=invoice, profile=self.get_profile(), role=self.get_role(), intervene=False)
        sign.process_signing()

        next_invoice_id = invoice.get_role_next_invoice(role=self.get_role())

        return next_invoice_id

    def get(self, request, *args, **kwargs):
        if is_ajax(request):
            try:
                invoice = Invoice.objects.get(pk=self.kwargs['pk'])
                with transaction.atomic():
                    if invoice.supplier:
                        if invoice.supplier.email_address:
                            profile = self.get_profile()

                            po = services.PurchaseOrderReport(invoice=invoice)
                            po.process_purchase_orders()

                            subject = f"Purchase Order from your Customer: {invoice.company.name}"
                            from_addr = settings.PO_FROM_EMAIL
                            recipient_list = (settings.ADMIN_EMAIL, invoice.supplier.email_address)
                            template = get_template('purchaseorders/email.html')
                            message = template.render({
                                'invoice': invoice,
                                'po': po,
                                'created_by': po.get_created_by(),
                                'approved_by': po.get_approved_by(),
                                'ordered_by': po.get_ordered_by()
                            })
                            invoice_id = self.file_invoice(invoice, profile)

                            send_mail(subject, message, from_addr, recipient_list, html_message=message)

                            note = 'Email sent to supplier'
                            invoice.log_activity(self.request.user, self.request.session['role'], note, Comment.EVENT)
                            redirect_url = reverse('all-invoices')
                            if invoice_id:
                                redirect_url = reverse('invoice_detail', kwargs={'pk': invoice_id})
                            return JsonResponse({'error': False,
                                                 'text': 'Supplier email send successfully.',
                                                 'redirect': redirect_url
                                                 })
                        else:
                            return JsonResponse({'error': True,
                                                 'text': 'Supplier email not found'
                                                 })
                    else:
                        return JsonResponse({'error': False,
                                             'text': 'Invoice flow not found, please try again.',
                                             'redirect': reverse('all-invoices')
                                             })
            except Invoice.DoesNotExist as exception:
                return JsonResponse({'error': True,
                                     'text': 'Purchase order not found.',
                                     'alert': True
                                     }, status=404)


class CancelPurchaseOrderView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(pk=int(self.kwargs['pk']))
            with transaction.atomic():

                cancel = invoice_services.CancelPurchaseOrder(invoice=invoice, profile=self.get_profile(), role=self.get_role())
                cancel.process_cancellation()

                invoice_id = invoice.get_role_next_invoice(role=self.get_role())

                note = 'Purchase order cancelled'
                invoice.log_activity(self.request.user, self.request.session['role'], note)
                requested_redirect_url = self.request.GET.get('redirect_url')
                if requested_redirect_url and requested_redirect_url != '':
                    redirect_url = requested_redirect_url
                else:
                    redirect_url = reverse('all-invoices')
                    if invoice_id:
                        redirect_url = reverse('invoice_detail', kwargs={'pk': invoice_id})
                return JsonResponse({'error': False,
                                     'text': 'Purchase order cancelled successfully.',
                                     'redirect': redirect_url
                                     })
        except Invoice.DoesNotExist as exception:
            logger.exception(exception)
            return JsonResponse({'error': True,
                                 'text': 'Purchase order does not exist',
                                 'alert': True
                                 }, status=404)


class LinkPurchaseOrderView(LoginRequiredMixin, TemplateView):
    template_name = 'purchaseorders/show.html'

    def get_invoice(self):
        return Invoice.objects.prefetch_related('purchase_order_links').get(pk=self.kwargs['invoice_id'])

    def get_purchase_orders(self, invoice):
        invoice_purchase_orders = [po.id for po in invoice.purchase_order_links.all()]
        return Invoice.objects.filter(
            invoice_type__code='purchase-order', status__slug='po-ordered', supplier=invoice.supplier,
            company=self.request.session['company']
        ).exclude(id__in=invoice_purchase_orders).order_by('invoice_id')

    def get_context_data(self, **kwargs):
        context = super(LinkPurchaseOrderView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['purchase_orders'] = self.get_purchase_orders(invoice)
        return context


class SaveInvoicePurchaseOrderView(LoginRequiredMixin, View):

    def get(self, reqeuest, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(pk=self.kwargs['invoice_id'])
            purchase_orders_ids = self.request.GET.get('purchase_orders')
            po_linked = Status.objects.get(slug='po-linked')
            if purchase_orders_ids:
                po_ids = Invoice.objects.filter(pk__in=[int(po_id) for po_id in purchase_orders_ids.split(',')])
                for po in po_ids:
                    invoice.purchase_order_links.add(po)
                    po.status = po_linked
                    po.save()

                    invoice.log_activity(self.request.user, self.request.session['role'],
                                         f'Invoice linked to purchase order {po}', None, po)
                return JsonResponse({'text': 'Purchase order links saved',
                                     'error': False,
                                     'reload': True
                                     })
        except Invoice.DoesNotExist:
            return JsonResponse({'text': 'Invoice order does not exist',
                                 'error': False,
                                 'reload': True
                                 }, status=404)


class ShowPurchaseOrderView(LoginRequiredMixin, TemplateView):
    template_name = 'purchaseorders/template.html'

    def get_invoice(self):
        return Invoice.objects.prefetch_related(
            'purchase_orders'
        ).select_related(
            'inventory_purchase_order'
        ).get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(ShowPurchaseOrderView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice

        po = services.PurchaseOrderReport(invoice)
        po.process_purchase_orders()
        context['po'] = po
        ordered_by = po.get_ordered_by()
        created_by = po.get_created_by()
        approved_by = po.get_approved_by()

        context['created_by'] = created_by
        context['approved_by'] = approved_by
        context['ordered_by'] = ordered_by
        context['action_type'] = getattr(self.kwargs, 'type', 'none')
        context['is_print'] = self.kwargs.get('type', None) == 'print'
        return context


class PurchaseOrderLinesView(LoginRequiredMixin, TemplateView):
    template_name = 'purchaseorders/lines.html'

    def get_invoice(self):
        return Invoice.objects.select_related('supplier', 'status', 'company').get(pk=self.kwargs['pk'])

    @staticmethod
    def get_company_linked_modes(company):
        linked_models = {}
        count = 0
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            if count < 2:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
                    company_object_id=company_object.id)
            count += 1
        return linked_models

    def get_role_permissions(self):
        role_permissions = dict()
        role_id = str(self.request.session['role'])
        if 'permissions' in self.request.session and role_id in self.request.session['permissions']:
            role_permissions = self.request.session['permissions'][role_id]
        return role_permissions

    def get_context_data(self, **kwargs):
        context = super(PurchaseOrderLinesView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        linked_models = self.get_company_linked_modes(invoice.company)

        po = services.PurchaseOrderReport(invoice)
        po.process_purchase_orders()
        context['po'] = po
        can_edit = False
        if 'can_edit_invoice' in self.request.session:
            if invoice.id in self.request.session['can_edit_invoice']:
                can_edit = True
        if invoice.is_locked:
            can_edit = False
        context['can_edit'] = can_edit
        context['linked_models'] = linked_models
        context['linked_models_count'] = len(linked_models)
        context['invoice'] = invoice
        return context

