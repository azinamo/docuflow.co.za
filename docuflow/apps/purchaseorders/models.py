from decimal import Decimal

from django.db import models
from django.db.models import JSONField
from django.conf import settings

from enumfields import EnumIntegerField

from docuflow.apps.common.utils import create_document_number
from . import enums


class PurchaseOrderQuerySet(models.QuerySet):

    # def __init__(self):
    #     super(PurchaseOrderQuerySet, self).filter(invoice__invoice_type__code='purchase-order')

    def in_order(self):
        return self.filter(invoice__status__slug__in=['po-ordered'])

    def for_branch(self, branch):
        return self.filter(invoice__branch=branch)


class PurchaseOrder(models.Model):
    invoice = models.OneToOneField('invoice.Invoice', related_name='purchaseorder', on_delete=models.CASCADE)
    purchase_order_id = models.CharField(max_length=255)
    memo_to_supplier = models.TextField(blank=True)
    parent = models.OneToOneField('self', related_name='backorder', null=True, blank=True, on_delete=models.CASCADE)
    received = models.OneToOneField('inventory.Received', related_name='received', null=True, blank=True, on_delete=models.CASCADE)

    objects = PurchaseOrderQuerySet.as_manager()

    class Meta:
        ordering = ['purchase_order_id']

    def __str__(self):
        padding = settings.DEFAULT_PADDING
        return create_document_number(self.invoice.company, "PO", str(self.invoice.invoice_id), padding)

    def save(self, *args, **kwargs):
        if not self.pk and not self.purchase_order_id:
            filter_options = {'invoice__branch_id': self.invoice.branch.id}
            self.purchase_order_id = self.generate_invoice_number(1, filter_options)
        return super(PurchaseOrder, self).save(*args, **kwargs)

    def generate_invoice_number(self, counter, filter_options):
        try:
            purchase_order = PurchaseOrder.objects.filter(**filter_options).latest('purchase_order_id')
            try:
                purchase_order_id = int(purchase_order.purchase_order_id)
                return purchase_order_id + 1
            except:
                return 1
        except:
            return counter

    def get_tracking_at_status(self, status_slug):
        if not self.trackings.exists():
            return ''
        tracking_by_types = {}
        for tracking in self.trackings.all():
            tracking_by_types[tracking.tracking_type] = tracking
        return tracking_by_types.get(status_slug)

    def get_ordered(self):
        return self.get_tracking_at_status('ordered')

    def get_created(self):
        return self.get_tracking_at_status('request')

    def get_approved(self):
        return self.get_tracking_at_status('approved')

    @property
    def requested_by(self):
        created = self.get_created()
        return f"{created.role}-{created.profile}" if created else ''

    @property
    def approved_by(self):
        approved = self.get_approved()
        return f"{approved.role}-{approved.profile}" if approved else ''

    @property
    def ordered_by(self):
        ordered = self.get_ordered()
        return f"{ordered.role}-{ordered.profile}" if ordered else ''


class PurchaseOrderItem(models.Model):
    purchase_order = models.ForeignKey(PurchaseOrder, related_name='purchaseorder_items', on_delete=models.CASCADE)
    inventory = models.ForeignKey('inventory.BranchInventory', null=True, blank=True, related_name='purchaseorders', on_delete=models.CASCADE)
    account = models.ForeignKey('company.Account', null=True, blank=True, related_name='purchaseorders', on_delete=models.CASCADE)
    description = models.CharField(max_length=255)
    quantity = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    unit = models.ForeignKey('company.Unit', null=True, blank=True, on_delete=models.CASCADE)
    unit_price = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    vat_percentage = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=5)
    vat_code = models.ForeignKey('company.VatCode', null=True, blank=True, on_delete=models.DO_NOTHING)
    discount_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    vat_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    net_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    total_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    status = EnumIntegerField(enums.PurchaseOrderItemStatus, default=enums.PurchaseOrderItemStatus.OPEN)

    class Meta:
        ordering = ('id', )

    def __str__(self):
        return self.description

    # @property
    # def net_amount(self):
    #     return self.unit_price * self.quantity

    @property
    def price_with_vat(self):
        return self.unit_price_with_vat * self.quantity

    @property
    def unit_price_with_vat(self):
        price_with_vat = self.unit_price
        if self.vat_code:
            price_with_vat = ((self.vat_code.percentage / 100) * float(self.unit_price)) + float(self.unit_price)
        return Decimal(price_with_vat)

    @property
    def is_account(self):
        return self.account

    @property
    def is_inventory(self):
        return self.inventory

    @property
    def is_text(self):
        return self.description


class Tracking(models.Model):
    purchase_order = models.ForeignKey(PurchaseOrder, related_name='trackings', on_delete=models.CASCADE)
    tracking_type = models.CharField(max_length=255)
    comment = models.TextField()
    data = JSONField(blank=True)
    profile = models.ForeignKey('accounts.Profile', null=True, blank=True, on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', null=True, blank=True, on_delete=models.DO_NOTHING)
    created_at = models.DateField(auto_created=True)
