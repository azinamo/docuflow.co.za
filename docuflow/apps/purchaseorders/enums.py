from enumfields import IntEnum


class PurchaseOrderItemStatus(IntEnum):
    OPEN = 1
    CLOSED = 2

    class Labels:
        OPEN = 'Open'
        CLOSED = 'Closed'
