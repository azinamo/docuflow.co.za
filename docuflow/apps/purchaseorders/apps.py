from django.apps import AppConfig


class PurchaseordersConfig(AppConfig):
    name = 'docuflow.apps.purchaseorders'
