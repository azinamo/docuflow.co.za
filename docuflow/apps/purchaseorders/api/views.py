from rest_framework import viewsets

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.purchaseorders.models import PurchaseOrder
from .serializers import PurchaseOrderSerializer


class PurchaseOrderViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = PurchaseOrderSerializer

    def get_queryset(self):
        return PurchaseOrder.objects.select_related('received').filter(
            invoice__company=self.get_company(), invoice__period__period_year=self.get_year()
        )
