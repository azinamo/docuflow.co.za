from rest_framework import serializers
from django.urls import reverse_lazy

from docuflow.apps.purchaseorders.models import PurchaseOrder
from docuflow.apps.invoice.api.serializers import InvoiceSerializer
from docuflow.apps.inventory.api.serializers import InventoryReceivedSerializer


class PurchaseOrderSerializer(serializers.ModelSerializer):
    detail_url = serializers.SerializerMethodField()
    invoice = InvoiceSerializer()
    reference = serializers.CharField(source='__str__')
    inventory_received = InventoryReceivedSerializer()

    class Meta:
        model = PurchaseOrder
        fields = ('id', 'reference', 'purchase_order_id', 'invoice', 'detail_url', 'inventory_received')
        datatables_always_serialize = ('id', 'detail_url', 'invoice')

    def get_detail_url(self, instance):
        return reverse_lazy('invoice_detail', args=(instance.invoice_id, ))

    def get_status(self, instance):
        return str(instance.status)
