from decimal import Decimal
import logging

from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.mail import send_mail
from django.db import transaction
from django.urls import reverse
from django.utils.timezone import now
from django.template.loader import get_template

from docuflow.apps.invoice.models import Invoice, Comment, FlowStatus, Status, InvoiceType
from docuflow.apps.invoice.services import SignInvoice, InvoiceFlow
from docuflow.apps.accounts.models import Role, Profile
from docuflow.apps.supplier.models import SupplierAuthorization
from docuflow.apps.invoice.exceptions import InvoiceException
from docuflow.apps.purchaseorders.models import PurchaseOrder, Tracking

logger = logging.getLogger(__name__)


class CreateInvoicePurchaseOrder:

    def __init__(self, company, profile, role, form, post_request):
        logger.info("---- Start creating invoice purchase order--- ")
        self.company = company
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request

    def get_invoice_type(self):
        invoice_type = InvoiceType.objects.purchase_order(company=self.company).first()
        if not invoice_type:
            raise InvoiceException('Document type Purchase order does not exists.')
        return invoice_type

    def create_invoice(self):
        logger.info("create invoice")
        invoice_type = self.get_invoice_type()
        invoice = self.form.save(commit=False)
        invoice.invoice_type = invoice_type
        invoice.company = self.company
        invoice.created_by = self.profile
        invoice.save()
        invoice.invoice_number = str(invoice)
        invoice.save()
        return invoice

    def execute(self):
        with transaction.atomic():
            invoice = self.create_invoice()
            if invoice:
                self.create_purchase_order_items(invoice)
                self.create_flow_levels(invoice)

    def create_purchase_order_items(self, invoice):
        total_net = 0
        total_vat = 0
        for field in self.post_request:
            if field[0:9] == 'quantity_':
                if self.post_request[field] != '':
                    quantity = Decimal(self.post_request[field])
                    key = field[8:]
                    description_key = f"description{key}"
                    value_key = f"value{key}"
                    unit_key = f"unit{key}"
                    vat_amount_key = f"vat_amount{key}"
                    vat_code_key = f"vat_code{key}"
                    vat_code = None
                    vat_amount = 0
                    order_amount = 0

                    if vat_code_key in self.post_request:
                        vat_code = self.post_request[vat_code_key]
                    if vat_amount_key in self.post_request and self.post_request[vat_amount_key] != '':
                        vat_amount = Decimal(self.post_request[vat_amount_key])
                    if value_key in self.post_request and self.post_request[value_key] != '':
                        order_amount = Decimal(self.post_request[value_key])

                    total_net += order_amount * Decimal(self.post_request[field])
                    total_vat += vat_amount
                    if self.post_request[description_key] != '':
                        PurchaseOrder.objects.create(
                            invoice=invoice,
                            unit=self.post_request[unit_key],
                            quantity=quantity,
                            description=self.post_request[description_key],
                            order_value=order_amount,
                            vat_code_id=vat_code,
                            vat_amount=vat_amount,
                            requested_by=f"{self.role}-{self.profile}"
                        )
            if invoice.supplier and invoice.supplier.vat_number:
                invoice.vat_number = invoice.supplier.vat_number
            invoice.accounting_date = now()
            invoice.accounting_date = now()
            invoice.invoice_date = now()
            invoice.vat_amount = total_vat
            invoice.net_amount = total_net
            invoice.total_amount = (total_vat + total_net)
            invoice.save()

    def create_flow_levels(self, invoice):
        # Create 3 flow levels in linked to this document
        po_request = PurchaseOrderRequest(invoice, self.profile, self.role)
        po_request.process_request()


class InvoicePurchaseOrder:

    def __init__(self, invoice):
        self.invoice = invoice
        self.total = 0
        self.total_order_value = 0
        self.total_vat = 0
        self.purchase_orders = []
        self.po_comments = self.get_invoice_po_comments()

    def get_invoice_po_comments(self):
        comments = Comment.objects.filter(status__slug__in=['po-ordered', 'po-request', 'po-approved'],
                                          log_type=Comment.EVENT,
                                          invoice=self.invoice
                                          ).order_by('-id')
        return comments

    def get_invoice_at_status(self, status_slug):
        if not self.po_comments:
            return ''
        comment = self.po_comments.filter(
            status__slug=status_slug
        ).first()
        if not comment:
            return ''
        return "{}-{}".format(comment.role, comment.participant.profile.code)

    def get_ordered_by(self):
        return self.get_invoice_at_status('po-ordered')

    def get_created_by(self):
        return self.get_invoice_at_status('po-request')

    def get_approved_by(self):
        return self.get_invoice_at_status('po-approved')


class PurchaseOrderReport(InvoicePurchaseOrder):

    def __init__(self, invoice):
        super().__init__(invoice)

    def process_purchase_orders(self):
        print(f"Purchase orders for {self.invoice.id}")
        purchase_orders = PurchaseOrder.objects.select_related(
            'invoice'
        ).filter(invoice_id=self.invoice.id)
        for purchase_order in purchase_orders:
            order_value = (purchase_order.order_value * purchase_order.quantity)
            total = order_value + purchase_order.vat_amount
            self.total += total
            self.total_order_value += order_value
            self.total_vat += purchase_order.vat_amount
            self.purchase_orders.append({
                'description': purchase_order.description,
                'vat_code': purchase_order.vat_code,
                'vat_amount': round(purchase_order.vat_amount, 2),
                'unit': purchase_order.unit,
                'quantity': purchase_order.quantity,
                'quantity_in_unit': "{} {}".format(purchase_order.quantity, purchase_order.unit),
                'unit_value': purchase_order.order_value,
                'order_value': round(order_value, 2),
                'total': round(total, 2),
                'id': purchase_order.id
            })


class PurchaseOrderRequest(object):

    def __init__(self, invoice, profile, role):
        self.invoice = invoice
        self.profile = profile
        self.role = role

    def process_request(self):
        has_auto_approval = self.has_auto_approval()
        logger.info(f"This supplier {self.invoice.supplier}, role {self.role}, has auto approvals {has_auto_approval}")
        status = self.get_po_status(has_auto_approval)
        if status:
            self.invoice.status = status
        self.invoice.save()

        self.create_flows(has_auto_approval)

        self.invoice.log_activity(self.profile.user, self.role, 'Needs the purchase order Approved', Comment.EVENT)

    # noinspection PyMethodMayBeStatic
    def get_po_status(self, is_auto_approved):
        if is_auto_approved:
            return Status.objects.get(slug='po-approved')
        else:
            return Status.objects.get(slug='po-request')

    def has_auto_approval(self):
        total_period_invoices_supplier = Invoice.objects.total_amount_by_supplier(
            supplier=self.invoice.supplier, period=self.invoice.period
        )
        supplier_auto_approvals = SupplierAuthorization.objects.prefetch_related(
            'role_authorizations'
        ).filter(
            supplier=self.invoice.supplier, amount__gt=total_period_invoices_supplier['total_value']
        ).first()

        if supplier_auto_approvals:
            logger.info(f"Supplier amount {supplier_auto_approvals.amount} vs "
                        f"period amount {total_period_invoices_supplier['total_value']}")
        if supplier_auto_approvals and supplier_auto_approvals.amount > total_period_invoices_supplier['total_value']:
            logger.info(supplier_auto_approvals.amount)
            logger.info(supplier_auto_approvals.role_authorizations)
            role_authorization = supplier_auto_approvals.role_authorizations.filter(
                role=self.role, amount__gt=self.invoice.total_amount
            ).first()
            logger.info("Supplier role authorization {}".format(role_authorization))
            if role_authorization:
                return True
        return False

    def create_flows(self, has_auto_approval):
        # Create 3 flow levels in linked to this document
        counter = self.invoice.flows.count()
        if self.role:
            counter += 1
            flow_status = FlowStatus.objects.get(slug='processed')
            InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, True, self.profile)

            if not self.role.supervisor_role:
                logger.info("No supervisor role ")
                flow_status = FlowStatus.objects.get(slug='processing')
                administrator_role = Role.objects.get(
                    slug='administrator',
                    company=self.invoice.company
                )
                if administrator_role:
                    counter += 1
                    InvoiceFlow.create_flow_role(self.invoice, administrator_role, counter, flow_status, False, None, True)
                    if has_auto_approval:
                        flow_status = FlowStatus.objects.filter(slug='in-flow').first()
                    else:
                        flow_status = FlowStatus.objects.filter(slug='processing').first()
                    counter += 1
                    InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, False)
                else:
                    is_signed = False
                    if has_auto_approval:
                        flow_status = FlowStatus.objects.filter(slug='processed').first()
                        is_signed = True
                    else:
                        flow_status = FlowStatus.objects.get(slug='processing')

                    counter += 1
                    InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, is_signed, None, True)
            else:
                logger.info("Has supervisor role {} ".format(self.role.supervisor_role))
                is_signed = False
                is_current = True
                next_role_flow_status = 'in-flow'
                if has_auto_approval:
                    logger.info("Auto approving the supervisor role --> {}".format(self.role.supervisor_role))
                    flow_status = FlowStatus.objects.get(slug='processed')
                    self.invoice.log_activity(self.profile.user, self.role, 'Approved by system', Comment.EVENT)
                    is_signed = True
                    next_role_flow_status = 'processing'
                    is_current = False
                else:
                    flow_status = FlowStatus.objects.get(slug='processing')
                counter += 1

                InvoiceFlow.create_flow_role(self.invoice, self.role.supervisor_role, counter, flow_status, is_signed,
                                             None, is_current)

                is_current = False
                if has_auto_approval:
                    logger.info("Supervisor has auto approval, next role status is -> (processing) ")
                    flow_status = FlowStatus.objects.get(slug='processing')
                    is_current = True
                else:
                    logger.info("Supervisor has no approval, next role status is -> (in-flow) ")
                    flow_status = FlowStatus.objects.get(slug='in-flow')
                counter += 1
                InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, False, None, is_current)


def approve(purchase_order: PurchaseOrder, profile: Profile, role: Role):
    logger.info("-----------APPROVE PURCHASE ORDER---------------")
    invoice = purchase_order.invoice

    current_flow = invoice.get_current_flow()
    if not current_flow:
        raise ValidationError('Purchase order current flow not found')

    logger.info(f"Current flow level is {current_flow}, Signed by {profile} as {role}")

    sign = SignInvoice(invoice, profile, role, False)
    sign.process_signing()

    invoice.mark_purchase_order_as_approved()

    text = f'This has been approved for ordering, use the order number {str(invoice)}'
    invoice.log_activity(profile.user, role, text)

    Tracking.objects.create(
        purchase_order=purchase_order,
        tracking_type='approved',
        comment='Purchase Order approved',
        profile=profile,
        role=role,
        created_at=now().date(),
        data={
            'link_url': reverse('invoice_detail', args=(purchase_order.invoice_id,))
        }
    )

    return invoice.get_role_next_invoice(role=role)


def order(purchase_order: PurchaseOrder, profile: Profile, role: Role):
    logger.info("-----------ORDER PURCHASE ORDER---------------")
    invoice = purchase_order.invoice

    current_flow = invoice.get_current_flow()
    if not current_flow:
        raise ValidationError('Purchase order current flow not found')

    logger.info(f"Current flow level is {current_flow}, Signed by {profile} as {role}")
    sign = SignInvoice(invoice, profile, role, False)
    sign.process_signing()

    invoice.log_activity(profile.user, role.id, 'Goods have been ordered')

    Tracking.objects.create(
        purchase_order=purchase_order,
        tracking_type='ordered',
        comment='Purchase Order ordered',
        profile=profile,
        role=role,
        created_at=now().date(),
        data={
            'link_url': reverse('invoice_detail', args=(purchase_order.invoice_id,))
        }
    )

    return invoice.get_role_next_invoice(role=role)


def decline(purchase_order: PurchaseOrder, profile: Profile, role: Role, comment=''):
    logger.info("-----------DECLINE PURCHASE ORDER---------------")
    invoice = purchase_order.invoice

    invoice.mark_purchase_order_as_declined()

    sign_invoice = SignInvoice(invoice, profile, role)
    sign_invoice.process_signing()

    if comment:
        invoice.log_activity(profile.user, role, comment)

    invoice.log_activity(profile.user, role,
                         'This has been declined for ordering, see comments if any why it was declined, otherwise '
                         'contact your supervisor')

    next_invoice_id = invoice.get_role_next_invoice(role=role)

    return next_invoice_id


def resubmit(purchase_order: PurchaseOrder, profile: Profile, role: Role):
    invoice = purchase_order.invoice

    sign_invoice = SignInvoice(invoice, profile, role)
    sign_invoice.process_signing()

    po_request = PurchaseOrderRequest(invoice, profile, role)
    po_request.process_request()

    next_invoice_id = invoice.get_role_next_invoice(role=role)


def send_email(purchase_order: PurchaseOrder, profile: Profile, role: Role):
    invoice = purchase_order.invoice
    if not invoice.supplier.email_address:
        raise ValidationError('Supplier email address not found')

    subject = f"Purchase Order from your Customer: {invoice.company.name} "
    from_addr = settings.PO_FROM_EMAIL
    recipient_list = (settings.ADMIN_EMAIL, invoice.supplier.email_address)
    template = get_template('purchaseorders/email.html')
    message = template.render({
        'purchase_order': purchase_order
    })
    invoice_id = file_invoice(invoice, profile, role)

    send_mail(subject, message, from_addr, recipient_list, html_message=message)

    note = 'Email sent to supplier'
    invoice.log_activity(profile.user, role, note)

    return invoice_id


def file_invoice(invoice, profile, role):
    sign = SignInvoice(invoice, profile, role, False)
    sign.process_signing()

    next_invoice_id = invoice.get_role_next_invoice(role)

    return next_invoice_id
