import logging

from django import forms
from django.urls import reverse_lazy, reverse
from django.utils.timezone import now

from docuflow.apps.company.models import VatCode, Branch
from docuflow.apps.purchaseorders.models import PurchaseOrder, PurchaseOrderItem, Tracking
from docuflow.apps.invoice.models import Invoice, InvoiceType
from docuflow.apps.supplier.models import Supplier
from docuflow.apps.period.models import Period
from docuflow.apps.common.widgets import DataAttributesSelect
from .services import PurchaseOrderRequest

logger = logging.getLogger(__name__)


def supplier_select_data(company):
    suppliers_data = {'data-detail-url': {'': ''}}
    suppliers = Supplier.objects.filter(company=company)
    for supplier in suppliers:
        suppliers_data['data-detail-url'][supplier.id] = reverse_lazy('suppliers-detail', args=(supplier.id, ))
    return DataAttributesSelect(choices=[('', '-----------')] + [(s.id, str(s.name)) for s in suppliers], data=suppliers_data)


def branch_select_data(company):
    branches_data = {'data-detail-url': {'': ''}}
    branches = Branch.objects.filter(company=company)
    for branch in branches:
        branches_data['data-detail-url'][branch.id] = reverse_lazy('branches-detail', args=(branch.id, ))
    return DataAttributesSelect(choices=[('', '-----------')] + [(s.id, str(s.label)) for s in branches], data=branches_data)


class PurchaseOrderForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(PurchaseOrderForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].widget = supplier_select_data(self.branch.company)
        self.fields['branch'].widget = branch_select_data(self.branch.company)
        self.fields['branch'].empty_label = None

    memo_to_supplier = forms.CharField(required=False, label='Memo to supplier',
                                       widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))

    class Meta:
        fields = ('supplier', 'memo_to_supplier', 'branch', 'total_amount', 'vat_amount', 'net_amount', 'open_amount')
        model = Invoice

        widgets = {
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'open_amount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(PurchaseOrderForm, self).clean()
        if not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')
        if not cleaned_data['branch']:
            self.add_error('branch', 'Branch is required')
        invoice_date = now().date()
        if not self.get_period_for_today(date=invoice_date):
            raise forms.ValidationError(f'Period not found for the date {invoice_date}')

        if not InvoiceType.objects.purchase_order(self.branch.company).exists():
            raise forms.ValidationError(f'Purchase order invoice type not found.')
        return cleaned_data

    def get_period_for_today(self, date):
        return Period.objects.get_by_date(company=self.branch.company, period_date=date).first()

    def save(self, commit=True):
        invoice = super().save(commit=False)
        invoice_date = now().date()
        invoice.company = self.branch.company
        invoice.invoice_type = InvoiceType.objects.purchase_order(self.branch.company).first()
        invoice.period = self.get_period_for_today(date=invoice_date)
        if invoice.supplier.vat_number:
            invoice.vat_number = invoice.supplier.vat_number
        invoice.supplier_name = str(self.cleaned_data['supplier'])
        invoice.supplier_number = self.cleaned_data['supplier'].code
        invoice.accounting_date = invoice_date
        invoice.invoice_date = invoice_date
        invoice.due_date = invoice.calculate_due_date()
        invoice.save()

        purchase_order = PurchaseOrder.objects.create(
            invoice=invoice,
            memo_to_supplier=self.cleaned_data['memo_to_supplier']
        )
        if purchase_order:
            invoice.invoice_number = str(purchase_order)
            invoice.save()

        self.create_po_items(purchase_order)

        # Create 3 flow levels in linked to this document
        po_request = PurchaseOrderRequest(invoice, self.profile, self.role)
        po_request.process_request()

        Tracking.objects.create(
            purchase_order=purchase_order,
            tracking_type='request',
            comment='Purchase Order requested',
            profile=self.profile,
            role=self.role,
            created_at=now().date(),
            data={
                'link_url': reverse('invoice_detail', args=(purchase_order.invoice_id,))
            }
        )

    def create_po_items(self, purchase_order):
        logger.info(" -------------- create_po_items ------ ")
        for field, value in self.request.POST.items():
            if field.startswith('inventory', 0, 9):
                if value:
                    row_id = field[10:]
                    logger.info(f"Inventory Item at row {row_id}")
                    i_data = {
                        'quantity': self.request.POST.get(f'quantity_{row_id}'),
                        'inventory': value,
                        'unit_price': self.request.POST.get(f'price_{row_id}'),
                        'unit': self.request.POST.get(f'unit_id_{row_id}'),
                        'vat_amount': self.request.POST.get(f'vat_amount_{row_id}'),
                        'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
                        'discount_amount': self.request.POST.get(f'total_discount_{row_id}'),
                        'net_amount': self.request.POST.get(f'total_price_excluding_{row_id}'),
                        'total_amount': self.request.POST.get(f'total_price_including_{row_id}'),
                        'purchase_order': purchase_order
                    }
                    inventory_item_form = InventoryPurchaseOrderItemForm(data=i_data, branch=self.branch)
                    if inventory_item_form.is_valid():
                        inventory_item_form.save()
                    else:
                        logger.info(inventory_item_form.errors)
            elif field.startswith('account', 0, 7):
                logger.info("Account Item")
                if value:
                    row_id = field[8:]
                    data = {
                        'quantity': self.request.POST.get(f'quantity_{row_id}'),
                        'account': value,
                        'unit': self.request.POST.get(f'unit_{row_id}'),
                        'description': self.request.POST.get(f'description_{row_id}'),
                        'unit_price': self.request.POST.get(f'price_{row_id}'),
                        'vat_amount': self.request.POST.get(f'vat_amount_{row_id}'),
                        'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
                        'discount_amount': self.request.POST.get(f'total_discount_{row_id}'),
                        'net_amount': self.request.POST.get(f'total_price_excluding_{row_id}'),
                        'total_amount': self.request.POST.get(f'total_price_including_{row_id}'),
                        'purchase_order': purchase_order
                    }
                    logger.info(data)

                    account_item_form = AccountPurchaseOrderItemForm(data=data, branch=self.branch)
                    if account_item_form.is_valid():
                        account_item_form.save()
                    else:
                        logger.info(account_item_form.errors)
            elif field.startswith('item_description', 0, 16):
                if value:
                    row_id = field[17:]
                    logger.info("Text Item")
                    data = {
                        'description': self.request.POST.get(f'item_description_{row_id}'),
                        'purchase_order': purchase_order
                    }
                    logger.info(data)

                    text_item_form = TextPurchaseOrderItemForm(data=data)
                    if text_item_form.is_valid():
                        text_item_form.save()
                    else:
                        logger.info(text_item_form.errors)


class DeclineForm(forms.Form):
    comment = forms.CharField(required=True, label='Do you want to add a reason why you are declining it',
                              widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))


class InventoryPurchaseOrderItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        branch = kwargs.pop('branch')
        super(InventoryPurchaseOrderItemForm, self).__init__(*args, **kwargs)
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=branch.company, is_active=True)
        self.fields['inventory'].queryset = self.fields['inventory'].queryset.filter(branch=branch)

    class Meta:
        fields = ('inventory', 'quantity', 'unit', 'unit_price', 'vat_code', 'vat_amount', 'total_amount',
                  'purchase_order', 'net_amount', 'discount_amount')
        model = PurchaseOrderItem

        widgets = {
            'vat_code': forms.Select(attrs={'class': 'chosen-select', 'data-calculate-line-amount': reverse_lazy('calculate_purchase_order_amount')})
        }


class AccountPurchaseOrderItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        branch = kwargs.pop('branch')
        super(AccountPurchaseOrderItemForm, self).__init__(*args, **kwargs)
        self.fields['vat_code'].queryset = self.fields['vat_code'].queryset.filter(company=branch.company, is_active=True)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=branch.company)

    class Meta:
        fields = ('account', 'description', 'quantity', 'unit', 'unit_price', 'vat_code', 'vat_amount', 'total_amount',
                  'purchase_order', 'net_amount', 'discount_amount')
        model = PurchaseOrderItem

        widgets = {
            'vat_code': forms.Select(attrs={'class': 'chosen-select', 'data-calculate-line-amount': reverse_lazy('calculate_purchase_order_amount')})
        }


class TextPurchaseOrderItemForm(forms.ModelForm):

    class Meta:
        fields = ('description', 'purchase_order')
        model = PurchaseOrderItem
