import logging
import os
import pprint
from decimal import Decimal

from django import forms
from django.conf import settings
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction

from docuflow.apps.accountposting.models import AccountPosting
from docuflow.apps.accounts.models import Role, Profile
from docuflow.apps.common.utils import create_checksum
from docuflow.apps.common.widgets import DataAttributesSelect
from docuflow.apps.company.models import Account, VatCode, PaymentMethod, FileType
from docuflow.apps.company.utils import create_dir, get_company_dir
from docuflow.apps.period.models import Period
from docuflow.apps.supplier.models import Supplier, Ledger as SupplierLedger
from docuflow.apps.workflow.models import Workflow
from .utils import move_invoice_document, invoice_upload_path
from .enums import InvoiceStatus
from .exceptions import AccountPostingNotSetException, DuplicateInvoiceNumber
from .models import Invoice, InvoiceType, InvoiceAccount, Status, Payment, Comment, InvoiceReference, FlowRole
from .services import FlowIntervention, AddToFlow, SignInvoice, add_supervisor_role
from .validators import FileValidator

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


def get_invoice_vat_code_widget(vat_codes):
    vat_codes_data = {'data-percentage': {'': ''}, 'data-is-default': {'': ''}}
    for vat_code in vat_codes:
        vat_codes_data['data-percentage'][vat_code.id] = vat_code.percentage
        vat_codes_data['data-is-default'][vat_code.id] = '1' if vat_code.is_default else '0'
    return DataAttributesSelect(choices=[('', '-----------')] + [(vc.id, str(vc)) for vc in vat_codes], data=vat_codes_data)


class InvoiceTypeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.module_type = kwargs.pop('module_type')
        super(InvoiceTypeForm, self).__init__(*args, **kwargs)
        self.fields['file'].queryset = FileType.objects.filter(company=self.company)
        # self.fields['is_account_posting'].widget = forms.HiddenInput()
        # self.fields['is_account_posting'].label = ''
        # self.fields['is_credit'].widget = forms.HiddenInput()
        # self.fields['is_credit'].label = ''

    class Meta:
        fields = ('name', 'file', 'is_account_posting', 'is_credit', 'number_start', 'format')
        model = InvoiceType

    def save(self, commit=True):
        invoice_type = super(InvoiceTypeForm, self).save(commit=False)
        invoice_type.company = self.company
        invoice_type.save()
        return invoice_type


class BaseInvoiceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        super(BaseInvoiceForm, self).__init__(*args, **kwargs)
        vat_codes = VatCode.objects.input().filter(company=self.company).all()
        vat_codes_widget = get_invoice_vat_code_widget(vat_codes)
        self.fields['vat_code'].widget = vat_codes_widget
        if self.instance.pk and not self.instance.is_editable:
            self.fields['period'].queryset = Period.objects.filter(pk=self.instance.period_id)
            self.fields['invoice_type'].queryset = InvoiceType.objects.filter(pk=self.instance.invoice_type_id)
            self.fields['supplier'].queryset = self.fields['supplier'].queryset.filter(pk=self.instance.supplier_id).order_by('code', 'name')
            self.fields['invoice_type'].empty_label = None
            self.fields['supplier'].empty_label = None
        else:
            self.fields['period'].queryset = Period.objects.open(company=self.company, year=self.year).order_by('-period')
            self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
            self.fields['invoice_type'].queryset = InvoiceType.objects.purchase(company=self.company)
            self.fields['supplier'].queryset = self.fields['supplier'].queryset.filter(
                company=self.company, is_active=True, is_blocked=False
            ).order_by('code', 'name')
        self.fields['period'].empty_label = None
        self.fields['period'].empty_label = None

    reference1 = forms.CharField(required=False, label='Reference 1')
    reference2 = forms.CharField(required=False, label='Reference 2')
    attachment = forms.FileField(required=False, label='Attachment')
    supplier_account_ref = forms.CharField(required=False, label='Supplier Acc Ref')
    sales_invoice = forms.IntegerField(required=False, widget=forms.HiddenInput())

    class Meta:
        fields = ['supplier', 'is_valid', 'supplier_name', 'supplier_number', 'telephone', 'invoice_number',
                  'company_registration_number', 'vat_number', 'accounting_date', 'invoice_date', 'vat_amount',
                  'vat_code', 'net_amount', 'total_amount', 'invoice_type', 'due_date', 'period', 'supplier',
                  'reference1', 'reference2', 'attachment', 'supplier_account_ref', 'sales_invoice']
        model = Invoice
        widgets = {
            'accounting_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'invoice_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'due_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'supplier': forms.Select(attrs={'class': 'chosen-select'}),
            'invoice_type': forms.Select(attrs={'class': 'chosen-select'}),
        }


class CreateInvoiceForm(BaseInvoiceForm):

    def __init__(self, *args, **kwargs):
        super(CreateInvoiceForm, self).__init__(*args, **kwargs)
        vat_codes = VatCode.objects.input().filter(company=self.company).all()
        default_vat_codes = [vat_code for vat_code in vat_codes if vat_code.is_default]
        default_vat_code = default_vat_codes[0] if default_vat_codes else None
        vat_codes_widget = get_invoice_vat_code_widget(vat_codes)

        self.fields['account_posting_proposal'].queryset = self.fields['account_posting_proposal'].queryset.filter(company=self.company)
        self.fields['vat_code'].widget = vat_codes_widget
        self.fields['vat_code'].initial = default_vat_code
        self.fields['workflow'].queryset = self.fields['workflow'].queryset.filter(company=self.company)
        self.fields['invoice_type'].queryset = self.get_invoice_type_queryset()

    add_to_flow = forms.BooleanField(required=False)
    image = forms.FileField(required=False, label='Add Image')

    class Meta(BaseInvoiceForm.Meta):
        model = Invoice
        fields = BaseInvoiceForm.Meta.fields + ['account_posting_proposal', 'workflow', 'comments', 'is_protected']

    def get_invoice_type_queryset(self):
        qs = InvoiceType.objects.purchase(company=self.company)
        if self.year.is_final:
            return qs.exclude(is_account_posting=True)
        return qs

    def clean(self):
        cleaned_data = super(CreateInvoiceForm, self).clean()
        accounting_date = cleaned_data.get('accounting_date', None)
        invoice_number = cleaned_data.get('invoice_number', None)
        supplier = cleaned_data['supplier']
        period = cleaned_data.get('period')
        total_amount = cleaned_data['total_amount']
        net_amount = cleaned_data.get('net_amount', 0)
        vat_amount = cleaned_data.get('vat_amount', 0)

        if not accounting_date:
            self.add_error('accounting_date', 'Accounting date is required')

        if 'due_date' in cleaned_data:
            if not cleaned_data['due_date']:
                self.add_error('due_date', 'Due date is now required')

        if 'invoice_date' in cleaned_data:
            if not cleaned_data['invoice_date']:
                self.add_error('invoice_date', 'Document date is required')

        if not invoice_number:
            self.add_error('invoice_number', 'Document number is required')

        if Invoice.objects.filter(invoice_number=invoice_number, supplier=supplier).exclude(invoice_number__isnull=True).exclude(invoice_number='').exists():
            self.add_error('invoice_number', 'Document number already exists')

        if total_amount:
            invoice_type = self.cleaned_data['invoice_type']
            if invoice_type:
                if total_amount == 0 and invoice_type.is_account_posting:
                    self.add_error('total_amount', 'Please enter valid amount')

        if accounting_date and period:
            if period and not period.is_valid(accounting_date):
                self.add_error('period', 'Accounting date and period do not match.')
                self.add_error('accounting_date', 'Accounting date and period do not match.')

        if net_amount and vat_amount:
            net_amount = net_amount if net_amount else 0
            vat_amount = vat_amount if vat_amount else 0
            total = net_amount + vat_amount
            if total != total_amount:
                self.add_error('total_amount', f'Vat {vat_amount} and  Net {net_amount} entered do not match the total '
                                               f'amount {total_amount}')

        return cleaned_data

    def get_default_status(self):
        return Status.objects.get(slug='arrived-at-logger')

    @staticmethod
    def create_dir(parent_dir, directory_name):
        directory = os.path.join(parent_dir, directory_name)
        if not os.path.exists(directory):
            os.mkdir(directory)
        return directory

    # noinspection PyMethodMayBeStatic
    def add_invoice_to_flow(self, invoice, profile,  role):
        if not invoice.is_account_posting_set():
            raise AccountPostingNotSetException('Default account posting proposal or company default '
                                                'expenditure is not set, please fix.')

        add_to_flow = AddToFlow(invoice, profile, role)
        add_to_flow.process_flow()

        invoice.log_activity(profile.user, role.id, "Added document to flow", Comment.EVENT)

    def save(self, commit=True):
        with transaction.atomic():
            invoice = super().save(commit=False)
            invoice.created_by = self.profile
            invoice.company = self.company
            invoice.status = self.get_default_status()
            if not invoice.company.is_vat_registered:
                invoice.net_amount = invoice.total_amount
            invoice.sub_total = invoice.total_amount
            invoice.open_amount = invoice.calculate_open_amount()
            invoice.save()

            if self.cleaned_data['comments']:
                log = Comment(invoice=invoice, log_type=Comment.COMMENT, participant=self.profile.user, role=self.role,
                              note=self.cleaned_data['comments'])
                log.save()

            invoice_image = self.request.POST.get('invoice_image', None)
            if invoice_image:
                source = f"{settings.BASE_DIR}{invoice_image}"
                if os.path.exists(source):
                    filename = os.path.basename(invoice_image)
                    company_dir = self.create_dir(settings.MEDIA_ROOT, invoice.company.slug)
                    invoice_dir = self.create_dir(company_dir, str(invoice.id))
                    destination = f"{invoice_dir}/{filename}"
                    os.rename(source, destination)
                    invoice.filename = filename
                    invoice.save()

            supplier = self.cleaned_data['supplier']
            if supplier:
                if invoice.vat_number:
                    supplier.vat_number = invoice.vat_number
                    supplier.save()

                if self.cleaned_data['supplier_account_ref']:
                    supplier.company_number = self.cleaned_data['supplier_account_ref']
                    supplier.save()

            invoice.invoice_references.all().delete()
            if self.cleaned_data['reference2']:
                InvoiceReference.objects.create(invoice=invoice, reference=self.cleaned_data['reference2'])
            if self.cleaned_data['reference1']:
                InvoiceReference.objects.create(invoice=invoice, reference=self.cleaned_data['reference1'])

            flow_intervention = FlowIntervention('save', invoice)
            flow_intervention.process_intervention()

            logger.info(f"Added to flow --> {self.cleaned_data['add_to_flow']} --> ")
            add_to_flow = self.cleaned_data['add_to_flow'] or self.request.POST.get('add_to_flow', None)
            if add_to_flow:
                if invoice.is_credit_type:
                    invoice.set_credit_note_amount()
                self.add_invoice_to_flow(invoice=invoice, profile=self.profile, role=self.role)

            FlowRole.objects.role_stats(role_id=self.role.id, year=invoice.period.period_year)

            return invoice


class InvoiceForm(BaseInvoiceForm):

    def __init__(self, *args, **kwargs):
        self.vat_change = 0
        self.net_change = 0
        self.vat_code_change = None
        super(InvoiceForm, self).__init__(*args, **kwargs)

    class Meta(BaseInvoiceForm.Meta):
        fields = BaseInvoiceForm.Meta.fields
        model = Invoice
        widgets = {
            'accounting_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'invoice_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'due_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'currency': forms.Select(attrs={'class': 'chosen-select'}),
            'supplier': forms.Select(attrs={'class': 'chosen-select'}),
            'invoice_type': forms.Select(attrs={'class': 'chosen-select'}),
        }

    def clean(self):
        cleaned_data = super(InvoiceForm, self).clean()
        accounting_date = cleaned_data.get('accounting_date')
        invoice_date = cleaned_data.get('invoice_date')
        invoice_number = cleaned_data.get('invoice_number')
        period = cleaned_data.get('period')

        if not accounting_date:
            self.add_error('accounting_date', 'Accounting date is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Document date is required')

        if not invoice_number:
            self.add_error('invoice_number', 'Document number is required')

        if not period:
            self.add_error('period', 'Period is required')

        total_amount = cleaned_data.get('total_amount', None)
        net_amount = cleaned_data.get('net_amount', None)
        vat_amount = cleaned_data.get('vat_amount', None)
        invoice_type = cleaned_data.get('invoice_type', None)

        if total_amount and invoice_type:
            if total_amount == 0 and invoice_type.is_account_posting:
                self.add_error('total_amount', 'Please enter valid amount')

        if net_amount and vat_amount:
            net_amount = net_amount if net_amount else 0
            vat_amount = vat_amount if vat_amount else 0
            vat_net = net_amount + vat_amount
            if net_amount > 0 and vat_amount > 0:
                if vat_net != total_amount:
                    self.add_error('vat_amount', f'Vat {vat_amount} and  Net {net_amount} entered do not match the '
                                                 f'total amount {total_amount}')

        if period and not period.is_valid(accounting_date):
            self.add_error('accounting_date', 'Payment date and period do not match.')
            self.add_error('period', 'Period and payment date do not match.')

        due_date = cleaned_data['due_date']
        if self.instance and not due_date:
            due_date = self.instance.calculate_due_date()
        if not due_date:
            self.add_error('due_date', 'Due date is required')

        self.net_change = self.calculate_net_amount_change(net_amount=self.cleaned_data['net_amount'])
        self.vat_change = self.calculate_vat_amount_change(vat_amount=self.cleaned_data['vat_amount'])

        vat_code = self.cleaned_data['vat_code']
        if vat_code != self.instance.vat_code:
            self.vat_code_change = vat_code

        return cleaned_data

    def calculate_net_amount_change(self, net_amount):
        if (net_amount and self.instance.net_amount) and net_amount != self.instance.net_amount:
            return net_amount - self.instance.net_amount
        return 0

    def calculate_vat_amount_change(self, vat_amount):
        if (vat_amount and self.instance.vat_amount) and vat_amount != self.instance.vat_amount:
            return vat_amount - self.instance.vat_amount
        return 0

    def save(self, commit=True):
        invoice = super().save(commit)

        # TODO, Use django_extensions Field Tracker, to check for changes

        if not self.cleaned_data['due_date']:
            invoice.due_date = invoice.calculate_due_date()
        invoice.sub_total = self.cleaned_data['total_amount']
        invoice.save()

        invoice.open_amount = invoice.calculate_open_amount()
        invoice.save()

        # If updated, also check the supplier ledger
        SupplierLedger.objects.create_from_invoice(invoice=invoice, profile=self.profile)

        if invoice.is_credit_type:
            invoice.set_credit_note_amount()

        invoice.invoice_references.all().delete()
        reference2 = self.cleaned_data.get('reference2', None)
        reference1 = self.cleaned_data.get('reference1', None)
        if reference2:
            InvoiceReference.objects.create(invoice=invoice, reference=reference2)
        if reference1:
            InvoiceReference.objects.create(invoice=invoice, reference=reference1)

        update_type = self.request.POST.get('update_type', None)
        if update_type and update_type == 'adjustment':
            invoice.make_invoice_adjustments(
                net_change=self.net_change,
                vat_change=self.vat_change,
                user=self.profile.user,
                role=self.role
            )

        flow_intervention = FlowIntervention(event_type='save', invoice=invoice)
        flow_intervention.process_intervention()

        return invoice


class EmailSupplierForm(forms.Form):
    to = forms.CharField(required=True)
    subject = forms.CharField(required=False)
    message = forms.CharField(required=True, widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))


class AccountPostingForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(AccountPostingForm, self).__init__(*args, **kwargs)
        self.fields['vat_code'].queryset = VatCode.objects.input().filter(company=company)

    comment = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 5, 'rows': 3}))
    vat_line = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    reinvoice = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    amount = forms.CharField(required=True)
    account = forms.CharField(required=True)
    vat_code = forms.ModelChoiceField(required=False, label='Vat Code', queryset=None,
                                      widget=forms.Select(attrs={'class': 'chosen-select'}))

    class Meta:
        model = InvoiceAccount


# TODO - Remove
class DistributeInvoiceAccountForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(DistributeInvoiceAccountForm, self).__init__(*args, **kwargs)
        self.fields['percentage'] = forms.DecimalField(
            required=False,
            label='Percentage of the amount.'
        )
        self.fields['vat_code'].queryset = VatCode.objects.input().filter(company=company)

    comment = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 5, 'rows': 3}))
    vat_line = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    reinvoice = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    amount = forms.CharField(required=True)
    account = forms.CharField(required=True)
    vat_code = forms.ModelChoiceField(required=False, label='Vat Code', queryset=None,
                                      widget=forms.Select(attrs={'class': 'chosen-select'}))

    class Meta:
        model = InvoiceAccount


class InvoiceApproveForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(InvoiceApproveForm, self).__init__(*args, **kwargs)

    comment = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 5, 'rows': 3}))
    vat_line = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    reinvoice = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    amount = forms.CharField(required=True)
    calculation_method = forms.ChoiceField(required=False, choices=(('amount', 'Flat Amount'), ('percentage',
                                                                                                'Percentage')))
    account = forms.CharField(required=True)

    class Meta:
        model = InvoiceAccount


class InvoiceAccountPostingForm(forms.ModelForm):
    comment = forms.CharField(required=True, label='Text', widget=forms.Textarea(attrs={'cols': 5, 'rows': 3}))
    amount = forms.CharField(required=False, label='Amount')
    account = forms.CharField(required=True)

    class Meta:
        model = InvoiceAccount
        fields = ('amount', 'comment')


class InvoiceAccountAmountForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        counter = 0
        if 'counter' in kwargs:
            counter = kwargs.pop('counter')
        super(InvoiceAccountAmountForm, self).__init__(*args, **kwargs)
        self.fields['account_%s' % counter] = forms.ModelChoiceField(required=False, label='Assign to account',
                                                                     queryset=Account.objects.filter(company=company))
        self.fields['percentage_%s' % counter] = forms.DecimalField(required=False, label='Percentage of the amount')

    counter = forms.HiddenInput()


class InvoiceApprovalForm(forms.Form):
    approval_comment = forms.CharField(required=True, label='Comment',
                                       widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))


class InvoiceDeclineApprovalForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        super().__init__(*args, **kwargs)
        self.fields['open_amount'].required = False
        self.fields['open_amount'].label = ''
        self.fields['open_amount'].widget = forms.HiddenInput()

    reason = forms.CharField(required=True, widget=forms.Textarea())

    class Meta:
        model = Invoice
        fields = ('open_amount', 'reason')

    def save(self, commit=True):
        invoice = super().save(commit=False)
        invoice.open_amount = 0
        invoice.save()

        invoice_flow = invoice.get_current_flow()

        Comment.objects.create(
            note=self.cleaned_data['reason'] or "The invoice was approved to be declined",
            participant=self.profile.user,
            status=invoice.status,
            role=self.role,
            invoice=invoice,
            log_type=Comment.EVENT
        )

        sign = SignInvoice(invoice=invoice, profile=self.profile, role=self.role, intervene=False)
        sign.process_signing()

        self.delete_roles(invoice=invoice, invoice_flow=invoice_flow)

        invoice = invoice.approve_decline()

        if invoice.is_declined:
            try:
                supplier_ledger = SupplierLedger.objects.get(
                    object_id=invoice.id,
                    content_type=ContentType.objects.get_for_model(invoice)
                )
                supplier_ledger.delete()
            except SupplierLedger.DoesNotExist:
                logger.info("Supplier ledger does not exists")

            self.unlink_goods_received(invoice=invoice)
        return invoice

    # noinspection PyMethodMayBeStatic
    def delete_roles(self, invoice, invoice_flow):
        flows = invoice.flows.all().order_by('order_number')
        for flow in flows:
            if flow.order_number > invoice_flow.order_number:
                flow.delete()

    # noinspection PyMethodMayBeStatic
    def unlink_goods_received(self, invoice):
        InvoiceAccount.objects.filter(invoice=invoice).update(
            goods_received=None, goods_returned=None
        )


class InvoiceCommentForm(forms.Form):
    attachment = forms.FileField(required=False, label='Attachment')
    comment = forms.CharField(required=False, widget=forms.Textarea())


class AddImageForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.invoice = None
        if 'invoice' in kwargs:
            self.invoice = kwargs.pop('invoice')
        super(AddImageForm, self).__init__(*args, **kwargs)

    image = forms.FileField(required=True, label='Add File',
                            validators=[FileValidator(allowed_extensions=('jpg', 'jpeg', 'pdf', 'gif', 'png'))])

    def execute(self):
        file = self.cleaned_data['image']
        if self.invoice:
            _dir = create_dir(os.path.join(get_company_dir(self.company), str(self.invoice.id)))
            destination_url = f'{settings.MEDIA_URL}{self.company.slug}/{str(self.invoice.id)}/{file}'
        else:
            _dir = create_dir(os.path.join(get_company_dir(self.company), 'tmp'))
            destination_url = f'{settings.MEDIA_URL}{self.company.slug}/tmp/{file}'

        destination = f'{_dir}/{file}'
        with open(destination, 'wb+') as f:
            for chunk in file.chunks():
                f.write(chunk)
        if self.invoice:
            self.update_invoice_file(file=file)
        return destination_url

    def update_invoice_file(self, file):
        invoice = self.invoice
        invoice.filename = file.name
        invoice.checksum = create_checksum(
            company_id=self.company.id,
            data=file.read(),
            filename=file.name
        )
        invoice.save()


class InvoiceParkForm(forms.Form):
    park_comment = forms.CharField(required=True, label='Please fill in a detail reason why you want to put '
                                                        'this invoice on hold',
                                   widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))


class InvoiceDeclineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.invoice = kwargs.pop('invoice')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super().__init__(*args, **kwargs)
        self.fields['note'].label = 'Please fill in a detail reason why you want to decline this invoice on hold'

    class Meta:
        model = Comment
        fields = ('note', )
        widgets = {
            'note': forms.Textarea(attrs={'cols': 5, 'rows': 10})
        }

    def save(self, commit=True):
        comment = super().save(commit=False)
        comment.invoice = self.invoice
        comment.log_type = Comment.COMMENT
        comment.participant = self.profile.user
        comment.status = self.invoice.status
        comment.role = self.role
        comment.save()

        invoice_flow = self.invoice.get_current_flow()

        if invoice_flow:

            sign = SignInvoice(self.invoice, self.profile, self.role)
            sign.process_signing()

            self.invoice.status = Status.objects.get(slug=InvoiceStatus.DECLINE_REQUEST.value)
            self.invoice.save()

            # add supervisor role to the level and with a status of processing
            add_supervisor_role(invoice=self.invoice, invoice_flow=invoice_flow, role=self.role)

        next_invoice_id = self.invoice.get_role_next_invoice(role=self.role)
        return next_invoice_id


class InvoiceAccountCommentForm(forms.ModelForm):
    class Meta:
        model = InvoiceAccount
        fields = ('comment', )

        widgets = {
            'comment': forms.Textarea(attrs={'cols': 5, 'rows': 10}),
        }


class AccountingDateForm(forms.Form):
    accounting_date = forms.DateField(required=True)


class InvoiceAccountForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(InvoiceAccountForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = Account.objects.filter(company=company)
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=company)

    vat_code = forms.ModelChoiceField(required=False, label='Vat Code', queryset=None)

    class Meta:
        fields = ('comment', 'percentage')
        model = InvoiceAccount


# TODO - Extend BaseInvoiceForm class
class InvoiceLogForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        super(InvoiceLogForm, self).__init__(*args, **kwargs)
        vat_codes = VatCode.objects.input().filter(company=self.company).all()
        vat_codes_widget = get_invoice_vat_code_widget(vat_codes)
        default_vat_codes = [vat_code for vat_code in vat_codes if vat_code.is_default]
        default_vat_code = default_vat_codes[0] if default_vat_codes else None

        self.file_url, self.file_src = invoice_upload_path(invoice=self.instance, filename=self.instance.filename)

        self.fields['supplier'].queryset = Supplier.objects.filter(company=self.company).order_by('code', 'name')
        self.fields['account_posting_proposal'].queryset = AccountPosting.objects.filter(company=self.company)
        self.fields['vat_code'].widget = vat_codes_widget
        self.fields['vat_code'].initial = default_vat_code
        self.fields['workflow'].queryset = Workflow.objects.filter(company=self.company)
        self.fields['workflow'].empty_label = None
        self.fields['invoice_type'].queryset = InvoiceType.objects.purchase(self.company)
        self.fields['period'].queryset = Period.objects.open(company=self.company, year=self.year).order_by('-period')
        self.fields['period'].empty_label = None

    reference1 = forms.CharField(required=False, label='Reference 1')
    reference2 = forms.CharField(required=False, label='Reference 2')
    supplier_account_ref = forms.CharField(required=False, label='Supplier Acc Ref')

    class Meta:
        fields = ('supplier_name', 'supplier_number', 'vat_number', 'due_date', 'accounting_date', 'invoice_date',
                  'vat_amount', 'net_amount', 'sub_total', 'total_amount', 'invoice_type', 'comments', 'period',
                  'vat_code', 'currency', 'supplier', 'account_posting_proposal', 'workflow', 'invoice_number',
                  'is_protected')
        model = Invoice
        widgets = {
            'accounting_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'invoice_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'due_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'rematching_until': forms.DateInput(attrs={'class': 'datepicker'}),
            'vat_amount': forms.TextInput(attrs={'class': 'form-control'}),
            'total_amount': forms.TextInput(attrs={'class': 'form-control'}),
            'is_protected': forms.CheckboxInput()
        }

    def clean(self):
        cleaned_data = super(InvoiceLogForm, self).clean()

        accounting_date = cleaned_data.get('accounting_date', None)
        if not accounting_date:
            self.add_error('accounting_date', 'Accounting date is required')

        due_date = cleaned_data.get('due_date', None)
        if not due_date:
            self.add_error('due_date', 'Due date is now required')

        invoice_date = cleaned_data.get('invoice_date', None)
        if not invoice_date:
            self.add_error('invoice_date', 'Document date is required')

        invoice_number = cleaned_data.get('invoice_number', None)
        if not invoice_number:
            self.add_error('invoice_number', 'Document number is required')

        invoice_type = cleaned_data.get('invoice_type', None)
        if invoice_type and invoice_type.is_account_posting:
            total_amount = cleaned_data.get('cleaned_data', None)
            if total_amount:
                total_amount = Decimal(total_amount)
                if int(total_amount) == 0:
                    self.add_error('total_amount', 'Please enter valid amount')

        period = cleaned_data.get('period', None)
        if period:
            if not period.is_valid(accounting_date):
                self.add_error('accounting_date', 'Period and accounting date do not match.')
                self.add_error('period', 'Period and accounting date do not match.')
        else:
            self.add_error('period', 'Period is required.')
        return cleaned_data

    def save(self, commit=True):

        invoice = super(InvoiceLogForm, self).save(commit=False)
        invoice.open_amount = invoice.calculate_open_amount()
        invoice.save()

        # TODO - Move this to clean method
        if invoice.is_duplicate_invoice_number:
            raise DuplicateInvoiceNumber('Document number already exists')

        if not invoice.due_date:
            invoice.due_date = invoice.calculate_due_date()
            invoice.save()

        invoice.invoice_references.all().delete()
        reference2 = self.cleaned_data.get('reference2', None)
        reference1 = self.cleaned_data.get('reference1', None)
        if reference2:
            InvoiceReference.objects.create(invoice=invoice, reference=reference2)
        if reference1:
            InvoiceReference.objects.create(invoice=invoice, reference=reference1)

        self.update_supplier_details(invoice)

        flow_intervention = FlowIntervention(event_type='save', invoice=invoice)
        flow_intervention.process_intervention()

        add_to_flow = self.request.POST.get('add_to_flow', None)

        if add_to_flow:
            if invoice.is_credit_type:
                invoice.set_credit_note_amount()
            self.add_invoice_to_flow(invoice=invoice)

        # move_invoice_document(invoice=invoice, src=self.file_src)

        return invoice

    def update_supplier_details(self, invoice):
        supplier = invoice.supplier
        if invoice.vat_number:
            supplier.vat_number = invoice.vat_number
        supplier_account_ref = self.cleaned_data.get('supplier_account_ref', None)
        if supplier_account_ref:
            supplier.company_number = supplier_account_ref
        supplier.save()

    def add_invoice_to_flow(self, invoice):
        if not invoice.is_account_posting_set():
            raise AccountPostingNotSetException('Default account posting proposal or company default '
                                                'expenditure is not set, please fix.')

        add_to_flow = AddToFlow(invoice=invoice, profile=self.profile, role=self.role)
        add_to_flow.process_flow()

        if invoice.comments:
            invoice.log_activity(user=self.request.user, role=self.role, comment=invoice.comments, log_type=Comment.EVENT)
        invoice.log_activity(user=self.request.user, role=self.role, comment="Added document to flow", log_type=Comment.EVENT)


class CapturedInvoiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(CapturedInvoiceForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['supplier'].queryset = Supplier.objects.filter(company=company, is_active=True)
            self.fields['company'].initial = company.name
            self.fields['invoice_type'] = InvoiceType.objects.filter(company=company)
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    supplier = forms.ModelChoiceField(required=False, label='Supplier', queryset=None )
    reference1 = forms.CharField(required=False, label='Reference 1')
    reference2 = forms.CharField(required=False, label='Reference 2')
    invoice_series = forms.ChoiceField(required=False, label='Invoice Series', choices=(('*', '*'),))
    serial_number = forms.ChoiceField(required=False, label='Serial Number', choices=( ('*', '*'),))
    number_of_lines = forms.CharField(required=False, label='Number of lines',)
    attachment_file_type = forms.ChoiceField(required=False, label='Attachment',
                                             choices=(('', '*'), ('pdf', 'Pdf'), ('jpg', 'Jpg')))
    alias = forms.CharField(required=False, label='Alias')

    class Meta:
        fields = ('supplier_name', 'supplier_number', 'vat_number', 'due_date', 'accounting_date', 'invoice_date',
                  'vat_amount', 'net_amount', 'sub_total', 'total_amount', 'invoice_type', 'comments')
        model = Invoice
        widgets = {
            'accounting_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'invoice_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'due_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'rematching_until': forms.DateInput(attrs={'class': 'datepicker'}),

        }


class InvoiceJournalReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company  = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(InvoiceJournalReportForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['role'].queryset = Role.objects.filter(company=company)
            self.fields['user'].queryset = Profile.objects.filter(company=company)
            self.fields['company'].initial = company.name
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    role = forms.ModelChoiceField(required=False, label='Role', queryset=None)
    user = forms.ModelChoiceField(required=False, label='User', queryset=None)
    report_number = forms.CharField(required=False, label='Report Number')
    debit_total = forms.CharField(required=False, label='Debit Total')
    vat_total = forms.CharField(required=False, label='Vat Total')
    creidit_total = forms.CharField(required=False, label='Credit Total')


class InvoiceReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company  = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(InvoiceReportForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['supplier'].queryset = Supplier.objects.filter(company=company, is_active=True,
                                                                       is_blocked=False)
            self.fields['company'].initial = company.name
            self.fields['invoice_type'] = InvoiceType.objects.filter(company=company)
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    supplier = forms.ModelChoiceField(required=False, label='Supplier', queryset=None )
    reference1 = forms.CharField(required=False, label='Reference 1')
    reference2 = forms.CharField(required=False, label='Reference 2')
    invoice_status = forms.ModelChoiceField(required=False, label='Flow status', queryset=Status.objects.all())
    serial_number = forms.ChoiceField(required=False, label='Serial Number', choices=(('*', '*'),))
    number_of_lines = forms.CharField(required=False, label='Number of lines',)
    attachment_file_type = forms.ChoiceField(required=False, label='Attachment',
                                             choices=(('', '*'), ('pdf', 'Pdf'), ('jpg', 'Jpg'))
                                             )
    credit = forms.ChoiceField(required=False, widget=forms.CheckboxInput())
    alias = forms.CharField(required=False, label='Alias')
    voucher_series = forms.CharField(required=False, label='Voucher Series')
    voucher_no = forms.CharField(required=False, label='Voucher No')

    class Meta:
        fields = ('supplier_name', 'supplier_number', 'vat_number', 'due_date', 'accounting_date', 'invoice_date',
                  'vat_amount', 'net_amount', 'sub_total', 'total_amount', 'invoice_type', 'comments', 'status')
        model = Invoice
        widgets = {
            'accounting_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'invoice_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'due_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'rematching_until': forms.DateInput(attrs={'class': 'datepicker'}),
        }


class InvoiceBottleneckReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(InvoiceBottleneckReportForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['role'].queryset = Role.objects.filter(company=company)
            self.fields['company'].initial = company.name
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    role = forms.ModelChoiceField(required=False, label='Role', queryset=None)
    amount = forms.CharField(required=False, label='Amount',)
    number_of_lines = forms.CharField(required=False, label='Number of lines',)
    from_accounting_date = forms.CharField(required=False, label='Accounting date')
    to_accounting_date = forms.CharField(required=False, label='Accounting date',)


class InvoiceProcessingDayReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(InvoiceProcessingDayReportForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['role'].queryset = Role.objects.filter(company=company)
            self.fields['company'].initial = company.name
            self.fields['supplier'].initial = Supplier.objects.filter(company=company, is_active=True, is_blocked=False)
            self.fields['account'].initial = Account.objects.filter(company=company)
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    role = forms.ModelChoiceField(required=False, label='Role', queryset=None)
    supplier = forms.ModelChoiceField(required=False, label='Supplier', queryset=Supplier.objects.all())
    account = forms.ModelChoiceField(required=False, label='Account', queryset=Account.objects.all())
    amount = forms.CharField(required=False, label='Amount')
    number_of_lines = forms.CharField(required=False, label='Number of lines')
    from_accounting_date = forms.CharField(required=False,label='Accounting date')
    to_accounting_date = forms.CharField(required=False, label='Accounting date')


class InvoicePaymentForecastReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(InvoicePaymentForecastReportForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['role'].queryset = Role.objects.filter(company=company)
            self.fields['company'].initial = company.name
            self.fields['supplier'].initial = Supplier.objects.filter(company=company, is_active=True, is_blocked=False)
            self.fields['account'].initial = Account.objects.filter(company=company)
        self.fields['company'].initial = company.name

    company = forms.CharField(required=False)
    role = forms.ModelChoiceField(required=False, label='Role', queryset=Role.objects.all())
    supplier = forms.ModelChoiceField(required=False, label='Supplier', queryset=Supplier.objects.all())
    account = forms.ModelChoiceField(required=False, label='Account', queryset=Account.objects.all())
    currency = forms.CharField(required=False, label='Currency')
    number_of_lines = forms.CharField(required=False, label='Number of lines')
    from_due_date = forms.CharField(required=False, label='Due date',)
    to_due_date = forms.CharField(required=False, label='Due date',)
    from_accounting_date = forms.CharField(required=False, label='Accounting date',)
    to_accounting_date = forms.CharField(required=False, label='Accounting date',)
    time_period = forms.CharField(required=False, label='Time Period')


class InvoicePaymentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(InvoicePaymentForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['payment_method'].queryset = PaymentMethod.objects.filter(company=company)
            self.fields['period'].queryset = Period.objects.filter(company=company, is_closed=False,
                                                                   period_year__is_active=True)

    period = forms.ModelChoiceField(required=True, label='Period', queryset=None)
    total_amount = forms.DecimalField(required=False, label='Total of above invoices', widget=forms.TextInput())
    total_discount = forms.DecimalField(required=False, label='Discount total as selected', widget=forms.TextInput())
    discount_disallowed = forms.DecimalField(required=False, label='Discount previously disallowed',
                                             widget=forms.TextInput())
    net_amount = forms.DecimalField(required=False, label='Net payment to be made', widget=forms.TextInput())
    payment_method = forms.ModelChoiceField(required=True, label='Payment Method',
                                            queryset=PaymentMethod.objects.filter())

    class Meta:
        model = Payment
        fields = ('period', 'payment_date', 'total_amount', 'total_discount', 'discount_disallowed',
                  'net_amount', 'payment_method', 'memo', 'reference')
        widgets = {
            'payment_date': forms.TextInput(attrs={'class': 'datepicker'}),
            'memo': forms.Textarea(attrs={'cols': 5, 'rows': 10}),
        }


class InvoiceAccountDistributeForm(forms.Form):

    month = forms.CharField(required=True, label='How many months', widget=forms.TextInput())
    start_date = forms.DateField(required=True, label='Starting Date',
                                 widget=forms.TextInput(attrs={'class': 'datepicker', 'readonly': 'readonly'}))


class PaymentProofOfPaymentForm(forms.Form):
    attachment = forms.FileField(required=False, label='Proof of payment')


class InvoiceUpdateForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(InvoiceUpdateForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['period'].queryset = Period.objects.filter(company=company, is_closed=False,
                                                                   period_year__is_active=True)
    period = forms.ModelChoiceField(required=True, label='Period', queryset=None)
