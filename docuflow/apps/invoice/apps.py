from django.apps import AppConfig


class InvoiceConfig(AppConfig):
    name = 'docuflow.apps.invoice'