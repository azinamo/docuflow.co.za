# import os
# import io
# import subprocess
# import csv
# import pkg_resources
# import logging
# import re
# import pyocr
# import pyocr.builders
# 
# import invoice2data.in_pdftotext as pdftotext
# from invoice2data.template import InvoiceTemplate
# from .template import read_templates
# from .utils import ordered_load
# 
# 
# from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
# from pdfminer.pdfpage import PDFPage
# from pdfminer.converter import PDFPageAggregator
# from pdfminer.layout import LAParams, LTTextBox, LTTextLine, LTTextBoxHorizontal
# 
# from wand.image import Image
# from PIL import Image as PI
# 
# from django.conf import settings
# 
# 
# logger = logging.getLogger(__name__)
# 
# 
# class InvoiceOcr(object):
#     def __init__(self, invoice):
#         self.invoice = invoice
# 
#     def get_supplier_template(self, invoice):
#         if invoice and invoice.supplier:
#             print("Get template for supplier {}".format(self.invoice.supplier))
#             if invoice.supplier.template:
#                 template_name = os.path.join(settings.TEMPLATES_DIR, self.invoice.supplier.template.filename)
#                 if os.path.exists(template_name):
#                     return template_name
#         return None
# 
#     def get_template(self):
#         template_name = self.get_supplier_template(self.invoice)
#         return template_name
# 
#     def get_invoice_file(self):logger
#         pdf = os.path.join(settings.MEDIA_ROOT, self.invoice.file_location)
#         if not os.path.exists(pdf):
#             return False
#         return pdf
# 
#     @staticmethod
#     def load_template(template_name, template_contents):
#         """
#         Load a template from a string template. Return list of dicts.
#         """
#         output = []
#         tpl = ordered_load(template_contents)
#         print(tpl)
#         tpl['template_name'] = template_name
# 
#         # Test if all required fields are in template:
#         # assert 'keywords' in tpl.keys(), 'Missing keywords field.'
#         # required_fields = ['date', 'amount', 'invoice_number']
#         # assert len(set(required_fields).intersection(tpl['fields'].keys())) == len(required_fields), \
#         #     'Missing required key in template {} . Found {}'.format(template_name, tpl['fields'].keys())
# 
#         # Keywords as list, if only one.
#         # if type(tpl['keywords']) is not list:
#         #     tpl['keywords'] = [tpl['keywords']]
# 
#         # if 'lines' in tpl:
#         #     assert 'start' in tpl['lines'], 'Lines start regex missing'
#         #     assert 'end' in tpl['lines'], 'Lines end regex missing'
#         #     assert 'line' in tpl['lines'], 'Line regex missing'
# 
#         output.append(InvoiceTemplate(tpl))
#         return output
# 
#     def execute(self):
# 
#         invoice_file = self.get_invoice_file()
#         print("Decrypted pdf filename %s " % (invoice_file,))
#         if invoice_file:
#             template_name = self.get_template()
# 
#             print("Template name is  %s " % (template_name,))
# 
#             #####################################################
#             # suppose you, according to user selections, builds
#             # a text content like the example
#             # Here I simulate taking the content from a file
#             #####################################################
#             try:
#                 template_content = open(template_name).read()
# 
#                 #####################################################
#                 # load the template and check if it correct
#                 #####################################################
#                 templates = self.load_template(template_name, template_content)
# 
#                 # keys you may display on the front end
#                 available_features = templates[0]['fields'].keys()  # ------> the user select the "template file" and
#                 # then enable features using checkboxes
# 
#                 # enabled_features = ['amount', 'invoice_number', 'date'] # --> Default features
#                 # suppose the user is interested on to 'amount' and 'invoice_number'
#                 enabled_features = ['amount', 'invoice_number', 'date']  # ------> the user submit the form with
#                 # enabled (these are mandatory in this version)
# 
#                 #####################################################
#                 # execute feature filtering
#                 #####################################################
#                 intersection = set(available_features) ^ set(enabled_features)
# 
#                 print("Intersection of fields {}".format(intersection))
# 
#                 for key in intersection:
#                     dct = templates[0]['fields']  # OrderedDict
#                     del dct[key]
# 
#                     #####################################################
#                     # extract data
#                     #####################################################
#                 extract = ExtractText(self.invoice, invoice_file, templates=templates, template=template_name, debug=True)  # --->
#                 result = extract.execute()
#                 print("Result ")
#                 print(result)
#                 # I have decrypted with qpdf (qpdf --decrypt  encrypted.pdf decrypted.pdf)
#                 # maybe you can decrypt every pdf the use submit to erver
# 
#                 #####################################################
#                 # write extracted data
#                 #####################################################
#                 # if result:
#                 #     template.result = result
#                 #     template.save()
#                 return result
#             except Exception as e:
#                 print('Exception {} occurred getting the pdf contents in  {}'.format(e, os.getcwd()))
#                 pass
# 
# 
# class ExtractText(object):
# 
#     def __init__(self, invoice, filename, templates, template, debug=False):
#         self.filename = filename
#         self.template = template
#         self.templates = templates
#         self.invoice = invoice
#         self.debut = debug
# 
#     def next(self):
#         next = None
#         return next
# 
#     def write_extracted_data(result, output_file):
#         """
#         Write extracted data to a csv file.
#         """
#         logger.info(result)
#         if result:
#             data = result
#             path = './invoices/' + output_file
# 
#             print("Write extracted data")
#             with open(path, "w") as csv_file:
#                 writer = csv.writer(csv_file, delimiter=',')
# 
#                 writer.writerow(['date', 'desc', 'amount'])
#                 line = data
#                 writer.writerow([
#                     line['date'].strftime('%d/%m/%Y'),
#                     line['desc'],
#                     line['amount']])
# 
#     def execute(self):
#         """
#         extract data from text PDF file.
#         """
#         print('START pdftotext result ===========================')
#         extracted_str = self.convert_with_pdf_to_text()
#         charcount = len(extracted_str)
#         print(extracted_str)
#         logger.debug('number of char in pdf2text extract: {}'.format(charcount))
#         print('END pdftotext result ===========================')
#         # if the original library is not able to extract text
#         # try with previous code
# 
#         print('START pdf miner  ===========================')
#         if charcount < 10:
#             logger.debug(' Convert with pdf miner')
#             extracted_str = self.convert_with_pdf_miner()
# 
#         charcount = len(extracted_str)
#         print(extracted_str)
#         print('END pdf miner  ===========================')
# 
#         print('START Tesseract===========================')
#         # Disable Tesseract for now.
#         if charcount < 40:
#             logger.info('Starting OCR')
#             # extracted_str = image_to_text.to_text(invoicefile)
#             extracted_str = self.convert_with_tessaract()
# 
#         print(extracted_str)
#         print('END Tesseract===========================')
# 
#         extracted_str = extracted_str.encode('utf-8')
# 
#         print('START extracted_str ===========================')
#         print(extracted_str)
#         print('END extracted_str =============================')
# 
#         if extracted_str:
#             self.invoice.contents = extracted_str
#             print("Now template save contents {}".format(self.invoice.id))
#             self.invoice.save()
#             print("saved contents {}".format(self.invoice.contents))
# 
#         for t in self.templates:
#             optimized_str = t.prepare_input(str(extracted_str))
#             # self.content = optimized_str
#             # self.save()
#             print('START optimized_str ===========================')
#             print(extracted_str)
#             print('END optimized_str =============================')
#             if t.matches_input(optimized_str):
#                 return t.extract(optimized_str)
# 
#         logger.error('No template for %s', self.filename)
#         return False
# 
#     #####################################################
#     # Method used inside "Convert" to process pdf
#     # extracted data and group
#     #####################################################
#     def parse_lt_objs_and_group(self, lt_objs):
#         """Iterate through the list of LT* objects and capture the text data contained in each
#             Try also to group some text which belons to the same box
#         """
#         row_objs = dict()
#         row_obj_list = []
# 
#         for lt_obj in lt_objs:
#             if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
#                 # text
#                 y0 = lt_obj.y0
#                 y1 = lt_obj.y1
#                 key = str(y0) + "-" + str(y1)
#                 if key not in row_objs:
#                     row_objs[key] = list()
#                     row_objs[key].append(lt_obj.get_text().replace("\n"," "))
#                     row_obj_list.append(key)    # keep ordered
#                 else:
#                     row_objs[key].append(lt_obj.get_text().replace("\n"," "))
# 
#         output_list = []
#         # concatenate key-values
#         for obj_key in row_obj_list:
#             output_list.append(" ".join(row_objs[obj_key]) + "\n")
# 
#         return output_list
# 
#     #####################################################
#     # End Previous Pdf 2 TxT Module
#     #####################################################
# 
#     def strip_excess_whitespace(self, text):
#         collapsed_spaces = re.sub(r"([^\S\r\n]+)", " ", text)
#         no_leading_whitespace = re.sub(
#             "([\n\r]+)([^\S\n\r]+)", '\\1', collapsed_spaces)
#         no_trailing_whitespace = re.sub("([^\S\n\r]+)$", '', no_leading_whitespace)
#         return no_trailing_whitespace
# 
#     #####################################################
#     # converts pdf, returns its text content as a string
#     #####################################################
#     def convert_with_pdf_to_text(self):
# 
#         result = pdftotext.to_text(self.filename).decode('utf-8')
#         return result
# 
#     #####################################################
#     # converts pdf, returns its text content as a string
#     #####################################################
#     def convert_with_pdf_miner(self, pages=None):
#         if not pages:
#             page_nums = set()
#         else:
#             page_nums = set(pages)
# 
#         rsrcmgr = PDFResourceManager()
#         laparams = LAParams()
#         device = PDFPageAggregator(rsrcmgr, laparams=laparams)
#         interpreter = PDFPageInterpreter(rsrcmgr, device)
# 
#         infile = open(self.filename, 'rb')
#         for page in PDFPage.get_pages(infile, page_nums):
#             interpreter.process_page(page)
#             # receive the LTPage object for this page
#             layout = device.get_result()
#             rows = self.parse_lt_objs_and_group(layout)
# 
#         infile.close()
# 
#         return "".join(rows)
# 
#     def convert_with_tessaract(self, pages=None):
#         tool = pyocr.get_available_tools()[0]
#         lang = tool.get_available_languages()[3]
# 
#         req_image = []
#         final_text = []
# 
#         image_pdf = Image(
#             filename=self.filename,
#             resolution=300
#         )
#         image_jpeg = image_pdf.convert('jpeg')
# 
#         for img in image_jpeg.sequence:
#             img_page = Image(image=img)
#             req_image.append(img_page.make_blob('jpeg'))
# 
#         for img in req_image:
#             txt = tool.image_to_string(
#                 PI.open(io.BytesIO(img)),
#                 lang=lang,
#                 builder=pyocr.builders.TextBuilder()
#             )
#             final_text.append(self.strip_excess_whitespace(txt))
# 
#         return "".join(final_text)
# 
