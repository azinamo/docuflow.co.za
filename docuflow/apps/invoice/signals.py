from django.dispatch import receiver
from django.db.models.signals import post_save

from docuflow.apps.invoice.models import Invoice, FlowRole


@receiver(post_save, sender=Invoice, dispatch_uid='added_purchase_invoice')
def handle_saved_invoice(sender, instance, created, **kwargs):
    pass
