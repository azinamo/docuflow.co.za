from django.utils.translation import gettext as __
from enumfields import IntEnum, Enum


class InvoiceStatus(Enum):
    AT_LOGGER = 'arrived-at-logger'
    IN_FLOW = 'in-the-flow'
    IN_FLOW_PRINTED = 'printed-status-still-in-the-flow'
    FINAL_SIGNED = 'final-signed'
    ACCOUNT_POSTED_PRINTED = 'end-flow-printed'
    DECLINE_REQUEST = 'decline-request-from-user'
    DECLINE_APPROVED = 'decline-approved'
    DECLINE_PRINTED = 'decline-printed'
    REJECTED = 'rejected-invoice'
    REJECTION_AWAITING_APPROVAL = 'rejected-approval-waiting'
    PO_REQUEST = 'po-request'
    PO_APPROVED = 'po-approved'
    PO_DECLINED = 'po-declined'
    PO_LINKED = 'po-linked'
    PO_ORDERED = 'po-ordered'
    PO_CANCELLED = 'po-cancelled'
    PO_RECEIVED = 'po-received'
    PAID = 'paid'
    PAID_NOT_ACCOUNT_POSTED = 'paid-not-account-posted'
    PAID_NOT_PRINTED = 'paid-not-printed'
    PAID_PRINTED = 'paid-printed'
    ADJUSTMENT = 'adjustments'
    PAID_PRINTED_NOT_ACCOUNT_POSTED = 'pp-not-account-posted'
    PARTIALLY_PAID = 'partially-paid'

    class Labels:
        AT_LOGGER = __('At Logger')
        IN_FLOW = __('In the flow not printed')
        IN_FLOW_PRINTED = __('In the flow printed')
        FINAL_SIGNED = __('Final Signed')
        ACCOUNT_POSTED_PRINTED = __('Account posting printed')
        PAID = __('Paid')
        PAID_NOT_ACCOUNT_POSTED = __('Paid not account posted')
        PAID_NOT_PRINTED = __('Paid not printed')
        PAID_PRINTED = __('Paid printed')
        ADJUSTMENT = __('Adjustment')
        REJECTED = __('Rejected')
        REJECTION_AWAITING_APPROVAL = __('Rejected, awaiting approval')

    @classmethod
    def posting_statuses(cls):
        return [InvoiceStatus.FINAL_SIGNED.value, InvoiceStatus.PAID_NOT_ACCOUNT_POSTED.value,
                InvoiceStatus.PAID_PRINTED_NOT_ACCOUNT_POSTED.value]

    @classmethod
    def update_incoming_from_statuses(cls):
        return ['in-the-flow', 'adjustments', 'decline-approved']

    @classmethod
    def get_payment_from_status(cls):
        return ['paid', 'paid-printed-not-account-posted', 'paid-not-printed', 'paid-not-account-posted', 'partially-paid']

    @classmethod
    def get_posting_from_status(cls):
        return ['final-signed',  'paid-not-account-posted', 'paid-printed-not-posted', 'paid-printed-not-account-posted']

    @classmethod
    def purchase_order(cls):
        return 'purchase-order'

    @classmethod
    def default_invoice_type(cls):
        return 'tax-invoice'

    @classmethod
    def default_status(cls):
        return 'arrived-at-logger'

    @classmethod
    def supplier_payment_statuses(cls):
        return ['final-signed', 'end-flow-printed', 'partially-paid']

    @classmethod
    def completed_statuses(cls):
        return ['final-signed', 'end-flow-printed']

    @classmethod
    def processing_statuses(cls):
        return ['in-the-flow', 'adjustments', 'printed-status-still-in-the-flow']

    @classmethod
    def processed_statuses(cls):
        return ['final-signed', 'end-flow-printed', 'partially-paid', 'paid-not-account-posted', 'paid-printed-not-posted',
                'pp-not-account-posted', 'paid-printed-not-account-posted']

    @classmethod
    def pending_posting_statuses(cls):
        return [InvoiceStatus.FINAL_SIGNED.value, InvoiceStatus.PAID_NOT_ACCOUNT_POSTED.value,
                InvoiceStatus.PAID_PRINTED_NOT_ACCOUNT_POSTED.value, InvoiceStatus.IN_FLOW_PRINTED.value]


class InvoiceFlowStatus(IntEnum):
    IN_FLOW = 1
    PARKED = 2
    PROCESSED = 3
    PROCESSING = 4
    INBOUND = 5
    ACTION_REQUIRED = 6


class FlowRoleStatus(IntEnum):
    PROCESSING = 1
    PROCESSED = 2
    ACTION_REQUIRED = 3
    PARKED = 4

    class Labels:
        PROCESSING = __('Processing')
        PROCESSED = __('Processed')
        ACTION_REQUIRED = __('Action Required')
        PARKED = __('Parked')


class JournalType(IntEnum):
    INCOMING = 0
    CANCELLED = 1
    POSTING = 2
    PAYMENTS = 3
    DISTRIBUTION = 4

    class Labels:
        INCOMING = __('Journal for incoming invoices')
        CANCELLED = __('Journal for cancelled invoices')
        POSTING = __('Journal for posting invoices')
        PAYMENTS = __('Journal for payments')
        DISTRIBUTION = __('Journal for distributions')


class CommentType(IntEnum):
    TRANSITION = 1
    EVENT = 2
    ROLE = 3
    COMMENT = 4
    LINK = 5

    class Labels:
        TRANSITION = __('Transition')
        EVENT = __('Event')
        ROLE = __('Role')
        COMMENT = __('Comment')
        LINK = __('Invoice Link')


class PaymentType(IntEnum):
    PAYMENT = 0
    ALLOCATION = 1
    SUNDRY = 2

    class Labels:
        PAYMENT = __('Payment')
        ALLOCATION = __('Allocation')
        SUNDRY = __('Sundry Payment')


class InvoiceType(Enum):
    TAX_INVOICE = 'tax-invoice'
    CREDIT_NOTE = 'tax-credit-note'
    PURCHASE_ORDER = 'purchase-order'
    PROFORMA = 'proforma-tax-invoice'
    INVOICE = 'invoice'


class PurchaseOrderStatus(IntEnum):
    OPEN = 1
    CLOSED = 2

    class Labels:
        OPEN = __('Open')
        CLOSED = __('Closed')


class DocumentFormat(IntEnum):
    ENG = 1
    SWE = 2
