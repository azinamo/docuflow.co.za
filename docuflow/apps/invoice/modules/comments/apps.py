from django.apps import AppConfig


class CommentsConfig(AppConfig):
    name = 'docuflow.apps.invoice.modules.comments'
