from django.urls import path

from . import views

urlpatterns = [
    path('<int:invoice_id>/', views.InvoiceCommentsView.as_view(), name='invoice_comments_index'),
    path('<int:invoice_id>/create/', views.InvoiceAddCommentView.as_view(), name='add_invoice_comment'),
    path('attachment/<int:pk>/', views.InvoiceCommentAttachmentView.as_view(), name='comment_attachment'),
    path('<int:pk>/edit/', views.InvoiceCommentEditView.as_view(), name='edit_invoice_comment'),
    path('<int:pk>/delete/', views.InvoiceCommentDeleteView.as_view(), name='delete_invoice_comment'),
]


