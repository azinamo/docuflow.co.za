import json
import logging
import pprint

from django.urls import reverse
from django.conf import settings
from django.shortcuts import redirect
from django.views.generic import ListView, FormView, View, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.http import HttpResponse
from django.db.models import Q, CharField, Value
from django.db.models.functions import Concat

from docuflow.apps.invoice.models import Comment, Invoice
from docuflow.apps.invoice.forms import InvoiceCommentForm

from docuflow.apps.common.mixins import ProfileMixin

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class InvoiceCommentsView(LoginRequiredMixin, ListView):
    model = Comment
    template_name = 'comments/index.html'
    context_object_name = 'comments'

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceCommentsView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['upload_url'] = settings.MEDIA_URL
        can_edit = False
        can_edit_invoice = self.request.session.get('can_edit_invoice', [])
        if invoice.id in can_edit_invoice:
            can_edit = True
        context['can_edit'] = can_edit
        return context

    def get_queryset(self):
        invoice = self.get_invoice()
        comments = Comment.objects.annotate(
            done_by=Concat(
                'participant__first_name', Value(' - '), 'participant__last_name', Value(''), output_field=CharField()
            )
        ).select_related(
            'participant', 'participant__profile', 'role'
        ).filter(
            Q(invoice_flow__invoice=invoice) | Q(invoice=invoice)
        ).values(
            'note', 'created_at', 'attachment', 'link_url', 'id', 'done_by', 'invoice_id'
        )
        return comments


class InvoiceAddCommentView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "comments/create.html"

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_form_class(self):
        return InvoiceCommentForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceAddCommentView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(InvoiceAddCommentView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return HttpResponse(
                json.dumps({'error': True, 'text': 'Invoice could not be saved, please fix the errors.',
                            'errors': form.errors}))
        return super(InvoiceAddCommentView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                invoice = self.get_invoice()
                invoice_flow = invoice.get_current_flow()
                comment = ''
                log_type = Comment.COMMENT
                if form.cleaned_data['comment']:
                    comment = form.cleaned_data['comment']
                    log_type = Comment.COMMENT
                    if comment:
                        if form.cleaned_data['attachment']:
                            invoice.log_invoice_activity(invoice_flow, self.request.user, self.request.session['role'],
                                                         comment,
                                                         log_type, self.request.FILES['attachment'])
                        else:
                            invoice.log_invoice_activity(invoice_flow, self.request.user, self.request.session['role'],
                                                         comment,
                                                         log_type, attachment=None)
                    return HttpResponse(
                        json.dumps({'error': False,
                                    'text': 'Invoice comment successfully saved.',
                                    'reload': True
                                    })
                    )
                elif 'attachment' in form.cleaned_data and form.cleaned_data['attachment']:
                    comment = 'Attachment Added.'
                    log_type = Comment.EVENT
                    if comment:
                        invoice.log_invoice_activity(invoice_flow, self.request.user, self.request.session['role'],
                                                     comment, log_type, attachment=self.request.FILES['attachment'])

                    return HttpResponse(
                        json.dumps({'error': False,
                                    'text': 'Invoice attachment successfully saved.',
                                    'reload': True
                                    })
                    )
                else:
                    return HttpResponse(
                        json.dumps({'error': True, 'text': 'Please add a comment or attachment'}))
                # invoice.workflow_activity.log_event()
            except Exception as exception:
                    return HttpResponse(
                        json.dumps({'error': True, 'text': 'Error occurred {}'.format(exception) }))
        else:
            return redirect(reverse('invoice_detail', kwargs={'pk': self.kwargs['pk']}))


class InvoiceCommentEditView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "comments/edit.html"

    def get_comment(self):
        return Comment.objects.get(pk=self.kwargs['pk'])

    def get_initial(self):
        initial = super(InvoiceCommentEditView, self).get_initial()
        invoice_comment = self.get_comment()
        if invoice_comment:
            initial['comment'] = invoice_comment.note
        return initial

    def get_form_class(self):
        return InvoiceCommentForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceCommentEditView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(InvoiceCommentEditView, self).get_context_data(**kwargs)
        context['comment'] = self.get_comment()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return HttpResponse(
                json.dumps({'error': True,
                            'text': 'Invoice comment could not be saved, please fix the errors.',
                            'errors': form.errors
                            })
                )
        return super(InvoiceCommentEditView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            invoice_comment = self.get_comment()
            try:
                comment = ''
                if form.cleaned_data['comment']:
                    invoice_comment.note = form.cleaned_data['comment']
                    if form.cleaned_data['attachment']:
                        invoice_comment.attachment = self.request.FILES['attachment']
                    invoice_comment.save()
                    return HttpResponse(
                        json.dumps({'error': False,
                                    'text': 'Invoice comment successfully updated.',
                                    'reload': True
                                    })
                    )
                elif 'attachment' in form.cleaned_data and form.cleaned_data['attachment']:
                    invoice_comment.note = 'Attachment Added.'
                    invoice_comment.attachment = self.request.FILES['attachment']
                    invoice_comment.save()
                    return HttpResponse(
                        json.dumps({'error': False,
                                    'text': 'Invoice attachment successfully updated.'
                                    })
                    )
                else:
                    return HttpResponse(
                        json.dumps({'error': True,
                                    'text': 'Please add a comment or attachment',
                                    'reload': True
                                    }))
            except Exception as exception:
                return HttpResponse(
                    json.dumps({'error': True, 'text': 'Error occurred {}'.format(exception)}))
        else:
            return redirect(reverse('invoice_detail', kwargs={'pk': self.kwargs['pk']}))


class InvoiceCommentDeleteView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            comment = Comment.objects.get(pk=self.kwargs['pk'])
            if comment:
                if comment.invoice_id:
                    invoice_id = comment.invoice_id
                else:
                    invoice_id = comment.invoice_flow.invoice_id
                comment.delete()
                messages.success(self.request, 'Comment deleted successfully')
        except:
            messages.error(self.request, 'An unexpected error occured')
        if invoice_id:
            return redirect(reverse('invoice_detail', kwargs={'pk': invoice_id}))


class InvoiceCommentAttachmentView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = "comments/view_attachment.html"

    def get_comment(self):
        return Comment.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceCommentAttachmentView, self).get_context_data(**kwargs)
        comment = self.get_comment()
        context['comment'] = comment
        context['upload_url'] = settings.MEDIA_URL
        return context