from django.urls import path

from . import views

urlpatterns = [
    path('', views.InvoiceFlowView.as_view(), name='invoice_flows'),
    path('create/<int:after>/', views.CreateFlowView.as_view(), name='create_invoice_flow'),
    path('changes/', views.InvoiceFlowHistoryView.as_view(), name='invoice_flow_log'),
    path('add/<int:flow_id>/role/', views.AddInvoiceFlowRoleView.as_view(), name='add_invoice_flow_role'),
    path('delete/<int:pk>/', views.DeleteInvoiceFlowView.as_view(), name='delete_invoice_flow'),
    path('delete/<int:flow_role_id>/role/', views.DeleteInvoiceFlowRoleView.as_view(),
         name='delete_invoice_flow_role'),
]