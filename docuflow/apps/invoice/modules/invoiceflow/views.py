import logging
import pprint

from annoying.functions import get_object_or_None
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.generic import ListView, FormView, View, CreateView, TemplateView, DeleteView

from docuflow.apps.accounts.enums import DocuflowPermission
from docuflow.apps.accounts.models import Profile
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.invoice.models import Invoice, Comment, FlowRole, Flow
from .forms import AddFlowRoleForm, AddFlowForm

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class InvoiceFlowView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = "invoiceflow/index.html"

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceFlowView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        flows_list = []
        can_edit = self.request.GET.get('can_edit', False)
        permissions = []
        profile_id = self.request.session.get('profile', None)

        flows = Flow.objects.prefetch_related('roles', 'roles__role').filter(invoice=invoice).order_by('order_number')
        for flow in flows:
            invoice_flow = {
                'is_current': flow.is_current,
                'is_signed': flow.is_signed,
                'is_processed': (flow.status and flow.status.slug == 'processed'),
                'can_edit': can_edit,
                'order_number': flow.order_number,
                'id': flow.id,
                'roles': []
            }
            for flow_role in flow.roles.all():
                can_delete = False
                signed_by = None
                if not flow_role.is_processed:
                    if DocuflowPermission.CAN_MANAGE_INVOICE_FLOW.value in permissions:
                        can_delete = True
                    elif flow_role.created_by:
                        if flow_role.created_by_id == profile_id:
                            can_delete = True

                if flow_role.signed_by:
                    signed_by = flow_role.signed_by.code

                invoice_flow['roles'].append(
                    {'is_pending': flow_role.is_pending,
                     'is_processed': flow_role.is_processed,
                     'is_action_required': flow_role.is_action_required,
                     'is_parked': flow_role.is_parked,
                     'role': flow_role.role,
                     'can_delete': can_delete,
                     'signed_at': flow_role.signed_at,
                     'signed_by': signed_by,
                     'id': flow_role.id
                     })

            flows_list.append(invoice_flow)
        context['flows'] = flows_list
        context['invoice'] = invoice
        context['can_edit'] = can_edit
        return context


class CreateFlowView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = "invoiceflow/create.html"
    form_class = AddFlowForm
    model = Flow

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_form_kwargs(self):
        kwargs = super(CreateFlowView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        kwargs['invoice'] = self.get_invoice()
        kwargs['after'] = self.kwargs['after']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateFlowView, self).get_context_data(**kwargs)
        context['invoice'] = self.get_invoice()
        context['order_number'] = int(self.kwargs['after'])
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Flow level could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateFlowView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            form.save()
            return JsonResponse({
                'error': False,
                'text': 'Flow successfully added.',
                'reload': True
            })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class AddInvoiceFlowRoleView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoiceflow/add_role.html"
    form_class = AddFlowRoleForm

    def get_invoice_flow(self):
        return Flow.objects.get(pk=self.kwargs['flow_id'])

    def get_form_kwargs(self):
        kwargs = super(AddInvoiceFlowRoleView, self).get_form_kwargs()
        kwargs['profile'] = self.get_profile()
        kwargs['flow'] = self.get_invoice_flow()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddInvoiceFlowRoleView, self).get_context_data(**kwargs)
        invoice_flow = self.get_invoice_flow()
        invoice = invoice_flow.invoice
        context['flow'] = invoice_flow
        context['invoice'] = invoice
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Role could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(AddInvoiceFlowRoleView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            form.save()
            return JsonResponse({'error': False,
                                 'text': 'Role successfully added.',
                                 'reload': True
                                 })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class DeleteInvoiceFlowView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            try:
                flow = Flow.objects.get(pk=self.kwargs['pk'])
                invoice_id = flow.invoice_id
                flow.delete()
                return JsonResponse({
                    'error': False,
                    'text': 'Flow successfully added.',
                    'redirect': reverse('invoice_detail', kwargs={'pk': invoice_id})
                })
            except Flow.DoesNotExist:
                return JsonResponse({
                    'error': True,
                    'text': 'Flow not found.',
                })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class DeleteInvoiceFlowRoleView(LoginRequiredMixin, DeleteView):
    template_name = 'invoiceflow/confirm_delete.html'
    model = FlowRole
    context_object_name = 'flow_role'

    def get_object(self, queryset=None):
        return FlowRole.objects.select_related('flow__invoice').get(pk=self.kwargs['flow_role_id'])

    def delete(self, request, *args, **kwargs):
        flow_role = self.get_object()
        flow_role.delete_flow_role()

        return JsonResponse({
            'error': False,
            'text': 'Flow successfully added.',
            'redirect': reverse('invoice_detail', kwargs={'pk': self.kwargs['invoice_id']})
        })


class InvoiceFlowHistoryView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Comment
    template_name = 'invoiceflow/flow_changes.html'
    context_object_name = 'comments'

    def get_context_data(self, **kwargs):
        context = super(InvoiceFlowHistoryView, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return Comment.objects.filter(invoice_flow__invoice__company=self.get_company())
