from django.apps import AppConfig


class InvoiceflowConfig(AppConfig):
    name = 'docuflow.apps.invoice.modules.invoiceflow'
