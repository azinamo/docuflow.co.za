import logging
import pprint

from django import forms

from docuflow.apps.accounts.models import Role
from docuflow.apps.invoice.models import Flow, FlowStatus, FlowRole

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class AddFlowForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.invoice = kwargs.pop('invoice')
        self.after = kwargs.pop('after')
        super(AddFlowForm, self).__init__(*args, **kwargs)
        self.fields['roles'].queryset = Role.objects.filter(company=self.company)

    roles = forms.ModelMultipleChoiceField(required=True, label='Roles', queryset=None)

    class Meta:
        model = Flow
        fields = ('roles', )

    def save(self, commit=True):
        flow = Flow.new(
            invoice=self.invoice,
            roles=self.cleaned_data['roles'],
            after=self.after,
            profile=self.profile
        )
        return flow


class AddFlowRoleForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile')
        self.company = kwargs.pop('company')
        self.flow = kwargs.pop('flow')
        super(AddFlowRoleForm, self).__init__(*args, **kwargs)
        self.fields['roles'].queryset = Role.objects.filter(company=self.company).exclude(pk__in=self.flow.roles.values_list('role_id', flat=True))

    roles = forms.ModelMultipleChoiceField(required=True, label='Roles', queryset=None)

    class Meta:
        model = Flow
        fields = ('roles', )

    def save(self, commit=True):
        self.flow.add_roles(
            profile=self.profile,
            roles=self.cleaned_data['roles']
        )
        return self.flow
