from django import forms

from docuflow.apps.invoice.models import Journal
from docuflow.apps.period.models import Period


class UpdateInvoiceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        super(UpdateInvoiceForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    date = forms.DateField(required=True, label='Upto and including')
    journal_transaction = forms.ChoiceField(choices=(
        ('totals', 'Totals only?.'),
        ('separate', 'Separate journal for each transaction?.'),
    ), label='How do you want your journals', required=False, widget=forms.RadioSelect())

    class Meta:
        model = Journal
        fields = ('period', 'date', )
        widgets = {
            'period': forms.Select(attrs={'class': 'form-control chosen-select'}),
        }

    def clean(self):
        cleaned_data = super().clean()
        date = cleaned_data.get('date')
        period = cleaned_data.get('period')
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and update date do not match')
        return cleaned_data

    def save(self, commit=True):
        update = super().save(commit=False)
        update.created_by = self.profile
        update.company = self.company
        update.role = self.role
        update.journal_type = Journal.POSTING
        update.save()
        return update
