from django.apps import AppConfig


class PostingConfig(AppConfig):
    name = 'docuflow.apps.invoice.modules.posting'
