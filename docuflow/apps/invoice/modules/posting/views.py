from decimal import Decimal

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.views.generic import ListView, FormView, View, TemplateView
from django.urls import reverse
from django.utils.timezone import now

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.company.models import ObjectItem
from docuflow.apps.invoice.models import Journal
from docuflow.apps.journals.models import JournalLine as LedgerLine
from docuflow.apps.invoice.reports import JournalForPostingReport, PostingWithIncomingReport, PostingJournal
from docuflow.apps.invoice.exceptions import InvoiceException
from .forms import UpdateInvoiceForm
from .services import CreatePostingUpdate


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'posting/index.html'
    model = Journal
    context_object_name = 'updates'
    paginate_by = 30

    def get_queryset(self):
        return Journal.objects.posting().select_related(
            'role', 'period'
        ).filter(
            company=self.get_company(), period__period_year=self.get_year()
        )

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['company'] = company
        context['year'] = year
        return context


class CreateUpdateView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = UpdateInvoiceForm
    template_name = 'posting/create.html'

    def get_initial(self):
        initial = super(CreateUpdateView, self).get_initial()
        initial['date'] = now().date()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateUpdateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the invoice updates, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateUpdateView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    update = form.save()

                    create_update = CreatePostingUpdate(
                        company=self.get_company(),
                        profile=self.get_profile(),
                        role=self.get_role(),
                        update=update
                    )
                    create_update.execute()

                return JsonResponse({
                    'error': False,
                    'text': ' Posting of invoices successfully updated',
                    'redirect': reverse('journal_for_posting_invoice')
                })
            except InvoiceException as exception:
                return JsonResponse({
                    'error': True,
                    'text': str(exception)
                })

        return super(CreateUpdateView, self).form_valid(form)


class JournalForPostingRecalculateLedgerView(LoginRequiredMixin, View):

    # noinspection PyMethodMayBeStatic
    def get_accounts_summary(self, accounts):
        accounts_summary = {}
        for invoice_id, invoice_accounts in accounts.items():
            for invoice_account in invoice_accounts:

                if invoice_account.account.id in accounts_summary:
                    if invoice_account.is_reverse:
                        accounts_summary[invoice_account.account.id]['credit'] += Decimal(invoice_account.total_amount)
                    else:
                        accounts_summary[invoice_account.account.id]['debit'] += Decimal(invoice_account.total_amount)
                else:
                    accounts_summary[invoice_account.account.id] = {'debit': 0, 'credit': 0,
                                                                    'vat_code': invoice_account.vat_code}
                    if invoice_account.is_reverse:
                        accounts_summary[invoice_account.account.id]['credit'] = Decimal(invoice_account.total_amount)
                    else:
                        accounts_summary[invoice_account.account.id]['debit'] = Decimal(invoice_account.total_amount)
        return accounts_summary

    def recalculate_journal_ledger(self, journal):

        report = JournalForPostingReport(journal)
        report.process_report()

        accounts_summary = self.get_accounts_summary(report.invoices_accounts)

        ledger = journal.ledger
        if ledger:
            for ledger_line in ledger.lines.all():
                ledger_line.delete()

            if report.expenditure_account:
                LedgerLine.objects.create(
                    ledger=ledger,
                    account=report.expenditure_account,
                    credit=Decimal(report.invoice_account_total)
                )

            for account_id, account in accounts_summary.items():
                account_line = LedgerLine.objects.filter(account=account_id, ledger=ledger).first()
                if account_line:
                    account_line.credit = Decimal(account['credit'])
                    account_line.debit = Decimal(account['debit'])
                    account_line.save()
                else:
                    account_line = LedgerLine.objects.create(
                        journal=ledger,
                        account_id=account_id,
                        credit=Decimal(account['credit']),
                        debit=Decimal(account['debit']),
                        vat_code=account['vat_code']
                    )
                    account_line.save()
        return ledger

    def get(self, request, *args, **kwargs):
        with transaction.atomic():
            journal = Journal.objects.get(pk=self.kwargs['pk'])
            if journal:
                self.recalculate_journal_ledger(journal)

                return JsonResponse({
                    'error': False,
                    'text': 'Journal for posting updated successfully'
                })
            else:
                return JsonResponse({
                    'error': True,
                    'text': 'Journal not found',
                    'redirect': reverse('journal_for_posting_invoice')
                })


class InvoicePostingView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'posting/invoices.html'

    def get_journal(self):
        return Journal.objects.select_related(
            'role', 'company', 'ledger', 'created_by', 'company__default_expenditure_account'
        ).get(pk=self.kwargs['pk'])

    def get_role_profiles(self, journal):
        role_profiles = []
        if journal.role:
            for profile in journal.role.profile_set.all():
                role_profiles.append(f"{profile.user.first_name} {profile.user.last_name}")
        return ', '.join(role_profiles)

    def get_company_object_items(self, company):
        object_items = ObjectItem.objects.select_related(
            'company_object'
        ).filter(company_object__company=company)
        company_object_items = {}
        for object_item in object_items.all():
            if object_item.company_object_id in object_items:
                company_object_items[object_item.company_object.id].append(object_item)
            else:
                company_object_items[object_item.company_object.id] = []
                company_object_items[object_item.company_object.id].append(object_item)
        return company_object_items

    def get_company_linked_models(self, company):
        linked_models = {}
        company_object_items = self.get_company_object_items(company)
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            linked_models[company_object.id] = {}
            linked_models[company_object.id]['values'] = {}
            linked_models[company_object.id]['label'] = company_object.label
            linked_models[company_object.id]['class'] = company_object
            linked_models[company_object.id]['object_items'] = []
            if company_object.id in company_object_items:
                linked_models[company_object.id]['object_items'] = company_object_items[company_object.id]
        return linked_models

    def get_context_data(self, **kwargs):
        context = super(InvoicePostingView, self).get_context_data(**kwargs)
        journal = self.get_journal()
        company = self.get_company()
        linked_models = self.get_company_linked_models(company)
        context['company'] = company
        context['journal'] = journal

        year = self.get_year()

        posting_report = PostingWithIncomingReport(journal, linked_models)
        posting_report.process_report()

        context['report'] = posting_report
        context['total_vat_net'] = posting_report.total_net + posting_report.total_vat
        context['year'] = year
        context['linked_models'] = linked_models
        context['linked_models_count'] = len(linked_models)
        return context


class InvoicePostingDontRecordView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'posting/invoices.html'

    def get_journal(self):
        return Journal.objects.select_related(
            'role', 'company', 'ledger', 'created_by', 'company__default_expenditure_account', 'period'
        ).prefetch_related(
            'journal_invoices', 'journal_invoices__invoice', 'journal_invoices__ledger'
        ).get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def get_role_profiles(self, journal):
        role_profiles = []
        if journal.role:
            for profile in journal.role.profile_set.all():
                role_profiles.append(f"{profile.user.first_name} {profile.user.last_name}")
        return ', '.join(role_profiles)

    def get_company_object_items(self, company):
        object_items = ObjectItem.objects.filter(company_object__company=company)
        company_object_items = {}
        for object_item in object_items.all():
            if object_item.company_object_id in object_items:
                company_object_items[object_item.company_object.id].append(object_item)
            else:
                company_object_items[object_item.company_object.id] = []
                company_object_items[object_item.company_object.id].append(object_item)
        return company_object_items

    def get_company_linked_models(self, company):
        linked_models = {}
        company_object_items = self.get_company_object_items(company)
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            linked_models[company_object.id] = {}
            linked_models[company_object.id]['values'] = {}
            linked_models[company_object.id]['label'] = company_object.label
            linked_models[company_object.id]['class'] = company_object
            linked_models[company_object.id]['object_items'] = []
            if company_object.id in company_object_items:
                linked_models[company_object.id]['object_items'] = company_object_items[company_object.id]
        return linked_models

    def get_context_data(self, **kwargs):
        context = super(InvoicePostingDontRecordView, self).get_context_data(**kwargs)
        journal = self.get_journal()
        company = journal.company
        linked_models = self.get_company_linked_models(company)
        context['company'] = company
        context['journal'] = journal
        year = self.get_year()
        posting_report = PostingJournal(journal, linked_models)
        posting_report.process_report()
        context['total_vat_net'] = posting_report.total_net + posting_report.total_vat
        context['report'] = posting_report
        context['year'] = year
        context['linked_models'] = linked_models
        context['linked_models_count'] = len(linked_models)
        return context
