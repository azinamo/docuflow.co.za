import pprint
import logging
from collections import defaultdict
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.urls import reverse

from docuflow.apps.common.utils import group_object_postings
from docuflow.apps.company.models import ObjectItem
from docuflow.apps.fixedasset.enums import FixedAssetStatus
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.invoice.models import Invoice, Status, InvoiceJournal, Journal
from docuflow.apps.invoice.exceptions import InvoiceException
from docuflow.apps.invoice.enums import InvoiceStatus
from docuflow.apps.supplier.models import Ledger as SupplierLedger, History
from docuflow.apps.supplier.services import add_invoice_to_age_analysis, is_supplier_balancing

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class CreatePostingUpdate(object):

    def __init__(self, company, profile, role, update):
        logger.info("------CreatePostingUpdate--------")
        self.company = company
        self.profile = profile
        self.role = role
        self.update = update
        self.date = update.date
        self.paid_printed_status = self.get_to_status('paid-printed')
        self.paid_not_printed = self.get_to_status('paid-not-printed')
        self.end_flow_status = self.get_to_status('end-flow-printed')

    # noinspection PyMethodMayBeStatic
    def get_to_status(self, slug):
        return Status.objects.get(slug=slug)

    @transaction.atomic()
    def execute(self):
        update = self.update
        if update:
            invoices = Invoice.objects.pending_posting(
                company=self.company,
                period=update.period,
                end_date=self.date
            )
            if invoices.count() == 0:
                raise InvoiceException('No invoices to process')

            for invoice in invoices:

                self.create_invoices_updates(update=update, invoices=[invoice])

                self.create_ledger(update=update, selected_invoices=[invoice])

                if not is_supplier_balancing(company=self.update.company, year=update.period.period_year, end_date=update.date, period=update.period):
                    raise InvoiceException('A variation on supplier age and balance sheet found')

    def create_invoices_updates(self, update, invoices):

        for invoice in invoices:
            InvoiceJournal.objects.create(
                invoice=invoice,
                journal=update,
                invoice_status=invoice.status,
                vat_amount=invoice.vat_amount,
                net_amount=invoice.net_amount,
                total_amount=invoice.total_amount,
                invoice_type=invoice.invoice_type,
                supplier=invoice.supplier,
                vat_number=invoice.vat_number,
                accounting_date=invoice.accounting_date,
                invoice_date=invoice.invoice_date,
                supplier_number=invoice.supplier_number
            )

            invoice.journal = update
            if invoice.status.slug == InvoiceStatus.FINAL_SIGNED.value:
                invoice.status = self.end_flow_status
            elif invoice.status.slug == InvoiceStatus.PAID_NOT_ACCOUNT_POSTED.value:
                invoice.status = self.paid_printed_status
            elif invoice.status.slug == InvoiceStatus.PAID_PRINTED_NOT_ACCOUNT_POSTED.value:
                invoice.status = self.paid_printed_status
            invoice.save()

            SupplierLedger.objects.create_from_invoice(
                invoice=invoice,
                profile=self.profile,
                status='completed',
                general_ledger=update.ledger
            )

            if not self.company.run_incoming_document_journal:
                add_invoice_to_age_analysis(invoice)
                history, is_new = History.objects.get_or_create(supplier=invoice.supplier, period=invoice.period)
                if history:
                    history.add_invoice(invoice)

            self.activate_invoice_accounts_assets(invoice)

    # noinspection PyMethodMayBeStatic
    def activate_invoice_accounts_assets(self, invoice):
        for invoice_account in invoice.invoice_accounts.all():
            for fixed_asset in invoice_account.fixed_asset.all():
                fixed_asset.status = FixedAssetStatus.ACTIVE
                fixed_asset.save()

    # noinspection PyMethodMayBeStatic
    def get_company_object_items(self, company):
        object_items = ObjectItem.objects.filter(company_object__company=company)
        company_object_items = {}
        for object_item in object_items.all():
            if object_item.company_object_id in object_items:
                company_object_items[object_item.company_object.id].append(object_item)
            else:
                company_object_items[object_item.company_object.id] = []
                company_object_items[object_item.company_object.id].append(object_item)
        return company_object_items

    def get_company_linked_models(self, company):
        linked_models = {}
        company_object_items = self.get_company_object_items(company)
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            linked_models[company_object.id] = {}
            linked_models[company_object.id]['values'] = {}
            linked_models[company_object.id]['label'] = company_object.label
            linked_models[company_object.id]['class'] = company_object
            linked_models[company_object.id]['object_items'] = []
            if company_object.id in company_object_items:
                linked_models[company_object.id]['object_items'] = company_object_items[company_object.id]
        return linked_models

    def get_posting_report(self, update, selected_invoices):
        linked_models = self.get_company_linked_models(update.company)

        if update.company.run_incoming_document_journal:
            report = PostingWithIncomingReport(
                journal=update,
                linked_models=linked_models,
                selected_invoices=selected_invoices
            )
            report.process_report()
        else:
            report = PostingJournal(journal=update, linked_models=linked_models, selected_invoices=selected_invoices)
            report.process_report()

        return report

    def prepare_ledger_lines(self, update, selected_invoices=None):
        report = self.get_posting_report(update=update, selected_invoices=selected_invoices)
        ledger_lines = defaultdict(list)

        content_type = ContentType.objects.filter(app_label='invoice', model='invoice').first()
        day_total = defaultdict(Decimal)
        for invoice_id, invoice in report.invoices.items():
            for posting_account in invoice['invoice_accounts']:
                credit = 0
                debit = 0
                vat_code = None

                if posting_account.is_reverse:
                    debit_amount = posting_account.amount * -1
                    debit = Decimal(debit_amount)
                else:
                    credit = Decimal(posting_account.amount)

                if posting_account.is_vat and posting_account.vat_code:
                    vat_code = posting_account.vat_code
                day_key = (invoice['accounting_date'], invoice['period'])
                ledger_lines[day_key].append({
                    'account': posting_account.account,
                    'credit': credit,
                    'debit': debit,
                    'supplier': invoice.get('supplier', None),
                    'text': str(posting_account),
                    'vat_code': vat_code,
                    'invoice': invoice.get('docuflow_unique'),
                    'object_items': posting_account.object_items,
                    'accounting_date': invoice.get('accounting_date'),
                    'object_id': invoice_id,
                    'content_type': content_type,
                    'suffix': posting_account.suffix,
                    'ledger_type_reference': invoice.get('supplier_name')
                })
                day_total[day_key] += posting_account.amount
        if report.default_account:
            # default_account_amount = Decimal(report.total)
            for day, total in day_total.items():
                ledger_lines[day].append({
                    'account': report.default_account,
                    'debit': total,
                    'credit': 0
                })
        return ledger_lines

    def create_ledger(self, update, selected_invoices=None):
        logger.info(f"------create ledger for posting invoices--------{update}")
        day_ledger_lines = self.prepare_ledger_lines(update=update, selected_invoices=selected_invoices)
        for day_key, ledger_lines in day_ledger_lines.items():
            ledger = CreateLedger(
                update.company,
                update.period.period_year,
                day_key[1],
                update.created_by,
                update.role,
                str(update),
                ledger_lines,
                day_key[0]
            )
            ledger = ledger.execute()
            if ledger:
                for ledger_line in ledger_lines:
                    if ledger_line.get('invoice'):
                        invoice_journal = InvoiceJournal.objects.filter(
                            journal=update, invoice=ledger_line['invoice']
                        ).first()
                        invoice_journal.ledger = ledger
                        invoice_journal.save()

                # update.ledger = ledger
                # update.save()


class PostingInvoiceAccount(object):
    def __init__(self, invoice_account, linked_models, journal, is_reverse=False):
        self.invoice_account = invoice_account
        self.is_vat = invoice_account.is_vat
        amount = self.get_amount(is_reverse)
        self.vat_amount = self.get_vat_amount(amount)
        self.net_amount = self.get_net_amount(amount)
        self.total_amount = amount
        self.amount = amount
        self.account = self.get_account()
        self.vat_code = self.get_vat_code()
        self.percentage = invoice_account.percentage
        self.comment = invoice_account.comment
        self.suffix = invoice_account.suffix
        self.date = invoice_account.created_at
        self.reinvoice = invoice_account.reinvoice
        self.signed = False
        account_profile = self.get_account_profile()
        self.profile = account_profile
        self.posted_to_accounts = account_profile
        self.linked_models_values = {}
        self.id = invoice_account.id
        self.invoice_id = invoice_account.invoice.id
        self.invoice = invoice_account.invoice
        self.url = self.get_edit_url()
        self.edit_url = self.get_edit_url()
        self.object_links = self.get_object_links(linked_models)
        self.ledger_number = self.get_ledger_number(journal)
        self.is_reverse = is_reverse
        self.period = invoice_account.invoice.period
        self.object_items = invoice_account.object_items.all()

    def __str__(self):
        if self.invoice_account.comment:
            return f"{str(self.invoice_account.invoice)}-{self.invoice_account.comment}"
        else:
            return f"{str(self.invoice_account.invoice)}"

    def get_account_profile(self):
        account_profile = self.invoice_account.profile.code
        if self.invoice_account.role:
            if self.invoice_account.is_posted is True:
                account_profile = self.invoice_account.profile.code
            else:
                account_profile = self.invoice_account.role
        return account_profile

    def get_edit_url(self):
        return reverse('edit_invoice_account', kwargs={'pk': self.invoice_account.id})

    def get_vat_amount(self, amount):
        if self.invoice_account.is_vat:
            return amount
        return 0

    def get_net_amount(self, amount):
        if not self.invoice_account.is_vat:
            return amount
        return 0

    def get_amount(self, is_reverse=False):
        amount = self.invoice_account.amount or Decimal(0)
        if is_reverse and amount:
            amount = amount * -1
        return amount

    def get_vat_code(self):
        return self.invoice_account.vat_code if self.invoice_account.vat_code else ''

    def get_account(self):
        account = self.invoice_account.account
        if self.invoice_account.distribution:
            account = self.invoice_account.invoice.company.default_distribution_account
        return account

    def get_object_links(self, linked_models):
        object_links = {}
        for key, linked_model in linked_models.items():
            object_links[key] = {'value': None, 'code': None, 'object_item': None}

        for link in self.invoice_account.object_items.all():
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.label, 'code': link.code, 'object_item': link}

        return object_links

    def get_ledger_number(self, journal):
        if journal and journal.ledger:
            return journal.ledger.journal_number
        return ''


class PostingAccount(object):
    def __init__(self, account, amount, linked_models, is_vat=False, is_reverse=False, object_items=None,
                 is_split=False, text='', vat_code=None, suffix=None):
        self.is_vat = is_vat
        self.amount = self.get_amount_value(amount, is_reverse)
        self.account = account
        self.linked_models_values = {}
        self.is_reverse = is_reverse
        self.object_items = object_items if object_items else []
        self.object_links = self.get_object_links(linked_models)
        self.is_split = is_split
        self.text = text
        self.vat_code = vat_code
        self.suffix = suffix if suffix else ''

    def __str__(self):
        return f"{self.text}"

    def get_amount_value(self, amount, is_reverse=False):
        return amount * -1 if is_reverse else amount

    def get_object_links(self, linked_models):
        object_links = {}
        for key, linked_model in linked_models.items():
            object_links[key] = {'value': None, 'code': None, 'object_item': None}

        for link in self.object_items:
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.label, 'code': link.code, 'object_item': link}

        return object_links


class PostingWithIncomingReport(object):

    def __init__(self, journal: Journal, linked_models, selected_invoices=None):
        self.journal = journal
        self.invoices = {}
        self.accounts = []
        self.vat_accounts = []
        self.total = 0
        self.total_net = 0
        self.total_vat = 0
        self.total_payment = 0
        self.total_discounts = 0
        self.invoice_total = 0
        self.accounts_total = 0
        self.default_supplier_account = None
        self.default_expenditure_account = None
        self.debit_vat_account = None
        self.linked_models = linked_models
        self.default_account = None
        if journal.company.default_expenditure_account:
            self.default_account = journal.company.default_expenditure_account
        if journal.company.default_supplier_account:
            self.default_supplier_account = journal.company.default_supplier_account
        self.selected_invoices = selected_invoices if selected_invoices else []

    def get_journal_invoices(self):
        qs = InvoiceJournal.objects.prefetch_related(
            'invoice__invoice_accounts', 'invoice__invoice_accounts__account', 'invoice__invoice_accounts__invoice',
            'invoice__invoice_accounts__invoice__supplier', 'invoice__invoice_accounts__invoice__period',
            'invoice__invoice_accounts__vat_code', 'invoice__invoice_accounts__vat_code__account',
            'invoice__invoice_accounts__profile', 'invoice__invoice_accounts__role',
            'invoice__invoice_accounts__object_items', 'invoice__invoice_references',
        ).select_related(
            'invoice', 'invoice__company', 'invoice__period', 'invoice__supplier', 'invoice__vat_code',
            'invoice__vat_code__account', 'invoice__invoice_type', 'supplier', 'linked_journal',
            'invoice_type', 'linked_journal__invoice_type', 'linked_journal__supplier', 'journal',
            'journal__ledger'
        ).filter(
            journal__journal_type=Journal.POSTING, journal=self.journal, invoice__company=self.journal.company
        )
        if self.selected_invoices:
            qs = qs.filter(invoice_id__in=[invoice.id for invoice in self.selected_invoices])
        return qs

    # noinspection PyMethodMayBeStatic
    def get_net_amount(self, invoice, is_reverse=False):
        net_amount = invoice.net_amount
        if is_reverse:
            net_amount = net_amount * -1
        return net_amount

    # noinspection PyMethodMayBeStatic
    def get_vat_amount(self, invoice, is_reverse=False):
        vat_amount = invoice.vat_amount
        if is_reverse:
            vat_amount = vat_amount * -1
        return vat_amount

    # noinspection PyMethodMayBeStatic
    def get_references(self, invoice):
        invoice_references = invoice.invoice_references.all()
        reference_1 = reference_2 = None
        if len(invoice_references) > 0:
            reference_1 = invoice_references[0]
        if len(invoice_references) > 1:
            reference_2 = invoice_references[1]
        return reference_1, reference_2

    def create_posting_account(self, invoice_account, object_items=None):
        posting_account = PostingAccount(
            account=invoice_account.account,
            amount=invoice_account.amount,
            linked_models=self.linked_models,
            is_vat=invoice_account.is_vat,
            is_reverse=True,
            object_items=object_items,
            is_split=False,
            text=f"{str(invoice_account.invoice)}-{invoice_account.comment}",
            vat_code=invoice_account.vat_code,
            suffix=invoice_account.suffix
        )
        logger.info(f" Posting invoice --> {posting_account.text} {posting_account}")
        if posting_account.is_vat:
            self.total_vat += invoice_account.amount
        else:
            self.total_net += invoice_account.amount
        self.total += invoice_account.amount
        return posting_account

    def get_invoice_accounts(self, invoice):
        invoice_accounts = invoice.invoice_accounts.all()
        accounts = []
        for invoice_account in invoice_accounts:
            if invoice_account.is_vat and invoice_account.amount != 0:
                if invoice_account.is_posted:
                    # split the amounts here
                    posting_account = self.create_posting_account(
                        invoice_account=invoice_account,
                        object_items=invoice_account.object_items.all()
                    )
                    self.accounts_total += invoice_account.amount
                    accounts.append(posting_account)
            else:
                posting_accounts = self.get_postings(invoice_account=invoice_account)
                for posting_account in posting_accounts:
                    self.total_vat += posting_account.amount
                    self.total_net += posting_account.amount
                    self.total += posting_account.amount
                    self.accounts_total += posting_account.amount
                    accounts.append(posting_account)

        self.invoice_total = self.accounts_total
        return accounts

    def get_postings(self, invoice_account):
        object_postings = invoice_account.object_postings.all()
        if len(object_postings) > 0:
            grouped_object_postings = group_object_postings(object_postings=object_postings)
            # split the amounts here
            posting_account = PostingAccount(
                account=invoice_account.get_posting_account(),
                amount=invoice_account.amount,
                linked_models=self.linked_models,
                is_vat=invoice_account.is_vat,
                is_reverse=True,
                object_items=invoice_account.object_items.all(),
                is_split=False,
                text=f"{str(invoice_account.invoice)}-{invoice_account.comment}",
                vat_code=invoice_account.vat_code,
                suffix=invoice_account.suffix
            )
            logger.info(f"1v -> Posting invoice --> {posting_account.text} -> {posting_account}")
            posting_accounts = [posting_account]
            for object_posting, object_postings in grouped_object_postings.items():
                posting_accounts = self.split_invoice_accounts(
                    object_postings=object_postings,
                    posting_accounts=posting_accounts
                )
            return posting_accounts
        else:
            posting_account = PostingAccount(
                account=invoice_account.get_posting_account(),
                amount=invoice_account.amount,
                linked_models=self.linked_models,
                is_vat=invoice_account.is_vat,
                is_reverse=True,
                object_items=invoice_account.object_items.all(),
                is_split=False,
                text=f"{invoice_account.invoice}-{invoice_account.comment}",
                vat_code=invoice_account.vat_code,
                suffix=invoice_account.suffix
            )
            logger.info(f" 2v -> Posting invoice --> {posting_account.text} -> {posting_account} ")
            return [posting_account]

    def split_invoice_accounts(self, object_postings, posting_accounts):
        split_accounts = []
        for posting_account in posting_accounts:
            for object_item_posting in object_postings:
                amount = (posting_account.amount * object_item_posting.percentage_fraction)

                object_items = []
                if posting_account.is_split:
                    for item in posting_account.object_items:
                        object_items.append(item)
                object_items.append(object_item_posting.object_item)

                object_posting_account = PostingAccount(
                    account=posting_account.account,
                    amount=amount,
                    linked_models=self.linked_models,
                    is_vat=posting_account.is_vat,
                    is_reverse=True,
                    object_items=object_items,
                    is_split=True,
                    text=posting_account.text,
                    vat_code=posting_account.vat_code,
                    suffix=posting_account.suffix
                )
                logger.info(f" Splitting -> Posting invoice --> {posting_account.text} -> {posting_account} ")
                split_accounts.append(object_posting_account)

        return split_accounts

    def process_report(self):
        logger.info(" --- Process report ---")
        journal_invoices = self.get_journal_invoices()
        for journal_invoice in journal_invoices:
            invoice = journal_invoice.invoice

            debit_nett = self.get_net_amount(journal_invoice)
            debit_vat = self.get_vat_amount(journal_invoice)
            if invoice.vat_code:
                self.vat_accounts.append({
                    'vat_code': invoice.vat_code,
                    'account': invoice.vat_code.account,
                    'accounting_date': invoice.accounting_date,
                    'amount': debit_vat,
                    'vat_account': f"{invoice.vat_code.account}-{invoice.vat_code}"
                })

            references = self.get_references(invoice)

            children = []
            if journal_invoice.linked_journal:
                linked_journal = journal_invoice.linked_journal

                link_debit_nett = self.get_net_amount(linked_journal)
                link_debit_vat = self.get_vat_amount(linked_journal)

                self.total_vat += link_debit_vat
                self.total_net += link_debit_nett
                self.total += (link_debit_nett + link_debit_vat)

            self.invoice_total = 0

            self.invoices[journal_invoice.id] = {
                'docuflow_unique': invoice,
                'invoice_id': invoice.id,
                'vat_type': invoice.vat_code,
                'accounting_date': invoice.accounting_date,
                'period': invoice.period,
                'invoice_date': invoice.invoice_date,
                'due_date': invoice.due_date,
                'supplier_name': invoice.supplier.name,
                'supplier': invoice.supplier,
                'document_type': invoice.invoice_type,
                'supplier_inv': invoice.invoice_number,
                'reference_1': references[0],
                'reference_2': references[1],
                'debit_nett': debit_nett,
                'debit_vat': debit_vat,
                'credit_total': self.accounts_total,
                'children': children,
                'invoice_accounts': self.get_invoice_accounts(invoice=journal_invoice.invoice)
            }


class PostingJournal(object):

    def __init__(self, journal, linked_models, selected_invoices=None):
        self.journal = journal
        self.invoices = {}
        self.accounts = []
        self.vat_accounts = []
        self.total = 0
        self.total_net = 0
        self.total_vat = 0
        self.total_payment = 0
        self.total_discounts = 0
        self.accounts_total = 0
        self.default_supplier_account = None
        self.default_expenditure_account = None
        self.debit_vat_account = None
        self.linked_models = linked_models
        self.default_account = None
        if journal.company.default_supplier_account:
            self.default_account = journal.company.default_supplier_account
        self.selected_invoices = selected_invoices if selected_invoices else []

    def get_journal_invoices(self):
        qs = InvoiceJournal.objects.select_related(
            'supplier', 'linked_journal', 'invoice_type', 'linked_journal__invoice_type', 'linked_journal__supplier'
        ). filter(
            journal__journal_type=Journal.POSTING, journal=self.journal, invoice__company=self.journal.company
        )
        if self.selected_invoices:
            qs = qs.filter(invoice_id__in=[invoice.id for invoice in self.selected_invoices])
        return qs

    def get_net_amount(self, invoice, is_reverse=False):
        net_amount = invoice.net_amount
        if is_reverse:
            net_amount = net_amount * -1
        return net_amount

    def get_vat_amount(self, invoice, is_reverse=False):
        vat_amount = invoice.vat_amount or 0
        if is_reverse:
            vat_amount = vat_amount * -1
        return vat_amount

    def get_references(self, invoice):
        invoice_references = invoice.invoice_references.all()
        reference_1 = reference_2 = None
        if len(invoice_references) > 0:
            reference_1 = invoice_references[0]
        if len(invoice_references) > 1:
            reference_2 = invoice_references[1]

        return reference_1, reference_2

    def process_report(self):
        journal_invoices = self.get_journal_invoices()
        for journal_invoice in journal_invoices:
            invoice = journal_invoice.invoice

            debit_nett = self.get_net_amount(journal_invoice)
            debit_vat = self.get_vat_amount(journal_invoice)

            if invoice.vat_code:
                self.vat_accounts.append({
                    'vat_code': invoice.vat_code,
                    'account': invoice.vat_code.account,
                    'amount': debit_vat,
                    'accounting_date': invoice.accounting_date,
                    'vat_account': f"{invoice.vat_code.account}-{invoice.vat_code}"
                })

            references = self.get_references(invoice=invoice)

            children = []
            invoice_accounts = []
            if journal_invoice.linked_journal:
                linked_journal = journal_invoice.linked_journal

                _link_debit_nett = self.get_net_amount(linked_journal)
                _link_debit_vat = self.get_vat_amount(linked_journal)

                self.total_vat += _link_debit_vat
                self.total_net += _link_debit_nett
                self.total += (_link_debit_nett + _link_debit_vat)

                _link_references = self.get_references(linked_journal.invoice)

            posting_accounts_total = 0
            for invoice_account in journal_invoice.invoice.invoice_accounts.all():
                if invoice_account.is_vat:
                    posting_invoice_account = PostingInvoiceAccount(invoice_account, self.linked_models, self.journal, True)
                    invoice_accounts.append(posting_invoice_account)
                    posting_accounts_total += posting_invoice_account.total_amount

                    self.total_vat += invoice_account.amount or Decimal(0)
                    self.accounts_total = invoice_account.amount or Decimal(0)

                else:
                    posting_invoice_account = PostingInvoiceAccount(invoice_account, self.linked_models, self.journal, True)
                    invoice_accounts.append(posting_invoice_account)
                    posting_accounts_total += posting_invoice_account.total_amount

                    self.total_net += invoice_account.amount
            self.total += posting_accounts_total

            self.invoices[journal_invoice.id] = {
                'docuflow_unique': invoice,
                'vat_type': invoice.vat_code,
                'accounting_date': invoice.accounting_date,
                'period': invoice.period,
                'invoice_date': invoice.invoice_date,
                'due_date': invoice.due_date,
                'supplier_name': invoice.supplier.name,
                'supplier': invoice.supplier,
                'document_type': invoice.invoice_type,
                'supplier_inv': invoice.invoice_number,
                'reference_1': references[0],
                'reference_2': references[1],
                'debit_nett': debit_nett,
                'debit_vat': debit_vat,
                'credit_total': (debit_vat + debit_nett),
                'children': children,
                'invoice_accounts': invoice_accounts
            }
