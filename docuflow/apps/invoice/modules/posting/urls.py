from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='journal_for_posting_invoice'),
    path('create/', views.CreateUpdateView.as_view(), name='create_journal_for_posting_invoice'),
    path('<int:pk>/detail/', views.InvoicePostingView.as_view(), name='posting_invoice_journal_detail'),
    path('<int:pk>/dont-record/detail/', views.InvoicePostingDontRecordView.as_view(),
         name='invoice_posting_dont_record_detail'),
    path('<int:pk>/recalculate/ledger/', views.JournalForPostingRecalculateLedgerView.as_view(),
         name='recalculate_posting_ledger')
]
