from calendar import monthrange
from datetime import datetime, date

from django import forms


class InvoiceAccountDistributeForm(forms.Form):
    def __init__(self, *args, **kwargs):
        invoice_date = None
        if 'invoice' in kwargs:
            invoice = kwargs.pop('invoice')
            if invoice:
                invoice_date = invoice.invoice_date
        super(InvoiceAccountDistributeForm, self).__init__(*args, **kwargs)

        self.fields['start_date'].widget = forms.TextInput(attrs={'data-min-date': invoice_date,
                                                                  'class': 'future_datepicker'
                                                                  })

    month = forms.CharField(required=True, label='How many months', widget=forms.TextInput())
    start_date = forms.DateField(required=True, label='Starting Date')
    #
    # def clean(self):
    #     cleaned_data = super(InvoiceAccountDistributeForm, self).clean()
    #
    #     if 'start_date' in cleaned_data and cleaned_data['start_date']:
    #         self.add_error('start_date', 'Start date is required')
    #
    #
    #     return cleaned_data