from django.apps import AppConfig


class InvoicedistributionConfig(AppConfig):
    name = 'invoicedistribution'
