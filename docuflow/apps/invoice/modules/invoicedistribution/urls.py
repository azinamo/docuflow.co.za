from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from . import views
urlpatterns = [

    url(r'^$', views.InvoiceAccountDistributionReportView.as_view(), name='invoice_account_distributions'),
    url(r'^create/invoice/(?P<invoice_id>\d+)/account/(?P<account_id>\d+)/distribute/$',
        csrf_exempt(views.InvoiceAccountCreateDistributionView.as_view()),
        name='create_invoice_account_distribution'),
    url(r'^invoice/(?P<invoice_id>\d+)/accounts/(?P<account_id>\d+)/distribute/$',
        views.InvoiceAccountDistributionView.as_view(),
        name='distribute_invoice_accounts'),
    url(r'^edit/invoice/(?P<invoice_id>\d+)/account/(?P<account_id>\d+)/distribution/$',
        csrf_exempt(views.InvoiceAccountEditDistributionView.as_view()),
        name='edit_invoice_account_distribution'),
    url(r'^(?P<pk>\d+)/show/$', views.ShowInvoiceAccountDistributionView.as_view(),
        name='show_invoice_account_distribution'),
    url(r'^account/(?P<account_id>\d+)/clear/$',
        csrf_exempt(views.InvoiceAccountClearDistributionView.as_view()),
        name='clear_invoice_account_distribution'),
    url(r'^account/(?P<account_id>\d+)/delete$',
        csrf_exempt(views.InvoiceAccountDeleteDistributionView.as_view()),
        name='delete_invoice_account_distribution'),
    url(r'^(?P<pk>\d+)/balances/$',
        csrf_exempt(views.ShowDistributionBalanceBreakdownView.as_view()),
        name='show_distribution_balance_breakdown'),
    # url(r'^edit/postings/account/(?P<pk>\d+)/distribute/$', views.InvoiceAccountEditDistrubutionView.as_view(),
    #     name='edit_invoice_account_distribution'),
    url(r'^(?P<pk>\d+)/journal/detail/$', views.DistributionInvoiceJournalView.as_view(),
        name='invoice_distribution_journal_detail'),
    url(r'^journal/invoice/create/$', views.CreateJournalForDistributionInvoiceView.as_view(),
        name='create_journal_for_distribution_invoice'),
    url(r'^journal/invoice/$', views.JournalForDistributionInvoiceView.as_view(),
        name='journal_for_distribution_invoice'),
    url(r'^balances/$', views.DistributionBalanceView.as_view(),
        name='invoice_account_distribution_balance'),
    url(r'^capture/journals/$', views.CaptureDistributionJournalView.as_view(),
        name='capture_distribution_journal'),
]

