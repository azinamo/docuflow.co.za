import logging
import pprint
from decimal import Decimal
from typing import List

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.invoice.exceptions import InvalidDefaultAccount, InvoiceException, IncomingInvoiceNotFound
from docuflow.apps.invoice.models import Invoice, Journal, InvoiceJournal, Status, InvoiceAccount
from docuflow.apps.invoice.reports import JournalReport
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.supplier.models import Ledger as SupplierLedger, History
from docuflow.apps.supplier.services import add_invoice_to_age_analysis, is_supplier_balancing, \
    add_to_supplier_age_analysis

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class CreateJournal(object):

    def __init__(self, update, date):
        self.update = update
        self.date = date
        self.from_statuses = self.get_from_statuses()
        self.to_status = self.get_to_invoice_status()

    def get_from_statuses(self):
        statuses = Status.objects.filter(slug__in=[])
        return [status.id for status in statuses]

    def get_to_invoice_status(self):
        return Status.objects.filter(slug='printed-status-still-in-the-flow').first()

    def get_decline_status(self):
        return Status.objects.filter(slug='decline-printed').first()

    def get_invoices(self):
        return Invoice.objects.pending_incoming(
            company=self.update.company, period=self.update.period, end_date=self.date
        ).select_related('status', 'supplier', 'company', 'invoice_type').order_by('created_at')

    def execute(self):
        invoices = self.get_invoices()
        if invoices.count() == 0:
            raise IncomingInvoiceNotFound('No invoices found')

        self.create_journal_invoices(invoices=invoices)

        self.create_ledger()

        if not is_supplier_balancing(company=self.update.company, year=self.update.period.period_year,
                                     end_date=self.update.date, period=self.update.period):
            raise InvoiceException('A variation on supplier age and balance sheet found')

    def create_journal_invoices(self, invoices):
        declined_status: Status = self.get_decline_status()

        invoices_journals = InvoiceJournal.objects.filter(
            invoice_id__in=[invoice.id for invoice in invoices]).order_by('-id').all()

        for invoice in invoices:
            self.create_invoice_journal(
                invoice=invoice,
                invoices_journals=invoices_journals,
                declined_status=declined_status
            )

    def create_invoice_journal(self, invoice: Invoice, invoices_journals: List[InvoiceJournal], declined_status: Status):
        has_incoming_journal = invoice.has_incoming_journal()

        invoice_journal = InvoiceJournal()
        invoice_journal.invoice = invoice
        invoice_journal.journal = self.update
        invoice_journal.invoice_status = invoice.status
        invoice_journal.invoice_type = invoice.invoice_type
        invoice_journal.supplier = invoice.supplier
        invoice_journal.vat_number = invoice.vat_number
        invoice_journal.accounting_date = invoice.accounting_date
        invoice_journal.invoice_date = invoice.invoice_date
        invoice_journal.supplier_number = invoice.supplier_number
        is_adjustment = invoice.is_adjustment
        if invoice.is_decline_approved:
            invoice_journal.vat_amount = invoice.vat_amount * -1
            invoice_journal.net_amount = invoice.net_amount * -1
            invoice_journal.total_amount = invoice.total_amount * -1
            invoice.journal = self.update
            invoice.status = declined_status
        else:
            invoice_journal.vat_amount = invoice.vat_amount
            invoice_journal.net_amount = invoice.net_amount
            invoice_journal.total_amount = invoice.total_amount

            for previous_invoice_journal in invoices_journals:
                if invoice.id == previous_invoice_journal.invoice_id:
                    invoice_journal.linked_journal = previous_invoice_journal

            invoice.status = self.to_status
        invoice_journal.save()
        invoice.save()

        if invoice.is_declined:
            decline_invoice(invoice=invoice)
            if has_incoming_journal:
                add_to_supplier_age_analysis(
                    supplier=invoice_journal.supplier,
                    period=invoice.period,
                    amount=(invoice.total_amount * -1)
                )

            InvoiceAccount.objects.filter(invoice=invoice).update(goods_received=None, goods_returned=None)
        else:
            if is_adjustment:
                # TODO - Add tests when there is an adjustment
                # add the different only to the age analysis
                amount = 0
                if invoice_journal.linked_journal:
                    amount = invoice.total_amount - invoice_journal.linked_journal.total_amount
                if amount != 0:
                    add_to_supplier_age_analysis(
                        supplier=invoice_journal.supplier,
                        period=invoice.period,
                        amount=amount
                    )
            else:
                add_invoice_to_age_analysis(invoice=invoice)
            history, is_new = History.objects.get_or_create(supplier=invoice.supplier, period=invoice.period)
            if history:
                history.add_invoice(invoice)
        # invoice.add_to_ledger(self.update.created_by, SupplierLedger.PENDING, self.update.ledger)

    def create_ledger(self):
        incoming_report = IncomingReport(self.update)
        incoming_report.generate()

        ledger_lines = self.prepare_ledger_lines(incoming_report.ledger_lines)
        ledger = CreateLedger(
            company=self.update.company,
            year=self.update.period.period_year,
            period=self.update.period,
            profile=self.update.created_by,
            role=self.update.role,
            text=str(self.update),
            journal_lines=ledger_lines,
            date=self.update.date
        )
        ledger = ledger.execute()
        if ledger:
            self.update.ledger = ledger
            self.update.save()
        return ledger

    # noinspection PyMethodMayBeStatic
    def prepare_ledger_lines(self, accounts_lines):
        lines = []
        for account, account_line in accounts_lines.items():
            lines.append(account_line)
        return lines


class IncomingReport(object):

    def __init__(self, journal):
        self.journal = journal
        self.invoices = {}
        self.vat_accounts = []
        self.total = 0
        self.total_net = 0
        self.total_vat = 0
        self.total_payment = 0
        self.total_discounts = 0
        self.ledger_lines = {}
        self.vat_account = self.get_vat_account(journal.company)
        self.expenditure_account = self.get_expenditure_account(journal.company)
        self.supplier_account = self.get_default_supplier_account(journal.company)

    # noinspection PyMethodMayBeStatic
    def get_vat_account(self, company):
        if company.default_vat_account:
            return company.default_vat_account
        raise InvalidDefaultAccount('Default vat account is not setup, please ensure its setup before you can '
                                    'run this report')

    # noinspection PyMethodMayBeStatic
    def get_expenditure_account(self, company):
        if company.default_expenditure_account:
            return company.default_expenditure_account
        raise InvalidDefaultAccount('Default expenditure account is not setup, please ensure its setup before you can '
                                    'run this report')

    # noinspection PyMethodMayBeStatic
    def get_default_supplier_account(self, company):
        if company.default_supplier_account:
            return company.default_supplier_account
        raise InvalidDefaultAccount('Default supplier account is not setup, please ensure its setup before you can '
                                    'run this report')

    def get_journal_invoices(self):
        return InvoiceJournal.objects.prefetch_related(
            'invoice__invoice_accounts', 'invoice__invoice_accounts__account',
            'invoice__invoice_accounts__invoice', 'invoice__invoice_accounts__invoice__supplier',
            'invoice__invoice_accounts__invoice__period', 'invoice__invoice_accounts__vat_code',
            'invoice__invoice_accounts__vat_code__account', 'invoice__invoice_accounts__profile',
            'invoice__invoice_accounts__role', 'invoice__invoice_accounts__object_items',
            'invoice__invoice_references',
        ).select_related(
            'invoice', 'invoice__company', 'invoice__period', 'invoice__supplier', 'invoice__vat_code',
            'invoice__vat_code__account', 'invoice__invoice_type', 'supplier', 'linked_journal', 'invoice_type',
            'linked_journal__invoice_type', 'linked_journal__supplier', 'journal', 'journal__ledger'
        ).filter(
            journal__journal_type=Journal.INCOMING, journal=self.journal, invoice__company=self.journal.company
        )

    # noinspection PyMethodMayBeStatic
    def get_references(self, invoice):
        invoice_references = invoice.invoice_references.all()
        reference_1 = reference_2 = None
        if len(invoice_references) > 0:
            reference_1 = invoice_references[0]
        if len(invoice_references) > 1:
            reference_2 = invoice_references[1]

        return reference_1, reference_2

    def add_ledger_line(self, key, credit, debit, invoice=None, vat_code=None):
        if key in self.ledger_lines:
            self.ledger_lines[key]['credit'] += credit if credit else 0
            self.ledger_lines[key]['debit'] += debit if debit else 0
        else:
            self.ledger_lines[key] = {
                'account': key[0],
                'credit': credit if credit else 0,
                'debit': debit if debit else 0,
                'accounting_date': self.journal.date,
                'object_id': self.journal.id,
                'vat_code': vat_code,
                'invoice': invoice,
                'content_type': ContentType.objects.get_for_model(self.journal)
            }

    def generate(self):
        journal_invoices = self.get_journal_invoices()
        content_type = ContentType.objects.get_for_model(model=Invoice, for_concrete_model=False)
        for journal_invoice in journal_invoices:
            invoice = journal_invoice.invoice

            if not invoice.has_incoming_journal() and invoice.is_declined:
                continue

            debit_nett = JournalReport.net_amount(journal_invoice)
            debit_vat = JournalReport.vat_amount(journal_invoice)
            credit_vat = 0
            invoice_total = debit_nett + debit_vat
            credit_vat_code = None
            credit_vat_account = None

            self.total_vat += debit_vat
            self.total_net += debit_nett
            self.total += invoice_total

            supplier_control_account = invoice.supplier.default_account
            if supplier_control_account:
                self.add_ledger_line(key=(supplier_control_account, ), credit=invoice_total, debit=0)
            elif self.supplier_account:
                self.add_ledger_line(key=(self.supplier_account, ), credit=invoice_total, debit=0)

            if self.expenditure_account:
                self.add_ledger_line(key=(self.expenditure_account, ), credit=0, debit=debit_nett)

            children = []
            if journal_invoice.linked_journal:
                linked_journal = journal_invoice.linked_journal

                if linked_journal.invoice.vat_code:
                    if linked_journal.vat_code:
                        credit_vat_code = linked_journal.vat_code
                        credit_vat_account = linked_journal.vat_code.account
                    else:
                        credit_vat_code = linked_journal.invoice.vat_code
                        credit_vat_account = linked_journal.invoice.vat_code.account

                    vat_amount = linked_journal.vat_amount
                    _debit_vat = _credit_vat = 0
                    if vat_amount < 0:
                        _debit_vat = vat_amount * -1
                    else:
                        _credit_vat = vat_amount

                    self.vat_accounts.append({
                        'journal_id': linked_journal.id,
                        'vat_code': linked_journal.invoice.vat_code,
                        'account': credit_vat_account,
                        'invoice': linked_journal.invoice,
                        'is_reverse': True,
                        'debit': _debit_vat,
                        'credit': _credit_vat,
                        'accounting_date': self.journal.date,
                        'object_id': invoice.id,
                        'content_type': content_type,
                        'ledger_type_reference': invoice.supplier.name,
                        'vat_account': f"{linked_journal.invoice.vat_code.account}-{linked_journal.invoice.vat_code}"
                    })
                    self.add_ledger_line(key=(credit_vat_account, invoice), credit=_credit_vat, debit=_debit_vat,
                                         vat_code=linked_journal.invoice.vat_code, invoice=linked_journal.invoice)

                _link_debit_nett = linked_journal.net * -1
                _link_debit_vat = linked_journal.vat * -1

                _total = _link_debit_nett + _link_debit_vat
                self.total_vat += _link_debit_vat
                self.total_net += _link_debit_nett
                self.total += _total
                _link_references = self.get_references(linked_journal.invoice)

                _supplier_control_account = linked_journal.invoice.supplier.default_account
                if _supplier_control_account:
                    self.add_ledger_line(key=(_supplier_control_account, ), credit=_total, debit=0, invoice=invoice)
                elif self.supplier_account:
                    self.add_ledger_line(key=(self.supplier_account, ), credit=_total, debit=0, invoice=invoice)

                if self.expenditure_account:
                    self.add_ledger_line(key=(self.expenditure_account, ), credit=0, debit=_link_debit_nett,
                                         invoice=invoice)

                children.append({
                    'invoice': linked_journal.invoice,
                    'update': linked_journal,
                    'net': _link_debit_nett,
                    'vat': _link_debit_vat,
                    'total': (_link_debit_nett + _link_debit_vat)
                })

            if invoice.vat_code:
                vat_code = journal_invoice.vat_code if journal_invoice.vat_code else invoice.vat_code
                account = vat_code.account if vat_code else self.vat_account
                self.vat_accounts.append({
                    'journal_id': journal_invoice.id,
                    'is_reverse': False,
                    'vat_code': journal_invoice.vat_code,
                    'account': account,
                    'invoice': invoice,
                    'debit': debit_vat,
                    'credit': credit_vat,
                    'credit_vat_code': credit_vat_code,
                    'credit_vat_account': credit_vat_account,
                    'accounting_date': self.journal.date,
                    'object_id': invoice.id,
                    'content_type': content_type,
                    'ledger_type_reference': invoice.supplier.name,
                    'vat_account': f"{account}-{journal_invoice.vat_code}"
                })
                self.add_ledger_line(key=(account, invoice), credit=credit_vat, debit=debit_vat, vat_code=vat_code,
                                     invoice=invoice)
            self.invoices[journal_invoice.id] = {
                'invoice': invoice,
                'update': journal_invoice,
                'children': children
            }


class IncomingJournal(object):

    def __init__(self, incoming_report, update):
        self.report = incoming_report
        self.update = update
        self.ledger_lines = []

    def generate_ledger_lines(self):
        for vat_account in self.report.vat_accounts:
            self.ledger_lines.append(vat_account)

        content_type = ContentType.objects.filter(app_label='invoice', model='journal').first()
        if self.report.expenditure_account:
            amount = self.report.total_net
            self.ledger_lines.append({
                'account': self.report.expenditure_account,
                'credit': 0,
                'debit': amount,
                'accounting_date': self.update.date,
                'object_id': self.update.id,
                'content_type': content_type
            })
        if self.report.supplier_account:
            amount = Decimal(self.report.total)
            self.ledger_lines.append({
                'account': self.report.supplier_account,
                'credit': amount,
                'debit': 0,
                'accounting_date': self.update.date,
                'object_id': self.update.id,
                'content_type': content_type
            })


def decline_invoice(invoice: Invoice):
    try:
        supplier_ledger = SupplierLedger.objects.get(
            object_id=invoice.id,
            content_type=ContentType.objects.get_for_model(invoice)
        )
        supplier_ledger.delete()
    except SupplierLedger.DoesNotExist:
        logger.info("Supplier ledger does not exists")
