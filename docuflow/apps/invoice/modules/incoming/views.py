import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.utils.timezone import now
from django.views.generic import ListView, FormView, TemplateView

from docuflow.apps.common import utils
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.invoice.exceptions import InvoiceException
from docuflow.apps.invoice.enums import InvoiceStatus
from docuflow.apps.invoice.models import Journal, Invoice, Status
from docuflow.apps.journals.exceptions import JournalNotBalancingException
from .forms import UpdateInvoiceForm
from .services import IncomingReport

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'incoming/index.html'
    context_object_name = 'updates'
    model = Journal
    paginate_by = 30

    def get_queryset(self):
        return Journal.objects.incoming().select_related(
            'role', 'created_by', 'period', 'company', 'ledger'
        ).filter(
            company=self.get_company(), period__period_year=self.get_year()
        )

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['year'] = self.get_year()
        return context


class CreateUpdateView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'incoming/create.html'
    form_class = UpdateInvoiceForm

    def get_initial(self):
        initial = super(CreateUpdateView, self).get_initial()
        initial['date'] = now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateUpdateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateUpdateView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['company'] = company
        context['year'] = year
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the invoice updates, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateUpdateView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    form.save()

                    return JsonResponse({
                        'text': 'Incoming invoices successfully updated',
                        'error': False,
                        'redirect': reverse('journal_for_incoming_invoice')
                    })
            except JournalNotBalancingException as exception:
                logger.exception(exception)
                return JsonResponse({
                    'text': str(exception),
                    'details': str(exception),
                    'error': True
                })
            except InvoiceException as exception:
                logger.exception(exception)
                return JsonResponse({
                    'text': str(exception),
                    'details': str(exception),
                    'error': True
                })
        return super(CreateUpdateView, self).form_valid(form)


class IncomingInvoicesView(ProfileMixin, TemplateView):
    template_name = 'invoice/update/incoming/incoming.html'

    def get_invoices(self, company):
        return Invoice.objects.select_related(
            'status', 'supplier', 'company', 'invoice_type'
        ).filter(
            status___slug__in=InvoiceStatus.update_incoming_from_statuses(),
            company=company,
            invoice_type__is_account_posting=True,
            period_id=self.request.GET['period']
        )

    def get_context_data(self, **kwargs):
        context = super(IncomingInvoicesView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        invoices = self.get_invoices(company=company)
        context['company'] = company
        context['year'] = year
        context['invoices'] = invoices
        return context


class IncomingInvoiceJournalView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'incoming/journal.html'

    def get_journal(self):
        return Journal.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(IncomingInvoiceJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        journal = self.get_journal()
        context['company'] = company
        context['journal'] = journal
        context['year'] = self.get_year()
        report = IncomingReport(journal=journal)
        report.generate()
        context['report'] = report
        return context
