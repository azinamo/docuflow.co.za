from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='journal_for_incoming_invoice'),
    path('create/', views.CreateUpdateView.as_view(), name='create_incoming_invoices'),
    path('<int:pk>/detail/', views.IncomingInvoiceJournalView.as_view(), name='invoice_journal_detail'),
    path('create/update/invoice/', views.CreateUpdateView.as_view(), name='create_update_incoming_report'),
    path('update/invoice/create/', views.CreateUpdateView.as_view(), name='create_journal_for_incoming_invoice'),
]
