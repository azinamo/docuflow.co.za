from django.apps import AppConfig


class IncomingConfig(AppConfig):
    name = 'docuflow.apps.invoice.modules.incoming'
