from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.PaymentsView.as_view(), name='payment_index'),
    path('create/<int:supplier_id>/', views.CreateSupplierPaymentView.as_view(), name='create_supplier_payment'),
    path('updates/', views.JournalForPaymentInvoiceView.as_view(), name='journal_for_payment_invoice'),
    path('update/detail/<int:pk>/', views.PaymentInvoiceJournalView.as_view(), name='payments_invoice_journal_detail'),
    path('update/create/', views.UpdatePaymentOfInvoicesView.as_view(), name='create_journal_for_payment_invoice'),
    path('invoices/unpaid/', views.UnpaidInvoicesView.as_view(), name='unpaid_invoice_by_supplier'),
    path('generate/unpaid/invoices/', views.GenerateUnpaidInvoicesView.as_view(), name='generate_unpaid_invoices'),
    path('record/<int:supplier_id>/', views.RecordPaymentView.as_view(), name='record_suppliers_payment'),
    path('record/supplier/<int:supplier_id>/invoice/<int:invoice_id>/',
         csrf_exempt(views.RecordPaymentView.as_view()), name='record_invoice_payment'),
    path('pay/invoices/', views.PayInvoicesView.as_view(), name='pay_supplier_invoice'),
    path('supplier/<int:payment_id>/advice/', csrf_exempt(views.PaymentAdviceView.as_view()), name='payment_advice'),
    path('<int:payment_id>/advice/', views.PaymentAdviceView.as_view(), name='payment_advice'),
    path('<int:payment_id>/email/', views.EmailSupplierPaymentView.as_view(), name='email_supplier_payment'),
    path('save/<int:payment_id>/', views.SavePaymentView.as_view(), name='save_payment'),
    path('report/', views.SupplierPaymentReportView.as_view(), name='supplier_payments'),
    path('list/', views.SupplierPaymentListView.as_view(), name='payments_list'),
    path('<int:pk>/edit/', views.EditPaymentView.as_view(), name='edit_payment'),
    path('<int:pk>/delete/', views.DeletePaymentView.as_view(), name='delete_payment'),
    path('enter/', views.EnterPaymentView.as_view(), name='enter_payment'),
    path('enter/pay/invoices/', views.EnterPaymentView.as_view(), name='enter_payment_pay_invoice'),
    path('sundry/', views.SundryPaymentView.as_view(), name='sundry_payment'),
    path('allocate/sundry/', views.AllocateSundryPaymentView.as_view(), name='allocate_sundry_payment'),
    path('sundry/invoices/', views.SundryPaymentInvoicesView.as_view(), name='sundry_payment_invoices'),
    path('allocate/sundry/invoices/', views.AllocateSundryPaymentInvoicesView.as_view(), name='allocate_sundry_payment_invoices'),
    path('sundry/amount/', views.SundryPaymentAmountView.as_view(), name='sundry_payment_amount'),
    path('not-allocated/', views.SundryPaymentNotAllocatedView.as_view(), name='sundry_payment_not_allocated'),
    path('invoice/<int:invoice_id>/', views.InvoicePaymentView.as_view(), name='invoice_payments'),
    path('list/', views.PaymentListView.as_view(), name='payments_list'),
]