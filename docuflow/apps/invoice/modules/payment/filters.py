import logging

from django_filters import rest_framework as filters

from docuflow.apps.invoice.models import Payment

logger = logging.getLogger(__name__)


class PaymentFilter(filters.FilterSet):
    supplier__name = filters.CharFilter(lookup_expr='icontains')
    supplier__code = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Payment
        fields = ['payment_date', 'payment_method', 'supplier_id', 'supplier__code', 'supplier__name', 'total_amount',
                  'company__slug']

    @property
    def qs(self):
        q = super(PaymentFilter, self).qs
        term = self.request.GET.get('q', None)
        slug = self.request.GET.get('company_slug', None)

        if term:
            q = q.filter(supplier__code__icontains=term) | q.filter(supplier__name__icontains=term) | \
                q.filter(payment_method__name__icontains=term)
        if slug:
            q = q.filter(company__slug=slug)
        return q
