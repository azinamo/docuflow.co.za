from django.apps import AppConfig

# from .signals import allocate_payment
# from docuflow.apps.supplier.services import add_payment_to_age_analysis


class PaymentConfig(AppConfig):
    name = 'docuflow.apps.invoice.modules.payment'

    # def ready(self):
    #     allocate_payment.connect(add_payment_to_age_analysis)
