class PaymentException(Exception):
    pass


class AccountNotAvailableException(PaymentException):
    pass


class NotAllocatedAmountException(Exception):
    pass
