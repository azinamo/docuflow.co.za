import logging
from decimal import Decimal
from collections import defaultdict

from django import forms
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as __

from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Account, VatCode, PaymentMethod
from docuflow.apps.invoice.models import Invoice, InvoicePayment, ChildPayment, Comment, Status, Payment, Journal
from docuflow.apps.period.models import Period
from docuflow.apps.supplier.models import Ledger as SupplierLedger, Supplier
from docuflow.apps.supplier.services import (add_payment_to_age_analysis, add_invoice_payment_to_age_analysis,
                                             is_supplier_balancing)
from docuflow.apps.supplier.modules.supplierledger.forms import LedgerPaymentForm
from docuflow.apps.supplier.modules.supplierledger.services import CreatePaymentLedger
from .exceptions import NotAllocatedAmountException, PaymentException
from .services import CreatePaymentUpdate

logger = logging.getLogger(__name__)


class PaymentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.can_edit = kwargs.pop('can_edit')
        super(PaymentForm, self).__init__(*args, **kwargs)
        accounts = Account.objects.filter(company=self.company)
        self.fields['payment_method'].queryset = PaymentMethod.objects.filter(company=self.company)
        self.fields['payment_method'].initial = self.company.default_payment_method
        self.fields['account'].queryset = accounts
        self.fields['account'].initial = self.company.default_bank_account
        self.fields['adjustment_account'].queryset = accounts
        self.fields['period'].queryset = Period.objects.open(self.company, year=self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=self.company, module=Module.PURCHASE.value)
        self.fields['supplier'].queryset = self.fields['supplier'].queryset.filter(company=self.company)
        if not self.can_edit:
            self.fields['total_amount'].widget.attrs.update(readonly='readonly')

    total_amount = forms.DecimalField(required=False, label=__('Total of above invoices'), widget=forms.TextInput())
    total_discount = forms.DecimalField(required=False, label=__('Discount total as selected'), widget=forms.TextInput())
    discount_disallowed = forms.DecimalField(required=False, label=__('Other adjustments to Payment'),
                                             widget=forms.TextInput())
    net_amount = forms.DecimalField(required=False, label=__('Net payment to be made'), widget=forms.TextInput())
    payment_method = forms.ModelChoiceField(required=True, label=__('Payment Method'), queryset=None)
    account = forms.ModelChoiceField(required=True, queryset=None)
    remittance_text = forms.CharField(required=False, label=__('Reason for adjustment'))

    class Meta:
        model = Payment
        fields = ('period', 'payment_date', 'account', 'total_amount', 'total_discount', 'discount_disallowed',
                  'net_amount', 'payment_method', 'memo', 'reference', 'attachment', 'vat_code', 'supplier',
                  'adjustment_account', 'remittance_text')
        widgets = {
            'payment_date': forms.TextInput(attrs={'class': 'datepicker'}),
            'memo': forms.Textarea(attrs={'cols': 5, 'rows': 10}),
            'supplier': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(PaymentForm, self).clean()

        payment_date = cleaned_data.get('payment_date')
        period = cleaned_data.get('period')
        supplier = cleaned_data.get('supplier')

        if not period:
            self.add_error('period', __('Period is required'))

        if not payment_date:
            self.add_error('payment_date', __('Date is required'))

        if period and payment_date:
            if not period.is_valid(payment_date):
                self.add_error('payment_date', __('Period and payment date do not match'))

        if not supplier:
            self.add_error('supplier', __('Supplier is required'))

        discount_disallowed = cleaned_data.get('discount_disallowed')
        adjustment_account = cleaned_data.get('adjustment_account')
        if discount_disallowed and discount_disallowed > 0:
            if not adjustment_account:
                self.add_error('adjustment_account', 'Adjustment account required')
            if not cleaned_data.get('remittance_text'):
                self.add_error('remittance_text', 'Reason for adjustment is required')

        return cleaned_data

    def save(self, commit=True):
        payment = super(PaymentForm, self).save(commit=False)
        payment.role_id = self.request.session['role']
        payment.profile_id = self.request.session['profile']
        payment.company = self.company
        payment.save()

        data = self.prepare_payment_data(payment=payment)

        self.handle_ledger_allocation(data=data)

        payment_amount = payment.total_paid
        self.payment_allocation(data=data)
        payment_amount = self.pay_credit_notes(data=data, payment_amount=payment_amount)
        self.pay_invoices(data=data, payment_amount=payment_amount)
        self.add_to_supplier_ledger(payment=payment)

        return payment

    def prepare_payment_data(self, payment: Payment):
        data = defaultdict(list)
        for field, value in self.data.items():
            if field.startswith('invoice_amount', 0, 14):
                invoice_id = field[15:]
                invoice = Invoice.objects.get(pk=invoice_id)
                is_discountable = self.data.get(f'discount_invoice_{invoice_id}')

                if invoice.is_credit_type:
                    credit_note_data = {
                        'invoice': invoice_id,
                        'payment': payment.id,
                        'allocated': Decimal(self.data.get(f'invoice_amount_{invoice_id}'))
                    }
                    if is_discountable:
                        credit_note_data['discount'] = self.data.get(f'invoice_discount_{invoice_id}')
                    data['credit_notes'].append(credit_note_data)
                else:
                    invoice_data = {
                        'invoice': invoice_id,
                        'payment': payment.id,
                        'allocated': Decimal(self.data.get(f'invoice_amount_{invoice_id}'))
                    }
                    if is_discountable:
                        invoice_data['discount'] = self.data.get(f'invoice_discount_{invoice_id}')
                    data['invoices'].append(invoice_data)
            elif field.startswith('payment_amount', 0, 14):
                payment_id = field[15:]
                data['payments'].append({'payment': payment_id, 'parent': payment.id, 'amount': value})
            elif field.startswith('ledger_amount', 0, 13):
                ledger_id = field[14:]
                data['ledger_payments'].append({'ledger': ledger_id, 'payment': payment.id, 'amount': value})
        return data

    def handle_ledger_allocation(self, data):
        for ledger_payment_data in data.get('ledger_payments', []):
            ledger_allocation_form = LedgerPaymentForm(data=ledger_payment_data, company=self.company)
            if ledger_allocation_form.is_valid():
                ledger_allocation_form.save()
            else:
                logger.info(ledger_allocation_form.errors)
                raise forms.ValidationError(__('Something went wrong saving the ledger payment'))

    def pay_credit_notes(self, data, payment_amount: Decimal):
        for credit_note_data in data.get('credit_notes', []):
            invoice_amount = credit_note_data['allocated']
            paid_invoice_amount = 0
            if invoice_amount >= payment_amount:
                paid_invoice_amount = invoice_amount
                payment_amount = 0
            elif invoice_amount < payment_amount:
                paid_invoice_amount = invoice_amount
                payment_amount = invoice_amount - payment_amount

            if paid_invoice_amount != 0:
                credit_note_data['allocated'] = paid_invoice_amount
                invoice_payment_form = InvoicePaymentForm(
                    data=credit_note_data, company=self.company, role=self.role, profile=self.profile
                )
                if invoice_payment_form.is_valid():
                    invoice_payment_form.save()
                else:
                    logger.info(invoice_payment_form.errors)
                    raise forms.ValidationError(__('Something went wrong saving the invoice payment'))
        return payment_amount

    def pay_invoices(self, data, payment_amount: Decimal):
        for invoice_data in data.get('invoices', []):
            invoice_amount = invoice_data['allocated']
            paid_invoice_amount = 0
            if invoice_amount >= payment_amount:
                paid_invoice_amount = invoice_amount
                payment_amount = 0
            elif invoice_amount < payment_amount:
                paid_invoice_amount = invoice_amount
                payment_amount = invoice_amount - payment_amount

            if paid_invoice_amount != 0:
                invoice_data['allocated'] = paid_invoice_amount
                invoice_payment_form = InvoicePaymentForm(
                    data=invoice_data, company=self.company, role=self.role, profile=self.profile
                )
                if invoice_payment_form.is_valid():
                    invoice_payment_form.save()
                else:
                    logger.info(invoice_payment_form.errors)
                    raise forms.ValidationError(__('Something went wrong saving the invoice payment'))
        return payment_amount

    def payment_allocation(self, data):
        for payment_data in data.get('payments', []):
            payment_allocation_form = PaymentAllocationForm(data=payment_data, company=self.company)
            if payment_allocation_form.is_valid():
                payment_allocation_form.save()
            else:
                logger.info(payment_allocation_form.errors)
                raise forms.ValidationError(__('Something went wrong saving the child payment'))

    # noinspection PyMethodMayBeStatic
    def add_to_supplier_ledger(self, payment: Payment):
        logger.info("Add payment to supplier ledger")
        create_payment = CreatePaymentLedger(payment=payment, status=SupplierLedger.PENDING)
        create_payment.execute()
        logger.info("End payment to supplier ledger")


class SundryPaymentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(SundryPaymentForm, self).__init__(*args, **kwargs)
        self.fields['payment_method'].queryset = PaymentMethod.objects.filter(company=self.company)
        self.fields['account'].queryset = Account.objects.for_bank().filter(company=self.company)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['supplier'].queryset = self.fields['supplier'].queryset.filter(company=self.company)

    period = forms.ModelChoiceField(required=True, label='Period', queryset=None)
    total_amount = forms.DecimalField(required=True, label='Payment Amount',
                                      widget=forms.TextInput(attrs={
                                          'data-payment-amount-url': reverse_lazy('sundry_payment_amount')})
                                      )
    net_amount = forms.DecimalField(required=False, label='Net payment to be made', widget=forms.TextInput())
    # balance = forms.DecimalField(required=False, widget=forms.TextInput())
    payment_method = forms.ModelChoiceField(required=True, label='Payment Method', queryset=None)
    account = forms.ModelChoiceField(required=True, label='Which Bank GL Acc Number', queryset=None)

    class Meta:
        model = Payment
        fields = ('supplier', 'period', 'payment_date', 'account', 'total_amount', 'net_amount', 'payment_method',
                  'memo', 'reference', 'attachment', 'comment', 'balance')
        widgets = {
            'payment_date': forms.TextInput(attrs={'class': 'datepicker'}),
            'memo': forms.Textarea(attrs={'cols': 5, 'rows': 10}),
            'supplier': forms.Select(attrs={'class': 'chosen-select',
                                            'data-supplier-invoices-url': reverse_lazy('sundry_payment_invoices')
                                            }),
        }

    def clean(self):
        cleaned_data = super(SundryPaymentForm, self).clean()

        if not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')

        payment_date = cleaned_data['payment_date']
        if payment_date:
            if 'period' in cleaned_data and cleaned_data['period']:
                period = cleaned_data['period']
                if not period.is_valid(payment_date):
                    self.add_error('payment_date', 'Payment date and period do not match.')
                    self.add_error('period', 'Period and payment date do not match.')

        return cleaned_data

    def save(self, commit=True):
        payment = super(SundryPaymentForm, self).save(commit=commit)

        comment = self.cleaned_data.get('comment', '') or payment.memo
        payment_balance = self.cleaned_data.get('balance', 0)

        if payment_balance > 0 and not comment:
            raise NotAllocatedAmountException('You have not allocated the amount of payment, should it'
                                              ' be left unallocated')

        payment.payment_type = Payment.SUNDRY
        payment.net_amount = payment_balance
        payment.role = self.role
        payment.profile = self.profile
        payment.company = self.company
        payment.save()

        if payment:
            for field in self.request.POST:
                if field.startswith('invoice_amount', 0, 14):
                    invoice_id = field[15:]

                    allocated = 0
                    allocate_id = f"allocate_{invoice_id}"
                    if allocate_id in self.request.POST and self.request.POST[allocate_id]:
                        allocated = self.monitize(self.request.POST[allocate_id])

                    invoice = Invoice.objects.select_related(
                        'status'
                    ).prefetch_related(
                        'payments'
                    ).filter(id=int(invoice_id)).first()

                    if invoice and allocated:
                        invoice_payment = InvoicePayment.objects.create(
                            invoice=invoice,
                            payment=payment,
                            allocated=allocated
                        )
                        if invoice.open_amount == 0:
                            if invoice.status.slug == 'final-signed':
                                invoice.status = Status.objects.get(slug='paid-not-account-posted')
                            elif invoice.status.slug == 'end-flow-printed':
                                invoice.status = Status.objects.get(slug='paid-not-printed')

                            invoice.date_paid = payment.payment_date
                            invoice.payment_date = payment.payment_date
                            invoice.save()

                        note = f"Payment made by {payment.payment_method} on {payment.payment_date}"
                        link_url = reverse('payment_advice', kwargs={'payment_id': payment.id})
                        invoice.log_activity(self.request.user, self.request.session['role'], note,
                                             Comment.LINK, None, None, link_url)

                        add_invoice_payment_to_age_analysis(invoice_payment=invoice_payment)

            self.add_to_supplier_ledger(payment=payment)
            add_payment_to_age_analysis(payment=payment)

        return payment

    # noinspection PyMethodMayBeStatic
    def add_to_supplier_ledger(self, payment):
        logger.info("Add payment to supplier ledger")
        create_payment = CreatePaymentLedger(payment=payment, status=SupplierLedger.PENDING)
        create_payment.execute()
        logger.info("End payment to supplier ledger")


class PaymentProofOfPaymentForm(forms.ModelForm):
    attachment = forms.FileField(required=False, label='Proof of payment')

    class Meta:
        model = Payment
        fields = ('attachment', )


class AllocateSundryPaymentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        if 'company' in kwargs:
            self.company = kwargs.pop('company')
        if 'year' in kwargs:
            self.year = kwargs.pop('year')
        super(AllocateSundryPaymentForm, self).__init__(*args, **kwargs)
        if self.company:
            self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
            self.fields['period'].empty_label = None
            self.fields['supplier'].queryset = self.fields['supplier'].queryset.filter(company=self.company)

    class Meta:
        model = Payment
        fields = ('supplier', 'period', 'payment_date')
        widgets = {
            'payment_date': forms.TextInput(attrs={'class': 'datepicker'}),
            'supplier': forms.Select(attrs={'class': 'chosen-select',
                                            'data-supplier-invoices-url': reverse_lazy('allocate_sundry_payment_invoices'),
                                            'data-ajax-url': reverse_lazy('allocate_sundry_payment_invoices')
                                            }),
        }

    def clean(self):
        cleaned_data = super(AllocateSundryPaymentForm, self).clean()

        if not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')

        payment_date = cleaned_data['payment_date']
        if payment_date:
            if 'period' in cleaned_data and cleaned_data['period']:
                period = cleaned_data['period']
                if not period.is_valid(payment_date):
                    self.add_error('payment_date', 'Payment date and period do not match.')
                    self.add_error('period', 'Period and payment date do not match.')

        return cleaned_data


class CommentForm(forms.Form):
    comment = forms.CharField(required=False, label='Reason', widget=forms.Textarea(attrs={
        'id': 'payment_comment',
        'cols': 5,
        'rows': 10
    }))


class UpdatePaymentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(UpdatePaymentForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(company=self.company, year=self.year).order_by('-period')
        self.fields['period'].empty_label = None

    date = forms.DateField(required=True, label='Upto and including')

    class Meta:
        model = Journal
        fields = ('period', 'date')
        widgets = {
            'period': forms.Select(attrs={'class': 'form-control chosen-select'}),
        }

    def clean(self):
        cleaned_data = super(UpdatePaymentForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        return cleaned_data

    def get_payments_to_update(self):
        logger.info(f"Payments to update for period {self.cleaned_data['period']} upto {self.cleaned_data['date']} ")
        return Payment.objects.pending_updates(
            company=self.company,
            period=self.cleaned_data['period'],
            end_date=self.cleaned_data['date']
        )

    def save(self, commit=True):
        payments = self.get_payments_to_update()
        if not payments:
            raise PaymentException('No payments to process')

        update = super().save(commit=False)
        update.created_by = self.profile
        update.company = self.company
        update.role = self.role
        update.journal_type = Journal.PAYMENTS
        update.save()

        payment_update = CreatePaymentUpdate(company=self.company, year=self.year, update=update)
        payment_update.execute()

        return update


class InvoicePaymentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(InvoicePaymentForm, self).__init__(*args, **kwargs)
        self.fields['invoice'].queryset = self.fields['invoice'].queryset.filter(company=self.company)
        self.fields['payment'].queryset = self.fields['payment'].queryset.filter(company=self.company)

    class Meta:
        model = InvoicePayment
        fields = ('invoice', 'payment', 'discount', 'allocated')

    def save(self, commit=True):
        invoice = self.cleaned_data['invoice']
        if invoice.open_amount == 0:
            raise PaymentException(f'Invoice {invoice} is already paid up, please check the payments')

        invoice_payment = super(InvoicePaymentForm, self).save(commit=True)

        invoice = invoice_payment.invoice
        payment = invoice_payment.payment
        if invoice.is_credit_type:
            invoice.allocated = invoice_payment.allocated * -1

        invoice.date_paid = payment.payment_date
        invoice.payment_date = payment.payment_date
        invoice.discount = invoice_payment.discount
        open_amount = invoice.calculate_open_amount()

        if invoice.is_final_signed:
            if open_amount == 0:
                invoice.status = Status.objects.get(slug='paid-not-account-posted')
            else:
                invoice.status = Status.objects.get(slug='pp-not-account-posted')
        elif invoice.is_end_of_flow_printed:
            if open_amount == 0:
                invoice.status = Status.objects.get(slug='paid')
            else:
                invoice.status = Status.objects.get(slug='partially-paid')
        elif invoice.is_partially_paid:
            if open_amount == 0:
                invoice.status = Status.objects.get(slug='paid')
        invoice.open_amount = open_amount
        invoice.save()

        note = f"Payment made by {payment.payment_method} on {payment.payment_date}"
        link_url = reverse('payment_advice', kwargs={'payment_id': payment.id})
        invoice.log_activity(
            user=self.profile.user,
            role=self.role,
            comment=note,
            log_type=Comment.LINK,
            attachment=None,
            linked_invoice=None,
            link_url=link_url
        )

        return invoice_payment


class PaymentAllocationForm(forms.ModelForm):
    """
        Using a payment that's not yet used to pay invoices in this payment
    """

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(PaymentAllocationForm, self).__init__(*args, **kwargs)
        self.fields['parent'].queryset = self.fields['parent'].queryset.filter(company=self.company)
        self.fields['payment'].queryset = self.fields['payment'].queryset.filter(company=self.company)

    class Meta:
        model = ChildPayment
        fields = ('parent', 'payment', 'amount')

    def save(self, commit=True):
        child_payment = super(PaymentAllocationForm, self).save(commit=commit)

        payment = child_payment.payment

        payment.balance = payment.balance - child_payment.amount
        payment.save()

        return child_payment


class PaymentFilterForm(forms.Form):
    supplier = forms.ModelChoiceField(queryset=Supplier.objects.all(), required=False)
    due_date = forms.DateField()
