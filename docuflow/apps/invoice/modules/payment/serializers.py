from rest_framework import serializers

from django.urls import reverse

from docuflow.apps.invoice.models import Payment


class PaymentSerializer(serializers.ModelSerializer):

    supplier = serializers.StringRelatedField(many=False)
    period = serializers.StringRelatedField(many=False)
    role = serializers.StringRelatedField(many=False)
    profile = serializers.StringRelatedField(many=False)
    payment_method = serializers.StringRelatedField(many=False)
    account = serializers.StringRelatedField(many=False)
    vat_code = serializers.StringRelatedField()
    payment_reference = serializers.SerializerMethodField()
    total_amount = serializers.DecimalField(
        decimal_places=2,
        max_digits=15
    )

    detail_url = serializers.SerializerMethodField()

    class Meta:
        model = Payment
        fields = '__all__'

    def get_payment_reference(self, instance):
        return instance.__str__()

    def get_detail_url(self, instance):
        return reverse('payment_advice', kwargs={'payment_id': instance.id})