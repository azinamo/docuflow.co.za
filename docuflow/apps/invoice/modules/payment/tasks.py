from django.conf import settings
from celery import shared_task
import logging

from docuflow.apps.company.models import Company, Account, ObjectItem, CompanyObject, PaymentMethod, VatCode, Branch
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.period.models import Period, Year

from .services import CreatePaymentUpdate

logger = logging.getLogger(__name__)


@shared_task
def process_payments_ledger(company_slug, year_id, period_id, profile_id, role_id, date):
    logger.info("--Start celery task now --")
    successes = 0
    failed = 0

    company = Company.objects.get(slug=company_slug)
    year = Year.object.get(pk=year_id)
    period = Period.objects.get(pk=period_id)
    profile = Profile.objects.get(pk=profile_id)
    role = Role.objects.get(pk=role_id)

    payment_update = CreatePaymentUpdate(company, year, period, profile, role, date)
    payment_update.execute()

    return "Payment update successfully created".format(successes, failed)
