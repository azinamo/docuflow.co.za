import logging
import pprint
from decimal import Decimal

from annoying.functions import get_object_or_None
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction

from docuflow.apps.common.enums import Module
from docuflow.apps.common.services import get_credit_amount, get_debit_amount
from docuflow.apps.invoice.models import Journal, Status, InvoicePayment, Payment
from docuflow.apps.journals.exceptions import JournalNotBalancingException
from docuflow.apps.journals.models import Journal as Ledger, JournalLine as LedgerLine
from docuflow.apps.supplier.models import Ledger as SupplierLedger, LedgerPayment
from docuflow.apps.supplier.modules.supplierledger.services import CreatePaymentLedger
from docuflow.apps.supplier.services import (add_payment_allocation_age_analysis, add_invoice_payment_to_age_analysis,
                                             is_supplier_balancing, add_journal_payment_to_age_analysis,
                                             clear_unallocated_on_age_analysis, add_payment_to_age_analysis)
from .exceptions import AccountNotAvailableException, PaymentException
from .reports import PaymentJournal

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class CreatePaymentUpdate:

    update = None

    def __init__(self, company, year, update: Journal):
        logger.info("---------CreatePaymentUpdate ----")
        self.company = company
        self.year = year
        self.update = update

    def get_payments_to_update(self):
        logger.info(f"- Payments to update for period {self.update.period} upto {self.update.date}  ----")
        return Payment.objects.pending_updates(self.company, period=self.update.period, end_date=self.update.date)

    def get_invoice_statuses(self):
        return Status.objects.filter(slug__in=['paid-printed', 'paid-printed-not-account-posted'])

    def execute(self):
        logger.info("---------EXECUTE   ----")
        payments = self.get_payments_to_update()
        if not payments:
            raise PaymentException('No payments to process')

        self.update_payments(update=self.update, payments=payments)
        self.create_payment_ledgers(journal=self.update)

        if not is_supplier_balancing(company=self.company, year=self.update.period.period_year,
                                     period=self.update.period, end_date=self.update.date):
            raise PaymentException('A variation on supplier age and balance sheet found')

    def update_payments(self, update, payments):
        logger.info(f"---------Update payments for journal   ----{update}")
        invoice_statuses = self.get_invoice_statuses()
        for payment in payments:
            payment.journal = update
            payment.save()

            self.update_invoice_payments(payment, invoice_statuses)
            self.update_journal_payments(payment)
            self.update_child_payments(payment)

    # noinspection PyMethodMayBeStatic
    def add_to_supplier_ledger(self, payment, ledger):
        logger.info(f"Add {payment}({payment.id}) -> {payment.total_amount} payment to supplier ledger")
        create_payment = CreatePaymentLedger(payment, SupplierLedger.COMPLETED, ledger)
        create_payment.execute()

    # noinspection PyMethodMayBeStatic
    def update_invoice_payments(self, payment, invoice_statuses):
        # logger.info(f"---------Update payment {payment} invoices with statuses {invoice_statuses} ----")
        paid_printed_status = invoice_statuses.filter(slug='paid-printed').first()
        paid_printed_not_posted_status = invoice_statuses.filter(slug='paid-printed-not-account-posted').first()
        total_invoice = 0
        for invoice_payment in payment.invoices.all():
            invoice = invoice_payment.invoice
            logger.info(f"Invoice -- {invoice} with open amount {invoice.open_amount},  paid using {invoice_payment.payment}({invoice_payment.payment_id}) => {invoice_payment.amount} and discount {invoice_payment.discount}")
            if invoice.status:
                if invoice.status.slug == 'paid' and paid_printed_status:
                    invoice.status = paid_printed_status
                elif invoice.status.slug == 'paid-not-printed' and paid_printed_status:
                    invoice.status = paid_printed_status
                elif invoice.status.slug == 'paid-not-account-posted' and paid_printed_not_posted_status:
                    invoice.status = paid_printed_not_posted_status
                invoice.save()
            if invoice_payment.amount:
                total_invoice += invoice_payment.amount
            if invoice_payment.discount:
                total_invoice += invoice_payment.discount
            add_invoice_payment_to_age_analysis(invoice_payment=invoice_payment)
        logger.info(f"Total invoice amount paid -> {total_invoice}")
        logger.info("-----------------------------------------------------------\r\n\n")

    # noinspection PyMethodMayBeStatic
    def update_journal_payments(self, payment):
        total_invoice = 0
        for ledger_payment in payment.supplier_ledgers.all():
            add_journal_payment_to_age_analysis(ledger_payment=ledger_payment)
            logger.info(f"Total invoice amount paid -> {ledger_payment.amount}")
        logger.info("-----------------------------------------------------------\r\n\n")

    # noinspection PyMethodMayBeStatic
    def update_child_payments(self, payment):
        for child_payment in payment.children.all():
            clear_unallocated_on_age_analysis(child_payment=child_payment)
            logger.info(f"Total invoice amount paid -> {child_payment.amount}")
        logger.info("-----------------------------------------------------------\r\n\n")

    # noinspection PyMethodMayBeStatic
    def prepare_ledger_lines(self, journal):
        logger.info(f"---------prepare_ledger_lines {journal} ----")
        journal_report = PaymentJournal(journal)
        journal_report.process_report()
        return journal_report

    # noinspection PyMethodMayBeStatic
    def get_vat_account(self, journal):
        account = journal.company.default_vat_account
        if not account:
            raise AccountNotAvailableException('Default vat account not found')
        return account

    # noinspection PyMethodMayBeStatic
    def get_supplier_account(self, journal):
        account = journal.company.default_supplier_account
        if not account:
            raise AccountNotAvailableException('Default supplier account not found')
        return account

    # noinspection PyMethodMayBeStatic
    def get_discount_received_account(self, journal):
        account = journal.company.default_discount_received_account
        if not account:
            raise AccountNotAvailableException('Default default_discount_received_account account not found')
        return account

    def create_payment_ledger(self, journal, date):
        ledger = Ledger.objects.create(
            company=journal.company,
            module=Module.PAYMENT,
            year=self.year,
            period=journal.period,
            created_by=self.update.created_by,
            role=journal.role,
            date=date,
            journal_text=str(journal)
        )
        return ledger

    def create_payment_ledgers(self, journal):
        content_type = ContentType.objects.filter(model='payment', app_label='invoice').first()

        supplier_account = None
        vat_account = None
        discount_received_account = None

        journal_report = PaymentJournal(journal)
        journal_report.process_report()

        if journal.company.default_vat_account:
            vat_account = journal.company.default_vat_account
        if journal.company.default_supplier_account:
            supplier_account = journal.company.default_supplier_account
        if journal.company.default_discount_received_account:
            discount_received_account = journal.company.default_discount_received_account
        bulk_ledger_items = []
        for payment_report in journal_report.payments:
            payment = payment_report['payment']
            if payment.account:
                ledger = self.create_payment_ledger(journal, payment.payment_date)
                if ledger:
                    total_debit = 0
                    payment_amount = payment.total_amount
                    net_payment_amount = payment.net_amount

                    payment_vat_on_discount = payment_report['total_vat_on_discount']
                    payment_total_discount = payment_report['total_discount']
                    # net_payment_amount = payment_report['net_amount']
                    total_credit = net_payment_amount

                    self.add_to_supplier_ledger(payment, ledger)

                    if payment_amount:
                        debit = get_debit_amount(credit=0, debit=payment_amount)
                        credit = get_credit_amount(credit=0, debit=payment_amount)
                        total_debit += Decimal(debit)
                        total_credit += Decimal(credit)

                        if payment.supplier.default_account:
                            supplier_account = payment.supplier.default_account
                        ledger_line = LedgerLine(**{'journal': ledger,
                                                    'account': supplier_account,
                                                    'credit': credit,
                                                    'debit': debit,
                                                    'text': str(journal),
                                                    'accounting_date': payment.payment_date,
                                                    'content_type': content_type,
                                                    'object_id': payment.id,
                                                    'ledger_type_reference': str(payment.supplier)
                                                    })
                        bulk_ledger_items.append(ledger_line)
                    if payment_vat_on_discount:
                        debit = get_debit_amount(debit=payment_vat_on_discount, credit=0)
                        credit = get_credit_amount(debit=payment_vat_on_discount, credit=0)

                        total_credit += Decimal(credit)
                        total_debit += Decimal(debit)

                        ledger_line = LedgerLine(**{'journal': ledger,
                                                    'account': vat_account,
                                                    'vat_code': payment.vat_code,
                                                    'credit': credit,
                                                    'debit': debit,
                                                    'text': str(journal),
                                                    'accounting_date': payment.payment_date,
                                                    'content_type': content_type,
                                                    'object_id': payment.id,
                                                    'ledger_type_reference': str(payment.supplier)
                                                    })
                        bulk_ledger_items.append(ledger_line)
                    if payment_total_discount:
                        credit = get_credit_amount(debit=payment_total_discount, credit=0)
                        debit = get_debit_amount(debit=payment_total_discount, credit=0)
                        total_credit += Decimal(credit)
                        total_debit += Decimal(debit)
                        # TODO - Add tests
                        if payment.adjustment_account:
                            discount_received_account = payment.adjustment_account
                        if not discount_received_account:
                            raise PaymentException('Account for discount received not found')

                        ledger_line = LedgerLine(**{'journal': ledger,
                                                    'account': discount_received_account,
                                                    'vat_code': payment.vat_code,
                                                    'credit': credit,
                                                    'debit': debit,
                                                    'text': payment.remittance_text if payment.remittance_text else str(journal),
                                                    'accounting_date': payment.payment_date,
                                                    'content_type': content_type,
                                                    'object_id': payment.id,
                                                    'ledger_type_reference': str(payment.supplier)
                                                    })
                        bulk_ledger_items.append(ledger_line)
                    ledger_line = LedgerLine(**{'journal': ledger,
                                                'account': payment.account,
                                                'credit': net_payment_amount,
                                                'debit': 0,
                                                'supplier': payment.supplier,
                                                'text': str(payment),
                                                'accounting_date': payment.payment_date,
                                                'content_type': content_type,
                                                'object_id': payment.id,
                                                'ledger_type_reference': str(payment.supplier)
                                                })
                    bulk_ledger_items.append(ledger_line)
                    difference = round(total_debit, 2) - round(total_credit, 2)
                    if difference != 0:
                        raise JournalNotBalancingException(f'Journals did not balance by {difference}, this transaction '
                                                           f'could not be saved {payment}')

        if len(bulk_ledger_items) > 0:
            LedgerLine.objects.bulk_create(bulk_ledger_items)


class AllocateSundryPayment:

    def __init__(self, company, role_id, profile_id, form, post_request):
        self.company = company
        self.role_id = role_id
        self.profile_id = profile_id
        self.form = form
        self.post_request = post_request

    def create_payment(self):
        payment = self.form.save(commit=False)
        # parent_payment.payment_date = datetime.now().date()
        payment.role_id = self.role_id
        payment.profile_id = self.profile_id
        payment.company = self.company
        payment.payment_type = Payment.ALLOCATION
        payment.save()
        return payment

    def monitize(self, string):
        return Decimal(string.replace(',', ''))

    def handle_payment_allocation(self, payment_allocation, field, value):
        logger.info(f'handle_payment_allocation {field} --> {value}')
        payment_id = field[18:]
        payment_amount = Decimal(value) * -1
        child_payment = Payment.objects.create_child_payment(payment_allocation, payment_id, payment_amount)
        if child_payment:
            add_payment_allocation_age_analysis(child_payment)

    def handle_invoice_payments(self, payment_allocation, field, value):
        logger.info(f'handle_invoice_payments {field} --> {value}')
        paid_not_posted_status = get_object_or_None(Status, slug='paid-not-account-posted')
        paid_not_printed_status = get_object_or_None(Status, slug='paid-not-printed')
        partially_paid_status = get_object_or_None(Status, slug='partially-paid')

        invoice_id = field[20:]
        amount_allocated = Decimal(value)
        if amount_allocated:
            invoice_payment = InvoicePayment.objects.create(
                payment=payment_allocation,
                invoice_id=invoice_id,
                allocated=amount_allocated
            )

            invoice = invoice_payment.invoice
            invoice_open_amount = invoice.calculate_open_amount()
            if invoice_open_amount > 0:
                if invoice.status.slug == 'end-flow-printed':
                    invoice.status = partially_paid_status
                    invoice.save()
            elif invoice_open_amount == 0:
                if invoice.status.slug == 'final-signed':
                    invoice.status = paid_not_posted_status
                elif invoice.status.slug == 'end-flow-printed':
                    invoice.status = paid_not_printed_status
                elif invoice.status.slug == 'partially-paid':
                    invoice.status = paid_not_printed_status

                invoice.date_paid = payment_allocation.payment_date
                invoice.payment_date = payment_allocation.payment_date
                invoice.save()

            invoice.open_amount = invoice.calculate_open_amount()
            invoice.save()

    def handle_ledger_payments(self, payment_allocation, field, value):
        logger.info(f'handle_ledger_payments {field} --> {value}')
        supplier_ledger_id = field[25:]
        amount_allocated = Decimal(value)
        if amount_allocated:
            amount = amount_allocated * -1
            ledger_payment = LedgerPayment()
            ledger_payment.ledger_id = supplier_ledger_id
            ledger_payment.payment = payment_allocation
            ledger_payment.amount = amount
            ledger_payment.save()

            supplier_ledger = SupplierLedger.objects.filter(id=supplier_ledger_id).first()
            supplier_ledger.balance = supplier_ledger.balance - amount
            supplier_ledger.save()

    def execute(self):
        with transaction.atomic():
            payment_allocation = self.create_payment()

            if payment_allocation:
                for field, value in self.post_request.items():
                    if field.startswith('payment_allocated_', 0, 18) and value:
                        self.handle_payment_allocation(payment_allocation, field, value)
                    if field.startswith('invoice_amount_paid_', 0, 20) and value:
                        self.handle_invoice_payments(payment_allocation, field, value)
                    if field.startswith('ledger_payment_allocated', 0, 24) and value:
                        self.handle_ledger_payments(payment_allocation, field, value)
            return payment_allocation


def delete_payment(payment: Payment):
    with transaction.atomic():
        invoice_payments = payment.invoices.all()
        invoices = []
        update_invoice_payments(payment=payment, is_reverse=True)

        for invoice_payment in invoice_payments:
            invoice = invoice_payment.invoice
            invoices.append(invoice)
            invoice.save()

            comments = invoice.invoice_comments.filter(link_url=payment.get_absolute_url())
            comments.delete()

        for child_payment in payment.children.all():
            payment_child = child_payment.payment
            payment_child.balance += payment_child.net_amount
            payment_child.save()

        for ledger_payment in payment.supplier_ledgers.all():
            ledger = ledger_payment.ledger
            ledger.balance += ledger_payment.amount
            ledger.save()

        # delete entry created by payment in the supplier ledger
        supplier_ledger = SupplierLedger.objects.filter(
            content_type=ContentType.objects.get_for_model(payment),
            object_id=payment.id
        )
        supplier_ledger.delete()

        payment.balance = payment.net_amount
        payment.delete()
        for invoice in invoices:
            open_amount = invoice.calculate_open_amount()
            if invoice.status.slug == 'paid-not-printed':
                invoice.status = Status.objects.get(slug='end-flow-printed')
            elif open_amount != 0:
                if invoice.status.slug == 'paid-not-account-posted':
                    invoice.status = Status.objects.get(slug='final-signed')
                elif invoice.status.slug in ['paid-not-printed', 'partially-paid', 'paid']:
                    invoice.status = Status.objects.get(slug='end-flow-printed')
            invoice.date_paid = None
            invoice.payment_date = None
            invoice.open_amount = open_amount
            invoice.save()

        # handle cases where journal has been used in making payments

        # A sundry payment not updated, must be reversed.
        if payment.payment_type == Payment.SUNDRY and not payment.journal:
            add_payment_to_age_analysis(payment=payment, is_reverse=True)


def update_invoice_payments(payment: Payment, is_reverse=False):
    logger.info(f"---------Update payment {payment}, is reversal  {is_reverse} ----")
    for invoice_payment in payment.invoices.all():
        invoice = invoice_payment.invoice
        if is_reverse:
            if invoice.open_amount != 0:
                if invoice.status.slug == 'paid-printed':
                    invoice.status = Status.objects.get(slug='paid')
                elif invoice.status.slug == 'paid-not-printed':
                    invoice.status = Status.objects.get(slug='paid')
                elif invoice.status.slug == 'pp-not-account-posted':
                    invoice.status = invoice.status = Status.objects.get(slug='paid-not-account-posted')
                invoice.save()
        else:
            if invoice.status.slug == 'paid':
                invoice.status = Status.objects.get(slug='paid-printed')
            elif invoice.status.slug == 'paid-not-printed':
                invoice.status = Status.objects.get(slug='paid-printed')
            elif invoice.status.slug == 'paid-not-account-posted':
                invoice.status = Status.objects.get(slug='pp-not-account-posted')
            invoice.save()
        if payment.journal:
            add_invoice_payment_to_age_analysis(invoice_payment=invoice_payment, is_reverse=is_reverse)
