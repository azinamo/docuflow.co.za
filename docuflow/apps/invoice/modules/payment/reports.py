import logging
from decimal import Decimal
import pprint


from docuflow.apps.invoice.models import Payment
from docuflow.apps.invoice.reports import JournalReport

from .exceptions import AccountNotAvailableException

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class PaymentJournal(JournalReport):

    def __init__(self, journal):
        logger.info("------------------- Generate payment journal------------")
        self.journal = journal
        super(JournalReport, self).__init__()
        self.invoices = {}
        self.accounts = []
        self.total = 0
        self.invoice_total = 0
        self.vat_on_discount_total = 0
        self.total_discount = 0
        self.total_vat = 0
        self.total_excl_vat = 0
        self.total_payment = 0
        self.total_net_payment = 0
        self.total_payment_discount = 0
        self.total_vat_on_discount = 0
        self.payments = []

        if not journal.company.default_supplier_account:
            raise AccountNotAvailableException('Please ensure the default supplier account is set')
        if not journal.company.default_expenditure_account:
            raise AccountNotAvailableException('Please ensure the default expenditure account is set')
        if not journal.company.default_vat_account:
            raise AccountNotAvailableException('Please ensure the default vat account is set')

        self.supplier_account = journal.company.default_supplier_account.code
        self.expenditure_account = journal.company.default_expenditure_account.code
        self.vat_account = journal.company.default_vat_account.code

        if journal.company.default_supplier_account:
            self.credit_account = journal.company.default_supplier_account.code
        if journal.company.default_discount_received_account:
            self.discount_received_account = journal.company.default_discount_received_account.code

    def get_payments(self):
        return Payment.objects.select_related(
            'company', 'account', 'period', 'supplier', 'payment_method', 'profile', 'role'
        ).prefetch_related(
            'invoices', 'invoices__invoice', 'invoices__invoice__invoice_type', 'invoices__invoice__supplier',
            'children', 'children__payment', 'children__payment__supplier'
        ).filter(journal=self.journal)

    @staticmethod
    def invoice_vat_percentage(invoice):
        if invoice.vat_code:
            return invoice.vat_code.percentage
        return 0

    @staticmethod
    def discount_vat(discount, vat_code):
        if vat_code:
            discount_vat = round((vat_code.percentage/(100 + vat_code.percentage)) * float(discount), 2)
            return discount_vat * -1
        return 0

    @staticmethod
    def net_discount(discount, vat_on_discount):
        net = (float(discount) * -1) - float(vat_on_discount)
        return net

    @staticmethod
    def calculate_net_payment(invoice_total, net_discount, vat_on_discount):
        net = float(invoice_total) + float(net_discount) + float(vat_on_discount)
        return net

    def calculate_remmitance(self):
        return float(self.total_excl_vat) + float(self.total_discount) + float(self.vat_on_discount_total) - float(self.total)

    def process_report(self):
        payments = self.get_payments()
        for payment in payments:
            invoice_total = 0
            total_vat_on_discount = 0
            display_self = True
            total_discount = payment.net_discount
            total_net_payment = payment.net_discount + payment.vat_on_discount
            self.vat_on_discount_total += float(payment.vat_on_discount)
            total_vat_on_discount += payment.vat_on_discount
            for invoice_payment in payment.invoices.all():
                display_self = False

                invoice_total += invoice_payment.amount
                self.total_excl_vat += invoice_payment.amount

                if invoice_payment.vat_on_discount:
                    total_net_payment += invoice_payment.vat_on_discount
                    total_vat_on_discount += invoice_payment.vat_on_discount
                    self.vat_on_discount_total += invoice_payment.vat_on_discount

                if invoice_payment.net_discount:
                    total_net_payment += invoice_payment.net_discount
                    total_discount += invoice_payment.net_discount

            for child_payment in payment.children.all():
                total_net_payment -= child_payment.amount
                invoice_total -= child_payment.amount
                display_self = False

            if payment.payment_type == Payment.SUNDRY:
                total_net_payment += payment.net_amount
                invoice_total += payment.net_amount
            elif display_self:
                total_net_payment += payment.net_amount
                invoice_total += payment.net_amount
            self.payments.append({
                'payment': payment,
                'display_self': display_self,
                'total_amount': total_net_payment,
                'total_vat_on_discount': total_vat_on_discount,
                'total_discount': total_discount,
                'payment_discount': payment.net_discount if payment.net_discount else 0
            })

            self.invoice_total += invoice_total
            self.total_net_payment += total_net_payment
            self.total_payment += total_net_payment
            self.total_discount += Decimal(total_discount)


class PaymentReport(object):

    def __init__(self, payment: Payment):
        self.payment = payment
        self.total = 0
        self.total_discount = 0
        self.invoice_total = 0
        self.discount_disallowed = 0
        self.invoice_payments = []
        self.child_payments = []
        self.ledger_payments = []

    # def get_child_payments(self):
    #     child_payments = []
    #     for child_payment in self.payment.children.all():
    #         self.total -= child_payment.amount
    #         self.invoice_total -= child_payment.amount
    #         child_payments.append(child_payment)
    #     return child_payments

    @staticmethod
    def calculate_percentage(total, value):
        percentage = 0
        if value > 0 and total > 0:
            percentage = (value/total) * 100
        return round(percentage, 2)

    def process_report(self):
        if self.payment.discount_disallowed:
            self.discount_disallowed = self.payment.discount_disallowed * -1
            self.total_discount -= self.discount_disallowed
            self.total -= self.payment.discount_disallowed

        for child_payment in self.payment.children.all():
            self.total -= child_payment.amount
            self.invoice_total -= child_payment.amount
            self.child_payments.append(child_payment)

        for supplier_ledger_payment in self.payment.supplier_ledgers.all():
            self.total += supplier_ledger_payment.amount
            self.invoice_total += supplier_ledger_payment.amount
            self.ledger_payments.append(supplier_ledger_payment)

        for invoice_payment in self.payment.invoices.all():
            amount_allocated = invoice_payment.allocated
            if invoice_payment.invoice.is_credit_type and amount_allocated > 0:
                amount_allocated = invoice_payment.allocated * -1
            invoice_total = amount_allocated - invoice_payment.discount
            self.total_discount += invoice_payment.invoice.discount
            self.total += invoice_total
            self.invoice_total += amount_allocated
            self.invoice_payments.append(invoice_payment)
