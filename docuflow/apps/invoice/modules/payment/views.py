import logging
import os
import pprint
from collections import OrderedDict
from datetime import datetime
from decimal import Decimal

from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.core.mail import EmailMessage
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.template.loader import get_template
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, View, TemplateView, ListView, DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django_filters.views import FilterView

from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.company.models import PaymentMethod, VatCode
from docuflow.apps.invoice.exceptions import InvalidPeriodAccountingDateException
from docuflow.apps.invoice.filters import UnpaidInvoiceFilter
from docuflow.apps.invoice.models import (
    Journal, Invoice, Status, InvoicePayment, Payment
)
from docuflow.apps.journals.exceptions import JournalNotBalancingException
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.period.models import Period
from docuflow.apps.supplier.models import Supplier, Ledger as SupplierLedger
from .exceptions import PaymentException, NotAllocatedAmountException
from .forms import PaymentForm, PaymentProofOfPaymentForm, SundryPaymentForm, AllocateSundryPaymentForm, CommentForm, \
    UpdatePaymentForm, PaymentFilterForm
from .reports import PaymentJournal, PaymentReport
from .services import AllocateSundryPayment, delete_payment

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class PaymentsView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'payment/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ajax_url'] = reverse('payment_index') + '?format=datatables'
        return context


class CreateSupplierPaymentView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = 'payment/create.html'
    form_class = PaymentForm
    model = Payment

    def get_default_vat_code(self):
        return VatCode.objects.default().filter(company=self.get_company()).first()

    def get_initial(self):
        initial = super(CreateSupplierPaymentView, self).get_initial()
        initial['payment_date'] = datetime.now().strftime('%Y-%m-%d')
        initial['supplier'] = self.get_supplier().id
        vat_code = self.get_default_vat_code()
        if vat_code:
            initial['vat_code'] = vat_code
        return initial

    def get_invoices(self, supplier_id):
        return Invoice.objects.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).filter(
            supplier_id=supplier_id, id__in=self.request.session.get('pay_supplier_invoices', [])
        )

    def get_payments(self, supplier_id):
        return Payment.objects.select_related('supplier').filter(
            supplier_id=supplier_id, id__in=self.request.session.get('selected_payments', []))

    def get_ledgers(self, supplier_id):
        return SupplierLedger.objects.with_available_amount().select_related('supplier').filter(
            supplier_id=supplier_id, id__in=self.request.session.get('selected_ledgers', [])
        )

    # noinspection PyMethodMayBeStatic
    def get_supplier_invoices(self, supplier_id, invoices, payments, ledgers):
        supplier_payments = {'total_due': 0, 'total_discount': 0, 'invoices': [], 'payments': [], 'ledgers': {}}
        total = 0
        total_due = 0
        for invoice in invoices.all():
            total += invoice.open_amount
            total_due += invoice.open_amount
            supplier_payments['total_discount'] += invoice.supplier_discount
            supplier_payments['invoices'].append(invoice)

        for payment in payments.all():
            if total_due > payment.balance:
                total_due = total_due - payment.balance
            else:
                total_due = 0
            # total += payment.display_available_amount
            # total_due += payment.display_available_amount`
            supplier_payments['payments'].append(payment)
        for ledger in ledgers.all():
            total_due += ledger.balance
            supplier_payments['ledgers'][ledger] = ledger.balance
        supplier_payments['total'] = total_due
        return supplier_payments

    def get_supplier(self):
        return Supplier.objects.get(pk=self.kwargs['supplier_id'])

    def get_form_kwargs(self):
        kwargs = super(CreateSupplierPaymentView, self).get_form_kwargs()
        invoices = self.get_invoices(self.get_supplier().id)
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        kwargs['can_edit'] = invoices.count() == 1
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateSupplierPaymentView, self).get_context_data(**kwargs)
        supplier = self.get_supplier()
        payments = self.get_payments(supplier.id)
        invoices = self.get_invoices(supplier.id)
        ledgers = self.get_ledgers(supplier.id)
        supplier_invoices = self.get_supplier_invoices(supplier.id, invoices, payments, ledgers)
        context['invoice_id'] = None
        context['supplier'] = supplier
        context['edit_amount'] = invoices.count() == 1
        context['supplier_invoices'] = supplier_invoices
        context['has_unallocated_payments'] = 0
        return context

    # noinspection PyMethodMayBeStatic
    def get_invoice_statuses(self):
        invoice_statuses = {'paid_status': get_object_or_None(Status, slug='paid'),
                            'paid_not_posted_status': get_object_or_None(Status, slug='paid-not-account-posted'),
                            'paid_not_printed_status': get_object_or_None(Status, slug='paid-not-printed')
                            }
        return invoice_statuses

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Supplier payment could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateSupplierPaymentView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    payment = form.save(commit=False)
                    invoice = None
                    if self.request.POST.get('is_invoice_payment') == '1':
                        invoice = self.get_invoices(self.kwargs['supplier_id'])[0]

                    if 'pay_supplier_invoices' in self.request.session:
                        del self.request.session['pay_supplier_invoices']
                    if 'selected_payments' in self.request.session:
                        del self.request.session['selected_payments']
                    if 'selected_ledgers' in self.request.session:
                        del self.request.session['selected_ledgers']
                    if invoice:
                        return JsonResponse({'error': False,
                                             'text': 'Payment successfully saved',
                                             'redirect': reverse('invoice_detail', kwargs={'pk': invoice.id})
                                             })
                    else:
                        return JsonResponse({'error': False,
                                             'text': 'Payment successfully saved',
                                             'redirect': reverse('payment_advice', kwargs={'payment_id': payment.id})
                                             })
            except (InvalidPeriodAccountingDateException, PaymentException) as err:
                logger.exception(err)
                return JsonResponse({'error': True,
                                     'text': str(err),
                                     'details': f'Error occurred {err}',
                                     })
        return super(CreateSupplierPaymentView, self).form_valid(form)


class UpdatePaymentOfInvoicesView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = UpdatePaymentForm
    template_name = 'payment/update_create.html'

    # noinspection PyMethodMayBeStatic
    def get_payments(self, company, period):
        return Payment.objects.prefetch_related('invoices', 'invoices__invoice__status').filter(
            period=period, company=company
        ).exclude(
            journal__isnull=False
        )

    def get_initial(self):
        initial = super(UpdatePaymentOfInvoicesView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(UpdatePaymentOfInvoicesView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the invoice updates, please fix the errors.',
                'errors': form.errors
            })
        return super(UpdatePaymentOfInvoicesView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            try:
                with transaction.atomic():
                    form.save()

                    return JsonResponse({'error': False,
                                         'text': 'Supplier payment saved successfully',
                                         'redirect': reverse('journal_for_payment_invoice')
                                         })
            except PaymentException as exception:
                logger.exception(exception)
                return JsonResponse({'error': True,
                                     'text': str(exception),
                                     'details': f"Exception {exception}"
                                     })
            except JournalNotBalancingException as exception:
                logger.exception(exception)
                return JsonResponse({'error': True,
                                     'text': str(exception),
                                     'details': f"Exception {exception}"
                                     })
        return super(UpdatePaymentOfInvoicesView, self).form_valid(form)


class JournalForPaymentInvoiceView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'payment/update_index.html'
    model = Journal
    paginate_by = 30
    context_object_name = 'journals'

    def get_periods(self, company, year):
        return Period.objects.open(company, year).order_by('-period')

    def get_queryset(self):
        return Journal.objects.payments(company=self.get_company(), year=self.get_year())

    def get_context_data(self, **kwargs):
        context = super(JournalForPaymentInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['year'] = year
        context['company'] = company
        context['periods'] = self.get_periods(company, year)
        return context


class PaymentInvoiceJournalView(LoginRequiredMixin, ProfileMixin, DetailView):
    model = Journal
    template_name = 'payment/update_detail.html'

    def get_object(self, queryset=None):
        return Journal.objects.select_related(
            'company', 'role', 'ledger', 'company__default_supplier_account',
            'company__default_expenditure_account',
            'company__default_vat_account', 'company__default_discount_received_account',
        ).get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(PaymentInvoiceJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        journal = self.get_object()

        context['company'] = company
        context['journal'] = journal

        journal_report = PaymentJournal(journal=journal)
        journal_report.process_report()

        role_profiles = journal.role.get_profiles()

        remittance = journal_report.calculate_remmitance()

        context['year'] = year
        context['remmitance'] = remittance
        context['role_profiles'] = role_profiles
        context['journal_report'] = journal_report
        return context


class SupplierPaymentMixin(object):

    # noinspection PyMethodMayBeStatic
    def get_supplier_invoices(self, invoices=None, payments=None, ledgers=None):
        payments = payments or []
        invoices = invoices or []
        ledgers = ledgers or []
        supplier_invoices = OrderedDict()
        for invoice in invoices:
            if invoice.supplier in supplier_invoices:
                supplier_invoices[invoice.supplier]['total'] += invoice.open_amount
                supplier_invoices[invoice.supplier]['total_discount'] += invoice.supplier_discount
                supplier_invoices[invoice.supplier]['invoices'].append(invoice)
            else:
                supplier_invoices[invoice.supplier] = {'invoices': [invoice],
                                                       'total': invoice.open_amount,
                                                       'total_discount': invoice.supplier_discount,
                                                       'payments': [],
                                                       'ledgers': [],
                                                       'total_payment': 0,
                                                       'total_payment_discount': 0
                                                       }
        for payment in payments:
            if payment.supplier in supplier_invoices:
                supplier_invoices[payment.supplier]['payments'].append(payment)
                if payment.total_discount:
                    supplier_invoices[payment.supplier]['total_payment_discount'] += payment.total_discount
                supplier_invoices[payment.supplier]['total_payment'] += payment.unpaid_amount
            else:
                supplier_invoices[payment.supplier] = {'total': 0,
                                                       'total_discount': 0,
                                                       'payments': [payment],
                                                       'invoices': [],
                                                       'ledgers': [],
                                                       'total_payment': payment.unpaid_amount,
                                                       'total_payment_discount': payment.total_discount
                                                       }
        for ledger in ledgers:
            if ledger.supplier in supplier_invoices:
                supplier_invoices[ledger.supplier]['ledgers'].append(ledger)
                supplier_invoices[ledger.supplier]['total_payment'] += ledger.balance
            else:
                supplier_invoices[ledger.supplier] = {'total': 0,
                                                      'total_discount': 0,
                                                      'ledgers': [ledger],
                                                      'invoices': [],
                                                      'payments': [],
                                                      'total_payment': ledger.balance
                                                      }
        return supplier_invoices


class UnpaidInvoicesView(LoginRequiredMixin, SupplierPaymentMixin, ProfileMixin, FormView):
    form_class = PaymentFilterForm
    template_name = 'payment/pay_suppliers.html'

    def get_context_data(self, **kwargs):
        context = super(UnpaidInvoicesView, self).get_context_data(**kwargs)
        company = self.get_company()
        self.request.session['pay_supplier_invoices'] = ''
        context['company'] = company
        context['year'] = self.get_year()
        return context


class GenerateUnpaidInvoicesView(LoginRequiredMixin, SupplierPaymentMixin, ProfileMixin, TemplateView):
    filterset_class = UnpaidInvoiceFilter
    template_name = 'payment/unpaid_invoices.html'

    # noinspection PyMethodMayBeStatic
    def get_invoices(self, company, supplier_id, due_date):
        return Invoice.objects.due_for_payments(company, supplier_id=supplier_id, end_date=due_date)

    # noinspection PyMethodMayBeStatic
    def get_available_payments(self, company, supplier_id, end_date):
        return Payment.objects.due_for_payment(company, supplier_id=supplier_id, end_date=end_date)

    # noinspection PyMethodMayBeStatic
    def get_available_ledgers(self, company, supplier_id, end_date):
        return SupplierLedger.objects.with_available_payments(company=company, supplier_id=supplier_id).select_related(
            'journal_line', 'journal_line__journal', 'supplier', 'period'
        )

    def get_context_data(self, **kwargs):
        context = super(GenerateUnpaidInvoicesView, self).get_context_data(**kwargs)
        # TODO - Compare this to old one and sync with the supplier balance view. Ensure they work the same, giving same balances
        company = self.get_company()
        supplier_id = self.request.GET.get('supplier_id')
        end_date = self.request.GET.get('due_date')
        invoices = self.get_invoices(company=company, supplier_id=supplier_id, due_date=end_date)
        payments = self.get_available_payments(company, supplier_id, end_date)
        ledgers = self.get_available_ledgers(company, supplier_id, end_date)
        self.request.session['pay_supplier_invoices'] = ''
        context['company'] = company
        context['supplier_invoices'] = self.get_supplier_invoices(invoices, payments, ledgers)
        context['year'] = self.get_year()
        return context


class SundryPaymentView(LoginRequiredMixin, SupplierPaymentMixin, ProfileMixin, FormView):
    template_name = 'payment/sundry.html'
    form_class = SundryPaymentForm

    def get_initial(self):
        initial = super(SundryPaymentView, self).get_initial()
        initial['payment_date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(SundryPaymentView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred creating the sundry payment, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(SundryPaymentView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                form.save(commit=False)

                return JsonResponse({'error': False,
                                     'text': 'Sundry payment successfully saved',
                                     'reload': True
                                     })
            except NotAllocatedAmountException as ex:
                logger.exception(ex)
                return JsonResponse({'text': str(ex),
                                     'details': f'Error occurred {str(ex)}',
                                     'warning': True,
                                     'url': reverse('sundry_payment_not_allocated')
                                     })
            except InvalidPeriodAccountingDateException as ex:
                logger.exception(ex)
                return JsonResponse({'error': True,
                                     'text': str(ex),
                                     'details': f'Error occurred {str(ex)}',
                                     'errors': {'period': ['Period and payment date do not match']}
                                     })
        return HttpResponse('Not allowed')


class SundryPaymentInvoicesView(SupplierPaymentMixin, ProfileMixin, TemplateView):
    template_name = 'payment/sundry_invoices.html'

    def get_invoices(self, company):
        filter_dict = {'company': company, 'invoice_type__is_account_posting': True}
        if 'supplier_id' in self.request.GET and self.request.GET['supplier_id']:
            filter_dict['supplier_id'] = self.request.GET['supplier_id']

        if 'due_date' in self.request.GET and self.request.GET['due_date']:
            filter_dict['due_date__lt'] = self.request.GET['due_date']
        invoices = Invoice.objects.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).prefetch_related(
            'invoice_journals', 'payments', 'invoice_journals__journal'
        ).filter(
            **filter_dict
        ).exclude(open_amount=0).order_by('supplier__name')
        return invoices

    # noinspection PyMethodMayBeStatic
    def calculate_totals(self, invoices, payments):
        total = 0
        total_open = 0
        new_open = 0
        for invoice in invoices:
            total += invoice.total_amount
            total_open += invoice.open_amount
            # new_open += invoice.open_amount
        for payment in payments:
            new_open += payment.balance * -1
        return total, total_open, new_open

    def get_available_payments(self, company):
        filter_dict = {'company': company}
        if 'supplier_id' in self.request.GET and self.request.GET['supplier_id']:
            filter_dict['supplier_id'] = self.request.GET['supplier_id']
        payments = Payment.objects.select_related(
            'company', 'supplier'
        ).prefetch_related('invoices').filter(**filter_dict).order_by('created_at')

        available_payments = []

        for payment in payments:
            if payment.available_amount > 0:
                available_payments.append(payment)
        return available_payments

    def get_context_data(self, **kwargs):
        context = super(SundryPaymentInvoicesView, self).get_context_data(**kwargs)
        company = self.get_company()
        invoices = self.get_invoices(company)
        payments = self.get_available_payments(company)
        total, total_open, new_open = self.calculate_totals(invoices, payments)
        context['company'] = company
        context['payment_methods'] = PaymentMethod.objects.filter(company=company)
        context['invoices'] = invoices
        context['payments'] = payments
        context['total'] = total
        context['total_open'] = total_open
        context['new_open'] = new_open
        return context


class SundryPaymentAmountView(TemplateView):
    template_name = 'payment/sundry_amount.html'

    def get_payment_amount(self):
        amount = self.request.GET.get('payment_amount', 0)
        payment_amount = Decimal('0')
        if amount:
            payment_amount = Decimal(amount)
        return payment_amount

    def get_context_data(self, **kwargs):
        context = super(SundryPaymentAmountView, self).get_context_data(**kwargs)
        total_amount = self.get_payment_amount()
        context['payment_amount'] = total_amount
        context['available_payment_amount'] = total_amount * -1
        context['allocated'] = 0
        return context


class SundryPaymentNotAllocatedView(FormView):
    template_name = 'payment/unallocated.html'

    def get_initial(self):
        initial = super(SundryPaymentNotAllocatedView, self).get_initial()
        return initial

    def get_form_class(self):
        return CommentForm

    def get_form_kwargs(self):
        kwargs = super(SundryPaymentNotAllocatedView, self).get_form_kwargs()
        return kwargs


class AllocateSundryPaymentView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'payment/allocate_sundry.html'
    form_class = AllocateSundryPaymentForm

    def get_initial(self):
        initial = super(AllocateSundryPaymentView, self).get_initial()
        initial['payment_date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(AllocateSundryPaymentView, self).get_form_kwargs()
        company = self.get_company()
        year = self.get_year()
        kwargs['company'] = company
        kwargs['year'] = year
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AllocateSundryPaymentView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred creating the sundry payment, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(AllocateSundryPaymentView, self).form_invalid(form)

    def monitize(self, string):
        return Decimal(string.replace(',', ''))

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                company = self.get_company()
                role_id = self.request.session['role']
                profile_id = self.request.session['profile']
                allocate_sundry = AllocateSundryPayment(company, role_id, profile_id, form, self.request.POST)
                parent_payment = allocate_sundry.execute()
                kwargs = {'payment_id': parent_payment.id}
                redirect_url = "{}?redirect_url={}".format(reverse('payment_advice', kwargs=kwargs),
                                                           reverse('sundry_payment'))
                return JsonResponse({'error': False,
                                     'text': 'Sundry payment allocated successfully',
                                     'redirect': redirect_url
                                     })
            except NotAllocatedAmountException as ex:
                return JsonResponse({'error': True,
                                     'text': ex.__str__(),
                                     'details': 'Error occurred {}'.format(ex.__str__()),
                                     'url': reverse('sundry_payment_not_allocated')
                                     })
            except InvalidPeriodAccountingDateException as ex:
                return JsonResponse({'error': True,
                                     'text': ex.__str__(),
                                     'details': 'Error occurred {}'.format(ex.__str__()),
                                     'errors': {'period': ['Period and payment date do not match']}
                                     })
        # return HttpResponse('Not allowed')


class AllocateSundryPaymentInvoicesView(SupplierPaymentMixin, ProfileMixin, TemplateView):
    template_name = 'payment/allocate_sundry_invoices.html'

    def get_filter_options(self):
        filters = {}
        supplier_id = self.request.GET.get('supplier_id', None)
        if supplier_id:
            filters['supplier_id'] = supplier_id
        return filters

    def get_invoices(self, company):
        invoice_filters = self.get_filter_options()
        invoice_filters['company'] = company
        due_date = self.request.GET.get('due_date', None)
        if due_date:
            invoice_filters['due_date__lt'] = due_date

        invoices = Invoice.objects.open_for_payments(invoice_filters)
        return invoices

    def calculate_totals(self, invoices, payments, ledger_payments):
        total = 0
        total_open = 0
        total_new_open = 0
        allocated_total = 0
        for invoice in invoices:
            total += invoice.total_amount
            total_open += invoice.open_amount
            total_new_open += invoice.open_amount
        for payment in payments:
            total += payment.balance
            total_open += payment.amount_available
        for ledger_payment in ledger_payments:
            total += ledger_payment.balance
            total_open += ledger_payment.available_amount
        return total, total_open, total_new_open, allocated_total

    def get_available_payments(self, company):
        payment_filters = self.get_filter_options()
        payment_filters['company'] = company
        return Payment.objects.with_available_amount(payment_filters).all()

    def get_ledger_payments(self, company):
        journal_filters = self.get_filter_options()
        general_ledgers = SupplierLedger.objects.with_available_payments(company=company, **journal_filters)
        return general_ledgers

    def get_context_data(self, **kwargs):
        context = super(AllocateSundryPaymentInvoicesView, self).get_context_data(**kwargs)
        company = self.get_company()

        payments = self.get_available_payments(company)
        ledger_payments = self.get_ledger_payments(company)
        invoices = self.get_invoices(company)

        total, total_open, total_new_open, allocated_total = self.calculate_totals(invoices, payments, ledger_payments)
        context['company'] = company
        context['payment_methods'] = PaymentMethod.objects.filter(company=company)
        context['invoices'] = invoices
        context['payments'] = payments
        context['ledger_payments'] = ledger_payments
        context['total'] = total
        context['total_open'] = total_open
        context['total_new_open'] = total_new_open
        context['allocated_total'] = allocated_total
        return context


class PayInvoicesView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        supplier_id = self.request.GET.get('parent_id', None)
        if not supplier_id:
            supplier_id = self.request.GET.get('supplier_id', None)
        if supplier_id:
            invoice_id = self.request.GET.get('invoice_id', None)
            if invoice_id:
                redirect_url = reverse('record_invoice_payment', kwargs={
                    'supplier_id': supplier_id,
                    'invoice_id': invoice_id
                })
                self.request.session['pay_supplier_invoices'] = [invoice_id]
                self.request.session['is_invoice_payment'] = True
            else:
                selected_invoices = self.request.GET.get('invoices', None)
                selected_payments = self.request.GET.get('payments', None)
                ledgers_payments = self.request.GET.get('ledgers', None)
                if selected_payments:
                    self.request.session['selected_payments'] = [payment_id
                                                                 for payment_id in selected_payments.split(',')
                                                                 if len(payment_id) > 0
                                                                 ]
                else:
                    self.request.session['selected_payments'] = []
                if ledgers_payments:
                    self.request.session['selected_ledgers'] = [ledger_id
                                                                for ledger_id in ledgers_payments.split(',')
                                                                if len(ledger_id) > 0
                                                                ]
                else:
                    self.request.session['selected_ledgers'] = []

                self.request.session['pay_supplier_invoices'] = [invoice_id
                                                                 for invoice_id in selected_invoices.split(',')
                                                                 if len(invoice_id) > 0
                                                                 ]
                self.request.session['is_supplier_payment'] = True
                if 'is_invoice_payment' in self.request.session:
                    del self.request.session['is_invoice_payment']
            create_payment_url = reverse('create_supplier_payment', kwargs={'supplier_id': supplier_id})
            return JsonResponse({'text': 'Paying invoice ....',
                                 'error': False,
                                 'create_payment': create_payment_url
                                 })
        else:
            return JsonResponse({'text': 'No supplier selected for payment',
                                 'error': True
                                 })


class RecordPaymentView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = 'payment/create.html'
    form_class = PaymentForm
    model = Payment

    def get_default_vat_code(self):
        return VatCode.objects.filter(company=self.get_company(), is_default=True).first()

    def get_initial(self):
        initial = super(RecordPaymentView, self).get_initial()
        initial['payment_date'] = datetime.now().strftime('%Y-%m-%d')
        vat_code = self.get_default_vat_code()
        if vat_code:
            initial['vat_code'] = vat_code
        return initial

    def get_selected_invoices(self):
        invoice_ids = self.request.session.get('pay_supplier_invoices', [])
        if len(invoice_ids) > 0:
            return Invoice.objects.select_related(
                'invoice_type', 'company', 'status', 'supplier', 'currency'
            ).filter(
                supplier_id=self.kwargs['supplier_id'], id__in=invoice_ids
            )
        return []

    def get_supplier_payments(self):
        payment_ids = self.request.session.get('selected_payments', [])
        if len(payment_ids) > 0:
            return Payment.objects.select_related(
                'company', 'supplier'
            ).filter(
                supplier_id=self.kwargs['supplier_id'], id__in=payment_ids
            )
        return []

    def get_supplier_documents(self):
        invoices = self.get_selected_invoices()
        payments = self.get_supplier_payments()
        supplier_payments = {'total': 0, 'total_discount': 0, 'invoices': [], 'payments': []}
        for invoice in invoices:
            supplier_payments['total'] += invoice.open_amount
            supplier_payments['total_discount'] += invoice.supplier_discount
            supplier_payments['invoices'].append(invoice)

        for payment in payments:
            supplier_payments['total'] += payment.display_available_amount
            supplier_payments['payments'].append(payment)
        return supplier_payments

    def get_supplier(self):
        return Supplier.objects.get(pk=int(self.kwargs['supplier_id']))

    def get_form_kwargs(self):
        kwargs = super(RecordPaymentView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['supplier'] = self.get_supplier()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(RecordPaymentView, self).get_context_data(**kwargs)
        supplier = self.get_supplier()
        company = self.get_company()
        is_invoice_payment = self.request.session.get('is_invoice_payment', False)
        context['invoice_id'] = self.kwargs.get('invoice_id', None)
        context['supplier'] = supplier
        context['edit_amount'] = False if is_invoice_payment else True
        context['is_invoice_payment'] = is_invoice_payment
        context['supplier_invoices'] = self.get_supplier_documents()
        context['has_unallocated_payments'] = Payment.objects.available(company, supplier).count() if is_invoice_payment else 0
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Supplier payment could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(RecordPaymentView, self).form_invalid(form)

    def get_invoice_statuses(self):
        invoice_statuses = {'paid_status': get_object_or_None(Status, slug='paid'),
                            'paid_not_posted_status': get_object_or_None(Status, slug='paid-not-account-posted'),
                            'paid_not_printed_status': get_object_or_None(Status, slug='paid-not-printed')
                            }
        return invoice_statuses

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                with transaction.atomic():
                    payment = form.save()

                invoice__id = self.kwargs.get('invoice_id', None)
                if invoice__id:
                    return JsonResponse({'error': False,
                                         'text': 'Payment successfully saved',
                                         'redirect': reverse('invoice_detail', kwargs={'pk': invoice__id})
                                         })
                else:
                    return JsonResponse({'error': False,
                                         'text': 'Payment successfully saved',
                                         'redirect': reverse('payment_advice', kwargs={'payment_id': payment.id})
                                         })
            except InvalidPeriodAccountingDateException as ex:
                logger.exception(ex)
                return JsonResponse({'error': True,
                                     'text': str(ex),
                                     'details': 'Error occurred {}'.format(ex.__str__()),
                                     })
            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'error': True,
                                     'text': 'Unexpected error occurred',
                                     'details': 'Error occurred {}'.format(exception),
                                     })
        else:
            return HttpResponseRedirect(reverse('invoice_detail', kwargs={'pk': self.kwargs['pk']}))


class PaymentAdviceView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = 'payment/detail.html'
    form_class = PaymentProofOfPaymentForm
    model = Payment

    def get_template_names(self):
        if self.request.GET.get('print', '') == '1':
            return 'payment/print.html'
        else:
            return 'payment/detail.html'

    def get_object(self, queryset=None):
        return Payment.objects.select_related(
            'company', 'supplier', 'payment_method', 'profile', 'role', 'period', 'period__period_year', 'account'
        ).prefetch_related(
            'children', 'invoices', 'supplier_ledgers'
        ).get(
            id=self.kwargs['payment_id']
        )

    def get_context_data(self, **kwargs):
        context = super(PaymentAdviceView, self).get_context_data(**kwargs)
        payment_report = PaymentReport(payment=self.object)
        payment_report.process_report()
        context['request_url'] = reverse('payment_advice', kwargs={'payment_id': self.kwargs['payment_id']})
        context['upload_url'] = settings.MEDIA_URL
        context['payment_report'] = payment_report
        context['is_print'] = self.request.GET.get('print', '') == '1'
        context['ledger'] = JournalLine.objects.filter(
            object_id=payment_report.payment.pk, content_type=ContentType.objects.get_for_model(payment_report.payment)
        ).first()
        return context

    def form_invalid(self, form):
        messages.error(self.request, f'Payment could not be saved, please fix the errors. {form.errors}')
        return super(PaymentAdviceView, self).form_invalid(form)

    def form_valid(self, form):
        payment = form.save()
        if payment.attachment:
            messages.success(self.request, 'Proof of payment successfully saved')
        else:
            messages.error(self.request, 'Proof of payment not found')
        return HttpResponseRedirect(reverse('unpaid_invoice_by_supplier'))


class EmailSupplierPaymentView(LoginRequiredMixin, View):

    def get_payment(self):
        return Payment.objects.select_related(
            'company', 'supplier', 'payment_method', 'profile', 'role', 'period', 'period__period_year', 'account'
        ).prefetch_related(
            'children', 'invoices', 'supplier_ledgers'
        ).get(
            id=self.kwargs['payment_id']
        )

    def get(self, request, *args, **kwargs):
        payment = self.get_payment()
        payment_report = PaymentReport(payment=payment)
        payment_report.process_report()

        if not payment.supplier.email_address:
            return JsonResponse({
                'text': 'Supplier email address not found',
                'error': True
            })

        subject = f"Payment Record/Remittance Advice {str(payment)}"
        recipient_list = [payment.supplier.email_address]

        template = get_template('payment/email.html')
        message = template.render({
            'payment_report': payment_report,
            'company': payment.company,
            'base_url': settings.BASE_URL
        })
        logger.info(f"Sending email to {recipient_list} bcc = {settings.ADMIN_EMAIL}")
        email = EmailMessage(
            subject=subject,
            body=message,
            from_email='info@docuflow.co.za',
            to=recipient_list,
            # bcc=settings.ADMIN_EMAIL,
            reply_to=['info@docuflow.co.za']
        )
        email.content_subtype = "html"
        if payment.attachment and os.path.exists(payment.attachment.path):
            logger.info(f"Payment has file located at {payment.attachment.path}")
            email.attach_file(payment.attachment.path)
        email.send()
        return JsonResponse({'error': False,
                             'text': 'Supplier email send successfully.'
                             })


class SavePaymentView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            return JsonResponse({'error': False,
                                 'text': 'Payment saved successfully.',
                                 'redirect': reverse('unpaid_invoice_by_supplier')
                                 })
        except Exception as exception:
            return JsonResponse({'error': True,
                                 'text': 'Error saving payment.',
                                 'detail': 'Exception: {}'.format(exception)
                                 })


class EnterPaymentView(LoginRequiredMixin, ProfileMixin, FilterView):
    template_name = 'payment/create.html'
    filter_class = UnpaidInvoiceFilter

    def get_invoices(self, company):
        statuses = ['final-signed', 'end-flow-printed']
        return Invoice.objects.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).filter(company=company, status__slug__in=statuses)

    def prepare_invoice(self, invoice):

        if invoice.is_credit_type:
            amount = invoice.total_amount * -1
        else:
            amount = invoice.total_amount

        inv = {'invoice_type': invoice.invoice_type,
               'unique_number': invoice,
               'id': invoice.id,
               'invoice_id': invoice.invoice_id,
               'invoice_date': invoice.invoice_date,
               'due_date': invoice.due_date,
               'amount': amount
               }

        return inv

    def get_supplier_invoices(self, invoices):
        supplier_invoices = {}
        for invoice in invoices.queryset.all():
            if invoice.supplier_id in supplier_invoices:
                _invoice = self.prepare_invoice(invoice)
                supplier_invoices[invoice.supplier_id]['total'] += _invoice['amount']
                supplier_invoices[invoice.supplier_id]['invoices'].append(_invoice)
            else:
                _invoice = self.prepare_invoice(invoice)
                supplier_invoices[invoice.supplier_id] = {'supplier': invoice.supplier,
                                                          'invoices': [_invoice],
                                                          'total': _invoice['amount']
                                                          }
        return supplier_invoices

    def get_context_data(self, **kwargs):
        context = super(EnterPaymentView, self).get_context_data(**kwargs)
        company = self.get_company()
        invoices = self.get_invoices(company)
        context['company'] = company
        context['payment_methods'] = PaymentMethod.objects.filter(company=company)
        context['supplier_invoices'] = self.get_supplier_invoices(invoices)
        return context


class SavePaymentProofOfPaymentView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):

        return JsonResponse({
            'text': 'Proof of payment uploaded successfully',
            'error': False
        })


class SupplierPaymentReportView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'payment/report.html'
    context_object_name = 'payments'
    model = Payment
    paginate_by = 30

    def get_queryset(self):
        return Payment.objects.select_related(
            'company', 'supplier', 'payment_method', 'period', 'period__period_year', 'account', 'journal'
        ).filter(
            company=self.get_company(),
            period__period_year=self.get_year()
        ).order_by(
            '-payment_date', 'supplier__name'
        )

    def get_context_data(self, **kwargs):
        context = super(SupplierPaymentReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['year'] = year
        context['company'] = company
        return context


class SupplierPaymentListView(ProfileMixin, View):

    def get_payments(self, filter_objects):
        return Payment.objects.filter(**filter_objects)

    def get(self, request, **kwargs):
        year = self.get_year()
        company = self.get_company()
        filters = {'company': company, 'period__period_year': year}
        payment_query = self.get_payments(filters)
        payments = []
        for payment in payment_query:
            payments.append({
                'remittance_number': str(payment),
                'date_paid': payment.payment_date,
                'supplier': str(payment.supplier),
                'total_amount': payment.total_amount,
                'payment_method': str(payment.payment_method),
                'period': str(payment.period),
                'updated': payment.is_open
            })
        return JsonResponse({'records': payments, 'queryRecordCount': payment_query.count(),
                             'totalRecordCount': payment_query.count()
                             })


class EditPaymentView(LoginRequiredMixin, ProfileMixin, UpdateView):
    model = Payment
    form_class = PaymentForm
    template_name = 'payment/edit.html'

    def get_form_kwargs(self):
        kwargs = super(EditPaymentView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['can_edit'] = self.get_object().is_open
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditPaymentView, self).get_context_data(**kwargs)

        payment_report = PaymentReport(payment=self.get_object())
        payment_report.process_report()
        context['upload_url'] = settings.MEDIA_URL
        context['payment_report'] = payment_report
        return context

    def form_invalid(self, form):
        messages.error(self.request, f'Payment could not be saved, please fix the errors. {form.errors}')
        return super(EditPaymentView, self).form_invalid(form)

    def form_valid(self, form):
        payment = form.save(commit=False)
        payment.save()
        messages.success(self.request, 'Payment updated successfully')
        return HttpResponseRedirect(reverse('payment_index'))


class DeletePaymentView(LoginRequiredMixin, DeleteView):
    template_name = 'payment/confirm_delete.html'
    model = Payment
    success_url = reverse_lazy('payment_index')

    def delete(self, request, *args, **kwargs):
        try:
            payment = Payment.objects.prefetch_related('invoices', 'children').get(pk=self.kwargs['pk'])
            delete_payment(payment)
            return JsonResponse({
                'text': 'Payment deleted successfully',
                'error': False,
                'reload': True
            })
        except Payment.DoesNotExist as e:
            return JsonResponse({
                'text': 'Payment does not exist',
                'error': True,
                'reload': True
            })


class InvoicePaymentView(TemplateView):
    template_name = 'payment/invoice_payment.html'

    def get_invoice(self):
        return Invoice.objects.prefetch_related('payments').filter(id=self.kwargs['invoice_id']).first()

    def get_invoice_payments(self):
        return InvoicePayment.objects.select_related('invoice').filter(invoice_id=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(InvoicePaymentView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['invoice_payments'] = invoice.payments.all()
        return context


class PaymentListView(ProfileMixin, View):

    def get(self):

        return Payment.objects.select_related(
            'supplier'
        ).filter(
            company=self.get_company()
        )

