from rest_framework import generics
from django_filters import rest_framework as filters


from docuflow.apps.invoice.models import Payment

from .serializers import PaymentSerializer

from .filters import PaymentFilter


class ApiIndexView(generics.ListAPIView):
    serializer_class = PaymentSerializer
    filterset_class = PaymentFilter

    def get_queryset(self):
        return Payment.objects.select_related(
            'company',
            'supplier',
            'period'
        ).order_by(
            '-payment_date'
        )


class ApiPaymentView(generics.GenericAPIView):

    serializer_class = PaymentSerializer

    def get_queryset(self):
        return Payment.objects.select_related(
            'company',
            'supplier',
            'period'
        ).order_by(
            '-payment_date'
        )
