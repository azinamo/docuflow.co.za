from rest_framework import serializers

from docuflow.apps.invoice.models import InvoiceType


class InvoiceTypeSerializer:

    class Meta:
        model = InvoiceType
        fields = '__all__'
