from rest_framework import generics

from docuflow.apps.invoice.models import InvoiceType

from .serializers import InvoiceTypeSerializer


class ApiInvoiceTypeIndexView(generics.ListAPIView):

    serializer_class = InvoiceTypeSerializer

    queryset = InvoiceType.objects.all()
