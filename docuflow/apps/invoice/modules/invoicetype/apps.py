from django.apps import AppConfig


class InvoicetypeConfig(AppConfig):
    name = 'docuflow.apps.invoice.modules.invoicetype'
