from django.urls import path

from . import views
from . import api_views

urlpatterns = [
    path('', views.InvoiceTypeView.as_view(), name='invoice_types'),
    path('create/', views.CreateInvoiceTypeView.as_view(), name='create_invoice_type'),
    path('<str:module_type>/create/', views.CreateModuleInvoiceTypeView.as_view(), name='create_module_invoice_type'),
    path('edit/<int:pk>/', views.EditInvoiceTypeView.as_view(), name='edit-invoice-type'),
    path('delete/<int:pk>/', views.DeleteInvoiceTypeView.as_view(), name='delete_invoice_type'),
    path('default/<int:pk>/', views.UpdateDefaultInvoiceTypeView.as_view(), name='update_default_document_type'),
    path('module/<str:module_type>/', views.ModuleInvoiceType.as_view(), name='module_invoice_type'),

]

urlpatterns += [
    path('api/', api_views.ApiInvoiceTypeIndexView.as_view(), name='api_invoice_types'),
]
