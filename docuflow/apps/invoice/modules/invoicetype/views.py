import pprint

from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as __
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.invoice.forms import InvoiceTypeForm
from docuflow.apps.invoice.models import InvoiceType

pp = pprint.PrettyPrinter(indent=4)


class InvoiceTypeView(LoginRequiredMixin, ProfileMixin, ListView):
    model = InvoiceType
    context_object_name = 'types'
    template_name = 'types/index.html'

    def get_queryset(self):
        return InvoiceType.objects.select_related('file').filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(InvoiceTypeView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class ModuleInvoiceType(LoginRequiredMixin, ListView):
    template_name = 'types/module_invoice_types.html'
    model = InvoiceType
    context_object_name = 'types'

    def get_queryset(self):
        company_id = self.request.session['company']
        filters = {}
        module = self.kwargs['module_type']
        if module == 'purchase':
            filters['file__module'] = Module.PURCHASE
        elif module == 'sales':
            filters['file__module'] = Module.SALES
        elif module == 'important-documents':
            filters['file__module'] = Module.DOCUMENT
        filters['company_id'] = company_id
        return InvoiceType.objects.filter(**filters)

    def get_context_data(self, **kwargs):
        context = super(ModuleInvoiceType, self).get_context_data(**kwargs)
        context['module_type'] = self.kwargs['module_type']
        return context


class CreateInvoiceTypeView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = InvoiceType
    template_name = 'types/create.html'
    success_message = __('Document type successfully saved')
    success_url = reverse_lazy('invoice_types')
    form_class = InvoiceTypeForm

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceTypeView, self).get_context_data(**kwargs)
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceTypeView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        invoice_type = form.save(commit=False)
        invoice_type.company = self.get_company()
        invoice_type.save()
        messages.success(self.request, __('Invoice type saved successfully'))
        return HttpResponseRedirect(self.get_success_url())


class CreateModuleInvoiceTypeView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = InvoiceType
    form_class = InvoiceTypeForm
    template_name = 'types/create.html'
    success_message = __('Document type successfully saved')
    success_url = reverse_lazy('invoice_types')

    def get_context_data(self, **kwargs):
        context = super(CreateModuleInvoiceTypeView, self).get_context_data(**kwargs)
        context['module_type'] = self.kwargs['module_type']
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateModuleInvoiceTypeView, self).get_form_kwargs()
        kwargs['module_type'] = self.kwargs['module_type']
        kwargs['company'] = self.get_company()
        return kwargs


class EditInvoiceTypeView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = InvoiceType
    form_class = InvoiceTypeForm
    template_name = 'types/edit.html'
    success_message = __('Document type successfully updated')
    success_url = reverse_lazy('invoice_types')

    def get_form_kwargs(self):
        kwargs = super(EditInvoiceTypeView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['module_type'] = self.get_object().file.module
        return kwargs


class DeleteInvoiceTypeView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, View):
    model = InvoiceType
    success_message = 'Document type deleted successfully'
    success_url = reverse_lazy('invoice_types')

    def get(self, request, *args, **kwargs):
        invoice_type = get_object_or_None(InvoiceType, id=self.kwargs['pk'])
        if invoice_type:
            invoice_type.delete()
            messages.success(self.request, __('Document type deleted successfully'))

        return redirect(reverse('invoice_types'))


class UpdateDefaultInvoiceTypeView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        document_types = InvoiceType.objects.filter(company_id=self.request.session['company']).all()
        for document_type in document_types:
            if document_type.id == int(self.kwargs['pk']):
                document_type.is_default = True
            else:
                document_type.is_default = False
            document_type.save()
        return JsonResponse({
            'text': __('Document type updated successfully'),
            'error': False
        })
