import json
import pprint
from decimal import Decimal

from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect, render
from django.views.generic import ListView, FormView, View, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.http import HttpResponse, JsonResponse
from django.db import transaction
from django.db.models import Prefetch

from annoying.functions import get_object_or_None

from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.company.models import Company, Account, ObjectItem
from docuflow.apps.common.mixins import CompanyMixin, ProfileMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.invoice.services import InvoiceAccountPostings
from docuflow.apps.invoice.models import InvoiceAccount, Invoice
from docuflow.apps.invoice.exceptions import ControlledByInventoryException
from .forms import InvoiceAccountForm, InvoiceAccountCommentForm, DistributeInvoiceAccountForm
from . import services

pp = pprint.PrettyPrinter(indent=4)


class InvoiceAccountView(LoginRequiredMixin, ListView):
    template_name = 'invoiceaccount/index.html'
    model = InvoiceAccount
    context_object_name = 'accounts'

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceAccountView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['can_transition'] = invoice.can_transition(self.request.user)
        return context

    def get_queryset(self):
        invoice = self.get_invoice()
        if 'in_trash' in self.request.GET and self.request.GET['in_trash']:
            return InvoiceAccount.objects.deleted_only().filter(invoice=invoice)
        else:
            return InvoiceAccount.objects.filter(invoice=invoice)


class InvoiceAccountPostingView(LoginRequiredMixin, TemplateView):
    model = InvoiceAccount
    context_object_name = 'accounts'
    template_name = 'invoiceaccount/postings.html'

    def get_invoice(self):
        vat_account_postings = InvoiceAccount.objects.filter(is_vat=True, deleted__isnull=True).exclude(amount=0)
        account_postings = InvoiceAccount.objects.filter(is_vat=False, deleted__isnull=True).exclude(amount=0)
        return Invoice.objects.prefetch_related(
            Prefetch('invoice_accounts', queryset=vat_account_postings, to_attr='vat_accounts'),
            Prefetch('invoice_accounts', queryset=account_postings, to_attr='account_postings'),
            'invoice_accounts__account', 'invoice_accounts__vat_code', 'invoice_accounts__profile',
            'invoice_accounts__role', 'invoice_accounts__role', 'invoice_accounts__object_items',
            'invoice_accounts__object_postings'
        ).filter(pk=self.kwargs['invoice_id']).first()

    @staticmethod
    def get_company_linked_modes(company):
        linked_models = {}
        count = 0
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            if count < 2:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
                    company_object_id=company_object.id)
            count += 1
        return linked_models

    def get_role_permissions(self):
        role_permissions = dict()
        role_id = str(self.request.session['role'])
        if 'permissions' in self.request.session and role_id in self.request.session['permissions']:
            role_permissions = self.request.session['permissions'][role_id]
        return role_permissions

    def get_context_data(self, **kwargs):
        context = super(InvoiceAccountPostingView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        linked_models = self.get_company_linked_modes(invoice.company)

        role_permissions = self.get_role_permissions()

        invoice_postings = InvoiceAccountPostings(invoice=invoice, role_permissions=role_permissions)
        expenditure_postings = invoice_postings.process_postings(
            linked_models=linked_models,
            account_postings=invoice.account_postings
        )
        vat_postings = invoice_postings.process_vat_postings(
            linked_models=linked_models,
            account_postings=invoice.vat_accounts
        )

        context['total'] = expenditure_postings.total + vat_postings.total
        context['expenditure_postings'] = expenditure_postings.accounts
        context['vat_postings'] = vat_postings.accounts
        context['account_posting_variance'] = invoice_postings.expenditure_variance
        context['vat_posting_variance'] = invoice_postings.vat_variance

        can_edit = self.is_editable(invoice=invoice)
        context['can_edit'] = can_edit
        context['linked_models'] = linked_models
        context['linked_models_count'] = len(linked_models)
        context['invoice'] = invoice
        context['content_type'] = ContentType.objects.get_for_model(InvoiceAccount, False)
        return context

    def is_editable(self, invoice):
        # editable = self.request.session.get('can_edit_invoice', False)
        # if 'can_edit_invoice' in self.request.session:
        #     if invoice.id in self.request.session['can_edit_invoice']:
        if invoice.is_locked:
            return False
        return True


class InvoiceAccountPostingCommentView(LoginRequiredMixin, ProfileMixin, UpdateView):
    context_object_name = 'accounts'
    model = InvoiceAccount
    form_class = InvoiceAccountCommentForm
    template_name = 'invoiceaccount/comment.html'
    context_object_name = 'invoice_account'

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Invoice account comment could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(InvoiceAccountPostingCommentView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            invoice_account = form.save()
            invoice_account.comment = form.cleaned_data['comment']
            invoice_account.save()
            return JsonResponse({
                'error': False,
                'reload': True,
                'text': 'Invoice account comment saved successfully'
            })
        else:
            return redirect(reverse('invoice_detail', kwargs={'pk': self.kwargs['pk']}))


class CreateInvoiceAccountView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = InvoiceAccount
    template_name = 'invoiceaccount/create.html'
    success_message = 'Invoice account successfully saved'
    form_class = InvoiceAccountForm

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceAccountView, self).get_context_data(**kwargs)
        context['invoice'] = self.get_invoice()
        return context

    def get_success_url(self):
        return reverse('invoice-account-list')

    def get_initial(self):
        initial = super(CreateInvoiceAccountView, self).get_initial()
        amount = self.request.GET.get('amount', 0)
        if amount:
            initial['amount'] = amount
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceAccountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['invoice'] = self.get_invoice()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': True,
                                 'text': 'Invoice account could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateInvoiceAccountView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            with transaction.atomic():
                form.save()
            return JsonResponse({
                'error': False,
                'text': 'Invoice saved successfully'
            })
        return HttpResponse("Not allowed")


class EditInvoiceAccountView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = "invoiceaccount/edit.html"
    model = InvoiceAccount
    form_class = InvoiceAccountForm

    def get_queryset(self):
        return InvoiceAccount.objects.select_related('invoice')

    def get_form_kwargs(self):
        kwargs = super(EditInvoiceAccountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['invoice'] = self.get_object().invoice
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditInvoiceAccountView, self).get_context_data(**kwargs)
        invoice_account = self.get_object()
        context['invoice'] = invoice_account.invoice
        context['is_new'] = False
        context['invoice_account'] = invoice_account
        return context

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({
                'error': True,
                'text': 'Invoice could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(EditInvoiceAccountView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            try:
                with transaction.atomic():
                    form.save(commit=False)
                    return JsonResponse({
                        'error': False,
                        'text': 'Invoice updated successfully'
                    })
            except ControlledByInventoryException as exception:
                return JsonResponse({
                    'error': True,
                    'text': str(exception),
                    'details': str(exception),
                    'modal': True
                }, status=400)
        return HttpResponse('Not allowed')


class EditInvoiceAccountPostingView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = InvoiceAccount
    form_class = InvoiceAccountForm
    template_name = 'invoiceaccount/edit.html'
    success_message = 'Invoice account successfully updated'
    success_url = reverse_lazy('invoice-account-list')


class DeleteAccountPostingView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get_object(self):
        return get_object_or_None(InvoiceAccount, pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        invoice_account = self.get_object()
        if invoice_account:
            services.delete_invoice_account(invoice_account)
            return JsonResponse({'text': 'Account successfully deleted', 'error': False})
        else:
            return JsonResponse({'text': 'Account not found', 'error': True})


class DeleteInvoiceAccountView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = InvoiceAccount
    template_name = 'invoiceaccount/confirm_delete.html'
    success_message = 'Invoice account successfully deleted'

    def get_success_url(self):
        return reverse('invoice-account-list')


class DistributeVatInvoiceAccountVarianceView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoiceaccount/distribute_vat_variance.html"
    form_class = DistributeInvoiceAccountForm

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_initial(self):
        initial = super(DistributeVatInvoiceAccountVarianceView, self).get_initial()
        invoice = self.get_invoice()
        variance = invoice.get_posting_variance(is_vat=True)
        if variance:
            initial['amount'] = variance
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(DistributeVatInvoiceAccountVarianceView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['is_new'] = False
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Variance amount could not be distributed, please fix the errors.',
                'errors': form.errors
            })
        return super(DistributeVatInvoiceAccountVarianceView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            invoice = self.get_invoice()

            amount = Decimal(form.cleaned_data['amount'])
            # if invoice.is_valid_distribution_amount(amount, True):
            account = get_object_or_None(Account, pk=form.cleaned_data['account'])
            invoice_account = InvoiceAccount.objects.create(
                invoice=invoice,
                is_vat=True,
                amount=amount,
                percentage=InvoiceAccount.calculate_account_percentage(invoice=invoice, amount=amount, is_vat=True),
                profile=self.get_profile(),
                comment=form.cleaned_data['comment'],
                reinvoice=form.cleaned_data['reinvoice'],
                vat_line=form.cleaned_data['vat_line'],
                account=account
            )
            if 'vat_code' in form.cleaned_data and form.cleaned_data['vat_code']:
                invoice_account.vat_code_id = int(form.cleaned_data['vat_code'])
                invoice_account.save()

            if invoice_account:
                invoice_account.object_items.clear()

                items = self.request.POST.getlist('object_item')
                invoice_account.save_object_items(items)
            return JsonResponse({
                'error': False,
                'text': 'Invoice updated successfully',
            })
        return HttpResponse('Not Allowed')


class AddInvoiceAccountView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoiceaccount/form.html"
    form_class = InvoiceAccountForm

    def get_invoice(self):
        return Invoice.objects.get(pk=self.request.GET['invoice_id'])

    def get_invoice_account(self):
        return InvoiceAccount.objects.get(pk=self.kwargs['pk'])

    def get_initial(self):
        initial = super(AddInvoiceAccountView, self).get_initial()
        invoice_account = self.get_invoice_account()
        if invoice_account:
            if invoice_account.is_posted is True:
                initial['amount'] = invoice_account.variance
            else:
                initial['amount'] = invoice_account.amount
            initial['percentage'] = invoice_account.percentage
            initial['profile'] = invoice_account.profile
            initial['comment'] = invoice_account.comment
            initial['reinvoice'] = invoice_account.reinvoice
            initial['vat_line'] = invoice_account.vat_line
            initial['account'] = invoice_account.account
        return initial

    def get_form_kwargs(self):
        kwargs = super(AddInvoiceAccountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddInvoiceAccountView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['is_new'] = False
        context['invoice_account'] = self.get_invoice_account()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Invoice could not be approved, please fix the errors.',
                'errors': form.errors
            })
        return super(AddInvoiceAccountView, self).form_invalid(form)

    def calculate_variance(self, invoice, invoice_account):
        invoice_accounts = InvoiceAccount.objects.filter(invoice=invoice, invoice_account=invoice_account)
        total = 0
        for _invoice_account in invoice_accounts:
            total = _invoice_account.amount + total
        return invoice_account.amount - total

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                holding_account = self.get_invoice_account()
                if holding_account:
                    invoice = holding_account.invoice
                    invoice_data = self.request.POST
                    profile = Profile.objects.get(user=self.request.user)
                    holding_account_amount = holding_account.amount
                    if holding_account.is_posted is True:
                        holding_account_amount = holding_account.variance

                    if invoice_data['calculation_method'] == 'percentage':
                        percentage_of_amount = form.cleaned_data['percentage']
                        amount = ((percentage_of_amount/100) * holding_account_amount)

                        percentage_of_amount = (amount / holding_account.amount) * 100
                    else:
                        amount = Decimal(form.cleaned_data['amount'])
                        percentage_of_amount = (amount/holding_account.amount) * 100

                    if amount <= 0:
                        raise ValueError(f'You are posting an amount of {amount}, please adjust')
                    account = get_object_or_None(Account, pk=form.cleaned_data['account'])
                    new_invoice_account = InvoiceAccount.objects.create(
                       invoice_account=holding_account,
                       account=account,
                       invoice=invoice,
                       amount=amount,
                       percentage=percentage_of_amount,
                       profile=profile,
                       comment=form.cleaned_data['comment'],
                       reinvoice=form.cleaned_data['reinvoice'],
                       vat_line=form.cleaned_data['vat_line']
                    )
                    if 'vat_code' in form.cleaned_data and form.cleaned_data['vat_code']:
                        new_invoice_account.vat_code_id = int(form.cleaned_data['vat_code'])
                        new_invoice_account.save()

                    items = invoice_data.getlist('object_item')
                    new_invoice_account.save_object_items(items)

                    holding_account.variance = self.calculate_variance(invoice, holding_account)
                    holding_account.is_posted = True
                    holding_account.save()
                return JsonResponse({'error': False,
                                     'text': 'Invoice updated successfully'
                                     })
            except ValueError as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': f'{str(exception)}'
                                     }, status=500)
        return HttpResponse('Not allowed')


class InvoiceAccountFormView(LoginRequiredMixin, CompanyMixin, FormView):
    template_name = "invoiceaccount/invoice_account.html"

    def get_role(self):
        return Role.objects.get(pk=self.request.session['role'])

    def get_form_kwargs(self):
        kwargs = super(InvoiceAccountFormView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceAccountFormView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['counter'] = 1
        return context

    def get_form_class(self):
        return InvoiceAccountForm


class InvoiceAccountPostingFormView(LoginRequiredMixin, CompanyMixin, FormView):
    template_name = "invoiceaccount/account_posting.html"

    def get_role(self):
        return Role.objects.get(pk=self.request.session['role'])

    def get_form_kwargs(self):
        kwargs = super(InvoiceAccountPostingFormView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['role'] = self.get_role()
        # kwargs['counter'] = 1
        return kwargs

    def get_invoice(self):
        return Invoice.objects.get(pk=self.request.GET['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceAccountPostingFormView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['counter'] = 1
        context['can_transition'] = invoice.can_transition(self.request.user)
        return context

    def get_form_class(self):
        return InvoiceAccountForm


# Distribute amount from the holding account to the relevant accounts, departments and areas
class DistributeHoldingAccountView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoiceaccount/distribute.html"
    form_class = InvoiceAccountForm

    def get_invoice_account(self):
        return InvoiceAccount.objects.select_related('invoice').get(pk=self.kwargs['pk'])

    def get_initial(self):
        initial = super(DistributeHoldingAccountView, self).get_initial()
        invoice_account = self.get_invoice_account()
        if invoice_account:
            if invoice_account.is_posted is True:
                initial['amount'] = invoice_account.variance
            else:
                initial['amount'] = invoice_account.amount
            if invoice_account.account:
                initial['account'] = invoice_account.account
            if invoice_account.vat_code:
                initial['vat_code'] = invoice_account.vat_code
            initial['percentage'] = invoice_account.percentage
            initial['profile'] = invoice_account.profile
            initial['comment'] = invoice_account.comment
            initial['reinvoice'] = invoice_account.reinvoice
            if invoice_account.is_vat:
                initial['vat_line'] = True
            else:
                initial['vat_line'] = invoice_account.vat_line
        return initial

    def get_form_kwargs(self):
        kwargs = super(DistributeHoldingAccountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(DistributeHoldingAccountView, self).get_context_data(**kwargs)
        invoice_account = self.get_invoice_account()
        context['invoice'] = invoice_account.invoice
        context['is_new'] = False
        context['invoice_account'] = invoice_account
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Invoice could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(DistributeHoldingAccountView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            holding_account = self.get_invoice_account()
            if holding_account:
                invoice = holding_account.invoice

                amount = form.cleaned_data['amount']
                account = form.cleaned_data['account']
                invoice_account = InvoiceAccount.objects.create(
                   invoice_account=holding_account,
                   account=account,
                   invoice=invoice,
                   amount=amount,
                   suffix=form.cleaned_data.get('suffix', ''),
                   is_vat=holding_account.is_vat,
                   percentage=InvoiceAccount.calculate_account_percentage(
                       invoice=invoice, amount=amount, is_vat=holding_account.is_vat
                   ),
                   profile=self.get_profile(),
                   comment=form.cleaned_data['comment'],
                   reinvoice=form.cleaned_data['reinvoice'],
                   vat_line=form.cleaned_data['vat_line'],
                   vat_code=form.cleaned_data['vat_code']
                )
                if invoice_account:
                    holding_account.is_posted = True
                    holding_account.save()

                    items = self.request.POST.getlist('object_item')
                    invoice_account.save_object_items(items)
            return JsonResponse({
                'error': False,
                'text': 'Invoice updated successfully'
            })
        return HttpResponse('Not allowed')


class InvoiceSaveAccountPercentageView(LoginRequiredMixin, View):

    def get_invoice(self):
        return Invoice.objects.get(pk=self.request.GET['invoice_id'])

    def get_company_account(self):
        return Account.objects.get(pk=self.request.GET['account_id'])

    def get(self, request, *args, **kwargs):
        if is_ajax(request):
            invoice = self.get_invoice()
            account = self.get_company_account()
            invoice_data_key = 'invoice_data_{}'.format(invoice.id)

            percentage_of_amount = Decimal(float(self.request.GET['percentage_of_amount']))
            account_id = self.request.GET['account_id']
            invoice_id = self.request.GET['invoice_id']
            invoice_amount = Decimal(float(invoice.amount))

            invoice_accounts = []
            invoice_data = {}
            if percentage_of_amount > 0:

                if invoice_data_key in self.request.session:
                    invoice_data = self.request.session.pop(invoice_data_key)

                if 'outstanding' in invoice_data:
                    invoice_amount = invoice_data['outstanding']

                if 'invoice_accounts' in invoice_data:
                    invoice_accounts = invoice_data['invoice_accounts']

                amount = invoice_amount * (percentage_of_amount/100)

                outstanding = invoice_amount - amount

                invoice_account = {
                    'percentage_of_amount': percentage_of_amount,
                    'account_id': account_id,
                    'invoice_id': invoice_id,
                    'amount': round(amount, 2),
                    'account': account.name
                 }
                invoice_accounts.append(invoice_account)

                invoice_data = {'outstanding': outstanding, 'invoice_accounts': invoice_accounts}

                self.request.session[invoice_data_key] = invoice_data

                self.request.session.save()

                self.request.session.modified = True

                invoice_data['outstanding'] = round(outstanding, 2)

                return JsonResponse({'error': False,
                                     'text': 'Saved successfully',
                                     'invoice_data': invoice_data
                                     })

        return render(request, "invoiceaccount/invoice_account.html", {})
