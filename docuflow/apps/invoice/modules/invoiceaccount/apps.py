from django.apps import AppConfig


class InvoiceaccountConfig(AppConfig):
    name = 'docuflow.apps.invoice.modules.invoiceaccount'
