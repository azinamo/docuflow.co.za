from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('postings/<int:invoice_id>/', views.InvoiceAccountPostingView.as_view(), name='account_postings_view'),
    path('add/<int:pk>/', csrf_exempt(views.AddInvoiceAccountView.as_view()), name='add_invoice_account'),
    path('edit/<int:pk>/', views.EditInvoiceAccountView.as_view(), name='edit_invoice_account'),
    path('distribute/holding/account/<int:pk>/', csrf_exempt(views.DistributeHoldingAccountView.as_view()), 
         name='distribute_invoice_account'),
    path('create/invoice/<int:invoice_id>/account/', views.CreateInvoiceAccountView.as_view(), 
         name='create_invoice_account'),
    path('distribute/vat/account/variance/<int:invoice_id>/', views.DistributeVatInvoiceAccountVarianceView.as_view(), 
         name='distribute_vat_invoice_account_variance'),
    path('form/', views.InvoiceAccountFormView.as_view(), name='invoice_account_form'),
    path('posting/form/', views.InvoiceAccountPostingFormView.as_view(), name='invoice_account_posting_form'),
    path('save/percentage/amount/', views.InvoiceSaveAccountPercentageView.as_view(), 
         name='save_invoice_account_percentage_amount'),
    path('postings/<int:pk>/comment/', views.InvoiceAccountPostingCommentView.as_view(), 
         name='account_postings_show_comment'),
    path('<int:invoice_id>/', views.InvoiceAccountView.as_view(), name='invoice_account_list'),
    path('<int:invoice_id>/create/', views.CreateInvoiceAccountView.as_view(), name='create-invoice-account'),
    path('<int:invoice_id>/edit/<int:pk>/', views.EditInvoiceAccountPostingView.as_view(), name='edit-invoice-account'),
    path('<int:invoice_id>/delete/<int:pk>/', views.DeleteInvoiceAccountView.as_view(), name='delete-invoice-account'),
    path('postings/<int:pk>/delete/', views.DeleteAccountPostingView.as_view(), name='delete_invoice_account_posting'),
]
