from django.contrib.contenttypes.fields import ContentType
from safedelete.config import HARD_DELETE

from docuflow.apps.distribution.models import Distribution
from docuflow.apps.invoice.models import InvoiceAccount
from docuflow.apps.accountposting.models import ObjectPosting
from docuflow.apps.company.models import ObjectItem


class CreateInvoiceAccount(object):

    def __init__(self, invoice, profile, role, **kwargs):
        self.invoice = invoice
        self.profile = profile
        self.role = role
        self.is_vat = kwargs.get('is_vat', False)
        self.kwargs = kwargs

    def execute(self):
        invoice_account = self.create()
        if invoice_account:
            self.create_object_items(invoice_account)

    def create(self):
        amount = self.kwargs.get('amount')
        invoice_account = InvoiceAccount.objects.create(
            invoice=self.invoice,
            account=self.kwargs.get('account'),
            amount=amount,
            profile=self.profile,
            role=self.role,
            goods_received=self.kwargs.get('received'),
            comment=self.kwargs.get('invoice'),
            percentage=self.calculate_percentage(amount),
            is_vat=self.is_vat,
            vat_code=self.kwargs.get('vat_code'),
            reinvoice=self.kwargs.get('reinvoice')
        )
        return invoice_account

    def create_object_items(self, invoice_account):
        object_items = self.kwargs.get('object_items')
        for object_item_id in object_items:
            object_item_str = str(object_item_id)
            if len(object_item_str) > 0:
                if object_item_str.startswith('object-posting', 0, 14):
                    object_posting_id = object_item_str[15:]
                    if object_posting_id:
                        object_posting = ObjectPosting.objects.get(pk=int(object_posting_id))
                        if object_posting:
                            invoice_account.object_postings.add(object_posting)
                else:
                    object_item = ObjectItem.objects.get(pk=int(object_item_id))
                    if object_item:
                        invoice_account.object_items.add(object_item)

    def calculate_percentage(self, amount):

        if self.is_vat is True:
            if self.invoice.vat_amount > 0:
                return round(amount / self.invoice.vat_amount, 2) * 100
        else:
            if self.invoice.net_amount > 0:
                return round(amount / self.invoice.net_amount, 2) * 100
        return 0


class UpdateInvoiceAccount(object):

    def __init__(self, invoice_account):
        self.invoice_account = invoice_account

    def execute(self):
        pass


def delete_invoice_account(invoice_account: InvoiceAccount):
    fixed_asset = getattr(invoice_account, 'fixed_asset')
    if fixed_asset.exists():
        fixed_asset = fixed_asset.first()
        if fixed_asset.is_pending:
            fixed_asset.delete()

    invoice_account.distribution = None
    invoice_account.save()

    distributions = Distribution.objects.filter(
        content_type=ContentType.objects.get_for_model(invoice_account),
        object_id=invoice_account.pk
    )
    if distributions.exists():
        distributions.delete()

    invoice_account.force_delete()
