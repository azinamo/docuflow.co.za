from django import forms
from django.conf import settings
from django.urls import reverse_lazy

from docuflow.apps.company.models import VatCode
from docuflow.apps.common.enums import Module
from docuflow.apps.invoice.models import InvoiceAccount
from docuflow.apps.invoice.services import recalculate_posting_vat


def get_role_accounts(role):
    if not role:
        return None
    return role.accounts.filter(sub_ledger__isnull=True)


class InvoiceAccountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.invoice = kwargs.pop('invoice')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        self.role = kwargs.pop('role')
        super(InvoiceAccountForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = get_role_accounts(role=self.role)
        self.fields['vat_code'].queryset = self.fields['vat_code'].queryset.filter(
            company=self.company, module=Module.PURCHASE.value)
        self.fields['vat_code'].required = False
        if self.instance.pk and self.instance.vat_code:
            self.fields['reinvoice'].initial = True

    class Meta:
        fields = ('amount', 'account', 'comment', 'percentage', 'vat_code', 'suffix', 'vat_line', 'reinvoice')
        model = InvoiceAccount
        widgets = {
            'vat_code': forms.Select(attrs={'class': 'chosen-select'}),
            'account': forms.Select(attrs={'class': 'chosen-select'}),
            'vat_line': forms.Select(choices=((False, 'No'), (True, 'Yes'))),
            'reinvoice': forms.Select(choices=((False, 'No'), (True, 'Yes'))),
            'suffix': forms.TextInput(attrs={'data-ajax-url': reverse_lazy('search_account_suffix')})
        }

    def save(self, commit=True):
        invoice_account = super().save(commit=False)
        is_vat = self.cleaned_data['vat_line']
        if self.instance.pk and self.instance.vat_code:
            is_vat = True
            invoice_account.account = self.cleaned_data['vat_code'].account
        invoice_account.is_vat = is_vat
        invoice_account.invoice = self.invoice
        invoice_account.profile = self.profile
        invoice_account.role = self.role
        invoice_account.is_posted = True
        invoice_account.percentage = InvoiceAccount.calculate_account_percentage(
            invoice=self.invoice, amount=self.cleaned_data['amount'], is_vat=invoice_account.is_vat
        )
        invoice_account.handle_inventory_account(self.cleaned_data['account'])
        invoice_account.save()

        invoice_account.object_items.clear()
        invoice_account.object_postings.clear()

        items = self.request.POST.getlist('object_item')
        invoice_account.save_object_items(items)

        # Ensure the vat is distributed to correct accounts
        recalculate_posting_vat(
            invoice=self.invoice,
            profile=self.profile,
            role=self.role
        )

        return invoice_account


class AccountPostingForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(AccountPostingForm, self).__init__(*args, **kwargs)
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=company, module=Module.PURCHASE)

    comment = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 5, 'rows': 3}))
    vat_line = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    reinvoice = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    amount = forms.CharField(required=True)
    account = forms.CharField(required=True)
    vat_code = forms.ModelChoiceField(required=False, label='Vat Code', queryset=None,
                                      widget=forms.Select(attrs={'class': 'chosen-select'}))

    class Meta:
        model = InvoiceAccount


class InvoiceAccountCommentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(InvoiceAccountCommentForm, self).__init__(*args, **kwargs)
        self.fields['comment'].required = True

    class Meta:
        model = InvoiceAccount
        fields = ('comment', )

        widgets = {
            'comment': forms.Textarea(attrs={'cols': 5, 'rows': 10}),
        }


class DistributeInvoiceAccountForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(DistributeInvoiceAccountForm, self).__init__(*args, **kwargs)
        self.fields['percentage'] = forms.DecimalField(
            required=False,
            label='Percentage of the amount.'
        )
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=company, module=Module.PURCHASE)

    comment = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 5, 'rows': 3}))
    vat_line = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    reinvoice = forms.ChoiceField(required=False, choices=((False, 'No'), (True, 'Yes')))
    amount = forms.CharField(required=True)
    account = forms.CharField(required=True)
    vat_code = forms.ModelChoiceField(required=False, label='Vat Code', queryset=None,
                                      widget=forms.Select(attrs={'class': 'chosen-select'}))

    class Meta:
        model = InvoiceAccount

