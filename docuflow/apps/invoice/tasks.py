from django.core.management import call_command
from huey.contrib.djhuey import db_task


@db_task()
def upload_invoice_to_s3(invoice_id: int):
    call_command('upload_invoice_files_to_s3', company_id=invoice_id)
