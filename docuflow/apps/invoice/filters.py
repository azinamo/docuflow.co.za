import django_filters
from django_filters import rest_framework as filters

from .models import Invoice, InvoiceType
from docuflow.apps.supplier.models import Supplier


def invoice_types(request):
    if request is None:
        return InvoiceType.objects.none()
    return InvoiceType.objects.filter(company=request.session['company'])


def suppliers(request):
    if request is None:
        return Supplier.objects.none()
    return Supplier.objects.filter(company=request.session['company']).order_by('name')


class InvoiceFilter(filters.FilterSet):
    supplier__name = filters.CharFilter(lookup_expr='icontains')
    supplier__code = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Invoice
        fields = ['invoice_date', 'invoice_type__name', 'supplier__code', 'supplier__name', 'total_amount',
                  'company__slug', 'invoice_number', 'net_amount']


class InvoiceCapturedFilter(django_filters.FilterSet):
    invoice_type = django_filters.ModelChoiceFilter(queryset=invoice_types)
    supplier = django_filters.ModelChoiceFilter(queryset=suppliers)
    total_amount = django_filters.NumberFilter(field_name='total_amount', lookup_expr='lt')

    class Meta:
        model = Invoice
        fields = ['invoice_date', 'invoice_type', 'supplier', 'supplier__code', 'total_amount']


class ShowInvoiceFilter(django_filters.FilterSet):

    def __init__(self, *args, **kwargs):
        super(ShowInvoiceFilter, self).__init__(*args, **kwargs)

    invoice_type = django_filters.ModelChoiceFilter(queryset=invoice_types)
    supplier = django_filters.ModelChoiceFilter(queryset=suppliers)
    # status = django_filters.ModelChoiceFilter(queryset=statuses)
    total_amount = django_filters.NumberFilter(field_name='total_amount', lookup_expr='lt')
    from_date = django_filters.DateFilter(field_name='invoice_date', lookup_expr='gt')
    to_date = django_filters.DateFilter(field_name='invoice_date', lookup_expr='lt')

    class Meta:
        model = Invoice
        fields = ['status', 'invoice_date', 'supplier', 'supplier__name', 'net_amount', 'vat_amount',
                  'total_amount', 'invoice_number', 'accounting_date', 'invoice_date', 'due_date', 'date_paid',
                  'invoice_type']


class UnpaidInvoiceFilter(django_filters.FilterSet):
    supplier = django_filters.ModelChoiceFilter(queryset=suppliers)

    class Meta:
        model = Invoice
        fields = ['supplier', 'due_date']


