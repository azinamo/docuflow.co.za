import logging
import os
from datetime import datetime, timedelta
from decimal import Decimal
from typing import List

import boto3
from annoying.functions import get_object_or_None
from botocore.exceptions import ClientError
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.db import models
from django.db import transaction
from django.db.models import F, Count, Q, Sum
from django.db.models.functions import Coalesce
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _
from enumfields import EnumIntegerField
from safedelete.models import SafeDeleteModel, SOFT_DELETE, HARD_DELETE_NOCASCADE, HARD_DELETE

from docuflow.apps.accountposting.models import AccountPosting, AccountPostingLine, ObjectPosting
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.auditlog.registry import auditlog
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Company, Account, VatCode, ObjectItem, CompanyObject, PaymentMethod
from docuflow.apps.fixedasset.models import Category
from docuflow.apps.intervention.models import Intervention
from docuflow.apps.journals.models import Journal as Ledger
from docuflow.apps.period.models import Period, Year
from docuflow.apps.workflow.models import Workflow
from . import enums
from . import managers
from . import utils
from .tasks import upload_invoice_to_s3
from .exceptions import ControlledByInventoryException, InvalidDistributionAmount, UnavailableWorkflow

logger = logging.getLogger(__name__)


# Rename to Document Type and move to system model
class InvoiceType(SafeDeleteModel):
    """
    TODO - Refactor this model, to allow that when we create new Tax Invoice or Credit Note, they can be realized by the system
    """
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name='document_types', on_delete=models.CASCADE)
    file = models.ForeignKey('company.FileType', null=True, related_name='file_invoice_types', on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=100)
    code = models.CharField(null=True, max_length=255)
    format = EnumIntegerField(enums.DocumentFormat, default=enums.DocumentFormat.ENG)
    is_account_posting = models.BooleanField(default=False, verbose_name='Account Posting')
    is_credit = models.BooleanField(default=False, verbose_name='Credit')
    is_default = models.BooleanField(default=False)
    number_start = models.CharField(max_length=255, blank=True)

    class QS(models.QuerySet):

        def sales(self, company):
            return self.filter(company=company, file__module__in=[Module.SALES])

        def default(self):
            return self.filter(is_default=True)

        def purchase(self, company):
            return self.filter(company=company, file__module__in=[Module.PURCHASE])

        def tax_invoice(self, company):
            return self.filter(company=company, code=enums.InvoiceType.TAX_INVOICE.value,
                               file__module__in=[Module.PURCHASE])

        def credit_note(self, company):
            return self.filter(company=company, code=enums.InvoiceType.CREDIT_NOTE.value,
                               file__module__in=[Module.PURCHASE])

        def sales_tax_invoice(self, company):
            return self.sales(company).filter(code=enums.InvoiceType.TAX_INVOICE.value)

        def sales_credit_note(self, company):
            return self.sales(company).filter(code=enums.InvoiceType.CREDIT_NOTE.value)

        def purchase_order(self, company):
            return self.filter(code=enums.InvoiceType.PURCHASE_ORDER.value, company=company)

        def get_proforma_invoice_type(self, company):
            return self.filter(code=enums.InvoiceType.PROFORMA.value, company=company).first()

        def for_document(self, company):
            return self.filter(company=company, file__module=Module.DOCUMENT)

    objects = QS.as_manager()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            if self.file and self.file.module:
                self.slug = slugify(f"{self.company.id}-{self.file.module}-{self.name}")
            self.slug = slugify(f"{self.company.id}-{self.name}")
        if not self.code:
            self.code = slugify(f"{self.name}")
        return super(InvoiceType, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('company', 'code', 'file')

    @property
    def module_file(self):
        if self.file:
            module_name = str(self.file.module) if self.file.module else ''
            return f"{module_name}-{self.file.name}"
        return ''

    @property
    def module_name(self):
        if self.file and self.file.module:
            return self.file.module.label
        return ''

    @property
    def format_type(self):
        return str(self.format).lower()

    @property
    def is_swe_format(self):
        return self.format == enums.DocumentFormat.SWE


# TODO - Remove model and use enums
class FlowStatus(models.Model):

    name = models.CharField(max_length=255)
    slug = models.SlugField()
    # TODO - Remove color field, not used anywhere
    color = models.CharField(max_length=50, default='gray')

    class Meta:
        verbose_name_plural = 'Invoice Flow Statuses'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(FlowStatus, self).save(*args, **kwargs)


class Status(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    code = models.CharField(max_length=200)
    # TODO - Remove color field, not used anywhere
    color = models.CharField(max_length=50, default='gray')

    class Meta:
        verbose_name_plural = 'Invoice Statuses'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super(Status, self).save(*args, **kwargs)


class Invoice(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    PENDING = 0
    PROCESSSING = 1
    COMPLETED = 2
    FAILED = 3

    STATUS_CHOICE_LIST = (
                (PENDING, 'Pending'),
                (PROCESSSING, 'Processing'),
                (COMPLETED, 'Retired'),
                (FAILED, 'Failed'),
            )

    AT_LOGGER = 1
    IN_FLOW = 2
    IN_FLOW_PRINTED = 3
    FINAL_SIGNED = 4
    ACCOUNT_POSTED_PRINTED = 5
    PAID = 6
    PAID_NOT_ACCOUNT_POSTED = 7
    PAID_NOT_PRINTED = 8
    PAID_PRINTED = 9
    ADJUSTMENT = 10
    REJECTED = 11
    REJECTION_AWAITING_APPROVAL = 12

    STATUSES = (
        (AT_LOGGER, 'At Logger'),
        (IN_FLOW, 'In the flow'),
        (IN_FLOW_PRINTED, 'In the flow printed'),
        (FINAL_SIGNED, 'Final Signed'),
        (ACCOUNT_POSTED_PRINTED, 'Account posting printed'),
        (PAID, 'Paid'),
        (PAID_NOT_ACCOUNT_POSTED, 'Paid not account posted'),
        (PAID_NOT_PRINTED, 'Paid not printed'),
        (PAID_PRINTED, 'Paid printed'),
        (ADJUSTMENT, 'Adjustment'),
        (REJECTED, 'Rejected'),
        (REJECTION_AWAITING_APPROVAL, 'Rejected, awaiting approval')
    )

    company = models.ForeignKey(Company, related_name='invoices', null=True, blank=True, on_delete=models.CASCADE)
    invoice_id = models.CharField(max_length=255, blank=True)
    supplier = models.ForeignKey('supplier.Supplier', related_name='invoice_supplier', null=True, blank=True, on_delete=models.DO_NOTHING)
    invoice_type = models.ForeignKey(InvoiceType, related_name='invoice_type', blank=True, null=True, on_delete=models.DO_NOTHING, verbose_name='Document Types')
    company_registration_number = models.CharField(max_length=255, blank=True)
    supplier_name = models.CharField(max_length=255, blank=True)
    supplier_number = models.CharField(max_length=255, blank=True)
    invoice_number = models.CharField(max_length=255, blank=True, default='', verbose_name='Document Number')
    serial_number = models.CharField(max_length=255, blank=True)
    invoice_date = models.DateField(null=True, blank=True, verbose_name='Document Date')
    accounting_date = models.DateField(null=True, blank=True)
    vat_number = models.CharField(max_length=255, blank=True)
    payment_date = models.DateField(null=True, blank=True)
    due_date = models.DateField(null=True, blank=True)
    sub_total = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    total_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    net_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    vat_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    discount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    open_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    telephone = models.CharField(max_length=255, blank=True)
    image = models.FileField(blank=True, null=True, upload_to=utils.upload_path)
    is_valid = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    ocr_status = models.IntegerField('Ocr Status', choices=STATUS_CHOICE_LIST, default=PENDING)
    currency = models.ForeignKey('company.Currency', related_name='currency', blank=True, null=True, on_delete=models.DO_NOTHING)
    workflow = models.ForeignKey(Workflow, related_name='invoice_workflow', blank=True, null=True, on_delete=models.DO_NOTHING)
    vat_code = models.ForeignKey(VatCode, related_name='vat_code', null=True, blank=True, on_delete=models.DO_NOTHING)
    account_posting_proposal = models.ForeignKey(AccountPosting, related_name='account_posting', null=True, blank=True, default=None, on_delete=models.DO_NOTHING)
    status = models.ForeignKey(Status, related_name='invoice_status', blank=True, null=True, on_delete=models.DO_NOTHING)
    is_protected = models.BooleanField(default=False)
    matching_result_purchase_order = models.CharField(blank=True, max_length=255)
    rematching_until = models.DateField(null=True, blank=True)
    intervention_at = models.DateTimeField(verbose_name='Intervention At', null=True)
    contents = models.TextField(blank=True)
    comments = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name='invoice_created', on_delete=models.DO_NOTHING)
    date_paid = models.DateTimeField(_('Date Paid'), null=True, blank=True)
    checksum = models.CharField(max_length=32, editable=False, unique=False,
                                help_text="The checksum of the original document (before it was  encrypted). "
                                          "We use this to prevent duplicate document imports.")
    filename = models.CharField(max_length=255, blank=True)
    journal = models.ForeignKey('Journal', null=True, blank=True, on_delete=models.DO_NOTHING)
    purchase_order_links = models.ManyToManyField('self')
    is_back_order = models.BooleanField(default=False)
    memo_to_supplier = models.TextField(blank=True, verbose_name=_('Memo to supplier'))
    period = models.ForeignKey(Period, null=True, blank=True, related_name='invoices', on_delete=models.DO_NOTHING)
    branch = models.ForeignKey('company.Branch', null=True, blank=True, related_name='branch_invoices', on_delete=models.DO_NOTHING)
    distribution_journal = models.BooleanField(default=False)
    module = EnumIntegerField(Module, default=Module.PURCHASE)
    sales_invoice = models.OneToOneField('sales.Invoice', null=True, blank=True, related_name='purchase_invoice', on_delete=models.SET_NULL)

    objects = managers.InvoiceManager.from_queryset(managers.InvoiceQuerySet)()

    class Meta:
        indexes = [
            models.Index(fields=['company', 'period', 'deleted', 'status']),
            models.Index(fields=['company', 'deleted']),
            models.Index(fields=['deleted'])
        ]

    def __str__(self):
        if self.company and self.company.slug:
            if self.is_purchase_order:
                company_str = self.company.name[0:3]
                invoice_id = str(self.invoice_id)
                padding = 5 if '-' in invoice_id else 4
                counter_str = invoice_id.rjust(padding, '0')
                prefix = "PO"
                po_str = f"{prefix}-{company_str.upper()}{counter_str}"
                if self.is_back_order and self.purchase_order_links:
                    link_po = self.purchase_order_links.first()
                    if link_po:
                        counter_str = str(link_po.invoice_id).rjust(padding, '0')
                    po_str = f"{prefix}{company_str.upper()}-{counter_str}-1"
                return po_str
            return f"{self.company.slug}-{self.invoice_id}"
        return f"{self.invoice_id}"

    def save(self, *args, **kwargs):
        if not self.invoice_id:
            filter_options = {'company_id': self.company_id}
            exclude_options = {}
            if self.is_purchase_order:
                filter_options['invoice_type__code'] = 'purchase-order'
                exclude_options['is_back_order'] = True
            else:
                exclude_options['invoice_type__code'] = 'purchase-order'
            self.invoice_id = self.generate_invoice_number(1, filter_options, exclude_options)
        return super(Invoice, self).save(*args, **kwargs)

    @property
    def is_paid(self):
        return self.payments.exists() and self.open_amount == 0

    @property
    def is_partially_paid(self):
        payments = self.payments
        balance = sum([payment.allocated for payment in payments.all()]) - self.total_amount
        return balance != 0 and payments.exists()

    @property
    def net(self):
        net_amount = 0
        if self.net_amount:
            net_amount = self.net_amount
        return net_amount

    @property
    def vat(self):
        vat_amount = 0
        if self.vat_amount:
            vat_amount = self.vat_amount
        return vat_amount

    @property
    def total(self):
        total_amount = 0
        if self.total_amount:
            total_amount = self.total_amount
        return total_amount

    @property
    def invoice_amount(self):
        return self.total

    @property
    def is_duplicate_invoice_number(self):
        if self.invoice_number and self.supplier:
            invoice_count = Invoice.objects.filter(
                invoice_number=self.invoice_number, supplier=self.supplier
            ).exclude(invoice_number__isnull=True).exclude(invoice_number='').count()
            return invoice_count > 1
        return False

    @property
    def reference_1(self):
        if self.invoice_references.count() > 0:
            return self.invoice_references.all()[0]

    @property
    def reference_2(self):
        if self.invoice_references.count() > 1:
            return self.invoice_references.all()[1]

    @property
    def has_account_posting(self):
        return self.invoice_type and self.invoice_type.is_account_posting

    @property
    def is_purchase_order(self):
        return self.invoice_type and self.invoice_type.code == 'purchase-order'

    @property
    def currency_code(self):
        if self.currency:
            return self.currency.code
        elif self.company.currency:
            return self.company.currency.code
        return ''

    @property
    def is_credit_type(self):
        return self.invoice_type and self.invoice_type.is_credit

    @property
    def has_file(self):
        return self.image or self.filename

    @property
    def file_url(self):
        return f"{self.company.slug}/{self.id}/{self.filename}"

    @property
    def location(self):
        return os.path.join(settings.MEDIA_ROOT, self.file_url)

    @property
    def tmp_url(self):
        return f"{self.company.slug}/tmp/{self.filename}"

    @property
    def file_tmp_location(self):
        return os.path.join(settings.MEDIA_ROOT, self.company.slug, 'tmp', self.filename)

    @property
    def invoice_location(self):
        file_location = self.file_location
        file_tmp_location = self.file_tmp_location
        if os.path.isfile(file_location):
            return file_location
        elif os.path.isfile(file_tmp_location):
            return file_tmp_location

    @property
    def file_location(self):
        if settings.UPLOAD_TO_S3:
            s3_client = boto3.client(
                's3',
                region_name=settings.AWS_S3_REGION_NAME,
                aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
            )

            try:
                url, file_path = utils.invoice_upload_path(
                    invoice=self,
                    filename=self.filename
                )
                if not file_path:
                    if self.image:
                        return self.image
                    else:
                        file_url = self.file_url
                        _file_location = self.location

                    tmp_url = self.tmp_url
                    tmp_location = self.file_tmp_location

                    upload_invoice_to_s3(invoice_id=self.pk)

                    if os.path.isfile(_file_location):
                        return file_url
                    elif os.path.isfile(tmp_location):
                        return tmp_url
                file_path = os.path.join(file_path, self.filename)
                if not os.path.exists(file_path):
                    s3_client.download_file(
                        settings.AWS_STORAGE_BUCKET_NAME,
                        url,
                        file_path
                    )
                return url
            except ClientError as exc:
                logger.exception(exc)
                if self.image:
                    return self.image
                else:
                    file_url = self.file_url
                    _file_location = self.location

                tmp_url = self.tmp_url
                tmp_location = self.file_tmp_location

                upload_invoice_to_s3(invoice_id=self.pk)

                if os.path.isfile(_file_location):
                    return file_url
                elif os.path.isfile(tmp_location):
                    return tmp_url
        else:
            if self.image:
                return self.image
            else:
                file_url = self.file_url
                _file_location = self.location

                tmp_url = self.tmp_url
                tmp_location = self.file_tmp_location

                upload_invoice_to_s3(invoice_id=self.pk)

                path = f"{str(self.company.slug)}/{str(self.period.period_year)}/invoices/{str(self.pk)}/{self.filename}"
                file_path = os.path.join(
                    settings.MEDIA_ROOT,
                    str(self.company.slug),
                    str(self.period.period_year),
                    'invoices',
                    str(self.pk),
                    self.filename
                )
                if os.path.isfile(file_path):
                    return path
                if os.path.isfile(_file_location):
                    return file_url
                elif os.path.isfile(tmp_location):
                    return tmp_url

    @property
    def is_intervention(self):
        flow = self.get_current_flow()
        return flow and flow.intervention_at

    @property
    def is_discountable(self):
        if self.supplier and self.supplier.discount:
            return True
        return False

    @property
    def supplier_discount(self):
        discount = 0
        if self.is_discountable:
            discount = self.total_amount * self.supplier.discount / 100
        return round(discount, 2)

    @property
    def percentage_discount(self):
        percentage = 0
        if self.discount > 0 and self.total > 0:
            percentage = (self.discount/self.total) * 100
        return round(percentage, 2)

    @property
    def is_inventory(self):
        return self.module == Module.INVENTORY

    @property
    def flow(self):
        return self.flows.filter(is_current=True, is_signed=False).first()

    @property
    def is_out_of_flow(self):
        return self.status.slug in utils.processed_statuses()

    @property
    def is_editable(self):
        statuses = utils.processed_statuses() + utils.processing_statuses()
        return self.status.slug in statuses

    @property
    def is_processing(self):
        return self.status.slug in utils.processing_statuses()

    @property
    def display_comments(self):
        return self.comments

    @property
    def incoming_journal_printed(self):
        if self.has_account_posting and self.company.run_incoming_document_journal:
            return self.status and self.status.slug == 'printed-status-still-in-the-flow'
        return True

    @property
    def printed_in_flow(self):
        if self.status and self.status.slug == 'printed-status-still-in-the-flow':
            return True
        return False

    @property
    def not_printed_in_flow(self):
        if self.status and self.status.slug == 'in-the-flow':
            return True
        return False

    @property
    def at_logger(self):
        if self.status and self.status.slug == 'arrived-at-logger':
            return True
        return False

    @property
    def is_approved(self):
        if self.is_purchase_order:
            return self.status and self.status.slug == 'po-approved'
        return False

    @property
    def is_po_declined(self):
        if self.is_purchase_order:
            return self.status and self.status.slug == 'po-declined'
        return False

    @property
    def is_requested(self):
        if self.is_purchase_order:
            return self.status and self.status.slug == 'po-request'
        return False

    @property
    def is_ordered(self):
        if self.is_purchase_order:
            return self.status and self.status.slug == 'po-ordered'
        return False

    @property
    def is_locked(self):
        return self.status and self.status.slug in ['paid', 'decline-printed']

    @property
    def is_declined(self):
        return self.status and self.status.slug in ['decline-printed']

    @property
    def is_parked(self):
        return self.status.slug == 'parked'

    @property
    def is_decline_requested(self):
        if self.status and self.status.slug == 'decline-request-from-user':
            return True
        return False

    @property
    def is_decline_approved(self):
        return self.status.slug == 'decline-approved'

    @property
    def is_final_signed(self):
        return self.status.slug == 'final-signed'

    @property
    def is_end_of_flow_printed(self):
        return self.status.slug == 'end-flow-printed'

    @property
    def is_adjustment(self):
        return self.status.slug == enums.InvoiceStatus.ADJUSTMENT.value

    @classmethod
    def new(
            cls,
            company: Company,
            checksum: str,
            filename: str,
            status: Status,
            invoice_type: InvoiceType,
            supplier=None
            ):

        invoice_data = {}
        if supplier:
            invoice_data['supplier'] = supplier
            if supplier.flow_proposal:
                invoice_data['workflow'] = supplier.flow_proposal
            if supplier.account_posting_proposal:
                invoice_data['account_posting_proposal'] = supplier.account_posting_proposal
            if supplier.vat_number:
                invoice_data['vat_number'] = supplier.vat_number
        accounting_date = timezone.now()

        period = utils.get_default_period(company=company, accounting_date=accounting_date)
        if period:
            invoice_data['period'] = period

        invoice, _ = cls.objects.update_or_create(
            company=company,
            checksum=checksum,
            status=status,
            defaults={
                'accounting_date': accounting_date,
                'filename': filename,
                'invoice_type': invoice_type,
                **invoice_data
            }
        )
        return invoice

    # noinspection PyMethodMayBeStatic
    def generate_invoice_number(self, counter, filter_options, exclude_options):
        try:
            invoice = Invoice.objects.filter(**filter_options).exclude(**exclude_options).latest('created_at')
            try:
                invoice_num = int(invoice.invoice_id)
            except ValueError as e:
                invoice_num = 0
            invoice_number = invoice_num + 1
            return invoice_number
        except Invoice.DoesNotExist as e:
            return counter
        except ValueError:
            return counter

    def calculate_open_amount(self, payment_filter=None):
        # TODO - Use aggregates to get total paid
        if not payment_filter:
            payment_filter = dict()
        if payment_filter:
            payments = self.payments.filter(**payment_filter)
        else:
            payments = self.payments
        total_paid = 0
        # payment_total = payments.aggregate(total_allocated=Sum('allocated')) # TODO - Test and ensure working fine
        if payments.count() > 0:
            # total_paid = payment_total['total_allocated']
            for payment in payments.all():
                total_paid += payment.allocated
        amount = self.total_amount - Decimal(total_paid)
        return amount

    def get_open_amount(self):
        if self.is_credit_type and self.open_amount > 0:
            return self.open_amount * -1
        return self.open_amount

    def set_credit_note_amount(self):
        if self.total_amount and self.total_amount > 0:
            self.total_amount = self.total_amount * -1
        if self.vat_amount and self.vat_amount > 0:
            self.vat_amount = self.vat_amount * -1
        if self.net_amount and self.net_amount > 0:
            self.net_amount = self.net_amount * -1
        if self.sub_total and self.sub_total > 0:
            self.sub_total = self.sub_total * -1
        if self.open_amount and self.open_amount > 0:
            self.open_amount = self.open_amount * -1
        logger.info(f"Now values = {self.total_amount} -> {self.vat_amount} -> {self.net_amount} -> {self.sub_total} -> {self.open_amount}")
        self.save()

    def set_in_flow_printed(self):
        self.status = Status.objects.get(slug='printed-status-still-in-the-flow')

    def calculate_due_date(self):
        if self.supplier and self.supplier.payment_terms == 'invoice_date':
            if self.supplier.payment_days and self.invoice_date:
                return self.invoice_date + timedelta(days=self.supplier.payment_days)
        return self.invoice_date

    # noinspection PyMethodMayBeStatic
    def set_change_comment(self, value, word):
        if value > 0:
            return f"{word} +{value}"
        else:
            return f"{word} {value}"

    def set_as_adjustment(self, user, role):
        current_flow = self.get_current_flow()
        comments = ['Adjustment']
        net_change = 0
        vat_change = 0
        if net_change:
            comments.append(self.set_change_comment('NET', net_change))
        if vat_change:
            comments.append(self.set_change_comment('VAT', vat_change))

        adjustment_status = get_object_or_None(Status, slug='adjustments')
        if adjustment_status and self.printed_in_flow:
            self.status = adjustment_status
            self.save()

        self.log_invoice_activity(current_flow, user, role, ','.join(comments), Comment.EVENT)

    def mark_as_received(self):
        received_status = get_object_or_None(Status, slug='po-received')
        if not received_status:
            raise ValueError('Purchase order could not be approved, purchase order received status not found. '
                             'Contact admin to setup statuses')
        self.status = received_status
        self.save()

    def mark_purchase_order_as_approved(self):
        po_status = get_object_or_None(Status, slug='po-approved')
        if not po_status:
            raise ValueError('Purchase order could not be approved, purchase order approved status not found. '
                             'Contact admin to setup statuses')

        self.status = po_status
        if not self.invoice_number:
            if hasattr(self, 'inventory_purchase_order'):
                self.invoice_number = str(self.inventory_purchase_order)
            else:
                self.invoice_number = str(self)
        self.save()

    def mark_purchase_order_as_declined(self):
        po_status = get_object_or_None(Status, slug='po-declined')
        if not po_status:
            raise ValueError('Purchase order could not be declined, purchase order decline status not found. '
                             'Contact admin to setup statuses')

        self.status = po_status
        self.save()

    def has_incoming_journal(self):
        return self.invoice_journals.filter(journal__journal_type=enums.JournalType.INCOMING).exists()

    def has_posting_journal(self):
        return self.invoice_journals.filter(journal__journal_type=enums.JournalType.POSTING).count()

    def invoice_number_exists(self):
        if self.invoice_number and self.supplier:
            invoice_count = Invoice.objects.filter(
                invoice_number=self.invoice_number, supplier=self.supplier
            ).exclude(invoice_number__isnull=True).exclude(id=self.id).exclude(invoice_number='').count()
            return invoice_count > 1
        return False

    def can_transition(self, user):
        if self.workflow:
            return True

    def scan_update(self, result):
        is_total_match = False

        if 'date' in result:
            self.invoice_date = result['date']

        if 'supplier_name' in result:
            self.supplier_name = result['supplier_name']
        elif 'issuer' in result:
            self.supplier_name = result['issuer']

        if 'telephone' in result:
            self.telephone = result['telephone']

        if 'invoice_number' in result:
            self.invoice_number = result['invoice_number']

        if 'amount' in result:
            try:
                self.total_amount = float(result['amount'])
                self.open_amount = float(result['amount'])
            except ValueError as e:
                self.total_amount = 0
        if 'tax' in result:
            try:
                self.vat_amount = result['tax']
            except ValueError as e:
                self.vat_amount = 0
        elif 'vat' in result:
            self.vat_amount = result['vat']

        if 'vat_number' in result:
            self.vat_number = result['vat_number']

        if 'company_number' in result:
            self.company_registration_number = result['company_number']

        if self.company_registration_number and self.vat_number and is_total_match is True:
            self.is_valid = True

        self.ocr_status = self.COMPLETED

        return self.save()

    def is_valid_distribution_amount(self, amount, is_vat=False, exclude_accounts=None):
        if exclude_accounts is None:
            exclude_accounts = []
        if amount > 0:
            total_posted = self.get_accounts_total(is_vat, exclude_accounts)
            if is_vat is True:
                total = total_posted + amount
                if total > self.vat_amount:
                    raise InvalidDistributionAmount(f'Vat amount posted must not exceed the invoice total vat {self.vat_amount}')
            else:
                total = total_posted + amount
                if total > self.net_amount:
                    raise InvalidDistributionAmount(f'Amount posted must not exceed the invoice total net amount {self.net_amount}')
        else:
            raise InvalidDistributionAmount('Amount should be greater than 0')
        return True

    def get_accounts_total(self, is_vat=False, exclude_accounts=None):
        if exclude_accounts is None:
            exclude_accounts = []
        accounts = InvoiceAccount.objects.filter(invoice=self, is_vat=is_vat).exclude(id__in=exclude_accounts)
        total = 0
        for account in accounts.all():
            total = total + account.amount
        return total

    def valid_invoice_totals(self):
        if self.company.is_vat_registered:
            totals = Decimal(0)
            if self.vat_amount:
                totals = self.vat_amount + totals
            if self.net_amount:
                totals = self.net_amount + totals

            if self.total_amount:
                if totals == self.total_amount:
                    return True

            return totals == self.total_amount
        return True

    def get_previous_flow(self,  current_flow):
        flows = self.flows.all().order_by('order_number')
        counter = 0
        for flow in flows:
            if flow:
                if flow.order_number < current_flow.order_number:
                    return flow
        return None

    def set_current_flow(self, flow):
        flow.is_current = True
        flow_status = get_object_or_None(FlowStatus, slug='processing')
        if flow_status:
            flow.status = flow_status
        flow.save()

    def flow_progress(self, current_flow):
        if not current_flow:
            current_flow = self.get_current_flow()

        flows = self.flows.all().order_by('order_number')
        counter = 0
        for flow in flows:
            if current_flow:
                if flow.order_number > current_flow.order_number:
                    self.set_current_flow(flow)
                    break
            else:
                if counter == 0:
                    self.set_current_flow(flow)
                    break
            counter = counter + 1

        if current_flow:
            current_flow.is_signed = True
            current_flow.is_current = False
            current_flow.sign_count = current_flow.roles.all().count()
            current_flow.save()

    def get_workflow(self):
        if self.workflow:
            return Workflow.objects.prefetch_related(
                'levels', 'levels__roles'
            ).filter(
                company=self.company, pk=self.workflow.id
            ).first()

        else:
            return Workflow.objects.prefetch_related(
                'levels', 'levels__roles'
            ).filter(
                company=self.company, is_default=True
            ).first()

    def add_to_workflow(self, workflow = None):
        if not workflow:
            workflow = self.get_workflow()

        if workflow:
            self.workflow = workflow
            # self.update_workflow_activity(workflow)

            self.update_status()
        else:
            raise UnavailableWorkflow("Workflow for the invoice not available, please choose the default workflow for "
                                      "your company")

    def update_workflow_activity(self, workflow):
        pass

    def update_status(self):
        invoice_status = None
        if self.status:
            status = self.status
            if status.slug == 'arrived-at-logger':
                invoice_status = get_object_or_None(Status, slug='in-the-flow')
            elif status.slug == 'in-the-flow':
                invoice_status = get_object_or_None(Status, slug='printed-status-still-in-the-flow')
            elif status.slug == 'final-signed':
                invoice_status = get_object_or_None(Status, slug='final-signed')
        else:
            invoice_status = get_object_or_None(Status, slug='in-the-flow')

        if invoice_status:
            self.status = invoice_status
            self.save()

    def get_current_flow(self):
        return Flow.objects.filter(invoice=self, is_current=True, is_signed=False).order_by('order_number').first()

    def get_current_state(self):
        return get_object_or_None(Flow, invoice=self, is_current=True, is_signed__isnull=True)

    def get_all_holding_accounts(self, is_vat=True):
        accounts = InvoiceAccount.objects.all_with_deleted().filter(invoice=self, is_holding_account=True, is_vat=is_vat)
        account_amounts = {}
        for account in accounts:
            account_amounts[account.id] = account.amount
        return account_amounts

    def get_holding_accounts(self, is_vat=True):
        return InvoiceAccount.objects.filter(invoice=self, is_holding_account=True, is_vat=is_vat)

    def get_general_accounts(self, is_vat=False):
        return InvoiceAccount.objects.filter(invoice=self, invoice_account_id__isnull=True, is_vat=is_vat)

    def get_postings(self, linked_models, is_vat=False):
        account_postings = {'total': 0, 'accounts': [], 'variance': 0}
        total = 0

        accounts = InvoiceAccount.objects.select_related(
            'account', 'profile', 'invoice', 'role', 'vat_code'
        ).prefetch_related(
            'object_items'
        ).filter(invoice=self, is_vat=is_vat).all()

        for account in accounts:
            if account.amount:
                total = account.amount + total
            invoice_account = account.prepare_invoice_account(linked_models)
            account_postings['accounts'].append(invoice_account)

        account_postings['total'] = total
        return account_postings

    def get_account_postings(self, linked_models):
        account_postings = self.get_postings(linked_models)
        vat_postings = self.get_postings(linked_models, True)
        total = account_postings['total'] + vat_postings['total']

        return {
                'total': total,
                'account_postings': account_postings,
                'account_posting_variance': self.get_posting_variance(),
                'vat_account_postings': vat_postings,
                'vat_posting_variance': self.get_posting_variance(True),
               }

    def get_posting_variance(self, is_vat=False):
        accounts = InvoiceAccount.objects.filter(invoice=self, is_vat=is_vat).aggregate(total=Sum('amount'))
        holding_total = 0
        default_total = accounts['total'] if accounts and accounts['total'] else 0
        variance = 0
        if is_vat:
            vat_amount = self.vat_amount or 0
            if vat_amount:
                variance = vat_amount - (default_total + holding_total)
        else:
            variance = self.net_amount - (default_total + holding_total)
        return variance

    def calculate_net_variance(self):
        accounts = InvoiceAccount.objects.filter(invoice=self, is_vat=False).aggregate(total=Sum('amount'))
        net_amount = self.net_amount or 0
        account_total = accounts['total'] if accounts and accounts['total'] else 0
        variance = 0
        if net_amount:
            variance = net_amount - account_total
        return variance

    def calculate_vat_variance(self):
        accounts = InvoiceAccount.objects.filter(invoice=self, is_vat=True).aggregate(total=Sum('amount'))
        vat_amount = self.vat_amount or 0
        account_total = accounts['total'] if accounts and accounts['total'] else 0
        variance = 0
        if vat_amount:
            variance = vat_amount - account_total
        return variance

    def has_variance(self):
        if not self.invoice_type.is_account_posting:
            return False
        vat_variance = self.calculate_vat_variance()
        expenditure_variance = self.calculate_net_variance()
        if vat_variance == 0 and expenditure_variance == 0:
            return False
        else:
            return True

    def has_vat_with_no_vat_code_type(self):
        if self.vat_amount == 0:
            return False
        if self.vat_code and self.vat_code.percentage == 0:
            return True
        vat_posting = InvoiceAccount.objects.select_related('vat_code').filter(
            vat_code__isnull=False, invoice=self
        ).exclude(amount=0).first()
        return vat_posting and vat_posting.vat_code.percentage == 0

    def get_invoice_account_linked_to_asset(self, exclude_invoice_accounts=None):
        exclude_invoice_accounts = exclude_invoice_accounts or []
        return InvoiceAccount.objects.link_to_asset(
            invoice=self, is_linked=False
        ).exclude(
            pk__in=[int(invoice_account_id) for invoice_account_id in exclude_invoice_accounts]
        ).first()

    def add_default_invoice_account_posting(self, profile):
        if self.account_posting_proposal:
            invoice_account = get_object_or_None(InvoiceAccount, invoice=self)
            if not invoice_account:
                InvoiceAccount.objects.create(
                    account=self.account_posting_proposal.account,
                    amount=self.net_amount,
                    profile=profile,
                    invoice=self
                )
            else:
                invoice_account.account = self.account_posting_proposal.account
                invoice_account.amount = self.total_amount
                invoice_account.profile = profile
                invoice_account.invoice = self

    def add_default_invoice_vat_posting(self, profile, role):
        if self.vat_code:
            invoice_account = get_object_or_None(InvoiceAccount, invoice=self, account=self.vat_code.account)
            if not invoice_account:
                InvoiceAccount.objects.create(
                    account=self.vat_code.account,
                    amount=self.vat_amount,
                    profile=profile,
                    role=role,
                    invoice=self,
                    is_vat=True
                )
            else:
                invoice_account.account = self.vat_code.account
                invoice_account.amount = self.vat_amount
                invoice_account.profile = profile
                invoice_account.invoice = self

    def complete_sign_invoice(self):
        signed_flows = 0
        total_flows = 0
        for flow in self.flows.all():
            total_flows = total_flows + 1
            if flow.is_signed is True:
                signed_flows = signed_flows + 1

        if self.is_purchase_order:
            invoice_status = None
            if self.status.slug != 'po-declined':
                invoice_status = get_object_or_None(Status, slug='po-ordered')
        else:
            invoice_status = get_object_or_None(Status, slug='final-signed')

        if invoice_status and (total_flows == signed_flows):
            self.status = invoice_status
            self.save()

    def get_role_next_invoice(self, role: Role, next_index: int = 0):
        # get the flows that the role is in, and get the invoice ids
        flows = FlowRole.objects.filter(
            is_signed=False, flow__is_current=True, role=role, flow__invoice__company=self.company
        ).exclude(
            Q(flow__invoice__status__slug__in=['decline-printed']) | Q(flow__invoice__pk=self.pk)
        ).values_list('flow__invoice__pk', flat=True)
        try:
            return flows[next_index]
        except IndexError:
            if len(flows) > 0:
                return flows[0]
        return None

    def log_add_to_flow(self, user, role):
        Comment.objects.create(
            invoice=self,
            log_type=Comment.EVENT,
            participant=user,
            note="Added document to flow",
            status=self.status,
            role=role
        )

    def log_activity(self, user, role, comment=None, log_type=None, attachment=None, linked_invoice=None,
                     link_url=None):
        # Log invoice activity history
        if not log_type:
            log_type = Comment.COMMENT
        log = Comment.objects.create(
            invoice=self,
            log_type=log_type,
            participant=user,
            note=comment,
            attachment=attachment,
            status=self.status,
            link_url=link_url
        )
        if log:
            if linked_invoice:
                log.linked_invoice = linked_invoice
            if isinstance(role, Role):
                log.role = role
            elif type(role) == int:
                log.role_id = role
            log.save()
        if comment and log_type == Comment.COMMENT:
            self.comments = comment
            self.save()

        return log

    def log_invoice_activity(self, invoice_flow, user, role, comment=None, log_type=None, attachment=None):
        # Log invoice activity history
        if not log_type:
            log_type = Comment.COMMENT
        log = Comment(
            invoice=self,
            invoice_flow=invoice_flow,
            log_type=log_type,
            participant=user,
            note=comment,
            attachment=attachment
        )
        if invoice_flow:
            log.status = invoice_flow.invoice.status
        if isinstance(role, Role):
            log.role = role
        elif type(role) == int:
            log.role_id = role
        log.save()
        if comment and log_type == Comment.COMMENT:
            self.comments = comment
            self.save()

        return log

    def has_default_company_account(self):
        if self.has_account_posting:
            default_expenditure_account = None
            if self.company.default_expenditure_account:
                default_expenditure_account = self.company.default_expenditure_account
            if default_expenditure_account:
                return InvoiceAccount.objects.filter(
                    is_vat=False,
                    invoice=self,
                    account=default_expenditure_account
                ).exclude(
                    amount=0
                ).count()  # TODO Use exists
        return False

    def validate_accounting_date_period(self):
        logger.info(f"Period --> {self.period}({self.period.id}) and period is closed {not self.period.is_open} ")
        return self.period and self.period.is_open

    def validate_accounting_date(self, accounting_date=None):
        if not accounting_date:
            accounting_date = self.accounting_date

        periods = Period.objects.filter(company=self.company, is_closed=False)
        is_valid = False
        for period in periods:
            if accounting_date > period.from_date and accounting_date <= period.to_date:
                is_valid = True
                break
        return is_valid

    def update_holding_accounts(self, invoice_account):
        holding_accounts = InvoiceAccount.objects.filter(
            is_holding_account=True, is_posted=False, amount__gt=0, invoice=self, is_vat=invoice_account.is_vat
        ).all()

        for account in holding_accounts:
            account.is_posted = True
            account.save()

    def get_images(self):
        images = [self.filename]
        if hasattr(self, 'document'):
            images.append(self.document.filename)
        return images

    def at_last(self):
        invoice_flows = self.flows.all()
        not_signed = []
        at_last = False
        for flow in invoice_flows:
            if not flow.is_signed:
                not_signed.append(flow)

        if len(not_signed) == 1:
            not_signed_flow_roles = []
            for flow_role in not_signed[0].roles.all():
                if not flow_role.is_signed:
                    not_signed_flow_roles.append(flow_role)

            if len(not_signed_flow_roles) == 1:
                at_last = True

        return at_last

    def generate_journal_unique_number(self, counter, role, user):
        role = Role.objects.get(pk=role)
        profile = Profile.objects.get(pk=user)
        counter_str = str(counter).rjust(5, '0')
        company_code = (self.company.code[0:3]).upper()
        role_code = (role.name[0:3]).upper()
        user_code = (profile.user.first_name[0:2]).upper()
        surname_code = (profile.user.last_name[0:2]).upper()
        unique_number = f"{company_code}-{role_code}-{user_code}-{surname_code}-JII-{counter_str}"

        result = get_object_or_None(Journal, report_unique_number=unique_number)
        if result:
            counter = counter + 1
            return self.generate_journal_unique_number(counter, role, user)
        else:
            return unique_number

    def in_journal(self, journal_type):
        check_journal = self.at_last()

        if check_journal is True:
            if self.journal:
                invoice_journal = InvoiceJournal.objects.filter(
                    invoice=self,
                    journal=self.journal,
                    journal__journal_type=journal_type,
                    invoice_status=self.status
                ).first()
            else:
                invoice_journal = InvoiceJournal.objects.filter(
                    invoice=self,
                    invoice_status=self.status,
                    journal__journal_type=journal_type
                ).first()
            if not invoice_journal:
                return False
        return True

    def make_invoice_adjustments(self, net_change, vat_change, user, role):
        current_flow = self.get_current_flow()
        comment = 'Adjustment: '
        has_adjusted = False
        if net_change:
            if net_change > 0:
                comment += f'NET +{net_change}'
            else:
                comment += f'NET {net_change}'
            has_adjusted = True
        if vat_change:
            if vat_change > 0:
                comment += f', VAT +{vat_change}'
            else:
                comment += f', VAT {vat_change}'
            has_adjusted = True

        if has_adjusted:
            self.log_invoice_activity(
                invoice_flow=current_flow,
                user=user,
                role=role,
                comment=comment,
                log_type=Comment.EVENT
            )
            adjustment_status = get_object_or_None(Status, slug='adjustments')
            if self.printed_in_flow:
                self.status = adjustment_status
            self.open_amount = self.calculate_open_amount()
            self.save()

    def is_allocated(self):
        at_last = self.at_last()
        fully_posted = False
        company_default_vat_account = self.company.default_vat_account
        company_default_exp_account = self.company.default_expenditure_account
        if at_last is True:
            invoice_holding_accounts = InvoiceAccount.objects.filter(invoice=self, is_holding_account=True).all()
            count = len(invoice_holding_accounts)
            posted_count = 0
            for invoice_account in invoice_holding_accounts:
                if invoice_account.is_posted:
                    posted_count = posted_count + 1
                else:
                    if invoice_account.is_vat and invoice_account.account != company_default_vat_account:
                        posted_count = posted_count + 1
                    elif invoice_account.account != company_default_exp_account:
                        posted_count = posted_count + 1
            return posted_count == count
        return True

    def is_account_posting_set(self):
        if self.has_account_posting:
            return self.account_posting_proposal or self.company.default_expenditure_account
        return True

    def revert_decline_request(self):
        if InvoiceJournal.objects.filter(invoice_id=self.pk).exists():
            self.status = Status.objects.get(slug=enums.InvoiceStatus.IN_FLOW_PRINTED.value)
        else:
            self.status = Status.objects.get(slug=enums.InvoiceStatus.IN_FLOW.value)
        self.save()
        return self

    def approve_decline(self):
        if not self.company.run_incoming_document_journal:
            self.status = Status.objects.get(slug=enums.InvoiceStatus.DECLINE_PRINTED.value)
        else:
            if self.has_incoming_journal():
                self.status = Status.objects.get(slug=enums.InvoiceStatus.DECLINE_APPROVED.value)
            else:
                self.status = Status.objects.get(slug=enums.InvoiceStatus.DECLINE_PRINTED.value)
        self.save()
        self.refresh_from_db()
        return self


class InvoiceReference(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='invoice_references', null=False, on_delete=models.CASCADE)
    reference = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Invoice references'
        get_latest_by = 'created_at'

    def __str__(self):
        return self.reference


class InvoiceAccount(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    account_posting_line = models.ForeignKey(AccountPostingLine, related_name='posting_line_invoices', null=True, blank=True, on_delete=models.DO_NOTHING)
    invoice = models.ForeignKey(Invoice, related_name='invoice_accounts',  on_delete=models.DO_NOTHING)
    account = models.ForeignKey(Account, related_name='account_invoices', on_delete=models.DO_NOTHING)
    vat_code = models.ForeignKey(VatCode, related_name='vat_code_invoice_accounts', null=True, blank=True, on_delete=models.DO_NOTHING)
    profile = models.ForeignKey(Profile, related_name='profile_invoice_accounts', on_delete=models.DO_NOTHING)
    role = models.ForeignKey(Role, related_name='role_created_invoice_accounts', null=True, blank=True, on_delete=models.DO_NOTHING)
    invoice_account = models.ForeignKey('self', related_name="parent", null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(blank=True)
    percentage = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    variance = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Date Time')
    updated_at = models.DateTimeField(auto_now=True)
    vat_line = models.BooleanField(default=False)
    reinvoice = models.BooleanField(default=False)
    is_holding_account = models.BooleanField(default=False, verbose_name='Holding Account')
    is_posted = models.BooleanField(default=False, verbose_name='Account Posted')
    is_vat = models.BooleanField(default=False, verbose_name='Vat Account')
    distribution = models.ForeignKey('distribution.Distribution', null=True, blank=True, related_name='invoice_accounts', on_delete=models.DO_NOTHING)
    goods_received = models.OneToOneField('inventory.Received', null=True, blank=True, related_name='invoice_account', on_delete=models.DO_NOTHING)
    goods_returned = models.OneToOneField('inventory.Returned', null=True, blank=True, related_name='invoice_account', on_delete=models.DO_NOTHING)
    object_items = models.ManyToManyField(ObjectItem)
    object_postings = models.ManyToManyField('accountposting.ObjectPosting')
    suffix = models.CharField(max_length=100, blank=True)

    class QS(models.QuerySet):

        def link_to_asset(self, invoice, is_linked=None):
            q = self.prefetch_related(
                'fixed_asset'
            ).filter(
                account_id__in=Category.objects.filter(company=invoice.company).values_list('account_id', flat=True),
                invoice=invoice
            )
            if is_linked is not None:
                q = q.exclude(Q(fixed_asset__isnull=is_linked) | Q(asset_addition__isnull=is_linked))
            return q

        def not_linked_to_asset(self):
            return self.filter(
                fixed_asset__deleted__isnull=True
            ).filter(
                account_id__in=list(Category.objects.values_list('account_id'))
            ).filter(Q(asset_addition__isnull=True) | Q(fixed_asset__isnull=True))

        def not_linked_to_inventory(self):
            return self.filter(goods_received__isnull=True, goods_returned__isnull=True)

    objects = managers.InvoiceAccountManager.from_queryset(QS)()

    class Meta:
        verbose_name_plural = 'Invoice Accounts'
        # unique_together = ('account', 'invoice')

    def __str__(self):
        return f"Invoice {self.invoice} - Account {self.account}"

    @classmethod
    def calculate_account_percentage(cls, invoice: Invoice, amount: Decimal, is_vat: bool = False):
        if is_vat is True:
            if invoice.vat_amount > 0:
                return round(amount / invoice.vat_amount, 2) * 100
        else:
            if invoice.net_amount > 0:
                return round(amount / invoice.net_amount, 2) * 100

    def calculate_variance(self, invoice):
        invoice_accounts = InvoiceAccount.objects.filter(invoice=invoice, invoice_account=self)
        total = 0
        for _invoice_account in invoice_accounts:
            total = _invoice_account.amount + total
        return self.amount - total

    def prepare_invoice_account(self, linked_models):
        account_profile = self.profile.code
        if self.role:
            if self.is_posted is True:
                account_profile = self.profile.code
            else:
                account_profile = self.role
        edit_url = reverse('edit_invoice_account', kwargs={'pk': self.id})
        vat_code = ''
        if self.vat_code:
            vat_code = self.vat_code
        account = self.account
        if self.distribution:
            account = self.invoice.company.default_distribution_account
        amount = self.amount
        if self.invoice.is_credit_type:
            amount = self.amount * -1
        account_data = {
                        'is_vat': self.is_vat,
                        'amount': amount,
                        'account': account,
                        'vat_code': vat_code,
                        'percentage': self.percentage,
                        'comment': self.comment,
                        'date': self.created_at,
                        'reinvoice': self.reinvoice,
                        'signed': False,
                        'profile': account_profile,
                        'posted_to_accounts': account_profile,
                        'linked_models_values': {},
                        'id': self.id,
                        'invoice_id': self.invoice.id,
                        'url': edit_url,
                        'edit_url': edit_url
                       }

        object_links = {}
        for key, linked_model in linked_models.items():
            object_links[key] = {'value': None, 'code': None}

        for link in self.object_items.all():
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.label, 'code': link.code}

        account_data['object_links'] = object_links
        return account_data

    def save_object_items(self, posted_object_items):
        self.object_items.clear()
        self.object_postings.clear()
        for object_item_id in posted_object_items:
            object_item_str = str(object_item_id)
            if len(object_item_str) > 0:
                if object_item_str.startswith('object-posting', 0, 14):
                    object_posting_id = object_item_str[15:]
                    if object_posting_id:
                        object_posting = ObjectPosting.objects.get(pk=int(object_posting_id))
                        if object_posting:
                            self.object_postings.add(object_posting)
                else:
                    object_item = ObjectItem.objects.get(pk=int(object_item_id))
                    if object_item:
                        self.object_items.add(object_item)

    def handle_inventory_account(self, account):
        is_controlled_by_inventory = False
        if account.sales_inventory_items.count() > 0: # sales account
            is_controlled_by_inventory = True
        elif account.cost_of_salaes_inventory_items.count() > 0: # cost of sales account
            is_controlled_by_inventory = True
        elif account.gl_inventory_items.count() > 0: # stock control
            is_controlled_by_inventory = True

        if is_controlled_by_inventory:
            supplier_goods_received = self.invoice.supplier.inventory_movements
            if supplier_goods_received.count() > 0:
                logger.info("Handle goods received issue")
            else:
                raise ControlledByInventoryException('You cannot post to this account directly from here, '
                                                     'you have to create a goods received note for this item as '
                                                     'the account number you are using is controlled by the '
                                                     'inventory ledger, do you want to create a goods received in '
                                                     'the inventory ledger now?')

    def get_posting_account(self):
        if self.distribution and self.invoice.company.default_distribution_account:
            return self.invoice.company.default_distribution_account
        return self.account

    @property
    def account_profile(self):
        if self.is_posted is True:
            return self.profile.code
        else:
            return self.role

    def force_delete(self):
        self.delete(force_policy=HARD_DELETE)

    @property
    def is_credit(self):
        return self.invoice.invoice_type.is_credit


class Flow(models.Model):
    invoice = models.ForeignKey(Invoice, related_name="flows", on_delete=models.CASCADE)
    order_number = models.IntegerField(default=1)
    status = models.ForeignKey(FlowStatus, related_name='invoice_flow_status', on_delete=models.DO_NOTHING)
    is_current = models.BooleanField(default=False)
    sign_count = models.IntegerField(default=0, null=True, blank=True)
    is_signed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated At')
    intervention_at = models.DateTimeField(verbose_name='Signed At', null=True)

    class QS(models.QuerySet):
        def current(self, invoice: Invoice):
            return self.filter(invoice=invoice, is_signed=True)

    objects = QS.as_manager()

    class Meta:
        ordering = ['-order_number']
        verbose_name = _('Invoice Flow')
        verbose_name_plural = _('Invoice Flow')
        indexes = [
            models.Index(fields=['is_current'])
        ]

    def __str__(self):
        return f"{self.invoice} -> {self.status}"

    @classmethod
    def new(cls, invoice: Invoice, roles: List[Role], after: int, profile: Profile) -> 'Flow':
        with transaction.atomic():
            counter = Flow.objects.filter(invoice=invoice).count()
            counter = counter + 1

            is_current = not Flow.objects.filter(invoice=invoice, is_signed=False).exists()

            flow = Flow.objects.create(
                invoice=invoice,
                order_number=counter,
                is_current=is_current,
                status=FlowStatus.objects.get(slug='processing')
            )

            for role in roles:
                FlowRole.objects.create(
                    role=role,
                    flow=flow,
                    status=FlowRole.PROCESSING,
                    created_by=profile
                )

            if after:
                after = int(after)
                flow.update_ordering_after(invoice=invoice, after=after)
            return flow

    def add_roles(self, roles: List[Role], profile: Profile):
        flow_roles_to_add = []
        for role in roles:
            flow_roles_to_add.append(
                FlowRole(
                    role=role,
                    flow=self,
                    status=FlowRole.PROCESSING,
                    created_by=profile
                )
            )
        FlowRole.objects.bulk_create(flow_roles_to_add)

    def is_last(self):
        return self.order_number == self.invoice.flows.count()

    def has_many_roles(self):
        return self.roles.count() > 1

    def has_pending_roles(self):
        return FlowRole.objects.filter(flow=self, is_signed=False).count() > 1

    def sign_flow_role(self, role, profile):
        role_flow = get_object_or_None(FlowRole, role=role, flow=self)
        if role_flow:
            role_flow.status = FlowRole.PROCESSED
            role_flow.is_signed = True
            role_flow.signed_at = datetime.now()
            role_flow.signed_by = profile
            role_flow.save()

    def complete_sign_level(self, invoice, progress=True):
        current_flow_roles = FlowRole.objects.filter(flow=self).all()
        signed_roles = 0
        total_roles = 0
        for current_flow_role in current_flow_roles:
            total_roles = total_roles + 1
            if current_flow_role.is_signed is True:
                signed_roles = signed_roles + 1

        if total_roles == signed_roles:
            invoice_flow_status = get_object_or_None(FlowStatus, slug='processed')
            if invoice_flow_status:
                self.status = invoice_flow_status
                self.is_signed = True
                self.save()
                if progress:
                    invoice.flow_progress(self)

        if not progress:
            self.is_signed = True
            self.is_current = False
            self.sign_count = self.roles.all().count()
            self.save()

    def adjust_order_numbers_after(self, invoice):
        for flow in invoice.flows.all().order_by('order_number'):
            if self.id != flow.id and flow.order_number >= self.order_number:
                order_number = flow.order_number + 1
                flow.order_number = order_number
                flow.save()

    def update_ordering_after(self, invoice: Invoice, after: int):
        for flow in invoice.flows.all().order_by('order_number'):
            if flow.order_number == after:
                in_flow = get_object_or_None(Flow, pk=self.id)
                if in_flow:
                    in_flow.order_number = flow.order_number + 1
                    in_flow.save()
            elif flow.order_number >= after:
                if not self.id == flow.id:
                    order_number = flow.order_number + 1
                    flow.order_number = order_number
                    flow.save()

    @property
    def current_role(self):
        return self.roles.filter(status=FlowRole.PROCESSING).first()

    @property
    def latest_signed(self):
        role_flows = FlowRole.objects.filter(flow=self, is_signed=True).order_by('-signed_at').all()
        if len(role_flows) > 0 and role_flows[0]:
            if role_flows[0].signed_by:
                return f"Signed By {role_flows[0].signed_by.code} at {role_flows[0].signed_at.strftime('%Y-%m-%d %H:%M:%S')}"
        return None


class FlowRole(models.Model):

    PROCESSING = 1
    PROCESSED = 2
    ACTION_REQUIRED = 3
    PARKED = 4

    # TODO - Remove these, using enums now
    STATUSES = (
        (PROCESSING, 'Processing'),
        (PROCESSED, 'Processed'),
        (ACTION_REQUIRED, 'Action Required'),
        (PARKED, 'Parked'),
    )

    flow = models.ForeignKey(Flow, related_name="roles", on_delete=models.CASCADE)
    role = models.ForeignKey(Role, related_name="flows", on_delete=models.CASCADE)
    status = EnumIntegerField(enums.FlowRoleStatus)
    is_signed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    signed_at = models.DateTimeField(null=True)
    signed_by = models.ForeignKey(Profile, related_name="profile_flow_signings", null=True, blank=True, on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(Profile, related_name="profile_flow_role_created", null=True, blank=True, on_delete=models.DO_NOTHING)
    reminded_at = models.DateTimeField(null=True)
    intervention = models.ForeignKey(Intervention, null=True, blank=True, on_delete=models.DO_NOTHING)

    class QS(models.QuerySet):
        def role_stats(self, role_id: int, year: Year):
            cache_key = f"{year.id if year else ''}_role_stats_{role_id}"
            role_stats = self.filter(
                role_id=role_id,
                flow__invoice__period__period_year__year__lte=year.year
            ).exclude(
                flow__invoice__status__slug__in=[enums.InvoiceStatus.DECLINE_PRINTED.value]
            ).aggregate(
                incoming=Count('pk', filter=Q(status=enums.FlowRoleStatus.PROCESSING, flow__is_current=False)),
                processing=Count('pk', filter=Q(status=enums.FlowRoleStatus.PROCESSING, flow__is_current=True))
            )

            cache.set(cache_key, role_stats)
            return role_stats

        def for_escalation(self):
            return self.filter(
                status=enums.FlowRoleStatus.PROCESSING, flow__is_current=True
            ).exclude(
                flow__invoice__invoice_type__code=enums.InvoiceType.PURCHASE_ORDER.value
            )

    objects = QS.as_manager()

    class Meta:
        indexes = [
            models.Index(fields=['role', 'is_signed'])
        ]

    def __str__(self):
        return f"{self.role.name}"

    @property
    def is_parked(self):
        return self.status == enums.FlowRoleStatus.PARKED

    @property
    def is_action_required(self):
        return self.status == enums.FlowRoleStatus.ACTION_REQUIRED

    @property
    def is_processing(self):
        return self.status == enums.FlowRoleStatus.PROCESSING

    @property
    def is_pending(self):
        return self.PROCESSING == self.status and self.flow.is_current

    @property
    def is_processed(self):
        return enums.FlowRoleStatus.PROCESSED == self.status

    @property
    def can_delete(self):
        return not (self.status in [enums.FlowRoleStatus.PROCESSED])

    def delete_flow_role(self):
        flow = self.flow
        flow_roles_count = flow.roles.count()

        self.delete()
        if flow_roles_count == 1:
            flow.delete()


class Journal(models.Model):

    INCOMING = 0
    CANCELLED = 1
    POSTING = 2
    PAYMENTS = 3
    DISTRIBUTION = 4

    JOURNAL_TYPES = (
        (INCOMING, 'Journal for incoming invoices'),
        (CANCELLED, 'Journal for cancelled invoices'),
        (POSTING, 'Journal for posting invoices'),
        (PAYMENTS, 'Journal for payments'),
        (DISTRIBUTION, 'Journal for distributions')
    )

    company = models.ForeignKey(Company, related_name='journals', on_delete=models.CASCADE)
    journal_id = models.PositiveIntegerField(null=True)
    period = models.ForeignKey(Period, on_delete=models.DO_NOTHING)
    report_unique_number = models.CharField(max_length=255)
    date = models.DateField(null=True, blank=True)
    journal_type = EnumIntegerField(enums.JournalType)
    ledger = models.ForeignKey(Ledger, related_name="journal_ledger", blank=True, null=True, on_delete=models.DO_NOTHING)
    role = models.ForeignKey(Role, verbose_name="journals", blank=True, null=True, on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(Profile, related_name='journals', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)

    class QS(models.QuerySet):

        def payments(self, company, year=None):
            q = Q()
            q.add(Q(company=company), Q.AND)
            q.add(Q(journal_type=enums.JournalType.PAYMENTS), Q.AND)
            if year:
                q.add(Q(period__period_year=year), Q.AND)
            return self.filter(q)

        def incoming(self):
            return self.filter(journal_type=enums.JournalType.INCOMING)

        def posting(self):
            return self.filter(journal_type=enums.JournalType.POSTING)

    objects = QS.as_manager()

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Invoice Journal')
        verbose_name_plural = _('Invoice Journals')

    def __str__(self):
        return f"{self.get_text()} {self.journal_id}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.journal_id:
            journal_count = Journal.objects.filter(
                company=self.company, journal_type=self.journal_type, period__period_year=self.period.period_year
            ).count()
            self.journal_id = journal_count + 1
        if not self.report_unique_number:
            text = self.get_text()
            self.report_unique_number = f"{text} {self.journal_id}"
        return super(Journal, self).save(*args, **kwargs)

    def generate_unique_number(self, company, profile, counter=1):
        counter_str = str(counter).rjust(5, '0')
        unique_number = f"{company.slug}-{profile.code}-JII-{counter_str}"

        result = Journal.objects.filter(report_unique_number=unique_number, journal_type=self.journal_type,
                                        company=company).first()
        if result:
            counter = counter + 1
            return self.generate_unique_number(company, profile, counter)
        else:
            return unique_number

    def get_text(self):
        if self.journal_type == enums.JournalType.INCOMING:
            return 'Incoming Invoice - Report'
        elif self.journal_type == enums.JournalType.POSTING:
            return 'Posting of Invoice - Report'
        elif self.journal_type == enums.JournalType.PAYMENTS:
            return 'Update Payments - Report'
        return 'Report'

    @property
    def creator(self):
        profile_str = role_str = ''
        if self.created_by:
            profile_str = f'{self.created_by}'
        if self.role:
            role_str = f'{self.role}'
        return f"{profile_str}-{role_str}"

    @property
    def ledgers(self):
        return {invoice_journal.ledger: invoice_journal.ledger for invoice_journal in self.journal_invoices.all()}


class InvoiceJournal(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='invoice_journals', on_delete=models.CASCADE)
    journal = models.ForeignKey(Journal, related_name='journal_invoices', on_delete=models.CASCADE)
    vat_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    total_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    net_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    invoice_type = models.ForeignKey(InvoiceType, related_name='journals', blank=True, null=True, on_delete=models.SET_NULL, verbose_name='Document Types')
    supplier = models.ForeignKey('supplier.Supplier', related_name='invoice_journals', null=True, blank=True, on_delete=models.SET_NULL)
    supplier_name = models.CharField(max_length=255, null=True, blank=True)
    supplier_number = models.CharField(max_length=255, blank=True)
    invoice_number = models.CharField(max_length=255, blank=True, default='', verbose_name='Document Number')
    order_number = models.CharField(max_length=255, blank=True)
    invoice_date = models.DateField(null=True, blank=True, verbose_name='Document Date')
    accounting_date = models.DateField(null=True, blank=True)
    vat_number = models.CharField(max_length=255, blank=True)
    invoice_status = models.ForeignKey(Status, related_name='status_journals', blank=True, null=True, on_delete=models.SET_NULL)
    linked_journal = models.ForeignKey('self', related_name='linked_invoice_journal', blank=True, null=True, on_delete=models.SET_NULL)
    vat_code = models.ForeignKey(VatCode, related_name='journal_vat_code', null=True, blank=True, on_delete=models.SET_NULL)
    ledger = models.ForeignKey('journals.Journal', null=True, related_name='+', on_delete=models.SET_NULL)

    class QS(models.QuerySet):

        def payments(self, company, year=None):
            q = Q()
            q.add(Q(journal__company=company), Q.AND)
            q.add(Q(journal__journal_type=enums.JournalType.PAYMENTS), Q.AND)
            if year:
                q.add(Q(journal__period__period_year=year), Q.AND)
            return self.filter(q)

        def incoming(self):
            return self.filter(journal__journal_type=enums.JournalType.INCOMING)

        def posting(self):
            return self.filter(journal__journal_type=enums.JournalType.POSTING)

    objects = QS.as_manager()

    @property
    def is_credit_type(self):
        if self.invoice_type:
            return self.invoice_type.is_credit
        return False

    @property
    def net(self):
        return self.net_amount

    @property
    def vat(self):
        return self.vat_amount

    @property
    def total(self):
        return self.total_amount


# TODO - Refactor this to a generic model
class Comment(models.Model):
    """
    Records what has happened and when in a particular run of a workflow. The
    latest record for the referenced WorkflowActivity will indicate the current
    state.
    """

    # The sort of things we can log in the workflow history
    TRANSITION = 1
    EVENT = 2
    ROLE = 3
    COMMENT = 4
    LINK = 5

    # Used to indicate what sort of thing we're logging in the workflow history
    TYPE_CHOICE_LIST = (
        (TRANSITION, 'Transition'),
        (EVENT, 'Event'),
        (ROLE, 'Role'),
        (COMMENT, 'Comment'),
        (LINK, 'Invoice Link')
    )
    # TODO Add generic fields so we can save any comment for any model in the invoice app
    invoice = models.ForeignKey(Invoice, related_name='invoice_comments', blank=True, null=True, on_delete=models.CASCADE)
    invoice_flow = models.ForeignKey(Flow, related_name='flow_comment', blank=True, null=True, on_delete=models.CASCADE)
    linked_invoice = models.ForeignKey(Invoice, related_name='comment_linked_invoices', blank=True, null=True, on_delete=models.CASCADE)
    link_url = models.TextField(max_length=255, blank=True, null=True)
    role = models.ForeignKey(Role, null=True, blank=True, help_text=_('The role who triggered this happening in the workflow history'),
                             on_delete=models.DO_NOTHING)
    log_type = models.IntegerField(help_text=_('The sort of thing being logged'), choices=TYPE_CHOICE_LIST)
    participant = models.ForeignKey(User, null=True, blank=True, help_text=_('The participant who triggered this happening in the workflow history'),
                                    on_delete=models.DO_NOTHING)
    created_by = models.CharField(max_length=255, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    note = models.TextField(_('Note'), blank=True)
    status = models.ForeignKey(Status, related_name='comment_invoice_status', null=True, blank=True, help_text=_('Invoice status'),
                               on_delete=models.DO_NOTHING)
    attachment = models.FileField(upload_to='uploads', null=True, blank=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Comments')
        verbose_name_plural = _('Comments')

    def __str__(self):
        return f"{self.note} created by {self.participant}"

    @property
    def is_linked_invoice(self):
        return self.log_type == Comment.LINK and self.linked_invoice

    @property
    def by(self):
        if self.created_by:
            return self.created_by
        elif self.participant:
            return "{}-{}".format(self.role, self.participant)

    # @property
    # def can_delete(self):
    #     if self.log_type == Comment.COMMENT:
    #         if self.role and self.role_id == self.request.session['role']:
    #             can_edit_delete = True


class PaymentQuerySet(models.QuerySet):

    def company(self, company):
        return self.filter(company=company)

    def open(self, supplier):
        return self.annotate(amount=F('balance') * -1).filter(supplier=supplier).exclude(balance=0)


class PaymentManager(models.Manager):

    # def annotate_sum_for_invoices(self, related_model_class, field_name, annotation_name):
    #     raw_query = "SELECT SUM{field} FROM {model} WHERE model.payment_id == payment.id".format(
    #         field=field_name, model=related_model_class._meta.db_table
    #     )
    #     annotation = {annotation_name: RawSQL(raw_query, [])}
    #     return self.annotate(**annotation)

    def for_supplier_age(self, company, from_date=None, to_date=None, is_last=False):
        return self.values(
            'supplier'
        ).annotate(
            total_open=Sum('balance')
        ).filter(
            company=company, supplier_id=models.OuterRef('id')
        ).exclude(balance=0).order_by().values_list('total_open')

    def calculate_available(self):
        return self.get_queryset().annotate(amount_available=F('balance') * -1)

    def due_for_payment(self, company, supplier_id=None, end_date=None):
        q = Q()
        q.add(Q(company=company, unpaid_total__gt=0), Q.AND)
        q.add(~Q(balance=0), Q.AND)
        if supplier_id:
            q.add(Q(supplier_id=supplier_id), Q.AND)
        if end_date:
            q.add(Q(payment_date__lte=end_date), Q.AND)
        return self.annotate(
            invoices_total=Coalesce(models.Sum('invoices__allocated'), Decimal('0')),
            payments_total=Coalesce(models.Sum('parent__amount'), Decimal('0')),
            allocated_total=models.F('invoices_total') + models.F('payments_total'),
            unpaid_total=models.F('total_amount') - models.F('allocated_total'),
            unpaid_amount=models.F('unpaid_total') * -1
        ).select_related(
            'company', 'supplier', 'period__period_year', 'role', 'profile'
        ).prefetch_related(
            'invoices', 'children', 'parent'
        ).filter(q)

    def due_for_payment_by_supplier(self, company, supplier_id=None, end_date=None):
        q = Q()
        q.add(Q(company=company, unpaid_total__gt=0), Q.AND)
        q.add(~Q(balance=0), Q.AND)
        if supplier_id:
            q.add(Q(supplier_id=supplier_id), Q.AND)
        if end_date:
            q.add(Q(payment_date__lte=end_date), Q.AND)
        return self.annotate(
            invoices_total=Coalesce(models.Sum('invoices__allocated'), Decimal('0')),
            payments_total=Coalesce(models.Sum('parent__amount'), Decimal('0')),
            allocated_total=models.F('invoices_total') + models.F('payments_total'),
            unpaid_total=models.F('total_amount') - models.F('allocated_total'),
            unpaid_amount=models.F('unpaid_total') * -1
        ).select_related(
            'supplier'
        ).filter(q).values(
            'supplier__code', 'supplier__name', 'unpaid_amount'
        )

    def available(self, company, supplier):
        return self.calculate_available().select_related(
            'company', 'supplier'
        ).prefetch_related(
            'invoices', 'children'
        ).filter(
            company=company, supplier=supplier
        ).exclude(
            amount_available=0
        )

    def with_totals(self, filter_options):
        return self.calculate_available().select_related(
            'company', 'supplier'
        ).prefetch_related(
            'invoices', 'children'
        ).filter(
            **filter_options
        )

    def with_available_amount(self, filter_options):
        return self.with_totals(filter_options).filter(balance__gt=0)

    def pending_updates(self, company, year=None, period=None, start_date=None, end_date=None):
        q = Q()
        q.add(Q(company=company), Q.AND)
        q.add(Q(journal__isnull=True), Q.AND)
        if start_date:
            q.add(Q(payment_date__gte=start_date), Q.AND)
        if end_date:
            q.add(Q(payment_date__lte=end_date), Q.AND)
        if period:
            q.add(Q(period=period), Q.AND)
        if year:
            q.add(Q(period__period_year__year__lte=year.year), Q.AND)

        return self.prefetch_related('invoices', 'invoices__invoice__status').filter(q)

    def create_child_payment(self, parent_payment, payment_id, amount):
        payment = Payment.objects.filter(id=payment_id).first()
        payment_amount = Decimal(amount)
        if payment and payment_amount:
            child_payment = ChildPayment.objects.create(parent=parent_payment, payment=payment, amount=payment_amount)
            payment.balance = payment.balance - child_payment.amount
            payment.save()
            return child_payment
        return None


def upload_payment_path(instance, filename):
    url, _ = utils.payments_upload_path(payment=instance, filename=filename)
    return url


class Payment(models.Model):

    PAYMENT = 0
    ALLOCATION = 1
    SUNDRY = 2

    PAYMENT_TYPES = (
        (PAYMENT, 'Payment'),
        (ALLOCATION, 'Allocation'),
        (SUNDRY, 'Sundry')
    )

    company = models.ForeignKey(Company, related_name='company_payments', on_delete=models.PROTECT)
    supplier = models.ForeignKey('supplier.Supplier', related_name='supplier_payments', on_delete=models.PROTECT)
    period = models.ForeignKey(Period, related_name='period_payments', on_delete=models.PROTECT)
    payment_id = models.PositiveIntegerField(null=True, blank=True)
    reference = models.CharField(max_length=255, blank=True)
    role = models.ForeignKey(Role, related_name='role_payments', on_delete=models.PROTECT)
    profile = models.ForeignKey(Profile, related_name='profile_payments', on_delete=models.PROTECT)
    payment_date = models.DateField(null=True, blank=True, verbose_name='Payment Date')
    payment_method = models.ForeignKey(PaymentMethod, null=True, verbose_name='Payment From', on_delete=models.PROTECT)
    adjustment_account = models.ForeignKey(Account, null=True, blank=True, on_delete=models.SET_NULL)
    discount_disallowed = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    remittance_text = models.TextField(verbose_name='Text on Remittance', blank=True)
    memo = models.TextField(blank=True,)
    comment = models.TextField(blank=True)
    account = models.ForeignKey(Account, related_name='payments', null=True, blank=True,
                                on_delete=models.PROTECT, verbose_name=_('Bank Account'))
    total_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    total_discount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    net_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    balance = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    filename = models.CharField(blank=True, max_length=255)
    attachment = models.FileField(upload_to=upload_payment_path, blank=True)
    payment_type = EnumIntegerField(enums.PaymentType, default=enums.PaymentType.PAYMENT)
    journal = models.ForeignKey('Journal', related_name='payments', null=True, blank=True, on_delete=models.SET_NULL)
    vat_code = models.ForeignKey('company.VatCode', related_name='vat_code_payments', null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = PaymentManager.from_queryset(PaymentQuerySet)()

    class Meta:
        ordering = ['created_at']
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')

    def __str__(self):
        if self.period:
            yr = str(self.period.period_year)[2:]
        else:
            yr = str(self.created_at.year)[2:]
        slug = ''
        if self.company:
            slug = self.company.slug
        return f"R{slug}-{yr}{str(self.payment_id).rjust(4, '0')}"

    def get_absolute_url(self):
        return reverse('payment_advice', kwargs={'payment_id': self.id})

    def description(self):
        currency_symbol = ''
        if self.company.currency:
            currency_symbol = self.company.currency.symbol
        if self.payment_type == self.ALLOCATION:
            amount = utils.money(self.amount_paid)
            description_str = "<strong> {} </strong > has made an allocation for the amount" "of <strong> {} {} " \
                              "</strong> on <strong> {} </strong>".format(self.company, currency_symbol, amount,
                                                                          self.payment_date)
        else:
            amount = utils.money(self.net_amount)
            description_str = "<strong> {} </strong > has made a payment by the way of <strong> {} </strong> for the " \
                              "amount of <strong> {} {} </strong> on <strong> {} </strong>".format(self.company,
                                                                                                   self.payment_method,
                                                                                                   currency_symbol,
                                                                                                   amount,
                                                                                                   self.payment_date)
        return description_str

    def generate_unique_number(self, company, counter=1):
        result = Payment.objects.filter(payment_id=counter, company=company).first()
        if result:
            counter = counter + 1
            return self.generate_unique_number(company, counter)
        else:
            return counter

    def save(self, *args, **kwargs):
        if not self.pk or not self.payment_id:
            payment_count = Payment.objects.filter(company=self.company).count()
            self.payment_id = self.generate_unique_number(self.company, payment_count)
        return super(Payment, self).save(*args, **kwargs)

    @property
    def payment_reference(self):
        return str(self)

    @property
    def total_allocated(self):
        paid_invoices = self.invoices.all()
        total_paid = 0
        for invoice_payment in paid_invoices:
            total_paid += invoice_payment.allocated
        child_payments = self.parent.all()
        for child_payment in child_payments:
            total_paid += child_payment.amount
        return total_paid

    @property
    def total_amount_paid(self):
        paid_invoices = self.invoices.all()
        total_paid = 0
        for invoice_payment in paid_invoices:
            total_paid += invoice_payment.allocated
        child_payments = self.children.all()
        for child_payment in child_payments:
            total_paid -= child_payment.amount
        return total_paid

    @property
    def total_paid(self):
        total_amount = self.total_amount
        total_amount += sum(ledger_payment.amount for ledger_payment in self.supplier_ledgers.all())
        return total_amount

    @property
    def available_amount(self):
        total_paid = self.total_allocated
        return self.total_amount - total_paid

    @property
    def amount_paid(self):
        if self.invoices.count() == 0:
            total_paid = self.total_amount
        else:
            paid_invoices = self.invoices.all()
            total_paid = 0
            for invoice_payment in paid_invoices:
                total_paid += invoice_payment.allocated
        return total_paid

    @property
    def display_available_amount(self):
        available = self.available_amount
        if available > 0:
            return available * -1
        return 0

    @property
    def displayable_balance(self):
        if self.balance > 0:
            return self.balance * -1
        return self.balance

    @property
    def is_open(self):
        if (self.period and self.period.is_locked) or self.journal:
            return False
        return True

    @property
    def net_discount(self):
        if self.discount_disallowed:
            net = float(self.discount_disallowed) - (float(self.vat_on_discount) * -1)
            return Decimal(net * -1)
        return 0

    @property
    def vat_on_discount(self):
        if self.discount_disallowed and self.vat_code:
            discounted_vat = (self.vat_code.percentage / 100) * float(self.discount_disallowed)
            return Decimal(discounted_vat * -1)
        return 0

    @property
    def payment_type_name(self):
        return str(self.payment_type)


class InvoicePayment(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='payments', on_delete=models.CASCADE)
    payment = models.ForeignKey(Payment, related_name='invoices', on_delete=models.CASCADE)
    allocated_reference = models.CharField(max_length=255, blank=True)
    allocated = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    discount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)

    @property
    def open(self):
        return self.invoice.total_amount - self.allocated

    @property
    def percentage_discount(self):
        if self.allocated > 0:
            return round((self.discount / self.allocated) * 100, 2)
        return 0

    @property
    def amount(self):
        if self.invoice.is_credit_type:
            if self.allocated < 0:
                return self.allocated
            return self.allocated * -1
        return self.allocated

    @property
    def net_amount(self):
        net_amount = self.allocated - (self.discount or 0)
        if self.invoice.is_credit_type:
            net_amount = net_amount * -1
        return net_amount

    @property
    def vat_on_discount(self):
        if self.invoice.vat_code:
            vat_code = self.invoice.vat_code
            discount_vat = round((vat_code.percentage / (100 + vat_code.percentage)) * float(self.discount or 0), 2)
            return discount_vat * -1
        return 0

    @property
    def net_discount(self):
        net = (float(self.discount or 0) * -1) - float(self.vat_on_discount)
        return net

    @property
    def net_payment(self):
        net = float(self.amount) + float(self.net_discount) + float(self.vat_on_discount)
        return net

    @property
    def vat(self):
        if self.invoice.vat_code:
            vat = self.invoice.vat_amount
            return vat
        return 0


class ChildPayment(models.Model):
    parent = models.ForeignKey(Payment, null=False, blank=False, related_name='children', on_delete=models.CASCADE)
    payment = models.ForeignKey(Payment, null=False, blank=False, related_name='parent', on_delete=models.CASCADE)
    amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)

    @property
    def amount_allocated(self):
        return self.amount * -1


auditlog.register(Invoice)
