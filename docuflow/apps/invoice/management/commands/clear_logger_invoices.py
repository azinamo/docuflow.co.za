from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils.timezone import now

from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Invoice


class Command(BaseCommand):
    help = "Clear the company invoices in logger statys, to restart the imports"

    def handle(self, *args, **kwargs):
        try:
            for company in Company.objects.all():
                month_ago_date = now() - timedelta(15)

                invoices = Invoice.objects.prefetch_related('flows').filter(
                    Q(company__slug=company.slug) | Q(company_id__isnull=True), status__slug='arrived-at-logger',
                    created_at__lte=month_ago_date, deleted__isnull=True
                )
                print(f"------START CLEARING FOR  {company.slug} ----- from {month_ago_date}. Found {invoices.count()}")
                invoices.delete()
                # print("\t\t----------FOUND {} INVOICES ------------".format(len(logger_invoices)))
                # invoice_ids = []
                # invoices = []
                # for invoice in logger_invoices:
                #     if invoice.created_at.date() == datetime.now().date():
                #         invoice_ids.append(invoice.id)
                #         invoices.append(invoice)
                total_invoices = 0

        except Exception as exception:
            print("Exception occurred {} ".format(exception))
