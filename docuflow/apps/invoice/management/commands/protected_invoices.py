from django.core.management.base import BaseCommand

from docuflow.apps.invoice.models import Invoice


class Command(BaseCommand):
    help = "Protected invoices"

    def handle(self, *args, **kwargs):
        for invoice in Invoice.objects.filter(invoice_type__name='Payroll'):
            invoice.is_protected = True
            invoice.save()
