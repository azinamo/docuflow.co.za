import pprint
from datetime import datetime
from calendar import monthrange

from django.core.management.base import BaseCommand

from docuflow.apps.invoice.models import Distribution, InvoiceAccount, DistributionAccount
from docuflow.apps.period.models import Year, Period

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Clear the company invoices, to restart the imports"

    def handle(self, *args, **kwargs):
        try:
            distributions = Distribution.objects.filter(id=13)
            pp.pprint(distributions)
            for distribution in distributions:
                months = distribution.nb_months
                start_date = distribution.starting_date
                invoice_accounts = InvoiceAccount.objects.filter(distribution=distribution)

                for invoice_account in invoice_accounts:
                    invoice_account_distributions = self.get_invoice_account_distributions(invoice_account, months,
                                                                                           start_date)

                    try:
                        for k, invoice_account_distribution in invoice_account_distributions['distributions'].items():
                            DistributionAccount.objects.create(
                                distribution=distribution,
                                account=invoice_account_distribution['distribution_account'],
                                credit=invoice_account_distribution['total'],
                                distribution_date=invoice_account_distribution['day'],
                                period=invoice_account_distribution['period']
                            )
                            for account_distribution in invoice_account_distribution['accounts']:
                                DistributionAccount.objects.create(
                                    distribution=distribution,
                                    account=account_distribution['account'],
                                    debit=account_distribution['amount'],
                                    distribution_date=invoice_account_distribution['day'],
                                    period=invoice_account_distribution['period']
                                )
                    except Exception as e:
                        print(e)

        except Exception as exception:
            print("Exception occurred {} ".format(exception))


    @staticmethod
    def calculate_total_credit(invoice_accounts):
        total = 0
        for acc in invoice_accounts:
            total += acc['amount']
        return total

    @staticmethod
    def get_distribution_invoice_accounts(invoice_account, nb_months, is_last_month):
        account = []
        prev_months_counter = (nb_months-1)
        if is_last_month:
            _accounts_total = round((invoice_account.amount/nb_months), 2) * prev_months_counter
            amount = round((invoice_account.amount - _accounts_total), 2)
        else:
            amount = invoice_account.amount/nb_months
        account.append({'amount': round(amount, 2),
                        'account': invoice_account.account,
                        'account_id': invoice_account.account.id,
                        'invoice_account_id': invoice_account.id
                        })
        return account

    def get_year(self, company):
        return Year.objects.filter(company=company).first()

    def get_invoice_account_distributions(self, invoice_account, months, start_date):
        company = invoice_account.invoice.company
        default_distribution_account = company.default_distribution_account
        year = self.get_year(company)
        current_month = start_date.month
        current_year = start_date.year
        day = start_date.day
        month_counter = 0
        accounts_distribution = {'distributions': {}, 'total_distribution': 0}
        counter = 1
        total_distribution = 0
        for month in range(months):
            _day = day
            _month = current_month + month_counter
            last_day = start_date.replace(year=current_year, month=_month, day=monthrange(current_year, _month)[1])
            if day > last_day.day:
                _day = last_day.day
            start_at = datetime.replace(start_date, current_year, _month, _day)
            is_last = (counter == months)

            distribution_invoice_accounts = self.get_distribution_invoice_accounts(invoice_account, months, is_last)
            total_credit = self.calculate_total_credit(distribution_invoice_accounts)
            total_distribution += total_credit
            period = self.calculate_period(company, year, start_at)
            accounts_distribution['distributions'][counter] = {'day': start_at,
                                                               'accounts': distribution_invoice_accounts,
                                                               'period': period,
                                                               'total': total_credit,
                                                               'distribution_account': default_distribution_account
                                                               }
            if _month == 12:
                current_month = 1
                month_counter = 0
                current_year = current_year + 1
            else:
                month_counter += 1
            counter += 1
        accounts_distribution['total_distribution'] = total_distribution
        return accounts_distribution

    def calculate_period(self, company, year, distribution_date):
        periods = Period.objects.filter(company=company, period_year=year)
        for period in periods:
            if period.from_date <= distribution_date.date() <= period.to_date:
                return period
