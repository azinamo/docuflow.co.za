import logging
from collections import defaultdict
import os

from django.core.management.base import BaseCommand
from django.conf import settings

from docuflow.apps.invoice.models import Invoice
from docuflow.apps.company.models import NonBankingDay, Company

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'To escalate a document that as per the EDIT ROLE screen, needs to be escalated to the supervisor'

    def handle(self, *args, **kwargs):
        for company in Company.objects.all():
            checksum_invoices = defaultdict(list)
            for invoice in Invoice.objects.filter(company=company, created_at__month=7, created_at__year=2022, deleted__isnull=True):
                checksum_invoices[invoice.checksum].append(invoice)

            if len(checksum_invoices) > 1:
                for checksum, invoices in checksum_invoices.items():
                    for invoice in invoices:
                        path = os.path.join(settings.MEDIA_ROOT, company.slug, str(invoice.id), invoice.filename)
                        if os.path.isfile(path):
                            self.stdout.write(self.style.SUCCESS(
                                f"File existing for checksum {checksum} -> {invoice} "))
                        else:
                            self.stdout.write(self.style.ERROR(
                                f"File not existing for checksum {checksum} -> {invoice.invoice_id} ({invoice.id})"
                                f" {invoice.filename} "))
                            invoice.delete()




