import os
import pprint
import logging
from shutil import copy

from django.conf import settings
from django.core.management.base import BaseCommand

from docuflow.apps.common.utils import upload_to_s3
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.invoice.utils import invoice_upload_path

pp = pprint.PrettyPrinter(indent=4)
log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Upload the invoice files to S3 if its not been uploaded yet"

    def add_arguments(self, parser):
        parser.add_argument('--invoice_id', nargs='*', default=None)

    def handle(self, *args, **kwargs):
        invoices_qs = Invoice.objects.select_related('company').exclude(invoice_type__code='purchase-order').order_by('-id')
        if kwargs['invoice_id']:
            invoices_qs = invoices_qs.filter(pk__in=kwargs['invoice_id'])
        else:
            invoices_qs = invoices_qs.exclude(ocr_status=Invoice.COMPLETED)

        for invoice in invoices_qs:
            url, path = invoice_upload_path(invoice=invoice, filename=invoice.filename)
            print(f"Processing invoice {invoice}({invoice.id}) -> file  {url} {path} ....")
            if not (url and path):
                continue

            dest = os.path.join(path, invoice.filename)
            if not (os.path.exists(path) and os.path.isfile(dest)):
                file_url = invoice.file_url
                file_path = invoice.location
                if os.path.exists(file_path) and os.path.isfile(file_path):
                    print(f"Try copy file from  {file_path} -> {dest} ....{os.path.exists(file_path)} {os.path.isfile(file_path)}")
                    copy(src=file_path, dst=dest)

            if os.path.exists(path) and os.path.isfile(dest):
                print(f"Uploading file {invoice.filename} ....")
                try:
                    upload_to_s3(
                        file_path=dest,
                        filename=url
                    )
                    invoice.ocr_status = Invoice.COMPLETED
                    invoice.save()
                except Exception as exception:
                    log.exception(exception)
                    print(f"Exception occurred {exception} ")
