import logging
import pprint

from django.core.management.base import BaseCommand

from docuflow.apps.invoice.models import Invoice
from docuflow.apps.invoice.utils import process_aws_ocr

pp = pprint.PrettyPrinter(indent=4)
log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Upload the invoice files to S3 if its not been uploaded yet"

    def add_arguments(self, parser):
        parser.add_argument('invoice_id', nargs='*', default=None)

    def handle(self, *args, **kwargs):
        invoice = Invoice.objects.select_related('company').exclude(invoice_type__code='purchase-order').order_by('-id').first()

        process_aws_ocr(
            invoice=invoice
        )
