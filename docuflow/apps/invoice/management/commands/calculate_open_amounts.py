import pprint

from django.contrib.contenttypes.fields import ContentType
from django.core.management.base import BaseCommand
from django.db import transaction

from docuflow.apps.invoice.models import Invoice, Journal
from docuflow.apps.company.models import Company, Account, VatCode
from docuflow.apps.period.models import Year, Period
from docuflow.apps.journals.models import JournalLine, Journal as Ledger


pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Calculates the open balance of an invoice"

    def add_arguments(self, parser):
        parser.add_argument('-c', '--supplier_id', type=str, help='Indicates the supplier invoices open amount to be calculated')
        parser.add_argument('-i', '--invoice_id', type=str, help='Indicates the invoice open amount to be calculated')
        parser.add_argument('-cc', '--company_id', type=str, help='Indicates the company invoice open amount to be calculated')

    def handle(self, *args, **kwargs):
        try:
            # slug = '41054'
            # period = Period.objects.get(period=1, period_year__year=2021, period_year__company__slug=slug)
            # accounts = list(set([invoice.vat_code.account for invoice in Invoice.objects.filter(company__slug=slug, period=period) if invoice.vat_code]))
            # print(accounts)
            # for journal_line in JournalLine.objects.filter(
            #     journal__period=period,
            #     content_type=ContentType.objects.get_for_model(model=Journal, for_concrete_model=False),
            #     account__in=accounts,
            #     vat_code__isnull=True
            # ):
            #     jnl = Journal.objects.filter(pk=journal_line.object_id).first()
            #     print(f"Journal {journal_line.journal_id} ({journal_line.id}) with an Credit of {journal_line.credit} and "
            #           f"Debit of {journal_line.debit}  is missing the vat code --> "
            #           f"{journal_line.journal.journal_number} {journal_line.journal.created_at}"
            #           f" --> created by {jnl} has {jnl.journal_invoices.count()}")
            #     print("-------------------------------")
            #     journal_line.vat_code = VatCode.objects.get(account=journal_line.account)
            #     journal_line.save()

            invoice_id = kwargs.get('invoice_id', None)
            company_id = kwargs.get('company_id', None)
            with transaction.atomic():
                statuses = ['in-the-flow', 'final-signed', 'end-flow-printed']
                statuses = []
                qc = Company.objects.filter(pk=11)
                if company_id:
                    qc.filter(pk=company_id)
                for company in qc:
                    current_year = Year.objects.open(company).first()
                    if current_year:
                        q = Invoice.objects.filter(company=company,
                                                   period__period_year=current_year,
                                                   accounting_date__month__lte=8
                                                   ).exclude(open_amount=0)

                        if invoice_id:
                            q = q.filter(pk=invoice_id)
                        print(f"\t\t----------{company}-> FOUND {q.count()} INVOICES for year {current_year} --")
                        for invoice in q:
                            if invoice.invoice_type.is_account_posting:
                                open_amount = invoice.calculate_open_amount()
                                print(f"<-- accounting date {invoice.accounting_date} Calculate open amount --> {invoice}({invoice.id}) => {open_amount}")
                                invoice.open_amount = 0
                                invoice.save()

        except Exception as exception:
            print("Exception occurred {} ".format(exception))
