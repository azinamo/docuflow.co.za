import os
import pprint

from django.conf import settings
from django.core.management.base import BaseCommand

from docuflow.apps.common.utils import upload_to_s3, create_dir
from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Payment
from docuflow.apps.invoice.utils import payments_upload_path

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Calculates the invoice files"

    def handle(self, *args, **kwargs):
        try:
            for company in Company.objects.exclude(slug='41054'):
                for payment in Payment.objects.select_related('company').filter(company=company).exclude(attachment='').order_by('-id').all():
                    filename = os.path.basename(payment.attachment.path)
                    original_src = os.path.join(settings.MEDIA_ROOT, 'uploads', filename)
                    print(f"Processing file {payment.attachment.path} ....")
                    if os.path.exists(payment.attachment.path) and os.path.isfile(payment.attachment.path):
                        print(f"Uploading file {payment.attachment.path} ....")
                        upload_to_s3(
                            file_path=payment.attachment.path,
                            filename=payments_upload_path(payment=payment, filename=filename)
                        )
                        payment.filename = filename

                        if 'uploads' in payment.attachment.path:
                            destination_dir = os.path.join(settings.MEDIA_ROOT, payment.company.slug, 'payments',
                                                       str(payment.id))

                            create_dir(destination_dir)

                            destination = os.path.join(settings.MEDIA_ROOT, payment.company.slug, 'payments', str(payment.id), filename)

                            print(f"Move {filename} to {destination} -> {os.path.exists(destination)}")
                            if os.path.exists(destination):
                                os.rename(original_src, destination)

                                payment.attachment = payments_upload_path(payment=payment, filename=filename)
                        payment.save()

        except Exception as exception:
            print(f"Exception occurred {exception} ")
