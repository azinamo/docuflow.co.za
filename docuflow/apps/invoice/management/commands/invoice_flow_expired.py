import logging
from datetime import datetime, timedelta, timezone

from django.core.management.base import BaseCommand
from django.core.mail import send_mail

from annoying.functions import get_object_or_None

from docuflow.apps.invoice.models import FlowRole

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Invoice flow expired"

    def notify_role(self, invoice, role):
        emails = []

        for role_profile in role.profiles.all():
            profile = role_profile.profile
            subject = 'Invoice processing expired'
            from_addr = 'no-reply@docuflow.co.za'
            recipient_list = ('admire@localhost.co.za', 'admire@originate.co.za', profile.user.email)
            message = f'Hey {profile.user.first_name} you have this invoice {invoice} in your flow that requires' \
                      f' processing. Your supervisor has been notified that you are behind on processing this. \r\n' \
                      'Please process it within 24hours or the invoice will be forwarded to your supervisor ' \
                      'for processing'
            send_mail(subject, message, from_addr, recipient_list)

    def handle(self, *args, **kwargs):
        subject = 'Invoice processing expired test'
        from_addr = 'no-reply@docuflow.co.za'
        recipient_list = ('admire@localhost.co.za', 'admire@originate.co.za', )
        message = 'Hey we are testing the crons you have this invoice in your flow that requires processing. \r\n' \
                  'Your supervisor has been notified that you are behind on processing this. \r\n' \
                  'Please process it within 24hours or the invoice will be forwarded to your supervisor ' \
                  'for processing'
        send_mail(subject, message, from_addr, recipient_list)

        max_days = 2
        invoice_flow_roles = FlowRole.objects.filter(status=FlowRole.PROCESSING).all()
        counter = len(invoice_flow_roles)
        logger.info(f"found {counter} invoice flow roles in the processing status ")
        date_n_days_ago = datetime.now() - timedelta(days=max_days)

        for invoice_flow_role in invoice_flow_roles:
            delta = datetime.now(timezone.utc) - invoice_flow_role.created_at
            role = invoice_flow_role.role
            logger.info(f"{role} has overdue invoice. {delta.days} Days invoice has been in inbox vs {max_days} it  "
                        f"should be in invoice")
            # Role has invoice in inbox for x-days. (According to period of time set in Role setup)
            if delta.days == max_days:
                if not invoice_flow_role.reminded_at:
                    self.notify_role(invoice_flow_role.flow.invoice, role)
                    invoice_flow_role.reminded_at = datetime.now(timezone.utc)
                    invoice_flow_role.save()
            elif delta.days > max_days or delta.days == 0:
                logger.info("Move invoice role status to Action required and create supervisor role")
                invoice_flow_role.status = FlowRole.ACTION_REQUIRED
                invoice_flow_role.save()

                # add supervisor role to the level and with a status of processing
                if role.supervisor_role:
                    supervisor_flow_role = get_object_or_None(FlowRole, flow=invoice_flow_role.flow,
                                                              role=role.supervisor_role)
                    logger.info(f"If role {role.supervisor_role} does not exist in flow, add it")
                    if not supervisor_flow_role:
                        supervisor_flow_role = FlowRole.objects.create(
                            role=role.supervisor_role,
                            status=FlowRole.PROCESSING,
                            flow=invoice_flow_role.flow
                        )
                        logger.info(f"Added new flow role {supervisor_flow_role}")

