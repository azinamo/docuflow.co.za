from django.core.management.base import BaseCommand

from django.db import connection

from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import InvoicePayment, Invoice, Journal, Distribution, Payment


class Command(BaseCommand):
    help = "Clear the company journals, to restart the imports"

    def handle(self, *args, **kwargs):
        try:
            slugs = []
            companies = Company.objects.filter(slug__in=slugs)
            # for company in companies:
            #
            #     payments = Payment.objects.filter(company=company, period__period_year__year=2020)
            #
            #     print(f" found {payments.count()} payments")
            #     for payment in payments:
            #         invoices_payments = InvoicePayment.objects.filter(payment_id=payment.id)
            #
            #         total = sum(invoices_payment.allocated for invoices_payment in invoices_payments if invoices_payment.allocated)
            #         if payment.total_amount == 0 and total > 0:
            #             print(f"No mismatch {payment.id} for supplier {payment.supplier} allocated and total {payment.total_amount} vs {total} \r\n")
            #             payment.total_amount = total
            #             payment.save()

                #     with connection.cursor() as cursor:
                #
                #         print('\t\t\t\t\t\t UPDATE invoice_invoice SET payment_id = NULL '
                #               'WHERE payment_id = {}'.format(payment.id))
                #         cursor.execute('UPDATE invoice_invoice SET payment_id = NULL WHERE'
                #                        ' payment_id = {}'.format(payment.id))
                #
                # with connection.cursor() as cursor:
                #     print("Deleting company payment \r\n")
                #     print('\t\t\t\t\t\t\t\t\t\t\t\tDELETE FROM invoice_payment WHERE company_id = {}'.format(company.id))
                #     cursor.execute('DELETE FROM invoice_payment WHERE company_id = {}'.format(company.id))

        except Exception as exception:
            print("Exception occurred {} ".format(exception))
