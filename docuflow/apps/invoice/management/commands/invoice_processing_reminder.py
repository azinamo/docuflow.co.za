import logging
from datetime import datetime, timezone, date

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from django.template.loader import get_template

from docuflow.apps.accounts.models import Profile
from docuflow.apps.company.models import NonBankingDay, Company
from docuflow.apps.invoice.models import FlowRole
from docuflow.apps.invoice.utils import is_weekend

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "To email reminder to each user on a role, where a document has not been processed within the given times " \
           "as set up in “Roles”"

    @staticmethod
    def is_non_banking_day(company):
        today = date.today()
        return NonBankingDay.objects.filter(company=company, date=today).exists()

    def can_send_reminder(self, company):
        return not (is_weekend() or self.is_non_banking_day(company))

    @staticmethod
    def notify_role(company, invoices, role_id):
        emails = []
        profiles = Profile.objects.filter(roles__in=[role_id])
        if profiles:
            for profile in profiles:
                logger.info(f"sending reminder to profile {profile.user.email} with role id {role_id}")
                subject = f'Reminder from DocuFlow-{company}'
                from_address = settings.DEFAULT_FROM_EMAIL
                recipient_list = [profile.user.email]
                template = get_template('invoice/email/processing_reminder.html')
                context = {'invoices': invoices, 'profile': profile, 'company': company}

                message = template.render(context)
                msg = EmailMultiAlternatives(subject, message, from_address, recipient_list, bcc=settings.ADMIN_EMAIL)
                msg.attach_alternative(message, "text/html")
                msg.send()

    @staticmethod
    def get_role_invoice_flows(company):
        return FlowRole.objects.filter(status=FlowRole.PROCESSING, flow__invoice__company=company).all()

    @staticmethod
    def is_overdue(role, flow_role):
        max_days = role.send_reminder_after_inbox
        if not max_days:
            return False
        delta = datetime.now(timezone.utc) - flow_role.created_at
        if delta.days >= max_days:
            return True
        return False

    @staticmethod
    def prepare_invoice(invoice):
        return {'invoice_unique': invoice, 'supplier': invoice.supplier.name, 'due_date': invoice.due_date,
                'comments': invoice.comments, 'amount': invoice.total_amount, 'currency': invoice.currency_code}

    def get_roles_invoices(self, company):
        invoice_flow_roles = self.get_role_invoice_flows(company)
        roles_invoices = {}
        for invoice_flow_role in invoice_flow_roles:
            role = invoice_flow_role.role
            if role.id not in roles_invoices:
                roles_invoices[role.id] = {'invoices': {}}

            if self.is_overdue(role, invoice_flow_role):
                if not invoice_flow_role.reminded_at:
                    # Role has invoice in inbox for x-days. (According to period of time set in Role setup)
                    # if not invoice_flow_role.reminded_at:
                    invoice = invoice_flow_role.flow.invoice
                    if invoice.id not in roles_invoices[role.id]['invoices']:
                        roles_invoices[role.id]['invoices'][invoice.id] = self.prepare_invoice(invoice)
                    invoice_flow_role.reminded_at = datetime.now(timezone.utc)
                    invoice_flow_role.save()
        return roles_invoices

    # noinspection PyMethodMayBeStatic
    def notify_reminder(self):
        subject = 'Reminder email'
        from_addr = 'no-reply@docuflow.co.za'
        recipient_list = ('admire@originate.co.za',)
        message = 'Reminder emails send to the users and their profiles'
        send_mail(subject, message, from_addr, recipient_list)

    def handle(self, *args, **kwargs):
        companies = Company.objects.all()
        for company in companies:
            can_send_reminder = self.can_send_reminder(company)
            logger.info(f"Sending reminder to company {company} and can send {can_send_reminder}")
            if can_send_reminder:

                roles_invoices = self.get_roles_invoices(company)
                logger.info(f"Found {len(roles_invoices)} role invoices pending")
                for role_id, role_invoices in roles_invoices.items():
                    if len(role_invoices['invoices']) > 0:
                        logger.info(f"Send reminder now for role id {role_id}")
                        self.notify_role(company, role_invoices['invoices'], role_id)

                        self.stdout.write(self.style.SUCCESS(f"Sending reminder to {role_id}"))
        self.notify_reminder()

