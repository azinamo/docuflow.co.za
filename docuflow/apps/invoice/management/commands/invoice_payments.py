import pprint
from decimal import Decimal
from collections import defaultdict
from django.core.management.base import BaseCommand
from django.db import transaction
from annoying.functions import get_object_or_None

from docuflow.apps.period.models import Period, Year
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.company.models import Company, Account, PaymentMethod
from docuflow.apps.invoice.models import Invoice, Journal, Payment, InvoicePayment, Status
pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Clear the company journals, to restart the imports"

    def handle(self, *args, **kwargs):
        try:
            with transaction.atomic():
                slugs = ['41054']
                companies = Company.objects.filter(slug__in=slugs)
                paid_printed_status = get_object_or_None(Status, slug='paid-printed')

                supplier_invoices = defaultdict(list)
                for company in companies:
                    print("\t\t----------{}-> FOUND {} INVOICES ------------".format(company, company.invoices.count()))
                    unpaid = 0
                    for invoice in company.invoices.filter(status=paid_printed_status).all():
                        invoice_payment = get_object_or_None(InvoicePayment, invoice=invoice)
                        if not invoice_payment and (invoice.payment_date or invoice.date_paid):
                            supplier_invoices[invoice.supplier].append(invoice)
                            unpaid += 1

                    self.stdout.write(self.style.SUCCESS("{} invoice are paid without payment history -> {}".format(unpaid, company)))

                self.create_supplier_payments(company, supplier_invoices)
        except Exception as exception:
            print("Exception occurred {} ".format(exception))

    def create_supplier_payments(self, company, company_invoices):
        payment_account = Account.objects.filter(company=company).first()
        payment_method = PaymentMethod.objects.filter(company=company).first()
        profile = Profile.objects.filter(company=company).first()
        role = Role.objects.filter(company=company).first()
        for supplier, supplier_invoices in company_invoices.items():
            date_invoices = defaultdict(list)
            date_invoice_totals = defaultdict(Decimal)
            for supplier_invoice in supplier_invoices:
                if supplier_invoice.payment_date:
                    date_invoices[supplier_invoice.payment_date].append(supplier_invoice)
                    date_invoice_totals[supplier_invoice.payment_date] += supplier_invoice.total_amount
                elif supplier_invoice.date_paid:
                    date_invoices[supplier_invoice.date_paid].append(supplier_invoice)
                    date_invoice_totals[supplier_invoice.date_paid] += supplier_invoice.total_amount

            for payment_date, invoices in date_invoices.items():
                period = Period.objects.get_by_date(company, payment_date).first()
                if len(invoices) > 0:
                    payment = Payment.objects.create(
                        payment_date=payment_date,
                        supplier=supplier,
                        total_amount=date_invoice_totals[payment_date],
                        net_amount=date_invoice_totals[payment_date],
                        company=supplier.company,
                        account=payment_account,
                        period=period,
                        payment_method=payment_method,
                        profile_id=profile.id,
                        role_id=role.id,
                    )
                    print("Payment {} created ".format(payment))
                    if payment:
                        for invoice in invoices:
                            InvoicePayment.objects.create(
                                payment=payment,
                                invoice=invoice,
                                allocated=invoice.total_amount
                            )
