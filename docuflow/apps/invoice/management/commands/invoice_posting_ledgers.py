from django.core.management.base import BaseCommand

from django.db import connection

from docuflow.apps.company.models import Company
from docuflow.apps.invoice.exceptions import InvoiceException
from docuflow.apps.invoice.models import InvoicePayment, InvoiceJournal, Journal, Payment
from docuflow.apps.invoice.modules.posting.services import CreatePostingUpdate
from docuflow.apps.supplier.services import is_supplier_balancing


class Command(BaseCommand):
    help = "Clear the company journals, to restart the imports"

    def handle(self, *args, **kwargs):
        slugs = []
        companies = Company.objects.filter(slug__in=['41054'])
        for company in companies:
            invoices_journals = InvoiceJournal.objects.posting().select_related('journal').filter(
                invoice__company=company,
                ledger__isnull=True,
                invoice__period__period_year__year=2023
            )

            for invoice_journal in invoices_journals:
                posting_update = CreatePostingUpdate(
                    company=company,
                    profile=invoice_journal.journal.created_by,
                    role=invoice_journal.journal.role,
                    update=invoice_journal.journal
                )
                posting_update.create_ledger(
                    update=invoice_journal.journal,
                    selected_invoices=[invoice_journal.invoice]
                )
                is_balancing = is_supplier_balancing(
                    company=company,
                    year=invoice_journal.journal.period.period_year,
                    end_date=invoice_journal.journal.date,
                    period=invoice_journal.journal.period
                )

                if not is_balancing:
                    raise InvoiceException('A variation on supplier age and balance sheet found')
