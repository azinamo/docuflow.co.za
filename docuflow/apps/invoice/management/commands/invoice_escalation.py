import logging
from datetime import datetime, date

from django.core.management.base import BaseCommand
from django.core.mail import send_mail

from docuflow.apps.invoice.models import FlowRole
from docuflow.apps.company.models import NonBankingDay, Company
from docuflow.apps.invoice.utils import is_weekend

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'To escalate a document that as per the EDIT ROLE screen, needs to be escalated to the supervisor'

    def is_non_banking_day(self, company):
        today = date.today()
        return NonBankingDay.objects.filter(company=company, date=today).exists()

    def can_escalate_invoices(self, company):
        return not (is_weekend() or self.is_non_banking_day(company))

    def notify_escalation(self):
        subject = 'Invoice processing expired test'
        from_addr = 'no-reply@docuflow.co.za'
        recipient_list = ('azinamo@gmail.com', 'admire@originate.co.za',)
        message = 'Hey we are testing the crons you have this invoice in your flow that requires processing. \r\n' \
                  'Your supervisor has been notified that you are behind on processing this. \r\n' \
                  'Please process it within 24hours or the invoice will be forwarded to your supervisor ' \
                  'for processing'

        send_mail(subject, message, from_addr, recipient_list)

    def handle(self, *args, **kwargs):
        companies = Company.objects.all()
        for company in companies:
            can_escalate = self.can_escalate_invoices(company)
            if can_escalate:
                invoice_flow_roles = FlowRole.objects.for_escalation()
                counter = invoice_flow_roles.count()
                logger.info(invoice_flow_roles)
                logger.info(f"Found {company} Counter -- {counter}")
                self.notify_escalation()
                # TODO - Escalate automatically

