import os
from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils.timezone import now

from docuflow.apps.invoice.models import Invoice


class Command(BaseCommand):
    help = "Check invoices with missing pdf files"

    def handle(self, *args, **kwargs):
        date_range = [now().date() - timedelta(days=8), now().date()]
        invoices = Invoice.objects.filter(created_at__date__range=date_range)
        print(f"Found {invoices.count()} invoice for date range --> {date_range}")
        for invoice in invoices:
            if invoice.filename:
                if not os.path.exists(invoice.location):
                    print(f"File {invoice.filename}({invoice.checksum}) for invoice {invoice}; id=>{invoice.id}; created_at=>{invoice.created_at}; company=>{invoice.company}({invoice.company_id})  does not exist")
