from openpyxl import load_workbook


class PayrollPostingUploadClient(object):

    # noinspection PyMethodMayBeStatic
    def get_upload_data(self, file, skip=1):
        wb = load_workbook(file, data_only=True)
        sh = wb.active
        data = {}
        counter = 0
        index = 0
        for row in sh.iter_rows():
            if counter > skip:
                data[index] = {}
                for c, cell in enumerate(row, start=1):
                    data[index][c] = cell.value
                index += 1
            counter += 1
        return data


payroll_upload_client = PayrollPostingUploadClient()
