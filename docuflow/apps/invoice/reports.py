import time
import pprint
from datetime import date

from django.db.models import Prefetch

from .models import *
from docuflow.apps.distribution.models import Distribution
from docuflow.apps.company.models import ObjectItem
from docuflow.apps.period.models import Period

pp = pprint.PrettyPrinter(indent=4)


class JournalReport(object):

    def __init__(self, journal):
        self.journal = journal

    def get_journal(self, journal_id):
        return Journal.objects.get(pk=journal_id)

    def get_net_amount(self, invoice, is_reverse=False):
        net_amount = invoice.net_amount
        if is_reverse:
            net_amount = net_amount * -1
        return net_amount

    def get_vat_amount(self, invoice, is_reverse=False):
        vat_amount = invoice.vat_amount or 0
        if is_reverse:
            vat_amount = vat_amount * -1
        return vat_amount

    @classmethod
    def net_amount(cls, invoice, is_reverse=False):
        net_amount = invoice.net_amount
        if is_reverse:
            net_amount = net_amount * -1
        return net_amount

    @classmethod
    def vat_amount(cls, invoice, is_reverse=False):
        vat_amount = invoice.vat_amount or 0
        if is_reverse:
            vat_amount = vat_amount * -1
        return vat_amount


class IncomingJournal(JournalReport):

    def __init__(self, journal):
        self.journal = journal
        super(JournalReport, self).__init__()
        self.invoices = {}
        self.accounts = []
        self.vat_accounts = []
        self.total = 0
        self.total_net = 0
        self.total_vat = 0
        self.total_payment = 0
        self.total_discounts = 0
        self.default_supplier_account = None
        self.default_expenditure_account = None
        self.debit_vat_account = None
        if journal.company.default_vat_account:
            self.vat_account = journal.company.default_vat_account
        if journal.company.default_expenditure_account:
            self.expenditure_account = journal.company.default_expenditure_account
        if journal.company.default_supplier_account:
            self.supplier_account = journal.company.default_supplier_account

    # def get_journal_invoices(self):
    #     journal_invoices = InvoiceJournal.objects.select_related('supplier',
    #                                                              'linked_journal',
    #                                                              'invoice_type',
    #                                                              'linked_journal__invoice_type',
    #                                                              'linked_journal__supplier'
    #                                                              ). \
    #         filter(journal__journal_type=Journal.INCOMING,
    #                journal=self.journal,
    #                invoice__company=self.journal.company
    #                )
    #     return journal_invoices

    def get_journal_invoices(self):
        journal_invoices = InvoiceJournal.objects.prefetch_related(
            'invoice__invoice_accounts',
            'invoice__invoice_accounts__account',
            'invoice__invoice_accounts__invoice',
            'invoice__invoice_accounts__invoice__supplier',
            'invoice__invoice_accounts__invoice__period',
            'invoice__invoice_accounts__vat_code',
            'invoice__invoice_accounts__vat_code__account',
            'invoice__invoice_accounts__profile',
            'invoice__invoice_accounts__role',
            'invoice__invoice_accounts__object_items',
            'invoice__invoice_references',
        ).select_related(
            'invoice',
            'invoice__company',
            'invoice__period',
            'invoice__supplier',
            'invoice__vat_code',
            'invoice__vat_code__account',
            'invoice__invoice_type',
            'supplier',
            'linked_journal',
            'invoice_type',
            'linked_journal__invoice_type',
            'linked_journal__supplier',
            'journal',
            'journal__ledger'
        ).filter(journal__journal_type=Journal.INCOMING,
                 journal=self.journal,
                 invoice__company=self.journal.company
                 )
        return journal_invoices

    def get_references(self, invoice):
        invoice_references = invoice.invoice_references.all()
        reference_1 = reference_2 = None
        if len(invoice_references) > 0:
            reference_1 = invoice_references[0]
        if len(invoice_references) > 1:
            reference_2 = invoice_references[1]

        return reference_1, reference_2

    def process_report(self):
        total_vat = 0
        total_credit_total = 0
        total_excl_vat = 0
        journal_invoices = self.get_journal_invoices()
        for journal_invoice in journal_invoices:
            invoice = journal_invoice.invoice

            debit_nett = self.get_net_amount(journal_invoice)
            debit_vat = self.get_vat_amount(journal_invoice)
            credit_vat = 0
            invoice_total = debit_nett + debit_vat
            credit_vat_code = None
            credit_vat_account = None

            self.total_vat += debit_vat
            self.total_net += debit_nett
            self.total += invoice_total

            children = []
            if journal_invoice.linked_journal:
                is_reverse = True
                linked_journal = journal_invoice.linked_journal

                if linked_journal.vat_code:
                    linked_invoice_vat_amount = self.get_vat_amount(linked_journal)
                    credit_vat = linked_invoice_vat_amount - debit_vat
                    credit_vat_code = linked_journal.invoice.vat_code
                    credit_vat_account = self.vat_account if self.vat_account else linked_journal.vat_code.account

                    self.vat_accounts.append({
                        'journal_id': journal_invoice.id,
                        'vat_code': linked_journal.invoice.vat_code,
                        'account':  self.vat_account,
                        'invoice': linked_journal.invoice,
                        'is_reverse': True,
                        'amount': debit_vat,
                        'vat_account': "{}-{}".format(linked_journal.invoice.vat_code.account,
                                                      linked_journal.invoice.vat_code)
                    })

                # _link_debit_nett = self.get_net_amount(linked_journal, is_reverse)
                # _link_debit_vat = self.get_vat_amount(linked_journal, is_reverse)
                _link_debit_nett = linked_journal.net * -1
                _link_debit_vat = linked_journal.vat * -1

                self.total_vat += _link_debit_vat
                self.total_net += _link_debit_nett
                self.total += (_link_debit_nett + _link_debit_vat)

                _link_references = self.get_references(linked_journal.invoice)

                supplier = linked_journal.supplier
                invoice_type = linked_journal.invoice_type
                if not supplier:
                    supplier = linked_journal.invoice.supplier
                if not invoice_type:
                    invoice_type = linked_journal.invoice.invoice_type

                children.append({
                    'invoice': linked_journal.invoice,
                    'update': linked_journal,
                    'net': _link_debit_nett,
                    'vat': _link_debit_vat,
                    'total': (_link_debit_nett + _link_debit_vat)
                })
            if invoice.vat_code:
                self.vat_accounts.append({
                    'jorunal_id': journal_invoice.id,
                    'is_reverse': False,
                    'vat_code': invoice.vat_code,
                    'account': invoice.vat_code.account,
                    'invoice': invoice,
                    'debit_vat': debit_vat,
                    'credit_vat': credit_vat,
                    'credit_vat_code': credit_vat_code,
                    'credit_vat_account': credit_vat_account,
                    'vat_account': "{}-{}".format(invoice.vat_code.account, invoice.vat_code)
                })
            self.invoices[journal_invoice.id] = {
                'invoice': invoice,
                'update': journal_invoice,
                'children': children
            }


class PostingInvoiceAccount(object):
    def __init__(self, invoice_account, linked_models, journal, is_reverse=False):
        self.invoice_account = invoice_account
        self.is_vat = invoice_account.is_vat
        amount = self.get_amount(is_reverse)
        self.vat_amount = 0
        self.net_amount = 0
        if invoice_account.is_vat:
            self.vat_amount = amount
        else:
            self.net_amount = amount
        self.total_amount = amount
        self.account = self.get_account()
        self.vat_code = self.get_vat_code()
        self.percentage = invoice_account.percentage
        self.comment = invoice_account.comment
        self.date = invoice_account.created_at
        self.reinvoice = invoice_account.reinvoice
        self.signed = False
        account_profile = self.get_account_profile()
        self.profile = account_profile
        self.posted_to_accounts = account_profile
        self.linked_models_values = {}
        self.id = invoice_account.id
        self.invoice_id = invoice_account.invoice.id
        self.invoice = invoice_account.invoice
        self.url = self.get_edit_url()
        self.edit_url = self.get_edit_url()
        self.object_links = self.get_object_links(linked_models)
        self.ledger_number = ''
        if journal.ledger:
            self.ledger_number = journal.ledger.journal_number
        self.is_reverse = is_reverse
        self.period = invoice_account.invoice.period

    def get_account_profile(self):
        account_profile = self.invoice_account.profile.code
        if self.invoice_account.role:
            if self.invoice_account.is_posted is True:
                account_profile = self.invoice_account.profile.code
            else:
                account_profile = self.invoice_account.role
        return account_profile

    def get_edit_url(self):
        return reverse('edit_invoice_account', kwargs={'pk': self.invoice_account.id})

    def get_amount(self, is_reverse=False):
        amount = self.invoice_account.amount
        # if self.invoice_account.invoice.is_credit_type:
        #     amount = self.invoice_account.amount * -1
        if is_reverse:
            amount = amount * -1
        return amount

    def get_vat_code(self):
        vat_code = ''
        if self.invoice_account.vat_code:
            vat_code = self.invoice_account.vat_code
        return vat_code

    def get_account(self):
        account = self.invoice_account.account
        if self.invoice_account.distribution:
            account = self.invoice_account.invoice.company.default_distribution_account
        return account

    def get_object_links(self, linked_models):
        object_links = {}
        for key, linked_model in linked_models.items():
            object_links[key] = {'value': None, 'code': None, 'object_item': None}

        for link in self.invoice_account.object_items.all():
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.label, 'code': link.code, 'object_item': link}

        for link in self.invoice_account.object_postings.all():
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.name, 'code': link.name, 'object_item': link}

        return object_links


class ForecastingReport(object):

    def __init__(self, company):
        self.company = company
        self.total = 0
        self.day_invoices = {}

    def get_invoices(self, filter_options):
        return Invoice.objects.select_related(
            'invoice_type', 'company', 'period', 'period__period_year'
        ).filter(
            **filter_options
        ).order_by(
            '-due_date'
        ).exclude(deleted__isnull=False)

    @staticmethod
    def is_due(invoice_due_date):
        if isinstance(invoice_due_date, date):
            if invoice_due_date < date.today():
                return True
        return False

    @staticmethod
    def sort_report(forecasting_report):
        ordered_dates = sorted(forecasting_report)
        sorted_report = []
        for d in ordered_dates:
            if d in forecasting_report:
                sorted_report.append(forecasting_report[d])
        return sorted_report

    def process_report(self, filter_options):
        invoices = self.get_invoices(filter_options)
        forecasting_report = {}
        for invoice in invoices:
            if invoice.due_date:
                invoice_total = invoice.open_amount

                timestamp = time.mktime(invoice.due_date.timetuple())

                # if invoice.is_credit_type:
                #     if invoice_total > 0:
                #         invoice_total = invoice_total * -1

                self.total += invoice_total
                if timestamp in forecasting_report:
                    forecasting_report[timestamp]['total'] += invoice_total
                    forecasting_report[timestamp]['invoices'].append(invoice)
                else:
                    is_due = ForecastingReport.is_due(invoice.due_date)
                    forecasting_report[timestamp] = {
                        'total': invoice_total,
                        'due_date': invoice.due_date,
                        'year': invoice.due_date.year,
                        'month': invoice.due_date.month,
                        'day': invoice.due_date.day,
                        'is_due': is_due,
                        'invoices': [invoice]
                    }
        self.day_invoices = ForecastingReport.sort_report(forecasting_report)


class DistributionJournal(JournalReport):

    def __init__(self, journal):
        self.journal = journal
        super(JournalReport, self).__init__()
        self.invoices = {}
        self.accounts = []
        self.vat_accounts = []
        self.total = 0
        self.total_net = 0
        self.total_vat = 0
        self.total_payment = 0
        self.total_discounts = 0
        self.default_supplier_account = None
        self.default_expenditure_account = None
        self.debit_vat_account = None
        if journal.company.default_vat_account:
            self.vat_account = journal.company.default_vat_account
        if journal.company.default_expenditure_account:
            self.expenditure_account = journal.company.default_expenditure_account
        if journal.company.default_supplier_account:
            self.supplier_account = journal.company.default_supplier_account

    def get_journal_invoices(self):
        return InvoiceJournal.objects.select_related(
            'supplier', 'linked_journal', 'invoice_type', 'linked_journal__invoice_type', 'linked_journal__supplier'
        ).filter(journal__journal_type=Journal.DISTRIBUTION, journal=self.journal, invoice__company=self.journal.company)

    def get_references(self, invoice):
        invoice_references = invoice.invoice_references.all()
        reference_1 = reference_2 = None
        if len(invoice_references) > 0:
            reference_1 = invoice_references[0]
        if len(invoice_references) > 1:
            reference_2 = invoice_references[1]

        return reference_1, reference_2

    def process_report(self):
        total_vat = 0
        total_credit_total = 0
        total_excl_vat = 0
        journal_invoices = self.get_journal_invoices()

        for journal_invoice in journal_invoices:
            invoice = journal_invoice.invoice

            debit_nett = self.get_net_amount(journal_invoice)
            debit_vat = self.get_vat_amount(journal_invoice)
            invoice_total = debit_nett + debit_vat
            if invoice.vat_code:
                self.vat_accounts.append({
                    'vat_code': invoice.vat_code,
                    'account': invoice.vat_code.account,
                    'amount': debit_vat,
                    'vat_account': "{}-{}".format(invoice.vat_code.account, invoice.vat_code)
                })

            self.total_vat += debit_vat
            self.total_net += debit_nett
            self.total += invoice_total

            references = self.get_references(invoice)

            children = []
            if journal_invoice.linked_journal:
                linked_journal = journal_invoice.linked_journal

                _link_debit_nett = self.get_net_amount(linked_journal)
                _link_debit_vat = self.get_vat_amount(linked_journal)

                self.total_vat += _link_debit_vat
                self.total_net += _link_debit_nett
                self.total += (_link_debit_nett + _link_debit_vat)

                _link_references = self.get_references(linked_journal.invoice)

                supplier = linked_journal.supplier
                invoice_type = linked_journal.invoice_type
                if not supplier:
                    supplier = linked_journal.invoice.supplier
                if not invoice_type:
                    invoice_type = linked_journal.invoice.invoice_type

                children.append({
                    'docuflow_unique': linked_journal.invoice,
                    'accounting_date': linked_journal.accounting_date,
                    'invoice_date': linked_journal.invoice_date,
                    'due_date': linked_journal.invoice.due_date,
                    'supplier_name': linked_journal.invoice.supplier.name,
                    'document_type': invoice_type,
                    'supplier_inv': linked_journal.invoice.invoice_number,
                    'reference_1': _link_references[0],
                    'reference_2': _link_references[1],
                    'debit_nett': _link_debit_nett,
                    'debit_vat': _link_debit_vat,
                    'id': linked_journal.id,
                    'vat_type': linked_journal.invoice.vat_code,
                    'credit_total': (_link_debit_vat + _link_debit_nett),
                })

            self.invoices[journal_invoice.id] = {
                'docuflow_unique': invoice,
                'vat_type': invoice.vat_code,
                'accounting_date': invoice.accounting_date,
                'invoice_date': invoice.invoice_date,
                'due_date': invoice.due_date,
                'supplier_name': invoice.supplier.name,
                'document_type': invoice.invoice_type,
                'supplier_inv': invoice.invoice_number,
                'reference_1': references[0],
                'reference_2': references[1],
                'debit_nett': debit_nett,
                'debit_vat': debit_vat,
                'credit_total': (debit_vat + debit_nett),
                'children': children
            }


class DistributionBalance(object):
    def __init__(self, company):
        self.company = company
        self.day_ranges = {}
        self.distributions = {}
        self.set_day_ranges()
        self.total = 0

    def set_day_ranges(self):
        period = self.get_period()
        self.day_ranges = {
            'one_twenty_or_more_days': {
                'from': period - 4,
                'to': period - 4,
                'totals': {},
                'total': 0
            },
            'ninety_days': {
                'from': period - 3,
                'to': period - 3,
                'totals': {},
                'total': 0
            },
            'sixty_days': {
                'from': period - 2,
                'to': period - 2,
                'totals': {},
                'total': 0
            },
            'thirty_days': {
                'from': period - 1,
                'to': period - 1,
                'totals': {},
                'total': 0
            },
            'current': {
                'from': period,
                'to': period,
                'totals': {},
                'total': 0
            }
        }

    def get_distributions(self, filter_options):
        return Distribution.objects.with_pending_accounts(filter_options)

    def process_report(self, options):
        distributions = self.get_distributions(options)

        self.get_distribution_day_range_totals(distributions)

    def get_period(self):
        try:
            heighest_period = Period.objects.filter(company=self.company, is_closed=False).order_by('-period').first()
            return heighest_period.period
        except Exception:
            return None

    def get_day_range_totals(self, distribution):
        distributions = {'one_twenty_or_more_days': 0,
                         'ninety_days': 0,
                         'sixty_days': 0,
                         'thirty_days': 0,
                         'current': 0,
                         'day_ranges': 0,
                         'total': 0,
                         'name': distribution,
                         'code': '',
                         'id': distribution.id
                         }
        for distribution_account in distribution.accounts.all():
            total = distribution_account.credit
            # if distribution.invoice.is_credit_type:
            #     total = total * -1
            distributions['total'] += total
            if distribution_account.period:
                if distribution_account.period.period < self.day_ranges['one_twenty_or_more_days']['from']:
                    distributions['one_twenty_or_more_days'] += total
                elif distribution_account.period.period == self.day_ranges['ninety_days']['from']:
                    distributions['ninety_days'] += total
                elif distribution_account.period.period == self.day_ranges['sixty_days']['from']:
                    distributions['sixty_days'] += total
                elif distribution_account.period.period == self.day_ranges['thirty_days']['from']:
                    distributions['thirty_days'] += total
                elif distribution_account.period.period == self.day_ranges['current']['from']:
                    distributions['current'] += total
        return distributions

    def get_distribution_day_range_totals(self, distributions):

        for distribution in distributions:
            totals = self.get_day_range_totals(distribution)
            self.distributions[distribution.id] = totals
            self.day_ranges['one_twenty_or_more_days']['total'] += totals['one_twenty_or_more_days']
            self.day_ranges['ninety_days']['total'] += totals['ninety_days']
            self.day_ranges['sixty_days']['total'] += totals['sixty_days']
            self.day_ranges['thirty_days']['total'] += totals['thirty_days']
            self.day_ranges['current']['total'] += totals['current']
            self.total += totals['total']


class JournalForPostingReport(JournalReport):

    def __init__(self, journal):
        super(JournalForPostingReport, self).__init__(journal)
        self.journal = journal
        self.invoice_account_total = 0
        self.net_total = 0
        self.vat_total = 0
        self.linked_models_count = 0
        self.linked_models = {}
        self.invoices_accounts = {}
        self.expenditure_account = None

    def get_company_object_items(self, company):
        object_items = ObjectItem.objects.select_related('company_object').filter(company_object__company=company).all()
        company_object_items = {}
        for object_item in object_items:
            if object_item.company_object_id in company_object_items:
                company_object_items[object_item.company_object_id].append(object_item)
            else:
                company_object_items[object_item.company_object_id] = [object_item]
        return company_object_items

    def get_company_linked_models(self):
        company_object_items = self.get_company_object_items(self.journal.company)
        company_objects = self.journal.company.company_objects.all()
        for company_object in company_objects:
            self.linked_models[company_object.id] = {}
            self.linked_models[company_object.id]['values'] = {}
            self.linked_models[company_object.id]['label'] = company_object.label
            self.linked_models[company_object.id]['class'] = company_object
            self.linked_models[company_object.id]['objects'] = []
            if company_object.id in company_object_items:
                self.linked_models[company_object.id]['objects'] = company_object_items[company_object.id]

    def process_report(self):
        self.get_company_linked_models()

        journal_invoices = InvoiceJournal.objects.select_related(
            'invoice', 'journal', 'invoice__supplier', 'invoice__invoice_type'
        ).prefetch_related('invoice__invoice_references').filter(
            journal__journal_type=Journal.POSTING,
            journal=self.journal,
            invoice__company=self.journal.company,
            invoice__invoice_type__is_account_posting=True
        ).all()

        self.get_journal_invoice_accounts(journal_invoices)
        #
        self.expenditure_account = self.journal.company.default_expenditure_account
        self.linked_models_count = len(self.linked_models)

    def get_invoices_accounts(self, journal_invoices):
        return InvoiceAccount.objects.select_related(
            'account', 'profile', 'invoice', 'invoice__invoice_type', 'invoice__period', 'vat_code',
            'invoice__company', 'role'
        ).prefetch_related('object_items').filter(
            invoice_id__in=[journal.invoice_id for journal in journal_invoices]
        )

    def get_journal_invoice_accounts(self, journal_invoices):
        accounts = self.get_invoices_accounts(journal_invoices).all()
        for invoice_account in accounts:
            if invoice_account.is_vat and invoice_account.amount != 0:
                if invoice_account.is_posted:
                    posting_account = PostingInvoiceAccount(invoice_account, self.linked_models, self.journal, True)
                    if invoice_account.invoice_id in self.invoices_accounts:
                        self.invoice_account_total += posting_account.total_amount
                        self.vat_total += posting_account.vat_amount
                        self.net_total += posting_account.net_amount
                        self.invoices_accounts[invoice_account.invoice_id].append(posting_account)
                    else:
                        self.invoice_account_total += posting_account.total_amount
                        self.vat_total += posting_account.vat_amount
                        self.net_total += posting_account.net_amount
                        self.invoices_accounts[invoice_account.invoice_id] = [posting_account]

                    for _invoice_account in accounts.filter(invoice_account_id=invoice_account.id):
                        if _invoice_account.is_vat and _invoice_account.is_holding_account is False:
                            _posting_account = PostingInvoiceAccount(_invoice_account, self.linked_models, self.journal)
                            if _invoice_account.invoice_id in self.invoices_accounts:
                                self.invoice_account_total += _posting_account.total_amount
                                self.vat_total += _posting_account.vat_amount
                                self.net_total += _posting_account.net_amount
                                self.invoices_accounts[_invoice_account.invoice_id].append(_posting_account)
                            else:
                                self.invoice_account_total += _posting_account.total_amount
                                self.vat_total += _posting_account.vat_amount
                                self.net_total += _posting_account.net_amount
                                self.invoices_accounts[_invoice_account.invoice_id] = [_posting_account]
            else:
                posting_account = PostingInvoiceAccount(invoice_account, self.linked_models, self.journal)

                if invoice_account.invoice_id in self.invoices_accounts:
                    self.invoice_account_total += posting_account.total_amount
                    self.vat_total += posting_account.vat_amount
                    self.net_total += posting_account.net_amount
                    self.invoices_accounts[invoice_account.invoice_id].append(posting_account)
                else:
                    self.vat_total += posting_account.vat_amount
                    self.net_total += posting_account.net_amount
                    self.invoice_account_total += posting_account.total_amount
                    self.invoices_accounts[invoice_account.invoice_id] = [posting_account]


class PostingWithIncomingReport(JournalReport):

    def __init__(self, journal, linked_models):
        self.journal = journal
        super(JournalReport, self).__init__()
        self.invoices = {}
        self.accounts = []
        self.vat_accounts = []
        self.total = 0
        self.total_net = 0
        self.total_vat = 0
        self.total_payment = 0
        self.total_discounts = 0
        self.invoice_total = 0
        self.default_supplier_account = None
        self.default_expenditure_account = None
        self.debit_vat_account = None
        self.linked_models = linked_models
        self.default_account = None
        if journal.company.default_expenditure_account:
            self.default_account = journal.company.default_expenditure_account
        if journal.company.default_supplier_account:
            self.default_supplier_account = journal.company.default_supplier_account

    def get_journal_invoices(self):
        journal_invoices = InvoiceJournal.objects.prefetch_related(
            'invoice__invoice_accounts',
            'invoice__invoice_accounts__account',
            'invoice__invoice_accounts__invoice',
            'invoice__invoice_accounts__invoice__supplier',
            'invoice__invoice_accounts__invoice__period',
            'invoice__invoice_accounts__vat_code',
            'invoice__invoice_accounts__vat_code__account',
            'invoice__invoice_accounts__profile',
            'invoice__invoice_accounts__role',
            'invoice__invoice_accounts__object_items',
            'invoice__invoice_references',
        ).select_related(
            'invoice', 'invoice__company', 'invoice__period', 'invoice__supplier', 'invoice__vat_code',
            'invoice__vat_code__account', 'invoice__invoice_type',
            'supplier',
            'linked_journal',
            'invoice_type',
            'linked_journal__invoice_type',
            'linked_journal__supplier',
            'journal',
            'journal__ledger'
        ).filter(journal__journal_type=Journal.POSTING,
                 journal=self.journal,
                 invoice__company=self.journal.company
                 )
        return journal_invoices

    def get_references(self, invoice):
        invoice_references = invoice.invoice_references.all()
        reference_1 = reference_2 = None
        if len(invoice_references) > 0:
            reference_1 = invoice_references[0]
        if len(invoice_references) > 1:
            reference_2 = invoice_references[1]

        return reference_1, reference_2

    def get_invoice_accounts(self, invoice):
        invoice_accounts = invoice.invoice_accounts.all()
        accounts_posted = []
        invoice_total = 0
        for invoice_account in invoice_accounts.all():
            if invoice_account.is_vat and invoice_account.amount != 0:
                if invoice_account.is_posted:
                    posting_account = PostingInvoiceAccount(
                        invoice_account=invoice_account,
                        linked_models=self.linked_models,
                        journal=self.journal,
                        is_reverse=True
                    )
                    self.total_vat += posting_account.vat_amount
                    self.total_net += posting_account.net_amount
                    self.total += posting_account.total_amount
                    invoice_total += posting_account.total_amount
                    accounts_posted.append(posting_account)
            else:
                posting_account = PostingInvoiceAccount(
                    invoice_account=invoice_account,
                    linked_models=self.linked_models,
                    journal=self.journal
                )
                self.total_vat += posting_account.vat_amount
                self.total_net += posting_account.net_amount
                self.total += posting_account.total_amount
                invoice_total += posting_account.total_amount
                accounts_posted.append(posting_account)
        self.invoice_total = invoice_total
        return accounts_posted, invoice_total

    def process_report(self):
        journal_invoices = self.get_journal_invoices()
        for journal_invoice in journal_invoices:
            invoice = journal_invoice.invoice

            debit_nett = self.get_net_amount(journal_invoice)
            debit_vat = self.get_vat_amount(journal_invoice)
            invoice_total = debit_nett + debit_vat
            if invoice.vat_code:
                self.vat_accounts.append({
                    'vat_code': invoice.vat_code,
                    'account': invoice.vat_code.account,
                    'amount': debit_vat,
                    'vat_account': "{}-{}".format(invoice.vat_code.account, invoice.vat_code)
                })

            references = self.get_references(invoice)

            children = []
            invoice_accounts = []
            if journal_invoice.linked_journal:
                linked_journal = journal_invoice.linked_journal

                _link_debit_nett = self.get_net_amount(linked_journal)
                _link_debit_vat = self.get_vat_amount(linked_journal)

                self.total_vat += _link_debit_vat
                self.total_net += _link_debit_nett
                self.total += (_link_debit_nett + _link_debit_vat)

                # _link_references = self.get_references(linked_journal.invoice)

                supplier = linked_journal.supplier
                invoice_type = linked_journal.invoice_type
                if not supplier:
                    supplier = linked_journal.invoice.supplier
                if not invoice_type:
                    invoice_type = linked_journal.invoice.invoice_type

            self.invoice_total = 0
            accounts = self.get_invoice_accounts(
                invoice=journal_invoice.invoice
            )

            self.invoices[journal_invoice.id] = {
                'docuflow_unique': invoice,
                'invoice_id': invoice.id,
                'vat_type': invoice.vat_code,
                'accounting_date': invoice.accounting_date,
                'invoice_date': invoice.invoice_date,
                'due_date': invoice.due_date,
                'supplier_name': invoice.supplier.name,
                'supplier': invoice.supplier,
                'document_type': invoice.invoice_type,
                'supplier_inv': invoice.invoice_number,
                'reference_1': references[0],
                'reference_2': references[1],
                'debit_nett': debit_nett,
                'debit_vat': debit_vat,
                'credit_total': accounts[1],
                'children': children,
                'invoice_accounts': accounts[0]
            }


class PostingJournal(JournalReport):

    def __init__(self, journal, linked_models):
        self.journal = journal
        super(JournalReport, self).__init__()
        self.invoices = {}
        self.accounts = []
        self.vat_accounts = []
        self.total = 0
        self.total_net = 0
        self.total_vat = 0
        self.total_payment = 0
        self.total_discounts = 0
        self.default_supplier_account = None
        self.default_expenditure_account = None
        self.debit_vat_account = None
        self.linked_models = linked_models
        self.default_account = None
        if journal.company.default_supplier_account:
            self.default_account = journal.company.default_supplier_account

    def get_journal_invoices(self):
        journal_invoices = InvoiceJournal.objects.select_related(
            'supplier', 'linked_journal', 'invoice_type', 'linked_journal__invoice_type', 'linked_journal__supplier',
            'invoice', 'invoice__vat_code', 'invoice__vat_code__account', 'invoice__supplier', 'invoice__period',
            'invoice__invoice_type', 'invoice__company'
        ).prefetch_related(
            'invoice__invoice_references',
            Prefetch('invoice__invoice_accounts',
                     queryset=InvoiceAccount.objects.prefetch_related(
                         'object_items', 'object_postings'
                     ).select_related(
                         'account', 'vat_code', 'profile', 'role', 'invoice__company__default_distribution_account'
                     ))
        ).filter(journal__journal_type=Journal.POSTING, journal=self.journal, invoice__company=self.journal.company)
        return journal_invoices

    def get_references(self, invoice):
        invoice_references = invoice.invoice_references.all()
        reference_1 = reference_2 = None
        if len(invoice_references) > 0:
            reference_1 = invoice_references[0]
        if len(invoice_references) > 1:
            reference_2 = invoice_references[1]

        return reference_1, reference_2

    def process_report(self):
        journal_invoices = self.get_journal_invoices()
        logger.info(f"PROCESS HERE {journal_invoices.count()}")
        for journal_invoice in journal_invoices:
            invoice = journal_invoice.invoice

            debit_nett = self.get_net_amount(journal_invoice)
            debit_vat = self.get_vat_amount(journal_invoice)
            invoice_total = debit_nett + debit_vat
            if invoice.vat_code:
                self.vat_accounts.append({
                    'vat_code': invoice.vat_code,
                    'account': invoice.vat_code.account,
                    'amount': debit_vat,
                    'vat_account': f"{invoice.vat_code.account}-{invoice.vat_code}"
                })

            self.total_vat += debit_vat
            self.total_net += debit_nett
            self.total += invoice_total

            references = self.get_references(invoice)

            children = []
            invoice_accounts = []
            if journal_invoice.linked_journal:
                linked_journal = journal_invoice.linked_journal

                _link_debit_nett = self.get_net_amount(linked_journal)
                _link_debit_vat = self.get_vat_amount(linked_journal)

                self.total_vat += _link_debit_vat
                self.total_net += _link_debit_nett
                self.total += (_link_debit_nett + _link_debit_vat)

                _link_references = self.get_references(linked_journal.invoice)

                supplier = linked_journal.supplier
                invoice_type = linked_journal.invoice_type
                if not supplier:
                    supplier = linked_journal.invoice.supplier
                if not invoice_type:
                    invoice_type = linked_journal.invoice.invoice_type

                # children.append({
                #     'docuflow_unique': linked_journal.invoice,
                #     'accounting_date': linked_journal.accounting_date,
                #     'invoice_date': linked_journal.invoice_date,
                #     'due_date': linked_journal.invoice.due_date,
                #     'supplier_name': linked_journal.invoice.supplier.name,
                #     'document_type': invoice_type,
                #     'supplier_inv': linked_journal.invoice.invoice_number,
                #     'reference_1': _link_references[0],
                #     'reference_2': _link_references[1],
                #     'debit_nett': _link_debit_nett,
                #     'debit_vat': _link_debit_vat,
                #     'id': linked_journal.id,
                #     'vat_type': linked_journal.invoice.vat_code,
                #     'credit_total': (_link_debit_vat + _link_debit_nett),
                # })
            posting_accounts_total = 0
            for invoice_account in journal_invoice.invoice.invoice_accounts.all():
                posting_invoice_account = PostingInvoiceAccount(invoice_account, self.linked_models, self.journal)
                invoice_accounts.append(posting_invoice_account)
                posting_accounts_total += posting_invoice_account.total_amount if posting_invoice_account.total_amount else 0

            diff = Decimal(invoice_total) - Decimal(posting_accounts_total)
            logger.info("Invoice total {} vs total of the postings {} -> diff {}".format(invoice_total, posting_accounts_total, diff))

            self.invoices[journal_invoice.id] = {
                'docuflow_unique': invoice,
                'vat_type': invoice.vat_code,
                'accounting_date': invoice.accounting_date,
                'invoice_date': invoice.invoice_date,
                'due_date': invoice.due_date,
                'supplier_name': invoice.supplier.name,
                'supplier': invoice.supplier,
                'document_type': invoice.invoice_type,
                'supplier_inv': invoice.invoice_number,
                'reference_1': references[0],
                'reference_2': references[1],
                'debit_nett': debit_nett,
                'debit_vat': debit_vat,
                'credit_total': (debit_vat + debit_nett),
                'children': children,
                'invoice_accounts': invoice_accounts
            }

