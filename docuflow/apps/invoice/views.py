import logging
import pprint
from collections import OrderedDict
from datetime import date
from decimal import Decimal

import xlwt
from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.db import transaction
from django.db.models import CharField, Value as V
from django.db.models import Subquery
from django.db.models.functions import Concat
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.utils.timezone import now
from django.views.generic import ListView, FormView, View, TemplateView
from django.views.generic.edit import UpdateView
from django_filters.views import FilterView

from docuflow.apps.accounts.models import Profile, Role, User
from docuflow.apps.auditlog.models import LogEntry
from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.supplier.models import Supplier
from .enums import FlowRoleStatus
from .exceptions import (UnavailableWorkflow, DuplicateInvoiceNumber, InvalidInvoiceTotal, InvoiceException,
                         AccountPostingNotSetException, InvalidPeriodAccountingDateException, InvoiceHasVariance,
                         DefaultAccountNotPosted, AccountingDateClosed, InvoiceJournalNotAvailable)
from .filters import InvoiceCapturedFilter, ShowInvoiceFilter
from .forms import (InvoiceForm, InvoiceDeclineForm, InvoiceLogForm, CapturedInvoiceForm,
                    InvoiceJournalReportForm, InvoiceBottleneckReportForm, InvoiceProcessingDayReportForm,
                    InvoicePaymentForecastReportForm, EmailSupplierForm, InvoiceParkForm,
                    AccountingDateForm, AddImageForm, CreateInvoiceForm, InvoiceDeclineApprovalForm, InvoiceCommentForm)
from .models import (Invoice, Status, Comment, Journal, InvoiceJournal, FlowRole, Flow, InvoiceAccount, Payment)
from .reports import ForecastingReport
from .services import (
    SignInvoice, UnSignInvoice, AddToFlow, FlowIntervention, change_decline_status, ImportPayrollJob,
    PayrollImportException, recalculate_posting_vat
)
from .utils import get_page_viewer, completed_statuses, processing_statuses

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class InvoicesView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/index.html'

    def get_context_data(self, **kwargs):
        context = super(InvoicesView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context


class RoleInvoiceMixin(object):

    # noinspection PyUnresolvedReferences
    def get_role_invoices(self, role, is_current, year=None):
        company_id = self.request.session['company']
        invoice_type = self.kwargs.get('invoice_type', None) or self.request.GET.get('invoice_type', None)
        invoice_status = self.request.GET.get('status', None)
        qs = Invoice.objects.filter_in_flow(company_id=company_id, roles=[role], is_current=is_current)

        filter_options = {}
        if invoice_status:
            if invoice_status == 'all':
                qs = Invoice.objects.in_flow(company_id=company_id, roles=[role], is_current=is_current)
            else:
                filter_options['status__slug'] = invoice_status
        else:
            if invoice_type in ['inflow', 'latest', 'flagged']:
                filter_options['workflow__isnull'] = False
            elif invoice_type == 'paid':
                filter_options['is_completed'] = True
                filter_options['workflow__isnull'] = False
            elif invoice_type == 'in_trash':
                filter_options['deleted__isnull'] = False
        qs = qs.with_intervention_check().annotate(
            df_number=Concat(
                'company__slug', V(' - '), 'invoice_id', V(''), output_field=CharField()
            ),
        ).select_related(
            'status', 'company', 'supplier', 'invoice_type', 'workflow'
        ).prefetch_related(
            'invoice_references', 'flows', 'flows__status', 'flows__roles', 'flows__roles__role'
        ).filter(
            **filter_options
        )
        if year:
            qs = qs.filter(period__period_year=year)
        qs = qs.order_by('-created_at')

        if is_current:
            self.request.session['can_edit_invoice'] = list(qs.values_list('pk', flat=True))
        return qs


class InvoicesListView(TemplateView, ProfileMixin, RoleInvoiceMixin):
    template_name = 'invoice/invoices.html'

    def invoice_current_state(self):
        return not self.request.GET.get('invoice_type') == 'my_upcoming_invoices'

    def get_context_data(self, **kwargs):
        self.request.session['can_edit_invoice'] = []
        context = super(InvoicesListView, self).get_context_data(**kwargs)
        is_current = self.invoice_current_state()
        year = self.get_year()
        invoice_type = self.request.GET.get('invoice_type')
        context['is_upcoming'] = (invoice_type == 'my_upcoming_invoices')
        context['year'] = year
        context['invoice_type'] = invoice_type
        context['invoices'] = self.get_role_invoices(is_current=is_current, role=self.get_role(), year=year)
        return context


class InvoicesByStatusIndexView(TemplateView):
    template_name = 'invoice/status_invoices.html'

    def get_status_invoices(self):
        filter_options = {'company_id': self.request.session['company'], 'status__slug': self.kwargs['slug']}
        invoices = Invoice.objects.annotate(
            df_number=Concat(
                'company__slug', V(' - '), 'invoice_id', V(''), output_field=CharField()
            )
        ).select_related(
            'status', 'company', 'supplier', 'invoice_type'
        ).filter(
            **filter_options
        ).values('id', 'status__name', 'invoice_type__name', 'supplier__name', 'supplier__code', 'total_amount',
                 'accounting_date', 'invoice_date', 'due_date', 'comments', 'flows__intervention_at', 'df_number',
                 'comments').distinct('id')
        return invoices

    def get_context_data(self, **kwargs):
        context = super(InvoicesByStatusIndexView, self).get_context_data(**kwargs)
        invoices = self.get_status_invoices()
        context['invoices'] = invoices
        context['status'] = Status.objects.filter(slug=self.kwargs['slug']).first()
        return context


class InvoicesLogView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Invoice
    template_name = 'invoice/log.html'
    context_object_name = 'invoices'
    paginate_by = 30

    def get_context_data(self, **kwargs):
        context = super(InvoicesLogView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_queryset(self):
        return Invoice.objects.get_at_logger(
            company=self.get_company()
        ).select_related(
            'company', 'status', 'invoice_type', 'workflow', 'supplier'
        ).exclude(
            deleted__isnull=False
        ).order_by(
            'invoice_id', 'accounting_date'
        )


class InvoicesLogDownloadView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoices(self):
        company = self.get_company()
        return Invoice.objects.filter(company=company, status__slug='arrived-at-logger')

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="invoice_logs.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Invoices Logs')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['Company', 'Supplier', 'Supplier\'s inv no', 'Accounting Date', 'Due Date', 'Total Amount',
                   'VAT Amount', 'VAT%', 'Currency', 'Information', 'Invoice Date']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        rows = self.get_invoices().values_list('company', 'supplier', 'supplier', 'accounting_date', 'due_date',
                                               'amount', 'vat_amount', 'vat', 'currency', 'currency', 'invoice_date')
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response


class InvoiceCreateView(LoginRequiredMixin, ProfileMixin, FormView):
    model = Invoice
    template_name = 'invoice/create.html'
    form_class = CreateInvoiceForm

    def get_context_data(self, **kwargs):
        context = super(InvoiceCreateView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(InvoiceCreateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the document, please fix the errors.',
                'errors': form.errors,
                'alert': True
            }, status=400)
        return super(InvoiceCreateView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            try:
                form.save()

                text = "Invoice successfully saved"
                if self.request.POST.get('add_to_flow', None):
                    text = 'Invoice successfully saved and added to flow'
                return JsonResponse({
                    'error': False,
                    'text': text,
                    'reload': True
                })
            except InvalidPeriodAccountingDateException as accounting_date_period_exception:
                logger.exception(accounting_date_period_exception)
                return JsonResponse({
                    'error': True,
                    'text': 'Invalid period and account date, please update',
                    'details': f'Error saving the invoice {str(accounting_date_period_exception)}',
                    'alert': True,
                    'errors': {
                        'accounting_date': ['Please update to match with period'],
                        'period': ['Please update to match with accounting date']
                    }
                })
            except DuplicateInvoiceNumber as duplicate_exception:
                logger.exception(duplicate_exception)
                return JsonResponse({
                    'error': True,
                    'text': str(duplicate_exception),
                    'details': f'Error saving the invoice {str(duplicate_exception)}',
                    'alert': True,
                    'errors': {'invoice_number': ['Invoice number already exists']}
                })
            except InvalidInvoiceTotal as totals_exception:
                logger.exception(totals_exception)
                return JsonResponse({
                    'error': True,
                    'text': str(totals_exception),
                    'details': f'Error saving the invoice {str(totals_exception)}',
                    'alert': True,
                    'errors': {'total_amount': ['Invoice totals do not match']}
                })
            except UnavailableWorkflow as e:
                return JsonResponse({
                    'error': True,
                    'text': str(e),
                    'details': f'Error saving the invoice {str(e)}',
                    'alert': True
                })
        return HttpResponse('Not allowed')


class InvoiceEditView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = Invoice
    template_name = 'invoice/edit.html'
    success_message = 'Document updated successfully'
    form_class = InvoiceForm
    success_url = reverse_lazy('all-invoices')

    def get_form_kwargs(self):
        kwargs = super(InvoiceEditView, self).get_form_kwargs()
        company = self.get_company()
        kwargs['company'] = company
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        return kwargs


class InvoiceDeleteView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        invoice = get_object_or_404(Invoice, pk=self.kwargs['pk'])
        invoice.delete()
        messages.success(self.request, 'Invoice successfully moved to trash')
        return HttpResponseRedirect(reverse('invoice_log'))


class InvoicesAddToFlowView(LoginRequiredMixin, View):

    def log_event(self, invoice, note=None):
        invoice.log_activity(self.request.user, self.request.session['role'], note, Comment.EVENT)

    def add_invoices_to_flow(self, ids, redirect_url):
        invoice_ids = [int(invoice_id) for invoice_id in ids.split(',')]
        invoices = Invoice.objects.filter(id__in=invoice_ids)
        if invoices:
            profile = Profile.objects.get(user=self.request.user)
            role = Role.objects.get(pk=self.request.session['role'])
            with transaction.atomic():
                for invoice in invoices:
                    add_to_flow = AddToFlow(invoice, profile, role)
                    add_to_flow.process_flow()
                    self.log_event(invoice, 'Added document to flow')
                return {'error': False, 'text': 'Documents successfully added to flow',
                        'redirect': redirect_url
                        }
        else:
            return {'error': False,
                    'text': 'No documents not found, please try again ',
                    'redirect': redirect_url
                    }

    def get(self, request, *args, **kwargs):
        redirect_url = reverse('invoice_log')
        if self.request.is_ajax():
            response = None
            if 'invoices' in self.request.GET:
                response = self.add_invoices_to_flow(self.request.GET['invoices'], redirect_url)
            return JsonResponse(response)
        else:
            return redirect(redirect_url)


class AddInvoiceToFlowView(LoginRequiredMixin, View):

    @staticmethod
    def logger_has_invoices(invoice):
        invoice = Invoice.objects.get_at_logger(invoice.company, [invoice.id]).first()
        if invoice:
            return invoice.id
        return None

    def add_invoice_to_flow(self, invoice):
        if not invoice.is_account_posting_set():
            raise AccountPostingNotSetException('Default account posting proposal or company default '
                                                'expenditure is not set, please fix.')

        profile = Profile.objects.get(user=self.request.user)
        role = get_object_or_None(Role, pk=self.request.session['role'])

        add_to_flow = AddToFlow(invoice, profile, role)
        add_to_flow.process_flow()

        invoice.log_activity(self.request.user, self.request.session['role'], "Added document to flow",
                             Comment.EVENT)

        return self.logger_has_invoices(invoice)

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            try:
                invoice = Invoice.objects.get(pk=self.kwargs['pk'])
                with transaction.atomic():
                    next_invoice_id = self.add_invoice_to_flow(invoice)
                    redirect_url = reverse('invoice_log')
                    if next_invoice_id:
                        redirect_url = reverse('invoice_queue_detail', kwargs={'pk': next_invoice_id})
                    return JsonResponse({'error': False,
                                         'text': 'Document successfully added to flow',
                                         'redirect': redirect_url
                                         })
            except Invoice.DoesNotExist:
                return JsonResponse({'error': True,
                                     'text': 'Invoice not found please refresh your page'
                                     })
            except AccountPostingNotSetException as e:
                logger.exception(e)
                return JsonResponse({'error': True,
                                     'text': str(e),
                                     'details': str(e),
                                     'alert': True
                                     })
            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'error': True,
                                     'text': 'Unexpected exception occurred trying to add document to flow ',
                                     'details': str(exception),
                                     'alert': True
                                     })
        else:
            return redirect(reverse('invoice_log'))


class InvoiceAccountCalculateAmountView(LoginRequiredMixin, View):

    def apply_operator(self, operator, result, amount_str):
        if operator == '*':
            result = Decimal(amount_str) * result
        elif operator == '/':
            divisor = Decimal(amount_str)
            if divisor > 0:
                result = result / divisor
        elif operator == '+':
            result += Decimal(amount_str)
        elif operator == '-':
            result -= Decimal(amount_str)
        return result

    def get_amount(self, amount_str):
        operators = ['*', '/', '+', '-']
        result = 0
        for operator in operators:
            if operator in amount_str:
                amounts = amount_str.split(operator)
                for amt in amounts:
                    return self.get_amount(amt)
            else:
                if amount_str:
                    result = self.apply_operator(operator, result, amount_str)
        return result

    def get(self, request, *args, **kwargs):
        amount = self.request.GET['amount']
        if amount:
            try:
                operators = ['*', '/', '+', '-']
                is_calculated = False
                result = Decimal('0')
                if '*' in amount:
                    amounts = amount.split('*')
                    result = float(amounts[0]) * float(amounts[1])
                    is_calculated = True

                if '/' in amount:
                    amounts = amount.split('/')
                    divisor = float(amounts[1])
                    if divisor > 0:
                        result = float(amounts[0]) / float(divisor)
                    is_calculated = True

                if '+' in amount:
                    amounts = amount.split('+')
                    pp.pprint(amounts)
                    result = float(amounts[0]) + float(amounts[1])
                    is_calculated = True

                if '-' in amount:
                    amounts = amount.split('-')

                    result = 0
                    if amounts[0]:
                        result += Decimal(amounts[0])
                    if amounts[1]:
                        result -= Decimal(amounts[1])
                    is_calculated = True

                if is_calculated:
                    return JsonResponse({'text': 'Amount calculated',
                                         'amount': round(result, 2),
                                         'error': False
                                         })
                else:
                    return JsonResponse({'text': 'Amount could not be calculated, please ensure correct operators are '
                                                 'used',
                                         'amount': amount,
                                         'error': False
                                         })
            except Exception as exception:
                return JsonResponse({'text': 'An unexpected error occurred, please try again',
                                     'error': True,
                                     'details': "{}".format(exception)
                                     })
        else:
            return HttpResponse('')


class InvoiceAddToFlowView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        invoice = Invoice.objects.get(pk=self.kwargs['pk'])
        add_to_flow = AddToFlow(invoice=invoice, profile=self.get_profile(), role=self.get_role())
        add_to_flow.process_flow()
        messages.success(self.request, 'Invoice added to flow successfully.')
        return HttpResponseRedirect(reverse('all-invoices'))


class InvoiceValidateDocumentNumberView(LoginRequiredMixin, View):

    @staticmethod
    def validate_invoice_document_number(document_number, supplier_id):
        return Invoice.objects.filter(invoice_number=document_number, supplier_id=supplier_id).count()

    def get(self, request, *args, **kwargs):
        if 'document_number' in self.request.GET:
            invoice_count = self.validate_invoice_document_number(
                document_number=self.request.GET['document_number'],
                supplier_id=self.request.GET['supplier_id']
            )
            if invoice_count:
                return JsonResponse({'error': True,
                                     'text': 'Document number already exists',
                                     })
            else:
                return JsonResponse({'error': False,
                                     'text': 'Document number valid'
                                     })
        return JsonResponse({'error': True,
                             'text': 'Please enter the document number'
                             })


class InvoiceValidateView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(pk=self.kwargs['pk'])
            invoice.validate_invoice()
            return JsonResponse({'error': False,
                               'text': 'Invoice validation completed',
                               'reload': True
                               })
        except Invoice.DoesNotExist as exception:
            return JsonResponse({'error': False,
                                 'text': 'Invoice not found',
                                 'details': "{}".format(exception),
                                 'reload': True
                                 })


class InvoicePrintView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(pk=self.kwargs['pk'])
            invoice.update_status()
            return JsonResponse({'error': False,
                                 'text': 'Invoice successfully updated',
                                 'reload': True
                                 })
        except Invoice.DoesNotExist as exception:
            return JsonResponse({'error': False,
                                 'text': 'Invoice not found',
                                 'details': "{}".format(exception),
                                 'reload': True
                                 })


class ChangeStatusInvoiceView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def post(self, request, **kwargs):
        if is_ajax(request=self.request):
            with transaction.atomic():
                invoice = self.get_invoice()
                invoice_flow = invoice.get_current_flow()

                profile = self.get_profile()
                role = self.get_role()

                if invoice_flow:
                    invoice.log_activity(self.request.user, self.request.session['role'],
                                         self.request.POST.get('approval_comment', 'Invoice decline changed status'),
                                         Comment.COMMENT)

                    sign = SignInvoice(invoice, profile, role)
                    sign.process_signing()

                    change_decline_status(invoice=invoice, flow=invoice_flow)

                invoice = invoice.revert_decline_request()

                next_invoice_id = invoice.get_role_next_invoice(role=role)
                redirect_url = reverse('all-invoices')
                if next_invoice_id:
                    redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                return JsonResponse({'error': False,
                                     'text': 'Invoice decline status changed successfully',
                                     'redirect': redirect_url
                                     })
        return HttpResponseRedirect(reverse('all-invoices'))


class ApproveInvoiceView(LoginRequiredMixin, ProfileMixin, View):
    template_name = "invoice/approve.html"

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def delete_roles(self, invoice, invoice_flow):
        flows = invoice.flows.all().order_by('order_number')
        for flow in flows:
            if flow.order_number > invoice_flow.order_number:
                flow.delete()

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            role = self.get_role()
            profile = self.get_profile()

            invoice = self.get_invoice()
            invoice_flow = invoice.get_current_flow()

            invoice_journals = InvoiceJournal.objects.filter(invoice_id=invoice.id).count()

            if invoice_flow:
                invoice.log_activity(self.request.user, self.request.session['role'],
                                     "The invoice was approved to be declined", Comment.EVENT)

                sign = SignInvoice(invoice, profile, role)
                sign.process_signing()

                self.delete_roles(invoice, invoice_flow)

            if not invoice.company.run_incoming_document_journal:
                invoice_status = get_object_or_None(Status, slug='decline-printed')
            elif invoice_journals == 0:
                invoice_status = get_object_or_None(Status, slug='decline-printed')
            else:
                invoice_status = get_object_or_None(Status, slug='decline-approved')

            invoice.open_amount = 0
            if invoice_status:
                invoice.status = invoice_status
            invoice.save()

            redirect_url = reverse('all-invoices')
            next_invoice_id = invoice.get_role_next_invoice(role=role)
            if next_invoice_id:
                redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
            return JsonResponse({'error': False,
                                 'text': 'Invoice decline approved successfully',
                                 'redirect': redirect_url
                                 })
        return HttpResponseRedirect(reverse('all-invoices'))


class DeclineInvoiceView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoice/decline.html"
    form_class = InvoiceDeclineForm

    def get_invoice(self):
        try:
            return Invoice.objects.get(pk=self.kwargs['pk'])
        except Invoice.DoesNotExist:
            return None

    def get_form_kwargs(self):
        kwargs = super(DeclineInvoiceView, self).get_form_kwargs()
        kwargs['invoice'] = self.get_invoice()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(DeclineInvoiceView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Invoice could not be declined, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(DeclineInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                next_invoice_id = form.save()

                redirect_url = reverse('all-invoices')
                if next_invoice_id:
                    redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})

                return JsonResponse({'error': False,
                                     'text': 'Invoice successfully declined.',
                                     'redirect': redirect_url
                                     })
        return super(DeclineInvoiceView, self).form_valid(form)


class ApproveInvoiceDeclineView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = "invoice/approve_decline.html"
    form_class = InvoiceDeclineApprovalForm
    model = Invoice

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ApproveInvoiceDeclineView, self).get_context_data(**kwargs)
        invoice = self.get_object()
        context['invoice'] = invoice
        context['is_new'] = True
        context['can_edit'] = False
        if invoice.id in self.request.session['can_edit_invoice']:
            context['can_edit'] = True
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Invoice decline could not be approved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(ApproveInvoiceDeclineView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                try:
                    invoice = form.save()

                    redirect_url = reverse('all-invoices')
                    next_invoice_id = invoice.get_role_next_invoice(role=self.get_role())
                    if next_invoice_id:
                        redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                    return JsonResponse({'error': False,
                                         'text': 'Invoice decline approved successfully',
                                         'redirect': redirect_url
                                         })
                except InvoiceException as exception:
                    return JsonResponse({'error': False, 'text': str(exception)})
        return super(ApproveInvoiceDeclineView, self).form_valid(form)


class InvoiceDeclineNotificationView(LoginRequiredMixin, TemplateView):
    template_name = "invoice/decline_requested.html"

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceDeclineNotificationView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['is_new'] = True
        context['can_edit'] = False
        if invoice.id in self.request.session['can_edit_invoice']:
            context['can_edit'] = True
        return context


class RejectInvoiceView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = "invoice/reject.html"

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(RejectInvoiceView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['is_new'] = True
        context['can_edit'] = False
        if invoice.id in self.request.session['can_edit_invoice']:
            context['can_edit'] = True
        return context


class SaveRejectInvoiceView(LoginRequiredMixin, View):
    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            invoice = self.get_invoice()

            invoice_status = Status.objects.get(slug='rejected-invoice')
            if invoice_status:
                invoice.status = invoice_status
                invoice.save()

            return JsonResponse({
                'error': False,
                'text': 'Invoice rejected successfully',
                'redirect': reverse('invoice_log')
            })
        return HttpResponseRedirect(reverse('all-invoices'))


class EmailSupplierView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoice/email_supplier.html"
    form_class = EmailSupplierForm

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_initial(self):
        initial = super(EmailSupplierView, self).get_initial()
        invoice = self.get_invoice()
        initial['to'] = ''
        initial['subject'] = "Query regarding an invoice: {}".format(invoice.supplier.company.name)
        initial['message'] = "Hi\r\n\n We would like to process your invoice, but we have a query about it we need " \
                             "to resolve. Please can you..."
        return initial

    def get_context_data(self, **kwargs):
        context = super(EmailSupplierView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['supplier'] = invoice.supplier
        return context

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            user = User.objects.get(pk=self.request.session['user'])
            if form.cleaned_data['to'] and form.cleaned_data['message']:
                subject = form.cleaned_data['subject']
                from_address = user.email
                recipient_list = (form.cleaned_data['to'], )
                message = form.cleaned_data['message']
                send_mail(subject, message, from_address, recipient_list)
                return JsonResponse({
                    'error': False,
                    'text': 'Supplier email send successfully.'
                })
        return HttpResponseRedirect(reverse('all-invoices'))


class EmailSupplierInvoiceView(LoginRequiredMixin, View):

    def get_invoice(self):
        return get_object_or_None(Invoice, pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            invoice = self.get_invoice()
            if not invoice:
                return JsonResponse({'error': True, 'text': 'Invoice not found.'})
            # TODO send the mail
            return HttpResponse({'error': False, 'text': 'Email successfully send.'})
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class ParkInvoiceView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoice/park.html"
    form_class = InvoiceParkForm

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super(ParkInvoiceView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ParkInvoiceView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Invoice could not be parked, please fix the errors.',
                'errors': form.errors
            })
        return super(ParkInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            invoice = self.get_invoice()
            if not invoice:
                return JsonResponse({
                    'error': True,
                    'text': 'Invoice not found.'
                })
            invoice_flow = invoice.get_current_flow()
            role = self.request.session['role']
            invoice_flow_role = get_object_or_None(FlowRole, role=role, flow=invoice_flow)
            if not invoice_flow_role:
                return JsonResponse({
                    'error': True,
                    'text': 'Park failed, role to park the invoice not found in current flow',
                })
            invoice_flow_role.status = FlowRole.PARKED
            invoice_flow_role.save()

            flow_intervention = FlowIntervention(event_type='park_it', invoice=invoice)
            flow_intervention.process_intervention()

            invoice.log_activity(self.request.user, self.request.session['role'],
                                 "Parked - {}".format(form.cleaned_data['park_comment']),
                                 Comment.COMMENT)

            return JsonResponse({
                'error': False,
                'text': 'Invoice successfully parked.',
                'redirect': reverse('invoice_detail', kwargs={'pk': invoice.id})
            })

        return HttpResponseRedirect(reverse('all-invoices'))


class UnparkInvoiceView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            invoice = self.get_invoice()
            if not invoice:
                return JsonResponse({
                    'error': True,
                    'text': 'Invoice not found.'
                })
            invoice_flow = invoice.get_current_flow()
            role = self.get_role()
            invoice_flow_role = get_object_or_None(FlowRole, role=role, flow=invoice_flow)
            if invoice_flow_role:
                invoice_flow_role.status = FlowRole.PROCESSING
                invoice_flow_role.save()

                invoice.log_activity(self.request.user, self.request.session['role'],
                                     "The invoice was unparked", Comment.EVENT)

            return JsonResponse({
                'error': False,
                'text': 'Invoice successfully parked.',
                'redirect': reverse('invoice_detail', kwargs={'pk': invoice.id})
            })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class ChangeAccountingDateView(LoginRequiredMixin, FormView):
    template_name = "invoice/change_accounting_date.html"
    form_class = AccountingDateForm

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred trying to save accounting date',
                                 'errors': form.errors
                                 })
        return super(ChangeAccountingDateView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            invoice_ids = self.request.POST.getlist('invoices[]')
            if invoice_ids:
                invoices = Invoice.objects.filter(id__in=invoice_ids).all()
                for invoice in invoices:
                    invoice.accounting_date = form.cleaned_data['accounting_date']
                    invoice.save()
                return JsonResponse({'error': False,
                                     'text': 'Invoices updated successfully.',
                                     'redirect': reverse('invoice_log')
                                     })
            else:
                return JsonResponse({'error': True,
                                     'text': 'Invoice not found.'
                                     })
        return HttpResponseRedirect(reverse('invoice_log'))


class UploadImageView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoice/upload_image.html"
    form_class = AddImageForm

    def get_form_kwargs(self):
        kwargs = super(UploadImageView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Invoice image could not be uploaded, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(UploadImageView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            destination_url = form.execute()
            return JsonResponse({
                'error': False,
                'text': 'File successfully uploaded.',
                'file': destination_url,
                'dismiss_modal': True
            })
        return HttpResponseRedirect(reverse('all-invoices'))


class AddImageView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "invoice/add_image.html"
    form_class = AddImageForm

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super(AddImageView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['invoice'] = self.get_invoice()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddImageView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Invoice image could not be uploaded, please fix the errors.',
                'errors': form.errors
            })
        return super(AddImageView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            destination_url = form.execute()
            return JsonResponse({
                'error': False,
                'text': 'Invoice successfully updated.',
                'reload': True,
                'destination_url': destination_url
            })
        return HttpResponseRedirect(reverse('all-invoices'))


class UpdateAccountingDateView(LoginRequiredMixin, View):

    def post(self, request, **kwargs):
        if is_ajax(request=self.request):
            invoices_ids = self.request.POST['invoices']
            if not invoices_ids:
                return JsonResponse({
                    'error': True,
                    'text': 'Invoices not found.'
                })

            invoices = Invoice.objects.filter(id__in=[int(invoice_id) for invoice_id in invoices_ids])
            accounting_date = self.request.POST['accounting_date']
            for invoice in invoices:
                invoice.accounting_date = accounting_date
                invoice.save()

            return JsonResponse({
                'error': False,
                'text': 'Invoices accounting dates updated.',
                'redirect': reverse('invoice_log')
            })
        else:
            return HttpResponseRedirect(reverse('invoice_log'))


class DeleteInvoiceFlowView(LoginRequiredMixin, View):
    def get_flow(self):
        return get_object_or_None(Flow, pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            flow = self.get_flow()
            if not flow:
                return JsonResponse({
                    'error': True,
                    'text': 'Flow not found.'
                })
            invoice_id = flow.invoice_id
            flow.delete()
            return JsonResponse({
                'error': False,
                'text': 'Flow successfully added.',
                'redirect': reverse('invoice_detail', kwargs={'pk': invoice_id})
            })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class DeleteInvoiceFlowRoleView(LoginRequiredMixin, View):

    def get_flow(self):
        return get_object_or_None(Flow, pk=self.kwargs['flow_id'])

    def get_flow_role(self):
        return get_object_or_None(FlowRole, pk=self.kwargs['role_id'])

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            flow = self.get_flow()
            if not flow:
                return JsonResponse({
                    'error': True,
                    'text': 'Flow not found.'
                })
            flow_role = self.get_flow_role()
            if flow_role:
                invoice_id = flow.invoice_id
                flow_roles = flow.roles.count()
                if flow_roles == 1:
                    flow_role.delete()
                    flow.delete()
                else:
                    flow_role.delete()

                return JsonResponse({
                    'error': False,
                    'text': 'Flow successfully added.',
                    'redirect': reverse('invoice_detail', kwargs={'pk': invoice_id})
                })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class InvoiceView(LoginRequiredMixin, TemplateView):
    template_name = 'invoice/show.html'

    def get_context_data(self, **kwargs):
        context = super(InvoiceView, self).get_context_data(**kwargs)
        invoice = get_object_or_404(Invoice, pk=self.kwargs['pk'])
        context['invoice'] = invoice
        can_transition = invoice.can_transition(self.request.user)
        context['can_transition'] = can_transition
        return context


class InvoiceQueueDetailView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = 'invoice/queue_detail.html'
    form_class = InvoiceLogForm
    model = Invoice

    def get_form_kwargs(self):
        kwargs = super(InvoiceQueueDetailView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def get_invoice(self, invoices):
        for invoice in invoices:
            if self.kwargs['pk'] == invoice.id:
                return invoice
        return Invoice.objects.select_related(
            'invoice_type', 'supplier', 'status', 'company', 'workflow', 'vat_code').get(pk=self.kwargs['pk'])

    def get_invoices(self, company):
        return Invoice.objects.select_related(
            'invoice_type', 'supplier', 'status', 'company', 'workflow', 'vat_code'
        ).filter(
            company=company, status__slug='arrived-at-logger'
        ).exclude(
            deleted__isnull=False
        ).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(InvoiceQueueDetailView, self).get_context_data(**kwargs)
        company = self.get_company()
        company_invoices = self.get_invoices(company=company)
        paginator = Paginator(company_invoices, 1)
        invoices = paginator.page(1)
        invoice = self.get_invoice(company_invoices)
        context['company'] = company
        context['invoices'] = invoices
        if 'page' in self.request.GET:
            page = self.request.GET['page']
            invoices = paginator.page(page)
            if invoices.paginator.count > 0:
                counter = 1
                for inv in invoices:
                    if counter == page:
                        invoice = inv
                    counter += 1
                else:

                    invoice = invoices[0]
            else:
                invoice = invoices[0]
        elif 'pk' in self.kwargs:
            invoice = self.get_invoice(invoices)

        context['invoice'] = invoice
        context['images'] = invoice.get_images()
        context['MEDIA_URL'] = settings.MEDIA_URL
        # view_data = get_page_viewer(self.request)
        context['locked_status'] = {'is_locked': False, 'locked_by': None}
        context['permissions'] = Role.objects.get_permissions_list(self.request.session['role'])
        context['profile'] = self.get_profile()
        context['year'] = self.get_year()
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(InvoiceQueueDetailView, self).form_invalid(form)

    # noinspection PyMethodMayBeStatic
    def get_next_logger_invoice(self, source_invoice):
        return Invoice.objects.get_at_logger(source_invoice.company, [source_invoice.id]).first()

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            try:
                invoice = form.save(commit=False)
                next_invoice = self.get_next_logger_invoice(invoice)
                redirect_url = reverse('invoice_log')
                if next_invoice:
                    redirect_url = reverse('invoice_queue_detail', kwargs={'pk': next_invoice.pk})

                text = 'Invoice updated successfully'
                if self.request.POST.get('add_to_flow', None):
                    text = 'Invoice successfully saved and added to flow'

                return JsonResponse({
                    'text': text,
                    'error': False,
                    'redirect': redirect_url
                })
            except (DuplicateInvoiceNumber, InvoiceException) as exception:
                return JsonResponse({
                    'text': str(exception),
                    'error': True
                })
            except AccountPostingNotSetException as exception:
                return JsonResponse({
                    'text': str(exception),
                    'error': True,
                })


class UpdateInvoiceView(LoginRequiredMixin, ProfileMixin, UpdateView):
    model = Invoice
    form_class = InvoiceForm

    def get_form_kwargs(self):
        kwargs = super(UpdateInvoiceView, self).get_form_kwargs()
        company = self.get_company()
        kwargs['company'] = company
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the invoice, please fix the errors.',
                'errors': form.errors,
                'alert': True
            }, status=400)
        return super(UpdateInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            try:
                with transaction.atomic():
                    form.save()

                return JsonResponse({
                    'error': False,
                    'text': 'Invoice successfully updated',
                    'reload': True
                })
            except InvalidPeriodAccountingDateException as accounting_date_period_exception:
                logger.exception(accounting_date_period_exception)
                return JsonResponse({
                    'error': True,
                    'text': 'Invalid period and account date, please update',
                    'details': f'Error saving the invoice {str(accounting_date_period_exception)}',
                    'alert': True,
                    'errors': {'accounting_date': ['Please update to match with period'],
                               'period': ['Please update to match with accounting date']
                               }
                })
        return HttpResponse('Not allowed')


class InvoiceSaveChangeView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = InvoiceLogForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceSaveChangeView, self).get_form_kwargs()
        company = self.get_company()
        kwargs['company'] = company
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the invoice, please fix the errors.',
                'errors': form.errors,
                'alert': True
            }, status=400)
        return super(InvoiceSaveChangeView, self).form_invalid(form)


class InvoiceDetailView(LoginRequiredMixin, ProfileMixin, RoleInvoiceMixin, TemplateView):
    template_name = 'invoice/detail.html'

    def get_invoice(self):
        if self.request.GET.get('status') == 'all':
            return Invoice.objects.fetch().get(company=self.get_company(), pk=self.kwargs['pk'])
        else:
            return Invoice.objects.fetch().not_declined().get(company=self.get_company(), pk=self.kwargs['pk'])

    def is_current(self, flow_role):
        return flow_role.is_signed is False and flow_role.flow.is_current

    def status_valid(self, flow_role):
        return flow_role.status not in [FlowRole.ACTION_REQUIRED]

    def is_valid_flow(self, flow_role):
        if flow_role.flow and flow_role.flow.invoice.company:
            return self.is_current(flow_role) and self.status_valid(flow_role)
        return False

    def get_invoices(self):
        invoice_filter = self.request.GET.get('type', '')
        status = self.request.GET.get('status', None)
        if invoice_filter == 'upcoming':
            role_invoices = self.get_role_invoices(is_current=False, role=self.get_role(), year=self.get_year())
        elif status:
            role_invoices = Invoice.objects.with_intervention_check().annotate(
                df_number=Concat(
                    'company__slug', V(' - '), 'invoice_id', V(''), output_field=CharField()
                ),
            ).select_related(
                'status', 'company', 'supplier', 'invoice_type', 'workflow'
            ).prefetch_related(
                'invoice_references', 'flows', 'flows__status', 'flows__roles'
            ).filter(
                status__slug__in=[status], company=self.get_company()
            )
        else:
            role_invoices = self.get_role_invoices(is_current=True, role=self.get_role(), year=self.get_year())
        return role_invoices

    def get_next_invoice(self, invoice, invoices):
        for _invoice in invoices:
            if invoice.id != _invoice.id:
                return _invoice
        return None

    def get_previous_invoice(self, invoice, invoices, page):
        for _invoice in invoices:
            if invoice.id != _invoice.id:
                return _invoice
        return None

    def get_last_invoice(self, invoice, invoices, page):
        counter = 0
        for _invoice in invoices:
            counter = counter + 1
            if invoice.id != _invoice.id and page == counter:
                return _invoice
        return None

    def role_permisssions(self, role_id):
        return Role.objects.get_permissions_list(role_id)

    def get_role_permissions(self):
        role_id = self.request.session['role']

        if 'permissions' in self.request.session:
            if role_id in self.request.session['permissions']:
                return self.request.session['permissions'][role_id]
            else:
                permissions = self.role_permisssions(role_id)
                self.request.session['permissions'][role_id] = permissions
                return permissions
        else:
            permissions = self.role_permisssions(role_id)
            self.request.session['permissions'] = {role_id: permissions}
            return permissions

    def get_page_invoice(self, company_invoices, paginator):
        invoice = None
        if 'page' in self.request.GET:
            page = self.request.GET['page']
            invoices = paginator.page(page)
            if not invoices.paginator.count > 0:
                page_num = 1
                for inv in company_invoices:
                    if page_num == int(page):
                        invoice = inv
                        break
                    page_num += 1
            else:
                invoice = invoices[0]

        if not invoice:
            invoice = self.get_invoice()
        return invoice

    def get_current_flow(self, invoice):
        for flow in invoice.flows.all():
            if flow.is_current and flow.is_signed == False:
                return flow
        return None

    def get_flow_role(self, invoice_flow):
        if invoice_flow:
            for flow_role in invoice_flow.roles.all():
                if int(self.request.session['role']) == flow_role.role_id:
                    return flow_role
            return None

    def flow_can_edit(self, invoice_flow, can_edit):
        if invoice_flow and invoice_flow.status:
            if invoice_flow.status.slug == 'parked':
                can_edit = False
        return can_edit

    def get_redirect_url(self, invoice):
        redirect_url = self.request.GET.get('redirect_url', None)
        status = self.request.GET.get('status', None)
        if not redirect_url:
            redirect_url = self.request.GET.get('redirect', None)
        if redirect_url:
            self.request.session[f'invoice_detail_{invoice.id}'] = redirect_url
        else:
            redirect_url = self.request.session.get(f'invoice_detail_{invoice.id}', None)
        if status:
            redirect_url = f"{redirect_url}&status={status}"
        return redirect_url

    def is_upcoming(self):
        return self.request.GET.get('type', None) == 'upcoming'

    def get_default_invoice(self, role_invoices, paginator):
        invoice = None
        page = self.request.GET.get('page', None)
        if page:
            invoices = paginator.page(page)
            if not invoices.paginator.count > 0:
                page_num = 1
                for inv in role_invoices:
                    if page_num == int(page):
                        invoice = inv
                        break
                    page_num += 1
            else:
                invoice = invoices[0]

        if not invoice:
            invoice = self.get_invoice()
        return invoice

    def get_context_data(self, **kwargs):
        context = super(InvoiceDetailView, self).get_context_data(**kwargs)

        role_invoices = self.get_invoices()
        paginator = Paginator(object_list=role_invoices, per_page=1)
        invoices = paginator.page(number=self.request.GET.get('page', 1))

        invoice = self.get_default_invoice(role_invoices=role_invoices, paginator=paginator)

        if not invoice:
            return redirect(reverse('all-invoices'))

        can_edit = False
        invoice_flow = self.get_current_flow(invoice)
        editable_invoices = self.request.session.get('can_edit_invoice', [])
        flow_status = None
        context['company'] = self.get_company()
        context['invoices'] = invoices

        context['invoice'] = invoice
        # context['next_invoice'] = next_invoice
        context['can_transition'] = None
        context['workflow'] = None
        context['intervention_comment'] = ''
        if invoice_flow:
            context['intervention_is_required'] = invoice_flow.intervention_at
            intervention_flow_role = get_object_or_None(FlowRole, flow=invoice_flow, intervention__isnull=False)
            if intervention_flow_role:
                context['intervention_comment'] = intervention_flow_role.intervention.comment

        if invoice:
            context['can_transition'] = invoice.can_transition(self.request.user)
            if invoice.workflow:
                context['workflow'] = invoice.workflow
            if invoice.id in editable_invoices:
                can_edit = True

        can_edit = self.flow_can_edit(invoice_flow, can_edit)
        invoice_flow_role = self.get_flow_role(invoice_flow)
        is_parked = False
        if invoice_flow_role and invoice_flow_role.status == FlowRole.PARKED:
            is_parked = True
            can_edit = False
        if invoice.is_locked:
            can_edit = False
        if invoice:
            if invoice.workflow:
                context['workflow'] = invoice.workflow
            can_edit = invoice.id in editable_invoices
        context['locked_status'] = get_page_viewer(self.request)
        context['is_parked'] = is_parked
        context['can_edit'] = can_edit
        context['flow_status'] = flow_status
        context['images'] = invoice.get_images()
        context['MEDIA_URL'] = settings.MEDIA_URL
        permissions = self.get_role_permissions()
        context['permissions'] = permissions
        context['is_upcoming'] = self.is_upcoming()
        context['redirect_url'] = self.get_redirect_url(invoice)
        context['profile_id'] = self.request.session.get('profile', None)
        context['year'] = self.get_year()

        context['can_order_purchase_order'] = invoice_flow_role
        context['can_approve_purchase_order'] = invoice_flow_role
        return context


class InvoiceShowDetailView(InvoiceDetailView):

    def get_invoice(self):
        return Invoice.objects.fetch().get(company=self.get_company(), pk=self.kwargs['pk'])


class InvoiceHistoryView(LoginRequiredMixin, TemplateView):
    template_name = 'invoice/changes.html'

    def get_context_data(self, **kwargs):
        context = super(InvoiceHistoryView, self).get_context_data(**kwargs)
        context['invoice_log_entries'] = LogEntry.objects.get_for_model(Invoice)
        return context


class InvoiceFlowHistoryView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Comment
    template_name = 'invoice/flow_changes.html'
    context_object_name = 'comments'

    def get_queryset(self):
        return Comment.objects.filter(invoice_flow__invoice__company=self.get_company())


class InvoiceSignView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return Invoice.objects.select_related('invoice_type').get(pk=self.kwargs['pk'])

    def sign_invoice(self, invoice: Invoice, flow: Flow):
        profile = self.get_profile()
        role = self.get_role()

        sign = SignInvoice(invoice=invoice, profile=profile, role=role)
        sign.process_signing()
        if flow and flow.intervention_at:
            invoice.log_activity(user=self.request.user, role=role, comment='Intervention signed off', log_type=Comment.EVENT)

        page_number = int(float(self.request.GET.get('page', 1)))
        count_invoices = int(float(self.request.GET.get('count', 1)))
        invoices_left = count_invoices - 1
        next_index = next_page = 1
        if invoices_left == page_number:
            next_index = page_number - 1
        elif invoices_left > page_number:
            next_index = page_number
        elif 0 < invoices_left < count_invoices:
            next_index = invoices_left - 1
        else:
            next_index = next_index - 1
        next_page = next_index + 1
        next_invoice_id = invoice.get_role_next_invoice(role=role, next_index=next_index)
        return next_invoice_id, next_page

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            try:
                invoice = self.get_invoice()

                with transaction.atomic():
                    # We need to know if the current flow state had been done you cannot
                    # add after it, so we hide the buttons to delete and/or add
                    current_flow = invoice.get_current_flow()

                    if not current_flow:
                        return JsonResponse({
                            'error': False,
                            'text': 'Document flow not found, please try again.',
                            'redirect': reverse('all-invoices')
                        })

                    # Ensure the vat is distributed to correct accounts
                    recalculate_posting_vat(
                        invoice=self.get_invoice(),
                        profile=self.get_profile(),
                        role=self.get_role()
                    )

                    has_variance = invoice.has_variance()
                    has_no_vat = invoice.has_vat_with_no_vat_code_type()
                    at_last = invoice.at_last()

                    if at_last:
                        fixed_asset_account = False
                        if invoice.has_account_posting:
                            fixed_asset_account = invoice.get_invoice_account_linked_to_asset(
                                exclude_invoice_accounts=self.request.GET.getlist('exclude_invoice_accounts[]')
                            )
                        if has_no_vat:
                            raise InvoiceException('You have to select a VAT code if you posting to a VAT account, '
                                                   'please fix in the document details and in your posting line')
                        elif has_variance:
                            raise InvoiceHasVariance('The document has not been totally allocated, do you want to '
                                                     'edit this')
                        elif invoice.has_default_company_account():
                            raise DefaultAccountNotPosted('The Default Account has not been posted, either a) add '
                                                          'someone to the flow or b) fix the line posting so there '
                                                          'is not default account in account posting section')
                        elif not invoice.incoming_journal_printed:
                            raise InvoiceJournalNotAvailable('Please wait a few hours with this invoice, it needs '
                                                             'to be added to a Journal of incoming invoices.')
                        elif fixed_asset_account:
                            return JsonResponse({
                                'error': False,
                                'text': 'You has posted to an asset please complete the asset details below',
                                'alert': True,
                                'create_asset': True,
                                'invoice_account_id': fixed_asset_account.id,
                                'asset_url': reverse('create_invoice_asset', kwargs={
                                    'invoice_id': invoice.id,
                                    'invoice_account_id': fixed_asset_account.id
                                })
                            })
                        else:
                            redirect_url = reverse('all-invoices')
                            next_invoice_id, next_page = self.sign_invoice(invoice=invoice, flow=current_flow)
                            if next_invoice_id:
                                redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                            return JsonResponse({
                                'error': False,
                                'text': 'Document successfully signed.',
                                'redirect': redirect_url
                            })
                    else:
                        has_valid_accounting_date = invoice.validate_accounting_date_period()
                        if has_no_vat:
                            raise InvoiceException(
                                'You have to select a VAT code if you posting to a VAT account, please fix in the'
                                ' document details and in your posting line')
                        elif has_variance:
                            return JsonResponse({
                                'error': True,
                                'text': 'The document has not  been totally allocated, either add a role to this '
                                        'flow or edit the account postings',
                                'alert': True,
                                'url': reverse('notify_invoice_has_variance', kwargs={'pk': invoice.id})
                            })
                        elif not has_valid_accounting_date:
                            raise AccountingDateClosed('The accounting date is closed, please change the '
                                                       'accounting date to the current period that is open')
                        else:
                            redirect_url = reverse('all-invoices')
                            next_invoice_id, next_page = self.sign_invoice(invoice, current_flow)
                            if next_invoice_id:
                                url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                                if next_page:
                                    redirect_url = f"{url}?page={next_page}"
                                else:
                                    redirect_url = url

                            self.request.session['invoice_fixed_assets'] = {}
                            return JsonResponse({
                                'error': False,
                                'text': 'Document successfully signed.',
                                'redirect': redirect_url
                                })
            except InvoiceException as ex:
                logger.exception(ex)
                return JsonResponse({
                    'error': True,
                    'text': str(ex),
                    'details': str(ex),
                    'alert': True
                })
        else:
            return HttpResponse("Not allowed")


class UndoInvoiceSignView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def undo_signing(self, invoice, current_flow):
        logger.info("Undo sign invoice at {}".format(current_flow))

        sign = UnSignInvoice(invoice=invoice, profile=self.get_profile(), role=self.get_role(), current_flow=current_flow)
        sign.execute()

    def get(self, request, *args, **kwargs):
        if is_ajax(request=self.request):
            try:
                with transaction.atomic():
                    invoice = self.get_invoice()

                    status_slug = invoice.status.slug
                    # We need to know if the current flow state had been done you cannot
                    # add after it, so we hide the buttons to delete and/or add

                    current_flow = Flow.objects.current(invoice=invoice).first()

                    if current_flow:
                        self.undo_signing(invoice, current_flow)
                        redirect_url = reverse('show_status_invoices', kwargs={'slug': status_slug})
                        return JsonResponse({
                            'error': False,
                            'text': 'Document successfully unsigned.',
                            'redirect': redirect_url
                            })
                    else:
                        return JsonResponse({
                            'error': False,
                            'text': 'Document flow not found, please try again.'
                        })
            except InvoiceException as ex:
                logger.exception(ex)
                return JsonResponse({'error': True,
                                     'text': f'{str(ex)}',
                                     'details': f'{str(ex)}',
                                     'alert': True
                                     })
        else:
            return HttpResponse("Not allowed")


class InvoiceNotifyWithVarianceView(LoginRequiredMixin, TemplateView):
    template_name = "invoice/variance_notification.html"

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceNotifyWithVarianceView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['can_edit'] = False
        if invoice.id in self.request.session['can_edit_invoice']:
            context['can_edit'] = True
        return context


class InvoiceSignWithVarianceView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def sign_invoice(self, invoice, current_flow):
        profile = Profile.objects.get(pk=self.request.session['profile'])
        role = Role.objects.get(pk=self.request.session['role'])
        sign = SignInvoice(invoice, profile, role)
        sign.process_signing()

        if current_flow and current_flow.intervention_at:
            invoice.log_invoice_activity(current_flow, self.request.user, self.request.session['role'],
                                         'Intervention signed off', Comment.EVENT)
        # invoice.log_invoice_activity(current_flow, self.request.user, self.request.session['role'],
        #                              'Invoice signed', Comment.EVENT)

        return invoice.get_role_next_invoice(role=self.get_role())

    def get(self, request, *args, **kwargs):
        with transaction.atomic():
            invoice = self.get_invoice()
            if not invoice:
                return JsonResponse({
                        'error': True,
                        'text': 'Invoice not found',
                        'alert': True
                    })
            # check if there is a role after the current active role
            current_flow = invoice.get_current_flow()

            if not current_flow:
                return JsonResponse({
                        'error': True,
                        'text': 'Invoice flow not found',
                        'alert': True
                    })

            at_last = invoice.at_last()

            has_variance = invoice.has_variance()

            # At last user
            if at_last:

                is_valid_accounting_date_period = invoice.validate_accounting_date_period()
                # Default Expense account as per SAL may not be in the account posting section
                # there are no more roles on this invoice
                # Check if there is Default account posting still in the account posting side of things

                if not invoice.incoming_journal_printed:  # 1. Invoice status must be 20
                    return JsonResponse({
                            'error': True,
                            'text': 'Please wait a few hours with this invoice, it needs to be '
                                    'added to a Journal of incoming invoices.',
                            'alert': True
                        })
                elif invoice.has_default_company_account():
                    return JsonResponse({
                            'error': True,
                            'text': 'The Default Account has not been posted, either a) add someone to the'
                                    ' flow or b) fix the line posting so there is not default account in '
                                    'account posting section',
                            'alert': True
                        })
                elif has_variance:  # There cannot be a variance
                    return JsonResponse({'error': True,
                                         'text': 'The invoice has not  been totally allocated, either add'
                                                 ' a role to this flow or edit the account postings',
                                         'alert': True,
                                         'url': reverse('notify_invoice_has_variance', kwargs={'pk': invoice.id})
                                         })
                elif not is_valid_accounting_date_period:  # check the ACCOUNTING DATE period is open
                    return JsonResponse({
                            'error': True,
                            'text': 'The accounting date is closed, please change the accounting date to'
                                    ' the current period that is open',
                            'alert': True
                        })
                else:
                    next_invoice_id = self.sign_invoice(invoice, current_flow)
                    redirect_url = reverse('all-invoices')
                    if next_invoice_id:
                        redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                    return JsonResponse({
                            'error': False,
                            'text': 'Invoice successfully signed.',
                            'redirect': redirect_url
                        })
            else:
                if invoice.validate_accounting_date_period():
                    next_invoice_id = self.sign_invoice(invoice, current_flow)
                    redirect_url = reverse('all-invoices')
                    if next_invoice_id:
                        redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
                    return JsonResponse({
                            'error': False,
                            'text': 'Invoice successfully signed.',
                            'redirect': redirect_url
                        })
                else:
                    return JsonResponse({
                            'error': True,
                            'text': 'The accounting date is closed, please change the accounting date to'
                                    ' the current period that is open',
                            'alert': True
                        })


class CreateJournalForCancelledInvoiceView(LoginRequiredMixin, ProfileMixin, View):

    def generate_unique_number(self, company, counter=1):
        role = Role.objects.get(pk=self.request.session['role'])
        profile = Profile.objects.get(pk=self.request.session['profile'])
        counter_str = str(counter).rjust(5, '0')
        company_code = (company.code[0:3]).upper()
        unique_number = "{}-{}-JCI-{}".format(company_code, profile.code, counter_str)

        result = get_object_or_None(Journal, report_unique_number=unique_number)
        if result:
            counter = counter + 1
            return self.generate_unique_number(company, counter)
        else:
            return unique_number

    def get(self, request, *args, **kwargs):
        from_invoice_status = Status.objects.get(slug='invoice-blocked')
        to_invoice_status = Status.objects.get(slug='invoice-deleted')
        if from_invoice_status and to_invoice_status:
            company = self.get_company()
            invoices = Invoice.objects.filter(status=from_invoice_status, company=company)
            if invoices:

                unique_number = self.generate_unique_number(company, 1)
                journal = Journal.objects.create(
                    report_unique_number=unique_number,
                    journal_type=Journal.CANCELLED,
                    created_by=self.request.user.profile,
                    company=company
                )
                for invoice in invoices:
                    InvoiceJournal.objects.create(
                        invoice=invoice,
                        journal=journal
                    )
                return JsonResponse({'error': False,
                                     'text': 'Journal for cancelled invoices successfully created'
                                     })
            else:
                return JsonResponse({'error': True,
                                     'text': 'No invoices to process'
                                     })


class JournalForCancelledInvoiceView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/journal/cancelled.html'

    def get_invoices(self, company):
        return Invoice.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(JournalForCancelledInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['invoices'] = self.get_invoices(company)
        return context


class CancelledInvoiceJournalView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/journal/cancelled_journal.html'

    def get_journal_invoice_accounts(self, journal, invoices_journals):
        return InvoiceAccount.objects.filter(
            invoice_id__in=[journal.invoice_id for journal in invoices_journals]
        ).all()

    def get_journal_invoices(self, journal):

        invoices_journals = InvoiceJournal.objects.prefetch_related('links').filter(
            journal__journal_type=Journal.INCOMING, journal=journal, invoice__invoice_type__is_account_posting=True
        ).all()
        report = {'invoices': {}, 'accounts': [], 'total': 0, 'total_vat':0, 'total_excl_vat': 0,
                  'invoice_account_total': 0}

        invoice_accounts = self.get_journal_invoice_accounts(journal=journal, invoices_journals=invoices_journals)

        for invoice_journal in invoices_journals:
            invoice = invoice_journal.invoice
            total_debit_nett = 0
            total_debit_vat = 0

            if invoice.is_credit_type:
                debit_nett = invoice_journal.net_amount * -1
                debit_vat = invoice_journal.vat_amount * -1
            else:
                debit_nett = invoice_journal.net_amount
                debit_vat = invoice_journal.vat_amount

                if invoice_journal.vat_amount:
                    report['total_vat'] = report['total_vat'] + debit_vat

            reference_1 = reference_2 = None
            invoice_references = invoice.invoice_references.all()
            if len(invoice_references) > 0:
                reference_1 = invoice_references[0]
            if len(invoice_references) > 1:
                reference_2 = invoice_references[1]

            children = []
            for invoice_journal_link in invoice_journal.links.all():
                _link_debit_nett = invoice_journal_link.net_amount * -1
                _link_debit_vat = (invoice_journal_link.vat_amount * -1)
                total_debit_nett = total_debit_nett + _link_debit_nett
                total_debit_vat = total_debit_vat + _link_debit_vat

                _link_reference_1 = _link_reference_2 = None
                invoice_references = invoice_journal_link.invoice.invoice_references.all()
                if len(invoice_references) > 0:
                    _link_reference_1 = invoice_references[0]
                if len(invoice_references) > 1:
                    _link_reference_2 = invoice_references[1]

                children.append({
                    'docuflow_unique': invoice_journal_link.invoice,
                    'accounting_date': invoice_journal_link.invoice.accounting_date,
                    'invoice_date': invoice_journal_link.invoice.invoice_date,
                    'due_date': invoice_journal_link.invoice.due_date,
                    'supplier_name': invoice_journal_link.invoice.supplier.name,
                    'document_type': invoice_journal_link.invoice.invoice_type,
                    'supplier_inv': invoice_journal_link.invoice.supplier.code,
                    'reference_1': _link_reference_1,
                    'reference_2': _link_reference_2,
                    'debit_nett': _link_debit_nett,
                    'debit_vat': _link_debit_vat,
                    'credit_total': (_link_debit_vat + _link_debit_nett),
                })

            report['total_excl_vat'] = report['total_excl_vat'] + total_debit_nett
            report['debit_nett'] = total_debit_nett
            report['debit_vat'] = total_debit_vat
            report['total'] = report['total'] + (total_debit_vat + total_debit_nett)

            report['invoices'][invoice_journal.id] = {
                'docuflow_unique': invoice,
                'accounting_date': invoice.accounting_date,
                'invoice_date': invoice.invoice_date,
                'due_date': invoice.due_date,
                'supplier_name': invoice.supplier.name,
                'document_type': invoice.invoice_type,
                'supplier_inv': invoice.supplier.code,
                'reference_1': reference_1,
                'reference_2': reference_2,
                'debit_nett': debit_nett,
                'debit_vat': debit_vat,
                'credit_total': (debit_vat + debit_nett),
                'children': children
            }

        for invoice_account in invoice_accounts:
            report_invoice_account = {'account': invoice_account.account.code,
                                      'account_description': invoice_account.account.name,
                                      'description': invoice_account.account,
                                      'debit': invoice_account.amount,
                                      'credit': invoice_account.amount
                                      }
            report['accounts'].append(report_invoice_account)
            report['total_credit'] = report['invoice_account_total'] + invoice_account.amount
            report['invoice_account_total'] = report['invoice_account_total'] + invoice_account.amount
        return report

    def get_journal(self):
        return Journal.objects.get(pk=self.kwargs['pk'])

    def get_role_profiles(self, journal):
        role_profiles = []
        if journal.role:
            for profile in journal.role.profile_set.all():
                role_profiles.append(f"{profile.user.first_name} {profile.user.last_name}")
        return ', '.join(role_profiles)

    def get_context_data(self, **kwargs):
        context = super(CancelledInvoiceJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        journal = self.get_journal()
        context['company'] = company
        context['journal'] = journal
        default_supplier_account = None
        default_expenditure_account = None
        debit_vat_account = None
        report = self.get_journal_invoices(journal)
        if company.default_vat_account:
            debit_vat_account = company.default_vat_account.code
        if company.default_expenditure_account:
            default_expenditure_account = company.default_expenditure_account.code
        if company.default_supplier_account:
            default_supplier_account = company.default_supplier_account.code
        report['debit_nett_account'] = default_expenditure_account
        report['debit_credit_account'] = default_supplier_account
        report['debit_vat_account'] = debit_vat_account
        invoice_accounts = []
        invoice_account_total = 0
        role_profiles = self.get_role_profiles(journal)

        context['invoice_account_total'] = invoice_account_total
        context['invoice_accounts'] = invoice_accounts
        context['role_profiles'] = role_profiles
        context['report'] = report
        return context


class InvoiceCapturedIndexView(LoginRequiredMixin, ProfileMixin, FilterView):
    template_name = 'invoice/captured/index.html'
    filterset_class = InvoiceCapturedFilter
    model = Invoice
    context_object_name = 'invoices'

    def get_queryset(self):
        return Invoice.objects.company(self.request.session['company']).paid()

    def get_context_data(self, **kwargs):
        context = super(InvoiceCapturedIndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context


class InvoiceCapturedDownloadView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoices(self):
        return Invoice.objects.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).filter(company=self.get_company())

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="captured_invoices.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Show Captured Invoices')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['ID', 'Company', 'Supplier', 'Invoice Date', ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        rows = self.get_invoices().values_list('id', 'company', 'supplier', 'invoice_date')
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response


class InvoiceShowIndexView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'invoice/show/report.html'

    def access_level(self, filter_options):
        permissions = []
        role_id = str(self.request.session['role'])
        if 'permissions' in self.request.session and role_id in self.request.session['permissions']:
            permissions = self.request.session['permissions'][role_id]
        if 'reports_invoice_limited_access_to_records_view_user_part_of_flow' in permissions:
            flows = Flow.objects.filter(id__in=Subquery(FlowRole.objects.filter(role_id=role_id).values('flow_id')))
            filter_options['id__in'] = [flow.invoice_id for flow in flows]
        return filter_options

    def get_context_data(self, **kwargs):
        context = super(InvoiceShowIndexView, self).get_context_data(**kwargs)
        context['filter'] = ShowInvoiceFilter
        context['status'] = self.request.GET.get('status')
        invoices_url = reverse('invoices_list_json')
        if self.request.GET.get('status'):
            invoices_url = invoices_url + f"?status={self.request.GET.get('status')}"
        context['invoices_url'] = invoices_url
        return context


class InvoiceShowReportView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'invoice/show/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['status_slug'] = self.request.GET.get('status', 'all')
        return context


class InvoiceShowDownloadView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoices(self):
        return Invoice.objects.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).filter(company=self.get_company()).exclude(deleted__isnull=False)

    def get(self,  *args, **kwargs):
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="show_invoices.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Show Invoices')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['ID', 'Company', 'Supplier', 'Invoice Date', ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        rows = self.get_invoices().values_list('id', 'company', 'supplier', 'invoice_date')
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response


class InvoiceBalanceIndexView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'invoice/balance/index.html'
    form_class = CapturedInvoiceForm

    def get_invoices(self, company):
        return Invoice.objects.filter(company=company)

    def get_form_kwargs(self):
        kwargs = super(InvoiceBalanceIndexView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(InvoiceBalanceIndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['invoices'] = self.get_invoices(company)
        return context


class InvoiceBalanceReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/balance/report.html'

    def get_context_data(self, **kwargs):
        context = super(InvoiceBalanceReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context


class InvoiceBottleneckIndexView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'invoice/bottleneck/index.html'
    form_class = InvoiceBottleneckReportForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceBottleneckIndexView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_invoices(self, company):
        return Invoice.objects.filter(company=company)

    def get_roles(self, company):
        return Role.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(InvoiceBottleneckIndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['roles'] = self.get_roles(company)
        return context


class InvoiceBottleneckReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/bottleneck/report.html'

    def get_invoices(self, company):
        return Invoice.objects.filter(company=company)

    def get_roles(self, company):
        return Role.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(InvoiceBottleneckReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['roles'] = self.get_roles(company)
        return context


class InvoiceProcessingDaysView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'invoice/processing_days/index.html'
    form_class = InvoiceProcessingDayReportForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceProcessingDaysView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(InvoiceProcessingDaysView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context


class InvoiceProcessingDaysReportView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'invoice/processing_days/report.html'
    form_class = InvoiceProcessingDayReportForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceProcessingDaysReportView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_suppliers(self, company):
        return Supplier.objects.select_related('company').filter(company=company, is_blocked=False, is_active=True)

    def get_context_data(self, **kwargs):
        context = super(InvoiceProcessingDaysReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['suppliers'] = self.get_suppliers(company)
        return context


class InvoiceForecastingIndexView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'invoice/forecasting/index.html'
    form_class = InvoicePaymentForecastReportForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceForecastingIndexView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(InvoiceForecastingIndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context


class InvoiceForecastingReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/forecasting/report.html'

    def get_filter_options(self, company):
        options = {'company': company, 'invoice_type__is_account_posting': True}
        invoice_statuses = []
        completed = completed_statuses()
        processing = processing_statuses()
        if 'status' in self.request.GET and self.request.GET['status']:
            for status in self.request.GET.getlist('status'):
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        else:
            invoice_statuses = completed
        if len(invoice_statuses) > 0:
            options['status__slug__in'] = invoice_statuses
        return options

    def get_available_payments(self, company):
        filter_dict = {'company': company}
        if 'supplier_id' in self.request.GET and self.request.GET['supplier_id']:
            filter_dict['supplier_id'] = self.request.GET['supplier_id']
        # TODO - Ensure same query is used as in the unpaid supplier invoices and supplier age analysis
        payments = Payment.objects.select_related(
            'company', 'supplier', 'period', 'period__period_year'
        ).prefetch_related(
            'invoices', 'children', 'parent'
        ).filter(**filter_dict)

        supplier_invoices = {}
        total_payment = 0
        for payment in payments:
            if payment.available_amount > 0:
                if payment.supplier in supplier_invoices:
                    supplier_invoices[payment.supplier]['payments'].append(payment)
                    if payment.available_amount:
                        supplier_invoices[payment.supplier]['total_payment'] += payment.display_available_amount
                    if payment.total_discount:
                        supplier_invoices[payment.supplier]['total_payment_discount'] += payment.total_discount

                    total_payment += payment.display_available_amount
                else:
                    total_payment += payment.display_available_amount
                    supplier_invoices[payment.supplier] = {'total': 0,
                                                           'total_discount': 0,
                                                           'payments': [payment],
                                                           'total_payment': payment.display_available_amount,
                                                           'total_payment_discount': payment.total_discount
                                                           }
        return supplier_invoices, total_payment

    def display_statuses(self, statuses):
        completed = completed_statuses()
        processing = processing_statuses()

        statuses_list = []
        for _completed_status in completed:
            if 'status__slug__in' in statuses and _completed_status in statuses['status__slug__in']:
                statuses_list.append('completed')

        for _processing_status in processing:
            if 'status__slug__in' in statuses and _processing_status in statuses['status__slug__in']:
                statuses_list.append('processing')
        return statuses_list

    def get_context_data(self, **kwargs):
        context = super(InvoiceForecastingReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        filter_options = self.get_filter_options(company)

        report = ForecastingReport(company)
        report.process_report(filter_options)

        selected_statuses = self.display_statuses(filter_options)
        payments, total_payment = self.get_available_payments(company)

        context['supplier_invoices'] = payments
        context['total_payment'] = total_payment
        context['statuses'] = selected_statuses
        context['company'] = company
        context['report'] = report.day_invoices
        context['total'] = report.total
        return context


class PaymentOnDueDateReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/forecasting/invoices.html'

    def get_filter_options(self, company):
        options = {'company': company, 'invoice_type__is_account_posting': True}
        invoice_statuses = []
        completed = completed_statuses()
        processing = processing_statuses()
        if 'status' in self.request.GET and self.request.GET['status']:
            for status in self.request.GET.getlist('status'):
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        else:
            invoice_statuses = completed
        if len(invoice_statuses) > 0:
            options['status__slug__in'] = invoice_statuses
        return options

    def get_due_date_invoices(self, due_date, company):
        due_date_invoices = {'total_vat': 0, 'total_net': 0, 'total': 0, 'invoices': []}
        filter_options = self.get_filter_options(company)
        filter_options['due_date'] = due_date
        invoices = Invoice.objects.select_related(
            'company', 'status', 'supplier', 'invoice_type'
        ).prefetch_related('invoice_references').filter(**filter_options).exclude(deleted__isnull=False)

        for invoice in invoices:
            due_date_invoices['total_vat'] = due_date_invoices['total_vat'] + invoice.vat_amount
            due_date_invoices['total_net'] = due_date_invoices['total_net'] + invoice.net_amount
            due_date_invoices['total'] = due_date_invoices['total'] + invoice.total

            due_date_invoices['invoices'].append({
                'invoice': invoice
            })
        return due_date_invoices

    def get_context_data(self, **kwargs):
        context = super(PaymentOnDueDateReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        due_date = date(int(self.kwargs['year']), int(self.kwargs['month']), int(self.kwargs['day']))

        report = self.get_due_date_invoices(due_date, company)

        context['company'] = company
        context['payments'] = []
        context['report'] = report
        context['due_date'] = due_date
        return context


class InvoiceFlowStatusIndexView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/flow_status/index.html'

    def get_role_profiles(self, flow_roles):
        role_users = {}
        for profile in Profile.objects.select_related('user').prefetch_related('roles').filter(roles__in=flow_roles.values_list('role_id', flat=True)).all():
            for profile_role in profile.roles.all():
                if profile_role in role_users:
                    role_users[profile_role].add(str(profile.user.first_name))
                else:
                    role_users[profile_role] = {str(profile.user.first_name)}
        return role_users

    def get_invoices_in_flow(self, company):
        flow_roles = FlowRole.objects.select_related(
            'role', 'flow__invoice__invoice_type'
        ).filter(
            status=FlowRoleStatus.PROCESSING,
            flow__invoice__company=company,
            flow__invoice__status__slug__in=processing_statuses()
        ).order_by('role__name')
        return flow_roles

    def signed_invoices_in_flow(self, flow_roles):
        flow_roles = FlowRole.objects.select_related(
            'role', 'flow__invoice__invoice_type'
        ).filter(
            status=FlowRoleStatus.PROCESSED,
            flow__invoice_id__in=flow_roles.values_list('flow__invoice_id')
        ).order_by('signed_at')
        return {
            flow_role.flow.invoice_id: flow_role
            for flow_role in flow_roles
        }

    def get_context_data(self, **kwargs):
        context = super(InvoiceFlowStatusIndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        invoices_in_flow = self.get_invoices_in_flow(company=company)
        signed_invoices = self.signed_invoices_in_flow(flow_roles=invoices_in_flow)
        role_users = self.get_role_profiles(flow_roles=invoices_in_flow)
        roles_invoices = OrderedDict()
        totals = {
            'processing': {'count': 0, 'value': 0, 'average_days': 0, 'days': 0},
            'due_in_five_days': {'count': 0, 'value': 0},
            'overdue': {'count': 0, 'value': 0},
        }
        invoice_types = {}
        for flow_role in invoices_in_flow:
            role = flow_role.role
            invoice = flow_role.flow.invoice
            invoice_type = invoice.invoice_type
            last_signed = signed_invoices.get(invoice.id, None)
            last_signed_at = None
            if last_signed:
                last_signed_at = last_signed.signed_at
            if role not in roles_invoices:
                roles_invoices[role] = {
                    'users': ', '.join(role_users.get(role, [])),
                    'processing': {
                        'count': 0,
                        'days': 0,
                        'average_days': 0,
                        'value': 0,
                        'invoice_types': {},
                        'invoices': []
                    },
                    'due_in_five_days': {
                        'count': 0,
                        'value': 0,
                        'invoice_types': {},
                        'invoices': []
                    },
                    'overdue': {
                        'count': 0,
                        'value': 0,
                        'invoice_types': {},
                        'invoices': []
                    },
                    'invoice_types': {

                    }
                }
            due_date = flow_role.flow.invoice.due_date
            due_days = 0
            if due_date:
                due_in = due_date - date.today()
                if due_in:
                    due_days = due_in.days

            days = 0
            if last_signed_at:
                created_at = date.today() - last_signed_at.date()
                if created_at:
                    days = created_at.days

            if due_days < 0:
                roles_invoices[role]['overdue']['count'] += 1
                roles_invoices[role]['overdue']['value'] += invoice.total_amount
                roles_invoices[role]['overdue']['invoices'].append({'invoice': invoice, 'days': days, 'due_days': due_days})
                totals['overdue']['count'] += 1
                totals['overdue']['value'] += invoice.total_amount
                if invoice_type in roles_invoices[role]['overdue']['invoice_types']:
                    roles_invoices[role]['overdue']['invoice_types'][invoice_type] += 1
                else:
                    roles_invoices[role]['overdue']['invoice_types'][invoice_type] = 1

                if invoice_type in roles_invoices[role]['invoice_types']:
                    if 'overdue' in roles_invoices[role]['invoice_types'][invoice_type]:
                        roles_invoices[role]['invoice_types'][invoice_type]['overdue']['count'] += 1
                        roles_invoices[role]['invoice_types'][invoice_type]['overdue']['value'] += invoice.total_amount
                    else:
                        roles_invoices[role]['invoice_types'][invoice_type]['overdue']['count'] = 1
                        roles_invoices[role]['invoice_types'][invoice_type]['overdue']['value'] = invoice.total_amount
                else:
                    roles_invoices[role]['invoice_types'][invoice_type] = {
                        'processing': {
                            'count': 0,
                            'days': 0,
                            'average_days': 0,
                            'value': 0
                        },
                        'due_in_five_days': {
                            'count': 0,
                            'value': 0,
                        },
                        'overdue': {
                            'count': 1,
                            'value': invoice.total_amount,
                        },
                    }
            elif due_days <= 5:
                roles_invoices[role]['due_in_five_days']['count'] += 1
                roles_invoices[role]['due_in_five_days']['value'] += invoice.total_amount
                roles_invoices[role]['due_in_five_days']['invoices'].append({'invoice': invoice, 'days': days, 'due_days': due_days})
                totals['due_in_five_days']['count'] += 1
                totals['due_in_five_days']['value'] += flow_role.flow.invoice.total_amount
                if invoice_type in roles_invoices[role]['due_in_five_days']['invoice_types']:
                    roles_invoices[role]['due_in_five_days']['invoice_types'][invoice_type] += 1
                else:
                    roles_invoices[role]['due_in_five_days']['invoice_types'][invoice_type] = 1

                if invoice_type in roles_invoices[role]['invoice_types']:
                    if 'due_in_five_days' in roles_invoices[role]['invoice_types'][invoice_type]:
                        roles_invoices[role]['invoice_types'][invoice_type]['due_in_five_days']['count'] += 1
                        roles_invoices[role]['invoice_types'][invoice_type]['due_in_five_days']['value'] += invoice.total_amount
                    else:
                        roles_invoices[role]['invoice_types'][invoice_type]['due_in_five_days']['count'] = 1
                        roles_invoices[role]['invoice_types'][invoice_type]['due_in_five_days']['value'] = invoice.total_amount
                else:
                    roles_invoices[role]['invoice_types'][invoice_type] = {
                        'processing': {
                            'count': 0,
                            'days': 0,
                            'average_days': 0,
                            'value': 0
                        },
                        'due_in_five_days': {
                            'count': 1,
                            'value': invoice.total_amount,
                        },
                        'overdue': {
                            'count': 0,
                            'value': 0,
                        },
                    }

            if flow_role.status == FlowRole.PROCESSING:
                roles_invoices[role]['processing']['count'] += 1
                roles_invoices[role]['processing']['days'] += days
                roles_invoices[role]['processing']['average_days'] += days
                roles_invoices[role]['processing']['value'] += invoice.total_amount
                roles_invoices[role]['processing']['invoices'].append({'invoice': invoice, 'days': days, 'due_days': due_days})
                totals['processing']['count'] += 1
                totals['processing']['days'] += days
                totals['processing']['value'] += invoice.total_amount
                if invoice_type in roles_invoices[role]['processing']['invoice_types']:
                    roles_invoices[role]['processing']['invoice_types'][invoice_type] += 1
                else:
                    roles_invoices[role]['processing']['invoice_types'][invoice_type] = 1

                if invoice_type in roles_invoices[role]['invoice_types']:
                    if 'processing' in roles_invoices[role]['invoice_types'][invoice_type]:
                        roles_invoices[role]['invoice_types'][invoice_type]['processing']['count'] += 1
                        roles_invoices[role]['invoice_types'][invoice_type]['processing']['days'] += due_days
                        roles_invoices[role]['invoice_types'][invoice_type]['processing']['value'] += invoice.total_amount
                    else:
                        roles_invoices[role]['invoice_types'][invoice_type]['processing']['count'] = 1
                        roles_invoices[role]['invoice_types'][invoice_type]['processing']['days'] = due_days
                        roles_invoices[role]['invoice_types'][invoice_type]['processing']['value'] = invoice.total_amount
                else:
                    roles_invoices[role]['invoice_types'][invoice_type] = {
                        'processing': {
                            'count': 1,
                            'days': 1,
                            'average_days': 0,
                            'value': invoice.total_amount
                        },
                        'due_in_five_days': {
                            'count': 0,
                            'value': 0,
                        },
                        'overdue': {
                            'count': 0,
                            'value': 0,
                        },
                    }

        for role, role_invoices in roles_invoices.items():
            days_qty = roles_invoices[role]['processing']['days']
            invoices_qty = roles_invoices[role]['processing']['count']
            avg = round(days_qty / invoices_qty) if invoices_qty > 0 else 0
            roles_invoices[role]['processing']['average_days'] = avg
            totals['processing']['average_days'] += avg
        context['roles_invoices'] = roles_invoices
        context['totals'] = totals
        return context


class InvoiceJournalsView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'invoice/journals/show.html'
    form_class = InvoiceJournalReportForm

    def get_form_kwargs(self):
        kwargs = super(InvoiceJournalsView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(InvoiceJournalsView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context


class InvoiceJournalsReportView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/journals/report.html'

    def get_invoices(self, company):
        return Invoice.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(InvoiceJournalsReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['invoices'] = self.get_invoices(company)
        return context


class InvoicesNotYetPostedView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/report/not_yet_posted.html'

    def get_invoices(self):
        company = self.get_company()
        report = {
            'total': Decimal(0),
            'total_vat': Decimal(0),
            'total_net': Decimal(0),
            'invoices': [],
            'report_date': now(),
            'company': company
        }
        for invoice in Invoice.objects.not_yet_posted(company=company):
            report['total_vat'] += invoice.vat
            report['total_net'] += invoice.net
            report['total'] += invoice.total_amount
            report['invoices'].append(invoice)
        return report

    def get_context_data(self, **kwargs):
        context = super(InvoicesNotYetPostedView, self).get_context_data(**kwargs)
        context['report'] = self.get_invoices()
        return context


class InvoiceImageDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def get_template_names(self):
        invoice = self.get_invoice()
        if invoice.sales_invoice:
            return 'invoice/sales_invoice.html'
        return 'invoice/image.html'

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def can_view_protected_document(self, invoice):
        if not invoice.is_protected:
            return True
        return self.get_profile().can_view_protected_document

    def get_context_data(self, **kwargs):
        context = super(InvoiceImageDetailView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        invoice_flow = invoice.get_current_flow()
        context['intervention_comment'] = ''
        if invoice_flow:
            context['intervention_is_required'] = invoice_flow.intervention_at
            intervention_flow_role = get_object_or_None(FlowRole, flow=invoice_flow, intervention__isnull=False)
            if intervention_flow_role:
                context['intervention_comment'] = intervention_flow_role.intervention.comment
        context['MEDIA_URL'] = settings.MEDIA_URL
        context['can_view_doc'] = self.can_view_protected_document(invoice)
        return context


class InvoiceTrackingView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'invoice/tracking.html'

    def get_invoice_tracking(self):
        invoice = Invoice.objects.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).prefetch_related(
            'invoice_references', 'invoice_journals', 'invoice_journals__journal', 'payments'
        ).filter(id=self.kwargs['invoice_id']).first()

        report = {'incoming': [], 'posting': [], 'payments': [], 'journal': [], 'payment_journal': []}
        for invoice_journal in invoice.invoice_journals.all():
            if invoice_journal.journal.journal_type == Journal.INCOMING:
                report['incoming'].append(invoice_journal)
            if invoice_journal.journal.journal_type == Journal.POSTING:
                report['posting'].append(invoice_journal)
        for invoice_payment in invoice.payments.all():
            report['payments'].append(invoice_payment)
        return report

    def get_context_data(self, **kwargs):
        context = super(InvoiceTrackingView, self).get_context_data(**kwargs)
        invoice_tracking = self.get_invoice_tracking()
        context['report'] = invoice_tracking
        return context


class InvoiceUploadPayrollView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'invoice/payroll.html'
    form_class = InvoiceCommentForm

    def get_context_data(self, **kwargs):
        context = super(InvoiceUploadPayrollView, self).get_context_data(**kwargs)
        context['invoice'] = Invoice.objects.get(pk=self.kwargs['invoice_id'])
        return context

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({
                'error': True,
                'text': 'Payroll upload was not successful, please try again',
                'errors': form.errors
            }, status=400)
        return super(InvoiceUploadPayrollView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            invoice = Invoice.objects.get(pk=self.kwargs['invoice_id'])

            try:
                import_payroll = ImportPayrollJob(
                    invoice=invoice,
                    file=form.cleaned_data['attachment'],
                    comment=form.cleaned_data['comment'],
                    profile=self.get_profile(),
                    role=self.get_role()
                )
                import_payroll.execute()
            except PayrollImportException as exc:
                logger.exception(exc)
                return JsonResponse({
                    'error': True,
                    'text': str(exc)
                })

            # car = form.save(commit=False)
            return JsonResponse({
                'error': False,
                'text': 'Payroll uploaded successfully, please try again',
                'reload': True
            })
        return super(InvoiceUploadPayrollView, self).form_valid(form)


class InvoiceMarkProtectedView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(pk=self.kwargs['pk'])
            invoice.is_protected = request.GET.get('is_protected')
            invoice.save()
            return JsonResponse({
                'text': 'Invoice updated successfully',
                'error': False
            }, status=200)
        except Invoice.DoesNotExist:
            return JsonResponse({
                'text': 'Invoice does not exist',
                'error': True
            }, status=404)
