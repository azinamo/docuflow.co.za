from django.core.exceptions import ValidationError


class DfException(Exception):
    pass


class InvoiceException(DfException):
    pass


class InvalidPeriodAccountingDateException(InvoiceException):
    pass


class UnavailableWorkflow(InvoiceException):
    """
    To be raised if the Invoice Workflow does not exist
    Invoice
    """


class InvalidInvoiceTotal(InvoiceException):
    """
    To be raised if the Invoice totals does not balance
    """


class InvoiceRequestedDecline(InvoiceException):
    """
    To be raised if the Invoice has been set to DECLINE(status 90)
    """


class InvalidDistributionAmount(InvoiceException):
    """
    To be raised if the Invoice amount entered exceeds the totals for the invoice
    """


class DuplicateInvoiceNumber(InvoiceException):
    """
    To be raised if there is duplicate invoice numbers
    """


class AccountPostingNotSetException(InvoiceException):
    pass


class InvoiceJournalNotAvailable(InvoiceException):
    pass


class DefaultAccountNotPosted(InvoiceException):
    pass


class InvoiceHasVariance(InvoiceException):
    pass


class AccountingDateClosed(InvoiceException):
    pass


class JournalNotBalancingException(InvoiceException):
    pass


class InvoiceTypeNotAvailable(InvoiceException):
    pass


class InvalidDefaultAccount(InvoiceException):
    pass


class ControlledByInventoryException(InvoiceException):
    pass


class IncomingInvoiceNotFound(InvoiceException):
    pass
