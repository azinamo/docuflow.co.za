import os
import logging
import hashlib
import img2pdf

from django.conf import settings
from fpdf import FPDF

logger = logging.getLogger(__name__)


class ImagePdfConverter(object):

    def __init__(self, invoice):
        self.invoice = invoice

    def convert_to_pdf(self):
        og_filename = self.invoice.filename
        extension = os.path.splitext(og_filename)[1].lower()
        if extension in ['.jpg', '.jpeg', '.png', '.gif']:
            logger.info(f"invoice-->{self.invoice.id} has file at "
                        f"{self.invoice.file_location}({self.invoice.invoice_location})")

            filename = self.convert()
            logger.info(f"New file {filename}")
            if filename:
                logger.info(f"invoice-->{self.invoice.id} has file at"
                            f" {self.invoice.file_location}({self.invoice.invoice_location})")
                self.invoice.filename = filename
                self.invoice.save()

    def convert(self):
        filename = self.convert_with_img2pdf()
        if not filename:
            filename = self.conver_with_fpdf()
        return filename

    def get_invoice_dir(self):
        invoice_dir = os.path.join(settings.MEDIA_ROOT, self.invoice.company.slug, str(self.invoice.id))
        if not os.path.exists(invoice_dir):
            os.mkdir(invoice_dir)
        return invoice_dir

    def convert_with_img2pdf(self):
        logger.info("Converting now ---> ")

        pdf_filename = self.pdf_filename()
        logger.info(f"New Filename --> {pdf_filename}")

        invoice_dir = self.get_invoice_dir()
        location = os.path.join(invoice_dir, pdf_filename)
        logger.info(f"Converting to ... {pdf_filename}")
        with open(location, "wb") as f:
            logger.info("File opened")
            f.write(img2pdf.convert(self.invoice.invoice_location))

        logger.info(f"Converting done ---> {pdf_filename}")
        return pdf_filename

    def conver_with_fpdf(self):
        pdf = FPDF()
        # compression is not yet supported in py3k version
        pdf.compress = False
        # Unicode is not yet supported in the py3k version; use windows-1252 standard font
        pdf_filename = self.pdf_filename()
        logger.info(f"Filename --> {self.invoice.filename}")

        logger.info(f"New Filename --> {pdf_filename}")

        invoice_dir = self.get_invoice_dir()
        location = os.path.join(invoice_dir, pdf_filename)
        logger.info(f"Converting to ... {pdf_filename}")

        pdf.image(self.invoice.invoice_location)
        pdf.output(location, 'F')

        logger.info(f"Converting done ---> {pdf_filename}")
        return pdf_filename

    def pdf_filename(self):
        text = f"{self.invoice.id}_{self.invoice.filename}"
        _hash = hashlib.md5(text.encode()).hexdigest()
        filename = f"{str(self.invoice.id)}_{_hash}.pdf"
        return filename
