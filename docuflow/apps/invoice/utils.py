"""
Keep helper functions not related to specific module.
"""
import hashlib
import logging
import os
from collections import OrderedDict
from typing import Any
import subprocess

import yaml
import boto3
from botocore.exceptions import ClientError
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intcomma
from django.utils.timezone import now

from docuflow.apps.accounts.models import UserOpenPage
from docuflow.apps.common.utils import create_dir, upload_to_s3
from docuflow.apps.period.models import Period


logger = logging.getLogger(__name__)


# borrowed from http://stackoverflow.com/a/21912744
def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))

    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)

    return yaml.load(stream, OrderedLoader)


def get_company_dir(company):
    path = os.path.join(settings.MEDIA_ROOT, company.slug)
    return create_dir(dir_name=path)


def hash_string(string):
    return hashlib.md5(string).hexdigest()


def update_incoming_from_statuses():
    return ['in-the-flow', 'adjustments', 'decline-approved']


def get_payment_from_status():
    return ['paid', 'paid-printed-not-account-posted', 'paid-not-printed', 'paid-not-account-posted', 'partially-paid']


def get_posting_from_status():
    return ['final-signed',  'paid-not-account-posted', 'paid-printed-not-posted', 'paid-printed-not-account-posted']


def purchase_order():
    return 'purchase-order'


def default_invoice_type():
    return 'tax-invoice'


def default_status():
    return 'arrived-at-logger'


def supplier_payment_statuses():
    return ['final-signed', 'end-flow-printed', 'partially-paid']


def completed_statuses():
    return ['final-signed', 'end-flow-printed']


def processing_statuses():
    return ['in-the-flow', 'adjustments', 'printed-status-still-in-the-flow']


def processed_statuses():
    return ['final-signed', 'end-flow-printed', 'partially-paid', 'paid-not-account-posted', 'paid-printed-not-posted',
            'pp-not-account-posted', 'paid-printed-not-account-posted']


def get_page_viewer(request):
    is_locked = False
    locked_by = None
    current_profile_id = request.session.get('profile', 0)
    users_on_page = UserOpenPage.objects.select_related(
        'profile'
    ).filter(
        page=request.build_absolute_uri()
    ).order_by('updated_at')
    c = 0
    if users_on_page.exists():
        document_owner = users_on_page.first()
        for page_user in users_on_page:
            if c == 0 and page_user.profile.id == current_profile_id:
                return {'is_locked': False, 'locked_by': locked_by}
            else:
                is_locked = True
                locked_by = document_owner.profile
            c += 1
    else:
        is_locked = True
    return {'is_locked': is_locked, 'locked_by': locked_by}


def get_page_sessions(request):
    from django.contrib.sessions.models import Session
    session_store = request.session
    sessions = Session.objects.exclude(session_key=session_store.session_key)
    for session in sessions:
        session_data = session.get_decoded()
        if 'user_page' in session_data and request.build_absolute_uri() == session_data['user_page']:
            return session_data
    return False


def money(dollars):
    if dollars:
        dollars = round(float(dollars), 2)
        return "%s%s" % (intcomma(int(dollars)), ("%0.2f" % dollars)[-3:])
    else:
        return "0.00"


def upload_path(instance: Any, company: str, year: str, folder_name: str, filename: str):
    path = os.path.join(settings.MEDIA_ROOT, company)
    create_dir(dir_name=path)
    path = os.path.join(settings.MEDIA_ROOT, company, year)
    create_dir(dir_name=path)
    path = os.path.join(settings.MEDIA_ROOT, company, year, folder_name)
    create_dir(dir_name=path)
    path = os.path.join(settings.MEDIA_ROOT, company, year, folder_name, str(instance.pk))
    create_dir(dir_name=path)
    if instance.pk:
        url = f"{company}/{year}/{folder_name}/{instance.pk}/{filename}"
    else:
        url = f"{company}/{year}/{folder_name}/{filename}"
    return url, path


def invoice_upload_path(invoice, filename):
    if invoice and not invoice.period:
        return None, None
    return upload_path(
        instance=invoice,
        company=invoice.company.slug,
        year=str(invoice.period.period_year.year),
        folder_name='invoices',
        filename=filename
    )


def payments_upload_path(payment, filename):
    if payment and not payment.period:
        return None, None
    return upload_path(
        instance=payment,
        company=payment.company.slug,
        year=str(payment.period.period_year.year),
        folder_name='payments',
        filename=filename
    )


def certificates_upload_path(certificate, filename):
    if certificate and not certificate.year:
        return None, None
    return upload_path(
        instance=certificate,
        company=certificate.company.slug,
        year=str(certificate.year.year),
        folder_name='certificates',
        filename=filename
    )


def is_weekend():
    return now().weekday() > 5


def get_default_period(company, accounting_date):
    period = Period.objects.get_by_date(company=company, period_date=accounting_date).first()
    if not period:
        period = Period.objects.opened().filter(company=company).last()
    return period


def process_aws_ocr(invoice):
    client = boto3.client(
        'textract',
        region_name=settings.AWS_S3_REGION_NAME,
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
    )

    try:
        client.analyze_document(file_path, settings.AWS_STORAGE_BUCKET_NAME, filename)
    except ClientError as exc:
        logger.info(f'File could not be uploaded to s3 {exc}')
        raise
    return urljoin(bucket_base_url(), filename)


def save_on_local(file_name: str, destination: str, original_src: str) -> str:
    decrypted_file_name = f"decrypted_{file_name}"
    src = os.path.join(destination, file_name)
    file_path = os.path.join(destination, decrypted_file_name)

    destination = os.path.join(destination, file_name)
    os.rename(original_src, destination)

    decrypt_pdf(source_file=src, destination_file=file_path)

    return file_path


def decrypt_pdf(source_file, destination_file):
    # used to generate temp file name. so we will not duplicate or replace anything
    try:
        subprocess.call(["qpdf", "--decrypt", source_file, destination_file])
    except subprocess.SubprocessError as err:
        logger.exception(err)


def move_invoice_document(invoice, src: str, filename: str = None):
    filename = filename if filename else invoice.filename
    original_src = os.path.join(src, invoice.filename)

    if not os.path.exists(original_src):
        return

    _upload_path, destination_path = invoice_upload_path(invoice=invoice, filename=filename)

    # convertor = ImagePdfConverter(invoice)
    # convertor.convert_to_pdf()
    if settings.UPLOAD_TO_S3:
        upload_to_s3(file_path=original_src, filename=_upload_path)

    file_path = save_on_local(
        file_name=filename,
        destination=destination_path,
        original_src=original_src
    )

    if file_path:
        invoice.filename = filename
        invoice.save()

    if os.path.isfile(original_src):
        os.remove(original_src)
