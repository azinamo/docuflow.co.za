from annoying.functions import get_object_or_None
from django import template

from docuflow.apps.accountposting.models import AccountPosting
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import VatCode, Currency
from docuflow.apps.invoice.forms import InvoiceLogForm, InvoiceForm
from docuflow.apps.invoice.models import Invoice, InvoiceType
from docuflow.apps.period.models import Period, Year
from docuflow.apps.workflow.models import Workflow

register = template.Library()


def get_profile_year(profile_id):
    profile = Profile.objects.get(id=profile_id)
    return profile.year


def get_year(company, profile_id, profile_year_id):
    if profile_year_id:
        return Year.objects.get(pk=profile_year_id)
    else:
        profile_year = get_profile_year(profile_id)
        if profile_year:
            return profile_year
        else:
            return Year.objects.filter(company=company, is_active=True).first()


def get_role_permissions(role_id):
    role_permissions = []
    if type(role_id) == int:
        role = Role.objects.prefetch_related('permission_groups').get(pk=role_id)
        if role:
            for permission_group in role.permission_groups.all():
                for permission in permission_group.permissions.all():
                    role_permissions.append(permission.codename)
    return role_permissions


def get_account_posting(invoice):
    account_posting = get_object_or_None(AccountPosting, company=invoice.company, is_default=True)
    if account_posting:
        return account_posting


def get_default_vat_code(invoice):
    company_vat_code = get_object_or_None(VatCode, company=invoice.company, is_default=True, module=Module.PURCHASE)
    if company_vat_code:
        return company_vat_code
    return None


def get_default_invoice_type(invoice):
    invoice_type = get_object_or_None(InvoiceType, company=invoice.company, is_default=True)
    if invoice_type:
        return invoice_type
    return None


def get_default_currency(invoice):
    if invoice.currency:
        return invoice.currency
    else:
        currency = Currency.objects.filter(
            company=invoice.company
        ).order_by('id').first()
        if currency:
            return currency
    return None


@register.inclusion_tag('invoice/edit_log_form.html')
def edit_log_form(invoice, role_permissions, profile=None, year=None):
    ref_1 = ref_2 = ''
    c = 0
    for reference in invoice.invoice_references.all():
        if c == 0:
            ref_1 = reference
        if c == 1:
            ref_2 = reference
        c = c + 1

    account_posting_proposal = get_account_posting(invoice)

    default_flow_proposal = get_object_or_None(Workflow, company=invoice.company, is_default=True)

    vat_code = get_default_vat_code(invoice)

    invoice_type = get_default_invoice_type(invoice)

    supplier = None
    workflow_proposal = None
    supplier_name = None
    supplier_number = None

    is_vat_registered = invoice.company.is_vat_registered
    vat_method = None

    period = Period.objects.filter(company=invoice.company, is_closed=False).order_by('-period').first()

    if default_flow_proposal:
        workflow_proposal = default_flow_proposal

    if invoice.workflow:
        workflow_proposal = invoice.workflow

    if invoice.account_posting_proposal:
        account_posting_proposal = invoice.account_posting_proposal

    if invoice.vat_code:
        vat_code = invoice.vat_code

    if invoice.invoice_type:
        invoice_type = invoice.invoice_type

    if invoice.period:
        period = invoice.period

    vat_number = invoice.vat_number
    supplier_account_ref = ''

    if invoice.supplier:
        supplier = invoice.supplier
        supplier_name = invoice.supplier.name
        supplier_number = invoice.supplier.code
        vat_method = invoice.supplier.vat_method
        vat_number = invoice.supplier.vat_number
        supplier_account_ref = invoice.supplier.company_number

    initials = {'supplier': supplier,
                'supplier_name': supplier_name,
                'supplier_number': supplier_number,
                'account_posting_proposal': account_posting_proposal,
                'workflow': workflow_proposal,
                'reference1': ref_1,
                'reference2': ref_2,
                'vat_code': vat_code,
                'due_date': invoice.due_date,
                'invoice_type': invoice_type,
                'vat_number': vat_number,
                'supplier_account_ref': supplier_account_ref,
                'period': period
                }
    # invoice.net_amount = invoice.total_amount

    form = InvoiceLogForm(
        instance=invoice,
        company=invoice.company,
        role=None,
        profile=profile,
        request=None,
        year=get_year(invoice.company, profile_id=profile.id, profile_year_id=year.id),
        initial=initials
    )
    return {
        'invoice': invoice,
        'form': form,
        'assigned_group_permissions': invoice,
        'is_vat_registered': is_vat_registered,
        'vat_method': vat_method,
        'permissions': role_permissions,
        'default_workflow': workflow_proposal
    }


def count_purchase_orders(invoice):
    return Invoice.objects.purchase_order(
        invoice.company,
        invoice.supplier,
        'po-ordered'
    ).count()


def count_receipts(invoice):
    return Invoice.objects.receipts(invoice.company, invoice.supplier).count()


@register.inclusion_tag('invoice/edit_form.html')
def edit_invoice_form(invoice, role_permissions, can_edit, profile_id=None, profile_year_id=None):

    has_receipts = False
    has_purchase_orders = False
    initials = {'invoice_type': invoice.invoice_type,
                'status': invoice.status,
                'due_date': invoice.due_date
                }
    is_vat_registered = invoice.company.is_vat_registered
    vat_method = None

    ref_1 = ref_2 = ''
    c = 0
    for reference in invoice.invoice_references.all():
        if c == 0:
            ref_1 = reference
        if c == 1:
            ref_2 = reference
        c = c + 1

    initials['reference1'] = ref_1
    initials['reference2'] = ref_2
    if invoice.supplier:
        initials['supplier'] = invoice.supplier
        initials['supplier_name'] = invoice.supplier.name
        initials['supplier_number'] = invoice.supplier.code
        initials['supplier_account_ref'] = invoice.supplier.company_number
        is_vat_registered = invoice.supplier.is_vat_registered
        vat_method = invoice.supplier.vat_method
        has_purchase_orders = count_purchase_orders(invoice)
        has_receipts = count_receipts(invoice)
    if invoice.vat_code:
        initials['vat_code'] = invoice.vat_code

    form = InvoiceForm(instance=invoice,
                       company=invoice.company,
                       year=get_year(invoice.company, profile_id, profile_year_id),
                       initial=initials,
                       role=None,
                       profile=None,
                       request=None
                       )

    return {'invoice': invoice,
            'form': form,
            'permissions': role_permissions,
            'is_vat_registered': is_vat_registered,
            'vat_method': vat_method,
            'can_edit': can_edit,
            'has_receipts': has_receipts,
            'has_purchase_orders': has_purchase_orders
            }

#
# @register.inclusion_tag('invoice/account_posting_form.html')
# def invoice_account_posting_form(invoice, role_id):
#     role = get_object_or_None(Role, pk=role_id)
#     initials = {
#                  'company': invoice.company,
#                  'role': invoice.role
#                }
#     form = InvoiceAccountPostingForm(instance=invoice, company=invoice.company, role=role, initial=initials)
#     return { 'invoice' : invoice, 'form': form }
