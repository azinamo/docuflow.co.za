import calendar
import logging
import pprint
from collections import defaultdict
from datetime import datetime, date
from decimal import Decimal

from django import template
from django.core.cache import cache

from docuflow.apps.common import utils
from docuflow.apps.common.enums import Module
from docuflow.apps.accountposting.models import ObjectPosting
from docuflow.apps.accounts.models import Role
from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Flow, FlowRole, InvoiceAccount
from docuflow.apps.period.services import get_current_year

register = template.Library()

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


def percentage(num, total):
    if total > 0:
        return round(num / total, 2) * 100
    return 0


def get_month_day_range(date):
    first_day = date.replace(day=1)
    last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
    return first_day, last_day


@register.filter(name='split')
def display_changes_list(changes_str):
    return changes_str.replace(';', '<br />')


@register.inclusion_tag('invoice/due_days.html', name='due_days')
def due_date(date_due, level=None):
    days = None
    if isinstance(date_due, datetime):
        date_due = date_due.date()

    if isinstance(date_due, date):
        delta = date_due - date.today()
        days = delta.days

    if days:
        days = days
    else:
        days = 0
    return {'due_days': abs(days), 'days': days, 'level': level}


def get_role_invoices(role_id, year):
    cache_key = utils.get_year_role_stats_cache_key(role_id=role_id, year=year)
    role_statistics = cache.get(cache_key)
    if not role_statistics:
        role_statistics = FlowRole.objects.role_stats(role_id, year)
        cache.set(cache_key, role_statistics)
    return role_statistics


@register.inclusion_tag('invoice/stats.html')
def role_stats(role_id, profile_id, company_id):
    year = get_current_year(profile_id=profile_id, company_id=company_id)

    role_statistics = get_role_invoices(role_id, year)
    completed = 0
    processing = 0
    incoming = 0
    total = role_statistics['processing'] + role_statistics['incoming']
    inc = percentage(incoming, total)
    proc = percentage(processing, total)
    comp = percentage(completed, total)
    return {
        'completed': completed,
        'processing': role_statistics['processing'],
        'incoming': role_statistics['incoming'],
        'completed_perc': comp,
        'processing_perc': proc,
        'incoming_perc': inc
    }


@register.inclusion_tag('invoice/linked_model_value.html')
def invoice_account_linked_model_values(content_type, account, can_edit):
    item_value = None
    object_links = account.object_links
    if content_type in object_links:
        if object_links[content_type]['value']:
            item_value = object_links[content_type]['value']
        else:
            item_value = ''
    return {'content_type': content_type, 'item_value': item_value, 'account': account, 'can_edit': can_edit}


@register.inclusion_tag('invoice/accounts/account_posting_line.html')
def account_posting_line(accounts, linked_models, invoice):
    return {'accounts': accounts, 'linked_models': linked_models, 'invoice': invoice}


@register.inclusion_tag('invoice/flow_level.html')
def account_posting_line(flow, invoice):
    return {'flow': flow, 'invoice': invoice}


@register.inclusion_tag('invoice/flow.html')
def invoice_flows(invoice, profile_id, permissions, can_edit):
    flows_list = []

    flows = Flow.objects.prefetch_related('roles', 'roles__role').filter(invoice=invoice).order_by('order_number')
    for flow in flows:
        invoice_flow = {
            'is_current': flow.is_current,
            'is_signed': flow.is_signed,
            'is_processed': (flow.status and flow.status.slug == 'processed'),
            'can_edit': can_edit,
            'order_number': flow.order_number,
            'id': flow.id,
            'roles': []
        }
        for flow_role in flow.roles.all():
            can_delete = False
            signed_by = None
            if not flow_role.is_processed:
                if 'can_manage_invoice_flow_add_and_delete' in permissions:
                    can_delete = True
                elif flow_role.created_by:
                    if flow_role.created_by_id == profile_id:
                        can_delete = True

            if flow_role.signed_by:
                signed_by = flow_role.signed_by.code

            invoice_flow['roles'].append(
                {'is_pending': flow_role.is_pending,
                 'is_processed': flow_role.is_processed,
                 'is_action_required': flow_role.is_action_required,
                 'is_parked': flow_role.is_parked,
                 'role': flow_role.role,
                 'can_delete': can_delete,
                 'signed_at': flow_role.signed_at,
                 'signed_by': signed_by,
                 'id': flow_role.id
                 })

        flows_list.append(invoice_flow)

    return {'flows': flows_list, 'invoice': invoice, 'can_edit': can_edit}


def get_locale():
    """
    Return locale string complaint with operating system
    """
    return 'en'


def money_fmt(value, places=2, curr='', sep=',', dp='.', pos='', neg='-', trailneg=''):
    """Convert Decimal to a money formatted string.

    places:  required number of places after the decimal point
    curr:    optional currency symbol before the sign (may be blank)
    sep:     optional grouping separator (comma, period, space, or blank)
    dp:      decimal point indicator (comma or period)
             only specify as blank when places is zero
    pos:     optional sign for positive numbers: '+', space or blank
    neg:     optional sign for negative numbers: '-', '(', space or blank
    trailneg:optional trailing minus indicator:  '-', ')', space or blank

    >>> d = Decimal('-1234567.8901')
    >>> moneyfmt(d, curr='$')
    '-$1,234,567.89'
    >>> moneyfmt(d, places=0, sep='.', dp='', neg='', trailneg='-')
    '1.234.568-'
    >>> moneyfmt(d, curr='$', neg='(', trailneg=')')
    '($1,234,567.89)'
    >>> moneyfmt(Decimal(123456789), sep=' ')
    '123 456 789.00'
    >>> moneyfmt(Decimal('-0.02'), neg='<', trailneg='>')
    '<0.02>'

    """
    q = Decimal(10) ** -places      # 2 places --> '0.01'
    sign, digits, exp = value.quantize(q).as_tuple()
    result = []
    digits = list(map(str, digits))
    build, next_digit = result.append, digits.pop
    if sign:
        build(trailneg)
    for i in range(places):
        build(next_digit() if digits else '0')
    build(dp)
    if not digits:
        build('0')
    i = 0
    while digits:
        build(next_digit())
        i += 1
        if i == 3 and digits:
            i = 0
            build(sep)
    build(curr)
    build(neg if sign else pos)
    return ''.join(reversed(result))


@register.filter
def money(amount, include_cents=True):
    return utils.format_money(amount=amount, include_cents=include_cents)


@register.filter
def average_value(value, quantity=0):
    if quantity > 0:
        return round(value / quantity)
    return 0


@register.filter('intspace')
def intspace(value):
    """
    Converts an integer to a string containing spaces every three digits.
    For example, 3000 becomes '3 000' and 45000 becomes '45 000'.
    See django.contrib.humanize app
    """

    value_str_split = str(value).split('.')
    decimal_places = 2
    if len(value_str_split) == 2:
        decimal_places = len(value_str_split[1])

    try:
        if isinstance(value, (int, float, str)):
            value = float(value)
    except (ValueError, TypeError):
        return value

    amount_items = str(value).split('.')[0]

    groups = len(amount_items)/3
    group_by = 3
    if groups > 0:
        grouped_amounts = {}
        g_index = 1
        counter = 0
        amount_items = amount_items[::-1]
        for v in amount_items:
            if counter < group_by:
                if g_index in grouped_amounts:
                    grouped_amounts[g_index].append(v)
                else:
                    grouped_amounts[g_index] = [v]
                counter += 1
            else:
                g_index += 1
                counter = 0
                if g_index in grouped_amounts:
                    grouped_amounts[g_index].append(v)
                else:
                    grouped_amounts[g_index] = [v]

        # grouped_amounts = grouped_amounts.reverse()
        upper_level = g_index
        amounts = []
        for i, item in grouped_amounts.items():
            item.reverse()
            upper_level -= 1
            amounts.append(item)

        amounts.reverse()

    format_str = f"%.{decimal_places}"

    new_value = f"{value}{format_str}"

    return value


def get_account_object_links(invoice_account):
    account_object_links = {}
    if invoice_account:
        for account_object_item in invoice_account.object_items.all():
            if account_object_item.company_object.id in account_object_links:
                account_object_links[account_object_item.company_object.id]['value'] = account_object_item.id
            else:
                account_object_links[account_object_item.company_object.id] = {'value': account_object_item.id}
    return account_object_links


def get_invoice_account_object_postings(invoice_account):
    object_postings_links = {}
    if invoice_account:
        for account_object_posting in invoice_account.object_postings.all():
            if account_object_posting.company_object.id in object_postings_links:
                object_postings_links[account_object_posting.company_object.id]['value'] = account_object_posting.id
            else:
                object_postings_links[account_object_posting.company_object.id] = {'value': account_object_posting.id}
    return object_postings_links


def get_invoice_account(invoice_account_id):
    invoice_account = None
    if invoice_account_id and type(invoice_account_id) == int:
        invoice_account = InvoiceAccount.objects.prefetch_related(
            'object_items',
            'object_postings'
        ).filter(pk=invoice_account_id).first()
    return invoice_account


def get_role_object_items(role):
    role_object_items = defaultdict(list)
    for object_item in role.object_items.all():
        role_object_items[object_item.company_object_id].append(object_item)
    return role_object_items


def get_company(company_id):
    return Company.objects.prefetch_related(
        'company_objects'
    ).filter(pk=company_id).first()


def get_role(role_id):
    role = None
    if role_id and type(role_id) == int:
        role = Role.objects.prefetch_related(
            'object_items',
        ).filter(pk=role_id).first()
    return role


def get_object_postings(company, role=None):
    company_objects_postings = defaultdict(list)
    objects_postings = ObjectPosting.objects.select_related(
        'company_object'
    ).filter(
        company_object__company=company
    ).all()
    for object_posting in objects_postings:
        company_objects_postings[object_posting.company_object.id].append(object_posting)
    return company_objects_postings


@register.inclusion_tag('invoiceaccount/linked_objects.html')
def invoice_account_linked_objects(company_id, role_id=None, invoice_account_id=None, form_style='inline'):
    company = get_company(company_id)
    role = get_role(role_id)
    invoice_account = get_invoice_account(invoice_account_id)
    object_categories = {}
    account = None
    accounts = []
    module_id = Module.PURCHASE
    module_id = str(module_id)

    account_object_links = get_account_object_links(invoice_account)
    account_object_posting_links = get_invoice_account_object_postings(invoice_account)

    role_object_items = get_role_object_items(role)

    if company:
        company_objects = company.company_objects.all().order_by('ordering')
        # company_objects_ids = [company_object.id for company_object in company_objects]

        company_objects_postings = get_object_postings(company, role)
        for company_object in company_objects:
            if module_id in company_object.modules:
                object_categories[company_object.id] = {}
                object_categories[company_object.id]['label'] = company_object
                object_categories[company_object.id]['value'] = None
                object_categories[company_object.id]['posting_value'] = None
                object_categories[company_object.id]['objects'] = []
                object_categories[company_object.id]['object_postings'] = {}
                if company_object.id in account_object_links:
                    object_categories[company_object.id]['value'] = account_object_links[company_object.id]['value']

                if company_object.id in account_object_posting_links:
                    object_categories[company_object.id]['posting_value'] = account_object_posting_links[company_object.id]['value']

                if company_objects_postings:
                    object_categories[company_object.id]['object_postings'] = company_objects_postings.get(company_object.id, {})

                if role_object_items:
                    role_objects = role_object_items.get(company_object.id, None)
                    if role_objects:
                        object_categories[company_object.id]['objects'] = role_objects
                else:
                    for objects_item in company_object.object_items.all():
                        object_categories[objects_item.company_object_id]['objects'].append(objects_item)

    return {'object_categories': object_categories, 'accounts': accounts, 'account': account, 'form_style': form_style}
