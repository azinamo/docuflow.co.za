from django.db.models import Count

from docuflow.apps.company.models import Branch, Company
from docuflow.apps.invoice.models import Invoice


def get_arrived_in_log(company: Company):
    return Invoice.objects.get_at_logger(company).select_related(
        'company', 'status', 'invoice_type', 'workflow', 'supplier'
    ).exclude(
        deleted__isnull=False
    ).order_by(
        'invoice_id', 'accounting_date'
    )
