from django import forms
from django.contrib import admin

from . import models


class InvoiceAccountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(InvoiceAccountForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['account'].queryset = self.fields['account'].queryset.filter(self.instance.invoice.company)
        else:
            self.fields['account'].queryset = self.fields['account'].queryset.none()

    class Meta:
        model = models.InvoiceAccount
        fields = ('account', 'amount', 'vat_code', 'is_vat')


class InvoicePaymentAdmin(admin.TabularInline):
    model = models.InvoicePayment
    list_select_related = ('payment', )
    fields = ('allocated', 'discount')
    extra = 0


@admin.register(models.Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    date_hierarchy = 'invoice_date'
    fieldsets = [('Basic Information', {'fields': ['invoice_number', 'invoice_date', 'accounting_date', 'due_date',
                                        'total_amount', 'net_amount', 'vat_amount', 'discount', 'open_amount', 'status']
                                        })
                 ]
    list_display = ('__str__', 'company', 'supplier', 'invoice_type', 'status', 'accounting_date', 'total_amount',
                    'open_amount', )
    list_filter = ('status', 'company', 'supplier')
    list_select_related = ('company', 'supplier', 'invoice_type', 'status', )
    autocomplete_fields = ['supplier', 'period']
    inlines = (InvoicePaymentAdmin, )
    search_fields = ('invoice_id', 'invoice_number')

    def has_add_permission(self, request):
        return False


@admin.register(models.Payment)
class PaymentAdmin(admin.ModelAdmin):
    date_hierarchy = 'payment_date'
    list_display = ('__str__', 'company', 'supplier', 'net_amount', 'balance', 'created_at', )
    fieldsets = [('Basic Information', {'fields': ['payment_date', 'memo', 'total_amount', 'total_discount',
                                        'net_amount', 'balance', 'attachment', 'payment_type']
                                        })
                 ]
    search_fields = ('payment_id', )
    list_select_related = ('company', 'supplier', 'period', 'period__period_year')

    inlines = (InvoicePaymentAdmin, )

    def has_add_permission(self, request):
        return False


@admin.register(models.InvoiceType)
class InvoiceTypeAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'company', 'number_start', 'file', 'is_account_posting', 'is_credit', 'is_default')
    search_fields = ('name', )
    list_select_related = ('company', 'file', )

    def has_add_permission(self, request):
        return False
