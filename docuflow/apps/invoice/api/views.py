from rest_framework import viewsets
from rest_framework_datatables.django_filters.backends import DatatablesFilterBackend

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.invoice.models import Invoice, Payment
from .filters import InvoiceFilter
from .serializers import InvoiceSerializer, PaymentSerializer


class InvoiceViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = InvoiceSerializer
    filter_backends = (DatatablesFilterBackend, )
    filterset_class = InvoiceFilter

    def get_queryset(self):
        return Invoice.objects.select_related(
            'status', 'company', 'supplier', 'invoice_type', 'currency'
        ).prefetch_related(
            'flows__status', 'invoice_references'
        ).filter(
            company=self.get_company(), period__period_year=self.get_year()
        ).order_by(
            'id', 'invoice_id', 'accounting_date'
        )


class PaymentViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = PaymentSerializer

    def get_queryset(self):
        return Payment.objects.select_related(
            'company', 'supplier', 'payment_method', 'period', 'period__period_year', 'account', 'journal'
        ).filter(
            company=self.get_company(), period__period_year=self.get_year()
        ).order_by('-payment_date', 'supplier__name')
