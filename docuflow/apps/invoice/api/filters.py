from django_filters import BooleanFilter, CharFilter
from django_filters import filters
from rest_framework_datatables.django_filters.filters import GlobalFilter
from rest_framework_datatables.django_filters.filterset import DatatablesFilterSet

from docuflow.apps.invoice.models import Invoice
from docuflow.apps.invoice.utils import completed_statuses


class GlobalCharFilter(GlobalFilter, filters.CharFilter):
    pass


class InvoiceFilter(DatatablesFilterSet):
    status = CharFilter(method='filter_by_status_slug')
    references = GlobalCharFilter(field_name='invoice_references__reference', lookup_expr='icontains')
    supplier_name = GlobalCharFilter(field_name='supplier__name', lookup_expr='icontains')
    invoice_number = GlobalCharFilter(lookup_expr='icontains')
    invoice_type = GlobalCharFilter(lookup_expr='icontains', field_name='invoice_type__name')
    reference = GlobalCharFilter(lookup_expr='icontains', field_name='invoice_id')

    class Meta:
        model = Invoice
        fields = ('status', 'supplier_name', 'invoice_number', 'invoice_type', 'references', 'reference')

    # noinspection PyMethodMayBeStatic
    def filter_by_status_slug(self, qs, name, value):
        if value:
            if value == 'all':
                return qs
            elif value == 'completed':
                statuses = completed_statuses()
                statuses.append('paid-printed')
                statuses.append('paid')
                return qs.filter(status__slug__in=statuses)
            elif value == "po-approved":
                return qs.filter(invoice_type__code="purchase-order", status__slug='po-approved')
            elif value == "in-the-flow":
                return qs.filter(status__slug__in=['in-the-flow', 'adjustments', 'printed-status-still-in-the-flow'])
            elif value == "rejected":
                return qs.filter(status__slug__in=['rejected - invoice', 'rejected-approval-waiting'])
            elif value == "paid":
                return qs.filter(status__slug__in=['paid', 'paid-printed'])
            elif value:
                return qs.filter(status__slug=value)
        return qs

    # noinspection PyMethodMayBeStatic
    def filter_by_references(self, qs, name, value):
        if value:
            return qs.filter(invoice_references__name__icontains=value).distinct()
        return qs
