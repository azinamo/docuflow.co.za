from django.urls import reverse
from rest_framework import serializers

from docuflow.apps.company.api.serializers import BranchSerializer
from docuflow.apps.invoice.enums import InvoiceStatus
from docuflow.apps.invoice.models import Invoice, Status, InvoiceType, Payment, InvoiceAccount
from docuflow.apps.period.serializers import PeriodSerializer
from docuflow.apps.supplier.api.serializers import SupplierSerializer


class InvoiceStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Status
        fields = ('id', 'name', 'slug')


class InvoiceTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = InvoiceType
        fields = ('id', 'name', 'is_account_posting', 'is_credit')


class MinimalInvoiceSerializer(serializers.ModelSerializer):
    detail_url = serializers.SerializerMethodField()
    reference = serializers.CharField(source='__str__')

    class Meta:
        model = Invoice
        fields = ['id', 'reference', 'supplier', 'supplier_name', 'invoice_number', 'accounting_date', 'invoice_date',
                  'payment_date', 'due_date', 'net_amount', 'vat_amount', 'total_amount', 'detail_url']
        datatables_always_serialize = ('id', 'detail_url')

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        if instance.status.slug in InvoiceStatus.DECLINE_PRINTED.value:
            return reverse('invoice_show_detail', kwargs={'pk': instance.id})
        else:
            return reverse('invoice_detail', kwargs={'pk': instance.id})


class InvoiceSerializer(MinimalInvoiceSerializer):
    flows = serializers.StringRelatedField(many=True)
    references = serializers.StringRelatedField(many=True, source='invoice_references')
    status = InvoiceStatusSerializer()
    invoice_type = InvoiceTypeSerializer()
    supplier = SupplierSerializer()
    period = PeriodSerializer()
    branch = BranchSerializer()

    class Meta(MinimalInvoiceSerializer.Meta):
        model = Invoice
        fields = MinimalInvoiceSerializer.Meta.fields + [
            'id', 'status', 'supplier', 'supplier_name', 'invoice_type', 'references', 'period', 'detail_url',
            'flows', 'branch']
        datatables_always_serialize = ('id', 'detail_url', 'references')


class PaymentSerializer(serializers.ModelSerializer):
    supplier = serializers.StringRelatedField(many=False)
    period = serializers.StringRelatedField(many=False)
    role = serializers.StringRelatedField(many=False)
    profile = serializers.StringRelatedField(many=False)
    payment_method = serializers.StringRelatedField(many=False)
    account = serializers.StringRelatedField(many=False)
    vat_code = serializers.StringRelatedField()
    payment_reference = serializers.CharField(source='__str__')
    total_amount = serializers.DecimalField(decimal_places=2, max_digits=15)
    detail_url = serializers.SerializerMethodField()
    delete_url = serializers.SerializerMethodField()
    is_updated = serializers.SerializerMethodField()
    is_locked = serializers.SerializerMethodField()

    class Meta:
        model = Payment
        fields = ('id', 'payment_reference', 'supplier', 'period', 'role', 'profile', 'payment_method', 'delete_url',
                  'account', 'vat_code', 'total_amount', 'payment_date', 'detail_url', 'is_updated', 'is_locked')
        datatables_always_serialize = ('id', 'detail_url', 'delete_url')

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse('payment_advice', kwargs={'payment_id': instance.pk})

    # noinspection PyMethodMayBeStatic
    def get_delete_url(self, instance):
        return reverse('delete_payment', args=(instance.pk, ))

    # noinspection PyMethodMayBeStatic
    def get_is_updated(self, instance):
        return True if instance.journal else False

    # noinspection PyMethodMayBeStatic
    def get_is_locked(self, instance):
        return False if instance.is_open else True
