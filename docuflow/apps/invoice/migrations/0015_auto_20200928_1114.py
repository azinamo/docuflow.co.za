# Generated by Django 3.1 on 2020-09-28 09:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('distribution', '0003_data'),
        ('invoice', '0014_auto_20200630_2029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoiceaccount',
            name='distribution',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='invoice_accounts', to='distribution.Distribution'),
        ),
    ]
