# Generated by Django 3.2 on 2022-07-15 23:39

from django.db import migrations, models
import django.db.models.deletion
import docuflow.apps.invoice.enums
import docuflow.apps.invoice.utils
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0018_auto_20220210_1641'),
        ('accounts', '0015_alter_permissiongroup_options'),
        ('company', '0033_auto_20220128_1007'),
        ('period', '0007_auto_20201205_2106'),
        ('invoice', '0026_alter_invoice_options'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='purchaseorder',
            name='invoice',
        ),
        migrations.RemoveField(
            model_name='purchaseorder',
            name='vat_code',
        ),
        migrations.AddField(
            model_name='payment',
            name='filename',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='invoicejournal',
            name='invoice_status',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='status_journals', to='invoice.status'),
        ),
        migrations.AlterField(
            model_name='invoicejournal',
            name='invoice_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='journals', to='invoice.invoicetype', verbose_name='Document Types'),
        ),
        migrations.AlterField(
            model_name='invoicejournal',
            name='linked_journal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='linked_invoice_journal', to='invoice.invoicejournal'),
        ),
        migrations.AlterField(
            model_name='invoicejournal',
            name='supplier',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='invoice_journals', to='supplier.supplier'),
        ),
        migrations.AlterField(
            model_name='invoicejournal',
            name='vat_code',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='journal_vat_code', to='company.vatcode'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='payments', to='company.account', verbose_name='Bank Account'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='adjustment_account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='company.account'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='attachment',
            field=models.FileField(blank=True, upload_to=docuflow.apps.invoice.utils.payments_upload_path),
        ),
        migrations.AlterField(
            model_name='payment',
            name='company',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, related_name='company_payments', to='company.company'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='payment',
            name='journal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='payments', to='invoice.journal'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='payment_method',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='company.paymentmethod', verbose_name='Payment From'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='payment',
            name='payment_type',
            field=enumfields.fields.EnumIntegerField(default=0, enum=docuflow.apps.invoice.enums.PaymentType),
        ),
        migrations.AlterField(
            model_name='payment',
            name='period',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, related_name='period_payments', to='period.period'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='payment',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='profile_payments', to='accounts.profile'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='role',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='role_payments', to='accounts.role'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='supplier',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, related_name='supplier_payments', to='supplier.supplier'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='payment',
            name='vat_code',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='vat_code_payments', to='company.vatcode'),
        ),
        migrations.DeleteModel(
            name='InvoiceJournalLink',
        ),
        migrations.DeleteModel(
            name='PurchaseOrder',
        ),
    ]
