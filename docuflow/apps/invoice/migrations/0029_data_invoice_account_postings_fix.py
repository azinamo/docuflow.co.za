# Generated by Django 3.1 on 2020-09-28 09:14
from decimal import Decimal

from django.db import migrations, models


def fix_invoice_account_postings(app, schema_editor):
    InvoiceAccount = app.get_model('invoice', 'InvoiceAccount')

    year = 2023
    for invoice_account in InvoiceAccount.objects.filter(invoice__period__period_year__year=year):
        if invoice_account.is_holding_account and invoice_account.is_posted:
            posted_to_accounts = invoice_account.parent.all()
            total_posted = Decimal(sum(posted_to_account.amount for posted_to_account in posted_to_accounts)).quantize(Decimal('.01'))
            if posted_to_accounts and total_posted == invoice_account.amount:
                for posted_to_account in posted_to_accounts:
                    posted_to_account.invoice_account = None
                    posted_to_account.save()
                invoice_account.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0028_alter_payment_attachment'),
    ]

    operations = [
        migrations.RunPython(fix_invoice_account_postings, reverse_code=migrations.RunPython.noop)
    ]
