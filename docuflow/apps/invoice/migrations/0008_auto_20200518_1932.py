# Generated by Django 2.0 on 2020-05-18 17:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0007_auto_20200422_1713'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='journal_type',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Journal for incoming invoices'), (1, 'Journal for cancelled invoices'), (2, 'Journal for posting invoices'), (3, 'Journal for payments'), (4, 'Journal for distributions')]),
        ),
        migrations.AlterField(
            model_name='journal',
            name='period',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='period.Period'),
        ),
    ]
