import logging

from docuflow.apps.accounts.models import UserOpenPage

logger = logging.getLogger(__name__)


class InvoiceViewerMiddleWare:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        if not request.is_ajax() and request.user.is_authenticated:
            profile_id = request.session.get('profile')
            if profile_id:
                open_page, _ = UserOpenPage.objects.update_or_create(
                    profile_id=profile_id,
                    defaults={
                        'page': request.build_absolute_uri(),
                        'agent': request.META['HTTP_USER_AGENT']
                    }
                )
                request.session['open_page_id'] = open_page.id

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        return response

