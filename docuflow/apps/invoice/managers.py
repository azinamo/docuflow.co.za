from django.db import models
from django.db.models import Q, Sum, F, Case, When
from django.utils.timezone import datetime

from . import enums
from . import utils


class InvoiceQuerySet(models.QuerySet):

    def paid(self):
        return self.filter(status__slug='paid')

    def status(self, status):
        return self.filter(status=status)

    def company(self, company):
        return self.filter(company=company)

    def filter_in_flow(self, company_id, roles, is_current=True):
        return self.not_declined().filter(
            company_id=company_id, flows__roles__role__in=roles, flows__is_current=is_current,
            flows__roles__is_signed=False
        )

    def in_flow(self, company_id, roles, is_current=True):
        return self.filter(
            company_id=company_id, flows__roles__role__in=roles, flows__is_current=is_current,
            flows__roles__is_signed=False
        )

    def with_intervention_check(self):
        return self.annotate(
            has_intervention=models.Count('flows', filter=Q(intervention_at__isnull=False))
        )

    def not_declined(self):
        return self.exclude(status__slug__in=['decline-printed'])

    def open(self, supplier):
        return self.annotate(
            amount=Case(
                When(open_amount__gt=0, then=F('open_amount')),
                When(open_amount__lt=0, then=F('open_amount')),
                When(status__slug__in=['paid', 'final-signed', 'paid-not-account-posted'], then=F('total_amount'))
            )
        ).filter(
            supplier=supplier,
            status__slug__in=[
                enums.InvoiceStatus.IN_FLOW_PRINTED.value, enums.InvoiceStatus.ACCOUNT_POSTED_PRINTED.value,
                enums.InvoiceStatus.PAID_NOT_ACCOUNT_POSTED.value, enums.InvoiceStatus.PARTIALLY_PAID.value,
                enums.InvoiceStatus.PAID_PRINTED.value
            ]
        ).exclude(amount=0)

    def invoices(self):
        return self.exclude(invoice_type__code='purchase-order')


class InvoiceManager(models.Manager):

    def paid(self):
        return self.get_queryset().filter(status__slug='paid')

    def company(self, company):
        return self.get_queryset().filter(company=company)

    def status(self, status):
        return self.get_queryset().filter(status=status)

    def get_at_logger(self, company, excludes=None, year=None):
        if not excludes:
            excludes = []

        qs = self.filter(company=company, status__slug='arrived-at-logger').exclude(
            deleted__isnull=False
        ).exclude(
            id__in=excludes
        )
        if year:
            qs = qs.filter(accounting_date__year=year.year)
        return qs

    def purchase_order(self, company, supplier=None, status_slug=None):
        q = self.filter(company=company, invoice_type__code='purchase-order')
        if supplier:
            q = q.filter(supplier=supplier)
        if status_slug:
            q = q.filter(status__slug=status_slug)
        return q

    def receipts(self, company, supplier):
        return self.filter(company=company, supplier=supplier, invoice_type__code='receipt')

    def fetch(self):
        return self.get_queryset().select_related(
            'company', 'status', 'invoice_type', 'workflow', 'supplier', 'inventory_purchase_order'
        ).prefetch_related(
            'invoice_references', 'flows__status', 'flows', 'flows__roles__role'
        )

    def with_open_amount(self, company, date, supplier=None):
        filter_options = {'invoice_type__is_account_posting': True, 'company': company}
        if supplier:
            filter_options['supplier'] = supplier
        return self.select_related(
            'invoice_type', 'period', 'company', 'status', 'supplier', 'currency', 'period__period_year'
        ).prefetch_related(
            'payments'
        ).filter(
            **filter_options
        ).filter(
            ~Q(open_amount=0),
            period__isnull=False,
            period__period_year__isnull=False
        ).order_by(
            'supplier__code'
        )

    def total_amount_by_supplier(self, supplier, period):
        return self.get_queryset().filter(
            supplier=supplier, period=period, invoice_type__code='purchase-order'
        ).exclude(
            status__slug='po-declined'
        ).aggregate(
            total_value=models.Sum('net_amount')
        )

    def due_for_payments(self, company, supplier_id=None, end_date=None):
        q = Q()
        q.add(Q(company=company), Q.AND)
        if supplier_id:
            q.add(Q(supplier_id=supplier_id), Q.AND)

        if end_date:
            q.add(Q(due_date__lte=end_date), Q.AND)
        return self.open_for_payments().filter(q)

    def open_for_payments(self, filters=None):
        if filters is None:
            filters = {}
        statuses = utils.completed_statuses() + ['paid-printed', 'partially-paid', 'pp-not-account-posted']
        return self.get_queryset().select_related(
            'invoice_type', 'company', 'status', 'supplier'
        ).prefetch_related(
            'invoice_journals', 'payments', 'invoice_journals__journal'
        ).filter(
            **filters
        ).filter(
            status__slug__in=statuses, invoice_type__is_account_posting=True
        ).exclude(
            open_amount=0
        ).order_by(
            'supplier__name'
        )

    def pending_payments(self, company, year, start_date=None, end_date=None):
        statuses = ['paid', 'paid-not-printed', 'partially-paid']
        filters = {'status__slug__in': statuses, 'invoice_type__is_account_posting': True}
        if start_date:
            filters['accounting_date__gte'] = start_date
        if end_date:
            filters['accounting_date__lte'] = end_date
        return self.get_queryset().select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).prefetch_related(
            'invoice_journals', 'payments', 'invoice_journals__journal'
        ).filter(
            company=company, period__period_year=year
        ).filter(
            **filters
        ).exclude(
            open_amount=0
        )

    def pending_incoming(self, company, start_date=None, end_date=None, period=None, year=None):
        q = models.Q()
        q.add(Q(company=company), Q.AND)
        q.add(Q(status__slug__in=enums.InvoiceStatus.update_incoming_from_statuses()), Q.AND)
        q.add(Q(invoice_type__is_account_posting=True), Q.AND)

        if start_date:
            q.add(Q(accounting_date__gte=start_date), Q.AND)
        if end_date:
            q.add(Q(accounting_date__lte=end_date), Q.AND)
        if period:
            q.add(Q(period=period), Q.AND)
        if year:
            q.add(Q(period__period_year__year__lte=year.year), Q.AND)
        return self.filter(q)

    def pending_posting(self, company, start_date=None, end_date=None, period=None, year=None, statuses=None):
        q = Q()
        q.add(Q(company=company), Q.AND)
        q.add(Q(invoice_type__is_account_posting=True), Q.AND)
        if not statuses:
            statuses = enums.InvoiceStatus.posting_statuses()
        q.add(Q(status__slug__in=statuses), Q.AND)
        if start_date:
            q.add(Q(accounting_date__gte=start_date), Q.AND)
        if end_date:
            q.add(Q(accounting_date__lte=end_date), Q.AND)
        if period:
            q.add(Q(period=period), Q.AND)
        if year:
            q.add(Q(period__period_year__year__lte=year.year), Q.AND)
        return self.select_related('status', 'supplier', 'company', 'invoice_type').filter(q)

    def count_pending_payment_update(self, company, start_date, end_date):
        posting_status_slug = ['final-signed', 'paid-not-account-posted', 'paid-printed-not-posted',
                               'pp-not-account-posted', 'paid-printed-not-account-posted']
        return self.filter(
            company=company, accounting_date__gte=start_date, accounting_date__lt=end_date,
            status__slug__in=posting_status_slug, invoice_type__is_account_posting=True
        ).count()

    # def monthly_report(self, company, start_date, end_date):
    #     status_codes = ['11', '20', '30']
    #     not_yet_posted_filter = Q()
    #     not_yet_posted_filter.add(Q(status__code__in=status_codes), Q.AND)
    #     not_yet_posted_filter.add(Q(invoice_type__is_account_posting=True), Q.AND)
    #     not_yet_posted_filter.add(Q(invoice_type__is_account_posting=True), Q.AND)
    #     not_yet_posted_filter.add(Q(invoice_type__is_account_posting=True), Q.AND)
    #
    #     return self.get_queryset().annotate(
    #         not_yet_posted_count=Case(
    #             When(not_yet_posted_filter, then=Count('id')), output_field=DecimalField()
    #         ),
    #         incoming_count=Case(
    #             When(incoming_report_filter, then=Count('id')), output_field=DecimalField()
    #         ),
    #         posting_count=Case(
    #             When(posting_report_filter, then=Count('id')), output_field=DecimalField()
    #         )
    #     ).filter(
    #         company=company,
    #         accounting_date__gte=start_date,
    #         accounting_date__lt=end_date,
    #     ).values('not_yet_posted_count', 'incoming_count', 'posting_count')

    def not_yet_posted(self, company, year=None):
        q = self.get_queryset().select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency'
        ).prefetch_related(
            'invoice_references', 'flows', 'flows__roles'
        ).filter(
            company=company, status__slug__in=utils.processing_statuses(), invoice_type__is_account_posting=True
        )
        if year:
            q = q.filter(period__period_year__year__lte=year.year)
        return q

    def supplier_age_analysis(self, filter_options):
        self.get_queryset().select_related(
            'invoice_type', 'period', 'company', 'status', 'supplier', 'currency'
        ).prefetch_related(
            'payments'
        ).filter(
            **filter_options
        ).order_by('supplier__code')

    def get_year_invoices(self, company, year, **kwargs):
        q = Q()
        if kwargs.get('status', None):
            status = kwargs.get('status')
            if status == 'rejected':
                q.add(Q(status__slug__in=['decline-approved', 'decline-printed']), Q.AND)
            else:
                q.add(Q(status__slug=kwargs.get('status')), Q.AND)

        return self.select_related(
            'invoice_type', 'company', 'status', 'supplier', 'currency', 'period'
        ).prefetch_related(
            'invoice_references', 'purchase_orders'
        ).filter(company=company, period__period_year=year).filter(
            q
        ).exclude(
            deleted__isnull=False
        ).order_by('id')

    def get_invoice_statuses(self, statuses):
        completed = utils.completed_statuses() + ['partially-paid']
        processing = []
        invoice_statuses = []
        if statuses:
            for status in statuses:
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        else:
            invoice_statuses = completed
        return invoice_statuses

    def get_sub_query_for_supplier_age(self, company, period, is_last=False):
        age_filter = {'company': company, 'invoice_type__is_account_posting': True, 'supplier_id': models.OuterRef('id')}
        if is_last:
            age_filter['accounting_date__lte'] = period.to_date
        else:
            age_filter['period'] = period
        return self.values(
            'supplier', 'period'
        ).annotate(
            total_open=Sum('open_amount')
        ).filter(
            **age_filter
        ).order_by().values_list('total_open')

    def get_total_for_supplier_age(self, company, year, period, is_last=False):
        age_filter = {'company': company, 'period__period_year': year, 'invoice_type__is_account_posting': True,
                      'supplier_id': models.OuterRef('id')}
        if is_last:
            age_filter['period__to_date__lte'] = period.to_date
        else:
            age_filter['period'] = period
        return self.values(
            'supplier', 'period'
        ).filter(
            **age_filter
        ).annotate(
            total_amount=Sum(F('total_amount'))
        ).order_by().values_list('total_amount')

    def day_forecasting(self, year, is_completed=False, is_processing=False):
        filters = {'company': year.company, 'invoice_type__is_account_posting': True, 'period__period_year': year}
        statuses = []
        if is_completed:
            statuses = utils.completed_statuses() + ['partially-paid']
        if is_processing:
            statuses = statuses + utils.processing_statuses()
        if statuses:
            filters['status__slug__in'] = statuses
        return self.values(
            'due_date'
        ).annotate(
            total_open=Sum('open_amount'),
            is_due=models.Case(
                models.When(due_date__lt=datetime.now().date(), then=True),
                models.When(due_date__gte=datetime.now().date(), then=False),
                output_field=models.BooleanField(),
                default=False
            )
        ).filter(**filters).exclude(open_amount=0).values(
            'due_date', 'total_open', 'is_due'
        ).order_by('due_date')


class PurchaseOrderManager(models.Manager):

    def get_queryset(self, company, supplier=None, status_slug=None):
        q = self.filter(company=company, invoice_type__code='purchase-order')
        if supplier:
            q = q.filter(supplier=supplier)
        if status_slug:
            q = q.filter(status__slug=status_slug)
        return q


class InvoiceAccountManager(models.Manager):
    """
    Invoice accounts manager
    """

    def get_queryset(self):
        return super(InvoiceAccountManager, self).get_queryset().filter(deleted__isnull=True)


# class PaymentQuerySet(models.QuerySet):
#
#     def company(self, company):
#         return self.filter(company=company)
#
#
# class PaymentManager(models.Manager):
#     def get_queryset(self):
#         return PaymentQuerySet(self.model, using=self._db)  # Important!
#
#     # def annotate_sum_for_invoices(self, related_model_class, field_name, annotation_name):
#     #     raw_query = "SELECT SUM{field} FROM {model} WHERE model.payment_id == payment.id".format(
#     #         field=field_name, model=related_model_class._meta.db_table
#     #     )
#     #     annotation = {annotation_name: RawSQL(raw_query, [])}
#     #     return self.annotate(**annotation)
#
#     def available(self, company, supplier):
#         return self.get_queryset().annotate(
#             amount_available=models.F('balance') * -1
#         ).select_related(
#             'company', 'supplier'
#         ).prefetch_related(
#             'invoices', 'children'
#         ).filter(
#             company=company, supplier=supplier
#         ).exclude(
#             amount_available=0
#         )
#
#     def with_totals(self, filter_options):
#         return self.get_queryset().annotate(
#             amount_available=models.F('balance') * -1
#         ).select_related(
#             'company', 'supplier'
#         ).prefetch_related(
#             'invoices', 'children'
#         ).filter(
#             **filter_options
#         )
#
#     def with_available_amount(self, filter_options):
#         return self.with_totals(filter_options).filter(balance__gt=0)
#
#     def pending_updates(self, company, year, start_date=None, end_date=None):
#         q = models.Q()
#         if start_date:
#             q.add(models.Q(payment_date__gte=start_date), models.Q.AND)
#         if end_date:
#             q.add(models.Q(payment_date__lt=end_date), models.Q.AND)
#         return self.get_queryset().filter(company=company, journal__isnull=True, period__period_year=year)
#
#     def process_deletion(self, payment_id):
#         final_signed_status = Status.objects.filter(slug='final-signed').first()
#         end_flow_printed_status = Status.objects.filter(slug='end-flow-printed').first()
#         with transaction.atomic():
#             payment = Payment.objects.prefetch_related(
#                 'invoices',
#                 'children'
#             ).get(pk=payment_id)
#
#             if payment:
#                 invoice_payments = payment.invoices.all()
#                 invoices = []
#                 for invoice_payment in invoice_payments:
#                     invoice = invoice_payment.invoice
#                     invoices.append(invoice)
#                     invoice.date_paid = None
#                     invoice.payment_date = None
#                     if invoice.status:
#                         if invoice.status.slug == 'paid-not-account-posted':
#                             invoice.status = final_signed_status
#                         elif invoice.status.slug in ['paid-not-printed', 'partially-paid']:
#                             invoice.status = end_flow_printed_status
#                     invoice.save()
#
#                     comments = invoice.invoice_comments.filter(link_url=payment.get_absolute_url())
#                     comments.delete()
#
#                 for child_payment in payment.children.all():
#                     payment_child = child_payment.payment
#                     payment_child.balance = payment_child.net_amount
#                     payment_child.save()
#
#                 payment.balance = payment.net_amount
#                 payment.delete()
#
#                 for invoice in invoices:
#                     open_amount = invoice.calculate_open_amount()
#                     invoice.open_amount = open_amount
#                     invoice.save()
#
#     def create_child_payment(self, parent_payment, payment_id, amount):
#         payment = Payment.objects.filter(id=payment_id).first()
#         payment_amount = Decimal(amount)
#         if payment and payment_amount:
#             child_payment = ChildPayment.objects.create(
#                 parent=parent_payment,
#                 payment=payment,
#                 amount=payment_amount
#             )
#             payment.balance = payment.balance - child_payment.amount
#             payment.save()

