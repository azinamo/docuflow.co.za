import logging
import pprint
from decimal import Decimal
from calendar import monthrange

from annoying.functions import get_object_or_None
from django.db import transaction
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.timezone import now, datetime

from docuflow.apps.accounts.enums import DocuflowPermission
from docuflow.apps.accounts.models import Role, Profile
from docuflow.apps.accountposting.enums import LineType
from docuflow.apps.company.models import ObjectItem, VatCode
from docuflow.apps.fixedasset.enums import FixedAssetStatus
from docuflow.apps.fixedasset.forms import InvoiceAssetForm
from docuflow.apps.intervention.models import Intervention
from docuflow.apps.period.models import Year, Period
from docuflow.apps.supplier.models import Ledger as SupplierLedger, SupplierAuthorization
from .enums import FlowRoleStatus
from .exceptions import (InvalidInvoiceTotal, InvoiceJournalNotAvailable, DefaultAccountNotPosted, InvoiceHasVariance,
                         UnavailableWorkflow, AccountingDateClosed, InvoiceException)
from docuflow.apps.payroll.models import LinkingCode, StatutorySetup
from .models import Invoice, InvoiceAccount, Flow, FlowRole, Status, FlowStatus, Comment
from .client import payroll_upload_client
from ..common.utils import get_cell_value

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class InvoiceSign(object):

    def __init__(self, invoice):
        try:
            with transaction.atomic():
                at_last = invoice.at_last()

                has_variance = invoice.has_variance()

                # At last user
                if at_last:

                    is_valid_accounting_date_period = invoice.validate_accounting_date_period()

                    in_incoming_invoice = invoice.printed_in_flow
                    # Default Expense account as per SAL may not be in the account posting section
                    has_default_account = invoice.has_default_company_account()
                    # there are no more roles on this invoice
                    # Check if there is Default account posting still in the account posting side of things

                    if not in_incoming_invoice and invoice.has_account_posting:  # 1. Invoice status must be 20
                        raise InvoiceJournalNotAvailable('Please wait a few hours with this invoice, it needs to be '
                                                         'added to a Journal of incoming invoices.')
                    elif has_default_account:
                        raise DefaultAccountNotPosted('The Default Account has not been posted, either a) add someone '
                                                      'to the  flow or b) fix the line posting so there is not default '
                                                      'account in  account posting section')
                    elif has_variance:  # There cannot be a variance
                        raise InvoiceHasVariance('The invoice has not  been totally allocated, either add a role to '
                                                 'this flow or edit the account postings')
                    elif not invoice.period.is_open:  # check the ACCOUNING DATE period is open
                        raise AccountingDateClosed('The accounting date is closed, please change the accounting date '
                                                   'to the current period that is open')
                    else:
                        sign = SignInvoice(invoice)
                        sign.process_signing()
                else:
                    if invoice.validate_accounting_date_period():
                        sign = SignInvoice(invoice)
                        sign.process_signing()
                    else:
                        raise AccountingDateClosed('The accounting date is closed, please change the accounting date '
                                                   'to the current period that is open')
        except InvoiceException as ex:
            logger.info(ex)


class SignInvoice(object):

    def __init__(self, invoice, profile, role, intervene=True):
        logger.info("---- start signing invoice ----")
        self.invoice = invoice
        self.profile = profile
        self.role = role
        self.current_flow = None
        self.intervene = intervene
        self.set_current_flow()

    def set_current_flow(self):
        self.current_flow = self.invoice.get_current_flow()

    def process_signing(self):
        logger.info("---- process_signing -- executing ----")
        with transaction.atomic():
            logger.info(f"----has intervention ---- {self.intervene}")
            if self.intervene:
                flow_intervention = FlowIntervention('signing', self.invoice)
                flow_intervention.process_intervention()

            sign_role = SignRole(self.current_flow, self.profile, self.role)
            sign_role.sign()

            sign_flow_level = SignFlowLevel(self.current_flow)
            sign_flow_level.sign()

            complete_invoice_sign = CompleteSignInvoice(self.invoice)
            complete_invoice_sign.sign_invoice()

            self.log_activity()

    def complete_sign_invoice(self):
        pass

    def log_activity(self):
        self.invoice.log_activity(self.profile.user, self.role, 'Invoice signed', Comment.EVENT)

    def sign_intervention(self):
        pass

    def has_variance(self):
        pass

    def has_default_account(self):
        pass

    def has_incoming_invoice(self):
        pass


class UnSignInvoice(object):

    def __init__(self, invoice: Invoice, profile: Profile, role: Role, current_flow: Flow):
        logger.info("---- start un-signing invoice ----")
        self.invoice = invoice
        self.profile = profile
        self.role = role
        self.current_flow = current_flow

    def execute(self):
        logger.info("---- executing ----")
        with transaction.atomic():
            logger.info(f"Current flow is --> {self.current_flow}")
            role_flow = FlowRole.objects.filter(flow=self.current_flow).last()
            if role_flow:
                role_flow.status = FlowRole.PROCESSING
                role_flow.is_signed = False
                role_flow.signed_at = None
                role_flow.signed_by = None
                role_flow.save()

            invoice_flow_status = FlowStatus.objects.get(slug='in-flow')
            if invoice_flow_status:
                self.current_flow.status = invoice_flow_status
                self.current_flow.is_signed = False
                self.current_flow.is_current = True
                self.current_flow.sign_count = 0
                self.current_flow.save()

            self.invoice.set_in_flow_printed()
            self.invoice.save()

            self.remove_fixed_assets()

            self.log_activity('Invoice unsigned')

    def remove_fixed_assets(self):
        for invoice_account in self.invoice.invoice_accounts.all():
            fixed_asset = invoice_account.fixed_asset.all()
            fixed_asset.delete()

    def complete_sign_invoice(self):
        pass

    def log_activity(self, comment=None):
        text = comment if comment else 'Invoice signed'
        self.invoice.log_activity(self.profile.user, self.role, text, Comment.EVENT)

    def sign_intervention(self):
        pass

    def has_variance(self):
        pass

    def has_default_account(self):
        pass

    def has_incoming_invoice(self):
        pass


class CancelPurchaseOrder(object):
    def __init__(self, invoice, profile, role):
        self.invoice = invoice
        self.profile = profile
        self.role = role
        self.current_flow = None
        self.set_current_flow()

    def set_current_flow(self):
        self.current_flow = self.invoice.get_current_flow()

    def process_cancellation(self):
        with transaction.atomic():
            if self.current_flow:
                logger.info(f"Current flow is {self.current_flow}")

                sign_role = SignRole(self.current_flow, self.profile, self.role)
                sign_role.sign()

                sign_flow_level = SignFlowLevel(self.current_flow)
                sign_flow_level.sign()

                cancelled_status = get_object_or_None(Status, slug='po-cancelled')

                if cancelled_status:
                    complete_invoice_sign = CompleteSignInvoice(self.invoice, cancelled_status)
                    complete_invoice_sign.sign_invoice()
            else:
                cancelled_status = get_object_or_None(Status, slug='po-cancelled')

                if cancelled_status:
                    complete_invoice_sign = CompleteSignInvoice(self.invoice, cancelled_status)
                    complete_invoice_sign.sign_invoice()


class SignRole(object):
    def __init__(self, flow, profile, role):
        logger.info(f" --- Sign Role {role}")
        self.flow = flow
        self.profile = profile
        self.role = role

    def sign(self):
        role_flow = get_object_or_None(FlowRole, role=self.role, flow=self.flow)
        if not role_flow:
            raise InvoiceException(f'Invoice could not be signed by the current role {self.role}')
        role_flow.status = FlowRole.PROCESSED
        role_flow.is_signed = True
        role_flow.signed_at = now()
        role_flow.signed_by = self.profile
        role_flow.save()
        FlowRole.objects.role_stats(role_id=self.role.id, year=self.flow.invoice.period.period_year)


class SignFlowLevel(object):

    def __init__(self, flow):
        logger.info("Sign flow level {}".format(flow))
        self.flow = flow

    def roles_signed(self, flow):
        flow_roles = FlowRole.objects.filter(flow=flow).all()
        signed_roles = 0
        total_roles = 0
        for flow_role in flow_roles:
            total_roles = total_roles + 1
            if flow_role.is_signed is True:
                signed_roles = signed_roles + 1
        return signed_roles == total_roles

    def sign(self):
        if self.roles_signed(self.flow):
            invoice_flow_status = get_object_or_None(FlowStatus, slug='processed')
            if invoice_flow_status:
                self.flow.status = invoice_flow_status
                self.flow.is_signed = True
                self.flow.save()
                flow_progress = FlowProgress(self.flow.invoice, self.flow)
                flow_progress.process_next_flow()


class FlowProgress(object):
    def __init__(self, invoice, flow):
        self.flow = flow
        self.invoice = invoice

    def process_next_flow(self):
        flows = self.invoice.flows.all().order_by('order_number')
        counter = 0
        for flow in flows:
            if self.flow:
                if flow.order_number > self.flow.order_number:
                    self.set_current_flow(flow)
                    break
            else:
                if counter == 0:
                    self.set_current_flow(flow)
                    break
            counter = counter + 1

        if self.flow:
            self.flow.is_signed = True
            self.flow.is_current = False
            self.flow.sign_count = self.flow.roles.all().count()
            self.flow.save()

    @staticmethod
    def set_current_flow(flow):
        flow.is_current = True
        flow_status = get_object_or_None(FlowStatus, slug='processing')
        if flow_status:
            flow.status = flow_status
        flow.save()


class CompleteSignInvoice(object):
    def __init__(self, invoice, status=None):
        self.invoice = invoice
        self.status = status

    def flows_signed(self):
        signed_flows = 0
        total_flows = 0
        for flow in self.invoice.flows.all():
            total_flows = total_flows + 1
            if flow.is_signed is True:
                signed_flows = signed_flows + 1
        logger.info(f"Total flows {total_flows} and signed flows {signed_flows} = {total_flows == signed_flows}")
        return total_flows == signed_flows

    def sign_invoice(self):
        logger.info('CompleteSignInvoice:sign_invoice')
        if self.flows_signed():
            logger.info('1 - can sign')
            if not self.status:
                logger.info(f'1 - can sign automatically and its a purchase order {self.invoice.is_purchase_order}')
                if self.invoice.is_purchase_order:
                    invoice_status = self.get_purchase_order_signing_status()
                    logger.info(f'2 - Next status is {invoice_status}')
                elif not self.invoice.invoice_type.is_account_posting:
                    invoice_status = self.get_non_posting_signing_status()
                else:
                    invoice_status = self.get_default_signing_status()
            else:
                invoice_status = self.status

            if invoice_status:
                self.invoice.status = invoice_status
                self.invoice.save()

                if self.invoice.has_account_posting and invoice_status.slug not in ['decline-approved', 'decline-request-from-user']:
                    not_linked = InvoiceAccount.objects.link_to_asset(invoice=self.invoice, is_linked=False).count()
                    if not_linked > 0:
                        raise InvoiceException(f'{not_linked} Invoice accounts linked to fixed assets accounts have not '
                                               f'been created, please fix')
            # self.create_asset_entry()

    def update_cache(self):
        pass
        # FlowRole.objects.role_stats(role_id=self)

    def get_default_signing_status(self):
        status = get_object_or_None(Status, slug='final-signed')
        return status

    def get_non_posting_signing_status(self):
        status = get_object_or_None(Status, slug='paid-printed')
        return status

    def get_purchase_order_signing_status(self):
        invoice_status = None
        if self.invoice.is_requested:
            return get_object_or_None(Status, slug='po-approved')
        elif self.invoice.is_approved:
            return get_object_or_None(Status, slug='po-ordered')
        elif self.invoice.is_ordered:
            return get_object_or_None(Status, slug='po-received')
        return invoice_status


class FlowIntervention(object):
    def __init__(self, event_type, invoice):
        self.event_type = event_type
        self.invoice = invoice

    def process_intervention(self):
        for invoice_account in self.invoice.invoice_accounts.all():
            self.intervene(invoice_account)

    def get_interventions(self):
        interventions = Intervention.objects.select_related(
            'supplier', 'account', 'document_type', 'role'
        ).prefetch_related('object_items').filter(
            company=self.invoice.company, event=self.event_type).all()
        return interventions

    def supplier_matches(self, intervention, invoice_account):
        if intervention.supplier:
            if intervention.supplier == invoice_account.invoice.supplier:
                return True
            else:
                return False
        return True

    def document_matches(self, intervention, invoice_account):
        if intervention.document_type:
            return intervention.document_type == invoice_account.invoice.invoice_type
        return True

    def account_matches(self, intervention, invoice_account):
        if intervention.account:
            return invoice_account.account == intervention.account
        return True

    def object_items_matches(self, intervention, invoice_account):
        if intervention.object_items.count() > 0:
            invoice_account_object_items = invoice_account.object_items.all()
            object_item_ids = [object_item.id for object_item in invoice_account_object_items]
            match_count = 0
            counter = 0
            for object_item in intervention.object_items.all():
                counter += 1
                if object_item.id in object_item_ids:
                    match_count += 1
            return match_count == counter
        return True

    def amount_greater_than(self, intervention, invoice_account):
        if intervention.amount_check == Intervention.GREATER_THAN:
            return invoice_account.amount > intervention.amount
        return True

    def amount_less_than(self, intervention, invoice_account):
        if intervention.amount_check == Intervention.LESS_THAN:
            return invoice_account.amount < intervention.amount
        return True

    def amount_equal(self, intervention, invoice_account):
        if intervention.amount_check == Intervention.EQUAL:
            return invoice_account.amount == intervention.amount
        return True

    def intervene(self, invoice_account):
        interventions = self.get_interventions()
        for intervention in interventions:
            if not self.supplier_matches(intervention, invoice_account):
                continue
            if not self.document_matches(intervention, invoice_account):
                continue
            if not self.account_matches(intervention, invoice_account):
                continue
            if not self.object_items_matches(intervention, invoice_account):
                continue
            if intervention.amount_check:
                if intervention.amount_check == Intervention.GREATER_THAN:
                    if invoice_account.amount < intervention.amount:
                        continue
                    else:
                        self.execute_intervention(intervention)
                        return intervention
                if intervention.amount_check == Intervention.LESS_THAN:
                    if invoice_account.amount > intervention.amount:
                        continue
                    else:
                        self.execute_intervention(intervention)
                        return intervention
                if intervention.amount_check == Intervention.EQUAL:
                    if invoice_account.amount == intervention.amount:
                        self.execute_intervention(intervention)
                        return intervention
                    else:
                        continue
            self.execute_intervention(intervention)
        return False

    def execute_intervention(self, intervention):
        if intervention:
            if not self.invoice.intervention_at:
                if intervention.role:
                    order_number = 1
                    if intervention.flow_position == Intervention.AT_CURRENT_LEVEL:
                        current_flow = self.invoice.get_current_flow()
                        FlowRole.objects.create(
                            flow=current_flow,
                            role=intervention.role,
                            status=FlowRole.PROCESSING,
                            intervention=intervention
                        )
                    else:
                        update_order_numbers = False
                        if intervention.flow_position == Intervention.AT_NEXT_STEP:
                            current_flow = self.invoice.get_current_flow()
                            order_number = current_flow.order_number + 1
                            update_order_numbers = True
                        elif intervention.flow_position == Intervention.AT_END:
                            order_number = self.invoice.flows.count() + 1

                        flow_status = get_object_or_None(FlowStatus, slug='in-flow')
                        if flow_status:
                            flow = Flow.objects.create(
                                invoice=self.invoice,
                                order_number=order_number,
                                intervention_at=now(),
                                status=flow_status
                            )
                            if flow:
                                FlowRole.objects.create(
                                    flow=flow,
                                    role=intervention.role,
                                    status=FlowRole.PROCESSING,
                                    intervention=intervention
                                )
                            if update_order_numbers:
                                flow.adjust_order_numbers_after(self.invoice)
                self.invoice.intervention_at = now()
                self.invoice.save()

    def log_intervention(self):
        pass


class InvoiceFlow(object):

    @staticmethod
    def create_flow_role(invoice, role, order_number, flow_status, is_signed=False, signed_by=None, is_current=False):
        flow = Flow.objects.create(
            order_number=order_number,
            invoice=invoice,
            status=flow_status,
            is_current=is_current
        )
        if flow:
            flow_role = FlowRole.objects.create(
                flow=flow,
                role=role,
                status=FlowRole.PROCESSED
            )
            if flow_status.slug != 'processed':
                flow_role.status = FlowRole.PROCESSING
                flow_role.save()

            if is_signed:
                flow.is_signed = True
                flow.signed_at = now()
                flow.save()

                if flow_role:
                    flow_role.is_signed = True
                    flow_role.signed_at = now()
                    flow_role.signed_by_id = signed_by
                    flow_role.save()
            return flow


class AddToFlow(object):

    def __init__(self, invoice, profile, role):
        self.invoice = invoice
        self.profile = profile
        self.role = role

    def get_workflow(self):
        return self.invoice.get_workflow()

    def get_invoice_status(self):
        if self.invoice.has_account_posting:
            return Status.objects.get(slug='in-the-flow')
        else:
            return Status.objects.get(slug='printed-status-still-in-the-flow')

    def add_invoice_flows(self, workflow, role):
        if not self.invoice.flows.exists():
            self.update_flows(workflow=workflow)

    @staticmethod
    def get_flow_level_status(statuses, level_counter):
        if level_counter == 0:
            flow_status = statuses.get('processing')
        else:
            flow_status = statuses.get('in-flow')
        return flow_status

    @staticmethod
    def get_flow_statuses():
        return {
            flow_status.slug: flow_status
            for flow_status in FlowStatus.objects.all()
        }

    def update_flows(self, workflow):
        counter = 0
        statuses = self.get_flow_statuses()
        flow_levels = workflow.levels.all().order_by('sort_order')
        for level in flow_levels:
            is_current = False
            if counter == 0:
                is_current = True
            flow_status = self.get_flow_level_status(statuses=statuses, level_counter=counter)

            invoice_flow = Flow.objects.create(
                invoice=self.invoice,
                order_number=level.sort_order,
                status=flow_status,
                is_current=is_current
            )
            if invoice_flow:
                for role in level.roles.all():
                    FlowRole.objects.create(
                        role=role,
                        flow=invoice_flow,
                        status=FlowRole.PROCESSING
                    )
            counter = counter + 1

    def add_account_postings(self, profile, role):
        # check if this invoice has a specific account posting proposal, if not use the company account proposal
        if self.invoice.account_posting_proposal:
            for account_posting_line in self.invoice.account_posting_proposal.lines.all():
                if account_posting_line.type == LineType.EXPENDITURE:
                    amount = (account_posting_line.percentage / 100) * self.invoice.net_amount
                    self.add_account_posting_line(
                        account_posting_line=account_posting_line,
                        profile=profile,
                        role=role,
                        amount=amount
                    )
            if self.invoice.vat_code:
                self.add_account_posting(
                    account=self.invoice.vat_code.account,
                    amount=self.invoice.vat_amount,
                    profile=profile,
                    role=role,
                    is_vat=True,
                    vat_code=self.invoice.vat_code
                )
        else:
            expenditure_account = self.invoice.company.default_expenditure_account
            vat_code = self.invoice.vat_code
            if expenditure_account:
                self.add_account_posting(
                    account=expenditure_account,
                    amount=self.invoice.net_amount,
                    profile=profile,
                    role=role,
                    is_vat=False
                )

            if vat_code:
                self.add_account_posting(
                    account=vat_code.account,
                    amount=self.invoice.vat_amount,
                    profile=profile,
                    role=role,
                    is_vat=True,
                    vat_code=self.invoice.vat_code
                )

    def add_account_posting(self, account, amount, profile, role, is_vat=False, vat_code=None):
        if amount != 0:
            InvoiceAccount.objects.create(
                account_posting_line=None,
                amount=amount,
                profile=profile,
                role=role,
                invoice=self.invoice,
                is_vat=is_vat,
                percentage=100,
                vat_code=vat_code,
                account=account
            )

    def add_account_posting_line(self, account_posting_line, profile, role, amount, is_vat=False):
        if amount != 0:
            invoice_account = InvoiceAccount.objects.create(
                account_posting_line=account_posting_line,
                amount=amount,
                profile=profile,
                role=role,
                invoice=self.invoice,
                percentage=account_posting_line.percentage,
                account=account_posting_line.account,
                is_vat=is_vat
            )
            if invoice_account:
                for line_object_item in account_posting_line.object_items.all():
                    invoice_account.object_items.add(line_object_item)

    def process_flow(self):
        with transaction.atomic():
            if not self.invoice.valid_invoice_totals():
                raise InvalidInvoiceTotal("The NETT AMOUNT plus the VAT AMOUNT do not add up to the TOTAL AMOUNT as "
                                          "recorded, please correct")

            if self.invoice.status and self.invoice.status.slug != 'arrived-at-logger':
                raise InvoiceException('Invoice already added to flow, please refresh logger')

            workflow = self.get_workflow()
            if not workflow:
                raise UnavailableWorkflow("Workflow for the invoice not available, please choose the default"
                                          " workflow for your company")
            if self.invoice.is_credit_type:
                self.invoice.set_credit_note_amount()

            self.invoice.status = self.get_invoice_status()
            self.invoice.save()

            self.add_invoice_flows(workflow=workflow, role=self.role)

            if self.invoice.invoice_type.is_account_posting:
                self.add_account_postings(profile=self.profile, role=self.role)

            SupplierLedger.objects.create_from_invoice(invoice=self.invoice, profile=self.profile)


class InvoiceAccountLine(object):  # TODO move most methods to Model

    def __init__(self, invoice_account: InvoiceAccount, linked_models, role_permissions):
        self.invoice_account = invoice_account
        self.is_vat = invoice_account.is_vat
        self.amount = invoice_account.amount
        self.distribution_html = ''
        self.distribution = invoice_account.distribution
        if self.invoice_account.distribution:
            self.account = invoice_account.invoice.company.default_distribution_account
            self.distribution_html = self.get_distribution_template(self.invoice_account.distribution)
        else:
            self.account = invoice_account.account
        self.vat_code = self.get_vat_code()
        self.percentage = invoice_account.percentage
        self.comment = invoice_account.comment
        self.date = invoice_account.created_at
        self.reinvoice = invoice_account.reinvoice
        self.suffix = invoice_account.suffix
        self.signed = False
        accounts_profile = self.get_account_profile()
        self.profile = accounts_profile
        self.posted_to_accounts = accounts_profile
        self.linked_models_values = {}
        self.id = invoice_account.id
        self.invoice_id = invoice_account.invoice.id
        self.url = self.get_edit_url()
        self.object_links = self.get_object_links(linked_models)
        self.is_default_account_posting = self.is_default_account_posting()
        self.can_distribute = self.can_distribute(role_permissions)

    def can_distribute(self, role_permissions):
        if self.is_default_account_posting or self.invoice_account.is_vat:
            return False
        if not self.invoice_account.account.can_distribute:
            return False
        return DocuflowPermission.CAN_DISTRIBUTE.value in role_permissions and self.invoice_account.invoice.is_processing

    def get_distribution_template(self, distribution):
        account_distribution = self.get_invoice_account_distribution(distribution)
        html = render_to_string('distribution/show.html', account_distribution)
        html = html.replace('"', '\"')
        return html

    def get_distribution_invoice_accounts(self, nb_months, is_last_month):

        prev_months_counter = (nb_months - 1)
        if is_last_month:
            _accounts_total = round((self.invoice_account.amount / nb_months), 2) * prev_months_counter
            amount = round((self.invoice_account.amount - _accounts_total), 2)
        else:
            amount = self.invoice_account.amount / nb_months
        account = {'amount': round(amount, 2),
                   'account': self.invoice_account.account.code,
                   'account_id': self.invoice_account.account.id,
                   'invoice_account_id': self.invoice_account.id
                   }
        return account

    def calculate_total_credit(self):
        return self.invoice_account.amount

    def get_invoice_account_distribution(self, distribution):
        default_distribution_account = self.invoice_account.invoice.company.default_distribution_account
        start_date = distribution.starting_date
        months = distribution.nb_months
        current_month = start_date.month
        current_year = start_date.year
        day = start_date.day
        month_counter = 0
        accounts_distribution = {'distributions': {}, 'total_distribution': 0}
        counter = 1
        total_distribution = 0
        for month in range(months):
            _day = day
            _month = current_month + month_counter
            last_day = start_date.replace(year=current_year, month=_month, day=monthrange(current_year, _month)[1])
            if day > last_day.day:
                _day = last_day.day
            start_at = start_date.replace(year=current_year, month=_month, day=_day)
            is_last = (counter == months)

            distribution_invoice_account = self.get_distribution_invoice_accounts(months, is_last)
            total_credit = self.calculate_total_credit()
            total_distribution += total_credit
            accounts_distribution['distributions'][counter] = {
                'day': start_at,
                'invoice_account': distribution_invoice_account,
                'period': month + 1,
                'total': total_credit,
                'distribution_account': default_distribution_account
            }
            if _month == 12:
                current_month = 1
                month_counter = 0
                current_year = current_year + 1
            else:
                month_counter += 1
            counter += 1
        accounts_distribution['total_distribution'] = total_distribution
        return accounts_distribution

    def is_default_account_posting(self):
        return self.invoice_account.invoice.company.default_expenditure_account == self.invoice_account.account

    def get_account_profile(self):
        account_profile = self.invoice_account.profile.code
        if self.invoice_account.role:
            if self.invoice_account.is_posted is True:
                account_profile = self.invoice_account.profile.code
            else:
                account_profile = self.invoice_account.role
        return account_profile

    def get_vat_code(self):
        if self.invoice_account.vat_code:
            return self.invoice_account.vat_code
        return ''

    def get_edit_url(self):
        return reverse('edit_invoice_account', kwargs={'pk': self.invoice_account.id})

    def get_object_links(self, linked_models):
        object_links = {}
        for key, linked_model in linked_models.items():
            object_links[key] = {'value': None, 'code': None}

        for link in self.invoice_account.object_items.all():
            object_links[link.company_object_id] = ''
            object_links[link.company_object_id] = {'value': link.label, 'code': link.code}

        for object_posting_link in self.invoice_account.object_postings.all():
            object_links[object_posting_link.company_object_id] = ''
            object_links[object_posting_link.company_object_id] = {'value': object_posting_link.name,
                                                                   'code': object_posting_link.name}

        return object_links


class AccountPosting(object):

    def __init__(self, invoice, is_vat=False, role_permissions=dict()):
        self.invoice = invoice
        self.is_vat = is_vat
        self.total = 0
        self.accounts = []
        self.variance = 0
        self.role_permissions = role_permissions

    def get_invoice_accounts(self):
        return self.invoice.vat_accounts

    def process_posting(self, linked_models, invoice_accounts):
        # invoice_accounts = self.get_invoice_accounts()
        for invoice_account in invoice_accounts:
            if invoice_account.amount:
                self.total += invoice_account.amount
            invoice_account_line = InvoiceAccountLine(
                invoice_account=invoice_account,
                linked_models=linked_models,
                role_permissions=self.role_permissions
            )
            self.accounts.append(invoice_account_line)


class InvoiceAccountPostings(object):

    def __init__(self, invoice, role_permissions):
        self.invoice = invoice
        self.vat_variance = self.calculate_vat_variance()
        self.expenditure_variance = self.calculate_variance()
        self.role_permissions = role_permissions

    def calculate_vat_variance(self):
        vat_variance = VarianceCalculator(invoice=self.invoice, is_vat=True)
        vat_variance.calculate_variance()
        return vat_variance.variance

    def calculate_variance(self):
        expenditure_variance = VarianceCalculator(invoice=self.invoice)
        expenditure_variance.calculate_variance()
        return expenditure_variance.variance

    def process_postings(self, linked_models, account_postings):
        account_posting = AccountPosting(invoice=self.invoice, is_vat=False, role_permissions=self.role_permissions)
        account_posting.process_posting(linked_models=linked_models, invoice_accounts=account_postings)
        return account_posting

    def process_vat_postings(self, linked_models, account_postings):
        account_posting = AccountPosting(invoice=self.invoice, is_vat=True, role_permissions=self.role_permissions)
        account_posting.process_posting(linked_models=linked_models, invoice_accounts=account_postings)
        return account_posting


class VarianceCalculator(object):

    def __init__(self, invoice, is_vat=False):
        self.invoice = invoice
        self.is_vat = is_vat
        self.variance = 0

    def get_accounts_total(self):
        # excludes posted holding accounts
        accounts = InvoiceAccount.objects.filter(invoice=self.invoice, is_vat=self.is_vat).all()
        total = 0
        for account in accounts:
            total = total + (account.amount if account.amount else 0)
        return total

    def calculate_variance(self):
        holding_total = 0
        total = self.get_accounts_total()

        if self.is_vat:
            vat_amount = self.invoice.vat_amount if self.invoice.vat_amount else 0
            self.variance = vat_amount - (total + holding_total)
        else:
            net_amount = self.invoice.net_amount if self.invoice.net_amount else 0
            self.variance = net_amount - (total + holding_total)


class AccountDistribution(object):

    def __init__(self, company, invoice, months, start_date):
        self.company = company
        self.invoice = invoice
        self.nb_months = months
        self.start_date = start_date
        self.distribution_account = company.default_distribution_account
        self.year = self.get_year()
        self.periods = self.get_periods()
        self.accounts_distributions = {}
        self.total_distribution = 0

    def get_year(self):
        return Year.objects.filter(company=self.company, is_open=True).first()

    def get_periods(self):
        return Period.objects.filter(company=self.company, period_year=self.year)

    # def get_invoice_accounts(self):
    #     return InvoiceAccount.objects.filter(distribution=self.distribution)

    def generate_account_distributions(self, invoice_account):
        if self.year:
            start_date = self.start_date
            current_month = start_date.month
            current_year = start_date.year
            day = start_date.day
            months = int(self.nb_months)
            month_counter = 0

            counter = 1
            for month in range(months):
                _day = day
                _month = current_month + month_counter
                last_day = start_date.replace(year=current_year, month=_month, day=monthrange(current_year, _month)[1])
                if day > last_day.day:
                    _day = last_day.day
                start_at = datetime.replace(start_date, current_year, _month, _day)
                is_last = (counter == months)

                invoice_account_distribution = DistributeInvoiceAccount(invoice_account, months, is_last)
                invoice_account_distribution.process_distribution()

                total_credit = self.calculate_total_credit(invoice_account_distribution.accounts)
                self.total_distribution += total_credit

                self.accounts_distributions[counter] = {'day': start_at,
                                                        'periods': invoice_account_distribution.accounts,
                                                        'period': self.calculate_period(start_at),
                                                        'total': total_credit,
                                                        'distribution_account': self.distribution_account
                                                        }
                if _month == 12:
                    current_month = 1
                    month_counter = 0
                    current_year = current_year + 1
                else:
                    month_counter += 1
                counter += 1

    @staticmethod
    def calculate_total_credit(invoice_accounts):
        total = 0
        for acc in invoice_accounts:
            total += acc['amount']
        return total

    def calculate_period(self, distribution_date):
        for period in self.periods:
            if period.from_date <= distribution_date.date() <= period.to_date:
                return period
        return None


class DistributeInvoiceAccount(object):

    def __init__(self, invoice_account, nb_months, is_last):
        self.invoice_account = invoice_account
        self.nb_months = nb_months
        self.is_last = is_last
        self.accounts = []

    def calculate_amount(self, prev_months_counter):
        if self.is_last:
            _accounts_total = round((self.invoice_account.amount/self.nb_months), 2) * prev_months_counter
            amount = round((self.invoice_account.amount - _accounts_total), 2)
        else:
            amount = self.invoice_account.amount/self.nb_months
        return amount

    def process_distribution(self):
        prev_months_counter = self.nb_months - 1
        amount = self.calculate_amount(prev_months_counter)
        self.accounts.append({'amount': round(amount, 2),
                              'account': self.invoice_account.account.code,
                              'account_id': self.invoice_account.account.id,
                              'invoice_account_id': self.invoice_account.id
                              })


def calculate_total_credit(invoice_accounts):
    total = 0
    for acc in invoice_accounts:
        total += acc['amount']
    return total


def get_distribution_invoice_accounts(invoice_accounts, nb_months, is_last_month):
    accounts = []
    prev_months_counter = (nb_months-1)
    for invoice_account in invoice_accounts:
        if is_last_month:
            _accounts_total = round((invoice_account.amount/nb_months), 2) * prev_months_counter
            amount = round((invoice_account.amount - _accounts_total), 2)
        else:
            amount = invoice_account.amount/nb_months

        debit_amount = 0
        credit_amount = 0
        if invoice_account.invoice.is_credit_type:
            credit_amount = round(amount, 2)
        else:
            debit_amount = round(amount, 2)

        accounts.append({'amount': round(amount, 2),
                         'debit': debit_amount,
                         'credit': credit_amount,
                         'code': invoice_account.account.code,
                         'account_id': invoice_account.account.id,
                         'account': invoice_account.account,
                         'invoice_account_id': invoice_account.id
                         })
    return accounts


def distribute_invoice_accounts(company, invoice_accounts, months, start_date, periods, invoice):
    default_distribution_account = company.default_distribution_account
    current_month = start_date.month
    current_year = start_date.year
    day = start_date.day
    month_counter = 0
    accounts_distribution = {'distributions': {}, 'total_distribution': 0}
    counter = 1
    total_distribution = 0
    for month in range(months):
        _day = day
        _month = current_month + month_counter
        last_day = start_date.replace(year=current_year, month=_month, day=monthrange(current_year, _month)[1])
        if day > last_day.day:
            _day = last_day.day
        start_at = datetime.replace(start_date, current_year, _month, _day)
        is_last = (counter == months)

        distribution_invoice_accounts = get_distribution_invoice_accounts(invoice_accounts, months, is_last)
        total_credit = calculate_total_credit(distribution_invoice_accounts)
        debit_amount = 0
        credit_amount = 0
        if invoice.is_credit_type:
            debit_amount = total_credit
        else:
            credit_amount = total_credit
        total_distribution += total_credit
        accounts_distribution['distributions'][counter] = {'day': start_at,
                                                           'accounts': distribution_invoice_accounts,
                                                           'counter': month + 1,
                                                           'period': calculate_period(periods, start_at),
                                                           'total': total_credit,
                                                           'debit': debit_amount,
                                                           'credit': credit_amount,
                                                           'distribution_account': default_distribution_account
                                                           }
        print(f"distribution of invoice accounts, is credit {invoice.is_credit_type}")
        pp.pprint(accounts_distribution)
        if _month == 12:
            current_month = 1
            month_counter = 0
            current_year = current_year + 1
        else:
            month_counter += 1
        counter += 1
    accounts_distribution['total_distribution'] = total_distribution
    return accounts_distribution


def calculate_period(periods, period_date):
    for period in periods:
        if period.from_date <= period_date.date() <= period.to_date:
            return period
    return None


def add_invoice_accounts_assets(invoice, invoice_accounts_assets, user, role):

    for invoice_account_id, asset in invoice_accounts_assets.items():
        try:
            invoice_account = InvoiceAccount.objects.get(pk=invoice_account_id, invoice=invoice)

            asset_form = InvoiceAssetForm(data=asset)
            if asset_form.is_valid():
                fixed_asset = asset_form.save(commit=False)
                fixed_asset.company = invoice.company
                fixed_asset.status = FixedAssetStatus.PENDING
                fixed_asset.date_purchased = invoice.accounting_date
                fixed_asset.supplier = invoice.supplier.name
                fixed_asset.asset_number = str(invoice_account.invoice)
                fixed_asset.invoice_account = invoice_account
                fixed_asset.cost_price = invoice_account.amount
                fixed_asset.save()

                Comment.objects.create(
                    invoice=invoice_account.invoice,
                    log_type=Comment.EVENT,
                    participant=user,
                    role=role,
                    note='Fixed asset created',
                    status=invoice_account.invoice.status,
                    link_url=reverse('show_fixed_asset', kwargs={'pk': fixed_asset.id})
                )

                posted_object_items = asset.get('object_items')
                if len(posted_object_items) > 0:
                    for posted_object_item in posted_object_items:
                        if posted_object_item != '':
                            object_item = ObjectItem.objects.filter(id=posted_object_item).first()
                            if object_item:
                                fixed_asset.object_items.add(object_item)
            else:
                logger.info(f"Errors found creating asset for invoice account {invoice_account}")
        except InvoiceAccount.DoesNotExist:
            logger.exception("Invoice account does not exists")


def change_decline_status(invoice: Invoice, flow: Flow):
    order_num = flow.order_number - 1
    previous_flow = Flow.objects.filter(order_number=order_num, invoice=invoice, is_signed=False).first()
    if not previous_flow:
        previous_flow = Flow.objects.filter(order_number=order_num, invoice=invoice).first()

    if not previous_flow:
        raise InvoiceException('Previous flow not found, please refresh and try again')

    flow_order_num = flow.order_number + 1

    flow = Flow.objects.create(
        invoice=invoice,
        order_number=flow_order_num,
        status=FlowStatus.objects.get(slug='in-flow'),
        is_current=True
    )

    for flow_role in previous_flow.roles.all():
        FlowRole.objects.create(
            flow=flow,
            role=flow_role.role,
            status=FlowRoleStatus.PROCESSING.value
        )
    flow.adjust_order_numbers_after(invoice)


def add_supervisor_role(invoice: Invoice, invoice_flow: Flow, role: Role):
    supervisor_role = role.supervisor_role
    if not role.supervisor_role:
        supervisor_role = Role.objects.administrator().get(company=invoice.company)
    if not supervisor_role:
        raise InvoiceException('No supervisor role found')

    order_number = invoice_flow.order_number + 1
    flow = Flow.objects.create(
        invoice=invoice,
        order_number=order_number,
        status=FlowStatus.objects.get(slug='in-flow'),
        is_current=True
    )
    FlowRole.objects.create(
        flow=flow,
        role=supervisor_role,
        status=FlowRoleStatus.PROCESSING.value
    )
    flow.adjust_order_numbers_after(invoice)


class PurchaseOrderRequest(object):

    def __init__(self, invoice, profile, role):
        self.invoice = invoice
        self.profile = profile
        self.role = role

    def process_request(self):

        has_auto_approval = self.has_auto_approval()
        logger.info(f"This supplier {self.invoice.supplier}, role {self.role}, has auto approvals {has_auto_approval}")
        status = self.get_po_status(has_auto_approval)
        if status:
            self.invoice.status = status
        self.invoice.save()

        self.create_flows(has_auto_approval)

        self.invoice.log_activity(self.profile.user, self.role, 'Needs the purchase order Approved', Comment.EVENT)

    def get_po_status(self, is_auto_approved):
        if is_auto_approved:
            return Status.objects.get(slug='po-approved')
        else:
            return Status.objects.get(slug='po-request')

    def has_auto_approval(self):
        total_period_invoices_supplier = Invoice.objects.total_amount_by_supplier(self.invoice.supplier,
                                                                                  self.invoice.period
                                                                                  )

        supplier_auto_approvals = SupplierAuthorization.objects.prefetch_related(
            'role_authorizations'
        ).filter(
            supplier=self.invoice.supplier,
            amount__gt=total_period_invoices_supplier['total_value']
        ).first()

        if supplier_auto_approvals:
            logger.info("Supplier amount {} vs period amount {}".format(supplier_auto_approvals.amount,
                                                                        total_period_invoices_supplier['total_value']
                                                                        ))
        if supplier_auto_approvals and supplier_auto_approvals.amount > total_period_invoices_supplier['total_value']:
            logger.info(supplier_auto_approvals.amount)
            logger.info(supplier_auto_approvals.role_authorizations)
            role_authorization = supplier_auto_approvals.role_authorizations.filter(
                role=self.role,
                amount__gt=self.invoice.total_amount
            ).first()
            logger.info("Supplier role authorization {}".format(role_authorization))
            if role_authorization:
                return True
        return False

    def create_flows(self, has_auto_approval):
        # Create 3 flow levels in linked to this document
        counter = self.invoice.flows.count()
        if self.role:
            counter += 1
            flow_status = FlowStatus.objects.get(slug='processed')
            InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, True, self.profile)

            if not self.role.supervisor_role:
                logger.info("No supervisor role ")
                flow_status = FlowStatus.objects.get(slug='processing')
                administrator_role = Role.objects.get(
                    slug='administrator',
                    company=self.invoice.company
                )
                if administrator_role:
                    counter += 1
                    InvoiceFlow.create_flow_role(self.invoice, administrator_role, counter, flow_status, False, None, True)
                    if has_auto_approval:
                        flow_status = FlowStatus.objects.filter(slug='in-flow').first()
                    else:
                        flow_status = FlowStatus.objects.filter(slug='processing').first()
                    counter += 1
                    InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, False)
                else:
                    is_signed = False
                    if has_auto_approval:
                        flow_status = FlowStatus.objects.filter(slug='processed').first()
                        is_signed = True
                    else:
                        flow_status = FlowStatus.objects.get(slug='processing')

                    counter += 1
                    InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, is_signed, None, True)
            else:
                logger.info("Has supervisor role {} ".format(self.role.supervisor_role))
                is_signed = False
                is_current = True
                next_role_flow_status = 'in-flow'
                if has_auto_approval:
                    logger.info("Auto approving the supervisor role --> {}".format(self.role.supervisor_role))
                    flow_status = FlowStatus.objects.get(slug='processed')
                    self.invoice.log_activity(self.profile.user, self.role, 'Approved by system', Comment.EVENT)
                    is_signed = True
                    next_role_flow_status = 'processing'
                    is_current = False
                else:
                    flow_status = FlowStatus.objects.get(slug='processing')
                counter += 1

                InvoiceFlow.create_flow_role(self.invoice, self.role.supervisor_role, counter, flow_status, is_signed,
                                             None, is_current)

                is_current = False
                if has_auto_approval:
                    logger.info("Supervisor has auto approval, next role status is -> (processing) ")
                    flow_status = FlowStatus.objects.get(slug='processing')
                    is_current = True
                else:
                    logger.info("Supervisor has no approval, next role status is -> (in-flow) ")
                    flow_status = FlowStatus.objects.get(slug='in-flow')
                counter += 1
                InvoiceFlow.create_flow_role(self.invoice, self.role, counter, flow_status, False, None, is_current)


class PayrollImportException(Exception):
    pass


class ImportPayrollJob(object):

    def __init__(self, invoice, file, comment, profile, role):
        self.invoice = invoice
        self.role = role
        self.profile = profile
        self.file = file
        self.comment = comment

    def execute(self):
        employees = payroll_upload_client.get_upload_data(file=self.file, skip=3)
        data = []
        counter = 0

        column_keys = {}
        for cc, cc_value in employees[0].items():
            if cc >= 5:
                val = str(cc_value).split('-')
                column_keys[cc] = val[0].strip()

        for count, asset_row in employees.items():
            data_dict = {
                'code': '', 'name': '', 'from_period': None, 'to_period': None
            }
            can_add = True if count > 0 else False
            for c, cell_value in asset_row.items():
                if c == 1:
                    val = get_cell_value(cell_value)
                    if val is None or val == '':
                        can_add = False
                    data_dict['code'] = str(val)
                if c == 2:
                    val = get_cell_value(cell_value)
                    if val is None or val == '':
                        can_add = False
                    data_dict['name'] = str(val)
                if c == 3:
                    val = get_cell_value(cell_value)
                    if val is None or val == '':
                        can_add = False
                    data_dict['from_period'] = str(val)
                if c == 4:
                    val = get_cell_value(cell_value)
                    if val is None or val == '':
                        can_add = False
                    data_dict['to_period'] = str(val)
                if c >= 5:
                    data_dict[column_keys[c]] = get_cell_value(cell_value)
            if can_add:
                counter += 1
                data.append(data_dict)
        return self._sync(data)

    def get_payroll_posting_setup(self):
        linkup_codes = {}
        for linkup_code in LinkingCode.objects.select_related('account').filter(setup__supplier__company=self.invoice.company):
            linkup_codes[linkup_code.code] = linkup_code
        return linkup_codes

    def get_statutory_setup(self):
        setups = {}
        for statutory_setup in StatutorySetup.objects.select_related('account').filter(company=self.invoice.company):
            setups[statutory_setup.code] = statutory_setup
        return setups

    def _sync(self, payroll_lines):
        """
        Sync payroll posting .
        """
        linkup_codes = self.get_payroll_posting_setup()
        statutory_setups = self.get_statutory_setup()
        data = self._prepare_data(payroll_lines=payroll_lines, linkup_codes=linkup_codes, statutory_setups=statutory_setups)

        logger.info(self.missing_data)
        if data:
            self.create_invoice_accounts(invoice_accounts_data=data)
        filename = None
        # if self.missing_data:
        #     filename = self.create_missing_data_report()
        return len(data), self.missing_data, filename

    # noinspection PyMethodMayBeStatic
    def _prepare_data(self, payroll_lines, linkup_codes, statutory_setups):
        """
        Prepares fixed assets data for storing in DB.
        """
        account_postings = {}
        self.missing_data = []
        codes = [linkup_code.code for c, linkup_code in linkup_codes.items() if linkup_code.description == 'UIF']
        uif_code = codes[0] if codes else None

        for payroll_line in payroll_lines or []:
            income_total = payroll_line.get('Income Total', 0)
            uif_amount = payroll_line.get(uif_code, 0)
            total_percentage = 0
            for field, value in payroll_line.items():
                linkup_code = linkup_codes.get(field)
                if linkup_code and value:
                    percentage_of_total_income = 0
                    multiplier = -1
                    if linkup_code.is_income and income_total > 0:
                        percentage_of_total_income = round((value / income_total) * 100, 2)
                        multiplier = 1

                    uif = 0
                    if uif_amount > 0 and percentage_of_total_income > 0:
                        uif = round((percentage_of_total_income / 100) * uif_amount, 2)
                        value += uif

                    value *= multiplier
                    suffix = linkup_code.description
                    account = linkup_code.account
                    statutory_setup = statutory_setups.get(linkup_code.code)

                    if statutory_setup and statutory_setup.is_included_in_salary_cost:
                        amount = value * Decimal((statutory_setup.company_cost / 100))
                        account = statutory_setup.account
                    else:
                        amount = value
                    if linkup_code.is_suffix:
                        suffix = payroll_line['name']
                    link_key = f"{linkup_code.code}_linkup_code_{suffix}"
                    account_postings[link_key] = {
                        'account': account,
                        'amount': amount,
                        'suffix': suffix,
                        'reinvoice': False,
                        'comment': self.comment,
                        # 'total_income': income_total,
                        # 'is_income': linkup_code.is_income,
                        # 'percentage_of_total_income': percentage_of_total_income,
                        # 'uif_amount': uif
                    }
                    total_percentage += percentage_of_total_income
            total_percentage = round(total_percentage)
            if total_percentage != 100:
                raise PayrollImportException(f'Income {income_total} percentages {total_percentage} did not add up to 100%')
        return account_postings

    def create_invoice_accounts(self, invoice_accounts_data):

        InvoiceAccount.objects.filter(invoice=self.invoice).delete()

        invoice_accounts_to_add = []
        for _, invoice_account_line in invoice_accounts_data.items():
            invoice_accounts_to_add.append(
                InvoiceAccount(
                    invoice=self.invoice,
                    profile=self.profile,
                    role=self.role,
                    **invoice_account_line,
                )
            )

        if invoice_accounts_to_add:
            InvoiceAccount.objects.bulk_create(invoice_accounts_to_add)

        log = Comment(
            invoice=self.invoice,
            log_type=Comment.COMMENT,
            participant=self.profile.user,
            role=self.role,
            note=self.comment
        )
        log.save()


def recalculate_posting_vat(invoice: Invoice, profile: Profile, role: Role):
    invoice_accounts_linked_to_assets = InvoiceAccount.objects.link_to_asset(invoice=invoice).all()

    if len(invoice_accounts_linked_to_assets) == 0:
        return

    vat_invoice_account = InvoiceAccount.objects.filter(invoice=invoice, is_vat=True, vat_code__is_capital=False).first()
    if not vat_invoice_account:
        return

    capital_input_vat = VatCode.objects.input().filter(company=invoice.company, is_capital=True).first()
    if not capital_input_vat:
        return

    is_credit_note = invoice.invoice_type.is_credit
    vat_amount = 0
    vat_invoice_account_amount = invoice.vat_amount
    for invoice_account in invoice_accounts_linked_to_assets:
        if capital_input_vat and capital_input_vat.percentage != 0:
            vat_amount += Decimal(Decimal(capital_input_vat.percentage / 100) * invoice_account.amount).quantize(Decimal('.01'))

    vat_amount = vat_amount * -1 if is_credit_note else vat_amount
    vat_invoice_account_amount = vat_invoice_account_amount * -1 if is_credit_note else vat_invoice_account_amount

    if vat_invoice_account:
        if vat_amount > vat_invoice_account_amount:
            vat_outstanding = 0
            vat_amount = vat_invoice_account_amount
        else:
            vat_outstanding = vat_invoice_account_amount - vat_amount

        vat_outstanding = vat_outstanding * -1 if is_credit_note else vat_outstanding
        vat_invoice_account.amount = vat_outstanding
        vat_invoice_account.percentage = InvoiceAccount.calculate_account_percentage(invoice=invoice, amount=vat_outstanding, is_vat=True)
        vat_invoice_account.save()

        if vat_invoice_account.amount == 0:
            vat_invoice_account.delete()

    if vat_amount != 0:
        vat_amount = vat_amount * -1 if is_credit_note else vat_amount
        InvoiceAccount.objects.update_or_create(
            invoice=invoice,
            account=capital_input_vat.account,
            vat_code=capital_input_vat,
            defaults={
                'amount': vat_amount,
                'role': role,
                'profile': profile,
                'is_vat': True,
                'is_posted': True,
                'percentage': InvoiceAccount.calculate_account_percentage(invoice=invoice, amount=vat_amount, is_vat=True)
            }
        )


def reverse_vat_journal(invoice: Invoice, profile: Profile, role: Role):
    # For invoices that have Capital Vat posted, lets reverse the original invoice vat line and add the Capital and Vat lines
    vat_invoice_account = InvoiceAccount.objects.filter(invoice=invoice, is_vat=True).first()
    if not vat_invoice_account:
        return

    capital_input_vat = VatCode.objects.input().filter(company=invoice.company, is_capital=True).first()
    if not capital_input_vat:
        return

    vat_amount = 0
    vat_invoice_account_amount = invoice.vat_amount
    for invoice_account in InvoiceAccount.objects.link_to_asset(invoice=invoice):
        if capital_input_vat and capital_input_vat.percentage != 0:
            vat_amount += Decimal(Decimal(capital_input_vat.percentage / 100) * invoice_account.amount).quantize(Decimal('.01'))

    if vat_invoice_account:
        if vat_amount > vat_invoice_account_amount:
            vat_outstanding = 0
            vat_amount = vat_invoice_account_amount
        else:
            vat_outstanding = vat_invoice_account_amount - vat_amount

        vat_invoice_account.amount = vat_outstanding
        vat_invoice_account.percentage = InvoiceAccount.calculate_account_percentage(invoice=invoice, amount=vat_outstanding, is_vat=True)
        vat_invoice_account.save()

        if vat_invoice_account.amount == 0:
            vat_invoice_account.delete()

    InvoiceAccount.objects.update_or_create(
        invoice=invoice,
        account=capital_input_vat.account,
        vat_code=capital_input_vat,
        defaults={
            'amount': vat_amount,
            'role': role,
            'profile': profile,
            'is_vat': True,
            'percentage': InvoiceAccount.calculate_account_percentage(invoice=invoice, amount=vat_amount, is_vat=True)
        }
    )


# def sign_invoice(invoice: Invoice, flow: Flow, profile: Profile, role: Role):
#
#     sign = SignInvoice(invoice=invoice, profile=profile, role=role)
#     sign.process_signing()
#
#     if flow and flow.intervention_at:
#         invoice.log_activity(user=self.request.user, role=role, comment='Intervention signed off', log_type=Comment.EVENT)
#
#     return next_invoice_id, next_page

