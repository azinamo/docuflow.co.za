import os
import re
import imaplib
import logging
from email import policy
from email.parser import BytesParser
from socket import gaierror

from django.conf import settings

from docuflow.apps.sources.models import IMAPEmail
from .exceptions import MailFetcherError
from .utils import clean_title

logger = logging.getLogger(__name__)


class MailFetchClient(object):

    def is_fetch_all(self):
        if settings.IS_TEST is True and settings.DEBUG is True:
            return False
        return False

    def get_message_parts(self, imap_session, message_id):
        typ, message_parts = imap_session.fetch(message_id, '(RFC822)')
        if typ != 'OK':
            raise MailFetcherError('Error fetching email')
        return message_parts

    def get_mail_message(self, message_parts):
        message = BytesParser(policy=policy.default).parsebytes(message_parts[0][1])
        return message

    def fetch_emails(self, source: IMAPEmail, inbox='INBOX'):
        try:
            imap_session = imaplib.IMAP4_SSL(source.host, source.port)
            logger.warn(f'Trying on {source.host}:{source.port} - {source.username.strip()}:{source.password.strip()}')
            typ, account_details = imap_session.login(source.username.strip(), source.password.strip())

            if typ != 'OK':
                raise MailFetcherError('Not able to sign in')

            inbox = source.mailbox or 'INBOX'

            imap_session.select(inbox)

            if self.is_fetch_all():
                typ, data = imap_session.search(None, 'NOT', 'DELETED')
            else:
                typ, data = imap_session.search(None, '(UNSEEN)')

            if typ != 'OK':
                raise MailFetcherError(f'Error searching Inbox -> {inbox}')

            if len(data) == 0:
                raise MailFetcherError(f'No emails found in {inbox}')

            mails = data[0].split()
            for msgId in mails:
                yield self.get_message_parts(imap_session, msgId)

            imap_session.close()
            imap_session.logout()

            mails = self.prepare_mails(mails=mails)

        except gaierror as exc:
            logger.warn(f'gethostbyname(socket.getfqdn()) failed... trying on {source.host}:{source.port}')
            logger.exception(exc)
            raise MailFetcherError(f"Connection to email source {source} failed. {exc}")
        except (ConnectionRefusedError, imaplib.IMAP4.error) as exc:
            logger.exception(exc)
            raise MailFetcherError(f"Connection to email source {source} failed. {exc}")

    def prepare_mails(self, mails):
        emails = []
        for mail_message in mails:
            mail_detail = {}
            message = self.get_mail_message(message_parts=mail_message)

            consume_dir = os.path.join(settings.CONSUMPTION_DIR)

            attachments = []
            for part in message.walk():
                attachment = self.process_message_part(part=part)
                if not attachment:
                    continue

                mail_detail['attachments'].append(attachment)
                mail_detail.setdefault('from', clean_title(title=message["From"]))

                response = self.save_file(
                    tmp_dir=consume_dir,
                    filename=attachment['filename'],
                    data=attachment['data']
                )

                file_path = response.get('file_path')
                if not file_path:
                    continue

                emails.append({
                    'subject': message['Subject'],
                    'filename': attachment['filename'],
                    'size': len(attachment['data']),
                    'on_disk': False,
                    'attachment_type': attachment['content_type'],
                    'location': file_path
                })

        return emails

    def process_message_part(self, part):
        content_type = part.get_content_type()
        filename = part.get_filename()
        disposition = part.get('Content-Disposition', 'none')
        mail_attachment = {}

        if self.is_valid_file_type(disposition, content_type) and bool(filename):
            file_data = part.get_payload(decode=True)

            mail_attachment = {
                'filename': filename,
                'data': file_data,
                'content_type': content_type
            }

        return mail_attachment

    # noinspection PyMethodMayBeStatic
    def is_valid_file_type(self, disposition, content_type):
        safe_suffix_regex = re.compile(r"^(application/(pdf|octet-stream))|(image/(png|jpg|jpeg|gif|tiff))$")

        if disposition.startswith('attachment'):
            m = safe_suffix_regex.match(content_type)
            if m:
                return True
        else:
            safe_suffix_regex = re.compile(r"^(application/(pdf|octet-stream))$")
            m = safe_suffix_regex.match(content_type)
            if m:
                return True
        return False

    # noinspection PyMethodMayBeStatic
    def save_file(self, tmp_dir, filename, data):
        logger.info(f"----Save file  {filename} --- to {tmp_dir}")

        file_path = os.path.join(tmp_dir, filename)
        with open(file_path, 'wb') as fp:
            fp.write(data)
        return {'file_path': file_path, 'filename': filename}


mail_fetch_client = MailFetchClient()
