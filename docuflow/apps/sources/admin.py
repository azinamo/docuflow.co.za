from django.contrib import admin

from . import models


@admin.register(models.IMAPEmail)
class ImapEmailAdmin(admin.ModelAdmin):
    list_display = ('label', 'mailbox', 'host', 'ssl', 'port', 'username', 'password', 'interval', 'company', 'enabled')
    fieldsets = [('Basic Information', {'fields': ['label', 'mailbox', 'host', 'ssl', 'port', 'username', 'password',
                                                   'company', 'enabled', 'interval']})
                 ]

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        if obj and obj.company:
            return True
        return False
