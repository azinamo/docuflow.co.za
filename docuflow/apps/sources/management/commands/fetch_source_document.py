from django.core.management import BaseCommand
from django.core.mail import send_mail

from docuflow.apps.sources.models import IMAPEmail
from docuflow.apps.sources.tasks import ocr_company_invoices


class Command(BaseCommand):
    help = "Pull email from the sources"

    def handle(self, *args, **options):
        sources = IMAPEmail.objects.filter(company__slug='41054')
        counter = 0
        for source in sources:
            if source.enabled:
               source.check_source()
               ocr_company_invoices.delay(source.company.id)
               counter += 1

        subject = 'Company invoices sources {}'.format(counter)
        from_addr = 'no-reply@docuflow.co.za'
        recipient_list = ('admire@localhost.co.za', 'admire@originate.co.za',)
        message = 'Done checking the sources, to get the invoices'
        send_mail(subject, message, from_addr, recipient_list)

        self.stdout.write(self.style.SUCCESS("Done pulling the emails"))
