from django.core.management import BaseCommand, CommandError
from django.core.mail import send_mail

from docuflow.apps.sources.models import IMAPEmail


class Command(BaseCommand):
    help = "Pull email from the imap emails"

    @staticmethod
    def notify_success(response):
        try:
            subject = 'Docuflow cron fetching the emails'
            from_addr = 'cron@docuflow.co.za'
            recipient_list = ('azinamo@gmail.com', 'admire@originate.co.za',)
            message = 'Docuflow Email fetching process completed successfully now'

            send_mail(subject, message, from_addr, recipient_list)
        except Exception as e:
            print("error occurred send the email {}".format(e.__str__()))

    def handle(self, *args, **options):
        slug = '41054'
        sources = IMAPEmail.objects.select_related('company').filter(company__slug=slug)
        counter = 0
        response = {}
        try:
            for source in sources:
                inbox = 'INBOX'
                print("FETCHING FROM company {} from folder --> {} ".format(source.company, inbox))
                if source.enabled:
                    print("Source _{}_ --> {} is enabled {} -> {}".format(source.label, source.host, source.enabled,
                                                                          source.password))
                    imap_session = source.get_connection()
                    if imap_session:
                        response = source.legacy_august_fetch(inbox)
                        # response = source.legacy_fetch(inbox)
                        response = source.check_source(inbox, False)
        except Exception as exception:
            print("Exception --> {}".format(exception))

        # self.notify_success()
        self.stdout.write(self.style.SUCCESS("Done pulling the emails"))
