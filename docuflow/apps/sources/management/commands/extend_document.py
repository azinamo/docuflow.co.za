from __future__ import unicode_literals
import os
import hashlib
from shutil import copyfile

from django.core.management.base import BaseCommand
from django.conf import settings

from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Invoice, Journal
from docuflow.apps.period.models import Year
from docuflow.apps.sources.utils import MailFetcher

from annoying.functions import get_object_or_None


class Command(BaseCommand):
    help = "Move the invoices documents(pdf) to correct folders if they fail on fetch from email"

    def add_arguments(self, parser):
        parser.add_argument('--company', nargs='+', help='Company')

    def handle(self, *args, **kwargs):
        mail_fetcher = MailFetcher()
        qs = Company.objects.filter(is_master=False)
        slugs = []
        if kwargs.get('company'):
            slugs.append(kwargs.get('company'))
        consume_dir = os.path.join(settings.CONSUMPTION_DIR)

        if slugs:
            qs = qs.filter(slug__in=slugs)
        for company in qs:
            year = Year.objects.started().filter(company=company).first()
            print(f"Company {company}({company.slug})  running year {year}")
            if not year:
                continue
            for invoice in Invoice.objects.get_at_logger(company=company, year=year):
                print(f"Invoice {invoice} in year {invoice.period.period_year}")
                mail_fetcher.rename_invoice_document(
                    invoice=invoice,
                    original_dir=consume_dir,
                    filename=invoice.filename
                )

