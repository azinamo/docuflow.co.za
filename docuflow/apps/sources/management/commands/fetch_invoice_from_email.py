import pprint
import logging
from time import sleep

from django.core.management import BaseCommand
from django.core.mail import send_mail

from docuflow.apps.sources.models import IMAPEmail
from docuflow.apps.sources.utils import MailFetcher
from docuflow.apps.sources.exceptions import MailFetcherError

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Pull email from the sources"

    def add_arguments(self, parser):
        parser.add_argument('--company_id', nargs='+', help='Company to fetch emails for')
        parser.add_argument('--source_id', nargs='+', help='Email source to fetch emails from')

    def generate_html(self, response):
        table_str = []
        table_str.append("<table>")
        table_str.append("<tr>")
        table_str.append("<th>Company</th>")
        table_str.append("<th>Checksum</th>")
        table_str.append("<th>Message</th>")
        table_str.append("<th>Invoice</th>")
        table_str.append("<th>Location</th>")
        table_str.append("<tr>")
        for k, res in response.items():
            mails = res.get('mails', [])
            for mail in mails:
                mail_attachments = mail.get('attachments', [])
                for attachment in mail_attachments:
                    table_str.append("<tr>")
                    table_str.append("<td>{}</td>".format(attachment.get('company')))
                    table_str.append("<td>{}</td>".format(attachment.get('checksum')))
                    table_str.append("<td>{}</td>".format(attachment.get('message')))
                    table_str.append("<td>{}</td>".format(attachment.get('invoice')))
                    table_str.append("<td>{}</td>".format(attachment.get('location')))
                    table_str.append("<tr>")
        table_str.append("<table>")
        return " ".join(table_str)

    def notify_success(self, response):
        subject = 'Docuflow cron fetching the emails'
        from_addr = 'cron@docuflow.co.za'
        recipient_list = ('azinamo@gmail.com', 'admire@originate.co.za',)
        response_table = self.generate_html(response)
        message = f'Docuflow Email fetching process completed successfully now\r\n .{response_table}'

        send_mail(subject, message, from_addr, recipient_list, html_message=message)

    def handle(self, *args, **options):
        qs = IMAPEmail.objects.select_related('company').filter(enabled=True)
        if options.get('company_id'):
            qs = qs.filter(company_id=options['company_id'])
        if options.get('source_id'):
            qs = qs.filter(pk=options['source_id'])
        sources = qs.all()

        job = MailFetcher()
        while True:
            counter = 0
            sleep_time = 5
            for source in sources:
                try:
                    logger.info(f'Fetching from {source}....')
                    job.execute(source=source)
                except MailFetcherError as exc:
                    logger.info(exc)
                counter += 1
                logger.info('\r\nSleeping....')
                sleep(sleep_time)
                logger.info('.... Waking up\r\n')
            self.stdout.write(self.style.SUCCESS("Done pulling the emails"))
