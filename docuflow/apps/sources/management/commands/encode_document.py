from __future__ import unicode_literals
import os
import hashlib
from shutil import copyfile

from django.core.management.base import BaseCommand
from django.conf import settings

from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Invoice, Journal, Distribution


from annoying.functions import get_object_or_None


class Command(BaseCommand):
    help = "Clear the company invoices, to restart the imports"

    def handle(self, *args, **kwargs):
        try:
            slugs = ['41054']
            companies = Company.objects.filter(slug__in=slugs)
            for company in companies:
                print("----------START ENCODING FOR  {} -----------".format(company))
                invoices = Invoice.objects.prefetch_related('flows').filter(company__slug=company.slug)

                company_dir = os.path.join(settings.MEDIA_ROOT, company.slug)
                tmp_dir = os.path.join(company_dir, 'tmp')
                print("\t\t\t----------------- LISTING THE FILES FILES NOW  from {}-------------".format(tmp_dir))

                if os.path.exists(tmp_dir):
                    for the_file in os.listdir(tmp_dir):
                        file_path = os.path.join(tmp_dir, the_file)
                        try:
                            if os.path.isfile(file_path):
                                extension = os.path.splitext(the_file)[1].lower()
                                print('Extension of file is {}'.format(extension))

                                filename = "{}{}".format(hashlib.md5(the_file.encode()).hexdigest(), extension)
                                print("{} --> Filename is {}".format(the_file, filename))
                                invoice = get_object_or_None(Invoice, filename=filename)
                                if invoice:
                                    print("We found and invoice {} --< {}that is supposed to be "
                                          "linked".format(invoice, invoice))

                                _filename = hashlib.md5(filename.encode()).hexdigest()
                                decrypted_file = "{}{}".format(_filename, extension)
                                print("{} --> Descrypted file  is {}".format(_filename, decrypted_file))

                                _invoice = get_object_or_None(Invoice, filename=decrypted_file)
                                if _invoice:
                                    print("We found and invoice {} --< {} from decrypted filename that is supposed "
                                          "to be linked".format(_invoice, _invoice))

                                for __invoice__ in invoices:

                                    __invoice_filename__ = "{}_{}".format(__invoice__.id, filename)
                                    __invoice_decrypted_filename__ = "{}_{}".format(__invoice__.id, decrypted_file)

                                    if __invoice__.filename == __invoice_filename__:
                                        print("FOUND --> UNDECRYPTED")
                                    elif __invoice_decrypted_filename__ == __invoice__.filename:
                                        print("FOUND --> DECRYPTED")
                                        __invoice__id__ = "{}".format(__invoice__.id)
                                        _invoice_dir = os.path.join(company_dir, __invoice__id__)
                                        self.create_dir(_invoice_dir)
                                        _destination = os.path.join(_invoice_dir, __invoice_decrypted_filename__)
                                        copyfile(file_path, _destination)

                                for __invoice in invoices:

                                    _invoice_filename = "{}_{}".format(__invoice.id, filename)
                                    _invoice_decrypted_filename = "{}_{}".format(__invoice.id, filename)

                                    if __invoice.filename == _invoice_filename:
                                        print("FOUND --> UNDECRYPTED")
                                    elif _invoice_decrypted_filename == __invoice.filename:
                                        print("FOUND --> DECRYPTED")
                                        _invoice_id_ = "".format(__invoice.id)
                                        invoice_dir = os.path.join(company_dir, __invoice.id)
                                        self.create_dir(invoice_dir)
                                        destination = os.path.join(invoice_dir, _invoice_decrypted_filename)
                                        copyfile(file_path, destination)

                                print("-------------")
                        except Exception as e:
                            print(e)

        except Exception as exception:
            print("Exception occurred {} ".format(exception))

    def create_dir(self, dir):
        if not os.path.exists(dir):
            os.mkdir(dir)
        return dir
