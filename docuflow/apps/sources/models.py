from __future__ import unicode_literals

import datetime
import imaplib
import logging
import poplib
import pprint
import sys
import uuid
from email.header import decode_header
from socket import gaierror

from dateutil import parser
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as __
from jsonfield import JSONField

from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import Invoice, Status, InvoiceType
from docuflow.apps.supplier.models import Supplier

logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=4)

sys.setrecursionlimit(2500)


class MailFetcherError(Exception):
    pass


class InvalidMessageError(Exception):
    pass


class Loggable(object):

    def __init__(self, group=None):
        self.logger = logging.getLogger(__name__)
        self.logging_group = group or uuid.uuid4()

    def log(self, level, message):
        getattr(self.logger, level)(message, extra={
            "group": self.logging_group
        })


class Source(models.Model):
    label = models.CharField(max_length=64)
    enabled = models.BooleanField(default=True)
    company = models.ForeignKey(Company, null=True, blank=True, related_name='company_sources', on_delete=models.CASCADE)

    def __self__(self):
        return self.label

    class Meta:
        ordering = ('label',)
        verbose_name = __('Source')
        permissions = (
            ('process_email_source', 'Process email source'),
        )


class EmailBaseModel(Source):
    """
    POP3 email and IMAP email sources are non-interactive sources that
    periodically fetch emails from an email account using either the POP3 or
    IMAP email protocol. These sources are useful when users need to scan
    documents outside their office, they can photograph a paper document with
    their phones and send the image to a designated email that is setup as a
    Mayan POP3 or IMAP source. Mayan will periodically download the emails
    and process them as Mayan documents.
    """
    SOURCE_UNCOMPRESS_CHOICES = (
        ('y', __('Always')),
        ('n', __('Never')),
    )
    host = models.CharField(max_length=128, verbose_name=__('Host'))
    ssl = models.BooleanField(default=True, verbose_name=__('SSL'))
    port = models.PositiveIntegerField(blank=True, null=True, help_text=__(
        'Typical choices are 110 for POP3, 995 for POP3 over SSL,'
        '143 for IMAP, 993 for IMAP over SSL '
    ))
    username = models.CharField(max_length=96, verbose_name=__('Username'))
    password = models.CharField(max_length=96, verbose_name=__('Password'))
    interval = models.PositiveIntegerField(
        default=60, help_text=__('Interval in seconds between checks for new documents.'), verbose_name=__('Interval')
    )
    uncompress = models.CharField(
        choices=SOURCE_UNCOMPRESS_CHOICES, help_text=__('Whether to expand or not, compressed archives.'),
        max_length=1, verbose_name=__('Uncompress'), default='y'
    )
    time = None

    def _set_time(self, message):
        self.time = datetime.datetime.now()
        message_time = message.get("Date")
        if message_time:
            try:
                self.time = parser.parse(message_time)
            except (ValueError, AttributeError):
                pass  # We assume that "now" is ok

    @staticmethod
    def getheader(header_text, default='ascii'):

        headers = decode_header(header_text)
        header_sections = [
            (text, charset or default) for text, charset in headers
        ]
        return ''.join(header_sections)

    class Meta:
        verbose_name = __('Email source')


class POP3Email(EmailBaseModel):
    source_type = 'pop3'

    timeout = models.PositiveIntegerField(default=60, verbose_name=__('Timeout'))

    # noinspection PyMethodMayBeStatic
    def get_absolute_url(self):
        return reverse('pop3-sources-index')

    def test_connection(self):
        logger.debug('Starting POP3 email fetch')
        logger.debug(f'host: {self.host}')
        logger.debug(f'ssl: {self.ssl}')

        response_dict = {}
        try:
            if self.ssl:
                mailbox = poplib.POP3_SSL(self.host, self.port)
            else:
                mailbox = poplib.POP3(self.host, self.port, timeout=self.timeout)

            mailbox.getwelcome()
            mailbox.user(self.username)
            mailbox.pass_(self.password)
            messages_info = mailbox.list()

            mailbox.quit()
            response_dict.update({'error': False, 'message': messages_info})
            return messages_info
        except (ConnectionRefusedError, gaierror) as exc:
            logger.exception(exc)
            response_dict.update({'error': True, 'message': str(exc)})

        return response_dict

    class Meta:
        verbose_name = __('POP email')
        verbose_name_plural = __('POP Email')


class IMAPEmail(EmailBaseModel):
    source_type = 'imap'

    mailbox = models.CharField(default='INBOX', help_text=__('IMAP Mailbox from which to check for messages.'),
                               max_length=64, verbose_name=__('Mailbox'))

    class Meta:
        verbose_name = __('Email Source')
        verbose_name_plural = __('Email Sources')

    def __str__(self):
        return f"{self.host}"

    def __repr__(self):
        return f"{self.host}:://{self.username}@{self.password}"

    def get_absolute_url(self):
        return reverse('imap-sources-index')

    def test_connection(self):
        logger.debug('Starting Imap test email connection')
        logger.debug('host: %', self.host)
        logger.debug('ssl: %', self.ssl)

        if self.ssl:
            mailbox = imaplib.IMAP4_SSL(self.host, self.port)
        else:
            mailbox = imaplib.IMAP4(self.host, self.port)

        login = mailbox.login(self.username, self.password)
        if not login[0] == "OK":
            raise MailFetcherError(f"Can't log into mail: {login[1]}")

        inbox = mailbox.select(self.mailbox)
        if not inbox[0] == "OK":
            raise MailFetcherError(f"Can't find the inbox: {inbox[1]}")

        return True


class WatchFolderSource(Source):
    """
    The watch folder is another non-interactive source that like the email
    source, works by periodically checking and processing documents. This
    source instead of using an email account, monitors a filesystem folder.
    Administrators can define watch folders, examples /home/mayan/watch_bills
    or /home/mayan/watch_invoices and users just need to copy the documents
    they want to upload as a bill or invoice to the respective filesystem
    folder. Mayan will periodically scan these filesystem locations and
    upload the files as documents, deleting them if configured.
    """
    is_interactive = False
    source_type = 'watch'
    SOURCE_UNCOMPRESS_CHOICES = (
        ('y', __('Always')),
        ('n', __('Never')),
    )

    interval = models.PositiveIntegerField(
        default=60, help_text=__('Interval in seconds between checks for new documents.'),  verbose_name=__('Interval')
    )
    folder_path = models.CharField(
        help_text=__('Server side filesystem path.'), max_length=255, verbose_name=__('Folder path')
    )
    uncompress = models.CharField(
        choices=SOURCE_UNCOMPRESS_CHOICES, help_text=__('Whether to expand or not, compressed archives.'),
        max_length=1, verbose_name=__('Uncompress'), default='y'
    )

    def get_absolute_url(self):
        return reverse('watch-sources-index')

    class Meta:
        verbose_name = __('Watch folder')


class SourceLog(models.Model):
    source = models.ForeignKey(Source, related_name='logs', verbose_name=__('Source'), on_delete=models.DO_NOTHING)
    datetime = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=__('Date time'), blank=True)
    message = models.TextField(blank=True, editable=False, verbose_name=__('Message'))
    response = JSONField(blank=True, null=True, editable=False, verbose_name=__('Response'))

    class Meta:
        get_latest_by = 'datetime'
        ordering = ('-datetime',)
        verbose_name = __('Log entry')
