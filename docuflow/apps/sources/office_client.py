import logging
import os
import uuid

from O365 import Account
from django.conf import settings

from docuflow.apps.sources.models import IMAPEmail
from .exceptions import MailFetcherError

logger = logging.getLogger(__name__)


class MailFetchClient(object):

    def is_fetch_all(self):
        if settings.IS_TEST is True and settings.DEBUG is True:
            return False
        return False

    # https://www.dataandstuff.co.uk/post/automating-office-365-email-attachment-downloads-with-python
    def fetch_emails(self, source: IMAPEmail):
        credentials = (settings.OFFICE_CLIENT_ID, settings.OFFICE_SECRET_VALUE)
        # account = Account(credentials=credentials)
        account = Account(credentials, auth_flow_type='credentials', tenant_id=settings.OFFICE_TENANT_ID)
        if not account.authenticate():
            raise MailFetcherError('Could not authenticate')

        mailbox = account.mailbox(settings.OFFICE_MAILBOX)
        query = mailbox.new_query().on_attribute('isRead').equals(False)  # get unread
        inbox_folder = mailbox.inbox_folder()
        #
        consume_dir = os.path.join(settings.CONSUMPTION_DIR)
        emails = []
        for message in inbox_folder.get_messages(download_attachments=True, query=query):
            if message.has_attachments:
                for attachment in message.attachments:
                    filename = f"{uuid.uuid4()}.pdf"
                    attachment_name = str(attachment.name).lower()
                    if '.pdf' in attachment_name and attachment.save(consume_dir, filename):
                        emails.append({
                            'name': attachment.name,
                            'filename': filename,
                            'subject': message.subject,
                            'size': attachment.size,
                            'on_disk': attachment.on_disk,
                            'attachment_type': attachment.attachment_type,
                            'location': os.path.join(consume_dir, filename)
                        })
            message.mark_as_read()
        return emails


mail_fetch_client = MailFetchClient()
