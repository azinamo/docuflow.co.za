from django.apps import AppConfig


class SourcesConfig(AppConfig):
    name = 'docuflow.apps.sources'
