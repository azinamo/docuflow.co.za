from django.urls import path

from . import views

urlpatterns = [
    path('imap/', views.ImapIndexView.as_view(), name='imap-sources-index'),
    path('imap/create/', views.ImapCreateSourceView.as_view(), name='imap-create-source'),
    path('imap/<int:pk>/edit/', views.ImapEditSourceView.as_view(), name='imap-edit-source'),
    path('imap/<int:pk>/delete/', views.ImapDeleteSourceView.as_view(), name='imap-delete-source'),
    path('imap/<int:pk>/fetch/', views.CheckSourceView.as_view(), name='imap-fetch-email'),
    path('imap/<int:pk>/test/connection/', views.ImapTestConnectionView.as_view(), name='image-test-email-connection'),

    path('watch/', views.WatchIndexView.as_view(), name='watch-sources-index'),
    path('watch/<int:pk>/edit/', views.WatchEditSourceView.as_view(), name='watch-edit-source'),
    path('watch/<int:pk>/delete/', views.WatchDeleteSourceView.as_view(), name='watch-delete-source'),
    path('watch/create/', views.WatchCreateSourceView.as_view(), name='watch-create-source'),

    path('fetch/emails/', views.SourceFetchMailView.as_view(), name='fetch_emails'),

    path('master/<int:master_company_id>/imap/', views.ImapIndexView.as_view(), name='master_imap_sources_index'),
    path('master/<int:master_company_id>/imap/<int:pk>/edit/', views.ImapEditSourceView.as_view(), name='edit_master_imap_source'),
    path('master/<int:master_company_id>/imap/<int:pk>/delete/', views.ImapDeleteSourceView.as_view(), name='delete_master_imap_source'),
    path('master/<int:master_company_id>/imap/create/', views.ImapCreateSourceView.as_view(), name='create_master_imap_source'),
    path('logs/', views.SourceLogView.as_view(), name='fetch_email_log'),
    path('view/<int:pk>/logs/', views.ShowLogView.as_view(), name='show_email_log'),
]
