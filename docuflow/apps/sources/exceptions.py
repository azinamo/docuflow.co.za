class MailFetcherError(Exception):
    """
    Custom exceptions raised while fetching and processing emails from sources
    """
