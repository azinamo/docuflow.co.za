from __future__ import unicode_literals

import datetime
import hashlib
import imaplib
import logging
import os
import re
import subprocess
import time
import uuid
from base64 import b64decode
from email import policy
from email.parser import BytesParser
from email.utils import collapse_rfc2231_value

import img2pdf
from annoying.functions import get_object_or_None
from dateutil import parser
from django.conf import settings
from django.core.files import File
from django.core.mail import send_mail
from django.utils import timezone
from wand.image import Image

from docuflow.apps.invoice.models import Invoice, Status, InvoiceType
from docuflow.apps.supplier.models import Supplier

logger = logging.getLogger(__name__)


class MailFetcherError(Exception):
    pass


class InvalidMessageError(Exception):
    pass


class Loggable(object):

    def __init__(self, group=None):
        self.logger = logging.getLogger(__name__)
        self.logging_group = group or uuid.uuid4()

    def log(self, level, message):
        getattr(self.logger, level)(message, extra={
            "group": self.logging_group
        })


class PseudoFile(File):
    def __init__(self, file, name):
        self.name = name
        self.file = file
        self.file.seek(0, os.SEEK_END)
        self.size = self.file.tell()
        self.file.seek(0)


class SourceUploadedFile(File):
    def __init__(self, source, file, extra_data=None):
        self.file = file
        self.source = source
        self.extra_data = extra_data


class Attachment(object):

    SAFE_SUFFIX_REGEX = re.compile(
        r"^(application/(pdf|octet-stream))|(image/(png|jpeg|gif|tiff))$")

    def __init__(self, data, content_type):

        self.content_type = content_type
        self.data = data
        self.suffix = None

        m = self.SAFE_SUFFIX_REGEX.match(self.content_type)
        if not m:
            raise MailFetcherError("Not-awesome file type: {}".format(self.content_type))
        self.suffix = m.group(2) or m.group(4)

    def read(self):
        return self.data


class Message(Loggable):
    """
    A crude, but simple email message class.  We assume that there's a subject
    and n attachments, and that we don't care about the message body.
    """

    SECRET = 'DOCUFLOW_SHARED_SECRET'

    def __init__(self, data, group=None):
        """
        Cribbed heavily from
        https://www.ianlewis.org/en/parsing-email-attachments-python
        """

        Loggable.__init__(self, group=group)

        self.subject = None
        self.email_from = None
        self.time = None
        self.attachment = None
        self.raw_filename = None

        message = BytesParser(policy=policy.default).parsebytes(data)
        self.subject = str(message["Subject"]).replace("\r\n", "")
        self.body = str(message.get_body())
        self.email_from = str(message["From"]).replace("\r\n", "")

        self.check_subject()
        # self.check_body()

        self._set_time(message)

        self.log("info", 'Importing email: "{}"'.format(self.subject))

        attachments = []
        # print('Importing email: "{}"'.format(self.subject))
        counter = 0
        for part in message.walk():

            # content_disposition = part.get("Content-Disposition")
            #
            # if not content_disposition:
            #     continue
            #
            # dispositions = content_disposition.strip().split(";")
            #
            # if not dispositions[0].lower() == "attachment" and \
            #    "filename" not in dispositions[1].lower():
            #     continue
            disposition = part.get('Content-Disposition', 'none')

            if disposition.startswith('attachment'):
                raw_filename = part.get_filename()
                if raw_filename:
                    filename = collapse_rfc2231_value(raw_filename)
                else:
                    filename = 'attachment-{}'.format(counter)
                    counter += 1

                logger.debug('filename: %s', filename)

                print("Disposition {} and file name is {}".format(disposition, filename))

                file_data = part.get_payload()

                self.raw_filename = filename
                print("Raw filename {} and now filename is {}".format(raw_filename, filename))

                attachments.append(Attachment(
                    b64decode(file_data), content_type=part.get_content_type()))

        if len(attachments) == 0:
            raise InvalidMessageError(
                "There don't appear to be any attachments to this message")

        if len(attachments) > 1:
            raise InvalidMessageError(
                "There's more than one attachment to this message. It cannot "
                "be indexed automatically."
            )

        self.attachment = attachments[0]

    def __bool__(self):
        return bool(self.attachment)

    def check_subject(self):
        pass

    def _set_time(self, message):
        self.time = datetime.datetime.now()
        message_time = message.get("Date")
        if message_time:
            try:
                self.time = parser.parse(message_time)
            except (ValueError, AttributeError):
                pass  # We assume that "now" is ok

    @property
    def file_name(self):
        return self.raw_filename


class MailFetcher(Loggable):

    def __init__(self, Source):

        Loggable.__init__(self)

        self._connection = None
        self._host = Source.host
        self._port = Source.port
        self._username = Source.username
        self._password = Source.password
        self._inbox = 'INBOX'
        self._company = Source.company

        company_dir = self.create_dir(os.path.join(settings.MEDIA_ROOT, self.company.slug))
        tmp_dir = company_dir
        if company_dir:
            tmp_dir = self.create_dir(os.path.join(company_dir, 'tmp'))

        self.company_dir = company_dir
        self.tmp_dir = tmp_dir
        print('Starting IMAP email fetch')
        print('host %s' % self._host)
        print('ssl %s' % self._port)

        self._enabled = bool(self._host)

        self.last_checked = datetime.datetime.now()

    @staticmethod
    def create_dir(directory):
        if not os.path.exists(directory):
            os.mkdir(directory)
        return directory

    def pull(self):
        """
        Fetch all available mail at the target address and store it locally in
        the consumption directory so that the file consumer can pick it up and
        do its thing.
        """
        invoice_status = get_object_or_None(Status, slug='arrived-at-logger')
        invoice_type = get_object_or_None(InvoiceType, is_default=True)
        if not invoice_type:
            invoice_type = get_object_or_None(InvoiceType, code='tax-invoice')

        print('------------------ PULL -----------')
        if self._enabled:

            # Reset the grouping id for each fetch
            self.logging_group = uuid.uuid4()

            self.log("debug", "Checking mail")
            subject_code = ""
            if self._company.slug:
                subject_code = "{}{}".format(subject_code, self._company.slug)

            print('------------------ SUBJECT CODE -----------'.format(subject_code))
            messages = self._get_messages(subject_code)
            print('------------------ DONE GETTING MESSAGE -----------'.format(messages))
            for message in messages:
                self.log("info", 'Storing email: "{}" and message filename is {}'.format(message.subject,
                                                                                         message.file_name))
                t = int(time.mktime(message.time.timetuple()))
                filename = self.clean_filename(message.file_name)
                print('Cleaned filename is now {}'.format(filename))
                hash_filename = "{}_{}".format(hashlib.md5(filename.encode()).hexdigest(), self._company.slug)
                file_src = os.path.join(self.tmp_dir, filename)

                supplier = self.get_supplier(message, subject_code)

                print('Storing email: "{}" and filename is {} created from {}, message filename {} ,'
                      ' {}'.format(message.subject, file_name, filename, message.file_name, hash_filename))
                if message.attachment.content_type in ['image/jpeg', 'image/png', 'image/gif']:
                    try:
                        filename = "{}.pdf".format(hashlib.md5(filename.encode()).hexdigest())
                        file_location = os.path.join(self.tmp_dir, filename)
                        # image_dir = os.path.join(settings.DOCUMENT_IMAGE_DIR, filename)

                        with open(file_src, "wb") as image_file:
                            print('Write file to disk is {} -> {}'.format(file_src, file_location))
                            image_file.write(message.attachment.data)

                        with open(file_location, "wb") as f1:
                            print("Convert and save the pdf")
                            f1.write(img2pdf.convert(file_src))

                            print('Subject {} and Message content type {}'.format(message.subject,
                                                                                  message.attachment.content_type))
                            os.utime(file_src, times=(t, t))

                            file_info = FileInfo.from_path(file_src)
                            print("Filename is {}".format(filename))
                            print(file_info)
                            #
                            if file_info is not None:
                                self.create_invoice(file_src, filename, file_info, message, supplier,
                                                    invoice_status, invoice_type)
                    except Exception as ex:
                        print('Exception {}'.format(ex.__str__()))
                else:
                    with open(file_src, "wb") as f:
                        print('Subject {} and Message content type {}'.format(message.subject,
                                                                              message.attachment.content_type))
                        f.write(message.attachment.data)
                        os.utime(file_src, times=(t, t))

                        file_info = FileInfo.from_path(file_src)
                        print("Filename is {}".format(filename))
                        print(file_info)
                        #
                        if file_info is not None:
                            self.create_invoice(file_src, filename, file_info, message, supplier, invoice_status,
                                                invoice_type)
                print('____________________________________________________________________________________________')
        self.last_checked = datetime.datetime.now()

    def create_dir(self, dir):
        print('*************** Creating directory {} ********************'.format(dir))
        if not os.path.exists(dir):
            try:
                os.mkdir(dir)
            except OSError as exception:
                print('Error occurred {}'.format(exception.__str__()))
        return dir

    def clean_filename(self, filename):
        chars = [' ', ';', '\'', '[', ']', '#', '$', ':', '-', ',', ]
        filename = str(filename).lower().lower()
        for char in chars:
            filename.replace(char, '_')
        return filename

    def create_checksum(self, company, message):
        print('Creating the checksum')
        checksum = hashlib.md5(message.attachment.data + bytes(company.id)).hexdigest()
        print('checksum {}'.format(checksum))
        return checksum

    def create_invoice(self, file_src, filename, file_info, message, supplier, invoice_status, invoice_type):

        checksum = self.create_checksum(message)

        print('Check if there is a duplicate')
        if self._check_duplicate(checksum=checksum):
            print("Skipping {0} as it appears to be a duplicate : {1}".format(checksum, message.file_name))
            print(file_info)
            print("\r\n\n")
            self.log(
                "info",
                "Skipping {} as it appears to be a duplicate ".format(checksum)
            )
            if supplier:
                self.notify_supplier(supplier, message)
        else:
            try:
                invoice = Invoice.objects.create(
                    company=self._company,
                    accounting_date=timezone.now(),
                    checksum=checksum,
                    filename=filename
                )
                if invoice:
                    invoice_dir = os.path.join(settings.MEDIA_ROOT, "{}_{}".format(self._company.slug, str(invoice.id)))
                    self.create_dir(invoice_dir)
                    decrypted_src = os.path.join(invoice_dir, filename)

                    print("Try to decrypt the pdf from {}  to {}".format(file_src, decrypted_src))
                    has_decrypted = self.decrypt_pdf(source_file=file_src, destination_file=decrypted_src)

                    if self._company.currency:
                        invoice.currency = self._company.currency
                    if supplier:
                        invoice.supplier = supplier
                        if supplier.flow_proposal:
                            invoice.workflow = supplier.flow_proposal
                        if supplier.account_posting_proposal:
                            invoice.account_posting_proposal = supplier.account_posting_proposal
                        if supplier.vat_number:
                            invoice.vat_number = supplier.vat_number
                    if invoice_status:
                        invoice.status = invoice_status

                    if invoice_type:
                        invoice.invoice_type = invoice_type
                    invoice.save()

                    if invoice:
                        invoice_src = self.rename_invoice_document(invoice, invoice_dir, file_src, decrypted_src,
                                                                    has_decrypted)
                        # self.extract_invoice_images(invoice, invoice_src)
            except Exception as exception:
                print(
                    "Something went wrong trying to create invoice {}".format(exception.__str__()))

    def rename_invoice_document(self, invoice, invoice_dir, original_src, decrypted_src, has_decrypted):
        if has_decrypted:
            source_file = decrypted_src
        else:
            source_file = original_src
        filename = "{}_{}.pdf".format(invoice.id, self._company.slug)
        destination = os.path.join(invoice_dir, filename)
        print('Source {} and dest {}'.format(source_file, destination))
        os.rename(source_file, destination)

        invoice.filename = filename
        invoice.save()

        return destination

    def extract_invoice_images(self, invoice, file_src):
        if os.path.isfile(file_src):
            document_image = None
            try:
                document_image = self.convert_to_image(invoice, filename=file_src)
                # else:
                #     document_image = self.convert_to_image(invoice, filename=file_name)
            except Exception as exception:
                print('Exception %s' % exception)

            # stats = os.stat(file_name)
            if document_image:
                self.extract_invoice_contents(invoice, document_image, file_src)

    def notify_supplier(self, supplier, message):
        subject = "Duplicate invoice found on docuflow {}".format(message.subject)
        from_addr = 'no-reply@docuflow.co.za'
        recipient_list = (settings.WEB_ERRORS, supplier.email_address)
        message = "An duplicate invoice has been found on docuflow {}".format(message.file_name)
        send_mail(subject, message, from_addr, recipient_list)
        return True

    def get_supplier(self, message, subject_code):
        supplier = self.get_supplier_from_email_subject(message.subject, subject_code)
        if not supplier:
            print('No supplier from the email subject, try get from email from')
            supplier = self.get_supplier_by_email_from(message.email_from)
        return supplier

    def get_supplier_from_email_subject(self, mail_subject, subject_code):
        match = re.search(r'\#+(?P<company_code>\w+)+\-+(?P<supplier_code>\w+)', mail_subject)
        if match:
            matches = match.groupdict()
            if 'supplier_code' in matches and matches['supplier_code'] != '':
                return get_object_or_None(Supplier, code=matches['supplier_code'], company=self._company)
        return None

    def get_supplier_by_email_from(self, email_from):
        match = re.search(r'([\w\.,]+@[\w\.,]+\.\w+)', email_from)
        suppliers = self._company.suppliers.all()
        for supplier in suppliers:
            domains = supplier.domains.all()
            for domain in domains:
                str = re.compile(domain.name)
                match = re.search(str, email_from)
                if match:
                    return supplier
        return None

    def convert_to_image(self, invoice,  filename):
        # used to generate temp file name. so we will not duplicate or replace anything
        image_filename = os.path.splitext(os.path.basename(filename))[0]
        document_image_name = None
        try:
            all_pages = Image(filename=filename, resolution=150)
            page_counter = 0
            is_multi_file = False
            contents = []
            for i, page in enumerate(all_pages.sequence):
                with Image(page) as img:
                    img.compression_quality = 80
                    img.format = 'png'
                    _filename = hashlib.md5(str(image_filename).encode('utf-8')).hexdigest()
                    if i == 0:
                        image_name = f"{_filename}.png"
                        document_image_name = image_name
                    else:
                        image_name = f"{_filename}_{i}.png"
                    destination_filename = os.path.join(settings.DOCUMENT_IMAGE_DIR, image_name)
                    img.save(filename=destination_filename)

                page_counter = (page_counter + 1)

            if page_counter > 1:
                is_multi_file = True

            return dict({
                'is_multi': is_multi_file,
                'nb_pages': page_counter,
                'image_filename': document_image_name,
                'contents': contents
            })
        except Exception as err:
            print('Exception %s' % err)
            return False

    def extract_invoice_contents(self, invoice, document_image, file_src):
        contents = None
        try:
            contents = self.extract_data(file_src)
        except Exception as extract_error:
            print(extract_error)
        if contents is None and 'contents' in document_image:
            invoice.contents = " ".join(document_image['contents'])
        invoice.save()

    def decrypt_pdf(self, source_file, destination_file):
        # used to generate temp file name. so we will not duplicate or replace anything
        print("Decrypt the pdf")
        try:
            subprocess.call(["qpdf", "--decrypt", source_file, destination_file])
            return True
        except Exception as err:
            print('Exception %s' % err)
            pass

    def _check_duplicate(self, checksum):
        return Invoice.objects.filter(checksum=checksum).exists()

    def _get_messages(self, subject_code):
        messages = []
        try:

            self._connect()
            self._login()
            for message in self._fetch(subject_code):
                if message:
                    messages.append(message)

            self._connection.close()
            self._connection.logout()

        except Exception as e:
            self.log("error", str(e))

        return messages

    def _connect(self):
        self._connection = imaplib.IMAP4_SSL(self._host, self._port)

    def _login(self):

        print('Logging in username {} and password {} '.format(self._username, self._password))
        login = self._connection.login(self._username, self._password)
        if not login[0] == "OK":
            print("Can't log into mail: {}".format(login[1]))
            raise MailFetcherError("Can't log into mail: {}".format(login[1]))

        inbox = self._connection.select(self._inbox)
        if not inbox[0] == "OK":
            raise MailFetcherError("Can't find the inbox: {}".format(inbox[1]))

    def _fetch(self):
        if settings.IS_TEST:
            print("WARNING: THIS IS GETTING ALL NOT DELETED EMAILS")
            messages = self._connection.search(None, 'NOT', 'DELETED')[1][0].split()
        else:
            print("GET UNSEEN EMAILS")
            messages = self._connection.search(None, '(UNSEEN)')[1][0].split()

        for num in messages:
            __, data = self._connection.fetch(num, "(RFC822)")

            message = None
            try:
                print('Getting message {}'.format(num))
                message = Message(data[0][1], self.logging_group)
            except InvalidMessageError as e:
                self.log("error", str(e))
            else:
                pass

            if message:
                yield message
