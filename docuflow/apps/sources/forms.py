from django import forms

from .models import IMAPEmail, POP3Email, WatchFolderSource


class EmailSetupBaseForm(forms.ModelForm):
    class Meta:
        fields = ['label', 'enabled', 'interval', 'uncompress', 'host', 'ssl', 'port', 'username', 'password']
        widgets = {
            'password': forms.widgets.PasswordInput(render_value=True)
        }


class IMAPEmailSetupForm(EmailSetupBaseForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(IMAPEmailSetupForm, self).__init__(*args, **kwargs)

    class Meta:
        fields = EmailSetupBaseForm.Meta.fields + ['mailbox', ]
        model = IMAPEmail

    def save(self, commit=True):
        imap = super(IMAPEmailSetupForm, self).save(commit=False)
        imap.company = self.company
        imap.save()
        return imap


class POP3EmailSetupForm(EmailSetupBaseForm):
    class Meta:
        fields = EmailSetupBaseForm.Meta.fields + ['timeout', ]
        model = POP3Email


class WatchFolderSetupForm(forms.ModelForm):
    class Meta:
        fields = ['label', 'enabled', 'interval', 'uncompress', 'folder_path']
        model = WatchFolderSource
