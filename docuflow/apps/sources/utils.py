from __future__ import unicode_literals

import base64
import hashlib
import logging
import os
import pprint
import re
import subprocess
import sys

import img2pdf
from django.conf import settings

from docuflow.apps.common.utils import create_dir, upload_to_s3, create_checksum
from docuflow.apps.company.models import Company
# from docuflow.apps.invoice.convert import ImagePdfConverter
from docuflow.apps.invoice.models import Invoice, Status, InvoiceType
from docuflow.apps.invoice.utils import invoice_upload_path
from .exceptions import MailFetcherError
from .models import IMAPEmail
from .office_client import mail_fetch_client

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)

sys.setrecursionlimit(2500)


class MailFetcher(object):
    """
    Fetch all available mail at the target address and store it locally in
    the consumption directory so that the file consumer can pick it up and
    do its thing.

    Processes emails fetched from the source email inboxes. It looks for any attachments which are either pdf, images etc.
    Only 1 document is allowed per email
    One document is processed and an invoice is created linked to that document, the invoice is place in Logger state.
    """

    def __init__(self):
        self.checksums = []

    # http://www.doughellmann.com/PyMOTW/imaplib/
    def execute(self, source: IMAPEmail, can_create_invoice: bool = True):
        invoice_status = self.get_default_status()
        companies = self.get_companies()

        mails = mail_fetch_client.fetch_emails(source=source)
        consume_dir = os.path.join(settings.CONSUMPTION_DIR)

        # iterating over all emails
        _attachment_responses = []
        checksums = self.get_invoices_checksums()

        for mail in mails:
            company, supplier = self.get_company_supplier_from_subject(
                companies=companies,
                subject=mail['subject'],
                source=source
            )
            if not company:
                continue

            file_data = self.read_file(location=mail['location'])
            filename = mail['filename']

            checksum = create_checksum(company_id=company.id, data=file_data, filename=mail['name'])

            if checksum in checksums:
                logger.info(f'Invoice with checksum {checksum} already exists {checksums[checksum]} ')
                continue

            invoice_type = self.get_default_invoice_type(company=company)

            if not can_create_invoice:
                continue

            invoice = Invoice.new(
                company=company,
                supplier=supplier,
                checksum=checksum,
                filename=filename,
                status=invoice_status,
                invoice_type=invoice_type
            )
            if invoice:
                self.rename_invoice_document(
                    invoice=invoice,
                    original_dir=consume_dir,
                    filename=invoice.filename
                )

    # noinspection PyMethodMayBeStatic
    def get_company_supplier_from_subject(self, companies, subject: str, source: IMAPEmail):
        if not subject:
            return None, None

        match = re.search(r'\#+(?P<company_code>\w+)+(\-+(?P<supplier_code>\w+))?', subject)
        if not match:
            match = re.search(r'(\#?)+(?P<company_code>\w+)+(\-+(?P<supplier_code>\w+))?', subject)

        company_slug = None
        supplier = None
        company = None
        supplier_code = None
        if match:
            matches = match.groupdict()
            company_code = matches.get('company_code', None)
            supplier_code = matches.get('supplier_code', None)
            if company_code and company_code != '':
                company_slug = company_code
            if supplier_code and supplier_code != '':
                supplier_code = supplier_code

        if company_slug:
            company = companies.get(company_slug)

        if supplier_code and company:
            supplier = self.get_supplier(company=company, code=supplier_code)

        if not company:
            if source.company:
                company = source.company

        return company, supplier

    # noinspection PyMethodMayBeStatic
    def get_supplier(self, company: Company, code: str):
        if not (code or company):
            return None
        for supplier in company.suppliers.all():
            if supplier.code == code:
                return supplier
        return None

    def get_invoices_checksums(self):
        invoices = Invoice.objects.filter(checksum__in=self.checksums, deleted__isnull=True)
        invoices_checksums = {}
        for invoice in invoices:
            invoices_checksums[invoice.checksum] = invoice
        return invoices_checksums

    # noinspection PyMethodMayBeStatic
    def rename_invoice_document(self, invoice: Invoice, original_dir: str, filename: str):
        original_src = os.path.join(original_dir, filename)

        file_name = hashlib.md5(filename.encode()).hexdigest()
        extension = os.path.splitext(filename)[1].lower()
        filename = f"{file_name}{extension}"

        upload_path, destination_path = invoice_upload_path(invoice=invoice, filename=filename)

        # convertor = ImagePdfConverter(invoice)
        # convertor.convert_to_pdf()
        if settings.UPLOAD_TO_S3:
            upload_to_s3(file_path=original_src, filename=upload_path)
            invoice.ocr_status = Invoice.COMPLETED

        try:
            file_path = self.save_on_local(
                file_name=filename,
                destination=destination_path,
                original_src=original_src
            )
            if file_path:
                invoice.filename = filename
                invoice.save()
            if os.path.isfile(original_src):
                os.remove(original_src)

        except FileNotFoundError:
            pass

    # noinspection PyMethodMayBeStatic
    def save_on_local(self, file_name, destination, original_src):
        decrypted_file_name = f"decrypted_{file_name}"
        src = os.path.join(destination, file_name)
        file_path = os.path.join(destination, decrypted_file_name)

        destination = os.path.join(destination, file_name)
        os.rename(original_src, destination)

        decrypt_pdf(source_file=src, destination_file=file_path)

        return file_path

    # noinspection PyMethodMayBeStatic
    def get_default_invoice_type(self, company):
        invoice_type = InvoiceType.objects.default().purchase(company=company).first()
        if invoice_type:
            return invoice_type

        invoice_type = InvoiceType.objects.tax_invoice(company=company).first()
        if invoice_type:
            return invoice_type

        invoice_type = InvoiceType.objects.purchase(company=company).first()
        if invoice_type:
            return invoice_type

        raise MailFetcherError(f'No purchase invoice type for company {company} found')

    # noinspection PyMethodMayBeStatic
    def get_companies(self):
        return {
            company.slug: company
            for company in Company.objects.select_related('currency').prefetch_related(
                'document_types', 'suppliers').all()
        }

    # noinspection PyMethodMayBeStatic
    def get_default_status(self):
        status = Status.objects.filter(slug='arrived-at-logger').first()
        if not status:
            raise MailFetcherError('Default invoice status not found, please ensure logger status is set')
        return status

    # noinspection PyMethodMayBeStatic
    def is_fetch_all(self):
        if settings.IS_TEST is True and settings.DEBUG is True:
            return False
        return False

    # noinspection PyMethodMayBeStatic
    def read_file(self, location):
        logger.info(f"----Read file from {location}")
        with open(location, 'rb+') as fp:
            contents = fp.read()
        return contents


def clean_title(title):
    return str(title).replace("\r\n", "")


def image_to_pdf(filename, source, destination_dir):
    file_name = hashlib.md5(filename.encode()).hexdigest()

    filename = f"{file_name}.pdf"

    pdf_dest = os.path.join(destination_dir, filename)

    with open(pdf_dest, "wb") as f:
        f.write(img2pdf.convert(source))


def decrypt_pdf(source_file, destination_file):
    # used to generate temp file name. so we will not duplicate or replace anything
    try:
        subprocess.call(["qpdf", "--decrypt", source_file, destination_file])
    except subprocess.SubprocessError as err:
        logger.exception(err)


# https://stackoverflow.com/questions/2941995/python-ignore-incorrect-padding-error-when-base64-decoding
def base64_decode(s):
    """Add missing padding to string and return the decoded base64 string."""
    log = logging.getLogger()
    s = str(s).strip()
    try:
        return base64.b64decode(s)
    except TypeError:
        padding = len(s) % 4
        if padding == 1:
            log.error(f"Invalid base64 string: {s}")
            return ''
        elif padding == 2:
            s += b'=='
        elif padding == 3:
            s += b'='
        return base64.b64decode(s)
