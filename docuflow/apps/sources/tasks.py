from django.core.management import call_command
from huey import crontab
from huey.contrib.djhuey import periodic_task, lock_task, db_task

from docuflow.apps.invoice.models import Invoice, Status


# @db_task()
# def fetch_company_emails(company_id: int):
#     call_command('fetch_invoice_from_email', company_id=company_id)
#
#
# @db_task()
# def fetch_source_emails(source_id: int):
#     call_command('fetch_invoice_from_email', source_id=source_id)
#
#
# @db_task()
# def ocr_company_invoices(company_id):
#     successes = 0
#     failed = 0
#     invoices = Invoice.objects.filter(company_id=company_id)
#     invoice_status = Status.objects.get(slug='arrived-at-logger')
#     for invoice in invoices:
#         result = invoice.extract_invoice_data(None, invoice)
#
#         if result:
#             invoice.scan_update(result)
#             successes += 1
#         else:
#             invoice.orc_status = invoice.FAILED
#             invoice.save()
#             failed += 1
#
#         if invoice_status:
#             invoice.status = invoice_status
#             invoice.save()
#
#     return f"{successes} invoice have been scanned successfully. {failed} failed to get the contents"
