import logging
import pprint

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, CreateView, View
from django.views.generic.edit import UpdateView, DeleteView

from docuflow.apps.common.mixins import ProfileMixin
from .exceptions import MailFetcherError
from .forms import WatchFolderSetupForm, IMAPEmailSetupForm
from .models import WatchFolderSource, IMAPEmail, SourceLog
# from .tasks import fetch_company_emails, fetch_source_emails

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class ImapIndexView(LoginRequiredMixin, ProfileMixin, ListView):
    model = IMAPEmail
    context_object_name = 'sources'
    template_name = 'imap/index.html'

    def get_queryset(self):
        return super().get_queryset().filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(ImapIndexView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class ImapEditSourceView(ProfileMixin, LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = IMAPEmail
    form_class = IMAPEmailSetupForm
    template_name = 'imap/edit.html'
    success_url = reverse_lazy('imap-sources-index')
    success_message = 'Email source updated successfully'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class ImapCreateSourceView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = IMAPEmail
    form_class = IMAPEmailSetupForm
    template_name = 'imap/create.html'
    success_url = reverse_lazy('imap-sources-index')
    success_message = 'Email source created successfully'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class ImapDeleteSourceView(LoginRequiredMixin, ProfileMixin, DeleteView):
    model = IMAPEmail
    template_name = 'imap/delete.html'
    success_url = reverse_lazy('imap-sources-index')


class CheckSourceView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        source = IMAPEmail.objects.get(pk=self.kwargs['pk'])
        # fetch_source_emails(source_id=source.pk)

        messages.success(self.request, 'Email successfully fetched from the source.')
        return HttpResponseRedirect(reverse_lazy('documents-index'))


class ImapTestConnectionView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        source = IMAPEmail.objects.get(pk=self.kwargs['pk'])
        try:
            source.test_connection()
            messages.success(self.request, 'Connected successfully to the email source.')
        except MailFetcherError as exc:
            logger.exception(exc)
            messages.error(self.request, 'Could not connect to the email source.')
        return HttpResponseRedirect(reverse_lazy('imap-sources-index'))


class WatchIndexView(LoginRequiredMixin, ProfileMixin, ListView):
    model = WatchFolderSource
    template_name = 'watch/index.html'
    context_object_name = 'sources'

    def get_queryset(self):
        return WatchFolderSource.objects.filter(company=self.get_company())


class WatchEditSourceView(LoginRequiredMixin, UpdateView):
    model = WatchFolderSource
    form_class = WatchFolderSetupForm
    template_name = 'watch/edit.html'

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'watch source update successfully.')
        return HttpResponseRedirect(reverse_lazy('watch-sources-index'))


class WatchCreateSourceView(LoginRequiredMixin, ProfileMixin, CreateView):
    model = WatchFolderSource
    form_class = WatchFolderSetupForm
    template_name = 'watch/create.html'
    success_url = reverse_lazy('watch-sources-index')

    def form_valid(self, form):
        form = form.save(commit=False)
        form.company = self.get_company()
        form.save()
        messages.success(self.request, 'Watch source created successfully.')
        return HttpResponseRedirect(reverse_lazy('watch-sources-index'))


class WatchDeleteSourceView(LoginRequiredMixin, DeleteView):
    model = WatchFolderSource
    template_name = 'watch/delete.html'
    success_url = reverse_lazy('watch-sources-index')


class SourceFetchMailView(LoginRequiredMixin, View):

    def get(self, request):
        # fetch_company_emails(request.session['company'])

        return JsonResponse({
            'text': 'Email are being processed, please wait ...',
            'error': False,
            'reload': True
        })


class SourceLogView(LoginRequiredMixin, ListView):
    model = SourceLog
    ordering = '-datetime'
    context_object_name = 'logs'
    template_name = 'imap/log_index.html'


class ShowLogView(LoginRequiredMixin, TemplateView):
    template_name = 'imap/log_detail.html'

    def get_log(self):
        return SourceLog.objects.filter(pk=self.kwargs['pk']).first()

    def get_context_data(self, **kwargs):
        context = super(ShowLogView, self).get_context_data(**kwargs)
        context['log'] = self.get_log()
        return context

