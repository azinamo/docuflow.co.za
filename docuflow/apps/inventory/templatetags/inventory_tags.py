from collections import defaultdict
from decimal import Decimal
import pprint

from django import template
from django.db.models import Count
from django.contrib.humanize.templatetags.humanize import intcomma

from docuflow.apps.inventory.models import GroupLevel, GroupLevelItem, BranchInventory

register = template.Library()
pp = pprint.PrettyPrinter(indent=4)


@register.inclusion_tag('inventory/group_level_links.html')
def inventory_group_levels(company_id, inventory=None, orientation='horizontal'):
    group_level_categories = {}
    group_levels = GroupLevel.objects.annotate(
        is_parent=Count('children')
    ).select_related('parent').filter(company_id=company_id).all()

    inventory_group_levels = defaultdict(dict)
    parent_levels = defaultdict(dict)
    if inventory and type(inventory) == int:
        inventory = BranchInventory.objects.prefetch_related(
            'group_level_items', 'group_level_items__parent').filter(id=inventory).first()
    if inventory and isinstance(inventory, BranchInventory):
        for group_level_item in inventory.group_level_items.all():
            parent_levels[group_level_item.group_level.parent].update({'value': group_level_item.parent_id})
            inventory_group_levels[group_level_item.group_level].update({'value': group_level_item.id})

    group_level_items = GroupLevelItem.objects.filter(
        group_level_id__in=[group_level.id for group_level in group_levels]).all()
    for group_level in group_levels:
        group_level_categories[group_level.id] = {}
        group_level_categories[group_level.id]['is_parent'] = group_level.is_parent
        group_level_categories[group_level.id]['parent'] = group_level.parent_id if group_level.parent else ''
        group_level_categories[group_level.id]['label'] = str(group_level)
        group_level_categories[group_level.id]['value'] = parent_levels[group_level]['value'] if group_level in parent_levels else None
        group_level_categories[group_level.id]['objects'] = []

        selected_group_level_item = inventory_group_levels.get(group_level, None)
        if selected_group_level_item:
            group_level_categories[group_level.id]['value'] = selected_group_level_item['value']

    for group_level_item in group_level_items:
        if group_level_item.group_level_id in group_level_categories:
            group_level_categories[group_level_item.group_level_id]['objects'].append(group_level_item)

    return {'group_level_categories': group_level_categories,  'form_style': orientation}


@register.simple_tag
def average_price(total_quantity, total_price):
    dollars = None
    if total_quantity > 0:
        dollars = round(total_price/total_quantity, 2)
    if dollars:
        dollars = round(float(dollars), 2)
        return "%s%s" % (intcomma(int(dollars)), ("%0.2f" % dollars)[-3:])
    else:
        return "0.00"


@register.filter
def quantity(qty, decimal_places=2):
    if not qty:
        qty = 0
    qty = Decimal(qty)
    if qty:
        if decimal_places == 4:
            dollars = round(float(qty), 4)
            return "%s%s" % (intcomma(int(dollars)), ("%0.4f" % dollars)[-3:])
        else:
            dollars = round(float(qty), 2)
            return "%s%s" % (intcomma(int(dollars)), ("%0.2f" % dollars)[-3:])
    return "0.00"
    #return round(qty)
    # if decimal_places == 4:
    #     return f"{qty:20,.4f}"
    # return f"{qty:20,.2f}"
