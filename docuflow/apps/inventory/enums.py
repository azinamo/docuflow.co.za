from django.utils.translation import gettext as __

from enumfields import IntEnum


class InventoryStatus(IntEnum):
    ACTIVE = 1
    DEACTIVATED = 2


class TransactionStatus(IntEnum):
    PENDING = 1
    COMPLETED = 2

    class Labels:
        COMPLETED = __('Closed')


class MovementType(IntEnum):
    GENERAL = 1
    PURCHASE_ORDER = 2
    OPENING_BALANCE = 3
    MANUFACTURING = 4
    STOCK_ADJUSTMENT = 5
    BRANCH_TRANSFER = 6
    CLOSING_BALANCE = 7


class SkuType(IntEnum):
    SKU = 1
    SERVICE = 2
    NOT_SKU = 3

    class Labels:
        SKU = __('Sku')
        SERVICE = __('Service with Cost provision')
        NOT_SKU = __('Usage')


class PriceType(IntEnum):
    LAST_PRICE = 1
    AVERAGE = 2
    OWN = 3


class SoldAsType(IntEnum):
    PACKAGE = 1
    ITEM = 2


class UpdatePriceAt(IntEnum):
    STOCKING = 1
    INVOICE = 2


class PurchaseOrderStatus(IntEnum):
    OPEN = 1
    CLOSED = 2

    class Labels:
        OPEN = __('Open')
        CLOSED = __('Closed')


class StockCountStatus(IntEnum):
    GENERATED = 1
    COUNTED = 2
    ADJUSTED = 3


class StockCountType(IntEnum):
    RANDOM = 0
    ALL_STOCK = 1
    HIGHEST_ITEM_VALUE = 2  # Value on the ledger
    HIGHEST_TOTAL_VALUE = 3  # Highest as per valuation report ()
    HIGHEST_QUANTITY = 4
    SELECT_STOCK = 5


class LedgerStatus(IntEnum):
    PENDING = 0
    COMPLETE = 1


class ManufacturingStatus(IntEnum):
    PROCESSING = 0
    COMPLETED = 1
    FAILED = 2
