# Generated by Django 2.0 on 2020-06-23 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0020_auto_20200623_0757'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventoryreceivedreturned',
            name='received_closing_quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=12, null=True),
        ),
        migrations.AddField(
            model_name='inventoryreceivedreturned',
            name='received_opening_quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=12, null=True),
        ),
        migrations.AddField(
            model_name='inventoryreceivedreturned',
            name='return_closing_quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=12, null=True),
        ),
        migrations.AddField(
            model_name='inventoryreceivedreturned',
            name='return_opening_quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=12, null=True),
        ),
    ]
