# Generated by Django 3.1 on 2022-02-23 03:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('period', '0007_auto_20201205_2106'),
        ('inventory', '0041_auto_20220220_1326'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventoryreceivedreturned',
            name='received_period',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='receivables', to='period.period'),
        ),
        migrations.AddField(
            model_name='inventoryreceivedreturned',
            name='returned_period',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='returns', to='period.period'),
        ),
    ]
