# Generated by Django 2.0 on 2020-05-10 13:10

from django.db import migrations, models
from django.contrib.contenttypes.models import ContentType


def update_object_owner_and_reference(apps, schema_editor):
    Ledger = apps.get_model('inventory', 'Ledger')
    for ledger in Ledger.objects.all():
        if ledger.supplier:
            ledger.object_owner = f"{ledger.supplier.code} - {ledger.supplier.name}"
        content_type = ContentType.objects.get(id=ledger.content_type.id)
        Model = apps.get_model(content_type.app_label, content_type.model)
        model_object = Model.objects.get(id=ledger.object_id)
        if model_object:
            if content_type.model == 'invoice':
                if model_object.customer:
                    ledger.object_owner = str(model_object.customer.name)
            if content_type.model == 'received':
                if model_object.supplier_id:
                    Supplier = apps.get_model('supplier', 'supplier')
                    supplier = Supplier.objects.get(id=model_object.supplier_id)
                    if supplier:
                        ledger.object_owner = f"{supplier.code} - {supplier.name}"
            ledger.ledger_reference = str(ledger.text)
        ledger.save()


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0010_auto_20200510_1510'),
    ]

    operations = [
        migrations.RunPython(update_object_owner_and_reference)
    ]
