# Generated by Django 2.0 on 2020-06-24 09:18

from django.db import migrations, models
import django.db.models.deletion

def add_inventory_to_fifo(apps, schema_editor):
    InventoryReceivedReturned = apps.get_model('inventory', 'InventoryReceivedReturned')
    InventoryReceived = apps.get_model('inventory', 'InventoryReceived')
    InventoryReturned = apps.get_model('inventory', 'InventoryReturned')
    for inventory_received_returned in InventoryReceivedReturned.objects.all():
        inventory_received = InventoryReceived.objects.filter(
            pk=inventory_received_returned.inventory_received_id
        ).first()
        if inventory_received:
            inventory_received_returned.inventory_id = inventory_received.inventory_id
            inventory_received_returned.save()
        else:
            inventory_returned = InventoryReturned.objects.filter(
                pk=inventory_received_returned.inventory_returned_id
            ).first()
            if inventory_returned:
                inventory_received_returned.inventory_id = inventory_returned.inventory_id
                inventory_received_returned.save()


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0021_auto_20200623_1521'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='inventoryreturned',
            options={'ordering': ('id',)},
        ),
        migrations.AddField(
            model_name='inventoryreceivedreturned',
            name='inventory',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fifo', to='inventory.BranchInventory'),
        ),
        migrations.RunPython(add_inventory_to_fifo)
    ]
