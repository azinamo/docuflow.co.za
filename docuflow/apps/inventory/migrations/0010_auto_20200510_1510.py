# Generated by Django 2.0 on 2020-05-10 13:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0009_auto_20200503_1754'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ledger',
            name='inventory_ledger_id',
        ),
        migrations.AddField(
            model_name='ledger',
            name='object_owner',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AddField(
            model_name='ledger',
            name='object_reference',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
