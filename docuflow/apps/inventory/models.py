import logging
from datetime import datetime
from decimal import Decimal
from typing import List

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Sum, Q, F, When, Case, DecimalField
from django.db.models.expressions import RawSQL
from django.db.models.functions import Coalesce
from django.urls import reverse
from django.utils.functional import cached_property
from enumfields import EnumIntegerField

from docuflow.apps.common.enums import Module
from docuflow.apps.common.fields import QuantityField, MoneyField
from docuflow.apps.common.utils import create_document_number
from docuflow.apps.sales.enums import InvoiceStatus, PickingSlipStatus, DeliveryStatus
from . import enums
from . import utils
from .managers import GroupLevelManager
from ..company.models import Branch

logger = logging.getLogger(__name__)


class GroupLevel(models.Model):
    company = models.ForeignKey('company.Company', related_name='group_levels', on_delete=models.CASCADE)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.SET_NULL)
    name = models.CharField(max_length=255)
    slug = models.SlugField()

    objects = GroupLevelManager()

    def __str__(self):
        return self.name


class GroupLevelItem(models.Model):
    group_level = models.ForeignKey(GroupLevel, related_name='items', on_delete=models.CASCADE)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.SET_NULL)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Inventory(models.Model):
    company = models.ForeignKey('company.Company', related_name='inventory_items', on_delete=models.CASCADE)
    inventory_id = models.PositiveIntegerField(null=True, blank=True)
    code = models.CharField(max_length=255, blank=True, verbose_name='Inventory Code', db_index=True)
    description = models.TextField(verbose_name='Inventory Description', db_index=True)
    image = models.ImageField(upload_to='media/uploads')

    def __str__(self):
        return self.code

    def full_name(self):
        return f"{self.code}-{self.description}"


class BranchInventoryManagerQuerySet(models.QuerySet):
    def branch(self, branch):
        return self.filter(branch=branch)

    def status(self, status):
        return self.filter(status=status)

    def company(self, company):
        return self.filter(company=company)


class AllInventoryManager(models.Manager):

    def get_queryset(self):
        return super(AllInventoryManager, self).get_queryset()


class BranchInventoryManager(models.Manager):

    def get_queryset(self):
        return super(BranchInventoryManager, self).get_queryset().filter(is_blocked=False)

    def active(self):
        return self.get_queryset().exclude(is_blocked=True)

    def annotate_sum_for_inventory(self, related_model_class, field_name, annotation_name):
        # https://code.djangoproject.com/ticket/10060
        raw_query = "SELECT SUM({field}) FROM {model} AS model WHERE model.inventory_id = inventory.id".format(
            field=field_name, model=related_model_class._meta.db_table)
        annotation = {annotation_name: RawSQL(raw_query, [])}
        return self.annotate(**annotation)

    def received(self):
        return self.annotate_sum_for_inventory(InventoryReceived, 'quantity', 'total_received')

    def returned(self):
        return self.annotate_sum_for_inventory(InventoryReturned, 'quantity', 'total_returned')

    def opening_balance(self):
        return self.annotate_sum_for_inventory(InventoryReceived, 'quantity', 'opening_balance')

    def get_detail(self):
        q_filter = Q()
        q_filter.add(Q(sales_invoice_items__invoice__updates__eq=0), Q.AND)

        return self.annotate(
            in_stock=F('history__in_stock'),
            being_picked=Sum('sales_pickingslipitems__quantity'),
            on_delivery=Sum('sales_deliveryitems__quantity'),
            on_invoice=models.Case(
                models.When(q_filter, models.Sum('sales_invoiceitems__quantity')),
                output_field=models.DecimalField()
            ),
            available_quantity=F('in_stock') - F('on_delivery') - F('being_picked') - F('on_invoice')
        )

    def in_hand(self):
        return self.annotate(
            total_received=models.Subquery(
                InventoryReceived.objects.filter(
                    inventory=models.OuterRef('id')
                ).annotate(total_quantity=models.Sum('quantity')).value(
                    'total_quantity'
                )[0]
            ),
            total_returned=models.Subquery(
                InventoryReturned.objects.filter(
                    inventory=models.OuterRef('id')
                ).annotate(total_quantity=models.Sum('quantity')).value(
                    'total_quantity'
                )[0]
            ),
            in_order=models.Subquery(
                PurchaseOrderInventory.objects.filter(
                    inventory=models.OuterRef('id'),
                    status=PurchaseOrderInventory.OPEN
                ).annotate(total_quantity=models.Sum('quantity')).value(
                    'total_quantity'
                )[0]
            )
        )

    def with_stock_levels(self, filter_options):
        return self.get_queryset().select_related(
            'inventory', 'history',
        ).filter(
            **filter_options
        )

    def valuation_report(self, branch, year, **kwargs):
        year_filter = Q()
        year_filter.add(Q(ledgers__period__period_year=year), Q.AND)

        opening_balance_filter = Q()
        opening_balance_filter.add(Q(inventory_received__received__period__period_year=year), Q.AND)
        opening_balance_filter.add(Q(inventory_received__received__movement_type=enums.MovementType.OPENING_BALANCE), Q.AND)

        return self.get_queryset().annotate(
            opening_total_value=Case(
                When(opening_balance_filter, then=Sum(F('inventory_received__price_excluding') * F('inventory_received__quantity'))),
                default=0,
                output_field=DecimalField()
            ),
            ledger_total_value=Case(
                When(year_filter, then=Sum(F('ledgers__value') * F('ledgers__quantity') * F('ledgers__rate'))),
                default=0,
                output_field=DecimalField()
            ),
            total_value=F('opening_total_value') + F('ledger_total_value'),
            total_quantity=Case(
                When(year_filter, then=Sum(F('ledgers__quantity'))),
                default=0,
                output_field=DecimalField()
            )
        ).prefetch_related(
            'ledgers', 'group_level_items', 'group_level_items__group_level', 'group_level_items__parent'
        ).select_related(
            'inventory'
        ).filter(
            branch=branch
        ).filter(
            **kwargs
        )

    def copy_previous_year_balances(self, transaction, branch, from_year, to_year):
        inventories = self.valuation_report(branch, from_year)
        transaction.inventory_received.all().delete()

        for inventory in inventories:
            total_quantity = inventory.total_quantity
            total_price = inventory.total_value
            average_price = 0
            if total_quantity > 0:
                average_price = round(total_price / total_quantity, 2)
            logger.info(f"Inventory {inventory}({inventory.id}) has quantity {inventory.total_quantity} and value "
                        f" --> at {inventory.total_value}, copy from {from_year}, to {to_year}")

            InventoryReceived.objects.update_or_create(
                received=transaction,
                inventory=inventory,
                received__period__period_year=to_year,
                defaults={'price_excluding': average_price, 'quantity': inventory.total_quantity}
            )

    def with_average_price(self):
        return self.aggregate(
            value_total=Coalesce(Sum(F('fifo__price') * F('fifo__quantity')), 0),
            total_qty=F('fifo__quantity'),
            avg_price=Case(
                When(total_qty__gt=0, then=F('value_total') / F('total_qty')),
                output_field=DecimalField()
            )
        )


class BranchInventory(models.Model):
    STOCKING = 1
    INVOICE = 2

    UPDATE_AT = (
        (STOCKING, 'Stocking'),
        (INVOICE, 'Invoice'),
    )

    PACKAGE = 1
    ITEM = 2

    SOLD_AS_TYPES = (
        (PACKAGE, 'Package'),
        (ITEM, 'Item'),
    )

    SKU = 1
    SERVICE = 2
    NOT_SKU = 3
    SKU_TYPES = (
        (SKU, 'Sku'),
        (SERVICE, 'Service with Cost provision'),
        (NOT_SKU, 'Usage'),
    )

    LAST_PRICE = 1
    AVERAGE = 2
    PRICE_TYPES = (
        (LAST_PRICE, 'Last Price'),
        (AVERAGE, 'Average')
    )

    inventory = models.ForeignKey(Inventory, related_name='branch_inventory_items', on_delete=models.CASCADE)
    branch = models.ForeignKey('company.Branch', related_name='inventory_items', on_delete=models.CASCADE)
    parent_branch = models.ForeignKey('company.Branch', null=True, blank=True, related_name='parent_inventory_items', on_delete=models.CASCADE)
    is_blocked = models.BooleanField(default=False, verbose_name='Block Item')
    is_pos_blocked = models.BooleanField(default=False, verbose_name='Block only from POS')
    is_recipe_item = models.BooleanField(default=False, verbose_name='Recipe item')
    sku_type = EnumIntegerField(enums.SkuType, default=enums.SkuType.SKU, verbose_name='SKU Item/Service Item')
    sku = models.CharField(max_length=255, verbose_name='SKU', blank=True)
    service_cost = models.DecimalField(verbose_name='Cost to provide as a % of Sale', null=True, blank=True,
                                       max_digits=5, decimal_places=2)
    description = models.TextField(null=True, blank=True, verbose_name='Branch Alternative Description')
    sales_account = models.ForeignKey('company.Account', null=True, blank=True, related_name='sales_inventory_items',
                                      verbose_name='Sales Account', on_delete=models.DO_NOTHING)
    cost_of_sales_account = models.ForeignKey('company.Account', null=True, blank=True,
                                              related_name='cost_of_salaes_inventory_items', verbose_name='Cost of Sales Account', on_delete=models.DO_NOTHING)
    gl_account = models.ForeignKey('company.Account', null=True, blank=True, related_name='gl_inventory_items',
                                   verbose_name='Inventory GL Account Number', on_delete=models.DO_NOTHING)
    stock_adjustment_account = models.ForeignKey('company.Account', null=True, blank=True,
                                                 related_name='stock_adjustment_account_inventory_items', verbose_name='Stock Adjustment Account Number', on_delete=models.DO_NOTHING)
    service_account = models.ForeignKey('company.Account', null=True, blank=True, verbose_name='Account where cost will be provided', on_delete=models.DO_NOTHING)
    bar_code = models.CharField(max_length=255, blank=True)
    cost_price = models.DecimalField(verbose_name='Default Cost Price (Ex Vat)', null=True, blank=True, default=0,
                                     decimal_places=2, max_digits=12)
    selling_price = models.DecimalField(verbose_name='Default Selling Price (Ex Vat)', default=0, null=True,
                                        blank=True, decimal_places=2, max_digits=12)
    vat_code = models.ForeignKey('company.VatCode', verbose_name='Input Vat', null=True, blank=True,
                                 related_name='vat_code_inventory_items', on_delete=models.DO_NOTHING)
    sale_vat_code = models.ForeignKey('company.VatCode', verbose_name='Output Vat', null=True, blank=True,
                                      related_name='output_vat_code_inventory_items', on_delete=models.DO_NOTHING)
    is_discounted = models.BooleanField(default=False, verbose_name='Can be discounted')
    measure = models.ForeignKey('company.Measure', null=True, blank=True, related_name='measure_inventories',
                                on_delete=models.DO_NOTHING)
    unit = models.ForeignKey('company.Unit', null=True, blank=True, verbose_name='Default Unit eg. Each, kg, etc',
                             on_delete=models.DO_NOTHING)
    sale_unit = models.ForeignKey('company.Unit', null=True, blank=True, related_name='unit_inventory_sales',
                                  verbose_name='Default Unit eg. Each, kg, etc', on_delete=models.DO_NOTHING)
    stock_unit = models.ForeignKey('company.Unit', null=True, blank=True, related_name='unit_stock_controls',
                                   verbose_name='Default Unit eg. Each, kg, etc', on_delete=models.DO_NOTHING)
    additional_text = models.CharField(max_length=200, blank=True, verbose_name='Additional Invoice Text')
    delivery_fee = models.DecimalField(verbose_name='Default Delivery Fee', default=0, null=True, blank=True,
                                       decimal_places=2, max_digits=12)
    sale_delivery_fee = models.DecimalField(verbose_name='Default Delivery Fee', default=0, null=True, blank=True,
                                            decimal_places=2, max_digits=12)
    package_size = models.JSONField(verbose_name='Size of Package/Item(L*B*H)', null=True, blank=True)
    sale_package_size = models.JSONField(verbose_name='Size of Package/Item(L*B*H)', null=True, blank=True)
    gross_weight = models.DecimalField(verbose_name='Gross Weight (kg)', null=True, blank=True, decimal_places=4,
                                       max_digits=15)
    weight = models.DecimalField(verbose_name='Net Weight (kg)', null=True, blank=True, decimal_places=4, max_digits=15)
    environment_fee = models.DecimalField(verbose_name='Environment Fee', null=True, blank=True, decimal_places=2,
                                          max_digits=15)
    sale_environment_fee = models.DecimalField(verbose_name='Default Environment Fee', null=True, blank=True,
                                               decimal_places=2, max_digits=15)
    last_counted = models.DateTimeField(null=True, blank=True)
    allow_negative_stocks = models.BooleanField(default=False, verbose_name='Allow negative stock levels')
    price_type = EnumIntegerField(enums.PriceType, default=enums.PriceType.LAST_PRICE, verbose_name='Use Price')
    critical_stock_level = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)
    lowest_stock_level = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)
    max_stock_level = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15,
                                          verbose_name='Max Stock Level to hold')
    level_to_order_stock = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15,
                                               verbose_name='Level at which stock is to be ordered')
    min_reorder = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15, verbose_name='Min Reorder')
    max_reorder = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15, verbose_name='Max Reorder')
    quantity_in_package = models.PositiveIntegerField(null=True, blank=True, verbose_name='Quantity items in package. eg. 6')
    sale_quantity_in_package = models.PositiveIntegerField(null=True, blank=True, verbose_name='Quantity items in package. eg. 6')
    sold_as_type = EnumIntegerField(enums.SoldAsType, default=enums.SoldAsType.ITEM, verbose_name='Sold as Package or Item')
    max_package_to_order = models.PositiveIntegerField(null=True, blank=True,
                                                       verbose_name='Max packages to Order when placing order')
    store_area = models.CharField(max_length=255, null=True, blank=True)
    rack = models.CharField(max_length=255, null=True, blank=True)
    bin_location = models.CharField(max_length=255, null=True, blank=True)
    supplier = models.ForeignKey('supplier.Supplier', verbose_name='Preferred Supplier', null=True, blank=True,
                                 on_delete=models.DO_NOTHING)
    supplier_code = models.CharField(max_length=50, null=True, blank=True,
                                     verbose_name='Supplier Product Code (if different from ours)',)
    update_price_at = EnumIntegerField(enums.UpdatePriceAt, null=True, blank=True)
    last_price_updated = models.DateField(null=True, blank=True)
    group_level_items = models.ManyToManyField(GroupLevelItem)

    objects = BranchInventoryManager()
    all_inventory = AllInventoryManager()

    class Meta:
        ordering = ['description']
        verbose_name_plural = 'Branch Inventories'

    def __str__(self):
        return f"{str(self.inventory.code)}"

    def save(self, *args, **kwargs):
        return super(BranchInventory, self).save(*args, **kwargs)

    def deactivate(self):
        self.is_blocked = True
        self.save()

    def set_on_delivery_quantity(self, quantity):
        inventory_history = self.history
        if not inventory_history:
            inventory_history = PurchaseHistory()
            inventory_history.inventory = self
        if inventory_history.on_delivery:
            inventory_history.on_delivery += Decimal(quantity)
        else:
            inventory_history.on_delivery = Decimal(quantity)
        inventory_history.save()

    def recalculate_balances(self, year, is_dry=False):
        total_qty, total_value, average = self.calculate_summary_values(year=year)

        year_balance, _ = Balance.objects.update_or_create(
            inventory=self,
            year=year,
            defaults={
                'closing_price': average,
                'closing_value': total_value,
                'closing_quantity': total_qty
            }
        )

        next_year_balance = Balance.objects.filter(year__year=F('year') + 1, inventory=self).first()
        if next_year_balance:
            next_year_balance.opening_price = average
            next_year_balance.opening_quantity = total_qty
            next_year_balance.opening_value = total_value
            next_year_balance.save()

            self.recalculate_balances(year=next_year_balance.year)

        if not is_dry:
            history, created = PurchaseHistory.objects.update_or_create(
                inventory=self,
                defaults={
                    'average_price': average,
                    'in_stock': total_qty or 0,
                    'total_value': total_value,
                    'on_picking': self.being_picked_total or 0,
                    'on_delivery': self.on_delivery_total or 0,
                    'on_order': self.on_invoice_total or 0,
                    'on_reserve': self.on_reserve or 0
                }
            )
        return total_qty, total_value, average

    def calculate_summary_values(self, year=None):
        movements = InventoryReceivedReturned.objects.filter(
            inventory=self
        ).filter(
            Q(returned_object_id__isnull=True, received_object_id__isnull=False) |
            Q(received_object_id__isnull=True, returned_object_id__isnull=False)
        )
        # if year:
        #     movements = movements.filter(
        #         Q(returned_period__period_year=year) | Q(received_period__period_year=year)
        #     )
        total_value = 0
        total_qty = 0
        total_received = 0
        total_returned = 0
        for mvt in movements:
            if mvt.quantity:
                if mvt.returned_object_id:
                    quantity = mvt.quantity * -1
                    ret_value = mvt.value_returned() * -1
                    total_received += ret_value
                    total_value += ret_value
                    total_qty += quantity
                    total_returned += quantity
                if mvt.received_object_id:
                    quantity = mvt.quantity
                    rec_value = mvt.value_received()
                    total_received += rec_value
                    total_value += rec_value
                    total_qty += quantity
                    total_received += quantity
        average = 0
        total_value = round(total_value, 2)
        if total_value and total_qty:
            average = round(total_value / total_qty, 2)
        return total_qty, total_value, average

    def inventory_movements(self, year=None):
        movements = InventoryReceivedReturned.objects.filter(
            inventory=self
        )

        total_value = 0
        total_qty = 0
        total_received = 0
        total_returned = 0
        returned_type = ContentType.objects.get_for_model(Returned, for_concrete_model=False)
        stock_adjustment_type = ContentType.objects.get_for_model(StockAdjustmentInventory, for_concrete_model=False)
        manufacturing_type = ContentType.objects.get_for_model(Manufacturing, for_concrete_model=False)
        received_type = ContentType.objects.get_for_model(Received, for_concrete_model=False)
        manufacturing_recipe_type = ContentType.objects.get_for_model(ManufacturingRecipe, for_concrete_model=False)
        invoice_item_type = ContentType.objects.filter(app_label='sales', model='invoiceitem').first()
        inventory_received_type = ContentType.objects.get_for_model(InventoryReceived, for_concrete_model=False)
        inventory_returned_type = ContentType.objects.get_for_model(InventoryReturned, for_concrete_model=False)

        for mvt in movements:
            if mvt.quantity:
                if mvt.returned_object_id:
                    return_is_included = True
                    if mvt.returned_type == returned_type:
                        returned = Returned.objects.get(pk=mvt.returned_object_id)
                        return_is_included = year and (returned.period.period_year == year)
                        mvt.returned_period = returned.period
                        mvt.save()
                    elif mvt.received_type == inventory_returned_type:
                        inventory_returned = mvt.received_object.returned
                        return_is_included = year and (inventory_returned.period.period_year == year)
                        mvt.returned_period = inventory_returned.period
                        mvt.save()
                    elif mvt.returned_type == stock_adjustment_type:
                        stock_adjustment_inventory = StockAdjustmentInventory.objects.get(pk=mvt.returned_object_id)
                        return_is_included = year and (stock_adjustment_inventory.adjustment.period.period_year == year)
                        mvt.returned_period = stock_adjustment_inventory.adjustment.period
                        mvt.save()
                    elif mvt.returned_type == manufacturing_type:
                        manufacturing = Manufacturing.objects.get(pk=mvt.returned_object_id)
                        return_is_included = year and (manufacturing.period.period_year == year)
                        mvt.returned_period = manufacturing.adjustment.period
                        mvt.save()
                    elif mvt.returned_type == manufacturing_recipe_type:
                        manufacturing_recipe = ManufacturingRecipe.objects.get(pk=mvt.returned_object_id)
                        return_is_included = year and (manufacturing_recipe.manufacturing.period.period_year == year)
                        mvt.returned_period = manufacturing_recipe.manufacturing.period
                        mvt.save()
                    elif mvt.returned_type == invoice_item_type:
                        return_is_included = year and (mvt.returned_object.invoice.period.period_year == year)
                        mvt.returned_period = mvt.returned_object.invoice.period
                        mvt.save()

                    if return_is_included:
                        quantity = mvt.quantity * -1
                        ret_value = mvt.value_returned() * -1
                        total_received += ret_value
                        total_value += ret_value
                        total_qty += quantity
                        total_returned += quantity
                if mvt.received_object_id:
                    received_is_included = True
                    if mvt.received_type == received_type:
                        received = Received.objects.get(pk=mvt.received_object_id)
                        received_is_included = year and (received.period.period_year == year)
                        mvt.received_period = received.period
                        mvt.save()
                    elif mvt.received_type == inventory_received_type:
                        inventory_received = mvt.received_object.received
                        received_is_included = year and (inventory_received.period.period_year == year)
                        mvt.received_period = inventory_received.period
                        mvt.save()
                    elif mvt.received_type == stock_adjustment_type:
                        stock_adjustment_inventory = StockAdjustmentInventory.objects.get(pk=mvt.received_object_id)
                        received_is_included = year and (stock_adjustment_inventory.adjustment.period.period_year == year)
                        mvt.received_period = stock_adjustment_inventory.adjustment.period
                        mvt.save()
                    elif mvt.received_type == manufacturing_type:
                        manufacturing = Manufacturing.objects.get(pk=mvt.received_object_id)
                        received_is_included = year and (manufacturing.period.period_year == year)
                        mvt.received_period = manufacturing.period
                        mvt.save()
                    elif mvt.received_type == manufacturing_recipe_type:
                        manufacturing_recipe = ManufacturingRecipe.objects.get(pk=mvt.received_object_id)
                        received_is_included = year and (manufacturing_recipe.manufacturing.period.period_year == year)
                        mvt.received_period = manufacturing_recipe.manufacturing.period
                        mvt.save()
                    elif mvt.received_type == invoice_item_type:
                        received_is_included = year and (mvt.received_object.invoice.period.period_year == year)
                        mvt.received_period = mvt.received_object.invoice.period
                        mvt.save()

                    if received_is_included:
                        quantity = mvt.quantity
                        rec_value = mvt.value_received()
                        total_received += rec_value
                        total_value += rec_value
                        total_qty += quantity
                        total_received += quantity
        average = 0
        total_value = round(total_value, 2)
        if total_value and total_qty:
            average = round(total_value / total_qty, 2)
        return total_qty, total_value, average

    @property
    def is_service_sku(self):
        return self.sku_type == enums.SkuType.SERVICE

    @property
    def is_not_sku(self):
        return self.sku_type in [enums.SkuType.NOT_SKU]

    @property
    def selling_price_with_vat(self):
        price = self.selling_price
        if self.vat_code and self.selling_price:
            price = self.vat_amount + self.selling_price
        return Decimal(price)

    @property
    def max_stock_to_hold(self):
        return self.lowest_stock_level

    @property
    def vat_amount(self):
        amount = 0
        if self.vat_code and self.selling_price and self.vat_code.percentage:
            amount = (self.vat_code.percentage / 100) * float(self.selling_price)
        vat = Decimal(amount)
        return Decimal(vat.quantize(Decimal('.01')))

    @property
    def full_name(self):
        if self.description:
            return f"{self.inventory.code} - {self.description}"
        else:
            return f"{self.inventory.code} - {self.inventory.description}"

    @property
    def descr(self):
        return self.description or self.inventory.description

    @property
    def current_price(self):
        history = getattr(self, 'history', None)
        return history.last_price_paid if history else self.average_price

    @property
    def total_value(self):
        history = getattr(self, 'history', None)
        return history.total_value if history else Decimal(0)

    @property
    def average_price(self):
        history = getattr(self, 'history', None)
        return history.average_price if history else Decimal(0)

    @property
    def in_stock(self):
        history = getattr(self, 'history', None)
        return history.in_stock if history else Decimal(0)

    @property
    def on_order(self):
        history = getattr(self, 'history', None)
        return history.on_order if history else Decimal(0)

    @property
    def on_reserve(self):
        history = getattr(self, 'history', None)
        return history.on_reserve if history else Decimal(0)

    @property
    def on_delivery(self):
        history = getattr(self, 'history', None)
        return history.on_delivery if history else Decimal(0)

    @property
    def on_picking(self):
        history = getattr(self, 'history', None)
        return history.on_picking if history else Decimal(0)

    @property
    def being_picked_total(self):
        # TODO - Add an async task using celery beat to calculate the on picking and save on inventory history
        being_picked = self.sales_pickingslipitem_related.filter(
            picking_slip__status=PickingSlipStatus.SEND_FOR_PICKING
        ).aggregate(total=Sum('quantity'))
        if being_picked:
            return being_picked['total']
        return 0

    @property
    def on_delivery_total(self):
        # TODO - Add an async task using celery beat to calculate the on delivery and save on inventory history
        on_delivery = self.sales_deliveryitem_related.exclude(
            delivery__status__in=[DeliveryStatus.INVOICED, DeliveryStatus.REMOVED]
        ).aggregate(total=Sum('quantity', filter=Q(delivery__deleted__isnull=True)))
        return on_delivery['total']

    @cached_property
    def on_invoice_total(self):
        # TODO - Add an async task using celery beat to calculate the on invoice and save on inventory history
        on_invoice = self.sales_invoiceitem_related.filter(
            invoice__picking_slip__isnull=True
        ).exclude(
            models.Q(invoice__status=InvoiceStatus.UPDATED) | models.Q(invoice__is_recurring=True),
        ).aggregate(
            total=Sum('quantity')
        )
        if on_invoice:
            return on_invoice['total']
        return 0

    @property
    def available_stock(self):
        history = getattr(self, 'history', None)
        return history.available_stock if history else Decimal(0)

    @property
    def invoicable_stock(self):
        history = getattr(self, 'history', None)
        return history.invoicable_stock if history else Decimal(0)

    @property
    def default_stock_adjustment_account(self):
        if self.stock_adjustment_account:
            return self.stock_adjustment_account
        elif self.branch.company.stock_adjustment_account:
            return self.branch.company.stock_adjustment_account
        else:
            return None

    @property
    def is_negative_allowed(self):
        if self.sku_type == enums.SkuType.SERVICE:
            return True
        return self.allow_negative_stocks

    @classmethod
    def copy_from_branch_inventory(cls, branch, branch_inventory):
        _branch_inventory = branch_inventory
        _branch_inventory.pk = None
        if branch_inventory.branch:
            _branch_inventory.parent_branch = branch_inventory.branch
        _branch_inventory.branch = branch
        _branch_inventory.save()
        return _branch_inventory

    @property
    def last_price_paid(self):
        history = getattr(self, 'history', None)
        if history:
            return history.last_price_paid
        return 0

    @property
    def last_price_paid_with_vat(self):
        last_price_paid = float(self.last_price_paid) if self.last_price_paid else 0
        if self.vat_code and last_price_paid:
            vat_percentage = self.vat_code.percentage if self.vat_code.percentage else 0
            if vat_percentage > 0:
                vat_amount = (vat_percentage / 100) * last_price_paid
                return vat_amount + last_price_paid
        return last_price_paid

    @cached_property
    def gross_margin(self):
        selling_price = self.selling_price if self.selling_price else Decimal(0)
        last_price_paid = Decimal(self.last_price_paid) if self.last_price_paid else Decimal(0)
        return selling_price - last_price_paid

    @property
    def gross_margin_percentage(self):
        selling_price = self.selling_price if self.selling_price else Decimal(0)
        if selling_price > 0:
            return (self.gross_margin / selling_price) * 100
        return Decimal(0)

    @cached_property
    def calculate_average_price(self):
        return self.calculate_summary_values()


class PurchaseHistory(models.Model):
    inventory = models.OneToOneField(BranchInventory, related_name='history', on_delete=models.CASCADE)
    supplier = models.ForeignKey('supplier.Supplier', null=True, blank=True, on_delete=models.DO_NOTHING)
    supplier_code = models.CharField(max_length=50, null=True, blank=True,
                                     verbose_name='Supplier Product Code (if different from ours)',)
    update_pricing_at = models.CharField(max_length=100, null=True, blank=True)
    last_price_update = models.DateTimeField(null=True, blank=True)
    price_paid = models.JSONField(null=True, blank=True)
    average_price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    total_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    in_stock = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    on_picking = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    on_order = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    on_reserve = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    on_delivery = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    updated_at = models.DateTimeField(auto_now=True)

    class QS(models.QuerySet):

        def branch_valuation(self, branch: Branch):
            return self.filter(inventory__branch=branch).aggregate(
                total=Sum(Coalesce(F('total_value'), 0), output_field=DecimalField())
            )

    objects = QS.as_manager()

    @cached_property
    def last_price_paid(self):
        if self.price_paid:
            prices = self.price_paid.get('price', None)
            if prices and len(prices) > 0:
                return prices[0]
        return None

    @property
    def second_last_price_paid(self):
        if self.price_paid:
            prices = self.price_paid.get('price', None)
            if prices and len(prices) > 1:
                return prices[1]
        return None

    @property
    def third_last_price_paid(self):
        if self.price_paid:
            prices = self.price_paid.get('price', None)
            if prices and len(prices) > 2:
                return prices[2]
        return None

    @property
    def fourth_last_price_paid(self):
        if self.price_paid:
            prices = self.price_paid.get('price', None)
            if prices and len(prices) > 3:
                return prices[3]
        return None

    @cached_property
    def invoicable_stock(self):
        quantity = Decimal(0)
        if self.in_stock:
            quantity += Decimal(self.in_stock)
        if self.on_picking:
            quantity -= Decimal(self.on_picking)
        if self.on_order:
            quantity -= Decimal(self.on_order)
        return quantity

    @cached_property
    def available_stock(self):
        available_quantity = Decimal(0)
        if self.in_stock:
            available_quantity += Decimal(self.in_stock)
        if self.on_picking:
            available_quantity -= Decimal(self.on_picking)
        if self.on_delivery:
            available_quantity -= Decimal(self.on_delivery)
        if self.on_order:
            available_quantity -= Decimal(self.on_order)
        return available_quantity


# TODO - Ensure that correct yearly balances show on the ledger
class Balance(models.Model):
    inventory = models.ForeignKey(BranchInventory, related_name='balances', on_delete=models.CASCADE)
    year = models.ForeignKey('period.Year', related_name='inventory_balance', on_delete=models.CASCADE)
    opening_quantity = QuantityField()
    opening_price = MoneyField()
    opening_value = MoneyField()
    closing_quantity = QuantityField()
    closing_price = MoneyField()
    closing_value = MoneyField()


class Recipe(models.Model):
    limit = models.Q(is_recipe_item=True)

    inventory = models.OneToOneField(BranchInventory, related_name='recipe', on_delete=models.CASCADE,
                                     limit_choices_to=limit)
    description = models.TextField(null=True, default='')
    recipe_id = models.PositiveIntegerField(default=0)
    profile = models.ForeignKey('accounts.Profile', on_delete=models.DO_NOTHING, verbose_name='Created By',
                                null=True, blank=True)
    role = models.ForeignKey('accounts.Role', null=True, blank=True, verbose_name='Created By', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"RECIPE{str(self.recipe_id).rjust(4, '0')}"

    def save(self, *args, **kwargs):
        if not self.recipe_id:
            self.recipe_id = Recipe.generate_reference(inventory=self.inventory, counter=self.recipe_id)
        return super(Recipe, self).save(*args, **kwargs)

    @staticmethod
    def generate_reference(inventory, counter):
        result = Recipe.objects.select_related('inventory', 'inventory__branch').filter(
            inventory__branch=inventory.branch, recipe_id=counter).count()
        if result:
            counter = counter + 1
            return Recipe.generate_reference(inventory, counter)
        else:
            return counter

    @property
    def fullname(self):
        return f"{str(self.inventory)}-{self.description}"


class RecipeInventory(models.Model):
    limit = models.Q(is_recipe_item=False)
    recipe = models.ForeignKey(Recipe, related_name='recipe_items', on_delete=models.CASCADE)
    inventory = models.ForeignKey(BranchInventory, related_name='recipe_items', on_delete=models.CASCADE,
                                  limit_choices_to=limit)
    quantity = models.DecimalField(default=0, null=True, blank=True, decimal_places=4, max_digits=12)
    unit = models.ForeignKey('company.Unit', related_name='unit_recipes', on_delete=models.CASCADE)
    rate = models.DecimalField(default=1, null=True, blank=True, decimal_places=6, max_digits=12)

    def __str__(self):
        return f"{self.inventory}-{self.quantity} at {self.rate}/{self.unit}"

    def calculate_required_quantity(self, rate, quantity=1):
        quantity_required = Decimal(0)
        if rate and quantity and self.quantity:
            quantity_required = rate * quantity * self.quantity
        return Decimal(round(quantity_required, 4))


class PurchaseOrder(models.Model):
    invoice = models.OneToOneField('invoice.Invoice', related_name='inventory_purchase_order', on_delete=models.CASCADE)
    purchase_order_id = models.DecimalField(max_digits=10, decimal_places=1)
    memo_to_supplier = models.TextField(blank=True)
    parent = models.OneToOneField('self', related_name='back_order', null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ('-id', )

    def __str__(self):
        padding = settings.DEFAULT_PADDING
        return create_document_number(self.invoice.company, "PO", self.invoice.invoice_id, padding)

    def save(self, *args, **kwargs):
        if not self.pk and not self.purchase_order_id:
            filter_options = {'invoice__branch_id': self.invoice.branch.id}
            self.purchase_order_id = self.generate_invoice_number(1, filter_options)
        return super(PurchaseOrder, self).save(*args, **kwargs)

    def generate_invoice_number(self, counter, filter_options):
        try:
            purchase_order = PurchaseOrder.objects.filter(**filter_options).latest('purchase_order_id')
            try:
                purchase_order_id = int(purchase_order.purchase_order_id)
                return purchase_order_id + 1
            except ValueError as exc:
                return 1
        except PurchaseOrder.DoesNotExist as exc:
            return counter

    def get_invoice_at_status(self, status_slug):
        if not self.invoice.invoice_comments.exists():
            return ''
        comment = self.invoice.invoice_comments.filter(
            status__slug=status_slug
        ).first()
        if not comment:
            return ''
        return f"{comment.role}-{comment.participant.profile.code}"

    def get_ordered_by(self):
        return self.get_invoice_at_status('po-ordered')

    def get_created_by(self):
        return self.get_invoice_at_status('po-request')

    def get_approved_by(self):
        return self.get_invoice_at_status('po-approved')


class PurchaseOrderInventory(models.Model):

    OPEN = 1
    CLOSED = 2

    STATUSES = (
        (OPEN, 'Open'),
        (CLOSED, 'Closed')
    )

    purchase_order = models.ForeignKey(PurchaseOrder, null=True, blank=True, related_name='inventory_items',
                                       on_delete=models.CASCADE)
    invoice = models.ForeignKey('invoice.Invoice', null=True, blank=True, related_name='inventory_purchase_orders',
                                on_delete=models.CASCADE)
    inventory = models.ForeignKey(BranchInventory, related_name='purchase_orders', on_delete=models.CASCADE)
    quantity = models.DecimalField(default=0, null=True, blank=True, decimal_places=4, max_digits=12, verbose_name='Qty')
    unit = models.ForeignKey('company.Unit', null=True, blank=True, related_name='unit_purchase_order_items',
                             on_delete=models.CASCADE)
    rate = models.DecimalField(default=1, null=True, blank=True, decimal_places=6, max_digits=12)
    unit_price = models.DecimalField(default=0, decimal_places=2, max_digits=12, verbose_name='Unit Price')
    order_value = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Net value/each')
    vat_code = models.ForeignKey('company.VatCode', null=True, blank=True, related_name='vat_inventory_item_pos',
                                 on_delete=models.DO_NOTHING)
    vat_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    status = EnumIntegerField(enums.PurchaseOrderStatus, default=enums.PurchaseOrderStatus.OPEN)

    def __str__(self):
        return f"PO-{self.invoice}-{str(self.inventory)}-item"

    def mark_as_closed(self):
        self.status = PurchaseOrderInventory.CLOSED
        self.save()

    @property
    def net_amount(self):
        return self.unit_price * self.quantity

    @property
    def price_with_vat(self):
        return self.unit_price_with_vat * self.quantity

    @property
    def unit_price_with_vat(self):
        price_with_vat = self.unit_price
        if self.vat_code:
            price_with_vat = ((self.vat_code.percentage / 100) * float(self.unit_price)) + float(self.unit_price)
        return Decimal(price_with_vat)

    @property
    def total_price(self):
        return self.order_value

    @property
    def total_price_with_vat(self):
        return self.net_amount + self.amount_vat

    @property
    def amount_vat(self):
        vat_amount = 0
        if self.vat_code:
            vat_amount = (float(self.vat_code.percentage) / 100) * float(self.order_value)
        return Decimal(vat_amount)

    @property
    def total_vat_amount(self):
        return self.amount_vat * self.quantity

    @property
    def supplier_code(self):
        if self.inventory.supplier_code:
            return self.inventory.supplier_code
        return self.inventory


class Transaction(models.Model):
    PENDING = 1
    COMPLETED = 2

    STATUSES = (
        (PENDING, 'Pending'),
        (COMPLETED, 'Closed')
    )
    branch = models.ForeignKey('company.Branch', related_name='inventory_movements', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', null=True, blank=True, related_name='inventory_movements', on_delete=models.CASCADE)
    supplier = models.ForeignKey('supplier.Supplier', null=True, blank=True, related_name='inventory_movements', on_delete=models.CASCADE)
    module = EnumIntegerField(Module, default=Module.INVENTORY)
    date = models.DateField()
    note = models.TextField(default='', verbose_name='Delivery Note No.')
    total_amount = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Price Ex')
    total_vat = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Vat')
    status = EnumIntegerField(enums.TransactionStatus, default=enums.TransactionStatus.PENDING)
    movement_reference = models.CharField(max_length=255, null=True, blank=True, verbose_name='Reference')
    profile = models.ForeignKey('accounts.Profile', on_delete=models.DO_NOTHING, verbose_name='Created By', null=True, blank=True)
    role = models.ForeignKey('accounts.Role', on_delete=models.DO_NOTHING, null=True, blank=True, verbose_name='Created By')
    journal = models.OneToOneField('journals.Journal', related_name='goods_received', on_delete=models.DO_NOTHING,
                                   null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class AllReceivedManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset()


class ReceivedManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().exclude(supplier__isnull=True)

    def returnable(self):
        """
         Return received goods which have inventory items, are no linked to an invoice and status is pending
         Only Goods received General or Purchase Orders are returnable
        """
        return self.annotate(
            count_items=models.Sum('inventory_received'),
            is_linked=Coalesce(models.Count('invoice_account'), 0)
        ).filter(
            movement_type__in=[enums.MovementType.GENERAL, enums.MovementType.PURCHASE_ORDER],
            status=enums.TransactionStatus.PENDING
        ).filter(
            count_items__gt=0
        ).exclude(
            is_linked__gt=0
        )

    def for_branch_and_year(self, branch, year):
        return self.filter(
            branch=branch, period__period_year=year,
            movement_type__in=[enums.MovementType.PURCHASE_ORDER, enums.MovementType.GENERAL]
        ).prefetch_related(
            'returns'
        ).select_related(
            'invoice_account', 'invoice_account__invoice', 'invoice_account__invoice__company',
            'invoice_account__invoice__status', 'invoice_account__invoice__invoice_type', 'period',
            'period__period_year', 'supplier'
        ).prefetch_related(
            'inventory_received'
        ).order_by(
            '-created_at'
        )

    def awaiting_invoicing(self, branch: Branch, statuses: List[str]):
        q_objects = Q()
        q_objects.add(Q(movement_type__in=[enums.MovementType.GENERAL, enums.MovementType.PURCHASE_ORDER]), Q.AND)
        q_objects.add(Q(branch=branch), Q.AND)

        q_invoice_objects = Q()
        q_invoice_objects.add(Q(invoice_account__isnull=True), Q.OR)
        q_invoice_objects.add(Q(returns__invoice_account__isnull=True), Q.OR)
        q_invoice_objects.add(Q(invoice_account__invoice__status__slug__in=statuses), Q.OR)

        q_objects.add(q_invoice_objects, Q.AND)

        return self.filter(q_objects).exclude(status=Received.COMPLETED).distinct()


class Received(Transaction):

    purchase_order = models.OneToOneField('purchaseorders.PurchaseOrder', null=True, blank=True,
                                          related_name='inventory_received', on_delete=models.CASCADE,
                                          verbose_name='PO Reference')
    movement_type = EnumIntegerField(enums.MovementType, default=enums.MovementType.PURCHASE_ORDER)
    reference = models.PositiveIntegerField(default=1)

    objects = ReceivedManager()
    all = AllReceivedManager()

    class Meta:
        verbose_name = 'Received'
        verbose_name_plural = 'Received'

    def __str__(self):
        year_str = str(self.period.period_year.year)
        reference = self.reference
        if self.movement_type == enums.MovementType.OPENING_BALANCE:
            reference = '0'
        return f"{self.movement_str}{year_str[-2:]}-{str(reference).rjust(4, '0')}"

    def save(self, *args, **kwargs):
        if not self.date:
            self.date = datetime.now()
        if not self.movement_reference and self.period:
            year = self.period.period_year
            movement_types = [enums.MovementType.OPENING_BALANCE]
            self.reference = Received.generate_reference(self.branch, self.reference, year, movement_types)
        return super(Received, self).save(*args, **kwargs)

    @staticmethod
    def generate_reference(branch, counter, year, movement_types):
        result = Received.objects.filter(branch=branch, period__period_year=year).last()
        if result:
            counter = result.reference + 1
            return counter
        else:
            return counter

    def save_inventory_received(self, inventory, quantity, price):
        return InventoryReceived.objects.create(
            received=self,
            inventory=inventory,
            quantity=quantity,
            price_excluding=price
        )

    @cached_property
    def total_excl(self):
        total = Decimal(0)
        for inventory_item in self.inventory_received.all():
            if inventory_item.quantity and inventory_item.price_excluding:
                total += inventory_item.quantity * inventory_item.price_excluding
        return Decimal(total).quantize(Decimal('0.01'))

    @property
    def vat_total(self):
        total = Decimal(0)
        for inventory_item in self.inventory_received.all():
            total = Decimal(inventory_item.vat_amount)
        return total

    @property
    def movement_str(self):
        if self.movement_type == enums.MovementType.STOCK_ADJUSTMENT:
            return "STOCK-ADJ"
        if self.movement_type == enums.MovementType.BRANCH_TRANSFER:
            return "IBT"
        else:
            return "GREC"

    @property
    def document_type(self):
        if self.movement_type == enums.MovementType.STOCK_ADJUSTMENT:
            return "Goods Received"
        else:
            return "Goods Received"

    @property
    def has_invoice_account(self):
        try:
            if not hasattr(self, 'invoice_account') or self.invoice_account is None:
                return False
            else:
                return self.invoice_account
        except:
            return False

    @property
    def invoice(self):
        invoice_account = getattr(self, 'invoice_account', None)
        if invoice_account:
            return invoice_account.invoice
        return None

    @property
    def full_return(self):
        if self.status == enums.TransactionStatus.COMPLETED and self.returns.count() > 0:
            return self.returns.first()
        else:
            return False

    @property
    def returned(self):
        if self.full_return:
            return self.full_return
        return None

    @property
    def total_inc(self):
        total = Decimal('0')
        for moved in self.inventory_received.all():
            if moved.price_including:
                total += moved.price_including
        return total

    @property
    def link_amount(self):
        return self.total_excl


class ReturnedManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().exclude(supplier__isnull=True)

    def listable(self):
        return self.filter(invoice__isnull=True).exclude(
            movement_type__in=[enums.MovementType.STOCK_ADJUSTMENT, enums.MovementType.MANUFACTURING]
        )


class Returned(Transaction):
    received_movement = models.ForeignKey(Received, null=True, blank=True, related_name='returns', on_delete=models.CASCADE, verbose_name='GR Reference',)
    movement_type = EnumIntegerField(enums.MovementType, default=enums.MovementType.PURCHASE_ORDER)
    invoice = models.ForeignKey('sales.Invoice', null=True, blank=True, related_name='inventory_sold', on_delete=models.CASCADE)
    reference = models.PositiveIntegerField(default=1)

    objects = ReturnedManager()

    def __str__(self):
        if self.invoice:
            return f"{self.invoice}"
        else:
            year_str = str(self.period.period_year.year)
            return f"{self.movement_str}{year_str[-2:]}-{str(self.reference).rjust(4, '0')}"

    def save(self, *args, **kwargs):
        if not self.date:
            self.date = datetime.now()
        if not self.reference and self.period:
            year = self.period.period_year
            self.reference = Returned.generate_reference(self.branch, self.reference, year)
        return super(Returned, self).save(*args, **kwargs)

    @staticmethod
    def generate_reference(branch, counter, year):
        result = Returned.objects.filter(
            branch=branch, period__period_year=year
        ).count()
        if result:
            counter = counter + 1
            return Returned.generate_reference(branch, counter, year)
        else:
            return counter

    @cached_property
    def total_excl(self):
        total = Decimal(0)
        for inventory_item in self.inventory_returned.all():
            if inventory_item.quantity and inventory_item.price_excluding:
                total += inventory_item.quantity * inventory_item.price_excluding
        return total * Decimal(-1)

    @property
    def vat_total(self):
        total = Decimal(0)
        for inventory_item in self.inventory_returned.all():
            total = Decimal(inventory_item.vat_amount)
        return total

    @property
    def document_type(self):
        if self.movement_type == enums.MovementType.STOCK_ADJUSTMENT:
            return "Stock Adjustment"
        return 'Goods Returned'

    @property
    def movement_str(self):
        if self.movement_type == enums.MovementType.STOCK_ADJUSTMENT:
            return "ADJ"
        elif self.movement_type == enums.MovementType.GENERAL:
            return "GGRET"
        else:
            return "GRET"

    @property
    def total_inc(self):
        total = Decimal('0')
        for moved in self.inventory_returned.all():

            if moved.price_including:
                total += moved.price_including
        return total

    @property
    def has_invoice_account(self):
        return getattr(self, 'invoice_account', None)

    @property
    def link_amount(self):
        if self.total_excl:
            return self.total_excl * -1
        return Decimal(0)

    @property
    def return_amount(self):
        if self.total_excl:
            return self.total_excl * -1
        return Decimal(0)

    @property
    def total(self):
        return self.total_amount * -1 if self.total_amount else Decimal(0)

    @property
    def is_completed(self):
        return self.status == enums.TransactionStatus.COMPLETED and self.received_movement


class StockAdjustment(models.Model):
    branch = models.ForeignKey('company.Branch', related_name='adjustments', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', null=True, blank=True, related_name='adjustments', on_delete=models.CASCADE)
    date = models.DateField()
    comment = models.TextField()
    adjustment_id = models.IntegerField()
    status = EnumIntegerField(enums.TransactionStatus, default=enums.TransactionStatus.PENDING)
    profile = models.ForeignKey('accounts.Profile', on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Stock Adjustment'

    def __str__(self):
        return f"STOCK-ADJUSTMENT-{self.adjustment_id}"

    def save(self, *args, **kwargs):
        if not self.date:
            self.date = datetime.now()
        if not self.adjustment_id:
            year = self.period.period_year
            filters = {'branch': self.branch, 'period__period_year': year}
            self.adjustment_id = StockAdjustment.generate_reference(filters)
        return super(StockAdjustment, self).save(*args, **kwargs)

    @staticmethod
    def generate_reference(filters):
        result = StockAdjustment.objects.filter(**filters).last()
        if result:
            filters['adjustment_id'] = result.adjustment_id + 1
            return StockAdjustment.generate_reference(filters)
        else:
            return filters.get('adjustment_id', 1)

    @property
    def total(self):
        return sum(inventory_adjustment.total for inventory_adjustment in self.inventories.all())


class StockAdjustmentInventory(models.Model):
    adjustment = models.ForeignKey(StockAdjustment, related_name='inventories', on_delete=models.CASCADE)
    inventory = models.ForeignKey(BranchInventory, on_delete=models.CASCADE)
    account = models.ForeignKey('company.Account', on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    price_type = EnumIntegerField(enums.PriceType, null=True, blank=True)
    returns = GenericRelation('InventoryReceivedReturned', 'returned_object_id', 'returned_type')
    received = GenericRelation('InventoryReceivedReturned', 'received_object_id', 'received_type')

    def __str__(self):
        return f"Inventory {self.inventory} adjustment {str(self.adjustment)}"

    @property
    def total(self):
        amount = 0
        if self.price and self.quantity:
            amount = self.quantity * self.price
        return Decimal(amount)


class InventoryReceivedManager(models.Manager):

    def create_received_inventory(self, transaction, inventory, unit_price, quantity, ordered=None, price_including=None,
                                  vat_code=None, unit_id=None, is_back_order=False, short=0, opening_balance=0):
        data = {
            'received': transaction,
            'inventory': inventory,
            'price_excluding': unit_price,
            'price_including': price_including,
            'ordered': ordered,
            'quantity': quantity,
            'short': short,
            'vat': vat_code,
            'is_back_order': is_back_order
        }
        if isinstance(unit_id, int):
            data['unit_id'] = unit_id
        inventory_received = InventoryReceived.objects.create(**data)
        if inventory_received:
            self.receive_inventory(inventory_received)

        return inventory_received

    # noinspection PyMethodMayBeStatic
    def receive_inventory(self, inventory_received, content_object=None, content_type=None):
        inventory_stock = inventory_received.inventory.in_stock
        opening_stock = inventory_stock
        returned_not_received = InventoryReceivedReturned.objects.filter(
            inventory_received__isnull=True, inventory_returned__inventory=inventory_received.inventory
        )
        outstanding = inventory_received.quantity
        received_quantity = outstanding
        if returned_not_received:
            for returned in returned_not_received:
                if outstanding > 0 and returned.quantity:
                    if outstanding >= returned.quantity:
                        returned.inventory_received = inventory_received
                        returned.inventory = inventory_received.inventory
                        returned.actual_price = inventory_received.price_excluding
                        returned.received_type = content_type
                        returned.received_object_id = content_object.id if content_object else None
                        returned.data = utils.prepare_opening_closing_balance(inventory_received, opening_stock, returned.quantity, returned.data)
                        returned.save()
                        inventory_stock += returned.quantity
                        outstanding -= returned.quantity
                        opening_stock += returned.quantity
                    else:
                        returned.inventory_received = inventory_received
                        returned.actual_price = inventory_received.price_excluding
                        returned.inventory = inventory_received.inventory
                        returned.received_type = content_type
                        returned.received_object_id = content_object.id if content_object else None
                        returned.data = utils.prepare_opening_closing_balance(
                            inventory_received, opening_stock, outstanding, returned.data
                        )
                        returned.save()
                        outstanding = 0
                        inventory_stock += returned.quantity
                        opening_stock += returned.quantity

        if outstanding > 0:
            inventory_stock += outstanding
            InventoryReceivedReturned.objects.create(
                inventory_received=inventory_received,
                inventory=inventory_received.inventory,
                received_type=content_type,
                received_object_id=content_object.id if content_object else None,
                quantity=outstanding,
                price=inventory_received.price_excluding,
                opening_balance=opening_stock,
                balance=inventory_stock,
                data=utils.prepare_opening_closing_balance(inventory_received, opening_stock, outstanding)
            )

    def branch(self, branch):
        return self.get_queryset().filter(received__branch=branch)

    def ledger(self, branch, year):
        return self.branch(branch).filter(
            received__period__period_year=year, received__movement_type=enums.MovementType.OPENING_BALANCE
        ).exclude(
            inventory__sku_type=enums.SkuType.SERVICE
        )

    def available_items(self, inventory):
        return self.annotate(
            quantity_returned=models.Case(
                models.When(returned_items__inventory_returned__isnull=False,
                            then=Coalesce(models.Sum('returned_items__quantity'), 0),
                            ),
                default=0,
                output_field=models.DecimalField()
            ),
            received_quantity_available=Coalesce(models.F('quantity') - models.F('quantity_returned'), 0)
        ).filter(
            inventory=inventory
        )

    def opening_balances(self, branch, year, **filters):
        return self.ledger(branch, year).select_related(
            'inventory', 'inventory__inventory', 'inventory__history'
        ).prefetch_related(
            'inventory__group_level_items', 'inventory__group_level_items__group_level'
        ).filter(
            **filters
        ).order_by(
            'inventory__inventory__code'
        )


class InventoryReceived(models.Model):
    received = models.ForeignKey(Received, related_name='inventory_received', on_delete=models.CASCADE)
    inventory = models.ForeignKey(BranchInventory, related_name='inventory_received', on_delete=models.CASCADE)
    invoice_item = models.ForeignKey('sales.InvoiceItem', related_name='inventory_bought', null=True, blank=True, on_delete=models.CASCADE)
    ordered = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12, verbose_name='Quantity Ordered')
    quantity = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12, verbose_name='Quantity Received')
    short = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12, verbose_name='Quantity Short')
    unit = models.ForeignKey('company.Unit', null=True, blank=True, related_name='unit_inventory_received', on_delete=models.CASCADE)
    rate = models.DecimalField(default=1, null=True, blank=True, decimal_places=6, max_digits=12)
    vat = models.ForeignKey('company.VatCode', null=True, blank=True, on_delete=models.DO_NOTHING)
    price_excluding = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Price Ex')
    price_including = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Price Inc')
    is_back_order = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = InventoryReceivedManager()

    def __str__(self):
        return f"Received {self.quantity} of {self.inventory} @ {self.price_excluding}"

    def update_received_inventory(self, inventory, quantity):
        InventoryReceivedReturned.objects.create(
            inventory_received=self,
            inventory=inventory,
            quantity=quantity,
            price=inventory.cost_price
        )

    def receive_returned(self, content_object=None, content_type=None):
        returned_inventory = InventoryReceivedReturned.objects.filter(
            inventory_received__isnull=True, inventory_returned__inventory=self.inventory
        )
        received_quantity = self.quantity
        quantity_outstanding = received_quantity
        for received_returned in returned_inventory:
            if received_quantity > 0 and received_returned.quantity:
                returned_quantity = received_returned.quantity
                if received_quantity >= returned_quantity:
                    received_returned.inventory_received = self
                    received_returned.received_object_id = content_object.id
                    received_returned.received_type = content_type
                    received_returned.actual_price = self.price_excluding
                    received_returned.inventory = self.inventory
                    received_returned.save()
                    received_quantity -= returned_quantity
                    quantity_outstanding -= returned_quantity
                else:
                    received_returned.inventory_received = self
                    received_returned.received_object_id = content_object.id
                    received_returned.received_type = content_type
                    received_returned.actual_price = self.price_excluding
                    received_returned.inventory = self.inventory
                    received_returned.save()
                    outstanding_quantity = returned_quantity - received_quantity
                    received_quantity = 0
                    quantity_outstanding -= 0

                    if outstanding_quantity > 0:
                        return_content_type = ContentType.objects.get_for_model(received_returned.inventory_returned)
                        InventoryReceivedReturned.objects.create(
                            quantity=outstanding_quantity,
                            inventory_returned=received_returned.inventory_returned,
                            returned_type=return_content_type,
                            returned_object_id=received_returned.inventory_returned.id,
                            price=received_returned.price,
                            inventory=received_returned.inventory
                        )

    @property
    def price_incl(self):
        price = 0
        if self.price_excluding and self.quantity:
            price = self.price_excluding
        return Decimal(price).quantize(Decimal('0.01'))

    @property
    def vat_amount(self):
        amount = 0
        if self.vat.percentage and self.vat.percentage > 0:
            amount = round((float(self.vat.percentage) / 100 * float(self.total_price_excluding)), 2)
        return Decimal(amount).quantize(Decimal('0.01'))

    @property
    def total_price_excluding(self):
        total = 0
        if self.price_excluding and self.quantity:
            total = Decimal(self.quantity) * self.price_excluding
        return Decimal(total).quantize(Decimal('0.01'))

    @property
    def total_price_including(self):
        total = self.total_price_excluding
        if self.price_including and self.quantity:
            total = Decimal(self.quantity) * self.price_including
        return Decimal(total).quantize(Decimal('0.01'))


class InventoryReturnedManager(models.Manager):

    def closing_balance(self, branch, year, inventory):
        return self.select_related(
            'inventory', 'inventory__inventory', 'inventory__history'
        ).prefetch_related(
            'inventory__group_level_items', 'inventory__group_level_items__group_level'
        ).filter(
            returned__period__period_year=year, returned__movement_type=enums.MovementType.CLOSING_BALANCE,
            returned__branch=branch, inventory=inventory
        ).exclude(
            inventory__sku_type=enums.SkuType.SERVICE
        ).order_by(
            'inventory__inventory__code'
        )

    def update_closing_balance(self, branch, year, inventory, quantity, price):
        closing_balance, is_new = InventoryReturned.objects.update_or_create(
            inventory=inventory, returned__branch=branch, returned__period__period_year=year,
            returned__movement_type=enums.MovementType.CLOSING_BALANCE,
            defaults={
                'quantity': quantity,
                'price_excluding': price
            }
        )
        return closing_balance

    def create_returned(self, transaction, inventory, price, quantity, price_including=0, vat_code_id=None, inventory_received=None):
        inventory_returned = InventoryReturned.objects.create(
            returned=transaction,
            inventory=inventory,
            price_excluding=price,
            price_including=price_including,
            quantity=quantity,
            vat_id=vat_code_id
        )
        if inventory_returned:
            received_returned = None
            if inventory_received:
                received_returned = InventoryReceivedReturned.objects.filter(
                    inventory_received=inventory_received, inventory_returned__isnull=True
                ).first()

            inventory_stock = inventory.in_stock
            quantity_outstanding = quantity
            if received_returned:
                received_quantity = received_returned.quantity
                received_outstanding = received_quantity - quantity

                received_returned.inventory_returned = inventory_returned
                received_returned.quantity = quantity
                received_returned.data = utils.prepare_opening_closing_balance(inventory_returned, inventory_stock,
                                                                               received_quantity * -1, received_returned.data)
                received_returned.save()
                inventory_stock -= received_quantity

                if received_outstanding > 0:
                    InventoryReceivedReturned.objects.create(
                        inventory_received=inventory_received,
                        inventory=inventory_received.inventory,
                        quantity=received_outstanding,
                        data=utils.prepare_opening_closing_balance(inventory_received, inventory_stock,
                                                                   received_outstanding),
                        price=price
                    )
                    quantity_outstanding = received_outstanding - quantity
                    inventory_stock += received_outstanding
                price = received_returned.price

            if quantity_outstanding > 0:
                InventoryReceivedReturned.objects.create(
                    inventory_returned=inventory_returned,
                    inventory=inventory_returned.inventory,
                    quantity=quantity_outstanding,
                    data=utils.prepare_opening_closing_balance(inventory_returned, inventory_stock,
                                                               quantity_outstanding * -1),
                    price=price
                )

        return inventory_returned


class InventoryReturned(models.Model):
    returned = models.ForeignKey(Returned, related_name='inventory_returned', on_delete=models.CASCADE)
    inventory = models.ForeignKey(BranchInventory, related_name='inventory_returned', on_delete=models.CASCADE)
    invoice_item = models.ForeignKey('sales.InvoiceItem', related_name='inventory_sold', null=True, blank=True, on_delete=models.CASCADE)
    quantity = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12, verbose_name='Quantity Returned')
    unit = models.ForeignKey('company.Unit', null=True, blank=True, related_name='unit_inventory_returned', on_delete=models.CASCADE)
    rate = models.DecimalField(default=1, null=True, blank=True, decimal_places=6, max_digits=12)
    vat = models.ForeignKey('company.VatCode', null=True, blank=True, on_delete=models.DO_NOTHING)
    price_excluding = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Price Ex')
    price_including = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Price Inc')
    created_at = models.DateTimeField(auto_now_add=True)
    manufacturing_recipe = models.ForeignKey('ManufacturingRecipe', null=True, blank=True,
                                             related_name='manufactured_items', on_delete=models.CASCADE)

    objects = InventoryReturnedManager()

    class Meta:
        ordering = ('id', )

    def __str__(self):
        return f"Returned {self.quantity} at {self.price_excluding}"

    @property
    def price_incl(self):
        price = 0
        if self.price_excluding and self.quantity:
            price = self.price_excluding
        return Decimal(price)

    @property
    def total_price_excluding(self):
        total = 0
        if self.price_excluding and self.quantity:
            total = self.quantity * self.price_excluding
        return Decimal(total)

    @property
    def vat_amount(self):
        amount = 0
        if self.vat and self.vat.percentage and self.vat.percentage > 0:
            amount = round((float(self.vat.percentage) / 100) * float(self.total_price_excluding), 2) * -1
        return Decimal(amount)

    @property
    def total_price_including(self):
        total = self.total_price_excluding * -1
        if self.price_including and self.quantity:
            total = self.quantity * self.price_including
        return Decimal(total)

    @property
    def total_cost(self):
        total_cost_price = 0
        for i_returned in self.return_received_items.all():
            if i_returned and i_returned.inventory_received:
                total_cost_price += i_returned.inventory_received.price_excluding * i_returned.quantity
        return total_cost_price


class InventoryReceivedReturned(models.Model):
    inventory_received = models.ForeignKey(InventoryReceived, null=True, blank=True, related_name='returned_items',
                                           on_delete=models.CASCADE)
    inventory_returned = models.ForeignKey(InventoryReturned, null=True, blank=True,
                                           related_name='return_received_items', on_delete=models.CASCADE)
    inventory = models.ForeignKey(BranchInventory, null=True, blank=True, related_name='fifo', on_delete=models.CASCADE)
    quantity = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)

    received_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.CASCADE,
                                      related_name='received')
    received_object_id = models.PositiveIntegerField(null=True, blank=True)
    received_object = GenericForeignKey('received_type', 'received_object_id')
    received_period = models.ForeignKey('period.Period', null=True, related_name='receivables', on_delete=models.PROTECT)

    returned_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.CASCADE,
                                      related_name='returns')
    returned_object_id = models.PositiveIntegerField(null=True, blank=True)
    returned_object = GenericForeignKey('returned_type', 'returned_object_id')
    returned_period = models.ForeignKey('period.Period', null=True, related_name='returns', on_delete=models.PROTECT)

    balance = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    opening_balance = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)

    # TODO Remove this field, once inventory stock balance logging is working
    data = models.JSONField(null=True)

    price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    actual_price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    returned_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    received_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)

    class QS(models.QuerySet):

        def get_available_items(self, inventory):
            return self.select_related(
                'inventory_received', 'inventory_received__received'
            ).filter(
                inventory_received__inventory=inventory,
                inventory_returned__isnull=True
            ).order_by(
                'inventory_received__id'
            )

        def returned_not_received(self, inventory):
            return self.filter(
                inventory=inventory, received_object_id__isnull=False, returned_object_id__isnull=True
            )

        def received_not_returned(self, inventory):
            return self.filter(
                inventory=inventory, received_object_id__isnull=False, returned_object_id__isnull=True
            )

        def calculate_received_total(self, content_type, content_object):
            return self.filter(
                received_type=content_type, received_object_id=content_object.id
            ).aggregate(
                total_amount=Sum(F('quantity') * F('price'))
            )

    objects = QS().as_manager()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return f"{self.inventory} received by {str(self.received_object)}, returned by {str(self.returned_object)}, a movement of {self.quantity} @ {self.price}"

    def stock_movement(self, inventory_transaction):
        if self.data:
            data = self.data.get(str(inventory_transaction.id), None)
            if data:
                return data.get('opening', 0), data.get('closing', 0)
        return 0, 0

    @property
    def total_cost(self):
        return self.price * self.quantity

    @property
    def each_price(self):
        amount = self.price
        if hasattr(self.inventory_returned, 'manufacturing_recipe') and self.inventory_returned.manufacturing_recipe:
            amount = round(self.total_cost / self.inventory_returned.manufacturing_recipe.quantity, 2)
        return Decimal(amount)

    @property
    def total(self):
        amount = self.price * self.quantity
        if hasattr(self.inventory_returned, 'manufacturing_recipe') and self.inventory_returned.manufacturing_recipe:
            amount = round(self.each_price * self.inventory_returned.manufacturing_recipe.quantity, 2)
        return Decimal(amount)

    def value_returned(self):
        if self.returned_value:
            return self.returned_value
        else:
            return self.quantity * self.price if self.price else 0

    def value_received(self):
        if self.received_value:
            return self.received_value
        else:
            return self.quantity * self.price if self.price else 0


class StockCount(models.Model):
    branch = models.ForeignKey('company.Branch', related_name='stock_counts', on_delete=models.CASCADE)
    captured_by = models.CharField(max_length=255, verbose_name='Who is counting stock?')
    date = models.DateField(verbose_name='Date of Stock Take')
    count_type = EnumIntegerField(enums.StockCountType, default=enums.StockCountType.ALL_STOCK)
    count = models.PositiveIntegerField(null=True, blank=True)
    show_stock_on_hand = models.BooleanField(default=False, verbose_name='Show Stock on Hand on Count Sheet')
    status = EnumIntegerField(enums.StockCountStatus, default=enums.StockCountStatus.GENERATED)
    ledger = models.OneToOneField('journals.Journal', null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return f"CS{str(self.id).rjust(settings.DEFAULT_PADDING, '0')}"  # Count sheet

    def mark_as_counted(self):
        self.status = enums.StockCountStatus.COUNTED
        self.save()

    def mark_as_adjusted(self):
        self.status = enums.StockCountStatus.ADJUSTED
        self.save()

    @property
    def count_type_name(self):
        return self.count_type

    @property
    def status_name(self):
        return self.status

    @property
    def is_generated(self):
        return self.status == enums.StockCountStatus.GENERATED

    @property
    def is_counted(self):
        return self.status == enums.StockCountStatus.COUNTED

    @property
    def is_adjusted(self):
        return self.status == enums.StockCountStatus.ADJUSTED

    @property
    def is_random(self):
        return self.count_type == enums.StockCountType.RANDOM

    @property
    def is_highest_item_value(self):
        return self.count_type == enums.StockCountType.HIGHEST_ITEM_VALUE

    @property
    def is_highest_total_value(self):
        return self.count_type == enums.StockCountType.HIGHEST_TOTAL_VALUE

    @property
    def is_highest_quantity(self):
        return self.count_type == enums.StockCountType.HIGHEST_QUANTITY

    @property
    def is_selected_stock(self):
        return self.count_type == enums.StockCountType.SELECT_STOCK

    @property
    def has_inventories(self):
        return self.inventory_items.count()


class InventoryStockCountManager(models.Manager):

    def with_variances(self, stock_count):
        return self.get_queryset().annotate(
            variance=F('quantity') - F('original_quantity')
        ).exclude(
            variance=0
        ).filter(
            stock_count=stock_count
        )


class InventoryStockCount(models.Model):
    stock_count = models.ForeignKey(StockCount, related_name='inventory_items', on_delete=models.CASCADE)
    inventory = models.ForeignKey(BranchInventory, related_name='stock_counts', on_delete=models.CASCADE)
    original_quantity = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    quantity = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12,)
    recount_amount = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    second_recount_amount = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    objects = InventoryStockCountManager()

    @property
    def calculated_variance(self):
        return self.quantity - self.original_quantity

    @property
    def count_variance(self):
        return self.original_quantity - self.quantity

    @property
    def total_value(self):
        total = 0
        if self.price and self.count_variance:
            total = self.price * self.count_variance
        return Decimal(total)


class LedgerManager(models.Manager):

    def sku(self, branch):
        return self.branch(branch).exclude(inventory__sku_type=BranchInventory.SERVICE)

    def branch(self, branch):
        return self.get_queryset().filter(inventory__branch=branch)

    def inventory_ledger(self, branch, year, **filters):
        return self.sku(branch).filter(**filters).exclude(
            inventory__is_blocked=True
        ).annotate(
            line_total=Case(
                When(total_value__isnull=False, then=F('total_value') * F('rate')),
                When(total_value__isnull=True, then=F('value') * F('quantity') * F('rate')),
                output_field=models.DecimalField()
            ),
            closing_value=Coalesce(
                models.Subquery(
                    Balance.objects.filter(inventory_id=models.OuterRef('id'), year=year).values('closing_value')[:1]
                ), 0),
            closing_price=Coalesce(
                models.Subquery(
                    Balance.objects.filter(inventory_id=models.OuterRef('id'), year=year).values('closing_price')[:1]
                ), 0),
            closing_quantity=Coalesce(
                models.Subquery(
                    Balance.objects.filter(inventory_id=models.OuterRef('id'), year=year).values('closing_quantity')[:1]
                ), 0),
            opening_quantity=Coalesce(
                models.Subquery(
                    InventoryReceived.objects.filter(
                        received__period__period_year=year, received__movement_type=enums.MovementType.OPENING_BALANCE,
                        inventory_id=models.OuterRef('id')
                    ).values('quantity')[:1]
                ), 0),
            opening_price=Coalesce(
                models.Subquery(
                    InventoryReceived.objects.filter(
                        received__period__period_year=year, received__movement_type=enums.MovementType.OPENING_BALANCE,
                        inventory_id=models.OuterRef('id')
                    ).values('price_excluding')[:1]
                ), 0),
            opening_value=Coalesce(F('opening_price'), 0) * Coalesce(F('opening_quantity'), 0)
        ).select_related(
            'supplier', 'period', 'inventory', 'inventory__inventory', 'journal', 'content_type',
            'inventory__history'
        ).prefetch_related(
            'inventory__group_level_items', 'inventory__group_level_items__group_level'
        ).order_by(
            'inventory__inventory__code', 'date', 'created_at', 'text'
        )

    def get_object_ledger(self, content_type, object_id):
        return self.filter(content_type=content_type, object_id=object_id).first()

    def add_invoice_item_line(self, invoice_item, module, unit_price, profile, quantity=None, supplier=None,
                              status=None, is_new=True):
        if status is None:
            status = enums.LedgerStatus.PENDING
        if quantity is None:
            if invoice_item.invoice.is_credit and invoice_item.quantity < 0:
                quantity = invoice_item.quantity * -1
            else:
                quantity = invoice_item.quantity
        if quantity > 0:
            is_return = not invoice_item.invoice.is_credit
            if is_return:
                quantity = quantity * -1
                unit_price = unit_price * -1 if unit_price else 0
            content_type = ContentType.objects.get_for_model(invoice_item)
            ledger = self.get_object_ledger(content_type, invoice_item.id)

            if ledger and not is_new:
                ledger.status = status
                ledger.quantity = quantity
                ledger.value = unit_price
                ledger.status = status
                ledger.save()
            if is_new:
                ledger = Ledger.objects.create(
                    inventory=invoice_item.inventory,
                    quantity=quantity,
                    value=unit_price,
                    created_by=profile,
                    period=invoice_item.invoice.period,
                    date=invoice_item.invoice.invoice_date,
                    supplier=supplier,
                    text=str(invoice_item.invoice),
                    module=module,
                    status=status,
                    rate=-1 if is_return else 1,
                    object_id=invoice_item.id,
                    content_type=content_type,
                    object_owner=str(invoice_item.invoice.customer.name),
                    object_reference=str(invoice_item.invoice)
                )
            return ledger
        return None

    def add_branch_transfer_line(self, transaction, inventory, quantity, price, text, is_return=False):
        if is_return:
            content_type = ContentType.objects.filter(app_label='inventory', model='returned').first()
        else:
            content_type = ContentType.objects.filter(app_label='inventory', model='received').first()
        ledger = Ledger.objects.create(inventory=inventory,
                                       quantity=quantity,
                                       value=price,
                                       date=transaction.date,
                                       period=transaction.period,
                                       created_by=transaction.profile,
                                       text=text,
                                       content_type=content_type,
                                       object_id=transaction.id,
                                       object_reference=str(transaction),
                                       object_owner=str(transaction.branch)
                                       )
        return ledger


class Ledger(models.Model):
    PENDING = 0
    COMPLETE = 1

    STATUSES = (
        (PENDING, 'Pending'),
        (COMPLETE, 'Complete')
    )

    limit = models.Q(app_label='inventory', model='movement') | \
            models.Q(app_label='inventory', model='inventorymovement') |\
            models.Q(app_label='inventory', model='inventorystockadjustment') | \
            models.Q(app_label='inventory', model='stockadjustment')

    inventory = models.ForeignKey(BranchInventory, related_name='ledgers', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', related_name='period_inventory_ledgers', null=True, blank=True, on_delete=models.DO_NOTHING)
    quantity = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=12)
    value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    total_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    rate = models.DecimalField(default=1, null=True, blank=True, decimal_places=6, max_digits=12)
    journal = models.ForeignKey('journals.Journal', null=True, blank=True, on_delete=models.CASCADE, related_name='inventory_ledgers')
    module = EnumIntegerField(Module, null=True, blank=True)
    content_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    object_owner = models.CharField(max_length=255, blank=True)
    object_reference = models.CharField(max_length=255, blank=True)
    text = models.CharField(max_length=255, null=True, blank=True)
    journal_number = models.CharField(max_length=255, null=True, blank=True)
    supplier = models.ForeignKey('supplier.Supplier', null=True, blank=True, on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey('accounts.Profile', on_delete=models.DO_NOTHING)
    date = models.DateField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = EnumIntegerField(enums.LedgerStatus, default=enums.LedgerStatus.COMPLETE)

    objects = LedgerManager()

    class Meta:
        ordering = ('id', 'date')

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        if not self.journal_number:
            year = self.period.period_year
            company = self.inventory.inventory.company
            self.journal_number = Ledger.generate_journal_number(company, year, 1)
        return super(Ledger, self).save(*args, **kwargs)

    @property
    def model_object(self):
        return self.content_object

    @property
    def customer(self):
        if self.content_type.model == 'invoice':
            invoice = self.content_type.get_object_for_this_type(pk=self.object_id)
            return invoice.customer.name
        return ''

    @property
    def is_return(self):
        if isinstance(self.content_object, Returned):
            return True
        return False

    @property
    def is_pending(self):
        return self.status == enums.LedgerStatus.PENDING

    @property
    def is_completed(self):
        return self.status == enums.LedgerStatus.COMPLETE

    @property
    def total(self):
        value = 0
        if self.total_value:
            value = self.total_value * self.rate
        elif self.value and self.quantity:
            value = self.value * self.quantity * self.rate
        return Decimal(value)

    @staticmethod
    def generate_journal_number(company, year, counter=1):
        journal_number = counter
        result = Ledger.objects.filter(inventory__inventory__company=company, period__period_year=year).last()
        if result and result.journal_number:
            counter = int(result.journal_number) + 1
            return counter
        else:
            return journal_number


class ManufacturingManager(models.Manager):

    def get_detail(self, manufacturing_id):
        return self.annotate(
            total_cost=Sum(
                F('recipe_items__manufactured_items__return_received_items__quantity') *
                F('recipe_items__manufactured_items__return_received_items__price')
            )
        ).select_related(
            'branch'
        ).prefetch_related(
            'recipe_items', 'recipe_items__recipe', 'recipe_items__recipe__inventory',
            'recipe_items__recipe__inventory__inventory',  'recipe_items__manufactured_items',
            'recipe_items__manufactured_items__inventory', 'recipe_items__manufactured_items__inventory__inventory',
            'recipe_items__manufactured_items__return_received_items',
            'recipe_items__manufactured_items__return_received_items__inventory_received',
            'recipe_items__manufactured_items__return_received_items__inventory_received__received',
            'recipe_items__manufactured_items__return_received_items__inventory_received__received__period',
            'recipe_items__manufactured_items__return_received_items__inventory_received__received__period__period_year'
        )


class Manufacturing(models.Model):
    branch = models.ForeignKey('company.Branch', related_name='manufacturing', on_delete=models.DO_NOTHING)
    description = models.TextField(null=True, blank=True)
    manufacturing_id = models.PositiveIntegerField(default=1)
    date = models.DateField(null=False)
    period = models.ForeignKey('period.Period', on_delete=models.DO_NOTHING, null=True, blank=True)
    profile = models.ForeignKey('accounts.Profile', on_delete=models.DO_NOTHING, verbose_name='Created By', null=True, blank=True)
    role = models.ForeignKey('accounts.Role', on_delete=models.DO_NOTHING, null=True, blank=True, verbose_name='Created By')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = EnumIntegerField(enums.ManufacturingStatus, default=enums.ManufacturingStatus.COMPLETED)
    failed_message = models.TextField(blank=True)

    objects = ManufacturingManager()

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return f"MANU{str(self.manufacturing_id).rjust(4, '0')}"

    # noinspection PyMethodMayBeStatic
    def get_absolute_url(self):
        return reverse('manufacturing_detail', args=(self.pk, ))

    def save(self, *args, **kwargs):
        if not self.pk:
            self.manufacturing_id = Manufacturing.generate_reference(self.branch, 1)
        return super(Manufacturing, self).save(*args, **kwargs)

    def complete(self):
        self.status = enums.ManufacturingStatus.COMPLETED
        self.save()
        return self

    @property
    def is_processing(self):
        return self.status == enums.ManufacturingStatus.PROCESSING

    @staticmethod
    def generate_reference(branch, counter):
        result = Manufacturing.objects.filter(branch=branch).first()
        if result:
            return int(result.manufacturing_id) + 1
        else:
            return counter


class ManufacturingRecipe(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='manufactured', on_delete=models.CASCADE)
    manufacturing = models.ForeignKey(Manufacturing, related_name='recipe_items', on_delete=models.CASCADE)
    quantity = models.DecimalField(default=0, null=True, blank=True, decimal_places=4, max_digits=12)
    rate = models.DecimalField(default=1, null=True, blank=True, decimal_places=6, max_digits=12)
    value = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    returns = GenericRelation('InventoryReceivedReturned', 'returned_object_id', 'returned_type')
    received = GenericRelation('InventoryReceivedReturned', 'received_object_id', 'received_type')
