from collections import OrderedDict

from django import forms
from django.urls import reverse_lazy
from django_select2.forms import HeavySelect2Widget

from docuflow.apps.common.widgets import DataAttributesSelect
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.company.models import Account, VatCode, Branch, Unit, Measure
from docuflow.apps.supplier.models import Supplier
from docuflow.apps.period.models import Period
from .models import Inventory, BranchInventory, PurchaseOrderInventory
from .enums import SkuType


class InventoryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.branch = kwargs.pop('branch')
        self.request = kwargs.pop('request')
        super(InventoryForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(InventoryForm, self).clean()
        inventory = BranchInventory.all_inventory.filter(
            inventory__code__iexact=cleaned_data['code'], branch=self.branch
        ).first()
        if inventory:
            if inventory.is_blocked:
                self.add_error('code', f"Inventory code {self.cleaned_data['code']} exists in {self.branch},"
                                       f" and is blocked.")
            else:
                self.add_error('code', f"Inventory code {self.cleaned_data['code']} exists for {self.company} ")
        return cleaned_data

    is_copied = forms.BooleanField(required=False)

    class Meta:
        model = Inventory
        fields = ('code', 'description', 'is_copied')

    def save(self, commit=True):
        inventory = super(InventoryForm, self).save(commit=False)
        inventory.company = self.company
        inventory.save()

        self.add_branch_inventories(inventory)

        self.request.session['is_copied'] = self.cleaned_data['is_copied']

        return inventory

    def add_branch_inventories(self, inventory: Inventory):
        is_copied = self.cleaned_data['is_copied']
        branch_inventories = [BranchInventory(inventory=inventory, branch=self.branch, is_blocked=True)]
        if is_copied:
            branches = Branch.objects.exclude(id=self.branch.pk)
            for branch in branches:
                branch_inventories.append(
                    BranchInventory(
                        inventory=inventory,
                        branch=branch,
                        parent_branch=self.branch,
                        is_blocked=True
                    )
                )
        BranchInventory.objects.bulk_create(branch_inventories)


class BranchInventoryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.request = kwargs.pop('request')
        self.inventory = None
        if 'inventory' in kwargs:
            self.inventory = kwargs.pop('inventory')

        super(BranchInventoryForm, self).__init__(*args, **kwargs)
        vat_codes = VatCode.objects.active(self.branch.company.id)

        units = Unit.objects.filter(measure__company=self.branch.company)
        accounts = Account.objects.filter(company=self.branch.company, is_active=True)
        suppliers = Supplier.objects.filter(company=self.branch.company, is_active=True)
        measures = Measure.objects.filter(company=self.branch.company)

        vat_code_data = {'data-calculate-vat-url': {'': ''}, 'data-percentage': {'': ''}}
        for vat_code in vat_codes:
            vat_code_data['data-calculate-vat-url'][vat_code.id] = reverse_lazy('customer_details',
                                                                                kwargs={'pk': vat_code.id})
            vat_code_data['data-percentage'][vat_code.id] = vat_code.percentage

        self.fields['supplier'].queryset = suppliers
        self.fields['sales_account'].queryset = accounts
        self.fields['cost_of_sales_account'].queryset = accounts
        self.fields['gl_account'].queryset = accounts
        self.fields['stock_adjustment_account'].queryset = accounts
        self.fields['service_account'].queryset = accounts

        self.fields['vat_code'].widget = DataAttributesSelect(
            choices=[('', '-----------')] + [(vc.id, str(vc)) for vc in vat_codes], data=vat_code_data)
        self.fields['sale_vat_code'].widget = DataAttributesSelect(
            choices=[('', '-----------')] + [(vc.id, str(vc)) for vc in vat_codes], data=vat_code_data)

        self.fields['unit'].queryset = units
        self.fields['sale_unit'].queryset = units
        self.fields['measure'].queryset = measures
        self.fields['delivery_fee'].label = 'Default Delivery Fee Ex Vat'
        self.fields['environment_fee'].label = 'Environment Fee Ex Vat'
        self.fields['sale_delivery_fee'].label = 'Default Delivery Fee Ex Vat'
        self.fields['sale_environment_fee'].label = 'Environment Fee Ex Vat'
        self.fields['sku'].label = 'Type of Control'

    class Meta:
        fields = ('is_blocked', 'is_pos_blocked', 'description', 'sales_account', 'cost_of_sales_account',
                  'gl_account', 'bar_code', 'selling_price', 'vat_code', 'is_discounted', 'unit', 'additional_text',
                  'delivery_fee', 'package_size', 'environment_fee', 'allow_negative_stocks', 'critical_stock_level',
                  'lowest_stock_level', 'max_stock_level', 'level_to_order_stock', 'quantity_in_package', 'min_reorder',
                  'max_reorder', 'store_area', 'bin_location', 'supplier', 'supplier_code', 'sku_type',
                  'sku', 'rack', 'sold_as_type', 'is_recipe_item', 'sale_package_size', 'sale_unit', 'cost_price',
                  'sale_quantity_in_package', 'sale_vat_code', 'sale_delivery_fee', 'measure', 'stock_unit',
                  'sale_environment_fee', 'stock_adjustment_account', 'weight', 'gross_weight', 'price_type',
                  'service_cost', 'service_account', 'group_level_items')
        model = BranchInventory

        widgets = {
            'sales_account': forms.Select(attrs={'class': 'chosen-select'}),
            'cost_of_sales_account': forms.Select(attrs={'class': 'chosen-select'}),
            'gl_account': forms.Select(attrs={'class': 'chosen-select'}),
            'stock_adjustment_account': forms.Select(attrs={'class': 'chosen-select'}),
            'service_account': forms.Select(attrs={'class': 'chosen-select'}),
            'vat_code': forms.Select(attrs={'class': 'chosen-select'}),
            'sale_vat_code': forms.Select(attrs={'class': 'chosen-select'}),
            'supplier': forms.Select(attrs={'class': 'chosen-select'}),
            'bar_code': forms.TextInput(),
            'price_type': forms.RadioSelect(),
            'measure': forms.Select(attrs={'class': 'chosen-select', 'data-units-url': reverse_lazy('measure_units')}),
        }

    def clean(self):
        cleaned_data = super(BranchInventoryForm, self).clean()
        sku_type = cleaned_data.get('sku_type')
        is_recipe = cleaned_data.get('is_recipe_item')
        is_service = sku_type == BranchInventory.SERVICE

        if not sku_type:
            self.add_error('sku_type', 'Sku type unit is required')

        if not is_service:
            if not cleaned_data.get('unit'):
                self.add_error('unit', 'Inventory unit is required')

        if not (is_recipe or is_service):
            if not cleaned_data.get('unit'):
                self.add_error('unit', 'Unit is required')
            if not cleaned_data.get('cost_price'):
                self.add_error('cost_price', 'Default cost is required')

        if sku_type in [SkuType.SKU.value, SkuType.SERVICE.value]:
            if not cleaned_data['sales_account']:
                self.add_error('sales_account', 'Sales account is required')

            if not is_recipe:
                if not cleaned_data['cost_of_sales_account']:
                    self.add_error('cost_of_sales_account', 'Cost of sales account is required')

            if sku_type == SkuType.SKU.value and not cleaned_data['gl_account']:
                self.add_error('gl_account', 'Inventory GL Account Number is required')

        if cleaned_data['service_cost']:
            if cleaned_data['service_cost'] > 99:
                self.add_error('service_cost', 'Cost to provide as a % of Sale cannot be more than 99%')

        return cleaned_data

    def save(self, commit=True):
        inventory = super().save(commit=False)
        if self.inventory:
            inventory.inventory = self.inventory
        inventory.company = self.branch.company
        inventory.branch = self.branch
        inventory.package_size = self.get_package_size()
        inventory.sale_package_size = self.get_package_size('sale_')
        inventory.save()
        self.save_m2m()

        self.save_group_levels(inventory)

    def get_package_size(self, typ=''):
        length = self.request.POST.get(f'{typ}l', None)
        b = self.request.POST.get(f'{typ}b', None)
        h = self.request.POST.get(f'{typ}h', None)
        package_size = {}
        if length:
            package_size['l'] = length
        if b:
            package_size['b'] = b
        if h:
            package_size['h'] = h
        return package_size

    def save_group_levels(self, inventory_item):
        items = self.request.POST.getlist('group_level_items')
        group_level_items = [int(group_level_item) for group_level_item in items if (len(group_level_item) > 0)]
        inventory_item.group_level_items.set(group_level_items)


class CopyInventoryForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        exclude_branch_id = []
        if 'excludes' in kwargs:
            exclude_branch_id = kwargs.pop('excludes')
        super(CopyInventoryForm, self).__init__(*args, **kwargs)
        self.fields['branches'].queryset = Branch.objects.filter(
            company=company).exclude(id__in=exclude_branch_id)

    branches = forms.ModelMultipleChoiceField(required=True, label='Copy to branches', queryset=None)


class CopyInventoryToBranchForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        branch = kwargs.pop('branch')
        super(CopyInventoryToBranchForm, self).__init__(*args, **kwargs)
        self.fields['branch'].queryset = Branch.objects.filter(company=company).exclude(id=branch.id)

    branch = forms.ModelChoiceField(required=True, label='Copy to branch', queryset=None)


class InventoryItemForm(forms.Form):

    form_classes = OrderedDict((
        ('inventory', InventoryForm),
        ('branch', BranchInventoryForm),
    ))


class BranchInventoryItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(BranchInventoryItemForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].queryset = Supplier.objects.filter(company=company, is_active=True)
        self.fields['branch'].queryset = Branch.objects.filter(company=company, is_active=True)
        self.fields['sales_account'].queryset = Account.objects.filter(company=company, is_active=True)
        self.fields['cost_of_sales_account'].queryset = Account.objects.filter(company=company, is_active=True)
        self.fields['gl_account'].queryset = Account.objects.filter(company=company, is_active=True)
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=company, is_active=True)

    class Meta:
        fields = ('is_blocked', 'is_pos_blocked', 'description', 'sales_account', 'cost_of_sales_account',
                  'gl_account', 'bar_code', 'selling_price', 'vat_code', 'is_discounted', 'unit', 'additional_text',
                  'delivery_fee', 'package_size', 'environment_fee', 'allow_negative_stocks', 'critical_stock_level',
                  'lowest_stock_level', 'max_stock_level', 'level_to_order_stock', 'quantity_in_package',
                  'max_package_to_order', 'store_area', 'bin_location', 'supplier', 'supplier_code', 'branch',
                  'sku_type', 'rack')
        model = BranchInventory

        widgets = {
            'sales_account': forms.Select(attrs={'class': 'chosen-select'}),
            'cost_of_sales_account': forms.Select(attrs={'class': 'chosen-select'}),
            'gl_account': forms.Select(attrs={'class': 'chosen-select'}),
            'vat_code': forms.Select(attrs={'class': 'chosen-select'}),
            'supplier': forms.Select(attrs={'class': 'chosen-select'}),
            'bar_code': forms.TextInput(),
        }

    def clean(self):
        cleaned_data = super(BranchInventoryItemForm, self).clean()

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')

        if 'branch' in cleaned_data and not cleaned_data['branch']:
            self.add_error('branch', 'Branch is required')
        return cleaned_data


class PurchaseOrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        branch = kwargs.pop('branch')
        super(PurchaseOrderForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].queryset = Supplier.objects.filter(company=company, is_active=True)
        self.fields['branch'].queryset = Branch.objects.filter(company=company,
                                                               id=branch.id,
                                                               is_active=True
                                                               )
        self.fields['branch'].empty_label = None

    memo_to_supplier = forms.CharField(required=False, label='Memo to supplier',
                                       widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))

    class Meta:
        fields = ('supplier', 'memo_to_supplier', 'branch')
        model = Invoice

    def clean(self):
        cleaned_data = super(PurchaseOrderForm, self).clean()

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')
        return cleaned_data


class DeclineForm(forms.Form):
    comment = forms.CharField(required=True, label='Do you want to add a reason why you are declining it',
                              widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))


class InventoryPurchaseOrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(InventoryPurchaseOrderForm, self).__init__(*args, **kwargs)
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=company, is_active=True)
        self.fields['inventory'].queryset = BranchInventory.objects.filter(company=company)

    vat_code = forms.ModelChoiceField(required=True, label='Vat Code', queryset=None,
                                      widget=forms.Select(attrs={'class': 'chosen-select',
                                                                 'data-calculate-line-amount':
                                                                     reverse_lazy('calculate_purchase_order_amount')}))

    class Meta:
        fields = ('inventory', 'quantity', 'unit', 'order_value', 'vat_code', 'vat_amount')
        model = PurchaseOrderInventory

        widgets = {
            'vat_amount': forms.HiddenInput(),
        }


class ImportOpeningBalanceForm(forms.Form):
    csv_file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)')


class ImportForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        super(ImportForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.company_year(company, year).order_by('-period')
        self.fields['period'].empty_label = None

    date = forms.DateField(required=True, label='Date', widget=forms.TextInput())
    period = forms.ModelChoiceField(required=True, label='Period', queryset=None)
    csv_file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)')

    def clean(self):
        cleaned_data = super(ImportForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']

        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and date do not match')

        csv_file = cleaned_data["csv_file"]
        if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx')):
            self.add_error('csv_file', 'File is not .csv, .xlsx or xls type')

        if csv_file.multiple_chunks():
            size = csv_file.size / (1000 * 1000)
            self.add_error('csv_file', "Uploaded file is too big (%.2f MB)." % size)

        return cleaned_data


class InventorySearchForm(forms.Form):
    inventory = forms.ChoiceField(widget=HeavySelect2Widget(data_view='inventory_item_search_list', attrs={
        'data-view-url': 'view/item/detail/'
    }))
