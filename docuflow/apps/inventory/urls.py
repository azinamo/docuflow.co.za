from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.BranchInventoryView.as_view(), name='inventory_index'),
    path('register/', views.InventoryRegisterView.as_view(), name='inventory_register'),
    path('ajax/list/', views.InventoryDataView.as_view(), name='branch_inventories'),
    path('create/', views.CreateInventoryView.as_view(), name='create_inventory_item'),
    path('create/<int:inventory_id>/branch/', views.CreateBranchInventoryView.as_view(),
         name='create_branch_inventory'),
    path('copy/<int:inventory_id>/branches/', views.CopyInventoryView.as_view(), name='copy_inventory_to_branches'),
    path('import/', views.ImportInventoryView.as_view(), name='import_inventory'),
    path('download/', views.DownloadInventoryRegisterView.as_view(), name='download_inventory_register'),
    path('copy/to/branch/', views.CopyInventoryToBranchView.as_view(), name='copy_inventory_to_branch'),
    path('branch/inventory/', views.BranchInventoryListView.as_view(), name='branch_inventory_list'),
    path('copy/year/balances/', views.CopyYearBalancesView.as_view(), name='copy_inventory_previous_year_balances'),
    path('calculate/selling-price/', views.CalculateSellingPriceWithVat.as_view(),
         name='calculate_selling_price_with_vat'),
    path('edit/<int:pk>/', views.EditInventoryView.as_view(), name='edit_inventory'),
    path('branch/edit/<int:pk>/', views.EditBranchInventoryView.as_view(), name='edit_branch_inventory'),
    path('branch/delete/<int:pk>/', views.DeleteBranchInventoryView.as_view(), name='delete_branch_inventory'),
    path('search/autocomplete/', views.SearchInventoryView.as_view(), name='autocomplete_search_list'),
    path('search/list/', views.SearchInventoryItemView.as_view(), name='inventory_item_search_list'),
    path('search/', views.InventoryDetailView.as_view(), name='search_inventory_item'),
    path('fetch/item/', views.InventoryView.as_view(), name='fetch_inventory_item'),
    path('view/item/', views.InventoryItemView.as_view(), name='view_inventory_item'),
    path('view/item/<int:pk>/detail/', views.InventoryDetailView.as_view(), name='view_inventory_item_detail'),
    path('view/item/detail/', views.InventoryDetailView.as_view(), name='detailed_view_inventory_item'),
    path('show/<int:pk>/rows/', views.InventoryItemRowsView.as_view(), name='show_inventory_item_rows'),
    path('order/<int:pk>/', csrf_exempt(views.OrderInventoryItemView.as_view()), name='order_inventory_item'),
    path('<int:pk>/fifo/', views.InventoryItemFifoView.as_view(), name='inventory_item_fifo'),
    path('import/opening/balances/', views.ImportOpeningBalanceView.as_view(), name='import_inventory_opening_balances'),
    path('valuation/summary', views.ValuationSummaryView.as_view(), name='valuation_summary')
]

urlpatterns += [
    path('grouplevel/', include('docuflow.apps.inventory.modules.grouplevel.urls')),
    path('purchaseorder/', include('docuflow.apps.inventory.modules.inventorypurchaseorder.urls')),
    path('stockcount/', include('docuflow.apps.inventory.modules.stockcount.urls')),
    path('received/', include('docuflow.apps.inventory.modules.received.urls')),
    path('returned/', include('docuflow.apps.inventory.modules.returned.urls')),
    path('recipes/', include('docuflow.apps.inventory.modules.recipes.urls')),
    path('adjustment/', include('docuflow.apps.inventory.modules.stockadjustment.urls')),
    path('ledger/', include('docuflow.apps.inventory.modules.ledger.urls')),
    path('manufacturing/', include('docuflow.apps.inventory.modules.manufacturing.urls')),
    path('branchmovement/', include('docuflow.apps.inventory.modules.branchmovement.urls')),
]