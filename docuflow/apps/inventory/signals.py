from django.dispatch import receiver
from django.db.models.signals import post_save

from docuflow.apps.inventory.models import Inventory, PurchaseHistory


@receiver(post_save, sender=Inventory)
def update_average_price(sender, **kwargs):
    purchase_history = PurchaseHistory.objects.filter(inventory=sender).first()
    if not purchase_history:
        purchase_history = PurchaseHistory.objects.create(
            inventory=sender,
            average_price=0,
        )
