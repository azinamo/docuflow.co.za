import logging

from docuflow.apps.inventory.models import BranchInventory, InventoryReceived, InventoryReturned, InventoryReceivedReturned
from docuflow.apps.inventory import utils

logger = logging.getLogger(__name__)


def received_inventory(inventory: BranchInventory, quantity: int, inventory_received: InventoryReceived,
                       inventory_returned: InventoryReturned) -> list:
    logger.info(f"Receive {quantity} specifically those returned/sold items {inventory_returned}")
    in_stock = inventory.in_stock
    total_received_qty = quantity
    retured_items = []
    for sold_inventory in inventory_returned.all():
        print(
            f"Sold inventory item being returned {sold_inventory} has fifo items {sold_inventory.return_received_items.all()}")
        for sold_item in sold_inventory.return_received_items.all():
            print(f"Qquantity received returned {sold_item.quantity} > {quantity}")
            if sold_item.quantity >= quantity:
                opening_balance = in_stock
                logger.info(
                    f"Sold {sold_item.quantity} but we receiving back {quantity} @ {sold_item.price}, "
                    f"Opening at {opening_balance}->Closing at {in_stock}")

                returned_received = InventoryReceivedReturned.objects.create(
                    inventory_received=inventory_received,
                    inventory=inventory_received.inventory,
                    inventory_returned=sold_inventory,
                    quantity=quantity,
                    price=sold_item.price,
                    opening_balance=opening_balance,
                    data=utils.prepare_opening_closing_balance(sold_inventory, in_stock, quantity)
                )
                in_stock += quantity
                total_received_qty -= quantity
                sold_item.quantity -= quantity
                # self.received_returned_cost += quantity_received * sold_item.price
                # self.returned_lines.append(returned_received)
                retured_items.append(returned_received)
    return retured_items
