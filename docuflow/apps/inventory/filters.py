from django_filters import rest_framework as filters

from .models import BranchInventory, Inventory

from docuflow.apps.company.models import Company, Account
from docuflow.apps.supplier.models import Supplier


def accounts(request):
    if request is None:
        return Account.objects.none()
    return Account.objects.filter(company=request.session['company'])


def suppliers(request):
    if request is None:
        return Supplier.objects.none()
    return Supplier.objects.filter(company=request.session['company'])


class BranchInventoryFilter(filters.FilterSet):
    inventory__code = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = BranchInventory
        fields = ['inventory__company__slug', 'inventory__code', 'description', 'is_recipe_item', 'supplier',
                  'supplier_code', 'sales_account', 'cost_of_sales_account', 'gl_account', 'bar_code']

