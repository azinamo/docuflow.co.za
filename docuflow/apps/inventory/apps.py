from django.apps import AppConfig


class InventoryConfig(AppConfig):
    name = 'docuflow.apps.inventory'

    def ready(self):
        import docuflow.apps.inventory.signal_receivers # noqa
