from decimal import Decimal

from celery import shared_task
from django.core.management import call_command
from huey.contrib.djhuey import task, db_task

from docuflow.apps.company.models import Company
from .models import BranchInventory


# @db_task()
# def calculate_inventory_stock(inventory_id):
#     inventory = BranchInventory.objects.prefetch_related(
#         'inventory_received', 'inventory_returned', 'purchase_orders', 'fixed_prices', 'sales_invoiceitem_related',
#         'sales_deliveryitem_related', 'purchase_orders'
#     ).select_related(
#         'history', 'stock_adjustment_account', 'cost_of_sales_account', 'gl_account'
#     ).filter(
#         id=inventory_id
#     ).first()
#
#     inventory.refresh_from_db()
#
#     return {'in_stock': inventory.in_stock,
#             'total_value': inventory.on_order,
#             'on_reserve': inventory.on_reserve,
#             'on_delivery': inventory.on_delivery,
#             'average_price': inventory.average_price
#             }
#
#
# @db_task()
# def calculate_inventories_stock_values():
#     slugs = []
#     companies = Company.objects.filter(slug__in=slugs)
#     company_values = {}
#     for company in companies:
#         total_inventory_value = Decimal(0)
#         total_inventory_quantity = Decimal(0)
#         q = BranchInventory.objects.filter(branch__company=company)
#
#         for inventory in q:
#             history = inventory.recalculate_balances()
#             if history:
#                 total_inventory_value += history.total_value or Decimal(0)
#                 total_inventory_quantity += history.in_stock or Decimal(0)
#             else:
#                 pass
#         company_values[str(company)] = {
#             'total_inventory_value': total_inventory_value,
#             'total_inventory_quantity': total_inventory_quantity
#         }
#     return {'message': 'Calculated successfully new inventory levels', 'companies': company_values}
#
#
# @db_task()
# def process_manufacturing(manufacturing_id):
#     call_command('process_manufactured_inventory', manufacturing_id=manufacturing_id)
#
#
# @db_task()
# def process_manufactured_inventory():
#     call_command('process_manufactured_inventory')
