from django.db.models import Count

from docuflow.apps.company.models import Branch
from docuflow.apps.inventory.models import BranchInventory, StockCount, InventoryStockCount, InventoryReceivedReturned


def get_received_not_returned(inventory: BranchInventory):
    return InventoryReceivedReturned.objects.select_related(
        'inventory'
    ).filter(
        inventory=inventory, received_object_id__isnull=False, returned_object_id__isnull=True
    ).order_by('id')
    # TODO add date to received-returned, so we can order by the date we receive or return


def get_returned_not_received(inventory: BranchInventory):
    return InventoryReceivedReturned.objects.select_related(
        'inventory'
    ).filter(inventory=inventory, received_object_id__isnull=True, returned_object_id__isnull=False)


def get_stock_count_with_inventories(stock_count_id: int):
    return StockCount.objects.select_related(
        'branch'
    ).prefetch_related('inventory_items', 'inventory_items__inventory').get(pk=stock_count_id)


def get_counted_inventory_with_variances(stock_count: StockCount):
    return InventoryStockCount.objects.with_variances(stock_count).select_related(
        'inventory', 'inventory__inventory', 'inventory__gl_account', 'inventory__stock_adjustment_account'
    )