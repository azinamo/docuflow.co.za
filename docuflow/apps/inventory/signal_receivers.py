from django.dispatch import receiver
from django.db.models.signals import post_save

from docuflow.apps.inventory.models import Ledger, InventoryReceivedReturned
from docuflow.apps.inventory.enums import LedgerStatus
from docuflow.apps.inventory.services import handle_year_closing_inventory_movement


@receiver(post_save, sender=Ledger, dispatch_uid='inventory_fifo')
def handle_inventory_ledger(sender, instance, created, **kwargs):
    if instance.status == LedgerStatus.COMPLETE:
        inventory = instance.inventory
        inventory.recalculate_balances(year=instance.period.period_year)
        inventory.refresh_from_db()
        handle_year_closing_inventory_movement(year=instance.period.period_year, inventory=inventory)

# @receiver(post_save, sender=InventoryReceived, dispatch_uid='inventory_received')
# def handle_received_inventory(sender, instance, created, **kwargs):
#     handle_year_closing_inventory_movement(year=instance.received.period.period_year, inventory=instance.inventory)
#
#
# @receiver(post_save, sender=InventoryReturned, dispatch_uid='inventory_returned')
# def handle_returned_inventory(sender, instance, created, **kwargs):
#     handle_year_closing_inventory_movement(year=instance.returned.period.period_year, inventory=instance.inventory)
#
#
# @receiver(post_save, sender=StockAdjustmentInventory, dispatch_uid='inventory_stock_adjustment')
# def handle_stock_adjustment_inventory(sender, instance, created, **kwargs):
#     handle_year_closing_inventory_movement(year=instance.adjustment.period.period_year, inventory=instance.inventory)
#
#
# @receiver(post_save, sender=InventoryStockCount, dispatch_uid='inventory_stock_count')
# def handle_stock_adjustment_inventory(sender, instance, created, **kwargs):
#     handle_year_closing_inventory_movement(year=instance.stock_count.period.period_year, inventory=instance.inventory)
#
