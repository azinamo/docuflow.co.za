from django.db import models


class GroupLevelManager(models.Manager):

    def eligible_parents(self, company):
        return self.get_queryset().annotate(
            children_count=models.Count('children')
        ).filter(
            parent__isnull=True, company=company
        )

