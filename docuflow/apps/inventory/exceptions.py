class InventoryException(Exception):
    pass


class InventoryCodeExists(InventoryException):
    pass


class InvalidPrice(InventoryException):
    pass


class JournalAccountUnavailable(InventoryException):
    pass


class NoReceivedInventoryException(InventoryException):
    pass


class PeriodNotFound(InventoryException):
    pass


class InvalidQuantity(InventoryException):
    pass


class NoImportInventory(InventoryException):

    def __str__(self):
        return 'No inventory items found to import'


class CompanyAccountCodeException(Exception):
    pass