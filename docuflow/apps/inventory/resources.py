from import_export import resources

from .models import BranchInventory


class BranchInventoryResource(resources.ModelResource):

    class Meta:
        model = BranchInventory
