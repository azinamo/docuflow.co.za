from django.urls import path

from . import views

urlpatterns = [
    path('', views.ManufacturingView.as_view(), name='manufacturing_index'),
    path('create/', views.CreateManufacturingView.as_view(), name='create_manufacturing'),
    path('<int:pk>/delete/', views.DeleteManufacturingView.as_view(), name='delete_manufacturing'),
    path('<int:pk>/detail/', views.ManufacturingDetailView.as_view(), name='manufacturing_detail'),
    path('recipe/items/', views.RecipeItemsView.as_view(), name='manufacturing_recipe_items'),
    path('add/recipe/item/', views.RecipeItemsView.as_view(), name='add_manufacturing_recipe_items'),
    path('edit/<int:pk>/inventory/item/', views.EditRecipeItemsView.as_view(),
         name='edit_manufacturing_recipe_items'),
    path('validate/inventory/quantities/', views.ValidateInventoryQuantityView.as_view(),
         name='validate_inventory_quantity'),
    path('recipe/cost/breakdown/', views.RecipeCostBreakdownView.as_view(), name='recipe_cost_breakdown'),
    path('inventory/items/list/', views.InventoryItemsView.as_view(), name='recipe_inventory_items_list')
]
