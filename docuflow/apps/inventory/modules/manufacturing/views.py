import logging
import pprint
from collections import defaultdict
from decimal import Decimal
from datetime import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import ListView, View, TemplateView, DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from django.db import transaction

from annoying.functions import get_object_or_None

from docuflow.apps.common.mixins import ProfileMixin, ActiveYearRequiredMixin, DataTableMixin
from docuflow.apps.common.utils import is_ajax_post, is_ajax
from docuflow.apps.inventory.models import Recipe, RecipeInventory, Manufacturing
from docuflow.apps.inventory.exceptions import InvalidQuantity, InventoryException
from .forms import CreateManufacturingForm
from . import services


pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class ManufacturingView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'manufacturing/index.html'

    def get_context_data(self, **kwargs):
        context = super(ManufacturingView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        context['year'] = self.get_year()
        return context


class CreateManufacturingView(LoginRequiredMixin, ActiveYearRequiredMixin, SuccessMessageMixin, CreateView):
    model = Manufacturing
    form_class = CreateManufacturingForm
    template_name = 'manufacturing/create.html'
    success_message = 'Manufacturing successfully created'
    success_url = reverse_lazy('manufacturing_index')

    def get_initial(self):
        initial = super(CreateManufacturingView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_context_data(self, **kwargs):
        context = super(CreateManufacturingView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateManufacturingView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['branch'] = self.get_branch()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the manufacturing, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateManufacturingView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            try:
                with transaction.atomic():
                    form.save(commit=False)
                    return JsonResponse({
                        'text': 'Manufacturing saved successfully.',
                        'error': False,
                        'redirect': reverse('manufacturing_index')
                    })
            except InventoryException as exception:
                return JsonResponse({
                    'text': str(exception),
                    'error': True
                })


class ManufacturingDetailView(DetailView):
    model = Manufacturing
    template_name = 'manufacturing/detail.html'
    context_object_name = 'manufacturing'

    def get_queryset(self):
        return Manufacturing.objects.get_detail(self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        manufacturing = self.get_object()
        total = 0
        recipes = defaultdict(list)
        for recipe_item in manufacturing.recipe_items.all():
            recipe_total = 0
            if recipe_item.returns.exists():
                for returned_line in recipe_item.returns.all():
                    total += returned_line.returned_value
                    recipe_total += returned_line.returned_value
                    recipes[recipe_item].append({'total': returned_line.returned_value, 'inventory': returned_line.inventory,
                                                 'quantity': returned_line.quantity, 'received': str(returned_line.received_object)})
            else:
                for manufactured_item in recipe_item.manufactured_items.all():
                    for fifo in manufactured_item.return_received_items.all():
                        total += fifo.total
                        recipe_total += fifo.total
                        recipes[recipe_item].append({'total': fifo.total, 'inventory': fifo.inventory,
                                                     'quantity': fifo.quantity})
        context['total'] = total
        context['recipes'] = dict(recipes)
        return context


class DeleteManufacturingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, View):
    success_url = reverse_lazy('manufacturing_index')

    def get(self, request, *args, **kwargs):
        recipe = get_object_or_None(Recipe, id=self.kwargs['pk'])
        if recipe:
            recipe.delete()
            messages.success(self.request, 'Manufacturing deleted successfully')
        return HttpResponseRedirect(reverse_lazy('manufacturing_index'))


class RecipeItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'manufacturing/recipe_items.html'

    def get_recipes(self):
        return Recipe.objects.select_related(
            'inventory', 'inventory__inventory', 'inventory__branch'
        ).filter(inventory__branch=self.get_branch())

    def get_context_data(self, **kwargs):
        context = super(RecipeItemsView, self).get_context_data(**kwargs)
        context['recipe_items'] = self.get_recipes()
        context['row'] = self.request.GET.get('row_id', 0)
        return context


class EditRecipeItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'manufacturing/edit_recipe_items.html'

    def get_manufacturing(self):
        manufacturing = Manufacturing.objects.filter(id=self.kwargs['pk']).first()
        return manufacturing

    def get_recipe_items(self):
        recipe_items = Recipe.objects.filter(inventory__branch=self.get_branch())
        return recipe_items

    def get_manufacturing_inventory_items(self, manufacturing):
        return manufacturing.recipe_items.all()

    def get_context_data(self, **kwargs):
        context = super(EditRecipeItemsView, self).get_context_data(**kwargs)
        recipe_items = self.get_recipe_items()
        manufacturing = self.get_manufacturing()
        manufacturing_inventory_items = self.get_manufacturing_inventory_items(manufacturing)
        context['recipe_items'] = recipe_items
        context['manufacturing_recipe_items'] = manufacturing_inventory_items
        context['row'] = len(manufacturing_inventory_items) + 1
        context['manufacturing'] = manufacturing
        return context


class ValidateInventoryQuantityView(ProfileMixin, View):

    def get_recipe(self):
        recipe_id = self.request.GET.get('recipe_id', None)
        if recipe_id:
            return get_object_or_None(Recipe, pk=recipe_id)
        return None

    def get(self, request, *args, **kwargs):
        try:
            recipe = self.get_recipe()
            unit_cost = 0
            total_cost = 0
            if recipe:
                for recipe_inventory in recipe.recipe_items.all():
                    logger.info(float(self.request.GET.get('quantities_{}'.format(recipe_inventory.id), 0)))
                    quantity = float(self.request.GET.get('quantities_{}'.format(recipe_inventory.id), 0))
                    logger.info("Quantity for {}({}) is {}".format(recipe_inventory.inventory, recipe_inventory.id, quantity))
                    # required_quantity = recipe_inventory.quantity * quantity
                    available_quantity = recipe_inventory.inventory.available_stock
                    if quantity > available_quantity:
                        raise InvalidQuantity('Quantity of {} used in recipe {} is not available to manufacture this '
                                              'product'.format(recipe_inventory.inventory.full_name, recipe.fullname))
                    unit_cost = float(recipe_inventory.quantity) * float(quantity) * float(recipe_inventory.inventory.current_price)
                    logger.info("Current price of  {} is {}".format(recipe_inventory.inventory, recipe_inventory.inventory.current_price))
                    logger.info("Unit cost is  for {} is {}".format(recipe_inventory.inventory, unit_cost))
                    total_cost += unit_cost

                    logger.info("Total cost is now {} is {}".format(recipe_inventory.inventory, total_cost))
                    # get quantity available
                    # multiply by the quantity requested
                    # if not more than requested, raise an exception

            return JsonResponse({'text': 'Inventory available to manufacture', 'error': False,
                                 'unit_cost': round(unit_cost, 2),
                                 'total_cost': round(total_cost, 2)
                                 })
        except InventoryException as exception:
            logger.exception(exception)
            return JsonResponse({'text': str(exception), 'error': True, 'alert': True})


class InventoryItemsView(View):

    def get(self, request, **kwargs):
        recipe_items = RecipeInventory.objects.select_related(
            'inventory'
        ).filter(
            recipe_id=self.request.GET.get('recipe_id')
        )
        recipe_inventory_items = []
        for recipe_item in recipe_items:
            item = dict()
            item['id'] = recipe_item.id
            item['quantity'] = recipe_item.quantity
            item['item_unit'] = str(recipe_item.unit)
            item['item_unit_code'] = recipe_item.unit.unit
            item['inventory'] = str(recipe_item.inventory)
            item['inventory_id'] = recipe_item.inventory.id
            item['unit_id'] = None
            item['unit_code'] = None
            item['sale_unit_id'] = None
            item['sale_unit_code'] = None
            item['stock_unit_id'] = None
            item['stock_unit_code'] = None
            if recipe_item.inventory.unit:
                item['unit_id'] = recipe_item.inventory.unit_id
                item['unit_code'] = recipe_item.inventory.unit.unit
            if recipe_item.inventory.stock_unit:
                item['stock_unit_id'] = recipe_item.inventory.stock_unit_id
                item['stock_unit_code'] = recipe_item.inventory.stock_unit.unit
            if recipe_item.inventory.sale_unit:
                item['sale_unit_id'] = recipe_item.inventory.sale_unit_id
                item['sale_unit_code'] = recipe_item.inventory.sale_unit.unit
            recipe_inventory_items.append(item)

        logger.info(recipe_inventory_items)
        return JsonResponse(recipe_inventory_items, safe=False)


class RecipeCostBreakdownView(TemplateView):
    template_name = 'manufacturing/cost_breakdown.html'

    # TODO Ensure that for multiple lines the value available is checked against running balance instead
    #  of a const balance
    def get_recipe_quantity(self, recipe_inventory):
        if recipe_inventory.quantity:
            return float(recipe_inventory.quantity)
        return 0

    def get_requested_quantity(self):
        quantity_requested = Decimal(0)
        quantity = self.request.GET.get('quantity', '0')
        if quantity != '' and quantity.isnumeric():
            quantity_requested = Decimal(quantity)
        return quantity_requested

    def get_recipe_items(self):
        return RecipeInventory.objects.select_related(
            'recipe', 'inventory', 'inventory__inventory', 'unit', 'inventory__unit', 'inventory__sale_unit',
            'inventory__stock_unit'
        ).prefetch_related(
            'inventory__inventory_received'
        ).filter(
            recipe_id=self.request.GET.get('recipe_id')
        ).all()

    def get_context_data(self, **kwargs):
        context = super(RecipeCostBreakdownView, self).get_context_data(**kwargs)
        try:
            recipe = Recipe.objects.get(pk=self.request.GET.get('recipe_id'))
            items = self.get_recipe_items()
            quantity = self.get_requested_quantity()

            recipe_items = services.get_recipe_cost_breakdown(
                recipe_items=items,
                quantity=quantity
            )

            context['recipe'] = recipe
            context['quantity'] = quantity
            context['total_cost'] = recipe_items.get('total_cost', 0)
            context['total'] = recipe_items.get('total', 0)
            context['recipe_items'] = recipe_items.get('recipe_items', [])
            context['unit_cost'] = recipe_items.get('unit_cost', 0)
            context['each_price'] = recipe_items.get('each_price', 0)
            context['row'] = int(self.request.GET.get('row', 0))
            context['error'] = False
        except InventoryException as exception:
            context['errors'] = str(exception)
        return context
