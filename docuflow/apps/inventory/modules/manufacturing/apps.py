from django.apps import AppConfig


class ManufacturingConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.manufacturing'
