import logging
from decimal import Decimal
from collections import defaultdict
import pprint

from django.contrib.contenttypes.fields import ContentType

from annoying.functions import get_object_or_None
from docuflow.apps.company.models import Unit
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.models import (BranchInventory, Ledger, InventoryReturned, Recipe, RecipeInventory,
                                            ManufacturingRecipe, Returned, InventoryReceivedReturned, Manufacturing)
from docuflow.apps.inventory.exceptions import InvalidQuantity
from docuflow.apps.inventory.services import AvailableInventory, FiFoReceived, FiFoReturned
from docuflow.apps.inventory.selectors import get_received_not_returned
from docuflow.apps.inventory.utils import prepare_opening_closing_balance

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class Manufacture(object):

    def __init__(self, manufacturing, recipe_items):
        logger.info('-----Manufacture------')
        self.manufacturing = manufacturing
        self.recipe_items = recipe_items

    def execute(self):
        logger.info("Manufacturing saved properly --> {}".format(self.manufacturing))
        available_received_inventory = self.get_received_inventory_items()
        for field in self.recipe_items:
            if field[0:9] == 'quantity_':
                if self.recipe_items[field] != '':
                    logger.info(self.recipe_items)
                    quantity = round(Decimal(self.recipe_items[field]), 4)
                    line_key = field[8:]
                    recipe_item = "recipe_item{}".format(line_key)
                    recipe_id = self.recipe_items.get(recipe_item, None)

                    if recipe_id:
                        recipe = get_object_or_None(Recipe, pk=recipe_id)
                        if recipe:
                            logger.info(f'-----Use recipe {str(recipe)}------ line {recipe_id}')
                            self.validate_quantity(recipe, quantity)
                            cost_key = f'recipe_{recipe_id}_total_cost'
                            _total_cost = self.get_input_value(cost_key)
                            logger.info(f"Recipe item id {recipe_id} and quantity is {quantity} at cost {_total_cost}")
                            recipe_manufactured = self.create_manufacturing(recipe, quantity)

                            # Returned inventory items used in recipe
                            total_cost = self.return_recipe_inventories(
                                self.manufacturing,
                                recipe_manufactured,
                                line_key
                            )
                            logger.info(f"Total cost  ===============================> {total_cost}")
                            self.create_inventory_received(recipe_manufactured, total_cost)

    def get_input_value(self, input_key):
        input_value = self.recipe_items.get(input_key, 0)
        value = Decimal(0)
        if input_value != '':
            value = Decimal(input_value)
        return value

    def validate_quantity(self, recipe, quantity):
        if quantity == '':
            raise InvalidQuantity(f"Invalid quantity entered for recipe {recipe.fullname}")

    def create_manufacturing(self, recipe, quantity):
        return ManufacturingRecipe.objects.create(
            manufacturing=self.manufacturing,
            quantity=quantity,
            recipe=recipe
        )

    def manufacture_recipe(self, quantity, recipe):
        return ManufacturingRecipe.objects.create(
            manufacturing=self.manufacturing,
            quantity=quantity,
            recipe=recipe
        )

    def create_inventory_received(self, recipe_manufactured: ManufacturingRecipe, total_cost):
        qty = self.get_model_quantity(recipe_manufactured)
        unit_cost = (total_cost / qty)
        fifo_received = FiFoReceived(
            inventory=recipe_manufactured.recipe.inventory,
            quantity=recipe_manufactured.quantity,
            unit_price=unit_cost,
            content_object=recipe_manufactured,
            content_type=ContentType.objects.get_for_model(recipe_manufactured)
        )
        fifo_received.execute()

        Ledger.objects.create(
            inventory=recipe_manufactured.recipe.inventory,
            content_type=ContentType.objects.get_for_model(recipe_manufactured),
            object_id=recipe_manufactured.id,
            quantity=recipe_manufactured.quantity,
            value=unit_cost,
            created_by=recipe_manufactured.manufacturing.profile,
            period=recipe_manufactured.manufacturing.period,
            date=recipe_manufactured.manufacturing.date,
            text=str(recipe_manufactured.manufacturing)
        )
        recipe_manufactured.recipe.inventory.recalculate_balances(year=recipe_manufactured.manufacturing.period.period_year)

    # noinspection PyMethodMayBeStatic
    def create_inventory_returned(self, returned, inventory, quantity, manufacturing_recipe=None):
        return InventoryReturned.objects.create(
            returned=returned,
            inventory=inventory,
            quantity=quantity,
            rate=1,
            manufacturing_recipe=manufacturing_recipe
        )

    # noinspection PyMethodMayBeStatic
    def create_returned_transaction(self, manufacturing):
        return Returned.objects.create(
            movement_type=MovementType.MANUFACTURING,
            branch=manufacturing.branch,
            date=manufacturing.date,
            profile=manufacturing.profile,
            role=manufacturing.role,
            period=manufacturing.period
        )

    # noinspection PyMethodMayBeStatic
    def get_available_inventories(self, recipe_items):
        # inventory_ids = [recipe_inventory.inventory_id ]
        availabilities = {}
        for recipe_item in recipe_items:
            availabilities[recipe_item.inventory] = InventoryReceivedReturned.objects.get_available_items(recipe_item.inventory).all()
        return availabilities

    # noinspection PyMethodMayBeStatic
    def get_recipe_quantity(self, recipe_inventory):
        if recipe_inventory.quantity:
            return recipe_inventory.quantity
        return 0

    # noinspection PyMethodMayBeStatic
    def get_model_quantity(self, model):
        if model.quantity:
            return model.quantity
        return Decimal(0)

    # noinspection PyMethodMayBeStatic
    def get_quantity_rate(self, recipe_inventory):
        rate = None
        if recipe_inventory.unit:
            rate = Unit.objects.rate(recipe_inventory.unit, recipe_inventory.inventory.stock_unit)
        return rate.current_rate if rate and rate.current_rate else Decimal(1)

    def return_recipe_inventories(self, manufacturing: Manufacturing, recipe_manufactured: ManufacturingRecipe,
                                  row_id: int):
        total_cost = Decimal(0)
        content_type = ContentType.objects.get_for_model(manufacturing)
        each_price_total = 0
        total = 0
        recipe_items = recipe_manufactured.recipe.recipe_items.all()

        available_inventories = self.get_available_inventories(recipe_items)
        manufacturing_qty = self.get_model_quantity(recipe_manufactured)
        for recipe_item in recipe_items:
            rate = self.get_quantity_rate(recipe_item)
            recipe_quantity = recipe_item.quantity
            # Recipe quantity
            quantity = recipe_item.calculate_required_quantity(rate=rate)
            # Total requested manufacturing recipe quantity
            total_quantity = recipe_item.calculate_required_quantity(rate=rate, quantity=manufacturing_qty)

            if not quantity or quantity <= 0:
                raise InvalidQuantity(f"Invalid quantity {quantity} on recipe inventory item {recipe_item}")

            fifo_returned = FiFoReturned(
                inventory=recipe_item.inventory,
                quantity=total_quantity,
                unit_price=Decimal('0'),
                content_object=recipe_manufactured
            )
            fifo_returned.execute()

            for returned_line in fifo_returned.returned_lines:
                ledger_quantity = returned_line.quantity * -1
                ledger_unit_cost = returned_line.price * -1
                unit_cost = returned_line.price * rate

                line_item_cost = returned_line.price * returned_line.quantity
                each_price = round(line_item_cost / manufacturing_qty, 2)
                each_price_total += each_price
                each_price_line_total = each_price * manufacturing_qty
                total += each_price_line_total
                each_value = each_price_line_total / ledger_quantity
                #
                _total_cost = unit_cost * manufacturing_qty * recipe_quantity
                total_cost += _total_cost

                Ledger.objects.create(
                    inventory=returned_line.inventory,
                    content_type=content_type,
                    object_id=manufacturing.id,
                    quantity=ledger_quantity,
                    value=each_value,
                    total_value=each_price_line_total,
                    rate=-1,
                    created_by=manufacturing.profile,
                    period=manufacturing.period,
                    date=manufacturing.date,
                    text=str(manufacturing)
                )
                recipe_item.inventory.recalculate_balances(year=manufacturing.period.period_year)
        return total

    def get_received_inventory_items(self):
        branch_inventories = BranchInventory.objects.prefetch_related(
            'inventory_returned',
            'inventory_received',
        ).filter(branch=self.manufacturing.branch)

        available_inventory_items = defaultdict(list)

        for branch_inventory in branch_inventories.all():
            total_returned = 0
            total_received = 0
            for _inventory_ret in branch_inventory.inventory_returned.all():
                total_returned += _inventory_ret.quantity

            for _inventory_rec in branch_inventory.inventory_received.all():
                total_received += _inventory_rec.quantity
                quantity_returned = 0
                for _inventory_rec_ret in _inventory_rec.returned_items.all():
                    quantity_returned += _inventory_rec_ret.quantity

                available_quantity = _inventory_rec.quantity - quantity_returned
                if available_quantity > 0:
                    available_inventory_items[branch_inventory.id].append({
                        'received': _inventory_rec, 'in_hand': available_quantity
                    })
        return available_inventory_items

    # noinspection PyMethodMayBeStatic
    def update_inventory_quantities(self, inventory_availability):
        for inventory_id, availability in inventory_availability.items():
            quantity = availability['quantity_out']
            returned = availability['returned']
            received_inventory_items = availability['received']
            for received_inventory in received_inventory_items:
                received_in_hand = received_inventory.get('in_hand')
                received_obj = received_inventory.get('received')
                if quantity > 0:
                    if received_in_hand > quantity:
                        received_obj.save()

                        InventoryReceivedReturned.objects.create(
                            inventory_received=received_obj,
                            inventory_returned=returned,
                            quantity=quantity,
                            inventory=received_obj.inventory
                        )
                        quantity = 0
                    else:
                        received_obj.save()
                        quantity = quantity - received_in_hand

                        InventoryReceivedReturned.objects.create(
                            inventory_received=received_obj,
                            inventory_returned=returned,
                            quantity=received_in_hand,
                            inventory=received_obj.inventory
                        )


class RecipeInventoryAvailabilitySummary:

    def __init__(self, recipe_item, quantity, rate):
        self.recipe_item = recipe_item
        self.rate = rate
        self.quantity = quantity

    def get_available_inventories(self):
        # inventory_ids = [recipe_inventory.inventory_id ]
        available = AvailableInventory(self.recipe_item.inventory)
        logger.info(available)
        inventory_availability = available.execute()
        return inventory_availability

    def is_available(self, availability):
        available = self.recipe_item.inventory.in_stock
        requested = self.get_requested_quantity()
        if requested > available:
            raise InvalidQuantity('Quantity of {} used in recipe {} is not available to manufacture this '
                                  'product'.format(self.recipe_item.inventory.full_name, self.recipe_item.recipe.fullname))
        return True

    def get_requested_quantity(self):
        quantity = 0
        rate = 1
        if self.quantity:
            quantity = float(self.quantity)
        if self.rate:
            rate = self.rate
        return rate * quantity

    def get_recipe_cost_breakdown(self):
        summary = {'total': 0, 'unit': 0, 'recipe_items': []}
        total_cost = 0
        logger.info('self.request.POST')
        inventory = self.recipe_item.inventory
        logger.info(inventory)

        availabilities = self.get_available_inventories()

        logger.info(self.recipe_item.inventory.id)
        rate = float(self.rate)
        logger.info('Conversion rate {}'.format(rate))

        available_quantity = inventory.in_stock
        requested = self.get_requested_quantity()
        logger.info('Quantity of {} required is  {} vs in stock {}'.format(self.recipe_item,
                                                                           requested,
                                                                           available_quantity))
        if self.is_available(requested, available_quantity):
            logger.info("Available --- Available --- Available -----> {}")
            qty = requested
            logger.info("Requested {}".format(qty))
            for inventory_received in availabilities:
                logger.info("Qty {}".format(qty))
                if qty > 0:
                    available_qty = float(inventory_received.get('available'))
                    logger.info("Passed, received {} ".format(available_qty))
                    inventory_received = inventory_received.get('received')
                    logger.info("From {}".format(inventory_received.received))
                    unit_price = float(inventory_received.price_excluding)
                    logger.info("Qty({})@ {}".format(qty, unit_price))
                    used_qty = 0
                    if available_qty > qty:
                        used_qty = qty
                        qty = 0
                    else:
                        qty = qty - available_qty
                        used_qty = available_qty
                    logger.info("We used {} from the  {} available, now left with {} ".format(used_qty,
                                                                                              available_qty,
                                                                                              qty
                                                                                              ))
                    item_cost = unit_price * float(used_qty)
                    unit_cost = item_cost * requested
                    logger.info("Total cost is  using --> {}".format(used_qty))
                    logger.info("Current price of  {} is {}".format(inventory, unit_price))
                    logger.info("Unit cost is  for {} is {}".format(inventory, unit_cost))
                    total_cost += unit_cost

                    logger.info("Total cost is now {} is {}".format(inventory, total_cost))
                    # get quantity available
                    summary['recipe_items'].append({
                            'total_cost': unit_cost,
                            'unit_price': item_cost,
                            'recipe_item': self.recipe_item.__str__(),
                            'received': inventory_received.received.__str__(),
                            'inventory': inventory.__str__(),
                            'quantity': round(used_qty, 4),
                            '_quantity': requested,
                            'item_quantity': self.recipe_item.quantity,
                            'rate': rate,
                            'available': available_quantity,
                            'item_unit': self.recipe_item.unit.unit,
                            'item_unit_id': self.recipe_item.unit_id,
                            'unit_id': inventory.unit_id if inventory.unit else None,
                            'unit_code': inventory.unit.unit if inventory.unit else None,
                            'sale_unit_id': inventory.sale_unit_id if inventory.sale_unit else None,
                            'sale_unit_code': inventory.sale_unit.unit if inventory.sale_unit else None,
                            'stock_unit_id': inventory.stock_unit_id if inventory.stock_unit else None,
                            'stock_unit_code': inventory.stock_unit.unit if inventory.stock_unit else None
                    })
        summary['total'] = total_cost
        summary['unit_cost'] = total_cost
        return summary


def manufacturing_inventory_received(recipe_manufactured: ManufacturingRecipe, total_cost):
    qty = recipe_manufactured.quantity
    unit_cost = total_cost / qty
    content_type = ContentType.objects.get_for_model(recipe_manufactured)
    logger.info(f"Create create_inventory_received at unit cost {unit_cost} from total {total_cost} using qty {qty}")
    fifo_received = FiFoReceived(
        inventory=recipe_manufactured.recipe.inventory,
        quantity=recipe_manufactured.quantity,
        unit_price=unit_cost,
        content_object=recipe_manufactured,
        content_type=content_type
    )
    fifo_received.execute()

    Ledger.objects.create(
        inventory=recipe_manufactured.recipe.inventory,
        content_type=content_type,
        object_id=recipe_manufactured.id,
        quantity=recipe_manufactured.quantity,
        value=unit_cost,
        created_by=recipe_manufactured.manufacturing.profile,
        period=recipe_manufactured.manufacturing.period,
        date=recipe_manufactured.manufacturing.date,
        text=str(recipe_manufactured.manufacturing)
    )
    recipe_manufactured.recipe.inventory.recalculate_balances(year=recipe_manufactured.manufacturing.period.period_year)


def return_recipe_manufactured(recipe_manufactured: ManufacturingRecipe):
    logger.info("Return recipe inventory items ")
    total_cost = Decimal(0)
    content_type = ContentType.objects.get_for_model(Manufacturing, for_concrete_model=False)
    each_price_total = 0
    total = 0
    logger.info("Recipe inventory items returned")
    recipe_items = recipe_manufactured.recipe.recipe_items.all()

    available_inventories = get_available_inventories(recipe_items)
    logger.info(available_inventories)
    manufacturing_qty = recipe_manufactured.quantity
    for recipe_item in recipe_items:
        rate = get_quantity_rate(recipe_item)
        recipe_quantity = recipe_item.quantity
        logger.info(f"- Handling {recipe_item.inventory} - *** Using Rate is {rate}, quantity is {recipe_quantity} ")
        # Recipe quantity
        quantity = recipe_item.calculate_required_quantity(rate=rate)
        # Total requested manufacturing recipe quantity
        total_quantity = recipe_item.calculate_required_quantity(
            rate=rate,
            quantity=manufacturing_qty
        )

        if not quantity or quantity <= 0:
            raise InvalidQuantity(f"Invalid quantity {quantity} on recipe inventory item {recipe_item}")

        fifo_returned = FiFoReturned(
            inventory=recipe_item.inventory,
            quantity=total_quantity,
            unit_price=Decimal('0'),
            content_object=recipe_manufactured
        )
        fifo_returned.execute()

        for returned_line in fifo_returned.returned_lines:
            ledger_quantity = returned_line.quantity * -1
            ledger_unit_cost = returned_line.price * -1
            unit_cost = returned_line.price * rate

            line_item_cost = returned_line.price * returned_line.quantity
            each_price = round(line_item_cost / manufacturing_qty, 2)
            each_price_total += each_price
            each_price_line_total = each_price * manufacturing_qty
            total += each_price_line_total
            each_value = each_price_line_total / ledger_quantity

            _total_cost = unit_cost * manufacturing_qty * recipe_quantity
            total_cost += _total_cost

            Ledger.objects.create(
                inventory=returned_line.inventory,
                content_type=content_type,
                object_id=recipe_manufactured.id,
                quantity=ledger_quantity,
                value=each_value,
                total_value=each_price_line_total,
                rate=-1,
                created_by=recipe_manufactured.manufacturing.profile,
                period=recipe_manufactured.manufacturing.period,
                date=recipe_manufactured.manufacturing.date,
                text=str(recipe_manufactured.manufacturing)
            )
            recipe_item.inventory.recalculate_balances(year=recipe_manufactured.manufacturing.period.period_year)
    return total


def get_available_inventories(recipe_items):
    availabilities = {}
    for recipe_item in recipe_items:
        availabilities[recipe_item.inventory] = get_received_not_returned(inventory=recipe_item.inventory)
    return availabilities


def get_quantity_rate(recipe_inventory):
    rate = None
    if recipe_inventory.unit:
        rate = Unit.objects.rate(unit=recipe_inventory.unit, unit_to=recipe_inventory.inventory.stock_unit)
    return rate.current_rate if rate and rate.current_rate else Decimal(1)


def is_available(recipe_item: RecipeInventory, requested: Decimal, availabilities: list):
    total_available = sum([availability.quantity for availability in availabilities])
    if requested > total_available:
        raise InvalidQuantity(f"Quantity of {recipe_item.inventory.full_name} used in recipe "
                              f"{recipe_item.recipe.fullname} is not available to manufacture this "
                              f"product")
    return True


def get_recipe_cost_breakdown(quantity: Decimal, recipe_items=None):
    summary = {'total': 0, 'unit': 0, 'recipe_items': [], 'each_price': 0, 'total_cost': 0}
    total_cost = 0
    recipe_items = recipe_items or []

    available_inventories = get_available_inventories(recipe_items=recipe_items)
    unit_cost = 0
    each_price_total = 0
    total = 0
    for recipe_inventory in recipe_items:
        rate = get_quantity_rate(recipe_inventory=recipe_inventory)  # TODO - Refactor how we get the unit conversion rates

        required_quantity = recipe_inventory.calculate_required_quantity(rate=rate, quantity=quantity)
        available_quantity = recipe_inventory.inventory.in_stock

        availabilities = available_inventories.get(recipe_inventory.inventory, None)
        if is_available(recipe_item=recipe_inventory, requested=required_quantity, availabilities=availabilities):
            qty = required_quantity
            for received_not_returned in availabilities:
                if qty > 0:
                    available_qty = received_not_returned.quantity

                    unit_price = received_not_returned.price
                    if available_qty > qty:
                        used_qty = qty
                        qty = 0
                        line_item_cost = unit_price * used_qty
                        # unit_cost += line_item_cost * required_quantity
                        total_cost += line_item_cost
                        unit_cost += unit_price

                        # get quantity available
                        each_price = round(line_item_cost / quantity, 2)
                        each_price_total += each_price
                        each_price_line_total = each_price * quantity

                    else:
                        qty = qty - available_qty
                        used_qty = available_qty

                        each_price_line_total, line_item_cost, each_price = calculate_price_for_full_return(
                            received_return=received_not_returned
                        )

                        # get quantity available
                        total_cost += line_item_cost
                        unit_cost += unit_price
                        each_price_total += each_price

                    total += each_price_line_total
                    summary['recipe_items'].append({
                            'total_cost': line_item_cost,
                            'total': each_price_line_total,
                            'id': received_not_returned.id,
                            'recipe_id': recipe_inventory.id,
                            'unit_price': unit_price,
                            'each_price': each_price,
                            'recipe_item': recipe_inventory,
                            'received': '',
                            'received_id': received_not_returned.received_object_id,
                            'inventory': recipe_inventory.inventory,
                            'description': str(recipe_inventory.inventory.description),
                            'quantity': round(used_qty, 4),
                            '_quantity': required_quantity,
                            'item_quantity': recipe_inventory.quantity,
                            'individual_quantity': recipe_inventory.quantity * rate,
                            'rate': rate,
                            'available': available_quantity,
                            'item_unit': recipe_inventory.unit.unit,
                            'item_unit_id': recipe_inventory.unit_id,
                            'unit_id': recipe_inventory.inventory.unit_id if recipe_inventory.inventory.unit else None,
                            'unit_code': recipe_inventory.inventory.unit.unit if recipe_inventory.inventory.unit else None,
                            'sale_unit_id': recipe_inventory.inventory.sale_unit_id if recipe_inventory.inventory.sale_unit else None,
                            'sale_unit_code': recipe_inventory.inventory.sale_unit.unit if recipe_inventory.inventory.sale_unit else None,
                            'stock_unit_id': recipe_inventory.inventory.stock_unit_id if recipe_inventory.inventory.stock_unit else None,
                            'stock_unit_code': recipe_inventory.inventory.stock_unit.unit if recipe_inventory.inventory.stock_unit else None
                    })
    summary['total_cost'] = total_cost
    summary['unit_cost'] = unit_cost
    summary['each_price'] = each_price_total
    summary['total'] = total
    return summary


def calculate_price_for_full_return(received_return: InventoryReceivedReturned):
    unit_price = received_return.price
    each_price_line_total = received_return.received_value

    return each_price_line_total, each_price_line_total, unit_price
    # logger.info(f"We used {used_qty} from the {available_qty} available, now left with {qty} ")
    # line_item_cost = unit_price * used_qty
    # # unit_cost += line_item_cost * required_quantity
    # logger.info(f"Total cost is  using --> {used_qty}")
    # logger.info(f"Current price of {recipe_inventory.inventory} is {unit_price}")
    # logger.info(f"Unit cost is for {recipe_inventory.inventory} is {unit_cost}")
    # total_cost += line_item_cost
    # unit_cost += unit_price
    #
    # # get quantity available
    # each_price = round(line_item_cost / quantity, 2)
    # each_price_total += each_price
    # each_price_line_total = each_price * quantity
    # logger.info(f"Each price is {each_price} * {quantity} ==> {each_price_line_total}")
    # total += each_price_line_total


def calculate_price_for_part_return(received_returned: InventoryReceivedReturned, available_quantity, qty, quantity, used_qty):
    logger.info(f"We used {used_qty} from the {available_quantity} available, now left with {quantity} ")
    unit_price = received_returned.price
    line_item_cost = unit_price * used_qty
    # unit_cost += line_item_cost * required_quantity
    logger.info(f"Total cost is  using --> {used_qty}")


    # get quantity available
    each_price = round(line_item_cost / quantity, 2)
    each_price_line_total = each_price * quantity
    logger.info(f"Each price is {each_price} * {quantity} ==> {each_price_line_total}")

    return each_price_line_total, line_item_cost, each_price


# noinspection PyMethodMayBeStatic
def return_recipe_inventory(inventory: BranchInventory, manufacturing_recipe: ManufacturingRecipe):
    content_type = ContentType.objects.get_for_model(manufacturing_recipe)

    recipe_items = get_recipe_cost_breakdown(
        recipe=manufacturing_recipe.recipe, quantity=manufacturing_recipe.quantity)
    manufacturing = manufacturing_recipe.manufacturing

    recipe_total_cost = Decimal(0)
    inventory_stock = inventory.in_stock

    received_returned_items = {
        rec_ret.id: rec_ret
        for rec_ret in InventoryReceivedReturned.objects.select_related('inventory', 'inventory_received').filter(
            pk__in=[rp['id'] for rp in recipe_items.get('recipe_items')])
    }
    ledgers = []
    for recipe_return_line in recipe_items.get('recipe_items'):
        received_returned_line = received_returned_items.get(recipe_return_line['id'])
        logger.info("PROCESS LINE ")
        logger.info(recipe_return_line)
        if received_returned_line:
            recipe_total_cost += recipe_return_line['total']
            # Returned inventory items used in recipe
            # total_cost = services.return_recipe_manufactured(manufacturing_recipe)
            logger.info(f"Total cost  ===============================> {recipe_return_line['total']}")

            data = prepare_opening_closing_balance(
                cls=manufacturing_recipe,
                opening_stock=inventory_stock,
                quantity=recipe_return_line['quantity'] * -1,
                data=received_returned_line.data
            )
            if manufacturing_recipe.pk in data:
                data[manufacturing_recipe.pk]['value'] = round(float(recipe_return_line['total']), 2)
            else:
                data[manufacturing_recipe.pk] = {'value': round(float(recipe_return_line['total']), 2)}
            balance = received_returned_line.quantity - recipe_return_line['quantity']
            logger.info(f"<------ Remove {recipe_return_line['quantity']} from {received_returned_line.quantity} "
                        f"and balance = {balance} ---->")
            if balance == 0:
                logger.info("FULL RETURN")
                data = prepare_opening_closing_balance(
                    cls=manufacturing_recipe,
                    opening_stock=inventory_stock,
                    quantity=balance,
                    data=received_returned_line.data
                )
                received_returned_line.data = data
                received_returned_line.returned_type = content_type
                received_returned_line.returned_object_id = manufacturing_recipe.pk
                received_returned_line.returned_value = received_returned_line.received_value
                received_returned_line.save()
            else:
                logger.info("PARTIAL RETURN")
                InventoryReceivedReturned.objects.create(
                    inventory_received=received_returned_line.inventory_received,
                    received_object_id=received_returned_line.received_object_id,
                    received_type=received_returned_line.received_type,
                    returned_type=content_type,
                    returned_object_id=manufacturing_recipe.pk,
                    quantity=recipe_return_line['quantity'],
                    inventory=received_returned_line.inventory,
                    price=received_returned_line.price,
                    data=data,
                    returned_value=recipe_return_line['total'],
                    received_value=recipe_return_line['total']
                )
                logger.info(f"Done with PARTIAL RETURN - {received_returned_line}")
                data = prepare_opening_closing_balance(
                    cls=manufacturing_recipe,
                    opening_stock=inventory_stock,
                    quantity=recipe_return_line['quantity'],
                    data=received_returned_line.data
                )
                received_returned_line.quantity = balance
                received_returned_line.received_value -= recipe_return_line['total']
                received_returned_line.data = data
                logger.info("Data")
                logger.info(received_returned_line)
                received_returned_line.save()
                logger.info("Done with update of partial return")

            recipe_return_line['inventory'].recalculate_balances(
                year=manufacturing_recipe.manufacturing.period.period_year
            )

            ledgers.append(Ledger(
                inventory=recipe_return_line['inventory'],
                content_type=content_type,
                object_id=manufacturing_recipe.id,
                quantity=recipe_return_line['quantity'],
                value=recipe_return_line['each_price'],
                total_value=recipe_return_line['total'],
                rate=-1,
                created_by=manufacturing_recipe.manufacturing.profile,
                period=manufacturing_recipe.manufacturing.period,
                date=manufacturing.date,
                text=str(manufacturing)
            ))
    Ledger.objects.bulk_create(ledgers)
    return recipe_total_cost


def received_inventory(inventory: BranchInventory, manufacturing_recipe: ManufacturingRecipe, total_cost):
    content_type = ContentType.objects.get_for_model(manufacturing_recipe)
    qty = manufacturing_recipe.quantity
    unit_cost = total_cost / qty
    logger.info(f"Create create_inventory_received at unit cost {unit_cost} from total {total_cost} using qty {qty}")
    fifo_received = FiFoReceived(
        inventory=inventory,
        quantity=manufacturing_recipe.quantity,
        unit_price=unit_cost,
        content_object=manufacturing_recipe,
        content_type=content_type
    )
    fifo_received.execute()

    inventory.recalculate_balances(year=manufacturing_recipe.manufacturing.period.period_year)

    Ledger.objects.create(
        inventory=inventory,
        content_type=content_type,
        object_id=manufacturing_recipe.id,
        quantity=manufacturing_recipe.quantity,
        value=unit_cost,
        created_by=manufacturing_recipe.manufacturing.profile,
        period=manufacturing_recipe.manufacturing.period,
        date=manufacturing_recipe.manufacturing.date,
        text=str(manufacturing_recipe.manufacturing),
        total_value=total_cost
    )