import logging
from decimal import Decimal

from django import forms
from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import Manufacturing, ManufacturingRecipe, Ledger, InventoryReceivedReturned, \
    BranchInventory
from docuflow.apps.inventory.services import FiFoReceived
from docuflow.apps.inventory.services import is_valuation_balancing
from docuflow.apps.inventory.utils import prepare_opening_closing_balance
from docuflow.apps.period.models import Period
from . import services

logger = logging.getLogger(__name__)


class CreateManufacturingForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(CreateManufacturingForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    class Meta:
        model = Manufacturing
        fields = ('period', 'date', 'description')

    def clean(self):
        cleaned_data = super(CreateManufacturingForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        return cleaned_data

    def save(self, commit=False):
        manufacturing = super(CreateManufacturingForm, self).save(commit=False)
        manufacturing.branch = self.branch
        manufacturing.profile = self.profile
        manufacturing.role = self.role
        manufacturing.save()

        content_type = ContentType.objects.get_for_model(ManufacturingRecipe, for_concrete_model=False)
        for field, value in self.request.POST.items():
            if field.startswith('quantity', 0, 8) and value:
                row_id = field[9:]
                data = {
                    'quantity': value,
                    'recipe': self.request.POST.get(f'recipe_item_{row_id}'),
                    'manufacturing': manufacturing
                }
                manufacturing_recipe_form = ManufacturingRecipeForm(
                    data=data, branch=self.branch, year=self.year, profile=self.profile,
                    period=self.cleaned_data['period'], content_type=content_type
                )
                if manufacturing_recipe_form.is_valid():
                    manufacturing_recipe_form.save()
                else:
                    logger.info(manufacturing_recipe_form.errors)

        # process_manufacturing(manufacturing_id=manufacturing.id)

        if not is_valuation_balancing(branch=self.branch, year=self.year):
            raise InventoryException('A variation in inventory accounts and inventory valuation detected.')
        return manufacturing


class ManufacturingRecipeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.period = kwargs.pop('period')
        self.content_type = kwargs.pop('content_type')
        self.ledgers = []
        super(ManufacturingRecipeForm, self).__init__(*args, **kwargs)
        self.fields['recipe'].queryset = self.fields['recipe'].queryset.filter(inventory__branch=self.branch)
        self.fields['manufacturing'].queryset = self.fields['manufacturing'].queryset.filter(branch=self.branch)

    class Meta:
        model = ManufacturingRecipe
        fields = ('recipe', 'manufacturing', 'quantity')

    def save(self, commit=True):
        manufacturing_recipe = super(ManufacturingRecipeForm, self).save(commit=False)
        manufacturing_recipe.save()

        inventory = manufacturing_recipe.recipe.inventory

        total_cost = self.return_recipe_inventory(
            inventory=inventory,
            manufacturing_recipe=manufacturing_recipe
        )

        manufacturing_recipe.value = total_cost
        manufacturing_recipe.save()

        self.received_inventory(
            inventory=inventory,
            manufacturing_recipe=manufacturing_recipe,
            total_cost=total_cost
        )

        Ledger.objects.bulk_create(self.ledgers)

        return manufacturing_recipe

    # noinspection PyMethodMayBeStatic
    def return_recipe_inventory(self, inventory: BranchInventory, manufacturing_recipe: ManufacturingRecipe):
        recipe_items = services.get_recipe_cost_breakdown(
            quantity=manufacturing_recipe.quantity,
            recipe_items=manufacturing_recipe.recipe.recipe_items.all()
        )
        manufacturing = manufacturing_recipe.manufacturing

        recipe_total_cost = Decimal(0)
        inventory_stock = inventory.in_stock
        received_returned_items = {
            rec_ret.id: rec_ret
            for rec_ret in InventoryReceivedReturned.objects.select_related('inventory', 'inventory_received').filter(
                pk__in=[rp['id'] for rp in recipe_items.get('recipe_items')])
        }
        for recipe_return_line in recipe_items.get('recipe_items'):
            received_returned_line = received_returned_items.get(recipe_return_line['id'])
            if received_returned_line:
                line_cost = recipe_return_line['total']
                recipe_total_cost += line_cost
                # Returned inventory items used in recipe
                # total_cost = services.return_recipe_manufactured(manufacturing_recipe)
                data = prepare_opening_closing_balance(
                    cls=manufacturing_recipe,
                    opening_stock=inventory_stock,
                    quantity=recipe_return_line['quantity'] * -1,
                    data=received_returned_line.data
                )
                if manufacturing_recipe.pk in data:
                    data[manufacturing_recipe.pk]['value'] = round(float(recipe_return_line['total']), 2)
                else:
                    data[manufacturing_recipe.pk] = {'value': round(float(recipe_return_line['total']), 2)}
                balance = received_returned_line.quantity - recipe_return_line['quantity']
                if balance == 0:
                    data = prepare_opening_closing_balance(
                        cls=manufacturing_recipe,
                        opening_stock=inventory_stock,
                        quantity=balance,
                        data=received_returned_line.data
                    )
                    received_returned_line.data = data
                    received_returned_line.returned_type = self.content_type
                    received_returned_line.returned_object_id = manufacturing_recipe.pk
                    received_returned_line.returned_value = received_returned_line.received_value
                    received_returned_line.save()
                else:
                    InventoryReceivedReturned.objects.create(
                        inventory_received=received_returned_line.inventory_received,
                        received_object_id=received_returned_line.received_object_id,
                        received_type=received_returned_line.received_type,
                        returned_type=self.content_type,
                        returned_object_id=manufacturing_recipe.pk,
                        quantity=recipe_return_line['quantity'],
                        inventory=received_returned_line.inventory,
                        price=received_returned_line.price,
                        data=data,
                        returned_value=recipe_return_line['total'],
                        received_value=recipe_return_line['total']
                    )
                    data = prepare_opening_closing_balance(
                        cls=manufacturing_recipe,
                        opening_stock=inventory_stock,
                        quantity=recipe_return_line['quantity'],
                        data=received_returned_line.data
                    )
                    received_returned_line.quantity = balance
                    received_returned_line.received_value -= recipe_return_line['total']
                    received_returned_line.data = data
                    received_returned_line.save()

                recipe_return_line['inventory'].recalculate_balances(year=self.year)

                self.ledgers.append(
                    Ledger(
                        inventory=recipe_return_line['inventory'],
                        content_type=self.content_type,
                        object_id=manufacturing_recipe.id,
                        quantity=recipe_return_line['quantity'],
                        value=recipe_return_line['each_price'],
                        total_value=recipe_return_line['total'],
                        rate=-1,
                        created_by=self.profile,
                        period=self.period,
                        date=manufacturing.date,
                        text=str(manufacturing)
                    )
                )
        return recipe_total_cost

    # noinspection PyMethodMayBeStatic
    def received_inventory(self, inventory: BranchInventory, manufacturing_recipe: ManufacturingRecipe, total_cost):
        qty = manufacturing_recipe.quantity
        unit_cost = total_cost / qty
        # ContentType.objects.get_for_model(self.content_object)
        fifo_received = FiFoReceived(
            inventory=inventory,
            quantity=manufacturing_recipe.quantity,
            unit_price=unit_cost,
            content_object=manufacturing_recipe,
            content_type=self.content_type
        )
        fifo_received.execute()

        inventory.recalculate_balances(year=self.year)

        self.ledgers.append(
            Ledger(
                inventory=inventory,
                content_type=self.content_type,
                object_id=manufacturing_recipe.id,
                quantity=manufacturing_recipe.quantity,
                value=unit_cost,
                created_by=self.profile,
                period=self.period,
                date=manufacturing_recipe.manufacturing.date,
                text=str(manufacturing_recipe.manufacturing),
                total_value=total_cost
            )
        )
