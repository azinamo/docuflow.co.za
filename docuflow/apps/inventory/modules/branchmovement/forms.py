from django import forms

from docuflow.apps.company.models import Branch
from docuflow.apps.inventory.models import StockAdjustment, Received, Returned
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.period.models import Period
from .services import MoveInventory
from ...exceptions import InventoryException
from ...services import is_valuation_balancing


class StockAdjustmentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        super(StockAdjustmentForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(company, year).order_by('-period')
        self.fields['period'].empty_label = None

    class Meta:
        model = StockAdjustment
        fields = ('date', 'period')

    def clean(self):
        cleaned_data = super(StockAdjustmentForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and date do not match')

        return cleaned_data


class InventoryStockBranchTransferForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.branch = kwargs.pop('branch')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        self.inventory = kwargs.pop('inventory')
        super(InventoryStockBranchTransferForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['branch'].queryset = Branch.objects.filter(company=self.company).exclude(id=self.branch.id)

    quantity = forms.DecimalField(required=True, label='Quantity', widget=forms.TextInput())
    reason = forms.CharField(required=True, label='Reason', widget=forms.Textarea())
    price = forms.DecimalField(required=False, label='Price', widget=forms.HiddenInput())
    on_hand = forms.DecimalField(required=True, label='On Hand', widget=forms.TextInput())
    date = forms.DateField(required=True, label='Date', widget=forms.TextInput())
    period = forms.ModelChoiceField(required=True, label='Period',
                                    queryset=None)
    branch = forms.ModelChoiceField(required=True, label='To Branch',
                                    queryset=None)

    def clean(self):
        cleaned_data = super(InventoryStockBranchTransferForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']

        if cleaned_data['quantity'] <= 0:
            self.add_error('quantity', 'Please enter valid quantity')

        if cleaned_data['quantity'] > cleaned_data['on_hand']:
            self.add_error('quantity', 'Quantity to move cannot be greater than quantity on hand')

        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and date do not match')

        return cleaned_data

    def execute(self):
        to_branch = self.cleaned_data.get('branch')

        returned = self.returned_transaction()
        received = self.received_transaction()

        move_inventory = MoveInventory(
            returned=returned,
            received=received,
            inventory=self.inventory,
            quantity=self.cleaned_data.get('quantity'),
            unit_price=self.cleaned_data.get('price')
        )

        move_inventory.execute()
        # TODO - Ensure we have a journal for a branch as well, so we can do valuation per branch correctly, as it is
        #  now, any movement of stock between branches will cause valuation not to balance, because for valuation we
        #  get data per branch, but journals are per company

        if not is_valuation_balancing(branch=self.branch, year=self.year):
            raise InventoryException('A variation in inventory accounts and inventory valuation detected.')

        if not is_valuation_balancing(branch=to_branch, year=self.year):
            raise InventoryException('A variation in inventory accounts and inventory valuation detected.')

        return to_branch

    def returned_transaction(self):
        return Returned.objects.create(
            period=self.cleaned_data['period'],
            date=self.cleaned_data['date'],
            note=self.cleaned_data['reason'],
            movement_type=MovementType.BRANCH_TRANSFER,
            profile=self.profile,
            branch=self.branch,
            role=self.role
        )

    def received_transaction(self):
        return Received.objects.create(
            period=self.cleaned_data['period'],
            date=self.cleaned_data['date'],
            note=self.cleaned_data['reason'],
            movement_type=MovementType.BRANCH_TRANSFER,
            profile=self.profile,
            branch=self.cleaned_data.get('branch'),
            role=self.role
        )
