import logging
import pprint
from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.views.generic import FormView, View, TemplateView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import BranchInventory
from .forms import InventoryStockBranchTransferForm

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class MoveBranchStockView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'branchmovement/index.html'

    def get_context_data(self, **kwargs):
        context = super(MoveBranchStockView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        context['year'] = self.get_year()
        return context


class CreateMoveBranchStockView(ProfileMixin, FormView):
    template_name = 'branchmovement/create.html'
    success_message = 'Branch stock movement successfully saved'
    form_class = InventoryStockBranchTransferForm

    def get_initial(self):
        initial = super(CreateMoveBranchStockView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        inventory = self.get_inventory()
        if inventory.current_price:
            initial['price'] = inventory.current_price
        return initial

    def get_inventory(self):
        return BranchInventory.objects.prefetch_related(
            'inventory_received', 'inventory_returned', 'inventory_received__returned_items'
        ).get(pk=self.kwargs['inventory_id'])

    def get_context_data(self, **kwargs):
        context = super(CreateMoveBranchStockView, self).get_context_data(**kwargs)
        context['inventory'] = self.get_inventory()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateMoveBranchStockView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        kwargs['role'] = self.get_role()
        kwargs['inventory'] = self.get_inventory()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the stock movement, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(CreateMoveBranchStockView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            try:
                with transaction.atomic():

                    to_branch = form.execute()

                    return JsonResponse({'text': f'Stock transferred to {to_branch} successfully',
                                         'error': False,
                                         'reload': True
                                         })
            except InventoryException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True,
                                     'alert': True
                                     })


class InventoryCalculatePriceView(View):

    def get(self, request, *args, **kwargs):
        quantity = float(self.request.GET.get('quantity', 0))
        price_type = self.request.GET.get('price_type', 'average')
        inventory_id = self.kwargs.get('inventory_id')
        if quantity > 0 and inventory_id:
            branch_inventory = BranchInventory.objects.filter(id=inventory_id).first()
            total = 0
            if branch_inventory:
                price = 0
                if price_type == 'average':
                    price = branch_inventory.average_price
                elif price_type == 'last_price':
                    price = branch_inventory.current_price
                total = float(price) * float(quantity)
            return JsonResponse({'total': round(total, 2)})
        return JsonResponse({'error': True})


class InventoryItemsView(ProfileMixin, TemplateView):
    template_name = 'branchmovement/inventory_items.html'

    def get_context_data(self, **kwargs):
        context = super(InventoryItemsView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        qs = BranchInventory.objects.select_related(
            'inventory', 'history', 'stock_adjustment_account', 'branch__company__stock_adjustment_account'
        ).filter(
            branch=self.get_branch()
        )
        code = self.request.GET.get('code')
        description = self.request.GET.get('description')
        if code != '':
            qs = qs.filter(inventory__code__icontains=code)
        if description != '':
            qs = qs.filter(description__icontains=description)

        inventories = qs.only(
            'id', 'inventory__code', 'description', 'stock_adjustment_account__name', 'stock_adjustment_account__code',
            'branch__company__stock_adjustment_account__code', 'branch__company__stock_adjustment_account__name',
            'history__inventory', 'history__in_stock'
        )
        context['inventory_items'] = inventories
        return context
