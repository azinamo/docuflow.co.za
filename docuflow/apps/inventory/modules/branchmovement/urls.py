from django.urls import path

from . import views

urlpatterns = [
    path('', views.MoveBranchStockView.as_view(), name='move_branch_stock'),
    path('create/<int:inventory_id>/', views.CreateMoveBranchStockView.as_view(), name='create_stock_branch_movement'),
    path('calculate/<int:inventory_id>/total/', views.InventoryCalculatePriceView.as_view(),
         name='adjustment_calculate_total'),
    path('inventory/items/', views.InventoryItemsView.as_view(), name='move_branch_inventory_items'),
]

