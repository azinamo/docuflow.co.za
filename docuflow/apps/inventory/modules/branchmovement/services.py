from collections import defaultdict
from decimal import Decimal
from typing import Union

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.exceptions import JournalAccountUnavailable
from docuflow.apps.inventory.models import BranchInventory, Received, InventoryReceived, Returned, InventoryReturned, \
    Ledger
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.inventory.services import CopyInventory, receive_inventory, return_inventory


class MoveInventory:

    def __init__(self, received: Received, returned: Returned, inventory: BranchInventory, quantity: Decimal,
                 unit_price: Decimal):
        self.inventory = inventory
        self.received = received
        self.returned = returned
        self.quantity = quantity
        self.unit_price = unit_price
        self.goods_received_account = self.get_goods_received_account()
        self.variance_account = self.get_default_inventory_variance_account()

    def get_to_branch_inventory(self, branch):
        branch_inventory = BranchInventory.objects.filter(
            branch=branch,
            inventory_id=self.inventory.inventory_id
        ).first()
        if not branch_inventory:
            copy = CopyInventory(inventory=self.inventory, branch=branch)
            branch_inventory = copy.execute()
        return branch_inventory

    def get_default_inventory_variance_account(self):
        default_account = self.received.branch.company.default_inventory_variance_account
        if not default_account:
            raise JournalAccountUnavailable('Inventory variance account not set up, please fix')
        return default_account

    def get_goods_received_account(self):
        goods_received_account = self.received.branch.company.default_goods_received_account
        if not goods_received_account:
            raise JournalAccountUnavailable('Default Goods Received Interim Account not available, please fix.')
        return goods_received_account

    def transfer_in(self, received_lines):
        """
        Move inventory from one branch to another, so all inventories from the source branch are recorded as returns and
        the same returned items are recorded as received inventories on the receiving branch
        """
        inventory = self.get_to_branch_inventory(branch=self.received.branch)
        ledger_lines = defaultdict(Decimal)
        control_lines = defaultdict(Decimal)

        for received_line in received_lines:
            inventory_received = InventoryReceived.objects.create(
                received=self.received,
                inventory=inventory,
                price_excluding=received_line.price,
                quantity=received_line.quantity
            )
            receive_inventory(
                inventory=inventory,
                price=received_line.price,
                quantity=received_line.quantity,
                year=self.received.period.period_year,
                content_object=inventory_received
            )

            self.create_inventory_journal(
                inventory=inventory,
                price=received_line.price,
                quantity=received_line.quantity,
                transaction=self.received,
                text=f'Interbranch transfer from {self.returned.branch}'
            )

            ledger_lines[inventory.gl_account] += received_line.received_value
            control_lines[self.goods_received_account] += received_line.received_value

        self.create_journal_lines(
            transaction=self.received,
            control_lines=control_lines,
            inventory_lines=ledger_lines
        )

        inventory.recalculate_balances(year=self.received.period.period_year)

        return self.received

    def transfer_out(self):
        inventory_returned = InventoryReturned.objects.create(
            returned=self.returned,
            quantity=self.quantity,
            price_excluding=self.unit_price,
            inventory=self.inventory
        )

        fifo_returned = return_inventory(
            inventory=self.inventory,
            quantity=self.quantity,
            year=self.returned.period.period_year,
            content_object=inventory_returned
        )

        returned_lines = fifo_returned['returned_lines']

        ledger_control_lines = defaultdict(Decimal)
        ledger_lines = defaultdict(Decimal)

        for returned_line in returned_lines:
            self.create_inventory_journal(
                inventory=self.inventory,
                price=returned_line.price,
                quantity=returned_line.quantity,
                transaction=self.returned,
                text=f'Interbranch transfer to {self.received.branch}'
            )
            ledger_lines[self.inventory.gl_account] += returned_line.returned_value

            ledger_control_lines[self.goods_received_account] += returned_line.returned_value

        self.create_journal_lines(
            transaction=self.returned,
            control_lines=ledger_control_lines,
            inventory_lines=ledger_lines
        )

        return returned_lines

    def execute(self):
        returned_lines = self.transfer_out()

        self.transfer_in(received_lines=returned_lines)

    # noinspection PyMethodMayBeStatic
    def create_inventory_journal(self, inventory: BranchInventory, price: Decimal, quantity: Decimal,
                                 transaction: Union[Received, Returned], text: str):
        Ledger.objects.create(
            inventory=inventory,
            quantity=quantity * -1 if isinstance(transaction, Returned) else quantity,
            value=price,
            date=transaction.date,
            period=transaction.period,
            created_by=transaction.profile,
            text=text,
            content_type=ContentType.objects.get_for_model(transaction),
            object_id=transaction.id,
            object_reference=str(transaction),
            object_owner=str(transaction.branch)
        )

    # noinspection PyMethodMayBeStatic
    def create_journal_lines(self, transaction: Union[Received, Returned], control_lines=None, inventory_lines=None):
        company = transaction.branch.company

        journal_lines = []
        for account, total in inventory_lines.items():
            journal_lines.append({'account': account,
                                  'debit': total,
                                  'is_reversal': False,
                                  'credit': 0,
                                  'accounting_date': transaction.date,
                                  'object_id': transaction.id,
                                  'content_type': ContentType.objects.get_for_model(transaction),
                                  'text': f"{str(transaction)}",
                                  'ledger_type_reference': transaction.supplier.name if transaction.supplier else ''
                                  })
        for account, total in control_lines.items():
            journal_lines.append({'account': account,
                                  'debit': 0,
                                  'credit': total,
                                  'is_reversal': False,
                                  'accounting_date': transaction.date,
                                  'object_id': transaction.id,
                                  'content_type': ContentType.objects.get_for_model(transaction),
                                  'text': f"{str(transaction)}",
                                  'ledger_type_reference': transaction.supplier.name if transaction.supplier else ''
                                  })
        ledger = None
        if len(journal_lines) > 0:
            create_ledger = CreateLedger(
                company=company,
                year=transaction.period.period_year,
                period=transaction.period,
                text=f"{str(transaction)}",
                module=Module.INVENTORY,
                role=transaction.role,
                date=transaction.date,
                profile=transaction.profile,
                journal_lines=journal_lines
            )
            ledger = create_ledger.execute()
        return ledger
