from django.apps import AppConfig


class BranchmovementConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.branchmovement'
