import logging
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.exceptions import JournalAccountUnavailable, InventoryException
from docuflow.apps.inventory.models import (BranchInventory, Ledger, Received, Returned, InventoryReceived,
                                            InventoryReturned)
from docuflow.apps.inventory.services import FifoInventoryAdjustment, RegisterInventoryHistory
from docuflow.apps.journals.models import Journal, JournalLine

logger = logging.getLogger(__name__)


class InventoryAdjustment:

    def __init__(self, branch, inventory, profile, role, **kwargs):
        logger.info("-----------InventoryAdjustment--------")
        self.branch = branch
        self.company = branch.company
        self.inventory = inventory
        self.profile = profile
        self.role = role
        self.kwargs = kwargs

    def execute(self):
        adjustment = Decimal(self.kwargs.get('adjustment', 0))
        if adjustment > 0:
            receive_inventory = InventoryReceivedAdjustment(self.branch, self.inventory,
                                                            self.profile, self.role, **self.kwargs)
            return receive_inventory.execute()
            # self.receive_inventory(adjustment, unit_price)
        else:
            returned_inventory = InventoryReturnedAdjustment(self.branch, self.inventory, self.profile,
                                                             self.role, **self.kwargs)
            return returned_inventory.execute()
            # self.return_inventory(adjustment, unit_price)


class BaseAdjustment:

    def get_stock_adjustment_account(self, account, inventory, company):
        if account:
            return account
        if inventory.stock_adjustment_account:
            return inventory.stock_adjustment_account
        if company.stock_adjustment_account:
            return company.stock_adjustment_account
        raise JournalAccountUnavailable('Stock adjustment account not set, please fix')

    def get_stock_control_account(self, inventory):
        if not inventory.gl_account:
            raise JournalAccountUnavailable('Stock control account not set, please fix')
        return inventory.gl_account

    def create_inventory_ledger(self, profile, inventory, price, quantity, stock_adjustment, content_type, **kwargs):
        if content_type:
            is_return = kwargs.get('is_return', None)
            text = kwargs.get('text', 'Stock Adjustment')
            Ledger.objects.create(
                inventory=inventory,
                content_type=content_type,
                object_id=stock_adjustment.id,
                quantity=quantity,
                value=price,
                date=stock_adjustment.date,
                created_by=profile,
                period=stock_adjustment.period,
                text=text,
                journal=kwargs.get('journal', None),
                rate=-1 if is_return else 1
            )


class InventoryReceivedAdjustment(BaseAdjustment):

    def __init__(self, branch, inventory, profile, role, **kwargs):
        logger.info('--------InventoryReceivedAdjustment---')
        self.branch = branch
        self.inventory = inventory
        self.profile = profile
        self.role = role
        self.kwargs = kwargs
        logger.info("Kwargs")
        logger.info(kwargs)

    def execute(self):
        adjustment = Decimal(self.kwargs.get('adjustment', 0))
        unit_price = Decimal(self.kwargs.get('unit_price', 0))
        stock_adjustment = self.create_received_adjustment()
        if stock_adjustment:
            inventory_received = self.create_inventory_received(stock_adjustment)

            content_type = self.get_receive_content_type()

            inventory_received.receive_returned()

            inventory_total = unit_price * adjustment
            journal = self.create_received_general_journal(stock_adjustment, inventory_total, content_type)

            self.create_inventory_ledger(self.profile,
                                         self.inventory,
                                         unit_price,
                                         adjustment,
                                         stock_adjustment,
                                         content_type,
                                         **{'journal': journal})
            history = RegisterInventoryHistory(inventory=inventory_received.inventory,
                                               transaction=stock_adjustment,
                                               price=unit_price,
                                               quantity=adjustment,
                                               date=stock_adjustment.date)
            history.create()

            return journal

    def create_inventory_received(self, stock_adjustment):
        return InventoryReceived.objects.create(
            received=stock_adjustment,
            quantity=self.kwargs.get('adjustment'),
            price_excluding=self.kwargs.get('unit_price'),
            inventory=self.inventory
        )

    def get_receive_content_type(self):
        return self.kwargs.get('receive_content_type', None)

    def receive_inventory(self, adjustment, unit_price):
        stock_adjustment = self.create_received_adjustment()
        if stock_adjustment:
            inventory_received = self.create_inventory_received(stock_adjustment)

            content_type = self.get_receive_content_type()

            inventory_received.receive_returned()

            inventory_total = unit_price * adjustment
            journal = self.create_received_general_journal(stock_adjustment, inventory_total, content_type)

            self.create_inventory_ledger(self.profile,
                                         self.inventory,
                                         unit_price,
                                         adjustment,
                                         stock_adjustment,
                                         content_type,
                                         journal)
            history = RegisterInventoryHistory(inventory=self.inventory,
                                               transaction=stock_adjustment,
                                               price=unit_price,
                                               quantity=adjustment,
                                               date=stock_adjustment.date)
            history.create()

    def create_received_general_journal(self, stock_adjustment, amount, content_type):
        logger.info('--------- create_received_general_journal -----')
        company = stock_adjustment.branch.company

        account = self.kwargs.get('account', None)
        text = self.kwargs.get('reason', None)

        stock_adjustment_account = self.get_stock_adjustment_account(account, self.inventory, company)
        control_account = self.get_stock_control_account(self.inventory)

        journal = Journal.objects.create(
            company=stock_adjustment.branch.company,
            year=stock_adjustment.period.period_year,
            period=stock_adjustment.period,
            journal_text=f"Stock adjustment - {stock_adjustment}  ({self.inventory})",
            module=Module.INVENTORY,
            role=stock_adjustment.role,
            date=stock_adjustment.date,
            created_by=stock_adjustment.profile
        )
        if journal:
            JournalLine.objects.create(
                journal=journal,
                account=control_account,
                debit=amount,
                is_reversal=False,
                text=text,
                accounting_date=stock_adjustment.date,
                object_id=stock_adjustment.id,
                content_type=content_type
            )
            JournalLine.objects.create(
                journal=journal,
                account=stock_adjustment_account,
                credit=amount,
                is_reversal=False,
                text=text,
                accounting_date=stock_adjustment.date,
                object_id=stock_adjustment.id,
                content_type=content_type
            )
        return journal

    def create_received_adjustment(self):
        return Received.objects.create(
            period=self.kwargs.get('period'),
            date=self.kwargs.get('date'),
            note=self.kwargs.get('reason', ''),
            movement_type=MovementType.STOCK_ADJUSTMENT,
            profile=self.profile,
            branch=self.branch,
            role=self.role
        )


class InventoryReturnedAdjustment(BaseAdjustment):

    def __init__(self, branch, inventory, profile, role, **kwargs):
        logger.info("----InventoryReturnedAdjustment---")
        self.branch = branch
        self.inventory = inventory
        self.profile = profile
        self.role = role
        self.kwargs = kwargs
        logger.info("Kwargs")
        logger.info(kwargs)

    def create_returned_adjustment(self):
        return Returned.objects.create(
            period=self.kwargs.get('period'),
            date=self.kwargs.get('date'),
            note=self.kwargs.get('reason'),
            movement_type=MovementType.STOCK_ADJUSTMENT,
            profile=self.profile,
            branch=self.branch,
            role=self.role
        )

    def create_inventory_returned(self, stock_adjustment, adjustment, unit_price):
        return InventoryReturned.objects.create(
            returned=stock_adjustment,
            quantity=adjustment,
            price_excluding=unit_price,
            inventory=self.inventory
        )

    def get_return_content_type(self):
        return self.kwargs.get('return_content_type', None)

    def execute(self):
        stock_adjustment = self.create_returned_adjustment()
        if stock_adjustment:
            adjustment = self.kwargs.get('adjustment', 0)
            unit_price = self.kwargs.get('unit_price', 0)
            inventory_returned = self.create_inventory_returned(stock_adjustment, adjustment, unit_price)

            fifo = FifoInventoryAdjustment(self.inventory, adjustment)
            fifo.update_received_inventory(inventory_returned)

            content_type = self.get_return_content_type()
            inventory_total = 0

            for returned_line in fifo.returned_lines:
                # self.inventory_fifo_processor(self.inventory, self.adjustment, inventory_returned)
                price = returned_line.price * -1
                quantity = returned_line.quantity * -1
                inventory_total += float(price) * float(quantity)
                self.create_inventory_ledger(self.profile,
                                             self.inventory,
                                             price,
                                             quantity,
                                             stock_adjustment,
                                             content_type,
                                             **{'journal': None, 'is_return': True}
                                             )
            return self.create_returned_general_journal(stock_adjustment, inventory_total, content_type)

    def create_returned_general_journal(self, stock_adjustment, amount, content_type):
        company = stock_adjustment.branch.company

        account = self.kwargs.get('account', None)
        text = self.kwargs.get('reason', None)

        stock_adjustment_account = self.get_stock_adjustment_account(account, self.inventory, company)
        control_account = self.get_stock_control_account(self.inventory)

        journal = Journal.objects.create(
            company=stock_adjustment.branch.company,
            year=stock_adjustment.period.period_year,
            period=stock_adjustment.period,
            journal_text=f"Stock adjustment - {stock_adjustment}  ({self.inventory})",
            module=Module.INVENTORY,
            role=stock_adjustment.role,
            date=stock_adjustment.date,
            created_by=stock_adjustment.profile
        )
        if journal:
            JournalLine.objects.create(
                journal=journal,
                account=control_account,
                credit=amount,
                is_reversal=False,
                text=text,
                content_type=content_type,
                object_id=stock_adjustment.id,
                accounting_date=stock_adjustment.date
            )
            JournalLine.objects.create(
                journal=journal,
                account=stock_adjustment_account,
                debit=amount,
                is_reversal=False,
                text=text,
                content_type=content_type,
                object_id=stock_adjustment.id,
                accounting_date=stock_adjustment.date
            )
        return journal


class AdjustInventoryStock:

    def __init__(self, company, branch, inventory, profile, role, form_data):
        logger.info("-----------AdjustInventoryStock--------")
        self.company = company
        self.branch = branch
        self.profile = profile
        self.role = role
        self.inventory = inventory
        self.adjustment = form_data.get('adjustment')
        self.unit_price = form_data.get('price', 0)
        self.form_data = form_data

    def execute(self):
        if self.adjustment > 0:
            if not self.unit_price:
                raise InventoryException('Unit price is not specified, please select or enter valid price')

            self.receive_inventory()
        else:
            self.return_inventory()

    def receive_inventory(self):
        stock_adjustment = Received.objects.create(
            period=self.form_data.get('period'),
            date=self.form_data.get('date'),
            note=self.form_data.get('reason'),
            movement_type=MovementType.STOCK_ADJUSTMENT,
            profile=self.profile,
            branch=self.branch,
            role=self.role
        )
        if stock_adjustment:
            inventory_received = InventoryReceived.objects.create_received_inventory(
                stock_adjustment, self.inventory, self.unit_price, self.adjustment
            )
            content_type = self.get_receive_content_type()
            inventory_total = Decimal(self.unit_price) * Decimal(self.adjustment)
            journal = self.create_received_general_journal(stock_adjustment, inventory_total, content_type)

            self.create_inventory_ledger(
                self.profile, self.inventory, self.unit_price, self.adjustment, stock_adjustment, content_type, journal)
            history = RegisterInventoryHistory(inventory=inventory_received.inventory,
                                               transaction=stock_adjustment,
                                               price=self.unit_price,
                                               quantity=self.adjustment,
                                               date=stock_adjustment.date)
            history.create()

    def get_stock_adjustment_account(self, account, inventory, company):
        if account:
            return account
        if inventory.stock_adjustment_account:
            return inventory.stock_adjustment_account
        if company.stock_adjustment_account:
            return company.stock_adjustment_account
        raise JournalAccountUnavailable('Stock adjustment account not set, please fix')

    def get_stock_control_account(self, inventory):
        if not inventory.gl_account:
            raise JournalAccountUnavailable('Stock control account not set, please fix')
        return inventory.gl_account

    def create_received_general_journal(self, stock_adjustment, amount, content_type):
        logger.info('--------- create_received_general_journal -----')
        company = stock_adjustment.branch.company

        account = self.form_data.get('account', None)
        text = self.form_data.get('reason', None)

        stock_adjustment_account = self.get_stock_adjustment_account(account, self.inventory, company)
        control_account = self.get_stock_control_account(self.inventory)

        journal = Journal.objects.create(
            company=stock_adjustment.branch.company,
            year=stock_adjustment.period.period_year,
            period=stock_adjustment.period,
            journal_text=f"Stock adjustment - {stock_adjustment}  ({self.inventory})",
            module=Module.INVENTORY,
            role=stock_adjustment.role,
            date=stock_adjustment.date,
            created_by=stock_adjustment.profile
        )
        if journal:
            JournalLine.objects.create(
                journal=journal,
                account=control_account,
                debit=amount,
                is_reversal=False,
                text=text,
                content_type=content_type,
                object_id=stock_adjustment.id,
                accounting_date=stock_adjustment.date
            )
            JournalLine.objects.create(
                journal=journal,
                account=stock_adjustment_account,
                credit=amount,
                is_reversal=False,
                text=text,
                content_type=content_type,
                object_id=stock_adjustment.id,
                accounting_date=stock_adjustment.date
            )
        return journal

    def create_returned_general_journal(self, stock_adjustment, amount, content_type):
        company = stock_adjustment.branch.company

        account = self.form_data.get('account', None)
        text = self.form_data.get('reason', None)

        stock_adjustment_account = self.get_stock_adjustment_account(account, self.inventory, company)
        control_account = self.get_stock_control_account(self.inventory)

        journal = Journal.objects.create(
            company=stock_adjustment.branch.company,
            year=stock_adjustment.period.period_year,
            period=stock_adjustment.period,
            journal_text=f"Stock adjustment - {stock_adjustment}  ({self.inventory})",
            module=Module.INVENTORY,
            role=stock_adjustment.role,
            date=stock_adjustment.date,
            created_by=stock_adjustment.profile
        )
        if journal:
            JournalLine.objects.create(
                journal=journal,
                account=control_account,
                credit=amount,
                is_reversal=False,
                text=text,
                content_type=content_type,
                object_id=stock_adjustment.id,
                accounting_date=stock_adjustment.date
            )
            JournalLine.objects.create(
                journal=journal,
                account=stock_adjustment_account,
                debit=amount,
                is_reversal=False,
                text=text,
                content_type=content_type,
                object_id=stock_adjustment.id,
                accounting_date=stock_adjustment.date
            )
        return journal

    def return_inventory(self):
        stock_adjustment = Returned.objects.create(
            period=self.form_data.get('period'),
            date=self.form_data.get('date'),
            note=self.form_data.get('reason'),
            movement_type=MovementType.STOCK_ADJUSTMENT,
            profile=self.profile,
            branch=self.branch,
            role=self.role
        )
        if stock_adjustment:
            inventory_returned = InventoryReturned.objects.create(
                returned=stock_adjustment,
                quantity=self.adjustment,
                price_excluding=self.unit_price,
                inventory=self.inventory
            )

            fifo = FifoInventoryAdjustment(self.inventory, self.adjustment)
            fifo.update_received_inventory(inventory_returned)
            content_type = self.get_return_content_type()
            inventory_total = 0

            for returned_line in fifo.returned_lines:
                # self.inventory_fifo_processor(self.inventory, self.adjustment, inventory_returned)
                price = returned_line.price * -1
                quantity = returned_line.quantity * -1
                inventory_total += float(price) * float(quantity)
                self.create_inventory_ledger(self.profile,
                                             self.inventory,
                                             price,
                                             quantity,
                                             stock_adjustment,
                                             content_type,
                                             None,
                                             True
                                             )
            self.create_returned_general_journal(stock_adjustment, inventory_total, content_type)

    def get_return_content_type(self):
        content_type = ContentType.objects.filter(app_label='inventory', model='returned').first()
        return content_type

    def get_receive_content_type(self):
        content_type = ContentType.objects.filter(app_label='inventory', model='received').first()
        return content_type

    def create_inventory_ledger(self,  profile, inventory, price, quantity, stock_adjustment, content_type,
                                journal=None, is_return=False):
        if content_type:
            Ledger.objects.create(
                inventory=inventory,
                content_type=content_type,
                object_id=stock_adjustment.id,
                quantity=quantity,
                value=price,
                date=stock_adjustment.date,
                created_by=profile,
                period=stock_adjustment.period,
                text='Stock Adjustment',
                journal=journal,
                rate=-1 if is_return else 1,
                object_reference=str(stock_adjustment)
                )

    def get_received_inventory_items(self, inventory_id):
        branch_inventory = BranchInventory.objects.prefetch_related(
            'inventory_returned', 'inventory_received', 'inventory_received__returned_items'
        ).filter(id=inventory_id).first()

        total_returned = 0
        total_received = 0
        available_inventory_items = []
        for _inventory_rec in branch_inventory.inventory_received.all():
            total_received += _inventory_rec.quantity
            received_returned = 0
            for rec_ret in _inventory_rec.returned_items.all():
                received_returned += rec_ret.quantity
            available_quantity = _inventory_rec.quantity - received_returned
            if available_quantity > 0:
                available_inventory_items.append({'received': _inventory_rec, 'in_hand': available_quantity})
        return available_inventory_items

    def get_last_price(self, branch_inventory):
        logger.info(f"Get current price from {branch_inventory}")
        current_price = branch_inventory.current_price
        return Decimal(current_price)

    def calculate_average_price(self, branch_inventory):
        logger.info(f"Get average price from {branch_inventory}")
        average_price = branch_inventory.average_price
        return Decimal(average_price)
