from django.apps import AppConfig


class StockadjustmentConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.stockadjustment'
