from django.urls import path

from . import views

urlpatterns = [
    path('', views.StockAdjustmentView.as_view(), name='stock_adjustment'),
    path('create/', views.CreateInventoryAdjustStockView.as_view(), name='create_stock_adjustment'),
    path('<int:pk>/detail/', views.ViewStockAdjustmentView.as_view(), name='view_adjustment'),
    path('calculate/<int:inventory_id>/total/', views.InventoryCalculatePriceView.as_view(), name='adjustment_calculate_total'),
    path('inventory/items/', views.InventoryItemsView.as_view(), name='stock_adjustment_inventory_items'),
]
