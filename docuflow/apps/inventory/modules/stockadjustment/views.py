import logging
import pprint
from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.forms import ValidationError
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import ListView, View, TemplateView, CreateView, DetailView

from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.models import Account
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import BranchInventory, StockAdjustment, InventoryReturned, InventoryReceived
from .forms import StockAdjustmentForm

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class StockAdjustmentView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'stockadjustment/index.html'


class AdjustInventoryStockView(ProfileMixin, ListView):
    template_name = 'stockadjustment/index.html'

    def get_context_data(self, **kwargs):
        context = super(AdjustInventoryStockView, self).get_context_data(**kwargs)
        return context


class CreateInventoryAdjustStockView(ProfileMixin, CreateView):
    template_name = 'stockadjustment/create.html'
    success_message = 'Stock adjustment successfully saved'
    form_class = StockAdjustmentForm
    model = StockAdjustment

    def get_initial(self):
        initial = super(CreateInventoryAdjustStockView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_context_data(self, **kwargs):
        context = super(CreateInventoryAdjustStockView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateInventoryAdjustStockView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['branch'] = self.get_branch()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the stock adjustment, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(CreateInventoryAdjustStockView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    form.save()
                    return JsonResponse({'text': 'Stock adjustment saved successfully',
                                         'error': False,
                                         'redirect': reverse('stock_adjustment')
                                         })
            except ValidationError as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True,
                                     })
            except InventoryException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'error': True,
                                     })


class ViewStockAdjustmentView(ProfileMixin, DetailView):
    template_name = 'stockadjustment/detail.html'
    model = StockAdjustment
    context_object_name = 'stock_adjustment'

    def get_queryset(self):
        queryset = super(ViewStockAdjustmentView, self).get_queryset()
        return queryset.select_related(
            'branch', 'branch__company', 'period'
        ).prefetch_related(
            'inventories', 'inventories__inventory', 'inventories__received', 'inventories__returns',
            'inventories__account'
        )

    def get_breakdown(self):
        inventory_adjustment = self.get_object()
        adjustments_breakdown = {}
        adjustment_total = 0
        for inventory_adjusted in inventory_adjustment.inventories.all():
            adjustments_breakdown[inventory_adjusted] = {'breakdown': [], 'breakdown_total': 0}
            for returned_item in inventory_adjusted.returns.all():
                quantity = returned_item.quantity
                price = returned_item.price
                inventory_item_breakdown = {}
                logger.info(f"returned item {returned_item} -> {returned_item.inventory_returned}")
                if isinstance(returned_item.inventory_returned, InventoryReturned):
                    opening, closing = returned_item.stock_movement(returned_item.inventory_returned)
                else:
                    opening, closing = returned_item.stock_movement(inventory_adjusted)
                inventory_item_breakdown['returned'] = returned_item
                inventory_item_breakdown['quantity'] = quantity
                inventory_item_breakdown['opening'] = opening
                inventory_item_breakdown['closing'] = closing
                total = price * quantity
                adjustment_total += total
                inventory_item_breakdown['price'] = price
                inventory_item_breakdown['total'] = total
                inventory_item_breakdown['quantity'] = quantity * -1
                adjustments_breakdown[inventory_adjusted]['breakdown_total'] += total
                adjustments_breakdown[inventory_adjusted]['breakdown'].append(inventory_item_breakdown)
            for received_item in inventory_adjusted.received.all():
                price = received_item.price
                quantity = received_item.quantity
                inventory_item_breakdown = {}
                logger.info(f"returned item {received_item} -> {received_item.inventory_received}")
                if isinstance(received_item.inventory_returned, InventoryReceived):
                    opening, closing = received_item.stock_movement(received_item.inventory_received)
                else:
                    opening, closing = received_item.stock_movement(inventory_adjusted)

                inventory_item_breakdown['returned'] = received_item
                inventory_item_breakdown['quantity'] = quantity
                inventory_item_breakdown['opening'] = opening
                inventory_item_breakdown['closing'] = closing
                total = price * quantity
                adjustment_total += total
                inventory_item_breakdown['price'] = price
                inventory_item_breakdown['total'] = total
                inventory_item_breakdown['quantity'] = quantity
                adjustments_breakdown[inventory_adjusted]['breakdown_total'] += total
                adjustments_breakdown[inventory_adjusted]['breakdown'].append(inventory_item_breakdown)
        adjustments_breakdown['total'] = adjustment_total
        return adjustments_breakdown

    def get_context_data(self, **kwargs):
        context = super(ViewStockAdjustmentView, self).get_context_data(**kwargs)
        context['adjustments'] = self.get_breakdown()
        return context


class InventoryCalculatePriceView(View):

    def get(self, request, *args, **kwargs):
        try:
            quantity = float(self.request.GET.get('quantity', 0))
            price_type = self.request.GET.get('price_type', 'average')
            inventory_id = self.kwargs.get('inventory_id')
            if quantity > 0 and inventory_id:
                branch_inventory = BranchInventory.objects.filter(id=inventory_id).first()
                total = 0
                if branch_inventory:
                    price = 0
                    if price_type == 'average':
                        price = branch_inventory.average_price
                    elif price_type == 'last_price':
                        price = branch_inventory.current_price
                    total = float(price) * float(quantity)
                return JsonResponse({'total': round(total, 2)})
            return JsonResponse({'error': True})
        except Exception as exception:
            return JsonResponse({'error': True, 'details': str(exception)})


class InventoryItemsView(ProfileMixin, TemplateView):
    template_name = 'stockadjustment/inventory_items.html'

    def get_context_data(self, **kwargs):
        context = super(InventoryItemsView, self).get_context_data(**kwargs)
        context['inventories'] = BranchInventory.objects.select_related(
            'inventory'
        ).filter(branch=self.get_branch()).only('id', 'inventory__code', 'description')
        context['accounts'] = Account.objects.filter(company=self.get_company()).only('id', 'code', 'name')
        context['row'] = int(self.request.GET.get('rows', 0))
        return context
