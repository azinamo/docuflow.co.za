import logging
from decimal import Decimal

from django import forms
from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.models import StockAdjustment, StockAdjustmentInventory, Ledger, InventoryReceivedReturned
from docuflow.apps.period.models import Period
from docuflow.apps.inventory.enums import PriceType
from docuflow.apps.journals.models import Journal, JournalLine
from docuflow.apps.inventory.exceptions import JournalAccountUnavailable, InventoryException
from docuflow.apps.inventory.services import FiFoReceived, FiFoReturned, is_valuation_balancing

logger = logging.getLogger(__name__)


class StockAdjustmentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.branch = kwargs.pop('branch')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(StockAdjustmentForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    class Meta:
        model = StockAdjustment
        fields = ('date', 'period', 'comment')
        widgets = {
            'comment': forms.Textarea()
        }

    def clean(self):
        cleaned_data = super(StockAdjustmentForm, self).clean()
        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and date do not match')

        return cleaned_data

    def create_journal(self, adjustment):
        return Journal.objects.create(
            company=adjustment.branch.company,
            year=adjustment.period.period_year,
            period=adjustment.period,
            journal_text=f"Stock adjustment - {adjustment}",
            module=Module.INVENTORY,
            role=adjustment.role,
            date=adjustment.date,
            created_by=adjustment.profile
        )

    def save(self, commit=True):
        adjustment = super().save(commit=False)
        adjustment.branch = self.branch
        adjustment.profile = self.profile
        adjustment.role = self.role
        adjustment.save()

        counter = 1
        journal = self.create_journal(adjustment)
        for key, value in self.request.POST.items():
            if key.endswith('_inventory'):
                row_id = key[15:-10]
                inventory_id = self.request.POST.get(f"inventory_form_{row_id}_inventory")
                type_of_price = self.request.POST.get(f"inventory_form_{row_id}_price_type")
                if type_of_price == 'last_price':
                    price_type = PriceType.LAST_PRICE
                elif type_of_price == 'own_price':
                    price_type = PriceType.OWN
                else:
                    price_type = PriceType.AVERAGE
                if inventory_id:
                    data = {
                        'inventory': inventory_id,
                        'account': self.request.POST.get(f"inventory_form_{row_id}_account"),
                        'quantity': self.request.POST.get(f"inventory_form_{row_id}_adjustment"),
                        'price': self.request.POST.get(f"inventory_form_{row_id}_price", 0),
                        'price_type': price_type,
                        'adjustment': adjustment
                    }
                    inventory_form = InventoryStockAdjustmentForm(branch=self.branch, journal=journal, data=data)
                    if inventory_form.is_valid():
                        inventory_form.save()
                    else:
                        for field_key, errors in dict(inventory_form.errors).items():
                            if isinstance(errors, list):
                                for err in errors:
                                    raise forms.ValidationError(err)
                    counter += 1
        if not is_valuation_balancing(self.branch, self.year):
            raise InventoryException('A variation in inventory accounts and inventory valuation detected.')
        return adjustment


class InventoryStockAdjustmentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.journal = kwargs.pop('journal')
        super(InventoryStockAdjustmentForm, self).__init__(*args, **kwargs)
        self.fields['inventory'].queryset = self.fields['inventory'].queryset.filter(branch=self.branch)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.branch.company)

    class Meta:
        model = StockAdjustmentInventory
        fields = ('inventory', 'quantity', 'price', 'price_type', 'account', 'adjustment')

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['quantity'] > 0:
            if cleaned_data['price'] == 0 or not cleaned_data['price']:
                raise forms.ValidationError(f"Please enter valid price for incoming inventory {cleaned_data['inventory']}")

    def save(self, commit=False):
        inventory_adjustment = super().save(commit=False)
        inventory_adjustment.save()
        if inventory_adjustment:
            stock_adjustment = inventory_adjustment.adjustment
            content_type = ContentType.objects.get_for_model(inventory_adjustment)
            if inventory_adjustment.quantity > 0:
                received = FiFoReceived(
                    inventory=inventory_adjustment.inventory,
                    quantity=inventory_adjustment.quantity,
                    unit_price=inventory_adjustment.price,
                    content_object=inventory_adjustment,
                    content_type=ContentType.objects.get_for_model(inventory_adjustment)
                )
                received.execute()

                total = self.get_adjustment_total(content_type, inventory_adjustment)

                self.create_inventory_received_journal(self.journal, inventory_adjustment, total, content_type)

                inventory_adjustment.inventory.recalculate_balances(year=stock_adjustment.period.period_year)

                self.create_inventory_ledger(self.journal, stock_adjustment, inventory_adjustment.inventory,
                                             inventory_adjustment, content_type, False, total)

            else:
                qty = inventory_adjustment.quantity * -1
                returned = FiFoReturned(
                    inventory=inventory_adjustment.inventory,
                    quantity=qty,
                    unit_price=inventory_adjustment.price,
                    content_object=inventory_adjustment
                )
                returned.execute()
                total = Decimal(0)
                logger.info("<--- Returned Lines --->")
                logger.info(returned.returned_lines)

                inventory_adjustment.inventory.recalculate_balances(year=stock_adjustment.period.period_year)

                for returned_line in returned.returned_lines:
                    self.create_inventory_ledger(self.journal, stock_adjustment, inventory_adjustment.inventory,
                                                 returned_line, content_type, True, returned_line.returned_value)
                    total += returned_line.returned_value
                    logger.info(f"Returned value {returned_line.returned_value}, now total amount of returned items {total}")

                self.create_returned_general_journal(self.journal, inventory_adjustment, total, content_type)

    def get_adjustment_total(self, content_type, inventory_adjustment):
        received_total = InventoryReceivedReturned.objects.calculate_received_total(content_type, inventory_adjustment)
        return received_total['total_amount']

    def get_stock_control_account(self, inventory):
        if not inventory.gl_account:
            raise JournalAccountUnavailable('Stock control account not set, please fix')
        return inventory.gl_account

    def get_stock_adjustment_account(self, inventory, company):
        if inventory.stock_adjustment_account:
            return inventory.stock_adjustment_account
        if company.stock_adjustment_account:
            return company.stock_adjustment_account
        raise JournalAccountUnavailable('Stock adjustment account not set, please fix')

    def create_inventory_received_journal(self, journal, inventory_adjustment, total, content_type):
        JournalLine.objects.create(
            journal=journal,
            account=self.get_stock_control_account(inventory_adjustment.inventory),
            debit=total,
            is_reversal=False,
            text=str(inventory_adjustment.inventory),
            accounting_date=journal.date,
            object_id=inventory_adjustment.id,
            content_type=content_type
        )
        JournalLine.objects.create(
            journal=journal,
            account=self.get_stock_adjustment_account(inventory_adjustment.inventory, journal.company),
            credit=total,
            is_reversal=False,
            text=str(inventory_adjustment.inventory),
            accounting_date=journal.date,
            object_id=inventory_adjustment.id,
            content_type=content_type
        )

    def create_inventory_ledger(self, journal, stock_adjustment, inventory, adjustment, content_type, is_return=False, total=None):
        if content_type:
            Ledger.objects.create(
                inventory=inventory,
                content_type=content_type,
                object_id=adjustment.id,
                quantity=adjustment.quantity * -1 if is_return else adjustment.quantity,
                value=adjustment.price,
                date=stock_adjustment.date,
                created_by=stock_adjustment.profile,
                period=stock_adjustment.period,
                journal=journal,
                rate=1,
                total_value=total,
                text='Stock Adjustment',
                object_reference=str(stock_adjustment)
            )

    def create_returned_general_journal(self, journal, inventory_adjustment, total, content_type):
        JournalLine.objects.create(
            journal=journal,
            account=self.get_stock_control_account(inventory_adjustment.inventory),
            credit=total,
            is_reversal=False,
            text=str(inventory_adjustment.inventory),
            content_type=content_type,
            object_id=inventory_adjustment.id,
            accounting_date=inventory_adjustment.adjustment.date
        )
        JournalLine.objects.create(
            journal=journal,
            account=self.get_stock_adjustment_account(inventory_adjustment.inventory, journal.company),
            debit=total,
            is_reversal=False,
            text=str(inventory_adjustment.inventory),
            content_type=content_type,
            object_id=inventory_adjustment.id,
            accounting_date=inventory_adjustment.adjustment.date
        )
