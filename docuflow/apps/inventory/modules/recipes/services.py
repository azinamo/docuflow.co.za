import logging
from decimal import Decimal

from docuflow.apps.inventory.models import RecipeInventory

logger = logging.getLogger(__name__)


class CreateRecipe:

    def __init__(self, branch, profile, role, form, post_request):
        logger.info("-----------Create Recipe-----------")
        self.branch = branch
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request

    def execute(self):
        self.validate_data()
        recipe = self.create()

        if recipe:
            create_recipe_item = CreateRecipeItem(recipe, self.post_request)
            create_recipe_item.execute()

    def validate_data(self):
        pass

    def create(self):
        recipe = self.form.save(commit=False)
        recipe.profile = self.profile
        recipe.role = self.role
        recipe.save()
        return recipe


class UpdateRecipe:

    def __init__(self, profile, role, form, request_post):
        logger.info("---- Update Recipe ----")
        self.profile = profile
        self.role = role
        self.form = form
        self.request_post = request_post
        logger.info(request_post)

    def execute(self):
        recipe = self.update()

        create_recipe_item = CreateRecipeItem(recipe, self.request_post)
        create_recipe_item.execute()

    def update(self):
        recipe = self.form.save(commit=False)
        recipe.profile = self.profile
        recipe.role = self.role
        recipe.save()
        return recipe


class CreateRecipeItem:

    def __init__(self, recipe, recipe_items):
        logger.info('Create recipe item')
        self.recipe = recipe
        self.recipe_items = recipe_items
        logger.info(recipe_items)

    def execute(self):
        self.create_recipe_items()

    def create_recipe_items(self):
        for field in self.recipe_items:
            if field[0:9] == 'quantity_':
                if self.recipe_items[field] != '':
                    key = field[8:]
                    quantity = float(self.recipe_items[field])
                    unit_id = self.recipe_items.get("unit{}".format(key), None)
                    inventory_item_id = self.recipe_items.get("inventory_item{}".format(key), None)
                    recipe_item_id = self.recipe_items.get("recipe_item_id{}".format(key), None)
                    logger.info("Recipe is {}".format(recipe_item_id))
                    if not recipe_item_id:
                        self.create(quantity, inventory_item_id, unit_id)
                    else:
                        self.update_receipe_item(recipe_item_id, quantity, inventory_item_id, unit_id)

    def create(self, quantity, inventory_id, unit_id):
        logger.info('Create {} for {} using {}'.format(inventory_id, quantity, unit_id))
        if unit_id and inventory_id:
            quantity = Decimal(quantity)
            logger.info("Unit --> {} na item id {} and quantity is {}".format(unit_id,
                                                                              inventory_id,
                                                                              quantity))
            recipe_inventory = RecipeInventory.objects.filter(
                recipe=self.recipe,
                inventory_id=inventory_id
            ).first()
            if recipe_inventory:
                logger.info("Already exist, do not create it")
                recipe_inventory.unit_id = unit_id
                recipe_inventory.quantity = quantity
                recipe_inventory.save()
            else:
                logger.info("Does not exist, create it")
                RecipeInventory.objects.create(
                    recipe=self.recipe,
                    unit_id=unit_id,
                    quantity=quantity,
                    inventory_id=inventory_id
                )

    def update_receipe_item(self, recipe_item_id, quantity, inventory_id, unit_id):
        logger.info('Create {} for {} using {}'.format(inventory_id, quantity, unit_id))
        if unit_id and inventory_id:
            quantity = Decimal(quantity)
            logger.info("Unit --> {} na item id {} and quantity is {}".format(unit_id,
                                                                              inventory_id,
                                                                              quantity))
            recipe_inventory = RecipeInventory.objects.filter(id=recipe_item_id).first()
            if recipe_inventory:
                logger.info("Already exist, update it")
                recipe_inventory.inventory_id = inventory_id
                recipe_inventory.unit_id = unit_id
                recipe_inventory.quantity = quantity
                recipe_inventory.save()
