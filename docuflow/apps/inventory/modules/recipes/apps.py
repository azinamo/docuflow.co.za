from django.apps import AppConfig


class RecipesConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.recipes'
