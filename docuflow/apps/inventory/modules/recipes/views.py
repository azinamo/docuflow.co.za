import logging
import pprint
from collections import defaultdict

from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse, reverse_lazy
from django.views.generic import View, TemplateView
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.models import Unit
from docuflow.apps.inventory.models import BranchInventory, Recipe, RecipeInventory
from .forms import CreateRecipeForm
from .services import CreateRecipe, UpdateRecipe

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class RecipeView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'recipes/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context


class CreateRecipeView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = Recipe
    form_class = CreateRecipeForm
    template_name = 'recipes/create.html'
    success_message = 'Recipe successfully created'
    success_url = reverse_lazy('recipe_index')

    def get_form_kwargs(self):
        kwargs = super(CreateRecipeView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the recipe, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateRecipeView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            with transaction.atomic():
                profile = self.get_profile()
                branch = self.get_branch()
                role = Role.objects.filter(id=self.request.session['role']).first()
                create_recipe = CreateRecipe(branch, profile, role, form, self.request.POST)
                create_recipe.execute()

                return JsonResponse({
                    'text': 'Recipe saved successfully.',
                    'error': False,
                    'redirect': reverse('recipe_index')
                })
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditRecipeView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = Recipe
    form_class = CreateRecipeForm
    template_name = 'recipes/edit.html'
    success_message = 'Recipe successfully updated'
    success_url = reverse_lazy('recipe_index')

    def get_form_kwargs(self):
        kwargs = super(EditRecipeView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred updating the recipe, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(EditRecipeView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            with transaction.atomic():
                profile = self.get_profile()
                role = Role.objects.filter(id=self.request.session['role']).first()

                update_recipe = UpdateRecipe(profile, role, form, self.request.POST)
                update_recipe.execute()

                return JsonResponse({
                    'text': 'Recipe saved successfully.',
                    'error': False,
                    'redirect': reverse('recipe_index')
                })
        else:
            return HttpResponseRedirect(self.get_success_url())


class DeleteRecipeView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, View):
    success_url = reverse_lazy('recipe_index')

    def get(self, request, *args, **kwargs):
        recipe = get_object_or_None(Recipe, id=self.kwargs['pk'])
        if recipe:
            recipe.delete()
            messages.success(self.request, 'Recipe deleted successfully')
        return HttpResponseRedirect(reverse_lazy('recipe_index'))


class RecipeItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'recipes/recipe_items.html'

    def get_context_data(self, **kwargs):
        context = super(RecipeItemsView, self).get_context_data(**kwargs)
        context['row'] = self.request.GET.get('row_id', 0)
        return context


class EditRecipeItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'recipes/edit_recipe_items.html'

    def get_recipe(self):
        recipe = Recipe.objects.prefetch_related(
            'recipe_items'
        ).filter(id=self.kwargs['pk']).first()
        return recipe

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch(), is_recipe_item=False)
        return branch_inventories

    def get_measure_units(self):
        units = Unit.objects.filter(measure__company=self.get_company())
        unit_list = defaultdict(list)
        for unit in units:
            unit_list[unit.measure].append(unit)
        return unit_list

    def get_recipe_inventory_items(self, recipe):
        recipe_items = {}
        measure_units = self.get_measure_units()
        for recipe_item in recipe.recipe_items.all():
            item_units = None
            if recipe_item.unit:
                item_units = measure_units.get(recipe_item.unit.measure)
            recipe_items[recipe_item] = {'units': item_units, 'unit': recipe_item.unit.measure}
        return recipe_items

    def get_context_data(self, **kwargs):
        context = super(EditRecipeItemsView, self).get_context_data(**kwargs)
        recipe = self.get_recipe()
        recipe_inventory_items = self.get_recipe_inventory_items(recipe)
        context['recipe_inventory_items'] = recipe_inventory_items
        context['row'] = len(recipe_inventory_items) + 1
        context['recipe'] = self.get_recipe()
        return context


class DeleteRecipeItemView(View):

    def get_inventory_recipe_item(self):
        return get_object_or_None(RecipeInventory, pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        recipe_inventory_item = self.get_inventory_recipe_item()
        if recipe_inventory_item:
            recipe_inventory_item.delete()
            return JsonResponse({
                'text': 'Recipe inventory deleted successfully',
                'error': False
            })
        else:
            return JsonResponse({
                'text': 'Recipe inventory not found',
                'error': True
            })
