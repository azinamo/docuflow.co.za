from collections import OrderedDict
from decimal import Decimal
from django import forms
from django.urls import reverse_lazy

from django_select2.forms import Select2Widget, ModelSelect2Widget

from docuflow.apps.inventory.models import BranchInventory, Received, Returned, Recipe
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.company.models import Account, VatCode, Branch
from docuflow.apps.supplier.models import Supplier
from docuflow.apps.period.models import Period, Year
from django.forms import ModelChoiceField


class InventoryModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return str(obj.full_name)


class CreateRecipeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        branch = kwargs.pop('branch')
        super(CreateRecipeForm, self).__init__(*args, **kwargs)
        self.fields['inventory'].queryset = BranchInventory.objects.filter(branch=branch, is_recipe_item=True)

    inventory = InventoryModelChoiceField(required=True, label='Inventory', queryset=None)

    class Meta:
        model = Recipe
        fields = ('inventory', 'description')

        widgets = {
            'inventory': forms.Select(attrs={
                'class': 'form-control chosen-select',
                'id': 'id_recipe_inventory',
                'data-items-ajax-url': reverse_lazy('recipe_inventory_items')}
            ),
        }
