from django.urls import path

from . import views

urlpatterns = [
    path('', views.RecipeView.as_view(), name='recipe_index'),
    path('create/', views.CreateRecipeView.as_view(), name='create_recipe'),
    path('<int:pk>/edit/', views.EditRecipeView.as_view(), name='edit_recipe'),
    path('<int:pk>/delete/', views.DeleteRecipeView.as_view(), name='delete_recipe'),
    path('inventory/items/', views.RecipeItemsView.as_view(), name='recipe_inventory_items'),
    path('add/inventory/item/', views.RecipeItemsView.as_view(), name='add_recipe_inventory_items'),
    path('delete/<int:pk>/inventory/item/', views.DeleteRecipeItemView.as_view(), name='delete_recipe_inventory_item'),
    path('edit/<int:pk>/inventory/item/', views.EditRecipeItemsView.as_view(), name='edit_recipe_inventory_items'),

]
