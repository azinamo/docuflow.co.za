import logging
from collections import defaultdict
from decimal import Decimal

from django import forms
from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse_lazy

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.models import (Returned, InventoryReturned, Ledger as InventoryLedger, BranchInventory,
                                            InventoryReceived)
from docuflow.apps.supplier.models import Supplier
from docuflow.apps.period.models import Period
from docuflow.apps.journals.models import Journal, JournalLine
from docuflow.apps.inventory.exceptions import JournalAccountUnavailable, InventoryException
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.services import is_valuation_balancing, return_received


logger = logging.getLogger(__name__)


class ReturnedForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.branch = kwargs.pop('branch')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(ReturnedForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).select_related(
            'period_year'
        ).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['supplier'].queryset = Supplier.objects.filter(company=self.company)
        self.fields['received_movement'].queryset = self.fields['received_movement'].queryset.filter(
            branch=self.branch
        ).select_related(
           'period', 'period__period_year'
        )

    note = forms.CharField(label='Note to Supplier for return', required=False)

    class Meta:
        model = Returned
        fields = ('supplier', 'period', 'date', 'received_movement', 'note', 'total_vat', 'total_amount')

        widgets = {
            'received_movement': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'note': forms.TextInput(attrs={'class': 'form-control'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'supplier': forms.Select(attrs={'class': 'form-control chosen-select',
                                            'id': 'id_supplier_returned',
                                            'data-goods-received-url': reverse_lazy('goods_received_list'),
                                            'data-inventory-ajax-url': reverse_lazy('supplier_inventory_list')
                                            }),
        }

    def clean(self):
        cleaned_data = super(ReturnedForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')
        return cleaned_data

    def save(self, commit=True):
        returned = super().save(commit=False)
        returned.branch = self.branch
        returned.movement_type = MovementType.GENERAL
        returned.profile = self.profile
        returned.role = self.role
        returned.save()

        account_journal_lines = defaultdict(Decimal)
        inventory_journal_lines = defaultdict(list)
        full_return = True

        for field, value in self.request.POST.items():
            if field.startswith('inventory_item', 0, 14):
                counter = field[15:]

                data = {
                    'inventory': value,
                    'quantity': self.request.POST.get(f'quantity_received_{counter}'),
                    'price': self.request.POST.get(f'price_{counter}'),
                    'price_including': self.request.POST.get(f'price_including_{counter}'),
                    'price_excluding': self.request.POST.get(f'price_{counter}'),
                    'vat': self.request.POST.get(f'vat_code_{counter}'),
                    'inventory_received': self.request.POST.get(f'inventory_received_{counter}'),
                    'returned': returned
                }
                total_price_excluding = 0
                inventory_returned_form = InventoryReturnedForm(data=data, branch=self.branch)
                if inventory_returned_form.is_valid():
                    inventory_returned = inventory_returned_form.save()

                    inventory_journal_lines[inventory_returned.inventory].append(inventory_returned)
                    account_journal_lines[inventory_returned.inventory.gl_account] += inventory_returned.total_price_excluding

                    # if inventory_returned.quantity != inventory_received.quantity:
                    #     full_return = False

        if full_return:
            received = returned.received_movement
            if not received.has_invoice_account:
                returned.status = Returned.COMPLETED
                received.save()
        returned.save()

        content_type = ContentType.objects.get_for_model(returned)

        journal = self.create_journal_entries(account_journal_lines, returned, content_type)
        self.create_inventory_journal(inventory_journal_lines, returned, journal, content_type)

        if not is_valuation_balancing(self.branch, self.year):
            raise InventoryException('A variation in inventory accounts and inventory valuation detected.')

        return returned

    def create_journal_entries(self, account_journal_lines, returned, content_type):
        if not self.company.default_goods_received_account:
            raise JournalAccountUnavailable('Default Goods Received Interim Account not available, please fix.')
        journal = Journal.objects.create(
            company=self.company,
            year=returned.period.period_year,
            period=returned.period,
            journal_text=f"Goods returned - {str(returned)}",
            module=Module.INVENTORY.value,
            role=self.role,
            date=returned.date,
            created_by=self.profile
        )
        if journal:
            # Reverse accounts as received returned
            total_amount = 0
            for account, total in account_journal_lines.items():
                JournalLine.objects.create(
                    journal=journal,
                    account=account,
                    credit=total,
                    is_reversal=False,
                    accounting_date=returned.date,
                    text=f"Goods returned - {str(returned)}",
                    object_id=returned.id,
                    content_type=content_type,
                    ledger_type_reference=returned.supplier.name if returned.supplier else ''
                )
                total_amount += total

            JournalLine.objects.create(
                journal=journal,
                account=self.company.default_goods_received_account,
                debit=total_amount,
                is_reversal=False,
                accounting_date=returned.date,
                text=f"Goods returned - {str(returned)}",
                object_id=returned.id,
                content_type=content_type,
                ledger_type_reference=returned.supplier.name if returned.supplier else ''
            )
        return journal

    def calculate_price_including(self, vat_code, price):
        if vat_code:
            price = ((vat_code.percentage / 100) * float(price)) + float(price)
        return Decimal(price)

    def create_inventory_journal(self, inventory_lines, movement, journal, content_type):

        for inventory, inventory_movements in inventory_lines.items():
            for inventory_movement in inventory_movements:
                value = inventory_movement.price_excluding * -1
                quantity = inventory_movement.quantity * -1

                InventoryLedger.objects.create(
                    inventory=inventory,
                    content_type=content_type,
                    object_id=movement.id,
                    quantity=quantity,
                    value=Decimal(value),
                    created_by=self.profile,
                    date=movement.date,
                    period=movement.period,
                    supplier=movement.supplier,
                    text=str(movement),
                    journal=journal,
                    rate=-1,
                    object_owner=str(movement.supplier),
                    object_reference=str(movement)
                    )

    def get_received_inventory_items(self):
        branch_inventories = BranchInventory.objects.prefetch_related(
            'inventory_returned', 'inventory_received', 'inventory_received'
        ).filter(
            branch=self.branch
        )

        available_inventory_items = defaultdict(list)

        for branch_inventory in branch_inventories.all():
            total_returned = 0
            total_received = 0
            for _inventory_rec in branch_inventory.inventory_received.all():
                total_received += _inventory_rec.quantity
                available_quantity = _inventory_rec.quantity - _inventory_rec.quantity_returned
                if available_quantity > 0:
                    available_inventory_items[branch_inventory.id].append({'received': _inventory_rec,
                                                                           'in_hand': available_quantity
                                                                           })
        return available_inventory_items


class InventoryReturnedForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super(InventoryReturnedForm, self).__init__(*args, **kwargs)
        self.fields['inventory'].queryset = self.fields['inventory'].queryset.filter(branch=self.branch)

    inventory_received = forms.IntegerField()

    class Meta:
        model = InventoryReturned
        fields = ('quantity', 'price_excluding', 'price_including', 'inventory', 'vat', 'returned')

    def save(self, commit=True):
        inventory_returned = super(InventoryReturnedForm, self).save(commit=commit)
        logger.info("Load account lines ")

        inventory_received = InventoryReceived.objects.get(pk=self.cleaned_data['inventory_received'])
        logger.info(f"Start {inventory_received} fifo adjustment with received value {self.cleaned_data['inventory_received']}")
        return_received(inventory=inventory_returned.inventory, quantity=inventory_returned.quantity,
                        received_object=inventory_received, return_object=inventory_returned,
                        year=inventory_returned.returned.period.period_year)
        return inventory_returned
