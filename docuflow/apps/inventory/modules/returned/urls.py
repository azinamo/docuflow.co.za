from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.ReturnedView.as_view(), name='inventory_returned_index'),
    path('create/', views.CreateReturnedView.as_view(), name='create_returned'),
    path('<int:pk>/edit/', views.CreateReturnedView.as_view(), name='edit_goods_returned'),
    path('<int:pk>/detail/', views.ReturnedDetailView.as_view(), name='view_returned'),
    path('received/list/', views.ListGoodsReceivedView.as_view(), name='goods_received_list'),
    path('received/<int:movement_id>/item/list/', views.ReturnReceiveItemsView.as_view(), name='return_received_items'),
    path('create/general/returned/', csrf_exempt(views.CreateGeneralReturnedView.as_view()),
         name='create_general_returned'),
    path('supplier/received/items/', views.SupplierInventoriesReceivedView.as_view(),
         name='supplier_inventory_received_items'),
    path('supplier/inventory/items/', views.SupplierInventoriesView.as_view(), name='supplier_inventory_list'),
    path('purchase_order/search/list/', views.SearchPurchaseOrderItemView.as_view(),
         name='search_purchase_order_search_list'),
    path('purchase_order/list/', views.PurchaseOrderListView.as_view(), name='goods_purchase_order_list'),
    path('<int:returned_id>/inventory/items/', views.ReturnedInventoryItemsView.as_view(),
         name='returned_inventory_items'),
]
