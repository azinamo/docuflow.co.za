import logging
import pprint
from collections import defaultdict
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.exceptions import JournalAccountUnavailable, \
    NoReceivedInventoryException
from docuflow.apps.inventory.models import BranchInventory, Returned, InventoryReceived, \
    InventoryReturned, Ledger as InventoryLedger, InventoryReceivedReturned
from docuflow.apps.inventory.services import RegisterInventoryHistory
from docuflow.apps.journals.models import Journal, JournalLine

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class CreateGoodsReturned:

    def __init__(self, company, branch, profile, role, form, returned_items):
        logger.info("----------- start --- CreateGoodsReturned -----")
        self.company = company
        self.branch = branch
        self.profile = profile
        self.role = role
        self.form = form
        self.returned_items = returned_items

    def execute(self):
        returned = self.create()
        if returned:
            self.create_returned_items(returned)

    def create(self):
        logger.info("------create returned item----- -")
        returned = self.form.save(commit=False)
        returned.branch = self.branch
        returned.movement_type = Returned.PURCHASE_ORDER
        returned.profile = self.profile
        returned.role = self.role
        returned.save()
        return returned

    def create_returned_items(self, returned):
        logger.info(" --- create returned items --- ")
        total_amount = Decimal('0')
        total_vat = Decimal('0')
        account_journal_lines = defaultdict(Decimal)
        inventory_journal_lines = defaultdict(list)
        full_return = True
        content_type = ContentType.objects.filter(app_label='inventory', model='returned').first()
        for field, value in self.returned_items.items():
            if field.startswith('inventory_item', 0, 14):
                counter = field[15:]

                inventory_id = self.returned_items.get('inventory_item_{}'.format(counter))
                inventory = BranchInventory.objects.filter(id=inventory_id).first()
                if inventory:
                    logger.info("Inventory {} item ".format(inventory))
                    quantity = self.returned_items.get('quantity_received_{}'.format(counter))
                    logger.info("Quantity --> {}".format(quantity))
                    price_excluding = Decimal(self.returned_items.get('price_{}'.format(counter), 0))
                    logger.info("price_excluding --> {}".format(price_excluding))
                    price_including = Decimal(self.returned_items.get('price_including_{}'.format(counter), 0))
                    logger.info("price_including --> {}".format(price_including))
                    total_price_excluding = Decimal(self.returned_items.get('total_price_excluding_{}'.format(counter), 0))
                    logger.info("price_including --> {}".format(total_price_excluding))
                    total_price_including = Decimal(self.returned_items.get('total_price_including_{}'.format(counter), 0))
                    logger.info("total_price_including --> {}".format(total_price_including))
                    vat_code_id = self.returned_items.get('vat_code_{}'.format(counter), None)
                    received_id = self.returned_items.get('inventory_received_{}'.format(counter), None)
                    # vat_amount = float(self.returned_items.get('vat_amount_{}'.format(counter), None))
                    # logger.info("vat_amount --> {}".format(vat_amount))
                    inventory_received = InventoryReceived.objects.filter(id=received_id).first()
                    if not inventory_received:
                        raise NoReceivedInventoryException(f"No inventory received found for the {inventory} trying to be returned")

                    quantity_returned = Decimal(quantity)
                    if quantity_returned != inventory_received.quantity:
                        full_return = False

                    price_excluding = Decimal(price_excluding)
                    price_including = Decimal(price_including)
                    total_price_excluding = Decimal(total_price_excluding)
                    total_price_including = Decimal(total_price_including)

                    logger.info("Price exc {}, include {}, qty {}, vat_id {} ".format(price_excluding, price_including, quantity_returned, vat_code_id))

                    inventory_returned = InventoryReturned.objects.create_returned(
                        transaction=returned,
                        inventory=inventory,
                        price=price_excluding,
                        price_including=price_including,
                        quantity=quantity_returned,
                        vat_code_id=vat_code_id,
                        inventory_received=inventory_received
                    )

                    logger.info("Load account lines ")
                    inventory_journal_lines[inventory].append(inventory_returned)
                    account_journal_lines[inventory.gl_account] += total_price_excluding

                    logger.info("Start fifo adjustment")
                    history = RegisterInventoryHistory(
                        inventory=inventory,
                        price=inventory_received.price_excluding,
                        quantity=quantity_returned * -1
                    )
                    history.create()

        if full_return:
            received = returned.received_movement
            if not received.has_invoice_account:
                returned.status = Returned.COMPLETED
                received.status = Returned.COMPLETED
                received.save()
        returned.save()

        journal = self.create_journal_entries(self.company, account_journal_lines, returned, content_type)
        self.create_inventory_journal(self.profile, inventory_journal_lines, returned, journal, content_type)

    def create_journal_entries(self, company, account_journal_lines, returned, content_type):

        if not company.default_goods_received_account:
            raise JournalAccountUnavailable('Default Goods Received Interim Account not available, please fix.')
        journal = Journal.objects.create(
            company=company,
            year=returned.period.period_year,
            period=returned.period,
            journal_text=f"Goods returned - {str(returned)}",
            module=Module.INVENTORY,
            role=self.role,
            date=returned.date,
            created_by=self.profile
        )
        if journal:
            # Reverse accounts as received returned
            total_amount = 0
            for account, total in account_journal_lines.items():
                JournalLine.objects.create(
                    journal=journal,
                    account=account,
                    credit=total,
                    is_reversal=False,
                    accounting_date=returned.date,
                    text=f"Goods returned - {str(returned)}",
                    object_id=returned.id,
                    content_type=content_type,
                    ledger_type_reference=returned.supplier.name if returned.supplier else ''
                )
                total_amount += total

            JournalLine.objects.create(
                journal=journal,
                account=company.default_goods_received_account,
                debit=total_amount,
                is_reversal=False,
                accounting_date=returned.date,
                text=f"Goods returned - {str(returned)}",
                object_id=returned.id,
                content_type=content_type,
                ledger_type_reference=returned.supplier.name if returned.supplier else ''
            )
        return journal

    def calculate_price_including(self, vat_code, price):
        if vat_code:
            price = ((vat_code.percentage / 100) * float(price)) + float(price)
        return Decimal(price)

    def create_inventory_journal(self, profile, inventory_lines, movement, journal, content_type):

        for inventory, inventory_movements in inventory_lines.items():
            for inventory_movement in inventory_movements:
                value = inventory_movement.price_excluding * -1
                quantity = inventory_movement.quantity * -1

                InventoryLedger.objects.create(
                    inventory=inventory,
                    content_type=content_type,
                    object_id=movement.id,
                    quantity=quantity,
                    value=Decimal(value),
                    created_by=profile,
                    date=movement.date,
                    period=movement.period,
                    supplier=movement.supplier,
                    text=str(movement),
                    journal=journal,
                    rate=-1,
                    object_owner=str(movement.supplier),
                    object_reference=str(movement)
                    )

    def get_received_inventory_items(self):
        branch_inventories = BranchInventory.objects.prefetch_related(
            'inventory_returned', 'inventory_received', 'inventory_received'
        ).filter(
            branch=self.branch
        )

        available_inventory_items = defaultdict(list)

        for branch_inventory in branch_inventories.all():
            total_returned = 0
            total_received = 0
            for _inventory_rec in branch_inventory.inventory_received.all():
                total_received += _inventory_rec.quantity
                available_quantity = _inventory_rec.quantity - _inventory_rec.quantity_returned
                if available_quantity > 0:
                    available_inventory_items[branch_inventory.id].append({'received': _inventory_rec,
                                                                           'in_hand': available_quantity
                                                                           })
        return available_inventory_items

    def update_inventory_quantities(self, inventory_availability):
        for inventory_id, availability in inventory_availability.items():
            quantity = availability['quantity_out']
            returned = availability['returned']
            received_inventory_items = availability['received']
            for received_inventory in received_inventory_items:
                received_in_hand = received_inventory.get('in_hand')
                received_obj = received_inventory.get('received')
                if quantity > 0:
                    if received_in_hand > quantity:

                        InventoryReceivedReturned.objects.create(
                            inventory_received=received_obj,
                            inventory_returned=returned,
                            quantity=quantity,
                            inventory=received_obj.inventory
                        )
                        quantity = 0
                    else:
                        quantity = quantity - received_in_hand

                        InventoryReceivedReturned.objects.create(
                            inventory_received=received_obj,
                            inventory_returned=returned,
                            quantity=received_in_hand,
                            inventory=received_obj.inventory
                        )
