from django.apps import AppConfig


class ReturnedConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.returned'
