import logging
import json
import pprint
from decimal import Decimal
from datetime import datetime
from collections import defaultdict

from django.urls import reverse
from django.views.generic import View, TemplateView, ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.db import transaction
from django.db.models import F, Sum, Q

from docuflow.apps.inventory.models import (BranchInventory, PurchaseOrderInventory,  Received, Returned,
                                            InventoryReceived, InventoryReturned)
from docuflow.apps.company.models import VatCode
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.common.mixins import ProfileMixin, ActiveYearRequiredMixin
from .forms import ReturnedForm

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class ReturnedView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Returned
    paginate_by = 30
    template_name = 'returned/index.html'
    context_object_name = 'returned_items'

    def get_queryset(self):
        return Returned.objects.listable().filter(
            branch=self.get_branch(), period__period_year=self.get_year()
        ).exclude(
            invoice__isnull=False,
        ).select_related(
            'invoice_account', 'period__period_year', 'invoice_account__invoice', 'invoice_account__invoice__invoice_type',
            'invoice_account__invoice__status', 'invoice_account__invoice__company', 'received_movement', 'supplier'
        ).prefetch_related(
            'inventory_returned'
        ).order_by('-created_at')

    def get_context_data(self, **kwargs):
        context = super(ReturnedView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        year = self.get_year()
        context['branch'] = branch
        context['year'] = year
        return context


class ReturnedDetailView(ProfileMixin, TemplateView):
    template_name = 'returned/detail.html'

    def get_transaction(self):
        return Returned.objects.prefetch_related(
            'inventory_returned'
        ).filter(
            pk=self.kwargs['pk']
        ).first()

    def get_context_data(self, **kwargs):
        context = super(ReturnedDetailView, self).get_context_data(**kwargs)
        returned = self.get_transaction()
        context['returned'] = returned
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        return context


class AddGeneralReturnedRowView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'returned/row.html'

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.prefetch_related(
            'inventory_returned', 'inventory_received'
        ).filter(branch=self.get_branch())

        available_inventory_items = []

        for b_in in branch_inventories.all():
            total_returned = 0
            total_received = 0
            for _inventory_ret in b_in.inventory_returned.all():
                total_returned += _inventory_ret.quantity

            for _inventory_rec in b_in.inventory_received.all():
                total_received += _inventory_rec.quantity

            in_hand = total_received - total_returned
            if in_hand > 0:
                available_inventory_items.append({'in_hand': in_hand, 'inventory': b_in})
        return available_inventory_items

    def get_vat_codes(self):
        return VatCode.objects.filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(AddGeneralReturnedRowView, self).get_context_data(**kwargs)
        context['inventory_items'] = self.get_inventory_items()
        context['vat_codes'] = self.get_vat_codes()
        context['row'] = self.request.GET.get('row_id', 0)
        return context


class ReturnReceiveItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'returned/received_items.html'

    def get_received_items(self):
        filter_options = {'received_id': self.kwargs['movement_id']}
        inventory_id = self.request.GET.get('inventory_id')
        if inventory_id:
            filter_options['inventory_id'] = inventory_id
        query = InventoryReceived.objects.prefetch_related(
            'returned_items'
        ).filter(**filter_options)

        inventory_items = {'total_ordered': 0, 'total_received': 0, 'total_short': 0, 'total_price_exl': 0,
                           'total_price_in': 0, 'total_total_price_exl': 0, 'total_total_price_inc': 0,
                           'inventory_items': [], 'total_in_hand': 0, 'total_vat': 0
                           }
        for received_inventory in query:
            if received_inventory.quantity:
                inventory_items['total_ordered'] += received_inventory.quantity
                inventory_items['total_received'] += received_inventory.quantity
            if received_inventory.short:
                inventory_items['total_short'] += received_inventory.short
            if received_inventory.price_excluding:
                inventory_items['total_price_exl'] += received_inventory.price_excluding
            if received_inventory.price_including:
                inventory_items['total_price_in'] += received_inventory.price_including
            total_returned = 0
            for returned in received_inventory.returned_items.all():
                total_returned += returned.quantity
            in_hand = received_inventory.quantity - total_returned

            inventory_items['total_total_price_exl'] += received_inventory.total_price_excluding
            inventory_items['total_total_price_inc'] += received_inventory.total_price_including
            inventory_items['total_in_hand'] += in_hand

            received = {'received': received_inventory, 'in_hand': in_hand}
            inventory_items['inventory_items'].append(received)
        return inventory_items

    def get_context_data(self, **kwargs):
        context = super(ReturnReceiveItemsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['received'] = self.get_received_items()
        return context


class CreateReturnedView(ActiveYearRequiredMixin, CreateView):
    template_name = 'returned/create.html'
    form_class = ReturnedForm
    model = Returned

    def get_initial(self):
        initial = super(CreateReturnedView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateReturnedView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateReturnedView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['branch'] = self.get_branch()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the goods returned, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateReturnedView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    form.save()

                return JsonResponse({'text': 'Goods returned saved successfully',
                                     'error': False,
                                     'redirect': reverse('inventory_returned_index')
                                     })
            except InventoryException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True,
                                     'alert': True
                                     })


class CreateGeneralReturnedView(LoginRequiredMixin, CreateReturnedView):
    template_name = 'returned/create_general.html'

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the goods returned, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateGeneralReturnedView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    available_received_inventory = self.get_received_inventory_items()
                    # pp.pprint(available_inventory_items)

                    company = self.get_company()
                    profile = self.get_profile()

                    returned = form.save(commit=False)
                    returned.branch = self.get_branch()
                    returned.movement_type = MovementType.GENERAL
                    returned.profile = self.get_profile()
                    returned.role_id = self.request.session['role']
                    returned.save()

                    if returned:
                        total_amount = Decimal('0')
                        total_vat = Decimal('0')
                        account_journal_lines = defaultdict(Decimal)
                        inventory_journal_lines = defaultdict(list)
                        inventory_availability = {}
                        for field, value in self.request.POST.items():
                            if field.startswith('inventory_item', 0, 14):
                                counter = field[15:]
                                inventory_id = self.request.POST.get('inventory_item_{}'.format(counter))
                                inventory = BranchInventory.objects.filter(id=inventory_id).first()

                                if inventory and inventory.id in available_received_inventory:
                                    quantity = self.request.POST.get('quantity_received_{}'.format(counter))
                                    price_excluding = self.request.POST.get('price_{}'.format(counter), 0)
                                    vat_code_id = self.request.POST.get('vat_code_{}'.format(counter), None)
                                    print("Quantity returned is {}".format(quantity))
                                    price_excluding = float(price_excluding)
                                    if price_excluding == 0 or price_excluding < 0:
                                        raise InvalidPrice("Price cannot be zero, do you want to use default price")

                                    received = Decimal(quantity)

                                    price_excluding = Decimal(price_excluding)
                                    price_including = Decimal(price_excluding)

                                    vat_code = None
                                    if vat_code_id:
                                        vat_code = VatCode.objects.filter(id=vat_code_id).first()
                                    if vat_code:
                                        price_including = self.calculate_price_including(vat_code, price_including)

                                    total_price_excluding = price_excluding * received
                                    total_amount += Decimal(price_including) * received
                                    total_vat += Decimal(price_including) * price_excluding
                                    print("Save -->< Quantity returned is {}".format(quantity))
                                    inventory_returned = InventoryReturned.objects.create(
                                        returned=returned,
                                        inventory=inventory,
                                        price_excluding=price_excluding,
                                        price_including=price_including,
                                        quantity=quantity,
                                        vat=vat_code
                                    )
                                    available_received = available_received_inventory.get(inventory.id)
                                    inventory_availability[inventory.id] = {'quantity_out': received,
                                                                            'returned': inventory_returned,
                                                                            'received': available_received
                                                                            }
                                    inventory_journal_lines[inventory].append(inventory_returned)
                                    account_journal_lines[inventory.gl_account] += total_price_excluding

                        returned.total_amount = total_amount
                        returned.total_vat = total_vat
                        returned.save()

                        self.create_journal_entries(company, profile, account_journal_lines, returned)
                        self.create_inventory_journal(profile, inventory_journal_lines, returned)

                        self.update_inventory_quantities(inventory_availability)
                return JsonResponse({'text': 'Goods returned saved successfully',
                                     'error': False,
                                     'redirect': reverse('inventory_returned_index')
                                     })
            except InventoryException as exception:
                return JsonResponse({'text': exception.__str__(),
                                     'details': exception.__str__(),
                                     'error': True,
                                     'alert': True
                                     })
            except Exception as exception:
                return JsonResponse({'text': 'Unexpected error occurred creating the goods returned',
                                     'details': exception.__str__(),
                                     'error': True
                                     })


class SearchPurchaseOrderItemView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        purchase_orders = []
        term = self.request.GET.get('term', None)
        if term:
            search_term = term
            company = self.get_company()
            company_str = company.name[0:3]
            po_key = "PO-{}".format(company_str.upper())
            key_len = len(po_key)
            if term.startswith(po_key, 0, key_len):
                search_term = int(str(search_term[key_len:]))
            elif term.startswith('PO', 0, 2):
                search_term = str(search_term[2:])

            print("Search for invoice is {} --> {}".format(search_term, term[0:2]))
            orders = PurchaseOrderInventory.objects.filter(invoice__invoice_id__icontains=search_term,
                                                           invoice__company=company,
                                                           invoice__invoice_type__code='purchase-order'
                                                           )
            items = {'results': []}
            for po_order in orders:
                po_items_url = reverse('receive_purchase_order_items', kwargs={'pk': po_order.id})
                purchase_orders.append({'text': po_order.invoice.__str__(), 'id': po_order.id,
                                        'invoice_id': po_order.invoice.id, 'po_items_url': po_items_url
                                        })

        return HttpResponse(json.dumps({'results': purchase_orders, 'total_count': orders.count()}))


class PurchaseOrderListView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        purchase_orders = []
        orders = Invoice.objects.filter(
            supplier_id=self.request.GET['supplier_id'],
            invoice_type__code='purchase-order',
            status__slug__in=['po-ordered'],
            branch=self.get_branch()
        )
        for po_order in orders:
            items_url = reverse('receive_purchase_order_items', kwargs={'invoice_id': po_order.id})
            purchase_orders.append({'text': po_order.__str__(), 'id': po_order.id,
                                    'invoice_id': po_order.id, 'items_url': items_url
                                    })

        return HttpResponse(json.dumps(purchase_orders))


class SupplierInventoriesView(ProfileMixin, View):
    def get(self, request, *args, **kwargs):

        inventory_items = defaultdict(list)
        # any inventory items that can be returned
        inventories_received = InventoryReceived.objects.select_related(
            'inventory').filter(inventory__branch=self.get_branch())
        for inventory_received in inventories_received:
            branch_inventory = inventory_received.inventory
            if branch_inventory.available_stock > 0:
                items_url = reverse('supplier_inventory_received_items')
                if branch_inventory.id not in inventory_items:
                    inventory_items[branch_inventory.id] = {'text': branch_inventory.full_name,
                                                            'id': branch_inventory.id,
                                                            'inventory_id': branch_inventory.id,
                                                            'received_items_url': items_url
                                                            }
        return HttpResponse(json.dumps(inventory_items))


class ListGoodsReceivedView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        received_items = []
        supplier_id = self.request.GET.get('supplier_id', None)
        if supplier_id:
            received_transactions = Received.objects.returnable().filter(supplier_id=supplier_id)
            logger.info("Total received for this supplier --> {}".format(received_transactions.count()))
            for received in received_transactions:
                logger.info("Inventory received -> {}".format(received))
                items_url = reverse('return_received_items', kwargs={'movement_id': received.id})
                received_items.append({'text': received.__str__(),
                                       'id': received.id,  'items_url': items_url
                                       })

        return HttpResponse(json.dumps(received_items))


class SupplierInventoriesReceivedView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        received_items = []
        supplier_id = self.request.GET.get('supplier_id', None)
        inventory_id = self.request.GET.get('inventory_id', None)
        q_objects = Q()
        if supplier_id:
            q_objects.add(Q(received__supplier_id=supplier_id) | Q(received__supplier_id__isnull=True), Q.AND)
            if inventory_id:
                q_objects.add(Q(inventory_id=inventory_id), Q.AND)

            q_objects.add(Q(received__movement_type__in=[MovementType.GENERAL, MovementType.PURCHASE_ORDER]), Q.AND)
            inventory_received = InventoryReceived.objects.prefetch_related(
                'returned_items',
            ).filter(
                q_objects
            )
            logger.info(inventory_received.query)
            for received_item in inventory_received:
                total_received = received_item.quantity
                total_return = 0
                for returned_received_item in received_item.returned_items.all():
                    total_return += returned_received_item.quantity
                quantity = total_received - total_return
                if quantity > 0:
                    items_url = reverse('return_received_items', kwargs={'movement_id': received_item.received.id})
                    received_items.append({'text': received_item.received.__str__(), 'id': received_item.received.id,
                                           'items_url': items_url
                                           })
            # goods_received = Received.objects.filter(**filter_options)
            # for received_item in goods_received:
            #     items_url = reverse('return_received_items', kwargs={'movement_id': received_item.id})
            #     reveived_items.append({'text': received_item.__str__(), 'id': received_item.id,
            #                            'items_url': items_url
            #                            })

        return HttpResponse(json.dumps(received_items))


class ReturnedInventoryItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'returned/inventory_items.html'

    def get_inventory_items(self, returned_id):
        filter_options = {}

        if returned_id:
            filter_options['returned_id'] = returned_id

        inventory_items = InventoryReturned.objects.prefetch_related(
            'return_received_items'
        ).annotate(
            quantity_returned=Sum('return_received_items__quantity'),
            available_quantity=F('quantity') - Sum('return_received_items__quantity'),
            total_returned=Sum(F('return_received_items__quantity') * F('price_excluding'))
        ).filter(
            **filter_options
        )
        data = {'total_price_excluding': 0, 'price_incl': 0, 'total_price': 0, 'inventory_items': []}
        for inventory_item in inventory_items:
            if inventory_item.quantity > 0:
                logger.info("For inventory --{}, quantity returned is {} @ {} when we returned {}, avai => {}".format(
                    inventory_item.inventory, inventory_item.quantity_returned, inventory_item.total_returned,
                    inventory_item.quantity, inventory_item.available_quantity)
                )
                row = {'inventory': inventory_item.inventory, 'quantity': 0, 'price_excluding': inventory_item.price_excluding,
                       'price_including': inventory_item.price_including, 'total_price': 0, 'total_including': 0,
                       'total_excluding': 0, 'vat_code': inventory_item.vat}
                total_price_excluding = inventory_item.quantity * inventory_item.price_excluding
                total_price_including = inventory_item.quantity * inventory_item.price_including
                data['total_price'] += total_price_including * -1
                data['total_price_excluding'] += total_price_excluding
                row['total_price_excluding'] = total_price_excluding * -1
                row['total_price_including'] = total_price_including * -1
                row['quantity'] = inventory_item.quantity
                data['inventory_items'].append(row)
        logger.info(data)
        return data

    def get_context_data(self, **kwargs):
        context = super(ReturnedInventoryItemsView, self).get_context_data(**kwargs)
        inventory_items = self.get_inventory_items(self.kwargs['returned_id'])
        context['inventory_returned'] = inventory_items
        return context
