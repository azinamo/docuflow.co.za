from django.urls import path

from . import views

urlpatterns = [
    path('', views.InventoryLedgerView.as_view(), name='inventory_ledger'),
    path('<int:inventory_id>/breakdown/', views.InventoryBreakdownView.as_view(), name='inventory_ledger_breakdown'),
    path('<int:pk>/detail/', views.InventoryLedgerDetailView.as_view(),
         name='inventory_ledger_inventory_line_detail'),
    path('generate/', views.GenerateLedgerView.as_view(), name='generate_inventory_ledger'),
    path('downoload/', views.DownloadLedgerView.as_view(), name='download_inventory_ledger'),

]
