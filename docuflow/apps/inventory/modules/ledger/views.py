import logging
import pprint
from collections import OrderedDict
from decimal import Decimal

import xlwt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponse
from django.views.generic import FormView, TemplateView, View

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.inventory.models import BranchInventory, Ledger, InventoryReceived, InventoryReceivedReturned
from docuflow.apps.inventory.services import is_valuation_balancing
from .forms import LedgerSearchForm

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class InventoryLedgerView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'ledger/index.html'
    form_class = LedgerSearchForm

    def get_inventories(self):
        return BranchInventory.objects.select_related('inventory').filter(branch=self.get_branch())

    def get_initial(self):
        initial = super(InventoryLedgerView, self).get_initial()
        year = self.get_year()
        initial['from_date'] = year.start_date
        initial['to_date'] = year.end_date
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(InventoryLedgerView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        year = self.get_year()
        context['year'] = year
        context['branch'] = branch
        context['is_balancing'] = is_valuation_balancing(branch=branch, year=year)
        return context


class GenerateLedgerView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'ledger/ledger.html'

    def get_context_data(self, **kwargs):
        context = super(GenerateLedgerView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        year = self.get_year()

        form = LedgerSearchForm(branch=branch, year=year, data=self.request.GET)
        if form.is_valid():
            context['branch'] = branch
            context['year'] = year
            context['inventories'] = form.execute()
        else:
            context['error'] = form.errors
        return context


class DownloadLedgerView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, **kwargs):
        branch = self.get_branch()
        year = self.get_year()
        form = LedgerSearchForm(branch=branch, year=year, data=self.request.GET)
        if form.is_valid():
            report = form.execute()

            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = f'attachment; filename="inventory_ledger_{str(self.get_company().slug)}.xls"'

            font_style = xlwt.XFStyle()
            font_style.font.bold = True

            wb = xlwt.Workbook(encoding='utf-8')
            ws = wb.add_sheet('Inventory Ledger')

            row_count = 1
            row = ws.row(0)
            row.write(0, str(self.get_company()), font_style)

            row_count += 1
            ws.write(row_count, 0, 'Inventory Ledger Report', font_style)

            row_count += 1
            ws.write(row_count, 0, 'Period dates(s)', font_style)
            ws.write(row_count, 1, report.from_date.strftime('%Y-%m-%d'))
            ws.write(row_count, 2, report.to_date.strftime('%Y-%m-%d'))

            row_count += 2
            ws.write(row_count, 0, 'SKU', font_style)
            ws.write(row_count, 1, 'Name', font_style)
            ws.write(row_count, 2, '', font_style)
            ws.write(row_count, 3, '', font_style)
            ws.write(row_count, 4, '', font_style)
            ws.write(row_count, 5, '', font_style)
            ws.write(row_count, 6, '', font_style)
            ws.write(row_count, 7, '', font_style)
            ws.write(row_count, 8, '', font_style)
            ws.write(row_count, 9, '', font_style)

            row_count += 1
            ws.write(row_count, 0, 'Period', font_style)
            ws.write(row_count, 1, 'Date', font_style)
            ws.write(row_count, 2, 'Module', font_style)
            ws.write(row_count, 3, 'Journal Number', font_style)
            ws.write(row_count, 4, 'Journal Text', font_style)
            ws.write(row_count, 5, 'PO/Invoice Delivery', font_style)
            ws.write(row_count, 6, 'Customer/Supplier', font_style)
            ws.write(row_count, 7, 'Quantity', font_style)
            ws.write(row_count, 8, 'Value', font_style)
            ws.write(row_count, 9, 'Balance', font_style)

            for inventory, ledgers in report.ledger.items():
                row_count += 1
                ws.write(row_count, 0, str(inventory))
                ws.write(row_count, 1, inventory.description)

                row_count += 1
                ws.write(row_count, 0, '')
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                ws.write(row_count, 5, '')
                ws.write(row_count, 6, 'Incoming Balance', font_style)
                ws.write(row_count, 7, ledgers.get('initial_quantity', 0))
                ws.write(row_count, 8, ledgers.get('unit_price', 0))
                ws.write(row_count, 9, ledgers.get('initial_value', 0), font_style)

                for line_key, ledger_line in ledgers['lines'].items():
                    row_count += 1
                    ws.write(row_count, 0, ledger_line['line'].period.period)
                    ws.write(row_count, 1, ledger_line['line'].date.strftime('%d-%M-%Y'))
                    ws.write(row_count, 2, ledger_line['line'].module_name)
                    ws.write(row_count, 3, str(ledger_line['line'].journal))
                    ws.write(row_count, 4, ledger_line['line'].text)
                    ws.write(row_count, 5, ledger_line['line'].object_reference)
                    ws.write(row_count, 6, ledger_line['line'].object_owner)
                    ws.write(row_count, 7, ledger_line['quantity'])
                    if ledger_line['line'].is_completed:
                        ws.write(row_count, 8, '')
                        ws.write(row_count, 9, '')
                    else:
                        ws.write(row_count, 8, ledger_line['value'])
                        ws.write(row_count, 9, ledger_line['balance'])

                row_count += 1
                ws.write(row_count, 0, '')
                ws.write(row_count, 1, '')

                row_count += 1
                ws.write(row_count, 0, '')
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                ws.write(row_count, 5, '')
                ws.write(row_count, 6, '')
                ws.write(row_count, 7, ledgers['qty'], font_style)
                ws.write(row_count, 8, ledgers['avg'], font_style)
                ws.write(row_count, 9, ledgers['value_total'], font_style)

            wb.save(response)
            return response
        else:
            return HttpResponse('Report could not be generated')


class InventoryBreakdownView(ProfileMixin, TemplateView):
    template_name = 'ledger/breakdown.html'

    def get_inventory_item(self):
        return BranchInventory.objects.get(pk=self.kwargs['inventory_id'])

    def get_returned_movement_breakdown(self, inventory):
        returned_items = InventoryReceivedReturned.objects.select_related(
            'inventory_returned', 'inventory_returned__returned'
        ).filter(inventory_received__isnull=True, inventory_returned__inventory=inventory)
        movements = OrderedDict()
        for returned_item in returned_items:
            movement_dict = {'total_received': 0, 'total_return': 0, 'total_price': Decimal(0), 'quantity': 0}
            breakdown = {}
            returned_qty = int(returned_item.quantity)
            total_received = returned_item.quantity
            total_return = 0

            for r in range(0, returned_qty):
                breakdown[r+1] = {'price': returned_item.price, 'returned_id': returned_item.id,
                                  'returned': returned_item.inventory_returned.returned
                                  }

            quantity = total_received - total_return
            total_price = 0
            if returned_item.price:
                total_price = returned_item.price * quantity

            movement_dict['quantity'] = quantity
            movement_dict['total_returned'] = total_received
            movement_dict['total_return'] = total_return
            movement_dict['total_price'] = total_price * -1
            movement_dict['breakdown'] = breakdown
            movements[returned_item] = movement_dict
        return movements

    def get_received_returned_inventory_movement_breakdown(self, inventory):
        received_items = InventoryReceived.objects.prefetch_related('returned_items').filter(inventory=inventory)
        movements = OrderedDict()
        for received_item in received_items:
            movement_dict = {'total_received': 0, 'total_return': 0, 'total_price': Decimal(0), 'quantity': 0}
            total_received = received_item.quantity
            total_return = 0

            breakdown = {}
            received_qty = int(received_item.quantity)
            for r in range(0, received_qty):
                breakdown[r+1] = {'received_id': received_item.id, 'returned': None}

            c = 1
            for returned_received_item in received_item.returned_items.all():
                returned_qty = int(returned_received_item.quantity)

                total_return += returned_received_item.quantity
                for rt in range(0, returned_qty):
                    if c in breakdown:
                        breakdown[c]['returned'] = returned_received_item.inventory_returned
                        c += 1
            quantity = total_received - total_return
            total_price = 0
            if received_item.price_excluding:
                total_price = received_item.price_excluding * quantity
            movement_dict['quantity'] = quantity
            movement_dict['total_received'] = total_received
            movement_dict['total_return'] = total_return
            movement_dict['total_price'] = total_price
            movement_dict['breakdown'] = breakdown
            movements[received_item] = movement_dict
        return movements

    def get_context_data(self, **kwargs):
        context = super(InventoryBreakdownView, self).get_context_data(**kwargs)
        inventory = self.get_inventory_item()
        context['inventory'] = inventory
        context['received_movements'] = self.get_received_returned_inventory_movement_breakdown(inventory)
        context['returned_movements'] = self.get_returned_movement_breakdown(inventory)
        return context


class InventoryLedgerDetailView(ProfileMixin, TemplateView):
    template_name = 'ledger/detail.html'

    def get_ledger(self):
        return Ledger.objects.get(pk=self.kwargs['pk'])

    def get_ledger_breakdown(self, ledger):
        quantity = abs(int(ledger.quantity))
        lines = []
        for i in range(1, quantity):
            lines.append(ledger)
        return lines

    def get_context_data(self, **kwargs):
        context = super(InventoryLedgerDetailView, self).get_context_data(**kwargs)
        ledger = self.get_ledger()
        context['ledger'] = ledger
        context['ledger_lines'] = self.get_ledger_breakdown(ledger)
        return context
