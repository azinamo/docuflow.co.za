from django import forms

from docuflow.apps.inventory.models import BranchInventory
from .services import GenerateLedger


class InventoryModelChoiceField(forms.ModelChoiceField):

    def label_from_instance(self, obj):
        return f"{obj.full_name}"


class LedgerSearchForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.year = kwargs.pop('year')
        super(LedgerSearchForm, self).__init__(*args, **kwargs)
        inventory_qs = BranchInventory.objects.select_related('inventory').filter(branch=self.branch)
        self.fields['from_inventory'].queryset = inventory_qs.order_by('inventory__code')
        self.fields['from_inventory'].empty_label = None
        self.fields['to_inventory'].queryset = inventory_qs.order_by('-inventory__code')
        self.fields['to_inventory'].empty_label = None

    from_date = forms.DateField(required=True, label='From Date')
    to_date = forms.DateField(required=True, label='To Date')
    from_inventory = InventoryModelChoiceField(required=False, label='From Inventory', queryset=None)
    to_inventory = InventoryModelChoiceField(required=False, label='To Inventory', queryset=None)
    is_potrait = forms.BooleanField(required=False, label='Potrait')
    is_landscape = forms.BooleanField(required=False, label='Landscape')

    def execute(self):
        ledger = GenerateLedger(
            branch=self.branch,
            year=self.year,
            from_date=self.cleaned_data['from_date'],
            to_date=self.cleaned_data['to_date'],
            from_inventory=self.cleaned_data['from_inventory'],
            to_inventory=self.cleaned_data['to_inventory']
        )

        return ledger.execute()
