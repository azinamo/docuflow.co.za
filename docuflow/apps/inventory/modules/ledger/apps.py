from django.apps import AppConfig


class LedgerConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.ledger'
