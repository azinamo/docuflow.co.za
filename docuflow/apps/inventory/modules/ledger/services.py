import logging
import pprint
from decimal import Decimal

from django.db.models import Prefetch, Subquery, Case, When, DecimalField, OuterRef, F
from django.db.models.functions import Coalesce

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import Ledger, InventoryReceived, BranchInventory, Balance

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class CreateInventoryLedger(object):

    def __init__(self, inventory, quantity, unit_price, date, period, profile, is_return=False, text='', supplier=None,
                 content_type=None, object_id=None, status=Ledger.COMPLETE, is_new=False, module=Module.INVENTORY,
                 value=None):
        logger.info(f"CREATE LEDGER FOR {inventory} and {quantity} at {unit_price}")
        self.inventory = inventory
        self.quantity = quantity
        self.unit_price = unit_price
        self.period = period
        self.date = date
        self.profile = profile
        self.supplier = supplier
        self.text = text
        self.content_type = content_type
        self.object_id = object_id
        self.is_return = is_return
        self.status = status
        self.is_new = is_new
        self.module = module

    def validate_quantity(self):
        quantity = self.quantity
        if quantity == 0:
            raise InventoryException(f'Inventory {quantity} quantity is not valid')

    def get_quantity(self):
        if self.is_return:
            return self.quantity * -1
        return self.quantity

    def get_price(self):
        if self.is_return:
            return self.unit_price * -1
        return self.unit_price

    def get_current_ledgers(self):
        if self.is_new:
            return False
        filters = {'inventory': self.inventory}
        if self.content_type:
            filters['content_type'] = self.content_type
            filters['object_id'] = self.object_id
        if self.supplier:
            filters['supplier'] = self.supplier
        return Ledger.objects.filter(**filters).first()

    def execute(self):
        logger.info('Execute --> create INVENTORY ledger')
        self.validate_quantity()
        if self.quantity != 0:
            logging.info(f"All valid, now save the ledger  for quantity {self.quantity}")

            ledger = self.get_current_ledgers()
            if not ledger:
                logger.info(f"New ledger line for inventory {self.inventory}, supplier =  {self.supplier}, "
                            f"quantity = {self.get_quantity()}, price = {self.get_price()}, profile = {self.profile},"
                            f" period = {self.period}, date = {self.date}, text={self.text}, status={self.status}")
                ledger = Ledger.objects.create(
                    inventory=self.inventory,
                    quantity=self.get_quantity(),
                    value=self.get_price(),
                    created_by=self.profile,
                    period=self.period,
                    date=self.date,
                    supplier=self.supplier,
                    text=self.text,
                    module=self.module,
                    status=self.status,
                    rate=-1 if self.is_return else 1
                )
                logger.info("Add content type with {}".format(self.content_type))
                if self.content_type:
                    ledger.content_type = self.content_type
                    ledger.object_id = self.object_id
                    ledger.save()
                logger.info("DOne creating new ledger")
            else:
                logger.info("*********************** Update ledger line*******************")
                ledger.status = self.status
                ledger.rate = -1 if self.is_return else 1
                ledger.quantity = self.get_quantity()
                ledger.value = self.get_price()
                ledger.date = self.date
                ledger.supplier = self.supplier
                ledger.text = self.text
                ledger.created_by = self.profile
                ledger.status = self.status
                ledger.save()
                logger.info("Ledger updated successfully")
            return ledger


class GenerateLedger:

    def __init__(self, branch, year, from_date, to_date, from_inventory, to_inventory):
        self.branch = branch
        self.year = year
        self.from_date = from_date
        self.to_date = to_date
        self.from_inventory = from_inventory
        self.to_inventory = to_inventory
        self.ledger = {}

    def get_inventory_filter(self):
        filters = {}
        if (self.from_inventory and self.to_inventory) and (self.from_inventory == self.to_inventory):
            filters['inventory__code'] = self.to_inventory.inventory.code
        else:
            if self.from_inventory:
                filters['inventory__code__gte'] = self.from_inventory.inventory.code
            if self.to_inventory:
                filters['inventory__code__lte'] = self.to_inventory.inventory.code
        return filters

    def add_opening_balances(self):
        filters = {'inventory__is_blocked': False, **self.get_inventory_filter()}

        opening_balances = InventoryReceived.objects.opening_balances(self.branch, self.year, **filters)
        logger.info(f"Opening balances --> {opening_balances.count()} for year {self.year}")
        for opening_balance in opening_balances:
            # inventory_totals = opening_balance.inventory.calculate_average_price
            total_price = 0
            unit_price = 0

            history = getattr(opening_balance.inventory, 'history', None)
            if opening_balance.price_excluding:
                unit_price = opening_balance.price_excluding
            if unit_price and opening_balance.quantity:
                total_price = unit_price * opening_balance.quantity
            if opening_balance.inventory in self.ledger:
                self.ledger[opening_balance.inventory]['initial_quantity'] += opening_balance.quantity
                self.ledger[opening_balance.inventory]['initial_balance'] += opening_balance.quantity
                self.ledger[opening_balance.inventory]['total_quantity'] += opening_balance.quantity
            else:
                self.ledger[opening_balance.inventory] = {'initial_balance': opening_balance.quantity,
                                                          'initial_quantity': opening_balance.quantity,
                                                          'unit_price': unit_price,
                                                          'value': total_price,
                                                          'initial_value': total_price,
                                                          'balance': total_price,
                                                          'total_quantity': opening_balance.quantity,
                                                          'total_value': total_price,
                                                          'lines': {},
                                                          'closing_value': opening_balance.inventory_value,
                                                          'closing_average_price': opening_balance.average_price,
                                                          'closing_in_stock': opening_balance.in_stock,
                                                          # 'value_total': history.total_value if history else 0,
                                                          # 'avg': history.average_price if history else 0,
                                                          # 'qty': history.in_stock if history else 0,
                                                          'object_key': 0
                                                          }

    def prepare_ledger_lines(self, ledgers):
        ledger_lines = {}
        for ledger in ledgers:
            ledger_line_key = (ledger.inventory, ledger.content_type_id, ledger.object_id)
            if ledger_line_key in ledger_lines:
                ledger_lines[ledger_line_key]['total'] += ledger.line_total
                ledger_lines[ledger_line_key]['quantity'] += ledger.quantity
            else:
                ledger_lines[ledger_line_key] = {'total': ledger.line_total, 'ledger': ledger,
                                                 'quantity': ledger.quantity}
        return ledger_lines

    def get_ledgers(self):
        filters = {'inventory__branch': self.branch, 'period__period_year': self.year,
                   **self.get_inventory_filter()}
        if self.to_date:
            filters['date__lte'] = self.to_date
        return Ledger.objects.inventory_ledger(branch=self.branch, year=self.year, **filters)

    def execute(self):
        return BranchInventory.objects.annotate(
            opening_value=Coalesce(
                Subquery(
                    Balance.objects.filter(inventory_id=OuterRef('id'), year=self.year).values('opening_value')[:1]
                ), Decimal('0')),
            opening_price=Coalesce(
                Subquery(
                    Balance.objects.filter(inventory_id=OuterRef('id'), year=self.year).values('opening_price')[:1]
                ), Decimal('0')),
            opening_quantity=Coalesce(
                Subquery(
                    Balance.objects.filter(inventory_id=OuterRef('id'), year=self.year).values('opening_quantity')[:1]
                ), 0),
            closing_value=Coalesce(
                Subquery(
                    Balance.objects.filter(inventory_id=OuterRef('id'), year=self.year).values('closing_value')[:1]
                ), Decimal('0')),
            closing_price=Coalesce(
                Subquery(
                    Balance.objects.filter(inventory_id=OuterRef('id'), year=self.year).values('closing_price')[:1]
                ), Decimal('0')),
            closing_quantity=Coalesce(
                Subquery(
                    Balance.objects.filter(inventory_id=OuterRef('id'), year=self.year).values('closing_quantity')[:1]
                ), Decimal('0')),
            # opening_quantity=Coalesce(
            #     Subquery(
            #         InventoryReceived.objects.filter(
            #             received__period__period_year=self.year, received__movement_type=MovementType.OPENING_BALANCE,
            #             inventory_id=OuterRef('id')
            #         ).values('quantity')[:1]
            #     ), 0),
            # opening_price=Coalesce(
            #     Subquery(
            #         InventoryReceived.objects.filter(
            #             received__period__period_year=self.year, received__movement_type=MovementType.OPENING_BALANCE,
            #             inventory_id=OuterRef('id')
            #         ).values('price_excluding')[:1]
            #     ), 0),
            # opening_value=Coalesce(F('opening_price'), 0) * Coalesce(F('opening_quantity'), 0)
        ).select_related('inventory').prefetch_related(
            Prefetch(
                'ledgers', Ledger.objects.annotate(
                    line_total=Case(
                        When(total_value__isnull=False, then=F('total_value') * F('rate')),
                        When(total_value__isnull=True, then=F('value') * F('quantity') * F('rate')),
                        output_field=DecimalField()
                    ),
                ).select_related(
                    'supplier', 'period', 'inventory', 'journal', 'content_type'
                ).filter(period__period_year=self.year).order_by('date', 'created_at', 'text')
            )
        ).filter(branch=self.branch).filter(**self.get_inventory_filter()).order_by('inventory__code')

        # for ledger_line in ledger_lines:
        #
            # ledger = ledger_line.get('ledger')
            # amount = ledger_line.get('total')
            # quantity = ledger_line.get('quantity')
            # ledger_data = {'line': ledger, 'value': 0, 'balance': 0, 'quantity': 0, 'is_pending': False}
            # if ledger.inventory in self.ledger:
            #     balance = amount + self.ledger[ledger.inventory]['balance']
            #     ledger_data['value'] = amount
            #     ledger_data['balance'] = balance
            #     ledger_data['quantity'] = quantity
            #     self.ledger[ledger.inventory]['lines'][ledger] = ledger_data
            #     self.ledger[ledger.inventory]['total_quantity'] += ledger.quantity
            #     self.ledger[ledger.inventory]['total_value'] += ledger.value
            #     self.ledger[ledger.inventory]['balance'] = balance
            # else:
            #     ledger_data['balance'] = amount
            #     ledger_data['quantity'] = quantity
            #     ledger_data['value'] = amount
            #     # history = getattr(ledger.inventory, 'history', None)
            #     self.ledger[ledger.inventory] = {
            #         'initial_balance': ledger.opening_quantity,
            #         'opening_quantity': ledger.opening_quantity,
            #         'opening_price': ledger.opening_price,
            #         'value': ledger.opening_value,
            #         'opening_value': ledger.opening_value,
            #         'lines': {ledger: ledger_data},
            #         'total_quantity': ledger.quantity,
            #         'total_value': amount,
            #         'balance': amount,
            #         'pending': [],
            #         'closing_value': ledger.closing_value,
            #         'closing_price': ledger.closing_price,
            #         'closing_quantity': ledger.closing_quantity
            #     }


class InventoryLedger(object):

    def __init__(self, year, branch):
        self.year = year
        self.branch = branch

    def get_opening_balances(self):
        opening_balances = InventoryReceived.objects.filter(
            received__period__period_year=self.year,
            received__branch=self.branch,
            received__movement_type=MovementType.OPENING_BALANCE
        )
        inventory_ledger = {}
        for opening_balance in opening_balances:
            total_price = 0
            unit_price = 0
            if opening_balance.price_excluding:
                unit_price = opening_balance.price_excluding
            if unit_price and opening_balance.quantity:
                total_price = unit_price * opening_balance.quantity
            inventory_ledger[opening_balance.inventory] = {'initial_balance': opening_balance.quantity,
                                                           'unit_price': unit_price,
                                                           'value': total_price,
                                                           'initial_value': total_price,
                                                           'balance': total_price,
                                                           'total_quantity': opening_balance.quantity,
                                                           'total_value': total_price,
                                                           'lines': []
                                                           }
        return inventory_ledger

    def get_ledgers(self):
        ledgers = Ledger.objects.select_related(
            'supplier', 'period', 'inventory'
        ).filter(inventory__branch=self.branch).order_by('date', 'created_at')
        return ledgers

    def generate(self):
        inventory_ledgers = self.get_opening_balances()
        ledgers = self.get_ledgers()

        for ledger in ledgers:
            ledger_data = {'line': ledger, 'value': 0, 'balance': 0, 'quantity_balance': 0}

            if ledger.inventory in inventory_ledgers:
                balance = inventory_ledgers[ledger.inventory]['balance']
                quantity = inventory_ledgers[ledger.inventory]['total_quantity']

                balance = ledger.total + balance
                total_quantity = ledger.quantity + quantity

                ledger_data['value'] = ledger.total
                ledger_data['balance'] = balance

                inventory_ledgers[ledger.inventory]['lines'].append(ledger_data)
                inventory_ledgers[ledger.inventory]['total_quantity'] += ledger.quantity
                inventory_ledgers[ledger.inventory]['total_value'] += ledger.value
                inventory_ledgers[ledger.inventory]['balance'] = balance
            else:
                ledger_data['balance'] = ledger.total
                ledger_data['quantity_balance'] = ledger.quantity
                ledger_data['value'] = ledger.total
                inventory_ledgers[ledger.inventory] = {'lines': [ledger_data],
                                                       'total_quantity': ledger.quantity,
                                                       'total_value': ledger.value,
                                                       'balance': ledger.total
                                                       }
        return inventory_ledgers
