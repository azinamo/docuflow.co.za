from django.apps import AppConfig


class GrouplevelConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.grouplevel'
