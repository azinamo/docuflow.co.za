from django.urls import path

from . import views

urlpatterns = [
    path('', views.GroupLevelView.as_view(), name='group_level_index'),
    path('create/', views.CreateGroupLevelView.as_view(), name='create_group_level'),
    path('<int:pk>/edit/', views.EditGroupLevelView.as_view(), name='edit_group_level'),
    path('<int:pk>/delete/', views.DeleteGroupLevelView.as_view(), name='delete_group_level'),
    path('<int:group_level_id>/items/', views.GroupLevelItemView.as_view(), name='group_level_item_index'),
    path('<int:group_level_id>/items/create/', views.CreateGroupLevelItemView.as_view(),
         name='create_group_level_item'),
    path('<int:group_level_id>/item/<int:pk>/edit/', views.EditGroupLevelItemView.as_view(),
         name='edit_group_level_item'),
    path('<int:group_level_id>/item/<int:pk>/delete/', views.DeleteGroupLevelItemView.as_view(),
         name='delete_group_level_item'),
    path('children/', views.GroupLevelChildrenView.as_view(), name='group_level_children'),
]

