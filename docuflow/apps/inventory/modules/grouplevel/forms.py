import logging
from django import forms

from docuflow.apps.inventory.models import GroupLevel, GroupLevelItem


logger = logging.getLogger(__name__)


class GroupLevelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(GroupLevelForm, self).__init__(*args, **kwargs)
        self.fields['parent'].queryset = GroupLevel.objects.eligible_parents(company)

    class Meta:
        model = GroupLevel
        fields = ('name', 'parent')


class GroupLevelItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        group_level = kwargs.pop('group_level')
        super(GroupLevelItemForm, self).__init__(*args, **kwargs)
        if group_level and group_level.parent:
            children_of_child = GroupLevelItem.objects.filter(group_level=group_level.parent)
            self.fields['parent'].queryset = children_of_child
        else:
            self.fields['parent'].queryset = self.fields['parent'].queryset.filter(group_level__company=group_level.company)

    class Meta:
        model = GroupLevelItem
        fields = ('name', 'parent')
