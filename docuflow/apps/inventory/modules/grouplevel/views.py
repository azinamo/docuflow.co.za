import logging

from django.utils.translation import gettext as __
from django.contrib import messages
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from annoying.functions import get_object_or_None
from django.http import HttpResponseRedirect, JsonResponse

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Company
from docuflow.apps.inventory.models import GroupLevel, GroupLevelItem
from .forms import GroupLevelItemForm, GroupLevelForm

logger = logging.getLogger(__name__)


# Create your views here.
class GroupLevelView(ProfileMixin, LoginRequiredMixin, ListView):
    model = GroupLevel
    template_name = 'grouplevel/index.html'
    context_object_name = 'group_levels'

    def get_template_names(self):
        if 'master_company_id' in self.kwargs:
            return 'grouplevel/master_index.html'
        return 'grouplevel/index.html'

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_queryset(self):
        return GroupLevel.objects.select_related(
            'parent'
        ).prefetch_related('items', 'items__parent').filter(company=self.get_company_id())

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_context_data(self, **kwargs):
        context = super(GroupLevelView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateGroupLevelView(ProfileMixin, LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = GroupLevel
    template_name = 'grouplevel/create.html'
    success_message = 'Group Level successfully created'

    def get_form_kwargs(self):
        kwargs = super(CreateGroupLevelView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_post_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('create_master_company_object', kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('create_group_level')

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_company_objects_index', kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('group_level_index')

    def get_form_class(self):
        return GroupLevelForm

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_context_data(self, **kwargs):
        context = super(CreateGroupLevelView, self).get_context_data(**kwargs)
        context['post_url'] = self.get_post_url()
        return context

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                group_level = form.save(commit=False)
                group_level.company = self.get_company()
                group_level.save()
                return JsonResponse({
                    'text': __('Group Level successfully updated'),
                    'error': False,
                    'reload': True
                })
            except Exception as exception:
                return JsonResponse({
                    'text': __('Unexpected error occurred'),
                    'error': True,
                    'details': exception.__str__()
                })
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditGroupLevelView(ProfileMixin, LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = GroupLevel
    template_name = 'grouplevel/edit.html'
    success_message = __('Group Level details successfully updated')
    form_class = GroupLevelForm
    success_url = reverse_lazy('group_level_index')

    def get_form_kwargs(self):
        kwargs = super(EditGroupLevelView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse_lazy('master_company_objects_index')
        return reverse_lazy('group_level_index')

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                group_level = form.save(commit=False)
                group_level.save()
                return JsonResponse({
                    'text': 'Group Level successfully updated',
                    'error': False,
                    'reload': True
                    })
            except Exception as exception:
                return JsonResponse({
                    'text': 'Unexpected error occurred',
                    'error': True,
                    'details': exception.__str__()
                })
        else:
            return HttpResponseRedirect(self.get_success_url())


class DeleteGroupLevelView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse_lazy('master_company_objects_index')
        return reverse_lazy('group_level_index')

    def get(self, *args, **kwargs):
        group_level = get_object_or_None(GroupLevel, pk=kwargs['pk'])
        group_level.delete()
        messages.success(self.request, 'Group level deleted successfully')
        return HttpResponseRedirect(self.get_success_url())


class GroupLevelItemView(LoginRequiredMixin, ListView):
    model = GroupLevelItem
    template_name = 'grouplevelitems/index.html'
    pagenate_by = 10
    context_object_name = 'group_level_items'

    def get_context_data(self, **kwargs):
        context = super(GroupLevelItemView, self).get_context_data(**kwargs)
        context['object'] = get_object_or_None(GroupLevel, pk=self.kwargs['group_level_id'])
        return context

    def get_queryset(self):
        return GroupLevelItem.objects.filter(company_object_id=self.kwargs['group_level_id'])


class CreateGroupLevelItemView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = GroupLevelItem
    template_name = 'grouplevelitems/create.html'
    success_message = 'Group Level Item successfully created'

    def get_group_level(self):
        return get_object_or_None(GroupLevel, pk=self.kwargs['group_level_id'])

    def get_form_kwargs(self):
        kwargs = super(CreateGroupLevelItemView, self).get_form_kwargs()
        kwargs['group_level'] = self.get_group_level()
        return kwargs

    def get_success_url(self):
        return reverse('group_level_item_index', kwargs={'group_level_id': self.kwargs['group_level_id']})

    def get_form_class(self):
        return GroupLevelItemForm

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_context_data(self, **kwargs):
        context = super(CreateGroupLevelItemView, self).get_context_data(**kwargs)
        context['group_level'] = self.get_group_level()
        return context

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                group_level_item = form.save(commit=False)
                group_level_item.company = self.get_company()
                group_level_item.group_level_id = self.kwargs['group_level_id']
                group_level_item.save()
                return JsonResponse({
                    'text': 'Group Level item successfully saved',
                    'error': False,
                    'reload': True
                })
            except Exception as exception:
                return JsonResponse({
                    'text': 'Unexpected error occurred',
                    'error': True,
                    'details': exception.__str__()
                })
        else:
            return HttpResponseRedirect(reverse('group_level_item_index', kwargs={'group_level_id':
                                                                                      self.kwargs['group_level_id']}))


class EditGroupLevelItemView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = GroupLevelItem
    template_name = 'grouplevelitems/edit.html'
    success_message = 'Group Level item details successfully updated'

    def get_group_level(self):
        return get_object_or_None(GroupLevel, pk=self.get_object().group_level_id)

    def get_form_kwargs(self):
        kwargs = super(EditGroupLevelItemView, self).get_form_kwargs()
        kwargs['group_level'] = self.get_group_level()
        return kwargs

    def get_form_class(self):
        return GroupLevelItemForm

    def get_success_url(self):
        return reverse('group_level_item_index', kwargs={'group_level_id': self.kwargs['group_level_id']})

    def get_context_data(self, **kwargs):
        context = super(EditGroupLevelItemView, self).get_context_data(**kwargs)
        context['group_level'] = get_object_or_None(GroupLevel, pk=self.kwargs['group_level_id'])
        context['group_level_item'] = get_object_or_None(GroupLevelItem, pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                group_level_item = form.save(commit=False)
                group_level_item.save()
                return JsonResponse({
                    'text': 'Group Level item successfully updated',
                    'error': False,
                    'reload': True
                })
            except Exception as exception:
                return JsonResponse({
                    'text': 'Unexpected error occurred',
                    'error': True,
                    'details': exception.__str__()
                })
        else:
            return HttpResponseRedirect(reverse('group_level_item_index', kwargs={
                'group_level_id': self.kwargs['group_level_id']}))


class DeleteGroupLevelItemView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get(self):
        group_level_item = get_object_or_None(GroupLevelItem, pk=self.kwargs['pk'])
        # group_level_item.delete()
        return HttpResponseRedirect(reverse_lazy('group_level_index'))


class GroupLevelChildrenView(LoginRequiredMixin, View):

    def get(self, request, **kwargs):
        group_level_item = GroupLevelItem.objects.get(pk=int(self.request.GET.get('parent_id')))
        group_level_items = GroupLevelItem.objects.filter(parent=group_level_item).values('id', 'name')
        print(list(group_level_items))
        return JsonResponse(list(group_level_items), safe=False)
