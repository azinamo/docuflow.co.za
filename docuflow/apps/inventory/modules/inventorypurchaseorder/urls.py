from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.PurchaseOrderView.as_view(), name='inventory_purchase_order_index'),
    path('request/', views.CreatePurchaseOrderView.as_view(), name='create_inventory_purchase_order'),
    path('add/row', views.AddInventoryItemRowView.as_view(), name='add_inventory_purchase_order_row'),
    path('decline/<int:pk>/', views.DeclineView.as_view(), name='decline_inventory_item'),
    path('resubmit/<int:pk>/', views.ResubmitPurchaseOrderView.as_view(), name='resubmit_inventory_item'),
    path('lines/postings/<int:pk>/view/', views.PurchaseOrderLinesView.as_view(),
         name='inventory_purchase_order_lines_view'),
    path('edit/<int:pk>/line/', csrf_exempt(views.EditPurchaseOrderLineView.as_view()),
         name='edit_inventory_purchase_order_line'),
    path('create/<int:invoice_id>/line/', csrf_exempt(views.CreatePurchaseOrderLineView.as_view()),
         name='create_inventory_purchase_order_line'),
    path('show/<int:invoice_id>/', views.ShowPurchaseOrderView.as_view(), name='show_inventory_purchase_order_view'),
    path('list/', views.PurchaseOrderListView.as_view(), name='inventory_purchase_order_list'),
    path('show/<int:invoice_id>/', views.PurchaseOrderPrintView.as_view(),
         name='show_inventory_purchase_order_print_view'),
]
