from django import forms
from django.urls import reverse_lazy


from docuflow.apps.inventory.models import BranchInventory, PurchaseOrderInventory
from docuflow.apps.invoice.models import Invoice
from docuflow.apps.company.models import VatCode, Branch
from docuflow.apps.supplier.models import Supplier


class PurchaseOrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        branch = kwargs.pop('branch')
        super(PurchaseOrderForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].queryset = Supplier.objects.filter(company=company, is_active=True)
        self.fields['branch'].queryset = Branch.objects.filter(company=company,
                                                               id=branch.id,
                                                               is_active=True
                                                               )
        self.fields['branch'].empty_label = None

    memo_to_supplier = forms.CharField(required=False, label='Memo to supplier',
                                       widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))

    class Meta:
        fields = ('supplier', 'memo_to_supplier', 'branch', 'total_amount', 'vat_amount', 'net_amount', 'open_amount')
        model = Invoice

        widgets = {
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'open_amount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(PurchaseOrderForm, self).clean()

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')
        return cleaned_data


class DeclineForm(forms.Form):
    comment = forms.CharField(required=True, label='Do you want to add a reason why you are declining it',
                              widget=forms.Textarea(attrs={'cols': 5, 'rows': 10}))


class InventoryPurchaseOrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        branch = kwargs.pop('branch')
        super(InventoryPurchaseOrderForm, self).__init__(*args, **kwargs)
        self.fields['vat_code'].queryset = VatCode.objects.filter(company=company, is_active=True)
        self.fields['inventory'].queryset = BranchInventory.objects.filter(branch=branch)

    vat_code = forms.ModelChoiceField(required=True, label='Vat Code', queryset=None,
                                      widget=forms.Select(attrs={'class': 'chosen-select',
                                                                 'data-calculate-line-amount':
                                                                     reverse_lazy('calculate_purchase_order_amount')}))

    class Meta:
        fields = ('inventory', 'quantity', 'unit', 'order_value', 'vat_code', 'vat_amount')
        model = PurchaseOrderInventory

        # widgets = {
        #     'vat_amount': forms.HiddenInput(),
        # }


