from django.apps import AppConfig


class InventorypurchaseorderConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.inventorypurchaseorder'
