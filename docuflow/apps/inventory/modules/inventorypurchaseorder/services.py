import logging
from datetime import datetime

from django.db import transaction

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.purchaseorders.models import PurchaseOrderItem, PurchaseOrder
from docuflow.apps.purchaseorders.services import PurchaseOrderRequest, InvoicePurchaseOrder

logger = logging.getLogger(__name__)


class CreatePurchaseOrder:

    def __init__(self, company, profile, role, form, invoice_items):
        logger.info(" -------------- Create Purchase Order ------ ")
        self.company = company
        self.profile = profile
        self.role = role
        self.form = form
        self.invoice_items = invoice_items

    def get_invoice_type(self):
        invoice_type = InvoiceType.objects.purchase_order(self.company).first()
        if not invoice_type:
            raise InventoryException('Document type Purchase order does not exists.')
        return invoice_type

    def create(self):
        invoice = self.form.save(commit=False)
        invoice.company = self.company
        invoice.invoice_type = self.get_invoice_type()
        invoice.module = Module.INVENTORY
        if invoice.supplier and invoice.supplier.vat_number:
            invoice.vat_number = invoice.supplier.vat_number
        invoice.accounting_date = datetime.now()
        invoice.invoice_date = datetime.now()
        invoice.save()
        return invoice

    def create_purchase_order(self, invoice):
        data = {'invoice': invoice, 'memo_to_supplier': self.form.cleaned_data['memo_to_supplier']}
        purchase_order = PurchaseOrder.objects.create(**data)
        return purchase_order

    def has_invoice_items(self):
        if len(self.invoice_items) > 0:
            return True
        raise InventoryException("No inventory items found for this purchase order")

    def execute(self):
        logger.info("---execute---")
        with transaction.atomic():
            if self.has_invoice_items():
                invoice = self.create()
                if invoice:
                    purchase_order = self.create_purchase_order(invoice)
                    self.create_invoice_items(purchase_order)

                    # Create 3 flow levels in linked to this document
                    po_request = PurchaseOrderRequest(invoice, self.profile, self.role)
                    po_request.process_request()

    def create_invoice_items(self, purchase_order):
        logger.info(" -------------- create_invoice_items ------ ")
        total_net = 0
        total_vat = 0
        logger.info("Invoice Items")
        logger.info(self.invoice_items)
        invoice_inventory_items = []
        for field in self.invoice_items:
            if field[0:9] == 'quantity_':
                if self.invoice_items[field] != '':
                    logger.info(self.invoice_items)
                    quantity = float(self.invoice_items[field])
                    key = field[8:]
                    inventory_key = 'inventory_item{}'.format(key)
                    inventory_id = self.invoice_items.get("inventory_item{}".format(key))
                    unit_price = float(self.invoice_items.get("value{}".format(key), 0))
                    unit_id = self.invoice_items.get("unit{}".format(key))
                    vat_amount = float(self.invoice_items.get("vat_amount{}".format(key), 0))
                    vat_code_id = self.invoice_items.get("vat_code{}".format(key), 0)

                    logger.info("Inventory id={}, order amount={}, unit id={}, vat amount={}, vat code id={}".format(
                        inventory_id, unit_price, unit_id, vat_amount, vat_code_id
                    ))
                    order_value = (unit_price * quantity)
                    total_net += order_value
                    total_vat += vat_amount

                    if self.invoice_items[inventory_key] != '':
                        invoice_item = {'purchase_order': purchase_order,
                                        'inventory_id': inventory_id,
                                        'quantity': quantity,
                                        'unit_price': unit_price,
                                        'order_value': order_value,
                                        'unit_id': unit_id,
                                        'vat_code_id': vat_code_id,
                                        'vat_amount': vat_amount}
                        invoice_inventory_items.append(invoice_item)
        logger.info("Invoice items")
        logger.info(invoice_inventory_items)
        invoice_item = CreatePurchaseOrderItem(invoice_inventory_items)
        invoice_item.execute()


class CreatePurchaseOrderItem:

    def __init__(self, order_items):
        logger.info("---- Create purchase order Items-----")
        self.order_items = order_items

    def execute(self):
        logger.info("---- EXECUTE-----")
        po_inventory_items = []
        for inventory_order_item in self.order_items:
            po_inventory_item = PurchaseOrderItem(**inventory_order_item)
            po_inventory_items.append(po_inventory_item)
        logger.info(po_inventory_items)
        PurchaseOrderItem.objects.bulk_create(po_inventory_items)
        logger.info("------------------DONE CREATE BULK LEDGERS-----------------------")


class PurchaseOrderSummary(InvoicePurchaseOrder):

    def __init__(self, purchase_order):
        super().__init__(purchase_order.invoice)
        self.purchase_order = purchase_order

    def process_purchase_orders(self):
        for purchase_order_inventory in self.purchase_order.inventory_items.all():
            order_value = (purchase_order_inventory.unit_price * purchase_order_inventory.quantity)
            total = order_value + purchase_order_inventory.vat_amount
            self.total += total
            self.total_order_value += order_value
            self.total_vat += purchase_order_inventory.vat_amount
            self.purchase_orders.append({
                'description': purchase_order_inventory.inventory.description,
                'vat_code': purchase_order_inventory.vat_code,
                'vat_amount': round(purchase_order_inventory.vat_amount, 2),
                'unit': purchase_order_inventory.inventory.unit,
                'quantity': purchase_order_inventory.quantity,
                'quantity_in_unit': "{} {}".format(purchase_order_inventory.quantity, purchase_order_inventory.unit),
                'unit_value': purchase_order_inventory.unit_price,
                'order_value': round(order_value, 2),
                'total': round(total, 2),
                'id': purchase_order_inventory.id
            })
