import json
import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.views.generic import FormView, View, TemplateView
from django.views.generic.edit import UpdateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.models import ObjectItem, VatCode, Unit
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import BranchInventory, PurchaseOrderInventory, PurchaseOrder
from docuflow.apps.invoice.models import Invoice
from .forms import PurchaseOrderForm, DeclineForm, InventoryPurchaseOrderForm
from .services import CreatePurchaseOrder, PurchaseOrderSummary

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class PurchaseOrderView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'inventorypurchaseorder/index.html'

    def get_context_data(self, **kwargs):
        context = super(PurchaseOrderView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context


class CreatePurchaseOrderView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'inventorypurchaseorder/create.html'
    success_message = 'Purchase order successfully saved'
    form_class = PurchaseOrderForm

    def get_context_data(self, **kwargs):
        context = super(CreatePurchaseOrderView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['inventory_items'] = BranchInventory.objects.filter(branch=self.get_branch())
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['units'] = Unit.objects.filter(measure__company=company)
        return context

    def get_success_url(self):
        return reverse('inventory_purchase_order_index')

    def get_form_kwargs(self):
        kwargs = super(CreatePurchaseOrderView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the purchase order, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreatePurchaseOrderView, self).form_invalid(form)

    def prepare_invoice_items_from_post(self):
        invoice_items = {}
        for field in self.request.POST.items():
            if field[0:9] == 'quantity_':
                if self.request.POST[field] != '':
                    logger.info(self.request.POST)
                    invoice_items['quantity'] = float(self.request.POST[field])
                    key = field[8:]
                    invoice_items['inventory_id'] = self.request.POST.get("inventory_item{}".format(key))
                    invoice_items['order_amount'] = float(self.request.POST.get("value{}".format(key), 0))
                    invoice_items['unit_id'] = self.request.POST.get("unit{}".format(key))
                    invoice_items['vat_amount'] = float(self.request.POST.get("vat_amount{}".format(key), 0))
                    invoice_items['vat_code_id'] = self.request.POST.get("vat_code{}".format(key), 0)
        return invoice_items

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                company = self.get_company()
                profile = self.get_profile()
                role = Role.objects.filter(pk=self.request.session['role']).first()

                purchase_order = CreatePurchaseOrder(company, profile, role, form, self.request.POST)
                purchase_order.execute()

                return JsonResponse({
                    'text': 'Purchase order saved successfully.',
                    'error': False,
                    'redirect': reverse('inventory_purchase_order_index')
                })
            except InventoryException as exception:
                logger.exception(exception)
                return JsonResponse({
                    'text': str(exception),
                    'error': True,
                    'details': str(exception)
                })
        return HttpResponse('Not Allowed')


class AddInventoryItemRowView(ProfileMixin, TemplateView):
    template_name = 'inventorypurchaseorder/row.html'

    def get_context_data(self, **kwargs):
        context = super(AddInventoryItemRowView, self).get_context_data(**kwargs)
        context['row'] = int(self.request.GET['row']) + 1
        company = self.get_company()
        context['units'] = Unit.objects.filter(measure__company=company)
        context['vat_codes'] = VatCode.objects.filter(company=company)
        return context


class ResubmitPurchaseOrderView(LoginRequiredMixin, ProfileMixin, View):

    def get_purchase_order(self):
        return BranchInventory.objects.get(pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        invoice = self.get_purchase_order()
        role = self.get_role()

        next_invoice_id = invoice.get_role_next_invoice(role=role)

        redirect_url = reverse('all-invoices')
        if next_invoice_id:
            redirect_url = reverse('invoice_detail', kwargs={'pk': next_invoice_id})
        return JsonResponse({'text': 'Purchase order successfully resubmitted',
                             'error': False,
                             'redirect': redirect_url
                             })


class DeclineView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "inventorypurchaseorder/decline.html"
    form_class = DeclineForm

    def get_invoice(self):
        return BranchInventory.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(DeclineView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Purchase order could not be declined, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(DeclineView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                return JsonResponse({'error': False,
                                     'text': 'Purchase order successfully declined.',
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'Error occurred {}'.format(exception)
                                     })

        return HttpResponseRedirect(reverse('create_inventory_purchase_order'))


class EditPurchaseOrderLineView(ProfileMixin, UpdateView):
    template_name = 'inventorypurchaseorder/edit.html'
    form_class = InventoryPurchaseOrderForm

    def get_object(self, queryset=None):
        return PurchaseOrderInventory.objects.get(pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super(EditPurchaseOrderLineView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditPurchaseOrderLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        purchaser_order = self.get_object()
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['purchase_order'] = purchaser_order
        return context

    def form_valid(self, form):
        try:
            form.save()
            return JsonResponse({'text': 'Line updated successfully',
                                 'error': False,
                                 'reload': True
                                 })
        except Exception as exception:
            return JsonResponse({'text': 'Unexpected error occurred, please try again',
                                 'error': True,
                                 'details': exception.__str__()
                                 })


class CreatePurchaseOrderLineView(ProfileMixin, FormView):
    template_name = 'inventorypurchaseorder/create_line.html'
    form_class = InventoryPurchaseOrderForm

    def get_form_kwargs(self):
        kwargs = super(CreatePurchaseOrderLineView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(CreatePurchaseOrderLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['vat_codes'] = VatCode.objects.filter(company=company)
        context['invoice'] = self.get_invoice()
        return context

    def form_valid(self, form):
        try:
            invoice = self.get_invoice()
            purchase_order_line = form.save(commit=False)
            purchase_order_line.invoice = invoice
            purchase_order_line.save()
            return JsonResponse({'text': 'Purchase order line saved successfully',
                                 'error': False
                                 })
        except Exception as exception:
            return JsonResponse({'text': 'Unexpected error occurred creating the purchase order line',
                                 'details': exception.__str__(),
                                 'error': True
                                 })


class PurchaseOrderSummaryMixin(object):

    def get_summary_report(self, purchase_order):
        po = PurchaseOrderSummary(purchase_order)
        po.process_purchase_orders()
        return po


class PurchaseOrderLinesView(LoginRequiredMixin, PurchaseOrderSummaryMixin, TemplateView):
    template_name = 'inventorypurchaseorder/lines.html'

    def get_purchase_order(self):
        return PurchaseOrder.objects.select_related(
            'invoice'
        ).prefetch_related(
            'inventory_items'
        ).get(invoice_id=self.kwargs['pk'])

    @staticmethod
    def get_company_linked_modes(company):
        linked_models = {}
        count = 0
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            if count < 2:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
                    company_object_id=company_object.id)
            count += 1
        return linked_models

    def get_role_permissions(self):
        role_permissions = dict()
        role_id = str(self.request.session['role'])
        if 'permissions' in self.request.session and role_id in self.request.session['permissions']:
            role_permissions = self.request.session['permissions'][role_id]
        return role_permissions

    def get_context_data(self, **kwargs):
        context = super(PurchaseOrderLinesView, self).get_context_data(**kwargs)
        purchase_order = self.get_purchase_order()
        linked_models = self.get_company_linked_modes(purchase_order.invoice.company)

        po = self.get_summary_report(purchase_order)

        context['po'] = po
        can_edit = False
        if 'can_edit_invoice' in self.request.session:
            if purchase_order.invoice.id in self.request.session['can_edit_invoice']:
                can_edit = True
        if purchase_order.invoice.is_locked:
            can_edit = False
        context['can_edit'] = can_edit
        context['linked_models'] = linked_models
        context['linked_models_count'] = len(linked_models)
        context['invoice'] = purchase_order.invoice
        return context


class ShowPurchaseOrderView(LoginRequiredMixin, TemplateView):

    def get_template_names(self):
        if self.request.GET.get('print') == '1':
            return 'inventorypurchaseorder/print.html'
        return 'inventorypurchaseorder/template.html'

    def get_purchase_order(self):
        return PurchaseOrder.objects.select_related(
            'invoice'
        ).prefetch_related(
            'inventory_items', 'invoice__invoice_comments'
        ).get(invoice_id=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(ShowPurchaseOrderView, self).get_context_data(**kwargs)
        purchase_order = self.get_purchase_order()
        context['purchase_order'] = purchase_order
        context['invoice'] = purchase_order.invoice
        ordered_by = purchase_order.get_ordered_by()
        created_by = purchase_order.get_created_by()
        approved_by = purchase_order.get_approved_by()
        context['created_by'] = created_by
        context['approved_by'] = approved_by
        context['ordered_by'] = ordered_by
        context['action_type'] = self.kwargs.get('type', 'none')
        context['is_print'] = self.request.GET.get('print') == '1'
        return context


class PurchaseOrderPrintView(LoginRequiredMixin, PurchaseOrderSummaryMixin, TemplateView):
    template_name = 'inventorypurchaseorder/print.html'

    def get_purchase_order(self):
        return PurchaseOrder.objects.select_related(
            'invoice'
        ).prefetch_related(
            'inventory_items'
        ).get(invoice_id=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(PurchaseOrderPrintView, self).get_context_data(**kwargs)
        purchase_order = self.get_purchase_order()
        context['invoice'] = purchase_order.invoice

        po = self.get_summary_report(purchase_order)

        context['po'] = po
        ordered_by = po.get_ordered_by()
        created_by = po.get_created_by()
        approved_by = po.get_approved_by()

        action_type = 'none'
        if 'type' in self.kwargs:
            action_type = self.kwargs['type']
        context['created_by'] = created_by
        context['approved_by'] = approved_by
        context['ordered_by'] = ordered_by
        context['action_type'] = action_type
        return context


class PurchaseOrderListView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        purchase_orders = []
        orders = PurchaseOrder.objects.filter(
            invoice__supplier_id=self.request.GET['supplier_id'],
            invoice__invoice_type__code='purchase-order',
            invoice__status__slug__in=['po-ordered'],
            invoice__branch=self.get_branch()
        )
        for po_order in orders:
            items_url = reverse('receive_purchase_order_items', kwargs={'invoice_id': po_order.id})
            purchase_orders.append({'text': po_order.__str__(), 'id': po_order.id,
                                    'invoice_id': po_order.invoice_id, 'items_url': items_url
                                    })

        return HttpResponse(json.dumps(purchase_orders))
