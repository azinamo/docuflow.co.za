from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.ReceivedView.as_view(), name='inventory_received_index'),
    path('create/', views.CreateReceivedView.as_view(), name='create_received'),
    path('create/general/', views.CreateGeneralReceivedView.as_view(), name='create_general_received'),
    path('add/general/row/', views.AddGeneralReceivedRowView.as_view(), name='add_inventory_received_row'),
    path('<int:pk>/detail/', csrf_exempt(views.ReceivedDetailView.as_view()), name='view_received'),
    path('supplier/inventory/received/items/', views.SupplierInventoriesReceivedView.as_view(),
         name='supplier_inventory_received_items'),
    path('supplier/inventory/items/', views.SupplierInventoriesView.as_view(), name='supplier_inventory_list'),
    path('purchase_order/search/list/', views.SearchPurchaseOrderItemView.as_view(),
         name='search_purchase_order_search_list'),
    path('purchase_order/list/', views.PurchaseOrderListView.as_view(), name='goods_purchase_order_list'),
    path('<int:purchase_order_id>/inventory/items/', views.ReceiveInventoryItemsView.as_view(),
         name='receive_purchase_order_items'),
    path('invoice/<int:invoice_id>/link/', views.ReceivePendingView.as_view(), name='link_invoice_goods_received'),
    path('received/<int:received_id>/inventory/items/', views.ReceivedInventoryItemsView.as_view(),
         name='received_inventory_items'),
    path('save/invoice/<int:invoice_id>/goods_received/', csrf_exempt(views.CreateInvoiceGoodsReceivedView.as_view()),
         name='save_invoice_goods_received'),
    path('no-invoice/', views.ReceivedNotInvoicedView.as_view(), name='goods_received_not_invoiced'),
    path('report', views.ReceivedAwaitingReportView.as_view(), name='good_received_awaiting_summary')
]
