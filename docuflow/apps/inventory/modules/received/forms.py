import logging
from collections import defaultdict
from decimal import Decimal

from django import forms
from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse_lazy
from django.utils.timezone import datetime

from docuflow.apps.common.enums import Module
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.exceptions import JournalAccountUnavailable, InventoryException
from docuflow.apps.inventory.models import (
    BranchInventory, Received, InventoryReceived, Ledger as InventoryLedger,
    PurchaseOrderInventory
)
from docuflow.apps.inventory.modules.inventorypurchaseorder.services import CreatePurchaseOrderItem
from docuflow.apps.inventory.services import FiFoReceived, is_valuation_balancing
from docuflow.apps.invoice.models import Invoice, Comment as InvoiceComment
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.period.models import Period
from docuflow.apps.purchaseorders.models import PurchaseOrder
from docuflow.apps.supplier.models import Supplier

logger = logging.getLogger(__name__)


class ReceivedForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')

        super(ReceivedForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['supplier'].queryset = Supplier.objects.filter(company=self.company)
        self.fields['purchase_order'].queryset = PurchaseOrder.objects.select_related(
            'invoice', 'invoice__company').filter(invoice__branch=self.branch)

    class Meta:
        model = Received
        fields = ('supplier', 'period', 'date', 'purchase_order',  'note', 'total_vat', 'total_amount')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'note': forms.TextInput(attrs={'class': 'form-control'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'supplier': forms.Select(attrs={'class': 'form-control chosen-select',
                                            'data-purchase-orders-ajax-url': reverse_lazy('goods_received_purchase_orders')}),
            'total_vat': forms.HiddenInput(),
            'total_amount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()
        date = cleaned_data['date']
        period = cleaned_data['period']
        supplier = cleaned_data.get('supplier')

        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        if not supplier:
            self.add_error('supplier', 'Supplier is required')

        inventory_items_counter = 0
        for field, value in self.request.POST.items():
            if field.startswith('inventory_item', 0, 14):
                row_id = field[15:]
                quantity = self.request.POST.get(f'quantity_received_{row_id}')
                if value and quantity:
                    inventory_items_counter += 1
        if inventory_items_counter == 0:
            self.add_error(None, 'No inventory lines saved, please capture the inventory to be received.')
        return cleaned_data

    # noinspection PyMethodMayBeStatic
    def get_transaction_type(self, purchase_order):
        if purchase_order is None:
            return MovementType.GENERAL
        else:
            return MovementType.PURCHASE_ORDER

    # noinspection PyMethodMayBeStatic
    def is_valid_back_order(self, quantity, ordered):
        return quantity != ordered

    def get_default_inventory_variance_account(self):
        default_account = self.branch.company.default_inventory_variance_account
        if not default_account:
            raise JournalAccountUnavailable('Inventory variance account not set up, please fix')
        return default_account

    def get_goods_received_account(self):
        goods_received_account = self.branch.company.default_goods_received_account
        if not goods_received_account:
            raise JournalAccountUnavailable('Default Goods Received Interim Account not available, please fix.')
        return goods_received_account

    def save(self, commit=True):
        received = super().save(commit=False)
        received.branch = self.branch
        received.movement_type = self.get_transaction_type(self.cleaned_data.get('purchase_order', None))
        received.profile = self.profile
        received.role = self.role
        received.save()

        ledger_inventory_lines = defaultdict(Decimal)
        ledger_control_lines = defaultdict(Decimal)
        ledger_lines = []
        adjustment_account = self.get_default_inventory_variance_account()
        goods_received_account = self.get_goods_received_account()
        invoice_total = Decimal(0)
        back_order_items = []
        counter = 0


        for field, value in self.request.POST.items():
            if field.startswith('inventory_item', 0, 14) and value:
                index = field[15:]
                vat_percentage = self.request.POST.get(f'vat_percentage_{index}', 0) or 0
                percentage = Decimal(vat_percentage)
                data = {
                    'ordered': self.request.POST.get(f'quantity_ordered_{index}'),
                    'short': self.request.POST.get(f'quantity_short_{index}', 0),
                    'quantity': self.request.POST.get(f'quantity_received_{index}', 0),
                    'price_excluding': self.request.POST.get(f'price_{index}', 0),
                    'price_including': self.request.POST.get(f'price_including_{index}', 0),
                    'unit': self.request.POST.get(f'unit_id_{index}', 0),
                    'vat': self.request.POST.get(f'vat_code_{index}', 0),
                    'inventory': self.request.POST.get(f'inventory_item_{index}', 0),
                    'received': received
                }
                inventory_received_form = InventoryReceivedForm(data=data, branch=self.branch)
                if inventory_received_form.is_valid():
                    is_back_ordered = bool(self.request.POST.get(f'back_order_{index}', False))
                    inventory = inventory_received_form.cleaned_data['inventory']
                    if inventory_received_form.cleaned_data['quantity']:
                        inventory_received = inventory_received_form.save()
                        counter += 1
                        if inventory_received:
                            fifo_received = FiFoReceived(
                                inventory=inventory,
                                quantity=inventory_received.quantity,
                                content_object=inventory_received,
                                unit_price=inventory_received.price_excluding,
                                content_type=ContentType.objects.get_for_model(inventory_received)
                            )
                            fifo_received.execute()

                            ledger_inventory_lines[inventory.gl_account] += inventory_received.total_price_excluding

                            if fifo_received.adjustment_total != 0:
                                ledger_inventory_lines[adjustment_account] += Decimal(fifo_received.adjustment_total).quantize(Decimal('0.01'))

                            if goods_received_account:
                                ledger_control_lines[goods_received_account] += inventory_received.total_price_excluding

                            invoice_total += inventory_received.total_price_excluding

                            inventory.recalculate_balances(year=received.period.period_year)

                            _inventory_ledger_lines = fifo_received.inventory_ledger_lines
                            ledger_lines.append(_inventory_ledger_lines)

                    quantity = inventory_received_form.cleaned_data['quantity']
                    ordered = inventory_received_form.cleaned_data['ordered']
                    if self.is_valid_back_order(quantity, ordered) and is_back_ordered:
                        purchase_order_id = self.request.POST.get(f'purchase_order_{index}', None)
                        vat = inventory_received_form.cleaned_data['vat']
                        unit = inventory_received_form.cleaned_data['unit']
                        unit_price = inventory_received_form.cleaned_data['price_excluding']
                        back_order_item = self.handle_back_order_line(
                            purchase_order_id=purchase_order_id, vat=vat, inventory=inventory, quantity=quantity,
                            unit=unit, ordered=ordered, unit_price=unit_price, percentage=percentage
                        )
                        back_order_items.append(back_order_item)
                elif inventory_received_form.errors:
                    logger.info(inventory_received_form.errors)
                    raise forms.ValidationError('Inventory line could not be saved')

        journal = self.create_journal_lines(
            received=received,
            control_lines=ledger_control_lines,
            inventory_lines=ledger_inventory_lines
        )
        self.create_ledger_lines(received, ledger_lines, journal)

        self.create_back_order_purchase_order(received, back_order_items)

        if received.purchase_order:
            received.purchase_order.invoice.mark_as_received()

        if not is_valuation_balancing(branch=self.branch, year=self.year):
            raise InventoryException('A variation in inventory accounts and inventory valuation detected.')

        received.journal = journal
        received.save()
        return received

    def handle_back_order_line(self, purchase_order_id, inventory, quantity, ordered, unit_price, unit, percentage, vat):
        purchase_order_line = None
        if purchase_order_id:
            purchase_order_line = self.get_purchase_order_line(purchase_order_id)

            if purchase_order_line:
                purchase_order_line.mark_as_closed()
        short_quantity = ordered - quantity
        return {'purchase_order_line': purchase_order_line,
                'inventory': inventory,
                'quantity': short_quantity,
                'unit_price': unit_price,
                'unit': unit,
                'percentage': percentage,
                'vat_code': vat
                }

    # noinspection PyMethodMayBeStatic
    def get_purchase_order_line(self, purchase_order_id):
        return PurchaseOrderInventory.objects.filter(id=purchase_order_id).first()

    # noinspection PyMethodMayBeStatic
    def create_journal_lines(self, received, control_lines, inventory_lines):
        company = received.branch.company

        journal_lines = []
        for account, total in inventory_lines.items():
            journal_lines.append({'account': account,
                                  'debit': total,
                                  'is_reversal': False,
                                  'credit': 0,
                                  'accounting_date': received.date,
                                  'object_id': received.id,
                                  'content_type': ContentType.objects.get_for_model(received),
                                  'text': f"Goods received - {str(received)}",
                                  'ledger_type_reference': received.supplier.name if received.supplier else ''
                                  })
        for account, total in control_lines.items():
            journal_lines.append({'account': account,
                                  'debit': 0,
                                  'credit': total,
                                  'is_reversal': False,
                                  'accounting_date': received.date,
                                  'object_id': received.id,
                                  'content_type': ContentType.objects.get_for_model(received),
                                  'text': f"Goods received - {str(received)}",
                                  'ledger_type_reference': received.supplier.name if received.supplier else ''
                                  })
        ledger = None
        if len(journal_lines) > 0:
            create_ledger = CreateLedger(
                company=company,
                year=received.period.period_year,
                period=received.period,
                text=f"Goods received - {str(received)}",
                module=Module.INVENTORY,
                role=received.role,
                date=received.date,
                profile=received.profile,
                journal_lines=journal_lines
            )
            ledger = create_ledger.execute()
        return ledger

    # noinspection PyMethodMayBeStatic
    def create_ledger_lines(self, received, ledger_lines, journal):
        bulk_ledger_items = []
        for inventory_item_lines in ledger_lines:
            for line in inventory_item_lines:
                InventoryLedger(**{'inventory': line.get('inventory'),
                                   'quantity': line.get('quantity'),
                                   'value': line.get('price'),
                                   'created_by': received.profile,
                                   'period': received.period,
                                   'date': received.date,
                                   'text': str(received),
                                   'status': InventoryLedger.COMPLETE,
                                   'supplier': received.supplier,
                                   'journal': journal,
                                   'content_type': ContentType.objects.get_for_model(received),
                                   'object_id': received.id,
                                   'object_owner': str(received.supplier),
                                   'object_reference': str(received)
                                   }).save()

        #         bulk_ledger_items.append(ledger_line)
        # InventoryLedger.objects.bulk_create(bulk_ledger_items)

    def create_back_order_purchase_order(self, received, back_order_items):
        if len(back_order_items) > 0:
            original_purchase_order = received.purchase_order

            invoice = self.create_invoice(original_purchase_order.invoice)
            if invoice:
                purchase_order = self.create_inventory_purchase_order(original_purchase_order, invoice)

                self.log_invoice_creation(received, invoice, original_purchase_order)
                # Add link back to the original invoice/po
                self.create_link_back(original_purchase_order.invoice, invoice)

                total_invoice_amount = 0
                total_vat_amount = 0

                po_inventory_items = []
                for back_order_item in back_order_items:
                    quantity = back_order_item.get('quantity')
                    unit_price = back_order_item.get('unit_price')
                    percentage = back_order_item.get('percentage')
                    vat_code = back_order_item.get('vat_code')

                    order_value = unit_price * quantity
                    total_invoice_amount += order_value
                    vat_amount = 0
                    if percentage:
                        vat_amount = self.calculate_vat(percentage, order_value)
                    total_vat_amount += vat_amount

                    po_inventory_items.append({
                        'purchase_order': purchase_order,
                        'inventory': back_order_item.get('inventory'),
                        'quantity': quantity,
                        'unit_id': back_order_item.get('unit_id'),
                        'total_amount': order_value,
                        'unit_price': unit_price,
                        'vat_code_id': vat_code.id,
                        'vat_amount': vat_amount
                    })

                create_invoice_item = CreatePurchaseOrderItem(po_inventory_items)
                create_invoice_item.execute()

                net_amount = total_invoice_amount - total_vat_amount

                invoice.total_amount = total_invoice_amount
                invoice.vat_amount = total_vat_amount
                invoice.net_amount = net_amount
                invoice.save()

    # noinspection PyMethodMayBeStatic
    def create_invoice(self, original_invoice):
        original_invoice_id = original_invoice.invoice_id.replace('-', '.') if '-' in original_invoice.invoice_id else original_invoice.invoice_id
        invoice_str = f"{Decimal(original_invoice_id) + Decimal('0.1')}"
        invoice_id = invoice_str.replace('.', '-')
        invoice = Invoice.objects.create(
            company=original_invoice.company,
            supplier=original_invoice.supplier,
            period=original_invoice.period,
            status=original_invoice.status,
            invoice_type=original_invoice.invoice_type,
            supplier_name=original_invoice.supplier_name,
            invoice_number=invoice_id,
            invoice_id=invoice_id,
            invoice_date=datetime.now(),
            total_amount=Decimal(0),
            branch=original_invoice.branch,
            accounting_date=original_invoice.accounting_date,
            is_back_order=True,
            module=Module.INVENTORY
        )
        return invoice

    # noinspection PyMethodMayBeStatic
    def create_inventory_purchase_order(self, original_purchase_order, invoice):
        purchase_order_id = original_purchase_order.invoice_id + Decimal('0.1')
        purchase_order = PurchaseOrder.objects.create(
            invoice=invoice,
            purchase_order_id=purchase_order_id,
            parent=original_purchase_order
        )
        return purchase_order

    # noinspection PyMethodMayBeStatic
    def log_invoice_creation(self, received, invoice, purchase_order):
        link_url = reverse_lazy('invoice_detail', kwargs={'pk': purchase_order.invoice.id})
        InvoiceComment.objects.create(
            invoice=invoice,
            note=f'This is a BACK ORDER purchase order from {str(purchase_order)} short delivery '
                 f'refer to {str(received)}',
            participant=received.profile.user,
            link_url=link_url,
            role=received.role,
            log_type=InvoiceComment.COMMENT
        )

    # noinspection PyMethodMayBeStatic
    def create_link_back(self, original_invoice, new_invoice):
        if original_invoice.is_back_order:
            po_link = original_invoice.purchase_order_links.first()
            new_invoice.purchase_order_links.add(po_link)
        else:
            new_invoice.purchase_order_links.add(original_invoice)

    # noinspection PyMethodMayBeStatic
    def calculate_vat(self, percentage, price):
        line_vat = 0
        if percentage > 0:
            line_vat = (float(percentage) / 100) * float(price)
        return Decimal(line_vat)


class InventoryReceivedForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super().__init__(*args, **kwargs)
        self.fields['inventory'].queryset = BranchInventory.objects.filter(branch=self.branch)
        self.fields['unit'].queryset = self.fields['unit'].queryset.filter(measure__company=self.branch.company)
        self.fields['received'].queryset = self.fields['received'].queryset.filter(branch=self.branch)

    class Meta:
        model = InventoryReceived
        fields = ('inventory', 'ordered', 'quantity', 'short', 'unit', 'vat', 'price_excluding',
                  'price_including', 'is_back_order', 'received')

    def clean(self):
        cleaned_data = super(InventoryReceivedForm, self).clean()
        inventory = cleaned_data.get('inventory', 0)
        price = cleaned_data.get('price_excluding', 0)
        if price == 0 or price < 0:
            raise ValueError(f"Price for {inventory} cannot be zero")

        return cleaned_data

    def save(self, commit=True):
        inventory_received = super().save(commit=True)

        inventory_received.inventory.recalculate_balances(year=inventory_received.received.period.period_year)

        return inventory_received
