import logging
from collections import defaultdict
from datetime import datetime
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.db.models import F, Sum
from django.urls import reverse

from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.common import utils
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Company, Account, VatCode
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.exceptions import InventoryException, InvalidPrice, JournalAccountUnavailable
from docuflow.apps.inventory.models import BranchInventory, PurchaseOrderInventory, Received, Returned, \
    InventoryReceived, \
    InventoryReturned, Ledger as InventoryLedger, InventoryReceivedReturned, PurchaseOrder
from docuflow.apps.inventory.modules.inventorypurchaseorder.services import CreatePurchaseOrderItem
from docuflow.apps.inventory.modules.ledger.services import CreateInventoryLedger
from docuflow.apps.inventory.services import RegisterInventoryHistory, FiFoReceived
from docuflow.apps.invoice.models import Invoice, Comment as InvoiceComment, InvoiceAccount
from docuflow.apps.invoice.utils import processed_statuses
from docuflow.apps.journals.services import CreateLedger

logger = logging.getLogger(__name__)


class ReceiveGoods(object):

    def __init__(self, branch, profile, role, year, form, received_items):
        logger.info("----ReceiveGoods--")
        self.form = form
        self.branch = branch
        self.profile = profile
        self.role = role
        self.received_items = received_items
        self.year = year

    def get_transaction_type(self):
        if 'purchase_order' in self.form.cleaned_data:
            return MovementType.PURCHASE_ORDER
        else:
            return MovementType.GENERAL

    def create(self):
        logger.info("--Create the received transaction--")
        received = self.form.save(commit=False)
        received.branch = self.branch
        received.movement_type = self.get_transaction_type()
        received.profile = self.profile
        received.role = self.role
        received.save()
        logger.info(f"--Created transaction --> {received}--")
        return received

    def execute(self):
        logger.info("----execute--")
        with transaction.atomic():
            received = self.create()
            if received:
                received_items = self.prepare_received_items()
                if received_items:
                    received_items = CreateReceiveItems(received, received_items)
                    received_items.execute()

                    if received.purchase_order:
                        received.purchase_order.invoice.mark_as_received()

    def is_valid_back_order(self, quantity, ordered):
        return quantity != ordered

    def prepare_received_items(self):
        logger.info("--- prepare_received_items ---- ")
        inventory_items = {}
        for field, value in self.received_items.items():
            logger.info(" {} ---- {}".format(field, value))
            if field.startswith('inventory_item', 0, 14):
                counter = field[15:]
                logger.info("COunter --> {}".format(counter))
                inventory_id = self.received_items.get(f'inventory_item_{counter}')
                inventory = None
                if inventory_id:
                    inventory = BranchInventory.objects.filter(id=inventory_id).first()

                if inventory:
                    quantity_ordered = self.received_items.get('quantity_ordered_{}'.format(counter), 0)
                    quantity_short = self.received_items.get('quantity_short_{}'.format(counter), 0)
                    quantity_received = self.received_items.get('quantity_received_{}'.format(counter), 0)
                    excluding = self.received_items.get('price_excluding_{}'.format(counter), 0)
                    price = self.received_items.get('price_{}'.format(counter), 0)
                    ordered = 0
                    short = 0
                    quantity = 0
                    price_excluding = 0
                    if excluding:
                        price_excluding = float(excluding)
                    elif price:
                        price_excluding = float(price)
                    if quantity_ordered:
                        ordered = float(self.received_items.get('quantity_ordered_{}'.format(counter), 0))
                    if quantity_received:
                        quantity = float(quantity_received)
                    if quantity_short:
                        short = float(quantity_short)
                    unit_id = self.received_items.get('unit_id_{}'.format(counter), None)
                    vat_code_id = self.received_items.get('vat_code_{}'.format(counter), None)
                    price_including = float(self.received_items.get('price_including_{}'.format(counter), None))
                    purchase_order_id = self.received_items.get('purchase_order_{}'.format(counter), None)
                    is_back_ordered = bool(self.received_items.get('back_order_{}'.format(counter), False))
                    percentage = float(self.received_items.get('vat_percentage_{}'.format(counter), 0))

                    if not self.is_valid_back_order(quantity, ordered):
                        is_back_ordered = False

                    unit_price = price_excluding
                    inventory_received_line = dict()
                    inventory_received_line['key'] = counter
                    inventory_received_line['inventory'] = inventory
                    inventory_received_line['short'] = short
                    inventory_received_line['unit_id'] = unit_id
                    inventory_received_line['quantity'] = quantity
                    inventory_received_line['ordered'] = ordered
                    inventory_received_line['percentage'] = percentage
                    inventory_received_line['is_back_ordered'] = is_back_ordered
                    inventory_received_line['unit_price'] = unit_price
                    inventory_received_line['vat_code_id'] = vat_code_id
                    inventory_received_line['price_including'] = price_including
                    inventory_received_line['purchase_order_id'] = purchase_order_id

                    inventory_items[counter] = inventory_received_line
        return inventory_items

    def get_money(self, value):
        price = 0
        if value and value != '':
            price = float(value.replace(",", ""))
        return price

    def create_purchase_history(self, inventory, received, price, quantity):
        history = RegisterInventoryHistory(inventory, received, price, quantity)
        history.create()

    def calculate_price_including(self, vat_code, price):
        if vat_code:
            price = ((vat_code.percentage / 100) * float(price)) + float(price)
        return Decimal(price)

    def create_link_back(self, original_invoice, new_invoice):
        if original_invoice.is_back_order:
            po_link = original_invoice.purchase_order_links.first()
            new_invoice.purchase_order_links.add(po_link)
        else:
            new_invoice.purchase_order_links.add(original_invoice)

    def create_inventory_journal(self,  profile, inventory_lines, received):
        content_type = ContentType.objects.filter(app_label='inventory', model='transaction').first()

        for inventory, inventory_movements in inventory_lines.items():
            inventory_total = Decimal(0)
            quantity = 0
            for inventory_movement in inventory_movements:
                quantity = float(inventory_movement.quantity)
                value = float(inventory_movement.price_excluding)

                InventoryLedger.objects.create(
                    inventory=inventory,
                    content_type=content_type,
                    object_id=received.id,
                    quantity=Decimal(quantity),
                    value=Decimal(value),
                    created_by=profile,
                    period=received.period,
                    date=received.date,
                    supplier=received.supplier,
                    text=received.__str__()
                )

    def update_recipe_inventory_items(self, received_recipes, profile):
        for recipe in received_recipes:
            for recipe_item in recipe.recipe_items.all():
                logger.info(recipe_item)
                inventory_returned = InventoryReturned.objects.create(
                    inventory=recipe,
                    quantity=recipe_item.quantity,
                )
                if inventory_returned:
                    InventoryReceivedReturned.objects.create(
                        inventory_returned=inventory_returned,
                        inventory_received=None,
                        quantity=recipe_item.quantity
                    )


class CreateReceiveItems:

    def __init__(self, transaction, inventory_items):
        logger.info("------------CreateReceiveItems-----------")
        self.transaction = transaction
        self.inventory_items = inventory_items

    def validate_price(self, price):
        if price == 0 or price < 0:
            raise InvalidPrice("Price cannot be zero, do you want to use default price")

    def get_content_type(self):
        return ContentType.objects.filter(app_label='inventory', model='received').first()

    def get_vat_code(self, vat_code_id):
        if vat_code_id:
            return VatCode.objects.filter(id=vat_code_id).first()
        return None

    def get_default_inventory_variance_account(self):
        default_account = self.transaction.branch.company.default_inventory_variance_account
        if not default_account:
            raise JournalAccountUnavailable('Inventory variance account not set up, please fix')
        return default_account

    def get_goods_received_account(self):
        goods_received_account = self.transaction.branch.company.default_goods_received_account
        if not goods_received_account:
            raise JournalAccountUnavailable('Default Goods Received Interim Account not available, please fix.')
        return goods_received_account

    def get_purchase_order_line(self, purchase_order_id):
        logger.info(f"GET purchase order line {purchase_order_id}")
        return PurchaseOrderInventory.objects.filter(id=purchase_order_id).first()

    def execute(self):
        logger.info("------------execute-----------")
        total_vat = 0
        total_amount = 0
        recipe_items = []
        ledger_inventory_lines = defaultdict(Decimal)
        ledger_control_lines = defaultdict(Decimal)
        ledger_lines = []
        adjustment_account = self.get_default_inventory_variance_account()
        goods_received_account = self.get_goods_received_account()
        invoice_total = 0
        back_order_items = []
        content_type = ContentType.objects.filter(app_label='inventory', model='received').first()
        for counter, inventory_item in self.inventory_items.items():
            inventory = inventory_item.get('inventory')
            unit_price = Decimal(inventory_item.get('unit_price'))
            quantity = Decimal(inventory_item.get('quantity'))
            vat_code_id = inventory_item.get('vat_code_id')
            unit_id = inventory_item.get('unit_id')
            price_including = Decimal(inventory_item.get('price_including', 0))
            purchase_order_id = inventory_item.get('purchase_order_id', None)
            is_back_ordered = inventory_item.get('is_back_ordered', False)
            ordered = Decimal(inventory_item.get('ordered', 0))
            short = Decimal(inventory_item.get('short', 0))
            percentage = inventory_item.get('percentage', 0)

            self.validate_price(unit_price)

            vat_code = self.get_vat_code(vat_code_id)
            line_vat = 0
            if percentage:
                line_vat = self.calculate_vat(percentage, price_including)
            total_vat += line_vat

            total_price_excluding = unit_price * quantity
            total_amount += price_including * quantity

            inventory_received = InventoryReceived.objects.create_received_inventory(
                self.transaction, inventory, unit_price, quantity, ordered, price_including, vat_code, unit_id,
                is_back_ordered, short)

            fifo_received = FiFoReceived(inventory, quantity, unit_price, inventory_received, content_type)
            fifo_received.execute()

            logger.info(f"Inventory {inventory} is recipe item {inventory.is_recipe_item}")
            # if inventory.is_recipe_item and inventory.recipe.count():
            #     recipe_items.append(inventory.recipe)

            if fifo_received.inventory_total != 0:
                ledger_inventory_lines[inventory.gl_account] += Decimal(fifo_received.inventory_total)

            if fifo_received.adjustment_total != 0:
                ledger_inventory_lines[adjustment_account] += Decimal(fifo_received.adjustment_total)

            if goods_received_account:
                ledger_control_lines[goods_received_account] += Decimal(total_price_excluding)

            invoice_total += total_price_excluding

            self.create_purchase_history(inventory, self.transaction, unit_price, quantity)

            purchase_order_line = None
            if purchase_order_id:
                purchase_order_line = self.get_purchase_order_line(purchase_order_id)

                if purchase_order_line:
                    purchase_order_line.mark_as_closed()

            _inventory_ledger_lines = fifo_received.inventory_ledger_lines
            ledger_lines.append(_inventory_ledger_lines)

            if is_back_ordered:
                short_quantity = ordered - quantity
                back_order_items.append({'purchase_order_line': purchase_order_line, 'inventory': inventory,
                                         'quantity': short_quantity, 'unit_price': unit_price,
                                         'unit_id': unit_id, 'percentage': percentage, 'vat_code_id': vat_code_id})

        journal = self.create_journal_lines(self.transaction, ledger_control_lines, ledger_inventory_lines, content_type)
        self.create_ledger_lines(ledger_lines, journal)

        self.create_back_order_purchase_order(back_order_items)

    def calculate_vat(self, percentage, price):
        line_vat = 0
        if percentage > 0:
            line_vat = (percentage / 100) * float(price)
        return line_vat

    def create_purchase_history(self, inventory, received, price, quantity):
        logger.info("--------------create_purchase_history---------------")
        history = RegisterInventoryHistory(inventory, received, price, quantity)
        history.create()

    def calculate_price_including(self, vat_code, price):
        if vat_code:
            price = ((vat_code.percentage / 100) * float(price)) + float(price)
        return Decimal(price)

    def create_invoice(self, original_invoice):
        invoice_id = f"{original_invoice.invoice_id}-{1}"

        invoice = Invoice.objects.create(
            company=original_invoice.company,
            supplier=original_invoice.supplier,
            status=original_invoice.status,
            invoice_type=original_invoice.invoice_type,
            supplier_name=original_invoice.supplier_name,
            invoice_number=invoice_id,
            invoice_id=invoice_id,
            invoice_date=datetime.now(),
            total_amount=Decimal(0),
            branch=original_invoice.branch,
            accounting_date=original_invoice.accounting_date,
            is_back_order=True,
            module=Module.INVENTORY
        )
        return invoice

    def create_inventory_purchase_order(self, original_purchase_order, invoice):
        purchase_order_id = original_purchase_order.purchase_order_id + Decimal('0.1')
        purchase_order = PurchaseOrder.objects.create(
            invoice=invoice,
            purchase_order_id=purchase_order_id,
            parent=original_purchase_order
        )
        return purchase_order

    def create_back_order_purchase_order(self, back_order_items):
        if len(back_order_items) > 0:
            original_purchase_order = self.transaction.purchase_order

            invoice = self.create_invoice(original_purchase_order.invoice)
            if invoice:
                purchase_order = self.create_inventory_purchase_order(original_purchase_order, invoice)

                self.log_invoice_creation(invoice, original_purchase_order)
                # Add link back to the original invoice/po
                self.create_link_back(original_purchase_order.invoice, invoice)

                total_invoice_amount = 0
                total_vat_amount = 0

                po_inventory_items = []
                for back_order_item in back_order_items:
                    quantity = back_order_item.get('quantity')
                    unit_price = back_order_item.get('unit_price')
                    inventory = back_order_item.get('inventory')
                    unit_id = back_order_item.get('unit_id')
                    percentage = back_order_item.get('percentage')
                    vat_code_id = back_order_item.get('vat_code_id')

                    order_value = float(unit_price) * float(quantity)
                    total_invoice_amount += order_value
                    vat_amount = 0
                    if percentage:
                        vat_amount = self.calculate_vat(percentage, order_value)
                    total_vat_amount += vat_amount

                    po_inventory_items.append({
                        'purchase_order': purchase_order,
                        'inventory': inventory,
                        'quantity': quantity,
                        'unit_id': unit_id,
                        'order_value': order_value,
                        'unit_price': unit_price,
                        'vat_code_id': vat_code_id,
                        'vat_amount': vat_amount
                    })

                create_invoice_item = CreatePurchaseOrderItem(po_inventory_items)
                create_invoice_item.execute()

                net_amount = total_invoice_amount - total_vat_amount

                invoice.total_amount = total_invoice_amount
                invoice.vat_amount = total_vat_amount
                invoice.net_amount = net_amount
                invoice.save()

    def log_invoice_creation(self, invoice, purchase_order):
        link_url = reverse('invoice_detail', kwargs={'pk': purchase_order.invoice.id})
        InvoiceComment.objects.create(
            invoice=invoice,
            note=f'This is a BACK ORDER purchase order from {str(purchase_order)} short delivery '
                 f'refer to {str(self.transaction)}',
            participant=self.transaction.profile.user,
            link_url=link_url,
            role=self.transaction.role,
            log_type=InvoiceComment.COMMENT
        )

    def create_link_back(self, original_invoice, new_invoice):
        if original_invoice.is_back_order:
            po_link = original_invoice.purchase_order_links.first()
            new_invoice.purchase_order_links.add(po_link)
        else:
            new_invoice.purchase_order_links.add(original_invoice)

    def create_journal_lines(self, received, control_lines, inventory_lines, content_type):
        logger.info("--------------create_journal_entries---------------x")
        company = received.branch.company

        journal_lines = []
        for account, total in inventory_lines.items():
            journal_lines.append({'account': account,
                                  'debit': total,
                                  'is_reversal': False,
                                  'credit': 0,
                                  'accounting_date': received.date,
                                  'object_id': received.id,
                                  'content_type': content_type,
                                  'text': f"Goods received - {str(received)}",
                                  'ledger_type_reference': received.supplier.name if received.supplier else ''
                                  })
            # total_amount += total
        for account, total in control_lines.items():
            journal_lines.append({'account': account,
                                  'debit': 0,
                                  'credit': total,
                                  'is_reversal': False,
                                  'accounting_date': received.date,
                                  'object_id': received.id,
                                  'content_type': content_type,
                                  'text': f"Goods received - {str(received)}",
                                  'ledger_type_reference': received.supplier.name if received.supplier else ''
                                  })
        ledger = None
        if len(journal_lines) > 0:
            create_ledger = CreateLedger(company=company,
                                         year=received.period.period_year,
                                         period=received.period,
                                         text=f"Goods received - {str(received)}",
                                         module=Module.INVENTORY,
                                         role=received.role,
                                         date=received.date,
                                         profile=received.profile,
                                         journal_lines=journal_lines
                                         )
            ledger = create_ledger.execute()
        return ledger

    def create_ledger_lines(self, ledger_lines, journal):
        bulk_ledger_items = []
        content_type = self.get_content_type()
        for inventory_item_lines in ledger_lines:
            for line in inventory_item_lines:
                ledger_line = InventoryLedger(**{'inventory': line.get('inventory'),
                                                 'quantity': line.get('quantity'),
                                                 'value': line.get('price'),
                                                 'created_by': self.transaction.profile,
                                                 'period': self.transaction.period,
                                                 'date': self.transaction.date,
                                                 'text': str(self.transaction),
                                                 'status': InventoryLedger.COMPLETE,
                                                 'supplier': self.transaction.supplier,
                                                 'journal': journal,
                                                 'content_type': line.get('content_type', content_type),
                                                 'object_id': self.transaction.id,
                                                 'object_owner': str(self.transaction.supplier),
                                                 'object_reference': str(self.transaction)
                                                 })

                bulk_ledger_items.append(ledger_line)
        InventoryLedger.objects.bulk_create(bulk_ledger_items)

    def create_inventory_journal(self, received, inventory, unit_price, quantity, content_type):
        create_ledger = CreateInventoryLedger(inventory,
                                              quantity, unit_price,
                                              received.date,
                                              received.period,
                                              received.profile,
                                              False,
                                              str(received),
                                              received.supplier,
                                              content_type,
                                              received.id
                                              )
        create_ledger.execute()


class PendingReceived(object):

    def __init__(self, branch):
        self.branch = branch

    def is_still_available(self, received):
        if received.total_returned is None or received.available_quantity > 0:
            return True
        return False

    def invoice_out_of_flow(self, invoice_account):
        statuses = processed_statuses()
        logger.info("Invoice status {} is in {} -> ".format(invoice_account.invoice.status.slug, statuses))
        if invoice_account.invoice.status.slug in statuses:
            return True
        return False

    def get_goods_received(self, supplier=None, with_returns=False):

        filter_options = dict()
        filter_options['received__movement_type__in'] = [MovementType.GENERAL, MovementType.PURCHASE_ORDER]
        filter_options['received__branch'] = self.branch
        if supplier:
            filter_options['received__supplier'] = supplier

        logger.info(filter_options)
        goods_received = InventoryReceived.objects.annotate(
            available_quantity=F('quantity') - Sum('returned_items__quantity'),
            total_returned=Sum('returned_items__quantity')
        ).filter(
            **filter_options
        ).select_related(
            'received',
            'received__purchase_order',
            'received__invoice_account'
        ).prefetch_related(
            'received__returns',
            'received__returns__invoice_account'
        ).order_by(
            '-created_at'
        )
        available_received_items = []
        total = 0
        for inventory_received in goods_received:
            logger.info("{}->{}".format(inventory_received.received.id, inventory_received.received.__str__()))
            logger.info("Available {} from {}".format(inventory_received.available_quantity, inventory_received.quantity))
            logger.info("total_returned {}".format(inventory_received.total_returned))
            # logger.info("total_price_excl {}".format(inventory_received.total_price_excl))
            logger.info("-")
            if self.is_still_available(inventory_received):
                line = {'received': None, 'invoice': None, 'amount': 0, 'invoice_id': None, 'is_return': False}
                if inventory_received.received.has_invoice_account:
                    invoice_account = inventory_received.received.invoice_account
                    logger.info("Linked to Invoice account ----> {} ".format(invoice_account))
                    if invoice_account:
                        if not self.invoice_out_of_flow(invoice_account):
                            continue

                        line['invoice'] = invoice_account.invoice
                        line['invoice_id'] = invoice_account.invoice_id

                available_quantity = inventory_received.available_quantity if inventory_received.available_quantity else inventory_received.quantity
                price_excluding = inventory_received.price_excluding if inventory_received.price_excluding else 0
                total_price_excl = price_excluding * available_quantity
                line['received'] = inventory_received
                line['amount'] = total_price_excl
                total += float(total_price_excl)
                available_received_items.append(line)

                if inventory_received.received.returns.count() > 0 and with_returns:
                    for returned in inventory_received.received.returns.all():
                        logger.info("Returned -- {}".format(returned))
                        if returned.has_invoice_account:
                            if not self.invoice_out_of_flow(returned.invoice_account):
                                amount = returned.total_excl * -1
                                total += float(amount)
                                line = {'returned': returned, 'is_return': True,
                                        'invoice': returned.invoice_account.invoice,
                                        'amount': amount,
                                        'invoice_id': returned.invoice_account.invoice_id
                                        }
                                available_received_items.append(line)
                        else:
                            amount = returned.total_excl * -1
                            total += float(amount)
                            line = {'returned': returned, 'is_return': True, 'invoice': None, 'amount': amount,
                                    'invoice_id': None}
                            available_received_items.append(line)
            logger.info("\r\n\n")
        logger.info(available_received_items)
        return available_received_items, total


class InvoiceLinking(PendingReceived):

    def __init__(self, branch, invoice, profile, role):
        super(PendingReceived, self).__init__(branch)
        self.invoice = invoice
        self.profile = profile
        self.role = role

    def get_list_from_string(self, goods_str):
        if goods_str != '':
            [int(x) for x in goods_str.split(",")]
        return []

    def execute(self, goods_received_str, goods_returned_str):
        goods_returned_list = self.get_list_from_string(goods_returned_str)
        goods_received_list = self.get_list_from_string(goods_received_str)

        logger.info(goods_received_list)
        logger.info(goods_returned_list)

        if len(goods_returned_list) == 0 and len(goods_received_list) == 0:
            raise InventoryException('No goods received or returned available')

        received_and_returned_goods, total = self.get_goods_received(self.invoice.supplier, True)

        self.delete_current_invoice_accounts(self.invoice)

        inventory_control_account = self.get_inventory_control_account()
        total_amount = self.handle_invoice_goods_linking(inventory_control_account, received_and_returned_goods,
                                                         goods_returned_list, goods_received_list)

        self.handle_variance(total_amount)

    def delete_current_invoice_accounts(self, invoice):
        net_accounts = invoice.invoice_accounts.all_with_deleted().filter(is_vat=False)
        logger.info(net_accounts.count())
        logger.info(net_accounts)
        invoice.invoice_accounts.all_with_deleted().filter(is_vat=False).delete()

    def calcaulate_variance(self, total_received):
        net_amount = 0
        if self.invoice.net_amount:
            net_amount = float(self.invoice.net_amount)
        variance = net_amount - float(total_received)
        return variance

    def get_variance_account(self, company):
        if not company.default_inventory_variance_account:
            raise InventoryException('Default variance account not set, please update')
        return company.default_inventory_variance_account

    def get_inventory_control_account(self):
        company = self.invoice.company
        if not company.default_goods_received_account:
            raise InventoryException('Default inventory control account not set, please update')
        return company.default_goods_received_account

    def calculate_percentage(self, invoice, amount):
        if invoice.net_amount > 0:
            return round(amount / invoice.net_amount, 2) * 100
        return 0

    def handle_received_inventory(self, received_and_returned_line, inventory_control_account):
        logger.info("--- handle_received_inventory ---")
        received = received_and_returned_line.get('received').received
        amount = received_and_returned_line.get('amount')
        logger.info(received)
        logger.info(received.id)

        invoice_account = InvoiceAccount.objects.filter(
            goods_received_id=received.id
        ).first()
        if not invoice_account:
            invoice_account = InvoiceAccount.objects.create(
                invoice=self.invoice,
                account=inventory_control_account,
                amount=amount,
                profile=self.profile,
                role=self.role,
                goods_received_id=received.id,
                comment=received.__str__(),
                percentage=self.calculate_percentage(self.invoice, amount)
            )
        return invoice_account

    def handle_returned_inventory(self, returned_line, inventory_control_account):
        logger.info("---Handle Returned line --")
        returned = returned_line.get('returned')
        amount = returned_line.get('amount')
        logger.info(returned)

        invoice_account = InvoiceAccount.objects.filter(
            goods_returned_id=returned.id
        ).first()
        if not invoice_account:
            invoice_account = InvoiceAccount.objects.create(
                invoice=self.invoice,
                account=inventory_control_account,
                amount=amount,
                profile=self.profile,
                role=self.role,
                goods_returned_id=returned.id,
                comment=returned.__str__(),
                percentage=self.calculate_percentage(self.invoice, amount)
            )
        return invoice_account

    def handle_invoice_goods_linking(self, inventory_control_account, received_and_returned_goods, goods_returned_list,
                                     goods_received_list):
        total_amount = 0
        logger.info("Handle goods and invoice linking")
        logger.info(received_and_returned_goods)
        logger.info(goods_received_list)

        logger.info(goods_returned_list)
        for received_and_returned_line in received_and_returned_goods:
            logger.info("Line --> ")
            logger.info(received_and_returned_line)
            is_return = received_and_returned_line.get('is_return')
            if is_return:
                logger.info("Is return")
                logger.info(received_and_returned_line)
                if received_and_returned_line['returned'].id in goods_returned_list:
                    logger.info("Go to handle returned")
                    invoice_account = self.handle_returned_inventory(
                        received_and_returned_line,
                        inventory_control_account
                    )
                    if invoice_account:
                        total_amount += float(invoice_account.amount)
            else:
                logger.info("Is received ")
                logger.info(received_and_returned_line)
                if received_and_returned_line['received'].received_id in goods_received_list:
                    logger.info("Go to handle received")
                    invoice_account = self.handle_received_inventory(
                        received_and_returned_line,
                        inventory_control_account
                    )
                    if invoice_account:
                        total_amount += float(invoice_account.amount)
        return total_amount

    def handle_variance(self, total_amount):
        variance = self.calcaulate_variance(total_amount)
        logger.info("Received Total = vs Invoice total = {}".format(total_amount, self.invoice.total_amount))
        logger.info("Variance --> {}".format(variance))
        if variance != 0:
            variance_account = self.get_variance_account(self.invoice.company)
            InvoiceAccount.objects.create(
                invoice=self.invoice,
                account=variance_account,
                amount=variance,
                profile=self.profile,
                role=self.role
            )


class SupplierGoodsReceived(PendingReceived):

    def __init__(self):
        logger.info("----- SupplierGoodsReceived -----")
        super(PendingReceived, self)
        # self.branch = branch
        self.data = defaultdict()
        self.total = 0
        logger.info("----- total  {}-----".format(self.total))
        logger.info(self.data)

    def execute(self):
        received_and_returned_goods, total = self.get_goods_received(None, True)
        for transaction in received_and_returned_goods:
            is_return = transaction.get('is_return', False)
            if not is_return:
                self.data[transaction.received.supplier_id].append(transaction.received)


def handle_received_inventory(goods_received: Received, inventory_control_account: Account, invoice: Invoice, role: Role, profile: Profile):
    logger.info("--- handle_received_inventory ---")
    amount = goods_received.link_amount
    logger.info(f"Received transaction {goods_received} ({goods_received.id}) for {amount} ")

    invoice_account = getattr(goods_received, 'invoice_account', None)
    if not invoice_account and amount != 0:
        invoice_account = InvoiceAccount.objects.create(
            invoice=invoice,
            account=inventory_control_account,
            amount=amount,
            profile=profile,
            role=role,
            goods_received_id=goods_received.id,
            comment=str(goods_received),
            percentage=calculate_invoice_account_line_percentage(invoice, amount)
        )

    return invoice_account


def handle_purchase_order_general_lines(goods_received: Received, invoice: Invoice, role: Role, profile: Profile):
    general_lines = []
    if goods_received.purchase_order:
        logger.info(f"Goods received  {goods_received} Purchase order  {goods_received.purchase_order}, has some general lines, lets post them ")
        for account_line in goods_received.purchase_order.purchaseorder_items.filter(account__isnull=False):
            invoice_account = InvoiceAccount.objects.create(
                invoice=invoice,
                account=account_line.account,
                amount=account_line.net_amount,
                profile=profile,
                role=role,
                comment=str(goods_received.purchase_order),
                percentage=calculate_invoice_account_line_percentage(invoice, account_line.net_amount)
            )
            general_lines.append(invoice_account)
    return general_lines


def handle_returned_inventory(goods_returned: Returned, inventory_control_account: Account, invoice: Invoice, role: Role, profile: Profile):
    amount = goods_returned.total_excl
    logger.info(f"Handle Returned transaction {goods_returned} --- ({goods_returned.id}) for {amount}")

    if amount != 0:
        logger.info(f"Create new invoice account for {inventory_control_account}")
        invoice_account, is_new = InvoiceAccount.objects.update_or_create(
            goods_returned_id=goods_returned.id,
            defaults={
                'invoice': invoice,
                'account': inventory_control_account,
                'amount': amount,
                'profile': profile,
                'role': role,
                'goods_returned_id': goods_returned.id,
                'comment': str(goods_returned),
                'percentage': calculate_invoice_account_line_percentage(invoice, amount)
            }
        )
        return invoice_account
    return None


def handle_variance(total_amount, invoice: Invoice, role: Role, profile: Profile):
    variance = calculate_variance(total_amount, invoice)
    logger.info(f"Total Received  = {total_amount} of type ({type(total_amount)}), rounded to {utils.round_decimal(total_amount)} and becomes {type({utils.round_decimal(total_amount)})} vs "
                f"Invoice total = {invoice.total_amount} of type ({type(invoice.total_amount)})")
    logger.info(f"Variance --> {variance} of type {type(variance)}")
    if variance != 0:
        variance_account = get_variance_account(invoice.company)
        InvoiceAccount.objects.create(
            invoice=invoice,
            account=variance_account,
            amount=variance,
            profile=profile,
            role=role,
            percentage=calculate_invoice_account_line_percentage(invoice, variance)
        )


def get_variance_account(company: Company):
    if not company.default_inventory_variance_account:
        raise InventoryException('Default variance account not set, please update')
    return company.default_inventory_variance_account


def calculate_variance(total_received, invoice):
    return (invoice.net_amount if invoice.net_amount else Decimal(0)) - utils.round_decimal(total_received)


def calculate_invoice_account_line_percentage(invoice: Invoice, amount):
    if invoice.net_amount > 0:
        return round(float(amount) / float(invoice.net_amount), 2) * 100
    return 0
