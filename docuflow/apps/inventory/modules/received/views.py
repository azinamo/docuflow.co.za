import json
import logging
import pprint
from datetime import datetime
from decimal import Decimal
from typing import List

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import F, Sum, Q
from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.views.generic import View, TemplateView, CreateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin, ActiveYearRequiredMixin, DataTableMixin
from docuflow.apps.common.utils import round_decimal, is_ajax, is_ajax_post
from docuflow.apps.company.models import VatCode
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import BranchInventory, Received, Returned, InventoryReceived
from docuflow.apps.invoice.models import Invoice, InvoiceAccount
from docuflow.apps.invoice.utils import processed_statuses, processing_statuses
from docuflow.apps.journals.services import get_total_for_account
from docuflow.apps.purchaseorders.models import PurchaseOrder, PurchaseOrderItem
from . import services
from .forms import ReceivedForm

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class ReceivedView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'received/index.html'


class CreateReceivedView(LoginRequiredMixin, ActiveYearRequiredMixin, CreateView):
    template_name = 'received/create.html'
    form_class = ReceivedForm
    model = Received

    def get_initial(self):
        initial = super(CreateReceivedView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateReceivedView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['role'] = Role.objects.filter(pk=self.request.session['role']).first()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateReceivedView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the goods received, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateReceivedView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    form.save()
                    return JsonResponse({'text': 'Goods received saved successfully',
                                         'error': False,
                                         'redirect': reverse('inventory_received_index')
                                         })
            except (InventoryException, ValueError) as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True,
                                     'alert': True
                                     })


class CreateGeneralReceivedView(CreateReceivedView):
    template_name = 'received/create_general.html'


class AddGeneralReceivedRowView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'received/row.html'

    def get_context_data(self, **kwargs):
        context = super(AddGeneralReceivedRowView, self).get_context_data(**kwargs)
        context['vat_codes'] = VatCode.objects.filter(company=self.get_company())
        context['row'] = self.request.GET.get('row_id', 0)
        return context


class ReceivedDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'received/detail.html'

    def get_transaction(self):
        received = Received.objects.select_related(
            'purchase_order'
        ).prefetch_related(
            'inventory_received',
        ).filter(
            pk=self.kwargs['pk']
        ).first()
        return received

    def get_context_data(self, **kwargs):
        context = super(ReceivedDetailView, self).get_context_data(**kwargs)
        received = self.get_transaction()
        context['received'] = received
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        return context


class SearchPurchaseOrderItemView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        purchase_orders = []
        term = self.request.GET.get('term', None)
        if term:
            search_term = term
            company = self.get_company()
            company_str = company.name[0:3]
            po_key = "PO-{}".format(company_str.upper())
            key_len = len(po_key)
            if term.startswith(po_key, 0, key_len):
                search_term = int(str(search_term[key_len:]))
            elif term.startswith('PO', 0, 2):
                search_term = str(search_term[2:])

            orders = PurchaseOrder.objects.filter(purchase_order_id__icontains=search_term, company=company)
            items = {'results': []}
            for po_order in orders:
                po_items_url = reverse('receive_purchase_order_items', kwargs={'pk': po_order.id})
                purchase_orders.append({'text': po_order.__str__(), 'id': po_order.id,
                                        'invoice_id': po_order.invoice.id, 'po_items_url': po_items_url
                                        })

        return HttpResponse(json.dumps({'results': purchase_orders, 'total_count': purchase_orders.count()}))


class PurchaseOrderListView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        purchase_orders = []
        orders = PurchaseOrder.objects.filter(
            invoice__supplier_id=self.request.GET['supplier_id'],
            invoice__status__slug__in=['po-ordered'],
            invoice__branch=self.get_branch()
        )
        for po_order in orders:
            items_url = reverse('receive_purchase_order_items', kwargs={'invoice_id': po_order.id})
            purchase_orders.append({'text': po_order.__str__(), 'id': po_order.id,
                                    'invoice_id': po_order.invoice_id, 'items_url': items_url
                                    })

        return HttpResponse(json.dumps(purchase_orders))


class SupplierInventoriesView(ProfileMixin, View):
    def get(self, request, *args, **kwargs):

        inventory_items = []
        branch_inventories = BranchInventory.objects.filter(
            supplier_id=self.request.GET['supplier_id'], branch=self.get_branch()
        )

        for branch_inventory in branch_inventories:
            inventory_items.append({
                'text': str(branch_inventory),
                'id': branch_inventory.id,
                'inventory_id': branch_inventory.id,
                'received_items_url': reverse('supplier_inventory_received_items')
            })

        return JsonResponse(inventory_items, safe=False)


class ListGoodsReceivedView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):

        received_items = []
        supplier_id = self.request.GET.get('supplier_id', None)
        if supplier_id:
            goods_received = Received.objects.filter(supplier_id=supplier_id, branch=self.get_branch())
            for received_item in goods_received:
                items_url = reverse('return_received_items', kwargs={'movement_id': received_item.id})
                received_items.append({
                    'text': str(received_item),
                    'id': received_item.id,
                    'items_url': items_url
                })

        return JsonResponse(received_items, safe=False)


class SupplierInventoriesReceivedView(LoginRequiredMixin, ProfileMixin, View):
    def get(self, request, *args, **kwargs):

        received_items = []
        supplier_id = self.request.GET.get('supplier_id', None)
        inventory_id = self.request.GET.get('inventory_id', None)
        if supplier_id:
            filter_options = {'movement__supplier_id': supplier_id}
            if inventory_id:
                filter_options['inventory_id'] = inventory_id

            received_item_qs = InventoryReceived.objects.prefetch_related(
                'returned_items'
            ).filter(**filter_options)

            for received_item in received_item_qs:
                total_received = received_item.quantity
                total_return = 0
                for returned_received_item in received_item.returned_items.all():
                    total_return += returned_received_item.quantity
                quantity = total_received - total_return
                if quantity > 0:
                    items_url = reverse('return_received_items', kwargs={'movement_id': received_item.transaction.id})
                    received_items.append({
                        'text': str(received_item.movement),
                        'id': received_item.transaction.id,
                        'items_url': items_url
                    })
        return JsonResponse(received_items, safe=False)


class ReceiveInventoryItemsView(ProfileMixin, TemplateView):
    template_name = 'received/purchase_order_items.html'

    def get_purchase_order_inventory_items(self):
        inventory_items = {'total_ordered': 0, 'total_received': 0, 'total_short': 0, 'total_price_exl': 0,
                           'total_price_in': 0, 'total_total_price_exl': 0, 'total_total_price_inc': 0,
                           'inventory_items': []
                           }
        for po_item in PurchaseOrderItem.objects.filter(purchase_order_id=self.kwargs['purchase_order_id'], inventory__isnull=False):
            inventory_items['total_ordered'] += po_item.quantity
            inventory_items['total_received'] += 0
            inventory_items['total_short'] += 0
            inventory_items['total_price_exl'] += po_item.unit_price
            inventory_items['total_price_in'] += po_item.price_with_vat
            inventory_items['total_total_price_exl'] += po_item.net_amount
            inventory_items['total_total_price_inc'] += po_item.total_amount
            inventory_items['inventory_items'].append(po_item)
        return inventory_items

    def get_context_data(self, **kwargs):
        context = super(ReceiveInventoryItemsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['po_items'] = self.get_purchase_order_inventory_items()
        return context


class PendingReceivedMixin(object):

    # noinspection PyMethodMayBeStatic
    def is_still_available(self, received, with_returns):
        if with_returns:
            return True
        if received.total_returned is None or received.available_quantity > 0:
            return True
        return False

    # noinspection PyMethodMayBeStatic
    def invoice_out_of_flow(self, invoice_account):
        statuses = processed_statuses()
        if invoice_account.invoice.status.slug in statuses:
            return True
        return False

    # noinspection PyUnresolvedReferences
    def only_get_goods_received(self, supplier=None, with_returns=False):
        statuses = processed_statuses()
        processed_statuses_list = statuses + ['paid-printed']

        exclude_options = Q()
        filter_options = dict()
        filter_options['movement_type__in'] = [MovementType.GENERAL, MovementType.PURCHASE_ORDER]
        filter_options['branch'] = self.get_branch()
        if supplier:
            filter_options['supplier'] = supplier
        exclude_invoice_options = Q()
        # if not include_linked:
        #     exclude_invoice_options.add(Q(invoice_account__invoice__status__slug__in=processed_statuses_list), Q.OR)
        exclude_options.add(Q(status=Received.COMPLETED), Q.OR)

        goods_received = Received.objects.annotate(
            total=Sum('total_amount'),
            total_of_received=Sum(F('inventory_received__quantity') * F('inventory_received__price_excluding')),
            total_of_returned=Sum(F('returns__inventory_returned__quantity') * F('returns__inventory_returned__price_excluding'))
        ).filter(
            **filter_options
        ).exclude(
            exclude_options
        ).exclude(
            exclude_invoice_options
        ).select_related(
            'purchase_order', 'invoice_account'
        ).prefetch_related(
            'returns', 'returns__invoice_account',
        ).order_by(
            '-created_at'
        )
        goods = []
        for g in goods_received:
            exclude_received = self.exclude_transaction(g, processed_statuses_list)
            if not exclude_received:
                goods.append(g)
            if with_returns:
                for goods_return in g.returns.all():
                    exclude_returned = self.exclude_transaction(goods_return, processed_statuses_list)
                    if not exclude_returned:
                        goods.append(goods_return)
        return goods

    # noinspection PyMethodMayBeStatic
    def exclude_transaction(self, trans, invoice_statuses):
        if trans.has_invoice_account:
            if trans.invoice_account and trans.invoice_account.invoice.status.slug in invoice_statuses:
                return True
        return False

    # noinspection PyUnresolvedReferences
    def get_goods_received(self, supplier=None, with_returns=False):
        statuses = processed_statuses()

        filter_options = dict()
        filter_options['received__movement_type__in'] = [MovementType.GENERAL, MovementType.PURCHASE_ORDER]
        filter_options['received__branch'] = self.get_branch()
        if supplier:
            filter_options['received__supplier'] = supplier

        goods_received = InventoryReceived.objects.annotate(
            available_quantity=F('quantity') - Sum('returned_items__quantity'),
            total_returned_quantity=Sum('returned_items__quantity'),
            quantity_returned=Sum('returned_items__quantity'),
            total_returned=Sum(F('returned_items__quantity') * F('price_excluding')),
        ).filter(
            **filter_options
        ).select_related(
            'received', 'received__purchase_order', 'received__invoice_account'
        ).prefetch_related(
            'received__returns',
            'received__returns__invoice_account'
        ).order_by(
            '-created_at'
        )
        available_received_items = {}
        total = 0
        for inventory_received in goods_received:
            if self.is_still_available(inventory_received, False):
                line = {'received': None, 'invoice': None, 'amount': 0, 'invoice_id': None, 'is_return': False}
                if inventory_received.received.has_invoice_account:
                    invoice_account = inventory_received.received.invoice_account
                    if invoice_account:
                        if invoice_account.invoice.status.slug in statuses:
                            continue
                        line['invoice'] = invoice_account.invoice
                        line['invoice_id'] = invoice_account.invoice_id

                available_quantity = inventory_received.available_quantity if inventory_received.available_quantity else inventory_received.quantity
                price_excluding = inventory_received.price_excluding if inventory_received.price_excluding else 0
                total_price_excl = price_excluding * available_quantity

                line['received'] = inventory_received
                line['amount'] = total_price_excl
                total += float(total_price_excl)
                if inventory_received.received in available_received_items:
                    available_received_items[inventory_received.received]['lines'].append(line)
                    available_received_items[inventory_received.received]['total'] += total_price_excl
                    available_received_items[inventory_received.received]['is_return'] = False
                else:
                    available_received_items[inventory_received.received] = {'total': total_price_excl,
                                                                             'lines': [line],
                                                                             'is_return': False,
                                                                             'invoice': line.get('invoice')
                                                                             }
                logger.info("\r\n-- Add dandle returns \n\n")
                if inventory_received.returned_items.count() > 0 and with_returns:

                    for received_returned in inventory_received.returned_items.all():
                        returned = received_returned.inventory_returned.returned
                        logger.info("Returned {}({}) --> {} @ {}".format(returned, received_returned.inventory_returned, received_returned.inventory_returned, received_returned.quantity, received_returned.price))
                        logger.info("\r\n\n\n")

            logger.info("\r\n\n")
        logger.info(available_received_items)
        return available_received_items, total


class GoodsReceivedMixin(object):

    # noinspection PyMethodMayBeStatic
    def get_inventory_items(self, received_id):
        filter_options = {}

        if received_id:
            filter_options['received_id'] = received_id

        inventory_items = InventoryReceived.objects.prefetch_related(
            'returned_items'
        ).annotate(
            quantity_returned=Sum('returned_items__quantity'),
            available_quantity=F('quantity') - Sum('returned_items__quantity'),
            total_returned=Sum(F('returned_items__quantity') * F('price_excluding'))
        ).filter(
            **filter_options
        )
        data = {'total_price_excluding': 0, 'price_incl': 0, 'total_price': 0, 'inventory_items': []}
        for inventory_item in inventory_items:
            logger.info("For inventory --{}, quantity returned is {} @ {} when we received {}, avai => {}".format(
                inventory_item.inventory, inventory_item.quantity_returned, inventory_item.total_returned,
                inventory_item.quantity, inventory_item.available_quantity)
            )
            row = {'inventory': inventory_item.inventory, 'quantity': 0, 'price_excluding': inventory_item.price_excluding,
                   'price_including': inventory_item.price_including, 'total_price': 0, 'total_including': 0,
                   'total_excluding': 0, 'vat_code': inventory_item.vat}
            if inventory_item.quantity > 0:
                total_price_excluding = inventory_item.quantity * inventory_item.price_excluding
                total_price_including = inventory_item.quantity * inventory_item.price_including
                data['total_price'] += total_price_including
                data['total_price_excluding'] += total_price_excluding
                row['total_price_excluding'] = total_price_excluding
                row['total_price_including'] = total_price_including
                row['quantity'] = inventory_item.quantity
                data['inventory_items'].append(row)
        return data


class ReceivePendingView(ProfileMixin, PendingReceivedMixin, TemplateView):
    template_name = 'received/link_to_invoice.html'

    def get_invoice(self):
        return Invoice.objects.filter(id=self.kwargs['invoice_id']).first()

    def get_context_data(self, **kwargs):
        context = super(ReceivePendingView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()

        context['company'] = self.get_company()
        context['invoice'] = invoice
        context['transactions'] = self.only_get_goods_received(supplier=invoice.supplier, with_returns=True)
        context['total'] = 0
        return context


class ReceivedInventoryItemsView(LoginRequiredMixin, ProfileMixin, GoodsReceivedMixin, TemplateView):
    template_name = 'received/inventory_items.html'

    def get_context_data(self, **kwargs):
        context = super(ReceivedInventoryItemsView, self).get_context_data(**kwargs)
        inventory_items = self.get_inventory_items(self.kwargs['received_id'])
        context['inventory_received'] = inventory_items
        return context


class CreateInvoiceGoodsReceivedView(ProfileMixin, PendingReceivedMixin, View):

    def get_invoice(self):
        return Invoice.objects.filter(
            id=self.kwargs['invoice_id']
        ).select_related(
            'company', 'company__default_inventory_variance_account', 'company__default_inventory_control_account'
        ).prefetch_related(
            'invoice_accounts'
        ).first()

    def get_list_from_string(self, goods_str):
        if goods_str != '':
            return [int(x) for x in goods_str.split(",")]
        return []

    def calcaulate_variance(self, total_received, invoice):
        return (invoice.net_amount if invoice.net_amount else Decimal(0)) - round_decimal(total_received)

    def get_variance_account(self, company):
        if not company.default_inventory_variance_account:
            raise InventoryException('Default variance account not set, please update')
        return company.default_inventory_variance_account

    def get_inventory_control_account(self, invoice):
        company = invoice.company
        if not company.default_goods_received_account:
            raise InventoryException('Default inventory control account not set, please update')
        return company.default_goods_received_account

    def calculate_percentage(self, invoice, amount):
        if invoice.net_amount > 0:
            return round(float(amount) / float(invoice.net_amount), 2) * 100
        return 0

    def handle_invoice_goods_linking(self, inventory_control_account, received_and_returned_goods, goods_returned_list,
                                     goods_received_list, invoice, role, profile):
        total_amount = Decimal(0)

        for inventory_transaction in received_and_returned_goods:
            if isinstance(inventory_transaction, Received):
                if inventory_transaction.id in goods_received_list:
                    invoice_account = services.handle_received_inventory(
                        goods_received=inventory_transaction,
                        inventory_control_account=inventory_control_account,
                        invoice=invoice,
                        role=role,
                        profile=profile
                    )
                    if invoice_account:
                        total_amount += invoice_account.amount
                    po_invoice_accounts = services.handle_purchase_order_general_lines(
                        goods_received=inventory_transaction,
                        invoice=invoice,
                        role=role,
                        profile=profile
                    )
                    if po_invoice_accounts:
                        total_amount += sum(po_invoice_account.amount for po_invoice_account in po_invoice_accounts)

            if isinstance(inventory_transaction, Returned):
                if inventory_transaction.id in goods_returned_list:
                    invoice_account = services.handle_returned_inventory(
                        goods_returned=inventory_transaction,
                        inventory_control_account=inventory_control_account,
                        invoice=invoice,
                        role=role,
                        profile=profile
                    )
                    if invoice_account:
                        total_amount += invoice_account.amount
        return total_amount

    def get(self, request, *args, **kwargs):
        try:
            logger.info("Assign goods received to the ")
            invoice = self.get_invoice()
            profile = self.get_profile()
            role = self.get_role()

            goods_received_str = self.request.GET.get('goods_received')
            goods_returned_str = self.request.GET.get('goods_returned')

            with transaction.atomic():
                # linking = InvoiceLinking(self.get_branch(), invoice, profile, role)
                # linking.execute(goods_received_str, goods_returned_str)

                logger.info(goods_received_str)
                logger.info(goods_returned_str)

                goods_returned_list = self.get_list_from_string(goods_returned_str)
                goods_received_list = self.get_list_from_string(goods_received_str)

                if len(goods_returned_list) == 0 and len(goods_received_list) == 0:
                    raise InventoryException('No goods received or returned available')

                received_and_returned_goods = self.only_get_goods_received(
                    supplier=invoice.supplier,
                    with_returns=True
                )

                inventory_control_account = self.get_inventory_control_account(invoice=invoice)

                total_amount = self.handle_invoice_goods_linking(
                    inventory_control_account=inventory_control_account,
                    received_and_returned_goods=received_and_returned_goods,
                    goods_returned_list=goods_returned_list,
                    goods_received_list=goods_received_list,
                    invoice=invoice,
                    role=role,
                    profile=profile
                )

                for invoice_account in InvoiceAccount.objects.not_linked_to_inventory().filter(is_vat=False, invoice=invoice):
                    invoice_account.force_delete()

                services.handle_variance(
                    total_amount=total_amount,
                    invoice=invoice,
                    role=role,
                    profile=profile
                )

                return JsonResponse({
                    'error': False,
                    'text': 'Goods received successfully linked to invoice'
                })
        except InventoryException as exception:
            logger.exception(exception)
            return JsonResponse({
                'error': True,
                'details': str(exception),
                'text': str(exception)
            })


class ReceivedNotInvoicedView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'received/received_not_invoiced.html'

    def get_not_invoices_transactions(self, with_returns=True):
        statuses = processing_statuses()
        goods_received = Received.objects.awaiting_invoicing(branch=self.get_branch(), statuses=statuses).annotate(
            total=Sum('total_amount'),
            excl_total=Sum('total_amount') - Sum('total_vat')
        ).select_related(
            'purchase_order', 'supplier', 'period', 'invoice_account', 'invoice_account__invoice', 'invoice_account__invoice__status',
            'period__period_year', 'purchase_order__invoice__company', 'purchase_order__invoice__period__period_year'
        ).prefetch_related(
            'returns', 'returns__invoice_account', 'returns__invoice_account__invoice__status', 'inventory_received',
            'returns__invoice_account__invoice__company', 'returns__invoice_account__invoice__supplier'
        ).distinct().order_by(
            '-created_at'
        )
        goods = []
        total_amount = 0
        for g in goods_received:
            if self.is_not_account_posted(received=g, statuses=statuses):
                goods.append(g)
                if g.total_excl:
                    total_amount += round(g.total_excl, 2)
                if with_returns:
                    for goods_return in g.returns.all():
                        goods.append(goods_return)
                        if goods_return.return_amount:
                            total_amount += float(goods_return.return_amount)
        return goods, total_amount

    # noinspection PyMethodMayBeStatic
    def is_not_account_posted(self, received: Received, statuses: List[str]):
        if received.has_invoice_account:
            return received.invoice_account.invoice.status.slug in statuses
        return True

    def get_context_data(self, **kwargs):
        context = super(ReceivedNotInvoicedView, self).get_context_data(**kwargs)
        transactions, total = self.get_not_invoices_transactions(False)
        context['transactions'] = transactions
        context['total'] = total
        return context


class ReceivedAwaitingReportView(ReceivedNotInvoicedView):
    template_name = 'received/report.html'

    def get_balance_sheet_total(self, company, year):
        return get_total_for_account(
            company=company,
            year=year,
            account=company.default_goods_received_account,
            start_date=year.start_date,
            end_date=year.end_date
        )

    def get_context_data(self, **kwargs):
        context = super(ReceivedAwaitingReportView, self).get_context_data(**kwargs)
        company = self.get_company()
        transactions, total = self.get_not_invoices_transactions(with_returns=False)
        balance_sheet_total = self.get_balance_sheet_total(company=company, year=self.get_year())
        balance_sheet_total = balance_sheet_total * -1 if balance_sheet_total else 0
        context['total'] = total
        context['balance_sheet_total'] = balance_sheet_total
        context['is_balancing'] = balance_sheet_total == total
        return context
