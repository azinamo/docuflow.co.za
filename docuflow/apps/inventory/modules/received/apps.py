from django.apps import AppConfig


class ReceivedConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.received'
