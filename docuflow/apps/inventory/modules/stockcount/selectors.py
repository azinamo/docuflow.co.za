from django.db.models import Count, Sum, F
from django.db.models.functions import Coalesce

from docuflow.apps.company.models import Branch
from docuflow.apps.inventory.models import StockCount, InventoryStockCount


def get_stock_count_summary_list(branch: Branch):
    return StockCount.objects.annotate(
        count_lines=Count('inventory_items'),
        count_quantity=Count('inventory_items__quantity'),
        report_lines=Count('branch__inventory_items')
    ).select_related(
        'branch'
    ).prefetch_related(
        'inventory_items', 'inventory_items__inventory', 'inventory_items__inventory__history'
    ).filter(branch=branch).order_by('-id')


def get_stock_count_with_inventories(stock_count_id: int):
    return StockCount.objects.select_related(
        'branch'
    ).prefetch_related(
        'inventory_items', 'inventory_items__inventory', 'inventory_items__inventory__inventory'
    ).filter(pk=stock_count_id).first()


def get_counted_inventory_with_variances(stock_count: StockCount):
    return InventoryStockCount.objects.with_variances(stock_count).select_related(
        'inventory', 'inventory__inventory', 'inventory__gl_account', 'inventory__stock_adjustment_account'
    )


def get_stock_count_with_total_value():
    return StockCount.objects.annotate(
        total_value=Sum(Coalesce('inventory_items__price', 0) * Coalesce(F('inventory_items__original_quantity') - F('inventory_items__quantity'), 0))
    ).select_related(
        'branch'
    ).prefetch_related(
        'inventory_items__inventory', 'inventory_items__inventory__inventory'
    )
