from django import forms

from docuflow.apps.inventory.models import BranchInventory, StockCount, InventoryStockCount

from docuflow.apps.company.models import Branch
from docuflow.apps.period.models import Period


class StockCountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(StockCountForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['branch'].queryset = Branch.objects.filter(company=company)

    class Meta:
        model = StockCount
        fields = ('date', 'branch', 'captured_by', 'show_stock_on_hand', 'count_type', 'count')

    def execute(self, company, request):
        stock_count = self.save(commit=False)
        stock_count.company = company
        count_type_count = f"count_{self.cleaned_data['count_type']}"
        if count_type_count in request.POST:
            stock_count.count = int(request.POST.get(count_type_count, 0))
        stock_count.save()

        if stock_count:
            inventories = self.get_stock_count_inventory_items(stock_count)
            if inventories.exists():
                for inventory in inventories:
                    InventoryStockCount.objects.create(
                        stock_count=stock_count,
                        inventory=inventory,
                        original_quantity=inventory.in_stock if inventory.in_stock else 0
                    )
        return stock_count

    def get_stock_count_inventory_items(self, stock_count):
        filter_options = {'branch': stock_count.branch}
        order_by = ''
        if stock_count.is_selected_stock:
            filter_options['inventory_id__in'] = [inventory_item.inventory_id for inventory_item in stock_count.inventory_items.all()]
        elif stock_count.is_random:
            order_by = '?'
        elif stock_count.is_highest_item_value:
            order_by = '-history__average_price'
        elif stock_count.is_highest_total_value:
            order_by = '-history__total_value'
        elif stock_count.is_highest_quantity:
            order_by = '-history__in_stock'
        query = BranchInventory.objects.with_stock_levels(filter_options)
        if order_by:
            query = query.order_by(order_by)
        if stock_count.count and stock_count.count > 0:
            return query[0:stock_count.count]
        return query


class StockAdjustmentForm(forms.Form):

    AVERAGE_PRICE = 'average'
    LAST_PRICE = 'last_price'

    PRICE_TYPES = (
        (AVERAGE_PRICE, 'Average Price'),
        (LAST_PRICE, 'Last Price')
    )

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        super(StockAdjustmentForm, self).__init__(*args, **kwargs)
        if year:
            self.fields['period'].queryset = Period.objects.open(company, year)

    period = forms.ModelChoiceField(queryset=None, required=True)
    date = forms.DateField(required=True)
    reason = forms.CharField(widget=forms.Textarea())
    price_type = forms.ChoiceField(choices=PRICE_TYPES, required=True)

    def clean(self):
        period = self.cleaned_data.get('period')
        date = self.cleaned_data.get('date')
        if date and period:
            if period and not period.is_valid(date):
                self.add_error('period', 'Adjustment date and period do not match.')
                self.add_error('date', 'Adjustment date and period do not match.')
        return self.cleaned_data


class InventoryStockCountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super(InventoryStockCountForm, self).__init__(*args, **kwargs)
        self.fields['inventory'].queryset = BranchInventory.objects.filter(branch=self.branch)

    class Meta:
        model = InventoryStockCount
        fields = ('quantity', 'inventory')
