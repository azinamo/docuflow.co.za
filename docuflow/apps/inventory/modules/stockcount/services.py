from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.inventory import services


def get_receive_content_type():
    return ContentType.objects.filter(app_label='inventory', model='received').first()


def get_return_content_type():
    return ContentType.objects.filter(app_label='inventory', model='returned').first()


def make_adjustments(stock_count, counted_inventories: list, price_type: str, date, period, profile, role, reason: str):
    receive_content_type = get_receive_content_type()
    return_content_type = get_return_content_type()

    for stock_counted_inventory_item in counted_inventories:
        inventory = stock_counted_inventory_item.inventory
        quantity = stock_counted_inventory_item.variance

        if price_type == 'average':
            unit_price = services.calculate_average_price(inventory)
        else:
            unit_price = services.get_last_price(inventory)

        reason = reason or f'Stock Count {stock_count} Adjustments '
        data = {'unit_price': unit_price,
                'adjustment': quantity,
                'reason': reason,
                'text': reason,
                'date': date,
                'receive_content_type': receive_content_type,
                'return_content_type': return_content_type,
                'period': period,
                'account': inventory.stock_adjustment_account
                }
        if quantity > 0:
            services.receive_inventory(
                inventory=inventory,
                quantity=quantity,
                price=unit_price,
                year=period.period_year,
                content_object=stock_count
            )
            total_amount = unit_price * quantity

            ledger = services.create_received_general_journal(stock_count.branch.company, inventory.stock_adjustment_account,
                                                              period, inventory, total_amount, reason, date, role, profile,
                                                              stock_count)
            services.create_inventory_ledger(profile, inventory, unit_price, quantity, stock_count, reason, date,
                                             period, False, ledger)
        else:
            quantity_returned = quantity * -1
            returned_lines = services.return_inventory(
                inventory=inventory,
                quantity=quantity_returned,
                year=period.period_year,
                content_object=stock_count
            )
            inventory_total = Decimal(0)
            for returned_line in returned_lines.get('returned_lines'):
                price = returned_line.price * -1
                quantity = returned_line.quantity * -1
                inventory_total += price * quantity
                ledger = services.create_received_general_journal(stock_count.branch.company,
                                                                  inventory.stock_adjustment_account, period, inventory,
                                                                  inventory_total, reason, date, role, profile,
                                                                  stock_count)

                services.create_inventory_ledger(profile, inventory, unit_price, quantity, stock_count, reason, date,
                                                 period, True, ledger)
        stock_counted_inventory_item.price = unit_price
        stock_counted_inventory_item.save()

    stock_count.mark_as_adjusted()
    stock_count.save()
