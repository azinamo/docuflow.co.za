from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.StockCountView.as_view(), name='stock_count_index'),
    path('create/', views.CreateStockCountView.as_view(), name='create_stock_count'),
    path('edit/<int:pk>/', views.EditStockCountView.as_view(), name='edit_stock_count'),
    path('<int:pk>/capture/stock/', views.StockCountCaptureStockView.as_view(), name='stock_count_capture_stock'),
    path('<int:pk>/detail/', views.StockCountDetailView.as_view(), name='stock_count_detail'),
    path('save/stockcount/<int:pk>/capture/', views.StockCountCaptureView.as_view(), name='save_stock_capture_count'),
    path('<int:pk>/variance/', views.StockCountVarianceView.as_view(), name='view_variance'),
    path('variance/<int:pk>/adjustment/', views.MakeStockVarianceAdjustmentView.as_view(),
         name='make_stock_variance_adjustment'),
    path('select/<int:pk>/inventories/', views.SelectStockCountInventoriesView.as_view(), name='select_stock_count'),
    path('save/<int:pk>/inventories/', csrf_exempt(views.SaveSelectStockCountInventory.as_view()),
         name='save_selected_stock_count_inventory'),
]
