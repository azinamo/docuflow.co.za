from django.apps import AppConfig


class StockcountConfig(AppConfig):
    name = 'docuflow.apps.inventory.modules.stockcount'
