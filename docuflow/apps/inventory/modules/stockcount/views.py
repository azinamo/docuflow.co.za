import logging
import pprint
from datetime import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, FormView, View, TemplateView, DetailView
from django.views.generic.edit import UpdateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common import utils
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.inventory.models import BranchInventory, StockCount, InventoryStockCount
from docuflow.apps.invoice import enums
from docuflow.apps.sales.selectors import get_invoices_pending_update
from . import selectors
from . import services
from .forms import StockCountForm, StockAdjustmentForm, InventoryStockCountForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class StockCountView(LoginRequiredMixin, ProfileMixin, ListView):
    model = StockCount
    template_name = 'stockcount/index.html'
    context_object_name = 'stock_counts'

    def get_context_data(self, **kwargs):
        context = super(StockCountView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def get_queryset(self):
        return selectors.get_stock_count_summary_list(self.get_branch())


class CreateStockCountView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'stockcount/create.html'
    success_message = 'Stock count successfully saved'
    form_class = StockCountForm
    success_url = reverse_lazy('stock_count_index')

    def get_context_data(self, **kwargs):
        context = super(CreateStockCountView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context

    def get_initial(self):
        initial = super(CreateStockCountView, self).get_initial()
        initial['date'] = datetime.now().date()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateStockCountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the stock count, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateStockCountView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            company = self.get_company()
            has_pending_updates = get_invoices_pending_update(
                company=company,
                invoice_types=[enums.InvoiceType.CREDIT_NOTE.value, enums.InvoiceType.TAX_INVOICE.value]
            ).exists()
            if has_pending_updates:
                return JsonResponse({
                    'error': True,
                    'text': 'There are pending sales invoices updates',
                    'alert': True
                })
            with transaction.atomic():
                stock_count = form.execute(company, self.request)
            redirect_url = reverse('stock_count_index')
            if stock_count.is_selected_stock:
                redirect_url = reverse('select_stock_count', kwargs={'pk': stock_count.id})
            return JsonResponse({
                'error': False,
                'text': 'Stock count generated successfully',
                'redirect': redirect_url
            })
        return super(CreateStockCountView, self).form_valid(form)


class EditStockCountView(LoginRequiredMixin, ProfileMixin, UpdateView):
    model = BranchInventory
    template_name = 'stockcount/edit.html'
    success_message = 'Inventory item successfully updated'
    form_class = StockCountForm
    success_url = reverse_lazy('stock_count_index')

    def get_context_data(self, **kwargs):
        context = super(EditStockCountView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        context['inventory_item'] = self.get_object()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditStockCountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        with transaction.atomic():
            stock_count = form.save(commit=False)
            company = self.get_company()
            stock_count.company = company
            stock_count.save()

            messages.success(self.request, 'Stock count updated successfully.')
            return HttpResponseRedirect(reverse('stock_count_index'))


class StockCountCaptureStockView(ProfileMixin, TemplateView):
    template_name = 'stockcount/capture_count.html'
    success_message = 'Stock adjustment successfully saved'

    def get_stock_count(self):
        try:
            return selectors.get_stock_count_with_inventories(self.kwargs['pk'])
        except StockCount.DoesNotExist:
            raise Http404('Stock count sheet does not exist')

    def get_context_data(self, **kwargs):
        context = super(StockCountCaptureStockView, self).get_context_data(**kwargs)
        stock_count = self.get_stock_count()
        context['stock_count'] = stock_count
        return context

    def get_success_url(self):
        return reverse('stock_count_index')


class StockCountDetailView(ProfileMixin, DetailView):
    model = StockCount
    template_name = 'stockcount/detail.html'
    context_object_name = 'stock_count'

    def get_queryset(self):
        return selectors.get_stock_count_with_total_value()


class StockCountCaptureView(View):

    def get_stock_count_sheet(self):
        try:
            return selectors.get_stock_count_with_inventories(self.kwargs['pk'])
        except StockCount.DoesNotExist:
            raise Http404('Stock count sheet does not exist')

    def post(self, request, **kwargs):
        try:
            stock_count_sheet = self.get_stock_count_sheet()
            with transaction.atomic():
                for inventory_item in stock_count_sheet.inventory_items.all():
                    data = {
                        'inventory': inventory_item.inventory_id,
                        'quantity': self.request.POST.get(f"quantity_counted_{inventory_item.id}", 0)
                    }
                    inventory_stock_form = InventoryStockCountForm(
                        data=data, branch=stock_count_sheet.branch
                    )
                    if inventory_stock_form.is_valid():
                        inventory_item.quantity = inventory_stock_form.cleaned_data['quantity']
                        inventory_item.save()
                    else:
                        logger.info(inventory_stock_form.errors)
                        raise ValueError(f"Inventory {inventory_item.inventory} could not be saved")
                stock_count_sheet.mark_as_counted()
            return JsonResponse({
                'text': 'Stock capture saved successfully.',
                'error': False,
                'redirect': reverse('stock_count_index')
            })
        except ValueError as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': str(exception),
                'error': True,
                'detail': str(exception)
            })


class StockCountVarianceView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def get_template_names(self):
        if self.request.GET.get('print') == '1':
            return 'stockcount/variance/print.html'
        return 'stockcount/variance/show.html'

    def get_stock_count(self):
        return get_object_or_404(StockCount, pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(StockCountVarianceView, self).get_context_data(**kwargs)
        stock_count = self.get_stock_count()
        inventories_counted = selectors.get_counted_inventory_with_variances(stock_count)
        context['stock_count'] = stock_count
        context['inventories_counted'] = inventories_counted
        context['has_positive_variance'] = len([inventory_counted for inventory_counted in inventories_counted if inventory_counted.variance > 0])
        return context


class MakeStockVarianceAdjustmentView(ProfileMixin, FormView):
    template_name = 'stockcount/make_adjustment.html'
    success_message = 'Stock adjustment successfully saved'
    form_class = StockAdjustmentForm

    def get_initial(self):
        initial = super(MakeStockVarianceAdjustmentView, self).get_initial()
        stock_count = self.get_stock_count()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        initial['reason'] = f"Stock Count {str(stock_count)} Adjustments "
        return initial

    def get_stock_count(self):
        return get_object_or_404(StockCount, pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(MakeStockVarianceAdjustmentView, self).get_context_data(**kwargs)
        context['stock_count'] = self.get_stock_count()
        return context

    def get_form_kwargs(self):
        kwargs = super(MakeStockVarianceAdjustmentView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the stock count adjustments, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(MakeStockVarianceAdjustmentView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                profile = self.get_profile()
                role = Role.objects.filter(id=self.request.session['role']).first()

                stock_count = self.get_stock_count()
                date = form.cleaned_data['date']
                period = form.cleaned_data['period']
                price_type = self.request.POST.get('price_type')

                counted_inventories = selectors.get_counted_inventory_with_variances(stock_count)

                services.make_adjustments(stock_count, counted_inventories, price_type, date, period, profile,
                                          role, form.cleaned_data.get('reason', None))

                return JsonResponse({
                    'text': 'Adjustment successfully processed',
                    'error': False,
                    'redirect': reverse('stock_count_index')
                })
        return super(MakeStockVarianceAdjustmentView, self).form_valid(form)


class SelectStockCountInventoriesView(TemplateView):
    template_name = 'stockcount/select_inventory_items.html'

    def get_inventory_items(self, stock_count):
        q_objects = Q()
        filter_options = {'branch_id': stock_count.branch_id}
        q = self.request.GET.get('q', '')
        if q:
            q_objects.add(Q(description__icontains=q), Q.OR)
            q_objects.add(Q(inventory__code__icontains=q), Q.OR)
        return BranchInventory.objects.with_stock_levels(filter_options).filter(q_objects)

    def get_stock_count(self):
        return get_object_or_404(StockCount, pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(SelectStockCountInventoriesView, self).get_context_data(**kwargs)
        stock_count = self.get_stock_count()
        context['stock_count'] = stock_count
        context['inventory_items'] = self.get_inventory_items(stock_count)
        return context


class SaveSelectStockCountInventory(View):

    def get_stock_count(self):
        return get_object_or_404(StockCount, pk=self.kwargs['pk'])

    def post(self, request, **kwargs):
        inventory_items = request.POST.getlist('inventory_item')
        stock_count_sheet = self.get_stock_count()
        for inventory_id in inventory_items:
            data = {
                'inventory': inventory_id,
                'quantity': 0
            }
            inventory_stock_form = InventoryStockCountForm(
                data=data, branch=stock_count_sheet.branch
            )
            if inventory_stock_form.is_valid():
                inventory = inventory_stock_form.cleaned_data['inventory']
                InventoryStockCount.objects.get_or_create(
                    stock_count=stock_count_sheet,
                    inventory=inventory,
                    defaults={
                        'original_quantity': inventory.in_stock
                    }
                )
        return JsonResponse({
            'text': 'Selected inventory saved',
            'error': False,
            'redirect': reverse('stock_count_index')
        })
