import logging
import pprint
from collections import defaultdict
from datetime import datetime
from decimal import Decimal
from typing import Union

from django.contrib.contenttypes.fields import ContentType
from django.db.models import Sum, F, DecimalField
from django.db.models.functions import Coalesce
from django.template.defaultfilters import slugify
from django.utils import timezone

from docuflow.apps.common.enums import Module
from docuflow.apps.company.enums import SubLedgerModule
from docuflow.apps.company.models import Company, Account, Branch, Unit, Measure
from docuflow.apps.inventory import selectors
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.journals.models import Journal, JournalLine
from docuflow.apps.journals.services import get_total_for_sub_ledger
from docuflow.apps.period.models import Period, Year
from . import utils
from .exceptions import CompanyAccountCodeException, NoImportInventory, JournalAccountUnavailable
from .models import (Inventory, InventoryReceivedReturned, InventoryReturned, BranchInventory, Ledger, Received,
                     InventoryReceived, PurchaseHistory, GroupLevel, GroupLevelItem, ManufacturingRecipe,
                     StockAdjustmentInventory)
# from .tasks import calculate_inventory_stock
from ..sales.models import InvoiceItem

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class FifoInventoryAdjustment(object):

    def __init__(self, inventory, quantity):
        self.inventory = inventory
        self.quantity = quantity
        self.received_returned_cost = 0
        self.returned_cost = 0
        self.total_cost = 0
        self.returned_lines = []
        logging.info(f"FIFO ON INVENTOrY {self.inventory} and a quantity of {self.quantity}")

    def get_quantity(self):
        quantity = abs(self.quantity)
        return Decimal(quantity)

    def get_inventory_stock(self):
        quantity = Decimal(0)
        if self.inventory.in_stock:
            quantity = self.inventory.in_stock
        return quantity

    def update_received_inventory_item(self, inventory_received, inventory_returned):
        """
        Receive {quantity_received} specifically those returned/sold items {inventory_returned}
        """
        quantity_received = self.get_quantity()
        inventory_quantity = self.get_inventory_stock()
        total_received_qty = quantity_received
        for sold_inventory in inventory_returned.all():
            """
            Handle Sold inventory item being returned
            """
            for sold_item in sold_inventory.return_received_items.all():
                quantity = sold_item.quantity
                if quantity >= quantity_received:
                    opening_balance = inventory_quantity
                    returned_received = InventoryReceivedReturned.objects.create(
                        inventory_received=inventory_received,
                        inventory=inventory_received.inventory,
                        inventory_returned=sold_inventory,
                        quantity=quantity_received,
                        price=sold_item.price,
                        opening_balance=opening_balance,
                        data=utils.prepare_opening_closing_balance(sold_inventory, inventory_quantity,
                                                                   quantity_received)
                    )
                    inventory_quantity += quantity_received
                    total_received_qty -= quantity_received
                    quantity -= quantity_received
                    self.received_returned_cost += quantity_received * sold_item.price
                    self.returned_lines.append(returned_received)

    def update_received_inventory(self, inventory_returned: InventoryReturned, content_object=None, content_type=None):
        """
         We return what we received first, if we return what we have not received, inventory goes into negative stock
        :param inventory_returned: InventoryReturned
        :param content_object: GenericObject - Inventory can be returned by different process, eg Adjustment, Sales or General Return
        :param content_type: ContentType returning the inventory
        :return:
        """
        received_inventory_items = InventoryReceivedReturned.objects.get_available_items(self.inventory)
        quantity = self.get_quantity()
        inventory_quantity = self.get_inventory_stock()
        inventory_stock = inventory_quantity
        total_quantity = 0
        inventory_unit_cost = 0
        total_returned_qty = 0
        for inventory_received_item in received_inventory_items:
            if quantity > 0:
                if inventory_received_item.quantity > quantity:
                    opening_balance = inventory_quantity
                    outstanding_quantity = inventory_received_item.quantity - quantity
                    inventory_received_item.quantity = quantity
                    inventory_received_item.inventory_returned = inventory_returned
                    inventory_received_item.returned_type = content_type
                    inventory_received_item.returned_object_id = content_object.id if content_object else None
                    inventory_received_item.data = utils.prepare_opening_closing_balance(
                        inventory_returned, inventory_stock, quantity * -1, inventory_received_item.data
                    )
                    inventory_received_item.save()
                    inventory_quantity -= quantity
                    inventory_stock -= quantity

                    if outstanding_quantity > 0:
                        InventoryReceivedReturned.objects.create(
                            inventory_received=inventory_received_item.inventory_received,
                            received_object_id=inventory_received_item.received_object_id,
                            received_type=inventory_received_item.received_type,
                            inventory=inventory_received_item.inventory,
                            quantity=outstanding_quantity,
                            price=inventory_received_item.price,
                            opening_balance=opening_balance,
                            data=utils.prepare_opening_closing_balance(
                                inventory_received_item.inventory_received, inventory_stock, outstanding_quantity)
                        )
                    total_returned_qty -= quantity

                    self.received_returned_cost += quantity * inventory_received_item.price
                    total_quantity -= quantity
                    quantity = 0
                    self.returned_lines.append(inventory_received_item)
                    inventory_unit_cost = inventory_received_item.price
                else:
                    quantity = quantity - inventory_received_item.quantity
                    opening_balance = inventory_quantity
                    inventory_quantity -= inventory_received_item.quantity
                    total_quantity -= inventory_quantity
                    total_returned_qty -= inventory_received_item.quantity
                    inventory_received_item.inventory_returned = inventory_returned
                    inventory_received_item.returned_type = content_type
                    inventory_received_item.returned_object_id = content_object.id if content_object else None
                    inventory_received_item.data = utils.prepare_opening_closing_balance(inventory_returned,
                                                                                         inventory_stock,
                                                                                         inventory_received_item.quantity * -1,
                                                                                         inventory_received_item.data)
                    inventory_received_item.save()
                    inventory_stock -= inventory_received_item.quantity
                    self.returned_lines.append(inventory_received_item)
                    self.received_returned_cost += inventory_received_item.quantity * inventory_received_item.price
                    inventory_unit_cost = inventory_received_item.price

        if quantity > 0 and self.inventory.allow_negative_stocks:
            """
            CAN RETURNED MORE THAN RECEIVED
            """
            price = Decimal(0)
            if self.inventory.price_type == BranchInventory.LAST_PRICE:
                if self.inventory.current_price:
                    price = self.inventory.current_price
            elif self.inventory.average_price:
                price = self.inventory.average_price

            quantity_left = quantity - Decimal(inventory_quantity)
            opening_balance = inventory_quantity
            balance = inventory_quantity - quantity
            total_quantity -= inventory_quantity
            total_returned_qty -= quantity

            received_returned = InventoryReceivedReturned.objects.create(
                inventory_returned=inventory_returned,
                inventory=inventory_returned.inventory,
                returned_type=content_type,
                returned_object_id=content_object.id if content_object else None,
                quantity=quantity_left,
                price=price,
                balance=balance,
                opening_balance=opening_balance,
                data=utils.prepare_opening_closing_balance(
                    inventory_returned, inventory_stock, quantity_left
                )
            )
            self.returned_lines.append(received_returned)
            self.returned_cost = Decimal(price) * Decimal(quantity_left)
            inventory_unit_cost = price
        self.inventory.recalculate_balances(year=inventory_returned.returned.period.period_year)
        # self.update_inventory_stock(total_returned_qty, inventory_unit_cost)

    def update_inventory_stock(self, quantity, unit_cost):
        pass
        # history = RegisterInventoryHistory(inventory=self.inventory, price=unit_cost, quantity=quantity)
        # history.create()

    # noinspection PyMethodMayBeStatic
    def get_available_received_items(self, branch_inventory):
        total_received = 0
        available_received_items = []
        for _inventory_rec in branch_inventory.inventory_received.all().order_by('created_at'):
            total_received += _inventory_rec.quantity
            received_returned = 0
            # available_received = AvailableReceived(_inventory_rec)
            # available_received.execute()
            # if available_received.available_quantity > 0:
            #     available_received_items.append(available_received)
            received_returned_items = _inventory_rec.returned_items.filter(inventory_returned__isnull=False,
                                                                           price__isnull=False).all()
            logger.info(
                f"Inventory RECEIVED --- {_inventory_rec}({_inventory_rec.id}) is of quantity {_inventory_rec.quantity} and returned {received_returned_items.count()} records ")
            for rec_ret in received_returned_items:
                received_returned += rec_ret.inventory_returned.quantity
                logger.info(f"Returned to {rec_ret.inventory_returned.quantity} from  {rec_ret.quantity}")
            available_quantity = _inventory_rec.quantity - received_returned
            logger.info(f"Total returned for the receive is {received_returned} now available {available_quantity}")
            if available_quantity > 0:
                available_received_items.append({'received': _inventory_rec, 'in_hand': available_quantity,
                                                 'received_items': received_returned_items})
        return available_received_items

    @property
    def total_returned_cost(self):
        return self.returned_cost


class AvailableInventory:

    def __init__(self, inventory):
        logger.info(f"-----Execute AvailableInventory inventory {inventory}({inventory.id})")
        self.inventory = inventory

    def get_received_still_available(self):
        logger.info("-----get_received_still_available-------")
        return InventoryReceivedReturned.objects.get_available_items(self.inventory)

    def execute(self):
        logger.info("-----execute AvailableInventory check-------")
        received_not_returned = self.get_received_still_available()
        available_received_items = []
        for received_not_returned in received_not_returned:
            logger.info(f"Received Qty = {received_not_returned.quantity} - Returned Qty = {received_not_returned.quantity}, available {received_not_returned.quantity}")
            available_received_items.append({'received': received_not_returned.inventory_received,
                                             'available': received_not_returned.quantity})
        logger.info("Available items for ")
        logger.info(available_received_items)
        return available_received_items


class ReceiveReturnedInventory:
    """
    If we have returned inventory but not matched to any received inventory, then when we receive inventory,
    first match all those returned(negative stock), to the received inventory
    """
    def __init__(self, inventory, quantity, price):
        self.inventory = inventory
        self.quantity = float(quantity)
        self.price = price

    def get_returned_inventory(self):
        returned_inventory = InventoryReceivedReturned.objects.filter(
            inventory_received__isnull=True,
            inventory_returned__inventory=self.inventory
        )
        return returned_inventory

    def execute(self):
        returned_inventory = self.get_returned_inventory()
        if returned_inventory.count() > 0:
            quantity_outstanding = self.quantity
            received_quantity = self.quantity

            for received_returned in returned_inventory:
                if received_quantity > 0 and received_returned.quantity:
                    returned_quantity = float(received_returned.quantity)
                    if received_quantity >= returned_quantity:
                        received_returned.inventory_received = self
                        received_returned.actual_price = self.price
                        received_returned.inventory = self.inventory
                        received_returned.save()
                        received_quantity -= returned_quantity
                        quantity_outstanding -= returned_quantity
                    else:
                        received_returned.inventory_received = self
                        received_returned.actual_price = self.price
                        received_returned.inventory = self.inventory
                        received_returned.save()
                        outstanding_quantity = returned_quantity - received_quantity
                        received_quantity = 0
                        quantity_outstanding -= 0

                        if outstanding_quantity > 0:
                            InventoryReceivedReturned.objects.create(
                                quantity=outstanding_quantity,
                                inventory_returned=received_returned.inventory_returned,
                                price=received_returned.price,
                                inventory=self.inventory
                            )


class AvailabilityOfInventory(object):

    def __init__(self, inventory):
        self.inventory = inventory
        self.total_received = 0
        self.in_hand = 0

    def get_available_received_items(self):
        received_items = InventoryReceived.objects.prefetch_related(
            'returned_items'
        ).annotate(total_returned=Sum('returned_items__quantity')).filter(
            inventory=self.inventory
        )
        return [received_item
                for received_item in received_items if received_item.total_returned < received_item.quantity
                ]

    def populate_available(self):
        received_items = self.get_available_received_items()


class AvailableReceived(object):

    def __init__(self, received):
        self.received = received
        self.available_quantity = 0

    def get_returned_items(self):
        total_returned = 0
        for returned_item in self.received.returned_items:
            total_returned += float(returned_item.quantity)
        return total_returned

    def execute(self):
        total_returned = self.get_returned_items()
        self.available_quantity = self.received.quantity - total_returned


class CreateReturnedInventory:

    def __init__(self, returned_transaction, inventory, quantity, is_returned=True, invoice_item=None):
        self.returned = returned_transaction
        self.inventory = inventory
        self.quantity = quantity
        self.is_returned = is_returned
        self.invoice_item = invoice_item
        logger.info(f"------------------CreateReturnedInventory---------------- for {self.inventory}.")

    def get_quantity(self):
        if self.quantity < 0:
            return self.quantity
        if self.is_returned:
            self.quantity = self.quantity * -1
        return self.quantity

    def execute(self):
        logger.info(f"--------Execute - CreateReturnedInventory {self.inventory} -> {self.quantity} ---")
        quantity = self.get_quantity()
        inventory = BranchInventory.objects.get(id=self.inventory.id)
        inventory_adjustment = FifoInventoryAdjustment(inventory, quantity)
        inventory_returned = InventoryReturned.objects.create(
            returned=self.returned,
            inventory=self.inventory,
            quantity=quantity,
            invoice_item=self.invoice_item
        )
        content_type = ContentType.objects.get_for_model(self.invoice_item)
        inventory_adjustment.update_received_inventory(inventory_returned, self.invoice_item, content_type)
        return inventory_adjustment


class ReceiveSoldInventoryItem(object):

    def __init__(self, transaction, invoice_item, sold_invoice_item):
        self.transaction = transaction
        self.invoice_item = invoice_item
        self.sold_invoice_item = sold_invoice_item
        self.ledger_lines = []
        self.received_returned_cost = Decimal(0)

    def create_inventory_received(self):
        return InventoryReceived.objects.create(
            received=self.transaction,
            inventory=self.invoice_item.inventory,
            quantity=self.invoice_item.quantity * -1 if self.invoice_item.quantity < 0 else self.invoice_item.quantity,
            price_excluding=self.invoice_item.price,
            invoice_item=self.invoice_item
        )

    def execute(self):
        returned_inventory = InventoryReturned.objects.filter(invoice_item=self.sold_invoice_item)
        logger.info(f"Returned inventory {returned_inventory} --> {returned_inventory.count()}")
        logger.info(
            f"Inventory item sold is {self.sold_invoice_item} --> ({self.sold_invoice_item.id}) linked to inventory returned {self.sold_invoice_item.inventory_sold.count()}")
        inventory_received = self.create_inventory_received()
        if inventory_received:
            quantity, total_cost = self.receive_returned_invoice_inventory(
                inventory=self.invoice_item.inventory,
                inventory_received=inventory_received,
                inventory_returned=returned_inventory
            )
            average_price = total_cost / quantity if quantity > 0 else 0

            self.invoice_item.inventory.recalculate_balances()

    def receive_returned_invoice_inventory(self, inventory, inventory_received, inventory_returned):
        logger.info(f"Receive {self.invoice_item.quantity} specifically those returned/sold items {inventory_returned}")
        inventory_stock = inventory.in_stock if inventory.in_stock else Decimal(0)
        unit_cost = Decimal(0)
        quantity_received = self.invoice_item.quantity * -1
        total_received_qty = self.invoice_item.quantity * -1
        for sold_inventory in inventory_returned.all():
            logger.info(
                f"Sold inventory item being returned {sold_inventory} has fifo items {sold_inventory.return_received_items.all()}")
            for sold_item in sold_inventory.return_received_items.all():
                quantity = sold_item.quantity
                logger.info(f"Quantity received returned {quantity} > {total_received_qty}")
                if quantity >= quantity_received:
                    opening_balance = inventory_stock
                    logger.info(
                        f"We have enough items we returned, so we receive all {quantity_received} @ {sold_item.price}")
                    # self.create_inventory_received(inventory, quantity_received, sold_item.price)
                    InventoryReceivedReturned.objects.create(
                        inventory_received=inventory_received,
                        inventory=inventory_received.inventory,
                        received_type=ContentType.objects.get_for_model(self.invoice_item),
                        received_object_id=self.invoice_item.id,
                        quantity=quantity_received,
                        price=sold_item.price,
                        data=utils.prepare_opening_closing_balance(inventory_received, inventory_stock,
                                                                   quantity_received),
                        opening_balance=opening_balance
                    )
                    inventory_stock += quantity_received
                    self.received_returned_cost += quantity_received * sold_item.price
                    self.ledger_lines.append({'quantity': quantity_received, 'price': sold_item.price})
                    quantity_received = 0
                    unit_cost += sold_item.price
                else:
                    opening_balance = inventory_stock
                    logger.info(f"Sold {quantity} but we receiving back {quantity_received} @ {sold_item.price}")
                    InventoryReceivedReturned.objects.create(
                        inventory_received=inventory_received,
                        inventory=inventory_received.inventory,
                        received_type=ContentType.objects.get_for_model(self.invoice_item),
                        received_object_id=self.invoice_item.id,
                        quantity=quantity,
                        price=sold_item.price,
                        data=utils.prepare_opening_closing_balance(inventory_received, inventory_stock, quantity),
                        opening_balance=opening_balance
                    )
                    inventory_stock += quantity
                    # self.create_inventory_received(inventory, quantity, sold_item.price)
                    quantity_received -= quantity
                    self.received_returned_cost = quantity * sold_item.price
                    self.ledger_lines.append({'quantity': quantity, 'price': sold_item.price})
                    unit_cost += sold_item.price
        return inventory_stock, unit_cost


class RegisterInventoryHistory(object):

    def __init__(self, inventory, transaction=None, price=None, quantity=None, on_reserve=None, on_delivery=None,
                 date=None, use_ledger_qty=False):
        logger.info(f"----------RegisterInventory ------ {inventory}--------{quantity}")
        self.inventory = inventory
        self.transaction = transaction
        self.price = price
        self.quantity = quantity
        self.on_reserve = on_reserve
        self.on_delivery = on_delivery
        self.use_ledger_qty = use_ledger_qty

    def create(self):
        history = PurchaseHistory.objects.filter(inventory=self.inventory).first()
        if not history:
            logger.info("Create new history")
            price_paid = {}
            if self.price:
                price_paid = {'price': [float(self.price)]}
            history = PurchaseHistory.objects.create(
                inventory=self.inventory,
                price_paid=price_paid
            )
        logger.info("Update history")
        if self.price and isinstance(history.price_paid, dict):
            current_prices = history.price_paid.get('price', [])
            price_history = self.create_price_history(current_prices, self.price)
            history.price_paid = {'price': price_history}
        if self.transaction and hasattr(self.transaction, 'supplier'):
            logger.info(f"Transaction has supplier {self.transaction.supplier}")
            history.supplier = self.transaction.supplier
        if self.quantity:
            logger.info(f"Transaction quantity {self.quantity} and current in stock is {history.in_stock}")
            if history.in_stock:
                history.in_stock += Decimal(self.quantity)
            else:
                history.in_stock = Decimal(self.quantity)
            logger.info(f"After quantity {self.quantity} and current in stock = {history.in_stock}")
        history.last_price_update = datetime.now()

        average_price, ledger_quantity = self.calculate_average_prices()
        logger.info(f"Ledger:: Average price is {average_price} and quantity is {ledger_quantity}")
        if average_price:
            history.average_price = average_price

        if self.use_ledger_qty and ledger_quantity:
            history.in_stock = Decimal(round(ledger_quantity, 2))
        logger.info(f"After All current in stock = {history.in_stock}")
        history.save()
        # calculate_inventory_stock.delay(self.inventory.id)

    def create_price_history(self, historical_prices, price):
        prices = [float(price)]
        if historical_prices:
            prices += historical_prices
        if len(prices) > 4:
            prices = prices[0:3]
        return prices

    def calculate_average_prices(self):
        calculator = InventoryAveragePriceCalculator(self.inventory)
        average_price = calculator.execute()
        return average_price, calculator.quantity


class InventoryQuantityCalculator(object):

    def __init__(self, inventory):
        logger.info("---------- InventoryAveragePriceCalculator ------------ ".format(inventory))
        self.inventory = inventory
        self.quantity = 0
        self.price_total = 0

    def opening_balances_total(self):
        opening_balances = InventoryReceived.objects.filter(
            received__branch=self.inventory.branch,
            inventory=self.inventory,
            received__movement_type=MovementType.OPENING_BALANCE
        )
        for opening_balance in opening_balances.all():
            total_price = 0
            unit_price = 0
            if opening_balance.price_excluding:
                unit_price = opening_balance.price_excluding
            if unit_price and opening_balance.quantity:
                total_price = unit_price * opening_balance.quantity
            logger.info("Total price in opening balance is {}".format(total_price))
            logger.info("Total quantity in opening balance is {}".format(opening_balance.quantity))
            self.price_total += float(total_price)
            if opening_balance.quantity:
                self.quantity += float(opening_balance.quantity)

    def ledgers_total(self):
        # TODO - Calculate average price in database using a query anf aggregates
        for ledger in self.inventory.ledgers.all():
            if ledger.total_value:
                self.price_total += float(ledger.total_value)
            if ledger.quantity:
                self.quantity += float(ledger.quantity)

    def execute(self):
        logger.info(f"Calculating average price for inventory {self.inventory}")
        avg = 0
        self.opening_balances_total()
        self.ledgers_total()
        logger.info(f"total is {self.price_total} vs counter {self.quantity}")
        if self.quantity > 0:
            avg = self.price_total / self.quantity
        return round(avg, 2)


class InventoryAveragePriceCalculator(object):

    def __init__(self, inventory):
        logger.info("---------- InventoryAveragePriceCalculator ------------ ".format(inventory))
        self.inventory = inventory
        self.price_total = Decimal(0)
        self.quantity = Decimal(0)

    def opening_balances_total(self):
        opening_balances = InventoryReceived.objects.filter(
            received__branch=self.inventory.branch, inventory=self.inventory,
            received__movement_type=MovementType.OPENING_BALANCE
        )
        for opening_balance in opening_balances.all():
            total_price = Decimal(0)
            unit_price = Decimal(0)
            if opening_balance.price_excluding:
                unit_price = opening_balance.price_excluding
            if unit_price and opening_balance.quantity:
                total_price = unit_price * opening_balance.quantity
            logger.info("Total price in opening balance is {}".format(total_price))
            logger.info("Total quantity in opening balance is {}".format(opening_balance.quantity))
            self.price_total += total_price
            if opening_balance.quantity:
                self.quantity += opening_balance.quantity

    def ledgers_total(self):
        for ledger in self.inventory.ledgers.all():
            if ledger.total_value:
                self.price_total += ledger.total_value
            if ledger.quantity:
                self.quantity += ledger.quantity

    def execute(self):
        logger.info("Calculating average price for inventory {}".format(self.inventory))
        avg = 0
        self.opening_balances_total()
        self.ledgers_total()
        logger.info("total is {} vs counter {}".format(self.price_total, self.quantity))
        if self.quantity > 0:
            avg = self.price_total / self.quantity
        return round(avg, 2)


class CopyInventory(object):
    def __init__(self, inventory: BranchInventory, branch: Branch):
        self.inventory = inventory
        self.branch = branch

    def get_inventory_values(self):
        inventory_values = BranchInventory.objects.values().filter(id=self.inventory.id).first()
        data = {
            k: val for k, val in inventory_values.items() if k not in ['id', 'branch_id']
        }
        return data

    def execute(self):
        inventory_values = self.get_inventory_values()
        branch_inventory = BranchInventory(**inventory_values)
        branch_inventory.parent_branch = self.inventory.branch
        branch_inventory.branch = self.branch
        branch_inventory.save()
        return branch_inventory


class ProcessImport(object):

    def __init__(self, company, year, branch, profile, role, period, date, inventories_data):
        self.company = company
        self.branch = branch
        self.profile = profile
        self.role = role
        self.inventories_data = inventories_data
        self.year = year
        self.period = period
        self.date = date

    def get_company_accounts(self, company):
        return company.company_accounts.all()

    def get_units(self):
        return Unit.objects.filter(measure__company=self.company)

    def get_measures(self):
        return None
        # return Measure.objects.filter(measure__company=self.get_company())

    def get_master_inventory(self, company, inventory_line):
        inventory_code = inventory_line.get('code', None)
        inventory_name = inventory_line.get('name', None)
        if inventory_code:
            inventory = company.inventory_items.filter(code__icontains=inventory_code).first()
            if inventory:
                inventory.description = inventory_name
                inventory.save()
            else:
                inventory = Inventory.objects.create(code=inventory_code, description=inventory_name, company=company)
            return inventory
        return None

    def get_inventory_branch(self, company, branch_name):
        branch = company.branches.filter(label__icontains=branch_name).first()
        if not branch:
            branch = Branch.objects.create(label=branch_name, company=company)
        return branch

    def get_branch_inventory(self, branch, master_inventory, inventory_line):
        description = inventory_line.get('branch_inventory_name', None)
        if description:
            inventory = BranchInventory.objects.filter(
                branch=branch, inventory=master_inventory
            ).first()

            if inventory:
                inventory.description = description
                inventory.save()
                return inventory, False
            else:
                inventory = BranchInventory.objects.create(
                    description=f"{master_inventory.code} - {description}",
                    inventory=master_inventory,
                    branch=branch
                )
                return inventory, True
        return None, False

    def get_measure(self, company, measure_name):
        measure = company.measures.filter(name__icontains=measure_name).first()
        if not measure:
            measure = Measure.objects.create(name=measure_name, company=company)
        return measure

    def get_unit(self, measure, unit_name):
        unit = Unit.objects.filter(name__icontains=unit_name, measure=measure).first()

        unit_slug = slugify(str(unit_name).lower())
        if unit:
            unit.unit = unit_slug
            unit.save()
        else:
            unit = Unit.objects.create(name=unit_name, measure=measure, unit=unit_slug)
        return unit

    def get_account(self, company, account_code):
        account = company.company_accounts.filter(code__icontains=account_code).first()
        if not account:
            raise CompanyAccountCodeException(f'Account code {account_code} not found, please add under accounts')
        return account

    def get_group_level(self, company, group_level_name):
        group_level = GroupLevel.objects.filter(name__icontains=group_level_name, company=company).first()
        if not group_level:
            group_level = GroupLevel.objects.create(company=company, name=group_level_name)
        return group_level

    def get_group_level_item(self, company, group_level, group_level_name):
        group_level_item = GroupLevelItem.objects.filter(name__icontains=group_level_name,
                                                         group_level__company=company).first()
        if not group_level_item:
            group_level_item = GroupLevelItem.objects.create(group_level=group_level, name=group_level_name)
        return group_level_item

    def get_sku(self, sku):
        _sku = str(sku).lower()
        if _sku == 'sku':
            return BranchInventory.SKU
        elif _sku == 'service':
            return BranchInventory.SERVICE
        elif _sku == 'not-sku':
            return BranchInventory.NOT_SKU
        return None

    def get_money(self, value):
        try:
            _value = str(value).replace(',', '.')
            return Decimal(float(_value))
        except:
            return Decimal('0')

    def get_vat_code(self, company, vat_code):
        vat_code = company.company_vat_codes.filter(label__icontains=vat_code).first()
        if not vat_code:
            raise CompanyAccountCodeException(f'Vat code {vat_code} not found, please add under vat')
        return vat_code

    def get_current_period(self, year):
        return Period.objects.open(year.company, year).order_by('-period').first()

    def create_received_transaction(self, period):
        return Received.objects.create(
            movement_type=MovementType.OPENING_BALANCE,
            branch=self.branch,
            period=period,
            date=datetime.now().date(),
            note='Imported',
            profile=self.profile,
            role=self.role
        )

    def add_group_levels(self, branch_inventory, inventory_line, group_levels):
        branch_inventory.group_level_items.clear()
        group_level_items = inventory_line.get('group_level_items', None)
        if group_level_items:
            for group_level in group_levels:
                for group_level_item in group_level_items:
                    level_group_level_item = group_level_item.get(group_level.id, None)
                    if level_group_level_item:
                        group_level_item = self.get_group_level_item(self.company, group_level,
                                                                     level_group_level_item)
                        if group_level_item:
                            branch_inventory.group_level_items.add(group_level_item)

    def save_opening_balance(self, branch_inventory, received, period, quantity):
        unit_price = branch_inventory.cost_price
        if received:
            InventoryReceived.objects.create_received_inventory(received, branch_inventory, unit_price, quantity)

    def execute(self):
        count_updated = 0
        count_new = 0
        total = 0
        year = self.year
        current_period = self.get_current_period(year)
        inventories = self.inventories_data.get('inventory', [])
        if len(inventories) <= 0:
            raise NoImportInventory("No inventory items found to import")

        received = self.create_received_transaction(current_period)
        ledger_lines = defaultdict(Decimal)
        group_levels = GroupLevel.objects.filter(company=self.company)
        account_journal_lines = defaultdict(Decimal)
        for inventory_line in inventories:
            master_inventory = self.get_master_inventory(self.company, inventory_line)
            if master_inventory:
                branch_name = inventory_line.get('branch', None)
                branch = self.get_inventory_branch(self.company, branch_name)
                if branch:
                    branch_inventory, is_new = self.get_branch_inventory(branch, master_inventory, inventory_line)
                    if branch_inventory:
                        count_new += 1 if is_new else 0
                        count_updated += 1 if not is_new else 0

                        measure_name = inventory_line.get('measure', None)
                        unit_name = inventory_line.get('purchase_unit', None)
                        sale_unit_name = inventory_line.get('sale_unit', None)
                        stock_unit_name = inventory_line.get('stock_unit', None)

                        gl_account_code = inventory_line.get('gl_account_code', None)
                        cos_account_code = inventory_line.get('cos_account_code', None)
                        sales_account_code = inventory_line.get('sales_account_code', None)
                        stock_adjustment_code = inventory_line.get('stock_adjustment_account', None)

                        default_cost = inventory_line.get('default_cost', 0)
                        default_sale_cost = inventory_line.get('default_sale_cost', 0)

                        sku = inventory_line.get('sku', None)
                        vat_input = inventory_line.get('vat_input', None)
                        output_vat = inventory_line.get('output_vat', None)
                        is_recipe = inventory_line.get('is_recipe', '')
                        price_type = inventory_line.get('price_type', 'average').lower()

                        package_size = {'l': inventory_line.get('l', None),
                                        'b': inventory_line.get('b', None),
                                        'h': inventory_line.get('h', None)
                                        }
                        sale_package_size = {'l': inventory_line.get('sale_l', None),
                                             'b': inventory_line.get('sale_b', None),
                                             'h': inventory_line.get('sale_h', None)
                                             }

                        opening_balance_quantity = inventory_line.get('in_stock')
                        net_weight = inventory_line.get('net_weight', 0)
                        gross_weight = inventory_line.get('gross_weight', '')

                        delivery_fee = inventory_line.get('delivery_fee', 0)
                        environment_fee = inventory_line.get('environment_fee', 0)

                        sale_delivery_fee = inventory_line.get('sale_delivery_fee', 0)
                        sale_environment_fee = inventory_line.get('sale_environment_fee', 0)

                        if measure_name:
                            measure = self.get_measure(self.company, measure_name)
                            if measure:
                                branch_inventory.measure = measure
                                if unit_name:
                                    branch_inventory.unit = self.get_unit(measure, unit_name)
                                if sale_unit_name:
                                    branch_inventory.sale_unit = self.get_unit(measure, sale_unit_name)
                                if stock_unit_name:
                                    branch_inventory.stock_unit = self.get_unit(measure, stock_unit_name)
                        if gl_account_code:
                            branch_inventory.gl_account = self.get_account(self.company, gl_account_code)
                        if cos_account_code:
                            branch_inventory.cost_of_sales_account = self.get_account(self.company, cos_account_code)
                        if sales_account_code:
                            branch_inventory.sales_account = self.get_account(self.company, sales_account_code)
                        if stock_adjustment_code:
                            branch_inventory.stock_adjustment_account = self.get_account(self.company,
                                                                                         stock_adjustment_code)
                        if sku:
                            branch_inventory.sku = self.get_sku(sku)
                        if default_cost is not None:
                            branch_inventory.cost_price = self.get_money(default_cost)
                        if default_sale_cost is not None:
                            branch_inventory.selling_price = self.get_money(default_sale_cost)
                        if vat_input:
                            branch_inventory.vat_code = self.get_vat_code(self.company, vat_input)
                        if vat_input:
                            branch_inventory.sale_vat_code = self.get_vat_code(self.company, output_vat)
                        if is_recipe == 'X':
                            branch_inventory.is_recipe_item = True
                        if price_type == 'last':
                            branch_inventory.price_type = BranchInventory.LAST_PRICE
                        else:
                            branch_inventory.price_type = BranchInventory.AVERAGE
                        if net_weight:
                            branch_inventory.weight = self.get_money(net_weight)
                        if gross_weight:
                            branch_inventory.gross_weight = self.get_money(gross_weight)

                        if package_size:
                            branch_inventory.package_size = package_size
                        if sale_package_size:
                            branch_inventory.sale_package_size = sale_package_size

                        if delivery_fee is not None:
                            branch_inventory.delivery_fee = delivery_fee
                        if sale_delivery_fee is not None:
                            branch_inventory.sale_delivery_fee = sale_delivery_fee
                        if environment_fee is not None:
                            branch_inventory.environment_fee = environment_fee
                        if sale_environment_fee is not None:
                            branch_inventory.sale_environment_fee = sale_environment_fee
                        branch_inventory.is_blocked = False
                        branch_inventory.save()
                        total += 1

                        self.add_group_levels(branch_inventory, inventory_line, group_levels)
                        qty = 0
                        if opening_balance_quantity:
                            _opening_balance_quantity = self.get_money(opening_balance_quantity)
                            qty = float(_opening_balance_quantity)
                            self.save_opening_balance(branch_inventory, received, current_period,
                                                      _opening_balance_quantity)
                        if qty > 0:
                            ledger_lines[branch_inventory] += Decimal(qty)

                        if branch_inventory.gl_account:
                            account_journal_lines[branch_inventory.gl_account] += (
                                        branch_inventory.cost_price * Decimal(qty))

                        branch_inventory.recalculate_balances()

        if len(account_journal_lines) > 0:
            self.create_journal_entries(account_journal_lines, received)
        return {'new': count_new, 'updated': count_updated, 'total': total}

    def get_receive_content_type(self):
        return ContentType.objects.filter(app_label='inventory', model='received').first()

    def create_ledgers(self, ledger_lines, received, journal):

        bulk_ledger_items = []
        content_type = self.get_receive_content_type()
        for inventory, quantity in ledger_lines.items():
            ledger_line = Ledger(**{'inventory': inventory,
                                    'quantity': quantity,
                                    'value': inventory.cost_price,
                                    'created_by': self.profile,
                                    'period': self.period,
                                    'date': self.date,
                                    'text': 'Opening Balance',
                                    'status': Ledger.COMPLETE,
                                    'content_type': content_type,
                                    'object_id': received.id,
                                    'journal': journal
                                    })
            logger.info(ledger_line)
            bulk_ledger_items.append(ledger_line)
        Ledger.objects.bulk_create(bulk_ledger_items)
        logger.info("------------------DONE CREATE BULK LEDGERS-----------------------")

    def create_journal_entries(self, account_journal_lines, received):
        company = received.branch.company
        stock_adjustment_account = company.stock_adjustment_account
        if not company.stock_adjustment_account:
            raise JournalAccountUnavailable('Default stock adjustment Account not available, please fix.')
        content_type = self.get_receive_content_type()
        journal = Journal.objects.create(
            company=company,
            year=self.year,
            period=received.period,
            journal_text=f"Opening Balance - {str(received)}",
            module=Module.INVENTORY,
            role=received.role,
            date=received.date,
            created_by=received.profile
        )
        if journal:
            total_amount = 0
            for account, total in account_journal_lines.items():
                JournalLine.objects.create(
                    journal=journal,
                    account=account,
                    debit=total,
                    is_reversal=False,
                    text=f"Opening Balanced - {str(received)}",
                    object_id=received.id,
                    content_type=content_type,
                    accounting_date=received.date
                )
                total_amount += total

            JournalLine.objects.create(
                journal=journal,
                account=stock_adjustment_account,
                credit=total_amount,
                is_reversal=False,
                text=f"Opening Balance - {str(received)}",
                object_id=received.id,
                content_type=content_type,
                accounting_date=received.date
            )
        return journal


class ReceiveReturnedInventoryItem(object):

    def __init__(self, content_type, received_object_item, sold_object_item, year):
        self.content_type = content_type
        self.received_object_item = received_object_item
        self.sold_object_item = sold_object_item
        self.ledger_lines = []
        self.received_returned_cost = Decimal(0)
        self.inventory_total = 0
        self.year = year

    def execute(self):
        inventory = self.received_object_item.inventory
        if self.sold_object_item:
            returned_inventory = InventoryReceivedReturned.objects.filter(
                returned_type=self.content_type,
                returned_object_id=self.sold_object_item.pk
            )

            self.receive_returned_invoice_inventory(
                inventory=inventory,
                inventory_returned_qs=returned_inventory
            )
        else:
            quantity_received = self.received_object_item.quantity * -1
            price = self.received_object_item.price
            returned_inventory = InventoryReceivedReturned.objects.create(
                inventory=inventory,
                received_type=ContentType.objects.get_for_model(self.received_object_item),
                received_object_id=self.received_object_item.id,
                quantity=quantity_received,
                price=price,
                data=utils.prepare_opening_closing_balance(
                    cls=self.received_object_item,
                    opening_stock=0,
                    quantity=quantity_received
                ),
                opening_balance=0,
                received_value=quantity_received * price

            )
            self.received_returned_cost += returned_inventory.received_value
            self.inventory_total += returned_inventory.received_value
            self.ledger_lines.append(returned_inventory)
        self.received_object_item.inventory.recalculate_balances(
            year=self.year
        )

    def receive_returned_invoice_inventory(self, inventory: BranchInventory, inventory_returned_qs):
        inventory_stock = inventory.in_stock if inventory.in_stock else Decimal(0)
        unit_cost = Decimal(0)
        quantity_received = self.received_object_item.quantity * -1
        for sold_item in inventory_returned_qs.all():
            quantity = sold_item.quantity
            if quantity >= quantity_received:
                opening_balance = inventory_stock
                received_returned = InventoryReceivedReturned.objects.create(
                    inventory=inventory,
                    received_type=ContentType.objects.get_for_model(self.received_object_item),
                    received_object_id=self.received_object_item.id,
                    quantity=quantity_received,
                    price=sold_item.price,
                    data=utils.prepare_opening_closing_balance(self.received_object_item, inventory_stock,
                                                               quantity_received),
                    opening_balance=opening_balance,
                    received_value=quantity_received * sold_item.price
                )
                inventory_stock += quantity_received
                self.received_returned_cost += received_returned.received_value
                self.inventory_total += received_returned.received_value
                self.ledger_lines.append(received_returned)
                quantity_received = 0
                unit_cost += sold_item.price
            else:
                opening_balance = inventory_stock
                received_returned = InventoryReceivedReturned.objects.create(
                    inventory=self.received_object_item.inventory,
                    received_type=ContentType.objects.get_for_model(self.received_object_item),
                    received_object_id=self.received_object_item.id,
                    quantity=quantity,
                    price=sold_item.price,
                    data=utils.prepare_opening_closing_balance(self.received_object_item, inventory_stock, quantity),
                    opening_balance=opening_balance,
                    received_value=quantity * sold_item.price
                )
                inventory_stock += quantity
                quantity_received -= quantity
                self.received_returned_cost = received_returned.received_value
                self.inventory_total += received_returned.received_value
                self.ledger_lines.append(received_returned)
                unit_cost += sold_item.price
        return inventory_stock, unit_cost


class FiFoReceived(object):

    def __init__(self, inventory: BranchInventory, quantity, unit_price, content_object, content_type):
        logger.info("---------------FiFoReceived----------------")
        self.inventory = inventory
        self.quantity = quantity
        self.unit_price = unit_price
        self.content_object = content_object
        self.content_type = content_type
        self.inventory_ledger_lines = []
        self.inventory_total = 0
        self.adjustment_total = 0
        self.ledger_lines = []

    def get_returned_inventory(self):
        return selectors.get_returned_not_received(self.inventory)

    def calculate_price_different(self, price):
        return self.unit_price - price

    def execute(self):
        """
        Handle inventory that was returned without any received, resulting in negative inventory, so when we receive,
        we must balance this negative inventory first
        """
        # logger.info("---------------EXECUTE - FiFoReceived----------------")
        returned_inventory = self.get_returned_inventory()
        received_quantity = self.quantity
        quantity_outstanding = self.quantity

        for received_returned in returned_inventory:
            if received_quantity > 0 and received_returned.quantity:
                returned_quantity = received_returned.quantity
                if received_quantity >= returned_quantity:
                    if isinstance(self.content_object, InventoryReceived):
                        received_returned.inventory_received = self.content_object
                    received_returned.actual_price = self.unit_price
                    received_returned.received_object_id = self.content_object.id
                    received_returned.received_type = self.content_type
                    received_returned.save()

                    self.inventory_total += received_returned.price * received_returned.quantity
                    self.inventory_ledger_lines.append({
                        'inventory': self.inventory,
                        'price': received_returned.price,
                        'quantity': received_returned.quantity,
                        'is_adjustment': False
                    })
                    self.ledger_lines.append(received_returned)
                    received_quantity -= returned_quantity
                    quantity_outstanding -= returned_quantity

                    self.add_ledger_line(received_returned, received_returned.quantity)
                else:
                    received_returned = self.update_received_returned(
                        received_returned=received_returned,
                        quantity=received_quantity
                    )
                    outstanding_quantity = returned_quantity - received_quantity
                    received_quantity = 0
                    quantity_outstanding = 0

                    if outstanding_quantity > 0:
                        # From what we received, we could not full-fill the returned/negative stock, lets create
                        # new returned line
                        self.create_returned_not_received_inventory(outstanding_quantity, received_returned)

        if received_quantity > 0:
            self.create_received_not_returned(received_quantity)

    # noinspection PyMethodMayBeStatic
    def create_returned_not_received_inventory(self, quantity, received_returned):
        """
        Not received all, so we now created a returned not received quantity of {quantity}")
        """
        returned_value = Decimal(quantity * received_returned.price).quantize(Decimal('0.01'))
        return InventoryReceivedReturned.objects.create(
            quantity=quantity,
            inventory_returned=received_returned.inventory_returned if received_returned.inventory_returned else None,
            inventory=received_returned.inventory,
            returned_object_id=received_returned.inventory_returned.id,
            returned_type=ContentType.objects.get_for_model(received_returned.inventory_returned),
            price=received_returned.price,
            returned_value=returned_value
        )

    def create_received_not_returned(self, quantity):
        received_value = Decimal(self.unit_price * quantity).quantize(Decimal('0.01'))
        received_returned = InventoryReceivedReturned.objects.create(
            inventory=self.inventory,
            inventory_received=self.content_object if isinstance(self.content_object, InventoryReceived) else None,
            received_object_id=self.content_object.id,
            received_type=self.content_type,
            quantity=quantity,
            price=self.unit_price,
            received_value=received_value,
            data=utils.prepare_opening_closing_balance(
                self.content_object, self.inventory.in_stock, quantity
            )
        )
        self.inventory_ledger_lines.append({
            'inventory': self.inventory, 'price': self.unit_price, 'quantity': quantity, 'is_adjustment': False
        })
        self.ledger_lines.append(received_returned)
        self.inventory_total += received_value

    def update_received_returned(self, received_returned: InventoryReceivedReturned, quantity):
        received_value = Decimal(quantity * self.unit_price).quantize(Decimal('0.01'))
        if isinstance(self.content_object, InventoryReceived):
            received_returned.inventory_received = self.content_object
        received_returned.received_value = received_value
        received_returned.actual_price = self.unit_price
        received_returned.received_object_id = self.content_object.id
        received_returned.received_type = self.content_type
        received_returned.save()

        self.inventory_total += received_returned.price * quantity
        self.inventory_ledger_lines.append({
            'inventory': self.inventory,
            'price': received_returned.price,
            'quantity': quantity
        })
        self.add_ledger_line(received_returned=received_returned, quantity=quantity)
        self.ledger_lines.append(received_returned)

        return received_returned

    def add_ledger_line(self, received_returned: InventoryReceivedReturned, quantity: Decimal):
        """
        There was a different in selling and buying price {received_returned.price} vs
        {self.unit_price} = {price_diff} -> {quantity} items
        """
        price_diff = self.calculate_price_different(received_returned.price)

        if price_diff != 0:
            self.adjustment_total += price_diff * quantity


class FiFoReturned(object):

    def __init__(self, inventory: BranchInventory, quantity: Union[int, Decimal], unit_price: Decimal,
                 content_object: Union[InvoiceItem, ManufacturingRecipe, StockAdjustmentInventory]):
        self.inventory = inventory
        self.quantity = quantity
        self.unit_price = unit_price
        self.content_object = content_object
        self.inventory_ledger_lines = []
        self.inventory_total = 0
        self.returned_cost = 0
        self.adjustment_total = 0
        self.ledger_lines = []
        self.returned_lines = []

    def calculate_price_different(self, price):
        return self.unit_price - price

    def execute(self):
        """
         We return what we received first, if we return what we have not received, inventory goes into negative stock
        :return:
        """
        received_inventory_items = selectors.get_received_not_returned(self.inventory)
        inventory_stock = inventory_quantity = self.inventory.in_stock
        quantity = self.quantity
        total_quantity = 0
        inventory_unit_cost = 0
        total_returned_qty = 0
        for inventory_received_item in received_inventory_items:
            if quantity > 0:
                if inventory_received_item.quantity > quantity:
                    opening_balance = inventory_quantity

                    returned_line = self.return_less(inventory_received_item, quantity, opening_balance)

                    inventory_quantity -= quantity
                    inventory_stock -= quantity
                    total_returned_qty -= quantity
                    self.inventory_total += returned_line.returned_value
                    total_quantity -= quantity
                    quantity = 0
                    self.returned_lines.append(returned_line)
                    inventory_unit_cost = inventory_received_item.price
                else:
                    quantity = quantity - inventory_received_item.quantity
                    opening_balance = inventory_quantity
                    inventory_quantity -= inventory_received_item.quantity
                    total_quantity -= inventory_quantity
                    total_returned_qty -= inventory_received_item.quantity

                    self.complete_return(inventory_received_item, inventory_stock)

                    inventory_stock -= inventory_received_item.quantity
                    self.returned_lines.append(inventory_received_item)
                    self.inventory_total += inventory_received_item.returned_value if inventory_received_item.returned_value else 0
                    inventory_unit_cost = inventory_received_item.price

        if quantity > 0 and self.inventory.allow_negative_stocks:
            price = Decimal(0)
            if self.inventory.price_type == BranchInventory.LAST_PRICE:
                if self.inventory.current_price:
                    price = self.inventory.current_price
            elif self.inventory.average_price:
                price = self.inventory.average_price

            quantity_left = quantity - Decimal(inventory_quantity)
            opening_balance = inventory_quantity
            balance = inventory_quantity - quantity
            total_quantity -= inventory_quantity
            total_returned_qty -= quantity

            received_returned = InventoryReceivedReturned.objects.create(
                inventory_returned=self.content_object if isinstance(self.content_object, InventoryReturned) else None,
                inventory=self.inventory,
                returned_type=ContentType.objects.get_for_model(self.content_object),
                returned_object_id=self.content_object.id,
                returned_value=Decimal(price * quantity_left).quantize(Decimal('0.01')),
                quantity=quantity_left,
                price=price,
                balance=balance,
                opening_balance=opening_balance,
                data=utils.prepare_opening_closing_balance(
                    self.content_object, inventory_stock, quantity_left
                )
            )
            self.returned_lines.append(received_returned)
            self.returned_cost = Decimal(price) * Decimal(quantity_left)
            inventory_unit_cost = price

    def return_less(self, inventory_received_item: InventoryReceivedReturned, quantity: Decimal, inventory_stock: Decimal):
        """
           Return less inventory, quantity than we received. The line to be returned\'s quantity is updated
           Create new closed line. This is to ensure the original line remains first priority (FIFO), to be
           returned next time we remove again
           If we doing partial return, we remove the value that's returned, we don't calculate as it caused rounding issues
        """
        outstanding_quantity = inventory_received_item.quantity - quantity
        returned_value = Decimal(quantity * inventory_received_item.price).quantize(Decimal('0.01'))
        returned_line = InventoryReceivedReturned.objects.create(
            inventory_received=inventory_received_item.inventory_received,
            received_object_id=inventory_received_item.received_object_id,
            received_type=inventory_received_item.received_type,
            received_value=returned_value,
            returned_value=returned_value,
            inventory=inventory_received_item.inventory,
            quantity=quantity,
            price=inventory_received_item.price,
            returned_type=ContentType.objects.get_for_model(self.content_object),
            returned_object_id=self.content_object.id,
            data=utils.prepare_opening_closing_balance(inventory_received_item.inventory_received, inventory_stock, quantity)
        )

        # if we doing partial return
        if outstanding_quantity > 0:
            received_value = inventory_received_item.received_value - returned_line.returned_value
            inventory_received_item.quantity = outstanding_quantity
            inventory_received_item.received_value = received_value
            inventory_received_item.data = utils.prepare_opening_closing_balance(inventory_received_item.inventory_received, inventory_stock,
                                                                                 outstanding_quantity)
            inventory_received_item.save()

        inventory_stock -= outstanding_quantity
        return returned_line

    def complete_return(self, inventory_received_item: InventoryReceivedReturned, inventory_stock: Decimal):
        """
            Return all inventory from this line
        """
        inventory_received_item.returned_type = ContentType.objects.get_for_model(self.content_object)
        inventory_received_item.returned_object_id = self.content_object.id
        inventory_received_item.returned_value = inventory_received_item.received_value
        inventory_received_item.data = utils.prepare_opening_closing_balance(
            cls=self.content_object,
            opening_stock=inventory_stock,
            quantity=inventory_received_item.quantity * -1,
            data=inventory_received_item.data
        )
        inventory_received_item.save()
        return inventory_received_item


def get_last_price(branch_inventory: BranchInventory):
    current_price = branch_inventory.current_price
    if not current_price:
        current_price = 0
    return Decimal(current_price)


def calculate_average_price(branch_inventory: BranchInventory):
    average_price = branch_inventory.average_price
    if not average_price:
        average_price = 0
    return Decimal(average_price)


def receive_inventory(inventory: BranchInventory, quantity: Decimal, price: Decimal, year: Year, content_object):
    quantity_outstanding = receive_returned(
        inventory=inventory,
        quantity=quantity,
        price=price,
        content_object=content_object
    )

    if quantity_outstanding > 0:
        InventoryReceivedReturned.objects.create(
            inventory=inventory,
            received_object_id=content_object.id,
            received_type=ContentType.objects.get_for_model(content_object),
            quantity=quantity_outstanding,
            received_value=Decimal(quantity_outstanding * price).quantize(Decimal('0.01')),
            data=utils.prepare_opening_closing_balance(
                content_object, inventory.in_stock, quantity_outstanding
            )
        )
    inventory.recalculate_balances(year=year)


def receive_returned(inventory: BranchInventory, quantity: Decimal, price: Decimal, content_object):
    """
        Handle inventory that was returned with any received, resulting in negative inventory, so when we receive,
        we must balance this negative inventory first
    """
    returned_not_received = InventoryReceivedReturned.objects.returned_not_received(
        inventory=inventory
    )
    received_quantity = quantity
    quantity_outstanding = received_quantity
    for inventory_returned in returned_not_received:
        if received_quantity > 0 and inventory_returned.quantity:
            returned_quantity = inventory_returned.quantity
            if received_quantity >= returned_quantity:
                # received_returned.inventory_received = inventory_received
                inventory_returned.received_object_id = content_object.id
                inventory_returned.received_type = ContentType.objects.get_for_model(content_object)
                inventory_returned.actual_price = price
                inventory_returned.inventory = inventory
                inventory_returned.save()
                received_quantity -= returned_quantity
                quantity_outstanding -= returned_quantity
            else:
                # received_returned.inventory_received = inventory_received
                inventory_returned.received_object_id = content_object.id
                inventory_returned.received_type = ContentType.objects.get_for_model(content_object)
                inventory_returned.actual_price = price
                inventory_returned.inventory = inventory
                inventory_returned.save()
                outstanding_quantity = returned_quantity - received_quantity
                received_quantity = 0
                quantity_outstanding -= 0

                if outstanding_quantity > 0:
                    return_content_type = ContentType.objects.get_for_model(inventory_returned.inventory_returned)
                    InventoryReceivedReturned.objects.create(
                        quantity=outstanding_quantity,
                        # inventory_returned=received_returned.inventory_returned,
                        returned_type=return_content_type,
                        returned_object_id=inventory_returned.inventory_returned.id,
                        price=inventory_returned.price,
                        inventory=inventory_returned.inventory
                    )
    return quantity_outstanding


def receive_object_inventory(inventory: BranchInventory, quantity: Decimal, price: Decimal, content_object):
    quantity_received = quantity
    logger.info(f"Receive {quantity_received} specifically those returned/sold items {content_object}")
    inventory_quantity = inventory.in_stock
    total_received_qty = quantity_received
    received_returned_cost = 0
    returned_lines = []
    for sold_inventory in content_object.all():
        for sold_item in sold_inventory.return_received_items.all():
            quantity = sold_item.quantity
            if quantity >= quantity_received:
                opening_balance = inventory_quantity

                logger.info(
                    f"Sold {quantity} but we receiving back {quantity_received} @ {sold_item.price}, Opening at {opening_balance}->Closing at {inventory_quantity}")
                returned_received = InventoryReceivedReturned.objects.create(
                    received_type=ContentType.objects.get_for_model(content_object),
                    received_object_id=content_object.id,
                    received_value=Decimal(quantity_received * sold_item.price).quantize(Decimal('0.01')),
                    inventory=inventory,
                    inventory_returned=sold_inventory,
                    quantity=quantity_received,
                    price=sold_item.price,
                    opening_balance=opening_balance,
                    data=utils.prepare_opening_closing_balance(sold_inventory, inventory_quantity,
                                                               quantity_received)
                )
                inventory_quantity += quantity_received
                total_received_qty -= quantity_received
                quantity -= quantity_received
                received_returned_cost += quantity_received * sold_item.price
                returned_lines.append(returned_received)
    return returned_lines


def return_inventory(inventory: BranchInventory, quantity: Decimal, year: Year, content_object):
    """
         We return what we received first, if we return what we have not received, inventory goes into negative stock
        :param inventory: BranchInventory
        :param quantity: Decimal
        :param year: Year
        :param content_object: GenericObject - Inventory can be returned by different process, eg Adjustment, Sales or General Return
        :return:
    """
    received_inventories = InventoryReceivedReturned.objects.received_not_returned(inventory=inventory)
    inventory_quantity = inventory.in_stock
    inventory_stock = inventory_quantity
    total_quantity = 0
    inventory_unit_cost = 0
    received_returned_cost = 0
    returned_lines = []
    logger.info(f"Initial Inventory Quantity = {inventory_quantity} working on {quantity}")
    total_returned_qty = 0

    for received_inventory in received_inventories:
        if quantity > 0:
            logger.info(f"Received {received_inventory.quantity} vs {quantity}")
            if received_inventory.quantity > quantity:
                opening_balance = inventory_quantity
                outstanding_quantity = received_inventory.quantity - quantity
                received_inventory.quantity = quantity
                received_inventory.returned_type = ContentType.objects.get_for_model(content_object)
                received_inventory.returned_object_id = content_object.id if content_object else None
                received_inventory.returned_value = Decimal(received_inventory.price * quantity).quantize(Decimal('0.01'))
                received_inventory.data = utils.prepare_opening_closing_balance(
                    cls=content_object,
                    opening_stock=inventory_quantity,
                    quantity=quantity * -1,
                    data=received_inventory.data
                )
                received_inventory.save()
                inventory_quantity -= quantity
                inventory_stock -= quantity

                if outstanding_quantity > 0:
                    InventoryReceivedReturned.objects.create(
                        inventory_received=received_inventory.inventory_received,
                        received_object_id=received_inventory.received_object_id,
                        received_type=received_inventory.received_type,
                        inventory=received_inventory.inventory,
                        quantity=outstanding_quantity,
                        price=received_inventory.price,
                        opening_balance=opening_balance,
                        received_value=Decimal(received_inventory.price * outstanding_quantity).quantize(Decimal('0.01')),
                        data=utils.prepare_opening_closing_balance(
                            cls=received_inventory.inventory_received,
                            opening_stock=inventory_stock,
                            quantity=outstanding_quantity
                        )
                    )
                total_returned_qty -= quantity

                received_returned_cost += quantity * received_inventory.price
                total_quantity -= quantity
                logger.info(f"Returned {quantity} now total returned is  {total_returned_qty}")
                quantity = 0
                returned_lines.append(received_inventory)
                inventory_unit_cost = received_inventory.price
            else:
                quantity = quantity - received_inventory.quantity
                opening_balance = inventory_quantity
                inventory_quantity -= received_inventory.quantity
                total_quantity -= inventory_quantity
                total_returned_qty -= received_inventory.quantity
                logger.info(
                    f"Opening balance={opening_balance}, balance={inventory_quantity}, Returned {received_inventory.quantity} now total returned is  {total_returned_qty}")
                # inventory_received_item.inventory_returned = inventory_returned
                received_inventory.returned_type = ContentType.objects.get_for_model(content_object)
                received_inventory.returned_object_id = content_object.id if content_object else None
                received_inventory.returned_value = received_inventory.received_value
                received_inventory.data = utils.prepare_opening_closing_balance(
                    cls=content_object,
                    opening_stock=inventory_stock,
                    quantity=received_inventory.quantity * -1,
                    data=received_inventory.data
                )
                received_inventory.save()
                inventory_stock -= received_inventory.quantity
                returned_lines.append(received_inventory)
                received_returned_cost += received_inventory.quantity * received_inventory.price
                inventory_unit_cost = received_inventory.price

    if quantity > 0 and inventory.allow_negative_stocks:
        price = Decimal(0)
        logger.info("----------ALLOWS negative stock and we going negative -------------")
        logger.info("we are using ")
        if inventory.price_type == BranchInventory.LAST_PRICE:
            logging.info("LAST PRICE")
            if inventory.current_price:
                price = inventory.current_price
        elif inventory.average_price:
            logging.info("AVERAGE PRICE")
            price = inventory.average_price

        quantity_left = quantity - Decimal(inventory_quantity)
        opening_balance = inventory_quantity
        balance = inventory_quantity - quantity
        total_quantity -= inventory_quantity
        total_returned_qty -= quantity
        logger.info(f"Returned without received {inventory_quantity} now total returned is  {total_quantity}")

        logger.info(f" --> {price} for {quantity_left} items ")
        received_returned = InventoryReceivedReturned.objects.create(
            # inventory_returned=inventory_returned,
            inventory=inventory,
            returned_type=ContentType.objects.get_for_model(content_object),
            returned_object_id=content_object.id if content_object else None,
            quantity=quantity_left,
            price=price,
            balance=balance,
            opening_balance=opening_balance,
            data=utils.prepare_opening_closing_balance(inventory_stock, quantity_left)
        )
        returned_lines.append(received_returned)
        returned_cost = Decimal(price) * Decimal(quantity_left)
        inventory_unit_cost = price
    logger.info(f"Total returned is now {total_returned_qty}")
    # update_inventory_stock(inventory, total_returned_qty, inventory_unit_cost)
    inventory.recalculate_balances(year=year)

    return {'returned_lines': returned_lines, 'total_cost': received_returned_cost}


def return_received(inventory: BranchInventory, year: Year, quantity: Decimal, received_object, return_object):
    """
         We return from specific object we received first, if we return what we have not received, inventory goes into negative stock
        :param inventory: BranchInventory
        :param year: Year
        :param quantity: Decimal
        :param received_object: GenericObject - Inventory can be received by different process, eg Adjustment, Sales or General Received
        :param return_object: GenericObject - Inventory can be returned by different process, eg Adjustment, Sales or General Return
        :return:
    """
    received_items = InventoryReceivedReturned.objects.filter(
        received_type=ContentType.objects.get_for_model(model=received_object),
        received_object_id=received_object.pk,
        returned_type__isnull=True
    )
    qty = quantity
    inventory_quantity = inventory.in_stock
    inventory_stock = inventory_quantity
    total_cost = 0
    returned_lines = []
    logger.info(f"Initial Inventory Quantity = {inventory_quantity} working on {quantity}")
    total_returned_qty = 0
    for received_item in received_items:
        if qty >= received_item.quantity:
            received_item.returned_type = ContentType.objects.get_for_model(return_object)
            received_item.returned_object_id = return_object.id
            received_item.returned_value = received_item.received_value
            received_item.save()
            logger.info(f"Total returned is now {received_item.quantity}")
            qty = qty - received_item.quantity
            returned_lines.append(received_item)
            total_cost += received_item.quantity * received_item.price
            inventory_stock -= received_item.quantity
            inventory_unit_cost = received_item.price
        elif qty < received_item.quantity:
            opening_balance = inventory_quantity
            received_item.returned_type = ContentType.objects.get_for_model(return_object)
            received_item.returned_object_id = return_object.id
            received_item.returned_value = qty * received_item.price
            received_item.data = utils.prepare_opening_closing_balance(received_object, inventory_stock, quantity * -1,
                                                                       received_item.data)
            received_item.save()
            outstanding_quantity = received_item.quantity - qty
            quantity = 0
            if outstanding_quantity > 0:
                InventoryReceivedReturned.objects.create(
                    received_type=ContentType.objects.get_for_model(received_item),
                    received_object_id=received_item.id,
                    quantity=outstanding_quantity,
                    price=received_item.price,
                    received_value=received_item.price * outstanding_quantity,
                    inventory=inventory,
                    opening_balance=opening_balance,
                    data=utils.prepare_opening_closing_balance(
                        received_item, inventory_stock, outstanding_quantity)
                )
            total_cost += qty * received_item.price
            returned_lines.append(received_item)
            inventory_unit_cost = received_item.price

    if qty > 0:
        returns = return_inventory(
            inventory=inventory,
            quantity=quantity,
            year=year,
            content_object=return_object
        )
        if returns:
            returned_lines = returns['returned_lines'] + returned_lines
            total_cost += returns['total_cost']

    inventory.recalculate_balances(year=year)

    return {'returned_lines': returned_lines, 'total_cost': total_cost}


def update_inventory_stock(inventory, quantity, unit_cost):
    history = RegisterInventoryHistory(inventory=inventory, price=unit_cost, quantity=quantity)
    history.create()


def get_available_received_items(self, branch_inventory):
    total_received = 0
    available_received_items = []
    for _inventory_rec in branch_inventory.inventory_received.all().order_by('created_at'):
        total_received += _inventory_rec.quantity
        received_returned = 0
        # available_received = AvailableReceived(_inventory_rec)
        # available_received.execute()
        # if available_received.available_quantity > 0:
        #     available_received_items.append(available_received)
        received_returned_items = _inventory_rec.returned_items.filter(inventory_returned__isnull=False,
                                                                       price__isnull=False).all()
        for rec_ret in received_returned_items:
            received_returned += rec_ret.inventory_returned.quantity

        available_quantity = _inventory_rec.quantity - received_returned
        if available_quantity > 0:
            available_received_items.append({'received': _inventory_rec, 'in_hand': available_quantity,
                                             'received_items': received_returned_items})
    return available_received_items


def get_stock_adjustment_account(account, inventory, company):
    if account:
        return account
    if inventory.stock_adjustment_account:
        return inventory.stock_adjustment_account
    if company.stock_adjustment_account:
        return company.stock_adjustment_account
    raise JournalAccountUnavailable('Stock adjustment account not set, please fix')


def get_stock_control_account(inventory):
    if not inventory.gl_account:
        raise JournalAccountUnavailable('Stock control account not set, please fix')
    return inventory.gl_account


def create_received_general_journal(company: Company, account: Account, period: Period, inventory: BranchInventory,
                                    amount, text: str, date, role, profile, content_object):
    logger.info('--------- create_received_general_journal -----')

    stock_adjustment_account = get_stock_adjustment_account(account, inventory, company)
    control_account = get_stock_control_account(inventory)

    journal = Journal.objects.create(
        company=company,
        year=period.period_year,
        period=period,
        journal_text=text,
        module=Module.INVENTORY,
        role=role,
        date=date,
        created_by=profile
    )
    if journal:
        JournalLine.objects.create(
            journal=journal,
            account=control_account,
            debit=amount,
            is_reversal=False,
            text=text,
            accounting_date=date,
            object_id=content_object.id,
            content_type=ContentType.objects.get_for_model(content_object)
        )
        JournalLine.objects.create(
            journal=journal,
            account=stock_adjustment_account,
            credit=amount,
            is_reversal=False,
            text=text,
            accounting_date=date,
            object_id=content_object.id,
            content_type=ContentType.objects.get_for_model(content_object)
        )
    return journal


def create_inventory_ledger(profile, inventory: BranchInventory, price: Decimal, quantity: Decimal, content_object,
                            text: str, date, period: Period, is_return=False, journal=None):
    Ledger.objects.create(
        inventory=inventory,
        content_type=ContentType.objects.get_for_model(content_object),
        object_id=content_object.id,
        quantity=quantity,
        value=price,
        date=date,
        created_by=profile,
        period=period,
        text=text,
        journal=journal,
        rate=-1 if is_return else 1
    )


def is_valuation_balancing(branch: Branch, year: Year) -> bool:
    running_year = Year.objects.started().get(company=branch.company)
    account_total = get_total_for_sub_ledger(
        year=running_year, company=branch.company, sub_ledger=SubLedgerModule.INVENTORY,
        start_date=running_year.start_date, end_date=running_year.end_date
    )

    valuation = PurchaseHistory.objects.branch_valuation(branch=branch)
    valuation_total = valuation['total'] if valuation and valuation['total'] else 0
    logger.info(f"Valuation --> {valuation_total} vs journals {account_total}, diff {valuation_total - account_total} ")
    return valuation_total == account_total


def handle_year_closing_inventory_movement(year: Year, inventory: BranchInventory):
    started_year = Year.objects.started().filter(company=year.company).first()
    if year.is_closing:
        transaction, is_new = Received.all.get_or_create(
            movement_type=MovementType.OPENING_BALANCE,
            branch=inventory.branch,
            period=started_year.first_period,
            defaults={
                'date': timezone.now().date(),
                'note': 'Year Closing'
            }
        )
        logger.info(f"Average price {inventory.average_price} and in stock is {inventory.in_stock}")

        opening_balance, is_new = InventoryReceived.objects.update_or_create(
            received=transaction,
            inventory=inventory,
            defaults={
                'price_excluding': inventory.average_price,
                'quantity': inventory.in_stock
            }
        )