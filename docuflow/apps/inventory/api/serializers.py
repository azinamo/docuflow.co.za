from django.urls import reverse
from rest_framework import serializers

from docuflow.apps.company.api.serializers import AccountSerializer
from docuflow.apps.company.api.serializers import UnitSerializer, MeasureSerializer, VatCodeSerializer
from docuflow.apps.inventory.models import (BranchInventory, GroupLevel, GroupLevelItem, PurchaseHistory, Received,
                                            Returned, StockAdjustment, Recipe, Manufacturing)
from docuflow.apps.invoice.api.serializers import MinimalInvoiceSerializer
from docuflow.apps.supplier.api.serializers import SupplierSerializer


class GroupLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupLevel
        fields = ('id', 'name', 'slug')


class GroupItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupLevelItem
        fields = ('id', 'name')


# class InventorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Inventory
#         fields = '__all__'


class InventoryHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchaseHistory
        fields = ('id', 'update_pricing_at', 'last_price_update', 'price_paid', 'average_price', 'total_value',
                  'in_stock', 'on_picking', 'on_order', 'on_reserve', 'on_delivery', 'updated_at')


class BranchInventorySerializer(serializers.ModelSerializer):
    detail_url = serializers.SerializerMethodField()
    reference = serializers.CharField(source='__str__')
    is_service = serializers.BooleanField(source='is_service_sku')
    description = serializers.CharField(source='full_name')
    code = serializers.CharField(source='inventory.code')

    class Meta:
        model = BranchInventory
        fields = ['id', 'reference', 'is_blocked', 'is_pos_blocked', 'is_recipe_item', 'sku_type', 'sku',
                  'service_cost', 'description', 'sales_account', 'cost_of_sales_account', 'gl_account', 'bar_code',
                  'cost_price', 'selling_price', 'selling_price_with_vat', 'is_recipe_item', 'is_discounted',
                  'measure', 'unit', 'sale_unit', 'stock_unit', 'additional_text', 'delivery_fee', 'sale_delivery_fee',
                  'gross_weight', 'weight', 'environment_fee', 'sale_environment_fee', 'last_counted', 'code',
                  'allow_negative_stocks', 'price_type', 'critical_stock_level', 'lowest_stock_level', 'supplier',
                  'max_stock_level', 'level_to_order_stock', 'min_reorder', 'max_reorder', 'quantity_in_package',
                  'sale_quantity_in_package', 'sold_as_type', 'group_level_items', 'detail_url', 'history', 'in_stock',
                  'store_area', 'rack', 'bin_location', 'supplier_code', 'last_price_updated', 'sale_vat_code',
                  'vat_code', 'is_service', 'stock_adjustment_account', 'current_price', 'average_price'
                  ]
        datatables_always_serialize = ['id', 'detail_url']

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse('edit_branch_inventory', args=(instance.pk, ))


class BranchInventoryDetailSerializer(BranchInventorySerializer):
    history = InventoryHistorySerializer()
    supplier = SupplierSerializer()
    sales_account = AccountSerializer()
    cost_of_sales_account = AccountSerializer()
    gl_account = AccountSerializer()
    stock_adjustment_account = AccountSerializer()
    unit = UnitSerializer()
    sale_unit = UnitSerializer()
    stock_unit = UnitSerializer()
    measure = MeasureSerializer()
    vat_code = VatCodeSerializer()
    sale_vat_code = VatCodeSerializer()
    detail_url = serializers.SerializerMethodField()
    reference = serializers.CharField(source='__str__')

    class Meta(BranchInventorySerializer.Meta):
        model = BranchInventory
        fields = BranchInventorySerializer.Meta.fields
        datatables_always_serialize = ['id', 'detail_url']


class InventoryReturnedSerializer(serializers.ModelSerializer):
    supplier = SupplierSerializer()
    detail_url = serializers.SerializerMethodField()
    reference = serializers.CharField(source='__str__')
    invoice = MinimalInvoiceSerializer()

    class Meta:
        model = Returned
        fields = ('id', 'reference', 'document_type', 'supplier', 'date', 'total_excl', 'total_amount', 'invoice',
                  'detail_url')
        datatables_always_serialize = ('id', 'detail_url', 'invoice')

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse('view_returned', args=(instance.pk, ))


class InventoryReceivedSerializer(serializers.ModelSerializer):
    supplier = SupplierSerializer()
    returned = InventoryReturnedSerializer()
    invoice = MinimalInvoiceSerializer(source='invoice_account.invoice')
    detail_url = serializers.SerializerMethodField()
    document = serializers.CharField(source='__str__')
    is_out_of_flow = serializers.SerializerMethodField()

    class Meta:
        model = Received
        fields = ('id', 'document', 'document_type', 'supplier', 'note', 'date', 'total_excl', 'total_amount',
                  'is_out_of_flow', 'returned', 'detail_url', 'invoice')
        datatables_always_serialize = ('id', 'detail_url', 'is_out_of_flow', 'full_return', 'invoice', 'returned')

    # noinspection PyMethodMayBeStatic
    def get_is_out_of_flow(self, instance):
        return instance.invoice and instance.invoice.is_out_of_flow

    # noinspection PyMethodMayBeStatic
    def get_invoiced(self, instance):
        if instance.has_invoice_account:
            return instance.has_invoice_account.invoice
        return None

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse('view_received', args=(instance.pk, ))


class StockAdjustmentSerializer(serializers.ModelSerializer):
    detail_url = serializers.SerializerMethodField()
    reference = serializers.CharField(source='__str__')

    class Meta:
        model = StockAdjustment
        fields = ('id', 'reference', 'date', 'comment', 'status', 'detail_url')
        datatables_always_serialize = ('id', 'detail_url')

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse('view_adjustment', args=(instance.pk, ))


class RecipeSerializer(serializers.ModelSerializer):
    inventory = BranchInventorySerializer()
    detail_url = serializers.SerializerMethodField()

    class Meta:
        model = Recipe
        fields = ['id', 'description', 'inventory', 'created_at', 'updated_at', 'detail_url']
        datatables_always_serialize = ('id', 'detail_url')

    # noinspection PyMethodMayBeStatic
    def get_detail_url(self, instance):
        return reverse('edit_recipe', args=(instance.pk, ))


class ManufacturingSerializer(serializers.ModelSerializer):
    detail_url = serializers.CharField(source='get_absolute_url')
    reference = serializers.CharField(source='__str__')

    class Meta:
        model = Manufacturing
        fields = ['id', 'reference', 'description', 'date', 'created_at', 'detail_url']
        datatables_always_serialize = ('id', 'detail_url')

