from rest_framework import viewsets

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.inventory.models import BranchInventory, Received, Returned, StockAdjustment, Recipe, Manufacturing

from . import serializers


class InventoryViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.BranchInventoryDetailSerializer

    def get_queryset(self):
        return BranchInventory.all_inventory.select_related(
            'inventory', 'supplier', 'branch', 'sales_account', 'cost_of_sales_account', 'gl_account', 'vat_code',
            'sale_vat_code', 'measure', 'unit', 'sale_unit', 'stock_unit', 'stock_adjustment_account', 'history'
        ).filter(
            branch=self.get_branch()
        ).order_by(
            'inventory__code', 'description'
        )


class InventoryReceivedViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.InventoryReceivedSerializer

    def get_queryset(self):
        return Received.objects.for_branch_and_year(branch=self.get_branch(), year=self.get_year())


class InventoryReturnedViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.InventoryReturnedSerializer

    def get_queryset(self):
        return Returned.objects.listable().exclude(
            invoice__isnull=False,
        ).select_related(
            'invoice_account', 'period__period_year', 'invoice_account__invoice', 'invoice_account__invoice__invoice_type',
            'invoice_account__invoice__status', 'invoice_account__invoice__company', 'received_movement', 'supplier'
        ).prefetch_related(
            'inventory_returned'
        ).order_by('-created_at')


class StockAdjustmentViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.StockAdjustmentSerializer

    def get_queryset(self):
        return StockAdjustment.objects.select_related(
            'branch'
        ).filter(branch=self.get_branch(), period__period_year=self.get_year()).order_by('-id')


class RecipeViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.RecipeSerializer

    def get_queryset(self):
        return Recipe.objects.select_related(
            'inventory', 'inventory__inventory'
        ).filter(inventory__branch=self.get_branch())


class ManufacturingViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.ManufacturingSerializer

    def get_queryset(self):
        return Manufacturing.objects.filter(branch=self.get_branch(), period__period_year=self.get_year())
