from django.contrib import admin


from . import models


@admin.register(models.BranchInventory)
class InventoryAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'description', 'branch', 'is_recipe_item', 'unit', 'sku_type', 'cost_price',
                    'selling_price')
    fields = ('branch', 'cost_price', 'selling_price')
    list_select_related = ('inventory', 'branch', 'branch__company', )
    list_filter = ('branch', )
    search_fields = ('inventory__code', 'description')


@admin.register(models.Received)
class ReceivedAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ('reference', 'note', 'purchase_order', 'movement_type', 'supplier', 'date', 'total_amount',
                    'total_excl', 'status')
    fields = ('date', 'total_amount', 'status')
    list_select_related = ('purchase_order', 'purchase_order__invoice', 'purchase_order__invoice__company',
                           'branch', 'period', 'period__period_year', 'supplier')
    list_filter = ('branch__company', 'status', 'supplier')
    search_fields = ('note', 'reference', 'movement_type')

    def get_queryset(self, request):
        return super().get_queryset(request).select_related(*self.list_select_related).prefetch_related('inventory_received')



