import json
import logging
import pprint
from collections import defaultdict
from datetime import datetime
from decimal import Decimal

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.contenttypes.fields import ContentType

from django.db import transaction
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, View, TemplateView
from django.views.generic.edit import UpdateView
from openpyxl import load_workbook

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin, CompanyLoginRequiredMixin, DataTableMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.company.models import Company, VatCode, Branch
from docuflow.apps.inventory.api.serializers import BranchInventoryDetailSerializer
from docuflow.apps.period.models import Period, Year
from docuflow.apps.reports.inventoryvaluation import InventoryValuation
from .enums import MovementType
from .exceptions import InventoryCodeExists, InventoryException
from .forms import (InventoryForm, BranchInventoryForm, CopyInventoryForm, ImportForm, CopyInventoryToBranchForm,
                    InventorySearchForm, ImportOpeningBalanceForm)
from .models import (Inventory, BranchInventory, Received, InventoryReceived, GroupLevel, InventoryReturned,
                     InventoryReceivedReturned, Returned, Manufacturing, ManufacturingRecipe,
                     StockAdjustmentInventory, )
from .resources import BranchInventoryResource
from .services import ProcessImport
from ..company.enums import SubLedgerModule
from ..journals.services import get_total_for_sub_ledger
from ..sales.models import InvoiceItem

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class InventoryRegisterView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'inventory/register.html'
    context_object_name = 'inventory_items'

    def get_context_data(self, **kwargs):
        context = super(InventoryRegisterView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context


class BranchInventoryView(CompanyLoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'inventory/index.html'

    def get_context_data(self, **kwargs):
        context = super(BranchInventoryView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context


class InventoryDataView(CompanyLoginRequiredMixin, ProfileMixin, View):

    def get_last_page(self, limit, total):
        last_page = int(total / limit)
        return last_page

    def get_filter_options(self):
        filters = json.loads(self.request.GET.get('filters'))
        q_object = Q()
        branch = self.get_branch()
        q_object.add(Q(branch=self.get_branch()), Q.AND)
        for filter in filters:
            if filter['field'] == 'inventory_code':
                q_object.add(Q(inventory__code__icontains=filter['value']), Q.AND)
            if filter['field'] == 'description':
                q_object.add(Q(description__icontains=filter['value']), Q.AND)
            if filter['field'] == 'supplier':
                q_object.add(Q(supplier__name__icontains=filter['value']) |
                             Q(supplier__code__icontains=filter['value']), Q.AND)
            if filter['field'] == 'gl_account':
                q_object.add(Q(gl_account__name__icontains=filter['value']) |
                             Q(gl_account__code__icontains=filter['value']), Q.AND)
            if filter['field'] == 'sales_account':
                q_object.add(Q(sales_account__name__icontains=filter['value']) |
                             Q(sales_account__code__icontains=filter['value']), Q.AND)
            if filter['field'] == 'cost_of_sales_account':
                q_object.add(Q(cost_of_sales_account__name__icontains=filter['value']) |
                             Q(cost_of_sales_account__code__icontains=filter['value']), Q.AND)
            if filter['field'] == 'is_recipe':
                q_object.add(Q(is_recipe=filter['value']), Q.AND)
        return q_object

    def get(self, request, *args, **kwargs):
        limit = int(self.request.GET.get('size', 20))

        page = int(self.request.GET.get('page', 1))
        start = limit * (page - 1)
        _limit = limit * page
        q_objects = self.get_filter_options()
        inventories = BranchInventory.all_inventory.select_related(
            'inventory', 'supplier', 'branch', 'sales_account', 'cost_of_sales_account', 'gl_account', 'vat_code',
            'sale_vat_code', 'measure', 'unit', 'history'
        ).filter(q_objects).order_by('inventory__code', 'description')
        inventory_data = []
        total = inventories.count()
        branch_inventories = inventories[start:_limit]
        for inventory in branch_inventories:
            inventory_data.append({
                'inventory_code': str(inventory),
                'description': inventory.description,
                'is_recipe': inventory.is_recipe_item,
                'is_active': False if inventory.is_blocked else True,
                'supplier': inventory.supplier.name if inventory.supplier else '',
                'supplier_code': inventory.supplier_code,
                'bar_code': inventory.bar_code,
                'sales_account': inventory.sales_account.code if inventory.sales_account else '',
                'cost_of_sales_account': inventory.cost_of_sales_account.code if inventory.cost_of_sales_account else '',
                'gl_account': inventory.gl_account.code if inventory.gl_account else '',
                'sales_account_name': inventory.sales_account.name if inventory.sales_account else '',
                'cost_of_sales_account_name': inventory.cost_of_sales_account.name if inventory.cost_of_sales_account else '',
                'gl_account_name': inventory.gl_account.name if inventory.gl_account else '',
                'selling_price': float(inventory.selling_price),
                'vat_amount': float(inventory.vat_amount),
                'vat_code': inventory.vat_code.percentage if inventory.vat_code else '',
                'selling_price_with_vat': float(inventory.selling_price_with_vat),
                'inventory_id': inventory.id,
                'edit_url': reverse('edit_branch_inventory', kwargs={'pk': inventory.id}),
                'in_hand': float(inventory.in_stock)
            })
        last_page = self.get_last_page(limit, total)
        return HttpResponse(json.dumps({'data': inventory_data, 'last_page': last_page}))


class CreateInventoryView(CompanyLoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'inventory/create.html'
    success_message = 'Inventory item successfully saved'
    form_class = InventoryForm
    success_url = reverse_lazy('inventory_index')

    def get_inventory(self):
        return Inventory.objects.get(pk=self.kwargs['inventory_id'])

    def get_context_data(self, **kwargs):
        context = super(CreateInventoryView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateInventoryView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['branch'] = self.get_branch()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the inventory item, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateInventoryView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                with transaction.atomic():
                    inventory = form.save(commit=False)
                    redirect_url = reverse('create_branch_inventory', kwargs={'inventory_id': inventory.id})
                    branch_inventory = BranchInventory.objects.filter(inventory=inventory, branch=self.get_branch()).first()
                    if branch_inventory:
                        redirect_url = reverse('edit_branch_inventory', kwargs={'pk': branch_inventory.id})
                return JsonResponse({
                    'error': False,
                    'text': 'Inventory item saved successfully',
                    'redirect': redirect_url
                })
            except InventoryCodeExists as exception:
                logger.info(exception)
                return JsonResponse({
                    'error': True,
                    'text': str(exception),
                    'details': str(exception),
                    'url': reverse('copy_inventory_to_branches', kwargs={'inventory_id': inventory.id})
                })


class EditInventoryView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = 'inventory/edit.html'
    model = Inventory
    success_message = 'Inventory item successfully updated'
    form_class = InventoryForm
    success_url = reverse_lazy('inventory_index')

    def get_context_data(self, **kwargs):
        context = super(EditInventoryView, self).get_context_data(**kwargs)
        inventory = self.object
        context['company'] = self.get_company()
        context['inventory'] = inventory
        return context

    def get_form_args_kwargs(self):
        kwargs = super(EditInventoryView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred updating the inventory item, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(EditInventoryView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                company = self.get_company()
                inventory = form.save(commit=False)
                inventory.company = company
                inventory.save()

            return JsonResponse({'error': False,
                                 'text': 'Inventory updated successfully',
                                 'reload': True
                                 })


class CreateBranchInventoryView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'inventory/create_branch.html'
    success_message = 'Inventory item successfully saved'
    form_class = BranchInventoryForm
    success_url = reverse_lazy('inventory_index')

    def get_inventory(self):
        return Inventory.objects.get(pk=self.kwargs['inventory_id'])

    def get_context_data(self, **kwargs):
        context = super(CreateBranchInventoryView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        context['inventory'] = self.get_inventory()
        return context

    def get_initial(self):
        initial = super(CreateBranchInventoryView, self).get_initial()
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateBranchInventoryView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['request'] = self.request
        kwargs['inventory'] = self.get_inventory()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the inventory item, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateBranchInventoryView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save(commit=False)

                # is_copied = self.request.session.get('is_copied', False)
                # if is_copied:
                #     branches = Branch.objects.filter(company=company).exclude(id=inventory_item.branch.id)
                #     for branch in branches:
                #         branch_inventory_item = inventory_item
                #         branch_inventory_item.pk = None
                #         branch_inventory_item.parent_branch = inventory_item.branch
                #         branch_inventory_item.branch = branch
                #         branch_inventory_item.save()

            return JsonResponse({
                'error': False,
                'text': 'Inventory item saved successfully',
                'redirect': reverse('inventory_index')
            })


class EditBranchInventoryView(LoginRequiredMixin, ProfileMixin, UpdateView):
    model = BranchInventory
    template_name = 'inventory/edit_branch.html'
    success_message = 'Inventory updated successfully updated'
    form_class = BranchInventoryForm

    def get_queryset(self):
        return BranchInventory.all_inventory.select_related(
            'history', 'supplier', 'gl_account', 'sales_account', 'cost_of_sales_account', 'sale_unit',
            'sale_vat_code', 'stock_unit', 'vat_code', 'unit', 'measure', 'inventory'
        ).prefetch_related(
            'inventory_received', 'group_level_items'
        )

    def get_context_data(self, **kwargs):
        context = super(EditBranchInventoryView, self).get_context_data(**kwargs)
        branch_inventory = self.get_object()
        history = getattr(branch_inventory, 'history', None)
        context['branch'] = self.get_branch()
        context['inventory'] = branch_inventory
        context['history'] = history
        return context

    def get_form_kwargs(self):
        kwargs = super(EditBranchInventoryView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['request'] = self.request
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the inventory item, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(EditBranchInventoryView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save(commit=False)
            return JsonResponse({
                'error': False,
                'text': 'Inventory updated successfully',
                'redirect': reverse('inventory_index')
            })


class DeleteBranchInventoryView(View):

    def get(self, request, *args, **kwargs):
        branch_inventory = BranchInventory.all_inventory.get(pk=self.kwargs['pk'])
        if branch_inventory:
            branch_inventory.delete()
            messages.success(self.request, 'Branch inventory deleted successfully')
        return HttpResponseRedirect(reverse('inventory_index'))


class CopyInventoryView(ProfileMixin, FormView):
    template_name = 'inventory/copy.html'
    form_class = CopyInventoryForm

    def get_branches(self):
        inventory = self.get_inventory()
        branches = Branch.objects.filter(company=self.get_company()).exclude(
            inventory_id__in=[branch_inventory.id for branch_inventory in inventory.branch_inventory_items.all()]).all()
        all_branches = []
        for branch in branches:
            all_branches.append(branch)
        return all_branches

    def get_form_kwargs(self, ):
        kwargs = super(CopyInventoryView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        inventory = self.get_inventory()
        kwargs['excludes'] = [branch_inventory.branch_id for branch_inventory in inventory.branch_inventory_items.all()]
        return kwargs

    def get_inventory(self):
        return Inventory.objects.get(pk=self.kwargs['inventory_id'])

    def get_context_data(self, **kwargs):
        context = super(CopyInventoryView, self).get_context_data(**kwargs)
        inventory = self.get_inventory()
        context['inventory'] = inventory
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred copying the inventory item, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CopyInventoryView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            with transaction.atomic():
                company = self.get_company()
                inventory_item = self.get_inventory()
                inventory_item_branches = BranchInventory.objects.filter(inventory=inventory_item).all()
                branches = Branch.objects.filter(
                    company=company
                ).exclude(
                    id__in=[inventory.branch_id for inventory in inventory_item_branches if inventory.branch]
                )
                for branch in branches:
                    logger.info("Copy inventory {} to {}".format(inventory_item, branch))
                    BranchInventory.objects.create(
                        branch=branch,
                        inventory=inventory_item,
                        description=inventory_item.description
                    )

            return JsonResponse({
                'error': False,
                'text': 'Inventory copied successfully',
                'redirect': reverse('inventory_index')
            })


class SearchInventoryView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        term = request.GET.get("term", "")
        results = []
        if term:
            inventories = BranchInventory.objects.filter(
                branch=self.get_branch()
            ).filter(
                Q(inventory__code__icontains=term) | Q(inventory__description__icontains=term) | Q(description__icontains=term)
            )[0:50]

            for inventory in inventories:
                detail_url = reverse('view_inventory_item_detail', kwargs={'pk': inventory.id})
                description = inventory.description if inventory.description else inventory.inventory.description

                results.append({'id': inventory.id, 'text': inventory.full_name, 'url': detail_url, 'code': inventory.inventory.code,
                                'description': description})

        return HttpResponse(json.dumps(results))


class SearchInventoryItemView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        term = request.GET.get("term", "")
        results = []
        if term:
            inventories = BranchInventory.objects.filter(
                branch=self.get_branch()
            ).filter(
                Q(inventory__code__icontains=term) | Q(inventory__description__icontains=term) | Q(description__icontains=term)
            )[0:50]

            for inventory in inventories:
                detail_url = reverse('view_inventory_item_detail', kwargs={'pk': inventory.id})
                description = inventory.description if inventory.description else inventory.inventory.description

                results.append({'id': inventory.id,
                                'text': inventory.full_name,
                                'url': detail_url,
                                'code': inventory.inventory.code,
                                'description': description
                                })

        return HttpResponse(json.dumps({'err': 'nil', 'results': results}), content_type='application/json')


class CalculateSellingPriceWithVat(View):

    def get(self, request, *args, **kwargs):
        vat_code_id = self.request.GET.get('vat_code_id')
        selling_price = self.request.GET.get('selling_price')
        if vat_code_id and selling_price:
            vat_code = VatCode.objects.get(pk=vat_code_id)
            price = float(selling_price)
            vat = ((vat_code.percentage / 100) * float(price))
            selling_price_with_vat = price + vat
            return JsonResponse({'price': selling_price_with_vat, 'error': False, 'vat': vat,
                                 'percentage': vat_code.percentage})
        return JsonResponse({'error': True})


class InventoryItemView(ProfileMixin, FormView):
    form_class = InventorySearchForm
    template_name = 'inventory/view.html'

    def get_context_data(self, **kwargs):
        context = super(InventoryItemView, self).get_context_data(**kwargs)
        return context


class InventoryView(ProfileMixin, View):

    def get(self, request):
        branch_inventory = BranchInventory.objects.prefetch_related(
            'inventory_received', 'inventory_returned', 'purchase_orders', 'fixed_prices', 'sales_invoiceitem_related',
            'sales_deliveryitem_related', 'purchase_orders'
        ).select_related(
            'history', 'stock_adjustment_account', 'cost_of_sales_account', 'gl_account'
        ).filter(
            id=self.request.GET.get('inventory_id')
        ).first()
        return JsonResponse(BranchInventoryDetailSerializer(branch_inventory).data)


class InventoryDetailView(ProfileMixin, TemplateView):
    template_name = 'inventory/detail.html'

    def get_inventory_by_text(self, term):
        if term:
            return BranchInventory.objects.prefetch_related(
                'inventory_received', 'inventory_returned', 'purchase_orders',
            ).filter(branch=self.get_branch()).filter(Q(description__icontains=term) | Q(inventory__code__icontains=term))
        return None

    def get_inventory_by_pk(self, inventory_id):
        return BranchInventory.objects.prefetch_related(
            'inventory_received', 'inventory_returned', 'purchase_orders', 'ledgers'
        ).filter(id=inventory_id)

    def get_inventories(self):
        pk = self.kwargs.get('pk', self.request.GET.get('inventory_id', None))
        term = self.request.GET.get('term', None)

        if pk:
            return self.get_inventory_by_pk(pk)
        elif term:
            return self.get_inventory_by_text(term)
        return None

    def get_inventory_detail(self, branch_inventory):
        logger.info(f"-------------get_inventory_detail ----------------{branch_inventory}")
        inventory_detail = {'total_ordered': 0, 'total_received': 0, 'total_short': 0, 'total_price_exl': 0,
                            'total_price_in': 0, 'total_total_price_exl': 0, 'total_total_price_inc': 0,
                            'inventory_items': [], 'total_in_hand': 0, 'total_returned': 0,
                            'on_reserve': 0, 'on_delivery': 0, 'current_in_hand': 0, 'on_order': 0
                            }

        total_in_hand = Decimal(0)
        current_in_hand = Decimal(0)
        on_reserve = 0
        on_delivery = 0
        on_order = 0
        if branch_inventory.in_stock:
            total_in_hand += branch_inventory.in_stock
            current_in_hand += branch_inventory.in_stock

        if branch_inventory.on_delivery:
            current_in_hand -= branch_inventory.on_delivery
            on_delivery += branch_inventory.on_delivery

        if branch_inventory.on_picking:
            current_in_hand -= branch_inventory.on_picking

        if branch_inventory.on_order:
            current_in_hand -= branch_inventory.on_order
            on_delivery += branch_inventory.on_order

        for inventory_purchase_order in branch_inventory.purchase_orders.all():
            is_linked = hasattr(inventory_purchase_order.purchase_order, 'inventory_received')
            if not is_linked:
                logger.info(f"Purchase order has -> {inventory_purchase_order.quantity} qty ---> {inventory_purchase_order.purchase_order} is linked {is_linked}")
                current_in_hand += inventory_purchase_order.quantity
                on_order += float(inventory_purchase_order.quantity)

        inventory_detail['inventory'] = branch_inventory
        inventory_detail['total_in_hand'] = total_in_hand
        inventory_detail['current_in_hand'] = current_in_hand
        inventory_detail['on_reserve'] = on_reserve
        inventory_detail['on_delivery'] = on_delivery
        inventory_detail['on_order'] = on_order

        for received_inventory in branch_inventory.inventory_received.all():
            if received_inventory.short:
                inventory_detail['total_short'] += received_inventory.short
            if received_inventory.price_excluding:
                inventory_detail['total_price_exl'] += received_inventory.price_excluding
            if received_inventory.price_including:
                inventory_detail['total_price_in'] += received_inventory.price_including

            inventory_detail['total_total_price_exl'] += received_inventory.total_price_excluding
            inventory_detail['total_total_price_inc'] += received_inventory.total_price_including
        logger.info(inventory_detail)
        return inventory_detail

    def get_detail(self, inventories):
        inventory_by_branch = {'own': {}, 'branches': []}
        totals = {'received': 0, 'returned': 0, 'balance': 0, 'ordered': 0, 'in_hand': 0}
        current_branch = self.get_branch()
        for branch_inventory in inventories:
            other_branch_inventories = BranchInventory.objects.prefetch_related(
                'inventory_received',
                'inventory_returned',
                'sales_invoice_items',
                'sales_invoice_items__invoice',
                'purchase_orders'
            ).filter(
                inventory=branch_inventory.inventory
            ).exclude(
                id=branch_inventory.id
            )
            if branch_inventory.branch_id == current_branch.id:
                inventory_by_branch['own'] = self.get_inventory_detail(branch_inventory)
            else:
                for other_branch_inventory in other_branch_inventories:
                    inventory_detail = self.get_inventory_detail(other_branch_inventory)
                    inventory_by_branch['branches'].append(inventory_detail)

        return {'branch_inventories': inventory_by_branch, 'totals': totals}

    def get_context_data(self, **kwargs):
        context = super(InventoryDetailView, self).get_context_data(**kwargs)
        inventories = self.get_inventories()
        if inventories:
            detail = self.get_detail(inventories)
            context['branch_inventory'] = detail['branch_inventories']
            context['totals'] = detail['totals']
        return context


class OrderInventoryItemView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return BranchInventory.objects.get(pk=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            with transaction.atomic():
                return JsonResponse({'error': False,
                                     'text': 'Purchase order successfully ordered.',
                                     })
        else:
            return redirect(reverse('invoice_detail', kwargs={'pk': self.kwargs['pk']}))


class SaveInvoicePurchaseOrderView(LoginRequiredMixin, View):

    def get_invoice(self):
        return BranchInventory.objects.get(pk=self.kwargs['invoice_id'])

    def get(self, request, *args, **kwargs):
        if 'purchase_orders' in self.request.GET:
            return JsonResponse({'text': 'Purchase order links saved',
                                 'error': False,
                                 'reload': True
                                 })
        else:
            return JsonResponse({'text': 'No purchase orders selected', 'error': True})


class InventoryItemRowsView(LoginRequiredMixin, TemplateView):
    template_name = 'purchase_order/rows.html'

    def get_invoice(self):
        return BranchInventory.objects.prefetch_related('purchase_orders').get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(InventoryItemRowsView, self).get_context_data(**kwargs)
        return context


class CalculateLineVatAmountView(View):

    def get(self, request, *args, **kwargs):
        unit_amount = self.request.GET.get('unit_amount', 0)
        quantity = self.request.GET.get('quantity', 0)
        vat_code = self.request.GET.get('vat_code', None)
        vat_amount = 0
        if vat_code and unit_amount:
            vat = VatCode.objects.get(pk=vat_code)
            if vat.percentage:
                vat_amount = (vat.percentage / 100) * (int(quantity) * float(unit_amount))
        return JsonResponse({'vat_amount': round(vat_amount, 2)})


class ImportInventoryView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'inventory/import.html'
    form_class = ImportForm

    def get_initial(self):
        initial = super(ImportInventoryView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(ImportInventoryView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_company(self):
        return Company.objects.prefetch_related(
            'company_accounts', 'company_vat_codes', 'branches', 'measures', 'measures__units', 'inventory_items'
        ).get(pk=self.request.session['company'])

    def clean_cell_value(self, cell_value):
        return str(cell_value).strip().lower().replace('/', ' ').replace('-', ' ').replace('[', ' ')

    def get_headings(self, row, group_levels):
        headings = {}
        counter = 0
        group_level_items = {}
        for cell in row:
            if cell.value:
                cell_value = self.clean_cell_value(cell.value)
                if cell_value == 'branch':
                    headings[counter] = 'branch'
                if cell_value == 'master code':
                    headings[counter] = 'code'
                if cell_value == 'master name':
                    headings[counter] = 'name'
                if cell_value == 'recipe':
                    headings[counter] = 'is_recipe'
                if cell_value == 'block':
                    headings[counter] = 'is_blocked'
                if cell_value == 'pos':
                    headings[counter] = 'pos'
                if cell_value == 'branch inventory name':
                    headings[counter] = 'branch_inventory_name'
                if cell_value == 'bar code':
                    headings[counter] = 'bar_code'
                if cell_value == 'sold as package':
                    headings[counter] = 'sold_as_package'
                group_level = group_levels.filter(name__icontains=cell_value).first()
                if group_level:
                    group_level_items[counter] = group_level.id
                    headings[counter] = 'group_level_items'
                if cell_value == 'measure' or cell_value == 'measures':
                    headings[counter] = 'measure'
                if cell_value == 'pur def unit':
                    headings[counter] = 'purchase_unit'
                if cell_value == 'supplier product code':
                    headings[counter] = 'supplier_product_code'
                if cell_value == 'cost of sale gl':
                    headings[counter] = 'cos_account_code'
                if cell_value == 'l':
                    headings[counter] = 'purchase_l'
                if cell_value == 'b':
                    headings[counter] = 'purchase_b'
                if cell_value == 'h':
                    headings[counter] = 'purchase_h'
                if cell_value == 'default cost':
                    headings[counter] = 'default_cost'
                if cell_value == 'vat input':
                    headings[counter] = 'vat_input'
                if cell_value == 'delivery fee' and not 'delivery_fee' in headings:
                    headings[counter] = 'delivery_fee'
                if cell_value == 'environment fee' and not 'environment_fee' in headings:
                    headings[counter] = 'environment_fee'
                if cell_value == 'preferred supplier':
                    headings[counter] = 'preferred_supplier'
                if cell_value == 'sale unit':
                    headings[counter] = 'sale_unit'
                if cell_value == 'discount':
                    headings[counter] = 'discount'
                if cell_value == 'sales gl':
                    headings[counter] = 'sales_account_code'
                if cell_value == 'qty pack':
                    headings[counter] = 'package_quantity'
                if cell_value == 'l':
                    headings[counter] = 'sale_l'
                if cell_value == 'b':
                    headings[counter] = 'sale_b'
                if cell_value == 'h':
                    headings[counter] = 'sale_h'
                if cell_value == 'default selling':
                    headings[counter] = 'default_sale_cost'
                if cell_value == 'default vat':
                    headings[counter] = 'output_vat'
                if cell_value == 'delivery fee':
                    headings[counter] = 'sale_delivery_fee'
                if cell_value == 'environment fee':
                    headings[counter] = 'sale_environment_fee'
                if cell_value == 'gross weight':
                    headings[counter] = 'gross_weight'
                if cell_value == 'net weight':
                    headings[counter] = 'net_weight'
                if cell_value == 'sku':
                    headings[counter] = 'sku'
                if cell_value == 'inventory gl':
                    headings[counter] = 'gl_account_code'
                if cell_value == 'stock adjust':
                    headings[counter] = 'stock_adjustment_account'
                if cell_value == 'default unit':
                    headings[counter] = 'stock_unit'
                if cell_value == 'neg stock':
                    headings[counter] = 'allow_negative_stock'
                if cell_value == 'last average':
                    headings[counter] = 'price_type'
                if cell_value == 'critical':
                    headings[counter] = 'critical'
                if cell_value == 'lowest':
                    headings[counter] = 'lowest'
                if cell_value == 'max':
                    headings[counter] = 'max_reorder'
                if cell_value == 'order level':
                    headings[counter] = 'order_level'
                if cell_value == 'min order':
                    headings[counter] = 'min_order'
                if cell_value == 'max order':
                    headings[counter] = 'max_order'
                if cell_value == 'store':
                    headings[counter] = 'store'
                if cell_value == 'rack':
                    headings[counter] = 'rack'
                if cell_value == 'bin':
                    headings[counter] = 'bin'
                if cell_value == 'register':
                    headings[counter+1] = 'register_name'
                if cell_value == 'in stock':
                    headings[counter+1] = 'in_stock'
                if cell_value == 'value of stock':
                    headings[counter+1] = 'value'
                counter += 1
        return headings, group_level_items

    def process_excel(self, csv_file, company):
        wb = load_workbook(csv_file, data_only=True)
        group_levels = GroupLevel.objects.filter(company=company)
        counter = 0
        inventory_data = []
        if 'Sheet1' in wb:
            ws = wb['Sheet1']
            headings = None
            items_group_levels = {}
            rows = ws.rows
            for _row in rows:
                if counter == 0:
                    headings, items_group_levels = self.get_headings(_row, group_levels)
                else:
                    if len(_row) > 1 and counter > 0:
                        c = 0
                        data_dict = {}
                        for cell in _row:
                            key = str(c)
                            col = headings.get(c, None)
                            if col:
                                if col == 'group_level_items' and cell.value:
                                    group_level_id = items_group_levels.get(c, None)
                                    if col in data_dict:
                                        data_dict[col].append({group_level_id: cell.value})
                                    else:
                                        data_dict[col] = [{group_level_id: cell.value}]
                                else:
                                    data_dict[col] = cell.value
                            c += 1
                        inventory_data.append(data_dict)

                counter += 1
        logger.info("----- Done uploading and loading data ------- ")
        logger.info(inventory_data[0])
        return {'inventory': inventory_data, 'total': counter}

    def get_csv_import_data(self, csv_file, company):
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        counter = 0
        data = []

        for i, line in enumerate(lines):
            if i > 0:
                fields = line.split(";")
                if len(fields) > 1 and (fields[0] and fields[1]):
                    data_dict = {'name': '', 'code': '', 'branch': '', 'branch_inventory_name': '',
                                 'group_level': '', 'group_level_item': '', 'measure': '', 'unit': '',
                                 'gl_account_code': '', 'default_cost': '', 'vat_input': '', 'sale_unit': '',
                                 'sales_account_code': '', 'output_vat': '', 'sku': '', 'cos_account_code': '',
                                 'opening_balance': ''
                                 }
                    for c, value in enumerate(fields):
                        if c == 0:
                            data_dict['code'] = value
                        if c == 1:
                            data_dict['name'] = value
                        if c == 2:
                            data_dict['branch'] = value
                        if c == 3:
                            data_dict['branch_inventory_name'] = value
                        if c == 4:
                            data_dict['group_level'] = value
                        if c == 5:
                            data_dict['group_level_item'] = value
                        if c == 6:
                            data_dict['measure'] = value
                        if c == 7:
                            data_dict['unit'] = value
                        # if c == 8:
                            # data_dict['mass'] = cell.value
                        if c == 9:
                            data_dict['gl_account_code'] = value
                        if c == 10:
                            data_dict['default_cost'] = value
                        if c == 11:
                            data_dict['vat_input'] = value
                        if c == 12:
                            data_dict['sale_unit'] = value
                        if c == 13:
                            data_dict['sales_account_code'] = value
                        # if c == 14:
                        #     data_dict['cos_account_code'] = cell.value
                        if c == 15:
                            data_dict['output_vat'] = value
                        if c == 16:
                            data_dict['sku'] = value
                        if c == 17:
                            data_dict['cos_account_code'] = value
                        if c == 18:
                            data_dict['opening_balance'] = value
                    data.append(data_dict)
        return {'account_data': data, 'total': counter}

    def form_valid(self, form):
        try:
            with transaction.atomic():
                company = self.get_company()
                csv_file = self.request.FILES["csv_file"]

                inventory_data = []
                if csv_file.name.endswith('.csv'):
                    inventory_data = self.get_csv_import_data(csv_file, company)
                elif csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx'):
                    inventory_data = self.process_excel(csv_file, company)

                period = form.cleaned_data['period']
                date = form.cleaned_data['date']
                profile = self.get_profile()
                import_result = {}
                role = Role.objects.get(id=self.request.session['role'])
                import_processor = ProcessImport(
                    company,
                    self.get_year(),
                    self.get_branch(),
                    profile,
                    role,
                    period,
                    date,
                    inventory_data
                )
                import_result = import_processor.execute()
                messages.success(self.request, '{} new inventories successfully imported.'.format(import_result['new']))

        except InventoryException as exception:
            messages.error(self.request, "Unable to upload file. {}".format(exception))
        except Exception as exception:
            messages.error(self.request, "Unexpected error occurred uploading the inventory. Please try again. {}".format(exception))
        return super(ImportInventoryView, self).form_valid(form)

    def get_success_url(self):
        return reverse('inventory_index')


class DownloadInventoryRegisterView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, **kwargs):
        branch = self.get_branch()

        inventory_qs = BranchInventory.all_inventory.select_related(
            'inventory', 'supplier', 'branch', 'sales_account', 'cost_of_sales_account', 'gl_account', 'vat_code',
            'sale_vat_code', 'measure', 'unit', 'history'
        ).filter(branch=branch).order_by('inventory__code', 'description')

        dataset = BranchInventoryResource().export(queryset=inventory_qs)

        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = f'attachment; filename="inventory_register_{str(self.get_company().slug)}.xls"'

        return HttpResponse(response)


class CopyInventoryToBranchView(ProfileMixin, FormView):

    template_name = 'inventory/copy_to_branch.html'
    form_class = CopyInventoryToBranchForm

    def get_form_kwargs(self):
        kwargs = super(CopyInventoryToBranchView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CopyInventoryToBranchView, self).get_context_data(**kwargs)
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred copying the inventory items, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CopyInventoryToBranchView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                branch = form.cleaned_data['branch']
                copy_data = form.cleaned_data.get('copy_data', False)
                inventories = self.request.POST.getlist('inventories[]', [])

                branch_inventories = BranchInventory.objects.filter(id__in=inventories)
                for branch_inventory in branch_inventories:
                    if copy_data:
                        BranchInventory.copy_from_branch_inventory(branch, branch_inventory)
                    else:
                        BranchInventory.copy_from_branch_inventory(branch, branch_inventory)

                return JsonResponse({
                    'error': False,
                    'text': 'Inventory copies successfully',
                    'reload': True
                })
            except Exception as err:
                return JsonResponse({
                    'error': True,
                    'text': 'Unable to copy inventory items, please try again',
                    'details': err.__str__(),
                    'reload': False
                })


class BranchInventoryListView(LoginRequiredMixin, ProfileMixin, View):

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_available_inventories(self, right_list):
        available_inventories = BranchInventory.objects.filter(
            branch=self.get_branch()
        ).order_by('inventory__code')
        left_list = []
        assinged_inventories_list = [inventory['inventory_id'] for inventory in right_list]
        for branch_inventory in available_inventories:
            if branch_inventory.inventory_id not in assinged_inventories_list:
                left_list.append({'id': branch_inventory.id, 'name': branch_inventory.full_name,
                                  'inventory_id': branch_inventory.inventory_id
                                  })
        return left_list

    def get_assigned_available_inventories(self, branch):
        branch_inventories = BranchInventory.objects.filter(branch=branch).order_by('inventory__code')
        right_list = []
        for branch_inventory in branch_inventories:
            right_list.append({'id': branch_inventory.id, 'name': branch_inventory.full_name,
                               'inventory_id': branch_inventory.inventory_id
                               })
        return right_list

    def branch_inventories(self, branch):
        right_list = self.get_assigned_available_inventories(branch)
        left_list = self.get_available_inventories(right_list)

        return {'right_list': right_list, 'left_list': left_list, 'error': False}

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            if 'branch_id' in self.request.GET:
                branch = Branch.objects.get(pk=self.request.GET['branch_id'])
                if branch:
                    try:
                        response = self.branch_inventories(branch)
                        return HttpResponse(json.dumps(response))
                    except Exception as exception:
                        return JsonResponse({'error': True,
                                             'text': 'Something went wrong try to get the branch inventories',
                                             'details': 'Something went wrong try to get the branch inventories {}'
                                                        ''.format(exception)
                                             })
                else:
                    return JsonResponse({'error': True,
                                         'text': 'Branch not found',
                                         'details': 'Branch not found and its pk {} '.format(self.kwargs['id'])
                                    })
            else:
                return JsonResponse({'error': True,
                                     'text': 'Please select the branch',
                                     })
        return HttpResponse('Not allowed')


class CopyYearBalancesView(LoginRequiredMixin, ProfileMixin, View):

    def get_branch_inventories(self, branch, year, **filter_options):
        return BranchInventory.objects.valuation_report(branch, year, **filter_options).order_by('inventory__code')

    def get_previous_year(self, current_year, company):
        return Year.objects.filter(company=company, year=current_year.year-1).first()

    def create_received_transaction(self, branch, period, profile, role):
        received_movement = Received.objects.filter(
            movement_type=MovementType.OPENING_BALANCE, branch=branch, period__period_year=period.period_year
        ).first()
        if not received_movement:
            return Received.objects.create(
                movement_type=MovementType.OPENING_BALANCE,
                branch=branch,
                period=period,
                date=datetime.now().date(),
                note='Copied Previous Year Closing Balance',
                profile=profile,
                role=role
            )
        return received_movement

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            branch = self.get_branch()
            year = self.get_year()
            profile = self.get_profile()
            role = get_object_or_404(Role, pk=self.request.session['role'])
            previous_year = self.get_previous_year(year, branch.company)
            period = Period.objects.get_by_date(branch.company, datetime.now().date()).first()

            received_transaction = self.create_received_transaction(branch, period, profile, role)
            if received_transaction:
                BranchInventory.objects.copy_previous_year_balances(received_transaction, branch, previous_year, year)
            return JsonResponse({'error': False, 'text': 'Copied successfully'})
        else:
            return JsonResponse({'error': True, 'text': 'Please select the branch'})


class ImportOpeningBalanceView(ProfileMixin, LoginRequiredMixin, SuccessMessageMixin, FormView):
    template_name = 'inventory/import_opening_balances.html'
    form_class = ImportOpeningBalanceForm

    def get_company(self):
        return Company.objects.filter(pk=self.request.session['company']).first()

    def get_inventory(self, inventories, inventory_code):
        return inventories.filter(
            inventory__code=inventory_code
        ).first()

    def get_branch_inventories(self, branch):
        return BranchInventory.objects.active().filter(branch=branch)

    def create_received_transaction(self, branch, period, profile, role):
        received_movement = Received.objects.filter(
            movement_type=MovementType.OPENING_BALANCE,
            branch=branch,
            period__period_year=period.period_year
        ).first()
        if not received_movement:
            return Received.objects.create(
                movement_type=MovementType.OPENING_BALANCE,
                branch=branch,
                period=period,
                date=datetime.now().date(),
                note='Import Year Opening Balance',
                profile=profile,
                role=role
            )
        return received_movement

    def import_inventory_balances(self, transaction, inventories_data):
        is_updated = 0
        is_new = 0
        total = 0
        logger.info(inventories_data)
        inventories = self.get_branch_inventories(transaction.branch)
        for inventory in inventories:
            try:
                inventory_balance = inventories_data['inventories'].get(inventory.inventory.code, None)
                logger.info(f"Inventory code {inventory.inventory.code} => exists {inventory_balance}")
                if inventory_balance:
                    is_new += 1
                    quantity = inventory_balance['quantity']
                    value = inventory_balance['value']
                    average_price = 0
                    if quantity > 0:
                        average_price = round(value / quantity, 2)
                    logger.info(f"Inventory {inventory}({inventory.id}) has quantity {quantity} and value "
                                f" --> at {value}, import to year {transaction.period.period_year}")

                    year_opening_balance, created = InventoryReceived.objects.update_or_create(
                        received=transaction,
                        inventory=inventory,
                        received__period__period_year=transaction.period.period_year,
                        defaults={'price_excluding': average_price, 'quantity': quantity}
                    )
                else:
                    year_opening_balance, created = InventoryReceived.objects.update_or_create(
                        received=transaction,
                        inventory=inventory,
                        received__period__period_year=transaction.period.period_year,
                        defaults={'price_excluding': 0, 'quantity': 0}
                    )
            except Exception as err:
                logger.exception(err)
                messages.error(self.request, err.__str__())
                # we need to log the error as they occurred and or probably display results of the upload
                print('Exception saving {}'.format(err))
            total += 1
        return {'new': is_new, 'updated': is_updated, 'total': total}

    def get_imported_data(self, csv_file, company):
        wb = load_workbook(csv_file, data_only=True)
        counter = 0
        data = {}
        if 'Sheet1' in wb:
            ws = wb['Sheet1']
            for row in ws.rows:
                if len(row) > 1 and counter > 0:
                    c = 0
                    data_dict = {'code': '', 'quantity': 0, 'value': ''}
                    for cell in row:
                        if c == 0:
                            data_dict['code'] = cell.value
                        if c == 1:
                            try:
                                logger.info(f"Converting {cell.value} to decimal failed")
                                data_dict['quantity'] = Decimal(cell.value) if cell.value else Decimal(0)
                            except ValueError as exception:
                                logger.info("Converting to decimal failed")
                                logger.exception(exception)
                                raise ValueError(f"Quantity {cell.value} could not be converted to decimal")

                        if c == 2:
                            if not cell.value or cell.value is None:
                                value = 0
                            else:
                                try:
                                    val = str(cell.value)
                                    splits = val.split('-')
                                    addition_splits = val.split('+')
                                    if len(splits) > 1:
                                        val = Decimal(float(splits[0])) - Decimal(float(splits[1]))
                                    elif len(addition_splits) > 1:
                                        val = Decimal(float(addition_splits[0])) + Decimal(float(addition_splits[1]))
                                    if val is None:
                                        value = 0
                                    else:
                                        value = Decimal(val) if val else Decimal(0)
                                except ValueError as exception:
                                    value = 0
                            data_dict['value'] = value
                        c += 1
                    data[data_dict['code']] = data_dict
                counter += 1
        return {'inventories': data, 'total': counter}

    def form_valid(self, form):
        try:
            company = self.get_company()
            csv_file = self.request.FILES["csv_file"]
            if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx')):
                messages.error(self.request, 'File is not .csv, .xlsx or xls type')
                return HttpResponseRedirect(reverse("import_inventory_opening_balances"))

            if csv_file.multiple_chunks():
                messages.error(self.request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 1000),))
                return HttpResponseRedirect(reverse("import_inventory_opening_balances"))

            opening_balances_data = []
            if csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx'):
                opening_balances_data = self.get_imported_data(csv_file, company)
            # loop over the lines and save them to the database. If there is an error store as string and display
            import_result = {}
            if len(opening_balances_data) > 0:
                branch = self.get_branch()
                year = self.get_year()
                profile = self.get_profile()
                role = get_object_or_404(Role, pk=self.request.session['role'])
                period = Period.objects.get_by_date(branch.company, datetime.now().date()).first()

                received_transaction = self.create_received_transaction(branch, period, profile, role)

                import_result = self.import_inventory_balances(received_transaction, opening_balances_data)

            messages.success(self.request, f"{import_result['new']} inventories successfully imported.")
        except ValueError as exception:
            logger.exception(exception)
            messages.error(self.request, f"Unable to import inventory balances,  {str(exception)}")
        except Exception as exception:
            logger.exception(exception)
            messages.error(self.request, f"Could not import the inventory balances, please try again")
        return super(ImportOpeningBalanceView, self).form_valid(form)

    def get_success_url(self):
        return reverse('import_inventory_opening_balances')


class InventoryItemFifoView(TemplateView):
    template_name = 'inventory/fifo.html'

    def get_fifo(self):
        total_value = 0
        total_qty = 0
        total_received = 0
        total_returned = 0
        average = 0
        returned_type = ContentType.objects.get_for_model(Returned, for_concrete_model=False)
        stock_adjustment_type = ContentType.objects.get_for_model(StockAdjustmentInventory, for_concrete_model=False)
        manufacturing_type = ContentType.objects.get_for_model(Manufacturing, for_concrete_model=False)
        received_type = ContentType.objects.get_for_model(Received, for_concrete_model=False)
        manufacturing_recipe_type = ContentType.objects.get_for_model(ManufacturingRecipe, for_concrete_model=False)
        invoice_item_type = ContentType.objects.filter(app_label='sales', model='invoiceitem').first()
        inventory_received_type = ContentType.objects.get_for_model(InventoryReceived, for_concrete_model=False)
        inventory_returned_type = ContentType.objects.get_for_model(InventoryReturned, for_concrete_model=False)

        movements = {}
        returned_ids = []
        adjustment_ids = []
        manufacturing_ids = []
        received_ids = []
        manufacturing_recipe_ids = []
        invoice_item_ids = []
        inventory_received_ids = []
        inventory_returned_ids = []
        mvts = {}
        for mvt in InventoryReceivedReturned.objects.select_related('inventory', 'received_type', 'returned_type').filter(inventory_id=self.kwargs['pk']):
            if mvt.quantity:
                if mvt.returned_object_id:
                    return_is_included = True
                    if mvt.returned_type == returned_type:
                        returned_ids.append(mvt.returned_object_id)
                        mvts[mvt] = mvt.returned_object_id
                    elif mvt.received_type == inventory_returned_type:
                        inventory_returned_ids.append(mvt.returned_object_id)
                        mvts[mvt] = mvt.returned_object_id
                    elif mvt.returned_type == stock_adjustment_type:
                        adjustment_ids.append(mvt.returned_object_id)
                        mvts[mvt] = mvt.returned_object_id
                    elif mvt.returned_type == manufacturing_type:
                        manufacturing_ids.append(mvt.returned_object_id)
                        mvts[mvt] = mvt.returned_object_id
                    elif mvt.returned_type == manufacturing_recipe_type:
                        manufacturing_recipe_ids.append(mvt.returned_object_id)
                        mvts[mvt] = mvt.returned_object_id
                    elif mvt.returned_type == invoice_item_type:
                        invoice_item_ids.append(mvt.returned_object_id)
                        mvts[mvt] = mvt.returned_object_id
                    if return_is_included:
                        quantity = mvt.quantity * -1
                        ret_value = mvt.value_returned() * -1
                        total_received += ret_value
                        total_value += ret_value
                        total_qty += quantity
                        total_returned += quantity
                if mvt.received_object_id:
                    received_is_included = True
                    if mvt.received_type == received_type:
                        received_ids.append(mvt.received_object_id)
                        mvts[mvt] = mvt.received_object_id
                    elif mvt.received_type == inventory_received_type:
                        inventory_received_ids.append(mvt.received_object_id)
                        mvts[mvt] = mvt.received_object_id
                    elif mvt.received_type == stock_adjustment_type:
                        adjustment_ids.append(mvt.received_object_id)
                        mvts[mvt] = mvt.received_object_id
                    elif mvt.received_type == manufacturing_type:
                        manufacturing_ids.append(mvt.received_object_id)
                        mvts[mvt] = mvt.received_object_id
                    elif mvt.received_type == manufacturing_recipe_type:
                        manufacturing_recipe_ids.append(mvt.received_object_id)
                        mvts[mvt] = mvt.received_object_id
                    elif mvt.received_type == invoice_item_type:
                        invoice_item_ids.append(mvt.received_object_id)
                        mvts[mvt] = mvt.received_object_id

                    if received_is_included:
                        quantity = mvt.quantity
                        rec_value = mvt.value_received()
                        total_received += rec_value
                        total_value += rec_value
                        total_qty += quantity
                        total_received += quantity

        inventory_returned = self.to_dict(items=InventoryReturned.objects.filter(pk__in=inventory_returned_ids).all())
        inventory_received = self.to_dict(items=InventoryReceived.objects.filter(pk__in=inventory_received_ids).all())
        invoice_items = self.to_dict(items=InvoiceItem.objects.select_related('invoice').filter(pk__in=invoice_item_ids).all())
        manufacturing_recipes = self.to_dict(items=ManufacturingRecipe.objects.select_related('manufacturing').filter(pk__in=manufacturing_recipe_ids).all())
        received_items = self.to_dict(items=Received.objects.filter(pk__in=received_ids).all())
        manufacturings = self.to_dict(items=Manufacturing.objects.filter(pk__in=manufacturing_ids).all())
        adjustments = self.to_dict(items=StockAdjustmentInventory.objects.select_related('adjustment').filter(pk__in=adjustment_ids).all())
        returned_items = self.to_dict(items=Returned.objects.filter(pk__in=returned_ids).all())

        for movement, movement_id in mvts.items():
            movements[movement_id] = {'mvt': movement, 'returned': None, 'received': None}
            if movement.returned_type == returned_type:
                movements[movement_id]['returned'] = returned_items.get(movement_id)
            if movement.received_type == inventory_returned_type:
                movements[movement_id]['returned'] = inventory_returned.get(movement_id)
            if movement.returned_type == stock_adjustment_type:
                movements[movement_id]['returned'] = adjustments.get(movement_id)
            if movement.returned_type == manufacturing_type:
                movements[movement_id]['returned'] = manufacturings.get(movement_id)
            if movement.returned_type == manufacturing_recipe_type:
                returned_manufacturing_recipe = manufacturing_recipes.get(movement_id)
                movements[movement_id]['returned'] = returned_manufacturing_recipe.manufacturing
            if movement.returned_type == invoice_item_type:
                movements[movement_id]['returned'] = invoice_items.get(movement_id)
            if movement.received_type == received_type:
                movements[movement_id]['received'] = received_items.get(movement_id)
            if movement.received_type == inventory_received_type:
                movements[movement_id]['received'] = inventory_received.get(movement_id)
            if movement.received_type == stock_adjustment_type:
                movements[movement_id]['received'] = adjustments.get(movement_id)
            if movement.received_type == manufacturing_type:
                movements[movement_id]['received'] = manufacturings.get(movement_id)
            if movement.received_type == manufacturing_recipe_type:
                received_manufacturing_recipe = manufacturing_recipes.get(movement_id)
                movements[movement_id]['received'] = received_manufacturing_recipe.manufacturing
            if movement.received_type == invoice_item_type:
                movements[movement_id]['received'] = invoice_items.get(movement_id)

        return movements, total_qty, total_value, average

    @staticmethod
    def to_dict(items):
        return {
            item.id: item for item in items
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # movements, total_qty, total_value, average = self.get_fifo()
        inventory = BranchInventory.objects.get(pk=self.kwargs['pk'])
        available_movements = InventoryReceivedReturned.objects.filter(
            inventory=inventory
        ).filter(
            Q(returned_object_id__isnull=True, received_object_id__isnull=False) |
            Q(received_object_id__isnull=True, returned_object_id__isnull=False)
        )

        total_value = 0
        total_qty = 0
        total_received = 0
        total_returned = 0
        for mvt in available_movements:
            if mvt.quantity:
                if mvt.returned_object_id:
                    quantity = mvt.quantity * -1
                    ret_value = mvt.value_returned() * -1
                    total_received += ret_value
                    total_value += ret_value
                    total_qty += quantity
                    total_returned += quantity
                if mvt.received_object_id:
                    quantity = mvt.quantity
                    rec_value = mvt.value_received()
                    total_received += rec_value
                    total_value += rec_value
                    total_qty += quantity
                    total_received += quantity
        average = 0
        total_value = round(total_value, 2)
        if total_value and total_qty:
            average = round(total_value / total_qty, 2)

        # context['movements'] = movements
        context['total_qty'] = total_qty
        context['total_value'] = total_value
        context['average'] = average
        context['inventory'] = inventory
        context['available_movements'] = available_movements
        return context


class ValuationSummaryView(ProfileMixin, TemplateView):
    template_name = 'inventory/valuation_summary.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        company = self.get_company()
        valuation_report = InventoryValuation(
            year=self.get_year(), company=company, branch=self.get_branch(),
            break_by='default_report', include_zeros=True
        )
        running_year = Year.objects.started().get(company=company)
        account_total = get_total_for_sub_ledger(
            year=running_year, company=company, sub_ledger=SubLedgerModule.INVENTORY,
            start_date=running_year.start_date, end_date=running_year.end_date
        )
        valuation = valuation_report.execute()
        valuation_total = valuation['total'] if valuation and valuation['total'] else 0

        context['account_total'] = account_total
        context['valuation_total'] = valuation_total
        context['is_balancing'] = valuation_total == account_total
        return context
