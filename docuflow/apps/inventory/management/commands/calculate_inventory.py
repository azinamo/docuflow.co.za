import pprint
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import Subquery, Sum, OuterRef, F, Prefetch
from django.db.models.functions import Coalesce

from docuflow.apps.company.models import Company
from docuflow.apps.inventory.models import (BranchInventory, Ledger, InventoryReceived, Balance, ManufacturingRecipe,
                                            InventoryReceivedReturned, Returned, Received, Manufacturing,
                                            InventoryReturned, StockAdjustmentInventory, StockAdjustment )
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.period.models import Year

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Clear the company journals, to restart the imports"

    def add_arguments(self, parser):
        parser.add_argument('-c', '--code', type=str, help='Indicates the inventory to be calculated')
        parser.add_argument('-i', '--inventory_id', type=str, help='Indicates the inventory_id to be calculated')

    def handle(self, *args, **kwargs):
        code = kwargs.get('code', None)
        inventory_id = kwargs.get('inventory_id', None)
        max_year = 2023
        min_year = 2020

        with transaction.atomic():
            slugs = ['41054']
            # movement_types = self.get_movement_types()
            companies = Company.objects.filter(slug__in=slugs)
            year = Year.objects.get(company__slug='41054', year=2021)
            for inventory in BranchInventory.objects.filter(branch__company__slug__in=slugs):
                closing_qty, closing_value, closing_price = inventory.recalculate_balances(year=year)

            # for company in companies:
            #     for year in Year.objects.filter(company=company, year__lt=max_year, year__gte=min_year).order_by('year'):
            #
            #         # self.inventory_movements(year=year, movement_types=movement_types)
            #
            #         try:
            #             previous_year = Year.objects.get(year=year.year - 1, company=company)
            #         except Year.DoesNotExist:
            #             previous_year = None
            #
            #         try:
            #             next_year = Year.objects.get(year=year.year + 1, company=company)
            #         except Year.DoesNotExist:
            #             next_year = None
            #
            #         qs = BranchInventory.objects.annotate(
            #             opening_quantity=Coalesce(Subquery(
            #                 InventoryReceived.objects.filter(
            #                     received__period__period_year=year, received__movement_type=MovementType.OPENING_BALANCE,
            #                     inventory_id=OuterRef('pk')
            #                 ).values('quantity')[:1]
            #             ), 0),
            #             opening_price=Coalesce(Subquery(
            #                 InventoryReceived.objects.filter(
            #                     received__period__period_year=year, received__movement_type=MovementType.OPENING_BALANCE,
            #                     inventory_id=OuterRef('id')
            #                 ).values('price_excluding')[:1]
            #             ), 0),
            #             closing_quantity=Coalesce(Subquery(
            #                 InventoryReceived.objects.filter(
            #                     received__period__period_year=next_year,
            #                     received__movement_type=MovementType.OPENING_BALANCE,
            #                     inventory_id=OuterRef('pk')
            #                 ).values('quantity')[:1]
            #             ), 0),
            #             closing_price=Coalesce(Subquery(
            #                 InventoryReceived.objects.filter(
            #                     received__period__period_year=next_year,
            #                     received__movement_type=MovementType.OPENING_BALANCE,
            #                     inventory_id=OuterRef('id')
            #                 ).values('price_excluding')[:1]
            #             ), 0),
            #             ledger_quantity=Coalesce(Subquery(
            #                 Ledger.objects.filter(
            #                     period__period_year=year, inventory_id=OuterRef('pk')
            #                 ).values('inventory_id').annotate(
            #                     l_total_quantity=Sum('quantity')
            #                 ).values('l_total_quantity')[:1]
            #             ), 0),
            #             total_quantity=F('opening_quantity') + F('ledger_quantity')
            #         ).prefetch_related(
            #             Prefetch('ledgers', Ledger.objects.filter(period__period_year=year)),
            #         ).filter(branch__company=company)
            #         if code:
            #             qs = qs.filter(inventory__code=code)
            #         if inventory_id:
            #             qs = qs.filter(pk=inventory_id)
            #
            #         for inventory in qs:
            #             print(f" Inventory {inventory} as at {year}: Next year = {next_year} vs {max_year}\r\n")
            #             inventory.inventory_movements(year=year)
            #
            #             # else:
            #             opening_qty = inventory.opening_quantity
            #             opening_price = inventory.opening_price
            #             opening_value = opening_qty * opening_price
            #
            #             closing_qty = inventory.closing_quantity
            #             closing_price = inventory.closing_price
            #             closing_value = closing_qty * closing_price
            #
            #             if next_year and next_year.year == max_year:
            #                 print("Get current balances")
            #                 closing_qty, closing_value, closing_price = inventory.calculate_summary_values()
            #
            #             Balance.objects.update_or_create(
            #                 inventory=inventory,
            #                 year=year,
            #                 defaults={
            #                     'closing_quantity': closing_qty,
            #                     'closing_price': closing_price,
            #                     'closing_value': closing_value,
            #                     'opening_quantity': opening_qty,
            #                     'opening_price': opening_price,
            #                     'opening_value': opening_value
            #                 }
            #             )
            #             print(f"\t Opening: Quantity = {opening_qty}; Price = {opening_price}; Value = {opening_value}"
            #                   f"\t Closing: Quantity = {closing_qty}; Price = {closing_price}; Value = {closing_value}"
            #                   f"  \r\n"
            #                   )

    # def get_movement_types(self):
    #     return {
    #         'returned_type': ContentType.objects.get_for_model(Returned, for_concrete_model=False),
    #         'stock_adjustment_type': ContentType.objects.get_for_model(StockAdjustmentInventory, for_concrete_model=False),
    #         'manufacturing_type': ContentType.objects.get_for_model(Manufacturing, for_concrete_model=False),
    #         'received_type': ContentType.objects.get_for_model(Received, for_concrete_model=False),
    #         'manufacturing_recipe_type': ContentType.objects.get_for_model(ManufacturingRecipe, for_concrete_model=False),
    #         'invoice_item_type': ContentType.objects.filter(app_label='sales', model='invoiceitem').first(),
    #         'inventory_received_type': ContentType.objects.get_for_model(InventoryReceived, for_concrete_model=False),
    #         'inventory_returned_type': ContentType.objects.get_for_model(InventoryReturned, for_concrete_model=False)
    #     }
    #
    # def inventory_movements(self, year, movement_types):
    #     total_value = 0
    #     total_qty = 0
    #     total_received = 0
    #     total_returned = 0
    #
    #     returned_type = movement_types['returned_type']
    #     stock_adjustment_type = movement_types['stock_adjustment_type']
    #     manufacturing_type = movement_types['manufacturing_type']
    #     received_type = movement_types['received_type']
    #     manufacturing_recipe_type = movement_types['manufacturing_recipe_type']
    #     invoice_item_type = movement_types['invoice_item_type']
    #     inventory_received_type = movement_types['inventory_received_type']
    #     inventory_returned_type = movement_types['inventory_returned_type']
    #
    #     for mvt in InventoryReceivedReturned.objects.all():
    #         if mvt.quantity:
    #             if mvt.returned_object_id:
    #                 return_is_included = True
    #                 if mvt.returned_type == returned_type:
    #                     returned = Returned.objects.get(pk=mvt.returned_object_id)
    #                     return_is_included = year and (returned.period.period_year == year)
    #                     mvt.returned_period = returned.period
    #                     mvt.save()
    #                 elif mvt.received_type == inventory_returned_type:
    #                     inventory_returned = mvt.received_object.returned
    #                     return_is_included = year and (inventory_returned.period.period_year == year)
    #                     mvt.returned_period = inventory_returned.period
    #                     mvt.save()
    #                 elif mvt.returned_type == stock_adjustment_type:
    #                     stock_adjustment_inventory = StockAdjustmentInventory.objects.get(pk=mvt.returned_object_id)
    #                     return_is_included = year and (stock_adjustment_inventory.adjustment.period.period_year == year)
    #                     mvt.returned_period = stock_adjustment_inventory.adjustment.period
    #                     mvt.save()
    #                 elif mvt.returned_type == manufacturing_type:
    #                     manufacturing = Manufacturing.objects.get(pk=mvt.returned_object_id)
    #                     return_is_included = year and (manufacturing.period.period_year == year)
    #                     mvt.returned_period = manufacturing.adjustment.period
    #                     mvt.save()
    #                 elif mvt.returned_type == manufacturing_recipe_type:
    #                     manufacturing_recipe = ManufacturingRecipe.objects.get(pk=mvt.returned_object_id)
    #                     return_is_included = year and (manufacturing_recipe.manufacturing.period.period_year == year)
    #                     mvt.returned_period = manufacturing_recipe.manufacturing.period
    #                     mvt.save()
    #                 elif mvt.returned_type == invoice_item_type:
    #                     return_is_included = year and (mvt.returned_object.invoice.period.period_year == year)
    #                     mvt.returned_period = mvt.returned_object.invoice.period
    #                     mvt.save()
    #
    #                 if return_is_included:
    #                     quantity = mvt.quantity * -1
    #                     ret_value = mvt.value_returned() * -1
    #                     total_received += ret_value
    #                     total_value += ret_value
    #                     total_qty += quantity
    #                     total_returned += quantity
    #             if mvt.received_object_id:
    #                 received_is_included = True
    #                 if mvt.received_type == received_type:
    #                     received = Received.objects.get(pk=mvt.received_object_id)
    #                     received_is_included = year and (received.period.period_year == year)
    #                     mvt.received_period = received.period
    #                     mvt.save()
    #                 elif mvt.received_type == inventory_received_type:
    #                     inventory_received = mvt.received_object.received
    #                     received_is_included = year and (inventory_received.period.period_year == year)
    #                     mvt.received_period = inventory_received.period
    #                     mvt.save()
    #                 elif mvt.received_type == stock_adjustment_type:
    #                     stock_adjustment_inventory = StockAdjustmentInventory.objects.get(pk=mvt.received_object_id)
    #                     received_is_included = year and (stock_adjustment_inventory.adjustment.period.period_year == year)
    #                     mvt.received_period = stock_adjustment_inventory.adjustment.period
    #                     mvt.save()
    #                 elif mvt.received_type == manufacturing_type:
    #                     manufacturing = Manufacturing.objects.get(pk=mvt.received_object_id)
    #                     received_is_included = year and (manufacturing.period.period_year == year)
    #                     mvt.received_period = manufacturing.period
    #                     mvt.save()
    #                 elif mvt.received_type == manufacturing_recipe_type:
    #                     manufacturing_recipe = ManufacturingRecipe.objects.get(pk=mvt.received_object_id)
    #                     received_is_included = year and (manufacturing_recipe.manufacturing.period.period_year == year)
    #                     mvt.received_period = manufacturing_recipe.manufacturing.period
    #                     mvt.save()
    #                 elif mvt.received_type == invoice_item_type:
    #                     received_is_included = year and (mvt.received_object.invoice.period.period_year == year)
    #                     mvt.received_period = mvt.received_object.invoice.period
    #                     mvt.save()
    #
    #                 if received_is_included:
    #                     quantity = mvt.quantity
    #                     rec_value = mvt.value_received()
    #                     total_received += rec_value
    #                     total_value += rec_value
    #                     total_qty += quantity
    #                     total_received += quantity
    #     average = 0
    #     total_value = round(total_value, 2)
    #     if total_value and total_qty:
    #         average = round(total_value / total_qty, 2)
    #     return total_qty, total_value, average
