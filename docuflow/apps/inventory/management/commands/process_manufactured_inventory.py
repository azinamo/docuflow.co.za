import pprint

from django.core.management.base import BaseCommand
from django.db import transaction

from docuflow.apps.inventory.enums import ManufacturingStatus
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import Manufacturing
from docuflow.apps.inventory.modules.manufacturing.services import return_recipe_inventory, received_inventory
from docuflow.apps.inventory.services import is_valuation_balancing

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Process all pending manufacturing inventory"

    def add_arguments(self, parser):
        parser.add_argument('-c', '--company', type=str, help='Indicates the company to be calculated')
        parser.add_argument('-i', '--manufacturing_id', type=int, default=None, help='Indicates the manufacturing_id to be calculated')

    def handle(self, *args, **kwargs):
        with transaction.atomic():
            qs = Manufacturing.objects.filter(status=ManufacturingStatus.PROCESSING)
            if kwargs['manufacturing_id']:
                qs = qs.filter(pk=kwargs['manufacturing_id'])
            for manufacturing in qs:
                self.stdout.write(self.style.SUCCESS(f"Started processing manufacturing {manufacturing}"))
                for manufacturing_recipe in manufacturing.recipe_items.all():
                    inventory = manufacturing_recipe.recipe.inventory

                    total_cost = return_recipe_inventory(
                        inventory=inventory,
                        manufacturing_recipe=manufacturing_recipe
                    )

                    manufacturing_recipe.value = total_cost
                    manufacturing_recipe.save()

                    received_inventory(
                        inventory=inventory,
                        manufacturing_recipe=manufacturing_recipe,
                        total_cost=total_cost
                    )

                manufacturing.complete()

                if not is_valuation_balancing(branch=manufacturing.branch, year=manufacturing.period.period_year):
                    raise InventoryException('A variation in inventory accounts and inventory valuation detected.')
