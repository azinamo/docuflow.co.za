import pprint
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import Subquery, Sum, OuterRef, F, Prefetch
from django.db.models.functions import Coalesce

from docuflow.apps.company.models import Company
from docuflow.apps.inventory.models import (BranchInventory, Ledger, InventoryReceived, Balance, ManufacturingRecipe,
                                            InventoryReceivedReturned, Returned, Received, Manufacturing,
                                            InventoryReturned, StockAdjustmentInventory, StockAdjustment )
from docuflow.apps.inventory.enums import MovementType
from docuflow.apps.period.models import Year

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Clear the company journals, to restart the imports"

    def add_arguments(self, parser):
        parser.add_argument('-c', '--code', type=str, help='Indicates the inventory to be calculated')
        parser.add_argument('-i', '--inventory_id', type=str, help='Indicates the inventory_id to be calculated')

    def handle(self, *args, **kwargs):
        code = kwargs.get('code', None)
        inventory_id = kwargs.get('inventory_id', None)
        with transaction.atomic():
            slugs = ['41054']
            companies = Company.objects.filter(slug__in=slugs)
            for company in companies:
                qs = BranchInventory.objects.filter(branch__company=company)
                if code:
                    qs = qs.filter(inventory__code=code)
                if inventory_id:
                    qs = qs.filter(pk=inventory_id)

                for inventory in qs:
                    in_stock = inventory.in_stock
                    total_qty, total_value, average = inventory.calculate_summary_values()

                    if total_qty != in_stock:
                        print(f"Inventory item {inventory}")
                        print(f"\t Quantity(Value ){in_stock}({inventory.total_value}) in stock vs Qty(Val) {total_qty}({total_value}) calculated in stock. \r\n")
                        print(f"\t Difference in stock = ({total_qty - in_stock}). Value = {total_value - inventory.total_value} \r\n")
