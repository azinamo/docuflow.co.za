import logging

"""
Keep helper functions not related to specific module.
"""
from django.db.models import Q

logger = logging.getLogger(__name__)


def prepare_opening_closing_balance(cls, opening_stock, quantity, data=None):
    if not data:
        data = {}
    opening = 0
    closing = 0
    if opening_stock:
        opening = float(opening_stock)
    if quantity:
        closing = opening + float(quantity)
    if cls:
        data[cls.pk] = {'opening': opening, 'closing': closing}
    return data


def add_inventory_filters(filters, branch):
    q_object = Q()
    q_object.add(Q(branch=branch), Q.AND)
    for f in filters:
        q_object.add(construct_search_query(f['field'], f['value']))
    return q_object


def construct_search_query(field, value):
    q_object = Q()
    if field == 'inventory_code':
        q_object.add(Q(inventory__code__icontains=value), Q.AND)
    if field == 'description':
        q_object.add(Q(description__icontains=value), Q.AND)
    if field == 'supplier':
        q_object.add(Q(supplier__name__icontains=value) | Q(supplier__code__icontains=value), Q.AND)
    if field == 'gl_account':
        q_object.add(Q(gl_account__name__icontains=value) | Q(gl_account__code__icontains=value), Q.AND)
    if field == 'sales_account':
        q_object.add(Q(sales_account__name__icontains=value) | Q(sales_account__code__icontains=value), Q.AND)
    if field == 'cost_of_sales_account':
        q_object.add(Q(cost_of_sales_account__name__icontains=value) | Q(cost_of_sales_account__code__icontains=value), Q.AND)
    if field == 'is_recipe':
        q_object.add(Q(is_recipe=value), Q.AND)
    return q_object
