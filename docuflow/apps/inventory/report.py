import pprint
from decimal import Decimal

from docuflow.apps.inventory.modules.ledger.services import GenerateLedger
from .models import GroupLevel, BranchInventory, PurchaseHistory


pp = pprint.PrettyPrinter(indent=4)


class InventoryValuation(object):
    """ Generates inventory valuation report """

    def __init__(self, year, branch, from_inventory=None, to_inventory=None, break_by=None):
        self.year = year
        self.branch = branch
        self.from_inventory = from_inventory
        self.to_inventory = to_inventory
        self.break_by = break_by

    def execute(self):
        if self.break_by in ['group_levels', 'parent_no_totals', 'parent_and_children_totals']:
            return self.get_break_by_group_levels()
        elif self.break_by == 'parent_totals':
            return self.get_break_down_parent_total()
        elif self.break_by == 'parent':
            return self.get_break_down_parent()
        elif self.break_by == 'children_totals':
            return self.get_break_down_children_total()
        else:
            return self.get_default_report()

    def get_inventories(self):
        filter_options = {}
        end_date = self.year.end_date
        if (self.to_inventory and self.from_inventory) and (self.to_inventory == self.from_inventory):
            filter_options['inventory__code__lte'] = self.to_inventory
        else:
            if self.from_inventory:
                filter_options['inventory__code__gte'] = self.from_inventory
            if self.to_inventory:
                filter_options['inventory__code__lte'] = self.to_inventory
        if self.branch:
            filter_options['branch'] = self.branch

        inventories = BranchInventory.objects.select_related(
            'history', 'inventory', 'branch',
        ).prefetch_related(
            'group_level_items', 'group_level_items__group_level'
        ).filter(**filter_options)

        return inventories

    def get_report_group_levels(self):
        return GroupLevel.objects.eligible_parents(self.branch.company)

    def get_group_levels_shell(self):
        report_shell = {'total': 0, 'total_quantity': 0, 'levels': {}}
        group_levels = self.get_report_group_levels()
        if len(group_levels) > 0:
            for group_level in group_levels:
                for group_level_item in group_level.items.all():
                    report_shell['levels'][group_level_item] = {'total': 0, 'total_quantity': 0, 'level_items': {}}
                    for child_level_item in group_level_item.children.all():
                        report_shell['levels'][group_level_item]['level_items'][child_level_item] = {'total': 0,
                                                                                                     'total_quantity': 0,
                                                                                                     'inventory_items': {}}
        return report_shell

    def get_break_by_group_levels(self):
        inventories = self.get_inventories()
        report = self.get_group_levels_shell()
        for inventory in inventories:
            total_value = inventory.total_value
            total_quantity = inventory.in_stock
            for level_item in inventory.group_level_items.all():
                parent_level_item = level_item.parent
                if parent_level_item in report['levels']:
                    report['levels'][parent_level_item]['total'] += total_value
                    report['levels'][parent_level_item]['total_quantity'] += total_quantity
                    report['levels'][parent_level_item]['level_items'][level_item]['total'] += total_value
                    report['levels'][parent_level_item]['level_items'][level_item]['total_quantity'] += total_quantity

                    report['levels'][parent_level_item]['level_items'][level_item]['inventory_items'][inventory] = inventory

        report['total'] = sum(g_level['total'] for g_level in report['levels'].values())
        report['total_quantity'] = sum(g_level['total_quantity'] for g_level in report['levels'].values())
        return report

    def get_break_down_parent(self):
        inventories = self.get_inventories()
        report = self.get_group_levels_shell()
        for inventory in inventories:
            total_value = inventory.total_value
            total_quantity = inventory.in_stock
            for level_item in inventory.group_level_items.all():
                parent_level_item = level_item.parent
                if parent_level_item in report['levels']:
                    report['levels'][parent_level_item]['total'] += total_value
                    report['levels'][parent_level_item]['total_quantity'] += total_quantity
                    report['levels'][parent_level_item]['level_items'][level_item]['total'] += total_value
                    report['levels'][parent_level_item]['level_items'][level_item]['total_quantity'] += total_quantity

        report['total'] = sum(g_level['total'] for g_level in report['levels'].values())
        report['total_quantity'] = sum(g_level['total_quantity'] for g_level in report['levels'].values())
        return report

    def get_break_down_parent_total(self):
        inventories = self.get_inventories()
        report = self.get_group_levels_shell()
        for inventory in inventories:
            total_value = inventory.total_value
            total_quantity = inventory.in_stock
            for level_item in inventory.group_level_items.all():
                parent_level_item = level_item.parent
                if parent_level_item in report['levels']:
                    report['levels'][parent_level_item]['total'] += total_value
                    report['levels'][parent_level_item]['total_quantity'] += total_quantity

        report['total'] = sum(g_level['total'] for g_level in report['levels'].values())
        report['total_quantity'] = sum(g_level['total_quantity'] for g_level in report['levels'].values())
        return report

    def get_break_down_children_total(self):
        inventories = self.get_inventories()
        report = self.get_group_levels_shell()
        for inventory in inventories:
            total_value = inventory.total_value
            total_quantity = inventory.in_stock
            for level_item in inventory.group_level_items.all():
                parent_level_item = level_item.parent
                if parent_level_item in report['levels']:
                    report['levels'][parent_level_item]['total'] += total_value
                    report['levels'][parent_level_item]['total_quantity'] += total_quantity
                    report['levels'][parent_level_item]['level_items'][level_item]['total'] += total_value
                    report['levels'][parent_level_item]['level_items'][level_item]['total_quantity'] += total_quantity

        report['total'] = sum(g_level['total'] for g_level in report['levels'].values())
        report['total_quantity'] = sum(g_level['total_quantity'] for g_level in report['levels'].values())
        return report

    def get_default_report(self):
        report = {'total': Decimal(0), 'total_quantity': Decimal(0), 'inventories': {}}
        inventories = self.get_inventories()
        for inventory in inventories:
            report['total'] += inventory.in_stock
            report['total_quantity'] += inventory.in_stock

            report['inventories'][inventory] = inventory
        return report
