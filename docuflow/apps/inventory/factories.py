# from django.utils.timezone import now
import datetime
import factory

from django.utils.timezone import now

from . import models


class GroupLevelFactory(factory.DjangoModelFactory):
    company = factory.SubFactory('docuflow.apps.company.factories.CompanyFactory')
    #  parent = factory.SubFactory('GroupLevelFactory')

    class Meta:
        model = models.GroupLevel


class GroupLevelItemFactory(factory.DjangoModelFactory):
    group_level = factory.SubFactory(GroupLevelFactory)


class InventoryFactory(factory.DjangoModelFactory):
    company = factory.SubFactory('docuflow.apps.company.factories.CompanyFactory')

    class Meta:
        model = models.Inventory
        django_get_or_create = ('code', )

    description = factory.Sequence(lambda n: "Base Oxygen - %s" % n)
    code = factory.Sequence(lambda n: "OXY - %s" % n)


class BranchInventoryFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)
    branch = factory.SubFactory('docuflow.apps.company.factories.BranchFactory')
    parent_branch = factory.SubFactory('docuflow.apps.company.factories.BranchFactory')
    sales_account = factory.SubFactory('docuflow.apps.company.factories.AccountFactory')
    cost_of_sales_account = factory.SubFactory('docuflow.apps.company.factories.AccountFactory')
    gl_account = factory.SubFactory('docuflow.apps.company.factories.InventoryControlAccountFactory')
    stock_adjustment_account = factory.SubFactory('docuflow.apps.company.factories.AccountFactory')
    vat_code = factory.SubFactory('docuflow.apps.company.factories.VatCodeFactory')
    sale_vat_code = factory.SubFactory('docuflow.apps.company.factories.VatCodeFactory')
    measure = factory.SubFactory('docuflow.apps.company.factories.MeasureFactory')
    unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')
    stock_unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')
    supplier = factory.SubFactory('docuflow.apps.supplier.factories.SupplierFactory')

    class Meta:
        model = models.BranchInventory
        django_get_or_create = ('description', )

    description = factory.Sequence(lambda n: "Oxygen %s" % n)
    is_blocked = False

    @factory.post_generation
    def bootstrap(self, create, extracted, **kwargs):
        # add purchase history
        history = PurchaseHistoryFactory(inventory=self)
        if 'in_stock' in extracted:
            history.in_stock = extracted.get('in_stock')
        history.save()


class BranchRecipeInventoryFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)
    branch = factory.SubFactory('docuflow.apps.company.factories.BranchFactory')
    parent_branch = factory.SubFactory('docuflow.apps.company.factories.BranchFactory')
    sales_account = factory.SubFactory('docuflow.apps.company.factories.AccountFactory')
    cost_of_sales_account = factory.SubFactory('docuflow.apps.company.factories.AccountFactory')
    gl_account = factory.SubFactory('docuflow.apps.company.factories.AccountFactory')
    stock_adjustment_account = factory.SubFactory('docuflow.apps.company.factories.AccountFactory')
    vat_code = factory.SubFactory('docuflow.apps.company.factories.VatCodeFactory')
    sale_vat_code = factory.SubFactory('docuflow.apps.company.factories.VatCodeFactory')
    measure = factory.SubFactory('docuflow.apps.company.factories.MeasureFactory')
    unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')
    stock_unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')
    supplier = factory.SubFactory('docuflow.apps.supplier.factories.SupplierFactory')
    is_recipe_item = True

    class Meta:
        model = models.BranchInventory


class PurchaseHistoryFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(BranchInventoryFactory)
    supplier = factory.SubFactory('docuflow.apps.supplier.factories.SupplierFactory')

    class Meta:
        model = models.PurchaseHistory
        django_get_or_create = ('inventory', 'supplier', )

    in_stock = 1000
    average_price = 10


class RecipeFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)
    profile = factory.SubFactory('docuflow.apps.accounts.factories.ProfileFactory')
    role = factory.SubFactory('docuflow.apps.accounts.factories.AccountantRoleFactory')

    class Meta:
        model = models.Recipe
        django_get_or_create = ('inventory', )


class RecipeInventoryFactory(factory.DjangoModelFactory):
    recipe = factory.SubFactory(RecipeFactory)
    inventory = factory.SubFactory(BranchRecipeInventoryFactory)
    unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')

    class Meta:
        model = models.RecipeInventory
        django_get_or_create = ('recipe', 'inventory', 'unit')
    quantity = 100.001


class PurchaseOrderFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory('docuflow.apps.sales.factories.InvoiceFactory')

    class Meta:
        model = models.PurchaseOrder
        django_get_or_create = ('invoice', )


class PurchaseOrderInventory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(BranchInventoryFactory)
    purchase_order = factory.SubFactory(PurchaseOrderFactory)
    invoice = factory.SubFactory('docuflow.apps.invoice.factories.InvoiceFactory')
    unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')
    vat_code = factory.SubFactory('docuflow.apps.company.factories.VatCodeFactory')

    class Meta:
        model = models.PurchaseOrderInventory


class TransactionFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory('docuflow.apps.company.factories.BranchFactory')
    period = factory.SubFactory('docuflow.apps.period.factories.PeriodFactory')
    supplier = factory.SubFactory('docuflow.apps.supplier.factories.SupplierFactory')
    profile = factory.SubFactory('docuflow.apps.accounts.factories.ProfileFactory')
    role = factory.SubFactory('docuflow.apps.accounts.factories.AccountantRoleFactory')
    date = datetime.date(2020, 1, 1)

    class Meta:
        model = models.Transaction
        django_get_or_create = ('period', 'supplier', 'profile', 'role')


class ReceivedFactory(factory.DjangoModelFactory):
    # transaction = factory.SubFactory(TransactionFactory)
    period = factory.SubFactory('docuflow.apps.period.factories.PeriodFactory')
    supplier = factory.SubFactory('docuflow.apps.supplier.factories.SupplierFactory')
    profile = factory.SubFactory('docuflow.apps.accounts.factories.ProfileFactory')
    role = factory.SubFactory('docuflow.apps.accounts.factories.AccountantRoleFactory')
    date = datetime.date(2020, 1, 1)
    total_amount = 1200
    total_vat = 200
    note = 'Default Note'

    class Meta:
        model = models.Received


class ReturnedFactory(factory.DjangoModelFactory):
    received_movement = factory.SubFactory(ReceivedFactory)
    invoice = factory.SubFactory('docuflow.apps.sales.factories.Invoice')

    class Meta:
        model = models.Returned


class StockAdjustmentFactory(factory.DjangoModelFactory):

    class Meta:
        model = models.StockAdjustment


# class OpeningBalanceFactory(factory.DjangoModelFactory):
#
#     class Meta:
#         model = models.OpeningBalance


class InventoryReceivedFactory(factory.DjangoModelFactory):
    received = factory.SubFactory(ReceivedFactory)
    inventory = factory.SubFactory(BranchInventoryFactory)
    invoice_item = factory.SubFactory('docuflow.apps.sales.factories.InvoiceItemFactory')
    unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')
    vat = factory.SubFactory('docuflow.apps.company.factories.VatCodeFactory')

    class Meta:
        model = models.InventoryReceived


class InventoryReturnedFactory(factory.DjangoModelFactory):
    returned = factory.SubFactory(ReturnedFactory)
    inventory = factory.SubFactory(BranchInventoryFactory)
    invoice_item = factory.SubFactory('docuflow.apps.sales.factories.InvoiceItemFactory')
    unit = factory.SubFactory('docuflow.apps.company.factories.UnitFactory')
    vat = factory.SubFactory('docuflow.apps.company.factories.VatCodeFactory')

    class Meta:
        model = models.InventoryReturned


class InventoryReceivedReturnedFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(InventoryFactory)

    class Meta:
        model = models.InventoryReceivedReturned
        django_get_or_create = ('inventory', )

    quantity = 2
    price = 10
    balance = 10
    received_value = 12
    returned_value = 15


class StockCountFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory('docuflow.apps.company.factories.BranchFactory')

    class Meta:
        model = models.StockCount
        django_get_or_create = ('branch', )

    date = factory.Faker('date')
    captured_by = factory.Faker('name')


class InventoryStockCountFactory(factory.DjangoModelFactory):
    stock_count = factory.SubFactory(StockCountFactory)
    inventory = factory.SubFactory(BranchInventoryFactory)

    class Meta:
        model = models.InventoryStockCount
        django_get_or_create = ('stock_count', 'inventory')

    quantity = 10
    original_quantity = 0


class LedgerFactory(factory.DjangoModelFactory):
    inventory = factory.SubFactory(BranchInventoryFactory)
    period = factory.SubFactory('docuflow.apps.period.factories.PeriodFactory')
    journal = factory.SubFactory('docuflow.apps.journal.factories.JournalFactory')
    created_by = factory.SubFactory('docuflow.apps.accounts.factories.ProfileFactory')

    class Meta:
        model = models.Ledger


class ManufacturingFactory(factory.DjangoModelFactory):
    branch = factory.SubFactory('docuflow.apps.company.factories.BranchFactory')
    period = factory.SubFactory('docuflow.apps.period.factories.PeriodFactory')
    profile = factory.SubFactory('docuflow.apps.accounts.factories.ProfileFactory')
    role = factory.SubFactory('docuflow.apps.accounts.factories.AccountantRoleFactory')

    class Meta:
        model = models.Manufacturing
        django_get_or_create = ('branch', 'period', 'profile', 'role')

    date = now().date()
    description = 'Lintels Manufactured'


class ManufacturingRecipeFactory(factory.DjangoModelFactory):
    manufacturing = factory.SubFactory(ManufacturingFactory)
    recipe = factory.SubFactory(ReceivedFactory)

    class Meta:
        model = models.ManufacturingRecipe
