from django.apps import AppConfig


class BudgetaccountConfig(AppConfig):
    name = 'docuflow.apps.budget.modules.budgetaccount'
