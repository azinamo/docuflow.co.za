from django.urls import path

from . import views

urlpatterns = [
    path('<int:budget_id>/account/<int:account_id>/detail/', views.AccountBudgetDetailView.as_view(),
         name='account_budget_detail'),
    path('save/<int:budget_id>/<int:account_id>/budget/', views.SaveAccountBudgetView.as_view(),
         name='save_account_budget'),
    path('<int:pk>/', views.CreateAccountsBudgetView.as_view(), name='create_accounts_budget'),
    path('<int:budget_id>/accounts/', views.BudgetAccountView.as_view(), name='budget_accounts'),
    path('edit/budget/<int:budget_id>/account/<int:account_id>/', views.EditBudgetAccountView.as_view(),
         name='edit_budget_account')
]
