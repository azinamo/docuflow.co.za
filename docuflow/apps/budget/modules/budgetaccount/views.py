import logging
import pprint
from decimal import Decimal

from django.views.generic import View, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.db import transaction
from django.db.models import Sum
from django.core.paginator import Paginator

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.budget.models import Budget, BudgetAccount, BudgetAccountPeriod
from docuflow.apps.company.models import Account
from docuflow.apps.period.models import Period

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


# Create your views here.
class BudgetAccountPeriodMixin(object):

    def get_year_periods(self, year):
        return Period.objects.prefetch_related(
            'budgets'
        ).filter(
            period_year=year
        ).order_by('period')

    def get_period_with_amounts(self, periods, account):
        period_amounts = {}
        total_amount = Decimal('0')
        for period in periods:
            budget_period = period.budgets.filter(budget_account__account=account).first()
            period_amounts[period] = budget_period
            total_amount += budget_period.amount if budget_period and budget_period.amount else Decimal(0)
        return period_amounts, total_amount


class CreateAccountsBudgetView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'budgetaccount/create.html'

    def get_budget(self):
        return Budget.objects.filter(
            pk=self.kwargs['pk']
        ).first()

    def get_context_data(self, **kwargs):
        context = super(CreateAccountsBudgetView, self).get_context_data(**kwargs)
        budget = self.get_budget()
        context['budget'] = budget
        context['page'] = 1
        context['total_pages'] = Account.objects.filter(
            company=budget.year.company,
            is_active=True
        ).exclude(
            code='',
            name=''
        ).count()
        return context


class BudgetAccountView(BudgetAccountPeriodMixin, TemplateView):
    template_name = 'budgetaccount/budget.html'

    def get_budget(self):
        return Budget.objects.select_related(
            'year',
            'year__company'
        ).prefetch_related(
            'accounts',
        ).filter(
            pk=self.kwargs['budget_id']
        ).first()

    def get_account(self, company, page, year):
        try:
            limit = 1
            start = limit * (page - 1)
            logger.info("start -- {}, limit {}".format(start, limit))
            accounts = Account.objects.prefetch_related(
                'budgets'
            ).filter(
                company=company,
                is_active=True
            ).exclude(
                code='',
                name=''
            ).order_by(
                'code'
            ).all()
            paginator = Paginator(accounts, 1)  # Show 25 contacts per page
            accounts = paginator.get_page(page)
            return accounts[0]
        except Exception as exception:
            logger.info("Exception")
            logger.info(exception)
            return None


    def get_context_data(self, **kwargs):
        context = super(BudgetAccountView, self).get_context_data(**kwargs)
        budget = self.get_budget()
        page = int(self.request.GET.get('page', 1))
        account = self.get_account(budget.year.company, page, budget.year)
        periods = self.get_year_periods(budget.year)
        periods_with_amounts, total_amount = self.get_period_with_amounts(periods, account)
        logger.info('logger.info(periods_with_amounts)')
        logger.info(periods_with_amounts)
        context['budget'] = budget
        context['account'] = account
        context['page'] = page
        context['total_amount'] = total_amount
        context['periods'] = periods_with_amounts
        return context


class SaveAccountBudgetView(ProfileMixin, View):

    def get_budget_account(self, budget_id, account_id):
        budget_account = BudgetAccount.objects.filter(
            account_id=account_id,
            budget_id=budget_id
        ).first()
        if not budget_account:
            budget_account = BudgetAccount.objects.create(
                account_id=account_id,
                budget_id=budget_id
            )
        return budget_account

    def post(self, request, **kwargs):
        try:
            with transaction.atomic():
                account_id = self.kwargs.get('account_id')
                budget_id = self.kwargs.get('budget_id')
                budget_account = self.get_budget_account(budget_id, account_id)
                logger.info("Budget account --> {}".format(budget_account))
                if budget_account:
                    logger.info("Budget account found --> {}".format(budget_account))
                    for field, value in self.request.POST.items():
                        if field.startswith('period', 0, 6) and value != '':
                            logger.info("Period --> {}".format(field[0:7]))
                            period_id = field[7:]
                            if not period_id:
                                raise ValueError('Period not found')
                            amount = float(value)
                            logger.info("Period id {} --> amount = {}".format(period_id, amount))
                            account_period = BudgetAccountPeriod.objects.filter(
                                budget_account=budget_account,
                                period_id=period_id
                            ).first()
                            if not account_period:
                                BudgetAccountPeriod.objects.create(
                                    period_id=period_id,
                                    budget_account=budget_account,
                                    amount=amount
                                )
                            else:
                                account_period.amount = amount
                                account_period.save()

                return JsonResponse({
                    'text': 'Account budget saved successfully',
                    'error': False,
                    'reload': True
                })
        except ValueError as exception:
            logger.exception('Account budget could not be saved')
            return JsonResponse({
                'text': str(exception),
                'error': True,
                'details': exception.__str__()
            })
        except Exception as exception:
            logger.exception('Account budget could not be saved')
            return JsonResponse({
                'text': 'Account budget could not be saved',
                'error': True,
                'details': exception.__str__()
            })


class AccountBudgetDetailView(TemplateView):
    template_name = 'budgetaccount/account_budget.html'

    def get_budget(self):
        return Budget.objects.select_related(
            'year'
        ).prefetch_related(
            'accounts'
        ).filter(
            pk=self.kwargs['budget_id']
        ).first()

    def get_account(self):
        return BudgetAccount.objects.annotate(
            budget_total=Sum('period_amounts__amount')
        ).prefetch_related(
            'period_amounts',
            'period_amounts__period'
        ).select_related(
            'account'
        ).filter(
            account_id=self.kwargs['account_id'],
            budget_id=self.kwargs['budget_id']
        ).first()

    def get_context_data(self, **kwargs):
        context = super(AccountBudgetDetailView, self).get_context_data(**kwargs)
        budget = self.get_budget()
        budget_account = self.get_account()
        logger.info("BudgetAccount")
        logger.info("BudgetAccount amount {}".format(budget_account.budget_total))
        for account_period in budget_account.period_amounts.all():
            logger.info("Amount --> {} --> {}".format(account_period.amount, account_period.id))
        context['budget'] = budget
        context['budget_account'] = budget_account
        context['account_periods'] = budget_account.period_amounts
        return context


class EditBudgetAccountView(BudgetAccountPeriodMixin, TemplateView):
    template_name = 'budgetaccount/edit_budget.html'

    def get_budget_account(self):
        return BudgetAccount.objects.annotate(
            budget_total=Sum('period_amounts__amount')
        ).prefetch_related(
            'period_amounts',
            'period_amounts__period'
        ).select_related(
            'account'
        ).filter(
            account_id=self.kwargs['account_id'],
            budget_id=self.kwargs['budget_id']
        ).first()

    def get_account(self):
        return Account.objects.prefetch_related(
            'budgets'
        ).filter(
            pk=self.kwargs['account_id']
        ).first()

    def get_budget(self):
        return Budget.objects.filter(
            pk=self.kwargs['budget_id']
        ).first()

    def get_context_data(self, **kwargs):
        context = super(EditBudgetAccountView, self).get_context_data(**kwargs)
        budget_account = self.get_budget_account()
        if budget_account:
            account = budget_account.account
            periods = self.get_year_periods(budget_account.budget.year)
            periods_with_amounts, total_amount = self.get_period_with_amounts(periods, account)
            context['account'] = budget_account.account
            context['budget'] = budget_account.budget
        else:
            account = self.get_account()
            budget = self.get_budget()
            periods = self.get_year_periods(budget.year)
            periods_with_amounts, total_amount = self.get_period_with_amounts(periods, account)
            context['account'] = account
            context['budget'] = budget
        context['budget_account'] = budget_account
        context['total_amount'] = total_amount
        context['account_periods'] = periods_with_amounts
        return context
