from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.BudgetsView.as_view(), name='budget_index'),
    path('<int:year_id>/year/', views.BudgetObjectListView.as_view(), name='view_year_budget'),
    path('create/<int:year_id>/', views.CreateBudgetView.as_view(), name='create_year_budget'),
    path('edit/<int:year_id>/', views.EditBudgetView.as_view(), name='edit_budget'),
    path('<int:pk>/detail/', views.BudgetDetailView.as_view(), name='view_budget'),
    path('whole-company/<int:pk>/detail/', views.WholeBudgetDetailView.as_view(), name='whole_budget_detail'),
    path('delete/<int:year_id>/', views.DeleteBudgetView.as_view(), name='delete_budget'),
    path('import/', views.ImportBudgetView.as_view(), name='import_budget'),
]

urlpatterns += [
    path('budgetaccount/', include('docuflow.apps.budget.modules.budgetaccount.urls')),
]
