import logging
from openpyxl import Workbook

from django.conf import settings

from docuflow.apps.budget.models import Budget, BudgetAccount, BudgetAccountPeriod
from docuflow.apps.company.models import Account
from docuflow.apps.period.models import Period
from docuflow.apps.common.utils import get_cell_value, to_decimal
from .client import excell_upload_client, csv_upload_client

logger = logging.getLogger(__name__)


class BudgetImportException(Exception):
    pass


def get_line_period(periods, journal_date):
    if not periods:
        return None
    try:
        return next(period for _, period in periods.items() if period.start_date <= journal_date.date() <= period.end_date)
    except StopIteration:
        return None


class ImportBudgetJob(object):

    def __init__(self, company, year, file):
        self.company = company
        self.year = year
        self.file = file
        self.accounts = {}
        self.periods = {}
        self.missing_data = []

    def execute(self):
        budget_data = {}
        if self.file.name.endswith('.csv'):
            budget_data = csv_upload_client.get_upload_data(file=self.file)
        elif self.file.name.endswith('.xls') or self.file.name.endswith('.xlsx'):
            budget_data = excell_upload_client.get_upload_data(file=self.file)
        data = []
        for row_counter, row in enumerate(budget_data, start=1):
            data_dict = {'code': '', 'name': '', 'period_1': 0, 'period_2': 0, 'period_3': 0, 'period_4': 0,
                         'period_5': 0, 'period_6': 0, 'period_7': 0, 'period_8': 0, 'period_9': 0, 'period_10': 0,
                         'period_11': 0, 'period_12': 0}
            for c, cell_value in row.items():
                if c == 0:
                    data_dict['code'] = str(get_cell_value(cell_value)).replace('.0', '')
                if c == 1:
                    data_dict['name'] = get_cell_value(cell_value)
                if c == 2:
                    data_dict['period_1'] = to_decimal(cell_value)
                if c == 3:
                    data_dict['period_2'] = to_decimal(cell_value)
                if c == 4:
                    data_dict['period_3'] = to_decimal(cell_value)
                if c == 5:
                    data_dict['period_4'] = to_decimal(cell_value)
                if c == 6:
                    data_dict['period_5'] = to_decimal(cell_value)
                if c == 7:
                    data_dict['period_6'] = to_decimal(cell_value)
                if c == 8:
                    data_dict['period_7'] = to_decimal(cell_value)
                if c == 9:
                    data_dict['period_8'] = to_decimal(cell_value)
                if c == 10:
                    data_dict['period_9'] = to_decimal(cell_value)
                if c == 11:
                    data_dict['period_10'] = to_decimal(cell_value)
                if c == 12:
                    data_dict['period_11'] = to_decimal(cell_value)
                if c == 13:
                    data_dict['period_12'] = to_decimal(cell_value)
                c += 1
            data.append(data_dict)
        if data:
            return self._sync(data)
        return None

    def _sync(self, budget_data):
        """
            Sync budget .
        """
        self.accounts = {account.code: account for account in Account.objects.filter(company=self.company)}
        self.periods = {f'period_{period.period}': period for period in Period.objects.filter(period_year=self.year)}

        if budget_data:
            data = self.prepare_budget_data(budget_data)

            self.create_budgets(budget_data=data)

            if self.missing_data:
                self.create_missing_data_report()

    def create_missing_data_report(self):
        wb = Workbook()
        ws1 = wb.active
        ws1.title = "Missing Accounts"

        row_count = 1
        for line in self.missing_data:
            ws1.cell(column=1, row=row_count, value=line['line']['department'])
            ws1.cell(column=2, row=row_count, value=line['line']['journal_number'])
            ws1.cell(column=3, row=row_count, value=line['line']['date'])
            ws1.cell(column=4, row=row_count, value=line['line']['text'])
            if line['missing'].get('branch', False):
                ws1.cell(column=5, row=row_count, value='Branch missing is missing from digits')
            if line['missing'].get('department', False):
                ws1.cell(column=6, row=row_count, value='Department is missing from digits')
            if line['missing'].get('sub_category', False):
                ws1.cell(column=7, row=row_count, value='Sub category is missing from digits')
            row_count += 1

        wb.save(filename=f'{settings.MEDIA_ROOT}/accounts/missing_data_{self.company.pk}.xlsx')

    # noinspection PyMethodMayBeStatic
    def prepare_budget_data(self, budget_data):
        data = []
        accounts = []
        for line in budget_data:
            can_add = True
            missing_data = {}
            account = self.accounts.get(line['code'])
            if not account:
                continue
            line_data = {'account': account, 'periods': []}
            for field in line:
                if field.startswith('period'):
                    period = self.periods.get(field)
                    line_data['periods'].append({'period': period, 'amount': line[field]})
            if can_add:
                data.append(line_data)
            elif missing_data:
                self.missing_data.append({'line': line, 'missing': missing_data})
        return data

    # noinspection PyMethodMayBeStatic
    def update_accounts(self, accounts_data, existing_accounts):
        """
            Creates new accounts in bulk.
        """
        fields = {'name', 'code', 'account_type', 'report', 'sub_ledger', 'report', 'cash_flow', 'not_deductable'}
        instances_to_update = []
        logging.info(accounts_data)
        for account in BudgetAccountPeriod.objects.filter(code__in=existing_accounts):
            logging.info(f"get car data with registration {account.code}")
            try:
                account_data = next(c for c in accounts_data if c['code'] == account.code)
                for field in fields:
                    setattr(account, field, account_data[field])
                account.is_active = True
                instances_to_update.append(account)
            except StopIteration:
                continue

        if instances_to_update:
            Account.objects.bulk_update(instances_to_update, fields=fields)

    # noinspection PyMethodMayBeStatic
    def create_budgets(self, budget_data):
        """
            Creates new budget in bulk.
        """

        budget, _ = Budget.objects.get_or_create(year=self.year)

        for budget_account_data in budget_data:
            budget_account, _ = BudgetAccount.objects.get_or_create(
                budget=budget,
                account=budget_account_data['account']
            )
            for period in budget_account_data['periods']:
                BudgetAccountPeriod.objects.get_or_create(
                    budget_account=budget_account,
                    period=period['period'],
                    defaults={
                        'amount': period['amount']
                    }
                )

