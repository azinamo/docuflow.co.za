import logging

from django.db import models
from django.db.models import Case, When, Sum, DecimalField, Q
from django.utils.translation import ugettext_lazy as _, ugettext as __

from docuflow.apps.company.enums import AccountType

logger = logging.getLogger(__name__)


class Budget(models.Model):
    year = models.ForeignKey('period.Year', related_name="budgets", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')
    object_items = models.ManyToManyField('company.ObjectItem')

    def __str__(self):
        return f"{self.year}-{self.year.type_name} budget "

    class Meta:
        ordering = ['-year']
        verbose_name = __('Budgets')
        verbose_name_plural = _('Budget')


class BudgetAccount(models.Model):
    budget = models.ForeignKey(Budget, related_name="accounts", on_delete=models.CASCADE)
    account = models.ForeignKey('company.Account', related_name="budgets", on_delete=models.CASCADE)

    class QS(models.QuerySet):

        def get_income_statement_totals(self, year, period=None, from_period=None, to_period=None):
            amount_filter = Q()
            if period == 'Whole':
                amount_filter.add(Q(period_amounts__period__period_year=year), Q.AND)
            else:
                if to_period:
                    amount_filter.add(Q(period_amounts__period__period__lte=to_period), Q.AND)
                if from_period:
                    amount_filter.add(Q(period_amounts__period__period__gte=from_period), Q.AND)
            return self.annotate(
                budget_amount=Case(
                    When(amount_filter, then=Sum('period_amounts__amount')), output_field=DecimalField()
                )
            ).select_related(
                'account'
            ).filter(
                account__account_type=AccountType.INCOME_STATEMENT, budget__year=year,
                account__company=year.company
            ).order_by(
                'account__code'
            ).values(
                'budget_amount', 'account_id', 'account__name', 'account__code', 'account__id', 'account__report__name',
                'account__report__slug'
            )

    objects = QS.as_manager()

    def __str__(self):
        return f'Budget Account {self.account} - {self.budget}'


class BudgetAccountPeriod(models.Model):
    budget_account = models.ForeignKey(BudgetAccount, related_name="period_amounts", on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', related_name="budgets", on_delete=models.CASCADE)
    amount = models.DecimalField(blank=True, null=True, decimal_places=2, max_digits=15)
