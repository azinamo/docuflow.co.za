from django import forms

from docuflow.apps.period.models import Year
from docuflow.apps.period.enums import BudgetType
from .utils import ImportBudgetJob


class BudgetForm(forms.Form):
    budget_type = forms.ChoiceField(choices=BudgetType.choices())


class ImportBudgetForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(ImportBudgetForm, self).__init__(*args, **kwargs)
        self.fields['year'].queryset = Year.objects.filter(company=self.company)

    year = forms.ModelChoiceField(queryset=None)
    file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)')

    def clean(self):
        file = self.cleaned_data['file']
        if not (file.name.endswith('.csv') or file.name.endswith('.xls') or file.name.endswith('.xlsx')):
            self.add_error('file', 'File is not .csv, .xlsx or xls type')

        if file.multiple_chunks():
            self.add_error('csv_file', "Uploaded file is too big (%.2f MB)." % (file.size / (1000 * 1000),))

    def execute(self):
        import_budget = ImportBudgetJob(
            company=self.company,
            year=self.cleaned_data['year'],
            file=self.cleaned_data['file']
        )
        import_budget.execute()

        return True
