from django.apps import AppConfig


class BudgetConfig(AppConfig):
    name = 'docuflow.apps.budget'
