import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Sum
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView, View, FormView

from docuflow.apps.common.mixins import CompanyMixin, ProfileMixin
from docuflow.apps.common.utils import is_ajax_post
from docuflow.apps.company.models import Account, CompanyObject
from docuflow.apps.period.models import Year
from .forms import BudgetForm, ImportBudgetForm
from .models import Budget
from .utils import BudgetImportException

logger = logging.getLogger(__name__)


class BudgetsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'budgets/index.html'
    context_object_name = 'years'

    def get_years(self, company):
        return Year.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(BudgetsView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = company.company_objects
        logger.info(objects)
        years = self.get_years(company)
        context['company'] = company
        context['years'] = years
        context['objects'] = objects
        return context


class BudgetObjectListView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'budgets/year_index.html'

    def get_budgets(self):
        return Budget.objects.filter(year_id=self.kwargs['year_id'])

    # noinspection PyMethodMayBeStatic
    def get_objects_tree(self, company_objects):
        tree = {}
        for company_object in company_objects:
            tree[company_object] = {}
            for object_item in company_object.object_items.all():
                tree[company_object][object_item] = object_item

    def get_context_data(self, **kwargs):
        context = super(BudgetObjectListView, self).get_context_data(**kwargs)
        company = self.get_company()
        objects = company.company_objects
        budgets = self.get_budgets()
        context['company'] = company
        context['budgets'] = budgets
        context['objects'] = objects
        return context


class CreateBudgetView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'budgets/create.html'
    form_class = BudgetForm

    def get_budget_year(self):
        return Year.objects.prefetch_related('budgets').filter(pk=self.kwargs['year_id']).first()

    def get_context_data(self, **kwargs):
        context = super(CreateBudgetView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_budget_year()
        company_objects = self.get_company_objects(company)
        context['company_objects'] = company_objects
        context['company'] = company
        context['year'] = year
        return context

    # noinspection PyMethodMayBeStatic
    def get_company_objects(self, company):
        return CompanyObject.objects.prefetch_related('object_items').filter(company=company)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the budget, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateBudgetView, self).form_invalid(form)

    # noinspection PyMethodMayBeStatic
    def create_budget(self, year):
        budget = Budget.objects.filter(year=year).first()
        if not budget:
            budget = Budget.objects.create(year=year)
        return budget

    def form_valid(self, form):
        # TODO - Refactor - move logic to form or model
        if is_ajax_post(request=self.request):
            budget_type = form.cleaned_data['budget_type']

            year = self.get_budget_year()

            year.budget_type = budget_type
            year.save()

            budget = self.create_budget(year=year)

            return JsonResponse({'text': 'Budget saved successfully',
                                 'error': False,
                                 'redirect': reverse('create_accounts_budget', kwargs={'pk': budget.id})
                                 })


class EditBudgetView(LoginRequiredMixin, CompanyMixin, UpdateView):
    template_name = 'budgets/edit.html'
    model = Budget
    form_class = BudgetForm

    def get_context_data(self, **kwargs):
        context = super(EditBudgetView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class BudgetDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'budgets/view.html'

    def get_budget(self):
        return Budget.objects.annotate(
            total_amount=Sum('accounts__period_amounts__amount')
        ).prefetch_related(
            'accounts'
        ).filter(
            id=self.kwargs['pk']
        ).first()

    def get_budget_accounts(self, budget):
        return Account.objects.with_budget_total(budget=budget).exclude(
            code='', name=''
        ).order_by(
            'code'
        )

    def get_context_data(self, **kwargs):
        context = super(BudgetDetailView, self).get_context_data(**kwargs)
        budget = self.get_budget()
        accounts = self.get_budget_accounts(budget)
        context['company'] = self.get_company()
        context['budget'] = budget
        context['accounts'] = accounts
        return context


class WholeBudgetDetailView(ProfileMixin, TemplateView):
    template_name = 'budgets/whole_company_view.html'

    def get_budget(self):
        return Budget.objects.annotate(
            total_amount=Sum('accounts__period_amounts__amount')
        ).prefetch_related(
            'accounts'
        ).filter(
            year_id=self.kwargs['pk']
        ).first()

    def get_budget_accounts(self, budget):
        return Account.objects.with_budget_total(budget=budget).exclude(
            code='', name=''
        ).order_by(
            'code'
        )

    def get_context_data(self, **kwargs):
        context = super(WholeBudgetDetailView, self).get_context_data(**kwargs)
        budget = self.get_budget()
        accounts = self.get_budget_accounts(budget)
        logger.info(accounts)
        context['company'] = self.get_company()
        context['budget'] = budget
        context['accounts'] = accounts
        return context


class DeleteBudgetView(LoginRequiredMixin, SuccessMessageMixin, View):
    model = Budget

    def get(self, request, *args, **kwargs):
        budget = Budget.objects.get(year_id=self.kwargs['year_id'])
        budget.delete()
        messages.success(self.request, f"Budget successfully deleted.")
        return HttpResponseRedirect(reverse('budget_index'))


class ImportBudgetView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, FormView):
    template_name = 'budgets/import.html'
    form_class = ImportBudgetForm
    success_url = reverse_lazy('budget_index')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        try:
            form.execute()
            messages.success(self.request, f"Budget successfully imported.")
        except BudgetImportException as ex:
            messages.error(self.request, str(ex))
        return super(ImportBudgetView, self).form_valid(form)
