# Generated by Django 3.1 on 2020-10-15 11:39

from django.db import migrations, models
import django.db.models.deletion
import docuflow.apps.provisionaltax.enums
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('period', '0002_auto_20200422_1713'),
        ('provisionaltax', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Estimate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tax_estimate_type', enumfields.fields.EnumField(enum=docuflow.apps.provisionaltax.enums.ProvisionalTaxEstimateType, max_length=10)),
                ('due_date', models.DateField()),
                ('turnover_results', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('turnover_annualized', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('tax_rate', models.DecimalField(decimal_places=2, default=0, max_digits=7)),
                ('estimate_results', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('estimate_income', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('tax_estimate_income', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('year_tax', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('period_tax', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('rebates', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('employee_tax', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('foreign_tax', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('tax_paid', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('late_payment_penalty', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('late_payment_interest', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('total_amount', models.DecimalField(decimal_places=3, default=0, max_digits=15)),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='period.period')),
                ('to_period', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='period.period')),
                ('year', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='tax_estimates', to='period.year')),
            ],
        ),
        migrations.DeleteModel(
            name='ProvisionalTaxEstimate',
        ),
    ]
