from django.urls import path

from . import views

urlpatterns = [
    path('', views.ProvisionalTaxEstimateView.as_view(), name='provisional_tax_estimate_index'),
    path('<str:tax_estimate_type>/create/', views.CreateProvisionalTaxEstimateView.as_view(),
         name='create_provisional_tax_estimate'),
    path('<int:pk>/edit/', views.EditProvisionalTaxEstimateView.as_view(), name='update_provisional_tax_estimate'),
    path('period/data/', views.PeriodDataView.as_view(), name='period_data')

]
