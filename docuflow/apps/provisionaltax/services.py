from decimal import Decimal


def calculate_annualized_amount(amount: Decimal, period) -> Decimal:
    annualized_amount = 0
    if amount:
        annualized_amount = (amount / Decimal(period)) * 12
    return Decimal(annualized_amount).quantize(Decimal('0.01'))


def calculate_period_estimates(provisional_period, turnover_amount: Decimal, deductable_expenses: Decimal,
                               non_deductable_expenses: Decimal):
    data = {
        'turnover': {
            'amount': 0,
            'annualized_amount': 0
        },
        'deductable': {
            'amount': 0,
            'annualized_amount': 0
        },
        'non_deductable': {
            'amount': 0,
            'annualized_amount': 0
        },
        'taxable_income': {
            'amount': 0,
            'annualized_amount': 0
        },
        'profit_loss': {
            'amount': 0,
            'annualized_amount': 0
        }
    }

    turnover_amount = turnover_amount * -1 if turnover_amount else 0
    data['turnover']['amount'] = turnover_amount
    data['turnover']['annualized_amount'] = calculate_annualized_amount(amount=turnover_amount, period=provisional_period)


    deducatable_amount = deductable_expenses
    deducatable_amount = deducatable_amount * -1 if deducatable_amount else 0
    data['deductable']['amount'] = deducatable_amount
    data['deductable']['annualized_amount'] = calculate_annualized_amount(amount=deducatable_amount, period=provisional_period)

    non_deductable_amount = non_deductable_expenses
    non_deductable_amount = non_deductable_amount * -1 if non_deductable_amount else 0
    data['non_deductable']['amount'] = non_deductable_amount
    data['non_deductable']['annualized_amount'] = calculate_annualized_amount(amount=non_deductable_amount,
                                                                              period=provisional_period)

    taxable_income = turnover_amount + deducatable_amount
    data['taxable_income']['amount'] = taxable_income
    data['taxable_income']['annualized_amount'] = calculate_annualized_amount(amount=taxable_income,
                                                                              period=provisional_period)

    profit_loss = taxable_income + non_deductable_amount
    data['profit_loss']['amount'] = profit_loss
    data['profit_loss']['annualized_amount'] = calculate_annualized_amount(amount=profit_loss,
                                                                           period=provisional_period)

    return data
