from dateutil.relativedelta import relativedelta

from django.db import models
from django.db.models import Sum
from django.utils.timezone import timedelta
from enumfields import EnumField

from .enums import ProvisionalTaxEstimateType
from .services import calculate_annualized_amount


# Create your models here.
class EstimateManager(models.Manager):

    # noinspection PyMethodMayBeStatic
    def get_first_period_tax_paid(self, year):
        try:
            estimate = Estimate.objects.get(
                year=year, tax_estimate_type=ProvisionalTaxEstimateType.FIRST
            )
            return estimate.total_amount
        except Estimate.DoesNotExist:
            return 0

    # noinspection PyMethodMayBeStatic
    def get_first_and_second_tax_paid(self, year):
        estimate = Estimate.objects.filter(
            year=year, tax_estimate_type__in=[ProvisionalTaxEstimateType.FIRST, ProvisionalTaxEstimateType.SECOND]
        ).aggregate(
            total=Sum('total_amount')
        )
        return estimate['total'] if estimate['total'] else 0


class Estimate(models.Model):
    tax_estimate_type = EnumField(ProvisionalTaxEstimateType)
    year = models.ForeignKey('period.Year', related_name='tax_estimates', on_delete=models.DO_NOTHING)
    due_date = models.DateField(null=False)
    to_period = models.ForeignKey('period.Period', related_name='+', on_delete=models.DO_NOTHING)
    turnover = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    tax_rate = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    estimated_income = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    income_deductable = models.DecimalField(default=0, max_digits=15, decimal_places=0, verbose_name='Income Statement Deductable')
    income_non_deductable = models.DecimalField(default=0, max_digits=15, decimal_places=0, verbose_name='Income Statement non-Deductable')
    estimated_taxable_income = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    assesed_loss_previous_year = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    year_tax = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    period_tax = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    rebates = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    employee_tax = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    foreign_tax = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    tax_paid = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    late_payment_penalty = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    late_payment_interest = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    total_amount = models.DecimalField(default=0, max_digits=15, decimal_places=0)
    profile = models.ForeignKey('accounts.Profile', on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, auto_created=True)
    updated_at = models.DateTimeField(auto_now=True, auto_created=True)

    objects = EstimateManager()

    @classmethod
    def calculate_due_date(cls, year, estimate_type):
        if estimate_type == 'first':
            return year.start_date + relativedelta(months=6) + timedelta(days=-1)
        elif estimate_type == 'second':
            return year.start_date + relativedelta(months=12) + timedelta(days=-1)
        return year.end_date

    @property
    def annual_turnover(self):
        return calculate_annualized_amount(amount=self.turnover, period=self.to_period.period)

    @property
    def annual_estimated_income(self):
        return calculate_annualized_amount(amount=self.estimated_income, period=self.to_period.period)

    @property
    def annual_estimated_taxable_income(self):
        return calculate_annualized_amount(amount=self.estimated_taxable_income, period=self.to_period.period)

    @property
    def annual_income_deductable(self):
        return calculate_annualized_amount(amount=self.income_deductable, period=self.to_period.period)

    @property
    def annual_income_non_deductable(self):
        return calculate_annualized_amount(amount=self.income_non_deductable, period=self.to_period.period)

    @property
    def taxable_income(self):
        amount = self.turnover + self.income_deductable
        return amount

    @property
    def annual_taxable_income(self):
        return calculate_annualized_amount(amount=self.taxable_income, period=self.to_period.period)

    @property
    def profit_loss(self):
        amount = self.taxable_income + self.income_non_deductable
        return amount

    @property
    def annual_profit_loss(self):
        return calculate_annualized_amount(amount=self.profit_loss, period=self.to_period.period)