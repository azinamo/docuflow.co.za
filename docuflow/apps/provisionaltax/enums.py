from enumfields import Enum


class ProvisionalTaxEstimateType(Enum):
    FIRST = 'first'
    SECOND = 'second'
    THIRD = 'third'

    @classmethod
    def get_period(cls, typ):
        if cls.FIRST.value == typ:
            return 1
        elif cls.SECOND.value == typ:
            return 2
        elif cls.THIRD.value == typ:
            return 3
