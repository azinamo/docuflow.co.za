import pprint

from django.http.response import JsonResponse
from django.db import transaction
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView, View
from django.urls import reverse, reverse_lazy
from django.conf import settings

from docuflow.apps.common import utils
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.period.models import Period
from docuflow.apps.system.models import Country
from docuflow.apps.reports.views import ReportMixin
from docuflow.apps.reports.incomestatement import IncomeStatement
from .models import Estimate
from .forms import ProvisionalTaxEstimateForm
from . import enums
from . import services


pp = pprint.PrettyPrinter(indent=4)


class ProvisionalTaxEstimateView(ProfileMixin, TemplateView):
    template_name = 'provisionaltax/index.html'

    def get_context_data(self, **kwargs):
        context = super(ProvisionalTaxEstimateView, self).get_context_data(**kwargs)
        year = self.get_year()
        estimates = {
            'first': {
                'value': enums.ProvisionalTaxEstimateType.FIRST.value,
                'label': str(enums.ProvisionalTaxEstimateType.FIRST),
                'line': Estimate.objects.filter(year=year, tax_estimate_type=enums.ProvisionalTaxEstimateType.FIRST.value).first()
            },
            'second': {
                'value': enums.ProvisionalTaxEstimateType.SECOND.value,
                'label': str(enums.ProvisionalTaxEstimateType.SECOND),
                'line': Estimate.objects.filter(year=year, tax_estimate_type=enums.ProvisionalTaxEstimateType.SECOND.value).first()
            },
            'third': {
                'value': enums.ProvisionalTaxEstimateType.THIRD.value,
                'label': str(enums.ProvisionalTaxEstimateType.THIRD),
                'line': Estimate.objects.filter(year=year, tax_estimate_type=enums.ProvisionalTaxEstimateType.THIRD.value).first()
            }
        }
        context['estimates'] = estimates
        context['year'] = year
        return context


class CreateProvisionalTaxEstimateView(ProfileMixin, CreateView):
    model = Estimate
    form_class = ProvisionalTaxEstimateForm
    template_name = 'provisionaltax/create.html'

    def get_initial(self):
        initial = super().get_initial()
        initial['due_date'] = Estimate.calculate_due_date(self.get_year(), self.kwargs['tax_estimate_type'])
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['tax_estimate_type'] = self.kwargs['tax_estimate_type']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        context['year'] = year
        context['tax_estimate_type'] = self.kwargs['tax_estimate_type']
        context['country'] = Country.objects.filter(country=settings.DEFAULT_COUNTRY).first()
        context['due_date'] = Estimate.calculate_due_date(self.get_year(), self.kwargs['tax_estimate_type'])
        context['period'] = enums.ProvisionalTaxEstimateType.get_period(self.kwargs['tax_estimate_type'])
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the provisional tax estimate, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateProvisionalTaxEstimateView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save()
                return JsonResponse({'text': 'Provisional tax saved successfully',
                                     'error': False,
                                     'redirect': reverse('provisional_tax_estimate_index')
                                     })


class EditProvisionalTaxEstimateView(ProfileMixin, UpdateView):
    model = Estimate
    form_class = ProvisionalTaxEstimateForm
    template_name = 'provisionaltax/edit.html'
    context_object_name = 'estimate'
    success_url = reverse_lazy('provisional_tax_estimate_index')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['tax_estimate_type'] = self.get_object().tax_estimate_type.value
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        context['year'] = year
        context['country'] = Country.objects.filter(country=settings.DEFAULT_COUNTRY).first()
        context['due_date'] = Estimate.calculate_due_date(year, self.get_object().tax_estimate_type.value)
        context['period'] = enums.ProvisionalTaxEstimateType.get_period(self.get_object().tax_estimate_type.value)
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred updating the provisional tax estimate, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(EditProvisionalTaxEstimateView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save()
                return JsonResponse({'text': 'Provisional tax saved successfully',
                                     'error': False,
                                     'redirect': reverse('provisional_tax_estimate_index')
                                     })


class PeriodDataView(ProfileMixin, ReportMixin, View):

    # noinspection PyMethodMayBeStatic
    def get_turnover(self, company, year, previous_year, period):
        income_statement = IncomeStatement(company=company, start_date=year.start_date, end_date=period.to_date,
                                           year=year, previous_year=previous_year, include_previous_year=True)
        income_statement.execute()

        report_type_lines = income_statement.lines['gross_profit'].get('report_type_lines', None)
        turnover_amount = 0
        if report_type_lines:
            account_type_lines = report_type_lines.get(('turnover', 'Turnover'))
            if account_type_lines:
                turnover_amount = account_type_lines.get('period_amount', 0)

        return turnover_amount

    def get(self, request):
        try:
            period = Period.objects.get(pk=self.request.GET.get('period_id'))
            provisional_period = period.period
            year = self.get_year()
            company = self.get_company()
            previous_year = self.get_previous_year(year)

            turnover_amount = self.get_turnover(company=company, year=year, previous_year=previous_year, period=period)
            is_deductable = False
            deductable_expenses = JournalLine.objects.total_deductable_expenses(self.get_company(), is_deductable, year,
                                                                                 period.to_date)

            is_deductable = True
            non_deductable_expenses = JournalLine.objects.total_deductable_expenses(self.get_company(), is_deductable, year,
                                                                                    period.to_date)
            data = services.calculate_period_estimates(provisional_period=provisional_period,
                                                       turnover_amount=turnover_amount,
                                                       deductable_expenses=deductable_expenses['total'],
                                                       non_deductable_expenses=non_deductable_expenses['total'])

            return JsonResponse(data)
        except Period.DoesNotExist:
            return JsonResponse({'text': 'Period not found', 'error': True})



