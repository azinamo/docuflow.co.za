from django.apps import AppConfig


class ProvisionaltaxConfig(AppConfig):
    name = 'docuflow.apps.provisionaltax'
