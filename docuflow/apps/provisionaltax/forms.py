from django import forms
from django.conf import settings
from django.urls import reverse_lazy
from django.db.models import Sum

from docuflow.apps.system.models import Country
from .enums import ProvisionalTaxEstimateType
from .models import Estimate


class ProvisionalTaxEstimateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.tax_estimate_type = kwargs.pop('tax_estimate_type')
        super(ProvisionalTaxEstimateForm, self).__init__(*args, **kwargs)
        self.fields['to_period'].queryset = self.fields['to_period'].queryset.filter(period_year=self.year)
        self.fields['to_period'].empty_label = None
        self.fields['tax_rate'].initial = Country.objects.get_tax_rate(country_code=self.company.country)
        self.fields['assesed_loss_previous_year'].initial = 0
        if self.tax_estimate_type == ProvisionalTaxEstimateType.SECOND.value:
            self.fields['tax_paid'].initial = Estimate.objects.get_first_period_tax_paid(self.year)
        elif self.tax_estimate_type == ProvisionalTaxEstimateType.THIRD.value:
            self.fields['tax_paid'].initial = Estimate.objects.get_first_and_second_tax_paid(self.year)

        if self.instance.pk:
            self.fields['annual_estimated_income'].initial = self.instance.annual_estimated_income
            self.fields['annual_turnover'].initial = self.instance.annual_turnover

    annual_turnover = forms.DecimalField(max_digits=15, decimal_places=2, widget=forms.HiddenInput())
    annual_estimated_income = forms.DecimalField(max_digits=15, decimal_places=2, widget=forms.HiddenInput())

    class Meta:
        model = Estimate
        fields = ('to_period', 'turnover', 'estimated_income', 'rebates', 'employee_tax', 'foreign_tax', 'tax_paid',
                  'late_payment_penalty', 'late_payment_interest', 'total_amount', 'due_date', 'tax_rate',
                  'period_tax', 'year_tax', 'assesed_loss_previous_year', 'income_deductable', 'income_non_deductable')
        widgets = {
            'to_period': forms.Select(attrs={'class': 'form-control', 'data-ajax-url': reverse_lazy('period_data')}),
            'turnover': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'due_date': forms.HiddenInput(),
            'estimated_income': forms.HiddenInput(),
            'income_deductable': forms.HiddenInput(),
            'income_non_deductable': forms.HiddenInput(),
            'tax_rate': forms.HiddenInput(),
            'period_tax': forms.HiddenInput(),
            'year_tax': forms.HiddenInput(),
            'rebates': forms.TextInput(),
            'employee_tax': forms.TextInput(),
            'foreign_tax': forms.TextInput(),
            'tax_paid': forms.TextInput(),
            'late_payment_penalty': forms.TextInput(),
            'late_payment_interest': forms.TextInput(),
            'assesed_loss_previous_year': forms.TextInput()
        }

    def save(self, commit=True):
        provisional_tax_estimate = super(ProvisionalTaxEstimateForm, self).save(commit=False)
        provisional_tax_estimate.year = self.year
        provisional_tax_estimate.role = self.role
        provisional_tax_estimate.profile = self.profile
        provisional_tax_estimate.tax_estimate_type = self.tax_estimate_type
        provisional_tax_estimate.save()
