from django import template

from docuflow.apps.pages.forms import EnquiryForm

register = template.Library()


@register.inclusion_tag('pages/enquiry.html')
def enquiry():
    form = EnquiryForm()
    return {'form': form}
