from django import forms

from docuflow.apps.company.models import Company


class EnquiryForm(forms.Form):
    name = forms.CharField(required=True, label='Name')
    email = forms.EmailField(required=True, label='Email')
    company = forms.CharField(required=True, label='Company')
    phone_number = forms.CharField(required=False, label='Phone Number')
    message = forms.CharField(required=True, widget=forms.Textarea())


class RegistrationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Company Name'

    area_code = forms.CharField(required=True, min_length=3)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    password1 = forms.CharField(widget=forms.PasswordInput(), required=True, label='Password')
    password2 = forms.CharField(widget=forms.PasswordInput(), required=True, label='Confirm Password')
    start_date = forms.DateField(
        widget=forms.TextInput(attrs={'class': 'future-dateicker datepicker'}),
        required=True,
        label='Year Start Date'
    )

    class Meta:
        model = Company
        fields = ['name', 'type', 'area_code', 'address_line_1', 'address_line_2', 'province', 'vat_number',
                  'company_number', 'start_date']

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()

        password = cleaned_data.get('password', None)
        confirm_password = cleaned_data.get('password2', None)

        if password and confirm_password:
            if password != confirm_password:
                self.add_error('password1', 'Password and password confirmation not the sames')

        return cleaned_data


