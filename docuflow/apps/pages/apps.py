from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'docuflow.apps.pages'
