from django.conf import settings
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import FormView
from django.http.response import JsonResponse, HttpResponse
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.db import transaction
from django.urls import reverse

from docuflow.apps.accounts.models import Profile, User
from docuflow.apps.common import utils
from docuflow.apps.subscription.models import SubscriptionPlan
from .forms import EnquiryForm, RegistrationForm
from .services import Registration


class HomepageView(TemplateView):
    template_name = "pages/homepage.html"


class EnquiryView(FormView):
    form_class = EnquiryForm

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred sending the enquiry, please try again',
                                 'errors': form.errors
                                 }, status=400)
        return super(EnquiryView, self).form_invalid(form)

    def notify_admin(self, context):
        subject = 'Docuflow enquiry'
        from_address = settings.DEFAULT_FROM_EMAIL
        recipient_list = settings.ADMIN_EMAIL
        template = get_template('mailer/enquiry.html')

        message = template.render(context)
        msg = EmailMultiAlternatives(subject, message, from_address, recipient_list, bcc=settings.ADMIN_EMAIL)
        msg.attach_alternative(message, "text/html")
        msg.send()

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            context = {'name': form.cleaned_data['name'],
                       'message': form.cleaned_data['message'],
                       'email': form.cleaned_data['email'],
                       'phone_no': form.cleaned_data['phone_number'],
                       'company': form.cleaned_data['company']
                       }
            self.notify_admin(context)

            return JsonResponse({'text': 'Email successfully send', 'error':  False})
        return HttpResponse('Not Allowed')


class PlansView(ListView):
    model = SubscriptionPlan
    context_object_name = 'plans'
    template_name = 'pages/plans.html'

    def get_context_data(self, **kwargs):
        context = super(PlansView, self).get_context_data(**kwargs)
        return context


class PlanSignUpView(FormView):
    form_class = RegistrationForm
    template_name = "home/signup.html"

    def get_user_profile(self):
        return Profile.objects.get(user=self.request.session['user'])

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            if form.is_valid():
                with transaction.atomic():
                    company = form.save()

                    subscription_plan = SubscriptionPlan.objects.get(slug=self.kwargs['slug'])

                    user = User.objects.create_user(
                        first_name=form.cleaned_data['first_name'],
                        last_name=form.cleaned_data['last_name'],
                        email=form.cleaned_data['email'],
                        password=form.cleaned_data['password1'],
                        username=company.generate_username(form.cleaned_data['first_name'], form.cleaned_data['last_name'])
                    )

                    registration_service = Registration(
                        company=company,
                        plan=subscription_plan,
                        user=user,
                        start_date=form.cleaned_data['start_date']
                    )
                    registration_service.execute(self.request.get_host())

                    return JsonResponse({
                        'text': 'Company registered successfully',
                        'error': False,
                        'redirect': reverse('complete_signup')
                    })

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Company could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(PlanSignUpView, self).form_invalid(form)


class SignUpThankYouView(TemplateView):
    template_name = 'home/thank_you.html'

    def get_context_data(self, **kwargs):
        context = super(SignUpThankYouView, self).get_context_data(**kwargs)
        return context

