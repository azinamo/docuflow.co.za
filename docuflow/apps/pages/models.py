# from django.contrib.sites.managers import CurrentSiteManager
# from django.contrib.sites.models import Site
# from django.db import models
# from django.conf import settings
# from django.utils.translation import ugettext_lazy as _, get_language
# from django.core.exceptions import ObjectDoesNotExist
#
# from sorl.thumbnail import ImageField
#
#
# from .exceptions import MissingTranslation
#
#
# class TranslatableModel(models.Model):
#     """
#     Base class for translatable models. Its subclasses should contain only language-independent
#     fields such as foreign keys.
#     """
#
#     class Meta:
#         abstract = True
#
#     def __unicode__(self):
#         return self.get_translation()
#
#     def has_translations(self):
#         """
#         Checks if this model has any translation available.
#         """
#         return self.translations.exists()
#
#     def has_translation(self, language=None):
#         """
#         Returns model translation in a language having given `language` code. If no language code
#         is given, current language of Django internationalization system is used.
#         """
#         language = language or get_language()
#         return self.translations.filter(language=language).exists()
#
#     def get_translation(self, language=None, fallback=True):
#         """
#         Returns model translation in a language having given `language` code. If no language code
#         is given, current language of Django internationalization system.
#
#         If `fallback` is True (default) first available translation is returned. Languages are
#         check in order of `LANGUAGES` setting.
#
#         If no translation was found in any of previous steps, `MissingTranslation` exception is
#         raised.
#         """
#         language = language or get_language()
#         try:
#             return self.translations.get(language=language)
#         except ObjectDoesNotExist:
#             if fallback:
#                 for other_language, other_language_name in settings.LANGUAGES:
#                     if other_language == language:
#                         continue
#                     try:
#                         return self.translations.get(language=other_language)
#                     except ObjectDoesNotExist:
#                         continue
#         raise MissingTranslation
#
#     def translated(self, field_name, default=None, language=None, fallback=True):
#         """
#         Returns field of translation. Arguments `language` and `fallback` are
#         same as passed to `get_translation` method. If `get_translation` raises
#         `MissingTranslation` exception, `default` value is returned (None for default).
#         """
#         try:
#             return getattr(self.get_translation(language=language, fallback=fallback), field_name)
#         except MissingTranslation:
#             return default
#
#
# def get_translation_model(translatable_model, verbose_name):
#     """
#     Returns a base class for translation of given `translatable_model`. Attribute `verbose_name`
#     is used for a foreign key connecting it to the model it translates.
#     """
#     class TranslationModel(models.Model):
#         model = models.ForeignKey(translatable_model, related_name='translations', verbose_name=verbose_name, on_delete=models.DO_NOTHING)
#         language = models.CharField(_("language"), max_length=15, choices=settings.LANGUAGES)
#
#         class Meta:
#             abstract = True
#         unique_together = (('model', 'language'),)
#
#     return TranslationModel
#
#
# class Page(TranslatableModel):
#     slug = models.SlugField(max_length=150, blank=False, null=False)
#     site = models.ManyToManyField(Site)
#     display = models.BooleanField(default=True)
#
#     objects = models.Manager()
#     on_site = CurrentSiteManager()
#
#     def __unicode__(self):
#         return self.slug
#
#     def __str__(self):
#         return self.translated('title')
#
#     class Meta:
#         verbose_name = 'Page'
#
#
# class PageTranslation(get_translation_model(Page, "pagecontent")):
#     title = models.CharField(max_length=150, blank=False, null=False)
#     body = models.TextField(blank=False, null=False)
#     meta_title = models.CharField("Title", max_length=255, null=True, blank=True)
#     meta_description = models.CharField("Description", max_length=255, null=True, blank=True)
#     meta_keywords = models.CharField("Keywords", max_length=255, null=True, blank=True)
#     created_at = models.DateTimeField(auto_now=True)
#     modified_at = models.DateTimeField(auto_now_add=True)
#
#
# class ContentBlock(TranslatableModel):
#     page = models.ForeignKey(Page, related_name='content_blocks', on_delete=models.DO_NOTHING)
#     sort_order = models.PositiveIntegerField(default=1)
#
#     def __str__(self):
#         return self.translated('title')
#
#     class Meta:
#         verbose_name = 'Content Block'
#         verbose_name_plural = 'Content Blocks'
#         ordering = ['-sort_order']
#
#
# class ContentBlockTranslation(get_translation_model(ContentBlock, "content")):
#     title = models.CharField(max_length=150, blank=False, null=False)
#     title = models.CharField(max_length=150, blank=False, null=False)
#     body = models.TextField(blank=False, null=False)
#     created_at = models.DateTimeField(auto_now=True)
#     modified_at = models.DateTimeField(auto_now_add=True)
#
#
# class NewsletterSubscriber(models.Model):
#     first_name = models.CharField(max_length=255, null=True, blank=True)
#     last_name = models.CharField(max_length=255, null=True, blank=True)
#     email = models.EmailField(max_length=255)
#     created_at = models.DateTimeField(auto_now=True)
#     modified_at = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f"{self.first_name} {self.last_name}"
#
#     class Meta:
#         verbose_name = "Newsletter Subscriber"
#         verbose_name_plural = "Newsletter Subscribers"
#         ordering = ['-created_at']
#
#
# class EmailContent(models.Model):
#     name = models.CharField(max_length=100, blank=False, null=False)
#     slug = models.SlugField(max_length=150, blank=False, null=False)
#     subject = models.CharField(max_length=150)
#     from_address = models.EmailField()
#     from_name = models.CharField(max_length=150)
#     body_html = models.TextField()
#     body_text = models.TextField()
#     created_at = models.DateTimeField(auto_now=True)
#     modified_at = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         verbose_name = "Email Template"
#         verbose_name_plural = "Email Templates"
#
#
# class HomeConfig(models.Model):
#     site = models.ForeignKey(
#         Site,
#         related_name='site_home',
#         on_delete=models.CASCADE
#     )
#     overview_title = models.CharField(
#         max_length=255,
#         blank=True,
#         null=True
#     )
#     overview_body = models.TextField(
#         blank=True,
#         null=True
#     )
#     overview_image = models.FileField(
#         upload_to='upload/home/',
#         blank=True
#     )
#     contact_phone_number = models.CharField(
#         max_length=255,
#         blank=True,
#         null=True
#     )
#     contact_email = models.EmailField(
#         max_length=255,
#         blank=True,
#         null=True
#     )
#     contact_address_line_1 = models.CharField(
#         max_length=255,
#         blank=True,
#         null=True
#     )
#     contact_address_line_2 = models.CharField(
#         max_length=255,
#         blank=True,
#         null=True
#     )
#     testimonials = models.ManyToManyField(
#         'Testimonial'
#     )
#
#     created_at = models.DateTimeField(
#         auto_now=True
#     )
#     modified_at = models.DateTimeField(
#         auto_now_add=True
#     )
#
#     class Meta:
#         verbose_name = "Home Configuration"
#         verbose_name_plural = "Home Configurations"
#
#     def __unicode__(self):
#         return "Home Configurations"
#
#
# class SiteSocialMedia(models.Model):
#     site = models.ForeignKey(
#         Site,
#         related_name='site_social_medias',
#         on_delete=models.CASCADE
#     )
#     social_media = models.ForeignKey(
#         SocialMedia,
#         related_name='sites',
#         on_delete=models.CASCADE
#     )
#     link = models.URLField(
#         verbose_name='Url Link'
#     )
#
#     def __str__(self):
#         return "{}-{}".format(self.site, self.social_media)
#
#
# class Testimonial(models.Model):
#     content = models.TextField(
#         verbose_name="Testimonial Quote",
#         blank=True,
#         null=True
#     )
#
#     def __str__(self):
#         return "{}".format(self.content)
#
#
# class Slider(models.Model):
#     site = models.ManyToManyField(
#         Site
#     )
#     caption = models.TextField(
#         verbose_name="Caption",
#         blank=True,
#         null=True
#     )
#     image = ImageField(
#         verbose_name="Slide Image",
#         blank=True,
#         null=True
#     )
#     sort = models.IntegerField(
#         _("Sort Order"),
#         default=0
#     )
#
#     def __str__(self):
#         return "{}".format(self.caption)
#
#     class Meta:
#         ordering = ['sort']
#         verbose_name = _("Image")
#         verbose_name_plural = _("Images")