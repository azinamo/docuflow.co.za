from django.urls import path

from . import views

urlpatterns = [
    path('', views.HomepageView.as_view(), name="homepage"),
    path('pricing/', views.PlansView.as_view(), name='plans_pricing'),
    path('signup/<str:slug>/', views.PlanSignUpView.as_view(), name='plan_signup'),
    path('thank-you/', views.SignUpThankYouView.as_view(), name='complete_signup'),
    path('enquiry/', views.EnquiryView.as_view(), name='enquiry'),
]
