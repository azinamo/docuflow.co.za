import logging
import pprint
import calendar
from datetime import date

from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib.auth.models import Permission
from django.db import transaction
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from docuflow.apps.accounts.models import Role, PermissionGroup
from docuflow.apps.accounts.tokens import account_activation_token
from docuflow.apps.company.utils import set_company_url_conf
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.period.models import Year
from docuflow.apps.period.services import process_year_periods
from docuflow.apps.accounts.enums import DocuflowPermission

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class RegistrationException(Exception):
    pass


class Registration(object):

    def __init__(self, company, plan, user, start_date=None):
        logger.info('---------START REGISTRATION----------')
        self.company = company
        self.plan = plan
        self.user = user
        self.start_date = start_date

    def assign_permissions(self, permission_group: PermissionGroup):

        permissions = Permission.objects.filter(
            codename__in=[perm.value for perm in DocuflowPermission]
        ).all()

        permission_group.permissions.set(permissions)

    # noinspection PyMethodMayBeStatic
    def create_name(self, label):
        name_list = label.upper().split(' ')
        counter = 0
        codename = ''
        for str in name_list:
            if counter < 2:
                codename += f"{str[0:3]} "
            counter = counter + 1
        return codename

    def create_default_roles(self, profile):
        logger.info(f'Create default roles and assign to profile -> {profile}')
        role_permission_groups = [
            {'role': 'Administrator', 'group': 'Level 10', 'is_default': False},
            {'role': None, 'group': 'Level 6', 'is_default': False},
            {'role': None, 'group': 'Level 8', 'is_default': False},
            {'role': None, 'group': 'Level 2', 'is_default': False},
            {'role': 'Logger', 'group': 'Logger', 'is_default': True}
        ]

        for role_permission in role_permission_groups:
            if role_permission['role']:
                role = Role.objects.create(
                    company=self.company,
                    label=role_permission['role'],
                    name=self.create_name(role_permission['role'])
                )
                profile.roles.add(role)

                permission_group = PermissionGroup.objects.create(
                    company=self.company,
                    label=role_permission['group'],
                    is_default=role_permission['is_default'],
                    name=f"{self.company.id}-{self.company.name}-{role_permission['group']}"
                )
                if permission_group:
                    role.permission_groups.add(permission_group)

                    if role.label == 'Administrator':
                        self.assign_permissions(permission_group=permission_group)

            else:
                PermissionGroup.objects.create(
                    company=self.company,
                    label=role_permission['group'],
                    is_default=role_permission['is_default'],
                    name=f"{self.company.id}-{self.company.name}-{role_permission['group']}"
                )

    def create_default_invoice_types(self):
        logger.info('create_default_invoice_types')
        invoice_types = [{'name': 'Tax Invoice', 'account_posting': True, 'is_credit': False, 'is_default': True},
                         {'name': 'Credit Note', 'account_posting': True, 'is_credit': True, 'is_default': False},
                         {'name': 'Statement', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Purchase Order', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Quote', 'account_posting': False, 'is_credit': False, 'is_default': False},
                         {'name': 'Invoice', 'account_posting': True, 'is_credit': False, 'is_default': False},
                        ]
        for invoice_type in invoice_types:
            InvoiceType.objects.create(
                name=invoice_type['name'],
                is_default=invoice_type['is_default'],
                is_account_posting=invoice_type['account_posting'],
                is_credit=invoice_type['is_credit'],
                company=self.company
            )

    # noinspection PyMethodMayBeStatic
    def add_urls(self, slug):
        set_company_url_conf(slug)

    # noinspection PyMethodMayBeStatic
    def create_default_year(self, company):
        if self.start_date:
            year_number = self.start_date.year
            start_date = self.start_date
            end_date = start_date.replace(day=calendar.monthrange(year=year_number, month=start_date.month)[1])
        else:
            year_number = timezone.now().date().year
            start_date = date(year_number, 1, 1)
            end_date = date(year_number, 12, 31)
        year, is_new = Year.objects.update_or_create(
            company=company,
            year=year_number,
            defaults={
                'is_active': True,
                'start_date': start_date,
                'end_date': end_date,
                'number_of_periods': 12
            }
        )
        if year:
            process_year_periods(year=year)

    def execute(self, host):
        try:
            with transaction.atomic():

                profile = self.user.profile
                profile.company = self.company
                profile.save()
                logger.info(f"Profile created -> {profile}")

                self.company.administrator = self.user
                self.company.subscription_plan = self.plan
                self.company.save()

                self.company.activate()
                logger.info(f"Company Admin assigned created -> {profile}")

                self.create_default_roles(self.user.profile)

                self.create_default_invoice_types()

                self.create_default_year(company=self.company)

                self.add_urls(self.company.slug)

                subject = 'Activate Your Docuflow Account'
                protocol = 'https' if settings.SECURE_SSL_HOST else 'http'

                login_url = f"{protocol}://{host}{reverse('company_login', kwargs={'slug': self.company.slug})}"
                email_data = {
                    'username': self.user.username,
                    'company': self.company,
                    'domain': host,
                    'uid': urlsafe_base64_encode(force_bytes(str(self.user.profile.id), strings_only=True)),
                    'token': account_activation_token.make_token(self.user),
                    'email': self.user.email,
                    'first_name': self.user.first_name,
                    'last_name': self.user.last_name,
                    'login_url': login_url
                }
                message = render_to_string('mailer/account_activation_email.html', email_data)
                self.user.email_user(subject, message, from_email=settings.DEFAULT_FROM_EMAIL, html_message=message)
                # send_signup_email.delay(email_data, subject, message)

        except RegistrationException as exception:
            pass
            # raise RegistrationException('An unexpected error occured registrating the company, please try again.')


class CreateDefaultRoles:

    def __init__(self, profile):
        self.profile = profile

    def get_default_roles(self):
        roles_with_permission_groups = [
            {'role': 'Administrator', 'group': 'Level 10', 'is_default': False, 'is_admin': True},
            {'role': None, 'group': 'Level 6', 'is_default': False, 'is_admin': False},
            {'role': None, 'group': 'Level 8', 'is_default': False, 'is_admin': False},
            {'role': None, 'group': 'Level 2', 'is_default': False, 'is_admin': False},
            {'role': 'Logger', 'group': 'Logger', 'is_default': True, 'is_admin': False}
        ]
        return roles_with_permission_groups

    def execute(self):
        roles_with_permission_groups = self.get_default_roles()
        roles = []

        for role_permission in roles_with_permission_groups:
            default_role = role_permission.get('role', None)
            if default_role:
                role = Role.objects.create(
                    company=self.profile.company,
                    label=default_role,
                    name=self.create_name(default_role)
                )
                roles.append(role)
                self.profile.roles.add(role)

                self.create_role_permission_group(role, role_permission.get('group', None),
                                                  role_permission.get('is_default', False),
                                                  role_permission.get('is_admin', False))
            else:
                self.create_role_permission_group(None, role_permission.get('group', None),
                                                  role_permission.get('is_default', False),
                                                  role_permission.get('is_admin', False)
                                                  )
        return roles

    def create_role_permission_group(self, role, role_group, is_default, is_admin):
        permission_group = PermissionGroup.objects.create(
            company=self.profile.company,
            label=role_group,
            is_default=is_default,
            name="{}-{}-{}".format(self.profile.company.id, self.profile.company.name, role_group)
        )
        if role and permission_group:
            role.permission_groups.add(permission_group)
            if is_admin:
                self.assgin_permissions(permission_group)
        return permission_group

    def create_name(self, label):
        name_list = label.upper().split(' ')
        counter = 0
        codename = ''
        for _str in name_list:
            if counter < 2:
                codename += "{} ".format(_str[0:3])
            counter = counter + 1
        return codename

    def assgin_permissions(self, permission_group):
        for permission in Permission.objects.all():
            permission_group.permissions.add(permission)


class AssignAdminPermissions:

    def __init__(self, company, profile):
        self.company = company
        self.profile = profile

    def is_admin(self):
        return self.company.administrator == self.profile

    def execute(self):
        if self.is_admin():
            try:
                for permission in Permission.objects.all():
                    self.profile.user.user_permissions.add(permission)
            except Exception as e:
                pass

# class Login:
#
#     def __init__(self, username, password, company_slug, request):
#         self.username = username
#         self.password = password
#         self.request = request
#         self.company_slug = company_slug
#
#     def get_company(self):
#         return Company.objects.filter(slug=self.company_slug).first()
#
#     def login_log(self, profile):
#         x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
#         if x_forwarded_for:
#             ipaddress = x_forwarded_for.split(',')[-1].strip()
#         else:
#             ipaddress = self.request.META.get('REMOTE_ADDR')
#
#         UserLoginLog.objects.create(
#             profile=profile,
#             message='Login successful',
#             remote_addr=ipaddress
#         )
#
#     def get_default_role(self, profile):
#         if profile.roles.count() > 0:
#             role = profile.roles.all()[0]
#             if role:
#                 return role.id
#             return None
#
#     def set_default_profile_year(self, company, profile):
#         year = Year.objects.filter(
#             company=company,
#             is_active=True,
#             status=OPEN
#         ).first()
#         if not year:
#             year = Year.objects.filter(
#                 company=company,
#                 status=OPEN
#             ).first()
#             if year:
#                 profile.year = year
#                 profile.save()
#                 return year.id
#             else:
#                 year = Year.objects.filter(company=company, status=OPEN).first()
#                 if year:
#                     profile.year = year
#                     profile.save()
#                     return year.id
#         else:
#             profile.year = year
#             profile.save()
#             return year.id
#
#     def get_default_branch(self, company, profile):
#         if profile.branch:
#             return profile.branch.id
#         else:
#             branch = Branch.objects.filter(company=company, is_default=True).first()
#             if branch:
#                 return branch.id
#             else:
#                 branch = Branch.objects.filter(company=company).first()
#                 if branch:
#                     return branch.id

