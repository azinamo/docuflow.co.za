import logging

from django.contrib.auth import logout
from django.urls import set_urlconf
from django.utils import translation

from docuflow.apps.company.models import Company
from docuflow.apps.company.utils import set_company_url_conf
from docuflow.apps.company.services import get_company_from_path

logger = logging.getLogger(__name__)


class CompanyMiddleWare:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        company, slug = get_company_from_path(request.path_info)
        logger.info(f"Company {company} from url path with slug {slug}")
        company_id = request.session.get('company', slug)
        logger.info(f"Company from session {company_id} and slug from url is {slug}")
        if company_id and not request.user.is_staff:
            # company in url is not same as company in session, give priority to company in session or logout
            logger.info(f"Company in url is not same as company in session, give priority to company "
                        f"in session or logout = {company and company.id != company_id}")
            if company and company.id != company_id:
                try:
                    company = Company.objects.get(pk=company_id)
                    slug = company.slug
                except Company.DoesNotExist:
                    logout(request)

            if not slug:
                slug = request.session.get('company_slug', slug)
            logger.info(f"Company slug is {slug}")
            urls_config = set_company_url_conf(slug=slug, overwrite=False)

            logger.info(f"Urls config is {urls_config}")

            set_urlconf(urls_config)
            request.urlconf = urls_config
        # logger.info(f"Now after setting urls config, its {getattr(request, 'urlconf', None)}")
        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response


class CompanyLocaleMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        _, slug = get_company_from_path(request.path_info)
        if slug in ['08052', '17051']:
            language_code = 'en'  # TODO, your logic
        else:
            language_code = 'en'  # TODO, your logic
        translation.activate(language_code)

        response = self.get_response(request)

        translation.deactivate()

        return response
