from django.contrib import admin

from . import models


@admin.register(models.Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'name', 'slug', 'administrator', 'status')
    list_select_related = ('administrator', 'default_distribution_account', 'default_flow_proposal',
                           'currency', 'default_vat_account', 'default_expenditure_account',
                           'default_supplier_account', 'subscription_plan', 'currency',
                           'default_trade_creditors_account', 'default_discount_received_account',
                           'default_discount_allowed_account', 'default_vat_receivable_account',
                           'default_vat_payable_account', 'default_profit_on_asset_account',
                           'default_loss_on_asset_account', 'default_customer_account', 'default_goods_received_account',
                           'stock_adjustment_account', 'default_inventory_control_account',
                           'default_inventory_variance_account', 'default_payroll_account', 'default_bank_account',
                           'default_payment_method'
                           )
    fields = ('name', 'area_code', 'vat_number', 'status', 'subscription_plan', 'contact_person')
    list_per_page = 50
    search_fields = ('name', )


@admin.register(models.Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'name', 'company', )
    list_select_related = ('company', )
    list_per_page = 50
    search_fields = ('name', )
    list_filter = ('company', )

    def has_add_permission(self, request):
        return False


@admin.register(models.Branch)
class BranchAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'company', )
    list_select_related = ('company', )
    list_per_page = 50
    search_fields = ('label', )
    list_filter = ('company', )

    def has_add_permission(self, request):
        return False