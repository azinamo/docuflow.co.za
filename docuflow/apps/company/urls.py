from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.CompaniesView.as_view(), name='my-companies'),
    path('create/', views.CompaniesCreateView.as_view(), name='create-company'),
    path('master/create/', views.CreateMasterCompanyView.as_view(), name='create_master_client'),
    path('edit/<int:pk>/', views.EditCompanyView.as_view(), name='edit-company'),
    path('update/<int:pk>/', views.EditCompanyView.as_view(), name='edit_company'),
    path('activate/<int:pk>/', views.ActivateCompanyView.as_view(), name='activate_company'),
    path('delete/<int:pk>/', views.CompanyDeleteView.as_view(), name='delete_company'),
    path('manage/', views.ManageCompanyView.as_view(), name='manage_company'),
    path('manage/<int:master_company_id>/master', views.ManageMasterCompanyView.as_view(), name='manage_master_company'),
    path('clients/', views.CompaniesView.as_view(), name='all_companies'),
    path('change/<int:pk>/view/', views.ChangeCompanyView.as_view(), name='change_company'),
    path('change/<int:pk>/role/view/', views.ChangeCompanyView.as_view(), name='company_role_login'),
    path('<str:status>/list/', views.ClientsView.as_view(), name='clients'),
    path('profile/', views.CompanyProfileView.as_view(), name='edit-company-profile'),
    path('subsidary/<int:pk>/create/', views.CompaniesCreateView.as_view(), name='create-subsidary-company'),
    path('default/accounts/', views.DefaultCompanyAccountsView.as_view(), name='default_accounts'),
    path('default/<str:module>/accounts/', views.DefaultAccountsView.as_view(), name='default_account'),
    path('master/create/', views.CreateMasterCompanyView.as_view(), name='create_master_company'),
    path('master/<int:pk>/edit', views.ManageCompanyView.as_view(), name='edit_master_company'),
]
urlpatterns += [
    path('', include('docuflow.apps.company.modules.nonbankingday.urls')),
    path('accounts/', include('docuflow.apps.company.modules.account.urls')),
    path('', include('docuflow.apps.company.modules.branch.urls')),
    path('', include('docuflow.apps.company.modules.currency.urls')),
    path('files/', include('docuflow.apps.company.modules.filetype.urls')),
    path('paymentmethod/', include('docuflow.apps.company.modules.paymentmethod.urls')),
    path('', include('docuflow.apps.company.modules.reporttype.urls')),
    path('vatcodes/', include('docuflow.apps.company.modules.vatcode.urls')),
    path('objects/', include('docuflow.apps.company.modules.objects.urls')),
    path('banks/', include('docuflow.apps.company.modules.banking.urls')),
    path('units/', include('docuflow.apps.company.modules.units.urls')),
    path('measures/', include('docuflow.apps.company.modules.measures.urls')),
    path('templates/', include('docuflow.apps.company.modules.companytemplates.urls')),
    path('deliverymethod/', include('docuflow.apps.company.modules.deliverymethod.urls')),
    path('defaultaccounts/', include('docuflow.apps.company.modules.defaultaccount.urls')),
]
