from django import forms
from django.core.cache import cache
from easy_thumbnails.widgets import ImageClearableFileInput

from docuflow.apps.system.models import ReportType
from docuflow.apps.company.enums import SubLedgerModule
from . import models


class TemplateForm(forms.Form):
    label = forms.CharField(required=True)


class DocumentTemplateForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(DocumentTemplateForm, self).__init__(*args, **kwargs)


class DefaultCompanyAccountsForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(DefaultCompanyAccountsForm, self).__init__(*args, **kwargs)
        self.fields['default_expenditure_account'].queryset = self.fields['default_expenditure_account']\
            .queryset.filter(company=self.company)
        self.fields['default_vat_account'].queryset = self.fields['default_vat_account'].queryset.filter(
            company=self.company)
        self.fields['default_distribution_account'].queryset = self.fields['default_distribution_account']\
            .queryset.filter(company=self.company)
        self.fields['default_supplier_account'].queryset = self.fields['default_supplier_account'].queryset.filter(
            company=self.company, sub_ledger=SubLedgerModule.SUPPLIER)
        self.fields['default_discount_received_account'].queryset = self.fields['default_discount_received_account'].queryset.filter(
            company=self.company)
        self.fields['default_vat_payable_account'].queryset = self.fields['default_vat_payable_account'].queryset.filter(
            company=self.company)
        self.fields['default_vat_receivable_account'].queryset = self.fields['default_vat_receivable_account'].queryset.filter(
            company=self.company)
        self.fields['default_profit_on_asset_account'].queryset = self.fields['default_profit_on_asset_account'].queryset.filter(
            company=self.company)
        self.fields['default_loss_on_asset_account'].queryset = self.fields['default_loss_on_asset_account'].queryset.filter(
            company=self.company)
        self.fields['default_customer_account'].queryset = self.fields['default_customer_account'].queryset.filter(
            company=self.company, sub_ledger=SubLedgerModule.CUSTOMER)
        self.fields['default_goods_received_account'].queryset = self.fields['default_goods_received_account'].queryset.filter(
            company=self.company)
        self.fields['default_discount_allowed_account'].queryset = self.fields['default_discount_allowed_account'].queryset.filter(
            company=self.company)
        self.fields['stock_adjustment_account'].queryset = self.fields['stock_adjustment_account'].queryset.filter(
            company=self.company)
        self.fields['default_inventory_control_account'].queryset = self.fields['stock_adjustment_account'].queryset.filter(
            company=self.company, sub_ledger=SubLedgerModule.INVENTORY)
        self.fields['default_inventory_variance_account'].queryset = self.fields['default_inventory_variance_account'].queryset.filter(
            company=self.company)
        self.fields['default_payroll_account'].queryset = self.fields['default_payroll_account'].queryset.filter(
            company=self.company)
        self.fields['default_bank_account'].queryset = self.fields['default_bank_account'].queryset.filter(
            company=self.company)
        self.fields['default_payment_method'].queryset = self.fields['default_payment_method'].queryset.filter(
            company=self.company)
        self.fields['default_retained_income_account'].queryset = self.fields['default_retained_income_account'].queryset.filter(
            company=self.company)
        self.fields['default_previous_year_account'].queryset = self.fields['default_previous_year_account'].queryset.filter(
            company=self.company)

    class Meta:
        model = models.Company
        fields = ['default_expenditure_account', 'default_vat_account', 'default_supplier_account',
                  'default_distribution_account', 'default_discount_received_account',
                  'default_discount_allowed_account', 'default_vat_payable_account', 'default_vat_receivable_account',
                  'default_loss_on_asset_account', 'default_profit_on_asset_account', 'default_customer_account',
                  'default_goods_received_account', 'stock_adjustment_account', 'default_inventory_control_account',
                  'default_inventory_variance_account', 'default_payroll_account', 'default_bank_account',
                  'default_payment_method', 'line_discount', 'default_retained_income_account',
                  'default_previous_year_account']
        widgets = {
            'default_expenditure_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_vat_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_supplier_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_distribution_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_flow_proposal': forms.Select(attrs={'class': 'chosen-select'}),
            'default_discount_received_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_vat_payable_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_vat_receivable_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_customer_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_loss_on_asset_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_profit_on_asset_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_goods_received_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_discount_allowed_account': forms.Select(attrs={'class': 'chosen-select'}),
            'stock_adjustment_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_inventory_control_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_inventory_variance_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_payroll_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_bank_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_retained_income_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_previous_year_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_payment_method': forms.Select(attrs={'class': 'chosen-select'})
        }

    def clean(self):
        cleaned_data = super(DefaultCompanyAccountsForm, self).clean()
        if self.instance.is_vat_registered is True:
            if cleaned_data['default_expenditure_account'] is None:
                self.add_error('default_expenditure_account', 'Default expenses account is required')
            if cleaned_data['default_vat_account'] is None:
                self.add_error('default_vat_account', 'Default vat account is required')
            if cleaned_data['default_supplier_account'] is None:
                self.add_error('default_supplier_account', 'Default supplier account is required')
            if cleaned_data['default_distribution_account'] is None:
                self.add_error('default_distribution_account', 'Default distribution account is required')
        return cleaned_data


class CompanyForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(CompanyForm, self).__init__(*args, **kwargs)
        self.fields['default_flow_proposal'].queryset = self.fields['default_flow_proposal'].queryset.filter(
            company=self.company)

    area_code = forms.CharField(required=True, min_length=3)

    class Meta:
        model = models.Company
        fields = ['name', 'code', 'address_line_1', 'address_line_2', 'area_code', 'province', 'landline_number',
                  'is_vat_registered', 'vat_category', 'vat_number', 'contact_person', 'email_address',
                  'currency', 'country', 'estimate_valid_until', 'type', 'website', 'enter_accounting_date',
                  'calculate_due_date', 'user_set_as_account_poster', 'transfer_invoice_lines_not_matching',
                  'account_posting_at_preliminary_recording', 'preliminary_recording_type', 'allow_direct_recording',
                  'default_flow_proposal', 'payment_terms', 'nb_of_months_to_check_duplicate_invoices',
                  'vat_code_fetched_from', 'erp_system', 'logo'
                  ]

        widgets = {
            'default_expenditure_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_vat_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_supplier_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_distribution_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_flow_proposal': forms.Select(attrs={'class': 'chosen-select'}),
            'area_code': forms.NumberInput(),
            'logo': ImageClearableFileInput
        }

    def clean(self):
        cleaned_data = super(CompanyForm, self).clean()
        if cleaned_data['is_vat_registered'] is True:
            # raise forms.ValidationError('Default expenses account is required')
            if cleaned_data['vat_number'] is None:
                self.add_error('vat_number', 'Vat number is required')
        return cleaned_data

    def save(self, commit=True):
        company = super(CompanyForm, self).save(commit=commit)
        _key = f"company_{company.id}"
        company = models.Company.objects.select_related(
            'default_distribution_account', 'default_flow_proposal', 'currency', 'default_vat_account',
            'default_expenditure_account', 'default_supplier_account',
        ).prefetch_related(
            'company_years', 'branches', 'company_vat_codes'
        ).filter(id=company.id).first()
        cache.set(_key, company)
        return company


class MasterCompanyForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        self.company = kwargs.pop('company')
        super(MasterCompanyForm, self).__init__(*args, **kwargs)
        self.fields['default_flow_proposal'].queryset = self.fields['default_flow_proposal'].queryset.filter(
            company__is_master=True)
        self.fields['currency'].queryset = self.fields['currency'].queryset.filter(
            company__is_master=True
        )

    area_code = forms.CharField(required=True, min_length=3)

    class Meta:
        model = models.Company
        fields = ['name', 'code', 'address_line_1', 'address_line_2', 'area_code', 'province', 'landline_number',
                  'is_vat_registered', 'vat_category', 'email_address', 'currency', 'invoice_series', 'type',
                  'enter_accounting_date', 'calculate_due_date', 'user_set_as_account_poster',
                  'transfer_invoice_lines_not_matching', 'account_posting_at_preliminary_recording',
                  'preliminary_recording_type', 'allow_direct_recording', 'default_flow_proposal', 'payment_terms',
                  'nb_of_months_to_check_duplicate_invoices', 'vat_code_fetched_from', 'erp_system'
                  ]
        widgets = {
            'default_expenditure_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_vat_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_supplier_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_distribution_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_flow_proposal': forms.Select(attrs={'class': 'chosen-select'}),
            'area_code': forms.NumberInput()
        }

    def clean(self):
        cleaned_data = super(MasterCompanyForm, self).clean()
        return cleaned_data


class EditCompanyForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        self.company = kwargs.pop('company')
        super(EditCompanyForm, self).__init__(*args, **kwargs)
        self.fields['default_flow_proposal'].queryset = self.fields['default_flow_proposal'].queryset.filter(
            company=self.company)

    area_code = forms.CharField(required=True, min_length=3)

    class Meta:
        model = models.Company
        fields = ['name', 'code', 'subscription_plan', 'status', 'address_line_1', 'address_line_2', 'area_code',
                  'province', 'landline_number', 'is_vat_registered', 'vat_number', 'accounting_system',
                  'contact_person', 'email_address', 'currency', 'estimate_valid_until', 'type', 'website',
                  'enter_accounting_date', 'calculate_due_date', 'user_set_as_account_poster',
                  'transfer_invoice_lines_not_matching', 'account_posting_at_preliminary_recording', 'preliminary_recording_type', 'allow_direct_recording',
                  'default_flow_proposal', 'payment_terms',
                  'nb_of_months_to_check_duplicate_invoices', 'vat_code_fetched_from', 'erp_system', 'logo'
                  ]

        widgets = {
            'default_expenditure_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_vat_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_supplier_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_distribution_account': forms.Select(attrs={'class': 'chosen-select'}),
            'default_flow_proposal': forms.Select(attrs={'class': 'chosen-select'}),
            'area_code': forms.NumberInput()
        }

    def clean(self):
        cleaned_data = super(EditCompanyForm, self).clean()
        if cleaned_data['is_vat_registered'] is True:
            # raise forms.ValidationError('Default expenses account is required')
            if cleaned_data['vat_number'] is None:
                self.add_error('vat_number', 'Vat number is required')
        return cleaned_data


class NonBankingDayForm(forms.ModelForm):
    class Meta:
        model = models.NonBankingDay
        fields = ('name', 'day')
        widgets = {
            'day': forms.DateInput(attrs={'class': 'datepicker'})
        }


class SupplierTemplateForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(SupplierTemplateForm, self).__init__(*args, **kwargs)
        # self.fields['template'].queryset = models.Template.objects.filter(company=company)

    # template = forms.ModelChoiceField(required=False, label='Select existing template', queryset=None)
    docfile = forms.FileField(required=False, label='Sample File/Invoice')
    label = forms.CharField(required=True, label='Template Name')


class VatCodeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(VatCodeForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = models.Account.objects.filter(company=company)

    class Meta:
        model = models.VatCode
        fields = ('code', 'label', 'percentage', 'account', 'is_default', 'valid_from', 'valid_to', 'is_capital')

        widgets = {
            'account': forms.Select(attrs={'class': 'chosen-select'}),
            'valid_from': forms.TextInput(attrs={'class': 'datepicker'}),
            'valid_to': forms.TextInput(attrs={'class': 'datepicker'})
        }

    def clean(self):
        cleaned_data = super(VatCodeForm, self).clean()
        if cleaned_data['valid_from'] and cleaned_data['valid_to']:
            if cleaned_data['valid_from'] > cleaned_data['valid_to']:
                self.add_error('valid_from', 'Valid from cannot be after valid to date')
        return cleaned_data


class ChoiceForm(forms.Form):
    """
    Form to be used in side by side templates used to add or remove
    items from a many to many field
    """
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices', [])
        label = kwargs.pop('label', 'Selection')
        help_text = kwargs.pop('help_text', None)
        disabled_choices = kwargs.pop('disabled_choices', ())
        super(ChoiceForm, self).__init__(*args, **kwargs)
        self.fields['selection'].choices = choices
        self.fields['selection'].label = label
        self.fields['selection'].help_text = help_text
        self.fields['selection'].widget.disabled_choices = disabled_choices
        self.fields['selection'].widget.attrs.update(
            {'size': 14, 'class': 'choice_form'}
        )

    selection = forms.MultipleChoiceField()


class CompanyObjectForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CompanyObjectForm, self).__init__(*args, **kwargs)
        # self.fields['modules'].choices = ((Module.SALES, 'Sales'), (Module.PURCHASE, 'Purchase'))

    class Meta:
        model = models.CompanyObject
        fields = ('label', 'modules', )


class ObjectItemForm(forms.ModelForm):
    class Meta:
        model = models.ObjectItem
        fields = ('code', 'label', )


class BranchForm(forms.ModelForm):
    class Meta:
        model = models.Branch
        fields = ('label', 'address_line_1', 'address_line_2', 'town', 'area_code')


class CurrencyForm(forms.ModelForm):
    class Meta:
        model = models.Currency
        fields = ['name', 'code', 'symbol']


class ReportTypeForm(forms.ModelForm):
    class Meta:
        model = ReportType
        fields = ['account_type', 'name']


class FileTypeForm(forms.ModelForm):
    class Meta:
        model = models.FileType
        fields = ['name', 'module']
