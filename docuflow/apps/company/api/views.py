from rest_framework import viewsets

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Branch, Account
from .serializers import BranchSerializer, AccountSerializer


class BranchViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = BranchSerializer

    def get_queryset(self):
        return Branch.objects.filter(company=self.get_company())


class AccountViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = AccountSerializer

    def get_queryset(self):
        return Account.all_objects.select_related(
            'company'
        ).filter(
            company=self.get_company()
        ).order_by(
            'code'
        )
