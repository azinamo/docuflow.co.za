from django.urls import reverse

from rest_framework import serializers

from docuflow.apps.company.models import Account, Unit, Measure, VatCode, Branch
from docuflow.apps.system.api.serializers import ReportTypeSerializer, CashFlowCategorySerializer


class VatCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = VatCode
        fields = ('id', 'company', 'account', 'label', 'code', 'percentage', 'is_active', 'is_default', 'valid_from',
                  'module')


class UnitSerializer(serializers.ModelSerializer):
    code = serializers.CharField(source='unit')

    class Meta:
        model = Unit
        fields = ('id', 'name', 'code')


class MeasureSerializer(serializers.ModelSerializer):
    units_url = serializers.SerializerMethodField()

    class Meta:
        model = Measure
        fields = ('id', 'name', 'extra_quantity', 'units_url')

    def get_units_url(self, instance):
        return reverse('measure_units')


class BranchSerializer(serializers.ModelSerializer):
    detail_url = serializers.CharField(source='get_absolute_url')

    class Meta:
        model = Branch
        fields = ('label', 'address_line_1', 'address_line_2', 'town', 'province', 'detail_url',
                  'id', 'is_default')
        datatables_always_serialize = ('id', 'detail_url')


class AccountSerializer(serializers.ModelSerializer):
    detail_url = serializers.CharField(source='get_absolute_url')
    sub_ledger = serializers.SerializerMethodField()
    report = ReportTypeSerializer()
    account_type = serializers.SerializerMethodField()
    vat_reporting = serializers.SerializerMethodField()
    cash_flow = CashFlowCategorySerializer()

    class Meta:
        model = Account
        fields = ('id', 'code', 'name', 'account_type', 'report', 'cash_flow', 'sub_ledger', 'vat_reporting',
                  'not_deductable', 'is_active', 'detail_url')
        datatables_always_serialize = ('id', 'detail_url')

    def get_company_slug(self, instance):
        return instance.company.slug

    def get_sub_ledger(self, instance):
        return '' if instance.sub_ledger is None else str(instance.sub_ledger)

    def get_account_type(self, instance):
        return str(instance.account_type_name) if instance.account_type else ''

    def get_vat_reporting(self, instance):
        return instance.vat_type
