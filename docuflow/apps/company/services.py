import os

from django.conf import settings

from .models import ObjectItem, Company
from . import utils


class CompanyLinkedModels(object):

    def __init__(self, company):
        self.company = company

    def get_company_object_items(self):
        object_items = ObjectItem.objects.select_related('company_object').filter(
            company_object__company=self.company).all()
        company_object_items = {}
        for object_item in object_items:
            if object_item.company_object_id in company_object_items:
                company_object_items[object_item.company_object_id].append(object_item)
            else:
                company_object_items[object_item.company_object_id] = [object_item]
        return company_object_items

    def get_company_linked_models(self):
        linked_models = {}
        company_object_items = self.get_company_object_items()
        company_objects = self.company.company_objects.all()
        for company_object in company_objects:
            linked_models[company_object.id] = {}
            linked_models[company_object.id]['id'] = company_object.id
            linked_models[company_object.id]['label'] = company_object.label
            linked_models[company_object.id]['class'] = company_object
            linked_models[company_object.id]['object_items'] = []
            if company_object.id in company_object_items:
                linked_models[company_object.id]['object_items'] = company_object_items[company_object.id]
        return linked_models


def create_company_urls(company: Company):
    slug = company.slug

    return utils.set_company_url_conf(slug)


def get_company_from_path(path):
    path_list = path.split('/')
    if path_list:
        try:
            company = Company.objects.get(slug=path_list[1])
            if company:
                return company, path_list[1]
        except Company.DoesNotExist:
            return None, None
    return None, None
