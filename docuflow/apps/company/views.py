import json
import logging

from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.cache import cache
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import set_urlconf, reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as __
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.accounts.models import Role
from . import utils
from .forms import CompanyForm, MasterCompanyForm, EditCompanyForm, DefaultCompanyAccountsForm
from .models import Company, CompanyStatus

logger = logging.getLogger(__name__)


class CompaniesView(LoginRequiredMixin, ListView):
    model = Company
    template_name = 'companies/index.html'
    paginate_by = 1000
    context_object_name = 'companies'

    def get_queryset(self):
        return Company.objects.select_related('subscription_plan').prefetch_related('invoices', 'company_roles', 'profiles').filter()


class ClientsView(LoginRequiredMixin, ListView):
    model = Company
    template_name = 'companies/index.html'
    paginate_by = 10
    context_object_name = 'companies'

    def get_template_names(self):
        if self.kwargs['status'] == 'new':
            return 'companies/new_request.html'
        return 'companies/index.html'

    def get_queryset(self):
        filter_options = dict()
        if self.kwargs['status'] == 'new':
            filter_options['status__slug'] = 'active'
        elif self.kwargs['status'] == 'pending':
            filter_options['status__slug'] = 'pending'
        elif self.kwargs['status'] == 'deactivated':
            filter_options['status__slug'] = 'deactivated'
        else:
            filter_options['status__slug'] = 'active'
        return Company.objects.prefetch_related('invoices', 'company_roles', 'profiles').filter(**filter_options)


class CompaniesCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Company
    form_class = CompanyForm
    template_name = 'companies/create.html'
    success_message = 'Company successfully created'
    success_url = reverse_lazy('my-companies')

    def get_form_kwargs(self):
        kwargs = super(CompaniesCreateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def form_valid(self, form):
        try:
            company = form.save(commit=False)
            parent_company = self.get_company()
            if parent_company:
                company.company = parent_company
            company.administrator = self.request.user
            company_status = get_object_or_None(CompanyStatus, slug='pending')
            if company_status:
                company.status = company_status
            company.save()
        except Exception as exception:
            messages.error(self.request, exception.__str__())
            return HttpResponseRedirect(reverse('create-company'))

        return HttpResponseRedirect(reverse('my-companies'))


class CreateMasterCompanyView(LoginRequiredMixin, CreateView):
    model = Company
    form_class = MasterCompanyForm
    template_name = 'companies/create.html'
    success_message = 'Master company successfully created'

    # def get_success_url(self):
    #     return reverse('my-companies')

    def get_form_kwargs(self):
        kwargs = super(CreateMasterCompanyView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_company(self):
        return Company.objects.filter(is_master=True).first()

    def form_valid(self, form):
        try:
            company = form.save(commit=False)
            company.administrator = self.request.user
            company.is_master = True
            company_status = get_object_or_None(CompanyStatus, slug='pending')
            if company_status:
                company.status = company_status
            company.save()
            messages.success(self.request, __("Master company successfully created"))
            return HttpResponseRedirect(reverse('edit_master_company', kwargs={'pk': company.id}))
        except Exception as exception:
            messages.error(self.request, __("Error creating the master company, please try again {}".format(exception.__str__())))
            return HttpResponseRedirect(reverse('create_master_company'))


class EditCompanyView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Company
    template_name = 'companies/edit.html'
    success_message = 'Company details successfully updated'
    form_class = EditCompanyForm
    success_url = reverse_lazy('manage_company')

    def get_form_kwargs(self):
        kwargs = super(EditCompanyView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['company'] = self.get_object()
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Company updated successfully.')
        return HttpResponseRedirect(reverse('edit_company', kwargs={'pk': self.kwargs['pk']}))


class ManageCompanyView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Company
    template_name = 'companies/edit.html'
    success_message = 'Company details successfully updated'
    form_class = CompanyForm
    success_url = reverse_lazy('manage_company')

    def get_object(self, queryset=None):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(ManageCompanyView, self).get_form_kwargs()
        kwargs['company'] = self.get_object()
        return kwargs


class ManageMasterCompanyView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Company
    form_class = CompanyForm
    template_name = 'companies/edit.html'
    success_message = 'Company details successfully updated'

    def get_success_url(self):
        company = self.get_company()
        return reverse('edit_master_company', kwargs={'pk': company.id})

    def get_object(self, queryset=None):
        try:
            return Company.objects.filter(id=self.kwargs['master_company_id']).first()
        except Exception as exception:
            return None

    def get_company(self):
        try:
            return Company.objects.filter(id=self.kwargs['master_company_id']).first()
        except Exception as exception:
            return None

    def get_form_kwargs(self):
        kwargs = super(ManageMasterCompanyView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['company'] = self.get_company()
        return kwargs


class DefaultCompanyAccountsView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Company
    form_class = DefaultCompanyAccountsForm
    template_name = 'companies/default_accounts.html'
    success_message = 'Company updated successfully'
    success_url = reverse_lazy('default_accounts')

    def get_object(self, queryset=None):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(DefaultCompanyAccountsView, self).get_form_kwargs()
        kwargs['company'] = self.get_object()
        return kwargs

    def form_valid(self, form):
        company = Company.objects.select_related(
            'default_distribution_account', 'default_flow_proposal', 'currency', 'default_vat_account',
            'default_expenditure_account', 'default_supplier_account', 'default_retained_income_account',
            'default_previous_year_account'
        ).prefetch_related(
            'company_years', 'branches', 'company_vat_codes'
        ).filter(id=self.request.session['company']).first()

        cache.set(f"company_{self.request.session['company']}", company)
        return super().form_valid(form)


class DefaultAccountsView(UpdateView):
    model = Company
    form_class = DefaultCompanyAccountsForm
    template_name = 'companies/default_accounts.html'
    success_message = 'Company updated successfully'
    success_url = reverse_lazy('default_accounts')

    def get_object(self, queryset=None):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(DefaultAccountsView, self).get_form_kwargs()
        kwargs['company'] = self.get_object()
        return kwargs


class CompanyDeleteView(LoginRequiredMixin, SuccessMessageMixin, View):
    success_message = 'Company deleted successfully'

    def get(self, request, *args, **kwargs):
        try:
            company = get_object_or_None(Company, pk=kwargs['pk'])
            if company:
                company.delete()
            messages.success(self.request, 'Company deleted successfully')
        except Exception as e:
            messages.error(self.request, 'Error occurred deleting the company, please try again')
        return HttpResponseRedirect(reverse('my-companies'))


class ActivateCompanyView(LoginRequiredMixin, View):
    success_message = 'Company deleted successfully'

    def get(self, request, *args, **kwargs):
        try:
            company = get_object_or_None(Company, pk=kwargs['pk'])
            if company:
                active_status = get_object_or_None(CompanyStatus, slug='active')
                if active_status:
                    company.status = active_status
                    company.save()
            messages.success(self.request, 'Company activated successfully')
        except Exception as e:
            messages.error(self.request, 'Error occurred activating the company, please try again')
        return HttpResponseRedirect(reverse('my-companies'))


class CompanyProfileView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    form_class = CompanyForm
    model = Company
    template_name = 'companies/profile.html'
    success_message = 'Company details updated successfully'
    success_url = reverse_lazy('edit-company-profile')

    def get_object(self, queryset=None):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(CompanyProfileView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['company'] = self.get_object()
        return kwargs


class ChangeCompanyView(LoginRequiredMixin, View):

    def get_company_url(self):
        return reverse('all-invoices')

    def get_company_role(self, company):
        role = Role.objects.filter(company=company).first()
        return role.id

    def get_role_profile(self, role):
        for profile in role.profile_set.all():
            if profile.user.is_active:
                return profile
        return role.company.administrator.profile

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            try:
                role = Role.objects.select_related('company').filter(id=self.kwargs['pk']).first()
                if role:
                    company = role.company
                    profile = self.get_role_profile(role)
                    if profile:
                        logout(self.request)

                        self.request.session['company'] = company.id
                        self.request.session['company_slug'] = company.slug
                        self.request.session['role'] = role.id
                        self.request.session['profile'] = profile.id
                        self.request.session['company_role'] = role.id

                        utils.set_company_url_conf(company.slug)
                        urls_config = "{}.routes.{}_urls".format('docuflow', company.slug)
                        set_urlconf(urls_config)
                        self.request.urlconf = urls_config

                        redirect_url = self.get_company_url()

                        login(self.request, profile.user)

                        return HttpResponse(
                            json.dumps({'error': False,
                                        'text': 'Company successfully updated',
                                        'redirect': redirect_url
                                        })
                        )
            except Exception as e:
                return HttpResponse(
                    json.dumps({'error': True,
                                'text': 'Login failed ',
                                'details': e.__str__()
                                })
                    )
            else:
                return HttpResponse(
                    json.dumps({'error': True,
                                'text': 'Company not found {} '.format(self.kwargs['pk']),
                                })
                )
        return HttpResponseRedirect(reverse('all_companies'))
