from docuflow.apps.company.models import Company


def get_company_by_slug(slug):
    try:
        return Company.objects.get(slug=slug)
    except Company.DoesNotExist:
        return None
    except Company.MultipleObjectsReturned:
        return None
