import logging
import os

from django.conf import settings

from docuflow.apps.common.utils import create_dir

logger = logging.getLogger(__name__)


def set_company_url_conf(slug: str, overwrite=True):
    app = 'docuflow'

    route_dir = os.path.join(settings.BASE_DIR, app, 'routes')
    create_dir(dir_name=route_dir)

    source = os.path.join(settings.BASE_DIR, app, 'urls', "company.py")
    destination = os.path.join(settings.BASE_DIR, app, "routes", f"{slug}_urls.py")

    if os.path.exists(destination) and not overwrite:
        return f"{app}.routes.{slug}_urls"

    with open(source, 'r') as r:
        contents = r.read().encode().decode('UTF-8')

    if contents:
        with open(destination, 'wb') as f:
            contents = contents.replace('company_slug', slug)
            f.write(bytes(contents, 'UTF-8'))

    return f"{app}.routes.{slug}_urls"


def get_company_dir(company):
    path = os.path.join(settings.MEDIA_ROOT, company.slug)
    return create_dir(dir_name=path)


