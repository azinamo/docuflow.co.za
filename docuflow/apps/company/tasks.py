import io
import os
import re
import subprocess

import pyocr
import pyocr.builders
from PIL import Image as PI
from celery import shared_task
from django.conf import settings
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox, LTTextLine
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from wand.image import Image

from docuflow.apps.company.models import Company


@shared_task
def ocr_template_document(supplier_template_id):
    successes = 0
    failed = 0
    template = Company.objects.get(pk=supplier_template_id)
    if template:
        print(template.docfile.path)
        result = extract_data(template.docfile.path)
        if result:
            template.contents = result
            template.save()

            create_default_template(result)

        successes = successes + 1

    return "{} template have been scanned successfully. {} failed to get the contents".format(successes, failed)


def create_default_template(contents):
    pass


def parse_lt_objs_and_group(lt_objs):
    """
    Method used inside "Convert" to process pdf extracted data and group
    Iterate through the list of LT* objects and capture the text data contained in each
            Try also to group some text which belongs to the same box
    """
    row_objs = dict()
    row_obj_list = []

    for lt_obj in lt_objs:
        if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
            # text
            y0 = lt_obj.y0
            y1 = lt_obj.y1
            key = str(y0) + "-" + str(y1)
            if key not in row_objs:
                row_objs[key] = list()
                row_objs[key].append(lt_obj.get_text().replace("\n", " "))
                row_obj_list.append(key)    # keep ordered
            else:
                row_objs[key].append(lt_obj.get_text().replace("\n", " "))

    output_list=[]
    # concatenate key-values
    for obj_key in row_obj_list:
        output_list.append(" ".join(row_objs[obj_key]) + "\n" )

    return output_list


def convert_with_pdfMiner(pdf_filename,  pages=None):
    """
    converts pdf, returns its text content as a string
    """
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    rsrcmgr = PDFResourceManager()
    laparams = LAParams()
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    infile = open(pdf_filename, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
        # receive the LTPage object for this page
        layout = device.get_result()
        rows = parse_lt_objs_and_group(layout)

    infile.close()

    return "".join(rows)


def strip_excess_whitespace(text):
    collapsed_spaces = re.sub(r"([^\r\n]+)", " ", text)
    no_leading_whitespace = re.sub("([\n\r]+)([^\n\r]+)", '\\1', collapsed_spaces)
    no_trailing_whitespace = re.sub("([^\n\r]+)$", '', no_leading_whitespace)
    return no_trailing_whitespace


def convert_with_tessaract(pdf_filename,  pages=None):
    tool = pyocr.get_available_tools()[0]
    lang = tool.get_available_languages()[3]

    req_image = []
    final_text = []

    image_pdf = Image(
        filename=pdf_filename,
        resolution=300)
    image_jpeg = image_pdf.convert('jpeg')

    for img in image_jpeg.sequence:
        img_page = Image(image=img)
        req_image.append(img_page.make_blob('jpeg'))

    for img in req_image:
        txt = tool.image_to_string(
            PI.open(io.BytesIO(img)),
            lang=lang,
            builder=pyocr.builders.TextBuilder()
        )
        final_text.append(strip_excess_whitespace(txt))

    return "".join(final_text)


def extract_data(invoicefile, templates=None, tmpl=None, debug=False):
    """
    extract data from text PDF file.
    """

    filename = str(invoicefile).lower().lower().replace(' ', '_').replace('-', '_').replace('[', '_').replace(']',
                                                                                                                    '').replace(
        ':', '').replace('#', '_')
    decrypted_filename = os.path.join(settings.CONSUMPTION_DECRYPTED_DIR, filename)
    decrypt_pdf(filename, decrypted_filename)

    extracted_str = convert_with_pdfMiner(invoicefile)

    charcount = len(extracted_str)

    # Disable Tesseract for now.
    if charcount < 40:
        print('Starting OCR')
        extracted_str = convert_with_tessaract(invoicefile)

    return extracted_str


def decrypt_pdf(source_file, destination_file):
    # used to generate temp file name. so we will not duplicate or replace anything
    try:
        subprocess.call(["qpdf", "--decrypt", source_file, destination_file])
        return True
    except Exception as err:
        pass
