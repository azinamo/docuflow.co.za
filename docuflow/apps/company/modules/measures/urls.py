from django.urls import path

from . import views

urlpatterns = [
    path('', views.MeasureView.as_view(), name='measure_index'),
    path('create/', views.CreateMeasureView.as_view(), name='create_measure'),
    path('<int:pk>/edit/', views.EditMeasureView.as_view(), name='edit_measure'),
    path('<int:pk>/delete/', views.DeleteMeasureView.as_view(), name='delete_measure'),
    path('<int:pk>/master/', views.UpdateMasterMeasureView.as_view(), name='update_master_measure'),
]
