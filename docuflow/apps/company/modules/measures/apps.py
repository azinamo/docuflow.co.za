from django.apps import AppConfig


class MeasuresConfig(AppConfig):
    name = 'docuflow.apps.company.modules.measures'
