import logging

from django.contrib import messages
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse

from annoying.functions import get_object_or_None

from docuflow.apps.company.models import Company, Measure
from .forms import MeasureForm

logger = logging.getLogger(__name__)
# Create your views here.


class MeasureView(LoginRequiredMixin, ListView):
    model = Measure
    template_name = 'measures/index.html'
    pagenate_by = 10
    context_object_name = 'measures'

    def get_context_data(self, **kwargs):
        context = super(MeasureView, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return Measure.objects.filter(
            company_id=self.request.session['company']
        ).order_by(
            'name'
        )


class CreateMeasureView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Measure
    template_name = 'measures/create.html'
    success_message = 'Measure successfully created'

    def get_success_url(self):
        return reverse('measure_index')

    def get_form_class(self):
        return MeasureForm

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_context_data(self, **kwargs):
        context = super(CreateMeasureView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        measure = form.save(commit=False)
        measure.company = self.get_company()
        measure.save()
        return HttpResponseRedirect(reverse('measure_index'))


class EditMeasureView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Measure
    template_name = 'measures/edit.html'
    success_message = 'Measure successfully updated'

    def get_form_class(self):
        return MeasureForm

    def get_success_url(self):
        return reverse('measure_index')


class DeleteMeasureView(LoginRequiredMixin, SuccessMessageMixin, View):
    success_url = reverse_lazy('measure_index')

    def get(self, request, *args, **kwargs):
        measure = get_object_or_None(Measure, id=self.kwargs['pk'])
        if measure:
            measure.delete()
            messages.success(self.request, 'Measure deleted successfully')
        return HttpResponseRedirect(reverse_lazy('measure_index'))


class UpdateMasterMeasureView(LoginRequiredMixin, View):

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_measure(self):
        return Measure.objects.get(pk=self.kwargs['pk'])

    def post(self, request, *args, **kwargs):
        try:
            measure = self.get_measure()
            if measure:
                company = self.get_company()
                for company_unit in company.units.all():
                    company_unit.is_default = False
                    company_unit.save()

                measure.is_default = True
                measure.save()

                self.request.session['company_unit_id'] = measure.id

                return JsonResponse({
                    'text': 'Default measure updated successfully',
                    'error': False
                    })
            else:
                return JsonResponse({
                    'text': 'Measure not found',
                    'error': True
                    })
        except Exception as exception:
            return JsonResponse({
                    'text': 'Unexpected error occurred, please try again',
                    'error': True,
                    'details': exception.__str__()
                })
