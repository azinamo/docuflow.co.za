from django import forms
from django.urls import reverse_lazy

from docuflow.apps.company.models import Measure


# Create your forms here.
class MeasureForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(MeasureForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Measure
        fields = ('name', 'extra_quantity', )



