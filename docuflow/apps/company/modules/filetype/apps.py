from django.apps import AppConfig


class FiletypeConfig(AppConfig):
    name = 'docuflow.apps.company.modules.filetype'
