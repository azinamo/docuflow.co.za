from django.urls import path

from . import views

urlpatterns = [
    path('', views.FileTypeView.as_view(), name='file_type_index'),
    path('create/', views.CreateFileTypeView.as_view(), name='create_file_type'),
    path('<int:pk>/edit/', views.EditFileTypeView.as_view(), name='edit_file_type'),
    path('<int:pk>/delete/', views.DeleteFileTypeView.as_view(), name='delete_file_type'),
    path('master/<int:master_company_id>/files/', views.FileTypeView.as_view(),
         name='master_file_type_index'),
    path('master/<int:master_company_id>/files/create/', views.CreateFileTypeView.as_view(),
         name='create_master_file_type'),
    path('master/<int:master_company_id>/files/<int:pk>/edit/', views.EditFileTypeView.as_view(),
         name='edit_master_file_type'),
    path('master/<int:master_company_id>/files/<int:pk>/delete/', views.DeleteFileTypeView.as_view(),
         name='delete_master_file_type'),
]
