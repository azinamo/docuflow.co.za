import logging

from django.utils.translation import gettext as __
from django.contrib import messages
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.http import HttpResponseRedirect

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import FileType, Company
from docuflow.apps.company.forms import FileTypeForm

logger = logging.getLogger(__name__)


class FileTypeView(LoginRequiredMixin, ProfileMixin, ListView):
    model = FileType
    context_object_name = 'file_types'

    def get_queryset(self):
        return super().get_queryset().filter(company=self.get_company())

    def get_template_names(self):
        if 'master_company_id' in self.kwargs:
            return 'filetypes/master_index.html'
        return 'filetypes/index.html'

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_context_data(self, **kwargs):
        context = super(FileTypeView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateFileTypeView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = FileType
    template_name = 'filetypes/create.html'
    form_class = FileTypeForm

    def get_context_data(self, **kwargs):
        context = super(CreateFileTypeView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_file_type_index',
                           kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('file_type_index')

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def form_valid(self, form):
        file_type = form.save(commit=False)
        file_type.company = self.get_company()
        file_type.save()
        messages.success(self.request, __('File type saved successfully'))
        return HttpResponseRedirect(self.get_success_url())


class EditFileTypeView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = FileType
    form_class = FileTypeForm
    template_name = 'filetypes/edit.html'
    success_message = __('File type successfully updated')

    def get_context_data(self, **kwargs):
        context = super(EditFileTypeView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_file_type_index',
                           kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('file_type_index')

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']


class DeleteFileTypeView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, DeleteView):
    model = FileType
    template_name = 'filetypes/confirm_delete.html'
    success_message = __('File type successfully deleted')

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_file_type_index',
                           kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('file_type_index')

    def get(self, request, *args, **kwargs):
        file_type = FileType.objects.get(pk=self.kwargs['pk'])
        file_type.delete()
        messages.success(self.request, __('File type deleted successfully'))
        return HttpResponseRedirect(self.get_success_url())
