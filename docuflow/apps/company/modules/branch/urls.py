from django.urls import path

from . import views

urlpatterns = [
    path('branch/', views.BranchView.as_view(), name='branch_index'),
    path('branch/create/', views.CreateBranchView.as_view(), name='create_branch'),
    path('branch/<int:pk>/edit/', views.EditBranchView.as_view(), name='edit_branch'),
    path('branch/<int:pk>/delete/', views.DeleteBranchView.as_view(), name='delete_branch'),
    path('branch/<int:pk>/master/', views.UpdateMasterBranchView.as_view(), name='update_master_branch'),
]
