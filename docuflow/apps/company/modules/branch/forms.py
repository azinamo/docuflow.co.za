from django import forms
from django.core.cache import cache

from docuflow.apps.company.models import Branch


class BranchForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        super(BranchForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Branch
        fields = ('label', 'address_line_1', 'address_line_2', 'town', 'area_code', 'is_head_office', 'is_invoicable')

    def save(self, commit=True):
        branch = super().save(commit=False)
        branch.company = self.company
        branch.save()

        for company_branch in self.company.branches.filter(is_head_office=True):
            if self.cleaned_data['is_head_office']:
                company_branch.is_head_office = False
            company_branch.save()

        branch.is_head_office = self.cleaned_data['is_head_office']
        branch.save()

        cache.set(f'profile_{self.profile.id}_branches', self.company.branches.filter(is_invoicable=True).all())

        return branch
