from django.apps import AppConfig


class BranchConfig(AppConfig):
    name = 'docuflow.apps.company.modules.branch'
