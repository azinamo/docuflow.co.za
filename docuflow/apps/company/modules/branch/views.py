import logging

from django.contrib import messages
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from annoying.functions import get_object_or_None

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Branch
from .forms import BranchForm


logger = logging.getLogger(__name__)


class BranchView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Branch
    template_name = 'branch/index.html'
    pagenate_by = 10
    context_object_name = 'branches'

    def get_queryset(self):
        return Branch.objects.filter(company=self.get_company())


class CreateBranchView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = Branch
    template_name = 'branch/create.html'
    success_message = 'Branch successfully created'
    success_url = reverse_lazy('branch_index')
    form_class = BranchForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        return kwargs


class EditBranchView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = Branch
    template_name = 'branch/edit.html'
    success_message = 'Branch successfully updated'
    success_url = reverse_lazy('branch_index')
    form_class = BranchForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        return kwargs


class DeleteBranchView(LoginRequiredMixin, SuccessMessageMixin, View):
    success_url = reverse_lazy('branch_index')

    def get(self, request, *args, **kwargs):
        branch = get_object_or_None(Branch, id=self.kwargs['pk'])
        if branch:
            branch.delete()
            messages.success(self.request, 'Branch deleted successfully')
        return HttpResponseRedirect(reverse_lazy('branch_index'))


class UpdateMasterBranchView(LoginRequiredMixin, ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            branch = Branch.objects.get(pk=self.kwargs['pk'])
            company = self.get_company()
            for company_branch in company.branches.all():
                company_branch.is_default = False
                company_branch.save()

            branch.is_default = True
            branch.save()

            self.request.session['company_branch_id'] = branch.id

            return JsonResponse({
                'text': 'Default branch updated successfully',
                'error': False
                })
        except Branch.DoesNotExist:
            return JsonResponse({
                    'text': 'Branch not found',
                    'error': True
                })