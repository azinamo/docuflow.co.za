from django.urls import path

from . import views

urlpatterns = [
    path('', views.VatCodeView.as_view(), name='vat_code_index'),
    path('list', views.VatCodeListView.as_view(), name='vat_code_list'),
    path('purchases/', views.PurchasesVatCodeView.as_view(), name='purchases_vat_code_index'),
    path('sales/', views.SalesVatCodeView.as_view(), name='sales_vat_code_index'),
    path('create/', views.CreateVatCodeView.as_view(), name='create_vatcode'),
    path('sales/create/', views.CreateSalesVatCodeView.as_view(), name='create_sales_vat_code'),
    path('<int:pk>/edit', views.EditVatCodeView.as_view(), name='edit_vatcode'),
    path('<int:pk>/delete', views.DeleteVatCodeView.as_view(), name='delete_vatcode'),
    path('autocomplete/search/', views.SearchVatCodeView.as_view(), name='autocomplete_vat_code'),
    path('account/vatcodes/list', views.AccountVatCodeListView.as_view(), name='account_vat_codes_list'),
    path('module/<str:type>/vatcodes/list', views.ModuleVatCodeListView.as_view(), name='module_vat_codes_list'),
    path('master/<int:master_company_id>/vatcode/', views.VatCodeView.as_view(), name='master_vat_code_index'),
    path('master/<int:master_company_id>/vatcode/create/', views.CreateVatCodeView.as_view(),
         name='create_master_vat_code'),
    path('master/<int:master_company_id>/vatcode/<int:pk>/edit', views.EditVatCodeView.as_view(),
         name='edit_vat_code'),
    path('master/<int:master_company_id>/vatcode/<int:pk>/delete', views.DeleteVatCodeView.as_view(),
         name='delete_master_vat_code'),
]
