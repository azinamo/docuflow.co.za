import json
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as __
from django.views.generic import TemplateView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django_filters.views import FilterView

from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.filters import VatCodeFilter
from docuflow.apps.company.forms import VatCodeForm, CompanyForm
from docuflow.apps.company.models import VatCode, Company

logger = logging.getLogger(__name__)


class VatCodeView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'vatcode/index.html'

    def get_context_data(self, **kwargs):
        context = super(VatCodeView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class PurchasesVatCodeView(LoginRequiredMixin, ProfileMixin, FilterView):
    model = VatCode
    filterset_class = VatCodeFilter
    pagenate_by = 10
    context_object_name = 'vat_codes'
    template_name = 'vatcode/purchase.html'

    def get_queryset(self):
        return VatCode.objects.input().filter(company=self.get_company()).exclude(deleted__isnull=False)

    def get_context_data(self, **kwargs):
        context = super(PurchasesVatCodeView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class SalesVatCodeView(LoginRequiredMixin, ProfileMixin, FilterView):
    model = VatCode
    filterset_class = VatCodeFilter
    pagenate_by = 10
    context_object_name = 'vat_codes'
    template_name = 'vatcode/sales.html'

    def get_queryset(self):
        return VatCode.objects.filter(company=self.get_company(), module=Module.SALES).exclude(deleted__isnull=False)

    def get_context_data(self, **kwargs):
        context = super(SalesVatCodeView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateVatCodeView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = VatCode
    form_class = VatCodeForm
    template_name = 'vatcode/create.html'
    success_message = __('Vat code successfully created')
    success_url = reverse_lazy('vat_code_index')

    def get_form_kwargs(self):
        kwargs = super(CreateVatCodeView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        vat_code = form.save(commit=False)
        vat_code.company = self.get_company()
        vat_code.module = Module.PURCHASE
        vat_code.save()
        return HttpResponseRedirect(reverse_lazy('vat_code_index'))


class CreateSalesVatCodeView(CreateVatCodeView):

    def form_valid(self, form):
        vat_code = form.save(commit=False)
        vat_code.company = self.get_company()
        vat_code.module = Module.SALES
        vat_code.save()
        return HttpResponseRedirect(reverse_lazy('vat_code_index'))


class EditVatCodeView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = VatCode
    form_class = VatCodeForm
    template_name = 'vatcode/edit.html'
    success_message = __('Vat code details successfully updated')
    success_url = reverse_lazy('vat_code_index')

    def get_form_kwargs(self):
        kwargs = super(EditVatCodeView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class DeleteVatCodeView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, DeleteView):
    model = VatCode
    template_name = 'vatcode/confirm_delete.html'
    success_message = __('Vat code deleted successfully')
    success_url = reverse_lazy('vat_code_index')


class AccountVatCodeListView(LoginRequiredMixin, TemplateView):

    def get_vat_codes(self):
        account_id = self.request.GET.get('account_id', None)
        if account_id:
            return VatCode.objects.filter(
                company_id=self.request.session['company'],
                account_id=account_id
            )

    def get(self, request, *args, **kwargs):
        vat_codes = self.get_vat_codes()
        codes = []
        for vat_code in vat_codes:
            codes.append({
                'id': vat_code.id,
                'label': vat_code.label,
                'code': vat_code.code,
                'percentage': vat_code.percentage,
                'is_default': vat_code.is_default
            })
        return JsonResponse(codes, safe=False)


class ModuleVatCodeListView(View):

    def get_vat_codes(self):
        q = VatCode.objects
        if self.kwargs['type'] == 'output':
            q = q.output()
        else:
            q = q.input()
        return q.filter(company_id=self.request.session['company'])

    def get(self, request, *args, **kwargs):
        vat_codes = self.get_vat_codes()
        codes = []
        for vat_code in vat_codes:
            codes.append({
                'id': vat_code.id,
                'label': vat_code.label,
                'code': vat_code.code,
                'percentage': vat_code.percentage,
                'is_default': vat_code.is_default
            })
        return JsonResponse(codes, safe=False)


class VatCodeListView(LoginRequiredMixin, TemplateView):

    def get_vat_codes(self):
        if float(self.request.GET['vat_amount']) > 0:
            return VatCode.objects.filter(company_id=self.request.session['company'], percentage__gt=0, is_active=True)
        elif float(self.request.GET['vat_amount']) <= 0:
            return VatCode.objects.filter(company_id=self.request.session['company'], percentage=0, is_active=True)

    def get(self, request, *args, **kwargs):
        vat_codes = self.get_vat_codes()
        codes = []
        for vat_code in vat_codes:
            codes.append({
                'id': vat_code.id,
                'label': vat_code.label,
                'code': vat_code.code,
                'percentage': vat_code.percentage,
                'is_default': vat_code.is_default
            })
        return JsonResponse(codes)


class VatCodeProfileView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    form_class = CompanyForm
    template_name = 'companies/profile.html'
    model = VatCode
    success_message = __('Company details updated successfully')
    success_url = reverse_lazy('edit-company-profile')

    def get_object(self, queryset=None):
        return VatCode.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(VatCodeProfileView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['company'] = self.get_object()
        return kwargs


class SearchVatCodeView(View):

    def get(self, request, *args, **kwargs):
        vat_codes = []
        if 'term' in self.request.GET:
            term = self.request.GET['term']
            q_object = ''
            if term:
                q_object = (Q(label__istartswith=term) | Q(code__istartswith=term))
            vat_codes = VatCode.objects.filter(company_id=self.request.session['company']).filter(q_object)
        vat_codes_list = []
        for vat_code in vat_codes:
            vat_codes_list.append({
                'code': vat_code.code,
                'text': f"{vat_code.code} - {vat_code.label}",
                'description': vat_code.label,
                'label': str(vat_code),
                'percentage': vat_code.percentage,
                'id': vat_code.id
            })
        return HttpResponse(json.dumps(vat_codes_list))
