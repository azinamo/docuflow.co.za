from django.apps import AppConfig


class VatcodeConfig(AppConfig):
    name = 'docuflow.apps.company.modules.vatcode'
