from django.apps import AppConfig


class DeliverymethodConfig(AppConfig):
    name = 'docuflow.apps.company.modules.deliverymethod'
