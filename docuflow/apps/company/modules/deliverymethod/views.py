import logging
import pprint

from django.contrib import messages
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.http import HttpResponseRedirect

from docuflow.apps.company.models import DeliveryMethod, Company
from docuflow.apps.accounts.models import Profile
from docuflow.apps.period.models import Year
from .forms import DeliveryMethodForm

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


# Create your views here.
class CompanyMixin(object):

    def get_company_year(self):
        company = self.get_company()
        return company.company_years.filter(is_active=True).first()

    def get_company(self):
        # company = cache.get('company', None)
        company = Company.objects.select_related(
            'default_distribution_account', 'default_flow_proposal', 'currency', 'default_vat_account',
            'default_expenditure_account', 'default_supplier_account'
        ).get(pk=self.request.session['company'])
        if not company:
            return company
        return company


class ProfileMixin(CompanyMixin):

    def get_profile_year(self):
        profile = Profile.objects.get(id=self.request.session.get('profile'))
        return profile.year

    def get_year(self):
        year_id = self.request.session.get('profile_year_id', None)
        if year_id:
            return Year.objects.get(pk=year_id)
        else:
            profile_year = self.get_profile_year()
            if profile_year:
                return profile_year
            else:
                return self.get_company_year()


class DeliveryMethodListJson(View):
    model = DeliveryMethod
    columns = ['id', 'code', 'name']
    order_columns = ['id', 'code', 'name']

    max_display_length = 30

    def render_column(self, row, column):
        # We want to render as a custom column
        return super(DeliveryMethodListJson, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(name__startswith=search)

        return qs


class DeliveryMethodView(ProfileMixin, ListView):
    model = DeliveryMethod
    template_name = 'deliverymethod/index.html'
    context_object_name = 'delivery_methods'

    def get_queryset(self):
        return DeliveryMethod.objects.select_related('company').filter(company=self.get_company()).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(DeliveryMethodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = DeliveryMethod
    template_name = 'deliverymethod/create.html'
    success_message = 'Delivery method successfully saved'

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryMethodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        return reverse('delivery_method_index')

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryMethodView, self).get_form_kwargs()
        return kwargs

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_class(self):
        return DeliveryMethodForm

    def form_valid(self, form):
        try:
            delivery_method = form.save(commit=False)
            company = self.get_company()
            delivery_method.company = company
            delivery_method.save()
            messages.success(self.request, 'Delivery method saved successfully')
            if 'redirect' in self.request.GET:
                return HttpResponseRedirect(self.request.GET['redirect'])
        except Exception as exception:
            messages.error(self.request, 'Unable to save delivery method;  {}.'.format(exception))
            return super(CreateDeliveryMethodView, self).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = DeliveryMethod
    template_name = 'deliverymethod/edit.html'
    success_message = 'Delivery method successfully updated'

    def get_context_data(self, **kwargs):
        context = super(EditDeliveryMethodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(EditDeliveryMethodView, self).get_form_kwargs()
        return kwargs

    def get_success_url(self):
        return reverse('delivery_method_index')

    def get_form_class(self):
        return DeliveryMethodForm


class DeleteDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = DeliveryMethod
    template_name = 'deliverymethod/confirm_delete.html'
    success_message = 'Delivery Method successfully deleted'

    def get_success_url(self):
        return reverse('delivery_method_index')

