from django.urls import path

from . import views

urlpatterns = [
    path('', views.DeliveryMethodView.as_view(), name='delivery_method_index'),
    path('create/', views.CreateDeliveryMethodView.as_view(), name='create_delivery_method'),
    path('edit/<int:pk>/', views.EditDeliveryMethodView.as_view(), name='edit_delivery_method'),
    path('delete/<int:pk>/', views.DeleteDeliveryMethodView.as_view(), name='delete_delivery_method'),
]
