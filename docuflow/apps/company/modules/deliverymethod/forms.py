from django import forms
from django.db.models import Q
from django.contrib.auth.models import User

from docuflow.apps.company.models import DeliveryMethod


class DeliveryMethodForm(forms.ModelForm):

    class Meta:
        model = DeliveryMethod
        fields = ('name', )


