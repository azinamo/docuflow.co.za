import logging
import json

from django.views.generic import ListView, View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.system.models import ReportType

logger = logging.getLogger(__name__)


class ReportTypeView(LoginRequiredMixin, ProfileMixin, ListView):
    model = ReportType
    template_name = 'reporttype/index.html'
    context_object_name = 'report_types'

    def get_context_data(self, **kwargs):
        context = super(ReportTypeView, self).get_context_data(**kwargs)
        return context


class AccountReportTypesView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        report_types = ReportType.objects.filter(account_type=self.request.GET['account_type'])
        _report_types = []
        for reporttype in report_types:
            _report_types.append({
                'name': reporttype.name,
                'id': reporttype.id
            })
        return HttpResponse(json.dumps(_report_types))
