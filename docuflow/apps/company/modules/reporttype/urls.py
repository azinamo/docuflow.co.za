from django.urls import path

from . import views

urlpatterns = [
    path('reporttype/', views.ReportTypeView.as_view(), name='report_type_index')
]
