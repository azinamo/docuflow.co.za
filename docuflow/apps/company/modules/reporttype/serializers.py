from rest_framework import serializers

from django.urls import reverse

from docuflow.apps.company.models import ReportType


class ReportTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportType
        fields = '__all__'
