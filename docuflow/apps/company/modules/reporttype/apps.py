from django.apps import AppConfig


class ReporttypeConfig(AppConfig):
    name = 'docuflow.apps.company.modules.reporttype'
