from django_filters import rest_framework as filters

from docuflow.apps.company.models import ReportType


class ReportTypeFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')
    code = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = ReportType
        fields = ['company__slug', 'name', 'code']

