import csv
import json
import logging
from calendar import monthrange
from datetime import datetime
from datetime import timedelta
from dateutil import parser
from django.utils import timezone
import pytz
import calendar
import pprint

from django.contrib import messages
from django.views.generic import ListView, TemplateView, View
from django_filters.views import FilterView
# from search_views.views import SearchListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.db.models import Q

from annoying.functions import get_object_or_None

from docuflow.apps.company.models import DefaultAccount, Company
from docuflow.apps.accounts.models import Profile
from docuflow.apps.period.models import Year, Period

from .forms import DefaultAccountsForm

from docuflow.apps.common.mixins import CompanyMixin, ProfileMixin

logger = logging.getLogger(__name__)
# Create your views here.

pp = pprint.PrettyPrinter(indent=4)


class DeliveryMethodListJson(View):
    model = DefaultAccount
    columns = ['id', 'code', 'name']
    order_columns = ['id', 'code', 'name']

    max_display_length = 30

    def render_column(self, row, column):
        # We want to render as a custom column
        return super(DeliveryMethodListJson, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(name__startswith=search)

        return qs


class DefaultAccountsView(ProfileMixin, TemplateView):
    template_name = 'defaultaccount/index.html'

    def get_context_data(self, **kwargs):
        context = super(DefaultAccountsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class ManageDefaultAccountsView(LoginRequiredMixin, SuccessMessageMixin, CompanyMixin, FormView):
    model = DefaultAccount
    template_name = 'defaultaccount/form.html'
    success_message = 'Default accounts successfully saved'

    def get_context_data(self, **kwargs):
        context = super(ManageDefaultAccountsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        return reverse('default_accounts_index')

    def get_object(self, queryset=None):
        return DefaultAccount.objects.filter(module=self.kwargs['module']).first()

    def get_form_kwargs(self):
        kwargs = super(ManageDefaultAccountsView, self).get_form_kwargs()
        company = self.get_company()
        kwargs['company'] = company
        return kwargs

    def get_form_class(self):
        return DefaultAccountsForm

    def form_valid(self, form):
        try:
            print(form.cleaned_data)
            print(self.request.POST)
            # default_account = form.save(commit=False)
            # company = self.get_company()
            # default_account.company = company
            # default_account.module = self.kwargs['module']
            # default_account.save()
            # messages.success(self.request, 'Default account saved successfully')
            if 'redirect' in self.request.GET:
                return HttpResponseRedirect(self.request.GET['redirect'])
        except Exception as exception:
            messages.error(self.request, 'Unable to save default accounts;  {}.'.format(exception))
            return super(ManageDefaultAccountsView, self).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = DefaultAccount
    template_name = 'defaultaccount/edit.html'
    success_message = 'Delivery method successfully updated'

    def get_context_data(self, **kwargs):
        context = super(EditDeliveryMethodView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(EditDeliveryMethodView, self).get_form_kwargs()
        return kwargs

    def get_success_url(self):
        return reverse('delivery_method_index')

    def get_form_class(self):
        return DefaultAccountsForm


class DeleteDeliveryMethodView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = DefaultAccount
    template_name = 'defaultaccount/confirm_delete.html'
    success_message = 'Delivery Method successfully deleted'

    def get_success_url(self):
        return reverse('delivery_method_index')

