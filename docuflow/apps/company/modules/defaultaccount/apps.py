from django.apps import AppConfig


class DefaultaccountConfig(AppConfig):
    name = 'docuflow.apps.company.modules.defaultaccount'
