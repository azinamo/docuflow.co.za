from django import forms

from docuflow.apps.company.models import DefaultAccount


class DefaultAccountsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(DefaultAccountsForm, self).__init__(*args, **kwargs)

        self.fields['expenditure_account'].queryset = self.fields['expenditure_account']\
            .queryset.filter(company=company)
        self.fields['vat_account'].queryset = self.fields['vat_account'].queryset.filter(
            company=company)
        self.fields['distribution_account'].queryset = self.fields['distribution_account']\
            .queryset.filter(company=company)
        self.fields['supplier_account'].queryset = self.fields['supplier_account'].queryset.filter(
            company=company)
        self.fields['discount_received_account'].queryset = self.fields['discount_received_account'].queryset.filter(
            company=company)
        self.fields['vat_payable_account'].queryset = self.fields['vat_payable_account'].queryset.filter(
            company=company)
        self.fields['vat_receivable_account'].queryset = self.fields['vat_receivable_account'].queryset.filter(
            company=company)
        self.fields['profit_on_asset_account'].queryset = self.fields['profit_on_asset_account'].queryset.filter(
            company=company)
        self.fields['loss_on_asset_account'].queryset = self.fields['loss_on_asset_account'].queryset.filter(
            company=company)
        self.fields['customer_account'].queryset = self.fields['customer_account'].queryset.filter(
            company=company)
        self.fields['goods_received_account'].queryset = self.fields['goods_received_account'].queryset.filter(
            company=company)
        self.fields['discount_allowed_account'].queryset = self.fields['discount_allowed_account'].queryset.filter(
            company=company)
        self.fields['stock_adjustment_account'].queryset = self.fields['stock_adjustment_account'].queryset.filter(
            company=company)

    class Meta:
        model = DefaultAccount
        fields = ['expenditure_account', 'vat_account', 'supplier_account',
                  'distribution_account', 'discount_received_account',
                  'discount_allowed_account', 'vat_payable_account', 'vat_receivable_account',
                  'loss_on_asset_account', 'profit_on_asset_account', 'customer_account',
                  'goods_received_account', 'stock_adjustment_account']
        widgets = {
            'expenditure_account': forms.Select(attrs={'class': 'chosen-select'}),
            'vat_account': forms.Select(attrs={'class': 'chosen-select'}),
            'supplier_account': forms.Select(attrs={'class': 'chosen-select'}),
            'distribution_account': forms.Select(attrs={'class': 'chosen-select'}),
            'flow_proposal': forms.Select(attrs={'class': 'chosen-select'}),
            'discount_received_account': forms.Select(attrs={'class': 'chosen-select'}),
            'vat_payable_account': forms.Select(attrs={'class': 'chosen-select'}),
            'vat_receivable_account': forms.Select(attrs={'class': 'chosen-select'}),
            'customer_account': forms.Select(attrs={'class': 'chosen-select'}),
            'loss_on_asset_account': forms.Select(attrs={'class': 'chosen-select'}),
            'profit_on_asset_account': forms.Select(attrs={'class': 'chosen-select'}),
            'goods_received_account': forms.Select(attrs={'class': 'chosen-select'}),
            'discount_allowed_account': forms.Select(attrs={'class': 'chosen-select'}),
            'stock_adjustment_account': forms.Select(attrs={'class': 'chosen-select'}),

        }

    def clean(self):
        cleaned_data = super(DefaultAccountsForm, self).clean()
        if self.instance.is_vat_registered is True:
            if cleaned_data['expenditure_account'] is None:
                self.add_error('expenditure_account', 'Default expenses account is required')
            if cleaned_data['vat_account'] is None:
                self.add_error('vat_account', 'Default vat account is required')
            if cleaned_data['supplier_account'] is None:
                self.add_error('supplier_account', 'Default supplier account is required')
            if cleaned_data['distribution_account'] is None:
                self.add_error('distribution_account', 'Default distribution account is required')
        return cleaned_data
