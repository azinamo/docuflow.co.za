from django.urls import path

from . import views

urlpatterns = [
    path('', views.DefaultAccountsView.as_view(), name='default_accounts_index'),
    path('manage/<int:module>/accounts/', views.ManageDefaultAccountsView.as_view(), name='default_accounts'),
]
