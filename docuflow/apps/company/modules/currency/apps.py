from django.apps import AppConfig


class CurrencyConfig(AppConfig):
    name = 'docuflow.apps.company.modules.currency'
