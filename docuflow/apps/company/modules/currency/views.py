import logging

from django.utils.translation import gettext as __
from django.contrib import messages
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.http import HttpResponseRedirect

from docuflow.apps.company.models import Currency, Company
from docuflow.apps.company.forms import CurrencyForm

logger = logging.getLogger(__name__)


# Create your views here.
class CurrencyView(LoginRequiredMixin, ListView):
    model = Currency
    context_object_name = 'currencies'

    def get_template_names(self):
        if 'master_company_id' in self.kwargs:
            return 'currency/master_index.html'
        return 'currency/index.html'

    def get_queryset(self):
        return Currency.objects.filter(company=self.get_company())

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_context_data(self, **kwargs):
        context = super(CurrencyView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateCurrencyView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Currency
    template_name = 'currency/create.html'
    success_message = __('Currency successfully saved')

    def get_context_data(self, **kwargs):
        context = super(CreateCurrencyView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_currency_index', kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('currency_index')

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_form_class(self):
        return CurrencyForm

    def form_valid(self, form):
        currency = form.save(commit=False)
        currency.company = self.get_company()
        try:
            currency.save()
        except Exception as err:
            messages.error(self.request, __('Unexpected error occurred, please try again.'))
            return super(CreateCurrencyView, self).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditCurrencyView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Currency
    template_name = 'currency/edit.html'
    success_message = __('Currency successfully updated')

    def get_context_data(self, **kwargs):
        context = super(EditCurrencyView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_currency_index', kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('currency_index')

    def get_form_class(self):
        return CurrencyForm


class DeleteCurrencyView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Currency
    template_name = 'currency/confirm_delete.html'
    success_message = __('Currency successfully deleted')

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_currency_index', kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('currency_index')

    def get(self, request, *args, **kwargs):
        try:
            currency = Currency.objects.get(pk=self.kwargs['pk'])
            currency.delete()
            messages.success(self.request, __('Currency deleted successfully'))
        except Exception as exception:
            messages.error(self.request, __('Unexpected error occurred'))
        return HttpResponseRedirect(self.get_success_url())
