from django.urls import path

from . import views

urlpatterns = [
    path('currency/', views.CurrencyView.as_view(), name='currency_index'),
    path('currency/create/', views.CreateCurrencyView.as_view(), name='create_currency'),
    path('currency/<int:pk>/edit/', views.EditCurrencyView.as_view(), name='edit_currency'),
    path('currency/<int:pk>/delete/', views.DeleteCurrencyView.as_view(), name='delete_currency'),
    path('master/<int:master_company_id>/currency/', views.CurrencyView.as_view(), name='master_currency_index'),
    path('master/<int:master_company_id>/currency/create/', views.CreateCurrencyView.as_view(),
         name='create_master_currency'),
    path('master/<int:master_company_id>/currency/<int:pk>/edit/', views.EditCurrencyView.as_view(),
         name='edit_master_currency'),
    path('master/<int:master_company_id>/currency/<int:pk>/delete/', views.DeleteCurrencyView.as_view(),
         name='delete_master_currency'),
]
