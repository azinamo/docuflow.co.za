from django.apps import AppConfig


class PaymentmethodConfig(AppConfig):
    name = 'docuflow.apps.company.modules.paymentmethod'
