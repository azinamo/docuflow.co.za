from django.urls import path

from . import views

urlpatterns = [
    path('', views.PaymentMethodView.as_view(), name='payment_method_index'),
    path('create/', views.CreatePaymentMethodView.as_view(), name='create_payment_method'),
    path('<int:pk>/edit/', views.EditPaymentMethodView.as_view(), name='edit_payment_method'),
    path('<int:pk>/delete/', views.DeletePaymentMethodView.as_view(), name='delete_payment_method'),
]
