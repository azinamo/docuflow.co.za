import logging

from django.contrib import messages
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse, reverse_lazy

from annoying.functions import get_object_or_None

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import PaymentMethod
from .forms import PaymentMethodForm


logger = logging.getLogger(__name__)


class PaymentMethodView(LoginRequiredMixin, ProfileMixin, ListView):
    model = PaymentMethod
    template_name = 'paymentmethod/index.html'
    pagenate_by = 10
    context_object_name = 'payment_methods'

    def get_context_data(self, **kwargs):
        context = super(PaymentMethodView, self).get_context_data(**kwargs)
        context['modules'] = Module
        return context

    def get_queryset(self):
        return super().get_queryset().filter(company=self.get_company())


class CreatePaymentMethodView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = PaymentMethod
    template_name = 'paymentmethod/create.html'
    success_message = 'Payment method successfully created'
    context_object_name = 'payment_method'
    form_class = PaymentMethodForm
    success_url = reverse_lazy('payment_method_index')

    def get_context_data(self, **kwargs):
        context = super(CreatePaymentMethodView, self).get_context_data(**kwargs)
        context['modules'] = Module
        return context

    def get_form_kwargs(self):
        kwargs = super(CreatePaymentMethodView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        form.save(commit=False)
        return HttpResponseRedirect(reverse('payment_method_index'))


class EditPaymentMethodView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = PaymentMethod
    form_class = PaymentMethodForm
    template_name = 'paymentmethod/edit.html'
    success_message = 'Payment method successfully updated'
    context_object_name = 'payment_method'
    success_url = reverse_lazy('payment_method_index')

    def get_form_kwargs(self):
        kwargs = super(EditPaymentMethodView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditPaymentMethodView, self).get_context_data(**kwargs)
        context['modules'] = Module
        return context


class DeletePaymentMethodView(LoginRequiredMixin, SuccessMessageMixin, View):
    success_url = reverse_lazy('payment_method_index')

    def get(self, request, *args, **kwargs):
        payment_method = get_object_or_None(PaymentMethod, id=self.kwargs['pk'])
        if payment_method:
            payment_method.delete()
            messages.success(self.request, 'Payment method deleted successfully')
        return HttpResponseRedirect(reverse_lazy('payment_method_index'))

