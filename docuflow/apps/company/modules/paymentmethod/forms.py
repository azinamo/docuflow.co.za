from django import forms

from docuflow.apps.company.models import PaymentMethod


class PaymentMethodForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(PaymentMethodForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.company)

    class Meta:
        model = PaymentMethod
        fields = ('payment_option', 'name', 'is_active', 'modules', 'is_depositable', 'account', 'journal_style')

        widgets = {
            'payment_option': forms.Select(attrs={'class': 'chosen-select'}),
            'account': forms.Select(attrs={'class': 'chosen-select'}),
            'journal_style': forms.RadioSelect(),
        }

    def save(self, commit=True):
        payment_method = super().save(commit=False)
        payment_method.company = self.company
        payment_method.save()

        # Will choose the payment method on cashup
        if payment_method.is_depositable:
            payment_method.account = None
            payment_method.save()

        return payment_method
