from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import PaymentMethod


def get_payment_options(company, payments=None):
    payments = payments or {}
    payment_methods = {}
    for method in PaymentMethod.objects.filter(company=company, modules__contains=Module.SALES.value):
        if method.id in payments:
            payment_methods[method] = payments[method.id]
        else:
            payment_methods[method] = None
    return payment_methods
