import logging

from django.utils.translation import gettext as __
from django.contrib import messages
from django.views.generic import ListView, TemplateView, View
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from annoying.functions import get_object_or_None
from django.http import HttpResponseRedirect, JsonResponse

from docuflow.apps.company.models import Company, CompanyObject, ObjectItem
from docuflow.apps.common.mixins import ProfileMixin
from .forms import ObjectItemForm, CompanyObjectForm

logger = logging.getLogger(__name__)


class CompanyObjectView(LoginRequiredMixin, ProfileMixin, ListView):
    model = CompanyObject
    template_name = 'objects/index.html'
    context_object_name = 'objects'

    def get_queryset(self):
        return CompanyObject.objects.prefetch_related('object_items').filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(CompanyObjectView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateObjectView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = CompanyObject
    form_class = CompanyObjectForm
    success_url = reverse_lazy('company_objects_index')
    template_name = 'objects/create.html'
    success_message = __('Object successfully created')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save(commit=False)
            return JsonResponse({
                'text': __('Object successfully updated'),
                'error': False,
                'reload': True
            })
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditObjectView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = CompanyObject
    template_name = 'objects/edit.html'
    success_message = __('Object details successfully updated')
    form_class = CompanyObjectForm
    success_url = reverse_lazy('company_objects_index')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            return JsonResponse({
                'text': 'Object successfully updated',
                'error': False,
                'reload': True
                })
        else:
            return HttpResponseRedirect(self.get_success_url())


class DeleteObjectView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get(self, *args, **kwargs):
        company_object = get_object_or_None(CompanyObject, pk=kwargs['pk'])
        company_object.delete()
        messages.success(self.request, 'Object deleted successfully')
        return HttpResponseRedirect(reverse_lazy('company_objects_index'))


class ObjectItemView(LoginRequiredMixin, ListView):
    model = ObjectItem
    template_name = 'objectitems/index.html'
    pagenate_by = 10
    context_object_name = 'objectitems'

    def get_context_data(self, **kwargs):
        context = super(ObjectItemView, self).get_context_data(**kwargs)
        context['object'] = get_object_or_None(CompanyObject, pk=self.kwargs['object_id'])
        return context

    def get_queryset(self):
        return ObjectItem.objects.filter(company_object_id=self.kwargs['object_id'])


class CreateObjectItemView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = ObjectItem
    template_name = 'objectitems/create.html'
    success_message = __('Object Item successfully created')

    def get_success_url(self):
        return reverse('object_item_index', kwargs={'object_id': self.kwargs['object_id']})

    def get_form_class(self):
        return ObjectItemForm

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_context_data(self, **kwargs):
        context = super(CreateObjectItemView, self).get_context_data(**kwargs)
        context['object'] = get_object_or_None(CompanyObject, pk=self.kwargs['object_id'])
        return context

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                object_item = form.save(commit=False)
                object_item.company = self.get_company()
                object_item.company_object_id = self.kwargs['object_id']
                object_item.save()
                return JsonResponse({
                    'text': 'Object item successfully saved',
                    'error': False,
                    'reload': True
                })
            except Exception as exception:
                return JsonResponse({
                    'text': 'Unexpected error occurred',
                    'error': True,
                    'details': exception.__str__()
                })
        else:
            return HttpResponseRedirect(reverse('object_item_index', kwargs={'object_id': self.kwargs['object_id']}))


class EditObjectItemView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = ObjectItem
    template_name = 'objectitems/edit.html'
    success_message = __('Object item details successfully updated')

    def get_form_class(self):
        return ObjectItemForm

    def get_success_url(self):
        return reverse('object_item_index', kwargs={'object_id': self.kwargs['object_id']})

    def get_context_data(self, **kwargs):
        context = super(EditObjectItemView, self).get_context_data(**kwargs)
        context['object'] = get_object_or_None(CompanyObject, pk=self.kwargs['object_id'])
        context['object_item'] = get_object_or_None(ObjectItem, pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        if self.request.is_ajax():
            object_item = form.save(commit=False)
            object_item.save()
            return JsonResponse({
                'text': 'Object item successfully updated',
                'error': False,
                'reload': True
            })
        else:
            return HttpResponseRedirect(reverse('company_objects_index'))


class DeleteObjectItemView(LoginRequiredMixin, SuccessMessageMixin, View):

    def get(self, request, *args, **kwargs):
        object_item = get_object_or_None(ObjectItem, pk=self.kwargs['pk'])
        object_item.delete()
        return HttpResponseRedirect(reverse_lazy('company_objects_index'))


class CompanyUrlViews(LoginRequiredMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        object_item = get_object_or_None(ObjectItem, pk=self.kwargs['pk'])
        object_item.delete()
        return HttpResponseRedirect(reverse_lazy('object_item_index'))
