from django.apps import AppConfig


class ObjectsConfig(AppConfig):
    name = 'docuflow.apps.company.modules.objects'
