from django import forms
from django.core.cache import cache
from easy_thumbnails.widgets import ImageClearableFileInput

from docuflow.apps.system.models import ReportType
from docuflow.apps.company.models import CompanyObject, ObjectItem


class ChoiceForm(forms.Form):
    """
    Form to be used in side by side templates used to add or remove
    items from a many to many field
    """
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices', [])
        label = kwargs.pop('label', 'Selection')
        help_text = kwargs.pop('help_text', None)
        disabled_choices = kwargs.pop('disabled_choices', ())
        super(ChoiceForm, self).__init__(*args, **kwargs)
        self.fields['selection'].choices = choices
        self.fields['selection'].label = label
        self.fields['selection'].help_text = help_text
        self.fields['selection'].widget.disabled_choices = disabled_choices
        self.fields['selection'].widget.attrs.update(
            {'size': 14, 'class': 'choice_form'}
        )

    selection = forms.MultipleChoiceField()


class CompanyObjectForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(CompanyObjectForm, self).__init__(*args, **kwargs)
        self.fields['parent'].queryset = self.fields['parent'].queryset.filter(company=self.company)

    class Meta:
        model = CompanyObject
        fields = ('label', 'parent', 'modules', )

    def save(self, commit=True):
        company_object = super().save(commit=commit)
        company_object.company = self.company
        company_object.save()
        return company_object


class ObjectItemForm(forms.ModelForm):

    class Meta:
        model = ObjectItem
        fields = ('code', 'label', )
