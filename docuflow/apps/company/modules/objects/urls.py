from django.urls import path

from . import views

urlpatterns = [
    path('', views.CompanyObjectView.as_view(), name='company_objects_index'),
    path('create/', views.CreateObjectView.as_view(), name='create_object'),
    path('<int:pk>/edit/', views.EditObjectView.as_view(), name='edit_object'),
    path('<int:pk>/delete/', views.DeleteObjectView.as_view(), name='delete_object'),
    path('<int:object_id>/items/', views.ObjectItemView.as_view(), name='object_item_index'),
    path('<int:object_id>/items/create/', views.CreateObjectItemView.as_view(), name='create_object_item'),
    path('<int:object_id>/item/<int:pk>/edit/', views.EditObjectItemView.as_view(),
         name='edit_object_item'),
    path('<int:object_id>/item/<int:pk>/delete/', views.DeleteObjectItemView.as_view(),
         name='delete_object_item'),
    path('master/<int:master_company_id>', views.CompanyObjectView.as_view(),
         name='master_company_objects_index'),
    path('master/<int:master_company_id>/create/', views.CreateObjectView.as_view(),
         name='create_master_company_object'),
    path('master/<int:master_company_id>/<int:pk>/edit/', views.EditObjectView.as_view(),
         name='edit_master_company_object'),
    path('master/<int:master_company_id>/<int:pk>/delete/', views.DeleteObjectView.as_view(),
         name='delete_master_company_object'),
]
