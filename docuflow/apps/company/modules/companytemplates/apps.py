from django.apps import AppConfig


class CompanytemplatesConfig(AppConfig):
    name = 'docuflow.apps.company.modules.companytemplates'
