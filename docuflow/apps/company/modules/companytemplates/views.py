import json
import logging
import os
import re

import yaml
from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, TemplateView, View
from django.views.generic.edit import UpdateView, DeleteView, FormView

from docuflow.apps.company.forms import TemplateForm, SupplierTemplateForm
from docuflow.apps.company.models import Company
from docuflow.apps.company.tasks import ocr_template_document
from docuflow.apps.documents.models import Document
from docuflow.apps.supplier.models import Supplier

logger = logging.getLogger(__name__)


class TemplateIndexView(LoginRequiredMixin, ListView):
    model = Document
    template_name = 'template/index.html'
    context_object_name = 'templates'

    def get_queryset(self):
        company = self.request.session['company']
        return Document.objects.filter(company=company)


class CreateTemplateView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    template_name = 'template/create.html'
    success_message = 'Template successfully created'

    def get_success_url(self):
        return reverse('templates-index')

    def get_form_class(self):
        return TemplateForm

    def form_valid(self, form):
        template = form.save(commit=False)
        template.company = self.request.session['company']
        template.save()
        return HttpResponseRedirect(reverse('template-index', args=(template.id,)))


class EditTemplateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Document
    template_name = 'template/edit.html'
    success_message = 'Template successfully updated.'

    def get_success_url(self):
        return reverse('templates-index')

    def get_form_class(self):
        return TemplateForm


class DefineTemplateView(LoginRequiredMixin, SuccessMessageMixin, TemplateView):
    template_name = 'template/define.html'
    success_message = 'Template successfully updated.'

    def get_object(self):
        return Document.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):

        context = super(DefineTemplateView, self).get_context_data(**kwargs)
        template = self.get_object()
        context['template'] = template
        contents = None
        if template.contents:
            cnts = template.contents
            contents = "<br />".join(cnts.split("\n"))

        context['contents'] = contents
        context['options'] = (('w', 'Word'), ('d', 'Number'), ('date', 'Date'))
        fields = [
            {'id': 'supplier_number', 'label': 'Supplier Number', 'ie': 'Supplier 12W09X2018'},
            {'id': 'invoice_number', 'label': 'Invoice Number', 'ie': 'Invoice Number 12W09X2018'},
            {'id': 'order_number', 'label': 'Order Number', 'ie':'Document No 12W09X2018'},
            {'id': 'invoice_date', 'label': 'Invoice Date*', 'ie': 'Date 12/09/2018'},
            {'id': 'due_date', 'label': 'Due Date', 'ie': 'Invoice Date 12/09/2018'},
            {'id': 'total_amount', 'label': 'Total Amount*', 'ie': ' Total R1.900.00'},
            {'id': 'vat_amount', 'label': 'Vat Amount', 'ie': 'Tax R190.00'},
            {'id': 'telephone', 'label': 'Telephone', 'ie': ''}
        ]
        context['invoice_fields'] = fields
        return context


class SaveTemplateInvoiceFieldsView(LoginRequiredMixin, View):

    def get_object(self):
        return Document.objects.get(pk=self.kwargs['pk'])

    def post(self, request, *args, **kwargs):
        template = self.get_object()
        print(self.request.POST)
        for field in self.request.POST:
            print(field)
            # if field in self.request.POST:
            #     if self.request.POST[field] != '':
            #         TemplateInvoiceFieldMapping.objects.create(
            #             field_name=field,
            #             template=template,
            #             regex=self.request.POST[field],
            #             pattern=field
            #         )

        messages.success(request, 'Updated the field mappings')
        return HttpResponseRedirect(reverse('define_template', args=(template.id, )))


class TemplateFieldDefinitionView(LoginRequiredMixin, TemplateView):
    template_name = 'template/define_template.html'
    success_message = 'Template successfully updated.'

    def get_object(self):
        if 'supplier_id' in self.kwargs:
            supplier = Supplier.objects.get(pk=self.kwargs['supplier_id'])
            if supplier.template:
                return supplier.template
        else:
            return Document.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(TemplateFieldDefinitionView, self).get_context_data(**kwargs)
        template = self.get_object()
        context['template'] = template
        contents = ""
        if template.contents:
            contents = "<br />".join(template.contents.split("\n"))

        context['contents'] = contents

        docs = Document.objects.filter(template_id=template.id)
        c = 0
        document = None
        for doc in docs:
            if c == 0:
                document = doc
                break
        context['document'] = document

        context['options'] = (('w', 'Word'), ('d', 'Number'), ('date', 'Date'), ('money', 'Money'))
        fields = [
            {'id': 'supplier_number', 'label': 'Supplier Number', 'ie': 'Supplier 12W09X2018'},
            {'id': 'invoice_number', 'label': 'Invoice Number', 'ie': 'Invoice Number 12W09X2018'},
            {'id': 'order_number', 'label': 'Order Number', 'ie':'Document No 12W09X2018'},
            {'id': 'invoice_date', 'label': 'Invoice Date*', 'ie': 'Date 12/09/2018'},
            {'id': 'vat_number', 'label': 'Vat Number', 'ie': 'VAT Number 12092018'},
            {'id': 'due_date', 'label': 'Due Date', 'ie': 'Invoice Date 12/09/2018'},
            {'id': 'total_amount', 'label': 'Total Amount*', 'ie': ' Total R1.900.00'},
            {'id': 'vat_amount', 'label': 'Vat Amount', 'ie': 'Tax R190.00'},
        ]
        context['invoice_fields']  = fields
        context['text'] = self.request.GET['text']
        return context


class DeleteTemplateView(LoginRequiredMixin, DeleteView):
    model = Document
    template_name = 'template/confirm_delete.html'
    success_url = reverse_lazy('templates-index')


class CreateSupplierTemplateView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    template_name = 'supplier/template/create.html'
    success_message = 'Supplier template successfully created.'

    def get_success_url(self):
        return reverse('create-supplier-domains', args=(self.kwargs['supplier_id'],))

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(CreateSupplierTemplateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_form_class(self):
        return SupplierTemplateForm

    def get_object(self):
        return Supplier.objects.get(pk=self.kwargs['supplier_id'])

    def form_valid(self, form):
        filename = None
        docfile = None
        supplier = self.get_object()
        supplier_template = None
        if supplier.template:
            supplier_template = get_object_or_None(Document, pk=supplier.template.id)

        can_ocr = False
        if form.cleaned_data['template']:
            filename = form.cleaned_data['template'].filename
        elif form.cleaned_data['docfile']:
            filename = ''
            if form.is_valid():
                docfile = self.request.FILES['docfile']
            can_ocr = True

            # self.handle_uploaded_file(form.cleaned_data['file'])

        if supplier_template:
            supplier_template.filename = filename
            if docfile:
                supplier_template.docfile = docfile
            supplier_template.label = form.cleaned_data['label']
            supplier_template.save()
        else:
            supplier_template = Document.objects.create(
                company=self.get_company(),
                label=form.cleaned_data['label'],
                filename=filename,
                docfile=docfile
            )
            if supplier_template:
                supplier.template = supplier_template
                supplier.save()

        if can_ocr and supplier_template:
            ocr_template_document.delay(supplier_template.id)

        return HttpResponseRedirect(reverse('supplier_template', kwargs={'supplier_id':self.kwargs['supplier_id']}))


class UpdateSupplierTemplateView(LoginRequiredMixin, View):

    def get_object(self):
        return Supplier.objects.get(pk=self.kwargs['supplier_id'])

    def date_formats(self):
        pass
        # formats = [
        #     {'format': 'dd / mm / yy', 'ie': '03/08/06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{2})'},
        #     {'format': 'dd / mm / yyyy', 'ie': '03/08/2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd / m / yyyy', 'ie': '3/8/2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'ddmmyy', 'ie': '030806', 'regex': '(\d{1,6})'},
        #     {'format': 'ddmmyyyy', 'ie': '03082006', 'regex': '(\d{1,8})'},
        #     {'format': 'ddmmmyy', 'ie': '03Aug06', 'regex': '(\d+\w+\d{2})'},
        #     {'format': 'ddmmmyyyy', 'ie': '03Aug2006', 'regex': '(\d+\w+\d{2})'},
        #     {'format': 'dd - mmm - yy', 'ie': '03-Aug-06', 'regex': '(\d{1,2}\/+\w\/+\d{4})'},
        #     {'format': 'dd - mmm - yyyy', 'ie': '03-Aug-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dmmmyy', 'ie': '3Aug06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dmmmyyyy', 'ie': '3Aug2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmm - yy', 'ie': '3-Aug-06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmm - yyyy', 'ie': '3-Aug-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmmm - yy', 'ie': '3-August-06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmmm - yyyy', 'ie': '3-August-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yymmdd', 'ie': '060803', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyymmdd', 'ie': '20060803', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yy / mm / dd', 'ie': '06/08/03', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyy / mm / dd', 'ie': '2006/08/03', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmddyy', 'ie': '080306', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmddyyyy', 'ie': '08032006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mm / dd / yy', 'ie': '08/03/06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mm / dd / yyyy', 'ie': '08/03/2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yy', 'ie': 'Aug-03-06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yyyy', 'ie': 'Aug-03-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yyyy', 'ie': 'Aug-03-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyy - mm - dd', 'ie': '2006-08-03', 'regex': '(\d{4}\-+\d{2}\-\d{2})'},
        #     {'format': 'dth mmmm yyyy', 'ie': '3 of August 200', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dd mmm yyyy', 'ie': '03 Aug 2006', 'regex': '(\d{1,2}\s+\w+\s+\d{4})'}
        # ]
        # return formats

    def money_formats(self):
        pass
        # formats = [
        #     {'format':'#.###,##', 'ie':'2.399.88', 'regex': '(\d+[\s+\.?+\d?]+\.\d+)'},
        #     {'format': '#,###.##', 'ie': '1,399.99', 'regex': '(\d+[\s+\,?+\d?]+\.\d+)'},
        #     {'format': '# ###.##', 'ie': '1 399.99', 'regex': '(\d+[\s+\,?+\d?]+\.\d+)'},
        #     {'format': '# ###,##', 'ie': '1 399,99', 'regex': '(\d+[\s+\,?+\d?]+,.\d+)'}
        # ]
        # return formats

    def post(self, request, *args, **kwargs):
        supplier = self.get_object()

        date_formats = self.date_formats()
        money_formats = self.money_formats()
        template_fields = {}
        for field in self.request.POST:
            t = field.split('_')[0]
            r = field.split('_')[1:]
            s = '_'.join(r)
            if s != '':
                if not s in template_fields:
                    template_fields[s] = {'pattern': '', 'before': '', 'after': '', 'type': '', 'regex': '', 'format': '', 'field_value': ''}

                if t == 'pattern':
                    template_fields[s]['pattern'] = self.request.POST[field]

                if t == 'before':
                    template_fields[s]['before'] = self.request.POST[field]

                if t == 'after':
                    template_fields[s]['after'] =  self.request.POST[field]

                if t == 'type':
                    template_fields[s]['type'] = self.request.POST[field]

                if t == 'format':
                    for dt in date_formats:
                        if self.request.POST[field] == dt['format']:
                            template_fields[s]['regex'] = dt['regex']
                    for my in money_formats:
                        if self.request.POST[field] == my['format']:
                            template_fields[s]['regex'] = my['regex']
                    template_fields[s]['format'] = self.request.POST[field]

        if supplier and supplier.template:
            template = get_object_or_None(Document, pk=supplier.template.id)

            fields = {}
            if template:
                for field, template_field in template_fields.items():
                    before = template_field['before']
                    after = template_field['after']
                    type = template_field['type']
                    pattern = template_field['pattern']
                    regex = template_field['regex']
                    format = template_field['format']
                    if before or after:
                        _before = "\\s+".join(before.split(' '))
                        _after = "\\s+".join(after.split(' '))
                        if type == 'w':
                            search_text = "{}{}{}".format(_before, '(\\w+)', _after)
                            fields[field] = search_text
                        elif type == 'd':
                            search_text = "{}{}{}".format(_before, '(\\d+)', _after)
                            if field == 'invoice_date':
                                fields['date'] = search_text
                            fields[field] = search_text
                        elif regex:
                            search_text = "{}{}{}".format(_before, regex, _after)
                            if field == 'total_amount':
                                fields['amount'] = search_text
                            if field == 'invoice_date':
                                fields['date'] = search_text
                            fields[field] = search_text

                    print('Field --> {} before --> {} and after {}'.format(field, before, after))
                    # mapping = get_object_or_None(TemplateInvoiceFieldMapping, template=template, field_name=field)
                    # if mapping:
                    #     mapping.type = type
                    #     mapping.before = before
                    #     mapping.after = after
                    #     mapping.pattern = pattern
                    #     mapping.regex = pattern
                    #     mapping.format = format
                    #     mapping.save()
                    # else:
                        # mapping = TemplateInvoiceFieldMapping.objects.create(
                        #     type=type,
                        #     before=before,
                        #     after=after,
                        #     field_name=field,
                        #     template=template,
                        #     pattern=pattern,
                        #     regex=regex,
                        #     format=format
                        # )
                data = {
                    'issuer': supplier.name,
                    'fields': fields,
                    'keywords': [
                       supplier.name
                    ],
                    'options': {
                        'remove_whitespace': False,
                        'currency': 'R'
                    }
                }
                filename = '_'.join([supplier.company.slug, supplier.code, '.yml'])
                file = os.path.join(settings.TEMPLATES_DIR, filename)

                f = open(file, 'w+', encoding='utf8')
                yaml.dump(data, f, default_flow_style=False, allow_unicode=True)
                f.close()

                template.filename = filename
                template.save()
                messages.success(request, 'Updated the field mappings')
            else:
                messages.error(request, 'Template not found, please try save again')
        else:
            messages.error(request, 'Supplier template not found, please try save again')
        return HttpResponseRedirect(reverse('supplier_template', kwargs={'supplier_id': supplier.id}))


class SupplierTemplateFieldValuesView(TemplateView):
    template_name = 'supplier/template/field_values.html'

    def get_object(self):
        return Supplier.objects.get(pk=self.kwargs['supplier_id'])

    def get_template(self, supplier_id):
        supplier = get_object_or_None(Supplier, pk=supplier_id)
        if supplier.template:
            return supplier.template
        return None

    def get_context_data(self, **kwargs):

        context = super(SupplierTemplateFieldValuesView, self).get_context_data(**kwargs)
        supplier = self.get_object()

        field = self.kwargs['field_id']
        template = self.get_template(supplier_id=supplier.id)
        context['supplier'] = supplier
        context['field'] = field
        context['template'] = template

        fields = [
            {'id': 'invoice_number',
             'label': 'Invoice Number',
             'ie': '',
             'type': '',
             'before': '',
             'after': '',
             'options': ['Invoice No', 'Invoice Num', 'INV']
             },
            {'id': 'invoice_date',
             'label': 'Invoice Date*',
             'ie': '',
             'type': 'date',
             'before': '',
             'after': '',
             'options': [],
             'formats': []
             },
             {
                'id': 'due_date',
                'label': 'Due Date',
                'ie': '',
                'type': 'date',
                'before': '',
                'after': '',
                'options': [],
                'formats': []
             },
             {
                'id': 'total_amount',
                'label': 'Total Amount',
                'ie': '',
                'type': 'money',
                'before': '',
                'after': '',
                'formats': [],
                 'options': []
             },
             {
                'id': 'vat_amount',
                'label': 'Vat Amount',
                'ie': '',
                'type': 'money',
                'before': '',
                'after': '',
                'formats': [],
                 'options': []
             },
             {
                'id': 'reference_1',
                'label': 'Reference 1',
                'ie': '',
                'type': 'word',
                'before': '',
                'after': '',
                 'options': []
             },
             {
                'id': 'reference_2',
                'label': 'Reference 2',
                'ie': '',
                'type': '',
                'before': '',
                'after': '',
                 'options': []
             }
        ]
        field_data = None
        context['options'] = []
        template_field = get_object_or_None(TemplateInvoiceFieldMapping, field_name=field, template=template)
        for f in fields:
            if f['id'] == field:
                field_data = f
                context['options'] = f['options']

        if template_field and template_field.option_values:
            context['options'] = template_field.option_values.split(',')

        context['field_data'] = field_data
        context['invoice_fields']  = fields
        return context


class SaveTemplateFieldOptionValues(View):

    def get_template(self, supplier_id):
        supplier = get_object_or_None(Supplier, pk=supplier_id)
        if supplier and supplier.template:
            return supplier.template
        return None

    def post(self, request, *args, **kwargs):
        field = self.kwargs['field_id']
        supplier_id = self.kwargs['supplier_id']
        template = self.get_template(supplier_id)
        option_values = self.request.POST.getlist('option_values[]')

        if template:
            template_field = get_object_or_None(TemplateInvoiceFieldMapping, field_name=field, template=template)
            if template_field:
                template_field.option_values = ','.join(option_values)
                template_field.save()

            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Values successfully saved.',
                            })
            )
        else:
            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Template not found',
                            })
            )


class SupplierTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'supplier/template/define.html'
    success_message = 'Template successfully updated.'

    def get_object(self):
        return Supplier.objects.get(pk=self.kwargs['supplier_id'])

    def get_default_yaml(self):
        template_name = os.path.join(settings.TEMPLATES_DIR, 'pastel.yml')
        result = None
        with open(template_name, 'r') as stream:
            try:
                result = yaml.load(stream)
            except yaml.YAMLError as exception:
                print(exception)
        return result

    def date_formats(self):
        pass
        # formats = [
        #     {'format':'dd / mm / yy', 'ie':'03 / 08 / 06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dd / mm / yyyy', 'ie': '03 / 08 / 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd / m / yyyy', 'ie': '3 / 8 / 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'ddmmyy', 'ie': '030806', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'ddmmyyyy', 'ie': '03082006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'ddmmmyy', 'ie': '03Aug06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'ddmmmyyyy', 'ie': '03Aug2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dd - mmm - yy', 'ie': '03 - Aug - 06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dd - mmm - yyyy', 'ie': '03 - Aug - 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dmmmyy', 'ie': '3Aug06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dmmmyyyy', 'ie': '3Aug2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmm - yy', 'ie': '3 - Aug - 06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmm - yyyy', 'ie': '3 - Aug - 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmmm - yy', 'ie': '3 - August - 06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmmm - yyyy', 'ie': '3 - August - 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yymmdd', 'ie': '060803', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyymmdd', 'ie': '20060803', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yy / mm / dd', 'ie': '06 / 08 / 03', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyy / mm / dd', 'ie': '2006 / 08 / 03', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmddyy', 'ie': '080306', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmddyyyy', 'ie': '08032006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mm / dd / yy', 'ie': '08 / 03 / 06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mm / dd / yyyy', 'ie': '08 / 03 / 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yy', 'ie': 'Aug - 03 - 06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yyyy', 'ie': 'Aug - 03 - 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yyyy', 'ie': 'Aug - 03 - 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyy - mm - dd', 'ie': '2006 - 08 - 03', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dth mmmm yyyy', 'ie': '3 of August 200', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dd mmm yyyy', 'ie': '03 Aug 2006', 'regex': '(\d{1,2}\s+\w+\s+\d{4})'}
        # ]
        # return formats

    def money_formats(self):
        pass
        # formats = [
        #     {'format':'#.###,##', 'ie':'2.399.88', 'regex': '(\d+[\s+\.?+\d?]+\.\d+)'},
        #     {'format': '#,###.##', 'ie': '1,399.99', 'regex': '(\d+[\s+\,?+\d?]+\.\d+)'},
        #     {'format': '# ###.##', 'ie': '1 399.99', 'regex': '(\d+[\s+\,?+\d?]+\.\d+)'},
        #     {'format': '# ###,##', 'ie': '1 399,99', 'regex': '(\d+[\s+\,?+\d?]+,.\d+)'}
        # ]
        # return formats

    def get_context_data(self, **kwargs):

        context = super(SupplierTemplateView, self).get_context_data(**kwargs)
        supplier = self.get_object()

        context['supplier'] = supplier
        contents = ""
        template = None

        if supplier.template and supplier.template.contents:
            template = supplier.template
            contents = "<br />".join(supplier.template.contents.split("\n"))

        context['contents'] = contents
        context['document'] = None

        fields = [
            {'id': 'invoice_number', 'label': 'Invoice Number', 'ie': '', 'type': '', 'before': '', 'after': '', 'info':'Invoice No.<br /> INV<br /> Invoice Num'},
            {'id': 'invoice_date',
             'label': 'Invoice Date*',
             'ie': '',
             'type': 'date',
             'before': '',
             'after': '',
             'info':'Invoice No.<br /> INV<br /> Invoice Num',
             'formats': self.date_formats()
             },
             {
                'id': 'due_date',
                'label': 'Due Date',
                'ie': '',
                'type': 'date',
                'before': '',
                'after': '',
                'info':'Invoice No.<br /> INV<br /> Invoice Num',
                'formats': self.date_formats()
             },
             {
                'id': 'total_amount',
                'label': 'Total Amount*',
                'ie': '',
                'type': 'money',
                'before': '',
                'after': '',
                'formats': self.money_formats()
             },
             {
                'id': 'vat_amount',
                'label': 'Vat Amount',
                'ie': '',
                'type': 'money',
                'before': '',
                'after': '',
                'formats': self.money_formats()
             },
             {
                'id': 'reference_1',
                'label': 'Reference 1',
                'ie': '',
                'type': 'word',
                'before': '',
                'after': ''
             },
             {
                'id': 'reference_2',
                'label': 'Reference 2',
                'ie': '',
                'type': '',
                'before': '',
                'after': ''
             }
        ]
        context['options'] = (('w', 'Word'), ('d', 'Number'), ('date', 'Date'), ('money', 'Money'))
        mappings = {}
        if template:
            for mpn in template.invoice_field_mapping.all():
                for f in fields:
                    if f['id'] == mpn.field_name:
                        f['before'] = mpn.before
                        f['after'] = mpn.after
                        f['type'] = mpn.type
                        f['pattern'] = mpn.pattern
                        f['format'] = mpn.format
                        f['field_value'] = mpn.field_value
        else:
            default_template = self.get_default_yaml()
            for f in fields:
                if f['type'] == 'date':
                    f['before'] = 'Date'
                    # f['before'] = 'Date' default_template['fields']['date']
                if f['id'] == 'vat_number':
                    f['before'] = 'VAT Reg.:'
                    # f['before'] = default_template['fields']['vat_number']
                if f['id'] == 'total_amount':
                    f['before'] = 'Date '
                    # f['before'] = default_template['fields']['amount']
                if f['id'] == 'total_amount':
                    f['before'] = 'Total '
                    # f['before'] = default_template['fields']['amount']
                # print(default_template)
                # print()
                # print(default_template['fields']['amount'])
                # print(default_template['fields']['invoice_number'])
                # print(default_template['fields']['vat_number'])
                # print(default_template['fields']['sub_total'])

        context['invoice_fields']  = fields
        return context


class SupplierTemplateMatchRegView(View):

    def date_formats(self):
        pass
        # formats = [
        #     {'format': 'dd / mm / yy', 'ie': '03/08/06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{2})'},
        #     {'format': 'dd / mm / yyyy', 'ie': '03/08/2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd / m / yyyy', 'ie': '3/8/2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'ddmmyy', 'ie': '030806', 'regex': '(\d{1,6})'},
        #     {'format': 'ddmmyyyy', 'ie': '03082006', 'regex': '(\d{1,8})'},
        #     {'format': 'ddmmmyy', 'ie': '03Aug06', 'regex': '(\d+\w+\d{2})'},
        #     {'format': 'ddmmmyyyy', 'ie': '03Aug2006', 'regex': '(\d+\w+\d{2})'},
        #     {'format': 'dd - mmm - yy', 'ie': '03-Aug-06', 'regex': '(\d{1,2}\/+\w\/+\d{4})'},
        #     {'format': 'dd - mmm - yyyy', 'ie': '03-Aug-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dmmmyy', 'ie': '3Aug06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dmmmyyyy', 'ie': '3Aug2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmm - yy', 'ie': '3-Aug-06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmm - yyyy', 'ie': '3-Aug-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmmm - yy', 'ie': '3-August-06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'd - mmmm - yyyy', 'ie': '3-August-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yymmdd', 'ie': '060803', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyymmdd', 'ie': '20060803', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yy / mm / dd', 'ie': '06/08/03', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyy / mm / dd', 'ie': '2006/08/03', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmddyy', 'ie': '080306', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmddyyyy', 'ie': '08032006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mm / dd / yy', 'ie': '08/03/06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mm / dd / yyyy', 'ie': '08/03/2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yy', 'ie': 'Aug-03-06', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yyyy', 'ie': 'Aug-03-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'mmm - dd - yyyy', 'ie': 'Aug-03-2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'yyyy - mm - dd', 'ie': '2006-08-03', 'regex': '(\d{4}\-+\d{2}\-\d{2})'},
        #     {'format': 'dth mmm yyyy', 'ie': '3 of August 2006', 'regex': '(\d{1,2}\/+\d{1,2}\/+\d{4})'},
        #     {'format': 'dd mmm yyyy', 'ie': '03 Aug 2006', 'regex': '(\d{1,2}\s+\w+\s+\d{4})'}
        # ]
        # return formats

    def money_formats(self):
        pass
        # formats = [
        #     {'format':'#.###,##', 'ie':'2.399.88', 'regex': '(\d+[\s+\.?+\d?]+\.\d+)'},
        #     {'format': '#,###.##', 'ie': '1,399.99', 'regex': '(\d+[\s+\,?+\d?]+\.\d+)'},
        #     {'format': '# ###.##', 'ie': '1 399.99', 'regex': '(\d+[\s+\,?+\d?]+\.\d+)'},
        #     {'format': '# ###,##', 'ie': '1 399,99', 'regex': '(\d+[\s+\,?+\d?]+,.\d+)'}
        # ]
        # return formats

    def get_template_fields(self):
        template_fields = {}
        date_formats = self.date_formats()
        money_formats = self.money_formats()
        for field in self.request.POST:
            t = field.split('_')[0]
            r = field.split('_')[1:]
            s = '_'.join(r)
            if s != '':
                if not s in template_fields:
                    template_fields[s] = {'pattern': '', 'before': '', 'after': '', 'type': '', 'regex': '', 'value': ''}

                if t == 'pattern':
                    template_fields[s]['pattern'] = self.request.POST[field]

                if t == 'before':
                    template_fields[s]['before'] = self.request.POST[field]

                if t == 'after':
                    template_fields[s]['after'] = self.request.POST[field]

                if t == 'type':
                    template_fields[s]['type'] = self.request.POST[field]

                if t == 'format':
                    for dt in date_formats:
                        if self.request.POST[field] == dt['format']:
                            template_fields[s]['regex'] = dt['regex']
                    for my in money_formats:
                        if self.request.POST[field] == my['format']:
                            template_fields[s]['regex'] = my['regex']
                    template_fields[s]['format'] = self.request.POST[field]

        return template_fields

    def get_object(self, supplier_id):
        return Supplier.objects.get(pk=supplier_id)

    def get_supplier_template_contents(self, supplier_id):
        supplier = self.get_object(supplier_id)

        if supplier.template and supplier.template.contents:
            return supplier.template.contents
        return None

    def prepare_regex(self, str):
        result_str = str
        if result_str:
            result_str = re.sub('\\s+', ' ', result_str) # make multiple spaces just one space

        result_str = "\\s+".join(result_str.split(' '))
        for e in ['(', ')', '#', '%', '@', '-', '!']:
            result_str.replace(e, '\\{}'.format(e))

        return result_str

    def post(self, request, *args, **kwargs):
        supplier_contents = self.get_supplier_template_contents(kwargs['supplier_id'])
        if not supplier_contents:
            return HttpResponse(
                json.dumps({'error': False,'text': 'Template contents not found'
                            })
            )

        template_fields = self.get_template_fields()
        results = []
        fields_output = {}
        for k, template_field in template_fields.items():
            fields_output[k] = None
            if template_field['before'] or template_field['after']:
                before = self.prepare_regex(template_field['before'])
                after = self.prepare_regex(template_field['after'])

                if template_field['type'] == 'w':
                    search_text = "{}{}{}".format(before, '(\\w+)', after)
                    print("Word -- > Search in text is {} {}, {}".format(search_text, k, template_field))
                    res_find = re.findall(search_text, supplier_contents)
                    if res_find:
                        fields_output[k] = res_find[0]
                # elif template_field['type'] == 'money':
                #     output = {k: None}
                #     search_text = "{}{}{}".format(before, '(\d+[\s+\d?]+\.\d+)', after)
                #     print("Has regex, search in text is {} {}, {}".format(search_text, k, template_field))
                #     res_find = re.findall(search_text, supplier_contents)
                #     if res_find:
                #         output[k] = res_find[0]
                #         results.append(output)
                elif template_field['type'] == 'd':
                    search_text = "{}{}{}".format(before, '([0-9]]+)', after)
                    res_find = re.findall(search_text, supplier_contents)
                    print("Number -->< Search in text is {} {}, {}".format(search_text, k, template_field))
                    if res_find:
                        fields_output[k] = res_find[0]
                elif template_field['regex']:
                    search_text = "{}{}{}".format(before, template_field['regex'], after)

                    print("Regex -> search in text is {} {}, {}".format(search_text, k, template_field))

                    res_find = re.findall(search_text, supplier_contents)
                    if res_find:
                        fields_output[k] = res_find[0]
                    # matching = re.
        return HttpResponse(
            json.dumps({'error': False,
                        'text': 'Processing finished',
                        'results': fields_output
                        })
        )


class SaveTemplateFieldDefinitionView(LoginRequiredMixin, View):
    def get_object(self):
        return Document.objects.get(pk=self.kwargs['pk'])

    def post(self, request, *args, **kwargs):
        template = self.get_object()
        if template:
            field  = self.request.POST['field']
            before = self.request.POST['before']
            after = self.request.POST['after']
            type = self.request.POST['type']
            pattern = self.request.POST['pattern']

            # mapping = get_object_or_None(TemplateInvoiceFieldMapping, template=template, field_name=field)
            # if mapping:
            #     mapping.type = type
            #     mapping.before = before
            #     mapping.after = after
            #     mapping.pattern = pattern
            #     mapping.save()
            # else:
            #     mapping = TemplateInvoiceFieldMapping.objects.create(
            #         type=type,
            #         before=before,
            #         after=after,
            #         field_name=field,
            #         template=template,
            #         pattern=pattern
            #     )
            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Mapping successfully saved.',
                            })
            )
        else:
            return HttpResponse(
                json.dumps({'error': False,
                            'text': 'Template not found',
                            })
            )
