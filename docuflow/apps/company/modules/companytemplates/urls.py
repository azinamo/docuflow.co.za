from django.urls import path

from . import views

urlpatterns = [
    path('', views.TemplateIndexView.as_view(), name='template-index'),
    path('create/', views.CreateTemplateView.as_view(), name='create-template'),
    path('<int:pk>/edit/', views.EditTemplateView.as_view(), name='edit-template'),
    path('<int:pk>/delete/', views.DeleteTemplateView.as_view(), name='delete-template'),
    path('<int:pk>/define/', views.DefineTemplateView.as_view(), name='define_template'),
    path('save/<int:pk>/define/', views.SaveTemplateInvoiceFieldsView.as_view(), name='save_template_invoice_fields'),
    path('field/define/<int:pk>/', views.TemplateFieldDefinitionView.as_view(), name='field_definition'),
    path('save/field/define/<int:pk>/', views.SaveTemplateFieldDefinitionView.as_view(), name='define_field_form'),
    path('supplier/<int:supplier_id>/', views.SupplierTemplateView.as_view(), name='supplier_template'),
    path('supplier/<int:supplier_id>/update/', views.UpdateSupplierTemplateView.as_view(),
         name='update_supplier_template'),
    path('supplier/<int:supplier_id>/create/', views.CreateSupplierTemplateView.as_view(),
         name='create_supplier_template'),
    path('supplier/field/define/<int:supplier_id>/', views.TemplateFieldDefinitionView.as_view(),
         name='supplier_template_field_definition'),
    path('supplier/<int:supplier_id>/match/', views.SupplierTemplateMatchRegView.as_view(),
         name='supplier_template_match_reg'),
    path('supplier/<int:supplier_id>/<str:field_id>/field/options/', views.SupplierTemplateFieldValuesView.as_view(),
         name='template_field_value_options'),
    path('supplier/save/<int:supplier_id>/<str:field_id>/field/values/', views.SaveTemplateFieldOptionValues.as_view(),
         name='save_template_field_value_options'),
]
