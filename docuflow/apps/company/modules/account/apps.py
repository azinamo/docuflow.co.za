from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = 'docuflow.apps.company.modules.account'
