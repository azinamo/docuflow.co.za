import xlrd


class ExcelAccountUploadClient:

    # noinspection PyMethodMayBeStatic
    def get_upload_data(self, file):
        counter = 0
        data = []
        book = xlrd.open_workbook(file_contents=file.read())
        sh = book.sheet_by_index(0)
        for rx in range(sh.nrows):
            if counter > 0:
                data[counter] = {}
                for c, cell in enumerate(sh.row(rx)):
                    data[counter][c] = cell.value
            counter += 1
        return data


class CsvAccountUploadClient:

    # noinspection PyMethodMayBeStatic
    def get_upload_data(self, file):
        file_data = file.read().decode("utf-8")
        lines = file_data.split("\n")
        counter = 0
        data = []
        for i, line in enumerate(lines):
            if counter > 0:
                fields = line.split(",")
                if len(fields) > 1 and (fields[0] and fields[1]):
                    data[counter] = fields
        return data


csv_upload_client = CsvAccountUploadClient()
excell_upload_client = ExcelAccountUploadClient()