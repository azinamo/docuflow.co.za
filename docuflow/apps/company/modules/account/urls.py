from django.urls import path

from . import views

urlpatterns = [
    path('', views.AccountsView.as_view(), name='account_list'),
    path('create/', views.CreateAccountView.as_view(), name='create_account'),
    path('<int:pk>/edit/', views.EditAccountView.as_view(), name='edit_account'),
    path('<int:pk>/delete/', views.DeleteAccountView.as_view(), name='delete_account'),
    path('import/', views.ImportAccountsView.as_view(), name='import_accounts'),
    path('export/', views.ExportAccountsView.as_view(), name='export_accounts'),
    path('autocomplete/search/', views.AccountsSearchView.as_view(), name='autocomplete_accounts_search'),
    path('search/', views.AccountSearchView.as_view(), name='company_accounts_search'),
    path('account/reporttype/list/', views.AccountReportTypesView.as_view(), name='account_report_types'),
]
