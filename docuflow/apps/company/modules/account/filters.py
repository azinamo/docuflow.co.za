from django_filters import rest_framework as filters

from docuflow.apps.company.models import Account


class AccountFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')
    code = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Account
        fields = ['company__slug', 'name', 'code']

