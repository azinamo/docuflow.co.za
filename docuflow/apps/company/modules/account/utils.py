import logging
from datetime import datetime
from openpyxl import Workbook

from django.conf import settings

from docuflow.apps.company.models import Account, VatCode
from docuflow.apps.system.enums import AccountType
from docuflow.apps.common.utils import get_cell_value
from docuflow.apps.system.models import ReportType, CashFlowCategory
from .client import excell_upload_client, csv_upload_client

logger = logging.getLogger(__name__)


class AccountImportException(Exception):
    pass


def get_line_period(periods, journal_date):
    if not periods:
        return None
    try:
        return next(period for _, period in periods.items() if period.start_date <= journal_date.date() <= period.end_date)
    except StopIteration:
        return None


class ImportAccountJob(object):

    def __init__(self, company, file):
        self.company = company
        self.file = file
        self.account_types = {}
        self.branches = {}
        self.report_types = {}
        self.vat_codes = {}
        self.cash_flow_categories = {}
        self.missing_data = []

    def execute(self):
        accounts_data = {}
        if self.file.name.endswith('.csv'):
            accounts_data = csv_upload_client.get_upload_data(file=self.file)
        elif self.file.name.endswith('.xls') or self.file.name.endswith('.xlsx'):
            accounts_data = excell_upload_client.get_upload_data(file=self.file)
        data = []
        for counter, row in accounts_data.items():
            data_dict = {'name': '', 'code': '', 'account_type': 1, 'report': None, 'sub_ledger': None,
                         'vat_reporting': None, 'cash_flow': None, 'not_deductable': False}
            for c, cell_value in row.items():
                if c == 1:
                    code = get_cell_value(cell_value)
                    data_dict['code'] = str(code).replace('.0', '')
                if c == 2:
                    data_dict['name'] = get_cell_value(cell_value)
                if c == 3:
                    data_dict['account_type'] = get_cell_value(cell_value)
                if c == 4:
                    data_dict['report'] = get_cell_value(cell_value)
                if c == 5:
                    data_dict['sub_ledger'] = get_cell_value(cell_value)
                if c == 6:
                    data_dict['vat_reporting'] = get_cell_value(cell_value)
                if c == 7:
                    data_dict['cash_flow'] = get_cell_value(cell_value)
                if c == 8:
                    data_dict['not_deductable'] = get_cell_value(cell_value)
                c += 1
            data.append(data_dict)
        if data:
            return self._sync(data)
        return None

    def _sync(self, accounts_data):
        """
            Sync accounts .
        """
        self.get_account_types(accounts_data)
        self.get_report_types()
        self.get_vat_codes()
        self.get_cash_flow_categories()

        if accounts_data:
            self.deactivate_accounts()

            existing_accounts = Account.all_objects.filter(
                code__in=[account['code'] for account in accounts_data]
            ).values_list('code', flat=True)

            data = self.prepare_accounts_data(accounts_data)

            if existing_accounts:
                self.update_accounts(data, existing_accounts)

            self.create_accounts(accounts_data=data, existing_accounts=existing_accounts)

            if self.missing_data:
                self.create_missing_data_report()

    def create_missing_data_report(self):
        wb = Workbook()
        ws1 = wb.active
        ws1.title = "Missing Accounts"

        row_count = 1
        for line in self.missing_data:
            ws1.cell(column=1, row=row_count, value=line['line']['department'])
            ws1.cell(column=2, row=row_count, value=line['line']['journal_number'])
            ws1.cell(column=3, row=row_count, value=line['line']['date'])
            ws1.cell(column=4, row=row_count, value=line['line']['text'])
            if line['missing'].get('branch', False):
                ws1.cell(column=5, row=row_count, value='Branch missing is missing from digits')
            if line['missing'].get('department', False):
                ws1.cell(column=6, row=row_count, value='Department is missing from digits')
            if line['missing'].get('sub_category', False):
                ws1.cell(column=7, row=row_count, value='Sub category is missing from digits')
            row_count += 1

        wb.save(filename=f'{settings.MEDIA_ROOT}/accounts/missing_data_{self.company.pk}.xlsx')

    # noinspection PyMethodMayBeStatic
    def prepare_accounts_data(self, accounts_data):
        data = []
        fields = {'name', 'code', 'account_type', 'report', 'sub_ledger', 'vat_reporting', 'cash_flow',
                  'not_deductable'}
        for line in accounts_data:
            can_add = True
            missing_data = {}
            line_data = {}
            for field in line:
                if field in fields:
                    if field == 'account_type':
                        acc_type = AccountType.get_value_from_label(label=str(line['account_type']))
                        if acc_type:
                            line_data[field] = acc_type
                    elif field == 'report':
                        report_type = self.report_types.get(str(line['report']))
                        if report_type:
                            line_data[field] = report_type
                    elif field == 'cash_flow':
                        cash_flow = self.cash_flow_categories.get(str(line['cash_flow']))
                        if cash_flow:
                            line_data[field] = cash_flow
                    elif field == 'vat_reporting':
                        cash_flow = self.vat_codes.get(str(line['vat_reporting']))
                        if cash_flow:
                            line_data[field] = cash_flow
                    elif field == 'not_deductable':
                        line_data[field] = True if line['not_deductable'] == 'Yes' else False
                    else:
                        line_data[field] = line[field]
            if can_add:
                data.append(line_data)
            elif missing_data:
                self.missing_data.append({'line': line, 'missing': missing_data})
        return data

    # noinspection PyMethodMayBeStatic
    def get_account_types(self, journals_data):
        pass
        # account_type = AccountType.get_value_from_label(get_cell_value(cell.value))
        # accounts = Account.objects.filter(code__in=[acc['account'] for acc in journals_data])
        # self.accounts = {account.code: account for account in accounts}

    def get_report_types(self):
        self.report_types = {str(report_type.name): report_type for report_type in ReportType.objects.all()}

    def get_vat_codes(self):
        self.vat_codes = {str(vat_code.code): vat_code for vat_code in VatCode.objects.filter(company=self.company)}

    def get_cash_flow_categories(self):
        self.cash_flow_categories = {str(cash_flow.name): cash_flow for cash_flow in CashFlowCategory.objects.all()}

    # noinspection PyMethodMayBeStatic
    def deactivate_accounts(self):
        for account in Account.all_objects.filter(company=self.company):
            account.is_active = False
            account.save()

    # noinspection PyMethodMayBeStatic
    def update_accounts(self, accounts_data, existing_accounts):
        """
            Creates new accounts in bulk.
        """
        fields = {'name', 'code', 'account_type', 'report', 'sub_ledger', 'report', 'cash_flow', 'not_deductable'}
        instances_to_update = []
        logging.info(accounts_data)
        for account in Account.all_objects.filter(code__in=existing_accounts):
            logging.info(f"get car data with registration {account.code}")
            try:
                account_data = next(c for c in accounts_data if c['code'] == account.code)
                for field in fields:
                    setattr(account, field, account_data[field])
                account.is_active = True
                instances_to_update.append(account)
            except StopIteration:
                continue

        if instances_to_update:
            Account.objects.bulk_update(instances_to_update, fields=fields)

    # noinspection PyMethodMayBeStatic
    def create_accounts(self, accounts_data, existing_accounts):
        """
            Creates new accounts in bulk.
        """
        account_lines = []
        for accounts_line in accounts_data:
            if accounts_line['code'] not in existing_accounts:
                account_lines.append(Account(company=self.company, is_active=True, **accounts_line))
        Account.objects.bulk_create(account_lines)

