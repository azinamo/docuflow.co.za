import logging
import pprint

from django import forms
from django.urls import reverse_lazy

from annoying.functions import get_object_or_None
import xlrd

from docuflow.apps.company.models import Account, VatCode
from docuflow.apps.system.models import ReportType, CashFlowCategory
from docuflow.apps.system.enums import AccountType
from docuflow.apps.company.enums import SubLedgerModule


pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class AccountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(AccountForm, self).__init__(*args, **kwargs)
        self.fields['vat_reporting'].queryset = VatCode.objects.filter(company=self.company)
        if self.instance.account_type:
            self.fields['report'].queryset = self.fields['report'].queryset.filter(account_type=self.instance.account_type)
        self.fields['account_type'].required = True

    class Meta:
        model = Account
        fields = ('name', 'code', 'account_type', 'report', 'vat_reporting', 'not_deductable', 'sub_ledger',
                  'cash_flow', 'is_active')

        widgets = {
            'account_type': forms.Select(attrs={'data-ajax-url': reverse_lazy('account_report_types')}),
            'report': forms.Select(attrs={'class': 'chosen-select'}),
            'sub_ledger': forms.Select(attrs={'class': 'chosen-select'}),
            'cash_flow': forms.Select(attrs={'class': 'chosen-select'})
        }

    def clean(self):
        cleaned_data = super(AccountForm, self).clean()
        if not cleaned_data.get('account_type'):
            self.add_error('account_type', 'Account type is required')
        query = Account.all_objects.filter(company=self.company, code=cleaned_data['code'])
        if self.instance:
            query = query.exclude(pk=self.instance.pk)
        if query.exists():
            self.add_error('code', 'Account code already exists')
        return cleaned_data


class ImportForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(ImportForm, self).__init__(*args, **kwargs)

    csv_file = forms.FileField(label='Upload file (*.csv | *.xls | .xlsx)')

    def clean(self):
        csv_file = self.cleaned_data['csv_file']
        if not (csv_file.name.endswith('.csv') or csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx')):
            self.add_error('csv_file', 'File is not .csv, .xlsx or xls type')

        if csv_file.multiple_chunks():
            self.add_error('csv_file', "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 1000),))

    def execute(self):
        logger.info("Start importing")
        csv_file = self.cleaned_data['csv_file']
        account_data = []
        if csv_file.name.endswith('.csv'):
            account_data = self.get_csv_data(csv_file)
        elif csv_file.name.endswith('.xls') or csv_file.name.endswith('.xlsx'):
            account_data = self.get_excell_data(csv_file)

        # loop over the lines and save them to the database. If there is an error store as string and display
        self.deactivate_accounts()

        import_result = self.save_accounts(account_data)

        return import_result

    # noinspection PyMethodMayBeStatic
    def deactivate_accounts(self):
        for account in Account.objects.filter(company=self.company):
            account.is_active = False
            account.save()

    def save_accounts(self, data):
        is_updated = 0
        is_new = 0
        total = 0
        for acc_row in data['account_data']:
            if 'code' in acc_row:
                code = acc_row.pop('code')
                account, is_created = Account.objects.update_or_create(
                    company=self.company,
                    code=code,
                    defaults=acc_row
                )
                logger.info(f"Account is updated {is_created} or created {not is_created}")
                account.is_active = True
                account.save()
                if is_updated:
                    is_updated += 1
                else:
                    is_new += 1
                total += 1
        return {'new': is_new, 'updated': is_updated, 'total': total}

    # noinspection PyMethodMayBeStatic
    def get_cell_value(self, term):
        if isinstance(term, str):
            return term.strip().strip('\n')
        return term

    # noinspection PyMethodMayBeStatic
    def get_excell_data(self, file):
        counter = 0
        data = []
        book = xlrd.open_workbook(file_contents=file.read())
        sh = book.sheet_by_index(0)
        for rx in range(sh.nrows):
            data_dict = {'name': '', 'code': '', 'account_type': 1, 'report': None, 'sub_ledger': None,
                         'vat_reporting': None, 'cash_flow': None, 'not_deductable': False}
            for c, cell in enumerate(sh.row(rx)):
                if c == 0:
                    code = self.get_cell_value(cell.value)
                    data_dict['code'] = str(code).replace('.0', '')
                if c == 1:
                    data_dict['name'] = self.get_cell_value(cell.value)
                if c == 2:
                    account_type = AccountType.get_value_from_label(self.get_cell_value(cell.value))
                    data_dict['account_type'] = account_type
                if c == 3:
                    report_type = get_object_or_None(ReportType, name=self.get_cell_value(cell.value))
                    if report_type and isinstance(report_type, ReportType):
                        data_dict['report'] = report_type
                if c == 4:
                    sub_ledger = SubLedgerModule.get_value_from_label(self.get_cell_value(cell.value))
                    data_dict['sub_ledger'] = sub_ledger
                if c == 5:
                    vat_report = get_object_or_None(VatCode, label=self.get_cell_value(cell.value),
                                                    company=self.company)
                    if vat_report:
                        data_dict['vat_reporting'] = vat_report
                if c == 6:
                    cash_flow = get_object_or_None(CashFlowCategory, name=self.get_cell_value(cell.value))
                    if cash_flow:
                        data_dict['cash_flow'] = cash_flow
                if c == 7:
                    cell_value = self.get_cell_value(cell.value)
                    data_dict['not_deductable'] = True if cell_value == 'Yes' else False
                c += 1
            counter += 1
            data.append(data_dict)
        return {'account_data': data, 'total': counter}

    # noinspection PyMethodMayBeStatic
    def get_csv_data(self, csv_file):
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        counter = 0
        data = []
        for i, line in enumerate(lines):
            if counter > 0:
                fields = line.split(",")
                if len(fields) > 1 and (fields[0] and fields[1]):
                    data_dict = {'name': '', 'code': '', 'account_type': 1, 'report': None, 'sub_ledger': None,
                                 'vat_reporting': None, 'cash_flow': None, 'not_deductable': False}

                    data_dict['code'] = self.get_cell_value(fields[0])
                    data_dict['name'] = self.get_cell_value(fields[1])
                    if fields[2]:
                        account_type = AccountType.get_value_from_label(self.get_cell_value(fields[2]))
                        data_dict['account_type'] = account_type
                    if fields[3]:
                        report_type = get_object_or_None(ReportType, name=self.get_cell_value(fields[3]))
                        if report_type and isinstance(report_type, ReportType):
                            data_dict['report'] = report_type
                    if fields[4]:
                        sub_ledger = SubLedgerModule.get_value_from_label(self.get_cell_value(fields[4]))
                        data_dict['sub_ledger'] = sub_ledger
                    if fields[5]:
                        vat_report = get_object_or_None(VatCode, label=self.get_cell_value(fields[5]), company=self.company)
                        if vat_report:
                            data_dict['vat_reporting'] = vat_report
                    if fields[6]:
                        cash_flow = get_object_or_None(CashFlowCategory, name=self.get_cell_value(fields[6]))
                        if cash_flow:
                            data_dict['cash_flow'] = cash_flow
                    if fields[7]:
                        cell_value = self.get_cell_value(fields[7])
                        data_dict['not_deductable'] = True if cell_value == 'Yes' else False
                    data.append(data_dict)
            counter += 1
        return {'account_data': data, 'total': counter}
