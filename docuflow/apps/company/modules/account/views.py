import csv
import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.models import Account
from docuflow.apps.system.models import ReportType
from .forms import AccountForm, ImportForm

logger = logging.getLogger(__name__)


class AccountsView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'account/index.html'


class CreateAccountView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = Account
    form_class = AccountForm
    template_name = 'account/create.html'
    success_message = 'Supplier successfully saved'
    success_url = reverse_lazy('account_list')

    def get_context_data(self, **kwargs):
        context = super(CreateAccountView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateAccountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        account = form.save(commit=False)
        account.company = self.get_company()
        account.save()
        messages.success(self.request, 'Account saved successfully')
        return HttpResponseRedirect(reverse('account_list'))


class EditAccountView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = Account
    form_class = AccountForm
    template_name = 'account/edit.html'
    success_message = 'Account successfully updated'
    success_url = reverse_lazy('account_list')

    def get_context_data(self, **kwargs):
        context = super(EditAccountView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditAccountView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class DeleteAccountView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Account
    success_message = 'Account successfully deleted'
    success_url = reverse_lazy('account_list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class ImportAccountsView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, FormView):
    template_name = 'account/import.html'
    form_class = ImportForm
    success_url = reverse_lazy('account_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        import_result = form.execute()
        messages.success(self.request, f"{import_result['new']} new accounts successfully imported.")
        return super(ImportAccountsView, self).form_valid(form)


class ExportAccountsView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=accounts.csv'

        writer = csv.writer(response)
        writer.writerow(['Code',
                         'Name',
                         'Type',
                         'Report Type',
                         'Sub Ledger',
                         'Vat Reporting',
                         'Cash Flow Category',
                         'Non Deductable'])
        for account in Account.objects.filter(company=self.get_company()):
            if account.code and account.name:
                writer.writerow([account.code,
                                 account.name,
                                 str(account.account_type),
                                 account.report.name if account.report else '',
                                 str(account.sub_ledger),
                                 account.vat_reporting.label if account.vat_reporting else '',
                                 account.cash_flow.name if account.cash_flow else '',
                                 'Yes' if account.not_deductable else 'No'
                                 ])
        # wb.save(response)
        return response


class AccountSearchView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        term = self.request.GET.get('term', None)
        q = Account.objects.filter(company=self.get_company())
        if term:
            q.filter(Q(name__istartswith=term) | Q(code__istartswith=term))
        accounts = [{
                'code': account.code,
                'name': account.name,
                'text': f"{account.code} - {account.name}",
                'label': str(account),
                'id': account.id
        } for account in q]
        return JsonResponse({'results': accounts, 'q':  self.request.GET.get('term')})


class AccountsSearchView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        term = self.request.GET.get('term')
        q = Account.objects.filter(company=self.get_company())
        if term:
            q = q.filter(Q(name__icontains=term) | Q(code__icontains=term))
        else:
            q = q.none()
        accounts = []
        for account in q:
            accounts.append({
                'code': account.code,
                'name': account.name,
                'text': f"{account.code} - {account.name}",
                'description': account.name,
                'label': str(account),
                'id': account.id
            })
        return JsonResponse(accounts, safe=False)


class AccountReportTypesView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        report_types = []
        for report_type in ReportType.objects.filter(account_type=self.request.GET['account_type']):
            report_types.append({
                'name': report_type.name,
                'id': report_type.id
            })
        return JsonResponse(report_types)
