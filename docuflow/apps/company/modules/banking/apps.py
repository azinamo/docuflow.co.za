from django.apps import AppConfig


class BankingConfig(AppConfig):
    name = 'docuflow.apps.company.modules.banking'
