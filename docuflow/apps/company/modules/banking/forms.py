from django import forms

from docuflow.apps.company.models import Bank


# Create your forms here.
class BankForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BankForm, self).__init__(*args, **kwargs)
        self.fields['bank_name'].required = True

    class Meta:
        model = Bank
        fields = ('account_holder', 'account_type', 'bank_name', 'account_number', 'branch')

    def execute(self, company):
        bank = self.save(commit=False)
        bank.company = company
        bank.is_default = company.bank_details.count() == 0
        bank.save()


