import logging

from django.contrib import messages
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse, reverse_lazy
from annoying.functions import get_object_or_None
from django.http import HttpResponseRedirect

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Bank
from .forms import BankForm

logger = logging.getLogger(__name__)


# Create your views here.
class BankView(LoginRequiredMixin, ListView):
    model = Bank
    template_name = 'bank/index.html'
    context_object_name = 'banks'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(BankView, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return Bank.objects.filter(company_id=self.request.session['company'])


class CreateBankView(ProfileMixin, LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Bank
    template_name = 'bank/create.html'
    success_message = 'Bank successfully created'
    success_url = reverse_lazy('bank_index')
    form_class = BankForm

    def form_valid(self, form):
        form.execute(self.get_company())
        return HttpResponseRedirect(reverse('bank_index'))


class EditBankView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Bank
    template_name = 'bank/edit.html'
    success_message = 'Bank successfully updated'
    success_url = reverse_lazy('bank_index')
    form_class = BankForm


class DeleteBankView(LoginRequiredMixin, SuccessMessageMixin, View):
    success_url = reverse_lazy('bank_index')

    def get(self, request, *args, **kwargs):
        bank = get_object_or_None(Bank, id=self.kwargs['pk'])
        if bank:
            bank.delete()
            messages.success(self.request, 'Bank deleted successfully')
        return HttpResponseRedirect(reverse_lazy('bank_index'))
