from django.urls import path

from . import views

urlpatterns = [
    path('', views.BankView.as_view(), name='bank_index'),
    path('create/', views.CreateBankView.as_view(), name='create_bank'),
    path('<int:pk>/edit/', views.EditBankView.as_view(), name='edit_bank'),
    path('<int:pk>/delete/', views.DeleteBankView.as_view(), name='delete_bank')
]
