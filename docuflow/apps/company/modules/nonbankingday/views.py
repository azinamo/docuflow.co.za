import logging

from django.contrib import messages
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.http import HttpResponseRedirect

from docuflow.apps.company.models import NonBankingDay, Company
from docuflow.apps.company.forms import NonBankingDayForm

logger = logging.getLogger(__name__)


# Create your views here.
class NonBankingDayView(LoginRequiredMixin, ListView):
    model = NonBankingDay
    template_name = 'nonbankingdays/index.html'
    context_object_name = 'nonbankingdays'

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_context_data(self, **kwargs):
        context = super(NonBankingDayView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateNonBankingDayView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = NonBankingDay
    template_name = 'nonbankingdays/create.html'
    success_message = 'Nonbanking days successfully saved'

    def get_context_data(self, **kwargs):
        context = super(CreateNonBankingDayView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_success_url(self):
        return reverse('nonbankingdays-list')

    # def get_form_kwargs(self):
    #     kwargs = super(CreateNonBankingDayView, self).get_form_kwargs()
    #     kwargs['company'] = self.get_company()
    #     return kwargs

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_class(self):
        return NonBankingDayForm

    def form_valid(self, form):
        nonbankingday = form.save(commit=False)
        nonbankingday.company = self.get_company()
        try:
            nonbankingday.save()
        except Exception:
            messages.error(
                self.request, 'Unable to save transition; integrity error.'
            )
            return super(
                CreateNonBankingDayView, self
            ).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class EditNonBankingDayView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = NonBankingDay
    template_name = 'nonbankingdays/edit.html'
    success_message = 'Non banking days successfully updated'

    def get_context_data(self, **kwargs):
        context = super(EditNonBankingDayView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    # def get_form_kwargs(self):
    #     kwargs = super(EditNonBankingDayView, self).get_form_kwargs()
    #     kwargs['company'] = self.get_company()
    #     return kwargs

    def get_success_url(self):
        return reverse('nonbankingdays-list')

    def get_form_class(self):
        return NonBankingDayForm


class DeleteNonBankingDayView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = NonBankingDay
    template_name = 'nonbankingdays/confirm_delete.html'
    success_message = 'Non banking days successfuly detelete'

    def get_success_url(self):
        return reverse('nonbankingdays-list')
