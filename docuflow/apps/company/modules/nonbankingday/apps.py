from django.apps import AppConfig


class NonbankingdayConfig(AppConfig):
    name = 'docuflow.apps.company.modules.nonbankingday'
