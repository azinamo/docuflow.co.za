from django.urls import path

from . import views

urlpatterns = [
    path('nonbankingdays/', views.NonBankingDayView.as_view(), name='nonbankingdays-list'),
    path('nonbankingdays/create/', views.CreateNonBankingDayView.as_view(), name='create-nonbankingdays'),
    path('nonbankingdays/edit/<int:pk>/', views.EditNonBankingDayView.as_view(), name='edit-nonbankingdays'),
    path('nonbankingdays/delete/<int:pk>/', views.DeleteNonBankingDayView.as_view(), name='delete-nonbankingdays'),
]
