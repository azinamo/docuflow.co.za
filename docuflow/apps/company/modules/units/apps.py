from django.apps import AppConfig


class UnitsConfig(AppConfig):
    name = 'docuflow.apps.company.modules.units'
