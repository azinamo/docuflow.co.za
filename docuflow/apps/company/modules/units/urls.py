from django.urls import path

from . import views

urlpatterns = [
    path('', views.UnitView.as_view(), name='unit_index'),
    path('create/', views.CreateUnitView.as_view(), name='create_unit'),
    path('<int:pk>/edit/', views.EditUnitView.as_view(), name='edit_unit'),
    path('<int:pk>/delete/', views.DeleteUnitView.as_view(), name='delete_unit'),
    path('<int:pk>/master/', views.UpdateMasterUnitView.as_view(), name='update_master_unit'),
    path('measure/', views.MeasureUnitsView.as_view(), name='measure_units'),
    path('<int:pk>/conversions/', views.UnitConversionsView.as_view(), name='unit_conversions'),
    path('autocomplete/search/', views.UnitsSearchView.as_view(), name='autocomplete_units_search'),
]
