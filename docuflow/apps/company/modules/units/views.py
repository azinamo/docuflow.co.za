import json
import logging
from decimal import Decimal

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.views.generic import ListView, View, TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse, reverse_lazy

from annoying.functions import get_object_or_None

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Company, Unit, UnitConversion
from .forms import UnitForm

logger = logging.getLogger(__name__)


class UnitView(LoginRequiredMixin, ListView):
    model = Unit
    template_name = 'units/index.html'
    pagenate_by = 10
    context_object_name = 'units'

    def get_context_data(self, **kwargs):
        context = super(UnitView, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return Unit.objects.select_related('measure').filter(measure__company_id=self.request.session['company']).order_by('measure')


class CreateUnitView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Unit
    template_name = 'units/create.html'
    success_message = 'Unit successfully created'

    def get_form_kwargs(self):
        kwargs = super(CreateUnitView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_success_url(self):
        return reverse('unit_index')

    def get_form_class(self):
        return UnitForm

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_context_data(self, **kwargs):
        context = super(CreateUnitView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        unit = form.save(commit=False)
        unit.save()
        return HttpResponseRedirect(reverse('unit_index'))


class EditUnitView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Unit
    template_name = 'units/edit.html'
    success_message = 'Unit successfully updated'

    def get_form_class(self):
        return UnitForm

    def get_success_url(self):
        return reverse('unit_index')


class DeleteUnitView(LoginRequiredMixin, SuccessMessageMixin, View):
    success_url = reverse_lazy('unit_index')

    def get(self, request, *args, **kwargs):
        unit = get_object_or_None(Unit, id=self.kwargs['pk'])
        if unit:
            unit.delete()
            messages.success(self.request, 'Unit deleted successfully')
        return HttpResponseRedirect(reverse_lazy('unit_index'))


class UpdateMasterUnitView(LoginRequiredMixin, View):

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_unit(self):
        return Unit.objects.get(pk=self.kwargs['pk'])

    def post(self, request, *args, **kwargs):
        try:
            unit = self.get_unit()
            if unit:
                company = self.get_company()
                for company_unit in company.units.all():
                    company_unit.is_default = False
                    company_unit.save()

                unit.is_default = True
                unit.save()

                self.request.session['company_unit_id'] = unit.id

                return JsonResponse({
                    'text': 'Default unit updated successfully',
                    'error': False
                    })
            else:
                return JsonResponse({
                    'text': 'Unit not found',
                    'error': True
                    })
        except Exception as exception:
            return JsonResponse({
                    'text': 'Unexpected error occurred, please try again',
                    'error': True,
                    'details': str(exception)
                })


class MeasureUnitsView(View):

    def get(self, request, *args, **kwargs):
        measure_units = Unit.objects.filter(measure_id=self.request.GET['measure'],
                                            measure__company_id=self.request.session['company']
                                            )
        units = []
        for measure_unit in measure_units:
            units.append({'id': measure_unit.id, 'name': str(measure_unit), 'unit': measure_unit.unit})
        return HttpResponse(json.dumps(units))


class UnitConversionsView(TemplateView):
    template_name = 'units/conversions.html'

    def get_unit(self):
        return Unit.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(UnitConversionsView, self).get_context_data(**kwargs)
        unit = self.get_unit()
        units = Unit.objects.units_with_conversions(unit)
        context['unit'] = unit
        context['units'] = units
        return context

    def post(self, *args, **kwargs):
        unit = self.get_unit()
        for field, value in self.request.POST.items():
            if field.startswith('unit_to_'):
                if value:
                    unit_to_id = field[8:]
                    UnitConversion.objects.update_or_create(
                        unit=unit,
                        unit_to_id=unit_to_id,
                        defaults={
                            'rate': Decimal(value)
                        }
                    )

        return JsonResponse({
            'text': 'Conversions saved successfully',
            'error': False,
            'reload': True
        })


class UnitsSearchView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        q_object = Q()
        if 'term' in self.request.GET:
            term = self.request.GET['term']
            if term:
                q_object.add(Q(name__istartswith=term), Q.AND)
        units = Unit.objects.filter(
            measure__company=self.get_company()
        ).filter(q_object)
        units_list = []
        for unit in units:
            units_list.append({
                'code': unit.name,
                'name': unit.name,
                'text': unit.name,
                'description': f"{unit.measure}-{unit.name}",
                'label': str(unit),
                'id': unit.id
            })
        return HttpResponse(json.dumps(units_list))
