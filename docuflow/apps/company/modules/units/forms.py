from django import forms

from docuflow.apps.company.models import Unit, Measure, UnitConversion


# Create your forms here.
class UnitForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(UnitForm, self).__init__(*args, **kwargs)
        self.fields['measure'].queryset = Measure.objects.filter(company=company).order_by('name')

    class Meta:
        model = Unit
        fields = ('name', 'measure', 'unit')


class UnitConversionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        unit = kwargs.pop('unit')
        super(UnitConversionForm, self).__init__(*args, **kwargs)
        self.fields['unit_to'].queryset = Unit.objects.filter(measure=unit.measure)

    class Meta:
        model = UnitConversion
        fields = ('unit_to', 'rate')

