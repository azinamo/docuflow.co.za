from .models import  Account
import django_filters


class AccountFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    code = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Account
        fields = ['name', 'code']


class VatCodeFilter(django_filters.FilterSet):
    label = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Account
        fields = ['label']