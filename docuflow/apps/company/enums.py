from django.utils.translation import gettext_lazy as __
from enumfields import IntEnum


class CompanyStatus(IntEnum):
    PENDING = 0
    ACTIVE = 1
    SUSPENDED = 2
    TEMPORARILY_INACTIVE = 3


class CompanyType(IntEnum):
    SOLE_PROPRIETOR = 1
    PARTNERSHIP = 2
    CLOSE_CORPORATION = 3
    PUBLIC_COMPANY = 4
    PRIVATE_COMPANY = 5
    PERSONAL_LIABILITY_COMPANY = 6
    BUSINESS_TRUST = 7
    GOVERNMENT = 8
    FOREIGN_COMPANY = 9


class VatCategory(IntEnum):
    CATEGORY_A = 1
    CATEGORY_B = 2
    CATEGORY_C = 3
    CATEGORY_D = 4
    CATEGORY_E = 5

    class Labels:
        CATEGORY_A = __('Category A - odd months')
        CATEGORY_B = __('Category B - even months')
        CATEGORY_C = __('Category C - every month')
        CATEGORY_D = __('Category D - Feb/Aug')
        CATEGORY_E = __('Category E - Once a year')


class AccountPosting(IntEnum):
    DEFAULT_ACCOUNT_POSTING = 1
    DO_NOT_RECORD = 2

    class Labels:
        DEFAULT_ACCOUNT_POSTING = __('Default account for company')
        DO_NOT_RECORD = __('Don\'t Record')


class VatFetch(IntEnum):
    VAT_FROM_SUPPLIER = 1
    VAT_FROM_INVOICE = 2
    VAT_FROM_LOGGER = 3

    class Labels:
        VAT_FROM_SUPPLIER = __('From supplier')
        VAT_FROM_INVOICE = __('From invoice')
        VAT_FROM_LOGGER = __('From logger')


class Forwarding(IntEnum):
    NEXT_ROLE = 1
    LOGGER = 2
    ACCOUNTANT = 3

    class Labels:
        NEXT_ROLE = __('Forward to next role')
        LOGGER = __('Forward to Logger')
        ACCOUNTANT = __('Forward to accountant')


class ErpSystem(IntEnum):
    PASTEL = 1
    COBRA = 2

    class Labels:
        PASTEL = __('Pastel')
        COBRA = __('Kobra II')


class PayrollSystem(IntEnum):
    PASTEL_PAYROLL = 1

    class Labels:
        PASTEL_PAYROLL = __('Pastel')


class AccountingDate(IntEnum):
    TODAY = 1

    class Labels:
        TODAY = __('Today\'s date')


class RecordingType(IntEnum):
    DEFAULT_RECORDING_TYPE = 1

    class Labels:
        DEFAULT_RECORDING_TYPE = __('When the first role in the flow gets the invoice')


# Account
class AccountType(IntEnum):
    INCOME_STATEMENT = 1
    BALANCE_SHEET = 2


class SubLedgerModule(IntEnum):
    INVENTORY = 0
    SUPPLIER = 1
    CUSTOMER = 2

    @classmethod
    def get_value_from_label(cls, label):
        for a in cls:
            if label == a.label:
                return a.value
        return None


# Report Type
class ReportType(IntEnum):
    INCOME_STATEMENT = 1
    BALANCE_SHEET = 2


class JournalStyle(IntEnum):
    SEPARATE = 0
    SUMMARIZED = 1


class LineDiscountHandling(IntEnum):
    REDUCE_SELLING_PRICE = 0
    EXPENSE_DISCOUNT = 1

    class Labels:
        REDUCE_SELLING_PRICE = __('Reduce the selling price with discount')
        EXPENSE_DISCOUNT = __('Expense the discount')
