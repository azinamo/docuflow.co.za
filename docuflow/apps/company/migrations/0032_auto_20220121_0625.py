# Generated by Django 3.1 on 2022-01-21 04:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0015_remove_supplier_template'),
        ('documents', '0003_remove_document_template'),
        ('company', '0031_auto_20210819_1356'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='templateinvoicefieldmapping',
            name='template',
        ),
        migrations.DeleteModel(
            name='Template',
        ),
        migrations.DeleteModel(
            name='TemplateInvoiceFieldMapping',
        ),
    ]
