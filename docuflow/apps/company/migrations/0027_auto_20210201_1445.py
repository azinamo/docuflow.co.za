# Generated by Django 3.1 on 2021-02-01 12:45

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0026_auto_20210125_1030'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='account',
            managers=[
                ('all', django.db.models.manager.Manager()),
            ],
        ),
    ]
