# Generated by Django 3.1 on 2020-12-03 11:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0001_initial'),
        ('system', '0008_vatcode'),
        ('company', '0021_auto_20201126_0803'),
    ]

    operations = [
        migrations.RenameField(
            model_name='company',
            old_name='nb_of_months_to_check_duplicate_inovices',
            new_name='nb_of_months_to_check_duplicate_invoices',
        ),
        migrations.AddField(
            model_name='company',
            name='default_previous_year_account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='default_previous_year_account', to='company.account', verbose_name='Default Previous Year'),
        ),
        migrations.AddField(
            model_name='company',
            name='default_retained_income_account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='default_retained_income_account', to='company.account', verbose_name='Default Retained Income'),
        ),
        migrations.AlterField(
            model_name='account',
            name='report',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='accounts', to='system.reporttype'),
        ),
        migrations.AlterField(
            model_name='company',
            name='area_code',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='default_flow_proposal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='company_default_workflow', to='workflow.workflow'),
        ),
        migrations.DeleteModel(
            name='ReportType',
        ),
    ]
