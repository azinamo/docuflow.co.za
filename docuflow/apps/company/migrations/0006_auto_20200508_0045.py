# Generated by Django 2.0 on 2020-05-07 22:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0005_auto_20200428_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='not_deductable',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='account',
            name='vat_reporting',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='account_reportings', to='company.VatCode'),
        ),
    ]
