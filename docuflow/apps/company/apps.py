from django.apps import AppConfig


class CompanyConfig(AppConfig):
    name = 'docuflow.apps.company'
