from django.core.management.base import BaseCommand

from docuflow.apps.company.models import Company
from docuflow.apps.company.services import create_company_urls


class Command(BaseCommand):
    help = "Dump some company data"

    def handle(self, *args, **kwargs):
        companies = Company.objects.all()
        urls = {}
        for company in companies:
            urls[company.slug] = create_company_urls(company)
        print(urls)
