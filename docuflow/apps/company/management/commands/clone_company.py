from django.core.management.base import BaseCommand

from docuflow.apps.company import models

# Company, ReportType, NonBankingDay, Account, VatCode , Unit, Measure


class Command(BaseCommand):
    help = "Clone one company to another company"

    def add_arguments(self, parser):
        parser.add_argument('company_slug', nargs='+', type=str)
        parser.add_argument('new_company', nargs='+', type=int)

    def handle(self, *args, **kwargs):
        companies = models.Company.objects.all()

        units = {
            'Mass': ('mcg', 'mg', 'g', 'kg', 'oz', 'lb', 'mt', 't'),
            'Volume': ('mm3', 'cm3', 'ml', 'l', 'kl', 'm3', 'km3', 'tsp', 'Tbs', 'in3', 'fl-oz', 'cup', 'pnt', 'qt',
                       'gal', 'ft3', 'yd3'),
            'Length': ('mm', 'cm', 'm', 'in', 'ft-us', 'ft', 'fathom', 'mi', 'nMi')
        }
        for _company in companies:
            for measure_name, measure_units in units.items():
                measure = models.Measure.objects.filter(name__icontains=measure_name, company=_company).first()
                if not measure:
                    measure = models.Measure.objects.create(name=measure_name, company=_company)

                for measure_unit in measure_units:
                    unit_measure = models.Unit.objects.filter(measure=measure, unit=measure_unit, name=measure_unit).first()
                    if not unit_measure:
                        models.Unit.objects.create(
                            name=measure_unit,
                            measure=measure,
                            unit=measure_unit
                        )
                    else:
                        unit_measure.name = measure_unit
                        unit_measure.save()
                print("-------------------------------------")
