import logging
import os
from decimal import Decimal

from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.db import models
from django.db.models import Case, F, Sum, Q, When, Subquery, OuterRef
from django.db.models.functions import Coalesce
from django.template.defaultfilters import slugify
from django.urls import reverse
from django_countries.fields import CountryField
from easy_thumbnails.fields import ThumbnailerImageField
from enumfields import EnumIntegerField, EnumField
from image_cropping import ImageRatioField
from multiselectfield import MultiSelectField
from safedelete.managers import SafeDeleteManager
from safedelete.models import SafeDeleteModel, SOFT_DELETE
from sorl.thumbnail import get_thumbnail

from docuflow.apps.budget.models import BudgetAccount
from docuflow.apps.common.enums import Module, Color
from docuflow.apps.subscription.models import SubscriptionPlan
from docuflow.apps.system.enums import AccountType
from . import enums
from .utils import set_company_url_conf

logger = logging.getLogger(__name__)


class CompanyStatus(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(max_length=200)
    color = EnumField(Color, max_length=100, default=Color.DEFAULT)
    slug = models.SlugField()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(CompanyStatus, self).save(*args, **kwargs)


def upload_path(instance, filename):
    file_data = filename.split('.')
    ext = file_data[-1]
    name = ('_'.join([d for d in file_data[0:-1] if d.strip() != ''])).replace(' ', '_')
    return os.path.join('logos', instance.slug, f"{name}.{ext}")


class Company(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    limit = models.Q(modules__in=[Module.PAYMENT.value])

    company_number = models.CharField(max_length=255, null=True, blank=True, verbose_name='Company Registration Number')
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=50, blank=True, null=True)
    company_code = models.CharField(max_length=255, null=True, blank=True)
    contact_person = models.CharField(max_length=255, blank=True, null=True)
    email_address = models.EmailField(max_length=255, blank=True, null=True)
    address_line_1 = models.CharField(max_length=255, null=True, blank=True)
    address_line_2 = models.CharField(max_length=255, null=True, blank=True)
    area_code = models.CharField(max_length=10, null=True, blank=True)
    province = models.CharField(max_length=255, null=True, blank=True)
    country = CountryField()
    vat_number = models.CharField(max_length=200, null=True, blank=True)
    accounting_system = models.CharField(max_length=200, null=True, blank=True)
    landline_number = models.CharField(max_length=200, null=True, blank=True)
    status = EnumIntegerField(enums.CompanyStatus, default=enums.CompanyStatus.PENDING)
    subscription_plan = models.ForeignKey(SubscriptionPlan, related_name="company_subscription", null=True, blank=True, on_delete=models.DO_NOTHING)
    company = models.ForeignKey('self', verbose_name='Company is a subsidiary of?.', related_name="parent", null=True, blank=True, on_delete=models.DO_NOTHING)
    administrator = models.ForeignKey(User, related_name='administrator', verbose_name='administrator', blank=True, null=True, on_delete=models.DO_NOTHING)
    is_vat_registered = models.BooleanField(default=True)
    vat = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Vat (%)',)
    currency = models.ForeignKey('system.Currency', blank=True, null=True, on_delete=models.DO_NOTHING)
    invoice_series = models.CharField(blank=True, null=True, max_length=255)
    estimate_valid_until = models.PositiveIntegerField(blank=True, null=True, verbose_name='Estimate (days)')
    type = EnumIntegerField(enums.CompanyType, default=enums.CompanyType.PRIVATE_COMPANY, verbose_name='Business Structure')
    website = models.CharField(max_length=255, verbose_name='Website Address', blank=True, null=True)
    enter_accounting_date = EnumIntegerField(enums.AccountingDate, blank=True, default=enums.AccountingDate.TODAY)
    calculate_due_date = models.BooleanField(null=True)
    user_set_as_account_poster = models.BooleanField(null=True)
    transfer_invoice_lines_not_matching = models.BooleanField(null=True)
    account_posting_at_preliminary_recording = EnumIntegerField(enums.AccountPosting, default=enums.AccountPosting.DEFAULT_ACCOUNT_POSTING)
    preliminary_recording_type = EnumIntegerField(enums.RecordingType, default=enums.RecordingType.DEFAULT_RECORDING_TYPE)
    allow_direct_recording = models.BooleanField(default=False)
    default_flow_proposal = models.ForeignKey('workflow.Workflow', related_name='company_default_workflow', blank=True, null=True, on_delete=models.DO_NOTHING)
    payment_terms = models.IntegerField(default=30)
    default_trade_creditors_account = models.ForeignKey('Account', related_name='default_trade_creditors_account', blank=True, null=True, verbose_name='Default Trade Creditors', on_delete=models.DO_NOTHING)
    default_expenditure_account = models.ForeignKey('Account', related_name='default_expenditure_account', blank=True, null=True, on_delete=models.DO_NOTHING)
    default_vat_account = models.ForeignKey('Account', related_name='default_vat_account', blank=True, null=True, verbose_name='Default Discount Vat Account', on_delete=models.DO_NOTHING)
    default_supplier_account = models.ForeignKey('Account', related_name='default_supplier_account', blank=True, null=True, on_delete=models.DO_NOTHING)
    default_distribution_account = models.ForeignKey('Account', related_name='default_distribution_account', blank=True, null=True, on_delete=models.DO_NOTHING)
    default_discount_received_account = models.ForeignKey('Account', related_name='default_discount_received_account', blank=True, null=True, verbose_name=' Discount Received Account', on_delete=models.DO_NOTHING)
    default_discount_allowed_account = models.ForeignKey('Account', related_name='default_discount_allowed_account', blank=True, null=True, verbose_name=' Discount Allowed Account', on_delete=models.DO_NOTHING)
    default_vat_receivable_account = models.ForeignKey('Account', related_name='default_vat_receivable_account', blank=True, null=True, verbose_name=' Vat Receivable Account', on_delete=models.DO_NOTHING)
    default_vat_payable_account = models.ForeignKey('Account', related_name='default_vat_payable_account', blank=True, null=True, verbose_name=' Vat Payable Account', on_delete=models.DO_NOTHING)
    default_profit_on_asset_account = models.ForeignKey('Account', related_name='default_profit_on_asset_account', blank=True, null=True, verbose_name='Profile on sale of Asset', on_delete=models.DO_NOTHING)
    default_loss_on_asset_account = models.ForeignKey('Account', related_name='default_loss_on_asset_account', blank=True, null=True, verbose_name='Loss on sale of Asset', on_delete=models.DO_NOTHING)
    default_customer_account = models.ForeignKey('Account', related_name='default_customer_account', blank=True, null=True, verbose_name='Default Customer Account', on_delete=models.DO_NOTHING)
    default_goods_received_account = models.ForeignKey('Account', related_name='default_goods_received_account', null=True, blank=True, verbose_name='Default Goods Received Interim Account', on_delete=models.DO_NOTHING)
    stock_adjustment_account = models.ForeignKey('Account', related_name='stock_adjustment_account', null=True, blank=True, verbose_name='Default Stock Adjustment Account', on_delete=models.DO_NOTHING)
    default_inventory_control_account = models.ForeignKey('Account', related_name='inventory_control_account', null=True, blank=True, verbose_name='Default Inventory Control Account', on_delete=models.DO_NOTHING)
    default_inventory_variance_account = models.ForeignKey('Account', related_name='inventory_variance_account', null=True, blank=True, verbose_name='Default Inventory Received Variance Account', on_delete=models.DO_NOTHING)
    default_payroll_account = models.ForeignKey('Account', related_name='default_payroll_account', null=True, blank=True, verbose_name='Default Payroll Account', on_delete=models.DO_NOTHING)
    default_bank_account = models.ForeignKey('Account', related_name='default_bank_account', null=True, blank=True, verbose_name='Default Bank Account', on_delete=models.DO_NOTHING)
    default_previous_year_account = models.ForeignKey('Account', related_name='default_previous_year_account', null=True, blank=True,
                                                      verbose_name='Default Previous Year', on_delete=models.DO_NOTHING)
    default_retained_income_account = models.ForeignKey('Account', related_name='default_retained_income_account', null=True, blank=True,
                                                        verbose_name='Default Retained Income', on_delete=models.DO_NOTHING)
    default_payment_method = models.ForeignKey('PaymentMethod', related_name='companies', null=True, blank=True, verbose_name='Default Payment Methods Account', on_delete=models.DO_NOTHING)
    nb_of_months_to_check_duplicate_invoices = models.IntegerField(blank=True, null=True)
    erp_system = EnumIntegerField(enums.ErpSystem, default=enums.ErpSystem.PASTEL)
    payroll = EnumIntegerField(enums.PayrollSystem, default=enums.PayrollSystem.PASTEL_PAYROLL)
    vat_category = EnumIntegerField(enums.VatCategory, null=True, blank=True)
    line_discount = EnumIntegerField(enums.LineDiscountHandling, default=enums.LineDiscountHandling.EXPENSE_DISCOUNT)
    vat_code_fetched_from = EnumIntegerField(enums.VatFetch, blank=False, default=enums.VatFetch.VAT_FROM_SUPPLIER)
    slug = models.CharField(max_length=255, blank=True)
    logo = ThumbnailerImageField(resize_source=dict(size=(350, 120), sharpen=True), upload_to=upload_path, blank=True)
    # # size is "width x height"
    cropping = ImageRatioField('logo', '350x120')

    is_master = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Companies'
        ordering = ['name', 'slug']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('my-companies')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self.generate_company_code(self.area_code, 51)
        if not self.company_code:
            if self.area_code:
                self.company_code = self.slug
        return super(Company, self).save(*args, **kwargs)

    def generate_slug(self, name, counter=1):
        name_list = name.split(' ')
        c = 0
        slug = ''
        for nm in name_list:
            if c == 0:
                slug = str(nm[0:3])
                break

        counter_str = str(counter).rjust(4, '0')
        company_slug = f"{slug}{counter_str}"
        result = get_object_or_None(Company, slug=company_slug)
        if result:
            counter = counter + 1
            return self.generate_slug(name, counter)
        else:
            return company_slug

    def generate_company_code(self, area_code, counter=1):
        code = area_code[1:3]
        counter_str = str(counter).rjust(3, '0')
        company_code = f"{code}{counter_str}"
        result = get_object_or_None(Company, company_code=company_code)
        if result:
            counter = counter + 1
            return self.generate_company_code(area_code, counter)
        else:
            return company_code

    def activate(self):
        self.status = enums.CompanyStatus.ACTIVE
        self.save()
        set_company_url_conf(self.slug)

    def generate_username(self, first_name, last_name):
        first = first_name[0:2]
        last = last_name[0:2]
        return f"{self.slug}{first.upper()}{last.upper()}"

    def get_months(self):
        return {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July',
                8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'}

    def get_month_repr(self, year, m, name):
        month_number = str(m).rjust(2, '0')
        return m, f"{year}{month_number} - {name}"

    def get_periods(self, year):
        return self.company_periods.filter(period_year=year).order_by('period')

    def get_period_repr(self, period):
        p_str = period.to_date.strftime('%Y%m-%B')
        return period.id, p_str

    def get_vat_periods(self, year):
        periods = self.get_periods(year)
        vat_periods = []
        if self.vat_category == enums.VatCategory.CATEGORY_A:
            for p in periods:
                if p.period % 2 != 0:
                    vat_period = self.get_period_repr(p)
                    vat_periods.append(vat_period)
        elif self.vat_category == enums.VatCategory.CATEGORY_B:
            for p in periods:
                if p.period % 2 == 0:
                    vat_period = self.get_period_repr(p)
                    vat_periods.append(vat_period)
        elif self.vat_category == enums.VatCategory.CATEGORY_C:
            for p in periods:
                vat_period = self.get_period_repr(p)
                vat_periods.append(vat_period)
        elif self.vat_category == enums.VatCategory.CATEGORY_D:
            for p in periods:
                if p.period == 2 or p.period == 8:
                    vat_period = self.get_period_repr(p)
                    vat_periods.append(vat_period)
        elif self.vat_category == enums.VatCategory.CATEGORY_E:
            for p in periods:
                vat_period = self.get_period_repr(p)
                vat_periods.append(vat_period)
        return vat_periods

    def get_object_items(self):
        object_items = ObjectItem.objects.filter(company_object__company=self)
        company_object_items = {}
        for object_item in object_items.all():
            if object_item.company_object_id in object_items:
                company_object_items[object_item.company_object.id].append(object_item)
            else:
                company_object_items[object_item.company_object.id] = []
                company_object_items[object_item.company_object.id].append(object_item)
        return company_object_items

    def get_objects(self):
        linked_models = {}
        company_object_items = self.get_object_items()

        for company_object in self.company_objects.all():
            linked_models[company_object.id] = {
                'label': company_object.label,
                'value': None
            }
            if company_object.id in company_object_items:
                linked_models[company_object.id]['objects'] = company_object_items[company_object.id]

        return linked_models

    def mark_as_suspended(self):
        self.status = enums.CompanyStatus.SUSPENDED
        self.save()

    def mark_as_active(self):
        self.status = enums.CompanyStatus.ACTIVE
        self.save()

    @property
    def vat_percentage(self):
        percentage = 0
        vat_code = self.vat_code
        if vat_code:
            percentage = vat_code.percentage
        return percentage

    @property
    def vat_code(self):
        return self.company_vat_codes.filter(is_default=True, module=Module.PURCHASE).first()

    @property
    def sales_vat_code(self):
        return self.company_vat_codes.filter(is_default=True, module=Module.SALES).first()

    @property
    def company_logo(self):
        return f"{settings.MEDIA_URL}{self.logo}"

    @property
    def run_incoming_document_journal(self):
        return self.account_posting_at_preliminary_recording == enums.AccountPosting.DEFAULT_ACCOUNT_POSTING

    @property
    def is_active(self):
        return self.status == enums.CompanyStatus.ACTIVE

    @property
    def invoice_logo(self):
        im = get_thumbnail(self.logo, '200x70', crop='center', quality=99)
        return im.url if im else self.company_logo

    @property
    def is_expense_discount(self):
        return self.line_discount == enums.LineDiscountHandling.EXPENSE_DISCOUNT

    @classmethod
    def create_company_dir(cls, folder):
        company_folder = os.path.join(settings.MEDIA_ROOT, folder)
        if not os.path.isdir(company_folder):
            slug_dir = Company.create_dir(company_folder)

        tmp_dir = os.path.join(company_folder, 'tmp')
        if not os.path.isdir(tmp_dir):
            return Company.create_dir(tmp_dir)
        return company_folder, tmp_dir

    @staticmethod
    def create_dir(directory_name):
        if not os.path.exists(directory_name):
            os.mkdir(directory_name)
        return directory_name


class AccountQuerySet(models.QuerySet):
    pass
    # def add_budget(self, year, period_from, period_to):
    #     return self.annotate(
    #         total=models.Subquery(
    #             return AccountBudget.objects.aggregate(
    #                 amount=models.Sum('amount')
    #             ).filter(period__period__gte=period_from, period__period__lte=period_to).values('amount')
    #         )
    #     )
    #
    # def add_journal(self, year, period_from, period_to):
    #     return self.annotate(
    #         total=models.Subquery(
    #             Journal.objects.aggregate(
    #                 amount=models.Sum(models.F('debit') - models.F('credit'))
    #             ).filter(period__period__gte=period_from, period__period__lte=period_to).values('amount')
    #         )
    #     )
    #
    # def add_total_paid(self, year, period_from, period_to):
    #     return self.annotate(
    #         total=models.Subquery(
    #             Payment.objects.aggregate(
    #                 amount=models.Sum('amount')
    #             ).filter(period__period__gte=period_from, period__period__lte=period_to).values('amount')
    #         )
    #     )


class AllAccountManager(models.Manager):

    def get_queryset(self):
        return super(AllAccountManager, self).get_queryset()


class AccountManager(models.Manager):

    def get_queryset(self):
        return super(AccountManager, self).get_queryset().filter(deleted__isnull=True, is_active=True)

    def available(self, company):
        return self.filter(company=company)

    def active(self, company):
        return self.filter(company=company, is_active=True)

    def balance_sheet(self):
        return self.filter(account_type=AccountType.BALANCE_SHEET)

    def income_statement(self):
        return self.filter(account_type=AccountType.INCOME_STATEMENT)

    def report_type_accumulated_total(self, company, slug, year, to_date):
        return self.filter(
            company=company, report__account_type=1, report__slug=slug, account_journals__journal__year=year,
            account_journals__journal__date__lt=to_date
        ).aggregate(
            total=Sum(Coalesce(F('account_journals__debit'), Decimal('0')) + Coalesce(F('account_journals__credit'), Decimal('0')) * -1)
        )

    def with_year_movement(self, year):
        return self.filter(
            company=year.company, account_journals__journal__year=year
        ).annotate(
            movement=Case(
                When(account_journals__deleted__isnull=True, then=Sum(Coalesce(F('account_journals__debit'), Decimal('0')) - Coalesce(F('account_journals__credit'), Decimal('0')))),
                output_field=models.DecimalField()
            )
        )

    def report_type_movement_total(self, company, slug, from_date, to_date):
        return self.filter(
            company=company, report__slug=slug, account_journals__journal__date__gte=from_date,
            account_journals__journal__date__lte=to_date
        ).aggregate(
            total=Sum(Coalesce(F('account_journals__debit'), Decimal('0')) + Coalesce(F('account_journals__credit'), Decimal('0')) * -1)
        )

    def expenses_nondeductable(self, company, not_deducatble, from_date, to_date):
        # is_non_deducatble, not ticked
        return self.filter(
            company=company, report__account_type=AccountType.INCOME_STATEMENT, not_deductable=not_deducatble
        ).exclude(
            report__slug='turnover'
        ).aggregate(
            total=Sum(Coalesce(F('account_journals__debit'), Decimal('0')) + Coalesce(F('account_journals__credit'), Decimal('0')) * -1,
                      filter=Q(account_journals__journal__date__gte=from_date, account_journals__journal__date__lt=to_date))
        )

    def get_for_report_type(self, company, account_type, slug, from_date, to_date):
        return self.filter(
            company=company, report__slug=slug, report__account_type=account_type,
            account_journals__journal__date__gte=from_date, account_journals__journal__date__lte=to_date
        ).aggregate(
            total=Sum(Coalesce(F('account_journals__debit'), Decimal('0')) + Coalesce(F('account_journals__credit'), Decimal('0')) * -1)
        )

    def with_budget_total(self, budget):
        return self.income_statement().annotate(
            total_amount=Subquery(
                BudgetAccount.objects.annotate(
                    total=Sum('period_amounts__amount')
                ).filter(
                    budget=budget, account_id=OuterRef('id')
                ).values(
                    'total'
                )[:1]
            )
        ).prefetch_related(
            'budgets'
        ).filter(
            company=budget.year.company
        )

    def with_opening_balance(self, year):
        return self.annotate(total_amount=Sum('opening_balances__year')).prefetch_related('budgets')

    def for_bank(self):
        return self.filter(report__slug='cash-resources')


class Account(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name="company_accounts", on_delete=models.CASCADE)
    name = models.CharField(max_length=255, db_index=True)
    code = models.CharField(max_length=200, blank=True, db_index=True, default='')
    is_vat = models.BooleanField(default=True)
    account_id = models.PositiveIntegerField(null=True)
    is_active = models.BooleanField(default=True)
    account_type = EnumIntegerField(enums.AccountType, null=True, blank=True)
    report = models.ForeignKey('system.ReportType', related_name='accounts', null=True, on_delete=models.SET_NULL)
    cash_flow = models.ForeignKey('system.CashFlowCategory', related_name='accounts', null=True, on_delete=models.SET_NULL)
    sub_ledger = EnumIntegerField(enums.SubLedgerModule, blank=True, null=True)
    vat_reporting = models.ForeignKey('VatCode', null=True, blank=True,  related_name='account_reportings', on_delete=models.SET_NULL)
    not_deductable = models.BooleanField(default=False, verbose_name='Non Deductable')

    all_objects = AllAccountManager()
    objects = AccountManager()

    class Meta:
        verbose_name_plural = 'Accounts'
        ordering = ('-code', )

    def __str__(self):
        return f"{self.code} - {self.name}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.account_id:
            accounts_count = Account.objects.filter(company=self.company).count()
            self.account_id = accounts_count + 1
        return super(Account, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('edit_account', args=(self.pk, ))

    def calculate_balance(self, previous_year, ):
        account_balance = self.account_journals.objects.get_for_account(
            year=previous_year, company=previous_year.company, account=self, start_date=previous_year.start_date,
            end_date=previous_year.end_date)
        opening_balance = self.opening_balances.filter(year=previous_year).first()
        balance = (opening_balance.amount if opening_balance else 0) + (account_balance['total'] if account_balance['total'] else 0)
        return balance

    @property
    def is_deductable(self):
        return not self.not_deductable

    @property
    def vat_type(self):
        if self.vat_reporting:
            return str(self.vat_reporting)
        return 'N/A'

    @property
    def account_type_name(self):
        return str(self.account_type)

    @property
    def get_index_url(self):
        return 'account-list'

    @property
    def sub_ledger_name(self):
        return str(self.sub_ledger)

    @property
    def is_income_statement(self):
        return self.account_type == enums.AccountType.INCOME_STATEMENT

    @property
    def is_balance_sheet(self):
        return self.account_type == enums.AccountType.BALANCE_SHEET

    @property
    def can_distribute(self):
        return self.account_type == enums.AccountType.INCOME_STATEMENT


class NonBankingDay(models.Model):
    name = models.CharField(max_length=200)
    day = models.DateField(default=None, null=False)
    company = models.ForeignKey(Company, default=None, null=False, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name_plural = 'Non banking days'
        ordering = ['day']

    def __str__(self):
        return self.name

    def is_non_banking_day(self, date):
        return self.day == date


class VatCodeQuerySet(models.QuerySet):

    def default(self):
        return self.filter(is_default=True)

    def module(self, module_type):
        return self.filter(module=module_type).exclude(deleted__isnull=False)


class VatCodeManager(models.Manager):

    def get_queryset(self):
        return super(VatCodeManager, self).get_queryset().filter(deleted__isnull=True)

    def default(self):
        return self.filter(is_default=True)

    def input(self):
        return self.get_queryset().module(Module.PURCHASE)

    def output(self):
        return self.get_queryset().module(Module.SALES)

    def submitted(self, company_id):
        return self.get_queryset().filter(company_id=company_id, percentage__gt=0).exclude(deleted__isnull=False)

    def active(self, company_id):
        return self.get_queryset().filter(company_id=company_id, is_active=True).exclude(deleted__isnull=False)

    def valid(self, company_id, from_date, to_date):
        return self.get_queryset().filter(company_id=company_id, valid_from__gt=from_date, valid_to__lt=to_date)


class VatCode(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name="company_vat_codes", on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name="account_vat_codes", on_delete=models.CASCADE)
    label = models.CharField(max_length=255)
    code = models.CharField(max_length=255, null=True)
    percentage = models.PositiveIntegerField(verbose_name='Percentage', default=1, null=False)
    is_active = models.BooleanField(default=True)
    is_default = models.BooleanField(default=False, verbose_name='Default')
    is_capital = models.BooleanField(default=False, verbose_name='Capital Vat')
    valid_from = models.DateField(null=True, blank=True)
    valid_to = models.DateField(null=True, blank=True)
    module = EnumIntegerField(Module, default=Module.PURCHASE)

    objects = VatCodeManager().from_queryset(VatCodeQuerySet)()

    class Meta:
        verbose_name_plural = 'Vat codes'
        ordering = ['code']

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        if self.is_default:
            VatCode.objects.filter(company=self.company, module=self.module).all().update(is_default=False)
        return super(VatCode, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('vat_code_index')

    @property
    def is_output(self):
        return self.module == Module.SALES

    @property
    def is_input(self):
        return self.module == Module.PURCHASE


class CompanyObject(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name="company_objects", on_delete=models.CASCADE)
    parent = models.ForeignKey('self', related_name="children", blank=True, null=True, on_delete=models.SET_NULL)
    label = models.CharField(max_length=255, verbose_name='Label', blank=True, null=True)
    modules = MultiSelectField(choices=Module.choices(), default=Module.PURCHASE.value)
    slug = models.CharField(max_length=255, blank=True, null=True)
    ordering = models.PositiveIntegerField()

    class Meta:
        verbose_name_plural = 'Company Objects'
        ordering = ['ordering']

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        if not self.pk:
            ordering = CompanyObject.objects.count()
            self.ordering = ordering + 1
        return super(CompanyObject, self).save(*args, **kwargs)


class ObjectItem(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company_object = models.ForeignKey(CompanyObject, related_name='object_items', on_delete=models.CASCADE)
    code = models.CharField(max_length=255, verbose_name='Code', blank=True, null=True)
    label = models.CharField(max_length=255, verbose_name='Label', blank=False, null=False)
    is_active = models.BooleanField(default=True)
    slug = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Object Items'

    class QS(models.QuerySet):

        def for_fixed_assets(self):
            return self.filter(company_object__modules__contains=Module.FIXED_ASSET.value)

    objects = QS.as_manager()

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        self.slug = slugify(self.label)
        return super(ObjectItem, self).save(*args, **kwargs)


class PaymentMethodManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(deleted__isnull=True)

    def sales(self):
        return self.filter(modules__contains=Module.SALES.value)


class PaymentMethod(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    SEPARATE = 0
    SUMMARIZED = 1

    JOURNAL_STYLES = (
        (SEPARATE, 'Separate'),
        (SUMMARIZED, 'Summarized')
    )

    company = models.ForeignKey(Company, related_name="payment_methods", on_delete=models.CASCADE)
    payment_option = models.ForeignKey('system.PaymentMethod', null=True, related_name="company_payment_options", on_delete=models.DO_NOTHING)
    account = models.ForeignKey('Account', blank=True, null=True, related_name="payment_methods", on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name='Name')
    is_active = models.BooleanField(default=True)
    is_depositable = models.BooleanField(default=False, verbose_name='Will choose account on cash up')
    has_change = models.BooleanField(default=False, verbose_name='Has Change')
    journal_style = EnumIntegerField(enums.JournalStyle, default=enums.JournalStyle.SEPARATE,
                                     verbose_name='How do want to handle it in journal')
    modules = MultiSelectField(choices=Module.choices(), default=Module.PURCHASE.value)

    objects = PaymentMethodManager()

    class Meta:
        verbose_name_plural = 'Payment Methods'
        ordering = ['name']
        unique_together = ('company', 'payment_option')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('payment_method_index')

    @property
    def module_list(self):
        modules = [int(m) for m in self.modules]
        return modules

    @property
    def is_summarized(self):
        return self.journal_style == enums.JournalStyle.SUMMARIZED


class BranchManager(SafeDeleteManager):

    def get_queryset(self):
        return super(BranchManager, self).get_queryset().filter(deleted__isnull=True)


class BranchQueryset(models.QuerySet):

    def invoicable(self):
        return self.filter(is_invoicable=True)


class Branch(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name="branches", on_delete=models.CASCADE)
    label = models.CharField(max_length=255, verbose_name='Label')
    address_line_1 = models.CharField(max_length=255, blank=True)
    address_line_2 = models.CharField(max_length=255, blank=True)
    town = models.CharField(max_length=255, blank=True)
    province = models.CharField(max_length=255, blank=True)
    area_code = models.CharField(max_length=255, blank=True)
    is_default = models.BooleanField(default=False)
    is_head_office = models.BooleanField(default=False, verbose_name='Head office')
    is_invoicable = models.BooleanField(default=True, verbose_name='Can be used for invoicing')

    objects = BranchManager.from_queryset(BranchQueryset)()
    # objects = BranchQueryset.as_manager()

    class Meta:
        ordering = ['label']
        verbose_name_plural = 'Branches'

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse('edit_branch', args=(self.pk, ))

    def save(self, *args, **kwargs):
        if self.company.branches.count() == 0:
            self.is_default = True
        return super(Branch, self).save(*args, **kwargs)


class Currency(models.Model):
    company = models.ForeignKey(Company, related_name='currencies', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=10)
    symbol = models.CharField(max_length=5)

    class Meta:
        verbose_name_plural = 'Currencies'
        ordering = ['name']

    def __str__(self):
        return f"{self.code} ({self.name})"


class FileType(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name='files', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=False, blank=False)
    slug = models.CharField(max_length=255, null=False, blank=False)
    # TODO Use enums
    module = EnumIntegerField(Module, verbose_name="Module")
    file_type_id = models.PositiveIntegerField(null=True)

    class Meta:
        verbose_name_plural = 'Files'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        if not self.pk or not self.file_type_id:
            count = FileType.objects.filter(company=self.company).count()
            self.file_type_id = count + 1
        return super(FileType, self).save(*args, **kwargs)

    @property
    def module_name(self):
        return self.module.label


class Measure(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name='measures', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=False, blank=False)
    extra_quantity = models.DecimalField(decimal_places=2, max_digits=10, default=0, verbose_name='Allowed extra quantity', help_text='This is the extra quantity that can be added to the ordered quantity.')

    class Meta:
        verbose_name_plural = 'Measure'
        ordering = ['name']

    def __str__(self):
        return self.name


class UnitManager(models.Manager):

    def units_with_conversions(self, unit):
        return self.annotate(
            current_rate=models.Subquery(
                UnitConversion.objects.filter(unit_to_id=models.OuterRef('id'), unit=unit).values('rate')[:1]
            )
        ).filter(measure=unit.measure).exclude(pk=unit.id)

    def rate(self, unit, unit_to):
        return self.annotate(
            current_rate=models.Subquery(
                UnitConversion.objects.filter(unit_to=unit_to, unit=unit).values('rate')[:1]
            )
        ).filter(
            measure=unit.measure, pk=unit.id
        ).first()


class Unit(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    measure = models.ForeignKey('Measure', related_name='units', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    unit = models.CharField(max_length=255, verbose_name='Unit')

    objects = UnitManager()

    class Meta:
        verbose_name_plural = 'Units'
        ordering = ['name']

    def __str__(self):
        return self.name


class UnitConversion(models.Model):
    unit = models.ForeignKey(Unit, related_name='conversions', on_delete=models.CASCADE)
    unit_to = models.ForeignKey(Unit, related_name='to_conversions', on_delete=models.CASCADE)
    rate = models.DecimalField(max_digits=15, decimal_places=4)

    class Meta:
        unique_together = ('unit', 'unit_to')


class DeliveryMethod(models.Model):
    company = models.ForeignKey(Company, related_name='delivery_methods', null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class DefaultAccount(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.OneToOneField('Company', related_name='default_account', on_delete=models.DO_NOTHING)
    trade_creditors_account = models.ForeignKey('Account', related_name='trade_creditors', blank=True, null=True, verbose_name='Default Trade Creditors', on_delete=models.DO_NOTHING)
    expenditure_account = models.ForeignKey('Account', related_name='expenditure', blank=True, null=True, verbose_name='Default Expenditure Account', on_delete=models.DO_NOTHING)
    vat_account = models.ForeignKey('Account', related_name='vat', blank=True, null=True, verbose_name='Default Discount Vat Account', on_delete=models.DO_NOTHING)
    supplier_account = models.ForeignKey('Account', related_name='supplier', blank=True, null=True, verbose_name='Default Supplier Account', on_delete=models.DO_NOTHING)
    distribution_account = models.ForeignKey('Account', related_name='distribution', blank=True, null=True, verbose_name='Default Distribution Account', on_delete=models.DO_NOTHING)
    discount_received_account = models.ForeignKey('Account', related_name='discount_received', blank=True, null=True, verbose_name='Default Discount Received Account', on_delete=models.DO_NOTHING)
    discount_allowed_account = models.ForeignKey('Account', related_name='discount_allowed', blank=True, null=True, verbose_name='Default Discount Allowed Account', on_delete=models.DO_NOTHING)
    vat_receivable_account = models.ForeignKey('Account', related_name='vat_receivable', blank=True, null=True, verbose_name='Default Vat Receivable Account', on_delete=models.DO_NOTHING)
    vat_payable_account = models.ForeignKey('Account', related_name='vat_payable', blank=True, null=True, verbose_name='Default Vat Payable Account', on_delete=models.DO_NOTHING)
    profit_on_asset_account = models.ForeignKey('Account', related_name='profit_on_asset', blank=True, null=True, verbose_name='Profile on sale of Asset', on_delete=models.DO_NOTHING)
    loss_on_asset_account = models.ForeignKey('Account', related_name='loss_on_asset', blank=True, null=True, verbose_name='Loss on sale of Asset', on_delete=models.DO_NOTHING)
    customer_account = models.ForeignKey('Account', related_name='customer', blank=True, null=True, verbose_name='Default Customer Account', on_delete=models.DO_NOTHING)
    goods_received_account = models.ForeignKey('Account', related_name='goods_received', null=True, blank=True, verbose_name='Default Goods Received Interim Account', on_delete=models.DO_NOTHING)
    stock_adjustment_account = models.ForeignKey('Account', related_name='stock_adjustment', null=True, blank=True, verbose_name='Default Stock Adjustment Account', on_delete=models.DO_NOTHING)
    module = EnumIntegerField(Module, default=Module.PURCHASE)


class Bank(models.Model):
    company = models.ForeignKey('Company', related_name='bank_details', on_delete=models.CASCADE)
    bank_name = models.CharField(max_length=150, blank=True)
    account_holder = models.CharField(max_length=255)
    account_type = models.CharField(max_length=200)
    account_number = models.CharField(max_length=200)
    branch = models.CharField(max_length=150)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return str(self.account_holder)


def get_company(company_id):
    cache_key = f"company_{company_id}"
    company = cache.get(cache_key)
    if not company:
        company = Company.objects.select_related(
            'default_distribution_account', 'default_flow_proposal', 'currency', 'default_vat_account',
            'default_expenditure_account', 'default_supplier_account',
        ).prefetch_related(
            'company_years', 'branches', 'company_vat_codes'
        ).get(id=company_id)
        cache.set(cache_key, company)
    return company
