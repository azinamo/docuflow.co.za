from django import template
import pprint
from annoying.functions import get_object_or_None

from docuflow.apps.company.models import Company, Account, ObjectItem
from docuflow.apps.common.enums import Module
from docuflow.apps.accounts.models import Role
from docuflow.apps.invoice.models import InvoiceAccount
from docuflow.apps.accountposting.models import AccountPostingLine
from docuflow.apps.intervention.models import Intervention


register = template.Library()
pp = pprint.PrettyPrinter(indent=4)


@register.inclusion_tag('companies/linked_objects.html')
def account_posting_object_items(company_id, account_posting_id=None):
    company = Company.objects.get(pk=company_id)
    object_categories = {}
    accounts = []
    account_object_links = {}
    account = None
    account_id = None
    if account_posting_id and type(account_posting_id) == int:
        account_posting_line = get_object_or_None(AccountPostingLine, pk=account_posting_id)
        if account_posting_line:
            account_id = account_posting_line.account_id
            for object_item in account_posting_line.object_items.all():
                account_object_links[object_item.company_object_id] = object_item.id

    if company:
        accounts = Account.objects.filter(company=company).all()

        company_objects = company.company_objects.all().order_by('ordering')
        company_objects_ids = [company_object.id for company_object in company_objects]

        objects_items = ObjectItem.objects.filter(company_object_id__in=company_objects_ids).all()

        for company_object in company_objects:
            object_categories[company_object.id] = {}
            object_categories[company_object.id]['label'] = company_object
            object_categories[company_object.id]['value'] = None
            object_categories[company_object.id]['objects'] = []
            if company_object.id in account_object_links:
                object_categories[company_object.id]['value'] = account_object_links[company_object.id]

        for objects_item in objects_items:
            if objects_item.company_object_id in object_categories:
                object_categories[objects_item.company_object_id]['objects'].append(objects_item)

    return {'object_categories': object_categories, 'accounts': accounts, 'account': account, 'account_id': account_id}


@register.inclusion_tag('companies/linked_objects.html')
def company_linked_objects(company_id, role_id=None, invoice_account_id=None, account_id=None, form_style='inline', module='purchase'):
    company = Company.objects.get(pk=company_id)
    object_categories = {}
    account_object_links = {}
    account = None
    role = None
    accounts = []
    module_id = Module.PURCHASE.value

    if module == 'purchase':
        module_id = Module.PURCHASE.value
    elif module == 'sales':
        module_id = Module.SALES.value
    module_id = str(module_id)

    if invoice_account_id and type(invoice_account_id) == int:
        invoice_account = InvoiceAccount.objects.select_related().get(pk=invoice_account_id)
        if invoice_account:
            account_id = invoice_account.account_id
            for account_object_item in invoice_account.object_items.all():
                if account_object_item.company_object.id in account_object_links:
                    account_object_links[account_object_item.company_object.id]['value'] = account_object_item.id
                else:
                    account_object_links[account_object_item.company_object.id] = {'value': account_object_item.id}

    if role_id and type(role_id) == int:
        role = Role.objects.prefetch_related('object_items').filter(pk=role_id).first()

    if company:
        company_objects = company.company_objects.all().order_by('ordering')
        company_objects_ids = [company_object.id for company_object in company_objects]

        objects_items = ObjectItem.objects.filter(company_object_id__in=company_objects_ids).all()
        for company_object in company_objects:
            if module_id in company_object.modules:
                object_categories[company_object.id] = {}
                object_categories[company_object.id]['label'] = company_object
                object_categories[company_object.id]['value'] = None
                object_categories[company_object.id]['objects'] = {}
                if company_object.id in account_object_links:
                    object_categories[company_object.id]['value'] = account_object_links[company_object.id]['value']

                if role:
                    for object_item in role.object_items.all():
                        if object_item.company_object.id in object_categories:
                            object_categories[object_item.company_object.id]['objects'][object_item.id] = object_item
                else:
                    for objects_item in objects_items:
                        if objects_item.company_object_id in object_categories:
                            object_categories[objects_item.company_object_id]['objects'][objects_item.id] = objects_item

        if role:
            accounts = role.accounts.all()
    return {'object_categories': object_categories, 'accounts': accounts, 'account': account, 'account_id': account_id,
            'form_style': form_style}


def get_company(company_id):
    return Company.objects.prefetch_related(
        'company_objects'
    ).filter(pk=company_id)


def get_role(role_id):
    return Role.objects.get(pk=role_id)


@register.inclusion_tag('companies/linked_objects.html')
def workflow_rule_company_linked_objects(company_id, workflow_rule_id=None):
    company = get_company(company_id)
    object_categories = {}
    accounts = Account.objects.filter(company=company).all()
    account_object_links = {}
    account = None
    account_id = None
    if workflow_rule_id and type(workflow_rule_id) == int:
        intervention = get_object_or_None(Intervention, pk=workflow_rule_id)
        if intervention:
            account_id = intervention.account_id
            for workflow_rule_object_item in intervention.object_items.all():
                if workflow_rule_object_item.company_object.id in account_object_links:
                    account_object_links[workflow_rule_object_item.company_object.id]['value'] = workflow_rule_object_item.id
                else:
                    account_object_links[workflow_rule_object_item.company_object.id] = {'value': workflow_rule_object_item.id}

    if company:
        try:
            company_objects = company.company_objects.all().order_by('ordering')
            company_objects_ids = [company_object.id for company_object in company_objects]
            objects_items = ObjectItem.objects.filter(company_object_id__in=company_objects_ids).all()

            for company_object in company_objects:
                object_categories[company_object.id] = {}
                object_categories[company_object.id]['label'] = company_object
                object_categories[company_object.id]['value'] = None
                object_categories[company_object.id]['objects'] = []
                if company_object.id in account_object_links:
                    object_categories[company_object.id]['value'] = account_object_links[company_object.id]['value']

            for objects_item in objects_items:
                if objects_item.company_object_id in object_categories:
                    object_categories[objects_item.company_object_id]['objects'].append(objects_item)
            return {'object_categories' : object_categories, 'accounts': accounts, 'account': account, 'account_id': account_id}
        except Exception as exception:
            return {'object_categories': [], 'accounts': [], 'account': None,
                    'account_id': None}


@register.inclusion_tag('companies/linked_objects.html')
def asset_linked_objects(company_id, workflow_rule_id=None):
    company = Company.objects.get(pk=company_id)
    object_categories = {}
    accounts = Account.objects.filter(company=company).all()
    account_object_links = {}
    account = None
    account_id = None
    if workflow_rule_id and type(workflow_rule_id) == int:
        intervention = get_object_or_None(Intervention, pk=workflow_rule_id)
        if intervention:
            account_id = intervention.account_id
            for workflow_rule_object_item in intervention.object_items.all():
                if workflow_rule_object_item.company_object.id in account_object_links:
                    account_object_links[workflow_rule_object_item.company_object.id]['value'] = workflow_rule_object_item.id
                else:
                    account_object_links[workflow_rule_object_item.company_object.id] = {'value': workflow_rule_object_item.id}

    if company:
        company_objects = company.company_objects.all().order_by('ordering')
        company_objects_ids = [company_object.id for company_object in company_objects]

        objects_items = ObjectItem.objects.filter(company_object_id__in=company_objects_ids).all()
        print(objects_items)
        for company_object in company_objects:
            object_categories[company_object.id] = {}
            object_categories[company_object.id]['label'] = company_object
            object_categories[company_object.id]['value'] = None
            object_categories[company_object.id]['objects'] = []
            if company_object.id in account_object_links:
                object_categories[company_object.id]['value'] = account_object_links[company_object.id]['value']

        for objects_item in objects_items:
            if objects_item.company_object_id in object_categories:
                object_categories[objects_item.company_object_id]['objects'].append(objects_item)

    return {'object_categories': object_categories, 'accounts': accounts, 'account': account, 'account_id': account_id}


