from django import template
from django.conf import settings

from annoying.functions import get_object_or_None

from docuflow.apps.company.models import Company, get_company
from docuflow.apps.company.enums import CompanyStatus

register = template.Library()


def percentage(num, total):
    if total > 0:
        return round(num/total, 2) * 100
    return 0


@register.inclusion_tag('companies/list.html')
def companies():
    all_companies = Company.objects.all()
    return {'companies': all_companies}


@register.simple_tag(name='current_company')
def current_company(company_id):
    current = None
    if company_id and type(company_id) == int:
        current = get_object_or_None(Company, pk=company_id)
    return current


@register.inclusion_tag('companies/stats.html')
def company_stats():
    active = 0
    processing = 0
    pending = 0
    total = 0
    for company in Company.objects.all():
        total = total + 1
        if company.status:
            if company.status == CompanyStatus.PENDING:
                pending += 1
            elif company.status == CompanyStatus.PENDING:
                processing = 1
            elif company.status == CompanyStatus.ACTIVE:
                active += 1

    pending_perc = percentage(pending, total)
    processing_perc = percentage(processing, total)
    active_perc = percentage(active, total)

    return {
        'active_perc': active_perc,
        'processing_perc': processing_perc,
        'pending_perc': pending_perc,
        'active': active,
        'processing': processing,
        'pending': pending
    }


@register.inclusion_tag('companies/master_menu.html')
def master_menu():
    company = Company.objects.filter(is_master=True).first()
    return {'company': company}


@register.inclusion_tag('companies/logo.html')
def display_logo(company, width=100, height=50, base_url=None, is_print=None, is_email=None):
    base_url = settings.BASE_URL if not base_url else base_url
    return {'company': company, 'width': width, 'height': height, 'base_url': base_url, 'is_print': is_print, 'is_email': is_email}


@register.inclusion_tag('companies/currency.html')
def currency(company_id):
    return {'company': get_company(company_id=company_id)}
