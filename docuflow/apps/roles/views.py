# import json
#
# from django.core.cache import cache
# from django.shortcuts import redirect
# from django.views.generic import ListView, TemplateView, FormView, View
# from django.views.generic.edit import CreateView, UpdateView
# from django_filters.views import FilterView
# from django.contrib import messages
# from django.contrib.messages.views import SuccessMessageMixin
# from django.http import HttpResponse, JsonResponse
# from django.urls import reverse
# from django.contrib.auth.mixins import LoginRequiredMixin
# from django.http import HttpResponseRedirect
# from django.core.paginator import Paginator
#
# from annoying.functions import get_object_or_None
#
# from docuflow.apps.accounts.models import Profile, Role, PermissionGroup
# from docuflow.apps.auditlog.models import LogEntry
# from docuflow.apps.common.mixins import ProfileMixin
# from docuflow.apps.common.utils import datatable_pages_menu
# from docuflow.apps.company.models import Company, Account, CompanyObject, ObjectItem
# from .forms import RoleForm,  RoleAuthorizationForm, RoleOverviewForm
# from .filters import RoleFilter
#
#
# class RolesView(LoginRequiredMixin, ProfileMixin, TemplateView):
#     template_name = 'roles/index.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(RolesView, self).get_context_data(**kwargs)
#         context['branch'] = self.get_branch()
#         context['company'] = self.get_company()
#         profile = self.get_profile()
#         menu_items = datatable_pages_menu(profile.number_of_records)
#         context['profile'] = profile
#         context['display_rows'] = profile.number_of_records
#         context['menu_length'] = json.dumps(menu_items)
#         return context
#
#
# class RoleSearchView(LoginRequiredMixin, ProfileMixin, FilterView):
#     model = Role
#     filterset_class = RoleFilter
#     context_object_name = 'roles'
#
#     def get_template_names(self):
#         if 'master_company_id' in self.kwargs:
#             return 'roles/master_index.html'
#         return 'roles/search.html'
#
#     def get_company_id(self):
#         if 'master_company_id' in self.kwargs:
#             return self.kwargs['master_company_id']
#         return self.request.session['company']
#
#     def get_company(self):
#         return Company.objects.get(pk=self.get_company_id())
#
#     def get_queryset(self):
#         company = self.get_company()
#         queryset = Role.objects.filter(company=company)
#         if 'role' in self.request.GET and self.request.GET['role'] != '':
#             queryset = queryset.filter(pk=int(self.request.GET['role']))
#         if 'name' in self.request.GET and self.request.GET['name'] != '':
#             queryset = queryset.filter(label__startswith=self.request.GET['name'])
#         if 'group' in self.request.GET and self.request.GET['group'] != '':
#             queryset = queryset.filter(pk=self.request.GET['group'])
#
#         return queryset
#
#
# class RolesOverviewLandingView(LoginRequiredMixin, FormView):
#     template_name = 'roles/overview/index.html'
#     form_class = RoleOverviewForm
#
#     def get_form_kwargs(self):
#         kwargs = super(RolesOverviewLandingView, self).get_form_kwargs()
#         kwargs['company'] = self.get_company()
#         return kwargs
#
#     def get_company(self):
#         return Company.objects.get(pk=self.request.session['company'])
#
#     def get_context_data(self, **kwargs):
#         context = super(RolesOverviewLandingView, self).get_context_data(**kwargs)
#         company = self.get_company()
#         context['company'] = company
#         return context
#
#
# class RolesOverviewView(LoginRequiredMixin, ListView):
#     model = Role
#     template_name = 'roles/overview/overview.html'
#     context_object_name = 'roles'
#
#     def get_company(self):
#         return Company.objects.get(pk=self.request.session['company'])
#
#     def get_queryset(self):
#         company = self.get_company()
#         return Role.objects.select_related('manager_role', 'supervisor_role').filter(company=company)
#
#
# class CreateRoleView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
#     template_name = 'roles/create.html'
#     model = Role
#     success_message = 'Role successfully created'
#
#     def get_form_class(self):
#         return RoleForm
#
#     def get_form_kwargs(self):
#         kwargs = super(CreateRoleView, self).get_form_kwargs()
#         kwargs['company'] = self.get_company()
#         return kwargs
#
#     def get_success_url(self):
#         if 'master_company_id' in self.kwargs:
#             return reverse('master_role_index',
#                            kwargs={'master_company_id': self.kwargs['master_company_id']})
#         return reverse('roles_index')
#
#     def get_company_id(self):
#         if 'master_company_id' in self.kwargs:
#             return self.kwargs['master_company_id']
#         return self.request.session['company']
#
#     def get_company(self):
#         return Company.objects.get(pk=self.get_company_id())
#
#     def company_user_accounts(self, company):
#         return Profile.objects.filter(company=company)
#
#     def permission_groups(self, company):
#         return PermissionGroup.objects.filter(company=company)
#
#     def role_permission_groups(self):
#         permission_groups= []
#         for permission_group in self.get_object().permission_groups.all():
#             permission_groups.append(permission_group.id)
#         return permission_groups
#
#     def get_context_data(self, **kwargs):
#         context = super(CreateRoleView, self).get_context_data(**kwargs)
#         company = self.get_company()
#         user_accounts = self.company_user_accounts(company)
#         context['permission_groups'] = self.permission_groups(company)
#         context['role_permission_groups'] = []
#         context['role_profiles'] = []
#         context['user_accounts'] = user_accounts
#         return context
#
#     def form_invalid(self, form):
#         messages.error(self.request, 'Role could not be save, please try again {}.'.format(form.errors))
#         if 'master_company_id' in self.kwargs:
#             return reverse('create_master_role',
#                            kwargs={'master_company_id': self.kwargs['master_company_id']})
#         return HttpResponseRedirect(reverse('create_role'))
#
#     def form_valid(self, form):
#         try:
#             role = form.save(commit=False)
#             name_list = role.label.upper().split(' ')
#             counter = 0
#             codename = ''
#             for str in name_list:
#                 if counter < 2:
#                     codename += "{} ".format(str[0:3])
#                 counter = counter + 1
#
#             company = self.get_company()
#             role.name = codename
#             role.company = company
#             role.is_active = True
#             role.save()
#
#             permission_groups = [int(permission_group_id) for permission_group_id in self.request.POST.getlist('permission_groups')]
#             for role_group_id in permission_groups:
#                 permission_group = PermissionGroup.objects.get(pk=role_group_id)
#                 if permission_group:
#                     role.permission_groups.add(permission_group)
#
#             profiles = [int(profile_id) for profile_id in self.request.POST.getlist('permission_group_user_accounts')]
#             for profile_id in profiles:
#                 profile = Profile.objects.get(pk=profile_id)
#                 if profile:
#                     user = profile.user
#                     profile.roles.add(role)
#
#                     user.user_permissions.clear()
#                     for permission_group in role.permission_groups.all():
#                         for permission in permission_group.permissions.all():
#                             user.user_permissions.add(permission)
#
#             messages.success(self.request, 'Role saved successfully.')
#             return HttpResponseRedirect(self.get_success_url())
#         except Exception as e:
#             messages.error(self.request, 'Role could not be save, please try again {}.'.format(e))
#             if 'master_company_id' in self.kwargs:
#                 return reverse('create_master_role',
#                                kwargs={'master_company_id': self.kwargs['master_company_id']})
#             return HttpResponseRedirect(reverse('create_role'))
#
#
# class EditRoleView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
#     template_name = "roles/edit.html"
#     model = Role
#     success_message = 'Role successfully updated'
#
#     def get_success_url(self):
#         if 'master_company_id' in self.kwargs:
#             return reverse('master_role_index',
#                            kwargs={'master_company_id': self.kwargs['master_company_id']})
#         return reverse('roles_index')
#
#     def get_company_id(self):
#         if 'master_company_id' in self.kwargs:
#             return self.kwargs['master_company_id']
#         return self.request.session['company']
#
#     def get_company(self):
#         return Company.objects.get(pk=self.get_company_id())
#
#     def get_object(self):
#         return Role.objects.prefetch_related('permission_groups').filter(pk=self.kwargs['pk']).first()
#
#     def company_user_accounts(self, company):
#         return Profile.objects.select_related('user').filter(company=company)
#
#     def permission_groups(self, company):
#         return PermissionGroup.objects.filter(company=company)
#
#     def role_permission_groups(self, role):
#         permission_groups= []
#         for permission_group in role.permission_groups.all():
#             permission_groups.append(permission_group.id)
#         return permission_groups
#
#     def get_context_data(self, **kwargs):
#         context = super(EditRoleView, self).get_context_data(**kwargs)
#         company = self.get_company()
#         role = self.get_object()
#         user_accounts = self.company_user_accounts(company)
#
#         role_profiles = [profile.id for profile in role.profile_set.all()]
#         context['permission_groups'] = self.permission_groups(company)
#         context['role_permission_groups'] = self.role_permission_groups(role)
#         context['role_profiles'] = role_profiles
#         context['user_accounts'] = user_accounts
#         return context
#
#     def get_form_class(self):
#         return RoleForm
#
#     def get_form_kwargs(self):
#         kwargs = super(EditRoleView, self).get_form_kwargs()
#         kwargs['company'] = self.get_company()
#         return kwargs
#
#     def form_valid(self, form):
#         try:
#             role = form.save(commit=False)
#             company = self.get_company()
#             role.name = generate_codename(role.label)
#             role.company = company
#             role.is_active = True
#             role.save()
#             # role.save_m2m()
#
#             role.permission_groups.clear()
#             permission_groups = [int(permission_group_id) for permission_group_id in self.request.POST.getlist('permission_groups')]
#             for role_group_id in permission_groups:
#                 permission_group = PermissionGroup.objects.get(pk=role_group_id)
#                 if permission_group:
#                     role.permission_groups.add(permission_group)
#
#             profiles = [int(profile_id) for profile_id in self.request.POST.getlist('permission_group_user_accounts')]
#
#             role.profile_set.clear()
#
#             for profile_id in profiles:
#                 profile = Profile.objects.get(pk=profile_id)
#                 if profile:
#                     user = profile.user
#                     profile.roles.add(role)
#
#                     user.user_permissions.clear()
#                     for permission_group in role.permission_groups.all():
#                         for permission in permission_group.permissions.all():
#                             user.user_permissions.add(permission)
#
#             messages.success(self.request, 'Role updated successfully.')
#             return HttpResponseRedirect(self.get_success_url())
#         except Exception as e:
#             messages.error(self.request, 'Role could not be save, please try again {}.'.format(e))
#             return HttpResponseRedirect(reverse('edit_role', args={'pk': self.kwargs['pk']}))
#
#
# class DeleteRoleView(LoginRequiredMixin, SuccessMessageMixin, View):
#
#     def get_success_url(self):
#         if 'master_company_id' in self.kwargs:
#             return reverse('master_role_index',
#                            kwargs={'master_company_id': self.kwargs['master_company_id']})
#         return reverse('roles_index')
#
#     def get(self, request, *args, **kwargs):
#         try:
#             role = Role.objects.get(pk=self.kwargs['pk'])
#             role.delete()
#             messages.success(self.request, 'Role deleted successfully')
#         except Exception as exception:
#             messages.error(self.request, 'Error occurred deleting the role, please try again')
#         return HttpResponseRedirect(self.get_success_url())
#
#
# class RoleChangesView(LoginRequiredMixin, TemplateView):
#     template_name = 'roles/changes.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(RoleChangesView, self).get_context_data(**kwargs)
#         context['log_entries'] = LogEntry.objects.get_for_model(Role)
#         return context
#
#
# class RoleMembersView(LoginRequiredMixin, TemplateView):
#     template_name = 'roles/members.html'
#     # def get(self, request, *args, **kwargs):
#
#
# class RoleAuthorizationSearchView(LoginRequiredMixin, FormView):
#     template_name = 'roles/search.html'
#
#     def get_form(self, form_class=None):
#         return RoleAuthorizationForm()
#
#     def get_company(self):
#         return Company.objects.get(pk=self.request.session['company'])
#
#     def get_roles(self, company):
#         return Role.objects.filter(company=company).values()
#
#     def get_context_data(self, **kwargs):
#         context = super(RoleAuthorizationSearchView, self).get_context_data(**kwargs)
#         company = self.get_company()
#         roles = self.get_roles(company)
#         context['roles'] = roles
#         context['groups'] = []
#         context['users'] = []
#         return context
#
#     def form_invalid(self, form):
#         messages.error(self.request, 'Role permissions could not be saved, please try again')
#         return HttpResponseRedirect(reverse('role_permission_detail'))
#
#     def form_valid(self, form):
#         try:
#             messages.success(self.request, 'Role permissions successfully.')
#             return HttpResponseRedirect(reverse('role_permission_detail'))
#         except Exception as e:
#             messages.error(self.request, 'Role permissions could not be saved, please try again {}.'.format(e))
#             return HttpResponseRedirect(reverse('role_permission_detail'))
#
#
# class RoleAuthorizationDetailView(LoginRequiredMixin, ProfileMixin, FormView):
#     template_name = 'roles/authorization_detail.html'
#     form_class = RoleAuthorizationForm
#
#     def get_company_linked_modes(self, company):
#         linked_models = {}
#         for company_object in company.company_objects.all():
#             linked_models[company_object.id] = {}
#             linked_models[company_object.id]['values'] = {}
#             linked_models[company_object.id]['label'] = company_object.label
#             linked_models[company_object.id]['class'] = company_object
#             linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
#                 company_object_id=company_object.id)
#         return linked_models
#
#     def get_roles(self, company):
#         return Role.objects.filter(company=company)
#
#     def get_context_data(self, **kwargs):
#         context = super(RoleAuthorizationDetailView, self).get_context_data(**kwargs)
#         company = self.get_company()
#         roles = self.get_roles(company)
#         company_objects = self.get_company_linked_modes(company)
#         paginator = Paginator(roles, 1)
#         page = 1
#         if 'page' in self.request.GET:
#             page = self.request.GET['page']
#         context['roles'] = paginator.page(page)
#         context['left_list'] = []
#         context['right_list'] = []
#         role = None
#         if 'role_id' in self.kwargs and 'page' not in self.request.GET:
#             role = get_object_or_None(Role, pk=self.kwargs['role_id'])
#         context['role'] = role
#         context['company_objects'] = company_objects
#         return context
#
#     def form_invalid(self, form):
#         messages.error(self.request, 'Role permissions could not be saved, please try again')
#         return HttpResponseRedirect(reverse('role_permission_detail'))
#
#     def form_valid(self, form):
#         try:
#             messages.success(self.request, 'Role permissions successfully.')
#             return HttpResponseRedirect(reverse('role_permission_detail'))
#         except Exception as e:
#             messages.error(self.request, 'Role permissions could not be saved, please try again {}.'.format(e))
#             return HttpResponseRedirect(reverse('role_permission_detail'))
#
#
# class RoleObjectPermissionsView(LoginRequiredMixin, TemplateView):
#     template_name = 'roles/object_permission_detail.html'
#
#     def get_company(self):
#         return Company.objects.get(pk=self.request.session['company'])
#
#     def get_roles(self, company):
#         return Role.objects.filter(company=company).order_by('id')
#
#     def get_context_data(self, **kwargs):
#         context = super(RoleObjectPermissionsView, self).get_context_data(**kwargs)
#         company = self.get_company()
#         roles = self.get_roles(company)
#         paginator = Paginator(roles, 1)
#         page = 1
#         if 'page' in self.request.GET:
#             page = self.request.GET['page']
#         context['roles'] = paginator.page(page)
#         return context
#
#
# class ObjectPermissionView(LoginRequiredMixin, View):
#
#     def get_company(self):
#         return Company.objects.get(pk=self.request.session['company'])
#
#     def get_accounts(self, role, company):
#         accounts = Account.objects.active(company).all()
#         selected_objects = []
#         right_list = []
#         left_list = []
#         for role_account in role.accounts.all():
#             right_list.append({'id': role_account.id,
#                                'name': f"{role_account.code}-{role_account.name}",
#                                'sub_ledger': role_account.sub_ledger
#                                })
#             selected_objects.append(role_account.id)
#
#         for account in accounts:
#             if account.id not in selected_objects:
#                 left_list.append({'id': account.id,
#                                   'name': f"{account.code}-{account.name}",
#                                   'sub_ledger': account.sub_ledger
#                                   })
#         return left_list, right_list
#
#     def get_objects(self, role, object_type):
#         company_object = CompanyObject.objects.prefetch_related(
#             'object_items'
#         ).select_related('company').filter(id=object_type).first()
#         left_list = []
#         right_list = []
#         if company_object:
#             selected_objects = []
#             role_object_items = role.object_items.filter().all()
#             for object_item in role_object_items:
#                 if object_item.company_object_id == company_object.id:
#                     right_list.append({'id': object_item.id, 'name': object_item.label})
#                     selected_objects.append(object_item.id)
#
#             for company_object_item in company_object.object_items.all():
#                 if company_object_item.id not in selected_objects:
#                     left_list.append({'id': company_object_item.id, 'name': company_object_item.label})
#         return left_list, right_list
#
#     def get(self, request, *args, **kwargs):
#         if self.request.is_ajax():
#             role_id = self.request.GET.get('role_id', None)
#             if role_id:
#                 object_type = self.request.GET.get('object', None)
#                 if object_type:
#                     company = self.get_company()
#                     role = Role.objects.prefetch_related('accounts', 'accounts__company').get(pk=role_id)
#
#                     if object_type == 'account':
#                         left_list, right_list = self.get_accounts(role, company)
#                     else:
#                         left_list, right_list = self.get_objects(role, object_type)
#                     return JsonResponse({'right_list': right_list, 'left_list': left_list, 'error': False})
#                 else:
#                     return JsonResponse({'error': True, 'text': 'Please select the object to assign role permissions'})
#             else:
#                 return JsonResponse({'error': True, 'text': f"Role not found"})
#         return HttpResponse("Not allowed")
#
#
# class RoleSaveObjectPermissionsView(LoginRequiredMixin, View):
#
#     def save_object_permissions(self, object_type, role, object_permissions):
#         if object_type == 'account':
#             role.accounts.clear()
#             for account_id in object_permissions:
#                 account = get_object_or_None(Account, pk=account_id)
#                 if account:
#                     role.accounts.add(account)
#         else:
#             company_object = get_object_or_None(CompanyObject, pk=object_type)
#
#             role_object_items = role.object_items.filter().all()
#             for object_item in role_object_items:
#                 if object_item.company_object_id == company_object.id:
#                     object_item.delete()
#
#             for object_item_id in object_permissions:
#                 object_item = get_object_or_None(ObjectItem, pk=object_item_id)
#                 if object_item:
#                     role.object_items.add(object_item)
#
#     def post(self, request, *args, **kwargs):
#         redirect_url = None
#         if self.request.is_ajax():
#             if 'role_id' in self.request.POST:
#                 if 'object_id' in self.request.POST and not (self.request.POST['object_id'] == ''):
#                     role_object_permissions = [int(p) for p in self.request.POST.getlist('role_object_permissions[]')]
#                     role = Role.objects.get(pk=self.request.POST['role_id'])
#                     if role:
#                         try:
#                             self.save_object_permissions(self.request.POST['object_id'], role, role_object_permissions)
#
#                             return JsonResponse({'error': False,
#                                                  'text': 'Role object permissions successfully updated',
#                                                  'redirect': redirect_url
#                                                  })
#                         except Exception as exception:
#                             return JsonResponse({'error': True,
#                                                  'text': f'Error saving the role object permissions {exception}',
#                                                  'redirect': redirect_url
#                                                  })
#                     else:
#                         return JsonResponse({'error': True,
#                                              'text': 'Role or object  not found, please select.',
#                                              'redirect': redirect_url
#                                              })
#                 else:
#                     return JsonResponse({'error': True,
#                                          'text': 'Please select the object to assign role permissions ',
#                                          })
#             else:
#                 return JsonResponse({'error': True,
#                                      'text': 'Role not found'
#                                      })
#         return redirect(redirect_url)
#
#
# class ChangeCurrentUserRoleView(LoginRequiredMixin, View):
#
#     def get_role_url(self, role):
#         if role.slug and role.slug == 'logger':
#             return reverse('invoice_log')
#         else:
#             return reverse('all-invoices')
#
#     def get(self, request, *args, **kwargs):
#         if self.request.is_ajax():
#             role = Role.objects.get(pk=self.kwargs['pk'])
#             if role:
#                 role_permissions = {role.id: []}
#                 for permission_group in role.permission_groups.all():
#                     for permission in permission_group.permissions.all():
#                         role_permissions[role.id].append(permission.codename)
#
#                 request.session['permissions'] = role_permissions
#                 redirect_url = self.get_role_url(role)
#
#                 self.request.session['role'] = role.id
#                 cache_key = f"user_{self.request.session['user']}_role"
#                 cache.set(cache_key, role)
#
#                 return JsonResponse({'error': False,
#                                      'text': 'Role successfully updated',
#                                      'redirect': redirect_url
#                                      })
#             else:
#                 return JsonResponse({'error': True,
#                                      'text': "Role not found {self.kwargs['role_id']} "
#                                      })
#         return redirect(reverse('all-invoices'))
#
#
# def generate_codename(label):
#     name_list = label.upper().split(' ')
#     counter = 0
#     codename = ''
#     for name in name_list:
#         if counter < 2:
#             codename += name[0:3]
#         counter = counter + 1
#     return codename
