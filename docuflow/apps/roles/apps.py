from django.apps import AppConfig


class RolesConfig(AppConfig):
    name = 'docuflow.apps.roles'
