from rest_framework import viewsets

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin
from .serializers import RoleSerializer


class RolesViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = RoleSerializer

    def get_queryset(self):
        return Role.objects.prefetch_related(
            'permission_groups'
        ).select_related(
            'manager_role', 'supervisor_role'
        ).filter(
            company=self.get_company()
        )
