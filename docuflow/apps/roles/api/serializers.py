from rest_framework import serializers

from docuflow.apps.accounts.models import Role


class RoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = ('name', 'label', 'id', 'manager_role', 'supervisor_role', 'authorization_amount',
                  'can_be_inserted_in_flow')

    def convert_object(self, obj):
        # Add any self-referencing fields here (if not already done)
        if 'manager_role' not in self.fields:
            self.fields['manager_role'] = RoleSerializer()
        return super(RoleSerializer, self).convert_object(obj)
