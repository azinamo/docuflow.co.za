from enumfields import Enum


class DefaultGroup(Enum):
    LEVEL_10 = 'level_10'
    LEVEL_8 = 'level_8'
    LEVEL_6 = 'level_6'
    LEVEL_2 = 'level_2'
    LOGGER = 'logger'
