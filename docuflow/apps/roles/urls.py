from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    # path('', views.RolesView.as_view(), name='roles_index'),
    # path('search/', views.RoleSearchView.as_view(), name='role_search'),
    # path('create/', views.CreateRoleView.as_view(), name='create_role'),
    # path('edit/<int:pk>/', views.EditRoleView.as_view(), name='edit_role'),
    # path('delete/<int:pk>/', views.DeleteRoleView.as_view(), name='delete_role'),
    # path('<int:pk>/group/members/', views.RoleMembersView.as_view(), name='role_members'),
    # path('authorization/detail/<int:role_id>/', views.RoleAuthorizationDetailView.as_view(),
    #      name='role_permission_detail'),
    # path('authorization/search/', views.RoleAuthorizationSearchView.as_view(), name='role_permission_search'),
    # path('object/permissions/detail/', views.RoleObjectPermissionsView.as_view(), name='role_object_permission_detail'),
    # path('object/permissions/', views.ObjectPermissionView.as_view(), name='role_object_permissions'),
    # path('permissions/', views.RoleObjectPermissionsView.as_view(), name='role_permissions_index'),
    # path('save/role/permissions/', csrf_exempt(views.RoleSaveObjectPermissionsView.as_view()),
    #      name='role_save_object_permissions'),
    # path('change/user/current/role/<int:pk>/', views.ChangeCurrentUserRoleView.as_view(), name='change_role'),
    # path('changes/', views.RoleChangesView.as_view(), name='role_changes'),
    # path('report/overview/', views.RolesOverviewLandingView.as_view(), name='roles_overview'),
    # path('report/overview/show/', views.RolesOverviewView.as_view(), name='roles_overview_report'),
    # path('master/<int:master_company_id>/', views.RolesView.as_view(), name='master_role_index'),
    # path('master/<int:master_company_id>/create/', views.CreateRoleView.as_view(), name='create_master_role'),
    # path('master/<int:master_company_id>/edit/<int:pk>/', views.EditRoleView.as_view(), name='edit_master_role'),
    # path('master/<int:master_company_id>/delete/<int:pk>/', views.DeleteRoleView.as_view(), name='delete_master_role'),
]
