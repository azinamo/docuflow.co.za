from django.core.cache import cache

from docuflow.apps.company.models import Company, Branch
from docuflow.apps.accounts.models import Profile
from docuflow.apps.period.models import Year


class CompanyMixin(object):

    def get_company_year(self):
        company = cache.get('company')
        if company is None:
            company = self.get_company()
        default_year = company.company_years.filter(is_active=True).first()
        if default_year:
            return default_year
        else:
            return company.company_years.first()

    def get_company_branch(self):
        company = self.get_company()
        default_branch = company.branches.filter(is_default=True).first()
        if default_branch:
            return default_branch
        else:
            return company.branches.first()

    def get_company(self):
        company = cache.get('company')
        if company is None:
            company = Company.objects.select_related(
                'default_distribution_account',
                'default_flow_proposal',
                'currency',
                'default_vat_account',
                'default_expenditure_account',
                'default_supplier_account',
            ).prefetch_related(
                'company_years',
                'branches'
            ).filter(id=self.request.session['company']).first()
            cache.set('company', company)
        return company


class ProfileMixin(CompanyMixin):

    def get_profile(self):
        profile = cache.get('profile')
        if profile is None:
            profile = Profile.objects.get(id=self.request.session.get('profile'))
            cache.set('profile', profile)
        return profile

    def get_profile_year(self):
        profile = self.get_profile()
        return profile.year

    def get_year(self):
        year_id = self.request.session.get('profile_year_id', None)
        if year_id:
            year = cache.get('year_{}'.format(year_id))
            if year is None:
                year = Year.objects.get(pk=year_id)
                cache.set('year_{}'.format(year_id))
            return year
        else:
            profile_year = self.get_profile_year()
            if profile_year:
                return profile_year
            else:
                return self.get_company_year()

    def get_profile_branch(self):
        return self.get_profile().branch

    def get_branch(self):
        branch_id = self.request.session.get('branch_id', None)
        if branch_id:
            branch = cache.get('branch_{}'.format(branch_id))
            if branch is None:
                branch = Branch.objects.get(pk=branch_id)
                cache.set('branch_{}'.format(branch_id))
            return branch
        else:
            profile_branch = self.get_profile_branch()
            if profile_branch:
                return profile_branch
            else:
                return self.get_company_branch()


class SetHeadline(object):

    headline = None

    def get_context_data(self, **kwargs):
        kwargs = super(SetHeadline, self).get_context_data(**kwargs)
        kwargs.update({'headline': self.get_headline()})
        return kwargs

    def get_headline(self):
        return self.headline
