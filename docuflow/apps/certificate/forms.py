import os

from django import forms
from django.conf import settings
from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.common.utils import create_dir, upload_to_s3
from docuflow.apps.accounts.models import Role, Profile
from docuflow.apps.comment.services import create_comments
from docuflow.apps.company.models import Company
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.invoice.validators import FileValidator
from docuflow.apps.payroll.models import Employee
from docuflow.apps.supplier.models import Supplier
from .models import Certificate
from .utils import certificates_upload_path


class CertificateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(CertificateForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].queryset = Supplier.objects.filter(company=self.company, is_active=True,is_blocked=False).order_by('code', 'name')
        self.fields['document_type'].queryset = InvoiceType.objects.for_document(company=self.company)
        self.fields['employee'].queryset = Employee.objects.filter(company=self.company)
        self.fields['remind_role'].queryset = Role.objects.filter(company=self.company)
        self.fields['profiles'].queryset = Profile.objects.active().filter(company=self.company)

    profiles = forms.ModelMultipleChoiceField(required=True, label='Viewing Profiles', queryset=None)
    supplier = forms.ModelChoiceField(required=False, label='Supplier', queryset=None)
    remind_role = forms.ModelChoiceField(required=False, label='Reminder Role', queryset=None)
    employee = forms.ModelChoiceField(required=False, label='Employee', queryset=None)
    reference_1 = forms.CharField(required=False, label='Reference 1')
    reference_2 = forms.CharField(required=False, label='Reference 2')
    attachment = forms.FileField(required=False, label='Attachment')

    class Meta:
        fields = ('document_type', 'supplier', 'supplier_name', 'document_number', 'reference_1', 'reference_2',
                  'employee', 'document_date', 'expiry_date', 'reminder', 'reminder_days', 'remind_role', 'comment',
                  'profiles'
                  )
        model = Certificate
        widgets = {
            'certificate_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'expiry_date': forms.DateInput(attrs={'class': 'datepicker'}),
        }

    def save(self, commit=False):
        certificate = super(CertificateForm, self).save(commit=False)
        certificate.company = self.company
        certificate.owner = self.profile
        certificate.supplier = self.cleaned_data.get('supplier')
        certificate.comments = self.cleaned_data.get('comments', '')
        certificate.save()

        self.save_m2m()

        create_comments(
            request=self.request,
            content_type=ContentType.objects.get_for_model(certificate),
            object_id=certificate.pk,
            profile=self.profile,
            role=self.role
        )

        certificate_file = self.request.POST.get('invoice_image', None)

        if certificate_file:
            source = f"{settings.BASE_DIR}{certificate_file}"
            filename = os.path.basename(certificate_file)
            company_dir = os.path.join(settings.MEDIA_ROOT, certificate.company.slug)
            create_dir(dir_name=company_dir)
            certificates_dir = os.path.join(settings.MEDIA_ROOT, certificate.company.slug, 'certificates')
            create_dir(dir_name=certificates_dir)
            invoice_dir = os.path.join(settings.MEDIA_ROOT, certificate.company.slug, 'certificates', str(certificate.id))
            create_dir(dir_name=invoice_dir)
            destination = f"{invoice_dir}/{filename}"
            os.rename(source, destination)
            certificate.filename = filename
            certificate.attachment = certificates_upload_path(instance=certificate, filename=filename)
            certificate.save()

            upload_to_s3(
                file_path=certificate.attachment.path,
                filename=certificates_upload_path(instance=certificate, filename=filename)
            )

        return certificate


class UploadDocumentForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.request = kwargs.pop('request')
        super(UploadDocumentForm, self).__init__(*args, **kwargs)

    image = forms.FileField(
        required=True, label='Add File',
        validators=[FileValidator(allowed_extensions=('jpg', 'jpeg', 'pdf', 'gif', 'png'))]
    )

    def execute(self):
        file = self.request.FILES.get('image')
        if not file:
            raise forms.ValidationError('File did not upload successfully, please try again')

        _, tmp_dir = Company.create_company_dir(self.company.slug)

        destination = f'{tmp_dir}/{file}'
        destination_url = f'{settings.MEDIA_URL}{self.company.slug}/tmp/{file}'
        with open(destination, 'wb+') as f:
            for chunk in file.chunks():
                f.write(chunk)
        return destination_url
