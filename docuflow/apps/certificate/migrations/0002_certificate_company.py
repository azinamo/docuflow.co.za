# Generated by Django 2.0 on 2020-03-28 16:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('certificate', '0001_initial'),
        ('company', '0002_auto_20200328_1821'),
    ]

    operations = [
        migrations.AddField(
            model_name='certificate',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='certificates', to='company.Company'),
        ),
    ]
