from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.CertificatesView.as_view(), name='certificates'),
    path('create/', views.CreateCertificateView.as_view(), name='create_certificate'),
    path('<int:pk>/detail/', views.EditCertificateView.as_view(), name='certificate_detail'),
    path('<int:pk>/show/', views.ShowCertificateView.as_view(), name='certificate_show'),
    path('update/<int:pk>/', views.UpdateCertificateView.as_view(), name='update_certificate'),
    path('delete/<int:pk>/', views.DeleteCertificateView.as_view(), name='delete_certificate'),
    path('upload/document/', csrf_exempt(views.UploadDocumentView.as_view()), name='upload_certificate_document'),
]

