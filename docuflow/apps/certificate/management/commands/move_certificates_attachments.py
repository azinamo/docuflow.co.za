import os
import pprint

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.files import File

from docuflow.apps.common.utils import upload_to_s3, create_dir
from docuflow.apps.company.models import Company
from docuflow.apps.certificate.models import Certificate
from docuflow.apps.certificate.utils import certificates_upload_path

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Calculates the invoice files"

    def handle(self, *args, **kwargs):
        try:
            for certificate in Certificate.objects.select_related('company').filter(attachment='').order_by('id').all():
                file = os.path.basename(certificate.filename)
                original_src = os.path.join(settings.MEDIA_ROOT, certificate.company.slug, str(certificate.pk), file)
                print(f"File location {original_src}")

                if not (os.path.exists(original_src) and os.path.isfile(original_src)):
                    continue
                certificates_dir = os.path.join(settings.MEDIA_ROOT, certificate.company.slug, 'certificates')
                create_dir(certificates_dir)
                certificate_dir = os.path.join(settings.MEDIA_ROOT, certificate.company.slug, 'certificates', str(certificate.pk))
                create_dir(certificate_dir)
                destination = os.path.join(settings.MEDIA_ROOT, certificate.company.slug, 'certificates', str(certificate.pk), file)
                if not (os.path.exists(destination) and os.path.isfile(destination)):
                    with open(original_src, 'rb+') as f:
                        data = f.read()
                        with open(destination, 'wb+') as ff:
                            ff.write(data)

                certificate.attachment = certificates_upload_path(instance=certificate, filename=file)
                certificate.save()
                print(f"Processing file {certificate.attachment.path} .... {os.path.exists(destination)} {os.path.isfile(destination)}")
                if os.path.exists(certificate.attachment.path) and os.path.isfile(certificate.attachment.path):
                    upload_to_s3(
                        file_path=certificate.attachment.path,
                        filename=certificates_upload_path(instance=certificate, filename=file)
                    )

        except Exception as exception:
            print(f"Exception occurred {exception} ")
