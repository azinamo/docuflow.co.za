import logging
from datetime import datetime, timezone

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from django.template.loader import get_template

from docuflow.apps.certificate.models import Certificate

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "To email reminder to each user on a role, where a document has not been processed within the given times " \
           "as set up in “Roles”"

    @staticmethod
    def notify(certificate: Certificate):
        for profile in certificate.profiles.all():
            logger.info(f"sending reminder to profile {profile.user.email} with role id {certificate}")
            subject = f'Reminder from DocuFlow-{certificate}'
            from_address = settings.DEFAULT_FROM_EMAIL
            recipient_list = [profile.user.email]
            template = get_template('certificate/email/processing_reminder.html')
            context = {'certificate': certificate, 'profile': profile, 'company': certificate}

            message = template.render(context)
            msg = EmailMultiAlternatives(subject, message, from_address, recipient_list, bcc=settings.ADMIN_EMAIL)
            msg.attach_alternative(message, "text/html")
            msg.send()

    # noinspection PyMethodMayBeStatic
    def notify_reminder(self):
        subject = 'Reminder email'
        from_addr = 'no-reply@docuflow.co.za'
        recipient_list = ('admire@originate.co.za',)
        message = 'Reminder emails send to the users and their profiles'
        send_mail(subject, message, from_addr, recipient_list)

    def handle(self, *args, **kwargs):
        for certificate in Certificate.objects.select_related('company').filter(reminder=True, expiry_date__isnull=False):
            delta = datetime.now(timezone.utc).date() - certificate.expiry_date
            logger.info(f"Certificate {certificate} expiry day {delta.days} and reminder days {certificate.reminder_days}")
            # if delta.days == certificate.reminder_days:
            #     # roles_invoices = self.get_roles_invoices(company)
            #     # logger.info(f"Found {len(roles_invoices)} role invoices pending")
            #     # for role_id, role_invoices in roles_invoices.items():
            #     # if len(role_invoices['invoices']) > 0:
            #     logger.info(f"Send reminder now for role id {certificate}")
            self.notify(certificate)
            #
            #     self.stdout.write(self.style.SUCCESS(f"Sending reminder to {certificate}"))
        self.notify_reminder()

