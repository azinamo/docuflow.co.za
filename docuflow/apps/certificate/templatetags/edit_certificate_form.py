from django import template

from docuflow.apps.company.models import Currency
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.accounts.models import Role
from docuflow.apps.period.models import Year
from docuflow.apps.certificate.forms import CertificateForm
from annoying.functions import get_object_or_None

register = template.Library()


def get_current_year(company):
    return Year.objects.filter(company=company, is_open=True).first()


def get_role_permissions(role_id):
    role_permissions = []
    if type(role_id) == int:
        role = Role.objects.prefetch_related('permission_groups').get(pk=role_id)
        if role:
            for permission_group in role.permission_groups.all():
                for permission in permission_group.permissions.all():
                    role_permissions.append(permission.codename)
    return role_permissions


def get_default_certificate_type(certificate):
    document_type = get_object_or_None(InvoiceType, company=certificate.company, is_default=True)
    if document_type:
        return document_type
    return None


def get_default_currency(invoice):
    if invoice.currency:
        return invoice.currency
    else:
        try:
            currency = Currency.objects.filter(company=invoice.company).order_by('id').first()
            if currency:
                return currency
        except:
            pass
    return None


@register.inclusion_tag('certificate/edit.html')
def edit_form(certificate, role_id, editable=True):
    role_permissions = get_role_permissions(role_id)

    document_type = get_default_certificate_type(certificate)
    supplier = None
    supplier_name = None
    supplier_number = None

    if certificate.document_type:
        document_type = certificate.document_type

    if certificate.supplier:
        supplier = certificate.supplier
        supplier_name = certificate.supplier.name
        supplier_number = certificate.supplier.code

    initials = {'supplier': supplier, 'supplier_name': supplier_name, 'supplier_number': supplier_number,
                'document_type': document_type
                }
    form = CertificateForm(instance=certificate, company=certificate.company, initial=initials, role=None,
                           profile=certificate.owner, request=None)
    return {
        'certificate': certificate,
        'form': form,
        'assigned_group_permissions': certificate,
        'permissions': role_permissions,
        'editable': editable
    }
