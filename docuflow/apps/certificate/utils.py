"""
Keep helper functions not related to specific module.
"""
import logging
import os
from django.conf import settings

from docuflow.apps.common.utils import create_dir, upload_to_s3

logger = logging.getLogger(__name__)


def certificates_upload_path(instance, filename):
    path = os.path.join(settings.MEDIA_ROOT, instance.company.slug, 'certificates')
    create_dir(dir_name=path)
    path = os.path.join(settings.MEDIA_ROOT, instance.company.slug, 'certificates', str(instance.pk))
    create_dir(dir_name=path)
    return f"{instance.company.slug}/certificates/{instance.pk}/{filename}"

