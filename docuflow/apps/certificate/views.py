import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import ListView, FormView, View, TemplateView, DetailView
from django.views.generic.edit import UpdateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from .forms import CertificateForm, UploadDocumentForm
from .models import Certificate

logger = logging.getLogger(__name__)


class CertificatesView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Certificate
    template_name = 'certificate/index.html'
    context_object_name = 'certificates'
    paginate_by = 30

    def get_queryset(self):
        return Certificate.objects.select_related('document_type', 'company', 'supplier').filter(
            company=self.get_company()).for_owner(self.get_profile()).distinct()


class CreateCertificateView(LoginRequiredMixin, ProfileMixin, FormView):
    model = Certificate
    template_name = 'certificate/create.html'
    form_class = CertificateForm

    def get_context_data(self, **kwargs):
        context = super(CreateCertificateView, self).get_context_data(**kwargs)
        context['content_type'] = ContentType.objects.get_for_model(Certificate, False)
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateCertificateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the certificate, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateCertificateView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            form.save()
            return JsonResponse({'error': False,
                                 'text': "Certificate successfully saved",
                                 'reload': True
                                 })
        return HttpResponse('Not allowed')


class UpdateCertificateView(LoginRequiredMixin, ProfileMixin, UpdateView):
    model = Certificate
    form_class = CertificateForm

    def get_context_data(self, **kwargs):
        context = super(UpdateCertificateView, self).get_context_data(**kwargs)
        context['content_type'] = ContentType.objects.get_for_model(Certificate, False)
        return context

    def get_form_kwargs(self):
        kwargs = super(UpdateCertificateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred updating the certificate, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(UpdateCertificateView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            form.save()
            return JsonResponse({'error': False,
                                 'text': 'Certificate successfully updated',
                                 'reload': True
                                 })
        return HttpResponse('Not allowed')


class UploadDocumentView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "certificate/upload_document.html"
    form_class = UploadDocumentForm

    def get_form_kwargs(self):
        kwargs = super(UploadDocumentView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(UploadDocumentView, self).get_context_data(**kwargs)
        return context

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': True,
                                 'text': 'Invoice image could not be uploaded, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(UploadDocumentView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(self.request):
            destination_url = form.execute()
            return JsonResponse({'error': False,
                                 'text': 'File successfully uploaded.',
                                 'file': destination_url,
                                 'dismiss_modal': True
                                 })
        return HttpResponseRedirect(reverse('create_certificate'))


class EditCertificateView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'certificate/edit_certificate.html'

    def get_certificate(self):
        return Certificate.objects.select_related('company', 'document_type', 'supplier').get(pk=self.kwargs['pk'])

    # noinspection PyMethodMayBeStatic
    def get_certificates(self, company):
        return Certificate.objects.select_related(
            'company', 'document_type', 'supplier'
        ).filter(company=company).for_owner(profile=self.get_profile()).order_by('id').distinct()

    # noinspection PyMethodMayBeStatic
    def role_permissions(self, role_id):
        role = Role.objects.prefetch_related('permission_groups').get(pk=role_id)
        permissions = []
        if role:
            for permission_group in role.permission_groups.all():
                for permission in permission_group.permissions.all():
                    permissions.append(permission.codename)
        return permissions

    def get_role_permissions(self):
        role = self.get_role()
        if 'permissions' in self.request.session:
            if role.id in self.request.session['permissions']:
                return self.request.session['permissions'][role.id]
            else:
                permissions = self.role_permissions(role.id)
                self.request.session['permissions'][role.id] = permissions
                return permissions
        else:
            permissions = self.role_permissions(role.id)
            self.request.session['permissions'] = {role.id: permissions}
            return permissions

    # noinspection PyMethodMayBeStatic
    def profile_can_edit(self):
        return True

    def get_context_data(self, **kwargs):
        context = super(EditCertificateView, self).get_context_data(**kwargs)
        company = self.get_company()
        company_certificates = self.get_certificates(company=company)
        paginator = Paginator(company_certificates, 1)
        certificates = paginator.page(1)

        certificate = None
        if 'page' in self.request.GET:
            page = self.request.GET['page']
            certificates = paginator.page(page)
            if not certificates.paginator.count > 0:
                page_num = 1
                for cert in certificates:
                    if page_num == int(page):
                        certificate = cert
                        break
                    page_num += 1
            else:
                certificate = certificates[0]

        if not certificate:
            certificate = self.get_certificate()

        permissions = self.get_role_permissions()
        is_parked = False
        is_locked = False
        if not certificate:
            return redirect(reverse('certificates'))
        context['company'] = company
        context['certificates'] = certificates
        context['certificate'] = self.get_certificate()
        can_edit = self.profile_can_edit()
        context['is_parked'] = is_parked
        context['can_edit'] = can_edit
        context['is_locked'] = is_locked
        context['images'] = []
        context['MEDIA_URL'] = settings.MEDIA_URL
        context['permissions'] = permissions
        context['content_type'] = ContentType.objects.get_for_model(certificate)
        return context


class ShowCertificateView(LoginRequiredMixin, ProfileMixin, DetailView):
    template_name = 'certificate/show.html'
    model = Certificate

    def get_queryset(self):
        return super().get_queryset().select_related(
            'company', 'document_type', 'supplier').prefetch_related('profiles')

    # noinspection PyMethodMayBeStatic
    def get_certificates(self):
        return Certificate.objects.select_related(
            'company', 'document_type', 'supplier'
        ).filter(company=self.get_company()).prefetch_related('profiles').for_owner(profile=self.get_profile()).order_by('id')

    # noinspection PyMethodMayBeStatic
    def role_permissions(self, role_id):
        role = Role.objects.prefetch_related('permission_groups').get(pk=role_id)
        permissions = []
        if role:
            for permission_group in role.permission_groups.all():
                for permission in permission_group.permissions.all():
                    permissions.append(permission.codename)
        return permissions

    # noinspection PyMethodMayBeStatic
    def get_role_permissions(self):
        role = self.get_role()

        if 'permissions' in self.request.session:
            if role.id in self.request.session['permissions']:
                return self.request.session['permissions'][role.id]
            else:
                permissions = self.role_permissions(role.id)
                self.request.session['permissions'][role.id] = permissions
                return permissions
        else:
            permissions = self.role_permissions(role.id)
            self.request.session['permissions'] = {role.id: permissions}
            return permissions

    # noinspection PyMethodMayBeStatic
    def profile_can_edit(self):
        return True

    def get_context_data(self, **kwargs):
        context = super(ShowCertificateView, self).get_context_data(**kwargs)
        company_certificates = self.get_certificates()
        paginator = Paginator(company_certificates, 1)
        certificates = paginator.page(1)

        certificate = None
        page = self.request.GET.get('page')
        show_current = self.request.GET.get('c') == '1'
        if page:
            certificates = paginator.page(page)
            if not certificates.paginator.count > 0:
                page_num = 1
                for cert in certificates:
                    if page_num == int(page):
                        certificate = cert
                        break
                    page_num += 1
            else:
                certificate = certificates[0]

        if not certificate or show_current:
            certificate = self.object

        permissions = self.get_role_permissions()
        is_parked = False
        is_locked = False
        if not certificate:
            return redirect(reverse('certificates'))
        context['company'] = self.get_company()
        context['certificates'] = certificates
        context['certificate'] = certificate
        can_edit = self.profile_can_edit()
        context['is_parked'] = is_parked
        context['can_edit'] = can_edit
        context['is_locked'] = is_locked
        context['images'] = []
        context['MEDIA_URL'] = settings.MEDIA_URL
        context['permissions'] = permissions
        context['content_type'] = ContentType.objects.get_for_model(Certificate, False)
        return context


class DeleteCertificateView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        certificate = Certificate.objects.get(pk=self.kwargs['pk'])
        if certificate:
            certificate.delete()
        messages.error(self.request, 'Document deleted successfully')
        return HttpResponseRedirect(reverse('certificates'))
