from django.db import models
from safedelete.models import SafeDeleteModel

from docuflow.apps.company.models import Company, Account, VatCode, ObjectItem, CompanyObject, PaymentMethod, Currency
from docuflow.apps.invoice.models import InvoiceType
from . import utils


class Certificate(models.Model):
    company = models.ForeignKey(Company, related_name='certificates', on_delete=models.CASCADE)
    document_type = models.ForeignKey('invoice.InvoiceType', related_name='certificates', on_delete=models.PROTECT,
                                      verbose_name='Document Type')
    supplier = models.ForeignKey('supplier.Supplier', related_name='certificates', on_delete=models.PROTECT)
    supplier_name = models.CharField(max_length=255, default='')
    document_number = models.CharField(max_length=255, default='', verbose_name='Document Number')
    certificate_id = models.PositiveIntegerField(null=True)
    reference_1 = models.CharField(max_length=255, default='')
    reference_2 = models.CharField(max_length=255, default='')
    employee = models.ForeignKey('payroll.Employee', null=True, blank=True, related_name='certificates', on_delete=models.PROTECT)
    document_date = models.DateField(verbose_name='Document Date', null=True, blank=True)
    expiry_date = models.DateField(null=True)
    reminder = models.BooleanField(default=False)
    reminder_days = models.PositiveIntegerField(default=30, verbose_name='Days before')
    remind_role = models.ForeignKey('accounts.Role', null=True, blank=True, related_name='certificate_reminders',
                                    on_delete=models.PROTECT)
    comment = models.TextField(blank=True)
    filename = models.CharField(max_length=255, blank=True)
    attachment = models.FileField(upload_to=utils.certificates_upload_path, blank=True, max_length=255)
    owner = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name='owner_documents',
                              on_delete=models.PROTECT)
    profiles = models.ManyToManyField('accounts.Profile')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-id', )

    class QS(models.QuerySet):

        def for_owner(self, profile):
            return self.filter(models.Q(owner=profile) | models.Q(profiles__in=[profile]))

    objects = QS.as_manager()

    def __str__(self):
        return f"{self.company.name[0:3].upper()}-{str(self.certificate_id).rjust(4, '0')}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.certificate_id:
            filter_options = {'company': self.company}
            self.certificate_id = self.generate_certificate_id(1, filter_options)
        return super(Certificate, self).save(*args, **kwargs)

    def generate_certificate_id(self, counter, filter_options):
        certificate = Certificate.objects.filter(**filter_options).first()
        if certificate:
            counter = certificate.certificate_id + 1
            filter_options['certificate_id'] = counter
            return self.generate_certificate_id(counter, filter_options)
        return counter

    @property
    def file_location(self):
        if not self.filename:
            return ''
        return f"{self.company.slug}/certificates/{self.pk}/{self.filename}"
