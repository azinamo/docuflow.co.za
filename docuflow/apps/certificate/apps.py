from django.apps import AppConfig


class CertificateConfig(AppConfig):
    name = 'docuflow.apps.certificate'
