from __future__ import unicode_literals


from django import forms
from django.contrib.auth import get_user_model


class UserForm(forms.ModelForm):
    """
    Form used to edit an user's mininal fields by the user himself
    """

    class Meta:
        fields = ('username', 'first_name', 'last_name', 'email')
        model = get_user_model()

