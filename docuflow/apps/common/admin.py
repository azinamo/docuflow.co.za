from django.contrib.admin.filters import SimpleListFilter
from django.utils.timezone import now


class YearFilter(SimpleListFilter):
    title = 'year'
    parameter_name = 'year'

    def lookups(self, request, model_admin):
        to_year = now().year + 1
        return [(year, year) for year in range(to_year, 2016, -1)]

    def queryset(self, request, queryset):
        return queryset.filter(period__period_year__year=self.value())

