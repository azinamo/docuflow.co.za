from __future__ import unicode_literals

import logging
import os
import re
import shutil
import hashlib
import ssl
import tempfile
import types
from collections import defaultdict, OrderedDict
from decimal import Decimal, InvalidOperation
from dateutil import parser
from datetime import datetime
from typing import List
from urllib.parse import urljoin
from functools import reduce

import boto3
from botocore.exceptions import ClientError
from django.db.models import Q
from django.conf import settings
from django.core.cache import cache
from django.utils.datastructures import MultiValueDict
from django.utils.http import urlencode as django_urlencode
from django.utils.http import urlquote as django_urlquote

from docuflow.apps.accounts.enums import NumberOfRecords
from .settings import setting_temporary_directory

logger = logging.getLogger(__name__)


def get_form(request, form_cls, prefix):
    data = request.POST if prefix in request.POST else None
    return form_cls(data, prefix=prefix)


# http://stackoverflow.com/questions/123198/how-do-i-copy-a-file-in-python
def copyfile(source, destination, buffer_size=1024 * 1024):
    """
    Copy a file from source to dest. source and dest
    can either be strings or any object with a read or
    write method, like StringIO for example.
    """
    source_descriptor = get_descriptor(source)
    destination_descriptor = get_descriptor(destination, read=False)

    while True:
        copy_buffer = source_descriptor.read(buffer_size)
        if copy_buffer:
            destination_descriptor.write(copy_buffer)
        else:
            break

    source_descriptor.close()
    destination_descriptor.close()


def encapsulate(function):
    # Workaround Django ticket 15791
    # Changeset 16045
    # http://stackoverflow.com/questions/6861601/
    # cannot-resolve-callable-context-variable/6955045#6955045
    return lambda: function


def fs_cleanup(filename, file_descriptor=None, suppress_exceptions=True):
    """
    Tries to remove the given filename. Ignores non-existent files
    """
    if file_descriptor:
        os.close(file_descriptor)

    try:
        os.remove(filename)
    except OSError:
        try:
            shutil.rmtree(filename)
        except OSError:
            if suppress_exceptions:
                pass
            else:
                raise


def get_descriptor(file_input, read=True):
    try:
        # Is it a file like object?
        file_input.seek(0)
    except AttributeError:
        # If not, try open it.
        if read:
            return open(file_input, 'rb')
        else:
            return open(file_input, 'wb')
    else:
        return file_input


def TemporaryFile(*args, **kwargs):
    kwargs.update({'dir': setting_temporary_directory.value})
    return tempfile.TemporaryFile(*args, **kwargs)


def mkdtemp(*args, **kwargs):
    kwargs.update({'dir': setting_temporary_directory.value})
    return tempfile.mkdtemp(*args, **kwargs)


def mkstemp(*args, **kwargs):
    kwargs.update({'dir': setting_temporary_directory.value})
    return tempfile.mkstemp(*args, **kwargs)


def return_attrib(obj, attrib, arguments=None):
    try:
        if isinstance(attrib, types.FunctionType):
            return attrib(obj)
        elif isinstance(
            obj, types.DictType
        ) or isinstance(obj, types.DictionaryType):
            return obj[attrib]
        else:
            result = reduce(getattr, attrib.split('.'), obj)
            if isinstance(result, types.MethodType):
                if arguments:
                    return result(**arguments)
                else:
                    return result()
            else:
                return result
    except Exception as exception:
        if settings.DEBUG:
            return 'Attribute error: %s; %s' % (attrib, exception)
        else:
            return unicode(exception)


def urlquote(link=None, get=None):
    """
    This method does both: urlquote() and urlencode()

    urlqoute(): Quote special characters in 'link'

    urlencode(): Map dictionary to query string key=value&...

    HTML escaping is not done.

    Example:

    urlquote('/wiki/Python_(programming_language)')
        --> '/wiki/Python_%28programming_language%29'
    urlquote('/mypath/', {'key': 'value'})
        --> '/mypath/?key=value'
    urlquote('/mypath/', {'key': ['value1', 'value2']})
        --> '/mypath/?key=value1&key=value2'
    urlquote({'key': ['value1', 'value2']})
        --> 'key=value1&key=value2'
    """
    if get is None:
        get = []

    assert link or get
    if isinstance(link, dict):
        # urlqoute({'key': 'value', 'key2': 'value2'}) -->
        # key=value&key2=value2
        assert not get, get
        get = link
        link = ''
    assert isinstance(get, dict), 'wrong type "%s", dict required' % type(get)
    # assert not (link.startswith('http://') or link.startswith('https://')),
    #    'This method should only quote the url path.
    #    It should not start with http(s)://  (%s)' % (
    #    link)
    if get:
        # http://code.djangoproject.com/ticket/9089
        if isinstance(get, MultiValueDict):
            get = get.lists()
        if link:
            link = '%s?' % django_urlquote(link)
        return '%s%s' % (link, django_urlencode(get, doseq=True))
    else:
        return django_urlquote(link)


def validate_path(path):
    if not os.path.exists(path):
        # If doesn't exist try to create it
        try:
            os.mkdir(path)
        except Exception as exception:
            logger.debug('unhandled exception: %s', exception)
            return False

    # Check if it is writable
    try:
        fd, test_filepath = tempfile.mkstemp(dir=path)
        os.close(fd)
        os.unlink(test_filepath)
    except Exception as exception:
        logger.debug('unhandled exception: %s', exception)
        return False

    return True


def group_object_postings(object_postings):
    object_posting_grouping = defaultdict(list)
    for object_posting in object_postings:
        for object_posting_split in object_posting.object_splitting.all():
            object_posting_grouping[object_posting].append(object_posting_split)
    return object_posting_grouping


def create_document_number(company, prefix, reference, padding=settings.DEFAULT_PADDING):
    company_str = company.name[0:3] if company else ""

    padding = padding + 2 if '-' in reference else padding

    counter_str = str(reference).rjust(padding, '0')
    if company_str == "":
        return f"{prefix}{counter_str}"
    else:
        return f"{prefix}-{company_str.upper()}{counter_str}"


def round_decimal(val, places=2, round_factor=0, normalize=True):
    try:
        if not isinstance(val, Decimal):
            val = Decimal(val)
        return Decimal(val.quantize(Decimal('.01')))
    except ValueError:
        return Decimal(val)


def cache_profile(company, **kwargs):
    for key, value in kwargs.items():
        if value is not None:
            cache.set(f"{key}_{company}", value)


def datatable_pages_menu(number_of_records: int) -> List[int]:
    default = NumberOfRecords.get_values()
    if number_of_records in default:
        return default
    else:
        menu_items = []
        for i in default:
            if i < number_of_records:
                menu_items.append(i)
            else:
                if number_of_records not in menu_items:
                    menu_items.append(number_of_records)
                else:
                    menu_items.append(i)
        return menu_items


def generate_tabbing_matrix(col_count: int, rows_count: int, counter_start=22, is_reload=False, row=1):
    """
        Generates matrix for displaying rows and columns with correct tabbing index
    """
    rows = {}
    tab_counter = (col_count * (row - 1)) + counter_start if is_reload else counter_start
    col_init = 0
    start = row if row > 1 else 1
    for i in range(start, rows_count):
        cols = OrderedDict()
        for c in range(col_init, col_count):
            tab_counter = tab_counter + 1
            cols[f'col{c}'] = tab_counter
        rows[i] = cols
    return rows


def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'


def is_ajax_post(request):
    return request.method == 'POST' and is_ajax(request)


def format_money(amount, include_cents=True):
    if not amount:
        amount = 0
    amount = Decimal(amount)
    if not include_cents:
        return f"{amount:20,.0f}"
    return f"{amount:20,.2f}"


def allow_https_images():
    if not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
        ssl._create_default_https_context = getattr(ssl, '_create_unverified_context')


def get_cell_value(term):
    if isinstance(term, str):
        return term.strip().strip('\n')
    return term


def to_decimal(value):
    try:
        if isinstance(value, str):
            for st in [',', ' ']:
                value = value.replace(st, '')
        elif isinstance(value, Decimal):
            return value
        return Decimal(value).quantize(Decimal('0.01'))
    except (ValueError, InvalidOperation, TypeError):
        return 0


def parse_date(date_str):
    try:
        return parser.parse(date_str)
    except (ValueError, OverflowError, parser.ParserError) as exc:
        logger.exception(exc)
    return None


def parse_datetime(value, date_format=''):
    try:

        if date_format:
            return datetime.strptime(value, date_format)
        elif '-' in value:
            result = datetime.strptime(value, '%d-%m-%Y')
            if not result:
                result = datetime.strptime(value, '%d-%b-%Y')
            return result
        if ' ' in value:
            try:
                result = datetime.strptime(value, '%d %m %Y')
            except ValueError:
                result = datetime.strptime(value, '%d %b %Y')
            return result
        elif '/' in value:
            return datetime.strptime(value, '%d/%m %Y')
        return datetime.strptime(value, '%Y%m%d')
    except ValueError:
        return None


def to_date(value, date_format=''):
    if not isinstance(value, str):
        return value
    date_obj = parse_date(date_str=value)
    if not date_obj:
        date_obj = parse_datetime(value=value, date_format=date_format)
    return date_obj


def get_profile_cache_key(user_id, company_id):
    return f"user_{user_id}_profile_{company_id}"


def get_profile_branch_cache_key(profile_id, company_id):
    return f"{profile_id}_branch_{company_id}"


def get_profile_branches_cache_key(profile_id, company_id):
    return f'{profile_id}_profile_{company_id}_branches'


def get_profile_role_cache_key(profile_id, company_id):
    return f"{profile_id}_role_{company_id}"


def get_profile_roles_cache_key(profile_id, company_id):
    return f'{profile_id}_profile_{company_id}_roles'


def get_profile_year_cache_key(profile_id, company_id):
    return f"{profile_id}_year_{company_id}"


def get_profile_years_cache_key(session):
    return f"{session['profile']}_years_{session['company']}"


def get_year_role_stats_cache_key(role_id, year):
    return f"{year.id}_role_stats_{role_id}" if year else f"year_role_stats_{role_id}"


def bucket_base_url():
    return f"https://{settings.AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com/"


def upload_to_s3(file_path: str, filename: str):
    s3_client = boto3.client(
        's3',
        region_name=settings.AWS_S3_REGION_NAME,
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
    )

    try:
        s3_client.upload_file(file_path, settings.AWS_STORAGE_BUCKET_NAME, filename)
    except ClientError as exc:
        logger.info(f'File could not be uploaded to s3 {exc}')
        raise
    return urljoin(bucket_base_url(), filename)


def flatten(multi_list, flat_list):
    for item in multi_list:
        if type(item) == list:
            flatten(item, flat_list)
        else:
            flat_list.append(item)
    return flat_list


def create_dir(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    return dir_name


def clean_filename(filename):
    extension = os.path.splitext(filename)[1].lower()
    file_name = re.sub('\W+', '_', filename)
    return f"{file_name}{extension}"


def make_hash(string):
    return hashlib.md5(string).hexdigest()


def create_checksum(company_id, data, filename):
    file_name = clean_filename(filename=filename)
    return make_hash(data + bytes(company_id) + bytes(file_name.encode('utf-8')))


def get_field_iexact_in(field: str, values):
    q_list = map(lambda t: Q(**{f'{field}__iexact': t}), values)
    q_list = reduce(lambda a, b: a | b, q_list)
    return q_list
