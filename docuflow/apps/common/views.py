from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView, ListView

from .mixins import ProfileMixin


class HomeView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'common/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context


class NoticeBoardView(TemplateView):
    """
    Notice board for authenticated user
    """
    template_name = 'common/notice_board.html'
    context_object_name = 'notifications'


def page_not_found_view(request, exception):

    return render(request, 'common/errors/404.html', {'exception': exception})


def error_view(request):
    return render(request, 'common/errors/500.html')


def permission_denied_view(request, exception):
    return render(request, 'common/errors/403.html', {'exception': exception})


def bad_request_view(request, exception):
    return render(request, 'common/errors/400.html', {'exception': exception})