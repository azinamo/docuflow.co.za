from enumfields import Enum, IntEnum


class SaProvince(Enum):
    EASTERN_CAPE = 'Eastern Cape'
    FREE_STATE = 'Free State'
    GAUTENG = 'Gauteng'
    KWAZULU_NATAL = 'Kwazulu Natal'
    LIMPOPO = 'Limpopo'
    MPUMALANGA = 'Mpumalanga'
    NORTHERN_CAPE = 'Northern Cape'
    NORTH_WEST = 'North West'
    WESTERN_CAPE = 'Western Cape'


class Module(IntEnum):
    PURCHASE = 0
    ACCOUNTING = 1
    INVENTORY = 2
    SALES = 3
    JOURNAL = 4
    SYSTEM = 5
    PAYMENT = 6
    DOCUMENT = 7
    FIXED_ASSET = 8

    class Labels:
        PURCHASE = 'Purchase'
        ACCOUNTING = 'Account'
        INVENTORY = 'Inventory'
        SALES = 'Sales'
        JOURNAL = 'Journal'
        SYSTEM = 'System'
        PAYMENT = 'Payments'
        DOCUMENT = 'Important Documents'
        FIXED_ASSET = 'Fixed Asset'


class DueStatus(Enum):
    OVERDUE = 'danger'
    DUE = 'warning'
    NOT_DUE = 'success'


class DayAge:
    CURRENT = 0
    THIRTY_DAY = 30
    SIXTY_DAY = 60
    NINETY_DAY = 90
    ONE_TWENTY_DAY = 120


class Color(Enum):
    DEFAULT = 'default'
    WARNING = 'yellow'
    PRIMARY = 'blue'
    DANGER = 'red'
    SUCCESS = 'green'
    INFO = 'orange'
