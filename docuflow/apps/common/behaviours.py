from django.db import models


class Loggable(models.Model):

    CREATED = 0
    UPDATED = 1
    DELETED = 2

    ACTIONS = (
        (CREATED, 'Created'),
        (UPDATED, 'Updated'),
        (DELETED, 'Deleted')
    )

    comment = models.TextField(null=False, blank=False)
    link_url = models.TextField(max_length=255, blank=True, null=True)
    profile = models.ForeignKey('accounts.Profile', related_name='comments', null=True, blank=True, on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', related_name='comments', null=True, blank=True, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Timestampable(models.Model):

    created_at = models.DateTimeField('Created At', auto_now_add=True)
    updated_at = models.DateTimeField('Updated At', auto_now=True)

    class Meta:
        abstract = True


class Distributable(models.Model):
    company = models.ForeignKey('company.Company', on_delete=models.CASCADE)
    supplier = models.ForeignKey('supplier.Supplier', null=True, blank=True, on_delete=models.DO_NOTHING)
    distribution_id = models.PositiveIntegerField(null=True, blank=True)
    nb_months = models.PositiveIntegerField()
    starting_date = models.DateField()
    role = models.ForeignKey('accounts.Role', on_delete=models.DO_NOTHING)
    profile = models.ForeignKey('accounts.Profile', on_delete=models.DO_NOTHING)
    total_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    def __str__(self):
        return f"DISTR-{self.distribution_id}"


class Agable(models.Model):
    period = models.ForeignKey('period.Period', related_name='+', on_delete=models.CASCADE)
    unallocated = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    one_twenty_day = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    ninety_day = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    sixty_day = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    thirty_day = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    current = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    balance = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    data = models.JSONField(null=True, blank=True)

    class Meta:
        abstract = True
