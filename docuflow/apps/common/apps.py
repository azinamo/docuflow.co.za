from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = 'docuflow.apps.common'
