from functools import partial

from rest_enumfield import EnumField


FullEnumField = partial(EnumField, to_choice=lambda x: int(x.value), to_repr=lambda x: {'value': x.value, 'label': x.label, 'name': x.name})