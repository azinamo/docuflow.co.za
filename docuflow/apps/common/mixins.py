from __future__ import unicode_literals

import logging
import json

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.db.models.query import QuerySet
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.utils.translation import ungettext, gettext as __

from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.company.models import get_company, ObjectItem, Branch
from docuflow.apps.period.models import Year
from . import utils

logger = logging.getLogger(__name__)

__all__ = (
    'DeleteExtraDataMixin', 'ExtraContextMixin', 'FormExtraKwargsMixin',
    'MultipleObjectMixin', 'ObjectActionMixin',
    'ObjectListPermissionFilterMixin', 'ObjectNameMixin',
    'ObjectPermissionCheckMixin', 'RedirectionMixin',
    'ViewPermissionCheckMixin', 'ProfileMixin', 'AddRowMixin', 'ActiveYearRequiredMixin', 'AjaxFormViewMixin',
    'DataTableMixin', 'CompanyMixin', 'DocuflowPermissionMixin'
)


class DeleteExtraDataMixin(object):

    # noinspection PyUnresolvedReferences
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        if hasattr(self, 'get_delete_extra_data'):
            self.object.delete(**self.get_delete_extra_data())
        else:
            self.object.delete()

        return HttpResponseRedirect(success_url)


class ExtraContextMixin(object):
    """
    Mixin that allows views to pass extra context to the template
    """

    extra_context = {}

    def get_extra_context(self):
        return self.extra_context

    # noinspection PyUnresolvedReferences
    def get_context_data(self, **kwargs):
        context = super(ExtraContextMixin, self).get_context_data(**kwargs)
        context.update(self.get_extra_context())
        return context


class FormExtraKwargsMixin(object):
    """
    Mixin that allows a view to pass extra keyword arguments to forms
    """

    form_extra_kwargs = {}

    def get_form_extra_kwargs(self):
        return self.form_extra_kwargs

    # noinspection PyUnresolvedReferences
    def get_form_kwargs(self):
        result = super(FormExtraKwargsMixin, self).get_form_kwargs()
        result.update(self.get_form_extra_kwargs())
        return result


class MultipleInstanceActionMixin(object):
    # TODO: Deprecated, replace views using this with
    # MultipleObjectFormActionView or MultipleObjectConfirmActionView

    model = None
    success_message = __('Operation performed on %(count)d object')
    success_message_plural = __('Operation performed on %(count)d objects')

    # noinspection PyUnresolvedReferences
    def get_pk_list(self):
        return self.request.GET.get(
            'id_list', self.request.POST.get('id_list', '')
        ).split(',')

    def get_queryset(self):
        return self.model.objects.filter(pk__in=self.get_pk_list())

    def get_success_message(self, count):
        return ungettext(
            self.success_message,
            self.success_message_plural,
            count
        ) % {
            'count': count,
        }

    # noinspection PyUnresolvedReferences
    def post(self, request, *args, **kwargs):
        count = 0
        for instance in self.get_queryset():
            try:
                self.object_action(instance=instance)
            except PermissionDenied:
                pass
            else:
                count += 1

        messages.success(
            self.request,
            self.get_success_message(count=count)
        )

        return HttpResponseRedirect(self.get_success_url())


class MultipleObjectMixin(object):
    """
    Mixin that allows a view to work on a single or multiple objects
    """

    model = None
    object_permission = None
    pk_list_key = 'id_list'
    pk_list_separator = ','
    pk_url_kwarg = 'pk'
    queryset = None
    slug_url_kwarg = 'slug'

    # noinspection PyUnresolvedReferences
    def get_pk_list(self):
        result = self.request.GET.get(
            self.pk_list_key, self.request.POST.get(self.pk_list_key)
        )

        if result:
            return result.split(self.pk_list_separator)
        else:
            return None

    # noinspection PyUnresolvedReferences
    def get_queryset(self):
        if self.queryset is not None:
            queryset = self.queryset
            if isinstance(queryset, QuerySet):
                queryset = queryset.all()
        elif self.model is not None:
            queryset = self.model._default_manager.all()

        pk = self.kwargs.get(self.pk_url_kwarg)
        slug = self.kwargs.get(self.slug_url_kwarg)
        pk_list = self.get_pk_list()

        if pk is not None:
            queryset = queryset.filter(pk=pk)

        # Next, try looking up by slug.
        if slug is not None and (pk is None or self.query_pk_and_slug):
            slug_field = self.get_slug_field()
            queryset = queryset.filter(**{slug_field: slug})

        if pk_list is not None:
            queryset = queryset.filter(pk__in=self.get_pk_list())

        if pk is None and slug is None and pk_list is None:
            raise AttributeError(
                'Generic detail view %s must be called with '
                'either an object pk, a slug or an id list.'
                % self.__class__.__name__
            )

        if self.object_permission:
            # return AccessControlList.objects.filter_by_access(
            #     self.object_permission, self.request.user, queryset=queryset
            # )
            return queryset
        else:
            return queryset


class ObjectActionMixin(object):
    """
    Mixin that performs an user action to a queryset
    """

    success_message = 'Operation performed on %(count)d object'
    success_message_plural = 'Operation performed on %(count)d objects'

    def get_success_message(self, count):
        return ungettext(
            self.success_message,
            self.success_message_plural,
            count
        ) % {
            'count': count,
        }

    def object_action(self, instance, form=None):
        pass

    def view_action(self, form=None):
        self.action_count = 0

        for instance in self.get_queryset():
            try:
                self.object_action(form=form, instance=instance)
            except PermissionDenied:
                pass
            else:
                self.action_count += 1

        messages.success(self.request, self.get_success_message(count=self.action_count))


class ObjectListPermissionFilterMixin(object):
    object_permission = None

    def get_queryset(self):
        queryset = super(ObjectListPermissionFilterMixin, self).get_queryset()

        if self.object_permission:
            # return AccessControlList.objects.filter_by_access(
            #     self.object_permission, self.request.user, queryset=queryset
            # )
            return queryset
        else:
            return queryset


class ObjectNameMixin(object):
    def get_object_name(self, context=None):
        if not context:
            context = self.get_context_data()

        object_name = context.get('object_name')

        if not object_name:
            try:
                object_name = self.object._meta.verbose_name
            except AttributeError:
                object_name = __('Object')

        return object_name


class ObjectPermissionCheckMixin(object):
    object_permission = None

    # noinspection PyUnresolvedReferences
    def get_permission_object(self):
        return self.get_object()

    # noinspection PyUnresolvedReferences
    def dispatch(self, request, *args, **kwargs):
        return super(ObjectPermissionCheckMixin, self).dispatch(request, *args, **kwargs)


class RedirectionMixin(object):
    post_action_redirect = None
    action_cancel_redirect = None

    # noinspection PyUnresolvedReferences
    def dispatch(self, request, *args, **kwargs):
        post_action_redirect = self.get_post_action_redirect()
        action_cancel_redirect = self.get_action_cancel_redirect()

        self.next_url = self.request.POST.get(
            'next', self.request.GET.get(
                'next', post_action_redirect if post_action_redirect else self.request.META.get(
                    'HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL)
                )
            )
        )
        self.previous_url = self.request.POST.get(
            'previous', self.request.GET.get(
                'previous', action_cancel_redirect if action_cancel_redirect else self.request.META.get(
                    'HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL)
                )
            )
        )

        return super(
            RedirectionMixin, self
        ).dispatch(request, *args, **kwargs)

    def get_action_cancel_redirect(self):
        return self.action_cancel_redirect

    def get_context_data(self, **kwargs):
        context = super(RedirectionMixin, self).get_context_data(**kwargs)
        context.update(
            {
                'next': self.next_url,
                'previous': self.previous_url
            }
        )

        return context

    def get_post_action_redirect(self):
        return self.post_action_redirect

    def get_success_url(self):
        return self.next_url or self.previous_url


class ViewPermissionCheckMixin(object):
    view_permission = None

    # noinspection PyUnresolvedReferences
    def dispatch(self, request, *args, **kwargs):
        """ Permission check for this class """
        if not request.user.has_perms(self.view_permission):
            raise PermissionDenied("You do not have permission to delete events")
        return super(ViewPermissionCheckMixin, self).dispatch(request, *args, **kwargs)


class CompanyLoginRequiredMixin(LoginRequiredMixin):

    # noinspection PyUnresolvedReferences
    def handle_no_permission(self):
        slug = self.kwargs.get('slug', None)
        if slug:
            messages.error(self.request, 'Please login to your company to access this resource')
            return HttpResponseRedirect(reverse('company_login', kwargs={'slug': slug}))
        return super(CompanyLoginRequiredMixin, self).handle_no_permission()


class CompanyMixin(object):

    # noinspection PyUnresolvedReferences
    def get_company_year(self):
        try:
            return Year.objects.get(is_active=True, company_id=self.request.session['company'])
        except Year.DoesNotExist:
            logger.info(f"Year does not exist")
            return Year.objects.filter(company_id=self.request.session['company']).first()

    def get_company_branch(self):
        company = self.get_company()
        default_branch = company.branches.filter(is_default=True).first()
        if default_branch:
            return default_branch
        else:
            return company.branches.first()

    # noinspection PyUnresolvedReferences
    def get_company(self):
        return get_company(company_id=self.request.session['company'])

    def get_company_object_items(self, company):
        object_items = ObjectItem.objects.select_related('company_object').filter(company_object__company=company).all()
        company_object_items = {}
        for object_item in object_items:
            if object_item.company_object_id in company_object_items:
                company_object_items[object_item.company_object_id].append(object_item)
            else:
                company_object_items[object_item.company_object_id] = [object_item]
        return company_object_items

    def get_company_linked_models(self, company):
        linked_models = {}
        company_object_items = self.get_company_object_items(company)
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            linked_models[company_object.id] = {}
            linked_models[company_object.id]['values'] = {}
            linked_models[company_object.id]['label'] = company_object.label
            linked_models[company_object.id]['class'] = company_object
            linked_models[company_object.id]['object_items'] = []
            if company_object.id in company_object_items:
                linked_models[company_object.id]['object_items'] = company_object_items[company_object.id]
        return linked_models

    def get_company_object_for_module(self, company, module):
        linked_models = {}
        company_object_items = self.get_company_object_items(company)
        company_objects = company.company_objects.all()
        module = str(module)
        for company_object in company_objects:
            if module in company_object.modules:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['object_items'] = []
                if company_object.id in company_object_items:
                    linked_models[company_object.id]['object_items'] = company_object_items[company_object.id]
        return linked_models


class ProfileMixin(CompanyMixin):

    # noinspection PyUnresolvedReferences
    def get_role(self):
        cache_key = utils.get_profile_role_cache_key(
            profile_id=self.request.session['profile'], company_id=self.request.session['company']
        )
        role = cache.get(cache_key)
        if role is None:
            try:
                try:
                    role = Role.objects.select_related(
                        'company', 'manager_role', 'supervisor_role'
                    ).prefetch_related('permission_groups').get(pk=self.request.session['role'])
                except Role.DoesNotExist:
                    role = self.get_profile().roles.first()
                cache.set(cache_key, role)
            except Role.DoesNotExist:
                return None
        return role

    # noinspection PyUnresolvedReferences
    def get_profile(self):
        cache_key = utils.get_profile_cache_key(
            user_id=self.request.session['user'], company_id=self.request.session['company']
        )
        profile = cache.get(cache_key)
        if profile is None:
            try:
                profile = Profile.objects.select_related(
                    'role', 'branch', 'year', 'user', 'company'
                ).prefetch_related(
                    'roles', 'branches',
                ).get(id=self.request.session.get('profile'))
                cache.set(cache_key, profile)
            except Profile.DoesNotExist:
                logger.info(f"Profile {self.request.session.get('profile')} does not exist")
        return profile

    # noinspection PyUnresolvedReferences
    def get_profile_year(self):
        profile = self.get_profile()
        return profile.year

    # noinspection PyUnresolvedReferences
    def get_year(self):
        user = self.request.session['user']
        cache_key = utils.get_profile_year_cache_key(
            company_id=self.request.session['company'], profile_id=self.request.session['profile']
        )
        year = cache.get(cache_key)
        if year is None:
            try:
                year = Year.objects.get(pk=self.request.session['year'])
            except Year.DoesNotExist:
                year = self.get_company_year()
            cache.set(cache_key, year)
        return year

    def get_profile_branch(self):
        return self.get_profile().branch

    # noinspection PyUnresolvedReferences
    def get_branch(self):
        cache_key = utils.get_profile_branch_cache_key(
            profile_id=self.request.session['profile'], company_id=self.request.session['company']
        )
        branch = cache.get(cache_key)
        if not branch:
            try:
                branch = Branch.objects.get(pk=self.request.session['branch'])
            except Branch.DoesNotExist:
                branch = self.get_profile_branch()
                if not branch:
                    branch = self.get_company_branch()
            cache.set(cache_key, branch)
        return branch


class BranchMixin(object):

    # noinspection PyUnresolvedReferences
    def get_company_branch(self):
        try:
            return Branch.objects.get(company_id=self.request.session['company'], is_default=True)
        except Branch.DoesNotExist:
            logger.info(f"Default branch for company {self.request.session.get('company')} does not exist")
            return None

    # noinspection PyUnresolvedReferences
    def get_profile_branch(self):
        try:
            return Branch.objects.get(pk=self.request.session['profile_branch'])
        except Branch.DoesNotExist:
            logger.info(f"Branch {self.request.session.get('profile_branch')} does not exist")
            return None

    # noinspection PyUnresolvedReferences
    def get_branch(self):
        cache_key = utils.get_profile_branch_cache_key(
            profile_id=self.request.session['profile'], company_id=self.request.session['company']
        )
        branch = cache.get(cache_key)
        if branch is None:
            branch_id = self.request.session.get('branch_id', None)
            if branch_id:
                branch = Branch.objects.get(pk=branch_id)
            else:
                branch = self.get_profile_branch()
                if not branch:
                    branch = self.get_company_branch()
            cache.set(cache_key, branch)
        return branch


class SetHeadline(object):
    headline = None

    # noinspection PyUnresolvedReferences
    def get_context_data(self, **kwargs):
        kwargs = super(SetHeadline, self).get_context_data(**kwargs)
        kwargs.update({'headline': self.get_headline()})
        return kwargs

    def get_headline(self):
        return self.headline


class AddRowMixin(object):

    # noinspection PyUnresolvedReferences
    def get_rows(self):
        row_id = self.request.GET.get('row_id', None)
        requested_rows = int(self.request.GET.get('rows', 20))
        if row_id:
            return int(row_id) + requested_rows
        if requested_rows <= 1:
            requested_rows = max(requested_rows, 2)
        return requested_rows

    # noinspection PyUnresolvedReferences
    def get_row_start(self):
        row_id = self.request.GET.get('row_id', None)
        if row_id:
            return int(row_id)
        return 1

    # noinspection PyMethodMayBeStatic
    def get_tabs_start(self, start_at, row_id, is_reload=False):
        if is_reload:
            return start_at
        if row_id:
            return start_at * row_id
        return start_at


class ActiveYearRequiredMixin(ProfileMixin, UserPassesTestMixin):
    permission_denied_message = 'You are not allowed to do this action for this year. Its been finalized'

    # noinspection PyUnresolvedReferences
    def test_func(self):
        return not self.get_year().is_final


class AjaxFormViewMixin(object):
    success_message = None
    failure_message = None
    redirect_url = None
    reload = False

    # noinspection PyUnresolvedReferences
    def dispatch(self, request, *args, **kwargs):
        """
            Turns a FormView into a special view that can handle AJAX requests.
        """
        self.request = request
        self.kwargs = kwargs
        if request.POST.get('skip_form', request.GET.get('skip_form')):
            self.kwargs = kwargs
            if hasattr(self, 'get_object'):
                self.object = self.get_object()
            return redirect(self.get_success_url())
        return super(AjaxFormViewMixin, self).dispatch(request, *args, **kwargs)

    # noinspection PyUnresolvedReferences
    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                    'text': self.failure_message if self.failure_message else 'Error occurred, please try again',
                    'error': True,
                    'errors': form.errors
                })
        return super(AjaxFormViewMixin, self).form_invalid(form)

    # noinspection PyUnresolvedReferences
    def form_valid(self, form):
        """
        Returns JSON success response or redirects to success URL.
        :param form: When you leave this as ``None``, the mixin
          tries to get the value from the POST or GET data. If you set this
          to ``True`` or ``False``, it will override eventually set POST or
          GET data for the key ``form_valid_redirect``.
        """
        form.save()
        if utils.is_ajax(request=self.request):
            response = {'text': self.success_message if self.success_message else 'Successful', 'error': False }
            if self.reload:
                response.update({'reload': True})
            if self.redirect_url:
                response.update({'redirect': self.redirect_url})
            else:
                try:
                    redirect_url = self.get_success_url()
                except AttributeError:
                    redirect_url = None
                response.update({'redirect': redirect_url})
            return JsonResponse(response)

        return super(AjaxFormViewMixin, self).form_valid(form)


class DataTableMixin(object):

    # noinspection PyUnresolvedReferences
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        profile = self.get_profile()
        menu_items = utils.datatable_pages_menu(number_of_records=profile.number_of_records.value)
        context['profile'] = profile
        context['display_rows'] = profile.number_of_records.value
        context['menu_length'] = json.dumps(menu_items)
        context['year'] = self.get_year()
        context['company'] = self.get_company()
        return context


class DocuflowPermissionMixin(LoginRequiredMixin, PermissionRequiredMixin):

    def get_permission_required(self):
        self.permission_required = f"user.{self.permission_required}"
        return super().get_permission_required()

    # noinspection PyUnresolvedReferences
    def has_permission(self):
        """
        Override this method to customize the way permissions are checked.
        """
        perms = self.get_permission_required()
        return self.request.user.has_perms(perms)
