from functools import partial

from django.db.models import DecimalField

QuantityField = partial(DecimalField, default=0, decimal_places=2, max_digits=12)
MoneyField = partial(DecimalField, default=0, decimal_places=2, max_digits=12)