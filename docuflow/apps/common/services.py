def get_credit_amount(credit, debit):
    amount = credit
    if debit and debit < 0:
        amount += debit * -1

    if credit and credit < 0:
        amount = 0
    return amount


def get_debit_amount(debit, credit):
    amount = debit
    if credit and credit < 0:
        amount += credit * -1

    if debit and debit < 0:
        amount = 0

    return amount
