from django.urls import path

from . import views

urlpatterns = [
    path('dashboard/', views.HomeView.as_view(), name='dashboard'),
    path('noticeboard/', views.NoticeBoardView.as_view(), name='notice_board'),
    path('notifications/', views.NoticeBoardView.as_view(), name='notification-settings'),
]
