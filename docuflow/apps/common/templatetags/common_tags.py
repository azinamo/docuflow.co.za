import logging

from django import template

register = template.Library()

logger = logging.getLogger(__name__)


@register.simple_tag
def calculate_tab_index(index, row, cols, is_start=False):
    return index * row + row


@register.filter
def quantity(qty):
    if qty:
        return round(float(qty), 3)
    else:
        return "0.00"


@register.inclusion_tag('common/buttons/previous_level.html')
def previous_level_btn(redirect_url=None):
    return {'redirect_url': redirect_url}

