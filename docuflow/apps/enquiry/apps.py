from django.apps import AppConfig


class EnquiryConfig(AppConfig):
    name = 'docuflow.apps.enquiry'
