from django.core.management.base import BaseCommand
from django.db.models import Count, Q

from docuflow.apps.period.models import Period, Year
from docuflow.apps.company.models import Company
from docuflow.apps.fixedasset.models import FixedAsset
from docuflow.apps.fixedasset.models import OpeningBalance


class Command(BaseCommand):
    help = "Add years to fixed assets"

    def add_parent_year_opening_balance(self, asset: FixedAsset, fixed_asset: FixedAsset):
        opening_balance, is_new = OpeningBalance.objects.get_or_create(
            year=fixed_asset.year,
            fixed_asset=asset,
            defaults={
                'asset_value': fixed_asset.opening_asset_value,
                'depreciation_value': fixed_asset.opening_depreciation_value
            }
        )
        if fixed_asset.parent_asset:
            self.add_parent_year_opening_balance(asset=asset, fixed_asset=fixed_asset.parent_asset)

    def handle(self, *args, **kwargs):
        try:
            companies = Company.objects.prefetch_related('company_years').all()
            for company in companies:
                print(f"----------START NOW {company} -----------")
                # years = {
                #     year.year: year
                #     for year in company.company_years.all()
                # }
                for year in company.company_years.all():
                    print(f"----------YEAR NOW {year} ({year.id}) -----------")
                    for fixed_asset in FixedAsset.all_objects.filter(company=company):
                        opening_balances_count = OpeningBalance.objects.filter(year=year, fixed_asset=fixed_asset).count()

                        if opening_balances_count > 1:
                            print(f"\t\t----------Fix {fixed_asset}({fixed_asset.id})  on year {year}({year.id}) -------- {opening_balances_count}")
        except Exception as exception:
            print("Exception occurred {} ".format(exception))
