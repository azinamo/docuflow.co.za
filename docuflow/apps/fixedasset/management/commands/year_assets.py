import pprint
from collections import defaultdict
from django.core.management.base import BaseCommand

from docuflow.apps.company.models import Branch
from docuflow.apps.fixedasset.models import (FixedAsset, OpeningBalance, FixedAssetUpdate, FixedAssetDisposal,
                                             FixedAssetAdjustment, FixedAssetPeriodValue, Location)

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Add years to fixed assets"
    grouped_assets = {}
    children = []

    def add_year_asset(self, fixed_asset: FixedAsset, year_fixed_asset: FixedAsset):
        self.grouped_assets[fixed_asset]['parents'][year_fixed_asset.year.year] = {'asset': year_fixed_asset, 'id': year_fixed_asset.id}
        self.add_year_opening_balance(fixed_asset=fixed_asset, year_fixed_asset=year_fixed_asset)
        year_fixed_asset.delete()
        if year_fixed_asset.parent_asset:
            self.add_year_asset(fixed_asset=fixed_asset, year_fixed_asset=year_fixed_asset.parent_asset)

    def add_year_opening_balance(self, fixed_asset: FixedAsset, year_fixed_asset: FixedAsset):
        print(f"Opening balance is {year_fixed_asset.opening_asset_value} "
              f"and depreciation balance is {year_fixed_asset.opening_depreciation_value}")
        opening_balance, is_new = OpeningBalance.objects.get_or_create(
            year=year_fixed_asset.year,
            fixed_asset=fixed_asset,
            defaults={
                'asset_value': year_fixed_asset.opening_asset_value,
                'depreciation_value': year_fixed_asset.opening_depreciation_value
            }
        )

        return opening_balance

    def add_child_asset(self, fixed_asset: FixedAsset, year_fixed_asset):
        assets = FixedAsset.all_objects.select_related('year').filter(parent_asset=year_fixed_asset)
        print(f'fixed asset parent is {year_fixed_asset}({year_fixed_asset.id}) in year {year_fixed_asset.year.year} '
              f'has {assets.count()}')
        self.add_year_opening_balance(fixed_asset=fixed_asset, year_fixed_asset=year_fixed_asset)
        if assets.count() > 0:
            self.children.append(year_fixed_asset.id)
            print("Children now -->")
            print(self.children)
            for child in assets.all():
                print(f"All come here"
                      f" but has child {child}({child.id}) in year --> {child.year.year}")
                self.add_child_asset(fixed_asset=fixed_asset, year_fixed_asset=child)

            year_fixed_asset.delete()
        else:
            year_fixed_asset.deleted = None
            year_fixed_asset.save()

            print(f"Link updated from {self.children} to {year_fixed_asset}({year_fixed_asset.id})")
            updates = FixedAssetUpdate.objects.filter(fixed_asset_id__in=self.children)
            disposals = FixedAssetDisposal.objects.filter(fixed_asset_id__in=self.children)
            adjustments = FixedAssetAdjustment.objects.filter(fixed_asset_id__in=self.children)
            period_values = FixedAssetPeriodValue.objects.filter(fixed_asset_id__in=self.children)
            print(f"Found {updates.count()} updates,  {disposals.count()} disposals, {adjustments.count()} adjustments,  {period_values.count()} period_values ")
            for fixed_asset_update in updates:
                fixed_asset_update.fixed_asset = year_fixed_asset
                fixed_asset_update.save()

            for fixed_asset_disposal in disposals:
                fixed_asset_disposal.fixed_asset = year_fixed_asset
                fixed_asset_disposal.save()

            for fixed_asset_adjustment in adjustments:
                fixed_asset_adjustment.fixed_asset = year_fixed_asset
                fixed_asset_adjustment.save()

            for fixed_asset_period in period_values:
                fixed_asset_period.fixed_asset = year_fixed_asset
                fixed_asset_period.save()

    def handle(self, *args, **kwargs):
        companies_branches = defaultdict(list)
        for branch in Branch.objects.select_related('company').all():
            companies_branches[branch.company].append(branch)

        for fixed_asset in FixedAsset.objects.select_related('company').filter(company__isnull=False).all():
            if fixed_asset.company:
                print(f"---{fixed_asset}({fixed_asset.id})------------------------------------\r\n")
                branches = companies_branches.get(fixed_asset.company)
                default_branches = [branch for branch in branches if branch.is_default]
                if not default_branches:
                    default_branch = branches[0]
                else:
                    default_branch = default_branches[0]
                Location.objects.get_or_create(
                    branch=default_branch,
                    fixed_asset=fixed_asset,
                    defaults={
                        'percentage': 100
                    }
                )
