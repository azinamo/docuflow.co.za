import logging
import uuid
from collections import defaultdict, OrderedDict, namedtuple
from decimal import Decimal
from typing import List, Dict, Optional, Iterable
from django import forms

from docuflow.apps.company.models import Company
from docuflow.apps.period.models import Year, Period
from docuflow.apps.common.utils import group_object_postings, round_decimal
from .exceptions import IncompleteFixedAssetAccounts
from .models import FixedAsset, Addition, Location, DepreciationSplit, OpeningBalance
from .enums import AdditionType, DepreciationMethod
# from .modules.assetupdates.services import CreateAssetsDepreciationLedger

logger = logging.getLogger(__name__)

SplitDepreciation = namedtuple('SplitDepreciation', [
    'id', 'fixed_asset', 'object_item', 'object_posting', 'percentage'
])


class FixedAssetValue:
    opening_asset_value = 0
    opening_depreciation_value = 0
    net_book_value = 0
    opening_net_book_value = 0
    closing_balance = 0
    closing_depreciation = 0
    last_depreciation_date = None

    def __init__(self, year, fixed_asset):
        self.year = year
        self.fixed_asset = fixed_asset

    def calculate_asset_closing_balance(self, opening_asset_balance, asset_additions, asset_disposals):
        return opening_asset_balance + asset_additions - asset_disposals

    def execute(self):
        asset_additions = self.fixed_asset.get_total_additions(self.year)
        asset_disposals, depreciation_disposal = self.fixed_asset.get_total_disposals()
        year_depreciation = self.fixed_asset.calculate_total_depreciation()

        opening_asset_balance = 0
        opening_depreciation_balance = 0

        if self.fixed_asset.opening_asset_value:
            opening_asset_balance = self.fixed_asset.opening_asset_value
        if self.fixed_asset.opening_depreciation_value:
            opening_depreciation_balance = self.fixed_asset.opening_depreciation_value

        asset_closing_balance = self.calculate_asset_closing_balance(opening_asset_balance, asset_additions, asset_disposals)
        depreciation_closing_balance = self.calculate_depreciation_closing_balance(opening_depreciation_balance, depreciation_disposal, year_depreciation)

        self.opening_asset_value = self.fixed_asset.opening_asset_value
        self.opening_depreciation_value = self.fixed_asset.opening_depreciation_value
        self.net_book_value = FixedAsset.calculate_net_book_value(asset_closing_balance, depreciation_closing_balance)
        self.opening_net_book_value = FixedAsset.calculate_opening_net_book_value(opening_asset_balance, opening_depreciation_balance)
        self.closing_balance = asset_closing_balance
        self.closing_depreciation = depreciation_closing_balance
        self.last_depreciation_date = None


class FixedAssetLedgerLine:

    def __init__(self, fixed_asset, amount, object_items, branch=None, is_split=False):
        self.fixed_asset = fixed_asset
        self.amount = round_decimal(val=amount)
        self.object_items = [object_item for object_item in object_items if object_item]
        self.is_split = is_split
        self.branch = branch

    def get_object_links(self):
        object_links = {}
        for link in self.object_items:
            object_links[link.company_object.id] = ''
            object_links[link.company_object.id] = {'value': link.label, 'code': link.code, 'object_item': link}

        return object_links

    def __str__(self):
        label = "_".join([str(object_item.id) for object_item in self.object_items])
        if self.branch:
            label = f"{label}_{self.branch}"
        return label


class Asset:
    opening_depreciation_value = 0
    opening_asset_value = None
    opening_balance = 0
    depreciation_value = 0
    net_book_value = 0
    opening_net_book_value = 0
    closing_balance = 0
    closing_depreciation = 0
    last_depreciation_date = 0

    def __init__(self, fixed_asset, year):
        self.fixed_asset = fixed_asset
        self.year = year
        self.name = fixed_asset.name

    # noinspection PyMethodMayBeStatic
    def calculate_closing_balance(self, opening_balance, additions, disposals):
        return opening_balance + additions - disposals

    # noinspection PyMethodMayBeStatic
    def calculate_depreciation_closing_balance(self, opening_balance, disposal, current):
        return opening_balance - disposal + current

    def execute(self):
        logger.info(f'----asset {self.fixed_asset}({self.fixed_asset.id}) calculations ----')
        current_year_depreciation = self.fixed_asset.calculate_total_depreciation()
        asset_additions = self.fixed_asset.get_total_additions(self.year)
        asset_disposals, depreciation_disposal = self.fixed_asset.get_total_disposals()

        logger.info(f'Current year {self.year} depreciation {current_year_depreciation}')
        logger.info(f'Current year {self.year} additions {asset_additions}')
        logger.info(f'Current year {self.year} asset disposals {asset_disposals} and depreciation disposal {depreciation_disposal}')

        opening_balance = OpeningBalance.objects.filter(fixed_asset=self.fixed_asset, year=self.year).first()
        if opening_balance:
            logger.info(f"Opening balance is {opening_balance.asset_value}  and depreciation value {opening_balance.depreciation_value}")
            opening_asset_balance = opening_balance.asset_value if opening_balance and opening_balance.asset_value else 0
            opening_depreciation_balance = opening_balance.depreciation_value if opening_balance and opening_balance.depreciation_value else 0

            asset_closing_balance = FixedAsset.calculate_closing_balance(
                opening_balance=opening_asset_balance,
                additions=asset_additions,
                disposals=asset_disposals
            )
            depreciation_closing_balance = self.calculate_depreciation_closing_balance(
                opening_balance=opening_depreciation_balance,
                disposal=depreciation_disposal,
                current=current_year_depreciation
            )

            net_book_value = asset_closing_balance - depreciation_closing_balance
            opening_net_book_value = opening_asset_balance - opening_depreciation_balance

            self.opening_asset_value = opening_asset_balance
            self.opening_depreciation_value = opening_depreciation_balance
            self.net_book_value = net_book_value
            self.opening_net_book_value = opening_net_book_value
            self.closing_balance = asset_closing_balance
            self.closing_depreciation = depreciation_closing_balance
            self.last_depreciation_date = None


def get_assets_balances(company, year):
    fixed_assets = FixedAsset.objects.for_year(company=company, year=year)
    assets = {}
    year_additions = Addition.objects.filter(
        fixed_asset__in=[fixed_asset.id for fixed_asset in fixed_assets], date__year=year.year
    ).all()
    for fixed_asset in fixed_assets:
        opening_asset_balance = 0
        opening_depreciation_balance = 0
        asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
        current_year_depreciation = fixed_asset.calculate_total_depreciation()

        asset_additions, asset_revaluation, additional_disposal = calculate_asset_additions(
            year_additions=year_additions,
            fixed_asset=fixed_asset
        )
        asset_disposals += additional_disposal

        if fixed_asset.opening_value:
            opening_asset_balance = fixed_asset.opening_value
        if fixed_asset.opening_depreciation:
            opening_depreciation_balance = fixed_asset.opening_depreciation

        asset_closing_balance = FixedAsset.calculate_closing_balance(
            opening_balance=opening_asset_balance,
            additions=asset_additions,
            disposals=asset_disposals,
            revaluation=asset_revaluation
        )
        depreciation_closing_balance = FixedAsset.calculate_closing_depreciation(
            opening_balance=opening_depreciation_balance,
            disposal=depreciation_disposal,
            depreciation=current_year_depreciation
        )
        net_book_value = FixedAsset.calculate_net_book_value(
            asset_closing_balance=asset_closing_balance,
            depreciation_closing_balance=depreciation_closing_balance
        )
        opening_net_book_value = FixedAsset.calculate_opening_net_book_value(
            opening_asset_balance=opening_asset_balance,
            opening_depreciation_balance=opening_depreciation_balance
        )

        asset = {
            'asset': fixed_asset,
            'initial_balance': opening_asset_balance,
            'initial_depreciation': opening_depreciation_balance,
            'disposals': asset_disposals,
            'additions': asset_additions,
            'revaluation': asset_revaluation,
            'cost_price': fixed_asset.cost_price if fixed_asset.cost_price else 0,
            'closing_balance': asset_closing_balance,
            'closing_depreciation': depreciation_closing_balance,
            'depreciation_disposal': depreciation_disposal,
            'current_year_depreciation': current_year_depreciation,
            'closing_book_value': net_book_value,
            'opening_net_book_value': opening_net_book_value
        }

        assets[fixed_asset.id] = asset
    return assets


def calculate_asset_additions(year_additions, fixed_asset):
    asset_additions = 0
    asset_revaluation = 0
    asset_disposal = 0
    for addition in year_additions:
        if fixed_asset.id == addition.fixed_asset_id:
            if addition.addition_type == AdditionType.REVALUATION:
                asset_revaluation += addition.amount
            elif addition.addition_type == AdditionType.ADDITION:
                asset_additions += addition.amount
            elif addition.addition_type == AdditionType.DISPOSAL:
                asset_disposal += addition.amount
    return asset_additions, asset_revaluation, asset_disposal


def get_year_fixed_assets(year, exclude_disposed=False):
    qs = FixedAsset.objects.filter(year=year)
    if exclude_disposed:
        qs = qs.exclude(fixed_asset_disposal__gt=0)
    assets = {}
    for fixed_asset in qs:
        assets[fixed_asset.id] = fixed_asset
    return assets


# noinspection PyUnresolvedReferences
def get_categorized_fixed_assets(fixed_assets: List[FixedAsset]) -> Dict:
    categories_assets = dict()
    assets_total = 0
    depreciation_total = 0

    for fixed_asset in fixed_assets:
        opening_assets_value = 0
        opening_depreciation_value = 0
        if hasattr(fixed_asset, 'opening_value') and fixed_asset.opening_value:
            opening_assets_value += fixed_asset.opening_value
            assets_total += fixed_asset.opening_value
        if hasattr(fixed_asset, 'opening_depreciation') and fixed_asset.opening_depreciation:
            opening_depreciation_value += fixed_asset.opening_depreciation
            depreciation_total += fixed_asset.opening_depreciation

        if fixed_asset.category in categories_assets:
            categories_assets[fixed_asset.category]['assets'].append(fixed_asset)
            categories_assets[fixed_asset.category]['total_assets_value'] += opening_assets_value
            categories_assets[fixed_asset.category]['total_depreciation_value'] += assets_total
        else:
            categories_assets[fixed_asset.category] = {
                'assets': [fixed_asset],
                'total_assets_value': opening_assets_value,
                'total_depreciation_value': opening_depreciation_value
            }

    return {'categories': categories_assets, 'assets_total': assets_total, 'depreciation_total': depreciation_total}


# noinspection PyUnresolvedReferences
def get_categorized_with_nbv(fixed_assets: List[FixedAsset], year: Year) -> Dict:
    assets_total = 0
    depreciation_total = 0
    category_assets = {}
    total_opening_assets_value = 0
    total_opening_depreciation_value = 0

    year_additions = Addition.objects.filter(
        fixed_asset__in=[fixed_asset.id for fixed_asset in fixed_assets], date__year=year.year
    ).all()

    for fixed_asset in fixed_assets:
        asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()

        # TODO - Refactor
        # asset_additions = fixed_asset.get_total_additions(year=year)
        asset_additions, revaluation, additional_disposal = calculate_asset_additions(
            year_additions=year_additions,
            fixed_asset=fixed_asset
        )
        asset_disposals += additional_disposal

        year_depreciation = fixed_asset.calculate_total_depreciation()

        opening_asset_balance = fixed_asset.opening_value
        opening_depreciation_balance = fixed_asset.opening_depreciation

        asset_closing_balance = opening_asset_balance + asset_additions + revaluation - asset_disposals
        depreciation_closing_balance = opening_depreciation_balance - depreciation_disposal + year_depreciation

        net_book_value = asset_closing_balance - depreciation_closing_balance
        opening_net_book_value = opening_asset_balance - opening_depreciation_balance

        total_opening_assets_value += opening_asset_balance
        total_opening_depreciation_value += opening_depreciation_balance

        if fixed_asset.category in category_assets:
            category_assets[fixed_asset.category]['assets'][fixed_asset] = {
                    'opening_asset_value': opening_asset_balance,
                    'opening_depreciation_value': opening_depreciation_balance,
                    'net_book_value': net_book_value,
                    'opening_net_book_value': opening_net_book_value,
                    'closing_balance': asset_closing_balance,
                    'closing_depreciation': depreciation_closing_balance
            }
            category_assets[fixed_asset.category]['opening_balance'] += opening_asset_balance
            category_assets[fixed_asset.category]['depreciation_value'] += opening_depreciation_balance
            category_assets[fixed_asset.category]['assets_closing'] += asset_closing_balance
            category_assets[fixed_asset.category]['depreciation_closing'] += depreciation_closing_balance
        else:
            category_assets[fixed_asset.category] = {
                'assets': {
                    fixed_asset: {
                        'opening_asset_value': opening_asset_balance,
                        'opening_depreciation_value': opening_depreciation_balance,
                        'net_book_value': net_book_value,
                        'opening_net_book_value': opening_net_book_value,
                        'closing_balance': asset_closing_balance,
                        'closing_depreciation': depreciation_closing_balance
                    }
                },
                'opening_balance': opening_asset_balance,
                'depreciation_value': opening_depreciation_balance,
                'assets_closing': asset_closing_balance,
                'depreciation_closing': depreciation_closing_balance
            }
            assets_total += total_opening_assets_value
            depreciation_total += total_opening_depreciation_value
    return {'categories': category_assets, 'assets_total': assets_total, 'depreciation_total': depreciation_total}


def generate_fixed_asset_register(fixed_assets: List[FixedAsset], year: Optional[Year] = None):

    register = {
        'current_year_depreciation': 0,
        'cost_price': 0,
        'disposals': 0,
        'additions': 0,
        'opening_net_book_value': 0,
        'opening_balance': 0,
        'closing_balance': 0,
        'depreciation_opening_balance': 0,
        'depreciation_closing_balance': 0,
        'net_book_value': 0,
        'revaluations': 0,
        'depreciation_disposal': 0,
        'depreciation': 0,
        'categories': OrderedDict(),
        'assets_count': 0,
        'assets': {}
    }
    category_assets = {}
    year_additions = []
    if year:
        year_additions = Addition.objects.filter(
            fixed_asset__in=[fixed_asset.id for fixed_asset in fixed_assets], date__year=year.year
        ).all()
    for fixed_asset in fixed_assets:
        if fixed_asset.category.category not in category_assets:
            category_assets[fixed_asset.category.category] = {
                'categories': {},
                'total_depreciation': 0,
                'cost_price': 0,
                'disposals': 0,
                'additions': 0,
                'opening_net_book_value': 0,
                'opening_balance': 0,
                'closing_balance': 0,
                'opening_depreciation_balance': 0,
                'closing_depreciation_balance': 0,
                'net_book_value': 0,
                'depreciation_disposal': 0,
                'depreciation': 0,
                'revaluations': 0,
                'assets_count': 0
            }
        if fixed_asset.category not in category_assets[fixed_asset.category.category]['categories']:
            category_assets[fixed_asset.category.category]['categories'][fixed_asset.category] = {
                    'assets': {},
                    'total_depreciation': 0,
                    'cost_price': 0,
                    'disposals': 0,
                    'additions': 0,
                    'opening_net_book_value': 0,
                    'opening_balance': 0,
                    'closing_balance': 0,
                    'opening_depreciation_balance': 0,
                    'closing_depreciation_balance': 0,
                    'net_book_value': 0,
                    'depreciation_disposal': 0,
                    'depreciation': 0,
                    'revaluations': 0,
                    'assets_count': 0
                }

        asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
        current_year_depreciation = fixed_asset.calculate_total_depreciation()

        # TODO - get the year additions as a annotation
        asset_additions, asset_revaluation, additional_disposal = calculate_asset_additions(
            year_additions=year_additions,
            fixed_asset=fixed_asset
        )

        asset_disposals += additional_disposal

        opening_asset_balance = get_fixed_asset_amount(fixed_asset=fixed_asset, field='opening_value')
        opening_depreciation_balance = get_fixed_asset_amount(fixed_asset=fixed_asset, field='opening_depreciation')

        asset_closing_balance = FixedAsset.calculate_closing_balance(
            opening_balance=opening_asset_balance,
            additions=asset_additions,
            disposals=asset_disposals,
            revaluation=asset_revaluation
        )
        depreciation_closing_balance = FixedAsset.calculate_closing_depreciation(
            opening_balance=opening_depreciation_balance,
            disposal=depreciation_disposal,
            depreciation=current_year_depreciation
        )

        net_book_value = FixedAsset.calculate_net_book_value(
            asset_closing_balance=asset_closing_balance,
            depreciation_closing_balance=depreciation_closing_balance
        )
        opening_net_book_value = FixedAsset.calculate_opening_net_book_value(
            opening_asset_balance=opening_asset_balance,
            opening_depreciation_balance=opening_depreciation_balance
        )

        asset = {
            'initial_balance': opening_asset_balance,
            'opening_depreciation_balance': opening_depreciation_balance,
            'disposals': asset_disposals,
            'additions': asset_additions,
            'revaluations': asset_revaluation,
            'cost_price': fixed_asset.cost_price if fixed_asset.cost_price else 0,
            'closing_balance': asset_closing_balance,
            'closing_depreciation': depreciation_closing_balance,
            'depreciation_disposal': depreciation_disposal,
            'current_year_depreciation': current_year_depreciation,
            'closing_book_value': net_book_value,
            'opening_net_book_value': opening_net_book_value
        }

        register['assets'][fixed_asset] = asset

        register['cost_price'] += fixed_asset.cost_price if fixed_asset.cost_price else 0
        register['opening_balance'] += opening_asset_balance
        register['closing_balance'] += asset_closing_balance
        register['depreciation_opening_balance'] += opening_depreciation_balance
        register['depreciation_closing_balance'] += depreciation_closing_balance
        register['revaluations'] += asset_revaluation
        register['net_book_value'] += net_book_value
        register['disposals'] += asset_disposals
        register['additions'] += asset_additions
        register['opening_net_book_value'] += opening_net_book_value
        register['depreciation_disposal'] += depreciation_disposal
        register['current_year_depreciation'] += current_year_depreciation
        register['assets_count'] += 1

        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['opening_net_book_value'] += opening_net_book_value
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['cost_price'] += fixed_asset.cost_price if fixed_asset.cost_price else 0
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['opening_balance'] += opening_asset_balance
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['closing_balance'] += asset_closing_balance
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['opening_depreciation_balance'] += opening_depreciation_balance
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['closing_depreciation_balance'] += depreciation_closing_balance
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['revaluations'] += asset_revaluation
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['net_book_value'] += net_book_value
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['disposals'] += asset_disposals
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['additions'] += asset_additions
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['depreciation_disposal'] += depreciation_disposal
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['depreciation'] += current_year_depreciation
        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['assets_count'] += 1

        category_assets[fixed_asset.category.category]['categories'][fixed_asset.category]['assets'][fixed_asset] = asset

        category_assets[fixed_asset.category.category]['opening_net_book_value'] += opening_net_book_value
        category_assets[fixed_asset.category.category]['cost_price'] += fixed_asset.cost_price if fixed_asset.cost_price else 0
        category_assets[fixed_asset.category.category]['opening_balance'] += opening_asset_balance
        category_assets[fixed_asset.category.category]['closing_balance'] += asset_closing_balance
        category_assets[fixed_asset.category.category]['opening_depreciation_balance'] += opening_depreciation_balance
        category_assets[fixed_asset.category.category]['closing_depreciation_balance'] += depreciation_closing_balance
        category_assets[fixed_asset.category.category]['revaluations'] += asset_revaluation
        category_assets[fixed_asset.category.category]['net_book_value'] += net_book_value
        category_assets[fixed_asset.category.category]['disposals'] += asset_disposals
        category_assets[fixed_asset.category.category]['additions'] += asset_additions
        category_assets[fixed_asset.category.category]['depreciation_disposal'] += depreciation_disposal
        category_assets[fixed_asset.category.category]['depreciation'] += current_year_depreciation
        category_assets[fixed_asset.category.category]['assets_count'] += 1

    register['categories'] = category_assets

    return register


def get_fixed_asset_amount(fixed_asset: FixedAsset, field: str):
    amount = getattr(fixed_asset, field, 0)
    if amount:
        return amount
    return 0


def asset_register(fixed_asset: FixedAsset, year: Year):
    year_additions = Addition.objects.filter(fixed_asset=fixed_asset, date__year=year.year).all()

    asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
    current_year_depreciation = fixed_asset.calculate_total_depreciation()

    asset_additions = sum(addition.amount for addition in year_additions if addition.addition_type == AdditionType.ADDITION)
    asset_revaluation = sum(addition.amount for addition in year_additions if addition.addition_type == AdditionType.REVALUATION)
    asset_disposals += sum(addition.amount for addition in year_additions if addition.addition_type == AdditionType.DISPOSAL)
    # TODO - get the year additions as a annotation

    opening_asset_balance = get_fixed_asset_amount(fixed_asset=fixed_asset, field='opening_value')
    opening_depreciation_balance = get_fixed_asset_amount(fixed_asset=fixed_asset, field='opening_depreciation')

    asset_closing_balance = FixedAsset.calculate_closing_balance(
        opening_balance=opening_asset_balance,
        additions=asset_additions,
        disposals=asset_disposals,
        revaluation=asset_revaluation
    )
    depreciation_closing_balance = FixedAsset.calculate_closing_depreciation(
        opening_balance=opening_depreciation_balance,
        disposal=depreciation_disposal,
        depreciation=current_year_depreciation
    )

    net_book_value = FixedAsset.calculate_net_book_value(
        asset_closing_balance=asset_closing_balance,
        depreciation_closing_balance=depreciation_closing_balance
    )
    opening_net_book_value = FixedAsset.calculate_opening_net_book_value(
        opening_asset_balance=opening_asset_balance,
        opening_depreciation_balance=opening_depreciation_balance
    )

    return {
        'initial_balance': opening_asset_balance,
        'opening_depreciation_balance': opening_depreciation_balance,
        'disposals': asset_disposals,
        'additions': asset_additions,
        'revaluation': asset_revaluation,
        'cost_price': fixed_asset.cost_price if fixed_asset.cost_price else 0,
        'closing_balance': asset_closing_balance,
        'closing_depreciation': depreciation_closing_balance,
        'depreciation_disposal': depreciation_disposal,
        'current_year_depreciation': current_year_depreciation,
        'closing_book_value': net_book_value,
        'opening_net_book_value': opening_net_book_value,
    }


def generate_fixed_assets_register(fixed_assets: List[FixedAsset], year: Optional[Year] = None):
    year_additions = []
    if year:
        year_additions = Addition.objects.filter(
            fixed_asset__in=[fixed_asset.id for fixed_asset in fixed_assets], date__year=year.year
        ).all()
    assets = {}
    for fixed_asset in fixed_assets:
        asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
        current_year_depreciation = fixed_asset.calculate_total_depreciation()

        asset_additions, asset_revaluation, additional_disposal = calculate_asset_additions(
            year_additions=year_additions,
            fixed_asset=fixed_asset
        )

        asset_disposals += additional_disposal

        opening_asset_balance = get_fixed_asset_amount(fixed_asset=fixed_asset, field='opening_value')
        opening_depreciation_balance = get_fixed_asset_amount(fixed_asset=fixed_asset, field='opening_depreciation')

        asset_closing_balance = FixedAsset.calculate_closing_balance(
            opening_balance=opening_asset_balance,
            additions=asset_additions,
            disposals=asset_disposals,
            revaluation=asset_revaluation
        )
        depreciation_closing_balance = FixedAsset.calculate_closing_depreciation(
            opening_balance=opening_depreciation_balance,
            disposal=depreciation_disposal,
            depreciation=current_year_depreciation
        )

        net_book_value = FixedAsset.calculate_net_book_value(
            asset_closing_balance=asset_closing_balance,
            depreciation_closing_balance=depreciation_closing_balance
        )
        opening_net_book_value = FixedAsset.calculate_opening_net_book_value(
            opening_asset_balance=opening_asset_balance,
            opening_depreciation_balance=opening_depreciation_balance
        )

        assets[fixed_asset] = {
            'initial_balance': opening_asset_balance,
            'opening_depreciation_balance': opening_depreciation_balance,
            'disposals': asset_disposals,
            'additions': asset_additions,
            'revaluation': asset_revaluation,
            'cost_price': fixed_asset.cost_price if fixed_asset.cost_price else 0,
            'closing_balance': asset_closing_balance,
            'closing_depreciation': depreciation_closing_balance,
            'depreciation_disposal': depreciation_disposal,
            'current_year_depreciation': current_year_depreciation,
            'closing_book_value': net_book_value,
            'opening_net_book_value': opening_net_book_value,
        }
    return assets


def get_periods_fixed_assets(company: Company, year: Year, period: Period):
    fixed_assets = FixedAsset.objects.for_year(company=company, year=year).all()

    periods = Period.objects.filter(period_year=year, period__lte=period.period).order_by('from_date')

    assets = get_periods_depreciable_assets(periods=periods, fixed_assets=fixed_assets)

    return assets


def get_periods_depreciable_assets(periods, fixed_assets):
    assets = defaultdict(list)
    for period in periods:
        for fixed_asset in fixed_assets:
            if not fixed_asset.has_all_accounts():
                raise IncompleteFixedAssetAccounts(f'Asset {fixed_asset} has missing accounts. All assets must '
                                                   f'have an account to ensure the journal balance')
            can_depreciate = fixed_asset.can_depreciate(period=period)
            if can_depreciate:
                assets[period].append(fixed_asset)
    return assets


def calculate_asset_depreciation(period: Period, year: Year, asset: Asset):
    periods = Period.objects.filter(period_year=year, period__lte=period.period).order_by('from_date')
    fixed_asset = asset.fixed_asset
    net_book_value = asset.net_book_value
    closing_net_book_value = asset.opening_net_book_value
    closing_balance = asset.closing_balance

    if fixed_asset.depreciation_method == DepreciationMethod.SECTION_12_B:
        depreciation = fixed_asset.calculate_section_12_b(
            year=year,
            periods=periods,
            period=period,
            net_book_value=net_book_value
        )
        asset_value = fixed_asset.cost_price - Decimal(depreciation)
    elif fixed_asset.depreciation_method == DepreciationMethod.SECTION_12_E:
        depreciation = fixed_asset.calculate_section_12_e(
            closing_net_book_value=closing_net_book_value,
            net_book_value=net_book_value
        )
        asset_value = fixed_asset.cost_price - Decimal(depreciation)
    else:
        depreciation = fixed_asset.calculate_period_depreciation(
            period=period,
            net_book_value=net_book_value,
            closing_net_book_value=closing_net_book_value,
            closing_balance=closing_balance
        )
        asset_value = net_book_value - Decimal(depreciation)
    return asset_value, depreciation


def calculate_depreciation(fixed_asset, period: Period, year: Year, net_book_value, closing_net_book_value, closing_balance, periods=None):
    if not periods:
        periods = Period.objects.filter(period_year=year, period__lte=period.period).order_by('from_date')

    if fixed_asset.depreciation_method == DepreciationMethod.SECTION_12_B:
        depreciation = fixed_asset.calculate_section_12_b(
            year=year,
            periods=periods,
            period=period,
            net_book_value=net_book_value
        )
        asset_value = fixed_asset.cost_price - Decimal(depreciation)
    elif fixed_asset.depreciation_method == DepreciationMethod.SECTION_12_E:
        depreciation = fixed_asset.calculate_section_12_e(
            closing_net_book_value=closing_net_book_value,
            net_book_value=net_book_value
        )
        asset_value = fixed_asset.cost_price - Decimal(depreciation)
    else:
        depreciation = fixed_asset.calculate_period_depreciation(
            period=period,
            net_book_value=net_book_value,
            closing_net_book_value=closing_net_book_value,
            closing_balance=closing_balance
        )
        asset_value = net_book_value - Decimal(depreciation)
    return round_decimal(asset_value), round_decimal(depreciation)


def depreciate_fixed_assets(update, year: Year, period: Period):
    periods_assets = get_periods_fixed_assets(
        company=year.company,
        year=year,
        period=period
    )

    assets_ledger_lines = defaultdict(Decimal)
    assets = [asset for assets in periods_assets.values() for asset in assets]
    assets_register = generate_fixed_assets_register(fixed_assets=assets, year=year)

    periods = Period.objects.filter(period_year=year, period__lte=period.period).order_by('from_date')

    for period, period_outstanding_fixed_assets in periods_assets.items():
        for fixed_asset in period_outstanding_fixed_assets:
            asset = assets_register.get(fixed_asset)
            if asset:
                asset_value, depreciation = calculate_depreciation(
                    fixed_asset=fixed_asset,
                    period=period,
                    year=year,
                    net_book_value=asset['closing_book_value'],
                    closing_balance=asset['closing_balance'],
                    closing_net_book_value=asset['opening_net_book_value'],
                    periods=periods
                )
                data = {
                    'period': period,
                    'asset_value': asset_value,
                    'depreciation_value': depreciation,
                    'fixed_asset': fixed_asset,
                    'asset_update': update
                }
                asset_update_form = FixedAssetUpdateForm(data=data, company=update.company)
                if asset_update_form.is_valid():
                    fixed_asset_update = asset_update_form.save()
                    assets_ledger_lines[fixed_asset_update.fixed_asset] += fixed_asset_update.depreciation_value
                else:
                    logger.info(asset_update_form.errors)
                    raise forms.ValidationError(f'Something went wrong saving fixed asset {fixed_asset} update')

    ledger = CreateAssetsDepreciationLedger(
        assets=assets_ledger_lines,
        update=update,
        date=update.date
    )
    ledger.execute()


def get_fixed_assets_postings(fixed_asset, depreciation, locations, object_splits):
    if fixed_asset.postings.count() > 0:
        grouped_object_postings = group_object_postings(fixed_asset.postings.all())
        fixed_asset_line = FixedAssetLedgerLine(
            fixed_asset=fixed_asset,
            amount=depreciation,
            object_items=fixed_asset.object_items.all(),
        )
        postings = [fixed_asset_line]

        for object_posting, object_postings in grouped_object_postings.items():
            postings = split_fixed_asset_lines(object_postings=object_postings, postings=postings)
    else:
        fixed_asset_line = FixedAssetLedgerLine(
            fixed_asset=fixed_asset,
            amount=depreciation,
            object_items=fixed_asset.object_items.all(),
            is_split=True
        )
        postings = [fixed_asset_line]

    if object_splits:
        object_split_postings = []
        for depreciation_object_line, depreciation_splits in object_splits.items():
            for asset_line in postings:

                amount = (depreciation_object_line.percentage / 100) * asset_line.amount
                if len(depreciation_splits) > 0:
                    for depreciation_split in depreciation_splits:
                        if isinstance(depreciation_split, dict):
                            for parent_item, split_items in depreciation_split.items():
                                parent_item_amount = (parent_item.percentage / 100) * amount

                                for split_item in split_items:
                                    split_item_amount = (split_item.percentage / 100) * parent_item_amount
                                    fixed_asset_line = FixedAssetLedgerLine(
                                        fixed_asset=asset_line.fixed_asset,
                                        amount=split_item_amount,
                                        object_items=[depreciation_object_line.object_item, parent_item.object_item, split_item.object_item],
                                        is_split=True
                                    )
                                    object_split_postings.append(fixed_asset_line)
                        else:
                            split_amount = (depreciation_split.percentage / 100) * amount
                            fixed_asset_line = FixedAssetLedgerLine(
                                fixed_asset=asset_line.fixed_asset,
                                amount=split_amount,
                                object_items=[depreciation_object_line.object_item, depreciation_split.object_item],
                                is_split=True
                            )
                            object_split_postings.append(fixed_asset_line)
                else:
                    fixed_asset_line = FixedAssetLedgerLine(
                        fixed_asset=asset_line.fixed_asset,
                        amount=amount,
                        object_items=[depreciation_object_line.object_item],
                        is_split=True
                    )
                    object_split_postings.append(fixed_asset_line)
        postings = object_split_postings
    #
    if locations:
        locations_postings = []
        for location in locations:
            for asset_line in postings:

                amount = (location.percentage / 100) * asset_line.amount

                fixed_asset_line = FixedAssetLedgerLine(
                    fixed_asset=asset_line.fixed_asset,
                    amount=amount,
                    object_items=asset_line.object_items,
                    branch=location.branch,
                    is_split=True,
                )
                locations_postings.append(fixed_asset_line)
        postings = locations_postings

    return postings


def split_fixed_asset_lines(object_postings, postings):
    logger.info("\r\n------------START FIXED ASSET OBJECT SPLITTING----------------")
    split_assets = []

    for posting_asset_line in postings:
        for object_item_posting in object_postings:
            amount = (posting_asset_line.amount * object_item_posting.percentage_fraction)
            object_items = []
            if posting_asset_line.is_split:
                for item in posting_asset_line.object_items:
                    object_items.append(item)
            object_items.append(object_item_posting.object_item)
            fixed_asset_line = FixedAssetLedgerLine(
                fixed_asset=posting_asset_line.fixed_asset,
                amount=amount,
                object_items=object_items,
                is_split=True
            )
            split_assets.append(fixed_asset_line)

    return split_assets


def get_assets_objects_splits(fixed_assets):
    object_splits = {}
    qs = DepreciationSplit.objects.select_related(
        'object_item', 'fixed_asset', 'object_posting'
    ).prefetch_related(
        'children__object_posting__object_splitting__object_item'
    ).filter(
        fixed_asset__in=fixed_assets, parent__isnull=True)
    for depreciation_split in qs:
        if depreciation_split.fixed_asset in object_splits:
            object_splits[depreciation_split.fixed_asset][depreciation_split] = split_objects(
                depreciation_split=depreciation_split)
        else:
            object_splits[depreciation_split.fixed_asset] = {
                depreciation_split: split_objects(depreciation_split=depreciation_split)
            }
    return object_splits


def split_objects(depreciation_split):
    splits = []
    if depreciation_split.object_posting:
        for object_posting_split in depreciation_split.object_posting.object_splitting.all():
            object_split = SplitDepreciation(
                uuid.uuid4(),
                depreciation_split.fixed_asset,
                object_posting_split.object_item,
                None,
                object_posting_split.percentage
            )
            posting_splits = {object_split: []}

            for child_depreciation_split in depreciation_split.children.all():
                if child_depreciation_split.object_posting:
                    for child_posting_split in child_depreciation_split.object_posting.object_splitting.all():
                        percentage = child_posting_split.percentage * (child_depreciation_split.percentage / 100)
                        child_object_split = SplitDepreciation(
                            uuid.uuid4(),
                            depreciation_split.fixed_asset,
                            child_posting_split.object_item,
                            None,
                            Decimal(percentage).quantize(Decimal('.01'))
                        )
                        posting_splits[object_split].append(child_object_split)
                else:
                    posting_splits[object_split].append(child_depreciation_split)

            splits.append(posting_splits)
    else:
        for child_depreciation_split in depreciation_split.children.all():
            if child_depreciation_split.object_posting:
                for child_posting_split in child_depreciation_split.object_posting.object_splitting.all():
                    percentage = child_posting_split.percentage * (child_depreciation_split.percentage / 100)
                    child_object_split = SplitDepreciation(
                        uuid.uuid4(),
                        depreciation_split.fixed_asset,
                        child_posting_split.object_item,
                        None,
                        Decimal(percentage).quantize(Decimal('.01'))
                    )
                    splits.append(child_object_split)
            else:
                splits.append(child_depreciation_split)
    return splits


def get_assets_locations(fixed_assets):
    assets_locations = defaultdict(list)
    for location in Location.objects.select_related('branch', 'fixed_asset').filter(fixed_asset__in=fixed_assets):
        assets_locations[location.fixed_asset].append(location)
    return assets_locations
