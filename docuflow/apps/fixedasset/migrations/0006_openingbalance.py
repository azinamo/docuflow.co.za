# Generated by Django 3.1 on 2020-12-04 17:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('period', '0006_auto_20201203_1351'),
        ('fixedasset', '0005_auto_20201203_1351'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpeningBalance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('asset_value', models.DecimalField(blank=True, decimal_places=2, max_digits=12, null=True)),
                ('depreciation_value', models.DecimalField(blank=True, decimal_places=2, max_digits=12, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('fixed_asset', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='opening_balances', to='fixedasset.fixedasset')),
                ('year', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fixed_assets_opening_balances', to='period.year')),
            ],
        ),
    ]
