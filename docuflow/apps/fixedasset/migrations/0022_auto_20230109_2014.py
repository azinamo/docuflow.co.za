# Generated by Django 3.2 on 2023-01-09 18:14

from django.db import migrations
import docuflow.apps.fixedasset.enums
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('fixedasset', '0021_addition_journal_line'),
    ]

    operations = [
        migrations.AddField(
            model_name='addition',
            name='addition_type',
            field=enumfields.fields.EnumField(default='ADDITION', enum=docuflow.apps.fixedasset.enums.AdditionType, max_length=50),
        ),
    ]
