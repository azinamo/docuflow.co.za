# Generated by Django 3.1 on 2022-01-18 02:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fixedasset', '0018_addition_invoice_account'),
    ]

    operations = [
        migrations.AddField(
            model_name='update',
            name='date',
            field=models.DateField(null=True),
        ),
    ]
