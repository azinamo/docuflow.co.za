from django.apps import AppConfig


class AssetlocationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'docuflow.apps.fixedasset.modules.assetlocation'
