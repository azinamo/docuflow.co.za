import logging
import pprint
from decimal import Decimal

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.http.response import JsonResponse
from django.views.generic import View, ListView, TemplateView, DeleteView
from django.urls import reverse

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Branch
from docuflow.apps.fixedasset.models import FixedAsset, Location

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class FixedAssetLocationView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Location
    context_object_name = 'locations'
    template_name = 'assetlocation/index.html'

    def get_queryset(self):
        return super().get_queryset().filter(fixed_asset=self.kwargs['fixed_asset_id'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fixed_asset'] = FixedAsset.objects.filter(pk=self.kwargs['fixed_asset_id']).first()
        context['branches'] = Branch.objects.filter(company=self.get_company())
        context['row'] = 1
        return context


class CreateFixedAssetLocationView(ProfileMixin, TemplateView):
    template_name = 'assetlocation/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fixed_asset = FixedAsset.objects.filter(pk=self.kwargs['fixed_asset_id']).first()
        context['fixed_asset'] = fixed_asset
        context['locations'] = Location.objects.filter(fixed_asset=fixed_asset)
        context['branches'] = Branch.objects.filter(company=self.get_company())
        context['row'] = self.request.GET.get('row')
        return context


class AddFixedAssetLocationLineView(ProfileMixin, TemplateView):
    template_name = 'assetlocation/location_line.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fixed_asset'] = FixedAsset.objects.filter(pk=self.kwargs['fixed_asset_id']).first()
        context['branches'] = Branch.objects.filter(company=self.get_company())
        context['row'] = self.request.GET.get('row')
        return context


class SaveFixedAssetLocationView(ProfileMixin, View):
    template_name = 'assetlocation/location_line.html'

    def post(self, *args, **kwargs):
        fixed_asset = FixedAsset.objects.filter(pk=kwargs['fixed_asset_id']).first()
        branches = {
            str(branch.id): branch
            for branch in Branch.objects.filter(company=self.get_company())
        }

        locations = []
        locations_total = Location.objects.filter(fixed_asset=fixed_asset).aggregate(total=Sum('percentage'))
        total = locations_total['total'] if locations_total and locations_total['total'] else 0
        for field, value in self.request.POST.items():
            if field.startswith('location_branch', 0, 16):
                row = field[16:]
                branch_id = self.request.POST[f'location_branch_{row}']
                percentage = self.request.POST[f'location_percentage_split_{row}']

                if not branch_id:
                    continue

                branch = branches.get(branch_id)
                if not branch:
                    continue

                if not percentage or percentage == '':
                    continue

                percentage_split = Decimal(percentage)
                total += percentage_split
                locations.append(
                    Location(
                        fixed_asset=fixed_asset,
                        branch=branch,
                        percentage=percentage_split
                    )
                )

        if total != 100:
            return JsonResponse({
                'text': 'Total percentage must equal to 100%',
                'error': True
            })

        Location.objects.bulk_create(locations)

        return JsonResponse({
            'text': 'Locations saved successfully',
            'error': False,
            'reload': True
        })


class DeleteFixedAssetLocationView(LoginRequiredMixin, ProfileMixin, DeleteView):
    template_name = 'assetlocation/confirm_delete.html'
    model = Location
    context_object_name = 'location'

    def get_success_url(self):
        return reverse('edit_fixed_asset', args=(self.kwargs['fixed_asset_id'], ))

    def delete(self, request, *args, **kwargs):
        super().delete(request, *args, **kwargs)
        return JsonResponse({
            'error': False,
            'text': 'Location deleted successfully',
            'reload': True
        })
