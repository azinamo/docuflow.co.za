from django.urls import path

from . import views

urlpatterns = [
    path('', views.FixedAssetLocationView.as_view(), name='fixed_asset_locations'),
    path('create/', views.CreateFixedAssetLocationView.as_view(), name='create_fixed_asset_location'),
    path('line/', views.AddFixedAssetLocationLineView.as_view(), name='add_location_line'),
    path('save/', views.SaveFixedAssetLocationView.as_view(), name='save_fixed_asset_location'),
    path('<int:pk>/delete', views.DeleteFixedAssetLocationView.as_view(), name='delete_fixed_asset_location'),
]
