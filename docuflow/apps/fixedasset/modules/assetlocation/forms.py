from django import forms

from docuflow.apps.fixedasset.models import Location


class AssetLocationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.fixed_asset = kwargs.pop('fixed_asset')
        super(AssetLocationForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Location
        fields = ('branch', 'percentage')
        widgets = {
            'date': forms.TextInput(attrs={'class': 'datepicker'})
        }

    def save(self, commit=True):
        fixed_asset_addition = super().save(commit=False)
        fixed_asset_addition.fixed_asset = self.fixed_asset
        fixed_asset_addition.save()
        return fixed_asset_addition
