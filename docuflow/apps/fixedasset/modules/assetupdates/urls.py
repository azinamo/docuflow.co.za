from django.urls import path

from . import views

urlpatterns = [
    path('', views.UpdateFixedAssetIndexView.as_view(), name='fixed_asset_update'),
    path('create/', views.CreateFixedAssetUpdateView.as_view(), name='create_fixed_asset_updates'),
    path('<int:pk>/detail/', views.FixedAssetUpdateDetailView.as_view(), name='fixed_asset_journal_detail'),
]

