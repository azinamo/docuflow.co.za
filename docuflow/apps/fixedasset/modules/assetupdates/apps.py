from django.apps import AppConfig


class AssetUpdatesConfig(AppConfig):
    name = 'docuflow.apps.fixedasset.modules.assetupdates'
