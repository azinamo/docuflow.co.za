import logging
from collections import defaultdict
from decimal import Decimal
from django import forms

from docuflow.apps.fixedasset.models import Update, FixedAssetUpdate
from docuflow.apps.fixedasset.exceptions import IncompleteFixedAssetAccounts, NotFixedAssetsFound
from docuflow.apps.period.models import Period
from docuflow.apps.fixedasset.services import generate_fixed_assets_register, get_periods_fixed_assets, \
    calculate_depreciation, get_fixed_assets_postings, get_assets_locations, get_assets_objects_splits
from . import services


logger = logging.getLogger(__name__)


class UpdateDepreciationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        super(UpdateDepreciationForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open_for_depreciation_update(self.year, self.company)
        self.fields['period'].required = True
        self.fields['period'].empty_label = None

    date = forms.DateField(required=True)

    class Meta:
        model = Update
        fields = ('period', 'date', )
        widgets = {
            'period': forms.Select(attrs={'class': 'form-control chosen-select'}),
        }

    def clean(self):
        cleaned_data = super(UpdateDepreciationForm, self).clean()

        date = cleaned_data.get('date', None)
        period = cleaned_data.get('period', None)
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and update date do not match')

        try:
            periods_assets = get_periods_fixed_assets(
                company=self.company,
                year=self.year,
                period=self.cleaned_data['period']
            )
            if len(periods_assets) == 0:
                self.add_error('period', 'No fixed assets to process')
        except IncompleteFixedAssetAccounts as exception:
            raise IncompleteFixedAssetAccounts(str(exception))

        return cleaned_data

    def save(self, commit=True):
        update = super(UpdateDepreciationForm, self).save(commit=False)
        update.year = self.year
        update.company = self.company
        update.role = self.role
        update.created_by = self.profile
        update.save()

        periods_assets = get_periods_fixed_assets(
            company=self.company,
            year=self.year,
            period=self.cleaned_data['period']
        )

        assets_ledger_lines = defaultdict(Decimal)
        assets = [asset for assets in periods_assets.values() for asset in assets]
        assets_register = generate_fixed_assets_register(fixed_assets=assets, year=self.year)

        assets_locations = get_assets_locations(fixed_assets=assets)
        assets_object_splits = get_assets_objects_splits(fixed_assets=assets)

        periods = Period.objects.filter(
            period_year=self.year,
            period__lte=self.cleaned_data['period'].period
        ).order_by('from_date')

        for period, period_outstanding_fixed_assets in periods_assets.items():
            for fixed_asset in period_outstanding_fixed_assets:
                asset = assets_register.get(fixed_asset)
                if asset:
                    asset_locations = assets_locations.get(fixed_asset, [])
                    asset_object_splits = assets_object_splits.get(fixed_asset, [])

                    asset_value, depreciation = calculate_depreciation(
                        fixed_asset=fixed_asset,
                        period=self.cleaned_data['period'],
                        year=self.year,
                        net_book_value=asset['closing_book_value'],
                        closing_balance=asset['closing_balance'],
                        closing_net_book_value=asset['opening_net_book_value'],
                        periods=periods
                    )
                    data = {
                        'period': period,
                        'asset_value': asset_value,
                        'depreciation_value': depreciation,
                        'fixed_asset': fixed_asset,
                        'asset_update': update
                    }

                    object_depreciation_lines = get_fixed_assets_postings(
                        fixed_asset=fixed_asset,
                        depreciation=depreciation,
                        locations=asset_locations,
                        object_splits=asset_object_splits
                    )

                    data['depreciation_value'] = sum(line.amount for line in object_depreciation_lines)

                    asset_update_form = FixedAssetUpdateForm(data=data, company=self.company)
                    if asset_update_form.is_valid():
                        fixed_asset_update = asset_update_form.save()
                        assets_ledger_lines[fixed_asset_update.fixed_asset] += fixed_asset_update.depreciation_value
                    else:
                        logger.info(asset_update_form.errors)
                        raise forms.ValidationError(f'Something went wrong saving fixed asset {fixed_asset} update')

        ledger = services.CreateAssetsDepreciationLedger(
            assets=assets_ledger_lines,
            update=update,
            date=update.date
        )
        ledger.execute()

        return update


class FixedAssetUpdateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(FixedAssetUpdateForm, self).__init__(*args, **kwargs)
        self.fields['fixed_asset'].queryset = self.fields['fixed_asset'].queryset.filter(company=self.company)
        self.fields['period'].queryset = self.fields['period'].queryset.filter(period_year__company=self.company)

    class Meta:
        model = FixedAssetUpdate
        fields = ('asset_update', 'fixed_asset', 'period', 'asset_value', 'depreciation_value')
