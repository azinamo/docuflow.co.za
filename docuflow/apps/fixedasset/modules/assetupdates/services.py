import logging
import pprint
from decimal import Decimal

from docuflow.apps.common.enums import Module
from docuflow.apps.fixedasset.enums import DepreciationMethod
from docuflow.apps.fixedasset.exceptions import IncompleteFixedAssetAccounts, NotFixedAssetsFound
from docuflow.apps.fixedasset.models import (
    FixedAsset, Update, FixedAssetUpdate
)
from docuflow.apps.fixedasset.services import (
    Asset, get_fixed_assets_postings, get_assets_locations, get_assets_objects_splits
)
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.period.models import Period

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class FixedAssetDepreciationPeriod:

    assets = {}
    has_assets = False

    def __init__(self, periods, fixed_assets):
        self.periods = periods
        self.fixed_assets = fixed_assets

    def execute(self):
        for period in self.periods:
            self.assets[period] = []
            for fixed_asset in self.fixed_assets:
                if not fixed_asset.has_all_accounts():
                    raise IncompleteFixedAssetAccounts(f'Asset {fixed_asset} has missing accounts. All assets must '
                                                       f'have an account to ensure the journal balance')
                can_depreciate = fixed_asset.can_depreciate(period)
                logger.info(f"Fixed asset {fixed_asset}({fixed_asset.company}) --> can depreciate {can_depreciate} at period ({period}->{period.period})\n")
                if can_depreciate:
                    self.assets[period].append(fixed_asset)
                    self.has_assets = True


class CreateDepreciationUpdate:
    asset_update = None

    def __init__(self, company, profile, period, year, role, date):
        self.company = company
        self.profile = profile
        self.role = role
        self.period = period
        self.year = year
        self.date = date

    def get_current_year_periods(self):
        return Period.objects.filter(period_year=self.year, period__lte=self.period.period).order_by('from_date')

    def get_year_periods(self):
        return Period.objects.filter(period_year=self.year).order_by('from_date')

    def get_year_fixed_assets(self):
        return FixedAsset.objects.for_year(company=self.company, year=self.year)

    def get_periods_fixed_assets(self):
        periods = self.get_current_year_periods()
        fixed_assets = self.get_year_fixed_assets()

        period_fixed_asset = FixedAssetDepreciationPeriod(periods, fixed_assets)
        period_fixed_asset.execute()

        if not period_fixed_asset.has_assets:
            raise NotFixedAssetsFound('No fixed assets to process')
        return period_fixed_asset

    def get_asset(self, fixed_asset):
        asset = Asset(fixed_asset, self.year)
        asset.execute()
        return asset

    def execute(self):
        period_fixed_assets = self.get_periods_fixed_assets()

        self.asset_update = self.create_update()

        assets_ledger_lines = []
        for period, period_outstanding_fixed_assets in period_fixed_assets.assets.items():
            for fixed_asset in period_outstanding_fixed_assets:
                asset = self.get_asset(fixed_asset)
                if asset:
                    depreciation = self.calculate_asset_depreciation(asset)
                    # self.create_ledger_line(journal, fixed_asset, depreciation)
                    assets_ledger_lines.append({'asset': asset.fixed_asset, 'depreciation': depreciation})
        self.create_ledger(assets_ledger_lines)

    def calculate_asset_depreciation(self, asset):
        periods = self.get_year_periods()
        fixed_asset = asset.fixed_asset
        net_book_value = asset.net_book_value
        closing_net_book_value = asset.opening_net_book_value
        closing_balance = asset.closing_balance

        if fixed_asset.depreciation_method == DepreciationMethod.SECTION_12_B:
            depreciation = fixed_asset.calculate_section_12_b(self.year, periods, self.period, net_book_value)
            asset_value = fixed_asset.cost_price - Decimal(depreciation)
        elif fixed_asset.depreciation_method == DepreciationMethod.SECTION_12_E:
            depreciation = fixed_asset.calculate_section_12_e(closing_net_book_value, net_book_value)
            asset_value = fixed_asset.cost_price - Decimal(depreciation)
        else:
            depreciation = fixed_asset.calculate_period_depreciation(self.period, net_book_value,
                                                                     closing_net_book_value,
                                                                     closing_balance)
            asset_value = net_book_value - Decimal(depreciation)

        fixed_asset_update = self.create_fixed_asset_update(fixed_asset, depreciation, asset_value)

        return Decimal(fixed_asset_update.depreciation_value).quantize(Decimal('.01'))

    def create_update(self):
        logger.info('\r\n------------create_update--------------')
        update = Update.objects.create(
            year=self.year,
            company=self.company,
            role=self.role,
            period=self.period,
            created_by=self.profile
        )
        return update

    def create_fixed_asset_update(self, fixed_asset, depreciation, asset_value):
        logger.info('\r\n------------create_fixed_asset_update--------------')
        fixed_asset_update = FixedAssetUpdate.objects.create(
            asset_update=self.asset_update,
            period=self.period,
            asset_value=asset_value,
            depreciation_value=Decimal(depreciation),
            book_value=asset_value,
            fixed_asset=fixed_asset
        )
        logger.info(f"fixed asset update created {fixed_asset_update}({fixed_asset_update.id})")
        return fixed_asset_update

    def create_ledger(self, assets):
        ledger = CreateAssetsDepreciationLedger(assets, self.asset_update, self.date)
        ledger.execute()


class DepreciationLedgerLine:

    def __init__(self, fixed_asset, account_line, percentage):
        self.fixed_asset = fixed_asset
        self.account_line = account_line


class CreateAssetsDepreciationLedger:

    def __init__(self, assets, update, date):
        self.assets = assets
        self.update = update
        self.date = date
        self.ledger_lines = []

    def prepare_ledger_accounts(self):
        logger.info('-------Prepare assets ledger lines ----------')
        depreciation_lines = {}
        acc_depreciation_lines = {}
        fixed_assets = self.assets.keys()

        assets_locations = get_assets_locations(fixed_assets=fixed_assets)
        assets_object_splits = get_assets_objects_splits(fixed_assets=fixed_assets)

        for fixed_asset, depreciation in self.assets.items():
            depreciation = depreciation or 0
            asset_locations = assets_locations.get(fixed_asset, [])
            asset_object_splits = assets_object_splits.get(fixed_asset, [])

            fixed_asset_lines = get_fixed_assets_postings(
                fixed_asset=fixed_asset,
                depreciation=depreciation,
                locations=asset_locations,
                object_splits=asset_object_splits
            )
            depreciation_amount = 0
            for fixed_asset_line in fixed_asset_lines:
                depreciation_account = fixed_asset.depreciation_account
                line_amount = fixed_asset_line.amount
                depreciation_amount += fixed_asset_line.amount
                account_line = "_".join([str(depreciation_account.id), str(fixed_asset_line)])
                if account_line in depreciation_lines:
                    depreciation_lines[account_line]['debit'] += line_amount
                    depreciation_lines[account_line]['lines'].append(fixed_asset_line)
                else:
                    depreciation_lines[account_line] = {
                        'debit': line_amount,
                        'credit': 0,
                        'account': depreciation_account,
                        'object_items': fixed_asset_line.object_items,
                        'branch': fixed_asset_line.branch,
                        'lines': [fixed_asset_line]
                    }

            accumulated_depreciation_account = fixed_asset.accumulated_depreciation_account
            if accumulated_depreciation_account in acc_depreciation_lines:
                acc_depreciation_lines[accumulated_depreciation_account]['credit'] += depreciation_amount
                acc_depreciation_lines[accumulated_depreciation_account]['lines'].append(fixed_asset)
            else:
                acc_depreciation_lines[accumulated_depreciation_account] = {
                    'credit': depreciation_amount,
                    'debit': 0,
                    'lines': [fixed_asset]
                }

        return depreciation_lines, acc_depreciation_lines

    def execute(self):
        logger.info('-------Execute ledger----------')
        depreciation_lines, acc_depreciation_lines = self.prepare_ledger_accounts()

        self.prepare_depreciation_ledger_lines(depreciation_lines=depreciation_lines)
        self.prepare_accumulated_depreciation_ledger_lines(depreciation_lines=acc_depreciation_lines)

        self.create_ledger()

    def create_ledger(self):
        ledger = CreateLedger(
            company=self.update.company,
            year=self.update.period.period_year,
            period=self.update.period,
            profile=self.update.created_by,
            role=self.update.role,
            text=f"Depreciation - Report {self.update.journal_id}",
            journal_lines=self.ledger_lines,
            date=self.date,
            module=Module.FIXED_ASSET
        )
        ledger = ledger.execute()
        if ledger:
            self.update.ledger = ledger
            self.update.save()
        return ledger

    def prepare_depreciation_ledger_lines(self, depreciation_lines):
        for line_key, depreciation_lines in depreciation_lines.items():
            account = depreciation_lines.get('account')
            self.ledger_lines.append({
                'account': account,
                'credit': depreciation_lines.get('credit', 0),
                'debit': depreciation_lines.get('debit', 0),
                'branch': depreciation_lines.get('branch', 0),
                'text': '',
                'accounting_date': self.date,
                'object_items': depreciation_lines.get('object_items', [])
            })

    def prepare_accumulated_depreciation_ledger_lines(self, depreciation_lines):
        for account, depreciation_lines in depreciation_lines.items():
            self.ledger_lines.append({
                'account': account,
                'credit': depreciation_lines.get('credit', 0),
                'debit': depreciation_lines.get('debit', 0),
                'text': '',
                'accounting_date': self.date,
                'object_items': depreciation_lines.get('object_items', [])
            })


class CalculateAssetDepreciation:

    def __init__(self, fixed_asset, net_book_value, closing_net_book_value, closing_balance):
        self.fixed_asset = fixed_asset
        self.net_book_value = net_book_value
        self.closing_net_book_value = closing_net_book_value
        self.closing_balance = closing_balance

    def get_depreciation_calculator(self):
        depreciation_rate = self.fixed_asset.depreciation_rate
        number_of_periods = self.fixed_asset.year.number_of_periods
        if self.fixed_asset.depreciation_rate:
            if self.net_book_value > 1:
                if self.fixed_asset.is_reducing_balance():
                    return ReducingBalance(depreciation_rate, self.closing_net_book_value, self.net_book_value, number_of_periods)
                elif self.fixed_asset.is_straight_line():
                    return StraightLine(depreciation_rate, self.closing_balance, self.net_book_value, number_of_periods)
                elif self.fixed_asset.is_section_12_e():
                    return SectionE(depreciation_rate, self.closing_net_book_value, number_of_periods)
        else:
            if self.fixed_asset.is_section_12_e():
                return SectionE(depreciation_rate, self.closing_net_book_value, number_of_periods)
        return None

    def execute(self):
        depreciation = 0

        depreciation_calculator = self.get_depreciation_calculator()
        if depreciation_calculator:
            depreciation = depreciation_calculator.calculate()
        return round(depreciation, 2)


class DepreciationCalculator:

    def __init__(self, depreciation_rate, closing_book_value, net_book_value, number_of_periods):
        self.closing_net_book_value = closing_book_value
        self.net_book_value = net_book_value
        self.depreciation_rate = depreciation_rate
        self.number_of_periods = number_of_periods

    def get_rate(self):
        return self.depreciation_rate / 100

    def calculate(self):
        raise NotImplementedError('Not implemented')


class ReducingBalance(DepreciationCalculator):

    def calculate(self):
        depreciation = (self.closing_net_book_value * self.get_rate()) / self.number_of_periods
        if depreciation < self.net_book_value:
            return depreciation
        else:
            return self.net_book_value - 1


class StraightLine:

    def __init__(self, depreciation_rate, closing_balance, net_book_value, number_of_periods):
        self.closing_balance = closing_balance
        self.net_book_value = net_book_value
        self.depreciation_rate = depreciation_rate
        self.number_of_periods = number_of_periods

    def get_rate(self):
        return self.depreciation_rate/100

    def calculate(self):
        depreciation = (self.closing_balance * self.get_rate()) / self.number_of_periods
        if depreciation < self.net_book_value:
            return depreciation
        else:
            return self.net_book_value - 1


class SectionE(DepreciationCalculator):

    def calculate(self):
        if self.closing_net_book_value > 0:
            return float(self.closing_net_book_value)
        else:
            return 0
