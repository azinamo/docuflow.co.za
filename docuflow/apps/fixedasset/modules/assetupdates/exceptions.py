from docuflow.apps.fixedasset.exceptions import FixedAssetsException


class NotFixedAssetsFound(FixedAssetsException):
    pass
