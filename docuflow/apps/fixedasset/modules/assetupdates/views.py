import logging
import pprint

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import HttpResponse, JsonResponse
from django.utils.timezone import now
from django.views.generic import CreateView, TemplateView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.fixedasset.exceptions import FixedAssetsException, IncompleteFixedAssetAccounts, NotFixedAssetsFound
from docuflow.apps.fixedasset.models import Update, FixedAssetUpdate
from docuflow.apps.journals.exceptions import JournalNotBalancingException
from docuflow.apps.period.models import Period
from .forms import UpdateDepreciationForm

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class FixedAssetUpdateView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'assetupdates/journal.html'

    def get_journal(self):
        return Update.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(FixedAssetUpdateView, self).get_context_data(**kwargs)
        company = self.get_company()
        journal = self.get_journal()
        context['company'] = company
        context['journal'] = journal
        context['year'] = self.get_year()

        return context


class CreateFixedAssetUpdateView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = 'assetupdates/create.html'
    form_class = UpdateDepreciationForm

    def get_initial(self):
        initial = super(CreateFixedAssetUpdateView, self).get_initial()
        initial['date'] = now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateFixedAssetUpdateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateFixedAssetUpdateView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['year'] = self.get_year()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the depreciation updates, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateFixedAssetUpdateView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                with transaction.atomic():

                    form.save()
                    return JsonResponse({
                        'error': False,
                        'text': 'Journal for fixed assets successfully created',
                        'reload': True
                    })
            except (NotFixedAssetsFound, IncompleteFixedAssetAccounts, FixedAssetsException, JournalNotBalancingException) as exception:
                return JsonResponse({
                    'error': True,
                    'text': str(exception)
                })
        return HttpResponse('Not allowed')


class UpdateFixedAssetIndexView(ProfileMixin, LoginRequiredMixin, TemplateView):
    template_name = 'assetupdates/index.html'

    def get_updates(self, company, year):
        return Update.objects.select_related('period', 'role').filter(
            company=company,
            period__period_year=year
        )

    def get_periods(self, company, year, journals):
        return Period.objects.open(company=company, year=year).exclude(
            id__in=[journal.period.id for journal in journals]
        ).order_by('period')

    def get_context_data(self, **kwargs):
        context = super(UpdateFixedAssetIndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        updates = self.get_updates(company=company, year=year)
        context['company'] = company
        context['year'] = year
        context['updates'] = updates
        context['periods'] = self.get_periods(company=company, year=year, journals=updates)
        return context


class FixedAssetUpdateDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'assetupdates/detail.html'

    def get_assets(self, journal, year):
        fixed_assets = self.get_journal_fixed_assets(journal)
        assets = []
        total_opening_assets_value = 0
        total_opening_depreciation_value = 0
        total_net_book_value = 0
        total_updated_depreciation = 0
        total_closing_depreciation = 0
        for journal_asset in fixed_assets:
            asset = {}
            fixed_asset = journal_asset.fixed_asset
            asset['id'] = fixed_asset.id
            asset['asset'] = fixed_asset
            asset['name'] = fixed_asset.name
            asset['opening_balance'] = 0
            asset['depreciation_value'] = 0

            year_depreciation = fixed_asset.calculate_total_depreciation()
            asset_additions = fixed_asset.get_total_additions(year)
            asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
            year_depreciation = year_depreciation - journal_asset.depreciation_value

            opening_asset_balance = 0
            opening_depreciation_balance = 0

            if fixed_asset.opening_asset_value:
                opening_asset_balance = fixed_asset.opening_asset_value
            if fixed_asset.opening_depreciation_value:
                opening_depreciation_balance = fixed_asset.opening_depreciation_value

            asset_closing_balance = opening_asset_balance + asset_additions - asset_disposals
            depreciation_closing_balance = opening_depreciation_balance - depreciation_disposal + year_depreciation

            net_book_value = asset_closing_balance - depreciation_closing_balance
            opening_net_book_value = opening_asset_balance - opening_depreciation_balance

            asset['opening_asset_value'] = fixed_asset.opening_asset_value
            asset['opening_depreciation_value'] = fixed_asset.opening_depreciation_value
            asset['net_book_value'] = net_book_value
            asset['opening_net_book_value'] = opening_net_book_value
            asset['closing_balance'] = asset_closing_balance
            asset['closing_depreciation'] = depreciation_closing_balance
            asset['last_depreciation_date'] = journal_asset.created_at
            asset['depreciation'] = journal_asset.depreciation_value
            asset['period'] = journal_asset.period

            total_updated_depreciation += journal_asset.depreciation_value
            total_closing_depreciation += journal_asset.depreciation_value
            if fixed_asset.opening_asset_value:
                total_opening_assets_value += fixed_asset.opening_asset_value
            if fixed_asset.opening_depreciation_value:
                total_opening_depreciation_value += fixed_asset.opening_depreciation_value
                total_closing_depreciation += fixed_asset.opening_depreciation_value

            total_net_book_value += net_book_value
            assets.append(asset)

        return {'assets': assets,
                'total_depreciation': total_opening_depreciation_value,
                'total_opening_depreciation': total_opening_depreciation_value,
                'total_updated_depreciation': total_updated_depreciation,
                'total_closing_depreciation': total_closing_depreciation,
                'assets_value': total_opening_assets_value,
                'total_net_book_value': total_net_book_value
                }

    def get_journal_update(self):
        return Update.objects.select_related('period', 'role', 'created_by', 'ledger').get(pk=self.kwargs['pk'])

    def get_journal_fixed_assets(self, asset_update):
        return FixedAssetUpdate.objects.select_related(
            'period', 'fixed_asset', 'fixed_asset__asset_account',
            'fixed_asset__accumulated_depreciation_account', 'fixed_asset__depreciation_account'
        ).prefetch_related(
            'fixed_asset__depreciation_adjustments', 'fixed_asset__fixed_asset_disposal',
            'fixed_asset__depreciation_updates'
        ).filter(
            asset_update_id=asset_update.id
        ).order_by('period__period')

    def get_context_data(self, **kwargs):
        context = super(FixedAssetUpdateDetailView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        asset_update = self.get_journal_update()
        # journal_fixed_assets = self.get_journal_fixed_assets(asset_update)
        assets = self.get_assets(asset_update, year)
        context['company'] = company
        context['year'] = year
        context['journal'] = asset_update
        context['updates'] = assets
        return context
