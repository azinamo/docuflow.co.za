import pprint
import logging

from django.http.response import JsonResponse
from django.views.generic import View, CreateView, ListView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.fixedasset.models import FixedAsset, Addition
from .forms import AssetAdditionForm

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class FixedAssetAdditionView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Addition
    context_object_name = 'additions'
    template_name = 'assetaddition/index.html'

    def get_queryset(self):
        return super().get_queryset().select_related(
            'fixed_asset', 'invoice_account', 'journal_line'
        ).filter(fixed_asset=self.kwargs['fixed_asset_id'])


class CreateFixedAssetAdditionView(ProfileMixin, CreateView):
    model = Addition
    form_class = AssetAdditionForm
    template_name = 'assetaddition/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fixed_asset'] = FixedAsset.objects.filter(pk=self.kwargs['fixed_asset_id']).first()
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['fixed_asset'] = FixedAsset.objects.filter(pk=self.kwargs['fixed_asset_id']).first()
        return kwargs

    def form_invalid(self, form):
        return JsonResponse({'error': True,
                             'text': 'Fixed asset addition could not be saved, please fix the errors.',
                             'errors': form.errors
                             }, status=400)

    def form_valid(self, form):
        form.save(commit=False)
        return JsonResponse({
            'error': False,
            'text': 'Fixed asset addition added.',
            'reload': True
        })


class EditFixedAssetAdditionView(ProfileMixin, UpdateView):
    model = Addition
    form_class = AssetAdditionForm
    template_name = 'assetaddition/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fixed_asset'] = FixedAsset.objects.filter(pk=self.kwargs['fixed_asset_id']).first()
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['fixed_asset'] = FixedAsset.objects.filter(pk=self.kwargs['fixed_asset_id']).first()
        return kwargs

    def form_invalid(self, form):
        return JsonResponse({
            'error': True,
            'text': 'Fixed asset addition could not be saved, please fix the errors.',
            'errors': form.errors
        }, status=400)

    def form_valid(self, form):
        form.save(commit=False)
        return JsonResponse({
            'error': False,
            'text': 'Fixed asset addition saved.',
            'reload': True
        })
