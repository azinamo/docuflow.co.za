from django.apps import AppConfig


class AssetadditionConfig(AppConfig):
    name = 'docuflow.apps.fixedasset.modules.assetaddition'
