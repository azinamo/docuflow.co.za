from django.urls import path

from . import views

urlpatterns = [
    path('', views.FixedAssetAdditionView.as_view(), name='fixed_asset_additions'),
    path('create/', views.CreateFixedAssetAdditionView.as_view(), name='create_fixed_asset_addition'),
    path('edit/<int:pk>/', views.EditFixedAssetAdditionView.as_view(), name='edit_fixed_asset_addition'),
]
