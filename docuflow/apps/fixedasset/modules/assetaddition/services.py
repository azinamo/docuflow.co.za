import logging
from decimal import Decimal
from datetime import datetime, timedelta

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.fixedasset.models import FixedAsset, Update, FixedAssetUpdate, Disposal, FixedAssetDisposal, DepreciationAdjustment, \
    FixedAssetAdjustment
from docuflow.apps.sales.models import Invoice, InvoiceItem, Receipt, ReceiptPayment, InvoiceReceipt
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.journals.models import Journal, JournalLine
from docuflow.apps.customer.models import Ledger as CustomerLedger, Customer

logger = logging.getLogger(__name__)


class CreateDisposal:

    def __init__(self, company, branch, year, profile, role, fixed_assets, disposal_data):
        logger.info("start create disposal")
        self.company = company
        self.branch = branch
        self.year = year
        self.profile = profile
        self.role = role
        self.fixed_assets = fixed_assets
        self.disposal_data = disposal_data

    def execute(self):
        disposal = self.create()
        logger.info(f"Saved disposal --> {disposal}")
        if disposal:
            assets_disposals = self.create_fixed_asset_disposals(disposal)
            invoice = self.create_sales_invoice(disposal, assets_disposals)
            if invoice:
                disposal.invoice_number = str(invoice)
                disposal.save()
        return disposal

    def create_fixed_asset_disposals(self, disposal):
        assets = self.fixed_assets
        logger.info("Dispose assets --> {}".format(assets))
        assets_disposals = {}
        for k, asset in assets['assets'].items():
            fixed_asset = asset['asset']
            if fixed_asset:
                assets_disposals[fixed_asset.id] = FixedAssetDisposal.objects.create(
                    disposal=disposal,
                    fixed_asset=fixed_asset,
                    book_value=asset['net_book_value'],
                    asset_value=asset['closing_balance'],
                    depreciation_value=asset['closing_depreciation']
                )
                fixed_asset.status = FixedAsset.PENDING_UPDATE
                fixed_asset.save()
        return assets_disposals

    def calculate_price_excluding(self, disposal):
        selling_price = float(disposal.selling_price)
        return selling_price - (disposal.vat_code.percentage/100) * selling_price

    def calculate_vat_amount(self, disposal):
        return disposal.vat_code.percentage / 100

    def create(self):
        disposal = Disposal()
        disposal.company = self.company
        disposal.period_id = self.disposal_data['period']
        disposal.selling_price = float(self.disposal_data['total_amount'])
        disposal.net_profit_loss = float(self.disposal_data['net_profit_loss'])
        disposal.disposal_date = self.disposal_data.get('invoice_date', datetime.now().date())
        disposal.role = self.role
        disposal.save()
        return disposal

    def last_depreciation_date(self, year, company):
        return None

    def prepare_invoice_data(self):
        data = {}
        invoice_data = self.disposal_data.split('&')
        logger.info("Invoice values")
        logger.info(invoice_data)
        for invoice_row in invoice_data:
            row_data = invoice_row.split('=')
            if len(row_data) > 1:
                if row_data[0] == 'payment_method':
                    if row_data[0] in data:
                        data[row_data[0]].append(row_data[1])
                    else:
                        data[row_data[0]] = [row_data[1]]
                else:
                    data[row_data[0]] = row_data[1]
        logger.info(invoice_data)
        return data

    def get_invoice_type(self):
        invoice_type = InvoiceType.objects.sales_tax_invoice(self.company).first()
        if not invoice_type:
            raise ValueError('Tax invoice type for the sales module not found')
        return invoice_type

    def create_sales_invoice(self, disposal, assets_disposals):
        if len(self.disposal_data) > 0:
            invoice_type = self.get_invoice_type()
            total_amount = Decimal(self.disposal_data.get('total_amount', 0))
            logger.info(f"Start create {invoice_type} invoice for {total_amount}")
            logger.info(self.disposal_data)
            invoice = Invoice()
            invoice.invoice_type = invoice_type
            invoice.branch = self.branch
            invoice.disposal = disposal
            invoice.total_amount = total_amount
            invoice.open_amount = total_amount
            invoice.sub_total = Decimal(self.disposal_data.get('sub_total', 0))
            invoice.net_amount = Decimal(self.disposal_data.get('net_amount', 0))
            invoice.vat_amount = Decimal(self.disposal_data.get('vat_amount', 0))
            if self.disposal_data.get('salesman', False):
                invoice.salesman_id = self.disposal_data['salesman']
            invoice.customer_id = self.disposal_data['customer']
            invoice.period_id = self.disposal_data['period']
            invoice.vat_number = self.disposal_data['vat_number']
            invoice.customer_name = self.disposal_data.get('customer_name', '')
            invoice.invoice_date = disposal.disposal_date
            due_date = self.disposal_data.get('date', None)
            logger.info("Due date {}".format(due_date))
            if due_date:
                invoice.due_date = due_date
            invoice.save()
            if invoice:
                logger.info("--- Invoice saved. Create customer ledger now ---")
                logger.info(f"Customer is default --> {invoice.customer.is_default}")
                if invoice.customer.is_default:
                    customer = Customer.objects.create_cash_customer(invoice.branch.company, invoice.customer_name)
                    if customer:
                        invoice.customer = customer
                        invoice.save()

                    address_line_1 = self.disposal_data.get('postal_street', None)
                    logger.info(f"Invoice should have postal address {address_line_1}")
                    postal_address = {}
                    delivery_address = {}
                    if address_line_1:
                        province = self.disposal_data.get('postal_province', None)
                        logger.info(f"Province {province}")
                        postal_address['street'] = address_line_1
                        postal_address['postal_code'] = self.disposal_data.get('postal_postal_code', '')
                        postal_address['country'] = self.disposal_data.get('postal_country', '')
                        if province:
                            postal_address['province'] = province
                        postal_address['city'] = self.disposal_data.get('postal_city', '')
                        invoice.postal = postal_address
                        logger.info(f"Save invoice address object {postal_address}")

                    address_line_1 = self.disposal_data.get('delivery_street', None)
                    logger.info(f"Invoice should have delivery address {address_line_1}")
                    if address_line_1:
                        province = self.disposal_data.get('delivery_province', None)
                        logger.info("Province {}".format(province))
                        delivery_address['street'] = address_line_1
                        delivery_address['postal_code'] = self.disposal_data.get('delivery_postal_code', '')
                        delivery_address['country'] = self.disposal_data.get('delivery_country', None)
                        if province:
                            delivery_address['province'] = province
                        delivery_address['city'] = self.disposal_data.get('delivery_city', None)
                        invoice.delivery_to = delivery_address
                        logger.info("Save invoice address object {}".format(delivery_address))

                    content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()

                    receipt = Receipt()
                    receipt.branch = self.branch
                    receipt.reference = str(invoice)
                    receipt.amount = invoice.total_amount
                    receipt.date = disposal.disposal_date
                    receipt.period = disposal.period
                    receipt.customer = invoice.customer
                    if content_type:
                        receipt.content_type = content_type
                        receipt.object_id = invoice.id
                    receipt.save()

                    if receipt:
                        InvoiceReceipt.objects.create(
                            invoice=invoice,
                            receipt=receipt,
                            amount=invoice.total_amount
                        )
                        open_amount = invoice.calculate_open_amount(invoice.total_amount)
                        invoice.open_amount = open_amount
                        if invoice.open_amount == 0:
                            invoice.mark_as_paid_not_printed()
                        else:
                            invoice.mark_as_partially_paid()
                        invoice.save()

                        if total_amount != 0:
                            payment_method = self.disposal_data.get('payment_method', None)
                            if payment_method is None:
                                raise ValueError('Payment is not specified')

                            for payment_method_id in payment_method:
                                logger.info("K->{}".format(payment_method_id))
                                amount = self.disposal_data.get('amount_{}'.format(payment_method_id), 0)
                                logger.info("Payment method id {} -- amount {}".format(payment_method_id, amount))
                                if amount and amount != '':
                                    ReceiptPayment.objects.create(
                                        amount=Decimal(amount),
                                        receipt=receipt,
                                        payment_method_id=payment_method_id
                                    )
                else:
                    if not invoice.due_date:
                        if invoice.customer.days and invoice.customer.payment_term:
                            if invoice.customer.payment_term == 1:
                                due_date = datetime.now() + timedelta(days=invoice.customer.days)
                            elif invoice.customer.payment_term == 2:
                                due_date = datetime.now() + timedelta(days=invoice.customer.days)
                            else:
                                due_date = datetime.now() + timedelta(days=invoice.customer.days)
                        else:
                            due_date = datetime.now() + timedelta(days=30)
                        invoice.due_date = due_date.date()
                        invoice.save()

                CustomerLedger.objects.create_customer_ledger(invoice, self.profile, True)

                for asset_id, asset in self.fixed_assets['assets'].items():
                    logger.info("{} --> {}".format(asset_id, asset))
                    fixed_asset = asset['asset']
                    price = self.disposal_data.get(f"price_{asset_id}", 0)
                    invoice_item = InvoiceItem()
                    invoice_item.description = self.disposal_data.get('description_{}'.format(asset_id), fixed_asset.name)
                    invoice_item.price = float(self.disposal_data.get('price_{}'.format(asset_id), 0))
                    invoice_item.net_price = float(self.disposal_data.get('total_price_excluding_{}'.format(asset_id), 0))
                    invoice_item.total_amount = float(self.disposal_data.get('total_price_including_{}'.format(asset_id), 0))
                    vat_code_id = self.disposal_data.get('vat_code_{}'.format(asset_id), None)
                    if vat_code_id:
                        invoice_item.vat_code_id = float(vat_code_id)
                        logger.info("Vat Code {}".format(invoice_item.vat_code_id))
                    vat_percentage = self.disposal_data.get('vat_percentage_{}'.format(asset_id), 0)
                    if vat_percentage != '':
                        invoice_item.vat = float(vat_percentage)
                    invoice_item.quantity = fixed_asset.quantity
                    invoice_item.invoice = invoice
                    invoice_item.account_id = self.disposal_data.get('account_id_{}'.format(asset_id), 0)
                    invoice_item.set_as_general_line()
                    invoice_item.save()

                    fixed_asset.invoice = invoice
                    fixed_asset.save()

                    asset_disposal = assets_disposals.get(asset_id)
                    if asset_disposal:
                        asset_disposal.invoice_item = invoice_item
                        asset_disposal.save()

            return invoice


class InvoiceDisposalLedger:

    def __init__(self, disposal, year, ledger):
        self.disposal = disposal
        self.year = year
        self.ledger = ledger

    def get_fixed_assets(self):
        return {}

    def execute(self):
        logger.info(f"Sales Journal created --> {self.ledger}")
        for disposed_asset in self.disposal.disposed_fixed_assets:
            self.create_asset_journal(disposed_asset.fixed_asset, self.ledger, disposed_asset.closing_balance)
            self.create_depreciation_journal(disposed_asset.fixed_asset, self.ledger, disposed_asset.closing_depreciation)
            line = JournalLine.objects.create(
                journal=self.ledger,
                account=self.ledger.company.default_profit_on_asset_account,
                debit=disposed_asset.net_book_value,
                text=f"{disposed_asset.fixed_asset} NBV",
                accounting_date=self.ledger.date,
                suffix='Disposals'
            )
            if line:
                for object_item in disposed_asset.fixed_asset.object_items.all():
                    line.object_items.add(object_item)

    def create_asset_journal(self, fixed_asset, journal, amount):
        logger.info(f"Create assets journal line --> {fixed_asset} --> {amount}")
        if fixed_asset.asset_account:
            line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.asset_account,
                credit=amount,
                text=f"{str(fixed_asset)}",
                accounting_date=journal.date,
                suffix='Disposals'
            )
        else:
            line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.category.account,
                credit=amount,
                text=f"{str(fixed_asset)}",
                accounting_date=journal.date,
                suffix='Disposals'
            )
        if line:
            for object_item in fixed_asset.object_items.all():
                line.object_items.add(object_item)

    def create_depreciation_journal(self, fixed_asset, journal, amount):
        logger.info(f"Create depreciation journal line --> {fixed_asset} --> {amount}")
        if fixed_asset.asset_account:
            line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.category.accumulated_depreciation_account,
                debit=amount,
                text=f"{str(fixed_asset)}",
                accounting_date=journal.date,
                suffix='Current Year Depreciation'
            )
        else:
            line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.category.accumulated_depreciation_account,
                debit=amount,
                text=f"{str(fixed_asset)}",
                accounting_date=journal.date,
                suffix='Current Year Depreciation'
            )
        if line:
            for object_item in fixed_asset.object_items.all():
                line.object_items.add(object_item)
