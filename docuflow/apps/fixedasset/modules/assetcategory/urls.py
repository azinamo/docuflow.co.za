from django.urls import path

from . import views

urlpatterns = [
    path('', views.AssetCategoryView.as_view(), name='asset_category_index'),
    path('create/', views.CreateAssetCategoryView.as_view(), name='create_asset_category'),
    path('<int:pk>/edit/', views.EditAssetCategoryView.as_view(), name='edit_asset_category'),
    path('<int:pk>/delete/', views.DeleteAssetCategoryView.as_view(), name='delete_asset_category'),
]
