from django.apps import AppConfig


class AssetcategoryConfig(AppConfig):
    name = 'docuflow.apps.fixedasset.modules.assetcategory'
