from django.db import models
from safedelete.models import SafeDeleteModel, SOFT_DELETE


class AssetCategory(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    STRAIGHT_LINE = 1
    REDUCING_BALANCE = 2
    SECTION_12_B = 3
    SECTION_12_E = 4

    DEPRECIATION_METHODS = (
        (STRAIGHT_LINE, 'Straight Line'),
        (REDUCING_BALANCE, 'Reducing Balance'),
        (SECTION_12_B, 'S12B'),
        (SECTION_12_E, 'S12E'),
    )

    company = models.ForeignKey(
        'company.Company',
        null=False,
        blank=False,
        related_name='asset_categories',
        on_delete=models.DO_NOTHING
    )
    name = models.CharField(
        max_length=255,
        null=False,
        blank=False
    )
    account = models.ForeignKey(
        'company.Account',
        null=False,
        blank=False,
        related_name='categories',
        verbose_name='Asset',
        on_delete=models.DO_NOTHING
    )
    depreciation_method = models.PositiveIntegerField(
        choices=DEPRECIATION_METHODS,
        blank=False,
        null=False
    )
    depreciation_account = models.ForeignKey(
        'company.Account',
        null=False,
        blank=False,
        related_name='depreciation_categories',
        on_delete=models.DO_NOTHING
    )
    depreciation_rate = models.DecimalField(
        null=True,
        blank=True,
        decimal_places=2,
        max_digits=12,
        verbose_name='Depreciation Rate (%)',
    )
    accumulated_depreciation_account = models.ForeignKey(
        'company.Account',
        null=True,
        blank=True,
        verbose_name='Accumulated Depreciation',
        related_name='accumulated_depreciation_categories',
        on_delete=models.DO_NOTHING
    )

    is_active = models.BooleanField(
        default=True
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

    @property
    def method_name(self):
        for method in self.DEPRECIATION_METHODS:
            if method[0] == self.depreciation_method:
                return method[1]
        return self.DEPRECIATION_METHODS[self.depreciation_method][1]

    @property
    def depreciation_rate_percentage(self):
        return "{}%".format(self.depreciation_rate)

    @property
    def is_section_b(self):
        return self.depreciation_method == self.SECTION_12_B

    @property
    def is_section_c(self):
        return self.depreciation_method == self.SECTION_12_E
