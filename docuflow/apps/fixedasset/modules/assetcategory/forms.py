from django import forms

from docuflow.apps.fixedasset.models import Category


# Create your forms here.
class AssetCategoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(AssetCategoryForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.company)
        self.fields['depreciation_account'].queryset = self.fields['depreciation_account'].queryset.filter(
            company=self.company)
        self.fields['accumulated_depreciation_account'].queryset = self.fields['accumulated_depreciation_account'].\
            queryset.filter(company=self.company)

    class Meta:
        model = Category
        fields = ['name', 'account', 'depreciation_method', 'depreciation_account', 'depreciation_rate',
                  'accumulated_depreciation_account', 'category']
        widgets = {
            'account': forms.Select(attrs={'class': 'chosen-select', 'label': 'Asset'}),
            'depreciation_account': forms.Select(attrs={'class': 'chosen-select'}),
            'accumulated_depreciation_account': forms.Select(attrs={'class': 'chosen-select'})
        }

    def clean(self):
        cleaned_data = super(AssetCategoryForm, self).clean()

        if not self.cleaned_data['category']:
            self.add_error('category', 'Category is required')

        return cleaned_data

    def save(self, commit=True):
        category = super().save(commit=False)
        category.company = self.company
        category.save()

        return category
