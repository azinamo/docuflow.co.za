import pprint

from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as __
from django.views.generic import ListView, View
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.fixedasset.models import Category
from .forms import AssetCategoryForm

pp = pprint.PrettyPrinter(indent=4)


class AssetCategoryView(ProfileMixin, LoginRequiredMixin, ListView):
    model = Category
    context_object_name = 'categories'
    template_name = 'assetcategory/index.html'

    def get_queryset(self):
        return super().get_queryset().select_related(
            'account', 'depreciation_account', 'accumulated_depreciation_account'
        ).filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(AssetCategoryView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class CreateAssetCategoryView(ProfileMixin, LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Category
    template_name = 'assetcategory/create.html'
    success_message = __('Asset category successfully saved')
    success_url = reverse_lazy('asset_category_index')
    form_class = AssetCategoryForm

    def get_form_kwargs(self):
        kwargs = super(CreateAssetCategoryView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class EditAssetCategoryView(ProfileMixin, LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Category
    template_name = 'assetcategory/edit.html'
    success_message = __('Asset category successfully updated')
    form_class = AssetCategoryForm
    success_url = reverse_lazy('asset_category_index')

    def get_form_kwargs(self):
        kwargs = super(EditAssetCategoryView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs


class DeleteAssetCategoryView(LoginRequiredMixin, SuccessMessageMixin, View):
    model = Category

    def get(self, request, *args, **kwargs):
        asset_category = get_object_or_None(Category, id=self.kwargs['pk'])
        if asset_category:
            asset_category.delete()
            messages.success(self.request, __('Asset category deleted successfully'))

        return redirect(reverse('asset_category_index'))


class UpdateDefaultAssetCategoryView(ProfileMixin, LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        asset_categories = Category.objects.filter(company=self.get_company()).all()
        for asset_category in asset_categories:
            if asset_category.id == int(self.kwargs['pk']):
                asset_category.is_default = True
            else:
                asset_category.is_default = False
            asset_category.save()
        return JsonResponse({
            'text': __('Asset category updated successfully'),
            'error': False
        })
