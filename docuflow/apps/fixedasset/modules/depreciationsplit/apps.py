from django.apps import AppConfig


class AssetobjectdepreciationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'docuflow.apps.fixedasset.modules.depreciationsplit'
