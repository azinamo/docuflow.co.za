from django.urls import path

from . import views

urlpatterns = [
    path('', views.DepreciationSplitView.as_view(), name='fixed_asset_object_depreciation'),
    path('create/', views.CreateDepreciationSplitView.as_view(), name='create_fixed_asset_object_depreciation'),
    path('line/', views.AddDepreciationSplitView.as_view(), name='add_object_depreciation_line'),
    path('save/', views.SaveDepreciationSplitView.as_view(), name='save_fixed_asset_object_depreciation'),
    path('<int:parent_id>/create/', views.CreateDepreciationSplitView.as_view(), name='create_child_depreciation_split'),
    path('<int:pk>/delete/', views.DeleteDepreciationSplitView.as_view(), name='delete_depreciation_split'),

]
