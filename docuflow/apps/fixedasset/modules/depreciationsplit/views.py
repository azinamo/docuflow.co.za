import logging
import pprint
from decimal import Decimal

from django.db.models import Sum
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import JsonResponse
from django.views.generic import View, ListView, TemplateView, DeleteView
from django.urls import reverse

from docuflow.apps.accountposting.models import ObjectPosting
from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import ObjectItem
from docuflow.apps.fixedasset.models import FixedAsset, DepreciationSplit

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class DepreciationSplitView(LoginRequiredMixin, ProfileMixin, ListView):
    model = DepreciationSplit
    context_object_name = 'depreciation_splits'
    template_name = 'depreciationsplit/index.html'

    def get_queryset(self):
        return super().get_queryset().select_related(
            'object_item', 'object_posting'
        ).prefetch_related(
            'children', 'children__object_item'
        ).filter(fixed_asset=self.kwargs['fixed_asset_id'], parent__isnull=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fixed_asset'] = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        return context


class CreateDepreciationSplitView(ProfileMixin, TemplateView):
    template_name = 'depreciationsplit/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fixed_asset = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        context['fixed_asset'] = fixed_asset
        splits = DepreciationSplit.objects.select_related('object_item').filter(fixed_asset=fixed_asset)
        parent_id = self.kwargs.get('parent_id')
        if parent_id:
            splits = splits.filter(parent_id=parent_id)
        context['depreciation_splits'] = splits
        context['row'] = 1
        context['parent_id'] = parent_id
        context['query_str'] = f'parent_id={parent_id}' if parent_id else ''
        return context


class CreateChildDepreciationSplitView(ProfileMixin, TemplateView):
    template_name = 'depreciationsplit/create_child.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fixed_asset = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        context['fixed_asset'] = fixed_asset
        context['depreciation_splits'] = DepreciationSplit.objects.select_related('object_item').filter(
            fixed_asset=fixed_asset, parent=self.kwargs['split_id']
        )
        context['row'] = 1
        return context


class AddDepreciationSplitView(ProfileMixin, TemplateView):
    template_name = 'depreciationsplit/split_line.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fixed_asset = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        context['fixed_asset'] = fixed_asset
        qs = DepreciationSplit.objects.select_related('object_item').filter(fixed_asset=fixed_asset)
        parent_split = None
        parent_id = self.request.GET.get('parent_id')
        object_items = ObjectItem.objects.for_fixed_assets().filter(company_object__company=self.get_company())

        object_postings = ObjectPosting.objects.filter(company_object__company=self.get_company())

        if parent_id:
            parent_split = DepreciationSplit.objects.select_related(
                'object_item', 'object_item__company_object'
            ).filter(pk=parent_id).first()

            qs = qs.filter(parent_id=parent_id)
            if parent_split.object_item:
                object_items = object_items.filter(company_object__parent=parent_split.object_item.company_object)
            else:
                object_items = object_items.filter(company_object__parent__isnull=False)

            object_postings = object_postings.filter(company_object__parent__isnull=False)
        else:
            object_items = object_items.filter(company_object__parent__isnull=True)
            object_postings = object_postings.filter(company_object__parent__isnull=True)

        context['depreciation_splits'] = qs
        context['parent_id'] = parent_id
        context['parent_split'] = parent_split
        context['object_items'] = object_items
        context['object_postings'] = object_postings
        context['row'] = self.request.GET.get('row')
        return context


class SaveDepreciationSplitView(ProfileMixin, View):

    def get_object_items(self):
        return {
            str(object_item.id): object_item
            for object_item in ObjectItem.objects.for_fixed_assets().filter(company_object__company=self.get_company())
        }

    def get_object_postings(self):
        return {
            str(object_posting.id): object_posting
            for object_posting in ObjectPosting.objects.filter(company_object__company=self.get_company())
        }

    def get_item(self, row_id, object_items, object_postings):
        item_row = self.request.POST[f'depreciation_split_item_{row_id}']
        object_posting = None
        object_item = None
        if item_row.startswith('object_posting', 0, 14):
            object_posting = object_postings.get(item_row[15:], None)
        elif item_row.startswith('object_item', 0, 11):
            object_item = object_items.get(item_row[12:], None)
        return object_item, object_posting

    def post(self, *args, **kwargs):
        fixed_asset = FixedAsset.objects.get(pk=kwargs['fixed_asset_id'])

        parent_id = self.request.GET.get('parent_id')

        object_items = self.get_object_items()
        object_postings = self.get_object_postings()

        depreciation_splits = []
        splits = DepreciationSplit.objects.filter(fixed_asset=fixed_asset, parent_id=parent_id).aggregate(total=Sum('percentage'))
        total = splits['total'] if splits and splits['total'] else 0

        for field, value in self.request.POST.items():
            if field.startswith('depreciation_split_item', 0, 23):
                row = field[24:]
                object_item, object_posting = self.get_item(
                    row_id=row,
                    object_items=object_items,
                    object_postings=object_postings
                )

                if not (object_item or object_posting):
                    continue

                percentage = self.request.POST[f'depreciation_percentage_split_{row}']
                if not percentage or percentage == '':
                    continue

                percentage_split = Decimal(percentage)
                total += percentage_split
                depreciation_splits.append(
                    DepreciationSplit(
                        fixed_asset=fixed_asset,
                        object_item=object_item,
                        object_posting=object_posting,
                        percentage=percentage_split,
                        parent_id=parent_id
                    )
                )

        if total != 100:
            return JsonResponse({
                'text': 'Total percentage must equal to 100%',
                'error': True
            })

        DepreciationSplit.objects.bulk_create(depreciation_splits)

        return JsonResponse({
            'text': 'Depreciation split saved successfully',
            'error': False,
            'reload': True
        })


class DeleteDepreciationSplitView(LoginRequiredMixin, ProfileMixin, DeleteView):
    template_name = 'depreciationsplit/confirm_delete.html'
    model = DepreciationSplit
    context_object_name = 'depreciation_split'

    def get_success_url(self):
        return reverse('edit_fixed_asset', args=(self.kwargs['fixed_asset_id'], ))

    def delete(self, request, *args, **kwargs):
        super().delete(request, *args, **kwargs)
        return JsonResponse({
            'error': False,
            'text': 'Depreciation split deleted successfully',
            'reload': True
        })
