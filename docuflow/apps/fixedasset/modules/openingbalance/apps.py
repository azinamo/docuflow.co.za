from django.apps import AppConfig


class OpeningbalanceConfig(AppConfig):
    name = 'docuflow.apps.fixedasset.modules.openingbalance'
