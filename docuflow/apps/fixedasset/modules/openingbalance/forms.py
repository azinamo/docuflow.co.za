from django import forms

from docuflow.apps.fixedasset.models import OpeningBalance


# Create your forms here
class OpeningBalanceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.fixed_asset = kwargs.pop('fixed_asset')
        self.year = kwargs.pop('year')
        super(OpeningBalanceForm, self).__init__(*args, **kwargs)

    class Meta:
        model = OpeningBalance
        fields = ('asset_value', 'depreciation_value')

    def save(self, commit=True):
        opening_balance = super().save(commit)
        opening_balance.fixed_asset = self.fixed_asset
        opening_balance.year = self.year
        opening_balance.save()

        return opening_balance