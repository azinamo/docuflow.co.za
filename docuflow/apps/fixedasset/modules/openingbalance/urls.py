from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.OpeningBalanceView.as_view(), name='fixed_asset_opening_balances'),
    path('update/opening/<int:fixed_asset_id>/balances/', csrf_exempt(views.UpdateOpeningBalanceView.as_view()),
         name='update_year_fixed_asset_balance'),
    path('copy/year/<int:pk>/balances/', views.CopyYearBalanceView.as_view(),
         name='copy_fixed_asset_previous_year_balances'),
]
