import collections
import logging
import pprint
from decimal import Decimal

from django.conf import settings
from django.views.generic import View, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from annoying.functions import get_object_or_None

from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.fixedasset.models import FixedAsset, Category, OpeningBalance, Update, DepreciationAdjustment, FixedAssetAdjustment
from docuflow.apps.fixedasset.services import get_categorized_fixed_assets, asset_register, generate_fixed_asset_register
from docuflow.apps.fixedasset.enums import DepreciationMethod
from docuflow.apps.period.models import Year, Period
from docuflow.apps.journals.models import Journal, JournalLine
from .forms import OpeningBalanceForm


pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class OpeningBalanceView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'openingbalances/index.html'

    def get_fixed_assets(self):
        return FixedAsset.objects.for_year(company=self.get_company(), year=self.get_year()).order_by('category__name')

    def get_context_data(self, **kwargs):
        context = super(OpeningBalanceView, self).get_context_data(**kwargs)
        categorized_assets = get_categorized_fixed_assets(fixed_assets=self.get_fixed_assets())
        context['categories'] = categorized_assets['categories']
        context['total'] = categorized_assets['assets_total']
        context['total_depreciation'] = categorized_assets['depreciation_total']
        context['year'] = self.get_year()
        return context


class UpdateOpeningBalanceView(LoginRequiredMixin, ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            fixed_asset_id = int(self.kwargs['fixed_asset_id'])
            fixed_asset = FixedAsset.objects.get(pk=fixed_asset_id)

            opening_balance = get_object_or_None(OpeningBalance, fixed_asset=fixed_asset, year=self.get_year())
            form = OpeningBalanceForm(data=self.request.POST, instance=opening_balance, year=self.get_year(),
                                      fixed_asset=fixed_asset)
            if form.is_valid():
                form.save()
                return JsonResponse({
                    'error': False,
                    'text': 'Opening balances successfully updated'
                })
            else:
                logger.info(f'Opening balance could not be create {form.errors}')
                return JsonResponse({
                    'error': False,
                    'text': 'Opening balances could not be updated, please try again'
                })
        except FixedAsset.DoesNotExist as e:
            return JsonResponse({'text': 'Fixed asset not found',
                                 'error': True
                                 })


class CopyYearBalanceView(ProfileMixin, View):

    def get_opening_balances(self, year):
        opening_balances = OpeningBalance.objects.filter(year=year)
        account_balances = {}
        for opening_balance in opening_balances:
            account_balances[opening_balance.fixed_asset_id] = opening_balance
        return account_balances

    def get_categories(self, company):
        return Category.objects.select_related(
            'account', 'depreciation_account', 'accumulated_depreciation_account'
        ).filter(company=company)

    def get_fixed_assets(self, company, year):
        return FixedAsset.objects.select_related(
            'company', 'category'
        ).filter(
            company=company, year=year
        ).order_by('date_purchased')

    def clear_this_year_assets(self, year):
        fixed_assets = FixedAsset.objects.filter(year=year, is_copy=True)
        fixed_assets.delete()

    def get_year_fixed_assets(self, year):
        return FixedAsset.objects.for_year(year=year, company=self.get_company()).exclude(fixed_asset_disposal__gt=0)

    def get_previous_year(self, current_year):
        y = int(current_year.year) - 1
        return Year.objects.filter(year=y, company=current_year.company).first()

    def get_year(self):
        return Year.objects.get(pk=self.kwargs['pk'])

    def clear_year_opening_balances(self, year):
        opening_balances = OpeningBalance.objects.filter(year=year)
        for opening_balance in opening_balances:
            opening_balance.asset_value = 0
            opening_balance.save()

    def get_current_year_fixed_assets(self, year):
        assets = {}
        for opening_balance in OpeningBalance.objects.select_related('fixed_asset').filter(year=year):
            if opening_balance.fixed_asset.parent_asset:
                assets[opening_balance.fixed_asset.parent_asset_id] = opening_balance.fixed_asset
        return assets

    def create_fixed_asset(self, year, fixed_asset, assets):
        fixed_asset_id = fixed_asset.id
        _fixed_asset = fixed_asset
        _fixed_asset.pk = None
        _fixed_asset.year = year
        _fixed_asset.is_copy = True
        _fixed_asset.parent_asset_id = fixed_asset_id

        if fixed_asset.depreciation_method == DepreciationMethod.SECTION_12_B:
            _fixed_asset.depreciation_rate = fixed_asset.get_year_rate(year)

        if fixed_asset_id in assets:
            _fixed_asset.opening_asset_value = assets[fixed_asset_id]['closing_balance']
            _fixed_asset.opening_depreciation_value = assets[fixed_asset_id]['closing_depreciation']
        _fixed_asset.save()
        return _fixed_asset

    def get_period_closing_date(self, year, company):
        fixed_asset_journal = Update.objects.filter(
            period__period_year=year).order_by('-period__from_date').first()
        if fixed_asset_journal and fixed_asset_journal.period.is_open:
            return fixed_asset_journal.period
        else:
            periods = Period.objects.open(year=year, company=company).prefetch_related(
                'assets_updates'
            ).order_by('from_date')
            for period in periods:
                if period.assets_updates.count() == 0:
                    return period
        return None

    def create_ledger(self, year,  period):
        journal = Journal.objects.create(
            company=year.company,
            module=Module.FIXED_ASSET,
            year=year,
            period=period,
            created_by_id=self.request.session['profile'],
            role_id=self.request.session['role'],
            journal_text='Depreciation Adjustment'
        )
        return journal

    def create_ledger_line(self, journal, fixed_asset, adjustment, reason):
        text = f"{str(fixed_asset)}"
        if reason:
            text = f"{str(fixed_asset)} - {reason}"
        if adjustment > 0:
            depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.depreciation_account,
                debit=Decimal(adjustment),
                text=text,
                accounting_date=journal.date
            )
            if depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    depreciation_line.object_items.add(object_item)

            accumulated_depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.accumulated_depreciation_account,
                credit=Decimal(adjustment),
                text=text,
                accounting_date=journal.date
            )
            if accumulated_depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    accumulated_depreciation_line.object_items.add(object_item)
        else:
            depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.depreciation_account,
                credit=Decimal(adjustment) * -1,
                text=text,
                accounting_date=journal.date
            )
            if depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    depreciation_line.object_items.add(object_item)

            accumulated_depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.accumulated_depreciation_account,
                debit=Decimal(adjustment) * -1,
                text=text,
                accounting_date=journal.date
            )
            if accumulated_depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    accumulated_depreciation_line.object_items.add(object_item)

    def create_deprectiaion_adjustment(self, depreciation_adjustment, fixed_asset, ledger):
        adjustment = fixed_asset.calculate_total_depreciation() * -1
        if fixed_asset and adjustment:
            reason = 'Deleted asset depreciated adjustment'
            FixedAssetAdjustment.objects.create(
                depreciation_adjustment=depreciation_adjustment,
                reason=reason,
                fixed_asset=fixed_asset,
                adjustment=Decimal(adjustment),
                opening_value=Decimal(fixed_asset.opening_asset_value),
                closing_value=Decimal(fixed_asset.opening_asset_value)
            )
            self.create_ledger_line(ledger, fixed_asset, float(adjustment), reason)

    def clear_deleted_assets(self, year, period, deleted_assets):
        depreciation_adjustment = DepreciationAdjustment.objects.create(
            year=year,
            period=period,
            company=year.company
        )
        ledger = self.create_ledger(year, period)
        for delete_asset in deleted_assets:
            self.create_deprectiaion_adjustment(depreciation_adjustment, delete_asset, ledger)
            self.clear_fixed_asset(delete_asset)

    def clear_fixed_asset(self, fixed_asset):
        fixed_asset.child_asset.clear()
        fixed_asset.delete()

    def get(self, request, *args, **kwargs):
        with transaction.atomic():
            year = self.get_year()

            previous_year = self.get_previous_year(current_year=year)
            if not previous_year:
                return JsonResponse({'text': 'Previous year not found', 'error': True})

            previous_year_fixed_assets = self.get_year_fixed_assets(year=previous_year)
            # TODO - Handle deleted fixed assets
            deleted_assets = []
            existing_assets = {}
            # if current_fixed_assets:
            #     for parent_asset_id, current_fixed_asset in current_fixed_assets.items():
            #         if current_fixed_asset.is_copy:
            #             if parent_asset_id in previous_year_fixed_assets:
            #                 existing_assets[current_fixed_asset.parent_asset_id] = current_fixed_asset
            #             else:
            #                 deleted_assets.append(current_fixed_asset)
            #
            # if deleted_assets:
            #     self.clear_deleted_assets(year, period, deleted_assets)

            if len(previous_year_fixed_assets) > 0:
                opening_balances_to_update = []
                opening_balances_to_create = []
                year_opening_balances = {
                    opening_balance.fixed_asset: opening_balance
                    for opening_balance in OpeningBalance.objects.filter(year=year)
                }

                registers = generate_fixed_asset_register(fixed_assets=previous_year_fixed_assets, year=previous_year)
                assets_register = registers['assets']

                for fixed_asset in previous_year_fixed_assets:
                    register = assets_register.get(fixed_asset)

                    if asset_register:
                        opening_balance = year_opening_balances.get(fixed_asset)
                        if opening_balance:
                            setattr(opening_balance, 'asset_value', register['closing_balance'])
                            setattr(opening_balance, 'depreciation_value', register['closing_depreciation'])
                            opening_balances_to_update.append(opening_balance)
                        else:
                            opening_balances_to_create.append(
                                OpeningBalance(
                                    fixed_asset=fixed_asset,
                                    year=year,
                                    asset_value=register['closing_balance'],
                                    depreciation_value=register['closing_depreciation']
                                )
                            )

                if opening_balances_to_create:
                    OpeningBalance.objects.bulk_create(opening_balances_to_create)

                if opening_balances_to_update:
                    OpeningBalance.objects.bulk_update(opening_balances_to_update, {'asset_value', 'depreciation_value'})

        return JsonResponse({
            'text': 'Opening balances copied successfully',
            'error': False,
            'reload': True
        })
