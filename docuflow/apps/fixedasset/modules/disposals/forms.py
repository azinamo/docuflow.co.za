
from django import forms

from docuflow.apps.common.enums import Module
from docuflow.apps.customer.utils import customer_select_data
from docuflow.apps.fixedasset.models import Disposal
from docuflow.apps.period.models import Period
from docuflow.apps.period.enums import PeriodStatus
from docuflow.apps.sales.models import Salesman


class AssetInvoiceDisposalForm(forms.Form):

    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(AssetInvoiceDisposalForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['period'] = Period.objects.filter(
                company=company,
                status=PeriodStatus.OPEN,
                period_year__is_active=True
            ).order_by('period')
            self.fields['period'].empty_label = None
            self.fields['customer'].widget = customer_select_data(company)
            self.fields['salesman'].queryset = Salesman.objects.filter(company=company)

    net_amount = forms.DecimalField(required=True)
    total_amount = forms.DecimalField(required=True)
    vat_amount = forms.DecimalField(required=True)
    salesman = forms.ModelChoiceField(label='Salesman', queryset=None)
    customer = forms.ModelChoiceField(label='Customer', queryset=None, required=True)
    period = forms.ModelChoiceField(label='Period', queryset=None, required=True)

    class Meta:
        widgets = {
            'date': forms.TextInput(attrs={'class': 'datepicker'})
        }

    def clean(self):
        cleaned_data = super(AssetInvoiceDisposalForm, self).clean()
        disposal_date = cleaned_data['date']
        if disposal_date:
            if 'period' in cleaned_data and cleaned_data['period']:
                period = cleaned_data['period']
                if not period.is_valid(disposal_date.date()):
                    self.add_error('date', 'Period and disposal date do not match.')
        return cleaned_data


class AssetDisposalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(AssetDisposalForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['period'].queryset = self.fields['period'].queryset.filter(
                company=company,
                status=PeriodStatus.OPEN,
                period_year__is_active=True
            ).order_by('period')
            self.fields['period'].empty_label = None
            self.fields['vat_code'].queryset = self.fields['vat_code'].queryset.filter(
                company=company, module=Module.SALES
            ).order_by('-is_default')
            self.fields['vat_code'].empty_label = None

    selling_price = forms.DecimalField(required=True, label='Selling Price')

    class Meta:
        model = Disposal
        fields = ['period', 'disposal_date', 'net_profit_loss', 'vat_code', 'selling_price']
        widgets = {
            'disposal_date': forms.TextInput(attrs={'class': 'datepicker'})
        }

    def clean(self):
        cleaned_data = super(AssetDisposalForm, self).clean()
        disposal_date = cleaned_data['disposal_date']
        if disposal_date:
            if 'period' in cleaned_data and cleaned_data['period']:
                period = cleaned_data['period']
                if not period.is_valid(disposal_date.date()):
                    self.add_error('disposal_date', 'Period and disposal date do not match.')
        return cleaned_data
