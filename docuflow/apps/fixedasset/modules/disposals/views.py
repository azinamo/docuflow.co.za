import logging
import pprint
from collections import OrderedDict

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import View, TemplateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import VatCode
from docuflow.apps.fixedasset.models import FixedAsset, Addition
from docuflow.apps.fixedasset.services import get_categorized_with_nbv, calculate_asset_additions
from docuflow.apps.fixedasset.exceptions import FixedAssetsException
from .services import CreateDisposal

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class InvalidPeriodDisposalDateException(Exception):
    pass


class FixedAssetDisposalView(LoginRequiredMixin, ProfileMixin, TemplateView):
    model = FixedAsset
    context_object_name = 'fixed_assets'
    template_name = 'disposals/index.html'

    def get_context_data(self, **kwargs):
        context = super(FixedAssetDisposalView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        assets = FixedAsset.objects.for_year(company=company, year=year).not_disposed().order_by('category__name', 'name')
        fixed_assets = get_categorized_with_nbv(fixed_assets=assets, year=year)
        context['company'] = company
        context['year'] = year
        context['categories'] = fixed_assets['categories']
        return context


class DisposeFixedAssetView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        fixed_assets = self.request.GET.getlist('fixed_assets[]')
        if len(fixed_assets) > 0:
            self.request.session['dispose_assets'] = ','.join(fixed_assets)
            return JsonResponse({
                'text': 'Redirecting ...',
                'error': False,
                'redirect': reverse('confirm_dispose_assets')
            })
        else:
            return JsonResponse({
                'text': 'Please select the fixed assets to dispose',
                'alert': True,
                'error': True
            })


class InvoiceAssetsView(ProfileMixin, TemplateView):
    template_name = 'disposals/assets.html'

    def get_assets(self, year, fixed_assets):
        assets = OrderedDict()
        total_opening_assets_value = 0
        total_opening_depreciation_value = 0
        total_net_book_value = 0
        year_additions = Addition.objects.filter(
            fixed_asset__in=[fixed_asset.id for fixed_asset in fixed_assets], date__year=year.year
        ).all()

        for fixed_asset in fixed_assets:
            assets[fixed_asset.id] = {
                'id': fixed_asset.id,
                'asset': fixed_asset,
                'name': fixed_asset.name,
                'opening_balance': 0,
                'depreciation_value': 0
            }
            asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
            year_depreciation = fixed_asset.calculate_total_depreciation()

            asset_additions, asset_revaluation, additional_disposal = calculate_asset_additions(
                year_additions=year_additions,
                fixed_asset=fixed_asset
            )
            asset_disposals += additional_disposal

            opening_asset_balance = 0
            opening_depreciation_balance = 0

            if fixed_asset.opening_value:
                opening_asset_balance = fixed_asset.opening_value
            if fixed_asset.opening_depreciation:
                opening_depreciation_balance = fixed_asset.opening_depreciation

            asset_closing_balance = opening_asset_balance + asset_additions - asset_disposals
            depreciation_closing_balance = opening_depreciation_balance - depreciation_disposal + year_depreciation

            net_book_value = asset_closing_balance - depreciation_closing_balance
            opening_net_book_value = opening_asset_balance - opening_depreciation_balance
            assets[fixed_asset.id]['opening_asset_value'] = fixed_asset.opening_value
            assets[fixed_asset.id]['opening_depreciation_value'] = fixed_asset.opening_depreciation
            assets[fixed_asset.id]['net_book_value'] = net_book_value
            assets[fixed_asset.id]['opening_net_book_value'] = opening_net_book_value
            assets[fixed_asset.id]['closing_balance'] = asset_closing_balance
            assets[fixed_asset.id]['closing_depreciation'] = depreciation_closing_balance
            assets[fixed_asset.id]['last_depreciation_date'] = None

            if fixed_asset.opening_value:
                total_opening_assets_value += fixed_asset.opening_value
            if fixed_asset.opening_depreciation:
                total_opening_depreciation_value += fixed_asset.opening_depreciation
            total_net_book_value += net_book_value

        return assets, total_opening_depreciation_value, total_opening_assets_value, total_net_book_value

    def get_fixed_assets(self, year):
        dispose_assets = self.request.session['dispose_assets']
        assets_list = dispose_assets.split(',')
        return FixedAsset.objects.for_year(company=year.company, year=year).filter(id__in=[int(asset_id) for asset_id in assets_list]).order_by('name')

    def get_context_data(self, **kwargs):
        context = super(InvoiceAssetsView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        assets = self.get_fixed_assets(year=year)

        fixed_assets, depreciation_value, assets_value, net_book_value = self.get_assets(year, assets)
        context['profit_on_asset_account'] = None
        context['loss_on_asset_account'] = None
        if company.default_profit_on_asset_account:
            context['profit_on_asset_account'] = company.default_profit_on_asset_account
        if company.default_loss_on_asset_account:
            context['loss_on_asset_account'] = company.default_loss_on_asset_account
        context['year'] = year
        context['fixed_assets'] = fixed_assets
        context['vat_codes'] = VatCode.objects.output().filter(company=company)
        return context


class ConfirmDisposeFixedAssetView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'disposals/confirm.html'

    def get_assets(self, year, fixed_assets):
        assets = OrderedDict()
        total_opening_assets_value = 0
        total_opening_depreciation_value = 0
        total_net_book_value = 0
        year_additions = Addition.objects.filter(
            fixed_asset__in=[fixed_asset.id for fixed_asset in fixed_assets], date__year=year.year
        ).all()

        for fixed_asset in fixed_assets:
            assets[fixed_asset.id] = {
                'id': fixed_asset.id,
                'asset': fixed_asset,
                'name': fixed_asset.name,
                'opening_balance': 0,
                'depreciation_value': 0
            }
            asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
            year_depreciation = fixed_asset.calculate_total_depreciation()

            asset_additions, asset_revaluation, additional_disposal = calculate_asset_additions(
                year_additions=year_additions,
                fixed_asset=fixed_asset
            )
            asset_disposals += additional_disposal

            opening_asset_balance = 0
            opening_depreciation_balance = 0

            if fixed_asset.opening_value:
                opening_asset_balance = fixed_asset.opening_value
            if fixed_asset.opening_depreciation:
                opening_depreciation_balance = fixed_asset.opening_depreciation

            asset_closing_balance = opening_asset_balance + asset_additions - asset_disposals
            depreciation_closing_balance = opening_depreciation_balance - depreciation_disposal + year_depreciation

            net_book_value = asset_closing_balance - depreciation_closing_balance
            opening_net_book_value = opening_asset_balance - opening_depreciation_balance
            assets[fixed_asset.id]['opening_asset_value'] = fixed_asset.opening_value
            assets[fixed_asset.id]['opening_depreciation_value'] = fixed_asset.opening_depreciation
            assets[fixed_asset.id]['net_book_value'] = net_book_value
            assets[fixed_asset.id]['opening_net_book_value'] = opening_net_book_value
            assets[fixed_asset.id]['closing_balance'] = asset_closing_balance
            assets[fixed_asset.id]['closing_depreciation'] = depreciation_closing_balance
            assets[fixed_asset.id]['last_depreciation_date'] = None

            if fixed_asset.opening_value:
                total_opening_assets_value += fixed_asset.opening_value
            if fixed_asset.opening_depreciation:
                total_opening_depreciation_value += fixed_asset.opening_depreciation
            total_net_book_value += net_book_value

        return {'assets': assets, 'total_depreciation': total_opening_depreciation_value,
                'assets_value': total_opening_assets_value, 'total_net_book_value': total_net_book_value}

    def get_disposal_assets(self, year):
        dispose_assets = self.request.session['dispose_assets']
        return FixedAsset.objects.for_year(company=year.company, year=year).prefetch_related(
            'object_items'
        ).select_related(
            'category', 'category__accumulated_depreciation_account'
        ).filter(id__in=[int(asset_id) for asset_id in dispose_assets.split(',')]).order_by('name')

    def get_fixed_assets(self, year):
        fixed_assets = {'assets': None, 'total_depreciation': 0, 'total_net_book_value': 0, 'assets_value': 0}
        if 'dispose_assets' in self.request.session:
            dispose_assets = self.get_disposal_assets(year=year)
            fixed_assets = self.get_assets(year, dispose_assets)
        return fixed_assets

    def get_context_data(self, **kwargs):
        context = super(ConfirmDisposeFixedAssetView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['company'] = company
        context['year'] = year
        context['fixed_assets'] = self.get_fixed_assets(year)
        return context


class CreateInvoiceDisposal(ProfileMixin, View):

    def get_assets(self, year, fixed_assets):
        assets = OrderedDict()
        total_opening_assets_value = 0
        total_opening_depreciation_value = 0
        total_net_book_value = 0
        year_additions = Addition.objects.filter(
            fixed_asset__in=[fixed_asset.id for fixed_asset in fixed_assets], date__year=year.year
        ).all()

        for fixed_asset in fixed_assets:
            assets[fixed_asset.id] = {
                'id': fixed_asset.id,
                'asset': fixed_asset,
                'name': fixed_asset.name,
                'opening_balance': 0,
                'depreciation_value': 0
            }
            asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
            year_depreciation = fixed_asset.calculate_total_depreciation()

            asset_additions, asset_revaluation, additional_disposal = calculate_asset_additions(
                year_additions=year_additions,
                fixed_asset=fixed_asset
            )
            asset_disposals += additional_disposal

            opening_asset_balance = 0
            opening_depreciation_balance = 0

            if fixed_asset.opening_value:
                opening_asset_balance = fixed_asset.opening_value
            if fixed_asset.opening_depreciation:
                opening_depreciation_balance = fixed_asset.opening_depreciation

            asset_closing_balance = opening_asset_balance + asset_additions - asset_disposals
            depreciation_closing_balance = opening_depreciation_balance - depreciation_disposal + year_depreciation

            net_book_value = asset_closing_balance - depreciation_closing_balance
            opening_net_book_value = opening_asset_balance - opening_depreciation_balance
            assets[fixed_asset.id]['opening_asset_value'] = fixed_asset.opening_value
            assets[fixed_asset.id]['opening_depreciation_value'] = fixed_asset.opening_depreciation
            assets[fixed_asset.id]['net_book_value'] = net_book_value
            assets[fixed_asset.id]['opening_net_book_value'] = opening_net_book_value
            assets[fixed_asset.id]['closing_balance'] = asset_closing_balance
            assets[fixed_asset.id]['closing_depreciation'] = depreciation_closing_balance
            assets[fixed_asset.id]['last_depreciation_date'] = None

            if fixed_asset.opening_value:
                total_opening_assets_value += fixed_asset.opening_value
            if fixed_asset.opening_depreciation:
                total_opening_depreciation_value += fixed_asset.opening_depreciation
            total_net_book_value += net_book_value

        return {'assets': assets, 'total_depreciation': total_opening_depreciation_value,
                'assets_value': total_opening_assets_value, 'total_net_book_value': total_net_book_value}

    def get_disposal_assets(self, year):
        dispose_assets = self.request.session['dispose_assets']
        return FixedAsset.objects.for_year(company=year.company, year=year).prefetch_related(
            'object_items'
        ).select_related(
            'category', 'category__accumulated_depreciation_account'
        ).filter(id__in=[int(asset_id) for asset_id in dispose_assets.split(',')])

    def get_fixed_assets(self, year):
        fixed_assets = {'assets': None, 'total_depreciation': 0, 'total_net_book_value': 0, 'assets_value': 0}
        if 'dispose_assets' in self.request.session:
            dispose_assets = self.get_disposal_assets(year=year)
            fixed_assets = self.get_assets(year, dispose_assets)
        return fixed_assets

    def post(self, request):
        try:
            with transaction.atomic():
                company = self.get_company()
                year = self.get_year()
                branch = self.get_branch()
                profile = self.get_profile()
                role = Role.objects.filter(id=self.request.session['role']).first()
                fixed_assets = self.get_fixed_assets(year)

                disposal_data = self.request.POST

                create_disposal = CreateDisposal(company, branch, year, profile, role, fixed_assets, disposal_data)
                disposal = create_disposal.execute()

            response = {'text': 'Asset disposal saved successfully',
                        'error': False,
                        'redirect': reverse('view_sales_invoice', kwargs={'pk': disposal.invoice.id})
                        }
            return JsonResponse(response)
        except InvalidPeriodDisposalDateException as ex:
            logger.exception(ex)
            return JsonResponse({
                'text': str(ex),
                'errors': {'disposal_date': ['Please update to match with period'],
                           'period': ['Please update to match with disposal date']
                           },
                'error': True,
                'details': str(ex)
            })
        except FixedAssetsException as err:
            logger.exception(err)
            return JsonResponse({
                'text': str(err),
                'error': True,
                'details': str(err)
            })


class SaveFixedAssetDisposalView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        fixed_assets = self.request.GET['fixed_assets']
        if fixed_assets:
            self.request.session['dispose_assets'] = fixed_assets.split(', ')
            return JsonResponse({
                'text': 'Redirecting ...',
                'error': False,
                'redirect': reverse('confirm_dispose_assets')
            })
        else:
            JsonResponse({
                'text': 'Please select the fixed assets to dispose',
                'alert': True,
                'error': True
            })
