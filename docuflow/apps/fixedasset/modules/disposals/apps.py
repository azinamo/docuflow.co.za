from django.apps import AppConfig


class DisposalsConfig(AppConfig):
    name = 'docuflow.apps.fixedasset.modules.disposals'
