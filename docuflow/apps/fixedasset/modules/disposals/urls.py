from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.FixedAssetDisposalView.as_view(), name='fixed_asset_disposal'),
    path('create/', views.DisposeFixedAssetView.as_view(), name='dispose_assets'),
    path('confirm/', views.ConfirmDisposeFixedAssetView.as_view(), name='confirm_dispose_assets'),
    path('store/', views.CreateInvoiceDisposal.as_view(), name='create_invoice_disposed_assets'),
    path('save/', views.SaveFixedAssetDisposalView.as_view(), name='save_assets_disposal'),
    path('invoice/assets/', csrf_exempt(views.InvoiceAssetsView.as_view()), name='invoice_disposal_assets')
]
