from django import forms

from docuflow.apps.fixedasset.models import Warranty


class WarrantyForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.fixed_asset = kwargs.pop('fixed_asset')
        super(WarrantyForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Warranty
        fields = ('supplier', 'period', 'end_date')
        widgets = {
            'end_date': forms.TextInput(attrs={'class': 'datepicker'})
        }

    def save(self, commit=True):
        warranty = super().save(commit=False)
        warranty.fixed_asset = self.fixed_asset
        warranty.save()
        return warranty
