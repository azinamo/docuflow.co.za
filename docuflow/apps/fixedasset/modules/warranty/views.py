import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import JsonResponse
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.fixedasset.models import FixedAsset, Warranty
from docuflow.apps.fixedasset.modules.warranty.forms import WarrantyForm

logger = logging.getLogger(__name__)


class WarrantyView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Warranty
    context_object_name = 'warranties'
    template_name = 'warranty/index.html'

    def get_queryset(self):
        return super().get_queryset().select_related(
            'fixed_asset'
        ).filter(fixed_asset=self.kwargs['fixed_asset_id'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fixed_asset'] = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        return context


class CreateWarrantyView(ProfileMixin, CreateView):
    template_name = 'warranty/create.html'
    form_class = WarrantyForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['fixed_asset'] = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fixed_asset = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        context['fixed_asset'] = fixed_asset
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the warranty, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateWarrantyView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            return JsonResponse({
                'error': False,
                'text': 'Warranty for fixed assets successfully created',
                'reload': True
            })
        return super().form_valid(form)


class EditWarrantyView(ProfileMixin, UpdateView):
    template_name = 'warranty/edit.html'
    form_class = WarrantyForm
    context_object_name = 'warranty'

    def get_object(self, queryset=None):
        return Warranty.objects.get(pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['fixed_asset'] = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fixed_asset = FixedAsset.objects.get(pk=self.kwargs['fixed_asset_id'])
        context['fixed_asset'] = fixed_asset
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the warranty, please fix the errors.',
                'errors': form.errors
            })
        return super(EditWarrantyView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            return JsonResponse({
                'error': False,
                'text': 'Warranty for fixed assets successfully updated',
                'reload': True
            })
        return super().form_valid(form)


class DeleteWarrantyView(ProfileMixin, LoginRequiredMixin, DeleteView):
    model = Warranty
    template_name = 'warranty/delete.html'
    context_object_name = 'warranty'

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the success URL.
        """
        Warranty.objects.get(pk=self.kwargs['pk']).delete()

        return JsonResponse({
            'text': 'Warranty successfully deleted.',
            'error': False,
            'reload': True
        })