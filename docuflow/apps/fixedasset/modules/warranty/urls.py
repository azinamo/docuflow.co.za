from django.urls import path

from . import views

urlpatterns = [
    path('', views.WarrantyView.as_view(), name='fixed_asset_warranty'),
    path('create/', views.CreateWarrantyView.as_view(), name='create_fixed_asset_warranty'),
    path('<int:pk>/edit/', views.EditWarrantyView.as_view(), name='edit_fixed_asset_warranty'),
    path('<int:pk>/delete/', views.DeleteWarrantyView.as_view(), name='delete_fixed_asset_warranty')
]
