from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.FixedAssetView.as_view(), name='fixed_asset_index'),
    path('register/', views.AssetRegisterView.as_view(), name='asset_register_index'),
    path('create/', views.CreateFixedAssetView.as_view(), name='create_fixed_asset'),
    path('list/', views.FixedAssetListView.as_view(), name='fixed_asset_list'),
    path('import/', views.ImportFixedAssetView.as_view(), name='import_fixed_assets'),
    path('<int:pk>/edit/', views.EditFixedAssetView.as_view(), name='edit_fixed_asset'),
    path('<int:pk>/show/', views.EditFixedAssetView.as_view(), name='show_fixed_asset'),
    path('<int:pk>/delete/', views.DeleteFixedAssetView.as_view(), name='delete_fixed_asset'),
    path('category/defaults/', views.CategoryFixedAssetDefaultsView.as_view(), name='asset_category_defaults'),
    path('invoice/<int:invoice_id>/<int:invoice_account_id>/create/', views.CreateInvoiceFixedAssetView.as_view(),
         name='create_invoice_asset'),
    path('journal/<int:journal_account_id>/create/', views.CreateJournalFixedAssetView.as_view(),
         name='create_journal_asset'),
    path('depreciation/adjustments/', views.DepreciationAdjustmentView.as_view(), name='depreciation_adjustments'),
    path('summary/', views.SummaryView.as_view(), name='fixed_assets_summary'),
    path('download/', views.DownloadView.as_view(), name='download_fixed_assets'),
    path('openingbalance/', include('docuflow.apps.fixedasset.modules.openingbalance.urls')),
    path('categories/', include('docuflow.apps.fixedasset.modules.assetcategory.urls')),
    path('update/', include('docuflow.apps.fixedasset.modules.assetupdates.urls')),
    path('disposals/', include('docuflow.apps.fixedasset.modules.disposals.urls')),
    path('<int:fixed_asset_id>/additions/', include('docuflow.apps.fixedasset.modules.assetaddition.urls')),
    path('<int:fixed_asset_id>/locations/', include('docuflow.apps.fixedasset.modules.assetlocation.urls')),
    path('<int:fixed_asset_id>/depreciation-split/', include('docuflow.apps.fixedasset.modules.depreciationsplit.urls')),
    path('<int:fixed_asset_id>/warranty/', include('docuflow.apps.fixedasset.modules.warranty.urls')),
]
