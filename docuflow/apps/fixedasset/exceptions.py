class FixedAssetsException(Exception):
    pass


class AssetOpeningBalanceException(Exception):
    pass


class IncompleteFixedAssetAccounts(FixedAssetsException):
    pass


class NotFixedAssetsFound(FixedAssetsException):
    pass
