from enumfields import IntEnum, Enum


class FixedAssetStatus(IntEnum):
    DISPOSED = 1
    ACTIVE = 2
    PENDING_UPDATE = 3
    PENDING = 4


class DepreciationMethod(IntEnum):
    STRAIGHT_LINE = 1
    REDUCING_BALANCE = 2
    SECTION_12_B = 3
    SECTION_12_E = 4

    class Labels:
        SECTION_12_B = 'S12B'
        SECTION_12_E = 'S12E'


class AdditionType(Enum):
    ADDITION = 'ADDITION'
    REVALUATION = 'REVALUATION'
    DISPOSAL = 'DISPOSAL'
    NEW_ASSET = 'NEW_ASSET'
