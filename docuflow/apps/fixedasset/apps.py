from django.apps import AppConfig


class FixedassetConfig(AppConfig):
    name = 'docuflow.apps.fixedasset'
