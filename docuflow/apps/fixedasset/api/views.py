from rest_framework import generics

from docuflow.apps.fixedasset.models import Update
from .serializers import UpdateSerializer


class UpdateView(generics.ListAPIView):
    queryset = Update.objects.all()
    serializer_class = UpdateSerializer
