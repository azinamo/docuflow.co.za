from rest_framework import serializers

from docuflow.apps.fixedasset.models import Update


class UpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Update
        fields = '__all__'
