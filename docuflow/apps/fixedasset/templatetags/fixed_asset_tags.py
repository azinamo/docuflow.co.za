import pprint
from collections import defaultdict

from django import template

from docuflow.apps.accountposting.models import ObjectPosting
from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import Company, ObjectItem
from docuflow.apps.fixedasset.models import FixedAsset

register = template.Library()
pp = pprint.PrettyPrinter(indent=4)


def get_fixed_asset(fixed_asset_id):
    return FixedAsset.objects.prefetch_related(
        'object_items', 'postings'
    ).filter(
        id=fixed_asset_id
    ).first()


def get_asset_object_items(fixed_asset):
    account_object_links = {}
    if fixed_asset:
        for fixed_asset_object_item in fixed_asset.object_items.all():
            if fixed_asset_object_item.company_object.id in account_object_links:
                account_object_links[fixed_asset_object_item.company_object.id]['value'] = fixed_asset_object_item.id
            else:
                account_object_links[fixed_asset_object_item.company_object.id] = {'value': fixed_asset_object_item.id}
    return account_object_links


def get_asset_object_postings(fixed_asset):
    object_postings_links = {}
    if fixed_asset:
        for object_posting in fixed_asset.postings.all():
            if object_posting.company_object.id in object_postings_links:
                object_postings_links[object_posting.company_object.id]['value'] = object_posting.id
            else:
                object_postings_links[object_posting.company_object.id] = {'value': object_posting.id}
    return object_postings_links


def get_fixed_asset_objects(company):
    company_objects = []
    for company_object in company.company_objects.order_by('ordering'):
        if Module.FIXED_ASSET.value in company_object.modules:
            company_objects.append(company_object)
    return company_objects


def get_object_postings(company):
    company_objects_postings = defaultdict(list)
    objects_postings = ObjectPosting.objects.select_related(
        'company_object'
    ).filter(
        company_object__company=company
    ).all()
    for object_posting in objects_postings:
        company_objects_postings[object_posting.company_object.id].append(object_posting)
    return company_objects_postings


@register.inclusion_tag('fixedasset/linked_objects.html')
def fixed_asset_linked_objects(company, fixed_asset=None):
    object_categories = {}

    # fixed_asset = get_fixed_asset(fixed_asset_id)

    account_object_links = get_asset_object_items(fixed_asset)
    account_object_posting_links = get_asset_object_postings(fixed_asset)

    if company:
        company_objects = get_fixed_asset_objects(company)

        company_objects_postings = get_object_postings(company)
        company_objects_ids = [company_object.id for company_object in company_objects]

        objects_items = ObjectItem.objects.filter(company_object_id__in=company_objects_ids).all()

        for company_object in company_objects:
            object_categories[company_object.id] = {}
            object_categories[company_object.id]['label'] = company_object
            object_categories[company_object.id]['value'] = None
            object_categories[company_object.id]['objects'] = []
            if company_object.id in account_object_links:
                object_categories[company_object.id]['value'] = account_object_links[company_object.id]['value']

            if company_object.id in account_object_posting_links:
                object_categories[company_object.id]['posting_value'] = account_object_posting_links[company_object.id]['value']

            if company_objects_postings:
                object_categories[company_object.id]['object_postings'] = company_objects_postings.get(company_object.id, {})

            for objects_item in objects_items:
                if objects_item.company_object_id == company_object.id:
                    object_categories[company_object.id]['objects'].append(objects_item)

    return {'object_categories': object_categories, 'fixed_asset': fixed_asset}
