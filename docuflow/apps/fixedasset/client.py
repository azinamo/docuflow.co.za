import os

from openpyxl import load_workbook

from django.conf import settings


class FixedAssetUploadClient(object):

    # noinspection PyMethodMayBeStatic
    def get_upload_data(self, file, start_at=0):
        wb = load_workbook(file, data_only=True)
        sh = wb.active
        data = {}
        counter = 0
        headers = {}
        for row in sh.iter_rows():
            if counter <= start_at:
                headers[counter] = {}
                for c, cell in enumerate(row, start=1):
                    headers[counter][c] = cell.value
            else:
                data[counter] = {}
                for c, cell in enumerate(row, start=1):
                    data[counter][c] = cell.value
            counter += 1
        return headers, data


fixed_asset_upload_client = FixedAssetUploadClient()
