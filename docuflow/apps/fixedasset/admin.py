from django.contrib import admin

from . import models
from .forms import FixedAssetAdminForm


class FixedAssetOpeningBalances(admin.TabularInline):
    model = models.OpeningBalance
    fields = ('year', 'asset_value', 'depreciation_value')


class FixedAssetAdditions(admin.TabularInline):
    model = models.Addition
    fields = ('name', 'date', 'amount')


@admin.register(models.FixedAsset)
class FixedAssetAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'cost_price', 'date_purchased', 'asset_number', 'depreciation_method', 'status')
    list_filter = ('company', 'category', )
    search_fields = ('name', )
    list_select_related = ('category', 'company', )
    inlines = [FixedAssetOpeningBalances, FixedAssetAdditions]
    form = FixedAssetAdminForm


@admin.register(models.Disposal)
class FixedAssetDisposalAdmin(admin.ModelAdmin):
    list_display = ('period', 'invoice_number', 'selling_price', 'vat_code', 'net_profit_loss', 'disposal_date', 'role')
    fields = ('invoice_number', 'selling_price', 'net_profit_loss', 'disposal_date')
    list_filter = ('period', )
    search_fields = ('invoice_number', )
    list_select_related = ('period', 'vat_code', 'role')
