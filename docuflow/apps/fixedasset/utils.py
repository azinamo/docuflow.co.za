import logging

from django.conf import settings
from django.db.models import Prefetch
from openpyxl import Workbook

from docuflow.apps.accountposting.models import ObjectPosting, ObjectItemPosting
from docuflow.apps.common.utils import get_cell_value, to_decimal, to_date, get_field_iexact_in, round_decimal
from docuflow.apps.company.models import ObjectItem, Branch
from docuflow.apps.fixedasset.models import Category, FixedAsset, OpeningBalance, Addition, DepreciationSplit, Location
from docuflow.apps.period.models import Year
from .client import fixed_asset_upload_client

logger = logging.getLogger(__name__)


class FixedAssetImportException(Exception):
    pass


def get_object_code(value):
    if isinstance(value, float):
        value = str(value).replace('.0', '')
    return value


class ImportFixedAssetJob(object):

    def __init__(self, company, year, file):
        self.year = year
        self.company = company
        self.file = file

        if year.company != company:
            raise FixedAssetImportException('Company mismatch error occurred, please logout and login again.')

    def execute(self):
        headers, assets_data = fixed_asset_upload_client.get_upload_data(file=self.file, start_at=1)
        data = []
        counter = 0
        split = ''
        kst_index = 12
        kst_name = headers[0][12].lower()

        project_index = 14
        project_name = headers[0][14].lower()

        for _, asset_row in assets_data.items():
            data_dict = {
                'name': '', 'category': '', 'cost_price': 0, 'date_purchased': None, 'own_asset_number': 0,
                'supplier': '', 'location': '', 'quantity': 0, 'opening_cost': 0, 'opening_account_depreciation': 0,
                'splits': {}, project_name: {}, kst_name: {}
            }
            m = 1
            can_add = True
            for c, cell_value in asset_row.items():
                if c == 1:
                    val = get_cell_value(cell_value)
                    if val is None or val == '':
                        can_add = False
                    data_dict['name'] = str(val)
                if c == 2:
                    val = get_cell_value(cell_value)
                    if val is None or val == '':
                        can_add = False
                    data_dict['category'] = str(val)
                if c == 3:
                    val = get_cell_value(term=cell_value)
                    if val is None or val == '':
                        can_add = False
                    data_dict['cost_price'] = str(val)
                if c == 4:
                    data_dict['date_purchased'] = get_cell_value(cell_value)
                    m += 1
                if c == 5:
                    own_asset_number = get_cell_value(cell_value)
                    if own_asset_number is None or own_asset_number == '':
                        can_add = False
                    data_dict['own_asset_number'] = str(own_asset_number)
                if c == 6:
                    data_dict['supplier'] = get_cell_value(cell_value)
                if c == 7:
                    data_dict['location'] = get_cell_value(cell_value)
                if c == 8:
                    quantity = get_cell_value(cell_value)
                    if quantity == '' or quantity is None:
                        quantity = 0
                    data_dict['quantity'] = quantity
                if c == 9:
                    data_dict['opening_cost'] = cell_value
                if c == 10:
                    data_dict['opening_account_depreciation'] = cell_value
                if c == 11:
                    branch = get_cell_value(cell_value)
                    data_dict['branch'] = branch
                if c == kst_index:
                    value = get_object_code(value=get_cell_value(cell_value))
                    if value:
                        split = value
                        if not can_add:
                            if split in data[counter-1]['splits']:
                                data[counter-1]['splits'][split]['projects']['values'][get_object_code(value=asset_row[14])] = asset_row[15]
                            else:
                                data[counter-1]['splits'][split] = {
                                    'percentage': asset_row[13],
                                    'source': kst_name,
                                    'projects': {
                                        'values': {
                                            get_object_code(value=asset_row[14]): asset_row[15],
                                        },
                                        'source': project_name,
                                    }
                                }
                        else:
                            data_dict['splits'][split] = {
                                'percentage': asset_row[13],
                                'source': kst_name,
                                'projects': {
                                    'values': {
                                        get_object_code(value=asset_row[14]): asset_row[15],
                                    },
                                    'source': project_name,
                                }
                            }
                if c == project_index:
                    project = get_object_code(value=get_cell_value(cell_value))
                    if project:
                        if not can_add:
                            if split in data[counter-1]['splits']:
                                data[counter-1]['splits'][split]['projects']['values'][get_object_code(value=project)] = asset_row[15]
                        else:
                            if split in data_dict['splits']:
                                data_dict['splits'][split]['projects']['values'][get_object_code(value=project)] = asset_row[15]
                            else:
                                data_dict['splits'][split] = {
                                    'percentage': asset_row[13],
                                    'source': kst_name,
                                    'projects': {
                                        'values': {
                                            get_object_code(value=asset_row[14]): asset_row[15],
                                        },
                                        'source': project_name,
                                    }
                                }

            if can_add:
                counter += 1
                data.append(data_dict)
        return self._sync(data)

    def get_object_items(self):
        object_items = {}
        for object_item in ObjectItem.objects.select_related('company_object').filter(company_object__company=self.company):
            label = object_item.company_object.label.lower()
            if label in object_items:
                object_items[label][str(object_item.code)] = object_item
            else:
                object_items[label] = {
                    str(object_item.code): object_item
                }

        object_postings = ObjectPosting.objects.prefetch_related(
            Prefetch('object_splitting', ObjectItemPosting.objects.select_related('object_item').filter(percentage__gt=0))
        ).filter(company_object__company=self.company)

        for object_posting in object_postings:
            object_items[str(object_posting.name)] = object_posting

        return object_items

    def get_existing_fixed_assets(self):
        return {
            fixed_asset.own_asset_number: fixed_asset
            for fixed_asset in FixedAsset.objects.filter(company=self.company)
        }

    def get_fixed_assets_categories(self):
        qs = Category.objects.select_related(
            'depreciation_account', 'accumulated_depreciation_account', 'account'
        ).filter(company=self.company)
        return {category.name: category for category in qs}

    def get_branches(self):
        return {
            branch.label: branch
            for branch in Branch.objects.filter(company=self.company)
        }

    def _sync(self, fixed_assets_data):
        """
        Sync fixed assets .
        """
        self.categories = self.get_fixed_assets_categories()
        self.object_items = self.get_object_items()
        self.branches = self.get_branches()

        data = self._prepare_data(assets_data=fixed_assets_data)

        logger.info(self.missing_data)
        if not self.missing_data:
            if data:
                existing_fixed_assets = self.get_existing_fixed_assets()
                self.create_or_update_fixed_assets(fixed_assets_data=data, existing_fixed_assets=existing_fixed_assets)
        filename = None
        # if self.missing_data:
        #     filename = self.create_missing_data_report()
        return len(data), self.missing_data, filename

    # noinspection PyMethodMayBeStatic
    def _prepare_data(self, assets_data):
        """
        Prepares fixed assets data for storing in DB.
        """
        fixed_assets = []
        format_to_decimal = {'cost_price', 'opening_cost', 'opening_account_depreciation'}
        format_to_date = {'date_purchased'}
        fields = {'name', 'category', 'cost_price', 'date_purchased', 'own_asset_number', 'supplier', 'location',
                  'quantity'} | format_to_decimal | format_to_date
        self.missing_data = []
        for fixed_asset_line in assets_data or []:
            data = {}
            can_add = True
            missing_data = {}
            missing_object_items = []
            for field in fixed_asset_line.keys():
                if field == 'category':
                    category: Category = self.categories.get(fixed_asset_line[field])
                    if not category:
                        can_add = False
                        missing_data['category'] = fixed_asset_line[field]
                    if category:
                        data[field] = category
                        data['depreciation_method'] = category.depreciation_method
                        data['depreciation_account'] = category.depreciation_account
                        data['depreciation_rate'] = category.depreciation_rate
                        data['accumulated_depreciation_account'] = category.accumulated_depreciation_account
                        data['asset_account'] = category.account
                        data[field] = category
                elif field == 'splits':
                    data['splits'] = {}
                    objects_total_percentage = 0
                    for item, split in fixed_asset_line['splits'].items():
                        source = split['source']
                        source_object_items = self.object_items.get(source, {})

                        object_item = source_object_items.get(str(item))
                        if not object_item:
                            object_item = self.object_items.get(str(item))

                        if not object_item:
                            can_add = False
                            missing_object_items.append(str(item))

                        object_percentage = round(split['percentage'] * 100, 2)
                        objects_total_percentage += object_percentage

                        data['splits'][object_item] = {
                            'percentage': object_percentage,
                            'projects': {}
                        }
                        projects = split.get('projects', {})
                        projects_total_percentage = 0
                        project_source_object_items = self.object_items.get(projects['source'], {})
                        for project_key, project_value in projects['values'].items():
                            project_object_item = project_source_object_items.get(str(project_key))

                            if not project_object_item:
                                project_object_item = self.object_items.get(str(project_key))

                            if not project_object_item:
                                can_add = False
                                missing_object_items.append(str(project_key))

                            projects_percentage = round(project_value * 100, 2)
                            projects_total_percentage += projects_percentage
                            data['splits'][object_item]['projects'][project_object_item] = projects_percentage
                        if projects_total_percentage != 100:
                            can_add = False
                            missing_data['message'] = f'Total {projects_total_percentage} percentage for object' \
                                                      f' items is not 100%'

                    if objects_total_percentage != 100:
                        can_add = False
                        missing_data['message'] = f'Total {objects_total_percentage} percentage for project object' \
                                                  f' items is not 100%'
                elif field == 'branch':
                    branch = self.branches.get(fixed_asset_line[field])
                    if not branch:
                        can_add = False
                        missing_data['branch'] = fixed_asset_line[field]
                    data['branch'] = branch
                elif field in format_to_decimal:
                    data[field] = to_decimal(value=fixed_asset_line[field])
                elif field in format_to_date:
                    data[field] = to_date(value=fixed_asset_line[field])
                elif field in fields:
                    data[field] = fixed_asset_line[field]

            if missing_object_items:
                missing_data['object_items'] = missing_object_items
            if can_add:
                fixed_assets.append(data)
            if missing_data:
                self.missing_data.append({'fixed_asset': fixed_asset_line, 'missing': missing_data})
        return fixed_assets

    def create_missing_data_report(self):
        wb = Workbook()
        ws = wb.active
        ws.title = "Missing fixed assets information"

        row_count = 1

        for missing_data in self.missing_data:
            ws.cell(column=1, row=row_count, value=missing_data['fixed_asset']['name'].strip().strip('\n'))
            if missing_data['missing'].get('category', False):
                ws.cell(column=4, row=row_count, value="Category is missing")
            if missing_data['missing'].get('branch', False):
                ws.cell(column=3, row=row_count, value=f"Branch is missing")
            if missing_data['missing'].get('objet_items', False):
                for c, object_item in enumerate(missing_data['missing'].get('objet_items'), start=2):
                    ws.cell(column=c, row=row_count, value=f"Object item {object_item}")
            row_count += 1

        filename = f'{settings.MEDIA_ROOT}/missing_fixed_assets_data.xlsx'
        wb.save(filename)

        return filename

    def create_fixed_assets(self, assets_data, existing_fixed_assets):
        assets_to_create = []
        fixed_assets = {}
        assets_numbers = []
        for asset in assets_data:
            fixed_asset = existing_fixed_assets.get(asset['own_asset_number'])
            if fixed_asset is not None:
                fixed_assets[asset['own_asset_number']] = fixed_asset
            else:
                assets_numbers.append(asset['own_asset_number'])
                assets_to_create.append(
                    FixedAsset(
                        year=self.year,
                        company=self.company,
                        **{
                            'own_asset_number': asset['own_asset_number'],
                            'name': asset['name'],
                            'category': asset['category'],
                            'cost_price': asset['cost_price'],
                            'date_purchased': asset['date_purchased'],
                            'supplier': asset['supplier'],
                            'location': asset['location'],
                            'quantity': asset['quantity'],
                            'depreciation_method': asset['depreciation_method'],
                            'depreciation_account': asset['depreciation_account'],
                            'depreciation_rate': asset['depreciation_rate'],
                            'accumulated_depreciation_account': asset['accumulated_depreciation_account'],
                            'asset_account': asset['asset_account'],
                        }
                    )
                )

        if assets_numbers:
            FixedAsset.objects.bulk_create(assets_to_create)

            for fixed_asset in FixedAsset.objects.filter(get_field_iexact_in(field='own_asset_number', values=assets_numbers), company=self.company):
                fixed_assets[fixed_asset.own_asset_number] = fixed_asset

        return fixed_assets

    # noinspection PyMethodMayBeStatic
    def get_existing_opening_balances(self, assets):
        qs = OpeningBalance.objects.select_related('fixed_asset').filter(
            get_field_iexact_in(field='fixed_asset__own_asset_number', values=assets)
        ).filter(year=self.year, fixed_asset__company=self.company)

        return {
            opening_balance.fixed_asset.own_asset_number: opening_balance
            for opening_balance in qs
        }

    # noinspection PyMethodMayBeStatic
    def get_existing_assets_additions(self, assets):
        qs = Addition.objects.select_related('fixed_asset').filter(
                get_field_iexact_in(field='fixed_asset__own_asset_number', values=assets)
            ).filter(fixed_asset__company=self.company)
        return {
            addition.fixed_asset.own_asset_number: addition
            for addition in qs
        }

    # noinspection PyMethodMayBeStatic
    def create_or_update_fixed_assets(self, fixed_assets_data, existing_fixed_assets):
        """
        Creates new budget in bulk.
        """
        assets_to_update = []
        fields = set()
        opening_balances_to_create = {}
        additions_to_create = {}
        locations_to_create = {}

        fixed_assets = self.create_fixed_assets(assets_data=fixed_assets_data, existing_fixed_assets=existing_fixed_assets)

        assets_numbers = fixed_assets.keys()
        if assets_numbers:
            Location.objects.filter(get_field_iexact_in(field='fixed_asset__own_asset_number', values=assets_numbers)).delete()
            DepreciationSplit.objects.filter(get_field_iexact_in(field='fixed_asset__own_asset_number', values=assets_numbers)).delete()

        opening_balances = self.get_existing_opening_balances(assets=assets_numbers)
        additions = self.get_existing_assets_additions(assets=assets_numbers)

        depreciation_projects_splits = {}
        depreciation_splits_to_add = []
        opening_balances_to_update = []
        additions_to_update = []

        for asset_data in fixed_assets_data:
            own_asset_number = asset_data.pop('own_asset_number')
            opening_cost = asset_data.pop('opening_cost')
            splits = asset_data.pop('splits')
            branch = asset_data.pop('branch')
            opening_account_depreciation = asset_data.pop('opening_account_depreciation')

            fixed_asset = existing_fixed_assets.get(own_asset_number)
            if fixed_asset:
                for field, val in asset_data.items():
                    setattr(fixed_asset, field, val)
                    fields.add(field)
                assets_to_update.append(fixed_asset)

                opening_balance = opening_balances.get(own_asset_number)

                if opening_balance:
                    opening_balance.asset_value = opening_cost
                    opening_balance.depreciation_value = opening_account_depreciation
                    opening_balances_to_update.append(opening_balance)
                else:
                    opening_balances_to_create[fixed_asset] = {
                        'asset_value': opening_cost,
                        'depreciation_value': opening_account_depreciation
                    }

                addition = additions.get(own_asset_number)
                if addition:
                    addition.amount = asset_data['cost_price']
                    addition.name = asset_data['name']
                    additions_to_update.append(addition)
                else:
                    additions_to_create[fixed_asset] = {
                        'date': asset_data['date_purchased'],
                        'amount': asset_data['cost_price']
                    }

                if branch:
                    locations_to_create[fixed_asset] = {
                        'branch': branch,
                        'percentage': 100
                    }

            else:
                fixed_asset = fixed_assets.get(own_asset_number)

                opening_balances_to_create[fixed_asset] = {
                    'asset_value': opening_cost,
                    'depreciation_value': opening_account_depreciation
                }

                additions_to_create[fixed_asset] = {
                    'date': asset_data['date_purchased'],
                    'amount': asset_data['cost_price']
                }

                if branch:
                    locations_to_create[fixed_asset] = {
                        'branch': branch,
                        'percentage': 100
                    }

            for object_item, split_data in splits.items():
                if isinstance(object_item, ObjectPosting):
                    for object_posting in object_item.object_splitting.all():
                        object_posting_percentage = round((split_data['percentage'] / 100) * float(object_posting.percentage), 2)
                        split_key = (fixed_asset, object_posting.object_item, round_decimal(object_posting_percentage))
                        depreciation_slit = DepreciationSplit(
                            fixed_asset=fixed_asset,
                            object_item=object_posting.object_item,
                            percentage=object_posting_percentage
                        )
                        depreciation_splits_to_add.append(depreciation_slit)
                        depreciation_projects_splits[split_key] = self.add_depreciation_split(
                            fixed_asset=fixed_asset,
                            depreciation_slit=depreciation_slit,
                            depreciation_lines=split_data.get('projects', {})
                        )
                else:
                    depreciation_slit = DepreciationSplit(
                        fixed_asset=fixed_asset,
                        object_item=object_item,
                        percentage=split_data['percentage']
                    )
                    split_key = (fixed_asset, object_item, round_decimal(split_data['percentage']))
                    depreciation_splits_to_add.append(depreciation_slit)

                    depreciation_projects_splits[split_key] = self.add_depreciation_split(
                        fixed_asset=fixed_asset,
                        depreciation_slit=depreciation_slit,
                        depreciation_lines=split_data.get('projects', {})
                    )

        self.create_assets_opening_balances(
            opening_balances_to_create=opening_balances_to_create
        )

        self.create_assets_additions(
            additions_to_create=additions_to_create
        )

        self.create_assets_locations(
            location_to_add=locations_to_create
        )

        if assets_to_update:
            FixedAsset.objects.bulk_update(assets_to_update, fields)

        if additions_to_update:
            Addition.objects.bulk_update(additions_to_update, {'amount', 'name'})

        if opening_balances_to_update:
            OpeningBalance.objects.bulk_update(opening_balances_to_update, {'depreciation_value', 'asset_value'})

        self.create_depreciation_splits(
            depreciation_splits=depreciation_splits_to_add,
            projects_splits=depreciation_projects_splits,
            assets_numbers=assets_numbers
        )

    # noinspection PyMethodMayBeStatic
    def create_assets_opening_balances(self, opening_balances_to_create):
        if not opening_balances_to_create:
            return

        opening_balances = []
        for fixed_asset, opening_balance in opening_balances_to_create.items():
            opening_balances.append(
                OpeningBalance(
                    year=self.year,
                    fixed_asset=fixed_asset,
                    asset_value=opening_balance['asset_value'],
                    depreciation_value=opening_balance['depreciation_value']
                )
            )
        OpeningBalance.objects.bulk_create(opening_balances)

    # noinspection PyMethodMayBeStatic
    def create_assets_additions(self, additions_to_create):
        if not additions_to_create:
            return

        additions = []
        for fixed_asset, addition in additions_to_create.items():
            additions.append(
                Addition(
                    fixed_asset=fixed_asset,
                    name=fixed_asset.name,
                    date=addition['date'],
                    amount=addition['amount']
                )
            )
        Addition.objects.bulk_create(additions)

    # noinspection PyMethodMayBeStatic
    def create_assets_locations(self, location_to_add):
        if not location_to_add:
            return
        locations = []
        for fixed_asset, location in location_to_add.items():
            locations.append(
                Location(
                    fixed_asset=fixed_asset,
                    branch=location['branch'],
                    percentage=location['percentage']
                )
            )
        Location.objects.bulk_create(locations)

    # noinspection PyMethodMayBeStatic
    def create_depreciation_splits(self, depreciation_splits, projects_splits, assets_numbers):

        if depreciation_splits:
            DepreciationSplit.objects.bulk_create(depreciation_splits)

        splits = {
            (split.fixed_asset, split.object_item, split.percentage): split
            for split in DepreciationSplit.objects.select_related('fixed_asset', 'object_item').filter(get_field_iexact_in(field='fixed_asset__own_asset_number', values=assets_numbers))
        }

        projects_to_add = []
        for split_key, depreciation_projects_split in projects_splits.items():
            depreciation_split = splits.get(split_key)
            for project in depreciation_projects_split:
                projects_to_add.append(
                    DepreciationSplit(
                        parent=depreciation_split,
                        **project,
                    )
                )

        if projects_to_add:
            DepreciationSplit.objects.bulk_create(projects_to_add)

    # noinspection PyMethodMayBeStatic
    def add_depreciation_split(self, fixed_asset, depreciation_lines, depreciation_slit):
        projects = []
        for project_object_item, percentage in depreciation_lines.items():
            if isinstance(project_object_item, ObjectPosting):
                for object_posting in project_object_item.object_splitting.all():
                    object_posting_percentage = round((float(percentage) / 100) * float(object_posting.percentage), 2)
                    projects.append(
                        {
                            'fixed_asset': fixed_asset,
                            'object_item': object_posting.object_item,
                            'percentage': object_posting_percentage
                        }
                    )
            else:
                projects.append(
                    {
                        'fixed_asset': fixed_asset,
                        'object_item': project_object_item,
                        'percentage': percentage
                    }
                )

        return projects
