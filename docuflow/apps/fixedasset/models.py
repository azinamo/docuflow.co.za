import logging

from enumfields import EnumIntegerField, EnumField
from django.db import models
from django.db.models import Sum, Q, Count, Case, When, Subquery
from django.utils.translation import gettext_lazy as _
from safedelete.models import SafeDeleteModel, SOFT_DELETE

from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.common.behaviours import Timestampable
from . import enums

logger = logging.getLogger(__name__)


class Category(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey('company.Company', related_name='fixed_asset_categories', on_delete=models.PROTECT)
    category = models.ForeignKey('system.FixedAssetCategory', null=True, related_name='sub_categories', on_delete=models.PROTECT)
    name = models.CharField(max_length=255)
    account = models.ForeignKey('company.Account', related_name='fixed_asset_categories', verbose_name='Asset',
                                on_delete=models.PROTECT)
    depreciation_method = EnumIntegerField(enums.DepreciationMethod)
    depreciation_account = models.ForeignKey('company.Account', on_delete=models.PROTECT)
    depreciation_rate = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12,
                                            verbose_name='Depreciation Rate (%)',)
    accumulated_depreciation_account = models.ForeignKey(
        'company.Account', null=True, blank=True, related_name='acc_depreciation_accounts',
        verbose_name='Accumulated Depreciation', on_delete=models.PROTECT
    )
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

    @property
    def method_name(self):
        return str(self.depreciation_method)

    @property
    def depreciation_rate_percentage(self):
        return f"{self.depreciation_rate}%"

    @property
    def is_section_b(self):
        return self.depreciation_method == enums.DepreciationMethod.SECTION_12_B

    @property
    def is_section_c(self):
        return self.depreciation_method == enums.DepreciationMethod.SECTION_12_E


class FixedAssetQueryset(models.QuerySet):

    def with_year_depreciation(self):
        return self.annotate(
            adjustments=Sum('depreciation_adjustments__adjustment'),
            depreciation_value=Sum('depreciation_updates__depreciation_value')
        )

    def with_year_addition(self):
        return self.annotate(year_addition=Sum('additions__amount'))

    def with_closing_balance(self):
        return self.annotate(closing_balance=Sum('additions__amount'))

    def disposed_previous_years(self, year):
        return self.annotate(
            disposed=Case(When(
                fixed_asset_disposal__disposal__period__period_year__year__lt=year.year,
                then=True
            ), output_field=models.BooleanField())
        )

    def with_opening_balance(self, year):
        return self.annotate(
            opening_value=Subquery(
                OpeningBalance.objects.filter(year=year, fixed_asset_id=models.OuterRef('id')).values_list('asset_value')[:1]
            ),
            opening_depreciation=Subquery(
                OpeningBalance.objects.filter(year=year, fixed_asset_id=models.OuterRef('id')).values_list('depreciation_value')[:1]
            ),
            opening_balance_count=Count('opening_balances', filter=Q(opening_balances__year=year), output_field=models.IntegerField())
        )

    def not_disposed(self):
        return self.annotate(
            disposed_count=Count('fixed_asset_disposal')
        ).filter(disposed_count=0)

    def net_book_value(self):
        return self.annotate()

    def active(self):
        return self.annotate()


class FixedAssetManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(deleted__isnull=True)

    def for_year(self, company, year):
        return self.with_opening_balance(year=year).with_year_depreciation().disposed_previous_years(
            year=year
        ).annotate(
            year_additions=Sum('additions__amount')
        ).select_related(
            'year', 'category', 'category__category', 'category__account', 'depreciation_account',
            'category__accumulated_depreciation_account', 'asset_account', 'accumulated_depreciation_account',
            'invoice_account', 'invoice_account__invoice', 'journal_line', 'journal_line__journal'
        ).prefetch_related(
            models.Prefetch('depreciation_updates',
                            queryset=FixedAssetUpdate.objects.filter(period__period_year=year)),
            models.Prefetch('depreciation_adjustments',
                            queryset=FixedAssetAdjustment.objects.filter(depreciation_adjustment__period__period_year=year)),
            models.Prefetch('fixed_asset_disposal',
                            queryset=FixedAssetDisposal.objects.filter(disposal__period__period_year=year)),
            models.Prefetch('additions',
                            queryset=Addition.objects.select_related(
                                'invoice_account', 'invoice_account__invoice', 'journal_line', 'journal_line__journal'
                            ).filter(date__year=year.year)),
            models.Prefetch('additions',
                            queryset=Addition.objects.select_related(
                                'invoice_account', 'invoice_account__invoice', 'journal_line', 'journal_line__journal'
                            ),
                            to_attr='asset_additions'),
        ).filter(
            company=company, disposed__isnull=True, opening_balance_count__gt=0
        )

    def get_for_year(self, company, year):
        return self.select_related(
            'year', 'category', 'category__account', 'depreciation_account'
        ).prefetch_related(
            'depreciation_updates', 'fixed_asset_disposal', 'depreciation_adjustments'
        ).filter(
            company=company, year=year
        ).annotate(
            adjustments=Sum('depreciation_adjustments__adjustment'),
            depreciation_value=Sum('depreciation_updates__depreciation_value')
        ).order_by('date_purchased', 'name')


class FixedAsset(SafeDeleteModel, Timestampable):
    _safedelete_policy = SOFT_DELETE
    company = models.ForeignKey('company.Company', on_delete=models.DO_NOTHING, related_name='fixed_assets')
    year = models.ForeignKey('period.Year', null=True, blank=True, on_delete=models.DO_NOTHING, related_name='year_fixed_assets')
    name = models.CharField(max_length=255)
    category = models.ForeignKey(Category, null=True, on_delete=models.DO_NOTHING, related_name='fixed_assets')
    cost_price = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    net_book_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    date_purchased = models.DateField()
    fixed_asset_id = models.PositiveIntegerField(null=True)
    asset_number = models.CharField(max_length=255, null=True, blank=True)
    own_asset_number = models.CharField(max_length=255, null=True, blank=True)
    supplier = models.CharField(max_length=255, null=True, blank=True)
    location = models.CharField(max_length=255, null=True, blank=True)
    quantity = models.PositiveIntegerField(null=True, blank=True, default=1)
    disposal = models.ForeignKey('FixedAssetDisposal', null=True, blank=True, on_delete=models.DO_NOTHING)
    asset_account = models.ForeignKey('company.Account', null=True, blank=True, related_name='assets',
                                      verbose_name='Asset', on_delete=models.DO_NOTHING)
    depreciation_method = EnumIntegerField(enums.DepreciationMethod, blank=True, null=True)
    depreciation_account = models.ForeignKey('company.Account', null=True, blank=True, related_name='depreciation_account_assets', on_delete=models.DO_NOTHING)
    depreciation_rate = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Depreciation Rate (%)',)
    accumulated_depreciation_account = models.ForeignKey('company.Account', null=True, blank=True, verbose_name='Accumulated Depreciation', related_name='accumulated_depreciation_assets', on_delete=models.DO_NOTHING)
    invoice_account = models.ForeignKey('invoice.InvoiceAccount', null=True, blank=True, related_name='fixed_asset', on_delete=models.SET_NULL)
    journal_line = models.OneToOneField('journals.JournalLine', null=True, related_name='fixed_asset',
                                        on_delete=models.SET_NULL)
    parent_asset = models.ForeignKey('self', null=True, blank=True, related_name='child_asset', on_delete=models.SET_NULL)
    is_copy = models.BooleanField(default=False)
    opening_asset_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    opening_depreciation_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    status = EnumIntegerField(enums.FixedAssetStatus, default=enums.FixedAssetStatus.ACTIVE)
    object_items = models.ManyToManyField('company.ObjectItem')
    postings = models.ManyToManyField('accountposting.ObjectPosting')

    objects = FixedAssetManager.from_queryset(FixedAssetQueryset)()

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk or not self.fixed_asset_id:
            fixed_asset_count = FixedAsset.objects.filter(company=self.company).count()
            self.fixed_asset_id = fixed_asset_count + 1
        return super(FixedAsset, self).save(*args, **kwargs)

    @property
    def is_pending_update(self):
        return self.status == enums.FixedAssetStatus.PENDING_UPDATE

    @property
    def is_pending(self):
        return self.status == enums.FixedAssetStatus.PENDING

    @property
    def is_reducing_balance(self):
        return self.depreciation_method == enums.DepreciationMethod.REDUCING_BALANCE

    @property
    def is_straight_line(self):
        return self.depreciation_method == enums.DepreciationMethod.STRAIGHT_LINE

    @property
    def is_section_12_e(self):
        return self.depreciation_method == enums.DepreciationMethod.SECTION_12_E

    @property
    def asset_number_str(self):
        return f"{self.company.slug}-{self.fixed_asset_id}"

    @property
    def depreciation_rate_percentage(self):
        return f"{self.depreciation_rate}%"

    @property
    def method_name(self):
        return str(self.depreciation_method)

    @property
    def label(self):
        if self.invoice_account:
            return str(self.invoice_account.invoice)
        return ''

    def get_total_additions(self, year):
        if year.start_date <= self.date_purchased <= year.end_date:
            return self.cost_price
        return 0

    def get_total_disposals(self):
        asset_disposals = 0
        depreciation_disposal = 0
        disposals = self.fixed_asset_disposal
        if disposals.count() > 0:
            disposal = disposals.first()
            if disposal:
                asset_disposals = disposal.asset_value
                depreciation_disposal = disposal.depreciation_value
        return asset_disposals, depreciation_disposal

    # noinspection PyMethodMayBeStatic
    def asset_closing_balance(self, opening_asset_balance, asset_additions, asset_disposals):
        return opening_asset_balance + asset_additions - asset_disposals

    def get_rate(self):
        return self.depreciation_rate / 100

    def calculate_period_depreciation(self, period, net_book_value, closing_net_book_value, closing_balance):
        depreciation = 0
        if self.depreciation_rate:
            if net_book_value > 1:
                if self.depreciation_method == enums.DepreciationMethod.REDUCING_BALANCE:
                    depreciation = (closing_net_book_value * self.get_rate()) / self.year.number_of_periods
                    if depreciation < net_book_value:
                        return depreciation
                    else:
                        return net_book_value - 1
                elif self.depreciation_method == enums.DepreciationMethod.STRAIGHT_LINE:
                    depreciation = (closing_balance * self.get_rate()) / self.year.number_of_periods
                    if depreciation < net_book_value:
                        return depreciation
                    else:
                        return net_book_value - 1
                elif self.depreciation_method == enums.DepreciationMethod.SECTION_12_E:
                    if closing_net_book_value > 0:
                        return float(closing_net_book_value)
                    else:
                        return 0
        else:
            if self.depreciation_method == enums.DepreciationMethod.SECTION_12_E:
                if closing_net_book_value > 0:
                    return float(closing_net_book_value)
                else:
                    return 0
        return round(depreciation, 2)

    def get_purchased_date_period(self, periods):
        for period in periods:
            if period.from_date <= self.date_purchased <= period.to_date:
                return period
        return None

    def get_purchased_date_year(self):
        for year in self.company.company_years.all():
            if year.start_date <= self.date_purchased <= year.end_date:
                return year.year
        return int(self.date_purchased.strftime('%Y'))

    def get_year_rate(self, year):
        purchase_year = self.get_purchased_date_year()

        if purchase_year:
            year_diff = year.year - purchase_year
        else:
            year_diff = year.year - int(self.date_purchased.strftime('%Y'))

        if year_diff == 0:
            return 50
        elif year_diff == 1:
            return 30
        elif year_diff == 2:
            return 20
        return 0

    def calculate_section_12_b(self, year, periods, period, net_book_value):
        year_purchased = self.get_purchased_date_year()
        year_diff = int(period.from_date.strftime('%Y')) - int(year_purchased)
        rate = 0
        depreciation = 0
        number_of_periods = 0
        if year_diff == 0:
            rate = 50 / 100
            purchase_period = self.get_purchased_date_period(periods)
            number_of_periods = self.get_period_left(year, purchase_period)
        elif year_diff == 1:
            rate = 30 / 100
            number_of_periods = self.year.number_of_periods
        elif year_diff == 2:
            rate = 20 / 100
            number_of_periods = self.year.number_of_periods
        if rate > 0:
            depreciation = (float(self.cost_price) * rate) / number_of_periods
            if depreciation < net_book_value:
                return depreciation
            else:
                return net_book_value - 1
        return depreciation

    def calculate_section_12_e(self, closing_net_book_value, net_book_value):
        if closing_net_book_value > 0:
            depreciation = float(closing_net_book_value)
            if depreciation < net_book_value:
                return depreciation
            else:
                return net_book_value - 1
        else:
            return 0

    def get_period_left(self, year, period):
        return (year.number_of_periods - period.period) + 1

    def calculate_total_depreciation(self):
        total = 0
        for period_update in self.depreciation_updates.all():
            total += period_update.depreciation_value
        total += self.get_total_adjusted_depreciation()
        return total

    def get_total_adjusted_depreciation(self):
        adjustments = self.depreciation_adjustments.all()
        total = 0
        for adjustment in adjustments:
            total += adjustment.adjustment
        return total

    # def calculate_total_depreciation(self):
    #     depreciation = self.depreciation_updates.aggregate(Sum('depreciation_value'))
    #     total = 0
    #     if depreciation['depreciation_value__sum']:
    #         total = depreciation['depreciation_value__sum']
    #     total += self.get_total_adjusted_depreciation()
    #     return total
    #
    # def get_total_adjusted_depreciation(self):
    #     adjustments = self.depreciation_adjustments.aggregate(Sum('adjustment'))
    #     if adjustments['adjustment__sum']:
    #         return adjustments['adjustment__sum']
    #     return 0
    #     # total = 0
    #     # for adjustment in adjustments:
    #     #     total += adjustment.adjustment
    #     # return total

    @classmethod
    def calculate_closing_balance(cls, opening_balance, additions, disposals, revaluation=0):
        return opening_balance + additions + revaluation - disposals

    @classmethod
    def calculate_closing_depreciation(cls, opening_balance, disposal, depreciation):
        return opening_balance - disposal + depreciation

    @classmethod
    def calculate_net_book_value(cls, asset_closing_balance, depreciation_closing_balance):
        return asset_closing_balance - depreciation_closing_balance

    @classmethod
    def calculate_opening_net_book_value(cls, opening_asset_balance, opening_depreciation_balance):
        return opening_asset_balance - opening_depreciation_balance

    def get_adjusted_depreciation(self):
        adjusted_depreciation = self.depreciation_adjustments.filter().first()
        if adjusted_depreciation:
            return adjusted_depreciation
        return None

    def can_depreciate(self, period):
        return self.date_purchased < period.to_date and not self.depreciation_updates.filter(period=period).exists()

    def has_all_accounts(self):
        if not self.depreciation_account:
            return False
        if not self.accumulated_depreciation_account:
            return False
        if not self.asset_account:
            return False
        return True


class FixedAssetPeriodValue(models.Model):
    fixed_asset = models.ForeignKey(FixedAsset, null=False, blank=False, on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', blank=False, null=False, on_delete=models.DO_NOTHING)
    book_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Asset Value')


class Addition(Timestampable):
    name = models.CharField(max_length=255, blank=True)
    addition_type = EnumField(enums.AdditionType, max_length=50, default=enums.AdditionType.ADDITION)
    fixed_asset = models.ForeignKey(FixedAsset, related_name='additions', on_delete=models.CASCADE)
    date = models.DateField()
    amount = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    invoice_account = models.OneToOneField('invoice.InvoiceAccount', null=True, related_name='asset_addition',
                                           on_delete=models.SET_NULL)
    journal_line = models.OneToOneField('journals.JournalLine', null=True, related_name='asset_addition',
                                        on_delete=models.SET_NULL)

    @property
    def is_addition(self):
        return self.addition_type == enums.AdditionType.ADDITION

    @property
    def is_revaluation(self):
        return self.addition_type == enums.AdditionType.REVALUATION

    @property
    def is_disposal(self):
        return self.addition_type == enums.AdditionType.DISPOSAL


class Update(models.Model):
    company = models.ForeignKey('company.Company', related_name='assets_updates', on_delete=models.CASCADE)
    year = models.ForeignKey('period.Year', related_name='assets_updates', on_delete=models.PROTECT)
    journal_id = models.PositiveIntegerField(null=True)
    period = models.ForeignKey('period.Period', related_name="assets_updates", on_delete=models.DO_NOTHING)
    date = models.DateField(null=True)
    report_unique_number = models.CharField(max_length=255, null=True, blank=True)
    ledger = models.ForeignKey('journals.Journal', related_name="ledger_fixed_asset_journals", blank=True, null=True,
                               on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', related_name="role_fixed_asset_journals", blank=True, null=True,
                             on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Datetime')
    created_by = models.ForeignKey('accounts.Profile', null=True, blank=True,
                                   related_name='profile_fixed_asset_journals', on_delete=models.DO_NOTHING)

    def save(self, *args, **kwargs):
        if not self.pk or not self.journal_id:
            journal_count = Update.objects.filter(company=self.company, year=self.year).count()
            self.journal_id = journal_count + 1
        if not self.report_unique_number:
            text = self.get_text()
            self.report_unique_number = f"{text} {self.journal_id}"
        return super(Update, self).save(*args, **kwargs)

    def get_text(self):
        return 'Depreciation Report - '

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Invoice Journal')
        verbose_name_plural = _('Invoice Journals')

    def __str__(self):
        return f"Depreciation Report - {self.journal_id}"


class FixedAssetUpdate(models.Model):
    # TODO - Refactor the fields and add tests
    fixed_asset = models.ForeignKey(FixedAsset, null=False, blank=False, on_delete=models.CASCADE, related_name='depreciation_updates')
    asset_update = models.ForeignKey(Update, blank=False, null=False, on_delete=models.DO_NOTHING, related_name='fixed_assets')
    period = models.ForeignKey('period.Period', related_name="fixed_assets_updates", on_delete=models.PROTECT)
    asset_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Asset Value')
    depreciation_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12,
                                             verbose_name='Depreciation Value')
    book_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12,
                                     verbose_name='Net Book Value')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Disposal(models.Model):
    company = models.ForeignKey('company.Company', blank=True, null=True, on_delete=models.DO_NOTHING, related_name='company_disposals')
    period = models.ForeignKey('period.Period', blank=True, null=True, on_delete=models.DO_NOTHING, related_name='period_disposals')
    invoice_number = models.CharField(max_length=255, null=True, blank=True)
    selling_price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Selling Price')
    vat_code = models.ForeignKey('company.VatCode', blank=True, null=True, on_delete=models.DO_NOTHING, related_name='vat_code_disposals')
    net_profit_loss = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Net Profit/Loss')
    disposal_date = models.DateTimeField(null=True, blank=True, verbose_name='Date of Disposal')
    role = models.ForeignKey('accounts.Role', blank=True, null=True, on_delete=models.DO_NOTHING, related_name='role_disposals')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class FixedAssetDisposal(models.Model):
    fixed_asset = models.ForeignKey(FixedAsset, null=False, blank=False, on_delete=models.CASCADE, related_name='fixed_asset_disposal')
    disposal = models.ForeignKey('fixedasset.Disposal', blank=True, null=True, on_delete=models.DO_NOTHING, related_name='disposed_fixed_assets')
    asset_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Asset Value')
    depreciation_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Depreciation Value')
    book_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Net Book Value')
    invoice_item = models.OneToOneField('sales.InvoiceItem', null=True, blank=True, related_name='disposal', on_delete=models.DO_NOTHING)


class DepreciationAdjustment(models.Model):
    company = models.ForeignKey('company.Company', null=True, blank=True, on_delete=models.CASCADE, related_name='depreciation_adjustments')
    year = models.ForeignKey('period.Year', blank=False, null=False, on_delete=models.DO_NOTHING, related_name='depreciation_adjustments')
    period = models.ForeignKey('period.Period', blank=True, null=True, on_delete=models.DO_NOTHING, related_name='depreciation_adjustments')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class FixedAssetAdjustment(models.Model):
    fixed_asset = models.ForeignKey(FixedAsset, null=False, blank=False, on_delete=models.CASCADE,
                                    related_name='depreciation_adjustments')
    depreciation_adjustment = models.ForeignKey(DepreciationAdjustment, null=False, blank=False,
                                                on_delete=models.CASCADE, related_name='fixed_assets')
    opening_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12,
                                        verbose_name='Opening Value')
    closing_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12,
                                        verbose_name='Closing Value')
    adjustment = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12, verbose_name='Adjustment')
    reason = models.TextField(null=True, blank=True)


class OpeningBalance(models.Model):
    year = models.ForeignKey('period.Year', related_name='fixed_assets_opening_balances', on_delete=models.CASCADE)
    fixed_asset = models.ForeignKey(FixedAsset, related_name='opening_balances', on_delete=models.CASCADE)
    asset_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    depreciation_value = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('year', 'fixed_asset')


class Location(models.Model):
    fixed_asset = models.ForeignKey(FixedAsset, related_name='locations', on_delete=models.CASCADE)
    branch = models.ForeignKey('company.Branch', related_name='fixed_assets_locations', on_delete=models.PROTECT)
    percentage = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('fixed_asset', 'branch')


class DepreciationSplit(models.Model):
    fixed_asset = models.ForeignKey(FixedAsset, related_name='object_depreciations', on_delete=models.CASCADE)
    object_item = models.ForeignKey('company.ObjectItem', null=True, on_delete=models.SET_NULL)
    object_posting = models.ForeignKey('accountposting.ObjectPosting', null=True, on_delete=models.SET_NULL)
    parent = models.ForeignKey('self', null=True, related_name='children', on_delete=models.CASCADE)
    percentage = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('fixed_asset', 'object_item', 'object_posting', 'parent')

    def __str__(self):
        return self.object_item.code if self.object_item else self.object_posting.name

    @property
    def description(self):
        return self.object_item.label if self.object_item else self.object_posting.name


class Warranty(models.Model):
    fixed_asset = models.ForeignKey(FixedAsset, related_name='warranties', on_delete=models.CASCADE)
    supplier = models.CharField(max_length=255)
    period = models.CharField(max_length=255, verbose_name='Warranty period')
    end_date = models.DateField(verbose_name='End of warranty date')
    created_at = models.DateTimeField(auto_now_add=True)

