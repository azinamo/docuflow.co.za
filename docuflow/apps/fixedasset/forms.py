import logging
from django.db import transaction

from django import forms
from django.urls import reverse_lazy

from docuflow.apps.accountposting.models import ObjectPosting
from docuflow.apps.company.models import Account, ObjectItem
from docuflow.apps.invoice.models import Comment, InvoiceAccount
from docuflow.apps.period.models import Period, Year
from docuflow.apps.period.enums import PeriodStatus
from .enums import FixedAssetStatus, DepreciationMethod, AdditionType
from .models import FixedAsset, Disposal, OpeningBalance, Addition, Category
from .utils import ImportFixedAssetJob

NEW_ASSET = 1
ADDITION = 2
REVALUATION = 3
DISPOSAL = 4

logger = logging.getLogger(__name__)


class FixedAssetForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.request = kwargs.pop('request')
        self.year = kwargs.pop('year')
        super(FixedAssetForm, self).__init__(*args, **kwargs)
        self.fields['depreciation_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['asset_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['accumulated_depreciation_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['category'].queryset = self.fields['category'].queryset.filter(company=self.company)

    class Meta:
        model = FixedAsset
        fields = ['name', 'category', 'cost_price', 'date_purchased', 'own_asset_number', 'supplier',
                  'quantity', 'location', 'depreciation_account', 'depreciation_method', 'depreciation_rate',
                  'accumulated_depreciation_account', 'asset_account']
        widgets = {
            'category': forms.Select(attrs={'class': 'chosen-select',
                                            'data-ajax-url': reverse_lazy('asset_category_defaults')}),
            'date_purchased': forms.TextInput(attrs={'class': 'datepicker'}),
        }

    def save(self, commit=True):
        fixed_asset = super().save(commit=False)
        fixed_asset.company = self.company
        fixed_asset.save()
        if not fixed_asset.year:
            fixed_asset.year = self.year
            fixed_asset.save()
        self.save_m2m()

        OpeningBalance.objects.get_or_create(
            year=fixed_asset.year,
            fixed_asset=fixed_asset,
            defaults={
              'asset_value': 0,
              'depreciation_value': 0
            }
        )

        Addition.objects.get_or_create(
            fixed_asset=fixed_asset,
            defaults={
              'date': self.cleaned_data['date_purchased'],
              'name': self.cleaned_data['name'],
              'amount': self.cleaned_data['cost_price'],
            }
        )

        posted_object_items = self.request.POST.getlist('object_item')
        fixed_asset.object_items.clear()
        fixed_asset.postings.clear()
        for object_item_id in posted_object_items:
            object_item_str = str(object_item_id)
            if len(object_item_str) > 0:
                if object_item_str.startswith('object-posting', 0, 14):
                    object_posting_id = object_item_str[15:]
                    if object_posting_id:
                        object_posting = ObjectPosting.objects.get(pk=int(object_posting_id))
                        if object_posting:
                            fixed_asset.postings.add(object_posting)
                else:
                    object_item = ObjectItem.objects.get(pk=int(object_item_id))
                    if object_item:
                        fixed_asset.object_items.add(object_item)
        return fixed_asset


class FixedAssetAdminForm(forms.ModelForm):

    class Meta:
        model = FixedAsset
        fields = ['company', 'name', 'category', 'cost_price', 'date_purchased', 'own_asset_number', 'supplier',
                  'quantity', 'location', 'depreciation_method', 'depreciation_rate', 'status']

    def save(self, commit=True):
        fixed_asset = super().save(commit=False)
        year = Year.objects.started().filter(company=fixed_asset.company).first()
        if not year:
            year = Year.objects.filter(is_active=True, company=fixed_asset.company).first()
        if not year:
            year = Year.objects.filter(company=fixed_asset.company).first()
        fixed_asset.year = year
        fixed_asset.save()

        self.save_m2m()

        if not fixed_asset.opening_balances.exists():
            OpeningBalance.objects.get_or_create(
                year=year,
                fixed_asset=fixed_asset,
                defaults={
                  'asset_value': 0,
                  'depreciation_value': 0
                }
            )

        if not fixed_asset.additions.exists():
            Addition.objects.get_or_create(
                date=self.cleaned_data['date_purchased'],
                fixed_asset=fixed_asset,
                name=self.cleaned_data['name'],
                defaults={
                  'amount': self.cleaned_data['cost_price'],
                }
            )

        if fixed_asset.own_asset_number:
            logger.info(f"Own asset number {fixed_asset.own_asset_number}")
            invoice_number = fixed_asset.own_asset_number.split('-')
            if len(invoice_number) == 2:
                invoice_account = InvoiceAccount.objects.not_linked_to_asset().filter(
                    invoice__company=fixed_asset.company,
                    invoice__invoice_id=invoice_number[1]
                ).first()
                logger.info(f"invoice_account {invoice_account}")
                if invoice_account:
                    logger.info(f"Enter Fixed Asset {fixed_asset}")
                    if not fixed_asset.invoice_account:
                        fixed_asset.invoice_account = invoice_account
                        fixed_asset.save()

                    addition = Addition.objects.filter(invoice_account__isnull=True, fixed_asset=fixed_asset).first()
                    logger.info(f"Enter Addition {addition}")
                    if addition and not addition.invoice_account:
                        addition.invoice_account = invoice_account
                        addition.save()

                    logger.info(f"Enter Fixed asset number {fixed_asset.asset_number}")
                    if not fixed_asset.asset_number:
                        fixed_asset.asset_number = fixed_asset.own_asset_number
                        fixed_asset.save()

        return fixed_asset


class AssetCategoryDefaultForm(forms.Form):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(AssetCategoryDefaultForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['depreciation_account'].queryset = Account.objects.filter(company=company)
            self.fields['asset_account'].queryset = Account.objects.filter(company=company)
            self.fields['accumulated_depreciation_account'].queryset = Account.objects.filter(company=company)

    depreciation_account = forms.ModelChoiceField(required=False, label='Depreciation Account', queryset=None)
    depreciation_method = forms.ChoiceField(required=False, label='Depreciation Method',
                                            choices=DepreciationMethod.choices())
    depreciation_rate = forms.DecimalField(required=False, label='Depreciation Rate')
    asset_account = forms.ModelChoiceField(required=False, label='Asset Account', queryset=None)
    accumulated_depreciation_account = forms.ModelChoiceField(required=False, label='Accumulated Depreciation Account',
                                                              queryset=None)

    class Meta:
        model = FixedAsset
        fields = ['depreciation_account', 'depreciation_method', 'depreciation_rate', 'asset_account',
                  'accumulated_depreciation_account']
        widgets = {
            'depreciation_account': forms.Select(attrs={'class': 'chosen-select'}),
        }

    def clean(self):
        cleaned_data = super(AssetCategoryDefaultForm, self).clean()
        return cleaned_data


class BaseAssetForm(forms.ModelForm):

    asset = forms.ModelChoiceField(queryset=None, required=False)
    category = forms.ModelChoiceField(queryset=None, required=False, widget=forms.Select(attrs={
        'data-ajax-url': reverse_lazy('fixed_asset_list')}))
    asset_option = forms.ChoiceField(
        choices=((NEW_ASSET, NEW_ASSET), ) + AdditionType.choices(), widget=forms.HiddenInput()
    )

    class Meta:
        model = FixedAsset
        fields = ['name', 'own_asset_number', 'supplier', 'quantity', 'location', 'asset_account',
                  'depreciation_account', 'depreciation_method', 'depreciation_rate',
                  'accumulated_depreciation_account', 'category', 'year', 'asset']
        widgets = {
            'date_purchased': forms.TextInput(attrs={'class': 'datepicker'}),
            'category': forms.HiddenInput(),
            'year': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()
        asset_option = cleaned_data.get('asset_option')
        fixed_asset = cleaned_data.get('asset')
        name = cleaned_data.get('name')
        additions = [AdditionType.ADDITION.value, AdditionType.REVALUATION.value, AdditionType.DISPOSAL.value]
        if (asset_option and asset_option in additions) and not fixed_asset:
            self.add_error('asset', 'Fixed asset is required')
        if not name:
            self.add_error('name', 'Name is required')

        return cleaned_data


class InvoiceAssetForm(BaseAssetForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.invoice_account = kwargs.pop('invoice_account')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(InvoiceAssetForm, self).__init__(*args, **kwargs)

        assets_qs = FixedAsset.objects.filter(company=self.company)
        categories_qs = Category.objects.filter(company=self.company)
        self.fields['category'].queryset = categories_qs
        self.fields['category'].empty_label = None

        fixed_asset_category = categories_qs.first()

        if 'category' in self.data:
            assets_qs = assets_qs.filter(category=self.data['category'])
        elif 'category' in self.initial:
            assets_qs = assets_qs.filter(category=self.initial['category'])
        elif fixed_asset_category:
            assets_qs = assets_qs.filter(category=fixed_asset_category)

        self.fields['depreciation_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['asset_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['accumulated_depreciation_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['asset'].queryset = assets_qs

    def save(self, commit=True):
        fixed_asset = self.cleaned_data.get('asset')
        if fixed_asset:
            addition, _ = Addition.objects.update_or_create(
                fixed_asset=fixed_asset,
                invoice_account=self.invoice_account,
                defaults={
                    'date': self.invoice_account.invoice.accounting_date,
                    'name': self.cleaned_data.get('name', fixed_asset.name),
                    'amount': self.invoice_account.amount
                }
            )
            return addition
        else:
            quantity = self.cleaned_data['quantity']
            amount = self.invoice_account.amount
            asset_amount = amount / quantity
            for i in range(0, quantity):
                fixed_asset = FixedAsset.objects.create(
                    name=self.cleaned_data['name'],
                    own_asset_number=self.cleaned_data['own_asset_number'],
                    supplier=self.invoice_account.invoice.supplier.name,
                    quantity=1,
                    location=self.cleaned_data['location'],
                    asset_account=self.cleaned_data['asset_account'],
                    depreciation_account=self.cleaned_data['depreciation_account'],
                    depreciation_method=self.cleaned_data['depreciation_method'],
                    depreciation_rate=self.cleaned_data['depreciation_rate'],
                    accumulated_depreciation_account=self.cleaned_data['accumulated_depreciation_account'],
                    category=self.cleaned_data['category'],
                    year=self.cleaned_data['year'],
                    company=self.company,
                    status=FixedAssetStatus.PENDING,
                    date_purchased=self.invoice_account.invoice.accounting_date,
                    asset_number=str(self.invoice_account.invoice),
                    invoice_account=self.invoice_account,
                    cost_price=asset_amount
                )

                OpeningBalance.objects.get_or_create(
                    year=fixed_asset.year,
                    fixed_asset=fixed_asset,
                    defaults={
                      'asset_value': 0,
                      'depreciation_value': 0
                    }
                )
                Addition.objects.update_or_create(
                    date=self.invoice_account.invoice.accounting_date,
                    fixed_asset=fixed_asset,
                    defaults={
                        'name': fixed_asset.name,
                        'amount': asset_amount
                    }
                )

                Comment.objects.create(
                    invoice=self.invoice_account.invoice,
                    log_type=Comment.EVENT,
                    participant=self.profile.user,
                    role=self.role,
                    note='Fixed asset created',
                    status=self.invoice_account.invoice.status,
                    link_url=reverse_lazy('show_fixed_asset', kwargs={'pk': fixed_asset.id})
                )

                posted_object_items = self.invoice_account.object_items
                if posted_object_items.exists():
                    object_items = ObjectItem.objects.filter(id__in=[object_item_id for object_item_id in posted_object_items])
                    fixed_asset.object_items.set(object_items)


class JournalAssetForm(BaseAssetForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.account = kwargs.pop('account')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.year = kwargs.pop('year')
        super(JournalAssetForm, self).__init__(*args, **kwargs)
        assets_qs = FixedAsset.objects.filter(company=self.company)
        categories_qs = Category.objects.filter(company=self.company)
        self.fields['category'].queryset = categories_qs
        self.fields['category'].empty_label = None

        fixed_asset_category = categories_qs.first()
        if 'category' in self.data:
            assets_qs = assets_qs.filter(category=self.data['category'])
        elif 'category' in self.initial:
            assets_qs = assets_qs.filter(category=self.initial['category'])
        elif fixed_asset_category:
            assets_qs = assets_qs.filter(category=fixed_asset_category)
        self.fields['depreciation_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['asset_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['accumulated_depreciation_account'].queryset = Account.objects.filter(company=self.company)
        self.fields['asset'].queryset = assets_qs
    credit = forms.DecimalField(required=False, widget=forms.HiddenInput())
    debit = forms.DecimalField(required=False, widget=forms.HiddenInput())
    date = forms.DateField(required=False, widget=forms.HiddenInput())

    class Meta(InvoiceAssetForm.Meta):
        model = FixedAsset
        fields = InvoiceAssetForm.Meta.fields + ['date', 'debit', 'credit']
        widgets = {
            'date_purchased': forms.TextInput(attrs={'class': 'datepicker'}),
            'category': forms.HiddenInput(),
            'year': forms.HiddenInput()
        }

    def save(self, commit=True):
        fixed_asset = self.cleaned_data.get('asset')
        value = self.cleaned_data['debit'] or self.cleaned_data['credit']
        return {
            'date': self.cleaned_data['date'].strftime('%Y-%m-%d'),
            'fixed_asset': fixed_asset.id,
            'name': self.cleaned_data.get('name', fixed_asset.name),
            'value': str(value),
            'year': self.year.id,
            'asset_option': self.data['asset_option'],
            'category': self.cleaned_data['category'].id,
            'own_asset_number': self.cleaned_data['own_asset_number'],
            'location': self.cleaned_data['location'],
            'quantity': self.cleaned_data['quantity'],
            'asset_account': self.cleaned_data['asset_account'].id if self.cleaned_data['asset_account'] else None,
            'accumulated_depreciation_account': self.cleaned_data['accumulated_depreciation_account'].id if self.cleaned_data['accumulated_depreciation_account'] else None,
            'depreciation_account': self.cleaned_data['depreciation_account'].id if self.cleaned_data['depreciation_account'] else None,
            'depreciation_method': self.cleaned_data['depreciation_method'] if self.cleaned_data['depreciation_method'] else None,
            'depreciation_rate': str(self.cleaned_data['depreciation_rate']) if self.cleaned_data['depreciation_rate'] else None
        }


class AssetDisposalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        super(AssetDisposalForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['period'].queryset = self.fields['period'].queryset.filter(
                company=company, status=PeriodStatus.OPEN, period_year__is_active=True
            ).order_by('period')
            self.fields['vat_code'].queryset = self.fields['vat_code'].queryset.filter(company=company)

    selling_price = forms.DecimalField(required=True, label='Selling Price')
    invoice_number = forms.CharField(required=True, label='Invoice Number')

    class Meta:
        model = Disposal
        fields = ['period', 'invoice_number', 'disposal_date', 'net_profit_loss', 'vat_code', 'selling_price']
        widgets = {
            'disposal_date': forms.TextInput(attrs={'class': 'datepicker'}),
        }

    def clean(self):
        cleaned_data = super(AssetDisposalForm, self).clean()
        disposal_date = cleaned_data['disposal_date']
        if disposal_date:
            if 'period' in cleaned_data and cleaned_data['period']:
                period = cleaned_data['period']
                if not period.is_valid(disposal_date.date()):
                    self.add_error('disposal_date', 'Period and disposal date do not match.')
        return cleaned_data


class DepreciationAdjustmentForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super(DepreciationAdjustmentForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(company=self.company, year=self.year).order_by('period')

    period = forms.ModelChoiceField(queryset=None, label='Period', required=True)


class ImportFixedAssetForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super(ImportFixedAssetForm, self).__init__(*args, **kwargs)

    file = forms.FileField(required=True)

    def clean(self):
        file = self.cleaned_data['file']
        if not (file.name.endswith('.csv') or file.name.endswith('.xls') or file.name.endswith('.xlsx')):
            self.add_error('file', 'File is not .csv, .xlsx or xls type')

        if file.multiple_chunks():
            self.add_error('file', "Uploaded file is too big (%.2f MB)." % (file.size / (1000 * 1000),))

    def execute(self):
        with transaction.atomic():
            sync_fixed_asset = ImportFixedAssetJob(company=self.company, year=self.year, file=self.cleaned_data['file'])
            return sync_fixed_asset.execute()
