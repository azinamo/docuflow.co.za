import collections
import datetime
import logging
import pprint
import xlwt
from openpyxl import Workbook
from datetime import timedelta
from decimal import Decimal

from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as __
from django.views.generic import FormView, View, TemplateView
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.common import utils
from docuflow.apps.common.enums import Module
from docuflow.apps.accounts.enums import DocuflowPermission
from docuflow.apps.common.mixins import ProfileMixin, DocuflowPermissionMixin
from docuflow.apps.company.models import Account
from docuflow.apps.fixedasset.models import OpeningBalance, Category
from docuflow.apps.fixedasset.enums import AdditionType
from docuflow.apps.invoice.models import InvoiceAccount, Invoice
from docuflow.apps.journals.models import Journal, JournalLine
from docuflow.apps.journals.services import get_total_report_type
from docuflow.apps.period.models import Period
from docuflow.apps.system.models import ReportType
from .forms import (FixedAssetForm, AssetCategoryDefaultForm, InvoiceAssetForm, DepreciationAdjustmentForm,
                    JournalAssetForm, ImportFixedAssetForm)
from .models import FixedAsset, Update, DepreciationAdjustment, FixedAssetAdjustment
from .services import generate_fixed_asset_register
from .utils import FixedAssetImportException
from ..invoice.services import recalculate_posting_vat

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class AssetRegisterView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'fixedasset/register.html'
    context_object_name = 'fixed_assets'
    # permission_required = DocuflowPermission.CAN_VIEW_FIXED_ASSETS.value

    # noinspection PyMethodMayBeStatic
    def get_period(self, company):
        return Period.objects.filter(company=company, is_closed=False).first()

    # noinspection PyMethodMayBeStatic
    def get_period_closing_date(self, year, company):
        fixed_asset_journal = Update.objects.filter(period__period_year=year).order_by('-period__from_date').first()
        if fixed_asset_journal:
            return fixed_asset_journal.period
        else:
            periods = Period.objects.prefetch_related(
                'assets_updates'
            ).filter(period_year=year, company=company).order_by('from_date')
            for period in periods:
                if period.assets_updates.count() == 0:
                    return period
        return None

    # noinspection PyMethodMayBeStatic
    def get_first_period_date(self, year, company) -> datetime.date:
        period = Period.objects.get(company=company, period=1, period_year=year)
        return period.from_date - timedelta(days=1)

    def get_fixed_assets(self):
        return FixedAsset.objects.for_year(company=self.get_company(), year=self.get_year()).exclude(
            Q(category__isnull=True) | Q(category__category__isnull=True)
        ).order_by('category__category__name', 'category__name', 'name').all()

    def get_context_data(self, **kwargs):
        context = super(AssetRegisterView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        last_journaled_period = self.get_period_closing_date(year, company)
        first_period = self.get_first_period_date(year, company)

        fixed_assets = self.get_fixed_assets()
        assets_count = len(fixed_assets)
        company_assets_count = FixedAsset.objects.active().filter(company=company).count()
        register = generate_fixed_asset_register(fixed_assets=fixed_assets, year=year)

        context['assets_count'] = assets_count
        context['company_assets_count'] = company_assets_count
        context['company'] = company
        context['year'] = year
        context['period'] = last_journaled_period
        context['first_period'] = first_period
        context['register'] = register
        return context


class FixedAssetView(LoginRequiredMixin, ProfileMixin, TemplateView):
    context_object_name = 'fixed_assets'
    template_name = 'fixedasset/index.html'

    def get_fixed_assets(self):
        return FixedAsset.objects.select_related(
            'year', 'category', 'category__account', 'category__depreciation_account',
            'category__accumulated_depreciation_account', 'depreciation_account', 'asset_account',
            'accumulated_depreciation_account', 'category__category'
        ).prefetch_related(
            'depreciation_updates', 'fixed_asset_disposal', 'depreciation_adjustments'
        ).filter(company=self.get_company()).order_by('category__category__name', 'category__name').exclude(
            Q(category__isnull=True) | Q(category__category__isnull=True)
        )

    def get_context_data(self, **kwargs):
        context = super(FixedAssetView, self).get_context_data(**kwargs)
        register = generate_fixed_asset_register(fixed_assets=self.get_fixed_assets())
        context['categories'] = register['categories']
        context['company'] = self.get_company()
        context['missing_report'] = self.request.session.get('missing_report')
        return context


class CreateFixedAssetView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = FixedAsset
    form_class = FixedAssetForm
    success_url = reverse_lazy('fixed_asset_index')
    template_name = 'fixedasset/create.html'
    success_message = __('Fixed Asset successfully saved')

    def get_context_data(self, **kwargs):
        context = super(CreateFixedAssetView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateFixedAssetView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['request'] = self.request
        return kwargs


class EditFixedAssetView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = FixedAsset
    form_class = FixedAssetForm
    template_name = 'fixedasset/edit.html'
    success_message = __('Fixed asset successfully updated')
    success_url = reverse_lazy('fixed_asset_index')
    context_object_name = 'fixed_asset'

    def get_queryset(self):
        return FixedAsset.objects.select_related(
            'category', 'company', 'year'
        ).prefetch_related('object_items', 'postings')

    def get_form_kwargs(self):
        kwargs = super(EditFixedAssetView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditFixedAssetView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context


class DeleteFixedAssetView(LoginRequiredMixin,  View):

    def get_success_url(self):
        return reverse('fixed_asset_index')

    def get(self, request, *args, **kwargs):
        fixed_asset = get_object_or_None(FixedAsset, id=self.kwargs['pk'])
        if fixed_asset:
            if fixed_asset.calculate_total_depreciation() > 0:
                messages.error(self.request, __('There is current depreciation, please clear that through the '
                                                'depreciation adjustment menu before you can delete the asset'))
            else:
                fixed_asset.delete()
                messages.success(self.request, __('Fixed asset deleted successfully'))
        return redirect(self.get_success_url())


class CategoryFixedAssetDefaultsView(ProfileMixin, FormView):
    form_class = AssetCategoryDefaultForm
    template_name = 'fixedasset/category_defaults.html'

    def get_initial(self):
        initial = super(CategoryFixedAssetDefaultsView, self).get_initial()
        asset_category = self.get_category()
        initial['accumulated_depreciation_account'] = asset_category.accumulated_depreciation_account
        initial['asset_account'] = asset_category.account
        initial['depreciation_account'] = asset_category.depreciation_account
        initial['depreciation_method'] = asset_category.depreciation_method
        initial['depreciation_rate'] = asset_category.depreciation_rate
        return initial

    def get_category(self):
        return Category.objects.get(pk=self.request.GET['category_id'])

    def get_form_kwargs(self):
        kwargs = super(CategoryFixedAssetDefaultsView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CategoryFixedAssetDefaultsView, self).get_context_data(**kwargs)
        category = self.get_category()
        context['is_section_b'] = category.is_section_b
        context['is_section_c'] = category.is_section_c
        return context


class UpdateFixedAssetCategoryView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        asset_categories = Category.objects.filter(company_id=self.request.session['company']).all()
        for asset_category in asset_categories:
            if asset_category.id == int(self.kwargs['pk']):
                asset_category.is_default = True
            else:
                asset_category.is_default = False
            asset_category.save()
        return JsonResponse({
            'text': __('Fixed asset updated successfully'),
            'error': False
        })


class DepreciationAdjustmentView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'fixedasset/depreciation_adjustments.html'
    form_class = DepreciationAdjustmentForm

    def get_form_kwargs(self):
        kwargs = super(DepreciationAdjustmentView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_opening_balances(self, company, year):
        opening_balances = OpeningBalance.objects.filter(year=year)
        account_balances = {}
        for opening_balance in opening_balances:
            account_balances[opening_balance.fixed_asset_id] = opening_balance
        return account_balances

    def get_categorized_fixed_assets(self, company, year):
        categories = self.get_categories(company)
        fixed_assets = FixedAsset.objects.for_year(company=company, year=year)

        category_fixed_assets = collections.OrderedDict()
        assets_total = 0
        depreciation_total = 0
        for category in categories:
            category_assets = collections.OrderedDict()
            total_opening_assets_value = 0
            total_opening_depreciation_value = 0
            for fixed_asset in fixed_assets:
                if fixed_asset.category_id == category.id:

                    year_depreciation = fixed_asset.calculate_total_depreciation()
                    asset_disposals, depreciation_disposal = fixed_asset.get_total_disposals()
                    opening_depreciation_balance = 0
                    if fixed_asset.opening_depreciation_value:
                        opening_depreciation_balance = fixed_asset.opening_depreciation_value

                    category_assets[fixed_asset.id] = {
                        'id': fixed_asset.id, 'asset': fixed_asset, 'opening_balance': 0, 'depreciation_value': 0
                    }
                    category_assets[fixed_asset.id]['opening_asset_value'] = fixed_asset.opening_asset_value
                    category_assets[fixed_asset.id]['opening_depreciation_value'] = fixed_asset.opening_depreciation_value

                    depreciation_closing_balance = opening_depreciation_balance - depreciation_disposal + year_depreciation

                    category_assets[fixed_asset.id]['closing_depreciation'] = depreciation_closing_balance
                    if fixed_asset.opening_asset_value:
                        total_opening_assets_value += fixed_asset.opening_asset_value
                    if fixed_asset.opening_depreciation_value:
                        total_opening_depreciation_value += fixed_asset.opening_depreciation_value
            if len(category_assets) > 0:
                category_fixed_assets[category.id] = {'assets': category_assets, 'category': category,
                                                      'total_assets_value': total_opening_assets_value,
                                                      'total_depreciation_value':  total_opening_depreciation_value
                                                      }
                assets_total += total_opening_assets_value
                depreciation_total += total_opening_depreciation_value

        return {'categories': category_fixed_assets, 'assets_total': assets_total,
                'depreciation_total': depreciation_total}

    def get_categories(self, company):
        return Category.objects.select_related(
            'account', 'depreciation_account', 'accumulated_depreciation_account'
        ).filter(company=company).order_by('name')

    def get_context_data(self, **kwargs):
        context = super(DepreciationAdjustmentView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        categorized_assets = self.get_categorized_fixed_assets(company, year=year)
        context['fixed_assets'] = categorized_assets['categories']
        context['total'] = categorized_assets['assets_total']
        context['total_depreciation'] = categorized_assets['depreciation_total']
        context['year'] = year
        return context

    def create_ledger(self, year,  period):
        return Journal.objects.create(
            company=year.company,
            module=Module.FIXED_ASSET,
            year=year,
            period=period,
            date=period.to_date,
            created_by_id=self.request.session['profile'],
            role_id=self.request.session['role'],
            journal_text='Depreciation Adjustment'
        )

    def create_ledger_line(self, journal, fixed_asset, adjustment, reason):
        text = f"{fixed_asset}"
        if reason:
            text = f"{fixed_asset} - {reason}"
        if adjustment > 0:
            depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.depreciation_account,
                debit=Decimal(adjustment),
                text=text,
                accounting_date=journal.date,
                suffix='Current Year Depreciation'
            )
            if depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    depreciation_line.object_items.add(object_item)

            accumulated_depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.accumulated_depreciation_account,
                credit=Decimal(adjustment),
                text=text,
                accounting_date=journal.date,
                suffix='Current Year Depreciation'
            )
            if accumulated_depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    accumulated_depreciation_line.object_items.add(object_item)
        else:
            depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.depreciation_account,
                credit=Decimal(adjustment) * -1,
                text=text,
                accounting_date=journal.date,
                suffix='Current Year Depreciation'
            )
            if depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    depreciation_line.object_items.add(object_item)

            accumulated_depreciation_line = JournalLine.objects.create(
                journal=journal,
                account=fixed_asset.accumulated_depreciation_account,
                debit=Decimal(adjustment) * -1,
                text=text,
                accounting_date=journal.date,
                suffix='Current Year Depreciation'
            )

            if accumulated_depreciation_line:
                for object_item in fixed_asset.object_items.all():
                    accumulated_depreciation_line.object_items.add(object_item)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Depreciation adjustments could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(DepreciationAdjustmentView, self).form_invalid(form)

    def form_valid(self, form):
        with transaction.atomic():
            company = self.get_company()
            year = self.get_year()
            period = form.cleaned_data['period']
            depreciation_adjustment = DepreciationAdjustment.objects.create(
                year=year,
                period=period,
                company=company
            )
            if depreciation_adjustment:
                ledger = self.create_ledger(year, period)
                for k, v in self.request.POST.items():
                    if k.startswith('opening_depreciation_', 0, 21):
                        fixed_asset_id = k[21:]
                        closing_str = self.request.POST.get(f'closing_depreciation_{fixed_asset_id}', None)
                        closing = 0
                        if closing_str:
                            closing = Decimal(closing_str.replace(',', ''))

                        adjustment = self.request.POST.get('depreciation_adjustment_value_{}'.format(fixed_asset_id), None)
                        reason = self.request.POST.get(f'reason_{fixed_asset_id}', None)

                        fixed_asset = FixedAsset.objects.filter(id=fixed_asset_id).first()

                        if fixed_asset and adjustment:
                            FixedAssetAdjustment.objects.create(
                                depreciation_adjustment=depreciation_adjustment,
                                reason=reason,
                                fixed_asset=fixed_asset,
                                adjustment=Decimal(adjustment),
                                opening_value=Decimal(v),
                                closing_value=Decimal(closing)
                            )
                            if ledger:
                                self.create_ledger_line(ledger, fixed_asset, float(adjustment), reason)
        return JsonResponse({
            'text': 'Asset adjustments saved successfully',
            'error': False,
            'redirect': reverse('asset_register_index')
        })


class FixedAssetListView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request):
        fixed_assets = list(FixedAsset.objects.for_year(
            company=self.get_company(), year=self.get_year()
        ).filter(category=request.GET.get('category')).values('id', 'name'))
        return JsonResponse(fixed_assets, safe=False)


class SummaryView(AssetRegisterView, TemplateView):
    template_name = 'fixedasset/summary.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.get_year()
        end_date = year.end_date
        start_date = year.start_date
        report_type = ReportType.objects.get(slug='fixed-assets')
        register = context['register']
        balance_sheet_total = get_total_report_type(
            company=self.get_company(),
            report_type=report_type,
            year=year,
            start_date=start_date,
            end_date=end_date
        )

        context['balance_sheet_total'] = balance_sheet_total
        context['is_balancing'] = register['net_book_value'] == balance_sheet_total
        return context


class CreateInvoiceFixedAssetView(ProfileMixin, CreateView):
    template_name = 'fixedasset/invoice_asset.html'
    form_class = InvoiceAssetForm
    model = FixedAsset

    def get_initial(self):
        initial = super(CreateInvoiceFixedAssetView, self).get_initial()
        invoice_account = self.get_invoice_account()
        asset_category = self.get_category(invoice_account.account)
        initial['name'] = invoice_account.comment
        initial['year'] = self.get_year().id
        initial['category'] = asset_category
        initial['asset_account'] = asset_category.account
        initial['accumulated_depreciation_account'] = asset_category.accumulated_depreciation_account
        initial['depreciation_method'] = asset_category.depreciation_method
        initial['depreciation_account'] = asset_category.depreciation_account
        initial['depreciation_method'] = asset_category.depreciation_method
        initial['depreciation_rate'] = asset_category.depreciation_rate
        return initial

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceFixedAssetView, self).get_context_data(**kwargs)
        invoice_account = self.get_invoice_account()
        count = InvoiceAccount.objects.link_to_asset(invoice=invoice_account.invoice).count()
        count_linked = InvoiceAccount.objects.link_to_asset(invoice=invoice_account.invoice, is_linked=True).count()
        context['invoice_account'] = invoice_account
        context['count'] = count
        context['count_linked'] = count_linked + 1
        context['new_asset'] = AdditionType.NEW_ASSET.value
        context['asset_addition'] = AdditionType.ADDITION.value
        return context

    def get_category(self, account):
        return Category.objects.filter(account_id=account.id).first()

    def get_invoice_account(self):
        return InvoiceAccount.objects.select_related('account').get(pk=self.kwargs['invoice_account_id'])

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['invoice_id'])

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceFixedAssetView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['invoice_account'] = self.get_invoice_account()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Fixed asset could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateInvoiceFixedAssetView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save(commit=False)

            return JsonResponse({
                'error': False,
                'text': 'Invoice fixed asset added.',
                'invoice_account': self.get_invoice_account().id
            })
        return super(CreateInvoiceFixedAssetView, self).form_valid(form)


class CreateJournalFixedAssetView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'fixedasset/journal_asset.html'
    form_class = JournalAssetForm

    def get_initial(self):
        initial = super(CreateJournalFixedAssetView, self).get_initial()
        journal_account = self.get_account()
        asset_category = self.get_category(journal_account)
        initial['year'] = self.get_year().id
        initial['category'] = asset_category
        initial['asset_account'] = asset_category.account
        initial['accumulated_depreciation_account'] = asset_category.accumulated_depreciation_account
        initial['depreciation_method'] = asset_category.depreciation_method
        initial['depreciation_account'] = asset_category.depreciation_account
        initial['depreciation_method'] = asset_category.depreciation_method
        initial['depreciation_rate'] = asset_category.depreciation_rate
        return initial

    def get_context_data(self, **kwargs):
        context = super(CreateJournalFixedAssetView, self).get_context_data(**kwargs)
        journal_account = self.get_account()
        asset_category = self.get_category(account=journal_account)
        context['account'] = self.get_account()
        context['count'] = 1
        context['count_linked'] = 1
        context['new_asset'] = AdditionType.NEW_ASSET.value
        context['asset_addition'] = AdditionType.ADDITION.value
        context['asset_revaluation'] = AdditionType.REVALUATION.value
        context['asset_disposal'] = AdditionType.DISPOSAL.value
        context['category'] = asset_category
        context['asset_account'] = asset_category.account
        if 'journal_fixed_asset_additions' in self.request.session:
            del self.request.session['journal_fixed_asset_additions']
        if 'journal_fixed_assets' in self.request.session:
            del self.request.session['journal_fixed_assets']
        context['company'] = self.get_company()
        return context

    def get_category(self, account):
        return Category.objects.filter(account_id=account.id).first()

    def get_account(self):
        return Account.objects.get(pk=self.kwargs['journal_account_id'])

    def get_form_kwargs(self):
        kwargs = super(CreateJournalFixedAssetView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['account'] = self.get_account()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['year'] = self.get_year()
        return kwargs

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Fixed asset could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateJournalFixedAssetView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                row_id = self.request.POST.get('row_id')
                journal_account_id = self.kwargs['journal_account_id']
                if 'journal_fixed_assets' in self.request.session:
                    del self.request.session['journal_fixed_assets']
    
                # linked_count = len([addition for addition in assets if addition['account'] == journal_account_id])
                # if not linked_count:
                #     linked_count = len([asset for asset in assets if asset['account'] == journal_account_id])
                assets = []
                if journal_account_id:
                    response = form.save(commit=False)

                    assets.append({
                        'account': journal_account_id,
                        'fixed_asset_data': response,
                        'row_id': row_id
                    })
                    self.request.session['journal_fixed_assets'] = assets
                return JsonResponse({
                    'error': False,
                    'text': 'Fixed asset successfully saved.'
                })
        return super(CreateJournalFixedAssetView, self).form_valid(form)


class ImportFixedAssetView(ProfileMixin, FormView):
    template_name = 'fixedasset/import.html'
    form_class = ImportFixedAssetForm
    success_url = reverse_lazy('fixed_asset_index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'missing_report' in self.request.session:
            self.request.session['missing_report'] = []
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def form_valid(self, form):
        try:
            created_count, missing_report, filename = form.execute()
            self.request.session['missing_report'] = missing_report
            messages.success(self.request, f'{created_count} fixed assets successfully imported.')
            if len(missing_report):
                messages.error(self.request, f"{len(missing_report)} fixed assets could not be imported.")
        except FixedAssetImportException as exc:
            messages.error(self.request, f'{exc} fixed assets successfully imported.')
        return super().form_valid(form)


class DownloadView(AssetRegisterView, ProfileMixin, View):
    template_name = 'fixedasset/summary.html'

    def get(self, request, *args, **kwargs):
        company = self.get_company()
        year = self.get_year()
        period = self.get_period_closing_date(year, company)
        first_period = self.get_first_period_date(year, company)

        fixed_assets = self.get_fixed_assets()
        register = generate_fixed_asset_register(fixed_assets=fixed_assets, year=year)

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = f'attachment; filename="fixed_assets_{company.slug}_{year.year}.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Fixed Assets')

        # Sheet header, first row
        row_count = 1
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        row = ws.row(0)
        row.write(0, str(company), font_style)

        row_count += 1
        ws.write(row_count, 0, '', font_style)
        ws.write(row_count, 1, '', font_style)
        ws.write(row_count, 2, 'Assets', font_style)
        ws.write(row_count, 3, '', font_style)
        ws.write(row_count, 4, '', font_style)
        ws.write(row_count, 5, '', font_style)
        ws.write(row_count, 6, '', font_style)
        ws.write(row_count, 7, 'Depreciation', font_style)
        ws.write(row_count, 8, '', font_style)
        ws.write(row_count, 9, '', font_style)
        ws.write(row_count, 10, '', font_style)
        ws.write(row_count, 11, 'Book Value', font_style)
        ws.write(row_count, 12, '', font_style)
        ws.write(row_count, 13, '', font_style)
        ws.write(row_count, 14, '', font_style)
        ws.write(row_count, 15, '', font_style)
        ws.write(row_count, 16, '', font_style)
        ws.write(row_count, 17, '', font_style)
        ws.write(row_count, 18, '', font_style)

        row_count += 1
        ws.write(row_count, 0, '', font_style)
        ws.write(row_count, 1, 'Fixed Asset', font_style)
        ws.write(row_count, 2, 'Account', font_style)
        ws.write(row_count, 3, 'DF Unique', font_style)
        ws.write(row_count, 4, 'Date Purchased', font_style)
        ws.write(row_count, 5, 'Cost Price', font_style)
        ws.write(row_count, 6, 'Method', font_style)
        ws.write(row_count, 7, 'Rate', font_style)
        ws.write(row_count, 8, f'Opening {year.start_date.strftime("%d/%m/%Y")}', font_style)
        ws.write(row_count, 9, 'Disposals', font_style)
        ws.write(row_count, 10, 'Addition', font_style)
        ws.write(row_count, 11, 'Revaluation', font_style)
        ws.write(row_count, 12, f'Closing {period.to_date.strftime("%d/%m/%Y")}', font_style)
        ws.write(row_count, 13, f'Opening {year.start_date.strftime("%d/%m/%Y")}', font_style)
        ws.write(row_count, 14, 'Disposal', font_style)
        ws.write(row_count, 15, 'Current Year', font_style)
        ws.write(row_count, 16, f'Closing {period.to_date.strftime("%d/%m/%%Y")}', font_style)
        ws.write(row_count, 17, period.to_date.strftime('%d/%m/%Y'), font_style)
        ws.write(row_count, 18, first_period.strftime('%d/%m/%Y'), font_style)

        row_count += 1
        for main_category, sub_categories in register['categories'].items():
            row_count += 1
            ws.write(row_count, 0, main_category.name)
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            row_count += 1
            for category, category_assets in sub_categories['categories'].items():
                ws.write(row_count, 0, category.name)
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                row_count += 1
                for fixed_asset, asset in category_assets['assets'].items():
                    ws.write(row_count, 0, fixed_asset.name)
                    ws.write(row_count, 1, fixed_asset.name)
                    ws.write(row_count, 2, fixed_asset.category.account.code)
                    ws.write(row_count, 3, fixed_asset.asset_number)
                    ws.write(row_count, 4, fixed_asset.date_purchased)
                    ws.write(row_count, 5, fixed_asset.cost_price)
                    ws.write(row_count, 6, fixed_asset.method_name, font_style)
                    ws.write(row_count, 7, fixed_asset.depreciation_rate_percentage, font_style)
                    ws.write(row_count, 8, asset['initial_balance'], font_style)
                    ws.write(row_count, 9, asset['disposals'], font_style)
                    ws.write(row_count, 10, asset['additions'], font_style)
                    ws.write(row_count, 11, asset['revaluations'], font_style)
                    ws.write(row_count, 12, asset['closing_balance'], font_style)
                    ws.write(row_count, 13, asset['opening_depreciation_balance'], font_style)
                    ws.write(row_count, 14, asset['depreciation_disposal'], font_style)
                    ws.write(row_count, 15, asset['current_year_depreciation'], font_style)
                    ws.write(row_count, 16, asset['closing_depreciation'], font_style)
                    ws.write(row_count, 17, asset['closing_book_value'], font_style)
                    ws.write(row_count, 18, asset['opening_net_book_value'], font_style)
                    row_count += 1

                    for addition in fixed_asset.asset_additions:
                        ws.write(row_count, 0, '', font_style)
                        ws.write(row_count, 1, f"   {addition.name}", font_style)
                        ws.write(row_count, 2, '-', font_style)
                        ws.write(row_count, 3, addition.date, font_style)
                        ws.write(row_count, 4, '', font_style)
                        ws.write(row_count, 5, '', font_style)
                        ws.write(row_count, 6, '', font_style)
                        ws.write(row_count, 7, '', font_style)
                        ws.write(row_count, 8, addition.amount if addition.is_disposal else '', font_style)
                        ws.write(row_count, 9, addition.amount if addition.is_addition else '', font_style)
                        ws.write(row_count, 10, addition.amount if addition.is_revaluation else '', font_style)
                        ws.write(row_count, 11, '', font_style)
                        ws.write(row_count, 12, '', font_style)
                        ws.write(row_count, 13, '', font_style)
                        ws.write(row_count, 14, '', font_style)
                        ws.write(row_count, 15, '', font_style)
                        ws.write(row_count, 16, '', font_style)
                        ws.write(row_count, 17, '', font_style)
                        ws.write(row_count, 18, '', font_style)
                        row_count += 1

                ws.write(row_count, 0, f'Totals')
                ws.write(row_count, 1, '')
                ws.write(row_count, 2, '')
                ws.write(row_count, 3, '')
                ws.write(row_count, 4, '')
                ws.write(row_count, 5, category_assets['cost_price'])
                ws.write(row_count, 6, '', font_style)
                ws.write(row_count, 7, '', font_style)
                ws.write(row_count, 8, category_assets['opening_balance'], font_style)
                ws.write(row_count, 9, category_assets['disposals'], font_style)
                ws.write(row_count, 10, category_assets['additions'], font_style)
                ws.write(row_count, 11, category_assets['revaluations'], font_style)
                ws.write(row_count, 12, category_assets['closing_balance'], font_style)
                ws.write(row_count, 13, category_assets['opening_depreciation_balance'], font_style)
                ws.write(row_count, 14, category_assets['depreciation_disposal'], font_style)
                ws.write(row_count, 15, category_assets['depreciation'], font_style)
                ws.write(row_count, 16, category_assets['closing_depreciation_balance'], font_style)
                ws.write(row_count, 17, category_assets['net_book_value'], font_style)
                ws.write(row_count, 18, category_assets['opening_net_book_value'], font_style)

                row_count += 1
            ws.write(row_count, 0, f'Sub Total ')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, sub_categories['cost_price'])
            ws.write(row_count, 6, '', font_style)
            ws.write(row_count, 7, '', font_style)
            ws.write(row_count, 8, sub_categories['opening_balance'], font_style)
            ws.write(row_count, 9, sub_categories['disposals'], font_style)
            ws.write(row_count, 10, sub_categories['additions'], font_style)
            ws.write(row_count, 11, sub_categories['revaluations'], font_style)
            ws.write(row_count, 12, sub_categories['closing_balance'], font_style)
            ws.write(row_count, 13, sub_categories['opening_depreciation_balance'], font_style)
            ws.write(row_count, 14, sub_categories['depreciation_disposal'], font_style)
            ws.write(row_count, 15, sub_categories['depreciation'], font_style)
            ws.write(row_count, 16, sub_categories['closing_depreciation_balance'], font_style)
            ws.write(row_count, 17, sub_categories['net_book_value'], font_style)
            ws.write(row_count, 18, sub_categories['opening_net_book_value'], font_style)

            row_count += 1
            ws.write(row_count, 0, f'Totals ')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')
            ws.write(row_count, 5, register['cost_price'])
            ws.write(row_count, 6, '', font_style)
            ws.write(row_count, 7, '', font_style)
            ws.write(row_count, 8, register['opening_balance'], font_style)
            ws.write(row_count, 9, register['disposals'], font_style)
            ws.write(row_count, 10, register['additions'], font_style)
            ws.write(row_count, 11, register['revaluations'], font_style)
            ws.write(row_count, 12, register['closing_balance'], font_style)
            ws.write(row_count, 13, register['depreciation_opening_balance'], font_style)
            ws.write(row_count, 14, register['depreciation_disposal'], font_style)
            ws.write(row_count, 15, register['current_year_depreciation'], font_style)
            ws.write(row_count, 16, register['depreciation_closing_balance'], font_style)
            ws.write(row_count, 17, register['net_book_value'], font_style)
            ws.write(row_count, 18, register['opening_net_book_value'], font_style)

            row_count += 1
            ws.write(row_count, 0, '')
            ws.write(row_count, 1, '')
            ws.write(row_count, 2, '')
            ws.write(row_count, 3, '')
            ws.write(row_count, 4, '')

            wb.save(response)
        return response
