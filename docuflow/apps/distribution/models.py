from decimal import Decimal

from django.contrib.contenttypes.fields import GenericForeignKey, ContentType
from django.db import models
from django.db.models import Sum, F, Case, When
from django.db.models.functions import Coalesce

from docuflow.apps.company.models import Account


class DistributionManager(models.Manager):

    def accounts_within_year(self, company, year):
        return self.get_queryset().annotate(
            total_in_year=models.Case(
                models.When(accounts__period__period_year=year, then=models.Count('accounts')),
                output_field=models.DecimalField()
            )
        ).select_related(
            'company', 'supplier'
        ).prefetch_related(
            'accounts'
        ).filter(
            company=company, total_in_year__gt=0
        )

    def with_pending_accounts(self, company, year=None):
        pending_accounts = DistributionAccount.objects.select_related(
            'period'
        ).filter(ledger_line__isnull=True, period__period_year=year)
        q = models.Q()
        q.add(models.Q(accounts__ledger_line__isnull=True), models.Q.AND)
        if year:
            q.add(models.Q(accounts__period__period_year=year), models.Q.AND)
        return self.annotate(
            credit=Case(When(q, then=Sum('accounts__credit')), output_field=models.DecimalField()),
            total_credit=Coalesce(F('credit'), Decimal('0'))
        ).select_related(
            'company', 'supplier'
        ).prefetch_related(
            models.Prefetch('accounts', queryset=pending_accounts)
        ).filter(
            company=company
        ).exclude(total_credit=0)


class Distribution(models.Model):
    company = models.ForeignKey('company.Company', related_name='distributions', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    content_type = models.ForeignKey(ContentType, related_name='distributions', on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    supplier = models.ForeignKey('supplier.Supplier', null=True, blank=True, related_name='distributions', on_delete=models.DO_NOTHING)
    distribution_id = models.PositiveIntegerField(null=True, blank=True)
    nb_months = models.PositiveIntegerField()
    starting_date = models.DateField()
    account = models.ForeignKey('company.Account', related_name='+', on_delete=models.DO_NOTHING)
    is_credit = models.BooleanField(default=False)
    total_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    role = models.ForeignKey('accounts.Role', related_name='distributions', on_delete=models.DO_NOTHING)
    profile = models.ForeignKey('accounts.Profile', related_name='distributions', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = DistributionManager()

    def __str__(self):
        return f"{self.title}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.distribution_id:
            distribution_count = Distribution.objects.filter(company=self.company).count()
            self.distribution_id = distribution_count + 1
        return super(Distribution, self).save(*args, **kwargs)

    @property
    def invoice(self):
        if self.content_type.model == 'invoiceaccount':
            return self.content_object.invoice
        return None

    @property
    def suffix(self):
        return getattr(self.content_object, 'suffix', '')


class DistributionAccountQueryset(models.QuerySet):

    def month_close_pending(self, period):
        this_year = models.Q()
        this_year.add(models.Q(period__period__lte=period.period), models.Q.AND)
        this_year.add(models.Q(period__period_year=period.period_year), models.Q.AND)

        previous_years = models.Q()
        previous_years.add(models.Q(period__period_year__year__lt=period.period_year.year), models.Q.AND)

        return self.filter(
            ledger_line__isnull=True,
            distribution__company=period.company
        ).filter(
            models.Q(this_year) | models.Q(previous_years)
        )

    def pending_update(self, period):
        return self.filter(ledger_line__isnull=True, period=period)

    def updated(self):
        return self.filter(ledger_line__isnull=False)

    def for_model(self, content_object):
        return self.filter(
            distribution__content_type=ContentType.objects.get_for_model(content_object),
            distribution__object_id=content_object.id
        )


class DistributionAccount(models.Model):
    distribution = models.ForeignKey(Distribution, related_name='accounts', on_delete=models.CASCADE)
    account = models.ForeignKey('company.Account', related_name='distributions', on_delete=models.PROTECT)
    debit = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    credit = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12)
    date = models.DateField()
    period = models.ForeignKey('period.Period', related_name='account_distributions', null=True, blank=True, on_delete=models.DO_NOTHING)
    ledger_line = models.ForeignKey('journals.JournalLine', related_name='account_distributions', null=True,
                                    blank=True, on_delete=models.DO_NOTHING)

    objects = DistributionAccountQueryset().as_manager()

    class Meta:
        ordering = ('date', '-account__code')
