import collections
from calendar import monthrange

from django.contrib.contenttypes.fields import ContentType
from django.utils.timezone import datetime

from docuflow.apps.common.enums import Module
from docuflow.apps.journals.models import JournalLine, Journal
from docuflow.apps.period.models import Period
from .models import Distribution, DistributionAccount


def create_distribution_accounts(distribution: Distribution, journal_line: JournalLine):
    journal_ine = JournalLine.objects.filter(distribution=distribution)
    months = int(distribution.nb_months)
    start_date = datetime.strptime(distribution.starting_date.strftime('%Y%m%d'), '%Y%m%d')

    periods = get_periods(distribution.company)
    accounts_distribution = distribute_line(distribution.company, months, start_date, periods, journal_line)

    for k, account_distribution in accounts_distribution['distributions'].items():
        DistributionAccount.objects.create(
            distribution=distribution,
            account=account_distribution['distribution_account'],
            debit=account_distribution['debit'],
            credit=account_distribution['credit'],
            distribution_date=account_distribution['day'],
            period=account_distribution['period']
        )
        for account_distribution_account in account_distribution['accounts']:
            DistributionAccount.objects.create(
                distribution=distribution,
                account=account_distribution_account['account'],
                credit=account_distribution_account['credit'],
                debit=account_distribution_account['debit'],
                distribution_date=account_distribution['day'],
                period=account_distribution['period'],
                invoice_account_id=account_distribution_account['invoice_account_id']
            )


def get_periods(company):
    return Period.objects.filter(company=company, period_year__isnull=False)


def calculate_total_credit(invoice_accounts):
    total = 0
    for acc in invoice_accounts:
        total += acc['amount']
    return total


def distribute_line(company, months, start_date, periods, account, amount, is_credit=False):
    default_distribution_account = company.default_distribution_account
    current_month = start_date.month
    current_year = start_date.year
    start_day = start_date.day
    month_counter = 0
    accounts_distribution = {'distributions': {}, 'total_distribution': 0}
    counter = 1
    total_distribution = 0
    for month in range(months):
        _month = current_month + month_counter

        last_day = get_last_day_of_month(start_date, current_year, _month)
        day = get_current_day(start_day, last_day.day)

        start_at = start_date.replace(year=current_year, month=_month, day=day)

        is_last = is_last_month(months, counter)

        account_distribution = distribute_account(amount, account, months, is_last, is_credit)
        line_total = account_distribution['amount']

        default_distribution = distribute_account(amount, default_distribution_account, months, is_last, not is_credit)
        total_distribution += line_total

        accounts_distribution['distributions'][counter] = {
            'day': start_at,
            'line_account': account_distribution,
            'counter': month + 1,
            'period': calculate_period(periods, start_at),
            'total': line_total,
            'default_account': default_distribution
        }
        if _month == 12:
            current_month = 1
            month_counter = 0
            current_year = current_year + 1
        else:
            month_counter += 1
        counter += 1
    accounts_distribution['total_distribution'] = total_distribution
    return accounts_distribution


def get_current_day(start_day, last_day):
    if is_last_day(start_day, last_day):
        return last_day
    return start_day


def is_last_day(start_day, last_day):
    return start_day > last_day


def get_last_day_of_month(start_date, year, month):
    return start_date.replace(year=year, month=month, day=monthrange(year, month)[1])


def is_last_month(months, counter):
    return counter == months


def distribute_account(line_amount, account, nb_months, is_last_month, is_credit, line_id=None):

    prev_months_counter = nb_months - 1
    amount = get_month_amount(nb_months, is_last_month, prev_months_counter, line_amount)

    return {'amount': round(amount, 2),
            'debit': 0 if is_credit else amount,
            'credit': amount if is_credit else 0,
            'code': account.code,
            'account_id': account.id,
            'account': account,
            'line_id': line_id
            }


def get_month_amount(nb_months, is_last, prev_months_counter, amount):
    if is_last:
        total = round((amount / nb_months), 2) * prev_months_counter
        return round((amount - total), 2)
    else:
        return amount / nb_months


def calculate_period(periods, date):
    if isinstance(date, datetime):
        date = date.date()
    for period in periods:
        if period.from_date <= date <= period.to_date:
            return period
    return None


def prepare_distribution_accounts(distribution: Distribution, linked_models: {}, include_distributed: bool = True):
    distr_acts = {}
    total_credit = 0
    total_debit = 0
    q = DistributionAccount.objects.select_related(
        'distribution', 'period', 'period__period_year', 'ledger_line', 'ledger_line__journal', 'account'
    ).order_by(
        'period__period'
    ).filter(
        distribution=distribution
    )
    if not include_distributed:
        q = q.filter(ledger_line__isnull=True)

    suffix = distribution.suffix if distribution.suffix else ''
    for distribution_account in q:
        if distribution_account.period and distribution_account.period.period_year:
            period_id = distribution_account.period.id
            year_id = distribution_account.period.period_year.id
            if distribution.company.default_distribution_account == distribution_account.account:
                distr_account = prepare_distribution_account(distribution_account=distribution_account,
                                                             linked_models=linked_models, suffix='')
            else:
                distr_account = prepare_distribution_account(distribution_account=distribution_account,
                                                             linked_models=linked_models, suffix=suffix)
            account = distr_account['account']
            if year_id in distr_acts:
                if period_id in distr_acts[year_id]['periods']:
                    if account.date in distr_acts[year_id]['periods'][period_id]['accounts']:
                        distr_acts[year_id]['periods'][period_id]['accounts'][account.date].append(distr_account)
                        total_credit += account.credit
                        total_debit += account.debit
                    else:
                        distr_acts[year_id]['periods'][period_id]['accounts'][account.date] = [distr_account]
                        total_credit += account.credit
                        total_debit += account.debit
                else:
                    distr_acts[year_id]['periods'][period_id] = {
                        'accounts': collections.OrderedDict(), 'period': account.period}
                    distr_acts[year_id]['periods'][period_id]['accounts'][account.date] = [distr_account]
                    total_credit += account.credit
                    total_debit += account.debit
            else:
                distr_acts[year_id] = {'year': account.period.period_year, 'periods': collections.OrderedDict()}
                distr_acts[year_id]['periods'][period_id] = {
                    'accounts': collections.OrderedDict(),
                    'period': account.period
                }
                distr_acts[year_id]['periods'][period_id]['accounts'][account.date] = [distr_account]
                total_credit += account.credit
                total_debit += account.debit
    return {'distributions':  distr_acts, 'total_credit': total_credit, 'total_debit': total_debit}


def prepare_distribution_account(distribution_account: DistributionAccount, linked_models: {}, suffix: str = ''):
    distr_acc = {'account': distribution_account, 'object_items': {}, 'suffix': suffix}
    for model_id, linked_model in linked_models.items():
        distr_acc['object_items'][model_id] = ''
    return distr_acc


def period_distribution(company, period: Period, profile, role):
    distribution_accounts = DistributionAccount.objects.pending_update(period=period)

    if distribution_accounts:
        journal: Journal = Journal.objects.create(**{
            'company': company,
            'year': period.period_year,
            'module': Module.JOURNAL.value,
            'period': period,
            'created_by': profile,
            'role': role,
            'date': period.to_date,
            'journal_text': 'Distribution of Invoices - Report',
        })

        for distribution_account in distribution_accounts:
            distribution = distribution_account.distribution
            suffix = ''
            if not company.default_distribution_account == distribution_account.account:
                suffix = distribution.suffix if distribution.suffix else ''
            line = JournalLine.objects.create(
                journal=journal,
                account=distribution_account.account,
                credit=distribution_account.credit,
                debit=distribution_account.debit,
                accounting_date=period.to_date,
                content_type=ContentType.objects.get_for_model(distribution_account),
                object_id=distribution_account.id,
                suffix=suffix
            )
            if line:
                distribution_account.ledger_line = line
                distribution_account.save()

                if distribution_account.distribution.content_type.model == 'invoiceaccount':
                    invoice_account = distribution_account.distribution.content_object

                    if invoice_account:
                        line.supplier = invoice_account.invoice.supplier
                        line.invoice = invoice_account.invoice
                        line.save()

                    object_items = invoice_account.object_items.all()
                    for object_item in object_items:
                        line.object_items.add(object_item)


def process_distribution_from_period(company, period: Period, profile, role):
    distribution_accounts = DistributionAccount.objects.month_close_pending(period=period)

    if distribution_accounts:
        journal: Journal = Journal.objects.create(**{
            'company': company,
            'year': period.period_year,
            'module': Module.JOURNAL.value,
            'period': period,
            'created_by': profile,
            'role': role,
            'date': period.to_date,
            'journal_text': 'Distribution of Invoices - Report',
        })

        for distribution_account in distribution_accounts:
            distribution = distribution_account.distribution
            suffix = ''
            if not company.default_distribution_account == distribution_account.account:
                suffix = distribution.suffix if distribution.suffix else ''
            line = JournalLine.objects.create(
                journal=journal,
                account=distribution_account.account,
                credit=distribution_account.credit,
                debit=distribution_account.debit,
                accounting_date=period.to_date,
                content_type=ContentType.objects.get_for_model(distribution_account),
                object_id=distribution_account.id,
                suffix=suffix
            )
            if line:
                distribution_account.ledger_line = line
                distribution_account.save()

                if distribution_account.distribution.content_type.model == 'invoiceaccount':
                    invoice_account = distribution_account.distribution.content_object

                    if invoice_account:
                        line.supplier = invoice_account.invoice.supplier
                        line.invoice = invoice_account.invoice
                        line.save()

                    object_items = invoice_account.object_items.all()
                    for object_item in object_items:
                        line.object_items.add(object_item)
