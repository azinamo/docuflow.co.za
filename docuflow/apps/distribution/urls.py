from django.urls import path

from . import views

urlpatterns = [
    path('', views.DistributionIndexView.as_view(), name='distributions_index'),
    path('<int:pk>/detail/', views.DistributionDetailView.as_view(), name='distribution_detail'),
    path('add/<int:content_type_id>/distribute/', views.CreateAccountDistributeView.as_view(),
         name='create_account_distribution'),
    path('distribute/amount/', views.DistributeAmountView.as_view(), name='distribute_amount'),
    path('<int:content_type_id>/<int:object_id>/create/', views.CreateDistributionView.as_view(),
         name='create_distribution'),
    path('<int:pk>/edit/', views.EditDistributionView.as_view(), name='edit_distribution'),
    path('account/<int:pk>/delete/', views.DeleteDistributionView.as_view(),
         name='delete_distribution'),
    path('balances/', views.DistributionBalanceView.as_view(), name='distribution_balance'),
    path('balances/<int:pk>/detail/', views.DistributionBalanceDetailView.as_view(), name='distribution_balance_detail'),
    path('download/balances/', views.DistributionBalanceDetailView.as_view(), name='download_distribution_balances'),
    path('create/journals/', views.CreateDistributionJournalView.as_view(), name='create_distribution_journal'),
    path('run/journal/', views.GenerateJournalView.as_view(), name='run_distribution_journal'),
    path('run/lock/<int:period>/month/journal/', views.RunMonthEndDistributionJournal.as_view(), name='run_month_end_distributions'),
    path('summary/', views.DistributionVsBalanceSheetView.as_view(), name='distributions_vs_balance_sheet'),
]
