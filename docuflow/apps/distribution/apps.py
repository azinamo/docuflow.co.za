from django.apps import AppConfig


class DistributionConfig(AppConfig):
    name = 'docuflow.apps.distribution'
