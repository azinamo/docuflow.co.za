import logging
import pprint
from calendar import monthrange
from decimal import Decimal
from datetime import datetime

from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.http import JsonResponse
from django.db import transaction
from django.urls import reverse
from django.utils.timezone import now
from django.views.generic import ListView, FormView, View, TemplateView, UpdateView, CreateView

from annoying.functions import get_object_or_None

from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax_post, is_ajax
from docuflow.apps.company.models import Company, ObjectItem, Account
from docuflow.apps.invoice.models import Invoice, InvoiceAccount, InvoiceJournal, Journal, Status
from docuflow.apps.invoice.utils import processing_statuses
from docuflow.apps.journals.models import JournalLine
from docuflow.apps.period.models import Period
from .models import Distribution
from .forms import DistributionForm, AccountDistributeForm, DistributeJournalForm
from . import services
from . import reports
from ..journals.services import get_total_for_account

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class DistributionIndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'distribution/index.html'
    models = Distribution
    context_object_name = 'distributions'

    def get_queryset(self):
        return Distribution.objects.accounts_within_year(self.get_company(), self.get_year())

    def get_context_data(self, **kwargs):
        context = super(DistributionIndexView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context


class CreateAccountDistributeView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'distribution/account/create.html'
    form_class = AccountDistributeForm

    def get_account(self):
        return Account.objects.get(pk=self.request.GET.get('account_id'))

    def get_form_kwargs(self):
        kwargs = super(CreateAccountDistributeView, self).get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateAccountDistributeView, self).get_context_data(**kwargs)
        context['suffix'] = self.request.GET.get('suffix')
        context['account'] = self.get_account()
        context['amount'] = Decimal(self.request.GET.get('amount'))
        context['content_type'] = ContentType.objects.get(pk=self.kwargs['content_type_id'])
        return context

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Account distribution could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateAccountDistributeView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save()
            return JsonResponse({'error': False,
                                 'text': 'Distribution successfully saved'
                                 })
        return super(CreateAccountDistributeView, self).form_valid(form)


class DistributeAmountView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'distribution/account/distributions.html'

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def get_account(self):
        return Account.objects.get(pk=self.request.GET.get('account_id'))

    def get_context_data(self, **kwargs):
        context = super(DistributeAmountView, self).get_context_data(**kwargs)
        form_kwargs = {
            'request': self.request,
            'profile': self.get_profile(),
            'role': self.get_role(),
            'company': self.get_company(),
            'year': self.get_year()
        }
        account_line_form = AccountDistributeForm(data=self.request.GET, **form_kwargs)
        if account_line_form.is_valid():
            months = account_line_form.cleaned_data['nb_months']
            start_date = account_line_form.cleaned_data['starting_date']
            company = self.get_company()
            amount = account_line_form.cleaned_data['total_amount']
            is_credit = True if self.request.GET.get('credit_amount') else False

            periods = self.get_periods(company)
            accounts_distribution = services.distribute_line(company, months, start_date, periods, self.get_account(),
                                                             amount, is_credit)

            context['months'] = months
            context['start_date'] = start_date
            context['distributions'] = accounts_distribution['distributions']
            context['total_distribution'] = accounts_distribution['total_distribution']
            context['suffix'] = self.request.GET.get('suffix', '') if self.request.GET.get('suffix') else ''
        else:
            context['error'] = 'An error occurred trying to generate the distributions, please refresh and try again'
            context['errors'] = account_line_form.errors
            logger.info(account_line_form.errors)
        return context


class CreateDistributionView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = 'distribution/create.html'
    model = Distribution
    form_class = DistributionForm

    def get_content_object(self):
        content_type = self.get_content_type()
        return content_type.get_object_for_this_type(pk=self.kwargs['object_id'])

    def get_content_type(self):
        return ContentType.objects.get(pk=self.kwargs['content_type_id'])

    def get_initial(self):
        initial = super().get_initial()
        content_object = self.get_content_object()
        initial['account'] = content_object.account
        initial['total_amount'] = content_object.amount
        if isinstance(content_object, JournalLine):
            initial['starting_date'] = content_object.accounting_date
        else:
            initial['starting_date'] = content_object.invoice.accounting_date
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        kwargs['content_type'] = self.get_content_type()
        kwargs['object_id'] = self.kwargs['object_id']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        content_object = self.get_content_object()
        context['content_type'] = self.get_content_type()
        context['object_id'] = self.kwargs['object_id']
        context['content_object'] = content_object
        if isinstance(content_object, JournalLine):
            context['suffix'] = content_object.suffix
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Distribution could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateDistributionView, self).form_invalid(form)

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def form_valid(self, form):
        if is_ajax(request=self.request):
            with transaction.atomic():
                form.save()
            return JsonResponse({
                'error': False,
                'text': 'Distribution successfully saved',
                'reload': True
            })
        return super(CreateDistributionView, self).form_valid(form)


class EditDistributionView(LoginRequiredMixin, ProfileMixin, UpdateView):
    template_name = 'distribution/edit.html'
    form_class = DistributionForm
    model = Distribution

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def get_initial(self):
        initial = super(EditDistributionView, self).get_initial()
        initial['total_amount'] = self.get_object().total_amount
        return initial

    def get_form_kwargs(self):
        kwargs = super(EditDistributionView, self).get_form_kwargs()
        distribution = self.get_object()
        kwargs['company'] = self.get_company()
        kwargs['role'] = self.get_role()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        kwargs['content_type'] = distribution.content_type
        kwargs['object_id'] = distribution.object_id
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditDistributionView, self).get_context_data(**kwargs)
        distribution = self.get_object()
        periods = self.get_periods(distribution.company)

        accounts_distributions = services.distribute_line(
            company=distribution.company,
            months=distribution.nb_months,
            start_date=distribution.starting_date,
            periods=periods,
            account=distribution.account,
            amount=distribution.total_amount,
            is_credit=distribution.is_credit
        )
        context['months'] = distribution.nb_months
        context['start_date'] = distribution.starting_date
        context['distributions'] = accounts_distributions['distributions']
        context['total_distribution'] = accounts_distributions['total_distribution']
        context['suffix'] = distribution.suffix
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Distribution could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(EditDistributionView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save()
            return JsonResponse({
                'error': False,
                'text': 'Distribution successfully updated',
                'reload': True
            })
        return super(EditDistributionView, self).form_valid(form)


class DeleteDistributionView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        response = dict()
        with transaction.atomic():
            distribution = Distribution.objects.get(pk=self.kwargs['pk'])
            if distribution:
                for distribution_account in distribution.accounts.all():
                    distribution_account.delete()

                distribution.content_object.distribution = None
                distribution.content_object.save()

                if not distribution.accounts.exists():
                    distribution.delete()

                response = {'text': 'Distribution deleted successfully',
                            'error': False,
                            'reload': True
                            }
        return JsonResponse(response)


class DistributionDetailView(LoginRequiredMixin, TemplateView):
    template_name = 'distribution/detail.html'

    @staticmethod
    def calculate_total_credit(invoice_accounts):
        total = 0
        for acc in invoice_accounts:
            total += acc['amount']
            yield total

    @staticmethod
    def get_distribution_invoice_accounts(invoice_accounts, nb_months, is_last_month):
        accounts = []
        prev_months_counter = (nb_months-1)
        for invoice_account in invoice_accounts:
            if is_last_month:
                _accounts_total = round((invoice_account.amount/nb_months), 2) * prev_months_counter
                amount = round((invoice_account.amount - _accounts_total), 2)
            else:
                amount = invoice_account.amount/nb_months
            accounts.append({'amount': round(amount, 2),
                             'account': invoice_account.account.code,
                             'account_id': invoice_account.account.id,
                             'invoice_account_id': invoice_account.id
                             })
            yield accounts

    def get_invoice_accounts(self, invoice_accounts, months, start_date):
        company = self.get_company()
        default_distribution_account = company.default_distribution_account
        current_month = start_date.month
        current_year = start_date.year
        day = start_date.day
        month_counter = 0
        accounts_distribution = {'distributions': {}, 'total_distribution': 0}
        counter = 1
        total_distribution = 0
        for month in range(months):
            _day = day
            _month = current_month + month_counter
            last_day = start_date.replace(year=current_year, month=_month, day=monthrange(current_year, _month)[1])
            if day > last_day.day:
                _day = last_day.day
            start_at = datetime.replace(start_date, current_year, _month, _day)
            is_last = (counter == months)

            distribution_invoice_accounts = self.get_distribution_invoice_accounts(invoice_accounts, months, is_last)
            total_credit = self.calculate_total_credit(distribution_invoice_accounts)
            total_distribution += total_credit
            accounts_distribution['distributions'][counter] = {'day': start_at,
                                                               'accounts': distribution_invoice_accounts,
                                                               'period': month + 1,
                                                               'total': total_credit,
                                                               'distribution_account': default_distribution_account
                                                               }
            if _month == 12:
                current_month = 1
                month_counter = 0
                current_year = current_year + 1
            else:
                month_counter += 1
            counter += 1
        accounts_distribution['total_distribution'] = total_distribution
        return accounts_distribution

    def get_company(self):
        return Company.objects.select_related('default_distribution_account').get(pk=self.request.session['company'])

    def get_distribution(self):
        return Distribution.objects.select_related(
            'company'
        ).prefetch_related(
            'accounts', 'accounts__account', 'accounts__period', 'accounts__ledger_line'
        ).get(pk=self.kwargs['pk'])

    def get_accounts(self, distribution):
        return InvoiceAccount.objects.filter(distribution=distribution)

    @staticmethod
    def get_company_linked_modes(company):
        linked_models = {}
        count = 0
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            if count < 2:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
                    company_object_id=company_object.id)
            count += 1
        return linked_models

    def get_context_data(self, **kwargs):
        context = super(DistributionDetailView, self).get_context_data(**kwargs)
        distribution = self.get_distribution()

        linked_models = self.get_company_linked_modes(distribution.company)
        distributions = services.prepare_distribution_accounts(distribution, linked_models)

        context['distribution'] = distribution
        context['distributions'] = distributions['distributions']
        context['total_debit'] = distributions['total_debit']
        context['total_credit'] = distributions['total_credit']
        context['linked_models'] = linked_models
        return context


class DistributionAccountsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'distribution/distribution_accounts.html'

    def get_periods(self, company):
        return Period.objects.filter(company=company, period_year__isnull=False)

    def get_invoice(self):
        return Invoice.objects.get(pk=int(self.kwargs['invoice_id']))

    def get_context_data(self, **kwargs):
        context = super(DistributionAccountsView, self).get_context_data(**kwargs)
        accounts = self.request.GET['account_ids']
        months = int(self.request.GET['months'])
        start_date = datetime.strptime(self.request.GET['start_date'], '%Y-%m-%d')
        company = self.get_company()

        accounts_ids = accounts.split(',')
        invoice = self.get_invoice()
        invoice_accounts = InvoiceAccount.objects.select_related(
            'account', 'invoice', 'invoice__invoice_type'
        ).filter(id__in=accounts_ids)
        periods = self.get_periods(company)
        accounts_distribution = services.distribute_account(company, invoice_accounts, months, start_date, periods, invoice)

        context['months'] = months
        context['start_date'] = start_date
        context['distributions'] = accounts_distribution['distributions']
        context['total_distribution'] = accounts_distribution['total_distribution']
        return context


class DistributionJournalView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'distribution/invoice_journal.html'

    def get_journal_invoice_accounts(self, journal, journal_invoices):
        return InvoiceAccount.objects.filter(invoice_id__in=[journal.invoice_id for journal in journal_invoices]).all()

    def get_journal(self):
        return Journal.objects.get(pk=self.kwargs['pk'])

    def get_role_profiles(self, journal):
        role_profiles = []
        if journal.role:
            for profile in journal.role.profile_set.all():
                role_profiles.append("{} {}".format(profile.user.first_name, profile.user.last_name))
        return ', '.join(role_profiles)

    def process_report(self, journal):
        journal_invoices = InvoiceJournal.objects.filter(journal=journal)
        return journal_invoices

    def get_context_data(self, **kwargs):
        context = super(DistributionJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        journal = self.get_journal()

        context['company'] = company
        context['journal'] = journal

        supplier_account = None
        expenditure_account = None
        vat_account = None
        discount_received_account = None

        report = reports.DistributionJournal(journal)
        report.process_report()

        if company.default_vat_account:
            vat_account = company.default_vat_account.code
        if company.default_expenditure_account:
            expenditure_account = company.default_expenditure_account.code
        if company.default_supplier_account:
            supplier_account = company.default_supplier_account.code
        if company.default_discount_received_account:
            discount_received_account = company.default_discount_received_account.code

        context['supplier_account'] = supplier_account
        context['expenditure_account '] = expenditure_account
        context['credit_account'] = supplier_account
        context['vat_account'] = vat_account
        context['discount_received_account'] = discount_received_account

        role_profiles = self.get_role_profiles(journal)

        context['role_profiles'] = role_profiles
        context['report'] = report
        return context


class CreateJournalForDistributionView(LoginRequiredMixin, ProfileMixin, View):
    def generate_unique_number(self, journal_key, company, counter=1):
        profile = Profile.objects.get(pk=self.request.session['profile'])
        counter_str = str(counter).rjust(5, '0')
        company_code = (company.code[0:3]).upper()
        user_code = (profile.user.first_name[0:2]).upper()
        unique_number = f"{company_code}-{user_code}-{journal_key}-{counter_str}"

        result = get_object_or_None(Journal, report_unique_number=unique_number)
        if result:
            counter = counter + 1
            return self.generate_unique_number(journal_key, company, counter)
        else:
            return unique_number

    def get_invoices(self, company, period):
        return Invoice.objects.select_related(
            'status', 'invoice_type', 'supplier', 'company'
        ).filter(
            status__slug__in=['paid', 'printed-status-still-in-the-flow'], period=period, company=company
        ).exclude(distribution_journal=True)

    def create_journal(self, company, role):
        unique_number = self.generate_unique_number('JDI', company, 1)
        journal = Journal.objects.create(
            report_unique_number=unique_number, journal_type=Journal.DISTRIBUTION, created_by=self.request.user.profile,
            company=company, role=role
        )
        return journal

    def get(self, request, *args, **kwargs):
        if 'period' in self.request.GET and self.request.GET['period']:
            try:
                to_invoice_status = Status.objects.get(slug='paid-printed')
                period = self.request.GET['period']
                company = self.get_company()
                if to_invoice_status:
                    invoices = self.get_invoices(company, period)
                    if invoices:
                        role = Role.objects.get(pk=self.request.session['role'])
                        journal = self.create_journal(company, role)
                        for invoice in invoices:
                            invoice.distribution_journal = True
                            invoice.save()

                            InvoiceJournal.objects.create(
                                invoice=invoice,
                                journal=journal,
                                invoice_status=invoice.status,
                                vat_amount=invoice.vat_amount,
                                net_amount=invoice.net_amount,
                                total_amount=invoice.total_amount,
                                invoice_type=invoice.invoice_type,
                                supplier=invoice.supplier,
                                vat_number=invoice.vat_number,
                                accounting_date=invoice.accounting_date,
                                invoice_date=invoice.invoice_date,
                                supplier_number=invoice.supplier_number
                            )
                        return JsonResponse({'error': False,
                                             'text': 'Journal for distribution successfully created',
                                             'redirect': reverse('journal_for_distribution_invoice')
                                             })
                    else:
                        return JsonResponse({'error': True,
                                             'text': 'No invoices to process',
                                             'redirect': reverse('journal_for_distribution_invoice')
                                             })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'Error occurred updating the invoices',
                                     'details': "Exception {}".format(exception)
                                     })
        else:
            return JsonResponse({'error': True,
                                 'text': 'Please select the period'
                                 })


class JournalForDistributionInvoiceView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'distribution/journal.html'

    def get_periods(self, company):
        return Period.objects.filter(company=company, is_closed=False)

    def get_context_data(self, **kwargs):
        context = super(JournalForDistributionInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['journals'] = self.get_journals(company)
        context['periods'] = self.get_periods(company)
        return context

    def get_journals(self, company):
        return Journal.objects.filter(journal_type=Journal.DISTRIBUTION, company=company,
                                      period__period_year__is_active=True)


class CreateDistributionJournalView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, TemplateView):
    template_name = 'distribution/journals/create.html'

    def get_statuses(self):
        return Status.objects.filter(
            slug__in=['final-signed', 'in-the-flow', 'end-flow-printed', 'paid', 'printed-status-still-in-the-flow',
                      'paid-printed']
        ).all()

    def display_statuses(self, statuses, selected_statuses):
        statuses_list = []
        return statuses_list

    def get_filter_options(self, company):
        options = {'company': company}
        invoice_statuses = []
        completed = ['end-flow-printed', 'paid', 'paid-printed']
        processing = processing_statuses()
        if 'status' in self.request.GET and self.request.GET['status']:
            for status in self.request.GET.getlist('status'):
                if status == 'completed':
                    invoice_statuses += completed
                if status == 'processing':
                    invoice_statuses += processing
        else:
            invoice_statuses = completed
        return options

    def get_context_data(self, **kwargs):
        context = super(CreateDistributionJournalView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()

        statuses = self.get_statuses()
        options = self.get_filter_options(company)

        report = reports.DistributionBalance(company=company, year=year)
        report.execute()

        context['options'] = options
        context['report'] = report
        context['company'] = company
        context['report_date'] = now()
        context['year'] = year
        context['statuses'] = self.display_statuses(statuses, options)
        return context


class DistributionBalanceView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, TemplateView):
    template_name = 'distribution/balance/index.html'

    def get_context_data(self, **kwargs):
        context = super(DistributionBalanceView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()

        distributions = Distribution.objects.with_pending_accounts(company=company)

        total = sum(distribution.total_credit for distribution in distributions)

        context['distributions'] = distributions
        context['total'] = total
        context['company'] = company
        context['report_date'] = now().date()
        context['year'] = year
        return context


class DistributionBalanceDetailView(LoginRequiredMixin, TemplateView):
    template_name = 'distribution/balance/detail.html'

    def get_company(self):
        return Company.objects.select_related(
            'default_distribution_account'
        ).get(
            pk=self.request.session['company']
        )

    def get_distribution(self):
        return Distribution.objects.select_related('company').prefetch_related(
            'accounts', 'accounts__account', 'accounts__period', 'accounts__ledger_line'
        ).get(pk=self.kwargs['pk'])

    @staticmethod
    def get_company_linked_modes(company):
        linked_models = {}
        count = 0
        company_objects = company.company_objects.all()
        for company_object in company_objects:
            if count < 2:
                linked_models[company_object.id] = {}
                linked_models[company_object.id]['values'] = {}
                linked_models[company_object.id]['label'] = company_object.label
                linked_models[company_object.id]['class'] = company_object
                linked_models[company_object.id]['objects'] = ObjectItem.objects.filter(
                    company_object_id=company_object.id)
            count += 1
        return linked_models

    def get_context_data(self, **kwargs):
        context = super(DistributionBalanceDetailView, self).get_context_data(**kwargs)
        company = self.get_company()

        linked_models = self.get_company_linked_modes(company)

        distribution = self.get_distribution()
        distributions = services.prepare_distribution_accounts(distribution=distribution, linked_models=linked_models,
                                                               include_distributed=False)
        context['linked_models'] = linked_models
        context['distribution'] = distribution
        context['distributions'] = distributions['distributions']
        context['total_debit'] = distributions['total_debit']
        context['total_credit'] = distributions['total_credit']
        context['suffix'] = distribution.suffix
        return context


class GenerateJournalView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = "distribution/journals/create.html"
    form_class = DistributeJournalForm

    # noinspection PyMethodMayBeStatic
    def get_periods(self):
        return Period.objects.open().active().get()

    def get_form_kwargs(self):
        kwargs = super(GenerateJournalView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(GenerateJournalView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred updating distributions, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(GenerateJournalView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            with transaction.atomic():
                form.execute()

                return JsonResponse({
                    'error': False,
                    'text': 'Distribution successfully done',
                    'reload': True
                })
        return super(GenerateJournalView, self).form_valid(form)


class RunMonthEndDistributionJournal(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        services.process_distribution_from_period(
            company=self.get_company(),
            period=Period.objects.get(pk=self.kwargs['period']),
            profile=self.get_profile(),
            role=self.get_role()
        )

        return JsonResponse({
            'text': 'Distribution journals successfully updated',
            'error': False,
            'reload': True
        })


class DistributionVsBalanceSheetView(ProfileMixin, TemplateView):
    template_name = 'distribution/balances.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()

        distributions = Distribution.objects.with_pending_accounts(company=company)
        total = sum(distribution.total_credit for distribution in distributions)

        account_total = get_total_for_account(
            year=year,
            company=company,
            account=company.default_distribution_account,
            start_date=year.start_date,
            end_date=year.end_date
        )

        context['total'] = total
        context['balance_sheet_total'] = account_total
        context['is_balancing'] = total == account_total
        return context
