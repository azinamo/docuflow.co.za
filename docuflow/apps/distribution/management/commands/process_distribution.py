import pprint
import logging
from time import sleep

from django.core.management import BaseCommand
from django.core.mail import send_mail
from django.db.models import Count

from docuflow.apps.distribution.services import period_distribution
from docuflow.apps.period.models import Period, Year
from docuflow.apps.distribution.models import Distribution
from docuflow.apps.company.models import Company

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Process distribution"

    def add_arguments(self, parser):
        parser.add_argument('company', type=str, help='Run distributions for the company')
        parser.add_argument('--year', type=int, help='Run distributions for the year')
        parser.add_argument('--period_id', nargs='+', type=int, help='Run distributions for the period')
        parser.add_argument('--distribution_id', nargs='+', help='Distribution id')

    def handle(self, *args, **options):
        year = None
        period = None
        distribution = None

        company = Company.objects.filter(slug=options['company']).first()

        not_posted = Distribution.objects.annotate(
            count_accounts=Count('accounts')
        ).filter(count_accounts=0, company=company)

        breakpoint()

        # if options['period_id']:
        #     period = Period.objects.filter(pk=options['period_id']).first()
        #     if not period:
        #         self.stdout.write(self.style.ERROR(f"Period {options['period_id']} not found"))
        #
        # if options['distribution_id']:
        #     distribution = Distribution.objects.filter(pk__in=options['distribution_id']).first()
        #     if not distribution:
        #         self.stdout.write(self.style.ERROR(f"Distribution {options['distribution_id']} not found"))
        #
        # if options['year']:
        #     year = Year.objects.filter(year=options['year'], company=company).first()
        #     if year:
        #         self.stdout.write(self.style.SUCCESS(f"Year {options['year']} found"))
        #     else:
        #         self.stdout.write(self.style.ERROR(f"Year {options['year']} not found"))
        #
        # if period:
        #     period_distribution(
        #         company=company,
        #         period=period,
        #         role=None,
        #         profile=None
        #     )
        # elif year:
        #     for period in Period.objects.filter(period_year=year).all():
        #         print(f"{period} on company {company}")
        #         period_distribution(
        #             company=company,
        #             period=period,
        #             role=None,
        #             profile=None
        #         )
        # elif distribution:
        #     for distribution_account in distribution.accounts.all():
        #         period_distribution(
        #             company=company,
        #             period=distribution_account.period,
        #             role=None,
        #             profile=None
        #         )
