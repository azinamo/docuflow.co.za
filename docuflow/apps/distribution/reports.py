from .models import Distribution


class DistributionBalance(object):

    def __init__(self, company, year):
        self.company = company
        self.day_ranges = {}
        self.distributions = []
        self.total = 0
        self.year = year

    def execute(self):
        self.distributions = Distribution.objects.with_pending_accounts(company=self.company)

        self.total = sum(distribution.total_credit for distribution in self.distributions)
