import pprint

from django import forms
from django.core.cache import cache
from django.utils.timezone import now

from docuflow.apps.company.models import Account
from docuflow.apps.period.models import Period
from .models import Distribution, DistributionAccount
from .services import period_distribution

pp = pprint.PrettyPrinter(indent=4)


class AccountDistributeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super(AccountDistributeForm, self).__init__(*args, **kwargs)
        if self.request.GET.get('date'):
            self.fields['starting_date'].initial = self.request.GET.get('date')
        else:
            self.fields['starting_date'].initial = now().date()
        self.fields['total_amount'].initial = self.request.GET.get('amount')
        self.fields['nb_months'].label = 'How many months'
        self.fields['total_amount'].label = None
        self.fields['total_amount'].required = True

    class Meta:
        model = Distribution
        fields = ('starting_date', 'nb_months', 'total_amount')
        widgets = {
            'total_amount': forms.HiddenInput()
        }

    def save(self, commit=True):
        distribution = super().save(commit=False)
        distribution.role = self.role
        distribution.profile = self.profile

        distributions = {}
        for field, value in self.data.items():
            if field.startswith('distribution', 0, 12):
                row_id = value
                accounts = self.data.getlist(f'accounts_{row_id}')
                data = {
                    'date': self.data.get(f'date_{row_id}'),
                    'period': self.data.get(f'period_{row_id}'),
                    'debit': self.data.get(f'debit_{row_id}'),
                    'credit': self.data.get(f'credit_{row_id}'),
                    'accounts': {}
                }
                for account_id in accounts:
                    data['accounts'][account_id] = {
                        'debit': self.data.get(f'accounts_{row_id}_debit_{account_id}'),
                        'credit': self.data.get(f'accounts_{row_id}_credit_{account_id}')
                    }
                distributions[row_id] = data

        cache_key = f'{self.company.pk}_{self.year.id}_journal_distributions_{self.profile.pk}_{self.role.pk}'
        journal_lines_distributions = cache.get(cache_key, {})

        journal_lines_distributions[self.data.get('row_id')] = {
            'lines': distributions,
            'distribution': {
                'total_amount': float(distribution.total_amount),
                'starting_date': str(distribution.starting_date),
                'nb_months': distribution.nb_months
            }
        }

        cache.set(cache_key, journal_lines_distributions, 240)
        return journal_lines_distributions


class DistributionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.content_type = kwargs.pop('content_type')
        self.object_id = kwargs.pop('object_id')
        super(DistributionForm, self).__init__(*args, **kwargs)
        self.fields['nb_months'].label = 'How many months'
        self.fields['total_amount'].label = None

    class Meta:
        model = Distribution
        fields = ('starting_date', 'nb_months', 'total_amount', 'account')
        widgets = {
            'starting_date': forms.TextInput(attrs={'class': 'future_datepicker'}),
            'total_amount': forms.HiddenInput(),
            'account': forms.HiddenInput()
        }

    def save(self, commit=True):
        distribution = super(DistributionForm, self).save(commit=False)
        distribution.content_type = self.content_type
        distribution.object_id = self.object_id
        distribution.profile = self.profile
        distribution.role = self.role
        distribution.company = self.company
        distribution.is_credit = False
        distribution.save()

        self.create_distribution_accounts(distribution=distribution)
        content_object = distribution.content_object

        if not content_object.account:
            content_object.account = self.get_distribution_account()
        if not content_object.distribution:
            content_object.distribution = distribution

        distribution.is_credit = True if content_object.is_credit else False
        distribution.title = str(content_object)
        if self.content_type.model == 'invoiceaccount':
            distribution.title = f"{content_object.invoice.supplier.name}-{str(content_object.invoice)}"
        elif self.content_type.model == 'journalline':
            content_object.distribution = distribution
            content_object.account = Account.objects.get(pk=self.company.default_distribution_account.pk)
            distribution.title = f"{content_object.journal.journal_text}-{str(content_object.journal)}"
        content_object.save()
        distribution.save()

        return distribution

    def get_distribution_account(self):
        default_distribution_account = self.company.default_distribution_account
        if not default_distribution_account:
            raise forms.ValidationError('Default distribution account is not set')
        return default_distribution_account

    def create_distribution_accounts(self, distribution: Distribution):
        if distribution.accounts.exists():
            for distribution_line in distribution.accounts.all():
                distribution_line.delete()

        for field, value in self.data.items():
            if field.startswith("distribution", 0, 12):
                accounts = self.data.getlist(f'accounts_{value}')
                for account_id in accounts:
                    debit = self.data.get(f'accounts_{value}_debit_{account_id}')
                    credit = self.data.get(f'accounts_{value}_credit_{account_id}')
                    data = {
                        'date': self.data.get(f'date_{value}'),
                        'period': self.data.get(f'period_{value}'),
                        'account': account_id,
                        'debit': round(float(debit), 2),
                        'credit': round(float(credit), 2),
                        'distribution': distribution
                    }
                    distribution_account_form = DistributionAccountForm(data=data, company=self.company)
                    if distribution_account_form.is_valid():
                        distribution_account_form.save()
                    else:
                        raise forms.ValidationError(distribution_account_form.errors)


class DistributionAccountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(DistributionAccountForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.company)
        self.fields['distribution'].queryset = self.fields['distribution'].queryset.filter(company=self.company)

    class Meta:
        model = DistributionAccount
        fields = ('account', 'debit', 'credit', 'period', 'distribution', 'date')


class InvoiceAccountDistributeForm(forms.Form):

    def __init__(self, *args, **kwargs):
        invoice_date = None
        if 'invoice' in kwargs:
            invoice = kwargs.pop('invoice')
            if invoice:
                invoice_date = invoice.invoice_date
        super(InvoiceAccountDistributeForm, self).__init__(*args, **kwargs)

        self.fields['start_date'].widget = forms.TextInput(attrs={
            'data-min-date': invoice_date,
            'class': 'future_datepicker',
            'readonly': 'readonly'
        })

    month = forms.CharField(required=True, label='How many months', widget=forms.TextInput())
    start_date = forms.DateField(required=True, label='Starting Date')


class DistributeJournalForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop('year')
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(DistributeJournalForm, self).__init__(*args, **kwargs)
        self.fields['periods'].queryset = Period.objects.open(self.company, self.year)

    periods = forms.ModelMultipleChoiceField(required=True, label='Periods', queryset=None)

    def execute(self):
        for period in self.cleaned_data['periods']:
            period_distribution(
                company=self.company,
                period=period,
                role=self.role,
                profile=self.profile
            )
