from django.contrib import admin

from docuflow.apps.distribution.models import Distribution, DistributionAccount


@admin.register(DistributionAccount)
class DistributionAccountAdmin(admin.ModelAdmin):
    list_select_related = ('account', 'period', 'ledger_line')
    fields = ('account', 'period', 'date', 'debit', 'credit', 'ledger_line')
    list_filter = ('distribution',)


@admin.register(Distribution)
class DistributionAdmin(admin.ModelAdmin):
    list_display = ('company', 'title', 'nb_months', 'starting_date', 'total_amount', 'is_credit', 'profile')
    fields = ('title', 'nb_months')
    list_select_related = ('company', 'profile')
    list_filter = ('company', )
