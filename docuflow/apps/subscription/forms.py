from django import forms

from .models import *


class SubscriptionForm(forms.ModelForm):

    class Meta:
        fields = ('name', 'monthly_price', 'invoices_from', 'invoices_to', 'description', 'is_active', 'modules')
        model = SubscriptionPlan
