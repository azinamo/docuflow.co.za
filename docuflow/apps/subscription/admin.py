from django.contrib import admin

# Register your models here.

from .models import SubscriptionPlan


# Register your models here.
class SubscriptionPlanAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'invoices_from', 'invoices_to', 'is_active')
    fields = ('name', 'description', 'price', 'invoices_from', 'invoices_to', 'is_active')


admin.site.register(SubscriptionPlan, SubscriptionPlanAdmin)