import json

from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from multiselectfield import MultiSelectField

from docuflow.apps.common.enums import Module


class SubscriptionPlan(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, default='')
    price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    monthly_price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    is_active = models.BooleanField(default=True, null=True)
    invoices_from = models.PositiveIntegerField(null=True, default=0)
    invoices_to = models.PositiveIntegerField(default=0, null=True)
    items = models.TextField(default='', null=True)
    slug = models.SlugField()
    modules = MultiSelectField(choices=Module.choices(), default=Module.PURCHASE.value)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('subscription_plan_detail', args=[str(self.slug)])

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(SubscriptionPlan, self).save(*args, **kwargs)

    @property
    def plan_items(self):
        if self.items:
            return json.loads(self.items)
        return []

    @property
    def module_list(self):
        return [int(m) for m in self.modules]
