from django.urls import path

from . import views

urlpatterns = [
    path('', views.SubscriptionPlansView.as_view(), name='plans'),
    path('packages/', views.PackagesView.as_view(), name='packages'),
    path('create/packages/', views.CreatePackagesView.as_view(), name='create_package'),
    path('edit/<int:pk>/packages/', views.EditPackagesView.as_view(), name='edit_package'),
]
