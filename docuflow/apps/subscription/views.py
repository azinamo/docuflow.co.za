import json

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, DetailView, UpdateView, CreateView

from docuflow.apps.common.enums import Module
from .forms import SubscriptionForm
from .models import SubscriptionPlan


class SubscriptionPlansView(ListView):
    model = SubscriptionPlan
    context_object_name = 'subscription_plans'
    template_name = 'home/plans.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(SubscriptionPlansView, self).get_context_data(**kwargs)
        context['user'] = self.request.session['user']
        return context


class SubscriptionPlanView(DetailView):
    model = SubscriptionPlan
    template_name = 'home/plan.html'
    context_object_name = 'subscription_plan'

    def get_context_data(self, **kwargs):
        context = super(SubscriptionPlanView, self).get_context_data(**kwargs)
        context['user'] = self.request.session['user']
        return context


class PackagesView(ListView):
    model = SubscriptionPlan
    context_object_name = 'packages'
    template_name = 'subscription/packages.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PackagesView, self).get_context_data(**kwargs)
        context['modules'] = Module.get_choices()
        return context


class CreatePackagesView(LoginRequiredMixin, CreateView):
    model = SubscriptionPlan
    template_name = 'subscription/create.html'
    form_class = SubscriptionForm

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Package saved successfully.')
        return HttpResponseRedirect(reverse('packages'))

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CreatePackagesView, self).get_context_data(**kwargs)
        return context


class EditPackagesView(LoginRequiredMixin, UpdateView):
    model = SubscriptionPlan
    template_name = 'subscription/edit.html'
    form_class = SubscriptionForm

    def form_valid(self, form):
        plan = form.save()
        items = self.request.POST.getlist('item')
        plan_items = [item for item in items if item != '']
        if plan_items:
            plan.items = json.dumps(plan_items)
            plan.save()

        messages.success(self.request, 'Package updated successfully.')
        return HttpResponseRedirect(reverse('edit_package', kwargs={'pk': self.kwargs['pk']}))

    def get_context_data(self, **kwargs):
        context = super(EditPackagesView, self).get_context_data(**kwargs)
        plan = self.get_object()
        context['plan'] = plan
        context['start'] = len(plan.plan_items) + 1
        return context
