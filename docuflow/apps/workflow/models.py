import django.dispatch
from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext as __
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from safedelete.models import SafeDeleteModel, SOFT_DELETE

from docuflow.apps.company.models import Company, ObjectItem, Account
from docuflow.apps.accounts.models import Role


############
# Exceptions
############
class UnableToActivateWorkflow(Exception):
    """
    To be raised if unable to activate the workflow because it did not pass the
    validation steps
    """


class UnableToCloneWorkflow(Exception):
    """
    To be raised if unable to clone a workflow model (and related models)
    """


class UnableToStartWorkflow(Exception):
    """
    To be raised if a WorkflowActivity is unable to start a workflow
    """


class UnableToProgressWorkflow(Exception):
    """
    To be raised if the WorkflowActivity is unable to progress a workflow with a
    particular transition.
    """


class UnableToLogWorkflowEvent(Exception):
    """
    To be raised if the WorkflowActivity is unable to log an event in the
    WorkflowHistory
    """


class UnableToAddCommentToWorkflow(Exception):
    """
    To be raised if the WorkflowActivity is unable to log a comment in the
    WorkflowHistory
    """


class UnableToDisableParticipant(Exception):
    """
    To be raised if the WorkflowActivity is unable to disable a participant
    """


class UnableToEnableParticipant(Exception):
    """
    To be raised if the WorkflowActivity is unable to enable a participant
    """

#########
# Signals
#########


# Fired when a role is assigned to a user for a particular run of a workflow
# (defined in the WorkflowActivity). The sender is an instance of the
# WorkflowHistory model logging this event.
role_assigned = django.dispatch.Signal()
# Fired when a role is removed from a user for a particular run of a workflow
# (defined in the WorkflowActivity). The sender is an instance of the
# WorkflowHistory model logging this event.
role_removed = django.dispatch.Signal()
# Fired when a new WorkflowActivity starts navigating a workflow. The sender is
# an instance of the WorkflowActivity model
workflow_started = django.dispatch.Signal()
# Fired just before a WorkflowActivity creates a new item in the Workflow History
# (the sender is an instance of the WorkflowHistory model)
workflow_pre_change = django.dispatch.Signal()
# Fired after a WorkflowActivity creates a new item in the Workflow History (the
# sender is an instance of the WorkflowHistory model)
workflow_post_change = django.dispatch.Signal() 
# Fired when a WorkflowActivity causes a transition to a new state (the sender is
# an instance of the WorkflowHistory model)
workflow_transitioned = django.dispatch.Signal()
# Fired when some event happens during the life of a WorkflowActivity (the 
# sender is an instance of the WorkflowHistory model)
workflow_event_completed = django.dispatch.Signal()
# Fired when a comment is created during the lift of a WorkflowActivity (the
# sender is an instance of the WorkflowHistory model)
workflow_commented = django.dispatch.Signal()
# Fired when an active WorkflowActivity reaches a workflow's end state. The
# sender is an instance of the WorkflowActivity model
workflow_ended = django.dispatch.Signal()

########
# Models
########


class Workflow(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE
    """
    Instances of this class represent a named workflow that achieve a particular
    aim through a series of related states / transitions. A name for a directed
    graph.
    """

    # A workflow can be in one of three states:
    # 
    # * definition: you're building the thing to meet whatever requirements you
    # have
    #
    # * active: you're using the defined workflow in relation to things in your
    # application - the workflow definition is frozen from this point on.
    #
    # * retired: you no longer use the workflow (but we keep it so it can be 
    # cloned as the basis of new workflows starting in the definition state)
    #
    # Why do this? Imagine the mess that could be created if a "live" workflow
    # was edited and states were deleted or orphaned. These states at least
    # allow us to check things don't go horribly wrong. :-
    DEFINITION = 0
    ACTIVE = 1
    RETIRED = 2

    STATUS_CHOICE_LIST = (
                (DEFINITION, _('In definition')),
                (ACTIVE, _('Active')),
                (RETIRED, _('Retired')),
            )
    company = models.ForeignKey(Company, related_name='company_workflows', blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField(_('Workflow Name'), max_length=128)
    slug = models.SlugField(_('Slug'))
    description = models.TextField(_('Description'), blank=True)
    status = models.IntegerField(_('Status'), choices=STATUS_CHOICE_LIST, default=DEFINITION)
    is_default = models.BooleanField(default=False)
    # These next fields are helpful for tracking the history and devlopment of a
    # workflow should it have been cloned
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    cloned_from = models.ForeignKey('self', null=True, on_delete=models.DO_NOTHING)
    # To hold error messages created in the validate method
    errors = {
                'workflow': [],
                'states': {},
                'transitions': {},
             }

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(Workflow, self).save(*args, **kwargs)

    class Meta:
        ordering = ['status', 'name']
        verbose_name = _('Workflow')
        verbose_name_plural = _('Workflows')
        permissions = (
                ('can_manage_workflows', __('Can manage workflows')),
            )


class Level(models.Model):
    """
    Represents how a workflow can move between different states. An edge
    between state "nodes" in a directed graph.
    """
    # This field is the result of denormalization to help with the Workflow
    # class's clone() method.
    workflow = models.ForeignKey(Workflow, related_name='levels', on_delete=models.CASCADE)
    sort_order = models.PositiveIntegerField(verbose_name='Sort Order', blank=False, default=1)
    # The roles referenced here define *who* are in this level
    roles = models.ManyToManyField(Role)

    def update_ordering(self, workflow_id, after):
        workflow_levels = Level.objects.filter(workflow_id=workflow_id).all()
        after = int(after)
        for workflow_level in workflow_levels:
            if workflow_level.id != self.id:
                if workflow_level.sort_order > after:
                    workflow_level.sort_order = workflow_level.sort_order + 1
                    workflow_level.save()

    def reset_ordering(self, workflow_id):
        workflow_levels = Level.objects.filter(workflow_id=workflow_id).all()
        counter = 1
        for workflow_level in workflow_levels:
            workflow_level.sort_order = counter
            workflow_level.save()
            counter += 1

    def __str__(self):
        return f"Level-{self.sort_order}"

    class Meta:
        ordering = ['sort_order']
        verbose_name = _('Level')
        verbose_name_plural = _('Levels')
