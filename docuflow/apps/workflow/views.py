import subprocess

from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.template import Context, loader
from django.views.generic import ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, View, FormView, DeleteView

from docuflow.apps.accounts.models import Role
from docuflow.apps.company.models import Company
from docuflow.apps.common.mixins import ProfileMixin
from .forms import WorkflowForm, AddFlowLevelForm
from .models import Workflow, Level


class WorkflowView(LoginRequiredMixin, ProfileMixin, ListView):
    model = Workflow
    template_name = 'workflow/index.html'
    context_object_name = 'workflows'

    def get_queryset(self):
        return Workflow.objects.filter(company=self.get_company())


class CreateWorkflowView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    form_class = WorkflowForm
    model = Workflow
    template_name = 'workflow/create.html'
    success_message = 'Workflow successfully created'
    success_url = reverse_lazy('workflow_index')

    def get_context_data(self, **kwargs):
        context = super(CreateWorkflowView, self).get_context_data(**kwargs)
        workflow_levels = [{'level': 1, 'roles': []}]
        context['levels'] = workflow_levels
        self.request.session['workflow_levels'] = workflow_levels
        return context

    def form_valid(self, form):
        workflow = form.save(commit=False)
        workflow.company = self.get_company()
        workflow.created_by = self.request.user
        workflow.save()
        form.save_m2m()

        if workflow:
            workflow_levels = self.request.session['workflow_levels']
            for workflow_level in workflow_levels:
                if workflow_level['level']:
                    roles = workflow_level['roles']
                    if len(roles) > 0:
                        level = Level.objects.create(
                            workflow=workflow,
                            sort_order=int(workflow_level['level'])
                        )
                        if level and workflow_level['roles']:
                            for role_id in workflow_level['roles']:
                                role = Role.objects.get(pk=role_id)
                                if role:
                                    level.roles.add(role)

        messages.success(self.request, 'Workflow saved successfully')
        if 'redirect' in self.request.GET:
            return HttpResponseRedirect(self.request.GET['redirect'])
        return HttpResponseRedirect(reverse('workflow_index'))


class EditWorkflowView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = Workflow
    form_class = WorkflowForm
    template_name = "workflow/edit.html"
    success_message = 'Workflow successfully updated'

    def get_context_data(self, **kwargs):
        context = super(EditWorkflowView, self).get_context_data(**kwargs)
        workflow = self.get_object()

        levels = workflow.levels
        if levels.count():
            workflow_levels = []
            for workflow_level in levels.all():
                lvl = {'level': workflow_level, 'roles': []}
                for role in workflow_level.roles.all():
                    lvl['roles'].append(role)
                workflow_levels.append(lvl)
        else:
            workflow_levels = [{'level': 1, 'roles': []}]
        context['levels'] = workflow_levels
        context['workflow'] = workflow
        return context

    def form_valid(self, form):
        workflow = form.save(commit=False)
        workflow.save()
        form.save_m2m()

        messages.success(self.request, 'Workflow updated successfully')
        return HttpResponseRedirect(reverse('workflow_index'))


class UpdateDefaultWorkflowView(LoginRequiredMixin, ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        company = self.get_company()
        company_workflows = Workflow.objects.filter(company=company).all()
        for company_workflow in company_workflows:
            company_workflow.is_default = False
            company_workflow.save()

        workflow = Workflow.objects.get(pk=self.kwargs['pk'])
        workflow.is_default = True
        workflow.save()

        return JsonResponse({
            'text': 'Workflow updated successfully',
            'error': False
        })


class ActivateFlowView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            workflow = Workflow.objects.get(pk=self.kwargs['pk'])
        except Workflow.DoesNotExist:
            messages.error(self.request, 'Workflow not found')

        messages.success(self.request, 'Workflow activated successfully.')

        return HttpResponseRedirect(reverse('workflow_index'))


class AddWorkLevelView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = AddFlowLevelForm
    template_name = "level/add_wk_level.html"

    def get_form_kwargs(self):
        kwargs = super(AddWorkLevelView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddWorkLevelView, self).get_context_data(**kwargs)
        context['after'] = int(self.kwargs['after'])
        context['level'] = int(self.kwargs['level'])
        context['workflow'] = get_object_or_None(Workflow, pk=self.kwargs['workflow_id'])
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Invoice flow level could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(AddWorkLevelView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            if form.cleaned_data['role']:
                workflow_levels = []
                workflow = get_object_or_None(Workflow, pk=self.kwargs['workflow_id'])
                if workflow:
                    level = int(self.kwargs['after'])
                    roles = form.cleaned_data['role']
                    if len(roles) > 0:
                        workflow_level = Level.objects.create(
                            workflow=workflow,
                            sort_order=level
                        )
                        if workflow_level:
                            for role_id in roles:
                                role = Role.objects.get(pk=role_id)
                                if role:
                                    workflow_level.roles.add(role)

                return JsonResponse({
                    'error': False,
                    'text': 'Level roles successfully added.',
                    'after': self.kwargs['after'],
                    'reload': True
                })
            else:
                return JsonResponse({
                    'error': True,
                    'text': 'Please select the roles'
                })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class AddWorkLevelRoleView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = AddFlowLevelForm
    template_name = "level/add_wk_level_role.html"

    def get_form_kwargs(self):
        kwargs = super(AddWorkLevelRoleView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddWorkLevelRoleView, self).get_context_data(**kwargs)
        context['level'] = int(self.kwargs['level'])
        context['workflow'] = get_object_or_None(Workflow, pk=self.kwargs['workflow_id'])
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Invoice flow level could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(AddWorkLevelRoleView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            roles = form.cleaned_data['role']
            if len(roles) == 0:
                return JsonResponse({
                    'error': True,
                    'text': 'Please select the roles'
                })
            workflow = get_object_or_None(Workflow, pk=self.kwargs['workflow_id'])
            if workflow:
                level = int(self.kwargs['level'])
                workflow_level = Level.objects.create(
                    workflow=workflow,
                    sort_order=level
                )
                if workflow_level:
                    for level_role in roles:
                        workflow_level.roles.add(level_role)

            return JsonResponse({
                'error': False,
                'text': 'Level roles successfully added.',
                'reload': True
            })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class AddLevelView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = AddFlowLevelForm
    template_name = "level/add_level.html"

    def get_form_kwargs(self):
        kwargs = super(AddLevelView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddLevelView, self).get_context_data(**kwargs)
        context['after'] = int(self.kwargs['after'])
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Invoice flow level could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(AddLevelView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            if form.cleaned_data['role']:
                workflow_levels = []
                if 'workflow_levels' in self.request.session:
                    workflow_levels = self.request.session['workflow_levels']

                level = int(self.kwargs['after']) + 1

                workflow_level = {'level': level, 'roles': [] }
                for role in form.cleaned_data['role']:
                    workflow_level['roles'].append(role.id)

                workflow_levels.append(workflow_level)
                self.request.session['workflow_levels'] = workflow_levels

                return JsonResponse({'error': False,
                                     'text': 'Level roles successfully added.',
                                     'after': self.kwargs['after'],
                                     'url': reverse('show_workflow_level_role', kwargs={'level': level})
                                     })
            else:
                return JsonResponse({
                    'error': True,
                    'text': 'Please select the roles'
                })
        else:
            return HttpResponseRedirect(reverse('all-invoices'))


class AddLevelRoleView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = AddFlowLevelForm
    template_name = "level/add_level_role.html"

    def get_form_kwargs(self):
        kwargs = super(AddLevelRoleView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddLevelRoleView, self).get_context_data(**kwargs)
        context['level'] = int(self.kwargs['level'])
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Invoice flow level could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(AddLevelRoleView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                if form.cleaned_data['role']:
                    workflow_levels = []
                    if 'workflow_levels' in self.request.session:
                        workflow_levels = self.request.session['workflow_levels']
                    workflow_level = {'level': self.kwargs['level'], 'roles': []}
                    for role in form.cleaned_data['role']:
                        workflow_level['roles'].append(role.id)

                    workflow_levels.append(workflow_level)
                    self.request.session['workflow_levels'] = workflow_levels
                    return JsonResponse({'error': False,
                                         'text': 'Level roles successfully added.',
                                         'level': self.kwargs['level'],
                                         'url': reverse('show_workflow_level_role', kwargs={'level': self.kwargs['level']})
                                         })
                else:
                    return JsonResponse({'error': True,
                                         'text': 'Please select the roles'
                                         })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': f'Error occurred {str(exception)}'
                                     })
        else:
            return HttpResponseRedirect(reverse('create_workflow'))


class CreateWorkflowLevelView(LoginRequiredMixin, FormView):
    template_name = "level/add_workflow_level.html"

    def get_form_class(self):
        return AddFlowLevelForm

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(CreateWorkflowLevelView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateWorkflowLevelView, self).get_context_data(**kwargs)
        context['workflow'] = Workflow.objects.get(pk=self.kwargs['workflow_id'])
        context['after'] = self.kwargs['after']
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Invoice flow level could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(CreateWorkflowLevelView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                if form.cleaned_data['role']:
                    sort_order = int(self.kwargs['after']) + 1
                    level = Level.objects.create(
                        workflow_id=self.kwargs['workflow_id'],
                        sort_order=sort_order
                    )
                    level.update_ordering(self.kwargs['workflow_id'], self.kwargs['after'])
                    if level:
                        for role in form.cleaned_data['role']:
                            role = get_object_or_None(Role, pk=role.id)
                            if role:
                                level.roles.add(role)

                    return JsonResponse({'error': False,
                                         'text': 'Level successfully added.',
                                         'reload': True
                                         })
                else:
                    return JsonResponse({
                        'error': True,
                        'text': 'Please select the roles'
                    })
            except Exception as exception:
                return JsonResponse({
                    'error': True,
                    'text': f'Error occurred {str(exception)}'
                })
        else:
            return HttpResponseRedirect(reverse('create_workflow'))


class CreateWorkflowLevelRoleView(LoginRequiredMixin, FormView):
    template_name = "level/add_workflow_level_role.html"

    def get_form_class(self):
        return AddFlowLevelForm

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(CreateWorkflowLevelRoleView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateWorkflowLevelRoleView, self).get_context_data(**kwargs)
        context['level'] = Level.objects.get(pk=self.kwargs['level_id'])
        context['workflow'] = Workflow.objects.get(pk=self.kwargs['workflow_id'])
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Invoice flow level could not be saved, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateWorkflowLevelRoleView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                if form.cleaned_data['role']:
                    level = get_object_or_None(Level, pk=self.kwargs['level_id'],
                                               workflow_id=self.kwargs['workflow_id'])
                    if level:
                        for role in form.cleaned_data['role']:
                            role = get_object_or_None(Role, pk=role.id)
                            if role:
                                level.roles.add(role)

                    return JsonResponse({
                        'error': False,
                        'text': 'Level roles successfully added.',
                        'reload': True
                    })
                else:
                    return JsonResponse({
                        'error': True,
                        'text': 'Please select the roles'
                    })
            except Exception as exception:
                return JsonResponse({
                    'error': True,
                    'text': f'Error occurred {str(exception)}'
                })
        else:
            return HttpResponseRedirect(reverse('create_workflow'))


class WorkflowLevelView(LoginRequiredMixin, TemplateView):
    template_name = 'level/show.html'

    def get_context_data(self, **kwargs):
        context = super(WorkflowLevelView, self).get_context_data(**kwargs)
        level = self.kwargs['level']
        context['flow_level'] = level
        level_roles = []
        if 'workflow_levels' in self.request.session:
            for workflow_level in self.request.session['workflow_levels']:
                if int(workflow_level['level']) == int(level):
                    for role_id in workflow_level['roles']:
                        role = Role.objects.get(pk=role_id)
                        level_roles.append(role)

        context['flow_level'] = {'level': level, 'roles': level_roles}

        return context


class DeleteLevelRoleView(LoginRequiredMixin, View):
    model = Workflow
    template_name = 'workflow/confirm_delete.html'
    success_message = 'Workflow successfully deleted'

    def get(self):
        reverse('workflow_index')


class DeleteWorkflowLevelRoleView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):

    def get(self, request, *args, **kwargs):
        level = get_object_or_None(Level, pk=self.kwargs['level_id'], workflow_id=self.kwargs['workflow_id'])
        if level:
            try:
                role = get_object_or_None(Role, pk=self.kwargs['role_id'])
                if role:
                    level.roles.remove(role)

                if level.roles.count() == 0:
                    level.delete()

                messages.success(self.request, 'Role deleted successfully.')
            except Exception as exception:
                messages.error(self.request, exception)
                return HttpResponseRedirect(reverse('edit_workflow', kwargs={'pk': self.kwargs['workflow_id']}))
        else:
            messages.error(self.request, 'Level not found')
        return HttpResponseRedirect(reverse('edit_workflow', kwargs={'pk': self.kwargs['workflow_id']}))


class DeleteWorkflowLevelView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        level = get_object_or_None(Level, pk=self.kwargs['level_id'], workflow_id=self.kwargs['workflow_id'])
        if level:
            try:

                level.delete()

                level.reset_ordering(self.kwargs['workflow_id'])

                messages.success(self.request, 'Workflow level deleted successfully.')
            except Exception as exception:
                messages.error(self.request, exception)
                return HttpResponseRedirect(reverse('edit_workflow', kwargs={'pk': self.kwargs['workflow_id']}))
        else:
            messages.error(self.request, 'Workflow not found')
        return HttpResponseRedirect(reverse('edit_workflow', kwargs={'pk': self.kwargs['workflow_id']}))


class ShowFlowView(LoginRequiredMixin, TemplateView):
    template_name = 'workflow/show.html'

    def get_workflow(self):
        return Workflow.objects.get(pk=self.request.GET['workflow_id'])

    def get_context_data(self, **kwargs):
        context = super(ShowFlowView, self).get_context_data(**kwargs)
        context['workflow'] = self.get_workflow()
        context['invoice_id'] = self.request.GET['invoice_id']
        return context


class DeleteWorkflowView(LoginRequiredMixin, SuccessMessageMixin, View):
    success_message = 'Workflow successfully deleted'

    def get(self, request, *args, **kwargs):
        try:
            wk = Workflow.objects.get(pk=self.kwargs['pk'])
            wk.delete()
            messages.success(self.request, 'Workflow deleted successfully')
        except Exception as e:
            messages.success(self.request, 'Error occurred deleting the workflow, please try again')
        return HttpResponseRedirect(reverse('workflow_index'))


###################
# Utility functions
###################
class GraphVizView(LoginRequiredMixin, TemplateView):
    template_name = 'grahpviz'

    def get_dotfile(self, workflow):
        """
        Given a workflow will return the appropriate contents of a .dot file for
        processing by graphviz
        """
        c = Context({'workflow': workflow})
        t = loader.get_template('graphviz/workflow.dot')
        return t.render(c)

    def get_context_data(self, **kwargs):
        """
        Returns a png representation of the workflow generated by graphviz given
        the workflow name (slug)

        The following constant should be defined in settings.py:

        GRAPHVIZ_DOT_COMMAND - absolute path to graphviz's dot command used to
        generate the image
        """
        if not hasattr(settings, 'GRAPHVIZ_DOT_COMMAND'):
            # At least provide a helpful exception message
            raise Exception("GRAPHVIZ_DOT_COMMAND constant not set in settings.py (to specify the absolute path to "
                            "graphviz's dot command)")
        w = get_object_or_404(Workflow, slug=self.kwargs['workflow_slug'])
        # Lots of "pipe" work to avoid hitting the file-system
        proc = subprocess.Popen('%s -Tpng' % settings.GRAPHVIZ_DOT_COMMAND,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                )
        response = HttpResponse(mimetype='image/png')
        response.write(proc.communicate(self.get_dotfile(w).encode('utf_8'))[0])
        return response


class GraphView(LoginRequiredMixin, TemplateView):
    template_name = 'graphviz/workflow.dot'

    def get_dotfile(self, workflow):
        """
        Given a workflow will return the appropriate contents of a .dot file for
        processing by graphviz
        """
        c = Context({'workflow': workflow})
        t = loader.get_template('graphviz/workflow.dot')
        return t.render(c)

    ################
    # view functions
    ################

    def dotfile(self, workflow_slug):
        """
        Returns the dot file for use with graphviz given the workflow name (slug)
        """
        w = get_object_or_404(Workflow, slug=workflow_slug)
        response = HttpResponse(mimetype='text/plain')
        response['Content-Disposition'] = 'attachment; filename=%s.dot'%w.name
        response.write(self.get_dotfile(w))
        return response


def get_dotfile(workflow):
    """
    Given a workflow will return the appropriate contents of a .dot file for
    processing by graphviz
    """
    # c = Context({'workflow': workflow})
    t = loader.get_template('graphviz/workflow.dot')
    return t.render({'workflow': workflow})

################
# view functions
################


def dotfile(request, pk):
    """
    Returns the dot file for use with graphviz given the workflow name (slug)
    """
    w = get_object_or_404(Workflow, id=pk)
    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s.dot'%w.name
    response.write(get_dotfile(w))
    return response


def graphviz(request, pk):
    """
    Returns a png representation of the workflow generated by graphviz given
    the workflow name (slug)

    The following constant should be defined in settings.py:

    GRAPHVIZ_DOT_COMMAND - absolute path to graphviz's dot command used to
    generate the image
    """
    if not hasattr(settings, 'GRAPHVIZ_DOT_COMMAND'):
        # At least provide a helpful exception message
        raise Exception("GRAPHVIZ_DOT_COMMAND constant not set in settings.py (to specify the absolute path to  "
                        "graphviz's dot command)")
    w = get_object_or_404(Workflow, slug=pk)
    # Lots of "pipe" work to avoid hitting the file-system
    proc = subprocess.Popen('%s -Tpng' % settings.GRAPHVIZ_DOT_COMMAND,
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            )
    response = HttpResponse(mimetype='image/png')
    response.write(proc.communicate(get_dotfile(w).encode('utf_8'))[0])
    return response
