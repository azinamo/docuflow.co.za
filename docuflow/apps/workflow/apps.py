from django.apps import AppConfig


class WorkflowConfig(AppConfig):
    name = 'docuflow.apps.workflow'
