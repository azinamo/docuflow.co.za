# -*- coding: UTF-8 -*-
from django.contrib import admin
from .models import Workflow

# class RoleAdmin(admin.ModelAdmin):
#     """
#     Role administration
#     """
#     list_display = ['name', 'description']
#     search_fields = ['name', 'description']
#     save_on_top = True

class WorkflowAdmin(admin.ModelAdmin):
    """
    Workflow administration
    """
    list_display = ['name', 'description', 'status', 'created_at', 'created_by',
            'cloned_from']
    search_fields = ['name', 'description']
    save_on_top = True
    exclude = ['created_on', 'cloned_from']
    list_filter = ['status']


# admin.site.register(Role, RoleAdmin)
admin.site.register(Workflow, WorkflowAdmin)
