from enumfields import Enum


class LineType(Enum):
    EXPENDITURE = 'expenditure'
    VAT = 'vat'
