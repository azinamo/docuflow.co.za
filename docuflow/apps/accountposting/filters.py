import django_filters

from docuflow.apps.accounts.models import Role


class AccountPostingFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Role
        fields = ['name']
