from django.apps import AppConfig


class ObjectitempostingConfig(AppConfig):
    name = 'docuflow.apps.accountposting.modules.objectitemposting'
