from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.paginator import Paginator
from django.db.models import Sum
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, TemplateView, View
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.accountposting.models import ObjectPosting, ObjectItemPosting
from docuflow.apps.accounts.models import Role, PermissionGroup
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax, is_ajax_post
from docuflow.apps.company.models import Account
from .forms import ObjectItemPostingForm


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    model = ObjectItemPosting
    template_name = 'objectitemposting/index.html'
    context_object_name = 'object_item_postings'

    def get_object_posting(self):
        return ObjectPosting.objects.annotate(percentage=Sum('object_splitting__percentage')).get(id=self.kwargs['object_posting_id'])

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['object_posting'] = self.get_object_posting()
        return context

    def get_queryset(self):
        return ObjectItemPosting.objects.filter(object_posting_id=self.kwargs['object_posting_id'])


class CreateObjectItemPostingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, CreateView):
    model = ObjectItemPosting
    form_class = ObjectItemPostingForm
    template_name = 'objectitemposting/create.html'
    success_message = 'Object item posting proposal successfully created'

    def get_form_kwargs(self):
        kwargs = super(CreateObjectItemPostingView, self).get_form_kwargs()
        object_posting = self.get_object_posting()
        kwargs['company'] = self.get_company()
        kwargs['company_object'] = object_posting.company_object
        return kwargs

    def get_success_url(self):
        return reverse('edit_object_posting', kwargs={'pk': self.kwargs['object_posting_id']})

    def get_object_posting(self):
        return ObjectPosting.objects.select_related('company_object').get(id=self.kwargs['object_posting_id'])

    def get_context_data(self, **kwargs):
        context = super(CreateObjectItemPostingView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['object_posting'] = self.get_object_posting()
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the object posting item, please fix the errors.',
                'errors': form.errors,
                'alert': True
            })
        return super(CreateObjectItemPostingView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            object_posting_item = form.save(commit=False)
            object_posting_item.object_posting = self.get_object_posting()
            object_posting_item.save()

            return JsonResponse({
                'error': False,
                'text': 'Object posting ite successfully saved',
                'reload': True
            })


class EditObjectItemPostingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    model = ObjectItemPosting
    form_class = ObjectItemPostingForm
    template_name = "objectitemposting/edit.html"
    context_object_name = 'object_item_posting'
    success_message = 'Object item posting successfully updated'
    success_url = reverse_lazy('posting_object_index')

    def get_object_posting(self):
        return ObjectPosting.objects.get(id=self.get_object().object_posting_id)

    def get_context_data(self, **kwargs):
        context = super(EditObjectItemPostingView, self).get_context_data(**kwargs)
        kwargs['object_posting'] = self.get_object_posting()
        return context

    def get_form_kwargs(self):
        kwargs = super(EditObjectItemPostingView, self).get_form_kwargs()
        object_posting = self.get_object_posting()
        kwargs['company'] = self.get_company()
        kwargs['company_object'] = object_posting.company_object
        return kwargs

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the object posting item, please fix the errors.',
                'errors': form.errors,
                'alert': True
            })
        return super(EditObjectItemPostingView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            object_posting_item = form.save(commit=False)
            object_posting_item.object_posting = self.get_object_posting()
            object_posting_item.save()

            return JsonResponse({
                'error': False,
                'text': 'Object posting ite successfully updated',
                'reload': True
            })


class DeleteObjectPostingView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        account_posting = ObjectPosting.objects.get(pk=self.kwargs['pk'])
        account_posting.delete()
        messages.success(request, 'Successfully deleted account posting')
        return HttpResponseRedirect(reverse('account_posting_index'))


class ObjectPostingView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'objectitemposting/detail.html'

    def get_object_postings(self, company):
        return ObjectPosting.objects.filter(company=company).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(ObjectPostingView, self).get_context_data(**kwargs)
        company = self.get_company()
        roles = self.get_object_postings(company)
        paginator = Paginator(roles, 1)
        page = 1
        if 'page' in self.request.GET:
            page = self.request.GET['page']
        context['account_postings'] = paginator.page(page)
        account_posting = None
        if 'page' in self.request.GET:
            page = self.request.GET['page']
            account_postings = paginator.page(page)
            if account_postings.paginator.count > 0:
                account_posting = account_postings[0]
        elif 'pk' in self.kwargs:
            account_posting = get_object_or_None(ObjectPosting, pk=self.kwargs['pk'])

        context['account_posting'] = account_posting
        context['accounts'] = Account.objects.filter(company=company)
        context['roles'] = Role.objects.filter(company=company)
        context['groups'] = PermissionGroup.objects.filter(company=company)
        context['company'] = company
        return context
