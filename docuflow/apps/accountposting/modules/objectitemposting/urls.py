from django.urls import path

from . import views

urlpatterns = [
    path('<int:object_posting_id>/list/', views.IndexView.as_view(), name='posting_object_item_index'),
    path('<int:object_posting_id>/create/', views.CreateObjectItemPostingView.as_view(),
         name='create_object_item_posting'),
    path('<int:pk>/edit/', views.EditObjectItemPostingView.as_view(), name='edit_object_item_posting'),
]