from django import forms

from docuflow.apps.accountposting.models import ObjectItemPosting


class ObjectItemPostingForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        if 'company_object' in kwargs:
            self.company_object = kwargs.pop('company_object')
        super(ObjectItemPostingForm, self).__init__(*args, **kwargs)
        self.fields['object_item'].queryset = self.fields['object_item'].queryset.filter(
            company_object_id=self.company_object.id
        )
        self.fields['object_item'].required = True
        self.fields['percentage'].required = True

    class Meta:
        model = ObjectItemPosting
        fields = ('object_item', 'percentage')
        widgets = {
            'object_item': forms.Select(attrs={'class': 'chosen-select'})
        }


