from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, TemplateView, View
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.accountposting.models import ObjectPosting
from docuflow.apps.accounts.models import Profile, Role, PermissionGroup
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.company.models import Account
from .forms import ObjectPostingForm


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'objectposting/index.html'
    model = ObjectPosting
    context_object_name = 'object_postings'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_queryset(self):
        return ObjectPosting.objects.select_related('company_object').filter(
            company_object__company=self.get_company(), deleted__isnull=True
        )


class CreateObjectPostingView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    template_name = 'objectposting/create.html'
    model = ObjectPosting
    success_message = 'Account posting proposal successfully created'
    form_class = ObjectPostingForm

    def get_form_kwargs(self):
        kwargs = super(CreateObjectPostingView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def permission_groups(self, company):
        return PermissionGroup.objects.filter(company=self.get_company(), is_active=True)

    def company_user_accounts(self, company):
        return Profile.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(CreateObjectPostingView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['permission_groups'] = self.permission_groups(company)
        context['user_accounts'] = self.company_user_accounts(company)
        return context

    def form_valid(self, form):
        account_posting = form.save(commit=False)
        account_posting.company = self.get_company()
        account_posting.save()
        form.save_m2m()
        form.save()

        messages.success(self.request, 'Account posting proposal saved successfully.')
        return HttpResponseRedirect(reverse('edit_object_posting', kwargs={'pk': account_posting.id}))


class EditObjectPostingView(LoginRequiredMixin, ProfileMixin, SuccessMessageMixin, UpdateView):
    template_name = "objectposting/edit.html"
    model = ObjectPosting
    context_object_name = 'object_posting'
    success_message = 'Object posting successfully updated'
    success_url = reverse_lazy('posting_object_index')
    form_class = ObjectPostingForm

    def company_user_accounts(self, company):
        return Profile.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(EditObjectPostingView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['user_accounts'] = self.company_user_accounts(company)
        return context

    def get_form_kwargs(self):
        kwargs = super(EditObjectPostingView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        object_posting = form.save(commit=False)
        object_posting.company = self.get_company()
        object_posting.save()
        form.save_m2m()
        form.save()

        messages.success(self.request, 'Object posting updated successfully.')
        return HttpResponseRedirect(self.get_success_url())


class DeleteObjectPostingView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        account_posting = ObjectPosting.objects.get(pk=self.kwargs['pk'])
        account_posting.delete()
        messages.success(request, 'Successfully deleted account posting')
        return HttpResponseRedirect(reverse('posting_object_index'))


class ObjectPostingView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'objectposting/detail.html'

    def get_object_postings(self, company):
        return ObjectPosting.objects.filter(company=company).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(ObjectPostingView, self).get_context_data(**kwargs)
        company = self.get_company()
        object_postings = self.get_object_postings(company)
        paginator = Paginator(object_list=object_postings, per_page=1)
        page = 1
        if 'page' in self.request.GET:
            page = self.request.GET['page']
        context['account_postings'] = paginator.page(page)
        account_posting = None
        if 'page' in self.request.GET:
            page = self.request.GET['page']
            account_postings = paginator.page(page)
            if account_postings.paginator.count > 0:
                account_posting = account_postings[0]
        elif 'pk' in self.kwargs:
            account_posting = get_object_or_None(ObjectPosting, pk=self.kwargs['pk'])

        context['account_posting'] = account_posting
        context['accounts'] = Account.objects.filter(company=company)
        context['roles'] = Role.objects.filter(company=company)
        context['groups'] = PermissionGroup.objects.filter(company=company)
        context['company'] = company
        return context
