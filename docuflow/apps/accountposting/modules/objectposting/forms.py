from django import forms

from docuflow.apps.accountposting.models import ObjectPosting


# Create your forms here.
class ObjectPostingForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(ObjectPostingForm, self).__init__(*args, **kwargs)
        self.fields['company_object'].queryset = self.fields['company_object'].queryset.filter(company=company)

    class Meta:
        model = ObjectPosting
        fields = ('name', 'company_object', 'is_active')
        widgets = {
            'company_object': forms.Select(attrs={'class': 'chosen-select'})
        }


