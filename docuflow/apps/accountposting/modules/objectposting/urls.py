from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='posting_object_index'),
    path('create/', views.CreateObjectPostingView.as_view(), name='create_object_posting'),
    path('<int:pk>/edit/', views.EditObjectPostingView.as_view(), name='edit_object_posting'),
    path('<int:pk>/delete/', views.DeleteObjectPostingView.as_view(), name='delete_object_posting'),
]