from django.apps import AppConfig


class ObjectpostingConfig(AppConfig):
    name = 'docuflow.apps.accountposting.modules.objectposting'
