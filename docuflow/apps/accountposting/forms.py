from django import forms

from .models import AccountPosting, AccountPostingLine
from docuflow.apps.accounts.models import Role, PermissionGroup
from docuflow.apps.workflow.models import Workflow


class AccountPostingLineForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(AccountPostingLineForm, self).__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=company)

    distribution = forms.IntegerField(required=False, label='How many months')

    class Meta:
        model = AccountPostingLine
        fields = ('percentage', 'distribution', 'account')

    def clean(self):
        cleaned_data = self.cleaned_data
        if not cleaned_data['percentage']:
            self.add_error('percentage', 'Please enter the percentage value')

        return cleaned_data


class AccountPostingForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AccountPostingForm, self).__init__(*args, **kwargs)

    class Meta:
        model = AccountPosting
        fields = ('label', )


class AddAccountPostingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super(AddAccountPostingForm, self).__init__(*args, **kwargs)
        self.fields['group'].queryset = PermissionGroup.objects.filter(company=company)
        self.fields['role'].queryset = Role.objects.filter(company=company)
        self.fields['workflow'].queryset = Workflow.objects.filter(company=company)

    class Meta:
        model = AccountPosting
        fields = ('workflow', 'role', 'group')
        widgets = {
            'role': forms.Select(attrs={'class': 'chosen-select'}),
            'group': forms.Select(attrs={'class': 'chosen-select'}),
            'workflow': forms.Select(attrs={'class': 'chosen-select'}),
        }
