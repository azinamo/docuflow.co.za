import json

from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.paginator import Paginator
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, TemplateView, FormView, View
from django.views.generic.edit import CreateView, UpdateView

from docuflow.apps.accounts.models import Profile, Role, PermissionGroup
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.common.utils import is_ajax_post, is_ajax
from docuflow.apps.company.models import Company, Account, ObjectItem
from .enums import LineType
from .forms import AccountPostingForm, AccountPostingLineForm
from .models import AccountPostingLine, AccountPosting


# TODO - Add tests and refactoring
class AccountPostingView(LoginRequiredMixin, ProfileMixin, ListView):
    model = AccountPosting
    context_object_name = 'account_postings'
    template_name = 'account_posting/index.html'

    def get_context_data(self, **kwargs):
        context = super(AccountPostingView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_queryset(self):
        return AccountPosting.objects.filter(company=self.get_company())


class AccountPostingCreateView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    template_name = 'account_posting/create.html'
    model = AccountPosting
    form_class = AccountPostingForm
    success_url = reverse_lazy('account_posting_index')
    success_message = 'Account posting proposal successfully created'

    def permission_groups(self, company):
        return PermissionGroup.objects.filter(company=self.get_company(), is_active=True)

    def company_user_accounts(self, company):
        return Profile.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(AccountPostingCreateView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['permission_groups'] = self.permission_groups(company)
        context['user_accounts'] = self.company_user_accounts(company)
        return context

    def form_valid(self, form):
        account_posting = form.save(commit=False)
        account_posting.company = self.get_company()
        account_posting.save()
        form.save_m2m()
        form.save()

        messages.success(self.request, 'Account posting proposal saved successfully.')
        return HttpResponseRedirect(reverse('show_account_posting', kwargs={'pk': account_posting.id}))


class AccountPostingEditView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    template_name = "account_posting/edit.html"
    model = AccountPosting
    form_class = AccountPostingForm
    success_message = 'Account posting successfully updated'
    success_url = reverse_lazy('account_posting_index')

    def company_user_accounts(self, company):
        return Profile.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(AccountPostingEditView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['user_accounts'] = self.company_user_accounts(company)
        return context

    def get_form_kwargs(self):
        kwargs = super(AccountPostingEditView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_valid(self, form):
        account_posting = form.save(commit=False)
        account_posting.company = self.get_company()
        account_posting.save()
        form.save_m2m()
        form.save()

        messages.success(self.request, 'Account posting updated successfully.')
        return HttpResponseRedirect(self.get_success_url())


class DeleteAccountPostingView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        account_posting = AccountPosting.objects.get(pk=self.kwargs['pk'])
        account_posting.delete()
        messages.success(request, 'Successfully deleted account posting')
        return HttpResponseRedirect(reverse('account_posting_index'))


class AccountPostingDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'account_posting/detail.html'

    def get_roles(self, company):
        return AccountPosting.objects.filter(company=company).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(AccountPostingDetailView, self).get_context_data(**kwargs)
        company = self.get_company()
        roles = self.get_roles(company)
        paginator = Paginator(roles, 1)
        page = 1
        if 'page' in self.request.GET:
            page = self.request.GET['page']
        context['account_postings'] = paginator.page(page)
        account_posting = None
        if 'page' in self.request.GET:
            page = self.request.GET['page']
            account_postings = paginator.page(page)
            if account_postings.paginator.count > 0:
                account_posting = account_postings[0]
        elif 'pk' in self.kwargs:
            account_posting = get_object_or_None(AccountPosting, pk=self.kwargs['pk'])

        context['account_posting'] = account_posting
        context['accounts'] = Account.objects.filter(company=company)
        context['roles'] = Role.objects.filter(company=company)
        context['groups'] = PermissionGroup.objects.filter(company=company)
        context['company'] = company
        return context


class AccountPostingUpdateView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        if self.request.is_ajax():
            account_posting = AccountPosting.objects.get(pk=self.kwargs['pk'])
            if account_posting:
                try:
                    lines = AccountPostingLine.objects.filter(account_posting=account_posting).all()
                    is_valid_total_percentage = account_posting.is_valid_total_percentage(lines)
                    if is_valid_total_percentage:
                        if 'account' in self.request.POST and self.request.POST['account'] != '':
                            account_posting.account_id = self.request.POST['account']

                        if 'name' in self.request.POST and self.request.POST['name'] != '':
                            account_posting.label = self.request.POST['name']

                        if 'account_from_supplier' in self.request.POST:
                            account_posting.account_from_supplier = True
                        else:
                            account_posting.account_from_supplier = False

                        if 'group' in self.request.POST and self.request.POST['group'] != '':
                            account_posting.group_id = self.request.POST['group']

                        if 'role' in self.request.POST and self.request.POST['role'] != '':
                            account_posting.role_id = self.request.POST['role']

                        if 'is_default' in self.request.POST:
                            account_posting.is_default = True
                        else:
                            account_posting.is_default = False

                        account_posting.save()

                        if len(lines) == 0:
                            return HttpResponse(
                                json.dumps({'error': True,
                                            'alert': True,
                                            'text': 'No VAT posting has been created, the system will apply your '
                                                    'default VAT account posting. \r\n Do you want to EDIT or Continue',
                                            })
                            )
                        else:
                            return HttpResponse(
                                json.dumps({'error': False,
                                            'text': 'Account posting successfully updated',
                                            })
                            )
                    else:
                        return HttpResponse(
                            json.dumps({'error': True,
                                        'alert': True,
                                        'text': 'Expenditure allocation must add up to 100%',
                                        })
                        )
                except Exception as exception:
                    return HttpResponse(
                        json.dumps({'error': True,
                                    'text': 'Error updating the account posting {}'.format(exception)
                                   })
                    )
            else:
                return HttpResponse(
                    json.dumps({'error': True,
                                'text': 'Account posting not found and its pk {} '.format(self.kwargs['id'])
                                })
                )
        return redirect(reverse('account_posting_index'))


class AccountPostingLinesView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'account_postingline/lines.html'

    def get_account_posting(self):
        return get_object_or_None(AccountPosting, pk=self.kwargs['account_posting_id'])

    def get_company_objects(self, company):
        return company.company_objects.all()

    def get_lines_by_type(self, account_posting, linked_models):
        lines_by_type = {}
        total = 0
        for line_type in LineType:
            if line_type == 'expenditure':
                lines_by_type[line_type] = {'title': str(line_type), 'lines': [], 'total': 0}

        for line in account_posting.lines.all():
            posting_line = {
                'account': line.account,
                'vat_code': line.vat_code,
                'percentage': line.percentage,
                'amount': line.amount,
                'distribution': line.distribution,
                'id': line.id,
                'object_items': linked_models,
                'value': None
            }
            if line.type.value in lines_by_type:
                total = total + line.percentage
                lines_by_type[line.type.value]['total'] = lines_by_type[line.type.value]['total'] + line.percentage
                for object_item in line.object_items.all():
                    if object_item.company_object_id in linked_models:
                        posting_line['object_items'][object_item.company_object_id]['value'] = object_item.label
                lines_by_type[line.type.value]['lines'].append(posting_line)
        return lines_by_type

    def get_context_data(self, **kwargs):
        context = super(AccountPostingLinesView, self).get_context_data(**kwargs)
        account_posting = self.get_account_posting()
        company = self.get_company()
        linked_models = company.get_objects()
        lines_by_type = self.get_lines_by_type(account_posting=account_posting, linked_models=linked_models)
        linked_models_values = {}
        context['account_posting_types'] = lines_by_type
        context['linked_models_values'] = linked_models_values
        context['account_posting'] = account_posting
        context['object_categories'] = self.get_company_objects(company)
        context['linked_models'] = linked_models
        return context


class AddAccountPostingLineView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'account_postingline/addline.html'
    form_class = AccountPostingLineForm

    def get_account_posting(self):
        return get_object_or_None(AccountPosting, pk=self.kwargs['account_posting_id'])

    def get_roles(self, company):
        return AccountPosting.objects.filter(company=company).order_by('id')

    def get_company_object_categories(self, company):
        return company.company_objects.all()

    def get_form_kwargs(self):
        kwargs = super(AddAccountPostingLineView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddAccountPostingLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        roles = self.get_roles(company)
        object_categories = self.get_company_object_categories(company)
        paginator = Paginator(roles, 1)
        page = 1
        if 'page' in self.request.GET:
            page = self.request.GET['page']
        context['roles'] = paginator.page(page)
        context['object_categories'] = object_categories
        context['line_type'] = self.kwargs['line_type']
        context['account_posting'] = self.get_account_posting()
        return context

    def form_invalid(self, form):
        if is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error adding account posting.',
                                 'errors': form.errors
                                 })
        return super(AddAccountPostingLineView, self).form_invalid(form)

    def form_valid(self, form):
        if is_ajax_post(request=self.request):
            account_posting = self.get_account_posting()
            total_percentage = account_posting.total_percentage(form.cleaned_data['percentage'], self.kwargs['line_type'])

            if total_percentage <= 100:
                account_posting.is_active = True
                account_posting.save()

                account_posting_line = AccountPostingLine.objects.create(
                    account_posting=account_posting,
                    percentage=form.cleaned_data['percentage'],
                    distribution=form.cleaned_data['distribution'],
                    type=self.kwargs['line_type'],
                    account=form.cleaned_data['account']
                )
                if account_posting_line:
                    account_posting_line.object_items.clear()
                    items = self.request.POST.getlist('object_item')

                    object_items = [int(object_item) for object_item in items if (len(object_item) > 0)]
                    for object_item_id in object_items:
                        object_item = ObjectItem.objects.get(pk=object_item_id)
                        if object_item:
                            account_posting_line.object_items.add(object_item)

                account_posting.update_status()

                return JsonResponse({
                    'error': False,
                    'text': 'Account posting added successfully',
                    'redirect': reverse('show_account_posting', kwargs={'pk': account_posting.id})
                })
            else:
                raise ValueError('Account posting proposal percentage cannot exceed 100%')

        return HttpResponseRedirect(reverse('all-invoices'))


class EditAccountPostingLineView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'account_postingline/edit.html'
    success_url = reverse_lazy('account_posting_index')
    form_class = AccountPostingLineForm

    def get_account_posting_line(self):
        return get_object_or_None(AccountPostingLine, pk=self.kwargs['pk'])

    def get_roles(self, company):
        return AccountPosting.objects.filter(company=company).order_by('id')

    def get_company_object_categories(self, company):
        return company.company_objects.all()

    def get_initial(self):
        initial = super(EditAccountPostingLineView, self).get_initial()
        account_posting_line = self.get_account_posting_line()
        if account_posting_line:
            initial['percentage'] = account_posting_line.percentage
            initial['distribution'] = account_posting_line.distribution
        return initial

    def get_form_kwargs(self):
        kwargs = super(EditAccountPostingLineView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditAccountPostingLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        roles = self.get_roles(company)
        object_categories = self.get_company_object_categories(company)
        paginator = Paginator(roles, 1)
        page = 1
        if 'page' in self.request.GET:
            page = self.request.GET['page']
        context['roles'] = paginator.page(page)
        context['object_categories'] = object_categories
        context['account_posting_line'] = self.get_account_posting_line()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return HttpResponse(
                json.dumps({'error': True, 'text': 'Error editing account posting.', 'errors': form.errors})
            )
        return super(EditAccountPostingLineView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':

            try:
                account_posting_line = self.get_account_posting_line()
                if account_posting_line:
                    account_posting = account_posting_line.account_posting

                    total_percentage = account_posting.total_percentage(form.cleaned_data['percentage'],
                                                                        account_posting_line.type, account_posting_line)

                    if total_percentage <= 100:
                        account_posting_line.percentage = form.cleaned_data['percentage']
                        account_posting_line.distribution = form.cleaned_data['distribution']
                        account_posting_line.account_id = form.cleaned_data['account']
                        account_posting_line.save()

                        account_posting_line.object_items.clear()

                        items = self.request.POST.getlist('object_item')
                        object_items = [int(object_item) for object_item in items if (len(object_item) > 0)]
                        for object_item_id in object_items:
                            object_item = ObjectItem.objects.get(pk=object_item_id)
                            if object_item:
                                account_posting_line.object_items.add(object_item)

                        account_posting.update_status()

                        return JsonResponse({
                            'error': False,
                            'text': 'Account posting updated successfully',
                            'redirect': self.get_success_url()
                        })
                    else:
                        raise ValueError('Account posting proposal percentage cannot exceed 100%')
            except Exception as exception:
                return JsonResponse({
                    'error': True,
                    'text': 'Error occurred saving the account posting',
                    'details': '{}'.format(exception.__str__())
                })
        return HttpResponseRedirect(reverse('account_posting_index'))


class DeleteAccountPostingLineView(LoginRequiredMixin, View):
    def get_line(self):
        return get_object_or_None(AccountPostingLine, pk=self.kwargs['pk'])

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_account_posting_index',
                           kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('account_posting_index')

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            line = self.get_line()
            if line:
                try:
                    account_posting = line.account_posting
                    line.delete()
                    if account_posting:
                        account_posting.update_status()

                    return HttpResponse(
                        json.dumps({'error': False,
                                    'text': 'Line successfully deleted.',
                                    'redirect': self.get_success_url()
                                    })
                    )
                except Exception as exception:
                    return HttpResponse(
                        json.dumps({'error': True,
                                    'text': 'Error occurred deleting line.',
                                    'errors': exception
                                    })
                    )
            else:
                return HttpResponse(
                    json.dumps({'error': True, 'text': 'Line not found.'})
                )
        else:
            return HttpResponseRedirect(self.get_success_url())


class AddLineView(LoginRequiredMixin, FormView):
    template_name = 'account_postingline/add_line.html'

    def get_account_posting(self):
        return get_object_or_None(AccountPosting, pk=self.kwargs['account_posting_id'])

    def get_form_class(self):
        return AccountPostingLineForm

    def get_success_url(self):
        if 'master_company_id' in self.kwargs:
            return reverse('master_account_posting_index',
                           kwargs={'master_company_id': self.kwargs['master_company_id']})
        return reverse('account_posting_index')

    def get_company_id(self):
        if 'master_company_id' in self.kwargs:
            return self.kwargs['master_company_id']
        return self.request.session['company']

    def get_company(self):
        return Company.objects.get(pk=self.get_company_id())

    def get_roles(self, company):
        return AccountPosting.objects.filter(company=company).order_by('id')

    def get_company_object_categories(self, company):
        return company.generic_category.all()

    def get_form_kwargs(self):
        kwargs = super(AddLineView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddLineView, self).get_context_data(**kwargs)
        company = self.get_company()
        roles = self.get_roles(company)
        object_categories = self.get_company_object_categories(company)
        paginator = Paginator(roles, 1)
        page = 1
        if 'page' in self.request.GET:
            page = self.request.GET['page']
        context['roles'] = paginator.page(page)
        context['object_categories'] = object_categories
        return context

    def calculate_percentage(self, percentage, account_posting):
        total = percentage
        for line in account_posting.lines.all():
            total = line.percentage + percentage
        return total

    def form_invalid(self, form):
        if self.request.is_ajax():
            return HttpResponse(
                json.dumps({'error': True, 'text': 'Error adding account posting.', 'errors': form.errors})
            )
        return super(AddLineView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                posting_lines = []
                if 'posting_lines' in self.request.session:
                    posting_lines.append(json.dumps(list(form.cleaned_data)))
                self.request.session['posting_lines'] = posting_lines

                return JsonResponse({
                    'error': False,
                    'text': 'Account posting added successfully',
                    })
            except Exception as exception:
                return JsonResponse({'error': True, 'text': '{}'.format(exception.__str__())})
        return HttpResponseRedirect(reverse('all-invoices'))
