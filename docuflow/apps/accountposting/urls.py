from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.AccountPostingView.as_view(), name='account_posting_index'),
    path('create', views.AccountPostingCreateView.as_view(), name='create_account_posting'),
    path('edit/<int:pk>/', views.AccountPostingEditView.as_view(), name='edit_account_posting'),
    path('delete/<int:pk>/', views.DeleteAccountPostingView.as_view(), name='delete_account_posting'),
    path('detail/<int:pk>/', views.AccountPostingDetailView.as_view(), name='show_account_posting'),
    path('update/<int:pk>/', csrf_exempt(views.AccountPostingUpdateView.as_view()), name='update_account_posting'),
    path('add/lines/', views.AddLineView.as_view(), name='add_line'),
    path('lines/<int:account_posting_id>/', views.AccountPostingLinesView.as_view(), name='account_posting_lines'),
    path('create/line', views.AddAccountPostingLineView.as_view(), name='create_accounting_posting_line'),
    path('add/line/<int:account_posting_id>/<str:line_type>/', views.AddAccountPostingLineView.as_view(), 
         name='add_accounting_posting_line'),
    path('delete/line/<int:pk>/', views.DeleteAccountPostingLineView.as_view(), name='delete_account_posting_line'),
    path('edit/postings/line/<int:pk>/', views.EditAccountPostingLineView.as_view(), name='edit_accounting_posting_line'),
    path('master/<int:master_company_id>/', views.AccountPostingView.as_view(), name='master_account_posting_index'),
    path('master/<int:master_company_id>/create/', views.AccountPostingCreateView.as_view(), 
         name='create_master_account_posting'),
    path('master/<int:master_company_id>/edit/<int:pk>/', views.AccountPostingEditView.as_view(), 
         name='edit_master_account_posting'),
    path('master/<int:master_company_id>/delete/<int:pk>/', views.DeleteAccountPostingView.as_view(),
        name='delete_master_account_posting'),
    path('master/<int:master_company_id>/detail/<int:pk>/', views.AccountPostingDetailView.as_view(),
        name='show_master_account_posting'),
    path('master/<int:master_company_id>/add/lines/', views.AddLineView.as_view(), name='add_master_line'),
    path('master/<int:master_company_id>/lines/<int:account_posting_id>/', views.AccountPostingLinesView.as_view(),
         name='master_account_posting_lines'),
    path('master/<int:master_company_id>/create/line', views.AddAccountPostingLineView.as_view(),
         name='create_master_accounting_posting_line'),
    path('master/<int:master_company_id>/add/line/<int:account_posting_id>/<str:line_type>/',
         views.AddAccountPostingLineView.as_view(), name='add_master_accounting_posting_line'),
    path('master/<int:master_company_id>/delete/line/<int:pk>/', views.DeleteAccountPostingLineView.as_view(),
         name='delete_master_account_posting_line'),
    path('master/<int:master_company_id>/edit/postings/line/<int:pk>/', views.EditAccountPostingLineView.as_view(),
         name='edit_master_accounting_posting_line')
]

urlpatterns += [
    path('objectposting/', include('docuflow.apps.accountposting.modules.objectposting.urls')),
    path('objectitemposting/', include('docuflow.apps.accountposting.modules.objectitemposting.urls')),
]
