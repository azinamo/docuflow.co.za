from django.db import models

from enumfields.fields import EnumField
from safedelete.models import SafeDeleteModel, SOFT_DELETE


from .enums import LineType


class AccountPosting(models.Model):
    company = models.ForeignKey('company.Company', related_name="company_account_posting_proposals", on_delete=models.CASCADE)
    label = models.CharField(blank=True, default='', max_length=255, verbose_name='Account Posting Proposal')
    workflow = models.ForeignKey('workflow.Workflow', null=True, related_name="account_posting_proposal_workflow",
                                 verbose_name='Account Posting Proposal', on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', blank=True, null=True, related_name="role_account_posting_proposals",
                             on_delete=models.SET_NULL)
    group = models.ForeignKey('accounts.PermissionGroup', blank=True, null=True,
                              related_name="group_account_posting_proposals", on_delete=models.SET_NULL)
    account_from_supplier = models.BooleanField(default=False)
    is_default = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label

    # noinspection PyMethodMayBeStatic
    def is_valid_total_percentage(self, lines):
        if len(lines) > 0:
            total_expenditure = 0
            for line in lines:
                if line.type == LineType.EXPENDITURE:
                    total_expenditure = line.percentage + total_expenditure
            if int(total_expenditure) == 100:
                return True
            else:
                return False
        return True

    def total_percentage(self, percentage=None, typ=LineType.EXPENDITURE, account_posting_line=None):
        total = 0
        percentage = percentage if percentage else 0
        for line in self.lines.all():
            if line.type == typ:
                if account_posting_line and account_posting_line.id == line.id:
                    total = percentage + total
                else:
                    total = line.percentage + total
        return total

    def update_status(self):
        total_percentage = self.total_percentage()
        if total_percentage == 100:
            self.is_active = True
            self.save()
        else:
            self.is_active = False
            self.save()
        return None

    def save(self, *args, **kwargs):
        if self.is_default:
            AccountPosting.objects.all().update(is_default=False)
        return super(AccountPosting, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Account Posting Proposal'


class AccountPostingLine(models.Model):

    account_posting = models.ForeignKey(AccountPosting, related_name='lines', on_delete=models.CASCADE)
    object_items = models.ManyToManyField('company.ObjectItem')
    vat_code = models.ForeignKey('company.VatCode', blank=True, null=True, related_name='line_vat_code',
                                 on_delete=models.DO_NOTHING)
    distribution = models.PositiveIntegerField(verbose_name='Days of Distribution', blank=True, null=True)
    account = models.ForeignKey('company.Account', verbose_name='Account', blank=True, null=True,
                                on_delete=models.DO_NOTHING)
    amount = models.DecimalField(default=0, verbose_name='Amount', max_digits=15, decimal_places=2)
    percentage = models.DecimalField(default=0, verbose_name='Percentage (%)', max_digits=10, decimal_places=2)
    type = EnumField(LineType, default=LineType.EXPENDITURE, max_length=50)

    class Meta:
        verbose_name_plural = 'Account Posting Proposal'

    def __str__(self):
        return str(self.pk)


class ObjectPosting(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(max_length=255)
    company_object = models.ForeignKey('company.CompanyObject', related_name='account_posting_objects',
                                       on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False, verbose_name='Active')

    def __str__(self):
        return self.name

    @property
    def total_percentage(self):
        total = 0
        for line in self.object_splitting.all():
            total += float(line.percentage)
        return total

    @property
    def is_valid_percentage(self):
        total_percentage = int(self.total_percentage)
        return total_percentage == 100


class ObjectItemPosting(models.Model):
    object_posting = models.ForeignKey(ObjectPosting, related_name='object_splitting', on_delete=models.CASCADE)
    object_item = models.ForeignKey('company.ObjectItem', related_name='object_item_postings', on_delete=models.CASCADE)
    percentage = models.DecimalField(default=0, verbose_name='Percentage (%)', max_digits=10, decimal_places=2)

    class Meta:
        unique_together = ('object_posting', 'object_item')

    def __str__(self):
        return str(self.object_item)

    def save(self, *args, **kwargs):
        self.check_percentage()
        return super(ObjectItemPosting, self).save(*args, **kwargs)

    def check_percentage(self):
        items_postings = ObjectItemPosting.objects.filter(object_posting_id=self.object_posting.id)
        if self.pk:
            items_postings = items_postings.exclude(id=self.pk)
        total = float(self.percentage)
        for items_posting in items_postings:
            total += float(items_posting.percentage)

        if total > 100:
            raise ValueError(f'Total percentage ({total}) for this object posting cannot be more than 100%, please fix')

    @property
    def percentage_fraction(self):
        if self.percentage:
            return self.percentage / 100
        return 0
