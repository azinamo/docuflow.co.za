from django.apps import AppConfig


class AccountpostingConfig(AppConfig):
    name = 'docuflow.apps.accountposting'
