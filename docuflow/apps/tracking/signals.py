from django.dispatch import Signal

track = Signal(providing_args=[
    'profile', 'role', 'verb', 'target', 'comment', 'timestamp'
])
