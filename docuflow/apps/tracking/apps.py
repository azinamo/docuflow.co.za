from django.apps import AppConfig

from .signals import track
from .models import tracking_handler


class TrackingConfig(AppConfig):
    name = 'docuflow.apps.tracking'

    def ready(self):
        track.connect(tracking_handler, dispatch_uid='tracking.models.tracking')