from six import text_type

from django.contrib.contenttypes.fields import ContentType, GenericForeignKey
from django.db import models
from django.utils import timezone


# Create your models here.
class TrackingManager(models.Manager):

    # noinspection PyMethodMayBeStatic
    def create_tracking(self, model_object, comment, profile, role, data):
        return Tracking.objects.create(
            object_id=model_object.pk,
            content_type=ContentType.objects.get_for_model(model_object),
            comment=comment,
            profile=profile,
            role=role,
            data=data
        )

    def get_tracking(self, model_object):
        return self.filter(object_id=model_object.id, content_type=ContentType.objects.get_for_model(model_object))


class Tracking(models.Model):
    owner_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name='trackings')
    owner_id = models.PositiveIntegerField(null=True, blank=True)
    owner = GenericForeignKey('content_type', 'object_id')

    target_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.CASCADE, related_name='trackings')
    target_id = models.PositiveIntegerField(null=True, blank=True)
    target = GenericForeignKey('content_type', 'object_id')

    verb = models.CharField(max_length=255)
    comment = models.TextField()
    data = models.JSONField(blank=True)
    profile = models.ForeignKey('accounts.Profile', related_name='trackings', on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', related_name='trackings', on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = TrackingManager()


def tracking_handler(verb, **kwargs):
    kwargs.pop('signal', None)
    profile = kwargs.pop('profile')
    role = kwargs.pop('role')
    owner = kwargs.pop('owner')
    optional_objs = [
        (kwargs.pop(opt, None), opt)
        for opt in ('target', 'action_object')
    ]
    comment = kwargs.pop('comment', None)
    timestamp = kwargs.pop('timestamp', timezone.now())

    tracking = Tracking(
        profile=profile,
        role=role,
        owner_type=ContentType.objects.get_for_model(owner),
        owner_id=owner.pk,
        verb=text_type(verb),
        comment=comment,
        created_at=timestamp,
    )

    # Set optional objects
    for obj, opt in optional_objs:
        if obj is not None:
            setattr(tracking, '%s_object_id' % opt, obj.pk)
            setattr(tracking, '%s_content_type' % opt,
                    ContentType.objects.get_for_model(obj))

    if len(kwargs):
        tracking.data = kwargs

    return tracking
