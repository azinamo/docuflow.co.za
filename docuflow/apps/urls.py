from django.conf.urls import re_path, include

urlpatterns = [
    re_path(r'^roles/', include('docuflow.apps.roles.urls')),
    re_path(r'^workflow/', include('docuflow.apps.workflow.urls')),
    re_path(r'^sources/', include('docuflow.apps.sources.urls')),
    re_path(r'^documents/', include('docuflow.apps.documents.urls')),
    re_path(r'^invoices/', include('docuflow.apps.invoice.urls')),
    re_path(r'^inventory/', include('docuflow.apps.inventory.urls')),
    re_path(r'^plans/', include('docuflow.apps.subscription.urls')),
    re_path(r'^system/', include('docuflow.apps.system.urls')),
    re_path(r'^common/', include('docuflow.apps.common.urls')),
    re_path(r'^company/', include('docuflow.apps.company.urls')),
    re_path(r'^users/', include('docuflow.apps.accounts.urls')),
    re_path(r'^suppliers/', include('docuflow.apps.supplier.urls')),
    re_path(r'^accountposting/', include('docuflow.apps.accountposting.urls')),
    re_path(r'^years/', include('docuflow.apps.period.urls')),
    re_path(r'^journals/', include('docuflow.apps.journals.urls')),
    re_path(r'^intervention/', include('docuflow.apps.intervention.urls')),
    re_path(r'^vat/', include('docuflow.apps.vat.urls')),
    re_path(r'^sales/', include('docuflow.apps.sales.urls')),
    re_path(r'^fixedasset/', include('docuflow.apps.fixedasset.urls'))
]
