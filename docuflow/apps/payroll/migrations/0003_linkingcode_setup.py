# Generated by Django 3.2 on 2023-02-21 14:00

from django.db import migrations, models
import django.db.models.deletion
import docuflow.apps.payroll.enums
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0028_alter_payment_attachment'),
        ('supplier', '0018_auto_20220210_1641'),
        ('payroll', '0002_auto_20210623_0907'),
    ]

    operations = [
        migrations.CreateModel(
            name='Setup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('payroll_system', enumfields.fields.EnumField(default='PASTEL', enum=docuflow.apps.payroll.enums.PayrollSystem, max_length=10)),
                ('payment_type', models.CharField(max_length=50)),
                ('document_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='invoice.invoicetype')),
                ('supplier', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='supplier.supplier')),
            ],
        ),
        migrations.CreateModel(
            name='LinkingCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code_type', models.CharField(max_length=255)),
                ('code', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=255)),
                ('docuflow_code', models.CharField(max_length=50)),
                ('is_suffix', models.BooleanField()),
                ('setup', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='payroll.setup')),
            ],
        ),
    ]
