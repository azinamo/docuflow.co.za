from django.urls import path

from . import views

urlpatterns = [
    path('setup', views.SetupPayrollView.as_view(), name="payroll_setup"),
    path('statutory/setup/', views.StatutorySetupPayrollView.as_view(), name="statutory_setup"),
    path('save/statutory/setup', views.SaveStatutorySetupView.as_view(), name="save_statutory_setup"),
    path('<int:pk>/manage', views.ManagePayrollSetupView.as_view(), name="manage_payroll_setup"),
    path('setup/<str:item_type>/items/', views.PayrollItemsView.as_view(), name='add_payroll_items_line'),
    path('<int:pk>/edit/', views.EditSetupLineView.as_view(), name='edit_payroll_setup_line'),
    path('<int:pk>/delete/', views.DeleteSetupLineView.as_view(), name='delete_payroll_setup_line')
]