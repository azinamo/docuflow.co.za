from django.db import transaction
from django.http.response import JsonResponse, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import FormView, TemplateView, UpdateView, DeleteView, View
from django.urls import reverse

from docuflow.apps.common.mixins import ProfileMixin, DocuflowPermissionMixin
from docuflow.apps.common import utils
from .models import Setup, LinkingCode, StatutorySetup
from .forms import PayrollSetupForm, LinkingCodeForm


class SetupPayrollView(DocuflowPermissionMixin, ProfileMixin, FormView):
    form_class = PayrollSetupForm
    template_name = "payroll/setup.html"
    permission_required = False

    def has_permission(self):
        return True

    def get(self, request, *args, **kwargs):
        setup = Setup.objects.filter(supplier__company=self.get_company()).first()
        if setup:
            return HttpResponseRedirect(reverse('manage_payroll_setup', args=(setup.pk, )))
        return super().get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rows'] = []
        context['default_codes'] = {1: 'UIF', 2: 'PAYE', 3: 'WCA'}
        return context

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            if form.is_valid():
                with transaction.atomic():
                    setup = form.save()
                    return JsonResponse({
                        'text': 'Payroll setup saved successfully',
                        'error': False,
                        'redirect_url': reverse('manage_payroll_setup', args=(setup.pk, ))
                    })

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Payroll setup could not be saved, please fix the errors.',
                'errors': form.errors
            })
        return super(SetupPayrollView, self).form_invalid(form)


class StatutorySetupPayrollView(DocuflowPermissionMixin, ProfileMixin, TemplateView):
    template_name = "payroll/statutory_setup.html"
    permission_required = False

    def has_permission(self):
        return True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        setups = StatutorySetup.objects.select_related('account').filter(company=self.get_company()).all()
        rows = {}
        if setups:
            for setup in setups:
                rows[setup.id] = {
                    'code': 'UIF',
                    'description': 'Unemployment Insurance Fund',
                    'setup': setup
                }
        else:
            rows = {
                1: {
                    'code': 'UIF',
                    'description': 'Unemployment Insurance Fund'
                },
                2: {
                    'code': 'WCA',
                    'description': 'Workmans Compensation'
                }
            }
        context['default_codes'] = rows
        return context


class ManagePayrollSetupView(DocuflowPermissionMixin, ProfileMixin, UpdateView):
    form_class = PayrollSetupForm
    model = Setup
    template_name = "payroll/setup.html"
    permission_required = False

    def has_permission(self):
        return True

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['income_items'] = LinkingCode.objects.filter(code_type='income', setup=self.object).order_by('id')
        context['deduction_items'] = LinkingCode.objects.filter(code_type='deductions', setup=self.object).order_by('id')
        context['deduction_codes'] = ['UIF', 'PAYE', 'WCA']
        context['default_codes'] = {1: 'UIF', 2: 'PAYE', 3: 'WCA'}
        return context

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            if form.is_valid():
                with transaction.atomic():
                    form.save()
                    return JsonResponse({
                        'text': 'Payroll setup updated successfully',
                        'error': False,
                        'reload': True
                    })

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Payroll setup could not be updated, please fix the errors.',
                'errors': form.errors
            })
        return super(ManagePayrollSetupView, self).form_invalid(form)


class PayrollItemsView(DocuflowPermissionMixin, ProfileMixin, TemplateView):
    template_name = 'payroll/setup_lines.html'
    permission_required = False

    def has_permission(self):
        return True

    def get_rows(self):
        row_id = self.request.GET.get('row_id', None)
        requested_rows = int(self.request.GET.get('rows', 2))
        if row_id:
            return int(row_id) + requested_rows
        if requested_rows <= 1:
            requested_rows = max(requested_rows, 2)
        return requested_rows

    def get_row_start(self):
        row_id = self.request.GET.get('row_id', None)
        if row_id:
            return int(row_id)
        return 1

    def get_tabs_start(self, row_id, is_reload=False):
        start_at = 6
        if is_reload:
            return start_at
        if row_id:
            return 6 * row_id
        return start_at

    def get_context_data(self, **kwargs):
        context = super(PayrollItemsView, self).get_context_data(**kwargs)
        is_reload = True if self.request.GET.get('reload', 0) == '1' else False
        col_count = self.request.GET.get('cols', 9)
        row_id = self.get_row_start()
        row_count = self.get_rows()
        tabs_start = self.get_tabs_start(row_id, is_reload)
        rows = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['is_deletable'] = int(row_id) > 0
        context['row_id'] = row_id
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['tabs_start'] = tabs_start
        context['start'] = 0
        context['rows'] = rows
        context['is_reload'] = is_reload
        context['row_start'] = row_id
        context['item_type'] = self.kwargs['item_type']
        return context


class EditSetupLineView(ProfileMixin, UpdateView):
    template_name = 'payroll/edit_setup.html'
    form_class = LinkingCodeForm
    model = LinkingCode
    context_object_name = 'setup_line'

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the setup line, please fix the errors.',
                'errors': form.errors
            })
        return super(EditSetupLineView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            return JsonResponse({
                'error': False,
                'text': 'Setup line successfully updated',
                'reload': True
            })
        return super().form_valid(form)


class DeleteSetupLineView(ProfileMixin, LoginRequiredMixin, DeleteView):
    model = LinkingCode
    template_name = 'payroll/delete_setup.html'
    context_object_name = 'setup_line'

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the success URL.
        """
        LinkingCode.objects.get(pk=self.kwargs['pk']).delete()

        return JsonResponse({
            'text': 'Linking code successfully deleted.',
            'error': False,
            'reload': True
        })


class SaveStatutorySetupView(ProfileMixin, LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):

        for field, value in request.POST.items():
            if field.startswith('statutory_code', 0, 14):
                row_id = field[15:]

                account_id = request.POST[f'statutory_account_{row_id}'] or None
                StatutorySetup.objects.update_or_create(
                    code=value,
                    company=self.get_company(),
                    defaults={
                        'description': request.POST[f'statutory_description_{row_id}'],
                        'account_id': account_id,
                        'is_included_in_salary_cost': request.POST.get(f'statutory_include_in_salary_{row_id}', False),
                        'company_cost': request.POST[f'statutory_company_cost_{row_id}']
                    }
                )
        return JsonResponse({
            'text': 'Statutory setup successfully saved.',
            'error': False,
            'reload': True
        })