from django.db import models
from safedelete.models import SafeDeleteModel, SOFT_DELETE
from enumfields.fields import EnumField

from docuflow.apps.company.models import Company
from .enums import PayrollSystem, PaymentType


class Setup(models.Model):
    supplier = models.ForeignKey('supplier.Supplier', on_delete=models.PROTECT)
    document_type = models.ForeignKey('invoice.InvoiceType', on_delete=models.PROTECT)
    payroll_system = EnumField(PayrollSystem, default=PayrollSystem.PASTEL)
    payment_type = EnumField(PaymentType, default=PaymentType.INDIVIDUAL_TRANSFER, max_length=50)


class Employee(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey(Company, related_name='employees', null=True, blank=True, on_delete=models.CASCADE)
    employee_id = models.PositiveIntegerField(null=True, blank=True)
    first_name = models.CharField(max_length=255)
    second_name = models.CharField(max_length=255, blank=True)
    surname = models.CharField(max_length=255, blank=False)
    address_line_1 = models.CharField(max_length=255, blank=True, verbose_name='Address')
    address_line_2 = models.CharField(max_length=255, blank=True, verbose_name='Address Line 1')
    city = models.CharField(max_length=255, blank=True)
    province = models.CharField(max_length=50, blank=True)
    postal_code = models.CharField(max_length=50, blank=True)
    birthday = models.DateField(null=True, blank=True)
    income_tax_number = models.CharField(max_length=255, blank=True)
    start_date = models.DateField(null=True, blank=True)
    termination_date = models.DateField(null=True, blank=True)


class StatutorySetup(models.Model):
    company = models.ForeignKey('company.Company', on_delete=models.PROTECT)
    code = models.CharField(max_length=50)
    description = models.CharField(max_length=255)
    account = models.ForeignKey('company.Account', null=True, on_delete=models.PROTECT)
    is_included_in_salary_cost = models.BooleanField()
    company_cost = models.DecimalField(decimal_places=2, max_digits=5, default=0)


class LinkingCode(models.Model):
    setup = models.ForeignKey(Setup, on_delete=models.PROTECT)
    code_type = models.CharField(max_length=255)
    code = models.CharField(max_length=50)
    description = models.CharField(max_length=255)
    account = models.ForeignKey('company.Account', on_delete=models.PROTECT)
    is_suffix = models.BooleanField()

    class Meta:
        unique_together = ('setup', 'code')

    @property
    def is_income(self):
        return self.code_type == 'income'

    @property
    def is_deduction(self):
        return self.code_type == 'deduction'
