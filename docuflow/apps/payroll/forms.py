from django import forms


from .models import Setup, LinkingCode


class PayrollSetupForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(PayrollSetupForm, self).__init__(*args, **kwargs)
        self.fields['supplier'].queryset = self.fields['supplier'].queryset.filter(company=self.company)
        self.fields['document_type'].queryset = self.fields['document_type'].queryset.filter(company=self.company)

    class Meta:
        model = Setup
        fields = ('supplier', 'document_type', 'payroll_system', 'payment_type')

    def save(self, commit=True):
        setup = super(PayrollSetupForm, self).save(commit=commit)

        linking_codes = []
        for field, value in self.data.items():
            if field.startswith('income_pastel_account_code', 0, 26):
                row_id = field[27:]
                linking_code = self.add_linking_code(
                    setup=setup,
                    code_type='income',
                    row_id=row_id
                )
                if linking_code:
                    linking_codes.append(linking_code)
            elif field.startswith('deductions_pastel_account_code', 0, 31):
                row_id = field[31:]
                linking_code = self.add_linking_code(
                    setup=setup,
                    code_type='deductions',
                    row_id=row_id
                )
                if linking_code:
                    linking_codes.append(linking_code)
        if linking_codes:
            LinkingCode.objects.bulk_create(linking_codes)

        return setup

    def add_linking_code(self, setup, code_type, row_id):
        account = self.data.get(f'{code_type}_account_{row_id}')
        if not account:
            return None

        pastel_code = self.data.get(f'{code_type}_pastel_account_code_{row_id}')
        if not pastel_code:
            return None
        return LinkingCode(
            setup=setup,
            code_type=code_type,
            code=pastel_code,
            description=self.data.get(f'{code_type}_pastel_account_description_{row_id}'),
            account_id=account,
            is_suffix=self.data.get(f'{code_type}_suffix_{row_id}')
        )


class LinkingCodeForm(forms.ModelForm):

    class Meta:
        model = LinkingCode
        fields = ('code', 'description', 'account', 'description', 'is_suffix')

