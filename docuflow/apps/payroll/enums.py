from enumfields import Enum


class PayrollSystem(Enum):
    PASTEL = 'PASTEL'


class PaymentType(Enum):
    TOTAL_PAYROLL_TRANSFER = 'TOTAL_PAYROLL_TRANSFER'
    INDIVIDUAL_TRANSFER = 'INDIVIDUAL_TRANSFER'
