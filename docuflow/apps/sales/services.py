import logging
import os
import pprint
from decimal import Decimal

from django.conf import settings
from django.contrib.contenttypes.fields import ContentType
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.template.loader import get_template, render_to_string
from django.urls import reverse
from weasyprint import HTML

from docuflow.apps.common.enums import Module
from docuflow.apps.common.utils import allow_https_images
from docuflow.apps.company.models import Company, Account, ObjectItem, VatCode
from docuflow.apps.customer.enums import AddressType
from docuflow.apps.customer.models import Ledger as CustomerLedger, Customer
from docuflow.apps.inventory.models import BranchInventory, Ledger as InventoryLedger
from docuflow.apps.invoice.models import InvoiceType, Invoice as PurchaseInvoice, Comment as PurchaseComment
from docuflow.apps.invoice.services import AddToFlow
from docuflow.apps.sales.modules.backorder.services import CreateBackOrderFromSalesInvoice
from docuflow.apps.sales.modules.receipts.services import CreateInvoiceReceipt
from docuflow.apps.sales.modules.updates.services import InvoiceUpdater
from . import enums
from .exceptions import NotEnoughCreditException, PaymentNotDoneException, SalesException
from .forms import InvoiceInventoryForm, InvoiceAccountItemForm, CreatePurchaseInvoiceMixin
from .models import Update, Estimate, Delivery, InvoiceReceipt, Tracking, Invoice, InvoiceItem, PickingSlip

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class PricePercentageCalculatable:

    def __init__(self, percentage, price, *args, **kwargs):
        self.percentage = percentage
        self.price = price

    def calculate(self):
        return (self.percentage / 100) * self.price


class DiscountCalculator(PricePercentageCalculatable):

    def __init__(self, price, discount, *args, **kwargs):
        self.price = price
        self.percentage = discount
        self.discount_amount = 0
        super(DiscountCalculator, self).__init__(*args, **kwargs)


class VatCalculator(PricePercentageCalculatable):

    def __init__(self, price, discount, *args, **kwargs):
        self.price = price
        self.percentage = discount
        self.vat_amount = 0
        super(VatCalculator, self).__init__(*args, **kwargs)


class SalesInvoice(object):

    def __init__(self, sales_items):
        self.sales_items = sales_items
        self.has_customer_discount = False
        self.customer_discount = 0
        self.vat_on_discount = 0
        self.total_amount = 0
        self.net_amount = 0
        self.vat_amount = 0
        self.sub_total = 0

    def calculate_totals(self):
        for sales_item in self.sales_items:
            self.total_amount += sales_item.total
            self.sub_total += sales_item.sub_total
            self.vat_amount += sales_item.vat_amount
            self.net_amount += self.net_amount

        if self.has_customer_discount:
            self.total_amount += self.customer_discount
            self.vat_amount += self.vat_on_discount
            self.net_amount += (self.customer_discount + self.vat_on_discount)

        if self.has_customer_discount:
            self.total_amount += self.customer_discount


class SaleInvoiceItem:

    def __init__(self, sales_invoice, price, quantity, vat_code, **kwargs):
        self.is_credit = kwargs.get('is_credit', False)
        self.price = self.get_unit_price(price)
        self.quantity = self.get_unit_quantity(quantity)
        self.vat_code = vat_code
        self.sales_invoice = sales_invoice
        self.kwargs = kwargs

    def get_unit_quantity(self, quantity):
        unit_quantity = quantity
        if self.is_credit:
            unit_quantity = unit_quantity * -1
        return unit_quantity

    def get_unit_price(self, price):
        return price

    def calculate_sub_total(self):
        discount_amount = self.calculate_discount_amount()
        total = self.price_including()
        if discount_amount:
            total += discount_amount
        return round(total, 2)

    def price_including(self):
        logger.info(f"Calculate price including vat --> {self.price}")
        return self.price + self.calculate_vat_amount()

    def calculate_vat_amount(self):
        logger.info(f"Calculate vat code --> {self.vat_code}")
        vat_percentage = self.get_vat_percentage()
        vat_amount = 0
        if vat_percentage:
            vat_amount = (float(vat_percentage) / 100 * self.price) * self.quantity
        return Decimal(vat_amount)

    def calculate_net_price(self):
        quantity = self.get_unit_quantity(self.quantity)
        logger.info(f"Calculate net price including vat --> {self.price} * {quantity}")
        return Decimal(quantity) * Decimal(self.price)

    def get_vat_percentage(self):
        logger.info(f"Get vat percentage --> {self.vat_code}")
        return self.vat_code.percentage if self.vat_code and self.vat_code.percentage else 0

    def calculate_discount_amount(self):
        discount = self.get_discount()
        logger.info(f"Calculate discount amount --> {discount}")
        discount_amount = 0
        if discount > 0:
            discount_amount = (float(discount) / 100) * float(self.price)
        return Decimal(discount_amount)

    def calculate_total_discount_amount(self):
        discount_amount = self.calculate_discount_amount()
        logger.info(f"Discount amount --> {discount_amount}")
        if discount_amount > 0:
            return self.quantity * discount_amount
        return 0

    def get_discount(self):
        return self.kwargs.get('discount', 0)

    def get_invoice_dict(self):
        logger.info("Get the sales invoice object")
        invoice = {}
        if isinstance(self.sales_invoice, Delivery):
            invoice['delivery'] = self.sales_invoice
        elif isinstance(self.sales_invoice, Estimate):
            invoice['estimate'] = self.sales_invoice
        elif isinstance(self.sales_invoice, Invoice):
            invoice['available_quantity'] = self.quantity
            invoice['invoice'] = self.sales_invoice
        elif isinstance(self.sales_invoice, PickingSlip):
            invoice['picking_slip'] = self.sales_invoice
        logger.info(invoice)
        return invoice


class SaleAccountLine(SaleInvoiceItem):

    def __init__(self, sales_invoice, invoice_item_cls, account, price, quantity, vat_code, **kwargs):
        logger.info(' --------- SaleAccountLine --------')
        super(SaleAccountLine, self).__init__(sales_invoice, price, quantity, vat_code)
        self.sales_invoice = sales_invoice
        self.invoice_item_cls = invoice_item_cls
        self.account = account
        self.quantity = quantity
        self.price = price
        self.vat_code = vat_code
        self.kwargs = kwargs

    def execute(self):
        logger.info(' --------- EXECUTE --------')
        vat_amount = self.kwargs.get('vat_amount')
        discount_amount = self.kwargs.get('discount_amount')
        invoice_item = self.invoice_item_cls.objects.create(
            **self.get_invoice_dict(),
            account=self.account,
            discount=self.get_discount(),
            price=self.price,
            net_price=self.calculate_net_price(),
            vat_code=self.vat_code,
            quantity=self.get_unit_quantity(self.quantity),
            vat=self.get_vat_percentage(),
            item_type=enums.InvoiceItemType.GENERAL,
            description=self.kwargs.get('description'),
            vat_amount=vat_amount
        )
        logger.info(f"Done creating account invoice item {invoice_item}")
        return invoice_item


class SalesInventoryLine(SaleInvoiceItem):

    def __init__(self, sales_invoice, invoice_item_cls, inventory, price, quantity, vat_code, **kwargs):
        logger.info(f" --------- SalesInventoryLine -------- {inventory}")
        super(SalesInventoryLine, self).__init__(sales_invoice, price, quantity, vat_code)
        self.sales_invoice = sales_invoice
        self.invoice_item_cls = invoice_item_cls
        self.inventory = inventory
        self.kwargs = kwargs
        self.is_credit = kwargs.get('is_credit', False)

    def execute(self):
        vat_amount = self.kwargs.get('vat_amount')
        discount_amount = self.kwargs.get('discount_amount')
        sub_total = self.calculate_net_price()
        # discount_amount = self.calculate_total_discount_amount()
        net_price = sub_total
        if discount_amount:
            net_price = sub_total - discount_amount
        invoice_item = self.invoice_item_cls.objects.create(
            **self.get_invoice_dict(),
            inventory=self.inventory,
            discount=self.get_discount(),
            price=self.price,
            net_price=net_price,
            vat_code=self.vat_code,
            quantity=self.get_unit_quantity(self.quantity),
            vat=self.get_vat_percentage(),
            item_type=enums.InvoiceItemType.INVENTORY,
            vat_amount=vat_amount
        )

        add_ledger = self.kwargs.get('add_ledger', True)
        if invoice_item and add_ledger:
            self.add_inventory_ledger(invoice_item)
        return invoice_item

    def add_inventory_ledger(self, invoice_item):
        logger.info(f"Create inventory ledger for {invoice_item.inventory} ")
        InventoryLedger.objects.add_invoice_item_line(
            invoice_item, Module.SALES, 0, self.sales_invoice.profile
        )


class CreateInvoice:

    def __init__(self, company, profile, role, form, post_request, invoice_type):
        logger.info(f"Create invoice {invoice_type} -------------")
        self.company = company
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request
        self.invoice_type = invoice_type

    def get_company_invoice_type(self):

        invoice_type = InvoiceType.objects.filter(
            code=self.invoice_type,
            company=self.company,
            file__module=Module.SALES
        ).first()
        if not invoice_type:
            raise SalesException(f"Invoice type not found ({self.invoice_type}) -> {self.company.id}")
        return invoice_type

    def get_multiplier(self, invoice_type):
        if invoice_type.is_credit:
            return -1
        return 1

    def create(self):
        invoice_type = self.get_company_invoice_type()
        multiplier = self.get_multiplier(invoice_type)

        logger.info(f"--- start create invoice --- with multiplier {multiplier} ")
        invoice = self.form.save(commit=False)
        invoice.profile = self.profile
        invoice.created_by = self.profile
        if invoice.total_amount:
            invoice.total_amount = invoice.total_amount * multiplier
        if invoice.net_amount:
            invoice.net_amount = invoice.net_amount * multiplier
        if invoice.sub_total:
            invoice.sub_total = invoice.sub_total * multiplier
        if invoice.discount_amount:
            invoice.discount_amount = invoice.discount_amount * multiplier
        if invoice.vat_amount:
            invoice.vat_amount = invoice.vat_amount * multiplier
        if invoice.total_amount:
            invoice.open_amount = invoice.total_amount
        invoice.role = self.role
        invoice.invoice_type = invoice_type
        if self.is_draft():
            invoice.mark_as_draft()
        invoice.save()
        return invoice

    def is_draft(self):
        save_type = self.post_request.get('save_type', None)
        if save_type == 'draft':
            return True
        return False

    def execute(self):
        raise NotImplementedError("")

    def save_object_items(self, invoice):
        logger.info("Create object items")
        items = self.post_request.getlist('object_item')
        for object_item_id in items:
            object_item_str = str(object_item_id)
            if len(object_item_str) > 0:
                object_item = ObjectItem.objects.get(pk=int(object_item_id))
                if object_item:
                    logger.info(f"Create object item =--> {object_item}")
                    invoice.object_items.add(object_item)

    def save_invoice_items(self, invoice, is_new=True):
        posted_invoice_items = self.post_request.items()

        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        counter = 0
        for field, value in posted_invoice_items:
            if field.startswith('inventory_item', 0, 14):
                row_id = field[15:]
                if not self.is_new_line(row_id, is_new):
                    self.add_inventory_invoice_item(invoice, row_id, content_type)
                counter += 1
            elif field.startswith('account', 0, 7):
                row_id = field[8:]
                if not self.is_new_line(row_id, is_new):
                    self.add_account_invoice_item(invoice, row_id, content_type)
                counter += 1
            elif field.startswith('item_description', 0, 16):
                row_id = field[17:]
                if not self.is_new_line(row_id, is_new):
                    self.add_general_invoice_item(invoice, row_id, content_type)
                counter += 1
        if is_new and counter == 0:
            raise ValueError('No invoice lines were selected, please ensure you have at least 1 line in your invoice')

        if invoice.customer:
            invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(discount_amount=invoice.customer_discount)
        invoice.save()

    def is_new_line(self, row_id, is_new):
        if is_new:
            return False
        return bool(self.post_request.get(f'id_{row_id}'))

    def add_account_invoice_item(self, invoice, row_id, content_type):
        logger.info(f"------------ add_account_invoice_item  -------------------- {row_id}")
        account_id = self.post_request.get('account_{}'.format(row_id), None)
        logger.info(f"Account id {account_id}")
        # if not account_id:
        #     raise ValidationError('Please select a valid account code for the account line')
        if account_id:
            account = Account.objects.filter(id=int(account_id)).first()
            if account:
                description = self.post_request.get('description_{}'.format(row_id))
                quantity = Decimal(self.post_request.get(f"quantity_{row_id}", 0))
                price = Decimal(self.post_request.get(f"price_{row_id}"))
                discount_val = self.post_request.get(f"discount_percentage_{row_id}", 0)
                vat_amount = Decimal(self.post_request.get(f'vat_amount_{row_id}'))

                discount = discount_val if discount_val else 0
                vat_code_id = self.post_request.get('vat_code_{}'.format(row_id), None)
                vat_percentage_val = self.post_request.get('vat_percentage_{}'.format(row_id), 0)

                vat_code = VatCode.objects.filter(pk=vat_code_id).first()
                create_invoice_item = SaleAccountLine(invoice, InvoiceItem, account, price, quantity, vat_code,
                                                      **{'discount': discount, 'is_credit': invoice.is_credit,
                                                         'description': description, 'vat_amount': vat_amount})
                create_invoice_item.execute()

    def add_inventory_invoice_item(self, invoice, row_id, content_type):
        logger.info("------------ add_inventory_invoice_item   -------------------- ")
        inventory_item_id = self.post_request.get(f'inventory_item_{row_id}', None)
        # if not inventory_item_id:
        #     raise ValidationError('Please select a valid inventory code for the inventory lines')
        if inventory_item_id:
            inventory_item = BranchInventory.objects.filter(id=int(inventory_item_id)).first()
            if inventory_item:
                quantity = Decimal(self.post_request.get(f'quantity_{row_id}', 0))
                price = Decimal(self.post_request.get(f'price_{row_id}'))
                discount = Decimal(self.post_request.get(f'discount_percentage_{row_id}', 0))
                vat_code_id = self.post_request.get(f'vat_code_{row_id}')
                vat_amount = Decimal(self.post_request.get(f'vat_amount_{row_id}'))

                vat_code = VatCode.objects.filter(pk=vat_code_id).first()
                create_invoice_item = SalesInventoryLine(invoice, InvoiceItem, inventory_item, price, quantity,
                                                         vat_code, **{'discount': discount,
                                                                      'is_credit': invoice.is_credit,
                                                                      'content_type': content_type, 'vat_amount': vat_amount}
                                                         )
                create_invoice_item.execute()

    def add_general_invoice_item(self, invoice, row_id, content_type):
        logger.info("------------ add_general_invoice_item   -------------------- ")
        description = self.post_request.get(f'item_description_{row_id}')
        if description:
            return InvoiceItem.objects.create_description_line(
                description=description,
                invoice=invoice
            )

    def update_invoice_item(self, invoice, row_id):
        invoice_item_id = self.post_request.get(f'id_{row_id}', None)
        logger.info("Invoice item id {} for row {}".format(invoice_item_id, row_id))
        if invoice_item_id:
            invoice_item = InvoiceItem.objects.filter(id=int(invoice_item_id)).first()
            logger.info(f"update invoice item {invoice}  for row {row_id} for inventory {invoice_item}")
            if invoice_item:
                quantity = Decimal(self.post_request.get('quantity_{}'.format(row_id), 0))
                price = Decimal(self.post_request.get('price_{}'.format(row_id)))
                discount = Decimal(self.post_request.get('discount_percentage_{}'.format(row_id), 0))
                vat_code_id = self.post_request.get('vat_code_{}'.format(row_id))
                vat = Decimal(self.post_request.get('vat_percentage_{}'.format(row_id), 0))
                net_price = Decimal(self.post_request.get('total_price_excluding_{}'.format(row_id),  0))

                invoice_item.discount = discount
                invoice_item.price = price
                invoice_item.net_price = net_price
                invoice_item.vat_code_id = vat_code_id
                invoice_item.quantity = quantity
                invoice_item.vat = vat
                invoice_item.save()
                return invoice_item

    def create_invoice_receipt(self, invoice, payment_data):
        logger.info(f"---------- create_invoice_receipt {invoice.total_amount}--------------")
        payment_date = payment_data.get('date')
        invoice_total = invoice.total_amount * -1 if invoice.is_credit else invoice.total_amount
        payment_method_amounts = {}
        total_amount = Decimal(0)
        for payment_method_data in payment_data['payment_methods']:
            method_amount = Decimal(payment_method_data['amount'])
            method_amount * -1 if invoice.is_credit else method_amount
            payment_method_amounts[payment_method_data['payment_method']] = method_amount
            total_amount += method_amount

        receipt_amount = total_amount if invoice.is_credit else total_amount * -1
        if total_amount != invoice_total:
            raise PaymentNotDoneException('Please enter all the payment amount.')

        create_receipt = CreateInvoiceReceipt(invoice, invoice.branch, payment_date, receipt_amount, payment_method_amounts)
        receipt = create_receipt.execute()

        if receipt:
            invoice_receipt = InvoiceReceipt.objects.create(
                invoice=invoice,
                receipt=receipt,
                amount=invoice.total_amount * 1 if invoice.is_credit else invoice.total_amount
            )
            open_amount = invoice.open_amount - invoice_receipt.amount
            invoice.open_amount = open_amount
            invoice.save()
        return invoice

    def create_docuflow_invoice(self, sales_invoice):
        company = Company.objects.filter(slug=sales_invoice.customer.df_number).first()
        if company:
            if not sales_invoice.customer.supplier:
                raise ValueError(f'Customer default supplier for DF#{sales_invoice.customer.df_number} not found')

            profile = sales_invoice.created_by
            if sales_invoice.total_amount < 0:
                invoice_type = InvoiceType.objects.credit_note(company).first()
            else:
                invoice_type = InvoiceType.objects.tax_invoice(company).first()

            default_vat_code = VatCode.objects.input().default().filter(company=company).first()

            invoice = PurchaseInvoice()
            invoice.company = company
            invoice.invoice_type = invoice_type
            if default_vat_code:
                invoice.vat_code = default_vat_code
            invoice.total_amount = sales_invoice.total_amount
            invoice.supplier = sales_invoice.customer.supplier
            invoice.vat_number = sales_invoice.customer.supplier.vat_number
            invoice.vat_amount = sales_invoice.vat_amount
            invoice.net_amount = sales_invoice.net_amount
            invoice.sub_total = sales_invoice.sub_total
            invoice.open_amount = sales_invoice.total_amount
            invoice.invoice_number = str(sales_invoice)
            invoice.invoice_date = sales_invoice.invoice_date
            invoice.due_date = sales_invoice.due_date if sales_invoice.due_date else sales_invoice.created_at.date()
            invoice.accounting_date = sales_invoice.created_at.date()
            invoice.created_by = sales_invoice.profile
            invoice.workflow = company.default_flow_proposal
            invoice.period = sales_invoice.period
            invoice.sales_invoice = sales_invoice
            invoice.save()

            if invoice and invoice.is_account_posting_set():
                try:
                    add_to_flow = AddToFlow(invoice, profile, sales_invoice.role)
                    add_to_flow.process_flow()
                    invoice.log_activity(profile.user, sales_invoice.role.id, "Added document to flow",
                                         PurchaseComment.EVENT)
                except Exception as exception:
                    raise SalesException(exception)

    def create_address(self, invoice, is_default=None):
        customer = invoice.customer
        postal_address = {}
        delivery_address = {}
        for k, v in self.post_request.items():
            if v != '':
                if k[0:7] == 'postal_':
                    postal_key = k[7:]
                    postal_address[postal_key] = v
                if k[0:9] == 'delivery_':
                    delivery_key = k[9:]
                    delivery_address[delivery_key] = v

        if len(delivery_address):
            invoice.delivery_to = delivery_address
            customer.save_address(postal_address, AddressType.DELIVERY)
        if len(postal_address) > 0:
            invoice.postal = postal_address
            customer.save_address(postal_address, AddressType.POSTAL)
        invoice.save()

    def has_payment(self, request):
        payment = request.session.get('sales_invoice_receipt')
        print("Payment informtation ")
        print(payment)
        if not payment:
            raise PaymentNotDoneException('Payment information is required')
        if payment['balance'] != 0:
            raise PaymentNotDoneException('There is still an outstanding balance that needs to be paid')
        return payment

    def set_invoice_payment_status(self, invoice):
        invoice.mark_as_paid_not_printed()

    def add_comment(self, invoice):
        comment = self.form.cleaned_data.get('comment', None)
        if comment:
            Tracking.objects.create_comment(
                object_id=invoice.id,
                content_type=ContentType.objects.filter(app_label='sales', model='invoice').first(),
                comment=comment,
                **{'profile': self.profile, 'role': self.role}
            )

    def create_customer(self, invoice):
        customer = Customer.objects.create_cash_customer(invoice.branch.company, invoice.customer_name)
        if customer:
            invoice.customer = customer
            invoice.save()
        return customer


class CreateCustomerInvoice(CreateInvoice):

    def validate_customer(self):
        customer = self.form.cleaned_data['customer']
        is_override = 'is_override' in self.form.cleaned_data
        available_credit = customer.available_credit if customer.available_credit else Decimal(0)
        total_amount = self.form.cleaned_data['total_amount']

        if not self.is_draft() and not is_override:
            self.validate_credit_limit(available_credit, total_amount)

    def validate_credit_limit(self, available_credit, total_amount):
        if available_credit < total_amount:
            raise NotEnoughCreditException('Not enough credit to invoice this, please contact accounts'
                                           ' department or enter your 4 digit over ride key')

    def validate_totals(self, invoice):
        total_excluding = sum(invoice_item.total_price_excluding for invoice_item in invoice.invoice_items.all())

        if total_excluding != invoice.net_amount:
            raise ValidationError("Totals of lines not adding up")

    def execute(self):
        logger.info("-- Execute --")
        logger.info(self.form.cleaned_data)
        customer = self.form.cleaned_data['customer']
        payment_data = {}
        if customer.is_cash:
            payment_data = self.has_payment()

        self.validate_customer()

        invoice = self.create()
        if invoice:
            is_default = invoice.customer.is_default
            if is_default:
                self.create_customer(invoice)
                self.create_address(invoice)
                invoice.refresh_from_db()

            self.add_comment(invoice)

            self.save_object_items(invoice)
            self.save_invoice_items(invoice)

            self.create_address(invoice, is_default)
            # Update the customer available credit
            CustomerLedger.objects.create_from_invoice(invoice, self.profile)

            create_delivery = self.form.cleaned_data.get('create_delivery', False)
            if create_delivery:
                invoice.create_delivery()

            if payment_data:
                invoice = self.create_invoice_receipt(invoice, payment_data)
                self.set_invoice_payment_status(invoice)

            action = self.post_request.get('action')
            if action == 'send_to_docuflow':
                self.create_docuflow_invoice(invoice)


class CreateCashInvoice(CreateInvoice):

    def execute(self):

        customer = self.form.cleaned_data['customer']
        payment_data = {}
        if customer.is_cash:
            payment_data = self.has_payment()

        invoice = self.create()
        if invoice:
            if invoice.customer.is_default:
                self.create_customer(invoice)
                self.create_address(invoice)
                invoice.refresh_from_db()

            self.add_comment(invoice)

            self.save_object_items(invoice)
            self.save_invoice_items(invoice)

            # Update the customer available credit
            CustomerLedger.objects.create_customer_ledger(invoice, self.profile, True)

            create_delivery = self.form.cleaned_data.get('create_delivery', False)
            if create_delivery:
                invoice.create_delivery()

            if payment_data:
                self.create_invoice_receipt(invoice, payment_data)
                self.set_invoice_payment_status(invoice)

            action = self.post_request.get('action')
            if action == 'send_to_docuflow':
                self.create_docuflow_invoice(invoice)


class CreateInvoiceFromEstimate(CreateInvoice):

    def __init__(self, company, estimate, profile, role, invoice, request):
        logger.info("-------------START CreateInvoiceFromEstimate ---------- ")
        self.estimate = estimate
        self.profile = profile
        self.company = company
        self.role = role
        self.invoice = invoice
        self.post_request = request.POST
        self.request = request

    def execute(self):
        logger.info("-------------EXECUTE---------- ")
        self.add_tracking()

    def add_tracking(self):
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.estimate),
            object_id=self.estimate.pk,
            comment=f'Estimate {str(self.estimate.status)}',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'self',
                'reference': str(self.invoice),
                'link_url': reverse('view_sales_invoice', args=(self.invoice.pk,))
            }
        )

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.invoice),
            object_id=self.invoice.pk,
            comment=f'Invoice created from estimate',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'estimate',
                'reference': str(self.estimate),
                'link_url': reverse('estimate_detail', args=(self.estimate.pk,))
            }
        )


class CreateInvoiceFromBackOrder(CreateInvoice):

    def __init__(self, company, back_order, profile, role, invoice, invoice_type, request):
        logger.info("-------------START CreateInvoiceFromBackOrder ---------- ")
        self.back_order = back_order
        self.profile = profile
        self.company = company
        self.role = role
        self.invoice = invoice
        self.invoice_type = invoice_type
        self.post_request = request.POST
        self.request = request

    def execute(self):
        logger.info("-------------EXECUTE---------- ")
        self.create_invoice_items_from_back_order()

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.back_order),
            object_id=self.back_order.pk,
            comment='Invoice created from back order',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'invoice',
                'reference': str(self.invoice),
                'link_url': reverse('view_sales_invoice', args=(self.invoice.id, ))
            }
        )

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.invoice),
            object_id=self.invoice.pk,
            comment='Invoice created from back order',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'backorder',
                'reference': str(self.back_order),
                'link_type': reverse('backorder_detail', args=(self.back_order.id, ))
            }
        )

    def create_invoice_items_from_back_order(self):
        logger.info("-------------create_invoice_items_from_estimates---------- ")
        back_order_items = []

        for back_order_item in self.back_order.back_order_items.all():
            is_back_ordered = bool(self.post_request.get(f'back_order_{back_order_item.id}', False))
            quantity = self.post_request.get(f'quantity_{back_order_item.id}', 0)
            vat_amount = self.post_request.get(f'vat_amount_{back_order_item.id}', 0)
            total_amount = self.post_request.get(f'total_price_including_{back_order_item.id}', 0)

            if back_order_item.is_inventory:
                data = {'discount': back_order_item.discount,
                        'quantity': quantity,
                        'inventory': back_order_item.inventory,
                        'price': back_order_item.price,
                        'vat_code': back_order_item.vat_code,
                        'vat': back_order_item.vat,
                        'vat_amount': vat_amount,
                        'total_amount': total_amount,
                        'invoice': self.invoice
                        }
                logger.info("Inventory item data")
                logger.info(data)
                inventory_item_form = InvoiceInventoryForm(data=data, branch=self.invoice.branch)
                if inventory_item_form.is_valid():
                    invoice_item = inventory_item_form.save()
                    if is_back_ordered:
                        back_order_quantity = invoice_item.quantity * -1
                        logger.info(f"New back order quantity is {back_order_quantity} from old back order {back_order_item.quantity} - {invoice_item.quantity}")
                        if back_order_quantity != 0:
                            back_order_items.append({'back_order_item': back_order_item, 'quantity': back_order_quantity})
                else:
                    logger.info(inventory_item_form.errors)
            if back_order_item.is_account:
                data = {
                    'account': back_order_item.account,
                    'discount': back_order_item.discount,
                    'price': back_order_item.price,
                    'vat_code': back_order_item.vat_code,
                    'quantity': quantity,
                    'vat': back_order_item.vat,
                    'description': back_order_item.description,
                    'vat_amount': back_order_item.vat_amount
                }
                account_item_form = InvoiceAccountItemForm(data=data, branch=self.invoice.branch)
                if account_item_form.is_valid():
                    account_item_form.save()
                else:
                    logger.info(account_item_form.errors)
        logger.info("Back order items")
        logger.info(back_order_items)
        if len(back_order_items) > 0:
            create_back_order = CreateBackOrderFromSalesInvoice(self.back_order, back_order_items)
            create_back_order.execute()
        else:
            self.back_order.mark_as_completed()
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(self.back_order),
                object_id=self.back_order.pk,
                comment='Back order processed',
                profile=self.profile,
                role=self.role,
                data={
                    'tracking_type': 'self',
                    'reference': str(self.back_order)
                }
            )


class CreateInvoiceFromDelivery(CreateInvoice):

    def __init__(self, company, delivery, profile, role, invoice, invoice_type, request):
        logger.info("-------------START CreateInvoiceFromDelivery ---------- ")
        self.delivery = delivery
        self.profile = profile
        self.company = company
        self.role = role
        self.invoice = invoice
        self.invoice_type = invoice_type
        self.post_request = request.POST
        self.request = request

    def execute(self):
        logger.info("-------------EXECUTE---------- ")

        logger.info(f"Handle open amount --> {self.invoice} ")

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.invoice),
            object_id=self.invoice.id,
            comment='Invoice created',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'delivery',
                'reference': str(self.delivery),
                'link_url': reverse('delivery_detail', args=(self.delivery.pk, ))
            }
        )

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.delivery),
            object_id=self.delivery.id,
            comment='Invoice created',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'invoice',
                'reference': str(self.invoice),
                'link_url': reverse('view_sales_invoice', args=(self.invoice.pk, ))
            }
        )

        if self.delivery.estimate:
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(self.delivery.estimate),
                object_id=self.delivery.estimate_id,
                comment='Invoice created',
                profile=self.profile,
                data={
                    'tracking_type': 'self',
                    'reference': str(self.invoice),
                    'link_url': reverse('invoice_detail', args=(self.invoice.id, ))
                }
            )
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(self.invoice),
                object_id=self.invoice.id,
                comment='Invoice created',
                profile=self.profile,
                data={
                    'tracking_type': 'estimate',
                    'reference': str(self.delivery.estimate),
                    'link_url': reverse('estimate_detail', args=(self.delivery.estimate_id, ))
                }
            )


class CreditInvoice(CreateInvoice):

    def __init__(self, profile, role, invoice, credit_note, request):
        logger.info("-------------START Credit Invoices ---------- ")
        self.invoice = invoice
        self.credit_note = credit_note
        self.profile = profile
        self.role = role
        self.request = request
        self.post_request = request.POST

    def get_invoice_type(self):
        invoice_type = InvoiceType.objects.sales_credit_note(self.invoice.branch.company).first()
        if not invoice_type:
            raise SalesException(f"Invoice type {invoice_type} not found")
        return invoice_type

    def execute(self):
        payment_data = None
        if self.invoice.customer.is_cash:
            payment_data = self.has_payment(self.request)

        if self.credit_note:
            self.copy_invoice_items_from_invoice(self.credit_note, self.invoice)
            if payment_data:
                self.create_invoice_receipt(self.credit_note, payment_data)
                self.set_invoice_payment_status(self.credit_note)

            linked_invoice = self.credit_note.linked_invoice

            update = Update.objects.create(
                branch=self.credit_note.branch,
                profile=self.profile,
                role=self.role,
                date=linked_invoice.invoice_date,
                period=linked_invoice.period
            )
            if update:
                if not linked_invoice.is_printed:
                    logger.info(f"Update invoice {linked_invoice} --> {linked_invoice.invoice_type}")
                    sales_update = InvoiceUpdater(update, linked_invoice.invoice_type.code)
                    sales_update.create_invoices_update([linked_invoice])

                logger.info(f"Update credit note {self.credit_note} --> {self.credit_note.invoice_type}")
                sales_update = InvoiceUpdater(update, self.credit_note.invoice_type.code)
                sales_update.create_invoices_update([self.credit_note])

                self.credit_note.open_amount = 0
                self.credit_note.save()

                linked_invoice.open_amount = 0
                linked_invoice.save()

    def validate_available_quantity(self, invoice_item, quantity):
        if invoice_item.available_quantity and quantity > invoice_item.available_quantity:
            raise SalesException('Invoice quantity credited not available')

    def copy_invoice_items_from_invoice(self, invoice, source_invoice):
        logger.info("-------------create_invoice_items_from_source invoice---------- ")
        for source_invoice_item in source_invoice.invoice_items.all():
            quantity_value = self.post_request.get('quantity_{}'.format(source_invoice_item.id), None)
            if quantity_value:
                invoiced_quantity = Decimal(quantity_value)
                net_price = Decimal(self.post_request.get('total_price_excluding_{}'.format(source_invoice_item.id)))

                self.validate_available_quantity(source_invoice_item, invoiced_quantity)

                if invoiced_quantity and invoiced_quantity != 0:
                    quantity = invoiced_quantity * -1
                    net_price = net_price * -1
                    price = source_invoice_item.price
                    discount = source_invoice_item.discount * -1
                    description = self.post_request.get(f"item_description_{source_invoice_item.id}") or source_invoice_item.description

                    invoice_item = InvoiceItem.objects.create(
                        invoice=invoice,
                        inventory=source_invoice_item.inventory,
                        description=description,
                        quantity=quantity,
                        available_quantity=quantity,
                        unit=source_invoice_item.unit,
                        price=price,
                        discount=discount,
                        vat=source_invoice_item.vat,
                        net_price=net_price,
                        vat_code=source_invoice_item.vat_code,
                        item_link=source_invoice_item,
                        account=source_invoice_item.account
                    )
                    if invoice_item and invoice_item.inventory:
                        InventoryLedger.objects.add_invoice_item_line(
                            invoice_item=invoice_item,
                            module=Module.SALES,
                            unit_price=price,
                            profile=invoice.profile,
                            quantity=invoiced_quantity
                        )
                        source_invoice_item.update_available_quantity(invoiced_quantity)


class CopyInvoice(CreateInvoice):

    def __init__(self, invoice, profile, role, form, post_request):
        logger.info("-------------START Copy Invoices ---------- ")
        self.invoice = invoice
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request

    def create_invoice(self, source_invoice):
        logger.info("-------------CREATE INVOICE FROM FORM---------- ")
        invoice = Invoice()
        invoice.branch = source_invoice.branch
        logger.info(f"total amount {self.form.cleaned_data['total_amount']}")

        total_amount = self.form.cleaned_data['total_amount']
        net_amount = self.form.cleaned_data['net_amount']
        sub_total = self.form.cleaned_data['sub_total']
        discount_amount = self.form.cleaned_data['discount_amount']
        line_discount = self.form.cleaned_data['line_discount']
        vat_amount = self.form.cleaned_data['vat_amount']
        invoice_total_amount = total_amount
        invoice.total_amount = invoice_total_amount
        invoice.open_amount = total_amount
        invoice.net_amount = net_amount
        invoice.sub_total = sub_total
        invoice.discount_amount = discount_amount
        invoice.line_discount = line_discount
        invoice.vat = source_invoice.vat
        invoice.vat_amount = vat_amount
        invoice.profile = source_invoice.profile
        invoice.role = source_invoice.role
        invoice.customer = source_invoice.customer
        invoice.period = source_invoice.period
        invoice.category = source_invoice.category
        invoice.vat_number = source_invoice.vat_number
        invoice.delivery_number = source_invoice.delivery_number
        invoice.customer_name = source_invoice.customer_name
        invoice.supplier_number = source_invoice.supplier_number
        invoice.invoice_date = source_invoice.invoice_date
        if source_invoice.estimate:
            invoice.estimate = source_invoice.estimate
            invoice.period = source_invoice.estimate.period
        if source_invoice.vat_on_customer_discount:
            invoice.vat_on_customer_discount = source_invoice.vat_on_customer_discount * -1
        invoice.delivery_to = source_invoice.delivery_to
        invoice.postal = source_invoice.postal
        invoice.role = source_invoice.role
        invoice.linked_invoice = source_invoice
        invoice.invoice_type = source_invoice.invoice_type
        invoice.save()
        CustomerLedger.objects.create_customer_ledger(invoice, self.profile, True)
        return invoice

    def execute(self):
        logger.info("-------------EXECUTE---------- ")
        payment_data = None
        if self.invoice.customer.is_cash:
            payment_data = self.get_payment_data()

        credit_invoice = self.create_invoice(self.invoice)
        if credit_invoice:
            self.copy_invoice_items_from_invoice(credit_invoice, self.invoice)
            if payment_data:
                self.create_invoice_receipt(credit_invoice, payment_data)
                self.set_invoice_payment_status(credit_invoice)

    def validate_available_quantity(self, invoice_item, quantity):
        if invoice_item.available_quantity and quantity > invoice_item.available_quantity:
            raise SalesException('Invoice quantity credited not available')

    def copy_invoice_items_from_invoice(self, invoice, source_invoice):
        logger.info("-------------create_invoice_items_from_source invoice---------- ")
        for source_invoice_item in source_invoice.invoice_items.all():
            quantity_value = self.post_request.get(f'quantity_{source_invoice_item.id}', None)
            if quantity_value:
                invoiced_quantity = Decimal(quantity_value)
                net_price = Decimal(self.post_request.get(f'total_price_excluding_{source_invoice_item.id}'))

                self.validate_available_quantity(source_invoice_item, invoiced_quantity)

                if invoiced_quantity and invoiced_quantity != 0:
                    quantity = invoiced_quantity
                    net_price = net_price
                    price = source_invoice_item.price
                    discount = source_invoice_item.discount

                    invoice_item = InvoiceItem.objects.create(
                        invoice=invoice,
                        inventory=source_invoice_item.inventory,
                        description=source_invoice_item.description,
                        quantity=quantity,
                        available_quantity=quantity,
                        unit=source_invoice_item.unit,
                        price=price,
                        discount=discount,
                        vat=source_invoice_item.vat,
                        net_price=net_price,
                        vat_code=source_invoice_item.vat_code,
                        item_link=source_invoice_item
                    )
                    if invoice_item:
                        InventoryLedger.objects.add_invoice_item_line(
                            invoice_item=invoice_item,
                            module=Module.SALES,
                            unit_price=price,
                            profile=invoice.profile,
                            quantity=quantity
                        )
                        source_invoice_item.update_available_quantity(invoiced_quantity)


class CreateInvoiceFromProformaInvoice(CreateInvoice):

    def __init__(self, company, proforma_invoice, profile, role, invoice, invoice_type, request):
        logger.info("-------------START CreateInvoiceFromEstimate ---------- ")
        self.proforma_invoice = proforma_invoice
        self.profile = profile
        self.company = company
        self.role = role
        self.invoice = invoice
        self.invoice_type = invoice_type
        self.request = request
        self.post_request = request.POST

    def get_invoice_type(self):
        invoice_type = InvoiceType.objects.sales_tax_invoice(self.company).first()
        if not invoice_type:
            raise SalesException(f"Invoice type {invoice_type} not found")
        return invoice_type

    def execute(self):
        logger.info("-------------EXECUTE---------- ")
        payment_data = {}
        if self.proforma_invoice.is_cash:
            logger.info("-------------CASH INVOICE, CHECK FOR FOR PAYMENT---------- ")
            payment_data = self.has_payment(self.request)

        invoice_items = self.proforma_invoice.invoice_items.all()
        self.create_invoice_items_from_proforma_invoice_items(self.invoice, invoice_items, ContentType.objects.get_for_model(self.invoice))

        CustomerLedger.objects.create_customer_ledger(self.invoice, self.profile, True)

        if payment_data:
            self.create_invoice_receipt(self.invoice, payment_data)

        if self.invoice.is_cash:
            logger.info("This is a cash invoice, so mark it as paid not printed")
            self.set_invoice_payment_status(self.invoice)
        else:
            self.invoice.open_amount = self.invoice.total_amount
            self.invoice.save()

        self.proforma_invoice.mark_as_completed()

    def create_invoice_items_from_proforma_invoice_items(self, invoice, invoice_items, content_type):
        logger.info("-------------create_invoice_items_from_proforma_invoice_itemss---------- ")
        back_order_invoice_items = []
        non_inventory_items = []
        for invoice_item in invoice_items:
            is_back_ordered = bool(self.post_request.get('back_order_{}'.format(invoice_item.id), False))
            quantity = Decimal(self.post_request.get('quantity_{}'.format(invoice_item.id), 0))
            quantity_ordered = Decimal(self.post_request.get('quantity_ordered_{}'.format(invoice_item.id), 0))
            # net_price = float(self.post_request.get('total_price_excluding_{}'.format(estimate_item.id), 0))

            if is_back_ordered:
                back_order_quantity = quantity_ordered - quantity
                back_order_invoice_items.append({'item': invoice_item, 'quantity': back_order_quantity})
            if invoice_item.is_inventory:
                create_invoice_item = SalesInventoryLine(invoice, InvoiceItem, invoice_item.inventory,
                                                         invoice_item.price, quantity, invoice_item.vat_code,
                                                         **{'content_type': content_type})
                create_invoice_item.execute()

            if invoice_item.is_account:
                non_inventory_items.append({'item': invoice_item, 'quantity': invoice_item.quantity})
                create_invoice_item = SaleAccountLine(invoice, InvoiceItem, invoice_item.account, invoice_item.price,
                                                      invoice_item.quantity, invoice_item.vat_code)
                create_invoice_item.execute()

        if len(back_order_invoice_items) > 0:
            back_order_invoice_items = back_order_invoice_items + non_inventory_items
            create_back_order = CreateBackOrderFromSalesInvoice(self.proforma_invoice, back_order_invoice_items)
            create_back_order.execute()


def create_docuflow_invoice(sales_invoice, role, profile, request):
    send_to_docuflow = CreatePurchaseInvoiceMixin()
    send_to_docuflow.create_docuflow_invoice(sales_invoice=sales_invoice, role=role, profile=profile, request=request)


def create_address_from_request(sales_invoice, request):
    customer = getattr(sales_invoice, 'customer')
    if not customer:
        return None

    postal_address = {}
    delivery_address = {}
    for k, v in request.POST.items():
        if v != '':
            if k[0:7] == 'postal_':
                postal_key = k[7:]
                postal_address[postal_key] = v
            if k[0:9] == 'delivery_':
                delivery_key = k[9:]
                delivery_address[delivery_key] = v
    if 'country' in postal_address and len(postal_address['country']) > 0:
        customer.save_address(postal_address, 2)
    if 'country' in delivery_address and len(delivery_address['country']) > 0:
        customer.save_address(delivery_address, 0)

    if len(delivery_address):
        sales_invoice.delivery_to = delivery_address
    if len(postal_address) > 0:
        sales_invoice.postal = postal_address
    sales_invoice.save()


def vat_recon(sales_invoice, sales_invoice_items):
    summary = {'vat_codes': {}, 'total_vat': 0, 'total_excl': 0, 'customer_discounts': {}}
    for sales_invoice_item in sales_invoice_items:
        if sales_invoice_item.vat_code:
            net_price = sales_invoice_item.total_price_excluding if sales_invoice_item.total_price_excluding else 0
            vat_amount = sales_invoice_item.vat_amount if sales_invoice_item.vat_amount else Decimal(0)
            if sales_invoice_item.vat_code in summary['vat_codes']:
                summary['vat_codes'][sales_invoice_item.vat_code]['total_excl'] += net_price
                summary['vat_codes'][sales_invoice_item.vat_code]['total_vat'] += vat_amount
                summary['total_vat'] += vat_amount
                summary['total_excl'] += net_price
            else:
                summary['vat_codes'][sales_invoice_item.vat_code] = {'total_excl': net_price,
                                                                     'total_vat': vat_amount
                                                                     }
                summary['total_vat'] += vat_amount
                summary['total_excl'] += net_price

    if sales_invoice.customer_discount != 0:
        vat = 0
        discount_total = 0
        if sales_invoice.vat_on_customer_discount:
            vat = sales_invoice.vat_on_customer_discount
        if sales_invoice.customer_discount:
            discount_total = sales_invoice.customer_discount
        summary['customer_discounts'] = {'total': discount_total, 'perc': 0, 'total_vat': vat}
        summary['total_vat'] += vat
        summary['total_excl'] += discount_total

    return summary


def delete_invoices(invoices, profile, role):
    for invoice in invoices:
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(invoice),
            object_id=invoice.id,
            comment="Invoice deleted",
            profile=profile,
            role=role,
            data={
                'tracking_type': 'self',
                'reference': str(invoice)
            }
        )
        invoice.delete()


def add_delivery_tracking(invoice: Invoice, delivery: Delivery, profile, role):
    logger.info("-------------EXECUTE TRACKING---------- ")
    logger.info(f"Handle  --> {invoice} ")

    Tracking.objects.create(
        content_type=ContentType.objects.get_for_model(invoice),
        object_id=invoice.id,
        comment='Invoice created',
        profile=profile,
        role=role,
        data={
            'tracking_type': 'delivery',
            'reference': str(delivery),
            'link_url': reverse('delivery_detail', args=(delivery.pk,))
        }
    )

    Tracking.objects.create(
        content_type=ContentType.objects.get_for_model(delivery),
        object_id=delivery.id,
        comment='Invoice created',
        profile=profile,
        role=role,
        data={
            'tracking_type': 'invoice',
            'reference': str(invoice),
            'link_url': reverse('view_sales_invoice', args=(invoice.pk,))
        }
    )

    if delivery.estimate:
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(delivery.estimate),
            object_id=delivery.estimate_id,
            comment='Invoice created',
            profile=profile,
            data={
                'tracking_type': 'self',
                'reference': str(invoice),
                'link_url': reverse('invoice_detail', args=(invoice.id,))
            }
        )
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(invoice),
            object_id=invoice.id,
            comment='Invoice created',
            profile=profile,
            data={
                'tracking_type': 'estimate',
                'reference': str(delivery.estimate),
                'link_url': reverse('estimate_detail', args=(delivery.estimate_id,))
            }
        )


def send_invoice_email(invoice: Invoice):

    subject = f"Invoice {str(invoice)}"
    recipient_list = [invoice.customer.email]

    template = get_template('sales/email.html')
    message = template.render({
        'invoice': invoice,
        'company': invoice.branch.company,
        'base_url': settings.BASE_URL
    })
    logger.info(f"Sending email to {recipient_list} bcc = {settings.ADMIN_EMAIL}")
    email = EmailMessage(
        subject=subject,
        body=message,
        from_email='info@docuflow.co.za',
        to=recipient_list,
        # bcc=settings.ADMIN_EMAIL,
        reply_to=['info@docuflow.co.za']
    )
    email.content_subtype = "html"
    attachment = generate_invoice_pdf(invoice=invoice)
    if attachment and os.path.exists(attachment):
        email.attach_file(attachment)
    email.send()


def generate_invoice_pdf(invoice: Invoice):
    allow_https_images()
    _, dir_name = Company.create_company_dir('sales')

    banks = invoice.branch.company.bank_details.all()
    context = {
        'invoice': invoice,
        'banks': banks,
        'account_details': banks[0] if banks else None,
        'is_email': True,
        'is_print': True
    }
    html_string = render_to_string('sales/mail.html', context)

    # Using Rendered HTML to generate PDF
    pdf_doc = HTML(string=html_string, base_url='')
    pdf = pdf_doc.write_pdf(stylesheets=[
        settings.STATIC_IMAGES_ROOT + 'css/font-awesome.min.css',
        settings.STATIC_IMAGES_ROOT + 'css/pdf.css',
        settings.STATIC_IMAGES_ROOT + 'css/print/bootstrap2.css',
    ])

    filename = os.path.join(dir_name,  f"{str(invoice)}_invoice.pdf")
    if os.path.exists(dir_name):
        with open(filename, 'wb') as f:
            f.write(pdf)
    return filename
