from django.apps import AppConfig


class SalesConfig(AppConfig):
    name = 'docuflow.apps.sales'

    def ready(self):
        import docuflow.apps.sales.signal_receivers # noqa
