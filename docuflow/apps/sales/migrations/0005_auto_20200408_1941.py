# Generated by Django 3.0.4 on 2020-04-08 17:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0004_auto_20200403_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='backorder',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Customer discount'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='discount_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total Discount'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='discount_percentage',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Discount(%)'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='line_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Line discount'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='net_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Net'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='sub_total',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat(%)'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='vat_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='vat_on_customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='net_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='received',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Received'),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='backorderitem',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='cashaccount',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Deposit'),
        ),
        migrations.AlterField(
            model_name='cashup',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Deposit'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Customer discount'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='discount_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total Discount'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='discount_percentage',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Discount(%)'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='line_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Line discount'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='net_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Net'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='sub_total',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat(%)'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='vat_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='vat_on_customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='net_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='ordered',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Ordered'),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='received',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Received'),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='deliveryitem',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Customer discount'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='deposit',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Deposit(%)'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='discount_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total Discount'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='discount_percentage',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Discount(%)'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='line_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Line discount'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='net_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Net'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='sub_total',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat(%)'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='vat_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='vat_on_customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='estimateitem',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimateitem',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimateitem',
            name='net_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimateitem',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimateitem',
            name='quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimateitem',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='estimateitem',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Customer discount'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='discount_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total Discount'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='discount_percentage',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Discount(%)'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='line_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Line discount'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='net_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Net'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='sub_total',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat(%)'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='vat_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='vat_on_customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='available_quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='net_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoiceitem',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='invoicepayment',
            name='amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Amount'),
        ),
        migrations.AlterField(
            model_name='invoicepayment',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Discount'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Customer discount'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='discount_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total Discount'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='discount_percentage',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Discount(%)'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='line_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Line discount'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='net_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Net'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='sub_total',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Total'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat(%)'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='vat_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='vat_on_customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Vat'),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='customer_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='net_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='picked',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Picked'),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='quantity',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='received',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True, verbose_name='Received'),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='total_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='pickingslipitem',
            name='vat',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=15, null=True),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Amount'),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='balance',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Balance'),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='change',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Change'),
        ),
        migrations.AlterField(
            model_name='receipt',
            name='discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True, verbose_name='Change'),
        ),
        migrations.AlterField(
            model_name='receiptpayment',
            name='amount',
            field=models.DecimalField(decimal_places=2, max_digits=15, null=True, verbose_name='Amount'),
        ),
    ]
