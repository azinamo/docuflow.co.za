# Generated by Django 2.0 on 2020-06-30 17:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0013_auto_20200611_0123'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='invoice',
            name='category',
        ),
        migrations.AddField(
            model_name='estimate',
            name='proforma_invoice',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
