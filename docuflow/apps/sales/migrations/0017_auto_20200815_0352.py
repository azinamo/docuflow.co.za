# Generated by Django 3.1 on 2020-08-15 01:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0016_auto_20200811_1848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='backorder',
            name='delivery_to',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='backorder',
            name='postal',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='delivery_to',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='delivery',
            name='postal',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='delivery_to',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='estimate',
            name='postal',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='delivery_to',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='postal',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='delivery_to',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='pickingslip',
            name='postal',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='proformainvoice',
            name='delivery_to',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='proformainvoice',
            name='postal',
            field=models.JSONField(null=True),
        ),
    ]
