# Generated by Django 3.1 on 2020-08-11 16:48

from django.db import migrations
import docuflow.apps.sales.enums
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0015_auto_20200704_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receipt',
            name='receipt_type',
            field=enumfields.fields.EnumIntegerField(default=1, enum=docuflow.apps.sales.enums.ReceiptType),
        ),
    ]
