from django.dispatch import receiver
from django.db.models.signals import post_save

from docuflow.apps.customer.models import Ledger as CustomerLedger, History, OpeningBalance as CustomerOpeningBalance, AgeAnalysis
from docuflow.apps.customer.services import age_customer_invoice
from docuflow.apps.sales.models import InvoiceUpdate
from docuflow.apps.period.models import Year


@receiver(post_save, sender=InvoiceUpdate, dispatch_uid='invoice_update')
def handle_invoice_update(sender, instance, created, **kwargs):
    if created:
        invoice = instance.invoice
        profile = instance.update.profile
        year = instance.update.period.period_year
        CustomerLedger.objects.mark_invoice_as_completed(invoice=invoice, profile=profile)

        invoice.mark_as_updated()

        age_customer_invoice(invoice=invoice)

        history, is_new = History.objects.get_or_create(customer=invoice.customer, period=invoice.period)
        if history:
            history.add_invoice(invoice=invoice)

        if year.is_closing:
            # Copy closing year Closing balance for customer to started year Opening balances for customer
            period_age, _ = AgeAnalysis.objects.get_or_create(period=year.last_period, customer=invoice.customer)
            started_year = Year.objects.started().filter(company=year.company).first()
            opening_balance, _ = CustomerOpeningBalance.objects.get_or_create(
                customer=period_age.customer,
                year=started_year
            )
            opening_balance.amount = period_age.balance
            opening_balance.save()
