import logging
import pprint
from decimal import Decimal
from typing import List

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as __

from django_select2.forms import HeavySelect2Widget
from django_countries.fields import CountryField, Country
from django_countries.widgets import CountrySelectWidget

from docuflow.apps.customer.modules.address.forms import PostalAddressForm, DeliveryAddressForm
from docuflow.apps.common.enums import Module, SaProvince
from docuflow.apps.common.widgets import DataAttributesSelect
from docuflow.apps.common.utils import to_decimal
from docuflow.apps.company.models import Company
from docuflow.apps.comment.services import create_comments
from docuflow.apps.company.models import VatCode
from docuflow.apps.customer.models import Ledger as CustomerLedger, Customer, Address
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.invoice.forms import CreateInvoiceForm as CreatePurchaseInvoiceForm
from docuflow.apps.inventory.models import Ledger as InventoryLedger
from docuflow.apps.period.models import Period, Year
from docuflow.apps.sales.modules.backorder.services import CreateBackOrderFromSalesInvoice
from docuflow.apps.sales.modules.updates.services import InvoiceUpdater
from docuflow.apps.sales.modules.receipts.services import CreateInvoiceReceipt
from docuflow.apps.workflow.models import Workflow
from .exceptions import PaymentNotDoneException, NotEnoughCreditException, SalesException
from .models import Invoice, Tracking, InvoiceItem, InvoiceReceipt, Receipt, SundryReceiptAllocation, Update
from . import enums

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


def customer_select_data(company, include_cash=True):
    customer_data = {'data-detail-url': {'': ''},
                     'data-payment-term': {'': ''},
                     'data-payment-days': {'': ''}
                     }
    query = Customer.objects.filter(company=company)
    if not include_cash:
        query = query.exclude_cash()
    customers = query.all().order_by('-is_default')
    for c in customers:
        customer_data['data-detail-url'][c.id] = reverse_lazy('customer_detail', kwargs={'pk': c.id})
        customer_data['data-payment-term'][c.id] = c.payment_term
        customer_data['data-payment-days'][c.id] = c.days
    return DataAttributesSelect(choices=[('', '-----------')] + [(c.id, str(c.name)) for c in customers], data=customer_data)


def validate_invoice_items_totals(invoice: Invoice, is_existing=False):
    if invoice.customer_discount:
        return validate_invoice_items_net_totals(invoice=invoice)
    logger.info(f"Found {InvoiceItem.objects.filter(invoice=invoice).count()} invoice items, is credit {invoice.is_credit}")
    total_amount = sum(invoice_item.total_amount for invoice_item in InvoiceItem.objects.filter(invoice=invoice) if invoice_item.total_amount)
    if not is_existing:
        # For new invoices the total amount is multiplied by 1
        total_amount = total_amount * -1 if invoice.is_credit else total_amount
    total_amount = abs(total_amount)
    invoice_total_amount = abs(invoice.total_amount)
    logger.info(f"Total from invoice {invoice.total_amount} vs from lines {total_amount}")
    if invoice_total_amount != total_amount:
        raise SalesException(__('Invoice lines total amount does not add to the invoice total, please ensure all lines'
                                ' are added correctly'))


def validate_invoice_items_net_totals(invoice: Invoice):
    items_net_amount = abs(sum(invoice_item.net_price for invoice_item in InvoiceItem.objects.filter(invoice=invoice) if invoice_item.net_price))
    logger.info(f"Found {InvoiceItem.objects.filter(invoice=invoice).count()} invoice items for credit note = {items_net_amount}, is credit {invoice.is_credit} => {invoice.sub_total}")
    total_amount = items_net_amount * -1 if invoice.is_credit else items_net_amount
    invoice_amount = abs(invoice.sub_total)
    logger.info(f"Total from invoice {invoice.sub_total} vs from lines {items_net_amount}")
    if invoice_amount != items_net_amount:
        raise forms.ValidationError(__('Invoice lines total amount does not add to the invoice total, please ensure '
                                    'all lines are added correctly'))


class BaseSalesInvoiceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        if 'company' in kwargs:
            self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')

        super().__init__(*args, **kwargs)

        self.fields['customer'].widget = customer_select_data(self.company)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.company)
        self.fields['branch'].empty_label = None
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    def create_address(self, sales_invoice, request):
        postal_address = {}
        delivery_address = {}
        for k, v in request.POST.items():
            if v != '':
                if k[0:7] == 'postal_':
                    postal_key = k[7:]
                    postal_address[postal_key] = v
                if k[0:9] == 'delivery_':
                    delivery_key = k[9:]
                    delivery_address[delivery_key] = v
        if 'country' in postal_address and len(postal_address['country']) > 0:
            sales_invoice.customer.save_address(postal_address, 2)
        if 'country' in delivery_address and len(delivery_address['country']) > 0:
            sales_invoice.customer.save_address(delivery_address, 0)

        if len(delivery_address):
            sales_invoice.delivery_to = delivery_address
        if len(postal_address) > 0:
            sales_invoice.postal = postal_address
        sales_invoice.save()

    def add_comment(self, invoice):
        pass

    def create_customer(self, company, customer_name):
        if not customer_name:
            raise ValueError(__('Customer name is required'))
        return Customer.objects.create_cash_customer(company, customer_name)

    # def execute(self, *args, **kwargs):
    #     raise NotImplementedError('Method execute not implemented')


class BaseSalesInventoryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super().__init__(*args, **kwargs)
        self.fields['inventory'].queryset = self.fields['inventory'].queryset.filter(branch=self.branch)
        self.fields['vat_code'].queryset = VatCode.objects.output().filter(company=self.branch.company)

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data['inventory']:
            self.add_error('inventory', __('Inventory is required'))

        # quantity = cleaned_data.get('quantity')
        # if not price:
        #     raise ValueError(f"Please enter a valid price for {cleaned_data['inventory']}")

        # if not quantity:
        #     raise ValueError(f"Please enter a valid quantity for {cleaned_data['inventory']}")

        return cleaned_data

    def get_vat_percentage(self):
        if self.cleaned_data['vat_code']:
            return self.cleaned_data['vat_code'].percentage
        return 0

    def calculate_sub_total(self):
        discount_amount = self.calculate_discount_amount()
        total = self.price_including()
        if discount_amount:
            total += discount_amount
        return round(total, 2)

    def price_including(self):
        return self.price + self.calculate_vat_amount()

    def calculate_vat_amount(self):
        vat_percentage = self.get_vat_percentage()
        vat_amount = 0
        if vat_percentage:
            vat = float(vat_percentage) / 100
            vat_amount = Decimal(vat) * self.cleaned_data['price'] * self.cleaned_data['quantity']
        return Decimal(vat_amount)

    def calculate_net_price(self):
        return self.cleaned_data['quantity'] * self.cleaned_data['price']


class BaseSalesAccountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.branch.company)
        self.fields['vat_code'].queryset = VatCode.objects.output().filter(company=self.branch.company)

    def clean(self):
        cleaned_data = super().clean()

        account = cleaned_data.get('account')
        price = cleaned_data.get('price')
        if not account:
            raise ValueError(f"Please select a valid account for the account line")

        # if not price:
        #     raise ValueError(f"Please enter a valid amount for the account line {cleaned_data['account']}")

        return cleaned_data


class BaseSalesDescriptionForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()

        description = cleaned_data.get('description')
        if not description:
            raise ValueError(f"Please enter a valid description for text line")

        return cleaned_data


class CreateInvoiceItemMixin:

    # noinspection PyUnresolvedReferences
    def save_invoice_items(self, invoice, allow_empty=False):
        # TODO - Ensure only new items gets saved. add more testing here
        logger.info("--------save_invoice_items-----------")
        counter = 0
        total_amount = 0
        for field, value in self.request.POST.items():
            if field.startswith('inventory', 0, 9):
                row_id = field[10:]
                has_inventory = self.request.POST.get(f'inventory_{row_id}')
                has_quantity = True if f'quantity_{row_id}' in self.request.POST else False
                if has_inventory and has_quantity:
                    inventory_item = self.handle_inventory_line(invoice, row_id)
                    if inventory_item:
                        logger.info(f"Inventory item {inventory_item} has a total amount of {inventory_item.total_amount}")
                        total_amount += inventory_item.total_amount
                        counter += 1
            elif field.startswith('account', 0, 7):
                row_id = field[8:]
                has_account = self.request.POST.get(f'account_{row_id}')
                has_quantity = False if f'quantity_{row_id}' in self.request.POST else True
                if has_account:
                    account_item = self.handle_account_line(invoice, row_id)
                    if account_item:
                        logger.info(account_item)
                        logger.info(f"Account item {account_item} has a total amount of {account_item.total_amount}")
                        total_amount += account_item.total_amount
                        counter += 1
            elif field.startswith('item_description', 0, 16):
                if value:
                    description_item = self.handle_description_line(invoice, field[17:])
                    if description_item:
                        counter += 1
        if counter == 0 and not allow_empty:
            raise forms.ValidationError('No invoice lines were selected, please ensure you have at least 1 line in'
                                        ' your invoice')

    # noinspection PyUnresolvedReferences
    def handle_inventory_line(self, invoice, row_id):
        data = {
            'inventory': self.request.POST.get(f'inventory_{row_id}', None),
            'quantity': self.request.POST.get(f'invoiced_{row_id}', 0),
            'price': self.request.POST.get(f'price_{row_id}', 0),
            'discount': self.request.POST.get(f'discount_percentage_{row_id}', 0),
            'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
            'vat': self.request.POST.get(f'vat_percentage_{row_id}'),
            'total_amount': self.request.POST.get(f'total_price_including_{row_id}'),
            'vat_amount': self.request.POST.get(f'vat_amount_{row_id}'),
            'invoice': invoice
        }
        try:
            inventory_item = InvoiceItem.objects.get(pk=self.request.POST.get(f'id_{row_id}'))
            inventory_form = InvoiceInventoryForm(instance=inventory_item, data=data, branch=invoice.branch)

        except InvoiceItem.DoesNotExist:
            inventory_form = InvoiceInventoryForm(data=data, branch=invoice.branch)
        if inventory_form.is_valid():
            inventory_item = inventory_form.save()
        else:
            logger.info(inventory_form.errors)
            raise ValidationError("Inventory line could not be saved")
        return inventory_item

    # noinspection PyUnresolvedReferences
    def handle_account_line(self, invoice, row_id, is_new_line=True):
        data = {
            'account': self.request.POST.get(f'account_{row_id}', 0),
            'quantity': self.request.POST.get(f'invoiced_{row_id}', 0),
            'price': self.request.POST.get(f'price_{row_id}', 0),
            'discount': self.request.POST.get(f'discount_percentage_{row_id}', 0),
            'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
            'vat': self.request.POST.get(f'vat_percentage_{row_id}'),
            'total_amount': self.request.POST.get(f'total_price_including_{row_id}'),
            'description': self.request.POST.get(f'description_{row_id}'),
            'vat_amount': self.request.POST.get(f'vat_amount_{row_id}'),
            'invoice': invoice
        }
        try:
            account_item = InvoiceItem.objects.get(pk=self.request.POST.get(f'id_{row_id}'))
            invoice_account_form = InvoiceAccountItemForm(instance=account_item, data=data, branch=invoice.branch)
        except InvoiceItem.DoesNotExist:
            invoice_account_form = InvoiceAccountItemForm(data=data, branch=invoice.branch)
        if invoice_account_form.is_valid():
            account_item = invoice_account_form.save()
        else:
            logger.info(invoice_account_form.errors)
            raise ValidationError("Invoice account line could not be saved")
        return account_item

    # noinspection PyUnresolvedReferences
    def handle_description_line(self, invoice, row_id):
        data = {
            'description': self.request.POST.get(f'item_description_{row_id}', 0),
            'invoice': invoice
        }
        description_item = None
        try:
            description_item = InvoiceItem.objects.get(pk=self.request.POST.get(f'id_{row_id}'))
            invoice_item_form = InvoiceTextItemForm(instance=description_item, data=data, branch=invoice.branch)
        except InvoiceItem.DoesNotExist:
            invoice_item_form = InvoiceTextItemForm(data=data, branch=invoice.branch)
        if invoice_item_form.is_valid():
            logger.info(invoice_item_form.errors)
            description_item = invoice_item_form.save(invoice)
            logger.info(f"Saved account item {description_item}")
        return description_item


class SalesInvoiceForm(CreateInvoiceItemMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        self.role = kwargs.pop('role')
        self.company = self.branch.company
        super(SalesInvoiceForm, self).__init__(*args, **kwargs)
        self.fields['delivery_address'].queryset = Address.objects.filter(customer__company=self.company)
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['customer'].widget = customer_select_data(self.branch.company)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.branch.company)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.branch.company)
        # TODO - Validate the invoice type, only allow sales invoice types
        self.fields['estimate_number'].label = 'Our Ref. Number'
        self.fields['po_number'].label = 'Your Ref. Number'
        if self.instance.pk:
            if self.instance.delivery_to:
                self.fields['delivery_country'].initial = self.instance.delivery_to.get('country', settings.COUNTRY_CODE)
            self.fields['postal_country'].required = False
        self.fields['delivery_country'].required = False

    create_delivery = forms.BooleanField(widget=forms.CheckboxInput(), required=False, label='Delivery')
    comment = forms.CharField(widget=forms.Textarea(), required=False, label='Comment')
    is_override = forms.BooleanField(required=False, widget=forms.RadioSelect())
    save_type = forms.CharField(required=False)
    customer_name = forms.CharField(required=False)
    postal_street = forms.CharField(required=False)
    postal_city = forms.CharField(required=False)
    postal_province = forms.CharField(required=False)
    postal_postal_code = forms.CharField(required=False)
    postal_country = CountryField(default=settings.COUNTRY_CODE).formfield()
    address_id = forms.IntegerField(required=False, widget=forms.HiddenInput())
    delivery_address = forms.ModelChoiceField(required=False, queryset=None)
    delivery_street = forms.CharField(required=False)
    delivery_city = forms.CharField(required=False)
    delivery_province = forms.CharField(required=False)
    delivery_postal_code = forms.CharField(required=False)
    delivery_country = CountryField(default=settings.COUNTRY_CODE).formfield()

    class Meta:
        model = Invoice
        exclude = ('intervention_at', 'invoice_id', 'created_at', 'created_by', 'status', 'delivery_to', 'postal')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'vat_number': forms.TextInput(attrs={'class': 'form-control',
                                                 'data-update-vat-url': reverse_lazy('update_customer_vat_number')}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'sub_total': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'invoice_type': forms.HiddenInput(),
            'postal_country': CountrySelectWidget()
        }

    def is_draft(self, request):
        return request.POST.get('save_type', None) == 'draft'

    def create_customer(self, invoice):
        customer = Customer.objects.create_cash_customer(invoice.branch.company, self.cleaned_data['customer_name'])
        if not customer:
            raise ValidationError('Cash customer could not be created, please try again')
        invoice.customer = customer
        invoice.save()
        return customer

    def create_address(self, invoice):
        if self.cleaned_data['postal_street']:
            postal_address = {
                'address_line_1': self.cleaned_data['postal_street'],
                'city': self.cleaned_data['postal_city'],
                'postal_code': self.cleaned_data['postal_postal_code'],
                'province': self.cleaned_data['postal_province'],
                'country': self.cleaned_data['postal_country'],
                'customer': invoice.customer
            }
            postal_address_form = PostalAddressForm(data=postal_address)
            if postal_address_form.is_valid():
                postal_address = postal_address_form.save(commit=False)
                postal_address.save()
            # else:
            #     logger.info(postal_address_form.errors)

        if self.cleaned_data['delivery_street']:
            delivery_address_data = {
                'address_line_1': self.cleaned_data['delivery_street'],
                'city': self.cleaned_data['delivery_city'],
                'postal_code': self.cleaned_data['delivery_postal_code'],
                'province': self.cleaned_data['delivery_province'],
                'country': self.cleaned_data['delivery_country'],
                'customer': invoice.customer
            }
            if self.cleaned_data['address_id']:
                try:
                    delivery_address = Address.objects.get(pk=self.cleaned_data['address_id'])
                except Address.DoesNotExist:
                    delivery_address = None
                delivery_address_form = DeliveryAddressForm(data=delivery_address_data, instance=delivery_address)
            else:
                delivery_address_form = DeliveryAddressForm(data=delivery_address_data)
            if delivery_address_form.is_valid():

                delivery_address_data.pop('customer')
                delivery_address_data['country_name'] = Country(code=self.cleaned_data['postal_country']).name
                invoice.delivery_to = delivery_address_data
                invoice.save()
            else:
                logger.info(delivery_address_form.errors)

    def get_multiplier(self, invoice_type):
        multiplier = -1 if invoice_type.is_credit else 1
        if self.instance.pk:
            multiplier = 1
        return multiplier

    def clear_removed_invoice_items(self, invoice):
        kept_items = []
        for field, value in self.request.POST.items():
            if field.startswith('id', 0, 2):
                kept_items.append(value)

        invoice_items = InvoiceItem.objects.filter(invoice=invoice).exclude(
            pk__in=[int(kept_id) for kept_id in kept_items]
        )
        InventoryLedger.objects.filter(
            object_id__in=invoice_items.values_list('pk'),
            content_type=ContentType.objects.get_for_model(model=InvoiceItem, for_concrete_model=False)
        ).delete()
        invoice_items.delete()


class ReceiptedInvoiceMixin(object):

    # noinspection PyUnresolvedReferences
    def get_receipt_payments(self):
        payment = self.request.session.get('sales_invoice_receipt')

        if not payment:
            raise PaymentNotDoneException('Payment information is required')
        if payment['balance'] != 0:
            raise PaymentNotDoneException('There is still an outstanding balance that needs to be paid')
        return payment

    # noinspection PyUnresolvedReferences
    def create_invoice_receipt(self, invoice, payment_data):
        logger.info(f"---------- create_invoice_receipt  {invoice.total_amount}--------------")
        logger.info(payment_data)
        payment_date = payment_data.get('date')
        payment_method_amounts = {}
        total_amount = Decimal(0)
        receipt_amount = 0
        for payment_method_data in payment_data['payment_methods']:
            method_amount = Decimal(payment_method_data['amount'])
            method_amount * -1 if invoice.is_credit else method_amount
            payment_method_amounts[payment_method_data['payment_method']] = method_amount
            payment_method_amount = round(method_amount, 2)
            total_amount += payment_method_amount
            receipt_amount += payment_method_amount
        for receipt_data in payment_data['receipts']:
            if receipt_data['amount'] != '':
                amt = receipt_data['amount'].replace(',', '')
                allocated_receipt_amount = Decimal(amt).quantize(Decimal('0.01'))
                total_amount += allocated_receipt_amount
                allocated_receipt = Receipt.objects.get(pk=receipt_data['receipt'])
                allocated_receipt.balance += allocated_receipt_amount
                allocated_receipt.save()

        receipt_amount = receipt_amount * -1
        if total_amount != invoice.invoice_total:
            raise PaymentNotDoneException('Please enter all the payment amount.')

        create_receipt = CreateInvoiceReceipt(invoice, invoice.branch, payment_date, receipt_amount, payment_method_amounts)
        receipt = create_receipt.execute()
        if receipt:
            invoice_receipt = InvoiceReceipt.objects.create(
                invoice=invoice,
                receipt=receipt,
                amount=invoice.total_amount
            )
            open_amount = invoice.open_amount - invoice_receipt.amount
            invoice.open_amount = open_amount
            invoice.save()

            for receipt_data in payment_data['receipts']:
                if receipt_data['amount'] != '':
                    SundryReceiptAllocation.objects.create(
                        receipt_id=receipt_data['receipt'],
                        parent=receipt,
                        amount=str(receipt_data['amount']).replace(',', '')
                    )

        if 'sales_invoice_receipt' in self.request.session:
            del self.request.session['sales_invoice_receipt']
        return invoice


class CreatePurchaseInvoiceMixin(object):

    # noinspection PyMethodMayBeStatic
    # noinspection PyUnresolvedReferences
    def create_docuflow_invoice(self, sales_invoice, role, profile, request, add_to_flow=True):
        if not sales_invoice.customer.df_number:
            raise SalesException(__('Df number not found'))

        company = Company.objects.filter(slug=sales_invoice.customer.df_number).first()

        if not company:
            raise SalesException(__(f'Company with df number {sales_invoice.customer.df_number} not found'))

        logger.info(f"df number is {sales_invoice.customer.df_number} -> {company}")
        logger.info(f"Send to document company {company} to supplier -> {sales_invoice.customer.supplier} "
                    f"")

        if not sales_invoice.customer.supplier:
            raise SalesException(__(f'Supplier not set on the customer'))

        if not sales_invoice.customer.supplier:
            raise SalesException(__(f'Supplier({sales_invoice.customer.supplier}) Vat number not correct in Customer'))

        if sales_invoice.total_amount < 0:
            invoice_type = InvoiceType.objects.credit_note(company).first()
        else:
            invoice_type = InvoiceType.objects.tax_invoice(company).first()

        if not invoice_type:
            raise SalesException(__(f'Invoice type not found for company {company.name}'))

        default_vat_code = VatCode.objects.input().default().filter(company=company).first()
        if not default_vat_code:
            raise SalesException(__(f'Default vat code for {company.name} not found'))

        accounting_date = sales_invoice.invoice_date
        if not sales_invoice.invoice_date:
            accounting_date = sales_invoice.created_at.date()

        flow_proposal = company.default_flow_proposal
        if not flow_proposal:
            flow_proposal = Workflow.objects.filter(is_default=True, company=company).first()

        if not flow_proposal:
            flow_proposal = Workflow.objects.filter(company=company).first()

        if not flow_proposal:
            raise SalesException(__(f'{company} Does not have default flow to send this invoice'))

        year = Year.objects.started().filter(company=company).first()
        period = Period.objects.open(company=company, year=year).first()
        if not period:
            raise SalesException(__(f'{company} Does not have open periods to send this invoice'))

        data = {
            'invoice_type': invoice_type.id,
            'vat_code': default_vat_code.id,
            'supplier': sales_invoice.customer.supplier.id,
            'supplier_name': sales_invoice.customer.supplier.name,
            'supplier_number': sales_invoice.customer.supplier.code,
            'vat_number': sales_invoice.customer.supplier.vat_number,
            'total_amount': sales_invoice.total_amount,
            'vat_amount': sales_invoice.vat_amount,
            'net_amount': sales_invoice.net_amount,
            'open_amount': sales_invoice.total_amount,
            'sub_total': sales_invoice.sub_total,
            'invoice_number': str(sales_invoice),
            'invoice_date': sales_invoice.due_date if sales_invoice.due_date else sales_invoice.created_at.date(),
            'accounting_date': accounting_date,
            'due_date': sales_invoice.due_date if sales_invoice.due_date else sales_invoice.created_at.date(),
            'workflow': flow_proposal.pk,
            'period': period.pk,
            'sales_invoice': sales_invoice.pk,
            'comments': f'Document send from {sales_invoice.branch.company}',
            'add_to_flow': add_to_flow
        }

        if period and not period.is_valid(accounting_date):
            raise SalesException(__(f"{company}'s period {period} that you are sending to is not yet open,"
                                    f" please contact your customer"))

        purchase_invoice_form = CreatePurchaseInvoiceForm(
            data=data,
            company=company,
            profile=profile,
            role=role,
            request=request,
            year=year
        )
        if purchase_invoice_form.is_valid():
            invoice = purchase_invoice_form.save()
            return invoice
        else:
            errors = dict(purchase_invoice_form.errors)
            logger.info(errors)
            raise SalesException(__('Invoice could not be send to docuflow'))


class CustomerInvoiceForm(ReceiptedInvoiceMixin, CreatePurchaseInvoiceMixin, SalesInvoiceForm):

    def clean(self):
        cleaned_data = super().clean()
        invoice_date = cleaned_data['invoice_date']
        period = cleaned_data['period']
        if not self.cleaned_data['customer']:
            self.add_error('customer', __('Customer is required'))

        if not period:
            self.add_error('period', __('Period is required'))

        if not invoice_date:
            self.add_error('invoice_date', __('Date is required'))

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', __('Period and payment date do not match'))

        if not (self.cleaned_data['is_pick_up'] or (self.instance and self.instance.is_pick_up)):
            if not self.cleaned_data['delivery_country']:
                self.add_error('delivery_country', __('Delivery country is required'))
            if not self.cleaned_data['delivery_street']:
                self.add_error('delivery_street', __('Delivery street is required'))

        return cleaned_data

    def add_comment(self, invoice):
        content_type = ContentType.objects.get_for_model(invoice)
        create_comments(self.request, content_type, invoice.id, invoice.profile, invoice.role)

    def save(self, commit=True):
        is_existing = bool(self.instance.pk)

        multiplier = self.get_multiplier(self.cleaned_data['invoice_type'])
        invoice = super().save(commit=False)
        invoice.branch = self.branch
        invoice.profile = self.profile
        invoice.role_id = self.request.session['role']
        invoice.save()
        self.save_m2m()

        invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)

        if not is_existing:
            if invoice.total_amount:
                invoice.total_amount = invoice.total_amount * multiplier
            if invoice.net_amount:
                invoice.net_amount = invoice.net_amount * multiplier
            if invoice.sub_total:
                invoice.sub_total = invoice.sub_total * multiplier
            if invoice.discount_amount:
                invoice.discount_amount = invoice.discount_amount * multiplier
            if invoice.vat_amount:
                invoice.vat_amount = invoice.vat_amount * multiplier
        if invoice.total_amount:
            invoice.open_amount = invoice.total_amount
        invoice.save()

        payment_data = {}
        if invoice.open_amount != 0:
            if invoice.customer.is_cash:
                if not invoice.is_fully_paid():
                    payment_data = self.get_receipt_payments()
            else:
                is_override = self.cleaned_data.get('is_override', None)
                available_credit = invoice.customer.calculate_available
                is_draft = self.cleaned_data.get('save_type', '') == 'draft'

                if not is_draft and not is_override:
                    if available_credit < invoice.total_amount:
                        raise NotEnoughCreditException(__('Not enough credit to invoice this, please contact accounts '
                                                       'department or enter your 4 digit over ride key'))

        if invoice.customer and invoice.customer.is_default:
            self.create_customer(invoice=invoice)
            invoice.refresh_from_db()
        self.create_address(invoice=invoice)

        self.add_comment(invoice=invoice)

        # self.save_object_items(invoice)
        self.clear_removed_invoice_items(invoice=invoice)
        self.save_invoice_items(invoice=invoice)

        validate_invoice_items_totals(invoice=invoice, is_existing=is_existing)

        CustomerLedger.objects.create_from_invoice(invoice=invoice, profile=self.profile)

        if payment_data:
            if invoice.receipts.exists():
                invoice.receipts.delete()
                # self.update_invoice_receipt(invoice)

            invoice = self.create_invoice_receipt(invoice=invoice, payment_data=payment_data)
            invoice.mark_as_paid_not_printed()

        # TODO - Check if we using the available credit just for display or for other checks vs
        #  available_credit = invoice.customer.calculate_available
        # Update the customer available credit
        invoice.update_customer_credit()

        action = self.request.POST.get('action')
        if action == 'send_to_docuflow':
            self.create_docuflow_invoice(sales_invoice=invoice, role=self.role, profile=self.profile,
                                         request=self.request)
        return invoice


class RecurringInvoiceForm(SalesInvoiceForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        self.role = kwargs.pop('role')
        self.company = self.branch.company
        super(SalesInvoiceForm, self).__init__(*args, **kwargs)

        self.fields['delivery_address'].queryset = Address.objects.filter(customer__company=self.company)
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['customer'].widget = customer_select_data(self.branch.company, False)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.branch.company)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.branch.company)
        # TODO - Validate the invoice type, only allow sales invoice types
        self.fields['estimate_number'].label = 'Our Ref. Number'
        self.fields['po_number'].label = 'Your Ref. Number'
        if self.instance.pk:
            self.fields['postal_country'].required = False

    def clean(self):
        if not self.cleaned_data['customer']:
            self.add_error('customer', 'Customer is required')

        return self.cleaned_data

    def save(self, commit=True):
        invoice = super().save(commit=False)
        invoice.is_recurring = True
        invoice.branch = self.branch
        invoice.profile = self.profile
        invoice.role_id = self.request.session['role']
        invoice.save()
        self.save_m2m()

        multiplier = self.get_multiplier(invoice.invoice_type)
        invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)

        if invoice.total_amount:
            invoice.total_amount = invoice.total_amount * multiplier
        if invoice.net_amount:
            invoice.net_amount = invoice.net_amount * multiplier
        if invoice.sub_total:
            invoice.sub_total = invoice.sub_total * multiplier
        if invoice.discount_amount:
            invoice.discount_amount = invoice.discount_amount * multiplier
        if invoice.vat_amount:
            invoice.vat_amount = invoice.vat_amount * multiplier
        if invoice.total_amount:
            invoice.open_amount = invoice.total_amount
        invoice.save()

        if not invoice.customer and invoice.customer.is_default:
            self.create_customer(invoice)
            invoice.refresh_from_db()
        self.create_address(invoice)

        self.clear_removed_invoice_items(invoice)
        self.save_invoice_items(invoice)

        validate_invoice_items_totals(invoice=invoice)

        Tracking.objects.create(
            profile=self.profile,
            content_type=ContentType.objects.get_for_model(invoice),
            object_id=invoice.id,
            comment='Recurring invoice created',
            data={
                'tracking_type': 'self',
                'reference': str(invoice)
            }
        )

        return invoice


class CreditInvoiceForm(ReceiptedInvoiceMixin, CreatePurchaseInvoiceMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        self.invoice = kwargs.pop('invoice')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(CreditInvoiceForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(company, year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=company)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=company)
        self.fields['invoice_type'].queryset = InvoiceType.objects.filter(company=company, file__module=Module.SALES.value)
        self.fields['customer_name'].required = True
        self.fields['customer'].widget = customer_select_data(company)
        self.fields['estimate_number'].label = 'Our Ref. Number'
        self.fields['po_number'].label = 'Your Ref. Number'

        self.initial['customer_name'] = str(self.invoice.customer)
        self.initial['estimate_number'] = self.invoice.estimate_number
        self.initial['estimate_number'] = self.invoice.estimate_number
        self.initial['salesman'] = self.invoice.salesman
        self.initial['pricing'] = self.invoice.pricing
        self.initial['customer'] = self.invoice.customer
        self.initial['po_number'] = self.invoice.po_number
        self.initial['estimate_number'] = self.invoice.estimate_number
        self.initial['vat_number'] = self.invoice.customer.vat_number
        self.initial['delivery_method'] = self.invoice.customer.delivery_method
        self.initial['due_date'] = self.invoice.due_date

    postal_province = forms.ChoiceField(required=False, choices=SaProvince.choices())
    delivery_province = forms.ChoiceField(required=False, choices=SaProvince.choices())

    postal_country = CountryField(default=settings.COUNTRY_CODE, blank=True).formfield()
    delivery_country = CountryField(default=settings.COUNTRY_CODE, blank=True).formfield()

    class Meta:
        model = Invoice
        exclude = ('intervention_at', 'invoice_id', 'created_at', 'created_by', 'payment_date', 'status',
                   'delivery_to', 'postal')
        widgets = {
            'note': forms.TextInput(attrs={'class': 'form-control'}),
            'invoice_date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'vat_code': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'sub_total': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(CreditInvoiceForm, self).clean()

        invoice_date = cleaned_data['invoice_date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Date is required')

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', 'Period and payment date do not match')

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')
        if 'total_amount' not in cleaned_data:
            self.add_error('total_amount', 'Amount is required')
        if 'vat_amount' not in cleaned_data:
            self.add_error('vat_amount', 'Amount is required')
        return cleaned_data

    def get_invoice_type(self):
        if self.invoice.invoice_type.code == enums.InvoiceType.TAX_INVOICE.value:
            invoice_type = InvoiceType.objects.sales_credit_note(company=self.invoice.branch.company).first()
        else:
            invoice_type = InvoiceType.objects.sales_tax_invoice(company=self.invoice.branch.company).first()
        if not invoice_type:
            raise forms.ValidationError(f"Invoice type {invoice_type} not found")
        return invoice_type

    def save(self, commit=True):
        credit_note = super().save(commit=False)
        logger.info("-------------CREATE INVOICE FROM FORM---------- ")
        credit_note.branch = self.invoice.branch
        total_amount = self.cleaned_data['total_amount']
        net_amount = self.cleaned_data['net_amount']
        sub_total = self.cleaned_data['sub_total']
        discount_amount = self.cleaned_data['discount_amount']
        line_discount = self.cleaned_data['line_discount']
        vat_amount = self.cleaned_data['vat_amount']
        if total_amount:
            invoice_total_amount = total_amount * -1
            credit_note.total_amount = invoice_total_amount
            credit_note.open_amount = total_amount * -1
        if net_amount:
            credit_note.net_amount = net_amount * -1
        if sub_total:
            credit_note.sub_total = sub_total * -1
        if discount_amount:
            credit_note.discount_amount = discount_amount * -1
        if line_discount:
            credit_note.line_discount = line_discount
        credit_note.vat = self.invoice.vat
        if vat_amount:
            credit_note.vat_amount = vat_amount * -1

        credit_note.profile = self.profile
        credit_note.role = self.role
        credit_note.customer = self.invoice.customer
        credit_note.customer_name = self.invoice.customer_name
        credit_note.supplier_number = self.invoice.supplier_number
        if self.invoice.estimate:
            credit_note.estimate = self.invoice.estimate
        if self.invoice.vat_on_customer_discount:
            credit_note.vat_on_customer_discount = self.invoice.vat_on_customer_discount * -1
        credit_note.delivery_to = self.invoice.delivery_to
        credit_note.postal = self.invoice.postal
        credit_note.role = self.invoice.role
        credit_note.linked_invoice = self.invoice
        credit_note.invoice_type = self.get_invoice_type()
        credit_note.save()

        CustomerLedger.objects.create_customer_ledger(invoice=credit_note, profile=self.profile, is_pending=True)

        self.add_comment(credit_note=credit_note)

        payment_data = None
        if credit_note.customer.is_cash:
            if not credit_note.is_fully_paid():
                payment_data = self.get_receipt_payments()

        self.copy_invoice_items_from_invoice(invoice=credit_note, source_invoice=self.invoice)

        validate_invoice_items_totals(invoice=credit_note)
        if payment_data:
            if credit_note.receipts.exists():
                credit_note.receipts.delete()
                # self.update_invoice_receipt(invoice)

            invoice = self.create_invoice_receipt(invoice=credit_note, payment_data=payment_data)
            invoice.mark_as_paid_not_printed()

        linked_invoice = credit_note.linked_invoice

        update = Update.objects.create(
            branch=credit_note.branch,
            profile=self.profile,
            role=self.role,
            date=credit_note.invoice_date,
            period=credit_note.period
        )
        if update:
            if not linked_invoice.is_printed:
                sales_update = InvoiceUpdater(update=update, invoice_type_code=linked_invoice.invoice_type.code)
                sales_update.create_invoices_update(invoices=[linked_invoice])

            sales_update = InvoiceUpdater(update=update, invoice_type_code=credit_note.invoice_type.code)
            sales_update.create_invoices_update(invoices=[credit_note])

            credit_note.open_amount = 0
            credit_note.save()

            linked_invoice.open_amount += credit_note.total_amount
            linked_invoice.save()

            invoice_receipts = self.invoice.receipts.all()
            if len(invoice_receipts) > 0:
                invoice_amount = self.invoice.total_amount
                # If credited invoice was linked to an invoice with a receipt, then we need to restore
                # the credited amount to the receipt balance
                for invoice_receipt in invoice_receipts:
                    if invoice_amount > 0:
                        receipt: Receipt = invoice_receipt.receipt 
                        if receipt:
                            receipt.balance += invoice_amount
                            receipt.save()
                            invoice_amount -= receipt.amount
                invoice_receipts.delete()

        action = self.request.POST.get('action')
        if action == 'send_to_docuflow':
            self.create_docuflow_invoice(
                sales_invoice=credit_note,
                role=self.role,
                profile=self.profile,
                request=self.request
            )

        return credit_note

    def copy_invoice_items_from_invoice(self, invoice, source_invoice):
        logger.info("-------------create_credit_invoice_items_from_source invoice---------- ")
        for source_invoice_item in source_invoice.invoice_items.all():
            quantity_value = self.request.POST.get(f'quantity_{source_invoice_item.id}')
            if source_invoice_item.is_description:
                InvoiceItem.objects.create(
                    invoice=invoice,
                    description=source_invoice_item.description,
                    item_type=enums.InvoiceItemType.TEXT
                )
            elif quantity_value:
                invoiced_quantity = to_decimal(value=quantity_value)
                multiplier = -1 if invoice.is_credit else 1
                net_price = to_decimal(value=self.request.POST.get(f'total_price_excluding_{source_invoice_item.id}')) * multiplier
                total_amount = to_decimal(value=self.request.POST.get(f'total_price_including_{source_invoice_item.id}')) * multiplier
                vat_amount = to_decimal(value=self.request.POST.get(f'total_vat_{source_invoice_item.id}')) * multiplier
                # self.validate_available_quantity(source_invoice_item, invoiced_quantity)

                if invoiced_quantity and invoiced_quantity != 0:
                    quantity = invoiced_quantity * -1
                    price = source_invoice_item.price
                    discount = source_invoice_item.discount
                    description = self.request.POST.get(f"item_description_{source_invoice_item.id}") or source_invoice_item.description

                    invoice_item = InvoiceItem.objects.create(
                        invoice=invoice,
                        inventory=source_invoice_item.inventory,
                        description=description,
                        quantity=quantity,
                        available_quantity=abs(quantity),
                        unit=source_invoice_item.unit,
                        price=price,
                        discount=discount,
                        vat=source_invoice_item.vat,
                        net_price=net_price,
                        vat_amount=vat_amount,
                        total_amount=total_amount,
                        vat_code=source_invoice_item.vat_code,
                        item_link=source_invoice_item,
                        account=source_invoice_item.account,
                        item_type=source_invoice_item.item_type
                    )
                    if invoice_item and invoice_item.inventory:
                        InventoryLedger.objects.add_invoice_item_line(
                            invoice_item=invoice_item,
                            module=Module.SALES,
                            unit_price=price,
                            profile=invoice.profile,
                            quantity=invoiced_quantity
                        )
                    source_invoice_item.update_available_quantity(quantity=invoiced_quantity)

    def add_comment(self, credit_note):
        create_comments(
            request=self.request,
            content_type=ContentType.objects.get_for_model(credit_note),
            object_id=credit_note.id,
            profile=credit_note.profile,
            role=credit_note.role
        )


class SalesItemForm(forms.Form):

    def __init__(self, **kwargs):
        counter = kwargs.pop('row_id')
        company = kwargs.pop('company')
        super(SalesItemForm, self).__init__(**kwargs)

        vat_codes = VatCode.objects.filter(company=company, module=Module.SALES)

        self.fields[f'line_type_{counter}'] = forms.ChoiceField(choices=(('i', 'I'), ('t', 'T')), label='Type')
        self.fields[f'inventory_{counter}'] = forms.ChoiceField(widget=HeavySelect2Widget(data_url=reverse('inventory_item_search_list'), attrs={
            'data-view-url': reverse_lazy('detailed_view_inventory_item')
        }))
        self.fields[f'ordered_{counter}'] = forms.DecimalField(label='Ordered')
        self.fields[f'quantity_{counter}'] = forms.DecimalField(label='Quantity')
        self.fields[f'price_{counter}'] = forms.DecimalField(label='Price')
        self.fields[f'price_including_{counter}'] = forms.DecimalField(label='Price Including')
        self.fields[f'invoiced_{counter}'] = forms.DecimalField(label='Invoiced')
        self.fields[f'discount_{counter}'] = forms.DecimalField(label='Discount')
        self.fields[f'discount_amount_{counter}'] = forms.DecimalField(label='Discount')
        self.fields[f'net_price_{counter}'] = forms.DecimalField(label='Discount')
        self.fields[f'vat_code_{counter}'] = forms.ModelChoiceField(label='Vat Code', queryset=vat_codes)
        self.fields[f'total_price_excluding_{counter}'] = forms.DecimalField(label='Total Excl.')
        self.fields[f'total_price_including_{counter}'] = forms.DecimalField(label='Total Incl.')


class NotEnoughCreditForm(forms.Form):

    override_key = forms.CharField(label='If you accept new available credit, enter the override key', required=False,
                                   widget=forms.NumberInput(attrs={'data-validate-url': reverse_lazy('validate_override_key')
                                                                   }), max_length=4)


class DisposalInvoiceForm(SalesInvoiceForm, ReceiptedInvoiceMixin):

    def __init__(self, *args, **kwargs):
        super(DisposalInvoiceForm, self).__init__(*args, **kwargs)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.branch.company)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.branch.company)
        # self.fields['customer'].queryset = self.fields['customer'].queryset.filter(company=company)
        self.fields['customer'].widget = customer_select_data(self.branch.company)
        self.fields['invoice_type'].queryset = InvoiceType.objects.filter(
            company=self.branch.company,
            file__module=Module.SALES.value
        )
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    def clean(self):
        cleaned_data = super(DisposalInvoiceForm, self).clean()

        invoice_date = cleaned_data['invoice_date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Date is required')

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', 'Period and payment date do not match')

        if 'customer' in cleaned_data and not cleaned_data['customer']:
            self.add_error('customer', 'Customer required')
        return cleaned_data

    def save(self, commit=True):
        invoice = super().save(commit=False)
        invoice.branch = self.branch
        invoice.profile = self.profile
        invoice.role_id = self.role
        invoice.save()
        self.save_m2m()

        multiplier = self.get_multiplier(invoice.invoice_type)
        invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)

        if invoice.total_amount:
            invoice.total_amount = invoice.total_amount * multiplier
        if invoice.net_amount:
            invoice.net_amount = invoice.net_amount * multiplier
        if invoice.sub_total:
            invoice.sub_total = invoice.sub_total * multiplier
        if invoice.discount_amount:
            invoice.discount_amount = invoice.discount_amount * multiplier
        if invoice.vat_amount:
            invoice.vat_amount = invoice.vat_amount * multiplier
        if invoice.total_amount:
            invoice.open_amount = invoice.total_amount
        invoice.save()

        payment_data = {}
        if invoice.open_amount != 0:
            if invoice.customer.is_cash:
                if not invoice.is_fully_paid():
                    payment_data = self.get_receipt_payments()
            else:
                is_override = self.cleaned_data.get('is_override', None)
                available_credit = invoice.customer.calculate_available
                is_draft = self.cleaned_data.get('save_type', '') == 'draft'

                if not is_draft and not is_override:
                    if available_credit < invoice.invoice_total:
                        raise NotEnoughCreditException('Not enough credit to invoice this, please contact accounts '
                                                       'department or enter your 4 digit over ride key')

        if not invoice.customer and invoice.customer.is_default:
            self.create_customer(invoice)
            invoice.refresh_from_db()
        self.create_address(invoice)

        self.save_invoice_items(invoice)

        validate_invoice_items_totals(invoice=invoice)

        # Update the customer available credit
        CustomerLedger.objects.create_from_invoice(invoice, self.profile)

        if payment_data:
            if invoice.receipts.exists():
                invoice.receipts.delete()
                # self.update_invoice_receipt(invoice)

            invoice = self.create_invoice_receipt(invoice, payment_data)
        invoice.mark_as_paid_not_printed()

        return invoice


class BackOrderInvoiceForm(ReceiptedInvoiceMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.back_order = kwargs.pop('back_order')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(BackOrderInvoiceForm, self).__init__(*args, **kwargs)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.company)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.company)
        self.fields['invoice_type'].queryset = InvoiceType.objects.filter(
            company=self.company,
            file__module=Module.SALES
        )
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    is_override = forms.BooleanField(required=False, widget=forms.RadioSelect())
    save_type = forms.CharField(required=False)

    class Meta:
        model = Invoice
        exclude = ('invoice_id', 'created_at', 'created_by', 'status', 'customer_name', 'customer', 'delivery_to', 'postal')
        widgets = {
            'note': forms.TextInput(attrs={'class': 'form-control'}),
            'invoice_date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'due_date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'vat_code': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(BackOrderInvoiceForm, self).clean()

        invoice_date = cleaned_data['invoice_date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Date is required')

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', 'Period and payment date do not match')

        if 'customer' in cleaned_data and not cleaned_data['customer']:
            self.add_error('customer', 'Customer required')
        return cleaned_data

    def get_company_invoice_type(self):
        invoice_type = InvoiceType.objects.sales_tax_invoice(self.company).first()
        if not invoice_type:
            raise forms.ValidationError("Sales tax invoice not found")
        return invoice_type

    def save(self, commit=True):
        logger.info("-------------CREATE INVOICE FROM FORM BACK ORDER {}---------- ".format(self.back_order))
        invoice = super().save(commit=False)
        invoice.branch = self.back_order.branch
        invoice.profile = self.profile
        invoice.backorder = self.back_order
        invoice.customer = self.back_order.customer
        invoice.customer_name = self.back_order.customer_name
        invoice.delivery_to = self.back_order.delivery_to
        invoice.vat_number = self.back_order.vat_number
        invoice.postal = self.back_order.postal
        invoice.period = self.back_order.period
        invoice.discount_percentage = self.back_order.discount_percentage
        invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)
        invoice.role = self.role
        invoice.invoice_type = self.get_company_invoice_type()
        invoice.save()

        invoice.open_amount = invoice.total_amount
        invoice.save()

        payment_data = {}
        if invoice.open_amount != 0:
            if invoice.customer.is_cash:
                payment_data = self.get_receipt_payments()
            else:
                is_override = self.cleaned_data.get('is_override', None)
                available_credit = invoice.customer.calculate_available
                is_draft = self.cleaned_data.get('save_type', '') == 'draft'

                if not is_draft and not is_override:
                    if available_credit < invoice.invoice_total:
                        raise NotEnoughCreditException('Not enough credit to invoice this, please contact accounts '
                                                       'department or enter your 4 digit over ride key')

        if payment_data:
            if invoice.receipts.exists():
                invoice.receipts.delete()
                # self.update_invoice_receipt(invoice)

            invoice = self.create_invoice_receipt(invoice, payment_data)
            invoice.mark_as_paid_not_printed()

        CustomerLedger.objects.create_customer_ledger(invoice, self.profile, True)

        return invoice


class DeliveryInvoiceForm(ReceiptedInvoiceMixin, CreateInvoiceItemMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.delivery = kwargs.pop('delivery')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super().__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    is_override = forms.BooleanField(required=False, widget=forms.RadioSelect())
    save_type = forms.CharField(required=False)

    class Meta:
        model = Invoice
        fields = ('period', 'invoice_date', 'due_date', 'sub_total', 'line_discount', 'net_amount', 'discount_amount',
                  'vat_amount', 'total_amount', 'customer_discount')
        widgets = {
            'note': forms.TextInput(attrs={'class': 'form-control'}),
            'invoice_date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'sub_total': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()

        invoice_date = cleaned_data.get('invoice_date', None)
        period = cleaned_data.get('period', None)
        if not period:
            self.add_error('period', 'Period is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Date is required')

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', 'Period and invoice date do not match')
        return cleaned_data

    def get_company_invoice_type(self):
        invoice_type = InvoiceType.objects.sales_tax_invoice(self.company).first()
        if not invoice_type:
            raise forms.ValidationError(f"Sales tax invoice type not found")
        return invoice_type

    def save(self, commit=True):
        logger.info(f"-------------CREATE INVOICE FROM FORM DELIVERY {self.delivery}---------- ")
        invoice = super().save(commit=False)
        invoice.branch = self.delivery.branch
        invoice.profile = self.profile
        invoice.delivery = self.delivery
        invoice.customer = self.delivery.customer
        if self.delivery.purchase_order:
            invoice.po_number = self.delivery.purchase_order
        if self.delivery.customer_name:
            invoice.customer_name = self.delivery.customer_name
        invoice.vat_number = self.delivery.vat_number
        invoice.discount_percentage = self.delivery.discount_percentage
        invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)
        invoice.role = self.role
        invoice.invoice_type = self.get_company_invoice_type()
        if self.delivery.delivery_to:
            invoice.delivery_to = self.delivery.delivery_to
        if self.delivery.postal:
            invoice.postal = self.delivery.postal
        invoice.open_amount = self.cleaned_data['total_amount']
        invoice.save()

        self.delivery.mark_as_invoiced()  # Mark before creating inventory items, so the inventory recalculates
        # with current status

        back_order_items = self.create_invoice_items_from_deliveries(invoice)

        self.add_extra_lines(invoice=invoice)

        validate_invoice_items_totals(invoice=invoice)

        if len(back_order_items) > 0:
            sales_invoice = self.delivery
            if self.delivery.picking_slip:
                sales_invoice = self.delivery.picking_slip
            create_back_order = CreateBackOrderFromSalesInvoice(sales_invoice, back_order_items)
            create_back_order.execute()
        # TODO - Check if we need to create a new back order record for back order items

        payment_data = {}
        if invoice.open_amount != 0:
            if invoice.customer.is_cash:
                payment_data = self.get_receipt_payments()
            else:
                is_override = self.cleaned_data.get('is_override', None)
                available_credit = invoice.customer.calculate_available
                is_draft = self.cleaned_data.get('save_type', '') == 'draft'

                if not is_draft and not is_override:
                    if available_credit < invoice.invoice_total:
                        raise NotEnoughCreditException('Not enough credit to invoice this, please contact accounts '
                                                       'department or enter your 4 digit over ride key')

        if payment_data:
            if invoice.receipts.exists():
                invoice.receipts.delete()
                # self.update_invoice_receipt(invoice)

            invoice = self.create_invoice_receipt(invoice, payment_data)
            invoice.mark_as_paid_not_printed()

            invoice.open_amount = 0
            invoice.save()

        CustomerLedger.create_from_invoice(invoice=invoice, profile=self.profile)

        self.add_delivery_tracking(invoice=invoice, delivery=self.delivery, profile=self.profile, role=self.role)

        return invoice

    def add_extra_lines(self, invoice: Invoice):
        for field, v in self.request.POST.items():
            if field.startswith('type', 0, 4):
                row_id = field[5:]
                if self.request.POST.get(f'inventory_{row_id}'):
                    self.handle_inventory_line(invoice=invoice, row_id=row_id)
                elif self.request.POST.get(f'account_{row_id}'):
                    self.handle_account_line(invoice=invoice, row_id=row_id)
                elif self.request.POST.get(f'item_description_{row_id}'):
                    self.handle_description_line(invoice=invoice, row_id=row_id)

    def add_delivery_tracking(self, invoice, delivery, profile, role):
        logger.info("-------------EXECUTE TRACKING---------- ")
        logger.info(f"Handle  --> {invoice} ")

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(invoice),
            object_id=invoice.id,
            comment='Invoice created',
            profile=profile,
            role=role,
            data={
                'tracking_type': 'delivery',
                'reference': str(delivery),
                'link_url': reverse('delivery_detail', args=(delivery.pk,))
            }
        )

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(delivery),
            object_id=delivery.id,
            comment='Invoice created',
            profile=profile,
            role=role,
            data={
                'tracking_type': 'invoice',
                'reference': str(invoice),
                'link_url': reverse('view_sales_invoice', args=(invoice.pk,))
            }
        )

        if delivery.estimate:
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(delivery.estimate),
                object_id=delivery.estimate_id,
                comment='Invoice created',
                profile=profile,
                data={
                    'tracking_type': 'self',
                    'reference': str(invoice),
                    'link_url': reverse('invoice_detail', args=(invoice.id,))
                }
            )
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(invoice),
                object_id=invoice.id,
                comment='Invoice created',
                profile=profile,
                data={
                    'tracking_type': 'estimate',
                    'reference': str(delivery.estimate),
                    'link_url': reverse('estimate_detail', args=(delivery.estimate_id,))
                }
            )

    def create_invoice_items_from_deliveries(self, invoice):
        back_order_items = []
        for sales_item in self.delivery.delivery_items.all():
            is_back_ordered = bool(self.request.POST.get(f'back_order_{sales_item.id}', False))
            quantity = self.request.POST.get(f'quantity_{sales_item.id}', 0)
            quantity_ordered = Decimal(self.request.POST.get(f'quantity_ordered_{sales_item.id}', 0))
            vat_amount = self.request.POST.get(f'vat_amount_{sales_item.id}', 0)
            discount_amount = self.request.POST.get(f'discount_amount_{sales_item.id}', 0)
            total_amount = self.request.POST.get(f'total_price_including_{sales_item.id}', 0)
            if sales_item.account:
                data = {
                    'account': sales_item.account,
                    'quantity': sales_item.quantity,
                    'price': sales_item.price,
                    'discount': sales_item.discount,
                    'description': sales_item.description,
                    'vat_code': sales_item.vat_code,
                    'vat_amount': vat_amount,
                    'discount_amount': discount_amount,
                    'total_amount': total_amount,
                    'invoice': invoice
                }
                account_form = InvoiceAccountItemForm(data=data, branch=invoice.branch)
                if account_form.is_valid():
                    account_form.save()
                else:
                    logger.info(account_form.errors)
                    forms.ValidationError('Account line could not be saved')
            elif sales_item.inventory:
                data = {
                    'inventory': sales_item.inventory,
                    'quantity': quantity,
                    'price': sales_item.price,
                    'discount': sales_item.discount,
                    'description': sales_item.description,
                    'vat_code': sales_item.vat_code,
                    'unit': sales_item.unit,
                    'vat_amount': vat_amount,
                    'discount_amount': discount_amount,
                    'total_amount': total_amount,
                    'invoice': invoice
                }
                inventory_form = InvoiceInventoryForm(data=data, branch=invoice.branch)
                if inventory_form.is_valid():
                    inventory_form.save()
                else:
                    logger.info(inventory_form.errors)
                    forms.ValidationError('Inventory line could not be saved')

                if is_back_ordered:
                    back_order_quantity = quantity_ordered - Decimal(quantity)
                    if back_order_quantity < quantity_ordered:
                        back_order_items.append({'item': sales_item, 'quantity': back_order_quantity})
            elif sales_item.is_description:
                data = {
                    'inventory': sales_item.inventory,
                    'description': sales_item.description,
                    'invoice': invoice
                }
                invoice_text_item_form = InvoiceTextItemForm(data=data, branch=invoice.branch)
                if invoice_text_item_form.is_valid():
                    invoice_text_item_form.save()
                else:
                    logger.info(invoice_text_item_form.errors)
                    forms.ValidationError('Text line could not be saved')
        return back_order_items

    def create_invoice_items_from_back_order(self, invoice, back_order_items):
        logger.info("-------------create_invoice_items_from_estimates---------- ")
        back_order_estimate_items = []

        for back_order_item in back_order_items:
            is_back_ordered = bool(self.request.POST.get(f'back_order_{back_order_item.id}', False))
            quantity = Decimal(self.request.POST.get(f'quantity_{back_order_item.id}', 0))
            quantity_ordered = Decimal(self.request.POST.get(f'quantity_ordered_{back_order_item.id}', 0))
            net_price = Decimal(self.request.POST.get(f'total_price_excluding_{back_order_item.id}', 0))

            discount = Decimal(back_order_item.discount)
            amount = Decimal(back_order_item.price)
            invoice_item = InvoiceItem.objects.create(
                invoice=invoice,
                inventory=back_order_item.inventory,
                description=back_order_item.description,
                quantity=quantity,
                available_quantity=quantity,
                unit=back_order_item.unit,
                price=amount,
                discount=discount,
                vat=back_order_item.vat,
                net_price=net_price,
                vat_code=back_order_item.vat_code,
                vat_amount=back_order_item.calculate_vat_amount()
            )
            if invoice_item:
                if is_back_ordered:
                    back_order_quantity = quantity_ordered - invoice_item.quantity
                    back_order_estimate_items.append({'back_order_item': back_order_item, 'quantity': back_order_quantity})

                InventoryLedger.objects.add_invoice_item_line(
                    invoice_item=invoice_item,
                    module=Module.SALES,
                    profile=invoice.profile,
                    unit_price=0,
                    quantity=quantity
                )


class EstimateInvoiceForm(CreateInvoiceItemMixin, ReceiptedInvoiceMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.estimate = kwargs.pop('estimate')
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super().__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    is_override = forms.BooleanField(required=False, widget=forms.RadioSelect())
    save_type = forms.CharField(required=False)

    class Meta:
        model = Invoice
        fields = ('period', 'invoice_date', 'due_date', 'sub_total', 'line_discount', 'net_amount', 'discount_amount',
                  'vat_amount', 'total_amount', 'customer_discount')
        widgets = {
            'note': forms.TextInput(attrs={'class': 'form-control'}),
            'invoice_date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'sub_total': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()

        invoice_date = cleaned_data.get('invoice_date', None)
        period = cleaned_data.get('period', None)
        if not period:
            self.add_error('period', 'Period is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Date is required')

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', 'Period and invoice date do not match')
        return cleaned_data

    def get_company_invoice_type(self):
        invoice_type = InvoiceType.objects.sales_tax_invoice(self.company).first()
        if not invoice_type:
            raise forms.ValidationError(f"Sales tax invoice type not found")
        return invoice_type

    def save(self, commit=True):
        invoice = super().save(commit=False)
        logger.info(f"-------------CREATE INVOICE FROM FORM estimate {self.estimate}---------- ")
        invoice.profile = self.profile
        invoice.branch = self.estimate.branch
        invoice.estimate = self.estimate
        invoice.discount_percentage = self.estimate.discount_percentage
        invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)
        if self.estimate.delivery_to:
            invoice.delivery_to = self.estimate.delivery_to
        if self.estimate.postal:
            invoice.postal = self.estimate.postal
        if self.estimate.customer:
            invoice.customer = self.estimate.customer
        invoice.customer_name = self.estimate.customer_name
        invoice.vat_number = self.estimate.vat_number
        invoice.role = self.role
        invoice.invoice_type = self.get_company_invoice_type()
        invoice.open_amount = invoice.total_amount
        invoice.save()

        payment_data = {}
        if invoice.open_amount != 0:
            if invoice.customer.is_cash:
                payment_data = self.get_receipt_payments()
            else:
                is_override = self.cleaned_data.get('is_override', None)
                available_credit = invoice.customer.calculate_available
                is_draft = self.cleaned_data.get('save_type', '') == 'draft'

                if not is_draft and not is_override:
                    logger.info(f"Available credit --> {available_credit} vs total invoice amount {invoice.invoice_total} == {(available_credit < invoice.invoice_total)}")
                    if available_credit < invoice.invoice_total:
                        raise NotEnoughCreditException('Not enough credit to invoice this, please contact accounts '
                                                       'department or enter your 4 digit over ride key')

        self.create_invoice_items_from_estimates(invoice)
        self.save_invoice_items(invoice, True)

        validate_invoice_items_totals(invoice=invoice)

        if payment_data:
            if invoice.receipts.exists():
                invoice.receipts.delete()

            invoice = self.create_invoice_receipt(invoice, payment_data)
            invoice.mark_as_paid_not_printed()

            invoice.open_amount = 0
            invoice.save()

        if invoice.customer:
            invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)
        invoice.save()

        CustomerLedger.objects.create_customer_ledger(invoice, self.profile, True)

        self.estimate.mark_as_completed()

        self.add_tracking(invoice)

        return invoice

    def create_invoice_items_from_estimates(self, invoice):
        logger.info("-------------create_invoice_items_from_estimates---------- ")
        back_order_estimate_items = []
        non_inventory_items = []
        for estimate_item in self.estimate.estimate_items.all():
            is_back_ordered = bool(self.request.POST.get(f'back_order_{estimate_item.id}', False))
            quantity = to_decimal(self.request.POST.get(f'quantity_{estimate_item.id}', 0))
            quantity_ordered = to_decimal(self.request.POST.get(f'quantity_ordered_{estimate_item.id}', 0))
            discount_amount = self.request.POST.get(f'discount_amount_{estimate_item.id}', 0)
            vat_amount = self.request.POST.get(f'vat_amount_{estimate_item.id}', 0)
            total_amount = float(self.request.POST.get('total_price_including_{}'.format(estimate_item.id), 0))

            if is_back_ordered:
                back_order_quantity = quantity_ordered - quantity
                back_order_estimate_items.append({'item': estimate_item, 'quantity': back_order_quantity})
            if estimate_item.is_inventory:
                data = {
                    'inventory': estimate_item.inventory,
                    'quantity': quantity,
                    'price': estimate_item.price,
                    'discount': estimate_item.discount,
                    'description': estimate_item.description,
                    'vat_code': estimate_item.vat_code,
                    'unit': estimate_item.unit,
                    'vat_amount': vat_amount,
                    'customer_discount': discount_amount,
                    'total_amount': total_amount,
                    'invoice': invoice
                }
                inventory_form = InvoiceInventoryForm(data=data, branch=invoice.branch)
                if inventory_form.is_valid():
                    inventory_form.save()
                else:
                    logger.info(inventory_form.errors)
                    forms.ValidationError('Inventory line could not be saved')
            elif estimate_item.is_account:
                non_inventory_items.append({'item': estimate_item, 'quantity': estimate_item.quantity})

                data = {
                    'account': estimate_item.account,
                    'quantity': quantity,
                    'price': estimate_item.price,
                    'discount': estimate_item.discount,
                    'description': estimate_item.description,
                    'vat_code': estimate_item.vat_code,
                    'vat_amount': vat_amount,
                    'discount_amount': discount_amount,
                    'total_amount': total_amount,
                    'invoice': invoice
                }
                account_form = InvoiceAccountItemForm(data=data, branch=invoice.branch)
                if account_form.is_valid():
                    account_form.save()
                else:
                    logger.info(account_form.errors)
                    forms.ValidationError('Account line could not be saved')
            elif estimate_item.is_description:
                data = {
                    'description': estimate_item.description,
                    'invoice': invoice
                }
                text_line_form = InvoiceTextItemForm(data=data, branch=invoice.branch)
                if text_line_form.is_valid():
                    text_line_form.save()
                else:
                    logger.info(text_line_form.errors)
                    forms.ValidationError('Text line could not be saved')

        if len(back_order_estimate_items) > 0:
            back_order_estimate_items = back_order_estimate_items + non_inventory_items
            create_back_order = CreateBackOrderFromSalesInvoice(self.estimate, back_order_estimate_items)
            back_order = create_back_order.execute()

            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(back_order),
                object_id=back_order.id,
                comment='Back order created from invoice',
                profile=self.profile,
                data={
                    'tracking_type': 'invoice',
                    'reference': str(invoice),
                    'link_url': reverse('view_sales_invoice', args=(invoice.id,))
                }
            )
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(invoice),
                object_id=invoice.id,
                comment='Back order created ',
                profile=self.profile,
                data={
                    'tracking_type': 'back_order',
                    'reference': str(back_order),
                    'link_url': reverse('backorder_detail', args=(back_order.id,))
                }
            )

    def add_tracking(self, invoice):
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.estimate),
            object_id=self.estimate.pk,
            comment=f'Estimate {str(self.estimate.status)}',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'self',
                'reference': str(invoice),
                'link_url': reverse('view_sales_invoice', args=(invoice.pk,))
            }
        )

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(invoice),
            object_id=invoice.pk,
            comment=f'Invoice created from estimate',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'estimate',
                'reference': str(self.estimate),
                'link_url': reverse('estimate_detail', args=(self.estimate.pk,))
            }
        )


class ProformaInvoiceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.proforma_invoice = kwargs.pop('proforma_invoice')
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super().__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    is_override = forms.BooleanField(required=False, widget=forms.RadioSelect())
    save_type = forms.CharField(required=False)

    class Meta:
        model = Invoice
        fields = ('period', 'invoice_date', 'due_date', 'sub_total', 'line_discount', 'net_amount', 'discount_amount',
                  'vat_amount', 'total_amount', 'customer_discount')
        widgets = {
            'note': forms.TextInput(attrs={'class': 'form-control'}),
            'invoice_date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'sub_total': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()

        invoice_date = cleaned_data.get('invoice_date', None)
        period = cleaned_data.get('period', None)
        if not period:
            self.add_error('period', 'Period is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Date is required')

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', 'Period and invoice date do not match')
        return cleaned_data

    def get_company_invoice_type(self):
        invoice_type = InvoiceType.objects.sales_tax_invoice(self.company).first()
        if not invoice_type:
            raise forms.ValidationError(f"Sales tax invoice type not found")
        return invoice_type

    def save(self, commit=True):
        invoice = super().save(commit=False)
        logger.info(f"-------------CREATE INVOICE FROM FORM estimate {self.estimate}---------- ")
        invoice.profile = self.profile
        invoice.branch = self.estimate.branch
        invoice.estimate = self.estimate
        invoice.period = self.estimate.period
        invoice.discount_percentage = self.estimate.discount_percentage
        invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)
        if self.estimate.delivery_to:
            invoice.delivery_to = self.estimate.delivery_to
        if self.estimate.postal:
            invoice.postal = self.estimate.postal
        if self.estimate.customer:
            invoice.customer = self.estimate.customer
        invoice.customer_name = self.estimate.customer_name
        invoice.vat_number = self.estimate.vat_number
        invoice.role = self.role
        invoice.invoice_type = self.get_company_invoice_type()
        invoice.open_amount = invoice.total_amount
        invoice.save()

        self.create_invoice_items_from_estimates(invoice)
        # self.save_invoice_items(invoice) TODO Ensure when we add new lines, they are also saved
        validate_invoice_items_totals(invoice=invoice)
        return invoice

    def create_invoice_items_from_estimates(self, invoice):
        logger.info("-------------create_invoice_items_from_estimates---------- ")
        back_order_estimate_items = []
        non_inventory_items = []
        for estimate_item in self.estimate.estimate_items.all():
            is_back_ordered = bool(self.request.POST.get(f'back_order_{estimate_item.id}', False))
            quantity = Decimal(self.request.POST.get(f'quantity_{estimate_item.id}', 0))
            quantity_ordered = Decimal(self.request.POST.get(f'quantity_ordered_{estimate_item.id}', 0))
            discount_amount = Decimal(self.request.POST.get(f'discount_amount_{estimate_item.id}', 0))
            vat_amount = Decimal(self.request.POST.get(f'vat_amount_{estimate_item.id}', 0))
            total_amount = float(self.request.POST.get('total_price_including_{}'.format(estimate_item.id), 0))

            if is_back_ordered:
                back_order_quantity = quantity_ordered - quantity
                back_order_estimate_items.append({'item': estimate_item, 'quantity': back_order_quantity})
            if estimate_item.is_inventory:
                data = {
                    'inventory': estimate_item.inventory,
                    'quantity': estimate_item.quantity,
                    'price': estimate_item.price,
                    'discount': estimate_item.discount,
                    'description': estimate_item.description,
                    'vat_code': estimate_item.vat_code,
                    'unit': estimate_item.unit,
                    'vat_amount': vat_amount,
                    'discount_amount': discount_amount,
                    'total_amount': total_amount,
                    'invoice': invoice
                }
                inventory_form = InvoiceInventoryForm(data=data, branch=invoice.branch)
                if inventory_form.is_valid():
                    inventory_form.save()
                else:
                    logger.info(inventory_form.errors)
                    forms.ValidationError('Inventory line could not be saved')

            if estimate_item.is_account:
                non_inventory_items.append({'item': estimate_item, 'quantity': estimate_item.quantity})

                data = {
                    'account': estimate_item.account,
                    'quantity': estimate_item.quantity,
                    'price': estimate_item.price,
                    'discount': estimate_item.discount,
                    'description': estimate_item.description,
                    'vat_code': estimate_item.vat_code,
                    'vat_amount': vat_amount,
                    'discount_amount': discount_amount,
                    'total_amount': total_amount,
                    'invoice': invoice
                }
                account_form = InvoiceAccountItemForm(data=data, branch=invoice.branch)
                if account_form.is_valid():
                    account_form.save()
                else:
                    logger.info(account_form.errors)
                    forms.ValidationError('Account line could not be saved')

        if len(back_order_estimate_items) > 0:
            back_order_estimate_items = back_order_estimate_items + non_inventory_items
            create_back_order = CreateBackOrderFromSalesInvoice(self.estimate, back_order_estimate_items)
            create_back_order.execute()

    def save_invoice_items(self, invoice, is_new=True):
        posted_invoice_items = self.request.POST.items()

        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        counter = 0
        for field, value in posted_invoice_items:
            if field.startswith('inventory', 0, 10):
                row_id = field[11:]
                if not self.is_new_line(row_id, is_new):
                    self.add_inventory_invoice_item(invoice, row_id, content_type)
                counter += 1
            elif field.startswith('account', 0, 7):
                row_id = field[8:]
                if not self.is_new_line(row_id, is_new):
                    self.add_account_invoice_item(invoice, row_id, content_type)
                counter += 1
            elif field.startswith('item_description', 0, 16):
                row_id = field[17:]
                if not self.is_new_line(row_id, is_new):
                    self.add_general_invoice_item(invoice, row_id, content_type)
                counter += 1
        if is_new and counter == 0:
            raise ValueError('No invoice lines were selected, please ensure you have at least 1 line in your invoice')

        if invoice.customer:
            invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)
        invoice.save()


class InvoiceInventoryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super().__init__(*args, **kwargs)
        self.fields['inventory'].queryset = self.fields['inventory'].queryset.filter(branch=self.branch)
        self.fields['invoice'].queryset = self.fields['invoice'].queryset.filter(branch=self.branch)

    class Meta:
        model = InvoiceItem
        fields = ('inventory', 'quantity', 'price', 'discount', 'net_price', 'unit', 'vat', 'vat_code', 'vat_amount',
                  'invoice', 'customer_discount', 'total_amount')

    def save(self, commit=True):
        quantity = self.get_unit_quantity(self.cleaned_data['quantity'])
        invoice_item = super().save(commit=False)
        invoice_item.item_type = enums.InvoiceItemType.INVENTORY
        invoice_item.quantity = quantity
        invoice_item.available_quantity = quantity
        invoice_item.vat = self.get_vat_percentage()
        invoice_item.net_price = self.calculate_net_price()
        invoice_item.save()

        if not invoice_item.invoice.is_recurring:
            self.add_inventory_ledger(invoice_item)

        invoice_item.inventory.recalculate_balances(year=invoice_item.invoice.period.period_year)
        return invoice_item

    def get_unit_quantity(self, quantity):
        unit_quantity = quantity
        multiplier = -1 if self.cleaned_data['invoice'].is_credit else 1
        multiplier = 1 if self.instance.pk else multiplier
        return unit_quantity * multiplier

    def calculate_sub_total(self):
        discount_amount = self.calculate_discount_amount()
        total = self.price_including()
        if discount_amount:
            total += discount_amount
        return round(total, 2)

    def price_including(self):
        return self.price + self.calculate_vat_amount()

    def calculate_vat_amount(self):
        vat_percentage = self.get_vat_percentage()
        vat_amount = 0
        if vat_percentage:
            vat_amount = (float(vat_percentage) / 100 * self.cleaned_data['price']) * self.cleaned_data['quantity']
        return Decimal(vat_amount)

    def calculate_net_price(self):
        quantity = self.get_unit_quantity(self.cleaned_data['quantity'])
        return quantity * self.cleaned_data['price']

    def get_vat_percentage(self):
        if self.cleaned_data['vat_code']:
            return self.cleaned_data['vat_code'].percentage
        return 0

    def add_inventory_ledger(self, invoice_item):
        InventoryLedger.objects.add_invoice_item_line(
            invoice_item, Module.SALES.value, invoice_item.price, self.cleaned_data['invoice'].profile
        )


class InvoiceAccountItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super(InvoiceAccountItemForm, self).__init__(*args, **kwargs)
        self.fields['invoice'].queryset = self.fields['invoice'].queryset.filter(branch=self.branch)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=self.branch.company)
        self.fields['vat'].queryset = VatCode.objects.output().filter(company=self.branch.company)
        self.fields['description'].required = False

    class Meta:
        model = InvoiceItem
        fields = ('account', 'description', 'quantity', 'unit', 'price', 'net_price', 'vat_code', 'vat', 'discount',
                  'vat_amount', 'invoice', 'total_amount')

    def save(self, commit=True):
        invoice = self.cleaned_data['invoice']
        account_line = super().save(commit=False)
        multiplier = self.get_multiplier(invoice=invoice)
        account_line.price = account_line.price
        account_line.item_type = enums.InvoiceItemType.GENERAL
        if self.cleaned_data['vat_code']:
            account_line.vat = self.cleaned_data['vat_code'].percentage
        account_line.available_quantity = account_line.quantity
        account_line.quantity = account_line.quantity * multiplier
        account_line.net_price = self.calculate_net_price() * multiplier
        account_line.vat_amount = account_line.vat_amount * multiplier
        account_line.save()
        return account_line

    def get_multiplier(self, invoice: Invoice):
        multiplier = -1 if invoice.is_credit else 1
        if self.instance.pk:
            multiplier = 1
        return multiplier

    def calculate_net_price(self):
        return self.cleaned_data['quantity'] * self.cleaned_data['price']


class InvoiceTextItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        super(InvoiceTextItemForm, self).__init__(*args, **kwargs)
        self.fields['invoice'].queryset = self.fields['invoice'].queryset.filter(branch=self.branch)

    class Meta:
        model = InvoiceItem
        fields = ('description', 'invoice')

    def save(self, commit=False):
        description_line = super().save(commit=False)
        description_line.item_type = enums.InvoiceItemType.TEXT
        description_line.save()
        return description_line


class SendRecurringInvoiceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.year = kwargs.pop('year')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super().__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    class Meta:
        model = Invoice
        fields = ('invoice_date', 'period')

    def clean(self):
        cleaned_data = super().clean()

        invoice_date = cleaned_data.get('invoice_date', None)
        period = cleaned_data.get('period', None)
        if not period:
            self.add_error('period', 'Period is required')

        if not invoice_date:
            self.add_error('invoice_date', 'Date is required')

        if period and invoice_date:
            if not period.is_valid(invoice_date):
                self.add_error('invoice_date', 'Period and invoice date do not match')

        return cleaned_data

    def save(self, commit=True):
        recurring_invoices = Invoice.objects.select_related(
            'customer', 'branch'
        ).prefetch_related(
            'invoice_items', 'invoice_items__inventory', 'invoice_items__account', 'invoice_items__vat_code'
        ).filter(pk__in=self.request.session['recurring_invoices'])

        for recurring_invoice in recurring_invoices:
            due_date = recurring_invoice.customer.calculate_due_date(self.cleaned_data['invoice_date'])
            invoice = Invoice.objects.create(
                branch=recurring_invoice.branch,
                customer=recurring_invoice.customer,
                customer_name=recurring_invoice.customer_name,
                period=self.cleaned_data['period'],
                invoice_date=self.cleaned_data['invoice_date'],
                phone_number=recurring_invoice.phone_number,
                sub_total=recurring_invoice.sub_total,
                discount_amount=recurring_invoice.discount_amount,
                line_discount=recurring_invoice.line_discount,
                net_amount=recurring_invoice.net_amount,
                total_amount=recurring_invoice.total_amount,
                vat_amount=recurring_invoice.vat_amount,
                is_pick_up=recurring_invoice.is_pick_up,
                delivery_to=recurring_invoice.delivery_to,
                postal=recurring_invoice.postal,
                customer_discount=recurring_invoice.customer_discount,
                supplier_number=recurring_invoice.supplier_number,
                po_number=recurring_invoice.po_number,
                vat_number=recurring_invoice.vat_number,
                delivery_number=recurring_invoice.delivery_number,
                accounting_date=recurring_invoice.accounting_date,
                payment_date=None,
                due_date=due_date,
                open_amount=recurring_invoice.total_amount,
                invoice_type=recurring_invoice.invoice_type,
                profile=self.profile,
                role=self.role,
                recurring=recurring_invoice
            )

            CustomerLedger.objects.create_from_invoice(invoice, self.profile)

            for recurring_invoice_item in recurring_invoice.invoice_items.all():
                invoice_item = InvoiceItem.objects.create(
                    invoice=invoice,
                    description=recurring_invoice_item.description,
                    quantity=recurring_invoice_item.quantity,
                    price=recurring_invoice_item.price,
                    discount=recurring_invoice_item.discount,
                    net_price=recurring_invoice_item.net_price,
                    inventory=recurring_invoice_item.inventory,
                    unit=recurring_invoice_item.unit,
                    customer_discount=recurring_invoice_item.customer_discount,
                    total_amount=recurring_invoice_item.total_amount,
                    vat=recurring_invoice_item.vat,
                    vat_code=recurring_invoice_item.vat_code,
                    account=recurring_invoice_item.account,
                    item_type=recurring_invoice_item.item_type,
                    vat_amount=recurring_invoice_item.vat_amount
                )
                if invoice_item.inventory:
                    self.add_inventory_ledger(invoice_item)

    def add_inventory_ledger(self, invoice_item):
        InventoryLedger.objects.add_invoice_item_line(
            invoice_item, Module.SALES.value, invoice_item.price, self.profile
        )
