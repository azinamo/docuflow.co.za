import pprint

from django.core.management.base import BaseCommand
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction

from docuflow.apps.company.models import Company
from docuflow.apps.sales.models import InvoiceItem, EstimateItem, BackOrderItem, PickingSlipItem, DeliveryItem, ProformaInvoiceItem
from docuflow.apps.inventory.models import InventoryReceivedReturned, InventoryReceived, InventoryReturned

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Link invoice items correctly"

    def handle(self, *args, **kwargs):
        with transaction.atomic():
            for inventory_returned in InventoryReturned.objects.filter(invoice_item_id__isnull=False):
                all_returned_received = InventoryReceivedReturned.objects.filter(
                    inventory_returned=inventory_returned
                )
                for returned_received in all_returned_received:
                    returned_received.returned_object_id = inventory_returned.invoice_item_id
                    returned_received.returned_type = ContentType.objects.get_for_model(inventory_returned.invoice_item)
                    returned_received.save()

            for inventory_received in InventoryReceived.objects.filter(invoice_item_id__isnull=False):
                all_received_returned = InventoryReceivedReturned.objects.filter(
                    inventory_received=inventory_received
                )
                for received_returned in all_received_returned:
                    received_returned.received_object_id = inventory_received.invoice_item_id
                    received_returned.received_type = ContentType.objects.get_for_model(inventory_received.invoice_item)
                    received_returned.save()
