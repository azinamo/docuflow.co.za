import pprint
from django.core.management.base import BaseCommand
from django.db import transaction

from docuflow.apps.company.models import Company
from docuflow.apps.sales.models import InvoiceItem, EstimateItem, BackOrderItem, PickingSlipItem, DeliveryItem, ProformaInvoiceItem

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = "Restart the calculation of vat amounts"

    def handle(self, *args, **kwargs):
        with transaction.atomic():
            for company in Company.objects.all():
                for invoice_item in InvoiceItem.objects.filter(invoice__branch__company=company):
                    invoice_item.vat_amount = invoice_item.total_vat_amount
                    invoice_item.save()

                for estimate_item in EstimateItem.objects.filter(estimate__branch__company=company):
                    estimate_item.vat_amount = estimate_item.total_vat_amount
                    estimate_item.save()

                for back_order_item in BackOrderItem.objects.filter(back_order__branch__company=company):
                    back_order_item.vat_amount = back_order_item.total_vat_amount
                    back_order_item.save()

                for picking_slip_item in PickingSlipItem.objects.filter(picking_slip__branch__company=company):
                    picking_slip_item.vat_amount = picking_slip_item.total_vat_amount
                    picking_slip_item.save()

                for delivery_item in DeliveryItem.objects.filter(delivery__branch__company=company):
                    delivery_item.vat_amount = delivery_item.total_vat_amount
                    delivery_item.save()

                for proforma_item in ProformaInvoiceItem.objects.filter(invoice__branch__company=company):
                    proforma_item.vat_amount = proforma_item.total_vat_amount
                    proforma_item.save()

