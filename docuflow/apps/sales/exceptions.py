from django.core.exceptions import ValidationError


class SalesException(Exception):
    pass


class QuantityNotAvailableException(SalesException):
    pass


class InsufficientAmount(SalesException):
    pass


class NotEnoughCreditException(SalesException):
    pass


class PaymentNotDoneException(SalesException):
    pass