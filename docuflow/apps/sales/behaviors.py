import logging

from django.db import models
from django.db.models import JSONField

from safedelete.models import SafeDeleteModel, SOFT_DELETE
from django_countries import countries

from docuflow.apps.common.enums import SaProvince

logger = logging.getLogger(__name__)


class Deliverable(models.Model):

    delivery_method = models.ForeignKey('company.DeliveryMethod', null=True, blank=True, related_name='+', on_delete=models.DO_NOTHING)
    delivery_term = models.ForeignKey('customer.DeliveryTerm', null=True, blank=True, related_name='+', on_delete=models.DO_NOTHING)
    date = models.DateField(null=True, blank=True, verbose_name='Delivery Date')
    delivery_time = models.TimeField(null=True, blank=True, verbose_name='Delivery Time Requested')
    purchase_order = models.CharField(max_length=255, null=True, blank=True, verbose_name='Purchase Order Reference')

    class Meta:
        abstract = True


class Addressable(models.Model):
    postal = JSONField(null=True)
    delivery_to = JSONField(null=True)
    is_pick_up = models.BooleanField(default=False, verbose_name='Picking Slip')

    class Meta:
        abstract = True

    def get_country_by_code(self, code):
        if code:
            countries_list = dict(countries)
            return countries_list.get(code, '')
        return ''

    def get_province(self, province):
        if province:
            return SaProvince(province)
        return ''

    @property
    def delivery_province(self):
        province = ''
        if self.delivery_to:
            province = self.delivery_to.get('province', None)
            if isinstance(province, int):
                return self.get_province(province)
            elif province is None:
                province = self.delivery_to.get('province_name', None)
        return province

    @property
    def postal_province(self):
        province = ''
        if self.postal:
            province = self.postal.get('province', None)
            if isinstance(province, int):
                province = self.get_province(province)
            elif province is None:
                province = self.postal.get('province_name', None)
        return province

    @property
    def delivery_country(self):
        country = ''
        if self.delivery_to:
            country_code = self.delivery_to.get('country', None)
            if country_code:
                country = self.get_country_by_code(country_code)
                if not country:
                    country = country_code
            elif country_code is None:
                country = self.delivery_to.get('country_name', None)
        return country

    @property
    def postal_country(self):
        country = ''
        if self.postal:
            country_code = self.postal.get('country', None)
            print(f"postal.country code is {country_code}")
            if country_code:
                country = self.get_country_by_code(country_code)
                if not country:
                    country = country_code
            elif country_code is None:
                country = self.postal.get('country_name', None)
                print(f"postal->country is {country}")
        return country

    @property
    def has_delivery_address(self):
        if self.delivery_to:
            is_pick_up = self.delivery_to.get('is_pick_up', False)
            if is_pick_up:
                return False
            delivery_address = self.delivery_to.get('street', None) or self.delivery_to.get('address_line_1', None)
            if delivery_address:
                return True
        return False


class Salable(SafeDeleteModel, models.Model):
    _safedelete_policy = SOFT_DELETE
    branch = models.ForeignKey('company.Branch', related_name="%(app_label)s_%(class)s_related", related_query_name="%(app_label)s_%(class)ss",
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )
    notes = models.TextField(null=True, blank=True, verbose_name='Notes')
    sub_total = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    net_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8, verbose_name='Net')
    vat = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8, verbose_name='Vat(%)')
    vat_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8, verbose_name='Vat')
    line_discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8, verbose_name='Line discount')
    discount_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=4, verbose_name='Total Discount')
    total_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8, verbose_name='Total')
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    profile = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING,
                                verbose_name='Created By')
    role = models.ForeignKey('accounts.Role', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                             related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING,
                             verbose_name='Created By Role')
    pricing = models.ForeignKey('customer.Pricing', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING,
                                verbose_name='Pricing')
    salesman = models.ForeignKey('Salesman', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                 related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING,
                                 verbose_name='Salesman')

    class Meta:
        abstract = True
        get_latest_by = 'created_at'

    @property
    def net_total(self):
        total = 0
        if self.sub_total:
            total += float(self.sub_total)
        if self.discount_amount:
            total += float(self.discount_amount)
        return total

    def get_vat(self):
        vat = 0
        if self.branch.company:
            default_vat = self.branch.company.company_vat_codes.filter(is_default=True).first()
            if default_vat:
                vat = default_vat.percentage
        return vat

    def calculate_vat_amount(self):
        vat_amount = 0
        if self.vat:
            vat_amount = (float(self.vat) / 100) * self.net_amount
        return vat_amount


class SalableItem(models.Model):
    PENDING = 0
    PICKED = 1
    INVOICED = 2

    STATUSES = (
        (PENDING, 'Pending'),
        (PICKED, 'Picked'),
        (INVOICED, 'Invoiced')
    )

    inventory = models.ForeignKey('inventory.BranchInventory',
                                  related_name="%(app_label)s_%(class)s_related", related_query_name="%(app_label)s_%(class)ss",
                                  null=True, blank=True, on_delete=models.CASCADE
                                  )

    description = models.TextField()
    quantity = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    unit = models.ForeignKey('company.Unit', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                             related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING)
    price = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    customer_discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    net_price = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    total_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    vat_code = models.ForeignKey('company.VatCode', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                 related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING)
    vat = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=8)
    status = models.PositiveIntegerField(default=PENDING, choices=STATUSES)

    class Meta:
        abstract = True
        get_latest_by = '-id'

    @property
    def status_name(self):
        return self.get_status_display()

    @property
    def is_picked(self):
        return self.status == self.PICKED

    @property
    def vat_amount(self):
        vat = 0
        price = float(self.price) - float(self.discount_amount)
        if self.vat:
            vat = float(self.vat)/100 * float(price)
        return vat

    @property
    def total_vat_amount(self):
        total = 0
        logger.info("---------- Calculating vat amount ---- {} == {}  ".format(self.vat, self.net_price))
        if self.vat and self.net_price:
            total = float(self.vat) / 100 * float(self.net_price)
        return total

    @property
    def discount_amount(self):
        discount = 0
        if self.discount:
            discount = float(self.discount)/100 * float(self.price)
        return discount

    @property
    def total_discount_amount(self):
        total = 0
        if self.discount_amount:
            total = float(self.discount_amount) * float(self.quantity)
        return total

    @property
    def price_including(self):
        price = float(self.price)
        if self.vat_amount:
            price = price + self.vat_amount
        return price

    @property
    def total_price_including(self):
        total_price = float(self.net_price) + float(self.total_vat_amount)
        return total_price

    @property
    def total_price_excluding(self):
        total_price = float(self.price) * float(self.quantity)
        if self.total_discount_amount:
            total_price -= float(self.total_discount_amount)
        return total_price

    @property
    def deliverable_quantity(self):
        return min(self.inventory.available_stock, self.quantity)

    @property
    def sub_total(self):
        sub_total = 0
        if self.price and self.quantity:
            sub_total = float(self.price) * float(self.quantity)
        return sub_total
