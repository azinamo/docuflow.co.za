from rest_framework import viewsets

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.sales.models import CashUp, Invoice, Receipt, Estimate, Delivery, PickingSlip, BackOrder, Update
from docuflow.apps.sales import enums
from . import serializers
from . import filters


class InvoiceViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.InvoiceSerializer

    def get_queryset(self):
        return Invoice.objects.select_related(
            'invoice_type', 'customer'
        ).filter(branch__company=self.get_company(), period__period_year=self.get_year())


class EstimateViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.EstimateSerializer
    filterset_class = filters.EstimateFilterSet

    def get_queryset(self):
        return Estimate.objects.filter(
            branch=self.get_branch()
        ).select_related('customer').order_by('created_at')


class DeliveryViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.DeliverySerializer

    def get_queryset(self):
        return Delivery.objects.select_related(
            'estimate', 'branch', 'customer'
        ).filter(
            branch=self.get_branch(), period__period_year__year__lte=self.get_year().year,
            status__in=[enums.DeliveryStatus.NOT_INVOICED, enums.DeliveryStatus.REMOVED]
        ).exclude(deleted__isnull=False)


class PickingSlipViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.PickingSlipSerializer

    def get_queryset(self):
        return PickingSlip.objects.select_related(
            'customer', 'estimate', 'estimate__customer', 'period'
        ).filter(
            branch=self.get_branch()
        )


class BackOrderViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.BackOrderSerializer

    def get_queryset(self):
        return BackOrder.objects.select_related(
            'customer', 'estimate', 'estimate__customer', 'period'
        ).prefetch_related(
            'back_order_items', 'back_order_items__inventory', 'back_order_items__inventory__history'
        ).filter(branch=self.get_branch(), period__period_year=self.get_year())


class ReceiptViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.ReceiptSerializer

    def get_queryset(self):
        return Receipt.objects.select_related(
            'customer', 'cash_up', 'branch', 'branch__company'
        ).filter(branch=self.get_branch(), period__period_year=self.get_year())


class CashUpViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.CashUpSerializer

    def get_queryset(self):
        return CashUp.objects.select_related(
            'profile', 'profile__user', 'branch', 'period'
        ).for_year(year=self.get_year()).for_branch(branch=self.get_branch())


class InvoiceUpdateViewSet(ProfileMixin, viewsets.ModelViewSet):
    serializer_class = serializers.InvoiceUpdateSerializer

    def get_queryset(self):
        return Update.objects.invoice_updates(company=self.get_company(), year=self.get_year()).select_related(
            'profile', 'profile__user', 'branch', 'period', 'role'
        )