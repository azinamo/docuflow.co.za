from django_filters import FilterSet, CharFilter, NumberFilter

from docuflow.apps.sales.models import Estimate


class EstimateFilterSet(FilterSet):
    is_template = CharFilter(method='filter_is_template')
    year = CharFilter(method='filter_year')

    class Meta:
        model = Estimate
        fields = ['is_template', 'year']

    # noinspection PyMethodMayBeStatic
    def filter_is_template(self, qs, name, value):
        if value == '1':
            return qs.filter(is_template=True)
        return qs.filter(is_template=False)

    # noinspection PyMethodMayBeStatic
    def filter_year(self, qs, name, value):
        if value:
            return qs.filter(period__period_year__year__lte=int(value))
        return qs.filter(is_template=False)
