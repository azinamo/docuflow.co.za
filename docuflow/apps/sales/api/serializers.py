from django.urls import reverse

from enumfields.drf.serializers import EnumSupportSerializerMixin
from rest_framework import serializers

from docuflow.apps.common.api.fields import FullEnumField
from docuflow.apps.accounts.api.serializers import ProfileSerializer, RoleSerializer
from docuflow.apps.customer.api.serializers import CustomerSerializer, CustomerSummarySerializer
from docuflow.apps.invoice.api.serializers import InvoiceTypeSerializer
from docuflow.apps.sales.models import CashUp, Estimate, Invoice, Receipt, Delivery, PickingSlip, BackOrder, Update
from docuflow.apps.sales.enums import InvoiceStatus


class InvoiceSerializer(EnumSupportSerializerMixin, serializers.ModelSerializer):
    customer = CustomerSummarySerializer()
    document_number = serializers.CharField(source='__str__')
    invoice_type = InvoiceTypeSerializer()
    detail_url = serializers.CharField(source='get_absolute_url')
    status = FullEnumField(InvoiceStatus)

    class Meta:
        model = Invoice
        fields = ('id', 'document_number', 'invoice_type', 'invoice_date', 'net_amount', 'total_amount', 'open_amount',
                  'due_date', 'status', 'customer', 'created_at', 'is_printed', 'detail_url', 'show_due_date',
                  'due_status', 'due_days')
        datatables_always_serialize = ('id', 'detail_url', 'show_due_date', 'is_printed', 'created_at', 'due_status',
                                       'due_days')


class EstimateSerializer(serializers.ModelSerializer):
    customer = CustomerSummarySerializer()
    document_number = serializers.CharField(source='__str__')
    detail_url = serializers.CharField(source='get_absolute_url')
    status = serializers.CharField(source='status.label')
    status_name = serializers.CharField(source='status.name')
    proforma_invoice = serializers.CharField(source='proforma_reference')
    is_expired = serializers.BooleanField()

    class Meta:
        model = Estimate
        fields = ('id', 'document_number', 'expires_at', 'net_amount', 'total_amount', 'status_name', 'customer',
                  'created_at', 'detail_url', 'proforma_invoice', 'status', 'is_expired')
        datatables_always_serialize = ('id', 'detail_url', 'created_at', 'status', 'is_expired')


class DeliverySerializer(serializers.ModelSerializer):
    customer = CustomerSummarySerializer()
    document_number = serializers.CharField(source='__str__')
    detail_url = serializers.CharField(source='get_absolute_url')
    status_name = serializers.CharField(source='status.label')
    status = serializers.CharField(source='status.name')

    class Meta:
        model = Delivery
        fields = ('id', 'document_number', 'total_amount', 'status', 'status_name', 'customer', 'date', 'detail_url')
        datatables_always_serialize = ('id', 'detail_url', 'status')


class PickingSlipSerializer(serializers.ModelSerializer):
    customer = CustomerSummarySerializer()
    document_number = serializers.CharField(source='__str__')
    detail_url = serializers.CharField(source='get_absolute_url')
    status_name = serializers.CharField(source='status.label')
    status = serializers.CharField(source='status.name')

    class Meta:
        model = PickingSlip
        fields = ('id', 'document_number', 'total_amount', 'status', 'status_name', 'customer', 'date', 'detail_url')
        datatables_always_serialize = ('id', 'detail_url', 'status')


class BackOrderSerializer(serializers.ModelSerializer):
    customer = CustomerSummarySerializer()
    document_number = serializers.CharField(source='__str__')
    detail_url = serializers.CharField(source='get_absolute_url')
    status_name = serializers.CharField(source='status.label')
    status = serializers.CharField(source='status.name')
    inventory_status = serializers.IntegerField()

    class Meta:
        model = BackOrder
        fields = ('id', 'document_number', 'total_amount', 'status', 'status_name', 'customer', 'date', 'detail_url',
                  'inventory_status')
        datatables_always_serialize = ('id', 'detail_url', 'status', 'inventory_status')


class CashUpSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()
    reference = serializers.CharField(source='__str__')
    detail_url = serializers.CharField(source='get_absolute_url')

    class Meta:
        model = CashUp
        fields = ('reference', 'date', 'total_amount', 'branch', 'period', 'role', 'profile', 'detail_url')
        datatables_always_serialize = ('id', 'detail_url', 'branch')


class ReceiptSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()
    cash_up = CashUpSerializer()
    detail_url = serializers.CharField(source='get_absolute_url')
    delete_url = serializers.SerializerMethodField()
    edit_url = serializers.SerializerMethodField()
    reference = serializers.CharField(source='__str__')

    class Meta:
        model = Receipt
        fields = ('reference', 'date', 'customer', 'amount', 'cash_up', 'is_open', 'id', 'detail_url', 'edit_url',
                  'delete_url')
        datatables_always_serialize = ('id', 'detail_url', 'edit_url', 'delete_url', 'is_open')

    def get_delete_url(self, instance):
        return reverse('delete_receipt', args=(instance.pk, ))

    def get_edit_url(self, instance):
        return reverse('edit_receipt', args=(instance.pk, ))


class InvoiceUpdateSerializer(serializers.ModelSerializer):
    role = RoleSerializer()
    profile = ProfileSerializer()
    reference = serializers.CharField(source='__str__')
    detail_url = serializers.CharField(source='get_absolute_url')

    class Meta:
        model = Update
        fields = ('reference', 'date', 'branch', 'period', 'role', 'profile', 'detail_url')
        datatables_always_serialize = ('id', 'detail_url', 'branch')

