import logging
from decimal import Decimal

from django.db import models
from django.db.models import Manager, Q, Sum, F
from django.db.models.query import QuerySet

from docuflow.apps.inventory.models import BranchInventory
from . import enums

logger = logging.getLogger(__name__)


class EstimateNotDeleteManager(models.Manager):

    def get_queryset(self):
        return super(EstimateNotDeleteManager, self).get_queryset().filter(deleted__isnull=True)


class EstimateItemManager(models.Manager):

    def create_inventory_line(self, estimate, inventory_id, quantity, price, unit_id=None,
                              discount=None, vat_code_id=None, vat=None, net_price=None, total_amount=None):
        return self.create(
            estimate=estimate,
            inventory_id=inventory_id,
            price=Decimal(price),
            quantity=Decimal(quantity),
            discount=Decimal(discount) if discount else 0,
            vat_code_id=vat_code_id,
            unit_id=unit_id,
            vat=vat,
            net_price=Decimal(net_price) if net_price else 0,
            total_amount=Decimal(total_amount) if total_amount else 0,
            item_type=enums.InvoiceItemType.INVENTORY
        )

    def create_account_line(self, estimate, account, quantity, price, unit_id=None,
                              discount=None, vat_code_id=None, vat=None, net_price=None, total_amount=None):
        return self.create(
            estimate=estimate,
            account=account,
            price=Decimal(price),
            quantity=Decimal(quantity),
            discount=Decimal(discount) if discount else 0,
            vat_code_id=vat_code_id,
            unit_id=unit_id,
            vat=vat,
            net_price=Decimal(net_price) if net_price else 0,
            total_amount=Decimal(total_amount) if total_amount else 0,
            item_type=enums.InvoiceItemType.GENERAL
        )


class InvoiceItemManager(Manager):
    pass


class UpdateQuerySet(QuerySet):

    def status(self, status):
        return self.filter(status=status)

    def company(self, company):
        return self.filter(company=company)


class PurchaseOrderManager(Manager):

    def get_queryset(self, company, supplier=None, status_slug=None):
        q = self.filter(company=company, invoice_type__code='purchase-order')
        if supplier:
            q = q.filter(supplier=supplier)
        if status_slug:
            q = q.filter(status__slug=status_slug)
        return q


class InvoiceManager(Manager):

    def __init__(self, company_id=None, *args, **kwargs):
        self.company_id = company_id
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        queryset = super(InvoiceManager, self).get_queryset().filter(deleted__isnull=True)
        if self.company_id:
            queryset = queryset.filter(branch__company_id=self.company_id)
        return queryset

    def paid(self):
        return self.get_queryset().paid()

    def not_paid(self):
        return self.get_queryset().exclude(open_amount=0).non_recurring()

    def company(self, company):
        return self.get_queryset().company(company)

    def customer(self, customer_id):
        return self.get_queryset().customer(customer_id)

    def status(self, status):
        return self.get_queryset().status(status)

    def open(self):
        return self.get_queryset().exclude(Q(open_amount=0) | Q(open_amount__isnull=True))

    def pending_updates(self, company, period=None, invoice_type=None, start_date=None, end_date=None, year=None):
        q = Q()
        q.add(Q(branch__company=company), Q.AND)
        if period:
            q.add(Q(period=period), Q.AND)
        if invoice_type:
            if isinstance(invoice_type, str):
                q.add(Q(invoice_type__code=invoice_type), Q.AND)
            else:
                q.add(Q(invoice_type=invoice_type), Q.AND)
        if start_date:
            q.add(Q(invoice_date__gte=start_date), Q.AND)
        if end_date:
            q.add(Q(invoice_date__lte=end_date), Q.AND)
        if year:
            q.add(Q(period__period_year__year__lte=year.year), Q.AND)

        return self.select_related(
            'branch', 'customer', 'customer__default_account', 'disposal'
        ).prefetch_related(
            'invoice_items', 'invoice_items__item_link', 'object_items', 'disposal__disposed_fixed_assets'
        ).filter(q).exclude(
            status=enums.InvoiceStatus.UPDATED
        )

    def purchase_order(self, company, supplier=None, status_slug=None):
        q = self.filter(company=company, invoice_type__code='purchase-order')
        if supplier:
            q = q.filter(supplier=supplier)
        if status_slug:
            q = q.filter(status__slug=status_slug)
        return q

    def receipts(self, company, supplier):
        return self.filter(company=company, supplier=supplier, invoice_type__code='receipt')

    def fetch(self):
        return self.get_queryset().select_related(
            'company', 'status', 'invoice_type', 'workflow', 'supplier'
        ).prefetch_related(
            'invoice_references', 'flows', 'flows__status'
        )

    def pending(self, branch):
        return self.get_queryset().filter(inventory__branch=branch)

    def pending_ledger(self, branch):
        return self.pending(branch).exclude(inventory__sku_type=BranchInventory.SERVICE)

    def customer_open_total(self, period, is_last=False):
        invoice_query = self.filter(customer_id=models.OuterRef('id')).order_by()
        if not period:
            return invoice_query.none()
        if is_last:
            invoice_query = invoice_query.filter(period_id__lte=period.id)
        else:
            invoice_query = invoice_query.filter(period=period)
        return invoice_query.values_list(models.Func('open_amount', function='SUM'))

    def with_totals(self):
        return self.annotate(
            quantity_total=Sum('invoice_items__quantity'),
            total_gross_weight=Sum(F('invoice_items__inventory__gross_weight') * F('invoice_items__quantity')),
            total_net_weight=Sum(F('invoice_items__inventory__weight') * F('invoice_items__quantity'))
        )

    def get_detail(self, invoice_id):
        return self.with_totals().prefetch_related(
            'invoice_items', 'invoice_items__inventory', 'invoice_items__inventory__inventory',
            'invoice_items__vat_code', 'invoice_items__account', 'object_items', 'object_items__company_object',
            'invoices', 'receipts', 'customer__addresses', 'invoice_items__vat_code__company',
            'invoice_items__inventory__unit', 'invoice_items__inventory__history'
        ).select_related(
            'linked_invoice', 'customer', 'branch', 'salesman', 'period', 'invoice_type', 'branch__company',
            'branch__company__currency', 'customer__company', 'customer__delivery_method'
        ).get(
            pk=invoice_id
        )

    @classmethod
    def factory(cls, model, company_id=None):
        manager = cls(company_id)
        manager.model = model
        return manager
