from django.contrib import admin

from docuflow.apps.customer.services import age_customer_invoice
from . import models


@admin.register(models.Estimate)
class EstimateAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    autocomplete_fields = ['branch', 'customer', 'period']
    fieldsets = [('Basic Information', {'fields': ['phone_number', 'vat_number', 'expires_at', 'total_amount',
                                                   'net_amount', 'vat_amount', 'status']})
                 ]
    list_display = ('__str__', 'customer', 'vat_number', 'is_template', 'date', 'total_amount', 'vat_amount')
    list_select_related = ('branch', 'customer', 'customer__company')
    list_per_page = 50
    list_filter = ('customer__name', )
    search_fields = ('estimate_id', )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class InvoiceItemAdmin(admin.TabularInline):
    model = models.InvoiceItem


class InvoiceReceiptAdmin(admin.TabularInline):
    model = models.InvoiceReceipt


@admin.register(models.Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    date_hierarchy = 'invoice_date'
    autocomplete_fields = ['branch', 'customer', 'period', 'invoice_type']
    fieldsets = [('Basic Information', {
        'fields': ['branch', 'customer', 'period', 'invoice_type', 'invoice_date', 'accounting_date', 'due_date',
                   'total_amount', 'net_amount', 'vat_amount', 'open_amount', 'status']})
                 ]
    list_display = ('__str__', 'get_customer', 'invoice_type', 'is_recurring', 'invoice_date', 'total_amount',
                    'open_amount', 'vat_amount')
    # list_filter = ('branch', 'customer')
    list_per_page = 50
    search_fields = ('invoice_id', )
    inlines = [InvoiceReceiptAdmin, ]

    def get_queryset(self, request):
        return super().get_queryset(request).select_related(
            'branch', 'customer', 'customer__company', 'created_by', 'invoice_type').prefetch_related('receipts')

    # noinspection PyMethodMayBeStatic
    def get_customer(self, invoice: models.Invoice):
        return invoice.customer.name

    def save_form(self, request, form, change):
        invoice = super().save_form(request, form, change)
        # age_customer_invoice(invoice=invoice)
        return invoice

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ReceiptAccountAdmin(admin.TabularInline):
    model = models.ReceiptAccount


class ReceiptPaymentAdmin(admin.TabularInline):
    model = models.ReceiptPayment


class SundryAllocationAdmin(admin.TabularInline):
    model = models.SundryReceiptAllocation


@admin.register(models.Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ('__str__', 'branch', 'customer', 'period', 'amount', 'balance', 'cash_up')
    fieldsets = [
        ('Basic Information', {
            'fields': ['date', 'payment_method', 'amount', 'balance', 'discount', 'notes', 'attachment']
        })
    ]
    list_filter = ('branch', 'customer')
    inlines = [ReceiptAccountAdmin, ReceiptPaymentAdmin, InvoiceReceiptAdmin]
    list_select_related = ('branch', 'customer', 'period', 'cash_up')
    list_per_page = 50
    search_fields = ('receipt_id', )

    def has_add_permission(self, request):
        return False


# class CashUpAccountAdmin(admin.TabularInline):
#     model = models.CashUp


@admin.register(models.CashUp)
class CashUpAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ('__str__', 'branch', 'total_amount', 'period', 'profile', 'role')
    list_filter = ('branch', )
    # inlines = [CashUpAccountAdmin, ]
    list_select_related = ('branch', 'profile', 'role', 'period')
    list_per_page = 50
    search_fields = ('cashup_id', )

    def has_add_permission(self, request):
        return False
