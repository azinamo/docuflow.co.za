import re

from django.conf import settings


from docuflow.apps.invoice.models import InvoiceType, Status
from docuflow.apps.common.enums import Module


def get_sales_invoice_type(company, code='invoice'):
    invoice_type = InvoiceType.objects.filter(code=code, company=company, file__module=Module.SALES).first()
    return invoice_type


def get_default_invoice_type(company, code='tax-invoice'):
    return InvoiceType.objects.filter(code=code, company=company, file__module=Module.PURCHASE).first()


def get_default_status():
    status = Status.objects.filter(slug='arrived-at-logger').first()
    return status


def get_invoice_type_number_counter(number_start):
    digits = re.findall(r'\d+', number_start)
    match_len = len(digits)
    if match_len > 0:
        return int(digits[match_len-1])
    return 1


def get_invoice_type_number_prefix(number_start):
    digits = re.findall(r'\d+', number_start)
    match_len = len(digits)
    if match_len > 0:
        return number_start[:-len(str(int(digits[match_len-1])))]
    return number_start


