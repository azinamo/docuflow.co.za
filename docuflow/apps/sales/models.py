import logging
import pprint
from datetime import datetime
from decimal import Decimal

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, ContentType, GenericRelation
from django.db import models
from django.db.models import JSONField
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.timezone import now
from django.utils.translation import gettext as __
from django_countries.fields import CountryField
from enumfields import EnumIntegerField, EnumField
from safedelete.managers import SafeDeleteManager, DELETED_VISIBLE
from safedelete.models import SafeDeleteModel, SOFT_DELETE

from docuflow.apps.common.enums import DueStatus, SaProvince, Module
from docuflow.apps.common.utils import create_document_number
from docuflow.apps.common.behaviours import Timestampable
from docuflow.apps.company.models import Company, ObjectItem, VatCode
from docuflow.apps.inventory.models import BranchInventory, PurchaseHistory, BranchInventory
from docuflow.apps.journals.models import Journal, JournalLine
from docuflow.apps.period.models import Year
from docuflow.apps.sales import utils
from docuflow.apps.sales.exceptions import QuantityNotAvailableException
from . import enums
from . import managers
from .behaviors import Addressable, Deliverable

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class SalesInvoiceManager(SafeDeleteManager):
    _safedelete_visibility = DELETED_VISIBLE


class SalesInvoice(SafeDeleteModel, Addressable, models.Model):
    _safedelete_policy = SOFT_DELETE

    branch = models.ForeignKey('company.Branch', related_name="%(app_label)s_%(class)s_related", related_query_name="%(app_label)s_%(class)ss",
                               null=True, blank=True, on_delete=models.CASCADE)
    sub_total = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    net_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Net')
    vat = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Vat(%)')
    vat_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Vat')
    line_discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Line discount')
    discount_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Total Discount')
    total_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Total')
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    profile = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING,
                                verbose_name='Created By')
    role = models.ForeignKey('accounts.Role', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                             related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING,
                             verbose_name='Created By Role'
                             )
    pricing = models.ForeignKey('customer.Pricing', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING, verbose_name='Pricing')
    salesman = models.ForeignKey('Salesman', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                 related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING, verbose_name='Salesman')
    address = models.ForeignKey('customer.Address', null=True, blank=True, on_delete=models.CASCADE,
                                related_name="%(app_label)s_%(class)s_related", related_query_name="%(app_label)s_%(class)ss")
    objects = SalesInvoiceManager()

    class Meta:
        abstract = True
        ordering = ['-created_at']

    def get_vat(self):
        vat = 0
        if self.branch.company:
            default_vat = self.branch.company.company_vat_codes.filter(is_default=True).first()
            if default_vat:
                vat = default_vat.percentage
        return vat

    def calculate_vat_amount(self):
        vat_amount = 0
        if self.vat:
            vat_amount = Decimal(float(self.vat) / 100) * self.net_amount
        return vat_amount

    @property
    def net_total(self):
        total = 0
        if self.sub_total:
            total += float(self.sub_total)
        if self.discount_amount:
            total += float(self.discount_amount)
        return total

    @property
    def total_sub(self):
        total = Decimal(0)
        if self.sub_total:
            total += self.sub_total
        return total


class CustomerInvoice(models.Model):
    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.CASCADE,
                                 related_name="%(app_label)s_%(class)s_related", related_query_name="%(app_label)s_%(class)ss")
    customer_name = models.CharField(max_length=255, null=True, blank=True, verbose_name='Customer')
    discount_percentage = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=4, verbose_name='Discount(%)')
    vat_on_customer_discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Vat')
    customer_discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Customer discount')
    vat_number = models.CharField(max_length=255, null=True, blank=True, verbose_name='Customer Vat')
    phone_number = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        abstract = True

    def get_customer_discount(self):
        discount = 0
        if self.customer:
            discount = self.customer.customer_discount
        return discount * -1

    def get_vat(self):
        vat = 0
        if self.customer.company:
            vat = self.customer.company.vat_percentage
        return vat

    def calculate_customer_discount(self, sub_total):
        amount = 0
        discount = self.get_customer_discount()
        if discount:
            amount = (float(discount) / 100) * float(sub_total)
        return Decimal(amount)

    def cal_vat_on_discount(self, discount_amount):
        vat = self.get_vat()
        vat_on_discount = 0
        if vat and discount_amount:
            vat_on_discount = (float(vat) / 100) * float(discount_amount)
        return Decimal(vat_on_discount)

    @property
    def is_cash(self):
        return self.customer.is_default or self.customer.is_cash

    @property
    def has_customer_discount(self):
        return self.customer_discount != 0

    @property
    def customer_discount_total(self):
        amount = 0
        if self.customer and self.customer.is_invoice_discount:
            if self.customer.invoice_discount:
                amount = (float(self.customer.invoice_discount) / 100) * float(self.net_amount)
        return Decimal(amount * -1)

    @property
    def full_name(self):
        if self.customer_name:
            return self.customer_name
        elif self.customer:
            return str(self.customer)

    @property
    def display_vat_number(self):
        return self.customer.vat_number if self.customer else ''

    @property
    def customer_code(self):
        if self.customer:
            return self.customer.number
        return ''

    @property
    def vat_customer_discount(self):
        amount = 0
        if self.customer.company:
            default_vat = self.customer.company.company_vat_codes.filter(is_default=True).first()
            if default_vat and self.customer_discount_total:
                amount = (float(default_vat.percentage) / 100) * float(self.customer_discount_total)
        return Decimal(amount)


class SalesItem(models.Model):

    PENDING = 0
    PICKED = 1
    INVOICED = 2
    SEND_FOR_PICKING = 3

    STATUSES = (
        (PENDING, 'Pending'),
        (PICKED, 'Picked'),
        (INVOICED, 'Invoiced'),
        (SEND_FOR_PICKING, 'Send for picking')
    )

    inventory = models.ForeignKey('inventory.BranchInventory', related_name="%(app_label)s_%(class)s_related",
                                  related_query_name="%(app_label)s_%(class)ss", null=True, blank=True, on_delete=models.CASCADE)
    description = models.TextField()
    item_type = EnumIntegerField(enums.InvoiceItemType, default=enums.InvoiceItemType.INVENTORY)
    quantity = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    unit = models.ForeignKey('company.Unit', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                             related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING)
    price = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    customer_discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    net_price = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    vat_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    total_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    vat_code = models.ForeignKey('company.VatCode', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                 related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING)
    vat = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=4)
    status = EnumIntegerField(enums.InvoiceItemStatus, default=enums.InvoiceItemStatus.PENDING)
    account = models.ForeignKey('company.Account', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                related_query_name="%(app_label)s_%(class)ss", on_delete=models.CASCADE)

    def __str__(self):
        return f'({self.pk}) -> {self.quantity} * {str(self.inventory)} inventory items @ {self.price}'

    class Meta:
        abstract = True
        get_latest_by = '-id'

    def set_as_general_line(self):
        self.item_type = enums.InvoiceItemType.GENERAL
        self.save()

    @property
    def status_name(self):
        return str(self.status)

    @property
    def is_picked(self):
        return self.status == enums.InvoiceItemStatus.PICKED

    @property
    def calculated_vat(self):
        vat = 0
        price = self.price - self.discount_amount
        if self.vat:
            vat = float(self.vat) / 100 * float(price)
        return Decimal(vat)

    @property
    def total_vat_amount(self):
        vat_amount = 0
        quantity = self.quantity if self.quantity else 0
        if self.vat and self.price:
            vat_amount = Decimal(float(self.vat) / 100) * self.price
        return Decimal(round(vat_amount * quantity, 2))

    @property
    def discount_amount(self):
        discount = 0
        if self.discount and self.price:
            discount = float(self.discount) / 100 * float(self.price)
        return Decimal(discount)

    @property
    def total_discount_amount(self):
        total = 0
        if self.discount_amount and self.quantity:
            total = float(self.discount_amount) * float(self.quantity)
        return Decimal(total)

    @property
    def price_including(self):
        price = self.price
        if self.vat and self.price:
            price = price + Decimal(float(self.vat) / 100 * float(self.price))
        return price

    @property
    def total_price_including(self):
        return self.total_price_excluding + (self.total_vat_amount or 0)

    @property
    def total_price_excluding(self):
        total_price = self.sub_total
        if self.total_discount_amount:
            total_price -= self.total_discount_amount
        return total_price

    @property
    def deliverable_quantity(self):
        return min(self.inventory.available_stock, self.quantity)

    @property
    def sub_total(self):
        amount = 0
        if self.price and self.quantity:
            amount = self.price * self.quantity
        return Decimal(amount)

    @property
    def picking_quantity(self):
        if self.inventory:
            available_qty = self.inventory.available_stock
            if available_qty > 0:
                return min(self.inventory.available_stock, self.quantity)
        return 0

    @property
    def picking_balance(self):
        picking_quantity = self.picking_quantity if self.picking_quantity else 0
        if self.quantity:
            return self.quantity - picking_quantity
        return 0

    @cached_property
    def is_available(self):
        if self.inventory:
            available_stock = self.inventory.available_stock
            if available_stock and available_stock > 0:
                if available_stock >= self.quantity:
                    return True
                else:
                    return -1
        return False

    @property
    def is_inventory_available(self):
        if self.inventory and self.inventory.available_stock:
            if self.inventory.available_stock >= self.quantity:
                return True
        return False

    @cached_property
    def is_invoicable(self):
        if self.inventory and self.inventory.invoicable_stock:
            if self.inventory.invoicable_stock >= self.quantity:
                return True
        return False

    @cached_property
    def invoicable_stock(self):
        if self.inventory.invoicable_stock > 0:
            return min(self.inventory.invoicable_stock, self.quantity)
        return Decimal(0)

    @property
    def is_inventory(self):
        return self.item_type == enums.InvoiceItemType.INVENTORY and self.inventory

    @property
    def is_account(self):
        return self.item_type == enums.InvoiceItemType.GENERAL and self.account

    @property
    def is_description(self):
        return self.item_type == enums.InvoiceItemType.TEXT

    @property
    def gross_weight(self):
        if (self.inventory and self.inventory.gross_weight) and self.quantity:
            return self.inventory.gross_weight * self.quantity
        return 0

    @property
    def net_weight(self):
        if (self.inventory and self.inventory.weight) and self.quantity:
            return self.inventory.weight * self.quantity
        return 0


class Address(models.Model):
    DELIVERY = 0
    POSTAL = 2

    TYPES = (
        (DELIVERY, 'Delivery'),
        (POSTAL, 'Postal')
    )

    address_type = models.PositiveIntegerField(default=DELIVERY, choices=DELIVERY)
    address_line_1 = models.CharField(max_length=255, null=False, blank=False, verbose_name='Street')
    address_line_2 = models.CharField(max_length=255, blank=True, null=True, verbose_name='Suburb')
    address_line_3 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    province = models.CharField(blank=True, max_length=50)
    postal_code = models.CharField(max_length=255, blank=True, null=True)
    country = CountryField(default=settings.COUNTRY_CODE)
    latitude = models.DecimalField(default=False, decimal_places=10, max_digits=15, blank=True, null=True)
    longitude = models.DecimalField(default=False, decimal_places=10, max_digits=15, blank=True, null=True)
    is_default = models.BooleanField(default=False)

    class Meta:
        abstract = True
        get_latest_by = '-id'

    def save_address(self, cls, address, address_type):
        address_line_1 = address.get('street', None)
        if len(address) > 0 and address_line_1:
            address_object = cls.objects.filter(address_type=address_type, estimate=self).first()
            if not address_object:
                address_object = cls()
                address_object.estimate = self

            address_object.address_type = address_type
            address_object.postal_code = address.get('postal_code', '')
            address_object.country = address.get('country', None)
            address_object.province = address.get('province', None)
            address_object.city = address.get('city', None)
            address_object.address_line_1 = address_line_1
            address_object.save()

    @property
    def province_name(self):
        return self.province

    @property
    def address_type_name(self):
        return self.get_address_type_display()


class Invoice(SalesInvoice, CustomerInvoice):

    invoice_id = models.CharField(max_length=255, blank=True, null=True)
    invoice_type = models.ForeignKey('invoice.InvoiceType', related_name='invoice_type_sales_invoice', blank=True,
                                     null=True, on_delete=models.DO_NOTHING, verbose_name='Document Types')
    linked_invoice = models.ForeignKey('self', null=True, blank=True, on_delete=models.DO_NOTHING, related_name='links')
    picking_slip = models.OneToOneField('PickingSlip', null=True, blank=True, related_name='invoice', on_delete=models.DO_NOTHING)
    estimate = models.ForeignKey('Estimate', null=True, blank=True, related_name='estimate', on_delete=models.DO_NOTHING)
    delivery = models.OneToOneField('Delivery', null=True, blank=True, related_name='deliveries', on_delete=models.DO_NOTHING)
    back_order = models.OneToOneField('BackOrder', null=True, blank=True, related_name='back_order', on_delete=models.DO_NOTHING)
    disposal = models.OneToOneField('fixedasset.Disposal', null=True, blank=True, related_name='invoice', on_delete=models.CASCADE)
    supplier_number = models.CharField(max_length=255, null=True, blank=True)
    invoice_number = models.CharField(max_length=255, null=True, blank=True, default='', verbose_name='Document Number')
    invoice_date = models.DateField(null=True, blank=True)
    accounting_date = models.DateField(null=True, blank=True)
    payment_date = models.DateField(null=True, blank=True)
    due_date = models.DateField(null=True, blank=True, verbose_name='Payment Due')
    open_amount = models.DecimalField(default=0, blank=True, decimal_places=2, max_digits=12, verbose_name='Credit Left')
    delivery_number = models.CharField(max_length=255, null=True, blank=True, verbose_name='Delivery Number')
    estimate_number = models.CharField(max_length=255, null=True, blank=True, verbose_name='Estimate Number')
    po_number = models.CharField(max_length=255, null=True, blank=True, verbose_name='PO Number')
    created_by = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name='sales_invoice_created',
                                   on_delete=models.DO_NOTHING)
    journal_line = models.OneToOneField('journals.JournalLine', null=True, blank=True, related_name='customer_invoice',
                                        on_delete=models.DO_NOTHING)
    period = models.ForeignKey('period.Period', null=True, blank=True, related_name='sales_invoices', on_delete=models.DO_NOTHING)
    is_recurring = models.BooleanField(default=False, verbose_name='Recurring')
    recurring = models.ForeignKey('self', null=True, blank=True, related_name='recurring_invoices', on_delete=models.SET_NULL)
    status = EnumIntegerField(enums.InvoiceStatus, default=enums.InvoiceStatus.PENDING)
    suffix = models.CharField(max_length=255, blank=True)
    object_items = models.ManyToManyField('company.ObjectItem', blank=True)

    class QS(models.QuerySet):

        def paid(self):
            return self.filter(status=5)

        def not_paid(self):
            return self.exclude(open_amount=0)

        def customer(self, customer_id):
            return self.filter(customer_id=customer_id)

        def non_recurring(self):
            return self.filter(models.Q(is_recurring=False) | models.Q(is_recurring__isnull=True)).distinct()

    # objects = managers.InvoiceManager()
    objects = managers.InvoiceManager.from_queryset(QS)()

    class Meta:
        ordering = ('-created_at',)
        get_latest_by = 'created_at'

    def __str__(self):
        category_str = ''
        counter_str = str(int(float(self.invoice_id))).rjust(4, '0')
        if self.invoice_type:
            if self.invoice_type.number_start:
                prefix = utils.get_invoice_type_number_prefix(self.invoice_type.number_start)
                return f"{prefix}{self.invoice_id}"
            else:
                category_str = 'INV'
                if self.invoice_type.is_credit:
                    category_str = 'CR'
        return f"{category_str}{counter_str}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.invoice_id:
            self.invoice_id = self.generate_invoice_number()
        return super(Invoice, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('view_sales_invoice', args=(self.pk,))

    # noinspection PyMethodMayBeStatic
    def generate_invoice_number(self):
        if self.invoice_type.number_start:
            year = Year.objects.started().filter(company=self.branch.company).first()
            invoice_count = Invoice.objects.filter(
                branch__company=self.branch.company, invoice_type=self.invoice_type, is_recurring=self.is_recurring,
                created_at__date__range=[year.start_date, year.end_date]
            ).count()
            number_start = utils.get_invoice_type_number_counter(self.invoice_type.number_start) + invoice_count
            invoice_id = self.get_next_invoice_number(number_start)
            return invoice_id
        else:
            return self.get_next_invoice_number(1)

    # noinspection PyMethodMayBeStatic
    def get_next_invoice_number(self, counter):
        invoice = Invoice.objects.filter(
            branch__company_id=self.branch.company_id, is_recurring=self.is_recurring, invoice_type=self.invoice_type
        ).first()
        if invoice:
            counter = int(float(invoice.invoice_id)) + 1
        return counter

    def update_customer_credit(self):
        customer = self.customer
        available_credit = Decimal(0)
        if customer.available_credit:
            available_credit = customer.available_credit
        if self.total_amount:
            available_credit -= self.total_amount
        customer.available_credit = available_credit
        customer.save()

    def mark_as_paid(self, date):
        self.save()

    def mark_as_paid_not_printed(self):
        if self.open_amount != 0:
            raise ValueError(f'You are trying to make the invoice as paid and not printed, but there is an open '
                             f'value of {self.open_amount}. Please ensure that the open is 0 before proceeding')
        logger.info("Mark invoice as paid not printed")
        self.set_status(enums.InvoiceStatus.PAID_NOT_PRINTED)

    def mark_as_partially_paid(self):
        self.set_status(enums.InvoiceStatus.PARTIALLY_PAID)

    def mark_as_updated(self):
        self.set_status(enums.InvoiceStatus.UPDATED)

    def mark_as_draft(self):
        self.set_status(enums.InvoiceStatus.DRAFT)

    def mark_as_completed(self):
        pass
        # self.status = enums.InvoiceStatus.COMPLETED
        # self.save()

    def make_copy(self):
        self.save()

    def credit_invoice(self):
        self.save()

    def send_email(self):
        pass

    def set_status(self, status):
        if status in [enums.InvoiceStatus.PAID_NOT_PRINTED]:
            logger.info("Set open amount to 0, since its paid, but not printed")
            self.open_amount = 0
        self.status = status
        self.save()

    def calculate_open_amount(self, amount):
        open_amount = self.open_amount - amount
        self.open_amount = open_amount
        self.save()
        return open_amount

    def save_object_items(self, posted_object_items):
        self.object_items.clear()
        for object_item_id in posted_object_items:
            object_item_str = str(object_item_id)
            if len(object_item_str) > 0:
                object_item = ObjectItem.objects.get(pk=int(object_item_id))
                if object_item:
                    self.object_items.add(object_item)

    def created_picking_slip(self):
        picking_slip = PickingSlip.objects.create(
            profile=self.profile,
            delivery=self,
            period=self.period,
            date=self.invoice_date,
        )

        if picking_slip:
            total_net_amount = 0
            total_amount = 0
            total_vat = 0
            total_discount = 0
            sub_total = 0
            for delivery_item in self.invoice_items.all():
                vat_amount, net_amount, amount, discount_amount = self.create_picking_item_from_delivery_item(picking_slip, delivery_item)
                total_vat += vat_amount
                total_net_amount += net_amount
                total_amount += amount
                total_discount += discount_amount
                sub_total += net_amount + discount_amount

            picking_slip.sub_total = sub_total
            picking_slip.total_amount = total_amount
            picking_slip.net_amount = total_net_amount
            picking_slip.discount_amount = total_discount
            picking_slip.vat_amount = total_vat
            picking_slip.save()

    def create_picking_item_from_delivery_item(self, picking_slip, delivery_item):
        vat_amount = 0
        amount = float(delivery_item.price)
        quantity = float(delivery_item.quantity)
        total = amount * quantity
        net_amount = amount * quantity
        if delivery_item.vat:
            vat_amount = delivery_item.total_vat_amount
        PickingSlipItem.objects.create(
            picking_slip=picking_slip,
            inventory=delivery_item.inventory,
            description=delivery_item.description,
            quantity=delivery_item.quantity,
            received=delivery_item.quantity,
            unit=delivery_item.unit,
            price=delivery_item.price,
            discount=delivery_item.discount,
            net_price=delivery_item.net_price,
            vat_code=delivery_item.vat_code,
            vat=delivery_item.vat
        )
        return vat_amount, net_amount, total, float(delivery_item.discount_amount)

    def create_inventory_journal(self, company, profile, role_id):
        year = self.period.period_year
        sales_journal = Journal.objects.create(
            company=company,
            year=year,
            period=self.period,
            journal_text=str(self),
            module=Module.SALES,
            role_id=role_id,
            created_by=profile,
            date=self.invoice_date
        )
        if sales_journal:
            content_type = ContentType.objects.filter(app_label='company', model='account').first()
            for sales_item in self.invoice_items.all():
                JournalLine.objects.create(
                    journal=sales_journal,
                    content_type=content_type,
                    object_id=sales_item.id,
                    debit=0,
                    credit=sales_item.net_price,
                    account=sales_item.inventory.sales_account,
                    accounting_date=sales_journal.date
                )
            JournalLine.objects.create(
                journal=sales_journal,
                content_type=content_type,
                object_id=self.id,
                debit=0,
                credit=self.vat_amount,
                account=company.default_vat_account,
                accounting_date=sales_journal.date
            )
            JournalLine.objects.create(
                journal=sales_journal,
                content_type=content_type,
                object_id=self.id,
                debit=self.total_amount,
                credit=0,
                account=company.default_customer_account,
                accounting_date=sales_journal.date
            )

        cost_journal = Journal.objects.create(
            company=company,
            year=year,
            period=self.period,
            journal_text=str(self),
            module=Module.SALES,
            role_id=role_id,
            created_by=profile,
            date=self.invoice_date
        )
        if cost_journal:
            content_type = ContentType.objects.filter(app_label='sales', model='invoiceitem').first()
            total = 0
            for sales_item in self.invoice_items.all():
                JournalLine.objects.create(
                    journal=cost_journal,
                    content_type=content_type,
                    object_id=sales_item.id,
                    debit=0,
                    credit=sales_item.inventory.cost_price,
                    account=sales_item.inventory.cost_of_sales_account,
                    accounting_date=cost_journal.date
                )
                total += sales_item.inventory.cost_price
            content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
            JournalLine.objects.create(
                journal=cost_journal,
                content_type=content_type,
                object_id=self.id,
                debit=total,
                credit=0,
                account=company.default_customer_account,
                accounting_date=cost_journal.date
            )

    def calculate_statement_discount(self, receipt_date):
        if self.customer.statement_discount:
            if self.due_date >= receipt_date:
                return self.total_amount * (self.customer.statement_discount / 100)
        return Decimal(0)

    def has_credit_balance(self):
        return sum(invoice_item.available_quantity for invoice_item in self.invoice_items.all() if invoice_item.available_quantity)

    def is_fully_paid(self):
        receipt_total = sum(receipt.amount for receipt in self.receipts.all())
        return receipt_total >= self.total_amount

    @property
    def debit(self):
        if self.total_amount > 0:
            return self.total_amount
        return 0

    @property
    def credit(self):
        if self.total_amount <= 0:
            return self.total_amount * -1
        return 0

    @property
    def total_with_discount(self):
        amount = self.total_amount if self.total_amount else 0
        amount += self.customer_discount * -1 if self.customer_discount else 0
        return amount

    @property
    def status_name(self):
        return str(self.status)

    @property
    def total_order_value(self):
        return self.customer_net_total + self.total_invoice_vat

    @property
    def customer_net_total(self):
        total = Decimal(0)
        if self.sub_total:
            total += self.sub_total
        if self.discount_amount:
            total += self.discount_amount
        return total

    @property
    def total_invoice_vat(self):
        amount = 0
        if self.customer.company:
            default_vat = self.customer.company.company_vat_codes.filter(is_default=True).first()
            if default_vat and self.customer_net_total:
                amount = (float(default_vat.percentage) / 100) * float(self.customer_net_total)
        return Decimal(amount)

    @property
    def is_credit(self):
        return self.invoice_type and self.invoice_type.is_credit

    @property
    def is_invoice(self):
        return self.invoice_type and self.invoice_type.code == enums.InvoiceType.TAX_INVOICE.value

    @property
    def is_creditable(self):
        if self.has_credit_balance() > 0:
            return True
        return not self.links.exists()

    @property
    def subtotal_amount(self):
        if self.is_credit:
            return self.sub_total * -1
        return self.sub_total

    @property
    def net_total(self):
        if self.is_credit and self.net_amount:
            return self.net_amount * -1
        return self.net_amount

    @property
    def vat_total(self):
        if self.is_credit and self.vat_amount:
            return self.vat_amount * -1
        return self.vat_amount

    @property
    def line_discount_total(self):
        if self.is_credit and self.line_discount:
            return self.line_discount * -1
        return self.line_discount

    @property
    def discount_amount_total(self):
        if self.is_credit and self.discount_amount:
            return self.discount_amount * -1
        return self.discount_amount

    @property
    def invoice_total(self):
        if self.is_credit and self.total_amount and self.total_amount < 0:
            return self.total_amount * -1
        return self.total_amount

    @property
    def outstanding(self):
        if self.is_credit and self.open_amount:
            return self.open_amount * -1
        return self.open_amount

    @property
    def is_printed(self):
        return self.status == enums.InvoiceStatus.UPDATED

    @property
    def is_draft(self):
        return self.status == enums.InvoiceStatus.DRAFT

    @property
    def show_due_date(self):
        return self.open_amount != 0 and self.due_date

    @property
    def statement_discount(self):
        if self.customer.statement_discount:
            if self.due_date >= self.customer.max_discount_date():
                return self.total_amount * (self.customer.statement_discount / 100)
        return Decimal(0)

    @property
    def discounted_amount(self):
        return self.total_amount - self.statement_discount

    @property
    def has_inventory_lines(self):
        return self.invoice_items.filter(inventory__isnull=False).exists()

    @property
    def gross_weight(self):
        invoice_items = self.invoice_items.filter(inventory__isnull=False).all()
        return sum(float(invoice_item.inventory.gross_weight) * float(invoice_item.quantity) for invoice_item in invoice_items if invoice_item.inventory.gross_weight)

    @property
    def net_weight(self):
        invoice_items = self.invoice_items.filter(inventory__isnull=False).all()
        return sum(float(invoice_item.inventory.weight) * float(invoice_item.quantity) for invoice_item in invoice_items if invoice_item.inventory.weight)

    @property
    def total_quantity(self):
        return sum(float(invoice_item.quantity) for invoice_item in self.invoice_items.all())

    @property
    def due_days(self):
        if not self.due_date:
            return 0
        delta = self.due_date - now().date()
        return delta.days

    @property
    def due_status(self):
        if self.due_days < 0:
            return DueStatus.OVERDUE.value
        elif self.due_days < 7:
            return DueStatus.DUE.value
        else:
            return DueStatus.NOT_DUE.value


class InvoiceItemManager(models.Manager):

    def pending_ledger(self, branch):
        return self.get_queryset().filter(
            invoice__status=enums.InvoiceStatus.PENDING, inventory__branch=branch
        ).exclude(
            inventory__sku_type=BranchInventory.SERVICE
        )

    def create_inventory_line(self, invoice, inventory, quantity, price, vat_code, **extras):
        pass

    def create_account_line(self, invoice, account, description):
        return InvoiceItem.objects.create(
            description=description,
            account=account,
            invoice=invoice,
            item_type=enums.InvoiceItemType.GENERAL,
            discount=0,
            price=0,
            net_price=0,
            vat_code_id=None,
            quantity=0,
            available_quantity=0,
            vat=0,
        )

    def create_description_line(self, invoice, description):
        return InvoiceItem.objects.create(
            description=description,
            invoice=invoice,
            item_type=enums.InvoiceItemType.INVENTORY,
            discount=0,
            price=0,
            net_price=0,
            vat_code_id=None,
            quantity=0,
            available_quantity=0,
            vat=0,
        )


class InvoiceItem(SalesItem):
    invoice = models.ForeignKey(Invoice, related_name='invoice_items', on_delete=models.CASCADE)
    item_link = models.ForeignKey('self', related_name='inventory_item_link', null=True, blank=True, on_delete=models.CASCADE)
    available_quantity = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)
    returns = GenericRelation('inventory.InventoryReceivedReturned', 'returned_object_id', 'returned_type')
    received = GenericRelation('inventory.InventoryReceivedReturned', 'received_object_id', 'received_type')

    objects = InvoiceItemManager()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return f"{self.invoice} {self.inventory} qty={self.quantity} @ {self.price}"

    def calculate_service_cost(self):
        cost = 0
        if self.inventory:
            cost = (float(self.inventory.service_cost) / 100) * float(self.price)
        return Decimal(cost)

    def update_available_quantity(self, quantity):
        available_quantity = self.available_quantity
        if available_quantity:
            available_quantity = available_quantity - quantity
        else:
            available_quantity = self.quantity - quantity
        self.available_quantity = available_quantity
        self.save()
        return self

    @property
    def item_description(self):
        if self.inventory:
            return self.inventory.description
        elif self.account:
            logger.info(f"Get disposal ==> {self.disposal}")
            try:
                if self.disposal:
                    return self.disposal.fixed_asset
                else:
                    return self.account
            except:
                return self.account
        else:
            return self.description

    @property
    def invoicable_quantity(self):
        if self.inventory:
            return min(self.available_quantity, self.inventory.in_stock)
        return self.quantity

    @property
    def total_vat_amount(self):
        vat_amount = self.vat_amount
        if vat_amount:
            return Decimal(round(vat_amount, 2)).quantize(Decimal('.01'))
        return Decimal(0).quantize(Decimal('.01'))


class TrackingManager(models.Manager):

    def create_tracking(self, model_object, comment, profile, role, data):
        return Tracking.objects.create(
            object_id=model_object.pk,
            content_type=ContentType.objects.get_for_model(model_object),
            comment=comment,
            profile=profile,
            role=role,
            data=data
        )

    def get_tracking(self, model_object):
        return self.filter(object_id=model_object.id, content_type=ContentType.objects.get_for_model(model_object))


class Tracking(models.Model):
    content_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.CASCADE, related_name='trackings')
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    comment = models.TextField()
    data = JSONField(blank=True)
    profile = models.ForeignKey('accounts.Profile', related_name='sales_comments', null=True, blank=True, on_delete=models.DO_NOTHING)
    role = models.ForeignKey('accounts.Role', related_name='sales_comments', null=True, blank=True, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = TrackingManager()


class ProformaInvoiceManager(models.Manager):

    def get_detail(self, proforma_invoice_id):
        query = ProformaInvoiceItem.objects.select_related(
            'inventory', 'inventory__inventory', 'account', 'vat_code'
        ).filter()
        return self.select_related(
            'period', 'salesman', 'customer', 'branch', 'branch__company'
        ).prefetch_related(
            models.Prefetch('invoice_items', queryset=query),
            'customer__addresses', 'branch__company__bank_details'
        ).get(
            pk=proforma_invoice_id
        )


class ProformaInvoice(SalesInvoice, CustomerInvoice):
    invoice_id = models.CharField(max_length=255)
    invoice_type = models.ForeignKey('invoice.InvoiceType', on_delete=models.DO_NOTHING)
    invoice_number = models.CharField(max_length=255, blank=True)
    invoice_date = models.DateField(null=True, blank=True)
    payment_date = models.DateField(null=True, blank=True)
    due_date = models.DateField(null=True, blank=True)
    open_amount = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=12)
    delivery_number = models.CharField(max_length=255, blank=True)
    estimate_number = models.CharField(max_length=255, blank=True)
    po_number = models.CharField(max_length=255, blank=True)
    created_by = models.ForeignKey('accounts.Profile', null=True, blank=True, on_delete=models.DO_NOTHING)
    period = models.ForeignKey('period.Period', null=True, blank=True, on_delete=models.DO_NOTHING)
    status = EnumIntegerField(enums.ProformaInvoiceStatus, default=enums.ProformaInvoiceStatus.OPEN)

    objects = ProformaInvoiceManager()

    class Meta:
        ordering = ('-created_at',)
        get_latest_by = 'created_at'

    def __str__(self):
        prefix_str = 'PINV'
        counter_str = str(self.invoice_id).rjust(4, '0')
        if self.invoice_type:
            if self.invoice_type.number_start:
                prefix = utils.get_invoice_type_number_prefix(self.invoice_type.number_start)
                return f"{prefix}{self.invoice_id}"
            else:
                prefix_str = 'PINV'
        return f"{prefix_str}{counter_str}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.invoice_id:
            filter_options = {'branch__company_id': self.branch.company_id}
            if self.invoice_type.number_start:
                invoice_count = ProformaInvoice.objects.filter(
                    branch__company=self.branch.company, invoice_type=self.invoice_type
                ).count()
                invoice_id = utils.get_invoice_type_number_counter(self.invoice_type.number_start) + invoice_count
                self.invoice_id = invoice_id
            else:
                self.invoice_id = self.generate_invoice_number(1, filter_options)
        return super(ProformaInvoice, self).save(*args, **kwargs)

    def generate_invoice_number(self, counter, filter_options):
        invoice = ProformaInvoice.objects.filter(**filter_options).first()
        if invoice:
            counter = int(float(invoice.invoice_id)) + 1
        return counter

    def mark_as_completed(self):
        self.status = enums.ProformaInvoiceStatus.INVOICED
        self.save()

    @property
    def total_quantity(self):
        return sum(invoice_item.quantity for invoice_item in self.invoice_items.all() if invoice_item.quantity)

    @property
    def gross_weight(self):
        invoice_items = self.invoice_items.filter(inventory__isnull=False).all()
        return sum(invoice_item.inventory.gross_weight * invoice_item.quantity for invoice_item in invoice_items if invoice_item.inventory.gross_weight)

    @property
    def net_weight(self):
        invoice_items = self.invoice_items.filter(inventory__isnull=False).all()
        return sum(invoice_item.inventory.weight * invoice_item.quantity for invoice_item in invoice_items if invoice_item.inventory.weight)

    @property
    def is_invoiced(self):
        return self.status == enums.ProformaInvoiceStatus.INVOICED


class ProformaInvoiceItem(SalesItem):
    invoice = models.ForeignKey(ProformaInvoice, related_name='invoice_items', on_delete=models.CASCADE)
    available_quantity = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return f"{self.invoice} {self.inventory} qty={self.quantity} @ {self.price}"

    def save(self, **kwargs):
        sub_total = self.quantity * self.price
        discount_amount = 0
        if self.discount and self.discount > 0:
            discount_amount = (float(self.discount) / 100) * float(sub_total)
        self.net_price = sub_total - Decimal(discount_amount)
        super(ProformaInvoiceItem, self).save(**kwargs)


class Payment(models.Model):

    PAYMENT = 0
    ALLOCATION = 1
    SUNDRY = 2

    PAYMENT_TYPES = (
        (PAYMENT, 'Payment'),
        (ALLOCATION, 'Allocation'),
        (SUNDRY, 'Sundry')
    )

    branch = models.ForeignKey('company.Branch', null=True, blank=True, related_name='branch_payments', on_delete=models.CASCADE)
    customer = models.ForeignKey('customer.Customer', null=True, blank=True, related_name='customer_invoice_payments', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', null=True, blank=True, related_name='period_sales_payments', on_delete=models.DO_NOTHING)
    payment_id = models.PositiveIntegerField(null=True, blank=True)
    reference = models.CharField(max_length=255, null=True, blank=True)
    role = models.ForeignKey('accounts.Role', related_name='sales_payments', on_delete=models.DO_NOTHING)
    profile = models.ForeignKey('accounts.Profile', related_name='sales_payments', on_delete=models.DO_NOTHING)
    date = models.DateField(null=True, blank=True, verbose_name='Payment Date')
    payment_method = models.ForeignKey('company.PaymentMethod', null=True, blank=True, related_name='sales_payments',
                                       on_delete=models.DO_NOTHING)
    total_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    discount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    open_amount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True)
    attachment = models.FileField(upload_to='uploads', null=True, blank=True)

    def __str__(self):
        return f"{self.payment_id}"


class InvoicePayment(models.Model):
    payment = models.ForeignKey(Payment, related_name='payments', on_delete=models.CASCADE)
    invoice = models.ForeignKey(Invoice, related_name='payments', on_delete=models.CASCADE)
    amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Amount')
    discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Discount')

    def total(self):
        amount = 0
        if self.amount:
            amount = self.amount
            if self.invoice.is_credit:
                amount = amount * -1
        return amount

    def __str__(self):
        return f"{self.payment}-{self.invoice}"


class EstimateManager(models.Manager):

    def get_queryset(self):
        return super(EstimateManager, self).get_queryset().filter(deleted__isnull=True)

    def active(self, branch):
        self.filter(branch=branch, expires_at__gte=now()).exclude(status=enums.EstimateStatus.COMPLETED)

    def with_totals(self):
        return self.annotate(
            quantity_total=models.Sum('estimate_items__quantity'),
            total_gross_weight=models.Sum(models.F('estimate_items__inventory__gross_weight') * models.F('estimate_items__quantity')),
            total_net_weight=models.Sum(models.F('estimate_items__inventory__weight') * models.F('estimate_items__quantity'))
        )

    def get_detail(self, estimate_id, **kwargs):
        estimate_item_query = EstimateItem.objects.select_related(
            'inventory', 'inventory__inventory', 'account', 'vat_code', 'inventory__history'
        ).filter()
        if 'is_picked_estimate_item' in kwargs:
            if kwargs['is_picked_estimate_item']:
                estimate_item_query = estimate_item_query.filter(status=enums.InvoiceItemStatus.PICKED)
            else:
                estimate_item_query = estimate_item_query.exclude(status=enums.InvoiceItemStatus.PICKED)
        if 'estimate_item_type' in kwargs:
            if kwargs['estimate_item_type'] == 'inventory':
                estimate_item_query = estimate_item_query.filter(item_type=enums.InvoiceItemType.INVENTORY)
            if kwargs['estimate_item_type'] == 'account':
                estimate_item_query = estimate_item_query.filter(item_type=enums.InvoiceItemType.GENERAL)
            if kwargs['estimate_item_type'] == 'text':
                estimate_item_query = estimate_item_query.filter(item_type=enums.InvoiceItemType.TEXT)

        return self.with_totals().select_related(
            'picking_slip', 'estimate_deliveries', 'period', 'salesman', 'customer', 'branch', 'branch__company'
        ).prefetch_related(
            models.Prefetch('estimate_items', queryset=estimate_item_query),
            'customer__addresses'
        ).get(
            pk=estimate_id
        )


class Estimate(SalesInvoice, CustomerInvoice):
    period = models.ForeignKey('period.Period', null=True, related_name='period_estimates', on_delete=models.CASCADE)
    estimate_id = models.CharField(max_length=255, verbose_name='Estimate Number')
    status = EnumIntegerField(enums.EstimateStatus, default=enums.EstimateStatus.OPEN)
    date = models.DateField(null=True, blank=True, verbose_name='Estimate Date')
    expires_at = models.DateTimeField(null=True, blank=True)
    is_deposit_required = models.BooleanField(default=False, verbose_name='Deposit Required')
    deposit = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Deposit(%)')
    proforma_invoice = models.IntegerField(blank=True, null=True)
    is_template = models.BooleanField(default=False, verbose_name='Template')
    template = models.ForeignKey('self', null=True, blank=True, related_name='invoices', on_delete=models.SET_NULL)

    objects = EstimateManager()
    not_deleted = managers.EstimateNotDeleteManager()

    class Meta:
        ordering = ['-created_at']
        unique_together = ('estimate_id', 'branch', 'deleted')

    def __str__(self):
        return f"EST{str(self.estimate_id).rjust(5, '0')}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.estimate_id:
            filter_options = {'branch': self.branch}
            counter = 1
            estimate = Estimate.objects.filter(**filter_options).order_by('-created_at').values('estimate_id').first()
            if estimate:
                counter = int(estimate['estimate_id']) + 1
            self.estimate_id = self.generate_estimate_number(counter=counter, filter_options=filter_options)
        return super(Estimate, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('estimate_detail', args=(self.pk, ))

    def generate_estimate_number(self, counter, filter_options):
        estimate = Estimate.not_deleted.filter(**filter_options).values('estimate_id').first()
        if estimate:
            counter = int(estimate['estimate_id']) + 1
            filter_options['estimate_id'] = counter
            return self.generate_estimate_number(counter, filter_options)
        return counter

    def mark_as_completed(self):
        self.status = enums.EstimateStatus.COMPLETED
        self.save()

    def mark_as_open(self):
        self.status = enums.EstimateStatus.OPEN
        self.save()

    def send_email(self):
        # TODO - Send email
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self),
            object_id=self.pk,
            comment=f'Estimate emailed',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'self',
                'reference': str(self)
            }
        )

    def copy_estimate_items(self, estimate_items, post):
        for estimate_item in estimate_items:
            quantity = float(post.get('quantity_{}'.format(estimate_item.id), 0))
            is_back_order = post.get('back_order_{}'.format(estimate_item.id), False)
            if is_back_order:
                back_order_qty = float(estimate_item.quantity) - quantity
            EstimateItem.objects.create(
                estimate=self,
                inventory=estimate_item.inventory,
                description=estimate_item.description,
                quantity=quantity,
                unit=estimate_item.unit,
                price=estimate_item.price,
                discount=estimate_item.discount,
                discount_amount=estimate_item.discount_amount,
                net_price=estimate_item.net_price,
                vat_code=estimate_item.vat_code,
                vat_amount=estimate_item.vat_amount
            )

    def valid_estimate_items(self, estimate_items):
        errors = []
        for estimate_item in estimate_items:
            if estimate_item.inventory:
                quantity_required = float(estimate_item.quantity)
                quantity_available = float(estimate_item.inventory.available_stock)
                if quantity_required > quantity_available:
                    errors.append("Quantity ({}) requested for {} is not available, do you want to create "
                                  "a back order? <br />".format(quantity_required, estimate_item.inventory))
        return errors

    def make_delivery_note(self, create_back_order=False):
        estimate_items = self.estimate_items.all()
        errors = self.valid_estimate_items(estimate_items)
        if errors:
            raise QuantityNotAvailableException('Deliveries could not be created.<br />{}'.format(''.join(errors)))

        delivery = Delivery(branch=self.branch,
                            total_amount=self.total_amount,
                            vat_amount=self.vat_amount,
                            net_amount=self.net_amount,
                            date=self.date,
                            period_id=self.period_id,
                            profile_id=self.profile_id,
                            role_id=self.role_id
                            )
        delivery.save()
        if delivery:
            self.copy_estimate_items_to_delivery_items(delivery, estimate_items)
        return delivery

    def copy_estimate_items_to_delivery_items(self, delivery, estimate_items):
        for estimate_item in estimate_items:
            DeliveryItem.objects.create(
                delivery=delivery,
                inventory=estimate_item.inventory,
                description=estimate_item.description,
                quantity=estimate_item.quantity,
                unit=estimate_item.unit,
                price=estimate_item.price,
                discount=estimate_item.discount,
                discount_amount=estimate_item.discount_amount,
                net_price=estimate_item.net_price,
                vat_code=estimate_item.vat_code,
                vat_amount=estimate_item.vat_amount
            )

    def copy_estimate_items_to_invoice_items(self, invoice, estimate_items):
        for estimate_item in estimate_items:
            InvoiceItem.objects.create(
                invoice=invoice,
                inventory=estimate_item.inventory,
                quantity=estimate_item.quantity,
                unit=estimate_item.unit,
                vat_code=estimate_item.vat_code,
                price=estimate_item.price,
                discount=estimate_item.discount,
                discount_amount=estimate_item.discount_amount,
                net_price=estimate_item.net_price,
                vat_amount=estimate_item.vat_amount,
                description=estimate_item.description,
            )

    def save_address(self, address, address_type):
        address_line_1 = address.get('street', None)
        if len(address) > 0 and address_line_1:
            address_object = {
                'street': address_line_1,
                'postal_code': address.get('postal_code', ''),
                'country': address.get('country', None),
                'province': address.get('province', None),
                'city': address.get('city', None)
            }
            if address_type == enums.AddressType.POSTAL:
                self.postal = address_object
            elif address_type == enums.AddressType.DELIVERY:
                self.delivery_to = address_object
            self.save()

    def execute_delete(self, profile, role_id=None):
        data = {'object_id': self.id,
                'content_type': ContentType.objects.get_for_model(self),
                'comment': 'Deleted',
                'profile': profile,
                'data': {
                        'tracking_type': 'self',
                        'reference': str(self)
                    }
                }
        if role_id:
            data['role_id'] = role_id
        Tracking.objects.create(**data)
        self.delete()

    def proforma_reference(self):
        if not self.proforma_invoice:
            return ''
        prefix = 'PINV'
        counter_str = str(self.estimate_id).rjust(4, '0')
        return f"{prefix}{counter_str}"

    @property
    def is_expired(self):
        return self.expires_at.date() < datetime.now().date()

    @property
    def status_name(self):
        return str(self.status.label)

    @property
    def is_partially_completed(self):
        return self.status == enums.EstimateStatus.PARTIALLY_COMPLETED

    @property
    def is_completed(self):
        return self.status == enums.EstimateStatus.COMPLETED

    @property
    def deposit_required(self):
        amount = 0
        if self.is_deposit_required and self.deposit:
            amount = round((float(self.deposit)/100) * float(self.net_total), 2)
        return Decimal(amount)

    @property
    def lines_total_vat_amount(self):
        total = 0
        for estimate_item in self.estimate_items.all():
            total += estimate_item.total_vat_amount
        return Decimal(round(total, 2))

    @property
    def calculate_sub_total(self):
        total = 0
        for estimate_item in self.estimate_items.all():
            total += estimate_item.discount_amount + estimate_item.total_price_excluding
        return Decimal(total)

    @property
    def total_quantity(self):
        return sum(estimate_item.quantity for estimate_item in self.estimate_items.all() if estimate_item.quantity)

    @property
    def gross_weight(self):
        estimate_items = self.estimate_items.filter(inventory__isnull=False).all()
        return sum(estimate_item.inventory.gross_weight * estimate_item.quantity for estimate_item in estimate_items if estimate_item.inventory.gross_weight)

    @property
    def net_weight(self):
        estimate_items = self.estimate_items.filter(inventory__isnull=False).all()
        return sum(estimate_item.inventory.weight * estimate_item.quantity for estimate_item in estimate_items if estimate_item.inventory.weight)

    @property
    def total_order_value(self):
        return self.customer_net_total + self.total_invoice_vat

    @property
    def customer_net_total(self):
        amount = Decimal(0)
        if self.sub_total:
            amount += self.sub_total
        if self.discount_amount:
            amount += self.discount_amount
        return amount

    @property
    def total_invoice_vat(self):
        amount = 0
        if self.customer and self.customer.company:
            default_vat = self.customer.company.company_vat_codes.filter(is_default=True).first()
            if default_vat and self.customer_net_total:
                amount = (float(default_vat.percentage)/100) * float(self.customer_net_total)
        return Decimal(amount)

    @property
    def has_delivery_address(self):
        if not self.is_pick_up:
            return self.delivery_to
        return False


class EstimateItem(SalesItem):
    estimate = models.ForeignKey(Estimate, related_name='estimate_items', on_delete=models.CASCADE)

    objects = managers.EstimateItemManager()

    class Meta:
        ordering = ['id']


class BackOrder(SalesInvoice, CustomerInvoice, Deliverable):

    PENDING = 0
    PROCESSED = 1

    STATUSES = (
        (PENDING, 'Not Invoiced'),
        (PROCESSED, 'Processed')
    )
    # TODO delivery, picking slip and estimate is a OneToOne relationship
    period = models.ForeignKey('period.Period', related_name='sales_back_orders', on_delete=models.CASCADE)
    estimate = models.ForeignKey(Estimate, null=True, blank=True, related_name='estimate_back_order', on_delete=models.CASCADE)
    delivery = models.ForeignKey('sales.Delivery', null=True, blank=True, related_name='delivery_back_order', on_delete=models.CASCADE)
    picking_slip = models.ForeignKey('sales.PickingSlip', null=True, blank=True, related_name='picking_slip_back_order', on_delete=models.CASCADE)
    parent = models.OneToOneField('self', null=True, blank=True, related_name='child_back_order', on_delete=models.CASCADE)
    purchase_order = models.CharField(max_length=255, null=True, blank=True, verbose_name='Purchase Order Reference')
    status = EnumIntegerField(enums.BackOrderStatus, default=enums.BackOrderStatus.PENDING)
    date = models.DateField(null=True, blank=True, verbose_name='Delivery Date')
    delivery_time = models.TimeField(null=True, blank=True, verbose_name='Delivery Time Requested')
    expires_at = models.DateTimeField(null=True, blank=True)
    is_pick_up = models.BooleanField(default=False, verbose_name='Pick Up')

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return f"BO{str(self.id).rjust(5, '0')}"

    def get_absolute_url(self):
        return reverse('backorder_detail', args=(self.pk,))

    def mark_as_completed(self):
        self.status = enums.BackOrderStatus.PROCESSED
        self.save()

    def mark_as_open(self):
        self.status = enums.BackOrderStatus.PENDING
        self.save()

    def mark_as_removed(self):
        self.status = enums.BackOrderStatus.REMOVED
        self.save()

    @property
    def inventory_status(self):
        count_available = 0
        back_order_items = self.back_order_items
        for back_order_item in back_order_items.all():
            if back_order_item.is_inventory_available:
                count_available += 1
        if count_available == back_order_items.count():
            return 1
        elif count_available > 0:
            return -1
        else:
            return 0

    @property
    def status_name(self):
        return str(self.status)

    @property
    def is_completed(self):
        return self.status == enums.BackOrderStatus.PROCESSED

    @property
    def is_removed(self):
        return self.status == enums.BackOrderStatus.REMOVED

    @property
    def can_manage(self):
        return self.status in [enums.BackOrderStatus.PENDING]

    @property
    def total_quantity(self):
        return sum([float(back_order_item.quantity) for back_order_item in self.back_order_items.all() if back_order_item.inventory])

    @property
    def gross_weight(self):
        return sum([float(back_order_item.inventory.gross_weight) * float(back_order_item.quantity) for back_order_item in self.back_order_items.all() if back_order_item.inventory and back_order_item.inventory.gross_weight])

    @property
    def net_weight(self):
        return sum([float(back_order_item.inventory.weight) * float(back_order_item.quantity) for back_order_item in self.back_order_items.all() if back_order_item.inventory and back_order_item.inventory.weight])


class BackOrderItem(SalesItem):

    back_order = models.ForeignKey(BackOrder, related_name='back_order_items', on_delete=models.CASCADE)
    received = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=4, verbose_name='Received')
    back_order_item = models.OneToOneField('self', null=True, blank=True, related_name='back_ordered_item',
                                           on_delete=models.CASCADE)

    class Meta:
        get_latest_by = '-id'


class Delivery(SalesInvoice, CustomerInvoice, Deliverable):

    NOT_INVOICED = 0
    AWAITING_PICKING = 1
    PICKED = 2
    AWAITING_DELIVERY = 3
    DELIVERY_READY = 4
    DELIVERED = 5
    OUT_FOR_DELIVERY = 6
    INVOICED = 7
    BACK_ORDER = 8

    STATUSES = (
        (NOT_INVOICED, 'Not Invoiced'),
        (AWAITING_PICKING, 'Waiting to be picked'),
        (PICKED, 'Picked'),
        (AWAITING_DELIVERY, 'Awaiting Delivery'),
        (DELIVERY_READY, 'Ready for delivery'),
        (OUT_FOR_DELIVERY, 'Out for delivery'),
        (DELIVERED, 'Delivered'),
        (INVOICED, 'Invoiced'),
        (BACK_ORDER, 'Back Order'),
        (BACK_ORDER, 'Back Order'),
    )

    DELIVERY = 0
    COLLECTION = 1

    CATEGORIES = (
        (DELIVERY, 'Delivery'),
        (COLLECTION, 'Collection')
    )

    period = models.ForeignKey('period.Period', related_name='sales_deliveries', on_delete=models.CASCADE)
    estimate = models.OneToOneField(Estimate, null=True, blank=True, related_name='estimate_deliveries', on_delete=models.CASCADE)
    backorder = models.OneToOneField(BackOrder, null=True, blank=True, related_name='+', on_delete=models.CASCADE)
    picking_slip = models.OneToOneField('sales.PickingSlip', null=True, blank=True, related_name='delivery', on_delete=models.CASCADE)
    parent = models.OneToOneField('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    delivery_id = models.CharField(max_length=255, verbose_name='Delivery Number')
    category = EnumIntegerField(enums.DeliveryCategory, default=enums.DeliveryCategory.DELIVERY)
    status = EnumIntegerField(enums.DeliveryStatus, default=enums.DeliveryStatus.NOT_INVOICED)
    expires_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        if self.category == Delivery.COLLECTION:
            return f"COL{str(self.delivery_id).rjust(5, '0')}"
        return f"DEL{str(self.delivery_id).rjust(5, '0')}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.delivery_id:
            filter_options = {'branch': self.branch, 'category': self.category}
            last_delivery = Delivery.objects.filter(**filter_options).order_by('created_at').last()
            counter = 1
            if last_delivery:
                counter = int(last_delivery.delivery_id) + 1
            self.delivery_id = self.generate_delivery_number(counter=counter, filter_options=filter_options)
        return super(Delivery, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('delivery_detail', args=(self.pk,))

    def generate_delivery_number(self, counter, filter_options):
        filter_options['delivery_id'] = counter
        delivery = Delivery.objects.filter(**filter_options)
        if delivery:
            counter = counter + 1
            return self.generate_delivery_number(counter=counter, filter_options=filter_options)
        return counter

    def mark_as_invoiced(self):
        self.status = enums.DeliveryStatus.INVOICED
        self.save()

    def delete(self, force_policy=None, **kwargs):
        for delivery_item in self.delivery_items.all():
            delivery_item.update_on_delivery()
        logger.info("Complete delete")
        super().delete(force_policy, **kwargs)

    def make_copy(self):
        _delivery = self
        _delivery.pk = None
        _delivery.save()

        if _delivery:
            self.copy_estimate_items_to_delivery_items(_delivery)
        return _delivery

    def copy_estimate_items_to_delivery_items(self, delivery):
        for delivery_item in self.delivery_items.all():
            DeliveryItem.objects.create(
                delivery=delivery,
                invoice_item_type=enums.InvoiceItemType.INVENTORY,
                inventory=delivery_item.inventory,
                description=delivery_item.description,
                quantity=delivery_item.quantity,
                unit=delivery_item.unit,
                price=delivery_item.price,
                discount=delivery_item.discount,
                net_price=delivery_item.net_price,
                vat_code=delivery_item.vat_code,
                vat=delivery_item.vat
            )

    def copy_delivery_items_to_invoice_items(self, invoice):
        for delivery_item in self.delivery_items.all():
            InvoiceItem.objects.create(
                invoice=invoice,
                invoice_item_type=enums.InvoiceItemType.INVENTORY,
                inventory=delivery_item.inventory,
                description=delivery_item.description,
                quantity=delivery_item.quantity,
                unit=delivery_item.unit,
                price=delivery_item.price,
                discount=delivery_item.discount,
                vat=delivery_item.vat,
                net_price=delivery_item.net_price,
                vat_code=delivery_item.vat_code
            )

    def create_delivery_item_from_estimate_item(self, invoice_item, quantity):
        vat_amount = 0
        discount_amount = 0
        discount = float(invoice_item.discount)
        amount = float(invoice_item.price)
        total = amount * quantity
        net_amount = amount * quantity
        if invoice_item.vat_code:
            vat_amount = (float(invoice_item.vat_code.percentage / 100) * amount) * quantity
        if discount > 0:
            discount_amount = (float(discount / 100) * amount)
        DeliveryItem.objects.create(
            delivery=self,
            inventory=invoice_item.inventory,
            description=invoice_item.description,
            quantity=quantity,
            received=quantity,
            unit=invoice_item.unit,
            price=amount,
            discount=discount,
            vat=invoice_item.vat,
            net_price=net_amount,
            vat_code=invoice_item.vat_code
        )
        inventory = BranchInventory.objects.filter(
            id=invoice_item.inventory_id
        ).first()
        inventory_history = inventory.history

        if not inventory_history:
             PurchaseHistory.objects.create(
                inventory=invoice_item.inventory,
                on_delivery=Decimal(quantity)
            )
        else:
            if inventory_history.on_delivery:
                inventory_history.on_delivery += Decimal(quantity)
            else:
                inventory_history.on_delivery = Decimal(quantity)
            inventory_history.save()

        return vat_amount, net_amount, total, discount_amount

    @property
    def display_prices(self):
        if self.customer and self.customer.is_price_on_delivery:
            return True
        return False

    @property
    def status_name(self):
        return str(self.status)

    @property
    def category_name(self):
        return str(self.category)

    def calculate_vat_on_customer_discount(self, customer_discount):
        amount = 0
        if self.customer.company:
            default_vat = self.customer.company.company_vat_codes.filter(is_default=True).first()
            if default_vat and customer_discount:
                amount = (float(default_vat.percentage)/100) * float(customer_discount)
        return amount

    @property
    def calc_vat_on_customer_discount(self):
        amount = 0
        if self.customer and self.customer.company:
            default_vat = self.customer.company.company_vat_codes.filter(is_default=True).first()
            if default_vat and self.customer_discount:
                amount = (float(default_vat.percentage)/100) * float(self.customer_discount)
        return amount

    @property
    def total_customer_invoice_vat(self):
        total = 0
        if self.vat_on_customer_discount:
            total += float(self.vat_on_customer_discount)
        if self.vat_amount:
            total += float(self.vat_amount)
        return total

    @property
    def total_order_value(self):
        return self.net_total + self.total_customer_invoice_vat

    @property
    def total_quantity(self):
        return sum([float(delivery_item.quantity) for delivery_item in self.delivery_items.all() if delivery_item.inventory])

    @property
    def gross_weight(self):
        return sum([float(delivery_item.inventory.gross_weight) * float(delivery_item.quantity) for delivery_item in self.delivery_items.all() if delivery_item.inventory and delivery_item.inventory.gross_weight])

    @property
    def net_weight(self):
        return sum([float(delivery_item.inventory.weight) * float(delivery_item.quantity) for delivery_item in self.delivery_items.all() if delivery_item.inventory and delivery_item.inventory.weight])

    @property
    def is_removed(self):
        return self.status == enums.DeliveryStatus.REMOVED

    @property
    def is_back_order(self):
        return self.status == enums.DeliveryStatus.BACK_ORDER

    @property
    def is_invoiced(self):
        return self.status in [enums.DeliveryStatus.INVOICED, enums.DeliveryStatus.DELIVERED]

    @property
    def can_manage(self):
        return self.status not in [enums.DeliveryStatus.REMOVED, enums.DeliveryStatus.INVOICED]

    @property
    def can_invoice(self):
        if self.picking_slip:
            invoice = getattr(self.picking_slip, 'invoice', None)
            return not invoice
        return True

    def save_address(self, address, address_type):
        address_line_1 = address.get('street', None)
        if len(address) > 0 and address_line_1:
            address_object = DeliveryAddress.objects.filter(address_type=address_type, delivery=self).first()
            if not address_object:
                address_object = DeliveryAddress()
                address_object.delivery = self

            address_object.address_type = address_type
            address_object.postal_code = address.get('postal_code', '')
            address_object.country = address.get('country', None)
            address_object.province = address.get('province', None)
            address_object.city = address.get('city', None)
            address_object.address_line_1 = address_line_1
            address_object.save()

    def copy_address(self, address_source, address_type):
        logger.info("Copy address from source to delivery ")
        address_object = DeliveryAddress()
        address_object.delivery = self
        address_object.address_type = address_type
        address_object.postal_code = address_source.postal_code
        address_object.country = address_source.country
        address_object.province = address_source.province
        address_object.city = address_source.city
        address_object.address_line_1 = address_source.address_line_1
        address_object.save()
        logger.info("Done copying address from source to picking slip")


class DeliveryAddress(Address):

    delivery = models.ForeignKey(Delivery, related_name='addresses', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.address_line_1}"


class DeliveryItem(SalesItem):
    delivery = models.ForeignKey(Delivery, related_name='delivery_items', on_delete=models.CASCADE)
    ordered = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Ordered')
    received = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Received')
    back_order_item = models.OneToOneField(BackOrderItem, null=True, blank=True, related_name='delivery_item', on_delete=models.CASCADE)

    class Meta:
        get_latest_by = '-id'

    def update_on_delivery(self, using=None, keep_parents=False):
        try:
            inventory_history = PurchaseHistory.objects.get(inventory_id=self.inventory_id)
            if inventory_history.on_delivery and self.quantity:
                inventory_history.on_delivery -= self.quantity
            inventory_history.save()
            self.reverse_back_order()
        except PurchaseHistory.DoesNotExist:
            logger.exception(f"Inventory {self.inventory} does not have history")

    def reverse_back_order(self):
        if self.back_order_item and self.back_order_quantity > 0:
            self.back_order_item.quantity -= self.back_order_quantity
            self.back_order_item.save()

    @property
    def back_order_quantity(self):
        return self.ordered - self.quantity


class PickingSlip(SalesInvoice, CustomerInvoice, Deliverable):

    IN_PROGRESS = 0
    COMPLETED = 1
    SEND_FOR_PICKING = 2

    STATUSES = (
        (IN_PROGRESS, 'In Progress'),
        (COMPLETED, 'Picked'),
        (SEND_FOR_PICKING, 'Being Picked')
    )
    notes = models.TextField(null=True, blank=True)
    period = models.ForeignKey('period.Period', related_name='picking_period', on_delete=models.CASCADE)
    estimate = models.OneToOneField('Estimate', null=True, blank=True, related_name='picking_slip', on_delete=models.CASCADE)
    back_order = models.OneToOneField('BackOrder', null=True, blank=True, related_name='+', on_delete=models.CASCADE)
    picking_id = models.DecimalField(max_digits=10, decimal_places=1, verbose_name='Picking Number')
    status = EnumIntegerField(enums.PickingSlipStatus, default=enums.PickingSlipStatus.IN_PROGRESS)
    date = models.DateField(null=True, blank=True, verbose_name='Picking Date')
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    picked_by = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name="picked_slips", on_delete=models.DO_NOTHING, verbose_name='Picked By')
    profile = models.ForeignKey('accounts.Profile', null=True, blank=True, related_name="%(app_label)s_%(class)s_related",
                                related_query_name="%(app_label)s_%(class)ss", on_delete=models.DO_NOTHING, verbose_name='Created By')

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        picking_number_list = str(self.picking_id).split(".")
        reference = picking_number_list[0]
        padding = settings.DEFAULT_PADDING
        if len(picking_number_list) > 1:
            decimal_part = int(picking_number_list[1])
            if decimal_part > 0:
                reference = f"{reference}-{decimal_part}"
                padding += 2
        return create_document_number(None, "PS", reference, padding)

    def save(self, *args, **kwargs):
        logger.info("Saving picking slip {} -> Picking id {}, pk => {}".format(self.branch, self.picking_id, self.pk))
        if not self.pk and not self.picking_id:
            filter_options = {'branch': self.branch}
            logger.info("filter_options --> {}".format(filter_options))
            self.picking_id = self.generate_picking_number(1, filter_options)
            logger.info("Picking id generated --> {}".format(self.picking_id))
        return super(PickingSlip, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('picking_detail', args=(self.pk,))

    def generate_picking_number(self, counter, filter_options):
        logger.info("Saving ---- generate_picking_number slip")
        picking_id = 0
        try:
            picking_slip = PickingSlip.objects.filter(**filter_options).latest('created_at')
            try:
                picking_id = int(picking_slip.picking_id)
            except:
                picking_id = 0
        except:
            return counter
        return picking_id + 1

    def mark_as_send_for_picking(self):
        self.status = enums.PickingSlipStatus.SEND_FOR_PICKING
        self.save()

    def mark_as_open(self):
        self.status = enums.PickingSlipStatus.IN_PROGRESS
        self.save()

    def mark_as_removed(self):
        self.status = enums.PickingSlipStatus.REMOVED
        self.save()

    def save_address(self, address, address_type):
        address_object = PickingSlipAddress.objects.filter(address_type=address_type, picking_slip=self).first()
        if not address_object:
            address_object = PickingSlipAddress()
            address_object.picking_slip = self

        address_object.address_type = address_type
        address_object.postal_code = address.get('postal_code', '')
        address_object.country = address.get('country', None)
        address_object.province = address.get('province', None)
        address_object.city = address.get('city', None)
        address_object.address_line_1 = address.get('street', None)
        address_object.save()

    def copy_address(self, address_source, address_type):
        logger.info("Copy address from source to picking slip")
        address_object = PickingSlipAddress()
        address_object.picking_slip = self
        address_object.address_type = address_type
        address_object.postal_code = address_source.postal_code
        address_object.country = address_source.country
        address_object.province = address_source.province
        address_object.city = address_source.city
        address_object.address_line_1 = address_source.address_line_1
        address_object.save()
        logger.info("Done copying address from source to picking slip")

    @property
    def status_name(self):
        return str(self.status)

    @property
    def is_completed(self):
        return self.status == enums.PickingSlipStatus.COMPLETED

    @property
    def is_send_for_picking(self):
        return self.status == enums.PickingSlipStatus.SEND_FOR_PICKING

    @property
    def is_in_progress(self):
        return self.status == enums.PickingSlipStatus.IN_PROGRESS

    @property
    def is_removed(self):
        return self.status == enums.PickingSlipStatus.REMOVED

    @property
    def can_manage(self):
        return self.status not in [enums.PickingSlipStatus.REMOVED, enums.PickingSlipStatus.COMPLETED]

    @property
    def total_received_quantity(self):
        return sum([float(picking_slip_item.received) for picking_slip_item in self.picking_slip_items.all() if picking_slip_item.received])

    @property
    def total_quantity(self):
        return sum([float(picking_slip_item.quantity) for picking_slip_item in self.picking_slip_items.all() if picking_slip_item.inventory])

    @property
    def gross_weight(self):
        return sum([picking_slip_item.gross_weight for picking_slip_item in self.picking_slip_items.all()])

    @property
    def net_weight(self):
        return sum([picking_slip_item.net_weight for picking_slip_item in self.picking_slip_items.all()])


class PickingSlipAddress(Address):

    picking_slip = models.ForeignKey(PickingSlip, related_name='addresses', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.address_line_1}"


class PickingSlipItem(SalesItem):

    picking_slip = models.ForeignKey(PickingSlip, related_name='picking_slip_items', on_delete=models.CASCADE)
    received = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Received')
    picked = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Picked')
    back_order_item = models.OneToOneField(BackOrderItem, null=True, blank=True, related_name='picking_slip_item',
                                           on_delete=models.CASCADE)

    class QS(models.QuerySet):

        def inventory(self):
            return self.select_related(
                'inventory', 'inventory__inventory', 'vat_code', 'inventory__history'
            ).filter(item_type=enums.InvoiceItemType.INVENTORY, inventory__isnull=False)

    objects = QS.as_manager()


class ReceiptManager(models.Manager):

    # def with_allocation_total(self):
    #     return self.annotate(
    #         allocation_total=models.Sum('accounts__amount') + models.F('amount') * -1,
    #         total_vat=models.Sum(models.F('accounts__vat_amount')),
    #         total_discounts=models.Sum('invoices__discount'),
    #         total_original=models.Sum('invoices__invoice__total_amount'),
    #         total_amounts=models.Sum('invoices__amount')
    #     )

    def open(self):
        return self.exclude(balance=0).exclude(balance__isnull=True)

    def customer_open_total(self, period, is_last=False):
        receipt_query = Receipt.objects.filter(customer_id=models.OuterRef('id')).order_by()
        if not period:
            return receipt_query.none()
        if is_last:
            receipt_query = receipt_query.filter(period_id__lte=period.id)
        else:
            receipt_query = receipt_query.filter(period=period)
        return receipt_query.values_list(models.Func('balance', function='SUM'))

    def pending_cash_up(self, branch=None, company=None, start_date=None, end_date=None, period=None, year=None):
        qs = self.prefetch_related(
            'payments', 'accounts', 'invoices'
        ).select_related(
            'content_type', 'customer', 'payment_method', 'period', 'period__period_year'
        ).filter(
            cash_up__isnull=True
        )
        if start_date:
            qs = qs.filter(date__gte=start_date)
        if end_date:
            qs = qs.filter(date__lte=end_date)
        if period:
            qs = qs.filter(period=period)
        if year:
            qs = qs.filter(period__period_year__year__lte=year.year)
        if not (branch or company):
            return self.none()
        if company:
            qs = qs.filter(branch__company=company)
        if branch:
            qs = qs.filter(branch=branch)
        return qs


class Receipt(models.Model):
    branch = models.ForeignKey('company.Branch', related_name='branch_receipts', on_delete=models.DO_NOTHING)
    customer = models.ForeignKey('customer.Customer', related_name='receipts', on_delete=models.DO_NOTHING)
    period = models.ForeignKey('period.Period', related_name='receipts', on_delete=models.DO_NOTHING)
    payment_method = models.ForeignKey('company.PaymentMethod', null=True, blank=True, related_name='receipts', on_delete=models.DO_NOTHING)
    date = models.DateField(verbose_name='Date')
    receipt_id = models.PositiveIntegerField(null=True, blank=True)
    reference = models.CharField(max_length=255, null=True, blank=True)
    content_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(null=True, blank=True,)
    content_object = GenericForeignKey('content_type', 'object_id')
    cash_up = models.ForeignKey('CashUp', related_name='receipts', blank=True, null=True, on_delete=models.DO_NOTHING)
    journal = models.ForeignKey('journals.Journal', null=True, blank=True, related_name='journal_receipts', on_delete=models.DO_NOTHING)
    amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Amount')
    balance = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Balance')
    change = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Change')
    discount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Change')
    notes = models.TextField(null=True, blank=True, verbose_name='Notes')
    receipt_type = EnumIntegerField(enums.ReceiptType, default=enums.ReceiptType.PAYMENT)
    attachment = models.FileField(null=True, blank=True, upload_to='uploads/sales/payments')
    created_by = models.ForeignKey('accounts.Profile', null=True, related_name='receipts', on_delete=models.SET_NULL)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = ReceiptManager()

    class Meta:
        ordering = ('-created_at', )

    def __str__(self):
        counter_str = str(self.receipt_id).rjust(4, '0')
        return f"REC{counter_str}"

    def save(self, *args, **kwargs):
        if not self.pk or not self.receipt_id:
            filter_options = {'branch': self.branch}
            self.receipt_id = self.generate_receipt_number(1, filter_options)
        return super(Receipt, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('receipt_detail', args=(self.pk,))

    def generate_receipt_number(self, counter, filter_options):
        receipt_id = 0
        try:
            receipt = Receipt.objects.filter(**filter_options).first()
            try:
                receipt_id = int(receipt.receipt_id)
            except:
                receipt_id = 0
        except:
            return counter
        return receipt_id + 1

    def calculate_balance(self):
        total = self.sundry_receipts.aggregate(total=models.Sum('amount'))
        allocated = total['total'] if total and total['total'] else 0
        balance = self.total_amount - allocated
        return balance

    @property
    def available_amount(self):
        if self.balance and self.balance > 0:
            return self.balance * -1
        return Decimal(0)

    @property
    def is_open(self):
        return not self.cash_up

    @property
    def open_amount(self):
        return self.balance * -1 if self.balance else 0

    @cached_property
    def invoices_total(self):
        return sum(invoice_receipt.invoice.total_amount for invoice_receipt in self.invoices.all())

    @cached_property
    def allocation_total(self):
        return sum(allocation.allocated for allocation in self.allocations.all())

    @cached_property
    def total_invoices_original(self):
        return self.invoices_total + self.allocation_total if self.allocation_total else 0

    @cached_property
    def total_invoices_open(self):
        return sum(invoice_receipt.invoice.open_amount for invoice_receipt in self.invoices.all())

    @cached_property
    def total_invoices(self):
        return sum(invoice_receipt.amount for invoice_receipt in self.invoices.all())

    @cached_property
    def total_journals(self):
        return sum(ledger_receipt.amount for ledger_receipt in self.customer_ledgers.all())

    @cached_property
    def total_allocation(self):
        return sum([self.total_journals if self.total_journals else 0, self.total_invoices if self.total_invoices else 0])

    @cached_property
    def total_payments(self):
        return sum(payment.amount for payment in self.payments.all())

    @cached_property
    def total_amount(self):
        return self.amount * -1 if self.amount else 0

    @cached_property
    def total_accounts_vat(self):
        return sum(receipt_account.vat_amount for receipt_account in self.accounts.all())

    @cached_property
    def total_vat(self):
        return self.total_accounts_vat + self.total_discounts_vat

    @cached_property
    def total_account(self):
        return sum(receipt_account.amount for receipt_account in self.accounts.all())

    @cached_property
    def total_discounts(self):
        return sum(invoice_receipt.discount for invoice_receipt in self.invoices.all())

    @property
    def total_discounts_vat(self):
        vat_code = VatCode.objects.input().filter(company=self.branch.company, is_default=True).first()
        if not vat_code:
            raise ValueError("Default input vat not found ")
        return Decimal(vat_code.percentage/(vat_code.percentage + 100)) * self.total_discounts

    @cached_property
    def total_allocated(self):
        logger.info(f"Total from accounts {self.total_account}")
        logger.info(f"Total from payments {self.total_payments}")
        logger.info(f"Total from journals {self.total_journals}")
        logger.info(f"Total from discounts {self.total_discounts}")
        return sum([self.total_account if self.total_account else 0,
                    self.change if self.payment_method and self.payment_method.has_change else 0,
                    self.total_payments if self.total_payments else 0,
                    self.total_journals if self.total_journals else 0,
                    self.total_discounts if self.total_discounts else 0])


class InvoiceReceipt(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='receipts', on_delete=models.CASCADE)
    receipt = models.ForeignKey(Receipt, related_name='invoices', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    discount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)


class SundryReceiptAllocation(models.Model):
    parent = models.ForeignKey(Receipt, related_name='allocations', on_delete=models.CASCADE)
    receipt = models.ForeignKey(Receipt, related_name='sundry_receipts', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    discount = models.DecimalField(default=0, null=True, blank=True, decimal_places=2, max_digits=12)

    @property
    def allocated(self):
        return self.amount * -1


class ReceiptPayment(models.Model):
    receipt = models.ForeignKey(Receipt, related_name='payments', on_delete=models.CASCADE)
    payment_method = models.ForeignKey('company.PaymentMethod', null=True, blank=True, related_name='receipt_payments', on_delete=models.DO_NOTHING)
    amount = models.DecimalField(null=True, max_digits=15, decimal_places=2, verbose_name=__('Amount'))


class ReceiptAccount(models.Model):
    receipt = models.ForeignKey(Receipt, related_name='accounts', on_delete=models.CASCADE)
    account = models.ForeignKey('company.Account', null=True, related_name='receipts', on_delete=models.DO_NOTHING)
    vat_code = models.ForeignKey('company.VatCode', null=True, related_name='receipts', on_delete=models.DO_NOTHING)
    vat_amount = models.DecimalField(null=True, max_digits=15, decimal_places=2, verbose_name=__('Vat Amount'))
    amount = models.DecimalField(null=True, max_digits=15, decimal_places=2, verbose_name=__('Amount'))


class CashUpQuerySet(models.QuerySet):

    def for_year(self, year):
        return self.filter(period__period_year=year)

    def for_branch(self, branch):
        return self.filter(branch=branch)


class CashUp(Timestampable):
    branch = models.ForeignKey('company.Branch', related_name='cashups', on_delete=models.CASCADE)
    period = models.ForeignKey('period.Period', related_name='period_cashups', on_delete=models.CASCADE)
    cashup_id = models.CharField(max_length=255, verbose_name='Cash Up Reference')
    date = models.DateField(null=True, blank=True, verbose_name='Cash Up Date')
    total_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Deposit')
    profile = models.ForeignKey('accounts.Profile', related_name='cashups', on_delete=models.DO_NOTHING, verbose_name='Created By')
    role = models.ForeignKey('accounts.Role', related_name='cashups', on_delete=models.DO_NOTHING, verbose_name='Created')
    journal = models.ForeignKey('journals.Journal', null=True, on_delete=models.DO_NOTHING)

    objects = CashUpQuerySet.as_manager()

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return "CUP{}".format(str(self.cashup_id).rjust(4, '0'))

    # noinspection PyMethodMayBeStatic
    def get_absolute_url(self):
        return reverse('cash_up_detail', args=(self.pk, ))

    def save(self, *args, **kwargs):
        if not self.pk or not self.cashup_id:
            filter_options = {'branch': self.branch}
            self.cashup_id = self.generate_cashup_reference(1, filter_options)
        return super(CashUp, self).save(*args, **kwargs)

    def generate_cashup_reference(self, counter, filter_options):
        filter_options['cashup_id'] = counter
        cashup = CashUp.objects.filter(**filter_options)
        if cashup:
            counter = counter + 1
            return self.generate_cashup_reference(counter, filter_options)
        return counter


class CashAccount(models.Model):

    account = models.ForeignKey('company.Account', related_name='cash_ups', on_delete=models.DO_NOTHING)
    cash_up = models.ForeignKey(CashUp, related_name='cash_accounts', on_delete=models.CASCADE)
    total_amount = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=2, verbose_name='Deposit')


class UpdateManager(models.Manager):

    def invoice_updates(self, company, year):
        return self.filter(update_type=Update.INVOICE, branch__company=company, period__period_year=year)


class Update(models.Model):

    INVOICE = 0
    PAYMENT = 1

    TYPES = (
        (INVOICE, 'Invoice'),
        (PAYMENT, 'Payment')
    )

    branch = models.ForeignKey('company.Branch', related_name='sales_updates', on_delete=models.DO_NOTHING)
    period = models.ForeignKey('period.Period', related_name='sales_updates', on_delete=models.DO_NOTHING)
    journal = models.ForeignKey('journals.Journal', null=True, blank=True, related_name='sales_updates', on_delete=models.DO_NOTHING)
    update_id = models.PositiveIntegerField()
    date = models.DateTimeField()
    role = models.ForeignKey('accounts.Role', related_name='sales_updates', on_delete=models.DO_NOTHING)
    profile = models.ForeignKey('accounts.Profile', related_name='sales_updates', on_delete=models.DO_NOTHING)
    update_type = models.PositiveSmallIntegerField(default=INVOICE, choices=TYPES)
    created_at = models.DateTimeField(auto_created=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = UpdateManager()

    class Meta:
        ordering = ('-id', )

    def __str__(self):
        return f"Update Report {str(self.update_id).rjust(4, '0')}"

    def save(self, *args, **kwargs):
        self.created_at = datetime.now()
        if not self.pk or not self.update_id:
            filter_options = {'branch': self.branch, 'update_type': self.update_type}
            self.update_id = self.generate_update_reference(1, filter_options)
        return super(Update, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('sales_update_detail', args=(self.pk, ))

    def generate_update_reference(self, counter, filter_options):
        try:
            update = Update.objects.filter(**filter_options).latest('created_at')
            try:
                update_ref = int(update.update_id)
            except:
                update_ref = 0
            reference = update_ref + 1
            return reference
        except:
            return counter


class InvoiceUpdate(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='invoices', on_delete=models.CASCADE)
    update = models.ForeignKey(Update, related_name='updates', on_delete=models.CASCADE)


class Salesman(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    company = models.ForeignKey('company.Company', related_name='salesmans', on_delete=models.CASCADE)
    code = models.CharField(max_length=255, null=False, blank=False)
    name = models.CharField(max_length=255, null=False, blank=False)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return f"{self.name}"
