from enumfields import IntEnum, Enum
from django.utils.translation import gettext as __


class EstimateStatus(IntEnum):
    OPEN = 0
    INVOICED = 1
    COMPLETED = 2
    PARTIALLY_COMPLETED = 3

    class Labels:
        OPEN = __('Open')
        INVOICED = __('Invoiced')
        COMPLETED = __('Converted')
        PARTIALLY_COMPLETED = __('Partially completed')


class InvoiceStatus(IntEnum):
    PENDING = 0
    UPDATED = 1
    DRAFT = 2
    PARTIALLY_PAID = 3
    PAID_NOT_PRINTED = 4
    COMPLETED = 5

    class Labels:
        PENDING = __('Pending')
        UPDATED = __('Updated')
        DRAFT = __('Draft')
        PARTIALLY_PAID = __('Paid partially')
        PAID_NOT_PRINTED = __('Paid not printed')


class InvoiceCategory(IntEnum):
    CASH_SALE = 0
    CUSTOMER_SALE = 1

    class Labels:
        CASH_SALE = __('Cash sale tax invoice')
        CUSTOMER_SALE = __('Cash sale tax credit')


class InvoiceItemStatus(IntEnum):
    PENDING = 0
    PICKED = 1
    INVOICED = 2
    SEND_FOR_PICKING = 3

    class Labels:
        PENDING = __('Pending')
        PICKED = __('Picked')
        INVOICED = __('Invoiced')
        SEND_FOR_PICKING = __('Send for picking')


class InvoiceItemType(IntEnum):
    INVENTORY = 0
    TEXT = 1
    GENERAL = 2

    class Labels:
        INVENTORY = 'I'
        TEXT = 'T'
        GENERAL = 'G'


class AddressType(IntEnum):
    DELIVERY = 0
    POSTAL = 2

    class Labels:
        DELIVERY = __('Delivery')
        POSTAL = __('Postal')


class PaymentType(IntEnum):
    PAYMENT = 0
    ALLOCATION = 1
    SUNDRY = 2

    class Labels:
        PAYMENT = __('Payment')
        ALLOCATION = __('Allocation')
        SUNDRY = __('Sundry')


class BackOrderStatus(IntEnum):
    PENDING = 0
    PROCESSED = 1
    REMOVED = 2

    class Labels:
        PENDING = __('Not available')


class DeliveryStatus(IntEnum):
    NOT_INVOICED = 0
    AWAITING_PICKING = 1
    PICKED = 2
    AWAITING_DELIVERY = 3
    DELIVERY_READY = 4
    DELIVERED = 5
    OUT_FOR_DELIVERY = 6
    INVOICED = 7
    BACK_ORDER = 8
    REMOVED = 9

    class Labels:
        NOT_INVOICED = __('Not Invoiced')
        AWAITING_PICKING = __('Waiting to be picked')
        PICKED = __('Picked')
        AWAITING_DELIVERY = __('Awaiting Delivery')
        DELIVERY_READY = __('Ready for delivery')
        OUT_FOR_DELIVERY = __('Out for delivery')
        DELIVERED = __('Completed')
        INVOICED = __('Invoiced')
        BACK_ORDER = __('Back Order')


class DeliveryCategory(IntEnum):
    DELIVERY = 0
    COLLECTION = 1


class PickingSlipStatus(IntEnum):
    IN_PROGRESS = 0
    COMPLETED = 1
    SEND_FOR_PICKING = 2
    REMOVED = 3

    class Labels:
        IN_PROGRESS = __('In Progress')
        COMPLETED = __('Completed')
        SEND_FOR_PICKING = __('Send for Picking')


class ReceiptType(IntEnum):
    PAYMENT = 1
    SUNDRY = 2
    ALLOCATION = 3

    class Labels:
        PAYMENT = __('Payment')
        SUNDRY = __('Sundry')


class InvoiceType(Enum):
    TAX_INVOICE = 'tax-invoice'
    CREDIT_INVOICE = 'tax-credit-note'

    class Labels:
        TAX_INVOICE = 'Invoice'
        CREDIT_INVOICE = 'Credit Note'

    @classmethod
    def get_by_value(cls, val):
        return next(it for it in cls if it.value == val)


class TrackingType(Enum):
    COMMENT = 1
    ACTION = 2


class CommentAction(IntEnum):
    CREATED = 0
    UPDATED = 1
    DELETED = 2


class ProformaInvoiceStatus(IntEnum):
    OPEN = 0
    INVOICED = 1

