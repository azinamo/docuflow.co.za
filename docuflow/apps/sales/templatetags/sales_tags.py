import pprint
from collections import defaultdict
from functools import wraps

from django import template
from django.conf import settings

from docuflow.apps.company.models import Company, ObjectItem
from docuflow.apps.common.enums import Module
from docuflow.apps.accounts.models import Role


register = template.Library()
pp = pprint.PrettyPrinter(indent=4)


def get_invoice_linked_objects(invoice):
    account_object_links = {}
    if invoice:
        for invoice_object_item in invoice.object_items.all():
            if invoice_object_item.company_object.id in account_object_links:
                account_object_links[invoice_object_item.company_object.id]['value'] = invoice_object_item.id
            else:
                account_object_links[invoice_object_item.company_object.id] = {'value': invoice_object_item.id}
    return account_object_links


def get_role_object_links(role_id):
    role_object_links = defaultdict(list)
    if role_id and type(role_id) == int:
        role = Role.objects.prefetch_related('object_items').filter(pk=role_id).first()
        if role:
            for object_item in role.object_items.all():
                role_object_links[object_item.company_object_id].append(object_item)
    return role_object_links
    

@register.inclusion_tag('sales/linked_objects.html')
def invoice_linked_objects(company_id, role_id=None, invoice=None, form_style='horizontal'):

    company = Company.objects.get(pk=company_id)
    object_categories = {}
    invoice_object_links = get_invoice_linked_objects(invoice)
    role_company_object_links = get_role_object_links(role_id)
    module_id = Module.SALES.value

    if company:
        company_objects = company.company_objects.all().order_by('ordering')
        company_objects_ids = [company_object.id for company_object in company_objects]

        objects_items = ObjectItem.objects.filter(company_object_id__in=company_objects_ids)
        for company_object in company_objects:
            if module_id in company_object.modules:
                object_categories[company_object.id] = {}
                object_categories[company_object.id]['label'] = company_object
                object_categories[company_object.id]['value'] = None
                object_categories[company_object.id]['objects'] = {}

                invoice_company_object = invoice_object_links.get(company_object.id, None)
                if invoice_company_object:
                    object_categories[company_object.id]['value'] = invoice_company_object['value']

                if role_company_object_links:
                    role_company_object_items = role_company_object_links.get(company_object.id, None)
                    if role_company_object_items:
                        object_categories[company_object.id]['objects'] = role_company_object_items
                else:
                    object_categories[company_object.id]['objects'] = objects_items.filter(
                        company_object_id=company_object.id
                    )
    return {'object_categories': object_categories, 'form_style': form_style}


@register.inclusion_tag('sales/statement_discount.html')
def statement_discount(invoice, receipt_date):
    print(f"Invoice {invoice} and receipt date is {receipt_date}")


@register.inclusion_tag('sales/cell/line_types.html')
def line_types(row_id, cell, module_url, line_type, tabs_start, with_general=True):
    tab_index = int(row_id) + int(cell) + int(tabs_start)
    return {'row_id': row_id, 'start': cell, 'module_url': module_url, 'line_type': line_type, 'tab_index': tab_index,
            'with_general': with_general}


@register.inclusion_tag('sales/cell/vat_code.html')
def vat_code_cell(row_id, cell, default_vat_code, tabs_start, value=None):
    tab_index = int(row_id) + int(cell) + int(tabs_start)
    return {'row_id': row_id, 'default_vat': default_vat_code, 'tab_index': tab_index, 'vat_code': value}


@register.inclusion_tag('sales/cell/inventory.html')
def inventory_cell(row_id, cell, tabs_start):
    tab_index = int(row_id) + int(cell) + int(tabs_start)
    return {'row_id': row_id, 'tab_index': tab_index}


@register.inclusion_tag('sales/cell/account.html')
def account_cell(row_id, cell, tabs_start):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tab_index': tab_index}


@register.inclusion_tag('sales/cell/account_description.html')
def account_description_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('sales/cell/description.html')
def description_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('sales/cell/quantity.html')
def quantity_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tabs_start': tabs_start, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('sales/cell/ordered.html')
def ordered_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tabs_start': tabs_start, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('sales/cell/invoiced.html')
def invoiced_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tabs_start': tabs_start, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('sales/cell/back_order.html')
def back_order_cell(row_id, cell, tabs_start):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tabs_start': tabs_start, 'tab_index': tab_index}


@register.inclusion_tag('sales/cell/unit.html')
def unit_cell(row_id, cell, tabs_start, is_open=False):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tabs_start': tabs_start, 'tab_index': tab_index, 'is_open': is_open}


@register.inclusion_tag('sales/cell/discount.html')
def discount_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tab_index': tab_index, 'value': value}


@register.inclusion_tag('sales/cell/price.html')
def price_cell(row_id, cell, tabs_start, value=None):
    tab_index = int(tabs_start) + int(cell) + int(row_id)
    return {'row_id': row_id, 'tab_index': tab_index, 'value': value}


