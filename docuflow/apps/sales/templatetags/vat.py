import logging

from django import template

from docuflow.apps.sales.services import vat_recon

register = template.Library()

logger = logging.getLogger(__name__)


@register.inclusion_tag('sales/vat_summary.html')
def vat_summary(sales_invoice, sales_invoice_items):
    summary = {}
    if sales_invoice:
        summary = vat_recon(sales_invoice=sales_invoice, sales_invoice_items=sales_invoice_items)
    return {'summary': summary}


@register.inclusion_tag('sales/formats/swe/vat_summary.html')
def swe_vat_summary(sales_invoice, sales_invoice_items):
    summary = {}
    if sales_invoice:
        summary = vat_recon(sales_invoice=sales_invoice, sales_invoice_items=sales_invoice_items)
    return {'summary': summary}
