from django.db.models import Q

from docuflow.apps.company.models import Company
from docuflow.apps.period.models import Period
from .models import Invoice
from .enums import InvoiceStatus


def get_invoices_pending_update(company: Company, period: Period = None, start_date=None, end_date=None, invoice_types=None):
    if not invoice_types:
        invoice_types = []

    q = Q()
    q.add(Q(branch__company=company), Q.AND)

    if period:
        q.add(Q(period=period), Q.AND)
    if invoice_types and isinstance(invoice_types, list):
        q.add(Q(invoice_type__code__in=invoice_types), Q.AND)
    if start_date:
        q.add(Q(invoice_date__gte=start_date), Q.AND)

    if end_date:
        q.add(Q(invoice_date__lte=end_date), Q.AND)

    return Invoice.objects.select_related(
        'branch', 'customer', 'customer__default_account', 'disposal'
    ).prefetch_related(
        'invoice_items', 'invoice_items__item_link', 'object_items', 'disposal__disposed_fixed_assets'
    ).filter(
        q
    ).exclude(
        status=InvoiceStatus.UPDATED
    )


def get_invoice_detail():
    return Invoice.objects.with_totals().prefetch_related(
        'invoice_items', 'invoice_items__inventory', 'invoice_items__inventory__inventory',
        'invoice_items__vat_code', 'invoice_items__account', 'object_items', 'object_items__company_object',
        'invoices', 'receipts', 'customer__addresses', 'invoice_items__vat_code__company',
        'invoice_items__inventory__unit', 'invoice_items__inventory__history'
    ).select_related(
        'linked_invoice', 'customer', 'branch', 'salesman', 'period', 'branch__company', 'invoice_type',
        'customer__company', 'customer__delivery_method'
    )