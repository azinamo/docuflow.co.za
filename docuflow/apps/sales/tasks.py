from celery import shared_task

from docuflow.apps.sales.models import Invoice
from docuflow.apps.sales.services import send_invoice_email


@shared_task
def send_invoice_email_task(invoice_id):
    invoice = Invoice.objects.get(pk=invoice_id)
    send_invoice_email(invoice)
    return "Invoice email has been send successfully"
