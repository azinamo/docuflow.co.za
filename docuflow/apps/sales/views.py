import logging
import pprint
from collections import defaultdict
from decimal import Decimal

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.core import signing
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext as __
from django.views.generic import FormView, View, TemplateView, UpdateView, ListView, DetailView
from django.core.exceptions import ValidationError

from docuflow.apps.accounts.models import Role
from docuflow.apps.comment.services import clear_session_comments
from docuflow.apps.common import utils
from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import CompanyMixin, ProfileMixin, DataTableMixin
from docuflow.apps.company.models import CompanyObject, VatCode, Unit
from docuflow.apps.customer.models import Customer
from docuflow.apps.inventory.models import BranchInventory
from docuflow.apps.invoice.models import InvoiceType
from . import enums
from . import selectors
from . import tasks
from .exceptions import NotEnoughCreditException, PaymentNotDoneException, SalesException
from .forms import (ProformaInvoiceForm, CustomerInvoiceForm, SalesItemForm, NotEnoughCreditForm, DisposalInvoiceForm,
                    CreditInvoiceForm, BackOrderInvoiceForm, DeliveryInvoiceForm, EstimateInvoiceForm,
                    RecurringInvoiceForm, SendRecurringInvoiceForm)
from .models import (Invoice, InvoiceItem, Estimate, EstimateItem, Delivery, DeliveryItem, BackOrder,
                     BackOrderItem, Tracking, ProformaInvoice, ProformaInvoiceItem)
from .services import (
    CreateCustomerInvoice, CreateInvoiceFromBackOrder, CreateInvoiceFromProformaInvoice, create_docuflow_invoice,
    delete_invoices
)
from ..inventory.exceptions import InventoryException

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class SalesDocumentsView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'sales/index.html'
    context_object_name = 'invoices'

    def get_context_data(self, **kwargs):
        context = super(SalesDocumentsView, self).get_context_data(**kwargs)
        company = self.get_company()

        context['invoice_type'] = InvoiceType.objects.sales(company=company).exists()
        context['tax_invoice_type'] = InvoiceType.objects.sales_tax_invoice(company=company).first()
        context['credit_invoice_type'] = InvoiceType.objects.sales_credit_note(company=company).first()
        context['year'] = self.get_year()
        context['branch'] = self.get_branch()
        return context


class CreateDocumentMixin:

    # noinspection PyUnresolvedReferences
    def get_invoice_type(self, company):
        invoice_type_code = self.kwargs.get('invoice_type', None)
        if invoice_type_code == enums.InvoiceType.TAX_INVOICE.value:
            return InvoiceType.objects.sales_tax_invoice(company=company).first()
        elif invoice_type_code == enums.InvoiceType.CREDIT_INVOICE.value:
            return InvoiceType.objects.sales_credit_note(company=company).first()
        return InvoiceType.objects.sales_tax_invoice(company=company).first()


class CreateDocumentView(ProfileMixin, LoginRequiredMixin, CreateDocumentMixin, FormView):
    template_name = 'sales/create.html'
    form_class = CustomerInvoiceForm

    def get_initial(self):
        initial = super(CreateDocumentView, self).get_initial()
        initial['invoice_date'] = now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        initial['invoice_type'] = self.get_invoice_type(self.get_company())
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateDocumentView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        kwargs['role'] = self.get_role()
        return kwargs

    def clear_invoice_receipt_session(self):
        if 'sales_invoice_receipt' in self.request.session:
            del self.request.session['sales_invoice_receipt']

    def get_context_data(self, **kwargs):
        context = super(CreateDocumentView, self).get_context_data(**kwargs)
        company = self.get_company()
        if self.request.method == 'GET':
            self.clear_invoice_receipt_session()

        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        clear_session_comments(self.request, content_type)
        if 'sales_invoice_receipt' in self.request.session:
            del self.request.session['sales_invoice_receipt']
        context['company'] = company
        context['default_vat'] = company.vat_percentage
        context['invoice_date'] = now().date()
        context['invoice_type'] = self.get_invoice_type(company)
        context['content_type'] = content_type
        context['cols'] = 12
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': __('Error occurred saving the sale invoice, please fix the errors.'),
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateDocumentView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    invoice = form.save()
                    self.clear_invoice_receipt_session()
                    redirect_url = reverse('sales_documents_index')
                    if self.request.POST.get('action') == 'print':
                        redirect_url = reverse('view_sales_invoice', args=(invoice.id, )) + "?print=1"

                    return JsonResponse({'text': 'Sales invoice saved successfully',
                                         'error': False,
                                         'redirect': redirect_url
                                         })
            except PaymentNotDoneException as exception:
                return JsonResponse({'receipt_url': reverse('create_cash_invoice_receipt'),
                                     'amount': form.cleaned_data['total_amount'],
                                     'customer_id': form.cleaned_data['customer'].id,
                                     'create_receipt': True,
                                     'text': str(exception)
                                     }, status=200)
            except NotEnoughCreditException as exception:
                customer = form.cleaned_data['customer']
                return JsonResponse({'amount': form.cleaned_data['total_amount'],
                                     'details': str(exception),
                                     'override_url': reverse('not_enough_credit',
                                                             kwargs={'customer_id': customer.id}) + f"?amount={form.cleaned_data['total_amount']}",
                                     'not_enough_credit': True
                                     }, status=200)
            except SalesException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True,
                                     }, status=500)
        return super(CreateDocumentView, self).form_valid(form)


class CreateCustomerDocumentView(ProfileMixin, LoginRequiredMixin, CreateDocumentMixin, FormView):
    template_name = 'sales/create.html'
    form_class = CustomerInvoiceForm

    def get_initial(self):
        initial = super(CreateCustomerDocumentView, self).get_initial()
        initial['invoice_date'] = now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateCustomerDocumentView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        kwargs['role'] = self.get_role()
        return kwargs

    def get_company_object(self, company):
        return CompanyObject.objects.prefetch_related('object_items').filter(company=company).first()

    def get_context_data(self, **kwargs):
        context = super(CreateCustomerDocumentView, self).get_context_data(**kwargs)
        company = self.get_company()
        company_object = self.get_company_object(company)
        context['company'] = company
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['default_vat'] = company.vat_percentage
        context['invoice_date'] = now().date()
        context['invoice_type'] = self.get_invoice_type(company)
        context['company_object'] = company_object
        context['cols'] = 12
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the sale invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateCustomerDocumentView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    company = self.get_company()
                    profile = self.get_profile()
                    role = Role.objects.filter(id=self.request.session['role']).first()

                    invoice_type = self.kwargs.get('invoice_type', 'tax-invoice')
                    logger.info(f"Create -> customer {form.cleaned_data['customer']} {invoice_type} ")

                    invoice = CreateCustomerInvoice(company, profile, role, form, self.request.POST, invoice_type)
                    invoice.execute()

                    return JsonResponse({'text': __('Sales invoice saved successfully'),
                                         'redirect': reverse('sales_documents_index'),
                                         'error': False
                                         })
            except NotEnoughCreditException as exception:
                customer = form.cleaned_data['customer']
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'url': reverse('not_enough_credit', kwargs={'customer_id': customer.id}),
                                     'error': True
                                     })


class NotEnoughCreditView(CompanyMixin, FormView):
    template_name = 'sales/not_enough_credit.html'
    form_class = NotEnoughCreditForm

    def get_context_data(self, **kwargs):
        context = super(NotEnoughCreditView, self).get_context_data(**kwargs)
        amount = Decimal(self.request.GET.get('amount'))
        customer = Customer.objects.with_balance().get(id=self.kwargs['customer_id'])
        new_balance = customer.available_amount - amount
        context['amount'] = amount
        context['new_balance'] = new_balance
        context['customer'] = customer
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': __('Error occurred saving the customer credit, please fix the errors.'),
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(NotEnoughCreditView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            pass


class SingleInvoiceMixin(object):

    def get_invoice(self):
        try:
            return Invoice.objects.get_detail(self.kwargs['pk'])
        except Invoice.DoesNotExist:
            logger.info(f"Invoice with id {self.kwargs['pk']} does not exist")


class MultipleInvoiceMixin(object):

    def get_invoices(self):
        invoices_ids = self.request.POST.getlist('invoices[]')
        return Invoice.objects.filter(id__in=[int(invoice_id) for invoice_id in invoices_ids])


class CreateInvoiceFromEstimateView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'sales/estimate/create.html'
    form_class = EstimateInvoiceForm

    def get_invoice_type(self, company):
        return InvoiceType.objects.sales_tax_invoice(company).first()

    def get_estimate(self):
        return Estimate.objects.prefetch_related('estimate_items').get(pk=self.kwargs['estimate_id'])

    def get_initial(self):
        initial = super(CreateInvoiceFromEstimateView, self).get_initial()
        today = now()
        initial['invoice_date'] = today.strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        estimate = self.get_estimate()
        if estimate:
            initial['estimate_number'] = str(estimate)
            initial['salesman'] = estimate.salesman
            initial['pricing'] = estimate.pricing
            initial['due_date'] = estimate.customer.calculate_due_date(today)
            initial['customer'] = estimate.customer
            initial['vat_number'] = estimate.customer.vat_number
            initial['delivery_method'] = estimate.customer.delivery_method
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceFromEstimateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['estimate'] = self.get_estimate()
        kwargs['role'] = Role.objects.filter(pk=self.request.session.get('role')).first()
        kwargs['request'] = self.request
        return kwargs

    def clear_receipts(self):
        if self.request.method == 'GET':
            if 'sales_invoice_receipt' in self.request.session:
                del self.request.session['sales_invoice_receipt']

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceFromEstimateView, self).get_context_data(**kwargs)
        company = self.get_company()
        estimate = self.get_estimate()
        self.clear_receipts()
        context['company'] = company
        context['estimate'] = estimate
        context['branch'] = self.get_branch()
        context['invoice_type'] = self.get_invoice_type(company)
        context['cols'] = 15
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': __('Error occurred saving the document, please fix the errors.'),
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateInvoiceFromEstimateView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            estimate = self.get_estimate()
            try:
                with transaction.atomic():
                    invoice = form.save()
                    # company = self.get_company()
                    # profile = self.get_profile()
                    # role = Role.objects.filter(pk=self.request.session.get('role')).first()
                    # create_invoice = CreateInvoiceFromEstimate(company, estimate, profile, role, invoice, self.request)
                    # create_invoice.execute()

                    return JsonResponse({'text': __('Invoice document saved successfully'),
                                         'error': False,
                                         'redirect': reverse('sales_documents_index')
                                         })
            except PaymentNotDoneException as exception:
                logger.exception(exception)
                return JsonResponse({'receipt_url': reverse('create_cash_invoice_receipt'),
                                     'amount': form.cleaned_data['total_amount'],
                                     'customer_id': estimate.customer.id,
                                     'create_receipt': True,
                                     'text': str(exception)
                                     })
            except NotEnoughCreditException as exception:
                logger.exception(exception)
                return JsonResponse({'amount': form.cleaned_data['total_amount'],
                                     'details': str(exception),
                                     'override_url': reverse('not_enough_credit', kwargs={'customer_id': estimate.customer_id}),
                                     'not_enough_credit': True
                                     })
            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the document',
                                     'details': str(exception),
                                     'error': True
                                     })


class CreateInvoiceFromBackOrderView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'sales/backorder/create.html'
    form_class = BackOrderInvoiceForm

    def get_invoice_type(self, company):
        return InvoiceType.objects.filter(
            code=self.kwargs.get('invoice_type', 'tax-invoice'),
            company=company,
            file__module=Module.SALES
        ).first()

    def get_back_order(self):
        return BackOrder.objects.prefetch_related(
            'back_order_items'
        ).filter(
            pk=self.kwargs['back_order_id']
        ).first()

    def get_initial(self):
        initial = super(CreateInvoiceFromBackOrderView, self).get_initial()
        initial['invoice_date'] = now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        back_order = self.get_back_order()
        if back_order:
            initial['period'] = back_order.period
            initial['estimate_number'] = str(back_order)
            initial['salesman'] = back_order.salesman
            initial['pricing'] = back_order.pricing
            if back_order.customer:
                initial['customer'] = back_order.customer
                initial['vat_number'] = back_order.customer.vat_number
                initial['delivery_method'] = back_order.customer.delivery_method
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceFromBackOrderView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['back_order'] = self.get_back_order()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = Role.objects.filter(pk=self.request.session.get('role')).first()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceFromBackOrderView, self).get_context_data(**kwargs)
        company = self.get_company()
        back_order = self.get_back_order()
        context['company'] = company
        context['back_order'] = back_order
        context['branch'] = self.get_branch()
        context['invoice_type'] = self.get_invoice_type(company)
        context['cols'] = 15
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the document, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateInvoiceFromBackOrderView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            back_order = self.get_back_order()
            try:
                with transaction.atomic():
                    invoice = form.save()

                    company = self.get_company()
                    profile = self.get_profile()

                    role = Role.objects.filter(pk=self.request.session.get('role')).first()

                    create_invoice = CreateInvoiceFromBackOrder(company, back_order, profile, role, invoice,
                                                                self.kwargs.get('invoice_type', 'tax-invoice'),
                                                                self.request
                                                                )
                    create_invoice.execute()

                    return JsonResponse({'text': __('Invoice document saved successfully'),
                                         'error': False,
                                         'redirect': reverse('sales_documents_index')
                                         })
            except PaymentNotDoneException as exception:
                logger.exception(exception)
                return JsonResponse({'receipt_url': reverse('create_cash_invoice_receipt'),
                                     'amount': form.cleaned_data['total_amount'],
                                     'customer_id': back_order.customer.id,
                                     'create_receipt': True,
                                     'text': str(exception)
                                     })
            except NotEnoughCreditException as exception:
                logger.exception(exception)
                return JsonResponse({'amount': form.cleaned_data['total_amount'],
                                     'details': str(exception),
                                     'override_url': reverse('not_enough_credit', kwargs={'customer_id': back_order.customer_id}),
                                     'not_enough_credit': True
                                     })
            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the document',
                                     'details': str(exception),
                                     'error': True
                                     })


class BackOrderInvoiceItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'sales/backorder/inventory_items.html'

    def get_back_order_items(self):
        back_order_items = BackOrderItem.objects.select_related(
            'inventory',
            'back_order'
        ).filter(
            back_order_id=self.kwargs['back_order_id']
        )
        return back_order_items

    def get_context_data(self, **kwargs):
        context = super(BackOrderInvoiceItemsView, self).get_context_data(**kwargs)
        context['back_order_items'] = self.get_back_order_items()
        return context


class CreateInvoiceFromDeliveryView(LoginRequiredMixin, ProfileMixin, CreateDocumentMixin, FormView):
    template_name = 'sales/delivery/create.html'
    form_class = DeliveryInvoiceForm

    def get_delivery(self):
        return Delivery.objects.prefetch_related('delivery_items').get(pk=self.kwargs['delivery_id'])

    def get_initial(self):
        initial = super(CreateInvoiceFromDeliveryView, self).get_initial()
        initial['invoice_date'] = now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        delivery = self.get_delivery()
        today = now()
        if delivery:
            customer = delivery.customer
            if customer:
                initial['due_date'] = customer.calculate_due_date(today)
            if delivery.estimate:
                initial['estimate_number'] = str(delivery.estimate)
            if delivery.purchase_order:
                initial['po_number'] = delivery.purchase_order
            initial['delivery_number'] = str(delivery)
            if delivery.customer:
                initial['customer'] = delivery.customer
                initial['vat_number'] = delivery.customer.vat_number
                initial['delivery_method'] = delivery.customer.delivery_method
            elif delivery.customer_name:
                initial['customer_name'] = delivery.customer_name
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceFromDeliveryView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['delivery'] = self.get_delivery()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceFromDeliveryView, self).get_context_data(**kwargs)
        company = self.get_company()
        delivery = self.get_delivery()
        context['company'] = company
        context['delivery'] = delivery
        context['branch'] = self.get_branch()
        context['invoice_type'] = self.get_invoice_type(company)
        context['default_vat'] = company.vat_percentage
        context['cols'] = 15
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the document, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateInvoiceFromDeliveryView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            delivery = self.get_delivery()
            try:
                with transaction.atomic():
                    invoice = form.save()
                    redirect_url = reverse('sales_documents_index')
                    if self.request.POST.get('action') == 'print':
                        redirect_url = reverse('view_sales_invoice', args=(invoice.pk, )) + "?print=1"
                    return JsonResponse({'text': __('Invoice document saved successfully'),
                                         'error': False,
                                         'redirect': redirect_url
                                         })
            except PaymentNotDoneException as exception:
                logger.exception(exception)
                return JsonResponse({'receipt_url': reverse('create_cash_invoice_receipt'),
                                     'amount': form.cleaned_data['total_amount'],
                                     'customer_id': delivery.customer.id,
                                     'create_receipt': True,
                                     'text': str(exception)
                                     })
            except NotEnoughCreditException as exception:
                logger.exception(exception)
                return JsonResponse({'amount': form.cleaned_data['total_amount'],
                                     'details': str(exception),
                                     'override_url': reverse('not_enough_credit', kwargs={'customer_id': delivery.customer_id}),
                                     'not_enough_credit': True
                                     })
            except SalesException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True
                                     })
        return super(CreateInvoiceFromDeliveryView, self).form_valid(form)


class CreateInvoiceFromDisposalView(LoginRequiredMixin, ProfileMixin, CreateDocumentMixin, FormView):
    template_name = 'sales/create_from_disposal.html'
    form_class = DisposalInvoiceForm

    def get_initial(self):
        initial = super(CreateInvoiceFromDisposalView, self).get_initial()
        initial['invoice_date'] = now().date()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceFromDisposalView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceFromDisposalView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        context['invoice_type'] = self.get_invoice_type(company)
        context['default_vat'] = company.vat_percentage
        context['cols'] = 15
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the disposal invoice, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(CreateInvoiceFromDisposalView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            with transaction.atomic():
                form.save(commit=False)

                return JsonResponse({'text': __('Invoice saved'), 'error': False })


class InvoiceDeliveryItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'sales/delivery/inventory_items.html'

    def get_delivery_items(self):
        delivery_items = DeliveryItem.objects.select_related(
            'inventory', 'account', 'delivery', 'vat_code'
        ).filter(
            delivery_id=self.kwargs['delivery_id']
        ).order_by('id')
        return delivery_items

    def get_context_data(self, **kwargs):
        context = super(InvoiceDeliveryItemsView, self).get_context_data(**kwargs)
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['delivery_items'] = self.get_delivery_items()
        return context


class InvoiceEstimateItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'sales/estimate/inventory_items.html'

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch())
        return branch_inventories

    def get_vat_codes(self):
        return VatCode.objects.output().filter(company=self.get_company())

    def get_estimate_items(self):
        return EstimateItem.objects.select_related(
            'inventory', 'estimate', 'vat_code'
        ).filter(estimate_id=self.kwargs['estimate_id'])

    def get_context_data(self, **kwargs):
        context = super(InvoiceEstimateItemsView, self).get_context_data(**kwargs)
        context['inventory_items'] = self.get_inventory_items()
        context['vat_codes'] = self.get_vat_codes()
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['estimate_items'] = self.get_estimate_items()
        return context


class EditCustomerInvoiceView(LoginRequiredMixin, ProfileMixin, CreateDocumentMixin, UpdateView):
    template_name = 'sales/edit/edit.html'
    model = Invoice
    context_object_name = 'invoice'

    def get_form_class(self):
        if self.get_object().is_recurring:
            return RecurringInvoiceForm
        return CustomerInvoiceForm

    def get_queryset(self):
        return selectors.get_invoice_detail()

    def get_initial(self):
        initial = super(EditCustomerInvoiceView, self).get_initial()
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(EditCustomerInvoiceView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        kwargs['role'] = self.get_role()
        return kwargs

    def get_invoice_items_rows(self, invoice_items, rows):
        items_rows = {}
        for i, invoice_item in enumerate(invoice_items):
            items_rows[i+1] = {'invoice_item': invoice_item, 'row': rows.get(i+1)}
        return items_rows

    def get_context_data(self, **kwargs):
        context = super(EditCustomerInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': __('Error occurred saving the estimate, please fix the errors.'),
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(EditCustomerInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    invoice = form.save()
                    redirect_url = reverse('sales_documents_index')
                    if invoice.is_recurring:
                        redirect_url = reverse('recurring_invoice')
                    return JsonResponse({'text': __('Invoice updated successfully'),
                                         'error': False,
                                         'redirect': redirect_url
                                         })
            except PaymentNotDoneException as exception:
                logger.exception(exception)
                invoice_receipt = self.get_object().receipts.first()
                if invoice_receipt:
                    receipt_url = reverse('edit_cash_invoice_receipt', args=(self.get_object().id,))
                else:
                    receipt_url = reverse('create_cash_invoice_receipt')
                return JsonResponse({'create_receipt': True,
                                     'customer_id': self.get_object().customer_id,
                                     'amount': form.cleaned_data['total_amount'],
                                     'receipt_url': receipt_url
                                     })
            except (SalesException, NotEnoughCreditException) as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'url': reverse('not_enough_credit', kwargs={'customer_id': self.get_object().customer_id}),
                                     'error': True
                                     })
        return super(EditCustomerInvoiceView, self).form_valid(form)


class InvoiceItemsView(LoginRequiredMixin, ProfileMixin, FormView):
    form_class = SalesItemForm

    def get_template_names(self):
        if self.request.GET.get('invoice_type') == 'estimate':
            return 'sales/estimate/invoice_items.html'
        elif self.request.GET.get('invoice_type') == 'delivery':
            return 'sales/delivery/invoice_items.html'
        elif self.request.GET.get('invoice_type') == 'backorder':
            return 'sales/backorder/invoice_items.html'
        elif self.request.GET.get('invoice_type') == 'proforma':
            return 'sales/proforma/invoice_items.html'
        return 'sales/invoice_items.html'

    def get_form_kwargs(self, **kwargs):
        kwargs = super(InvoiceItemsView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['row_id'] = self.request.GET.get('row_id', 0)
        return kwargs

    def get_vat_code(self):
        return VatCode.objects.output().filter(company=self.get_company(), is_default=True).first()

    def get_rows(self):
        row_id = self.request.GET.get('row_id', None)
        requested_rows = int(self.request.GET.get('rows', 20))
        if row_id:
            return int(row_id) + requested_rows
        if requested_rows <= 1:
            requested_rows = max(requested_rows, 2)
        return requested_rows

    def get_row_start(self):
        row_id = self.request.GET.get('row_id', None)
        if row_id:
            return int(row_id)
        return 1

    def get_tabs_start(self, row_id, is_reload=False):
        start_at = 26
        if is_reload:
            return start_at
        if row_id:
            return 26 * row_id
        return start_at

    def get_module_url(self):
        if self.request.GET.get('invoice_type'):
            return reverse('add_invoice_items') + "?invoice_type=" + self.request.GET.get('invoice_type')
        return reverse('add_invoice_items')

    def get_context_data(self, **kwargs):
        context = super(InvoiceItemsView, self).get_context_data(**kwargs)
        vat_code = self.get_vat_code()
        is_reload = True if self.request.GET.get('reload', 0) == '1' else False
        col_count = self.request.GET.get('cols', 9)
        row_id = self.get_row_start()
        row_count = self.get_rows()
        tabs_start = self.get_tabs_start(row_id, is_reload)
        rows = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['is_deletable'] = int(row_id) > 0
        context['row_id'] = row_id
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['default_vat_code'] = vat_code
        context['module_url'] = self.get_module_url()
        context['tabs_start'] = tabs_start
        context['start'] = 0
        context['rows'] = rows
        context['is_reload'] = is_reload
        context['row_start'] = row_id
        return context


class EditInvoiceItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'sales/edit/invoice_items.html'

    def get_invoice_items(self):
        return InvoiceItem.objects.select_related(
            'vat_code', 'inventory', 'account'
        ).filter(invoice_id=self.kwargs['invoice_id'])

    def get_vat_codes(self):
        return VatCode.objects.output().filter(company=self.get_company())

    def get_units(self):
        return Unit.objects.filter(measure__company=self.get_company())

    # noinspection PyMethodMayBeStatic
    def get_invoice_items_rows(self, invoice_items, rows):
        items_rows = {}
        for i, invoice_item in enumerate(invoice_items):
            items_rows[i+1] = {'invoice_item': invoice_item, 'row': rows.get(i+1)}
        return items_rows

    def get_context_data(self, **kwargs):
        context = super(EditInvoiceItemsView, self).get_context_data(**kwargs)
        invoice_items = self.get_invoice_items()
        context['cols'] = 12
        context['vat_code'] = VatCode.objects.output().filter(company=self.get_company(), is_default=True).first()
        is_reload = False
        tabs_start = 26
        col_count = 9
        row_id = 0
        row_count = len(invoice_items) + 1
        rows = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        item_rows = self.get_invoice_items_rows(invoice_items, rows)
        context['rows'] = item_rows
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['item_rows'] = item_rows
        return context


class InvoiceDetailView(LoginRequiredMixin, ProfileMixin, DetailView):
    template_name = 'sales/detail.html'
    model = Invoice

    def get_queryset(self):
        return Invoice.objects.with_totals().prefetch_related(
            'invoice_items', 'invoice_items__inventory', 'invoice_items__inventory__inventory',
            'invoice_items__vat_code', 'invoice_items__account', 'object_items', 'object_items__company_object',
            'invoices', 'receipts', 'customer__addresses', 'invoice_items__vat_code__company',
            'invoice_items__inventory__unit', 'invoice_items__inventory__history'
        ).select_related(
            'linked_invoice', 'customer', 'branch', 'salesman', 'period', 'invoice_type', 'branch__company',
            'branch__company__currency', 'customer__company', 'customer__delivery_method'
        )

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'sales/print.html'
        return 'sales/detail.html'

    def get_context_data(self, **kwargs):
        context = super(InvoiceDetailView, self).get_context_data(**kwargs)
        invoice = self.get_object()
        banks = invoice.branch.company.bank_details.all()
        context['is_print'] = self.request.GET.get('print', False) == '1'
        context['invoice'] = invoice
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        context['content_type'] = ContentType.objects.filter(app_label='sales', model='invoice').first()
        context['banks'] = banks
        context['account_details'] = banks[0] if banks else None
        context['customer_discount'] = invoice.customer_discount
        return context


class InvoiceTrackingView(LoginRequiredMixin, ProfileMixin, SingleInvoiceMixin,  TemplateView):
    template_name = 'sales/tracking.html'

    def get_tracking(self, invoice):
        trackings = Tracking.objects.select_related(
            'profile', 'role'
        ).filter(
            object_id=invoice.id, content_type=ContentType.objects.get_for_model(invoice)
        )
        tracking_by_types = defaultdict(list)
        for tracking in trackings:
            tracking_type = tracking.data.get('tracking_type')
            if tracking_type:
                tracking_by_types[tracking_type].append(tracking)
        return tracking_by_types

    def get_invoice_tracking(self):
        invoice = self.get_invoice()

        tracking = {'receipts': [], 'updates': [], 'posting': [], 'payments': [], 'journal': [],
                    'payment_journal': [], 'invoices': [], 'linked_invoice': []}
        for invoice_update in invoice.invoices.all():
            tracking['updates'].append(invoice_update)
        for invoice_payment in invoice.payments.all():
            tracking['payments'].append(invoice_payment)
        for invoice_rec in invoice.receipts.all():
            tracking['receipts'].append(invoice_rec)
        if invoice.links:
            for invoice_link in invoice.links.all():
                tracking['invoices'].append(invoice_link)
        if invoice.linked_invoice:
            tracking['linked_invoice'].append(invoice.linked_invoice)
        logger.info(tracking)
        return tracking

    def get_context_data(self, **kwargs):
        context = super(InvoiceTrackingView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        tracking = self.get_tracking(invoice)
        context['invoice'] = invoice
        context['tracking'] = self.get_invoice_tracking()
        context['picking_slip_tracking'] = tracking.get('pickingslip')
        context['delivery_tracking'] = tracking.get('delivery')
        context['estimate_tracking'] = tracking.get('estimate')
        context['backorder_tracking'] = tracking.get('backorder')
        return context


class CompleteDraftInvoiceView(ProfileMixin, SingleInvoiceMixin, FormView):
    template_name = 'sales/complete_draft.html'
    form_class = NotEnoughCreditForm

    def get_context_data(self, **kwargs):
        context = super(CompleteDraftInvoiceView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['customer'] = invoice.customer
        context['invoice'] = invoice
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred updating invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CompleteDraftInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                invoice = self.get_invoice()

                invoice.status = enums.InvoiceStatus.PENDING
                invoice.save()

                return JsonResponse({'text': __('Document saved successfully'),
                                     'error': False,
                                     'redirect': reverse('view_sales_invoice', kwargs={'pk': invoice.id})
                                     })
        return super(CompleteDraftInvoiceView, self).form_invalid(form)


class MarkInvoicesAsPaidView(ProfileMixin, MultipleInvoiceMixin, View):

    def post(self, request, *args, **kwargs):
        invoices = self.get_invoices()
        invoices_to_pay = []
        for invoice in invoices:
            if not invoice.receipt:
                invoices_to_pay.append(invoice.id)
        if len(invoices_to_pay) > 0:
            self.request.session['invoices'] = invoices_to_pay
            return JsonResponse({
                'text': __('Invoices successfully marked as paid'),
                'error': False,
                'alert': True,
                'url': reverse('create_invoices_receipt')
            })
        else:
            return JsonResponse({
                'text': __('All of the selected invoices are already paid'),
                'error': True
            })


class SingleInvoiceActionView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(pk=self.request.GET.get('invoice_id'))
            action = self.kwargs['action']
            if action == 'print':
                return JsonResponse({
                    'text': __('Redirecting ...'),
                    'error': False,
                    'redirect': reverse('view_sales_invoice', kwargs={'pk': invoice.id}) + "?print=1"
                })
            elif action == 'credit':
                if invoice.is_creditable:
                    return JsonResponse({
                        'text': __('Redirecting ...'),
                        'error': False,
                        'redirect': reverse('credit_invoice', kwargs={'pk': invoice.id})
                    })
                else:
                    return JsonResponse({
                        'text': __('Invoice already credited'),
                        'warning': False
                    })
            elif action == 'copy':
                return JsonResponse({
                    'text': __('Redirecting ...'),
                    'error': False,
                    'redirect': reverse('copy_invoice', kwargs={'pk': invoice.id})
                })
            elif action == 'edit':
                if invoice.is_printed:
                    return JsonResponse({
                        'text': __('Document no longer editable'),
                        'warning': True
                    })
                else:
                    return JsonResponse({
                        'text': __('Redirecting ...'),
                        'error': False,
                        'redirect': reverse('edit_invoice', args=(invoice.id, ))
                    })
            elif action == 'create_picking_slip':
                if invoice.is_credit:
                    return JsonResponse({
                        'text': __('Credit note cannot be send for picking'),
                        'warning': True
                    })
                elif invoice.picking_slip:
                    return JsonResponse({
                        'text': __(f'This document has already been sent for picking on picking slip {str(invoice.picking_slip)}'),
                        'warning': True
                    })
                return JsonResponse({
                    'text': 'Redirecting ...',
                    'error': False,
                    'redirect': reverse('create_picking_slip_from_invoice', kwargs={'invoice_id': invoice.id})
                })
            else:
                return JsonResponse({
                    'text': __('Action does not exist yet, please try again later'),
                    'error': False,
                    'reload': True
                })
        except Invoice.DoesNotExist as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': __('Invoice not found'),
                'error': True,
                'details': str(exception)
            })


class BulkInvoiceActionView(LoginRequiredMixin, ProfileMixin, View):

    def post(self, request, *args, **kwargs):
        invoices = Invoice.objects.filter(id__in=[int(invoice_id) for invoice_id in self.request.POST.getlist('invoices[]')])
        if invoices:
            action = self.kwargs['action']
            if action == 'recurring_send':
                self.request.session['recurring_invoices'] = [invoice.id for invoice in invoices]
                return JsonResponse({
                    'text': __('Redirecting ...'),
                    'url': reverse('send_recurring_invoices')
                })
            if action == 'delete':
                delete_invoices(invoices, self.get_profile(), Role.objects.get(pk=self.request.session['role']))
                return JsonResponse({
                    'text': __('Invoices deleted'),
                    'error': False,
                    'reload': True
                })
            else:
                return JsonResponse({
                    'text': __('Action does not exist yet, please try again later'),
                    'warning': True
                })
        else:
            return JsonResponse({
                'text': __('Invoices not found'),
                'error': True
            })


class SendEmailView(ProfileMixin, MultipleInvoiceMixin, View):

    def post(self, request, *args, **kwargs):
        invoices = self.get_invoices()
        for invoice in invoices:
            if invoice.customer.email:
                tasks.send_invoice_email_task(invoice_id=invoice.id)
        return JsonResponse({
            'text': __('Invoices email successfully send'),
            'error': False
        })


class SendInvoiceEmailView(LoginRequiredMixin, SingleInvoiceMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(pk=self.kwargs['pk'])
            if not invoice.customer.email:
                return JsonResponse({
                    'text': 'Customer email address not found',
                    'error': True
                })
            tasks.send_invoice_email_task(invoice_id=invoice.id)

            return JsonResponse({
                'text': __('Invoices email successfully send'),
                'error': False
            })
        except Invoice.DoesNotExist:
            return JsonResponse({
                'text': __('Invoice not found'),
                'error': False
            })


class CountryView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            countries = list(countries)
            return JsonResponse({
                'countries': countries,
                'error': False
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Countries could not be fetched',
                'error': True,
                'details': str(exception)
            })


class CopyInvoiceView(ProfileMixin, SingleInvoiceMixin,  FormView):
    template_name = 'sales/copy_invoice.html'
    form_class = CustomerInvoiceForm

    def get_initial(self):
        initial = super(CopyInvoiceView, self).get_initial()
        initial['invoice_date'] = now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        invoice = self.get_invoice()
        if invoice:
            initial['period'] = invoice.period
            initial['customer_name'] = str(invoice.customer)
            initial['invoice_date'] = invoice.invoice_date
            initial['estimate_number'] = invoice.estimate_number
            initial['salesman'] = invoice.salesman
            initial['pricing'] = invoice.pricing
            initial['customer'] = invoice.customer
            initial['vat_number'] = invoice.customer.vat_number
            initial['delivery_method'] = invoice.customer.delivery_method
        return initial

    def get_form_kwargs(self):
        kwargs = super(CopyInvoiceView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CopyInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        invoice = self.get_invoice()
        context['company'] = company
        context['invoice'] = invoice
        context['invoice_items'] = invoice.invoice_items.filter(available_quantity__gt=0).all()
        context['branch'] = self.get_branch()
        context['invoice_type'] = InvoiceType.objects.sales_credit_note(company).first()
        context['cols'] = 11
        context['default_vat'] = company.vat_percentage
        context['invoice_date'] = now().date()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the document, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CopyInvoiceView, self).form_invalid(form)

    def clear_invoice_receipt_session(self):
        if 'sales_invoice_receipt' in self.request.session:
            del self.request.session['sales_invoice_receipt']

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    invoice = form.save()
                    self.clear_invoice_receipt_session()
                    redirect_url = reverse('sales_documents_index')
                    if self.request.POST.get('action') == 'print':
                        redirect_url = reverse('view_sales_invoice', args=(invoice.id,)) + "?print=1"

                    return JsonResponse({'text': 'Sales invoice copied successfully',
                                         'error': False,
                                         'redirect': redirect_url
                                         })
            except PaymentNotDoneException as exception:
                return JsonResponse({'receipt_url': reverse('create_cash_invoice_receipt'),
                                     'amount': form.cleaned_data['total_amount'],
                                     'customer_id': form.cleaned_data['customer'].id,
                                     'create_receipt': True,
                                     'text': str(exception)
                                     })
            except NotEnoughCreditException as exception:
                logger.exception(exception)
                customer = form.cleaned_data['customer']
                return JsonResponse({'amount': form.cleaned_data['total_amount'],
                                     'details': str(exception),
                                     'override_url': reverse('not_enough_credit',
                                                             kwargs={
                                                                 'customer_id': customer.id}) + f"?amount={form.cleaned_data['total_amount']}",
                                     'not_enough_credit': True
                                     })
            except SalesException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True,
                                     })
        return super(CopyInvoiceView, self).form_valid(form)


class CreditInvoiceView(ProfileMixin, SingleInvoiceMixin,  FormView):
    template_name = 'sales/credit_invoice.html'
    form_class = CreditInvoiceForm

    def get_initial(self):
        initial = super(CreditInvoiceView, self).get_initial()
        initial['invoice_date'] = now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreditInvoiceView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['invoice'] = self.get_invoice()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreditInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        invoice = self.get_invoice()
        context['company'] = company
        context['invoice'] = invoice
        context['invoice_items'] = invoice.invoice_items.all()
        context['branch'] = self.get_branch()
        context['invoice_type'] = InvoiceType.objects.sales_credit_note(company).first()
        context['cols'] = 11
        context['default_vat'] = company.vat_percentage
        context['invoice_date'] = now().date()
        context['content_type'] = ContentType.objects.get(app_label='sales', model='invoice')
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the document, please fix the errors.',
                'errors': form.errors,
                'alert': True
            }, status=400)
        return super(CreditInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    form.save()

                    return JsonResponse({
                        'text': __('Invoice document saved successfully'),
                        'error': False,
                        'redirect': reverse('sales_documents_index')
                    })
            except PaymentNotDoneException as exception:
                return JsonResponse({
                    'receipt_url': reverse('create_cash_invoice_receipt'),
                    'amount': form.cleaned_data['total_amount'],
                    'create_receipt': True,
                    'text': str(exception),
                    'error': True
                })
            except (InventoryException, SalesException, ValidationError) as exception:
                return JsonResponse({
                    'text': str(exception),
                    'error': True
                }, status=400)
        return super(CreditInvoiceView, self).form_valid(form)


class CreditInvoiceItemsView(EditInvoiceItemsView):
    template_name = 'sales/credit_invoice_items.html'

    def get_invoice_items(self):
        return InvoiceItem.objects.filter(
            invoice_id=self.kwargs['invoice_id'],
            available_quantity__gt=0
        )


class SendInvoiceToDocuflowView(ProfileMixin, SingleInvoiceMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            invoice = self.get_invoice()
            if invoice:
                create_docuflow_invoice(
                    sales_invoice=invoice,
                    role=self.get_role(),
                    profile=self.get_profile(),
                    request=request
                )
                return JsonResponse({
                    'text': __('Invoice send to docuflow successfully'),
                    'error': False,
                    'reload': True
                })
            else:
                return JsonResponse({
                    'text': __('No invoice found'),
                    'error': True,
                    'reload': True
                })
        except SalesException as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': str(exception),
                'details': str(exception),
                'error': True,
            }, status=500)


class InvoiceDocumentView(TemplateView):
    template_name = 'sales/view.html'

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'sales/print.html'
        return 'sales/view.html'

    def get_invoice(self, invoice_id):
        return Invoice.objects.prefetch_related(
            'invoice_items', 'invoice_items__inventory', 'object_items', 'object_items__company_object', 'invoices', 'receipts'
        ).select_related(
            'linked_invoice', 'branch', 'branch__company'
        ).filter(
            pk=invoice_id
        ).first()

    def get_context_data(self, **kwargs):
        context = super(InvoiceDocumentView, self).get_context_data(**kwargs)
        data = signing.loads(self.kwargs['signature'])
        invoice = self.get_invoice(data['document_id'])
        context['invoice'] = invoice
        context['branch'] = invoice.branch
        context['company'] = invoice.branch.company
        return context


class CreateInvoiceFromProformaView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'sales/proforma/create.html'
    form_class = ProformaInvoiceForm

    def get_proforma_invoice(self):
        return ProformaInvoice.objects.prefetch_related(
            'invoice_items'
        ).filter(
            pk=self.kwargs['proforma_invoice_id']
        ).first()

    def get_initial(self):
        initial = super(CreateInvoiceFromProformaView, self).get_initial()
        today = now()
        initial['invoice_date'] = today.strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        proforma_invoice = self.get_proforma_invoice()
        if proforma_invoice:
            initial['salesman'] = proforma_invoice.salesman
            initial['pricing'] = proforma_invoice.pricing
            # initial['due_date'] = proforma_invoice.calculate_due_date(today)
            initial['customer'] = proforma_invoice.customer
            initial['vat_number'] = proforma_invoice.customer.vat_number
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateInvoiceFromProformaView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['proforma_invoice'] = self.get_proforma_invoice()
        kwargs['request'] = self.request
        kwargs['role'] = Role.objects.get(pk=self.request.session['role'])
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceFromProformaView, self).get_context_data(**kwargs)
        company = self.get_company()
        proforma_invoice = self.get_proforma_invoice()
        context['company'] = company
        context['proforma_invoice'] = proforma_invoice
        context['branch'] = self.get_branch()
        context['invoice_type'] = proforma_invoice.invoice_type
        context['cols'] = 15
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the document, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateInvoiceFromProformaView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    invoice = form.save()

                    proforma_invoice = self.get_proforma_invoice()

                    create_invoice = CreateInvoiceFromProformaInvoice(
                        company=self.get_company(), proforma_invoice=proforma_invoice, profile=self.get_profile(),
                        role=self.get_role(), invoice=invoice, invoice_type='tax-invoice', request=self.request
                    )
                    create_invoice.execute()

                    return JsonResponse({'text': __('Invoice document saved successfully'),
                                         'error': False,
                                         'redirect': reverse('sales_documents_index')
                                         })
            except PaymentNotDoneException as exception:
                logger.exception(exception)
                return JsonResponse({'text': '',
                                     'alert': 'True',
                                     'url': reverse('create_cash_invoice_receipt')
                                     })
        return super(CreateInvoiceFromProformaView, self).form_valid(form)


class ProformaInvoiceItemsView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'sales/proforma/inventory_items.html'

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch())
        return branch_inventories

    def get_vat_codes(self):
        return VatCode.objects.output().filter(company=self.get_company())

    def get_invoice_items(self):
        return ProformaInvoiceItem.objects.select_related(
            'inventory', 'invoice', 'vat_code'
        ).filter(invoice_id=self.kwargs['proforma_invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(ProformaInvoiceItemsView, self).get_context_data(**kwargs)
        context['inventory_items'] = self.get_inventory_items()
        context['vat_codes'] = self.get_vat_codes()
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['invoice_items'] = self.get_invoice_items()
        return context


class RecurringInvoiceView(LoginRequiredMixin, ProfileMixin, ListView):
    paginate_by = 30
    model = Invoice
    template_name = 'sales/recurring/index.html'
    context_object_name = 'invoices'

    def get_queryset(self):
        return Invoice.objects.select_related(
            'invoice_type', 'customer'
        ).filter(branch__company=self.get_company(), is_recurring=True)

    def get_context_data(self, **kwargs):
        context = super(RecurringInvoiceView, self).get_context_data(**kwargs)
        profile = self.get_profile()
        company = self.get_company()

        context['invoice_type'] = InvoiceType.objects.sales(company).exists()
        context['tax_invoice_type'] = InvoiceType.objects.sales_tax_invoice(company).first()
        context['year'] = self.get_year()
        context['branch'] = self.get_branch()
        context['company'] = company
        context['profile'] = profile
        return context


class CreateRecurringDocumentView(CreateDocumentView):
    template_name = 'sales/recurring/create.html'
    form_class = RecurringInvoiceForm

    def get_initial(self):
        initial = super(CreateRecurringDocumentView, self).get_initial()
        initial['branch'] = self.get_branch()
        initial['invoice_type'] = self.get_invoice_type(self.get_company())
        del initial['invoice_date']
        return initial

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    logger.info("Save on recurring")
                    invoice = form.save()
                    redirect_url = reverse('recurring_invoice')

                    return JsonResponse({'text': __('Sales invoice saved successfully'),
                                         'error': False,
                                         'redirect': redirect_url
                                         })
            except SalesException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True,
                                     })
        return super(CreateRecurringDocumentView, self).form_valid(form)


class RecurringDocumentDetailView(LoginRequiredMixin, ProfileMixin, DetailView):
    template_name = 'sales/recurring/detail.html'
    model = Invoice
    context_object_name = 'invoice'

    def get_context_data(self, **kwargs):
        context = super(RecurringDocumentDetailView, self).get_context_data(**kwargs)
        context['is_print'] = self.request.GET.get('print', False) == '1'
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        context['content_type'] = ContentType.objects.filter(app_label='sales', model='invoice').first()
        return context


class SendRecurringDocumentView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'sales/recurring/send.html'
    form_class = SendRecurringInvoiceForm

    def get_initial(self):
        initial = super(SendRecurringDocumentView, self).get_initial()
        today = now()
        initial['invoice_date'] = today.strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(SendRecurringDocumentView, self).get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = Role.objects.get(pk=self.request.session['role'])
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(SendRecurringDocumentView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': __('Error occurred saving the recurring documents, please fix the errors.'),
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(SendRecurringDocumentView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save()

                return JsonResponse({'text': __('Invoice document saved successfully'),
                                     'error': False,
                                     'redirect': reverse('sales_documents_index')
                                     })
        return super(SendRecurringDocumentView, self).form_valid(form)
