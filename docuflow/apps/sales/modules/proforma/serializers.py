from rest_framework import generics

from docuflow.apps.sales.models import Estimate, EstimateItem


class EstimateSerializer(generics.ListAPIView):

    class Meta:
        model = Estimate
        fields = '__all__'


class EstimateItemSerializer(generics.ListAPIView):

    class Meta:
        model = EstimateItem
        fields = '__all__'
