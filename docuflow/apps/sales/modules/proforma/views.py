from decimal import Decimal
import logging
import pprint
from datetime import datetime, timedelta

from django.urls import reverse
from django.views.generic import CreateView, View, TemplateView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.db import transaction
from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.sales.models import ProformaInvoice, ProformaInvoiceItem
from docuflow.apps.inventory.models import BranchInventory
from docuflow.apps.company.models import VatCode, Unit
from docuflow.apps.accounts.models import Role
from docuflow.apps.sales.exceptions import QuantityNotAvailableException
from docuflow.apps.common.mixins import ProfileMixin
from .forms import CreateProformaInvoiceForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


# Create your views here.
class ProformaInvoiceView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'proforma/index.html'
    paginate_by = 30
    context_object_name = 'invoices'
    model = ProformaInvoice

    def get_queryset(self):
        return ProformaInvoice.objects.filter(branch=self.get_branch()).select_related(
            'invoice_type', 'customer'
        ).only('id', 'invoice_id', 'invoice_type__name', 'invoice_type__number_start',
               'customer__number', 'customer__name', 'invoice_date', 'net_amount', 'total_amount', 'status')

    def get_context_data(self, **kwargs):
        context = super(ProformaInvoiceView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        context['branch'] = branch
        return context


class CreateProformaInvoiceView(ProfileMixin, LoginRequiredMixin, CreateView):
    model = ProformaInvoice
    template_name = 'proforma/create.html'
    form_class = CreateProformaInvoiceForm

    def get_initial(self):
        initial = super(CreateProformaInvoiceView, self).get_initial()
        company = self.get_company()
        today = datetime.now()
        initial['invoice_date'] = today.strftime('%Y-%m-%d')
        if company and company.estimate_valid_until:
            expiry_date = today + timedelta(company.estimate_valid_until)
            initial['due_date'] = expiry_date.strftime('%Y-%m-%d')
        else:
            initial['due_date'] = today.strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        return initial

    def get_form_class(self):
        return CreateProformaInvoiceForm

    def get_form_kwargs(self):
        kwargs = super(CreateProformaInvoiceView, self).get_form_kwargs()
        company = self.get_company()
        kwargs['company'] = company
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateProformaInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['default_vat'] = company.vat_percentage
        context['date'] = datetime.now()
        context['cols'] = 12
        context['invoice_type'] = InvoiceType.objects.get_proforma_invoice_type(company)
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the proforma invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateProformaInvoiceView, self).form_invalid(form)

    def calculate_price_including(self, vat_code, price):
        if vat_code:
            price = ((vat_code.percentage / 100) * float(price)) + float(price)
        return Decimal(price)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    branch = self.get_branch()
                    profile = self.get_profile()
                    role = Role.objects.filter(pk=self.request.session.get('role')).first()
                    form.execute(branch, profile, role, self.request)

                    return JsonResponse({'text': 'Invoice saved successfully',
                                         'error': False,
                                         'redirect': reverse('sales_proforma_invoice_index')
                                         })

            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the invoice',
                                     'details': str(exception),
                                     'error': True
                                     })


class EditProformaInvoiceView(CreateProformaInvoiceView):
    template_name = 'proforma/edit_cash.html'

    def get_initial(self):
        initial = super(EditProformaInvoiceView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        return initial

    def get_form_class(self):
        return CreateProformaInvoiceForm

    def get_form_kwargs(self):
        kwargs = super(EditProformaInvoiceView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditProformaInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the estimate, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(EditProformaInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    company = self.get_company()
                    profile = self.get_profile()

                    estimate = form.save(commit=False)
                    estimate.branch = self.get_branch()
                    estimate.profile = self.get_profile()
                    estimate.role_id = self.request.session['role']
                    estimate.save()

                    if estimate:
                        self.save_estimate_items(estimate, True)

                    return JsonResponse({'text': 'Estimate saved successfully',
                                         'error': False,
                                         'redirect': reverse('estimates_index')
                                         })

            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the estimate',
                                     'details': str(exception),
                                     'error': True
                                     })


class InventoryLineView(LoginRequiredMixin, ProfileMixin, TemplateView):
    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch())
        return branch_inventories

    def get_vat_codes(self):
        return VatCode.objects.output().filter(company=self.get_company())

    def get_units(self):
        return Unit.objects.filter(measure__company=self.get_company())


class AddProformaInvoiceItemRowView(InventoryLineView):
    template_name = 'proforma/invoice_items.html'

    def get_context_data(self, **kwargs):
        context = super(AddProformaInvoiceItemRowView, self).get_context_data(**kwargs)
        vat_codes = self.get_vat_codes()
        row_id = self.request.GET.get('row_id', 0)
        context['inventory_items'] = self.get_inventory_items()
        context['vat_codes'] = vat_codes
        context['units'] = self.get_units()
        context['is_deletable'] = int(row_id) > 0
        context['row_id'] = row_id
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['default_vat_code'] = vat_codes.filter(is_default=True).first()
        return context


class ProformaInvoiceDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'proforma/detail.html'

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'proforma/print.html'
        return 'proforma/detail.html'

    def get_invoice(self):
        return ProformaInvoice.objects.get_detail(self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(ProformaInvoiceDetailView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        invoice_items = invoice.invoice_items.all()
        context['invoice'] = invoice
        context['invoice_items'] = invoice_items
        context['branch'] = self.get_branch()
        context['account_details'] = invoice.branch.company.bank_details.first() if invoice.branch.company.bank_details else None
        context['cols'] = 11
        context['range'] = range(0, 11)
        context['is_print'] = self.request.GET.get('print', 0) == '1'
        context['content_type'] = ContentType.objects.filter(app_label='sales', model='estimate').first()
        return context


class ReceiveInventoryItemsView(ProfileMixin, TemplateView):
    template_name = 'proforma/purchase_order_items.html'

    def get_purchase_order_inventory_items(self):
        query = ProformaInvoiceItem.objects.filter(invoice_id=self.kwargs['invoice_id'])

        inventory_items = {'total_ordered': 0, 'total_received': 0, 'total_short': 0, 'total_price_exl': 0,
                           'total_price_in': 0, 'total_total_price_exl': 0, 'total_total_price_inc': 0,
                           'inventoryitems': []
                           }
        for po_item in query:
            inventory_items['total_ordered'] += po_item.quantity
            inventory_items['total_received'] += 0
            inventory_items['total_short'] += 0
            inventory_items['total_price_exl'] += po_item.order_value
            inventory_items['total_price_in'] += po_item.price_with_vat
            inventory_items['total_total_price_exl'] += po_item.total_price
            inventory_items['total_total_price_inc'] += po_item.total_price_with_vat
            inventory_items['inventoryitems'].append(po_item)
        return inventory_items

    def get_context_data(self, **kwargs):
        context = super(ReceiveInventoryItemsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['inventory_items'] = self.get_purchase_order_inventory_items()
        return context


class MultipleProformaInvoicesMixin(object):

    def get_estimates(self):
        invoices_ids = self.request.POST.getlist('invoices[]')
        return ProformaInvoice.objects.filter(id__in=[int(invoice_id) for invoice_id in invoices_ids])


class CopyEstimateView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        return ProformaInvoice.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimate()
            if estimate:
                estimate.make_copy()
            return JsonResponse({
                'text': 'Invoice copied successfully',
                'error': False
            })
        except ProformaInvoice.DoesNotExist:
            return JsonResponse({
                'text': 'Invoice not found',
                'error': True
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class CreateEstimateCopiesView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        estimates_ids = self.request.POST.getlist('estimates[]')
        return ProformaInvoice.objects.filter(id__in=[int(estimate_id) for estimate_id in estimates_ids]).first()

    def post(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimate()
            if estimate:
                redirect_url = reverse('create_estimate_copy', kwargs={'estimate_id': estimate.id})
                return JsonResponse({
                    'text': 'Redirecting to copy page ....',
                    'error': False,
                    'redirect': redirect_url
                })
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True,
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class RemoveProformaInvoiceView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return ProformaInvoice.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            estimate = self.get_invoice()
            if estimate:
                estimate.execute_delete(self.get_profile(), self.request.session.get('role'))
            return JsonResponse({
                'text': 'Invoice removed successfully',
                'error': False,
                'redirect': reverse('sales_proforma_invoice_index')
            })
        except ProformaInvoice.DoesNotExist:
            return JsonResponse({
                'text': 'Invoice not found',
                'error': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class RemoveEstimatesView(LoginRequiredMixin, ProfileMixin, MultipleProformaInvoicesMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            invoices = self.get_invoices()
            if invoices:
                for invoice in invoices:
                    invoice.execute_delete(self.get_profile(), self.request.session.get('role'))
            return JsonResponse({
                'text': 'Invoice invoices deleted successfully',
                'error': False,
                'reload': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class CreateInvoiceFromProformaInvoiceView(LoginRequiredMixin, ProfileMixin, View):

    def get_invoice(self):
        return ProformaInvoice.objects.filter(id=self.request.POST.get('invoices[]')).first()

    def post(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                invoice = self.get_invoice()
                if invoice:
                    redirect_url = reverse('create_invoice_from_proforma_invoice', kwargs={'proforma_invoice_id': invoice.id})
                return JsonResponse({
                    'text': 'Redirecting to invoice ....',
                    'error': False,
                    'redirect': redirect_url
                })
        except ProformaInvoice.DoesNotExist:
            return JsonResponse({
                'text': 'Invoice not found',
                'error': True
            })
        except QuantityNotAvailableException as exception:
            return JsonResponse({
                'text': str(exception),
                'alert': True,
                'url': reverse('estimate_invoice_back_order_notice'),
                'error': True
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class CreateEstimatesInvoicesView(LoginRequiredMixin, ProfileMixin, MultipleProformaInvoicesMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimates().first()
            if estimate:
                redirect_url = reverse('create_estimate_invoice', kwargs={'estimate_id': estimate.id})
                return JsonResponse({
                    'text': 'Redirecting to invoice ....',
                    'error': False,
                    'redirect': redirect_url
                })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class PrintEstimateView(LoginRequiredMixin, ProfileMixin, MultipleProformaInvoicesMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimates().first()
            if estimate:
                return JsonResponse({
                    'text': 'Redirecting to print page ...',
                    'error': False,
                    'redirect': reverse('proforma_invoice_print_version', kwargs={'pk': estimate.id})
                })
        except ProformaInvoice.DoesNotExist:
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class ProformaInvoicePrintView(LoginRequiredMixin, ProfileMixin, TemplateView):

    template_name = 'estimates/print.html'

    def get_estimate(self):
        return ProformaInvoice.objects.filter(id=self.kwargs['pk']).first()

    def get_context_data(self, **kwargs):
        context = super(ProformaInvoicePrintView, self).get_context_data(**kwargs)
        estimate = self.get_estimate()
        context['estimate'] = estimate
        context['cols'] = 9
        return context

