from django.apps import AppConfig


class ProformaConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.proforma'
