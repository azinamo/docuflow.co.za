from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.ProformaInvoiceView.as_view(), name='sales_proforma_invoice_index'),
    path('create/', csrf_exempt(views.CreateProformaInvoiceView.as_view()), name='create_proforma_invoice'),
    path('edit/<int:pk>/', views.EditProformaInvoiceView.as_view(), name='edit_proforma_invoice'),
    path('add/invoice/items/', views.AddProformaInvoiceItemRowView.as_view(), name='add_proforma_invoice_items'),
    path('<int:pk>/detail/', views.ProformaInvoiceDetailView.as_view(), name='proforma_invoice_detail'),
    path('create/copy/', csrf_exempt(views.CreateEstimateCopiesView.as_view()), name='copy_proforma_invoices'),
    path('remove/<int:pk>/', views.RemoveProformaInvoiceView.as_view(), name='remove_proforma_invoice'),
    path('remove/estimates/', csrf_exempt(views.RemoveEstimatesView.as_view()), name='remove_proforma_invoices'),
    path('create/<int:pk>/invoice/', views.CreateInvoiceFromProformaInvoiceView.as_view(),
         name='create_invoice_proforma_invoice'),
    path('create/invoices/', csrf_exempt(views.CreateInvoiceFromProformaInvoiceView.as_view()),
         name='make_proforma_invoices'),
    path('print/', csrf_exempt(views.PrintEstimateView.as_view()), name='print_proforma_invoice'),
    path('<int:pk>/print/', views.ProformaInvoicePrintView.as_view(), name='proforma_invoice_print_version')
]
