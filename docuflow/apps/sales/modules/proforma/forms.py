from django import forms
from django.urls import reverse_lazy

from docuflow.apps.sales.models import ProformaInvoice, ProformaInvoiceItem
from docuflow.apps.period.models import Period, Year
from docuflow.apps.customer.models import Customer
from docuflow.apps.company.models import VatCode
from docuflow.apps.sales.enums import EstimateStatus, InvoiceItemType
from docuflow.apps.invoice.models import InvoiceType

from docuflow.apps.sales.forms import BaseSalesInvoiceForm, BaseSalesInventoryForm, BaseSalesDescriptionForm, BaseSalesAccountForm


class CreateProformaInvoiceForm(BaseSalesInvoiceForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.company)

    telephone = forms.CharField(required=False)
    customer_vat_number = forms.CharField(required=False)
    comment = forms.CharField(widget=forms.Textarea(), required=False, label='Comment')

    class Meta:
        model = ProformaInvoice
        exclude = ('notes', 'created_at', 'created_by', 'status', 'delivery_to', 'postal', 'invoice_id', 'invoice_type')

        widgets = {
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(CreateProformaInvoiceForm, self).clean()

        date = cleaned_data['invoice_date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('invoice_date', 'Date is required')

        print(f"Invoice date is {date} and period is {period.from_date} => {period.to_date}")

        if period and date:
            if not period.is_valid(date):
                self.add_error('invoice_date', 'Period and payment date do not match')

        return cleaned_data

    def get_invoice_type(self, company):
        return InvoiceType.objects.get_proforma_invoice_type(company)

    def add_addresses(self, invoice, request, customer=None):
        postal_address = {}
        delivery_address = {}
        for k, v in request.POST.items():
            if v != '':
                if k[0:7] == 'postal_':
                    postal_key = k[7:]
                    postal_address[postal_key] = v
                if k[0:9] == 'delivery_':
                    delivery_key = k[9:]
                    delivery_address[delivery_key] = v
        delivery_address['is_pick_up'] = self.cleaned_data.get('is_pick_up', False)
        if customer:
            if 'country' in postal_address and len(postal_address['country']) > 0:
                customer.save_address(postal_address, 2)
            if 'country' in delivery_address and len(delivery_address['country']) > 0:
                customer.save_address(delivery_address, 0)
        invoice.delivery_to = delivery_address
        invoice.postal = postal_address
        invoice.save()

    def create_customer(self, invoice):
        customer = Customer.objects.create_cash_customer(invoice.branch.company, invoice.customer_name,
                                                         **{'phone_number': self.cleaned_data['telephone'],
                                                            'vat_number': self.cleaned_data['customer_vat_number']
                                                            })
        if customer:
            invoice.customer = customer
            invoice.save()
        return customer

    def execute(self, branch, profile, role, request):
        invoice = self.save(commit=False)
        invoice.invoice_type = self.get_invoice_type(branch.company)
        invoice.profile = profile
        invoice.created_by = profile
        invoice.role = role
        invoice.save()
        if invoice:
            customer = None
            if invoice.customer.is_default:
                customer = self.create_customer(invoice)
                invoice.refresh_from_db()
            self.add_addresses(invoice, request, customer)

            self.create_invoice_items(invoice, request)

            if invoice.customer:
                invoice.vat_on_customer_discount = invoice.cal_vat_on_discount(invoice.customer_discount)
            invoice.save()

    def create_invoice_items(self, invoice, request):
        counter = 0
        for field, value in request.POST.items():
            if field.startswith('inventory_item', 0, 14):
                row_id = field[15:]
                data = {
                    'inventory': request.POST.get(f'inventory_item_{row_id}', 0),
                    'quantity': request.POST.get(f'quantity_{row_id}', 0),
                    'price': request.POST.get(f'price_{row_id}', 0),
                    'discount': request.POST.get(f'discount_percentage_{row_id}', 0),
                    'vat_code': request.POST.get(f'vat_code_{row_id}'),
                    'vat': request.POST.get(f'vat_percentage_{row_id}'),
                    'total_amount': request.POST.get(f'total_price_including_{row_id}'),
                    'invoice': invoice
                }
                invoice_item_form = ProformaInvoiceInventoryItemForm(data=data, branch=invoice.branch)
                if invoice_item_form.is_valid():
                    invoice_item_form.execute(invoice)

                counter += 1
            elif field.startswith('account', 0, 7):
                row_id = field[8:]
                data = {
                    'account': request.POST.get(f'account_{row_id}', 0),
                    'quantity': request.POST.get(f'quantity_{row_id}', 0),
                    'price': request.POST.get(f'price_{row_id}', 0),
                    'discount': request.POST.get(f'discount_percentage_{row_id}', 0),
                    'vat_code': request.POST.get(f'vat_code_{row_id}'),
                    'vat': request.POST.get(f'vat_percentage_{row_id}'),
                    'total_amount': request.POST.get(f'total_price_including_{row_id}'),
                    'description': request.POST.get(f'description_{row_id}'),
                    'invoice': invoice
                }
                invoice_item_form = ProformaInvoiceAccountItemForm(data=data, branch=invoice.branch)
                if invoice_item_form.is_valid():
                    invoice_item_form.execute(invoice)
                counter += 1
            elif field.startswith('item_description', 0, 16):
                row_id = field[17:]
                data = {
                    'description': self.request.POST.get(f'item_description_{row_id}', 0),
                    'invoice': invoice
                }
                invoice_item_form = ProformaInvoiceTextItemForm(data=data)
                if invoice_item_form.is_valid():
                    invoice_item_form.execute(invoice)
                counter += 1
        if counter == 0:
            raise ValueError(
                'No invoice lines were selected, please ensure you have at least 1 line in your invoice')


class ProformaInvoiceInventoryItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        branch = kwargs.pop('branch')
        super(ProformaInvoiceInventoryItemForm, self).__init__(*args, **kwargs)

        self.fields['inventory'].queryset = self.fields['inventory'].queryset.filter(branch=branch)
        self.fields['vat'].queryset = VatCode.objects.output().filter(company=branch.company)

    class Meta:
        model = ProformaInvoiceItem
        fields = ('inventory', 'quantity', 'unit', 'price', 'net_price', 'vat_code', 'vat', 'discount')

    def execute(self, invoice):
        inventory_invoice_item = self.save(commit=False)
        inventory_invoice_item.item_type = InvoiceItemType.INVENTORY
        if self.cleaned_data['vat_code']:
            inventory_invoice_item.vat = self.cleaned_data['vat_code'].percentage
        inventory_invoice_item.unit = self.cleaned_data['inventory'].unit
        inventory_invoice_item.invoice_id = invoice.id
        inventory_invoice_item.save()
        return inventory_invoice_item


class ProformaInvoiceAccountItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        branch = kwargs.pop('branch')
        super(ProformaInvoiceAccountItemForm, self).__init__(*args, **kwargs)

        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=branch.company)
        self.fields['vat'].queryset = VatCode.objects.output().filter(company=branch.company)

    class Meta:
        model = ProformaInvoiceItem
        fields = ('account', 'description', 'quantity', 'unit', 'price', 'net_price', 'vat_code', 'vat', 'discount')

    def execute(self, invoice):
        account_line = self.save(commit=False)
        account_line.item_type = InvoiceItemType.GENERAL
        if self.cleaned_data['vat_code']:
            account_line.vat = self.cleaned_data['vat_code'].percentage
        account_line.invoice = invoice
        account_line.save()
        return account_line


class ProformaInvoiceTextItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProformaInvoiceTextItemForm, self).__init__(*args, **kwargs)

    class Meta:
        model = ProformaInvoiceItem
        fields = ('description', )

    def execute(self, invoice):
        description_line = self.save(commit=False)
        description_line.item_type = InvoiceItemType.TEXT
        description_line.invoice = invoice
        description_line.save()
        return description_line
