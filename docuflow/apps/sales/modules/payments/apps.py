from django.apps import AppConfig


class PaymentsConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.payments'
