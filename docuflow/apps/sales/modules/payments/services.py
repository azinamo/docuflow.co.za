import logging
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.urls import reverse

from docuflow.apps.common.enums import Module
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.customer.models import Ledger as CustomerLedger, LedgerPayment
from docuflow.apps.sales.enums import ReceiptType, InvoiceStatus
from docuflow.apps.sales.models import Invoice, Receipt, Tracking, InvoiceReceipt, SundryReceiptAllocation
from .exceptions import NotAllocatedAmountException

logger = logging.getLogger(__name__)


class CreateSundryPayment:

    def __init__(self, company, branch, profile, role, form, post_request, balance=0):
        logger.info("CreateSundryPayment")
        self.company = company
        self.branch = branch
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request
        self.balance = self.get_balance_amount(balance)

    def get_balance_amount(self, amount):
        if amount != '':
            return float(amount)
        return 0

    def monitize(self, string):
        return Decimal(string.replace(',', ''))

    def get_comment(self):
        return self.post_request.get('comment', None)

    def has_complete_payment(self):
        logger.info("Has complete payment {} -->".format(self.balance))
        return True
        if self.balance:
            comment = self.get_comment()
            if self.balance > 0 and not comment:
                raise NotAllocatedAmountException('You have not allocated the amount of payment, should it'
                                                  ' be left unallocated')
        return True

    def create(self):
        logger.info("Create payment receipt")
        receipt = self.form.save(commit=False)
        receipt.receipt_type = ReceiptType.SUNDRY
        receipt.branch = self.branch
        receipt.balance = self.balance
        receipt.role = self.role
        receipt.profile = self.profile
        receipt.company = self.company
        receipt.notes = self.get_comment()
        receipt.save()
        logger.info("Done creating payment receipt {}".format(receipt))

        # create_receipt = CreateInvoiceReceipt(invoice, invoice.branch, payment_date, receipt_amount,
        #                                       payment_method_amounts)
        # receipt = create_receipt.execute()

        content_type = ContentType.objects.filter(
            app_label='sales',
            model='receipt'
        ).first()

        CustomerLedger.objects.create(
            customer=receipt.customer,
            period=receipt.period,
            quantity=1,
            amount=self.balance,
            module=Module.SALES,
            created_by=self.profile,
            date=receipt.date,
            status=LedgerStatus.PENDING,
            text=receipt.__str__(),
            content_type=content_type,
            object_id=receipt.id
        )

        return receipt

    def execute(self):
        logger.info("Execute")
        if self.has_complete_payment():
            receipt = self.create()
            if receipt:
                self.create_invoice_payments(receipt)

    def create_invoice_payments(self, receipt):
        logger.info("start creating invoice payments")
        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        for field in self.post_request:
            if field.startswith('invoice_amount', 0, 14):
                invoice_id = field[15:]

                allocated = 0
                allocate_id = "allocate_{}".format(invoice_id)
                if allocate_id in self.post_request and self.post_request[allocate_id]:
                    allocated = Decimal(self.post_request[allocate_id])

                invoice = Invoice.objects.prefetch_related(
                    'receipts'
                ).filter(
                    id=int(invoice_id)
                ).first()

                if invoice and allocated:
                    invoice_receipt = InvoiceReceipt.objects.create(
                        invoice_id=invoice_id,
                        receipt=receipt,
                        amount=allocated
                    )
                    if invoice_receipt:
                        open_amount = invoice.open_amount - invoice_receipt.amount
                        if open_amount > 0:
                            invoice.status = InvoiceStatus.PARTIALLY_PAID
                        else:
                            invoice.status = InvoiceStatus.PAID_NOT_PRINTED
                        invoice.open_amount = open_amount
                        invoice_receipt.save()

                    Tracking.objects.create(
                        content_type=content_type,
                        object_id=invoice.id,
                        comment=f"Payment of {invoice_receipt.amount} made by {str(receipt)}",
                        profile=self.profile,
                        role=self.role,
                        data={'link_url': reverse('receipt_detail', kwargs={'pk': receipt.id})}
                    )


class AllocateSundryPayment:

    def __init__(self, company, branch, profile, role, form, post_request):
        logger.info("AllocateSundryPayment")
        self.branch = branch
        self.company = company
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request
        logger.info("Complete --- AllocateSundryPayment")

    def execute(self):
        logger.info("EXECUTE")
        with transaction.atomic():
            receipt = self.create()
            if receipt:
                for field, value in self.post_request.items():
                    if value != '':
                        if field.startswith('payment_allocated_', 0, 18):
                            self.create_receipt_payments(receipt, field, value)
                        if field.startswith('invoice_amount_paid', 0, 20):
                            self.create_invoice_receipts(receipt, field, value)
                        if field.startswith('ledger_payment_allocated', 0, 24):
                            self.create_ledger_payment(receipt, field, value)
            return receipt

    def create(self):
        logger.info("Create allocation receipt")
        receipt = self.form.save(commit=False)
        receipt.role = self.role
        receipt.branch = self.branch
        receipt.profile = self.profile
        receipt.company = self.company
        receipt.payment_type = ReceiptType.ALLOCATION
        receipt.save()
        logger.info("Done creating allocation receipt {}".format(receipt))

        content_type = ContentType.objects.filter(
            app_label='sales',
            model='receipt'
        ).first()

        CustomerLedger.objects.create(
            customer=receipt.customer,
            period=receipt.period,
            quantity=1,
            amount=receipt.amount,
            module=Module.SALES,
            created_by=self.profile,
            date=receipt.date,
            status=LedgerStatus.PENDING,
            text=str(receipt),
            content_type=content_type,
            object_id=receipt.id
        )

        return receipt

    def create_receipt_payments(self, parent_receipt, field, value):
        logger.info("CREATE receipt payments")
        payment_id = field[18:]
        receipt = Receipt.objects.filter(id=payment_id).first()
        if receipt and value:
            payment_amount = Decimal(value) * -1
            child_payment = SundryReceiptAllocation.objects.create(
                parent=parent_receipt,
                receipt=receipt,
                amount=payment_amount
            )
            if child_payment:
                receipt.balance = receipt.balance - child_payment.amount
                receipt.save()

    def create_invoice_receipts(self, receipt, field, value):
        logger.info("CREATE receipt invoices")
        invoice_id = field[20:]
        amount_allocated = self.post_request.get('invoice_amount_paid_{}'.format(invoice_id))
        if amount_allocated:
            amount_allocated = Decimal(amount_allocated)
            invoice_payment = InvoiceReceipt.objects.create(
                receipt=receipt,
                invoice_id=invoice_id,
                amount=amount_allocated
            )
            if invoice_payment:
                invoice = invoice_payment.invoice

                invoice.open_amount = invoice.open_amount - invoice_payment.amount
                invoice.save()

    def create_ledger_payment(self, parent_receipt, field, value):
        ledger_id = field[25:]
        logger.info("CREATE ledger payments {} on ledger {}".format(value, ledger_id))
        ledger = CustomerLedger.objects.filter(
            id=ledger_id
        ).first()
        if ledger and value:
            payment_amount = Decimal(value) * -1
            ledger_payment = LedgerPayment.objects.create(
                payment=parent_receipt,
                ledger=ledger,
                amount=payment_amount
            )
            if ledger_payment:
                ledger.balance = ledger.balance - ledger_payment.amount
                ledger.save()
