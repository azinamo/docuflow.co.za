import logging
import pprint
from datetime import datetime

from django.views.generic import FormView, View, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import reverse
from django.db.models import Count

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.sales.models import Invoice, Receipt
from docuflow.apps.customer.models import Customer, Ledger
from docuflow.apps.company.models import VatCode
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.sales.enums import ReceiptType
from .forms import PaymentForm, SundryPaymentForm, UnAllocatedAmountForm, AllocateSundryPaymentForm
from .exceptions import InvalidPeriodAccountingDateException, NotAllocatedAmountException
from .services import CreateSundryPayment, AllocateSundryPayment

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


# Create your views here.
class PaymentsView(TemplateView):

    template_name = 'payments/index.html'

    def get_invoice(self):
        return Invoice.objects.prefetch_related('payments').filter().first()

    def get_invoice_payments(self):
        return Invoice.objects.select_related('invoice').filter()

    def get_context_data(self, **kwargs):
        context = super(PaymentsView, self).get_context_data(**kwargs)
        context['invoices'] = []
        return context


class CreatePaymentView(ProfileMixin, TemplateView):

    template_name = 'payments/create.html'

    def get_invoices(self):
        return Invoice.objects.annotate(
            count_payments=Count('receipts')
        ).filter(
            branch__company=self.get_company(),
            customer__is_default=False,
            count_payments=0
        )

    def get_customer_invoices(self):
        invoices = self.get_invoices()
        customer_invoices = {}
        total = 0
        for invoice in invoices:
            total += invoice.outstanding
            if invoice.customer:
                if invoice.customer in customer_invoices:
                    customer_invoices[invoice.customer]['invoices'].append(invoice)
                    customer_invoices[invoice.customer]['total'] += invoice.outstanding
                else:
                    customer_invoices[invoice.customer] = {'invoices': [invoice], 'total': invoice.outstanding}
        return customer_invoices

    def get_context_data(self, **kwargs):
        context = super(CreatePaymentView, self).get_context_data(**kwargs)
        context['customers_invoices'] = self.get_customer_invoices()
        return context


class PayCustomerInvoicesView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        customer_id = self.request.GET.get('parent_id', None)
        if customer_id:
            invoice_id = self.request.GET.get('invoice_id', None)
            if invoice_id:
                redirect_url = reverse('create_customer_invoices_receipt', kwargs={
                    'customer_id': customer_id,
                    'invoice_id': invoice_id
                })
                self.request.session['customer_invoices'] = [invoice_id]
            else:
                selected_invoices = self.request.GET.get('invoices', None)
                selected_payments = self.request.GET.get('payments', None)
                if selected_payments:
                    self.request.session['selected_payments'] = selected_payments.split(',')
                    # invoices = Invoice.objects.filter(id__in=selected_invoices.split(','))
                    # payments = Payment.objects.filter(id__in=selected_payments.split(','))
                else:
                    self.request.session['selected_payments'] = []
                redirect_url = reverse('create_customer_invoices_receipt', kwargs={
                    'customer_id': customer_id
                })
                self.request.session['customer_invoices'] = selected_invoices.split(',')

            return JsonResponse({'text': 'Paying invoice ....',
                                 'error': False,
                                 'alert': True,
                                 'url': redirect_url
                                 })
        else:
            return JsonResponse({'text': 'No customer selected for payment',
                                 'error': True
                                 })


class CreateCustomerInvoicesPaymentView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'payments/create_payment.html'

    def get_default_vat_code(self):
        vat_code = VatCode.objects.filter(company=self.get_company(), is_default=True).first()
        return vat_code

    def get_initial(self):
        initial = super(CreateCustomerInvoicesPaymentView, self).get_initial()
        initial['payment_date'] = datetime.now().strftime('%Y-%m-%d')
        vat_code = self.get_default_vat_code()
        if vat_code:
            initial['vat_code'] = vat_code
        return initial

    def get_form_class(self):
        return PaymentForm

    def get_customer_invoices(self, customer, payments=None):
        if not payments:
            payments = []
        invoice_ids = self.request.GET.get('invoices', '')
        invoice_ids = map(lambda x: int(x), invoice_ids.split(','))
        invoices = Invoice.objects.filter(customer=customer, id__in=invoice_ids)

        customer_payments = {'total': 0, 'total_discount': 0, 'invoices': [], 'payments': []}
        for invoice in invoices.all():
            customer_payments['total'] += invoice.open_amount
            customer_payments['invoices'].append(invoice)
        return customer_payments

    def get_customer(self):
        return Customer.objects.get(pk=int(self.request.GET.get('parent_id', 0)))

    def get_form_kwargs(self):
        kwargs = super(CreateCustomerInvoicesPaymentView, self).get_form_kwargs()
        company = self.get_company()
        year = self.get_year()
        kwargs['company'] = company
        kwargs['year'] = year
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateCustomerInvoicesPaymentView, self).get_context_data(**kwargs)
        customer = self.get_customer()
        # payments = self.get_supplier_payments()
        customer_invoices = self.get_customer_invoices(customer)
        context['invoice_id'] = self.kwargs.get('invoice_id', None)
        context['customer'] = customer
        context['customer_invoices'] = customer_invoices
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Supplier payment could not be saved, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(CreateCustomerInvoicesPaymentView, self).form_invalid(form)

    # def create_invoice_payment(self, payment, invoice_id, invoice_statuses):
    #     discount = 0
    #     discount_ref = "discount_invoice_{}".format(invoice_id)
    #     if discount_ref in self.request.POST:
    #         discount = float(self.request.POST['invoice_discount_{}'.format(invoice_id)])
    #
    #     invoice = get_object_or_None(Invoice, pk=int(invoice_id))
    #     if invoice:
    #         if invoice.status.slug == 'final-signed':
    #             invoice.status = invoice_statuses.get('paid_not_posted_status')
    #         elif invoice.status.slug == 'end-flow-printed':
    #             invoice.status = invoice_statuses.get('paid_not_printed_status')
    #
    #         invoice.date_paid = payment.payment_date
    #         invoice.payment_date = payment.payment_date
    #         if discount > 0:
    #             invoice.discount = Decimal(discount)
    #         invoice.save()
    #
    #         InvoicePayment.objects.create(
    #             invoice=invoice,
    #             payment=payment,
    #             discount=Decimal(discount),
    #             allocated=invoice.total_amount
    #         )
    #
    #         invoice.open_amount = invoice.calculate_open_amount()
    #         invoice.save()
    #
    #         note = "Payment made by {} on {}".format(payment.payment_method,
    #                                                  payment.payment_date)
    #         link_url = reverse('payment_advice', kwargs={'payment_id': payment.id})
    #         invoice.log_activity(self.request.user, self.request.session['role'], note,
    #                              Comment.LINK, None, None, link_url)
    #
    # def create_child_payment(self, parent_payment, child_payment_id):
    #     payment = Payment.objects.filter(id=child_payment_id).first()
    #     if payment:
    #         child_payment = ChildPayment.objects.create(
    #             parent=parent_payment,
    #             payment=payment,
    #             amount=payment.available_amount
    #         )

    def form_valid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': False,
                                 'text': 'Payment successfully saved'
                                 })
        #     supplier = self.get_supplier()
        #     try:
        #         with transaction.atomic():
        #             invoice_statuses = {'paid_status': get_object_or_None(Status, slug='paid'),
        #                                 'paid_not_posted_status': get_object_or_None(Status,
        #                                                                              slug='paid-not-account-posted'),
        #                                 'paid_not_printed_status': get_object_or_None(Status,
        #                                                                               slug='paid-not-printed')
        #                                 }
        #
        #             payment = form.save(commit=False)
        #             payment_date = form.cleaned_data['payment_date']
        #             if payment_date:
        #                 if 'period' in form.cleaned_data and form.cleaned_data['period']:
        #                     period = form.cleaned_data['period']
        #                     if not period.is_valid(payment_date):
        #                         raise InvalidPeriodAccountingDateException('Period and payment date do not match.')
        #             payment.role_id = self.request.session['role']
        #             payment.profile_id = self.request.session['profile']
        #             payment.company = supplier.company
        #             payment.supplier = supplier
        #             payment.save()
        #
        #             if payment:
        #                 for field in self.request.POST:
        #                     if field.startswith('invoice_amount', 0, 14):
        #                         invoice_id = field[15:]
        #                         self.create_invoice_payment(payment, invoice_id, invoice_statuses)
        #
        #                     if field.startswith('payment_amount', 0, 14):
        #                         payment_id = field[15:]
        #                         self.create_child_payment(payment, payment_id)
        #
        #         invoice__id = self.kwargs.get('invoice_id', None)
        #         if invoice__id:
        #             return JsonResponse({'error': False,
        #                                  'text': 'Payment successfully saved',
        #                                  'redirect': reverse('invoice_detail', kwargs={'pk': invoice__id})
        #                                  })
        #         else:
        #             return JsonResponse({'error': False,
        #                                  'text': 'Payment successfully saved',
        #                                  'redirect': reverse('payment_advice', kwargs={'payment_id': payment.id})
        #                                  })
        #     except InvalidPeriodAccountingDateException as ex:
        #         return JsonResponse({'error': True,
        #                              'text': ex.__str__(),
        #                              'details': 'Error occurred {}'.format(ex.__str__()),
        #                              })
        #     except Exception as exception:
        #         return JsonResponse({'error': True,
        #                              'text': 'Unexpected error occurred',
        #                              'details': 'Error occurred {}'.format(exception),
        #                              })
        # else:
        #     return HttpResponseRedirect(reverse('invoice_detail', kwargs={'pk': self.kwargs['pk']}))


class UpdatePaymentView(TemplateView):

    template_name = 'payments/invoice_payment.html'

    def get_invoice(self):
        return Invoice.objects.prefetch_related('payments').filter(id=self.kwargs['invoice_id']).first()

    def get_invoice_payments(self):
        return Invoice.objects.select_related('invoice').filter(invoice_id=self.kwargs['invoice_id'])

    def get_context_data(self, **kwargs):
        context = super(UpdatePaymentView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['invoice'] = invoice
        context['invoice_payments'] = invoice.payments.all()
        return context


class CreateSundryPaymentView(ProfileMixin, FormView):
    template_name = 'payments/sundry.html'

    def get_context_data(self, **kwargs):
        context = super(CreateSundryPaymentView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        initial = super(CreateSundryPaymentView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_class(self):
        return SundryPaymentForm

    def get_form_kwargs(self):
        kwargs = super(CreateSundryPaymentView, self).get_form_kwargs()
        company = self.get_company()
        year = self.get_year()
        kwargs['company'] = company
        kwargs['year'] = year
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            comment = self.request.POST.get('comment', None)
            is_alert = True
            if comment:
                is_alert = False
            logger.info("Comment is {} thus alert is {}".format(comment, is_alert))
            return JsonResponse({'error': True,
                                 'text': 'Error occurred creating the sundry payment, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': is_alert
                                 })
        return super(CreateSundryPaymentView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                company = self.get_company()
                branch = self.get_branch()
                profile = Profile.objects.filter(id=self.request.session['profile']).first()
                role = Role.objects.filter(id=self.request.session['role']).first()
                balance = self.request.POST.get('balance', 0)

                create_sundry = CreateSundryPayment(company, branch, profile, role, form, self.request.POST, balance)
                create_sundry.execute()

                return JsonResponse({'error': False,
                                     'text': 'Sundry payment successfully saved',
                                     'reload': True
                                     })
            except NotAllocatedAmountException as ex:
                return JsonResponse({'text': ex.__str__(),
                                     'details': 'Error occurred {}'.format(ex.__str__()),
                                     'warning': True,
                                     'url': reverse('sundry_receipt_not_allocated')
                                     })
            except InvalidPeriodAccountingDateException as ex:
                return JsonResponse({'error': True,
                                     'text': ex.__str__(),
                                     'details': 'Error occurred {}'.format(ex.__str__()),
                                     'errors': {'period': ['Period and payment date do not match']}
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occured, please try again',
                                     'details': 'Error saving the invoice {}'.format(exception.__str__()),
                                     'alert': True
                                     })
        return HttpResponse('Not allowed')


class NotAllocateSundryPaymentView(ProfileMixin, FormView):
    template_name = 'payments/unallocated.html'
    form_class = UnAllocatedAmountForm


class AllocateSundryPaymentView(ProfileMixin, FormView):
    template_name = 'payments/allocate_sundry.html'

    def get_initial(self):
        initial = super(AllocateSundryPaymentView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_class(self):
        return AllocateSundryPaymentForm

    def get_form_kwargs(self):
        kwargs = super(AllocateSundryPaymentView, self).get_form_kwargs()
        company = self.get_company()
        year = self.get_year()
        kwargs['company'] = company
        kwargs['year'] = year
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AllocateSundryPaymentView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred creating the sundry payment, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(AllocateSundryPaymentView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                company = self.get_company()
                branch = self.get_branch()
                role = Role.objects.filter(id=self.request.session['role']).first()
                profile = self.get_profile()
                # parent_payment.payment_date = datetime.now().date()
                allocate_sundry = AllocateSundryPayment(company, branch, profile, role, form, self.request.POST)
                receipt = allocate_sundry.execute()

                kwargs = {'pk': receipt.id}
                redirect_url = "{}?redirect_url={}".format(reverse('receipt_detail', kwargs=kwargs),
                                                           reverse('sundry_payment'))
                return JsonResponse({'error': False,
                                     'text': 'Sundry payment allocated successfully',
                                     'redirect': redirect_url
                                     })
            except NotAllocatedAmountException as ex:
                return JsonResponse({'error': True,
                                     'text': str(ex),
                                     'details': f'Error occurred {str(ex)}',
                                     'url': reverse('sundry_payment_not_allocated')
                                     })
            except InvalidPeriodAccountingDateException as ex:
                return JsonResponse({'error': True,
                                     'text': str(ex),
                                     'details': f'Error occurred {str(ex)}',
                                     'errors': {'period': ['Period and payment date do not match']}
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': f'Error saving the invoice {str(exception)}',
                                     'alert': True
                                     })
        # return HttpResponse('Not allowed')


class CustomerInvoicesView(ProfileMixin, TemplateView):
    template_name = 'payments/sundry_invoices.html'

    def get_context_data(self, **kwargs):
        context = super(CustomerInvoicesView, self).get_context_data(**kwargs)
        company = self.get_company()
        branch = self.get_branch()
        invoices = Invoice.objects.customer(self.request.GET.get('customer_id')).not_paid()
        sundry_payments = Receipt.objects.filter(
            branch=branch,
            receipt_type=ReceiptType.SUNDRY
        )
        logger.info("Customer invoices")
        logger.info(invoices)
        context['company'] = company
        context['payments'] = sundry_payments
        context['invoices'] = invoices
        return context


class CustomerAllocateSundryView(ProfileMixin, TemplateView):
    template_name = 'payments/allocate_sundry_invoices.html'

    def get_requested_customer(self):
        return self.request.GET.get('customer_id', None)

    def get_ledger_payments(self):
        journal_filters = {}
        journal_filters['customer_id'] = self.get_requested_customer()
        logger.info("Ledger payments")
        logger.info(journal_filters)
        general_ledgers = Ledger.objects.with_available_payments(journal_filters)
        logger.info(general_ledgers)
        return general_ledgers

    def get_payments(self, branch):
        journal_filters = {'branch': branch}
        journal_filters['customer_id'] = self.get_requested_customer()
        journal_filters['receipt_type'] = ReceiptType.SUNDRY
        journal_filters['balance__gt'] = 0
        return Receipt.objects.filter(**journal_filters)

    def get_invoices(self):
        return Invoice.objects.customer(
            self.request.GET.get('customer_id')
        ).not_paid()

    def get_context_data(self, **kwargs):
        context = super(CustomerAllocateSundryView, self).get_context_data(**kwargs)
        company = self.get_company()
        branch = self.get_branch()
        invoices = self.get_invoices()
        sundry_payments = self.get_payments(branch)
        ledger_payments = self.get_ledger_payments()
        logger.info("Customer invoices")
        logger.info(sundry_payments)
        context['company'] = company
        context['payments'] = sundry_payments
        context['ledger_payments'] = ledger_payments
        context['invoices'] = invoices
        return context
