from django import forms
from django.urls import reverse_lazy

from docuflow.apps.sales.models import Receipt, Invoice
from docuflow.apps.period.models import Period
from docuflow.apps.customer.models import Customer
from docuflow.apps.customer.fields import CustomerModelChoiceField


class PaymentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        year = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        if 'year' in kwargs:
            year = kwargs.pop('year')
            print("Year for periods is ".format(year))
        super(PaymentForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['period'].queryset = Period.objects.open(company, year=year).order_by('-period')
            self.fields['period'].empty_label = None

    period = forms.ModelChoiceField(required=True, label='Period', queryset=None)
    amount = forms.DecimalField(required=False, label='Total of above invoices', widget=forms.TextInput())
    discount = forms.DecimalField(required=False, label='Discount total as selected', widget=forms.TextInput())

    class Meta:
        model = Receipt
        fields = '__all__'
        widgets = {
            'date': forms.TextInput(attrs={'class': 'datepicker'}),
            'notes': forms.Textarea(attrs={'cols': 5, 'rows': 10}),
        }


class SundryPaymentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        year = None
        if 'company' in kwargs:
            company = kwargs.pop('company')
        if 'year' in kwargs:
            year = kwargs.pop('year')
        super(SundryPaymentForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['period'].queryset = Period.objects.open(company, year).order_by('-period')
            self.fields['period'].empty_label = None
            self.fields['customer'].queryset = Customer.objects.invoicable().filter(company=company)

    period = forms.ModelChoiceField(required=True, label='Period', queryset=None)

    class Meta:
        model = Receipt
        fields = ('customer', 'period', 'date', 'amount', 'notes', 'reference', 'attachment', 'balance')
        widgets = {
            'date': forms.TextInput(attrs={'class': 'datepicker'}),
            'notes': forms.Textarea(attrs={'cols': 5, 'rows': 10}),
            'customer': forms.Select(attrs={'class': 'chosen-select',
                                            'data-customer-invoices-url': reverse_lazy('customer_payment_invoices')
                                            }),
        }

    def clean(self):
        cleaned_data = super(SundryPaymentForm, self).clean()

        if not cleaned_data['customer']:
            self.add_error('customer', 'Customer is required')

        payment_date = cleaned_data['date']
        if payment_date:
            if 'period' in cleaned_data and cleaned_data['period']:
                period = cleaned_data['period']
                if not period.is_valid(payment_date):
                    self.add_error('date', 'Date and period do not match.')
                    self.add_error('period', 'Period and payment date do not match.')

        return cleaned_data


class AllocateSundryPaymentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        if 'company' in kwargs:
            self.company = kwargs.pop('company')
        if 'year' in kwargs:
            self.year = kwargs.pop('year')
        super(AllocateSundryPaymentForm, self).__init__(*args, **kwargs)
        if self.company:
            self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
            self.fields['period'].empty_label = None
            self.fields['customer'].queryset = Customer.objects.invoicable().filter(company=self.company)

    class Meta:
        model = Receipt
        fields = ('customer', 'period', 'date')
        widgets = {
            'date': forms.TextInput(attrs={'class': 'datepicker'}),
            'customer': forms.Select(attrs={'class': 'chosen-select',
                                            'data-customer-invoices-url': reverse_lazy('customer_allocate_payments')
                                            }),
        }


class UnAllocatedAmountForm(forms.Form):
    comment = forms.CharField(required=False, label='Reason', widget=forms.Textarea(attrs={'id': 'payment_comment',
                                                                                           'cols': 5, 'rows': 10}))

