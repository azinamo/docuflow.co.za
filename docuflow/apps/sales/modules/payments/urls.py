from django.urls import path

from . import views

urlpatterns = [
    path(r'', views.PaymentsView.as_view(), name='sales_payments_index'),
    path(r'create/', views.CreatePaymentView.as_view(), name='create_sales_payments'),
    path(r'pay/invoices/', views.CreateCustomerInvoicesPaymentView.as_view(), name='pay_customer_invoices'),
    path(r'update/', views.UpdatePaymentView.as_view(), name='update_sales_payments'),
    path(r'pay/customers/invoices/', views.PayCustomerInvoicesView.as_view(), name='pay_customers'),
    path(r'customers/invoices/', views.CustomerInvoicesView.as_view(), name='customer_payment_invoices'),
    path(r'sundry/create/', views.CreateSundryPaymentView.as_view(), name='create_sales_sundry_payments'),
    path(r'sundry/allocate/', views.AllocateSundryPaymentView.as_view(), name='allocate_sundry_payments'),
    path(r'allocate/customers/invoices/', views.CustomerAllocateSundryView.as_view(), name='customer_allocate_payments'),
    path(r'sundry/not/allocate/', views.NotAllocateSundryPaymentView.as_view(), name='sundry_receipt_not_allocated'),
]

