from django.urls import path

from . import views

urlpatterns = [
    path('', views.DeliveryView.as_view(), name='sales_delivery_index'),
    path('create/', views.CreateDeliveryView.as_view(), name='create_sales_delivery'),
    path('add/delivery/items/', views.AddDeliveryItemView.as_view(), name='add_delivery_items'),
    path('bulk/action/', views.BulkDeliveryActionView.as_view(), name='remove_delivery_note'),
    path('<int:pk>/detail/', views.DeliveryDetailView.as_view(), name='delivery_detail'),
    path('create/from/<int:estimate_id>/estimate/', views.CreateDeliveryFromEstimateView.as_view(),
         name='create_estimate_delivery'),
    path('estimate/(<int:estimate_id>/delivery/items/', views.EstimateDeliveryItemView.as_view(),
         name='estimate_delivery_items'),
    path('create/from/<int:back_order_id>/back-order/', views.CreateDeliveryFromBackOrderView.as_view(),
         name='create_back_order_delivery_note'),
    path('back-order/<int:back_order_id>/delivery/items/', views.BackOrderDeliveryItemView.as_view(),
         name='back_order_delivery_items'),
    path('create/delivery/from/<int:picking_slip_id>/pickingslip/', views.CreateDeliveryFromPickingSlipView.as_view(),
         name='create_picking_slip_delivery'),
    path('pickingslip/<int:picking_slip_id>/items/', views.PickingSlipDeliveryItemView.as_view(),
         name='picking_slip_delivery_items'),
    path('pickings/', views.PickingView.as_view(), name='delivery_pickings'),
    path('<int:pk>/tracking/', views.DeliveryTracking.as_view(), name='delivery_tracking'),
    path('<str:action>/', views.SingleDeliveryActionView.as_view(), name='single_delivery_action'),
]
