import logging

from django import forms
from django.conf import settings
from django.contrib.contenttypes.models import ContentType

from docuflow.apps.sales.models import Delivery, DeliveryItem, Tracking
from docuflow.apps.customer.models import Customer, Address
from docuflow.apps.customer.modules.address.forms import DeliveryAddressForm, PostalAddressForm
from docuflow.apps.period.models import Period
from docuflow.apps.sales.enums import InvoiceItemType
from docuflow.apps.sales.modules.backorder.services import CreateBackOrderFromSalesInvoice
from docuflow.apps.sales.forms import customer_select_data, BaseSalesInventoryForm, BaseSalesDescriptionForm
from django_countries.fields import CountryField


logger = logging.getLogger(__name__)


class DeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.year = kwargs.pop('year')
        super(DeliveryForm, self).__init__(*args, **kwargs)
        self.fields['customer'].widget = customer_select_data(self.branch.company)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.branch.company)
        self.fields['branch'].empty_label = None
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['delivery_term'].queryset = self.fields['delivery_term'].queryset.filter(company=self.branch.company)
        self.fields['delivery_method'].queryset = self.fields['delivery_method'].queryset.filter(company=self.branch.company)

    customer_name = forms.CharField(required=False)
    postal_street = forms.CharField(required=False)
    postal_city = forms.CharField(required=False)
    postal_province = forms.CharField(required=False)
    postal_postal_code = forms.CharField(required=False)
    postal_country = CountryField(default=settings.COUNTRY_CODE).formfield()
    address_id = forms.IntegerField(required=False, widget=forms.HiddenInput())
    delivery_address = forms.ChoiceField(required=False, choices=[])
    delivery_street = forms.CharField(required=False)
    delivery_city = forms.CharField(required=False)
    delivery_province = forms.CharField(required=False)
    delivery_postal_code = forms.CharField(required=False)
    delivery_country = CountryField(default=settings.COUNTRY_CODE).formfield()

    class Meta:
        model = Delivery
        fields = ('branch',  'period', 'date', 'customer', 'delivery_time', 'purchase_order', 'delivery_term',
                  'delivery_method', 'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount',
                  'customer_discount', 'line_discount', 'customer_name', 'postal_city', 'postal_country',
                  'postal_street', 'postal_postal_code', 'postal_province', 'delivery_city', 'delivery_street',
                  'delivery_postal_code', 'delivery_province', 'delivery_country', 'address_id')
        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'address_id': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not cleaned_data['customer']:
            self.add_error('customer', 'Customer is required')

        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        # if 'customer_name' in cleaned_data and not cleaned_data['customer_name']:
        #     self.add_error('customer_name', 'Customer name is required')
        # if 'postal_street' in cleaned_data and not cleaned_data['postal_street']:
        #     self.add_error('postal_street', 'Street address is required')
        # if 'postal_city' in cleaned_data and not cleaned_data['postal_city']:
        #     self.add_error('postal_city', 'City is required')
        # if 'postal_country' in cleaned_data and not cleaned_data['postal_country']:
        #     self.add_error('Postal country is required')
        # if 'delivery_street' in cleaned_data and not cleaned_data['delivery_street']:
        #     self.add_error('delivery_street', 'Delivery street address is required')
        # if 'delivery_city' in cleaned_data and not cleaned_data['delivery_city']:
        #     self.add_error('delivery_city', 'Delivery city is required')
        # if 'delivery_country' in cleaned_data and not cleaned_data['delivery_country']:
        #     self.add_error('delivery_country', 'Delivery country is required')

        return cleaned_data

    def save(self, commit=True):
        delivery = super().save(commit=False)
        delivery.profile = self.profile
        delivery.role_id = self.role
        delivery.save()

        if delivery.customer:
            delivery.vat_on_customer_discount = delivery.cal_vat_on_discount(delivery.customer_discount)

        if delivery.customer.is_default:
            customer = Customer.objects.create_cash_customer(self.branch.company, self.request.POST.get('customer_name'))
            if customer:
                delivery.customer = customer
                delivery.save()
            delivery.refresh_from_db()
        self.create_postal_address(delivery)
        self.create_delivery_address(delivery)

        back_order_items = []
        for field, value in self.request.POST.items():
            if field.startswith('inventory', 0, 9):
                if value:
                    row_id = field[10:]
                    data = {
                        'inventory': self.request.POST.get(f'inventory_{row_id}'),
                        'ordered': self.request.POST.get(f'ordered_{row_id}'),
                        'quantity': self.request.POST.get(f'quantity_{row_id}'),
                        'discount': self.request.POST.get(f'discount_percentage_{row_id}', 0),
                        'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
                        'vat': self.request.POST.get(f'vat_percentage_{row_id}'),
                        'vat_amount': self.request.POST.get(f'vat_amount_{row_id}'),
                        'net_price': self.request.POST.get(f'total_price_excluding_{row_id}'),
                        'price': self.request.POST.get(f'price_{row_id}'),
                        'unit': self.request.POST.get(f'unit_id_{row_id}'),
                        'delivery': delivery
                    }
                    back_order = self.request.POST.get(f'back_order_{row_id}', False)
                    inventory_form = InventoryDeliveryItemForm(data=data, branch=self.branch)
                    if inventory_form.is_valid():
                        delivery_item = inventory_form.save(commit=False)
                        delivery.save()
                        if back_order:
                            back_order_qty = delivery_item.ordered - delivery_item.quantity
                            if back_order_qty > 0:
                                back_order_items.append({'item': delivery_item, 'quantity': back_order_qty})
            elif field.startswith('item_description', 0, 16):
                if value:
                    row_id = field[17:]
                    data = {
                        'description': self.request.POST.get(f'item_description_{row_id}'),\
                        'delivery': delivery
                    }
                    delivery_text_form = TextDeliveryItemForm(data=data)
                    if delivery_text_form.is_valid():
                        delivery_item = delivery_text_form.save(commit=False)
                        delivery_item.save()
                    else:
                        logger.info(delivery_text_form.errors)
        if len(back_order_items) > 0:
            create_back_order = CreateBackOrderFromSalesInvoice(delivery, back_order_items)
            create_back_order.execute()
        return delivery

    def create_postal_address(self, delivery):
        if self.cleaned_data['postal_street']:
            data = {
                'address_line_1': self.cleaned_data['postal_street'],
                'city': self.cleaned_data['postal_city'],
                'province': self.cleaned_data['postal_province'],
                'postal_code': self.cleaned_data['postal_postal_code'],
                'country': self.cleaned_data['postal_country'],
                'customer': delivery.customer
            }
            postal_address_form = PostalAddressForm(data=data)
            if postal_address_form.is_valid():
                postal_address = postal_address_form.save(commit=True)
                return postal_address
            else:
                logger.info(postal_address_form.errors)
                return None

    def create_delivery_address(self, delivery):
        if self.cleaned_data['delivery_street']:
            data = {
                'address_line_1': self.cleaned_data['delivery_street'],
                'city': self.cleaned_data['delivery_city'],
                'province': self.cleaned_data['delivery_province'],
                'postal_code': self.cleaned_data['delivery_postal_code'],
                'country': self.cleaned_data['delivery_country'],
                'customer': delivery.customer
            }
            if self.cleaned_data['address_id']:
                try:
                    delivery_address = Address.objects.get(pk=self.cleaned_data['address_id'])
                except Address.DoesNotExist:
                    delivery_address = None
                delivery_address_form = DeliveryAddressForm(data=data, instance=delivery_address)
            else:
                delivery_address_form = DeliveryAddressForm(data=data)
            if delivery_address_form.is_valid():
                delivery_address = delivery_address_form.save(commit=True)
                delivery_address.save()
                return delivery_address
            else:
                logger.info(delivery_address_form.errors)
                return None


class InventoryDeliveryItemForm(BaseSalesInventoryForm):

    class Meta:
        model = DeliveryItem
        fields = ('price', 'quantity', 'ordered', 'unit', 'inventory', 'vat_code', 'vat', 'discount', 'net_price',
                  'vat_amount', 'delivery', 'total_amount')

    def save(self, commit=True):
        delivery_item = super().save(commit=False)
        delivery_item.item_type = InvoiceItemType.INVENTORY
        delivery_item.save()

        delivery_item.inventory.recalculate_balances(year=delivery_item.delivery.period.period_year)

        return delivery_item


class TextDeliveryItemForm(BaseSalesDescriptionForm):

    class Meta:
        model = DeliveryItem
        fields = ('description', 'delivery')

    def save(self, commit=True):
        delivery_item = super().save(commit=False)
        delivery_item.item_type = InvoiceItemType.TEXT
        delivery_item.save()
        return delivery_item


class EstimateDeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.estimate = kwargs.pop('estimate')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(EstimateDeliveryForm, self).__init__(*args, **kwargs)

        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.company)
        self.fields['delivery_term'].queryset = self.fields['delivery_term'].queryset.filter(company=self.company)
        self.fields['delivery_method'].queryset = self.fields['delivery_method'].queryset.filter(company=self.company)
        self.fields['customer'].widget = customer_select_data(self.company)

    class Meta:
        model = Delivery
        fields = ('branch',  'date', 'customer', 'estimate', 'delivery_time', 'purchase_order', 'delivery_term',
                  'delivery_method', 'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount',
                  'customer_discount', 'line_discount', 'customer_name')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'customer_name': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(EstimateDeliveryForm, self).clean()

        delivery_method = cleaned_data['delivery_method']
        if not delivery_method:
            self.add_error('delivery_method', 'Delivery method is required')

        return cleaned_data

    def save(self, commit=True):
        logger.info("Create delivery")
        delivery = super().save(commit=False)
        delivery.branch = self.estimate.branch
        delivery.profile = self.profile
        delivery.estimate = self.estimate
        delivery.customer = self.estimate.customer
        delivery.customer_name = self.estimate.customer_name
        delivery.period = self.estimate.period
        delivery.discount_percentage = self.estimate.discount_percentage
        delivery.vat_on_customer_discount = delivery.cal_vat_on_discount(delivery.customer_discount)
        delivery.role = self.role
        if self.estimate.delivery_to:
            delivery.delivery_to = self.estimate.delivery_to
        if self.estimate.postal:
            delivery.postal = self.estimate.postal
        delivery.save()
        logger.info(f"Delivery {delivery} created successfully")
        return delivery


class BackOrderDeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.backorder = kwargs.pop('backorder')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.year = kwargs.pop('year')
        super(BackOrderDeliveryForm, self).__init__(*args, **kwargs)

        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.company)
        self.fields['delivery_term'].queryset = self.fields['delivery_term'].queryset.filter(company=self.company)
        self.fields['delivery_method'].queryset = self.fields['delivery_method'].queryset.filter(company=self.company)
        self.fields['customer'].widget = customer_select_data(self.company)

    class Meta:
        model = Delivery
        fields = ('branch',  'date', 'customer', 'estimate', 'delivery_time', 'purchase_order', 'delivery_term',
                  'delivery_method', 'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount',
                  'customer_discount', 'line_discount', 'customer_name')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'customer_name': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(BackOrderDeliveryForm, self).clean()

        delivery_method = cleaned_data['delivery_method']
        if not delivery_method:
            self.add_error('delivery_method', 'Delivery method is required')

        return cleaned_data

    def save(self, commit=True):
        logger.info("Create delivery")
        delivery = super().save(commit=False)
        delivery.branch = self.backorder.branch
        delivery.profile = self.profile
        delivery.backorder = self.backorder
        delivery.customer = self.backorder.customer
        delivery.customer_name = self.backorder.customer_name
        delivery.period = self.backorder.period
        delivery.discount_percentage = self.backorder.discount_percentage
        delivery.vat_on_customer_discount = delivery.cal_vat_on_discount(delivery.customer_discount)
        delivery.role = self.role
        if self.backorder.delivery_to:
            delivery.delivery_to = self.backorder.delivery_to
        if self.backorder.postal:
            delivery.postal = self.backorder.postal
        delivery.save()
        logger.info(f"Delivery {delivery} created successfully")
        return delivery


class PickingSlipDeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.picking_slip = kwargs.pop('picking_slip')
        print('---------PickingSlipDeliveryForm---------------')
        super(PickingSlipDeliveryForm, self).__init__(*args, **kwargs)

        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.company)
        self.fields['delivery_term'].queryset = self.fields['delivery_term'].queryset.filter(company=self.company)
        self.fields['delivery_method'].queryset = self.fields['delivery_method'].queryset.filter(company=self.company)
        self.fields['customer'].widget = customer_select_data(self.company)

    class Meta:
        model = Delivery
        fields = ('branch',  'period', 'date', 'customer', 'delivery_time', 'purchase_order', 'delivery_term',
                  'delivery_method', 'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount',
                  'customer_discount', 'line_discount')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(PickingSlipDeliveryForm, self).clean()

        delivery_method = cleaned_data['delivery_method']
        if not delivery_method:
            self.add_error('delivery_method', 'Delivery method is required')
        return cleaned_data

    def save(self, commit=True):
        delivery = super().save(commit=False)
        delivery.profile = self.profile
        delivery.content_type = ContentType.objects.get_for_model(self.picking_slip)
        delivery.object_id = self.picking_slip.id
        delivery.purchase_order = self.picking_slip.purchase_order
        delivery.discount_percentage = self.picking_slip.discount_percentage
        delivery.role_id = self.request.session['role']
        delivery.save()

        if delivery:
            picking_items = self.picking_slip.picking_slip_items.all()
            delivery.create_delivery_items_from_estimates(picking_items, self.request.POST)
            if delivery.is_picking_slip_required:
                delivery.created_picking_slip()
            self.picking_slip.mark_as_completed()

            Tracking.objects.create(
                object_id=self.picking_slip.id,
                content_type=ContentType.objects.get_for_model(self.picking_slip),
                comment='Delivery created from picking slip',
                profile=self.get_profile(),
                role_id=self.request.session.get('role'),
                data={
                    'tracking_type': 'delivery',
                    'reference': str(delivery)
                }
            )

            Tracking.objects.create(
                object_id=delivery.id,
                content_type=ContentType.objects.get_for_model(delivery),
                comment='Delivery created from picking slip',
                profile=self.get_profile(),
                role_id=self.request.session.get('role'),
                data={
                    'tracking_type': 'pickingslip',
                    'reference': str(self.picking_slip)
                }
            )


class PickingSlipForm(forms.Form):
    pass
