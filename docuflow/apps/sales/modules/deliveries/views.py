import logging
import pprint
from collections import defaultdict
from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import FormView, View, TemplateView, CreateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common import utils
from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.models import VatCode
from docuflow.apps.inventory.models import BranchInventory
from docuflow.apps.sales.models import (
    Delivery, EstimateItem, Estimate, PickingSlip, PickingSlipItem, CustomerInvoice, BackOrder, BackOrderItem, Tracking)
from .forms import DeliveryForm, EstimateDeliveryForm, PickingSlipForm, PickingSlipDeliveryForm, BackOrderDeliveryForm
from .services import CreateDeliveryFromEstimate, CreateDeliveryFromBackOrder, remove_delivery, email_delivery, \
    complete_delivery

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class DeliveryView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'deliveries/index.html'
    context_object_name = 'deliveries'

    def get_context_data(self, **kwargs):
        context = super(DeliveryView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        context['branch'] = self.get_branch()
        return context


class PickingView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'deliveries/pickings.html'

    def get_picking_slips(self, branch):
        return PickingSlip.objects.filter(
            delivery__branch=branch,
            status__in=[PickingSlip.IN_PROGRESS]
        )

    def get_context_data(self, **kwargs):
        context = super(PickingView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        context['picking_slips'] = self.get_picking_slips(branch)
        context['branch'] = branch
        return context


class CreateDeliveryFromBackOrderView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'deliveries/backorder/create.html'
    form_class = BackOrderDeliveryForm

    def get_back_order(self):
        return BackOrder.objects.prefetch_related(
            'back_order_items'
        ).filter(
            pk=self.kwargs['back_order_id']
        ).first()

    def get_initial(self):
        initial = super(CreateDeliveryFromBackOrderView, self).get_initial()
        initial['branch'] = self.get_branch()
        back_order = self.get_back_order()
        if back_order.customer:
            initial['customer'] = back_order.customer
            initial['delivery_method'] = back_order.customer.delivery_method
            initial['delivery_term'] = back_order.customer.delivery_term
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryFromBackOrderView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['backorder'] = self.get_back_order()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = Role.objects.filter(id=self.request.session['role']).first()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryFromBackOrderView, self).get_context_data(**kwargs)
        company = self.get_company()
        back_order = self.get_back_order()
        context['company'] = company
        context['back_order'] = back_order
        context['branch'] = self.get_branch()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        context['profile'] = self.get_profile()
        context['date'] = datetime.now()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the delivery, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateDeliveryFromBackOrderView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    delivery = form.save()
                    logger.info("POSTING")
                    company = self.get_company()
                    profile = self.get_profile()
                    back_order = self.get_back_order()
                    role = self.get_role()

                    create_delivery = CreateDeliveryFromBackOrder(company, profile, role, back_order, delivery, self.request.POST)
                    create_delivery.execute()

                    return JsonResponse({'text': 'Delivery saved successfully',
                                         'error': False,
                                         'redirect': reverse('sales_delivery_index')
                                         })
            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the delivery',
                                     'details': str(exception),
                                     'error': True
                                     })


class BackOrderDeliveryItemView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'deliveries/backorder/inventory_items.html'

    def get_back_order_items(self):
        back_order_items = BackOrderItem.objects.select_related(
            'inventory', 'back_order'
        ).filter(
            back_order_id=self.kwargs['back_order_id'], inventory__isnull=False
        )
        return back_order_items

    def get_context_data(self, **kwargs):
        context = super(BackOrderDeliveryItemView, self).get_context_data(**kwargs)
        context['back_order_items'] = self.get_back_order_items()
        return context


class CreateDeliveryFromEstimateView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'deliveries/estimate/create.html'
    form_class = EstimateDeliveryForm

    def get_template_names(self):
        estimate = self.get_estimate()
        if isinstance(estimate, CustomerInvoice):
            return'deliveries/estimate/create.html'
        else:
            return 'deliveries/estimate/create_cash.html'

    def get_estimate(self):
        return Estimate.objects.prefetch_related('estimate_items').get(pk=self.kwargs['estimate_id'])

    def get_initial(self):
        initial = super(CreateDeliveryFromEstimateView, self).get_initial()
        initial['branch'] = self.get_branch()
        initial['date'] = datetime.now().date()
        estimate = self.get_estimate()
        if estimate.customer:
            initial['customer'] = estimate.customer
            initial['delivery_method'] = estimate.customer.delivery_method
            initial['delivery_term'] = estimate.customer.delivery_term
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryFromEstimateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        kwargs['estimate'] = self.get_estimate()
        kwargs['role'] = Role.objects.filter(id=self.request.session['role']).first()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryFromEstimateView, self).get_context_data(**kwargs)
        company = self.get_company()
        estimate = self.get_estimate()
        context['company'] = company
        context['estimate'] = estimate
        context['branch'] = self.get_branch()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        context['profile'] = self.get_profile()
        context['date'] = datetime.now()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the sale invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateDeliveryFromEstimateView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            with transaction.atomic():
                delivery = form.save()

                logger.info("POSTING")
                company = self.get_company()
                profile = self.get_profile()
                estimate = self.get_estimate()
                role = Role.objects.filter(id=self.request.session['role']).first()

                create_delivery = CreateDeliveryFromEstimate(delivery, company, profile, role, estimate, self.request.POST)
                create_delivery.execute()

                return JsonResponse({'text': 'Delivery created successfully',
                                     'error': False,
                                     'redirect': reverse('sales_delivery_index')
                                     })


class CreateDeliveryView(ProfileMixin, LoginRequiredMixin, CreateView):
    template_name = 'deliveries/create.html'
    form_class = DeliveryForm
    model = Delivery

    def get_initial(self):
        initial = super(CreateDeliveryView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.request.session['role']
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['date'] = datetime.now()
        context['default_vat'] = company.vat_percentage
        context['cols'] = 12
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the sale invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateDeliveryView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    delivery = form.save()
                    redirect_url = reverse('sales_delivery_index')
                    if self.request.POST.get('next') == 'print':
                        redirect_url = reverse('delivery_detail', args=(delivery.id, )) + "?print=1"
                    return JsonResponse({'text': 'Sales delivery saved successfully',
                                         'error': False,
                                         'redirect': redirect_url
                                         })

            except ValueError as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True
                                     })
            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the delivery',
                                     'details': str(exception),
                                     'error': True
                                     })


class AddDeliveryItemView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'deliveries/delivery_items.html'

    def get_vat_code(self):
        return VatCode.objects.output().filter(company=self.get_company(), is_default=True).first()

    def get_rows(self):
        requested_rows = int(float(self.request.GET.get('rows', 20)))
        if requested_rows <= 1:
            requested_rows = max(requested_rows, 2)
        return requested_rows

    def get_context_data(self, **kwargs):
        context = super(AddDeliveryItemView, self).get_context_data(**kwargs)
        row_id = int(self.request.GET.get('row_id', 0))
        vat_code = self.get_vat_code()
        col_count = 12
        row_count = self.get_rows()
        tabs_start = 26
        tab_start = int(self.request.GET.get('tab_start', 26))
        is_reload = True if self.request.GET.get('reload', 0) == '1' else False
        context['is_deletable'] = int(row_id) > 0
        context['row_id'] = row_id
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['default_vat_code'] = vat_code
        context['module_url'] = reverse('add_delivery_items')
        context['tabs_start'] = tabs_start
        context['start'] = 0
        context['rows'] = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['is_reload'] = is_reload
        context['row_start'] = row_id
        return context


class EstimateDeliveryItemView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'deliveries/estimate/inventory_items.html'

    def get_estimate_items(self):
        estimate_items = EstimateItem.objects.select_related(
            'inventory',
            'estimate',
            'vat_code'
        ).filter(
            estimate_id=self.kwargs['estimate_id']
        )
        return estimate_items

    def get_context_data(self, **kwargs):
        context = super(EstimateDeliveryItemView, self).get_context_data(**kwargs)
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['estimate_items'] = self.get_estimate_items()
        return context


class DeliveryDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'deliveries/print.html'
        return 'deliveries/detail.html'

    def get_delivery(self):
        return Delivery.objects.prefetch_related('delivery_items').get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(DeliveryDetailView, self).get_context_data(**kwargs)
        delivery = self.get_delivery()
        context['delivery'] = delivery
        context['delivery_items'] = delivery.delivery_items.filter(inventory__isnull=False)
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()

        cols = 11 if delivery.display_prices else 4
        context['cols'] = cols
        context['range'] = range(0, cols)
        context['is_print'] = self.request.GET.get('print', 0) == '1'
        return context


class DeliveryPickingDetailView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'deliveries/picking_detail.html'
    form_class = PickingSlipForm

    def get_picking_slip(self):
        picking_slip = PickingSlip.objects.prefetch_related(
            'picking_slip_items'
        ).filter(
            pk=self.kwargs['pk']
        ).first()
        return picking_slip

    def get_context_data(self, **kwargs):
        context = super(DeliveryPickingDetailView, self).get_context_data(**kwargs)
        picking_slip = self.get_picking_slip()
        context['picking_slip'] = picking_slip
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        cols = 7
        context['cols'] = cols
        context['range'] = range(0, cols)
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the picking slip, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(DeliveryPickingDetailView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():

                    picking_slip = self.get_picking_slip()
                    post = self.request.POST
                    back_order_items = {}
                    for picking_slip_item in picking_slip.picking_slip_items.all():
                        picked_qty = float(post.get('picked_{}'.format(picking_slip_item.id), 0))
                        is_back_order = post.get('back_order_{}'.format(picking_slip_item.id), False)
                        if is_back_order:
                            back_order_qty = float(picking_slip_item.received) - picked_qty
                            back_order_items[picking_slip_item] = back_order_qty
                        picking_slip_item.picked = picked_qty
                        picking_slip_item.save()

                    picking_slip.status = PickingSlip.COMPLETED
                    picking_slip.save()

                    if back_order_items:
                        back_order_picking_slip = PickingSlip.objects.create(
                            period=picking_slip.period,
                            content_type=picking_slip.content_type,
                            object_id=picking_slip.object_id,
                            date=picking_slip.date,
                            profile=self.get_profile()
                        )
                        for qty, back_order_item in back_order_items.items():
                            PickingSlipItem.objects.create(
                                picking_slip=back_order_picking_slip,
                                received=qty
                            )
                    # company = self.get_company()
                    # profile = self.get_profile()
                    #
                    # delivery = form.save(commit=False)
                    # delivery.profile = profile
                    # # invoice.category = self.get_invoice_category()
                    # delivery.role_id = self.request.session['role']
                    # delivery.save()
                    #
                    # if delivery:
                    #     self.save_delivery_items(delivery)
                    #     if delivery.is_picking_slip_required:
                    #         delivery.created_picking_slip()
                    #         delivery.save()

                    return JsonResponse({'text': 'Picking slip saved successfully',
                                         'error': False,
                                         'redirect': reverse('delivery_pickings')
                                         })

            except Exception as exception:
                return JsonResponse({'text': 'Unexpected error occurred saving the delivery',
                                     'details': str(exception),
                                     'error': True
                                     })


class MultipleDeliveriesMixin(object):

    def get_deliveries(self):
        deliveries_ids = self.request.GET.getlist('deliveries[]')
        return Delivery.objects.filter(id__in=[int(delivery_id) for delivery_id in deliveries_ids])


class BulkDeliveryActionView(LoginRequiredMixin, ProfileMixin, MultipleDeliveriesMixin, View):

    def get(self, request, *args, **kwargs):
        deliveries = self.get_deliveries()
        if deliveries:
            role = Role.objects.get(pk=self.request.session['role'])
            for delivery in deliveries:
                remove_delivery(delivery, self.get_profile(), role)

            return JsonResponse({
                'text': 'Delivery removed successfully',
                'error': False,
                'reload': True
            })
        else:
            return JsonResponse({
                'text': 'Delivery not found',
                'error': False
            })


class SingleDeliveryActionView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            delivery = Delivery.objects.get(pk=self.request.GET['delivery_id'])
            action = self.kwargs['action']
            if action == 'invoice':
                if delivery.is_removed:
                    return JsonResponse({
                        'text': 'Delivery removed',
                        'warning': True
                    })
                elif delivery.can_invoice:
                    redirect_url = reverse('create_delivery_invoice', kwargs={'delivery_id': delivery.id})
                else:
                    return JsonResponse({
                        'text': 'Already invoiced',
                        'warning': True
                    })
                return JsonResponse({
                    'text': 'Redirecting ....',
                    'error': False,
                    'redirect': redirect_url
                })
            elif action == 'remove':
                if delivery.is_removed:
                    return JsonResponse({
                        'text': 'Already removed',
                        'warning': True
                    })
                else:
                    remove_delivery(delivery=delivery, profile=self.get_profile(), role=self.get_role())
                    return JsonResponse({
                        'text': 'Delivery removed successfully',
                        'error': False,
                        'reload': True
                    })
            elif action == 'email':
                email_delivery(delivery)
                return JsonResponse({
                    'text': 'Delivery emailed successfully',
                    'error': False,
                    'reload': True
                })
            elif action == 'complete':
                complete_delivery(delivery, self.get_profile(), self.get_role())
                return JsonResponse({
                    'text': 'Delivery completed successfully',
                    'error': False,
                    'reload': True
                })

            else:
                return JsonResponse({
                    'text': 'No action found',
                    'warning': True
                })

        except Delivery.DoesNotExist:
            return JsonResponse({
                'text': 'Delivery not found',
                'error': True,
                'alert': True,
            })


class CreateDeliveryFromPickingSlipView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'deliveries/pickingslip/create.html'
    form_class = PickingSlipDeliveryForm

    def get_picking_slip(self):
        return PickingSlip.objects.prefetch_related('picking_slip_items').filter(pk=self.kwargs['picking_slip_id']).first()

    def get_initial(self):
        initial = super(CreateDeliveryFromPickingSlipView, self).get_initial()
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryFromPickingSlipView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = Role.objects.get(pk=self.request.session['role'])
        kwargs['picking_slip'] = self.get_picking_slip()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryFromPickingSlipView, self).get_context_data(**kwargs)
        company = self.get_company()
        picking_slip = self.get_picking_slip()
        context['company'] = company
        context['picking_slip'] = picking_slip
        context['branch'] = self.get_branch()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the sale invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateDeliveryFromPickingSlipView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            with transaction.atomic():
                form.save()
                return JsonResponse({'text': 'Delivery created successfully',
                                     'error': False,
                                     'redirect': reverse('sales_delivery_index')
                                     })


class PickingSlipDeliveryItemView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'deliveries/pickingslip/picking_slip_items.html'

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch())
        return branch_inventories

    def get_vat_codes(self):
        return VatCode.objects.output().filter(company=self.get_company())

    def get_picking_slip_items(self):
        picking_slip_items = PickingSlipItem.objects.select_related(
            'inventory', 'picking_slip', 'vat_code'
        ).filter(picking_slip_id=self.kwargs['picking_slip_id'])
        return picking_slip_items

    def get_context_data(self, **kwargs):
        context = super(PickingSlipDeliveryItemView, self).get_context_data(**kwargs)
        context['inventory_items'] = self.get_inventory_items()
        context['vat_codes'] = self.get_vat_codes()
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['picking_slip_items'] = self.get_picking_slip_items()
        return context


class DeliveryTracking(TemplateView):
    template_name = 'deliveries/tracking.html'

    def get_delivery(self):
        return Delivery.objects.filter(pk=self.kwargs['pk']).first()

    def get_trackings(self, delivery):
        content_type = ContentType.objects.filter(app_label='sales', model='delivery').first()
        trackings = Tracking.objects.select_related(
            'profile', 'role'
        ).filter(
            object_id=delivery.id, content_type=content_type
        )
        tracking_by_types = defaultdict(list)
        for tracking in trackings:
            tracking_type = tracking.data.get('tracking_type')
            if tracking_type:
                tracking_by_types[tracking_type].append(tracking)
        return tracking_by_types

    def get_context_data(self, **kwargs):
        context = super(DeliveryTracking, self).get_context_data(**kwargs)
        delivery = self.get_delivery()
        tracking = self.get_trackings(delivery)
        context['delivery'] = delivery
        context['tracking'] = tracking.get('self')
        context['backorder_tracking'] = tracking.get('back_order')
        context['picking_tracking'] = tracking.get('pickingslip')
        context['invoice_tracking'] = tracking.get('invoice')
        return context
