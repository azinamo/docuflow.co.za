import logging
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse

from docuflow.apps.sales.models import Delivery, DeliveryItem, Tracking, BackOrder, BackOrderItem
from docuflow.apps.sales.modules.backorder.services import CreateBackOrderFromSalesInvoice
from docuflow.apps.sales import enums
from .forms import InventoryDeliveryItemForm

logger = logging.getLogger(__name__)


class CreateFromSalesInvoice:

    def __init__(self, sales_invoice, form, branch, profile, period):
        self.sales_invoice = sales_invoice
        self.form = form
        self.branch = branch
        self.profile = profile
        self.period = period

    def create(self, content_type):
        logger.info("Create delivery")
        delivery = self.form.save(commit=False)
        delivery.branch = self.branch
        delivery.profile = self.profile
        delivery.estimate = self.sales_invoice
        delivery.period = self.period
        delivery.content_type = content_type
        delivery.object_id = self.sales_invoice.id
        delivery.vat_on_customer_discount = delivery.cal_vat_on_discount(delivery.customer_discount)
        delivery.role = self.role
        delivery.save()
        logger.info(f"Delivery {delivery} created successfully")
        return delivery


class CreateDeliveryFromBackOrder:

    def __init__(self, company, profile, role, back_order, delivery, post_request):
        logger.info("Create delivery from back order")
        self.company = company
        self.profile = profile
        self.role = role
        self.back_order = back_order
        self.delivery = delivery
        self.post_request = post_request

    def execute(self):
        logger.info("Execute")

        self.create_address(self.delivery)
        self.create_delivery_items()

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.delivery),
            object_id=self.delivery.pk,
            comment='Delivery created from back order',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'backorder',
                'reference': str(self.delivery),
                'link_url': reverse('backorder_detail', args=(self.back_order.id, ))
            }
        )
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.back_order),
            object_id=self.back_order.pk,
            comment='Delivery created',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'delivery',
                'reference': str(self.delivery),
                'link_url': reverse('delivery_detail', args=(self.delivery.pk, ))
            }
        )

    def create_address(self, delivery):
        pass

    def create_delivery_items(self):
        logger.info(f"Create delivery items for {self.delivery}")

        back_order_sales_items = []
        total_on_back_order = Decimal(0)
        for sales_item in self.back_order.back_order_items.all():
            is_back_ordered = bool(self.post_request.get(f'back_order_{sales_item.id}', False))
            quantity_ordered = Decimal(self.post_request.get(f'quantity_ordered_{sales_item.id}', 0))
            data = {
                'quantity': self.post_request.get(f'quantity_{sales_item.id}', 0),
                'vat_amount': self.post_request.get(f'vat_amount_{sales_item.id}', 0),
                'total_amount': self.post_request.get(f'total_price_including_{sales_item.id}', 0),
                'delivery': self.delivery,
                'inventory': sales_item.inventory,
                'description': sales_item.description,
                'received': self.post_request.get(f'quantity_{sales_item.id}', 0),
                'unit': sales_item.unit,
                'price': sales_item.price,
                'discount': sales_item.discount,
                'vat': sales_item.vat,
                'vat_code': sales_item.vat_code,

            }
            delivery_item_form = InventoryDeliveryItemForm(data=data, branch=self.delivery.branch)
            if delivery_item_form.is_valid():
                if delivery_item_form.cleaned_data['quantity'] > 0:
                    delivery_item = delivery_item_form.save()
                    back_order_quantity = quantity_ordered - delivery_item.quantity
                    if is_back_ordered:
                        if back_order_quantity > 0:
                            total_on_back_order += back_order_quantity
                            back_order_sales_items.append({'item': sales_item,
                                                           'quantity': delivery_item.quantity * -1
                                                           })
                    else:
                        back_order_sales_items.append({'item': sales_item,
                                                       'quantity': delivery_item.quantity * -1
                                                       })

        if len(back_order_sales_items) > 0 and total_on_back_order > 0:
            self.create_sales_back_order(back_order_sales_items)
        else:
            self.back_order.status = BackOrder.PROCESSED
            self.back_order.save()

            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(self.back_order),
                object_id=self.back_order.pk,
                comment='Back order processed',
                profile=self.profile,
                role=self.role,
                data={
                    'tracking_type': 'self',
                    'reference': str(self.back_order),
                    'link_url': reverse('backorder_detail', args=(self.back_order.id, ))
                }
            )


    def create_sales_back_order(self, back_order_items):
        create_back_order = CreateBackOrderFromSalesInvoice(self.back_order, back_order_items)
        create_back_order.execute()

    def create_back_order_item(self, back_order, estimate_item, quantity):
        logger.info("Create a back order of the estimate item {} --> {} -< {}".format(back_order, estimate_item, quantity))
        vat_amount = 0
        discount_amount = 0
        discount = float(estimate_item.discount)
        amount = float(estimate_item.price)
        total = amount * quantity
        net_amount = amount * quantity
        if estimate_item.vat_code:
            vat_amount = (float(estimate_item.vat_code.percentage / 100) * amount) * quantity
        if discount > 0:
            discount_amount = (float(discount / 100) * amount)
        BackOrderItem.objects.create(
            back_order=back_order,
            inventory=estimate_item.inventory,
            description=estimate_item.description,
            quantity=quantity,
            received=quantity,
            unit=estimate_item.unit,
            price=amount,
            discount=discount,
            vat=estimate_item.vat,
            net_price=net_amount,
            vat_code=estimate_item.vat_code
        )
        logger.info("Back order item created successfull --> ")
        return vat_amount, net_amount, amount, discount_amount


class CreateDeliveryFromEstimate:

    def __init__(self, delivery, company, profile, role, estimate, post_request):
        logger.info("Create delivery from estimate")
        self.delivery = delivery
        self.company = company
        self.profile = profile
        self.role = role
        self.estimate = estimate
        self.post_request = post_request

    def execute(self):
        logger.info("Execute")
        self.create_delivery_items()
        self.estimate.mark_as_completed()
        self.add_tracking()

    def create_delivery_items(self):
        back_order_estimate_items = []
        for estimate_item in self.estimate.estimate_items.all():
            is_back_ordered = bool(self.post_request.get(f'back_order_{estimate_item.id}', False))
            quantity_ordered = self.post_request.get(f'quantity_ordered_{estimate_item.id}', 0)
            quantity = self.post_request.get(f'quantity_{estimate_item.id}', 0)
            delivery_item_data = {
                'quantity': quantity,
                'ordered': quantity_ordered,
                'vat_amount': self.post_request.get(f'vat_amount_{estimate_item.id}', 0),
                'discount_amount': self.post_request.get(f'discount_amount_{estimate_item.id}', 0)
            }
            quantity = Decimal(quantity)
            quantity_ordered = Decimal(quantity_ordered)
            logger.info(f"Estimate item")
            logger.info(delivery_item_data)

            if estimate_item.is_account:
                self.create_delivery_account_item(estimate_item)
            elif estimate_item.is_description:
                self.create_delivery_text_item(estimate_item)
            elif estimate_item.inventory and quantity > 0:
                delivery_item_data['unit'] = estimate_item.unit
                delivery_item_data['inventory'] = estimate_item.inventory
                delivery_item_data['price'] = estimate_item.price
                delivery_item_data['vat_code'] = estimate_item.vat_code
                delivery_item_data['discount'] = estimate_item.discount
                delivery_item_data['vat'] = estimate_item.vat
                delivery_item_data['delivery'] = self.delivery
                # TODO - Check if we need the net price
                delivery_inventory_item_form = InventoryDeliveryItemForm(data=delivery_item_data, branch=self.delivery.branch)
                if delivery_inventory_item_form.is_valid():
                    delivery_inventory_item = delivery_inventory_item_form.save(commit=False)
                    delivery_inventory_item.save()
                else:
                    logger.info(delivery_inventory_item_form.errors)

            if is_back_ordered:
                back_order_quantity = quantity_ordered - quantity
                back_order_estimate_items.append({'item': estimate_item,
                                                  'quantity': back_order_quantity
                                                  })

        if len(back_order_estimate_items) > 0:
            create_back_order = CreateBackOrderFromSalesInvoice(self.delivery, back_order_estimate_items)
            back_order = create_back_order.execute()

            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(back_order),
                object_id=back_order.id,
                comment='Back order created from delivery',
                profile=self.profile,
                data={
                    'tracking_type': 'picking_slip',
                    'reference': str(self.delivery),
                    'link_url': reverse('picking_detail', args=(self.delivery.id,))
                }
            )
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(self.delivery),
                object_id=self.delivery.id,
                comment='Back order created ',
                profile=self.profile,
                data={
                    'tracking_type': 'back_order',
                    'reference': str(back_order),
                    'link_url': reverse('backorder_detail', args=(back_order.id,))
                }
            )

    def create_delivery_account_item(self, estimate_item):
        logger.info(f"Create delivery account item --> {self.delivery} -> {estimate_item}")
        return DeliveryItem.objects.create(
            delivery=self.delivery,
            price=estimate_item.price,
            quantity=estimate_item.quantity,
            vat_code=estimate_item.vat_code,
            vat_amount=estimate_item.vat_amount,
            discount=estimate_item.discount,
            description=estimate_item.description,
            vat=estimate_item.vat,
            item_type=enums.InvoiceItemType.GENERAL,
            account=estimate_item.account,
            received=estimate_item.quantity,
            ordered=estimate_item.quantity,
            net_price=estimate_item.net_price,
        )

    def create_delivery_text_item(self, estimate_item):
        logger.info(f"Create delivery text item --> {self.delivery} -> {estimate_item}")
        return DeliveryItem.objects.create(
            delivery=self.delivery,
            description=estimate_item.description,
            item_type=enums.InvoiceItemType.TEXT
        )

    def add_tracking(self):

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.estimate),
            object_id=self.estimate.pk,
            comment=f'Estimate {str(self.estimate.status)}',
            profile=self.profile,
            role=self.role,
            data={'tracking_type': 'delivery',
                  'delivery_id': self.delivery.id,
                  'reference': str(self.delivery),
                  'link_url': reverse('delivery_detail', args=(self.delivery.pk, ))
                  }
        )

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.delivery),
            object_id=self.delivery.pk,
            comment=f'Delivery created from estimate',
            profile=self.profile,
            role=self.role,
            data={'tracking_type': 'self',
                  'reference': str(self.estimate),
                  'link_url': reverse('estimate_detail', args=(self.estimate.pk, ))
                  }
        )


def email_delivery(delivery):
    pass


def remove_delivery(delivery: Delivery, profile, role):

    if delivery.delivery_back_order.exists():
        for back_order in delivery.delivery_back_order.all():
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(back_order),
                object_id=back_order.id,
                comment='Delivery removed',
                profile=profile,
                role=role,
                data={
                    'tracking_type': 'delivery',
                    'reference': str(delivery),
                    'link_url': reverse('delivery_detail', args=(delivery.id, ))
                }
            )
            back_order.mark_as_removed()

    if delivery.picking_slip:
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(delivery.picking_slip),
            object_id=delivery.picking_slip_id,
            comment=f'Delivery note removed',
            profile=profile,
            role=role,
            data={
                'tracking_type': 'delivery',
                'reference': str(delivery),
                'action': str(enums.CommentAction.DELETED),
                'comment_type': str(enums.TrackingType.ACTION)
            }
        )
        delivery.picking_slip.mark_as_send_for_picking()
        for picking_item in delivery.picking_slip.picking_slip_items.filter(item_type=enums.InvoiceItemType.INVENTORY):
            picking_item.inventory.recalculate_balances(year=delivery.period.period_year)

        delivery.picking_slip = None
    elif delivery.backorder:
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(delivery.backorder),
            object_id=delivery.backorder_id,
            comment=f'Delivery note removed',
            profile=profile,
            role=role,
            data={
                'tracking_type': 'backorder',
                'reference': str(delivery),
                'action': str(enums.CommentAction.DELETED),
                'comment_type': str(enums.TrackingType.ACTION)
            }
        )
        delivery.backorder.mark_as_open()
        delivery.backorder = None
    elif delivery.estimate:
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(delivery.estimate),
            object_id=delivery.estimate_id,
            comment=f'Delivery note removed',
            profile=profile,
            role=role,
            data={
                'tracking_type': 'delivery',
                'reference': str(delivery),
                'action': str(enums.CommentAction.DELETED),
                'comment_type': str(enums.TrackingType.ACTION)
            }
        )
        delivery.estimate.mark_as_open()
        delivery.estimate = None

    delivery.status = enums.DeliveryStatus.REMOVED
    delivery.save()

    for delivery_item in delivery.delivery_items.all():
        delivery_item.update_on_delivery()

    Tracking.objects.create(
        content_type=ContentType.objects.get_for_model(delivery),
        object_id=delivery.pk,
        comment=f'Removed delivery note',
        profile=profile,
        role=role,
        data={
            'tracking_type': 'self',
            'reference': str(delivery),
            'action': str(enums.CommentAction.DELETED),
            'comment_type': str(enums.TrackingType.ACTION)
        }
    )


def complete_delivery(delivery: Delivery, profile, role):
    delivery.status = enums.DeliveryStatus.DELIVERED
    delivery.save()

    for delivery_item in delivery.delivery_items.filter(item_type=enums.InvoiceItemType.INVENTORY):
        delivery_item.inventory.recalculate_balances(year=delivery.period.period_year)

    Tracking.objects.create(
        content_type=ContentType.objects.get_for_model(delivery),
        object_id=delivery.pk,
        comment=f'Delivery completed',
        profile=profile,
        role=role,
        data={
            'tracking_type': 'self',
            'reference': str(delivery),
            'action': str(enums.CommentAction.DELETED),
            'comment_type': str(enums.TrackingType.ACTION)
        }
    )