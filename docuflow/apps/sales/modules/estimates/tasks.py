from celery import shared_task

from docuflow.apps.sales.models import Estimate


@shared_task
def send_estimate_email(estimate_id, email_address):
    estimate = Estimate.objects.get(pk=estimate_id)

    return 'Estimate email successfully sent'
