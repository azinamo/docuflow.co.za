import logging
import pprint
from collections import defaultdict
from datetime import datetime, timedelta

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse, reverse_lazy
from django.utils.timezone import now
from django.views.generic import FormView, View, TemplateView, CreateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.comment.services import clear_session_comments
from docuflow.apps.common import utils
from docuflow.apps.common.mixins import ProfileMixin, AddRowMixin, AjaxFormViewMixin, DataTableMixin
from docuflow.apps.company.models import VatCode, Unit
from docuflow.apps.inventory.models import BranchInventory
from docuflow.apps.sales.exceptions import QuantityNotAvailableException
from docuflow.apps.sales.models import Estimate, EstimateItem, Tracking
from .forms import (CustomerEstimateForm, CopyEstimateToCustomerForm, DeliveryBackOrderForm, CopyEstimateForm,
                    CreateTemplateEstimateForm)
from .services import print_profoma

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class EstimateView(LoginRequiredMixin, PermissionRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'estimates/index.html'
    permission_required = []
    permission_denied_message = 'You do not have access to this module'

    def get_context_data(self, **kwargs):
        context = super(EstimateView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        context['year'] = self.get_year()
        return context


class EstimateTemplatesView(EstimateView):
    template_name = 'estimates/templates.html'


class CreateEstimateView(ProfileMixin, LoginRequiredMixin, CreateView):
    template_name = 'estimates/create.html'
    form_class = CustomerEstimateForm

    def get_initial(self):
        initial = super(CreateEstimateView, self).get_initial()
        company = self.get_company()
        today = datetime.now()
        initial['date'] = today.strftime('%Y-%m-%d')
        if company and company.estimate_valid_until:
            expiry_date = today + timedelta(company.estimate_valid_until)
            initial['expires_at'] = expiry_date.strftime('%Y-%m-%d')
        else:
            initial['expires_at'] = today.strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateEstimateView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        kwargs['is_template'] = self.kwargs.get('estimate_type', False)
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateEstimateView, self).get_context_data(**kwargs)
        company = self.get_company()
        content_type = ContentType.objects.filter(app_label='sales', model='estimate').first()

        clear_session_comments(self.request, content_type)

        context['company'] = company
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['default_vat'] = company.vat_percentage
        context['date'] = datetime.now()
        context['cols'] = 12
        context['content_type'] = content_type
        context['is_template'] = self.kwargs.get('estimate_type', False)
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the estimate, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateEstimateView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            estimate = form.save()

            redirect_url = reverse('estimates_index')
            text = 'Estimate saved successfully'
            if self.request.POST.get('next') == 'print':
                redirect_url = reverse('estimate_detail', args=(estimate.id, )) + "?print=1"
            elif self.request.POST.get('next') == 'email':
                text = 'Estimate saved and emailed successfully'
            elif self.kwargs.get('estimate_type', False) == 'template':
                redirect_url = reverse('estimate_templates')

            return JsonResponse({'text': text,
                                 'error': False,
                                 'redirect': redirect_url
                                 })
        return super(CreateEstimateView, self).form_valid(form)


class CreateEstimateFromTemplateView(ProfileMixin, CreateView):
    template_name = 'estimates/create_from_estimate.html'
    form_class = CreateTemplateEstimateForm
    model = Estimate

    def get_initial(self):
        initial = super(CreateEstimateFromTemplateView, self).get_initial()
        initial['date'] = now().date().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateEstimateFromTemplateView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['template'] = Estimate.objects.get(pk=self.kwargs['pk'])
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateEstimateFromTemplateView, self).get_context_data(**kwargs)
        context['estimate'] = Estimate.objects.get(pk=self.kwargs['pk'])
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the estimate, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateEstimateFromTemplateView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            form.save()

            redirect_url = reverse('estimates_index')
            text = 'Estimate saved successfully'

            return JsonResponse({'text': text,
                                 'error': False,
                                 'redirect': redirect_url
                                 })
        return super(CreateEstimateFromTemplateView, self).form_valid(form)


class InventoryLineView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch())
        return branch_inventories

    def get_vat_code(self):
        return VatCode.objects.output().filter(company=self.get_company(), is_default=True).first()

    def get_units(self):
        return Unit.objects.filter(measure__company=self.get_company())


class SalesItemMixin(AddRowMixin):

    def get_module_url(self):
        if self.request.GET.get('invoice_type'):
            return reverse('add_invoice_items') + "?invoice_type=" + self.request.GET.get('invoice_type')
        return reverse('add_invoice_items')


class AddEstimateItemRowView(SalesItemMixin, InventoryLineView):
    template_name = 'estimates/estimate_items.html'

    def get_context_data(self, **kwargs):
        context = super(AddEstimateItemRowView, self).get_context_data(**kwargs)
        row_id = self.get_row_start()
        is_reload = True if self.request.GET.get('reload', 0) == '1' else False
        vat_code = self.get_vat_code()
        col_count = 12
        row_count = self.get_rows()
        tabs_start = self.get_tabs_start(26, row_id, is_reload)
        context['inventory_items'] = self.get_inventory_items()
        context['units'] = self.get_units()
        context['is_deletable'] = int(row_id) > 0
        context['row_id'] = row_id
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['default_vat_code'] = vat_code
        context['module_url'] = reverse('add_estimate_items')
        context['tabs_start'] = tabs_start
        context['start'] = 0
        context['rows'] = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['is_reload'] = is_reload
        context['row_start'] = row_id
        context['is_copy'] = self.request.GET.get('is_copy')
        return context


class EstimateDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'estimates/detail.html'

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'estimates/print.html'
        return 'estimates/detail.html'

    def get_estimate(self):
        return Estimate.objects.get_detail(self.kwargs['pk'])

    def get_page_items(self, estimate):
        max_per_page = 20
        counter = 0
        pager = 1
        estimate_items = {}
        for estimate_item in estimate.estimate_items.all():
            if counter < max_per_page:
                if pager in estimate_items:
                    estimate_items[pager].append(estimate_item)
                else:
                    estimate_items[pager] = [estimate_item]
                counter += 1
            else:
                counter = 1
                pager += 1
                estimate_items[pager] = [estimate_item]
        return estimate_items

    def get_context_data(self, **kwargs):
        context = super(EstimateDetailView, self).get_context_data(**kwargs)
        estimate = self.get_estimate()

        if self.request.GET.get('print', 0) == '1':
            estimate_items = self.get_page_items(estimate)
        else:
            estimate_items = estimate.estimate_items.all()
        context['estimate'] = estimate

        context['estimate_items'] = estimate_items
        context['account_details'] = estimate.branch.company.bank_details.first() if estimate.branch.company.bank_details else None
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        context['cols'] = 11
        context['range'] = range(0, 11)
        context['is_print'] = self.request.GET.get('print', 0) == '1'
        context['content_type'] = ContentType.objects.filter(app_label='sales', model='estimate').first()
        return context


class ReceiveInventoryItemsView(ProfileMixin, TemplateView):
    template_name = 'estimates/purchase_order_items.html'

    def get_purchase_order_inventory_items(self):
        query = EstimateItem.objects.filter(invoice_id=self.kwargs['invoice_id'])

        inventory_items = {'total_ordered': 0, 'total_received': 0, 'total_short': 0, 'total_price_exl': 0,
                           'total_price_in': 0, 'total_total_price_exl': 0, 'total_total_price_inc': 0,
                           'inventoryitems': []
                           }
        for po_item in query:
            inventory_items['total_ordered'] += po_item.quantity
            inventory_items['total_received'] += 0
            inventory_items['total_short'] += 0
            inventory_items['total_price_exl'] += po_item.order_value
            inventory_items['total_price_in'] += po_item.price_with_vat
            inventory_items['total_total_price_exl'] += po_item.total_price
            inventory_items['total_total_price_inc'] += po_item.total_price_with_vat
            inventory_items['inventoryitems'].append(po_item)
        return inventory_items

    def get_context_data(self, **kwargs):
        context = super(ReceiveInventoryItemsView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        context['inventory_items'] = self.get_purchase_order_inventory_items()
        return context


class MultipleEstimatesMixin(object):

    def get_estimates(self):
        estimates_ids = self.request.POST.getlist('estimates[]')
        return Estimate.objects.filter(id__in=[int(estimate_id) for estimate_id in estimates_ids])


class EmailEstimateView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        return Estimate.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimate()
            if estimate:
                estimate.send_email()
            return JsonResponse({
                'text': 'Estimate email send successfully',
                'error': False
            })
        except Estimate.DoesNotExist:
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class EmailEstimatesView(LoginRequiredMixin, ProfileMixin, MultipleEstimatesMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            estimates = self.get_estimates()
            if estimates:
                for estimate in estimates:
                    estimate.send_email()
            return JsonResponse({
                'text': 'Estimate email send successfully',
                'error': False
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class CopyEstimateView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        return Estimate.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            # estimate = self.get_estimate()
            # if estimate:
            #     estimate.make_copy()
            return JsonResponse({
                'text': 'Select one estimate to copy',
                'error': True
            })
        except Estimate.DoesNotExist:
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class SingleEstimateActionView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            estimate = Estimate.objects.get(pk=self.request.GET['estimate_id'])

            action = self.kwargs['action']
            redirect_url = ''

            if action == 'copy':
                redirect_url = reverse('create_estimate_copy', kwargs={'estimate_id': estimate.id})
            elif action == 'send_estimate':
                return JsonResponse({
                    'text': 'Processing ....',
                    'error': False,
                    'url': reverse('create_estimate_from_template', args=(estimate.id, ))
                })
            elif action == 'picking':
                if hasattr(estimate, 'picking_slip'):
                    return JsonResponse({
                        'text': 'Estimate already converted to picking slip',
                        'warning': True
                    })
                redirect_url = reverse('create_estimate_picking_slip', kwargs={'estimate_id': estimate.id})
            elif action == 'delivery':
                redirect_url = reverse('create_estimate_delivery', kwargs={'estimate_id': estimate.id})
            elif action == 'invoice':
                redirect_url = reverse('create_estimate_invoice', kwargs={'estimate_id': estimate.id})
            elif action == 'print':
                redirect_url = reverse('estimate_detail', args=(estimate.id, )) + "?print=1"
            return JsonResponse({
                'text': 'Redirecting ....',
                'error': False,
                'redirect': redirect_url
            })
        except Estimate.DoesNotExist as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True,
                'details': str(exception)
            })


class DeliveryNoteEstimateView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        return Estimate.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimate()
            redirect_url = reverse('estimates_index')
            if estimate:
                redirect_url = reverse('create_estimate_delivery', kwargs={'estimate_id': estimate.id})
            return JsonResponse({
                'text': 'Redirecting ....',
                'error': False,
                'redirect': redirect_url
            })
        except Estimate.DoesNotExist:
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True
            })
        except QuantityNotAvailableException as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': str(exception),
                'alert': True,
                'url': reverse('estimate_delivery_note_back_order_notice'),
                'error': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class CreateEstimatesDeliveryNotesView(LoginRequiredMixin, ProfileMixin, MultipleEstimatesMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimates().first()
            if estimate:
                redirect_url = reverse('create_estimate_delivery', kwargs={'estimate_id': estimate.id})
                return JsonResponse({
                    'text': 'Redirecting ....',
                    'error': False,
                    'redirect': redirect_url
                })
        except QuantityNotAvailableException as exception:
            return JsonResponse({
                'text': str(exception),
                'alert': True,
                'url': reverse('estimate_delivery_note_back_order_notice'),
                'error': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': exception
            })


class BackOrderDeliveryNoteView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'estimates/delivery_back_order.html'
    form_class = DeliveryBackOrderForm

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the delivery note backorder, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(BackOrderDeliveryNoteView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    return JsonResponse({'text': 'Backorder created successfully',
                                         'error': False,
                                         })
            except Exception as exception:
                return JsonResponse({'text': 'Unexpected error occurred creating estimate',
                                     'details': str(exception),
                                     'error': True
                                     })


class CreateBackOrderDeliveryNoteView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        return Estimate.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimate()
            if estimate:
                estimate.delete()
            return JsonResponse({
                'text': 'Estimate removed successfully',
                'error': False,
                'redirect': reverse('estimates_index')
            })
        except Estimate.DoesNotExist:
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True
            })
        except Exception as exception:
            return JsonResponse({'text': 'Unexpected error occurred creating estimate',
                                 'details': str(exception),
                                 'error': True
                                 })


class RemoveEstimateView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        return Estimate.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            estimate = self.get_estimate()
            if estimate:
                estimate.execute_delete(self.get_profile(), self.request.session.get('role'))
            return JsonResponse({
                'text': 'Estimate removed successfully',
                'error': False,
                'redirect': reverse('estimates_index')
            })
        except Estimate.DoesNotExist:
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class RemoveEstimatesView(LoginRequiredMixin, ProfileMixin, MultipleEstimatesMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            estimates = self.get_estimates()
            if estimates:
                for estimate in estimates:
                    estimate.execute_delete(self.get_profile(), self.request.session.get('role'))
            return JsonResponse({
                'text': 'Estimate invoices deleted successfully',
                'error': False,
                'reload': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class CreateEstimateInvoiceView(LoginRequiredMixin, ProfileMixin, View):

    def get_estimate(self):
        return Estimate.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                estimate = self.get_estimate()
                if estimate:
                    redirect_url = reverse('create_estimate_invoice', kwargs={'estimate_id': estimate.id})
                return JsonResponse({
                    'text': 'Redirecting to invoice ....',
                    'error': False,
                    'redirect': redirect_url
                })
        except Estimate.DoesNotExist:
            return JsonResponse({
                'text': 'Estimate not found',
                'error': True
            })
        except QuantityNotAvailableException as exception:
            return JsonResponse({
                'text': str(exception),
                'alert': True,
                'url': reverse('estimate_invoice_back_order_notice'),
                'error': True
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class BackOrderInvoiceNoteView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'estimates/invoice_back_order.html'


class EstimatePrintProformaView(EstimateDetailView):

    def get_template_names(self):
        return 'estimates/print.html'

    def get_context_data(self, **kwargs):
        context = super(EstimatePrintProformaView, self).get_context_data(**kwargs)
        estimate = self.get_estimate()

        estimate_items = self.get_page_items(estimate)
        context['estimate'] = estimate

        if not estimate.proforma_invoice:
            print_profoma(estimate, self.get_profile(), Role.objects.get(pk=self.request.session['role']))

        context['estimate_items'] = estimate_items
        context['account_details'] = estimate.branch.company.bank_details.first() if estimate.branch.company.bank_details else None
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        context['cols'] = 11
        context['range'] = range(0, 11)
        context['is_print'] = 1
        context['is_proforma_invoice'] = True
        context['content_type'] = ContentType.objects.filter(app_label='sales', model='estimate').first()
        return context
    # def get_context_data(self, **kwargs):
    #     context = super(EstimatePrintProformaView, self).get_context_data(**kwargs)
    #     estimate = Estimate.objects.get_detail(self.kwargs['pk'])
    #     context['estimate_items'] = estimate.estimate_items.all()
    #     context['cols'] = 9
    #     context['is_print'] = True
    #     context['is_proforma_invoice'] = True
    #
    #     context['estimate'] = estimate
    #     context['company'] = self.get_company()
    #     context['account_details'] = estimate.branch.company.bank_details.first() if estimate.branch.company.bank_details else None
    #     return context


class EstimatePrintView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'estimates/print.html'

    def get_estimate(self):
        return Estimate.objects.filter(id=self.kwargs['pk']).first()

    def get_context_data(self, **kwargs):
        context = super(EstimatePrintView, self).get_context_data(**kwargs)
        estimate = self.get_estimate()
        context['estimate'] = estimate
        context['cols'] = 9
        return context


class CreateEstimateCopyView(LoginRequiredMixin, ProfileMixin, CreateView):
    template_name = 'estimates/copy/create.html'
    form_class = CopyEstimateForm
    model = Estimate

    def get_estimate(self):
        return Estimate.objects.prefetch_related('estimate_items').get(pk=self.kwargs['estimate_id'])

    def get_initial(self):
        initial = super(CreateEstimateCopyView, self).get_initial()
        company = self.get_company()
        today = datetime.now()
        initial['date'] = today.strftime('%Y-%m-%d')
        if company and company.estimate_valid_until:
            expiry_date = today + timedelta(company.estimate_valid_until)
            initial['expires_at'] = expiry_date.strftime('%Y-%m-%d')
        else:
            initial['expires_at'] = today.strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateEstimateCopyView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        kwargs['estimate'] = self.get_estimate()
        kwargs['profile'] = self.get_profile()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateEstimateCopyView, self).get_context_data(**kwargs)
        company = self.get_company()
        estimate = self.get_estimate()
        context['company'] = company
        context['estimate'] = estimate
        context['branch'] = self.get_branch()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        context['profile'] = self.get_profile()
        context['date'] = datetime.now()
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred copying the estimate, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateEstimateCopyView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():
                form.save()
                return JsonResponse({'text': 'Estimate copied and saved successfully',
                                     'error': False,
                                     'redirect': reverse('estimates_index')
                                     })
        return super(CreateEstimateCopyView, self).form_valid(form)


class CopyEstimateToCustomerView(LoginRequiredMixin, ProfileMixin, AjaxFormViewMixin, CreateView):
    template_name = 'estimates/copy_to_customer.html'
    form_class = CopyEstimateToCustomerForm
    model = Estimate
    success_message = 'Estimate successfully copied'
    failure_message = 'Error occurred copying the estimate'
    redirect_url = reverse_lazy('estimates_index')
    success_url = reverse_lazy('estimates_index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['estimate'] = Estimate.objects.get(pk=self.kwargs['estimate_id'])
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['estimate'] = Estimate.objects.get(pk=self.kwargs['estimate_id'])
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        return kwargs


class CopyEstimateItemsView(LoginRequiredMixin, ProfileMixin, SalesItemMixin, TemplateView):
    template_name = 'estimates/copy/estimate_items.html'

    def get_estimate_items(self):
        return EstimateItem.objects.filter(estimate_id=self.kwargs['estimate_id'])

    def get_vat_code(self):
        return VatCode.objects.output().filter(company=self.get_company(), is_default=True).first()

    def get_units(self):
        return Unit.objects.filter(measure__company=self.get_company())

    def get_estimate_items_rows(self, invoice_items, rows):
        items_rows = {}
        for i, invoice_item in enumerate(invoice_items):
            items_rows[i+1] = {'invoice_item': invoice_item, 'row': rows.get(i+1)}
        return items_rows

    def get_context_data(self, **kwargs):
        context = super(CopyEstimateItemsView, self).get_context_data(**kwargs)
        estimate_items = self.get_estimate_items()
        is_reload = False
        tabs_start = 26
        col_count = 9
        row_id = 0
        row_count = len(estimate_items) + 1
        rows = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['vat_code'] = self.get_vat_code()
        context['units'] = self.get_units()
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        item_rows = self.get_estimate_items_rows(self.get_estimate_items(), rows)
        context['item_rows'] = item_rows
        return context


class AddCopyEstimateItemRowView(InventoryLineView):
    template_name = 'estimates/estimate_items.html'

    def get_context_data(self, **kwargs):
        context = super(AddCopyEstimateItemRowView, self).get_context_data(**kwargs)
        context['units'] = self.get_units()
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        return context


class EstimateTracking(TemplateView):
    template_name = 'estimates/tracking.html'

    def get_estimate(self):
        return Estimate.objects.filter(pk=self.kwargs['pk']).first()

    def get_comments(self, estimate):
        content_type = ContentType.objects.filter(app_label='sales', model='estimate').first()
        trackings = Tracking.objects.select_related(
            'profile', 'role'
        ).filter(
            object_id=estimate.id, content_type=content_type
        )
        tracking_by_types = defaultdict(list)
        for tracking in trackings:
            tracking_type = tracking.data.get('tracking_type')
            if tracking_type:
                tracking_by_types[tracking_type].append(tracking)
        return tracking_by_types

    def get_context_data(self, **kwargs):
        context = super(EstimateTracking, self).get_context_data(**kwargs)
        estimate = self.get_estimate()
        tracking = self.get_comments(estimate)
        context['estimate'] = estimate
        context['tracking'] = tracking.get('self')
        context['delivery_tracking'] = tracking.get('delivery')
        context['picking_tracking'] = tracking.get('picking_slip')
        context['invoice_tracking'] = tracking.get('invoice')
        return context


class UpdatePickUpStateView(ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        logger.info('Update pick  delivery status')
        logger.info(self.kwargs)
        estimate = Estimate.objects.filter(id=self.kwargs['pk']).first()
        if estimate:
            if estimate.is_pick_up:
                estimate.is_pick_up = False
                content_type = ContentType.objects.filter(app_label='sales', model='estimate').first()
                Tracking.objects.create(**{'object_id': estimate.id, 'content_type': content_type,
                                           'comment': 'Delivery updated not to pickup', 'profile': self.get_profile(),
                                           'role_id': self.request.session.get('role')
                                           })
            else:
                estimate.is_pick_up = True
            estimate.save()
        return JsonResponse({
            'error': False,
            'text': 'Estimate updated successfully',
            'reload': True
        })
