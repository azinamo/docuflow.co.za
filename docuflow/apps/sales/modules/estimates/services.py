import logging

from django.contrib.contenttypes.fields import ContentType
from django.db import transaction

from docuflow.apps.comment.models import Comment
from docuflow.apps.customer.models import Customer
from docuflow.apps.sales.models import Estimate, EstimateItem, Tracking
from docuflow.apps.sales.enums import InvoiceItemType

logger = logging.getLogger(__name__)


class CreateEstimate:

    def __init__(self, branch, profile, role, form, post_request):
        self.branch = branch
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request

    def create(self):
        estimate = self.form.save(commit=False)
        estimate.branch = self.branch
        estimate.profile = self.profile
        estimate.role = self.role
        estimate.vat_number = self.form.cleaned_data['customer_vat_number']
        estimate.save()
        return estimate

    def add_comment(self, estimate):
        comment = self.form.cleaned_data.get('comment', None)
        if comment:
            Comment.objects.create_comment(
                object_id=estimate.id,
                content_type=ContentType.objects.filter(app_label='sales', model='estimate').first(),
                comment=comment,
                **{'profile': self.profile, 'role': self.role}
            )

    def create_customer(self, estimate):
        customer = Customer.objects.create_cash_customer(estimate.branch.company, estimate.customer_name)
        if customer:
            estimate.customer = customer
            estimate.save()
        return customer

    def execute(self):
        with transaction.atomic():
            estimate = self.create()
            if estimate:
                self.add_comment(estimate)
                self.create_estimate_items(estimate)
                customer = None
                if estimate.customer.is_default:
                    customer = self.create_customer(estimate)
                    estimate.refresh_from_db()
                self.add_addresses(estimate, customer)

    def add_addresses(self, estimate, customer=None):
        postal_address = {}
        delivery_address = {}
        for k, v in self.post_request.items():
            if v != '':
                if k[0:7] == 'postal_':
                    postal_key = k[7:]
                    postal_address[postal_key] = v
                if k[0:9] == 'delivery_':
                    delivery_key = k[9:]
                    delivery_address[delivery_key] = v
        is_pick_up = self.form.cleaned_data.get('is_pick_up', False)
        if is_pick_up:
            delivery_address['is_pick_up'] = is_pick_up
        if customer:
            if 'country' in postal_address and len(postal_address['country']) > 0:
                customer.save_address(postal_address, 2)
            if 'country' in delivery_address and len(delivery_address['country']) > 0:
                customer.save_address(delivery_address, 0)
        estimate.delivery_to = delivery_address
        estimate.postal = postal_address
        estimate.save()

    def create_estimate_items(self, estimate):
        for field, value in self.post_request.items():
            if field.startswith('inventory_item', 0, 14):
                row_id = field[15:]
                inventory_item_id = self.post_request.get('inventory_item_{}'.format(row_id))
                if inventory_item_id:
                    estimate_item = self.add_inventory_estimate(estimate, row_id, inventory_item_id)
            elif field.startswith('account', 0, 7):
                row_id = field[8:]
                account_id = self.post_request.get('account_{}'.format(row_id))
                if account_id:
                    estimate_item = self.add_account_estimate_item(estimate, row_id, account_id)
            elif field.startswith('item_description', 0, 16):
                row_id = field[17:]
                description = self.post_request.get('item_description_{}'.format(row_id))
                if description:
                    estimate_item = self.add_general_estimate_item(estimate, row_id, description)

        if estimate.customer_discount:
            vat_on_customer_discount = estimate.cal_vat_on_discount(estimate.customer_discount)
            estimate.discount_percentage = estimate.get_customer_discount()
            estimate.vat_on_customer_discount = vat_on_customer_discount
        estimate.save()

    def add_inventory_estimate(self, estimate, row_id, inventory_item_id):
        logger.info("add inventory estimate {}  for row {} for inventory {}".format(estimate, row_id, inventory_item_id))
        quantity = self.post_request.get('quantity_{}'.format(row_id))
        price = self.post_request.get('price_{}'.format(row_id), 0)
        discount = self.post_request.get('discount_percentage_{}'.format(row_id), 0)
        vat_code_id = self.post_request.get('vat_code_{}'.format(row_id))
        unit_id = self.post_request.get('unit_id_{}'.format(row_id))
        vat = self.post_request.get('vat_percentage_{}'.format(row_id), 0)
        net_price = self.post_request.get('total_price_excluding_{}'.format(row_id), 0)
        total_amount = self.post_request.get('total_price_including_{}'.format(row_id), 0)
        return EstimateItem.objects.create_inventory_line(
            estimate=estimate,
            inventory_id=inventory_item_id,
            price=price,
            quantity=quantity,
            discount=discount,
            vat_code_id=vat_code_id,
            unit_id=unit_id,
            vat=vat,
            net_price=net_price,
            total_amount=total_amount
        )

    def add_account_estimate_item(self, estimate, row_id, account_id):
        logger.info("add account  estimate line {}  for row {} for account {}".format(estimate, row_id, account_id))
        quantity = self.post_request.get('quantity_{}'.format(row_id))
        price = float(self.post_request.get('price_{}'.format(row_id), 0))
        discount_percentage = self.post_request.get('discount_percentage_{}'.format(row_id), None)
        discount = 0
        if discount_percentage:
            discount = float(discount_percentage)

        vat_code_id = self.post_request.get('vat_code_{}'.format(row_id), None)
        unit_id = self.post_request.get('unit_id_{}'.format(row_id), None)
        vat_percentage = self.post_request.get('vat_percentage_{}'.format(row_id), None)
        vat = 0
        if vat_percentage:
            vat = float(vat_percentage)
        total_vat = self.post_request.get('total_vat_{}'.format(row_id), None)
        vat_amount = 0
        if total_vat:
            vat_amount = float(total_vat)
        total_price_excluding = self.post_request.get('total_price_excluding_{}'.format(row_id), None)
        net_price = 0
        if total_price_excluding:
            net_price = float(total_price_excluding)
        total_price_including = self.post_request.get('total_price_including_{}'.format(row_id), None)
        total_amount = 0
        if total_price_including:
            total_amount = float(total_price_including)
        sub = self.post_request.get('sub_total_{}'.format(row_id), None)
        sub_total = 0
        if sub:
            sub_total = float(sub)
        description = self.post_request.get('description_{}'.format(row_id))

        estimate_item = EstimateItem.objects.create(
            estimate=estimate,
            description=description,
            account_id=account_id,
            price=price,
            quantity=quantity,
            discount=discount,
            vat_code_id=vat_code_id,
            unit_id=unit_id,
            vat=vat,
            net_price=net_price,
            total_amount=total_amount,
            item_type=InvoiceItemType.GENERAL
        )
        return estimate_item

    def add_general_estimate_item(self, estimate, row_id, description):
        logger.info("add general inventory estimate {}  for row {} for inventory {}".format(estimate, row_id, description))
        quantity = self.post_request.get('quantity_{}'.format(row_id))
        price = float(self.post_request.get('price_{}'.format(row_id), 0))
        discount = float(self.post_request.get('discount_percentage_{}'.format(row_id), 0))
        vat = self.post_request.get('vat_percentage_{}'.format(row_id))
        net_price = self.post_request.get('net_price_{}'.format(row_id))
        unit_id = self.post_request.get('unit_{}'.format(row_id))
        vat_code_id = self.post_request.get('vat_code_{}'.format(row_id))

        estimate_item = EstimateItem.objects.create(
            estimate=estimate,
            description=description,
            price=price,
            net_price=net_price,
            quantity=quantity,
            unit_id=unit_id,
            vat_code_id=vat_code_id,
            vat=vat,
            discount=discount,
            item_type=InvoiceItemType.TEXT
        )
        return estimate_item


class CreateInventoryEstimateItem:

    def __init__(self, estimate, inventory, quantity, price, vat_code, discount):
        self.estimate = estimate
        self.inventory = inventory
        self.quantity = quantity
        self.price = price
        self.vat_code = vat_code
        self.discount = discount

    def execute(self):
        pass


class CreateAccountEstimateItem:

    def __init__(self, estimate, account, quantity, price, vat_code, discount):
        self.estimate = estimate
        self.account = account
        self.quantity = quantity
        self.price = price
        self.vat_code = vat_code
        self.discount = discount

    def execute(self):
        pass


class CreateTextEstimateItem:

    def __init__(self, estimate, text):
        self.estimate = estimate
        self.text = text

    def execute(self):
        pass


def print_profoma(estimate: Estimate, profile, role):
    if not estimate.proforma_invoice:
        proforma_count = Estimate.not_deleted.filter(
            branch=estimate.branch,
            period__period_year=estimate.period.period_year,
            proforma_invoice__isnull=False
        ).only('proforma_invoice').count()
        if proforma_count > 0:
            estimate.proforma_invoice = proforma_count + 1
        else:
            estimate.proforma_invoice = 1

    estimate.save()
    estimate.refresh_from_db()

    Tracking.objects.create(
        content_type=ContentType.objects.get_for_model(estimate),
        object_id=estimate.id,
        comment='Proforma printed',
        profile=profile,
        role=role,
        data={
            'tracking_type': 'self',
            'reference': f'{estimate.proforma_reference()}'
        }
    )

    return estimate
