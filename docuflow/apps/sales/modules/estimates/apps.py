﻿from django.apps import AppConfig


class EstimatesConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.estimates'
