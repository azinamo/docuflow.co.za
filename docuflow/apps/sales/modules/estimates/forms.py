import logging

from django import forms
from django.conf import settings
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.urls import reverse

from django_countries.fields import CountryField, Country

from docuflow.apps.period.models import Period
from docuflow.apps.common.enums import SaProvince
from docuflow.apps.comment.services import create_comments
from docuflow.apps.customer.models import Customer, Address
from docuflow.apps.sales.models import Estimate, EstimateItem, Tracking
from docuflow.apps.sales.enums import InvoiceItemType
from docuflow.apps.sales.forms import BaseSalesInventoryForm, BaseSalesDescriptionForm, BaseSalesAccountForm, customer_select_data
from docuflow.apps.customer.modules.address.forms import PostalAddressForm, DeliveryAddressForm
from .tasks import send_estimate_email

logger = logging.getLogger(__name__)


class CreateEstimateItemsMixin(object):

    # noinspection PyUnresolvedReferences
    def create_estimate_items(self, estimate, min_items=1):
        counter = 0
        for field, value in self.request.POST.items():
            if field.startswith('inventory_', 0, 10):
                if value:
                    row_id = field[10:]
                    data = {
                        'inventory': self.request.POST.get(f'inventory_{row_id}', 0),
                        'quantity': self.request.POST.get(f'quantity_{row_id}', 0),
                        'unit': self.request.POST.get(f'unit_id_{row_id}', 0),
                        'price': self.request.POST.get(f'price_{row_id}', 0),
                        'discount': self.request.POST.get(f'discount_percentage_{row_id}', 0),
                        'customer_discount': self.request.POST.get(f'total_discount_{row_id}', 0),
                        'vat_amount': self.request.POST.get(f'vat_amount_{row_id}'),
                        'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
                        'vat': self.request.POST.get(f'vat_percentage_{row_id}'),
                        'net_price': self.request.POST.get(f'total_price_excluding_{row_id}', 0),
                        'total_amount': self.request.POST.get(f'total_price_including_{row_id}')
                    }
                    estimate_item_form = EstimateInventoryItem(data=data, branch=estimate.branch)
                    logger.info(f"{field}={value}:: Is inventory --> {field.startswith('inventory_', 0, 10)}")
                    if estimate_item_form.is_valid():
                        estimate_item_form.execute(estimate)
                        counter += 1
                    else:
                        logger.info(estimate_item_form.errors)
                        raise ValueError(f"Inventory line {counter} count not be saved")

            elif field.startswith('account', 0, 7):
                if value:
                    row_id = field[8:]
                    data = {
                        'account': self.request.POST.get(f'account_{row_id}', None),
                        'quantity': self.request.POST.get(f'quantity_{row_id}', 0),
                        'price': self.request.POST.get(f'price_{row_id}', 0),
                        'discount': self.request.POST.get(f'discount_percentage_{row_id}', 0),
                        'vat_amount': self.request.POST.get(f'vat_amount_{row_id}'),
                        'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
                        'vat': self.request.POST.get(f'vat_percentage_{row_id}'),
                        'total_amount': self.request.POST.get(f'total_price_including_{row_id}'),
                        'net_price': self.request.POST.get(f'total_price_excluding_{row_id}', 0),
                        'description': self.request.POST.get(f'description_{row_id}', '')
                    }
                    estimate_item_form = EstimateAccountItem(data=data, branch=estimate.branch)
                    if estimate_item_form.is_valid():
                        estimate_item_form.execute(estimate)
                        counter += 1
                    else:
                        logger.info(estimate_item_form.errors)
                        ValueError(f"Account line {counter} count not be saved")
            elif field.startswith('item_description', 0, 16):
                if value:
                    row_id = field[17:]
                    data = {
                        'description': self.request.POST.get(f'item_description_{row_id}', 0),
                        'estimate': estimate
                    }
                    estimate_item_form = EstimateTextItem(data=data)
                    logger.info(f"{field}={value}:: Is text description --> {field.startswith('item_description', 0, 16)}")
                    if estimate_item_form.is_valid():
                        estimate_item_form.execute(estimate)
                        counter += 1
                    else:
                        logger.info(estimate_item_form.errors)
                        ValueError(f"Text line {counter} count not be saved")
        if counter < min_items:
            raise forms.ValidationError('Please add valid estimate lines')


class CreateEstimateAddressMixin(object):

    # noinspection PyUnresolvedReferences
    def create_address(self, estimate):
        if self.cleaned_data['postal_street']:
            postal_address_data = {
                'address_line_1': self.cleaned_data['postal_street'],
                'city': self.cleaned_data['postal_city'],
                'postal_code': self.cleaned_data['postal_postal_code'],
                'province': self.cleaned_data['postal_province'],
                'country': self.cleaned_data['postal_country'],
                'customer': estimate.customer
            }

            postal_address = estimate.customer.postal_address
            if postal_address:
                postal_address_form = PostalAddressForm(data=postal_address_data, instance=postal_address)
            else:
                postal_address_form = PostalAddressForm(data=postal_address_data)

            if postal_address_form.is_valid():
                # if not estimate.customer.postal_address:
                postal_address_form.save()
                postal_address_data.pop('customer')
                logger.info(Country(code=self.cleaned_data['postal_country']).name)
                postal_address_data['country_name'] = Country(code=self.cleaned_data['postal_country']).name
                estimate.postal = postal_address_data
                estimate.save()
            else:
                logger.info(postal_address_form.errors)

        if self.cleaned_data['delivery_street']:
            delivery_address_data = {
                'address_line_1': self.cleaned_data['delivery_street'],
                'city': self.cleaned_data['delivery_city'],
                'postal_code': self.cleaned_data['delivery_postal_code'],
                'province': self.cleaned_data['delivery_province'],
                'country': self.cleaned_data['delivery_country'],
                'customer': estimate.customer
            }
            delivery_address_form = DeliveryAddressForm(data=delivery_address_data)
            if delivery_address_form.is_valid():
                delivery_address_data.pop('customer')
                delivery_address_data['country_name'] = Country(code=self.cleaned_data['postal_country']).name
                estimate.delivery_to = delivery_address_data
                estimate.save()
            else:
                logger.info(delivery_address_form.errors)


class CustomerEstimateForm(CreateEstimateItemsMixin, CreateEstimateAddressMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        self.year = kwargs.pop('year')
        self.is_template = kwargs.pop('is_template')
        super().__init__(*args, **kwargs)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.branch.company)
        self.fields['expires_at'].label = 'Expires At'
        self.fields['customer'].widget = customer_select_data(self.branch.company)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.branch.company)
        self.fields['branch'].empty_label = None
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['delivery_country'].required = False
        self.fields['delivery_address'].queryset = Address.objects.filter(customer__company=self.branch.company)
        if self.is_template:
            self.fields['period'].widget = forms.HiddenInput()
            self.fields['period'].required = False
            self.fields['expires_at'].widget = forms.HiddenInput()
            self.fields['expires_at'].required = False
            self.fields['date'].widget = forms.HiddenInput()
            self.fields['date'].required = False

    customer_vat_number = forms.CharField(required=False, label='Customer Vat Number')
    comment = forms.CharField(widget=forms.Textarea(), required=False, label='Comment')
    email_address = forms.EmailField(required=False)
    customer_name = forms.CharField(required=False)
    postal_street = forms.CharField(required=False)
    postal_city = forms.CharField(required=False)
    postal_province = forms.CharField(required=False)
    postal_postal_code = forms.CharField(required=False)
    postal_country = CountryField(default=settings.COUNTRY_CODE).formfield(required=False)
    delivery_address = forms.ModelChoiceField(required=False, queryset=None)
    delivery_street = forms.CharField(required=False)
    delivery_city = forms.CharField(required=False)
    delivery_province = forms.CharField(required=False)
    delivery_postal_code = forms.CharField(required=False)
    delivery_country = CountryField(default=settings.COUNTRY_CODE).formfield(required=False)

    class Meta:
        model = Estimate
        fields = ('branch', 'customer', 'customer_name', 'period', 'date', 'expires_at', 'is_deposit_required',
                  'deposit', 'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount',
                  'customer_discount', 'line_discount', 'salesman', 'is_pick_up', 'discount_percentage',
                  'email_address', 'phone_number', 'is_template')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()

        if not self.cleaned_data['customer']:
            self.add_error('customer', 'Customer is required')

        if not self.is_template:
            period = cleaned_data.get('period')
            date = cleaned_data['date']
            if not period:
                self.add_error('period', 'Period is required')

            if not date:
                self.add_error('date', 'Date is required')

            if period and date:
                if not period.is_valid(date):
                    self.add_error('date', 'Period and payment date do not match')

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')

        if self.request.POST.get('next') == 'email' and not self.cleaned_data['email_address']:
            self.add_error('email_address', 'Email address is required to send the email')

        is_pick_up = self.cleaned_data['is_pick_up']
        if not is_pick_up and not self.cleaned_data['delivery_country']:
            self.add_error('delivery_country', 'Delivery country is required')

        return cleaned_data

    def save(self, commit=True):
        with transaction.atomic():
            estimate = super(CustomerEstimateForm, self).save(commit=False)
            estimate.profile = self.profile
            estimate.role = self.role
            estimate.vat_number = self.cleaned_data['customer_vat_number']
            estimate.save()

            if estimate.customer.is_default:
                customer_data = {}
                phone_number = self.cleaned_data.get('phone_number')
                vat_number = self.cleaned_data.get('vat_number')
                if phone_number:
                    customer_data['phone_number'] = phone_number
                if vat_number:
                    customer_data['vat_number'] = vat_number
                customer = Customer.objects.create_cash_customer(
                    estimate.branch.company, estimate.customer_name,
                    **customer_data
                )
                if customer:
                    estimate.customer = customer
                    estimate.save()

                if customer:
                    estimate.customer = customer
                    estimate.save()
                estimate.refresh_from_db()

            customer = estimate.customer
            if not customer.salesman:
                customer.salesman = estimate.salesman
            if not customer.vat_number and estimate.vat_number:
                customer.vat_number = estimate.vat_number
            if not customer.phone_number and estimate.phone_number:
                customer.phone_number = estimate.phone_number
            customer.save()

            self.create_address(estimate)

            self.add_comment(estimate, self.profile, self.role)

            self.create_estimate_items(estimate)

            if self.request.POST.get('next') == 'email':
                send_estimate_email(estimate.id, self.cleaned_data['email_address'])
        return estimate

    def create_customer(self, company, customer_name):
        if not customer_name:
            raise ValueError('Customer name is required')
        return Customer.objects.create_cash_customer(company, customer_name)

    def add_comment(self, estimate, profile, role):
        content_type = ContentType.objects.filter(app_label='sales', model='estimate').first()
        create_comments(self.request, content_type, estimate.id, profile, role)


class CopyEstimateForm(CreateEstimateItemsMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.estimate = kwargs.pop('estimate')
        self.year = kwargs.pop('year')
        self.profile = kwargs.pop('profile')
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)
        self.fields['salesman'].queryset = self.fields['salesman'].queryset.filter(company=self.branch.company)
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=self.branch.company)
        self.fields['branch'].empty_label = None
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    telephone = forms.CharField(required=False)
    customer_vat_number = forms.CharField(required=False)
    comment = forms.CharField(widget=forms.Textarea(), required=False, label='Comment')

    class Meta:
        model = Estimate
        fields = ('branch', 'customer', 'customer_name', 'period', 'date', 'expires_at', 'is_deposit_required',
                  'deposit', 'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount',
                  'customer_discount', 'line_discount', 'salesman', 'is_pick_up', 'discount_percentage')
        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')
        return cleaned_data

    def save(self, commit=True):
        estimate = super().save(commit=False)
        content_type = ContentType.objects.filter(app_label='sales', model='estimate').first()
        estimate.branch = self.branch
        estimate.customer = self.estimate.customer
        estimate.profile = self.profile
        estimate.period = estimate.period
        estimate.content_type = content_type
        estimate.object_id = self.estimate.id
        estimate.role_id = self.request.session['role']
        estimate.delivery_to = self.estimate.delivery_to
        estimate.postal = self.estimate.postal
        estimate.save()

        estimate.vat_on_customer_discount = estimate.cal_vat_on_discount(estimate.customer_discount)
        estimate.save()

        self.copy_estimate_to_items(estimate)
        self.create_estimate_items(estimate, 0)
        self.add_tracking(estimate)

    def copy_estimate_to_items(self, estimate):
        for field, value in self.request.POST.items():
            if field.startswith('estimate_id', 0, 11):
                row_id = field[12:]
                estimate_item = EstimateItem.objects.get(pk=value)
                if estimate_item.is_inventory:
                    data = {
                        'quantity': self.request.POST.get(f'quantity_{row_id}'),
                        'price': self.request.POST.get(f'price_{row_id}'),
                        'net_price': self.request.POST.get(f'total_price_excluding_{row_id}'),
                        'total_amount': self.request.POST.get(f'total_price_including_{row_id}'),
                        'vat': self.request.POST.get(f'vat_percentage_{row_id}'),
                        'discount': self.request.POST.get(f'discount_percentage_{row_id}'),
                        'vat_code': estimate_item.vat_code,
                        'inventory': estimate_item.inventory,
                        'unit': estimate_item.unit,
                        'vat_amount': self.request.POST.get(f'vat_amount_{row_id}')
                    }
                    estimate_item_form = EstimateInventoryItem(data=data, branch=self.branch)
                    if estimate_item_form.is_valid():
                        inventory_estimate_item = estimate_item_form.save(commit=False)
                        inventory_estimate_item.estimate = estimate
                        inventory_estimate_item.item_type = InvoiceItemType.INVENTORY
                        inventory_estimate_item.save()
                    else:
                        logger.info(estimate_item_form.errors)
                        raise forms.ValidationError('Copying inventory line failed')

                elif estimate_item.is_account:
                    data = {
                        'quantity': self.request.POST.get(f'quantity_{row_id}'),
                        'price': self.request.POST.get(f'price_{row_id}'),
                        'net_price': self.request.POST.get(f'total_price_excluding_{row_id}'),
                        'vat': self.request.POST.get(f'vat_percentage_{row_id}'),
                        'discount': self.request.POST.get(f'discount_percentage_{row_id}'),
                        'vat_code': self.request.POST.get(f'vat_code_{row_id}'),
                        'account': estimate_item.account,
                        'description': estimate_item.description,
                        'unit': estimate_item.unit,
                        'vat_amount': self.request.POST.get(f'vat_amount_{row_id}')
                    }
                    account_form = EstimateAccountItem(data=data, branch=self.branch)
                    if account_form.is_valid():
                        account_estimate_item = account_form.save(commit=False)
                        account_estimate_item.item_type = InvoiceItemType.GENERAL
                        account_estimate_item.estimate = estimate
                        account_estimate_item.save()

                elif estimate_item.is_description:
                    data = {
                        'description': self.request.POST.get(f'description_{row_id}'),
                    }
                    estimate_item_form = EstimateTextItem(data=data)
                    if estimate_item_form.is_valid():
                        text_estimate_item = estimate_item_form.save(commit=False)
                        text_estimate_item.estimate = estimate
                        text_estimate_item.item_type = InvoiceItemType.TEXT
                        text_estimate_item.save()

    def add_tracking(self, estimate):
        Tracking.objects.create(
            object_id=estimate.pk,
            content_type=ContentType.objects.get_for_model(estimate),
            profile=self.profile,
            comment='Estimate copied',
            data={
                'reference': str(self.estimate),
                'tracking_type': 'self',
                'link_url': reverse('estimate_detail', args=(self.estimate.pk, ))
            }
        )
        Tracking.objects.create(
            object_id=self.estimate.pk,
            content_type=ContentType.objects.get_for_model(self.estimate),
            profile=self.profile,
            comment='Estimate copy created',
            data={
                'reference': str(estimate),
                'tracking_type': 'self',
                'link_url': reverse('estimate_detail', args=(estimate.pk, ))
            }
        )


class EstimateInventoryItem(BaseSalesInventoryForm):

    class Meta:
        model = EstimateItem
        fields = ('inventory', 'quantity', 'vat', 'unit', 'price', 'vat_code', 'discount', 'net_price', 'total_amount',
                  'vat_amount', 'customer_discount')

    def execute(self, estimate):
        estimate_item = self.save(commit=False)
        estimate_item.item_type = InvoiceItemType.INVENTORY
        estimate_item.unit = self.cleaned_data['inventory'].unit
        estimate_item.estimate = estimate
        estimate_item.save()
        if estimate_item.vat_code and not estimate_item.vat:
            estimate_item.vat = estimate_item.vat_code.percentage
        return estimate_item


class EstimateAccountItem(BaseSalesAccountForm):

    class Meta:
        model = EstimateItem
        fields = ('account', 'description', 'quantity', 'price', 'vat_code', 'discount', 'net_price', 'total_amount',
                  'vat_amount', 'vat')

    def execute(self, estimate):
        estimate_item = self.save(commit=False)
        estimate_item.item_type = InvoiceItemType.GENERAL
        estimate_item.estimate = estimate
        estimate_item.save()
        if estimate_item.vat_code and not estimate_item.vat:
            estimate_item.vat = estimate_item.vat_code.percentage
        return estimate_item


class EstimateTextItem(BaseSalesDescriptionForm):

    class Meta:
        model = EstimateItem
        fields = ('description', )

    def execute(self, estimate):
        estimate_item = self.save(commit=False)
        estimate_item.item_type = InvoiceItemType.TEXT
        estimate_item.estimate = estimate
        estimate_item.save()
        return estimate_item


class CashEstimateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        super(CashEstimateForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(company, year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=company)
        self.fields['branch'].empty_label = None

    postal_province = forms.ChoiceField(required=False, choices=SaProvince.choices())
    delivery_province = forms.ChoiceField(required=False, choices=SaProvince.choices())

    postal_country = CountryField(default=settings.COUNTRY_CODE).formfield()
    delivery_country = CountryField(default=settings.COUNTRY_CODE).formfield()
    comment = forms.CharField(widget=forms.Textarea(), required=False, label='Comment')

    class Meta:
        model = Estimate
        fields = ('branch', 'customer_name', 'period', 'date', 'expires_at', 'is_deposit_required', 'phone_number',
                  'sub_total', 'discount_amount', 'line_discount', 'deposit', 'net_amount', 'total_amount',
                  'vat_amount', 'is_pick_up')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(CashEstimateForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        if 'supplier' in cleaned_data and not cleaned_data['supplier']:
            self.add_error('supplier', 'Supplier is required')
        return cleaned_data


class DeliveryBackOrderForm(forms.Form):
    date = forms.DateField(required=False, label='BackOrder')


class CreateTemplateEstimateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.year = kwargs.pop('year')
        self.estimate_template = kwargs.pop('template')
        super(CreateTemplateEstimateForm, self).__init__(*args, **kwargs)
        self.fields['expires_at'].label = 'Expires At'
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    class Meta:
        model = Estimate
        fields = ['period', 'date', 'expires_at']

    def clean(self):
        cleaned_data = super().clean()

        date = cleaned_data.get('date', None)
        period = cleaned_data.get('period', None)
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and invoice date do not match')

        return cleaned_data

    def save(self, commit=True):
        estimate = Estimate.objects.create(
            branch=self.estimate_template.branch,
            customer=self.estimate_template.customer,
            customer_name=self.estimate_template.customer_name,
            period=self.cleaned_data['period'],
            date=self.cleaned_data['date'],
            phone_number=self.estimate_template.phone_number,
            sub_total=self.estimate_template.sub_total,
            discount_amount=self.estimate_template.discount_amount,
            line_discount=self.estimate_template.line_discount,
            net_amount=self.estimate_template.net_amount,
            total_amount=self.estimate_template.total_amount,
            vat_amount=self.estimate_template.vat_amount,
            is_pick_up=self.estimate_template.is_pick_up,
            delivery_to=self.estimate_template.delivery_to,
            postal=self.estimate_template.postal,
            customer_discount=self.estimate_template.customer_discount,
            vat_number=self.estimate_template.vat_number,
            expires_at=self.cleaned_data['expires_at'],
            profile=self.profile,
            role=self.role,
            template=self.estimate_template
        )

        for estimate_item in self.estimate_template.estimate_items.all():
            EstimateItem.objects.create(
                estimate=estimate,
                description=estimate_item.description,
                quantity=estimate_item.quantity,
                price=estimate_item.price,
                discount=estimate_item.discount,
                net_price=estimate_item.net_price,
                inventory=estimate_item.inventory,
                unit=estimate_item.unit,
                customer_discount=estimate_item.customer_discount,
                total_amount=estimate_item.total_amount,
                vat=estimate_item.vat,
                vat_code=estimate_item.vat_code,
                account=estimate_item.account,
                item_type=estimate_item.item_type,
                vat_amount=estimate_item.vat_amount
            )


class CopyEstimateToCustomerForm(CreateEstimateItemsMixin, CreateEstimateAddressMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.estimate = kwargs.pop('estimate')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        super().__init__(*args, **kwargs)
        self.fields['customer'].widget = customer_select_data(self.estimate.branch.company)
        self.fields['delivery_address'].queryset = Address.objects.filter(customer__company=self.estimate.branch.company)

    postal_street = forms.CharField(required=False)
    postal_city = forms.CharField(required=False)
    postal_province = forms.CharField(required=False)
    postal_postal_code = forms.CharField(required=False)
    postal_country = CountryField(default=settings.COUNTRY_CODE).formfield(required=False)
    delivery_address = forms.ModelChoiceField(required=False, queryset=None)
    delivery_street = forms.CharField(required=False)
    delivery_city = forms.CharField(required=False)
    delivery_province = forms.CharField(required=False)
    delivery_postal_code = forms.CharField(required=False)
    delivery_country = CountryField(default=settings.COUNTRY_CODE).formfield(required=False)

    class Meta:
        model = Estimate
        fields = ('customer', 'postal_street', 'postal_city', 'postal_province', 'postal_postal_code', 'postal_country',
                  'delivery_street', 'delivery_city', 'delivery_province', 'delivery_postal_code', 'delivery_address',
                  'delivery_country'
                  )
        widgets = {
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
        }

    def save(self, commit=True):
        estimate = super().save(commit=False)
        estimate.branch = self.estimate.branch
        estimate.customer = self.cleaned_data['customer']
        estimate.profile = self.estimate.profile
        estimate.period = self.estimate.period
        estimate.content_type = ContentType.objects.get_for_model(self.estimate)
        estimate.object_id = estimate.id
        estimate.role_id = self.request.session['role']
        estimate.date = self.estimate.date
        estimate.net_amount = self.estimate.net_amount
        estimate.total_amount = self.estimate.total_amount
        estimate.vat_amount = self.estimate.vat_amount
        estimate.sub_total = self.estimate.sub_total
        estimate.discount_amount = self.estimate.discount_amount
        estimate.customer_discount = self.estimate.customer_discount
        estimate.line_discount = self.estimate.line_discount
        estimate.sub_total = self.estimate.sub_total
        estimate.sub_total = self.estimate.sub_total
        estimate.sub_total = self.estimate.sub_total
        estimate.expires_at = self.estimate.expires_at
        # estimate.is_pick_up = self.estimate.is_pick_up
        estimate.salesman = self.estimate.salesman
        estimate.discount_percentage = self.estimate.discount_percentage
        estimate.save()

        estimate.vat_on_customer_discount = estimate.cal_vat_on_discount(estimate.customer_discount)
        estimate.save()

        self.create_address(estimate=estimate)
        self.copy_estimate_items(estimate=estimate)
        self.add_tracking(estimate=estimate)

    def copy_estimate_items(self, estimate):
        for estimate_item in self.estimate.estimate_items.all():
            if estimate_item.is_inventory:
                EstimateItem.objects.create(
                    quantity=estimate_item.quantity,
                    price=estimate_item.price,
                    net_price=estimate_item.net_price,
                    total_amount=estimate_item.total_amount,
                    vat=estimate_item.vat,
                    discount=estimate_item.discount,
                    vat_code=estimate_item.vat_code,
                    inventory=estimate_item.inventory,
                    unit=estimate_item.unit,
                    vat_amount=estimate_item.vat_amount,
                    estimate=estimate,
                    item_type=InvoiceItemType.INVENTORY
                )
            elif estimate_item.is_account:
                EstimateItem.objects.create(
                    quantity=estimate_item.quantity,
                    price=estimate_item.price,
                    net_price=estimate_item.net_price,
                    total_amount=estimate_item.total_amount,
                    vat=estimate_item.vat,
                    discount=estimate_item.discount,
                    vat_code=estimate_item.vat_code,
                    account=estimate_item.account,
                    unit=estimate_item.unit,
                    vat_amount=estimate_item.vat_amount,
                    estimate=estimate,
                    item_type=InvoiceItemType.GENERAL
                )
            elif estimate_item.is_description:
                EstimateItem.objects.create(
                    description=estimate_item.description,
                    estimate=estimate,
                    item_type=InvoiceItemType.TEXT
                )

    def add_tracking(self, estimate):
        Tracking.objects.create(
            object_id=estimate.pk,
            content_type=ContentType.objects.get_for_model(estimate),
            profile=self.profile,
            comment='Estimate copied',
            data={
                'reference': str(self.estimate),
                'tracking_type': 'self',
                'link_url': reverse('estimate_detail', args=(self.estimate.pk, ))
            }
        )
        Tracking.objects.create(
            object_id=self.estimate.pk,
            content_type=ContentType.objects.get_for_model(self.estimate),
            profile=self.profile,
            comment='Estimate copy created',
            data={
                'reference': str(estimate),
                'tracking_type': 'self',
                'link_url': reverse('estimate_detail', args=(estimate.pk, ))
            }
        )