import django_filters

from docuflow.apps.sales.models import Estimate


class EstimateFilter(django_filters.FilterSet):
    estimate_id = django_filters.CharFilter(lookup_expr='icontains')
    customer = django_filters.CharFilter(lookup_expr='icontains', field_name='customer__number')
    customer_name = django_filters.CharFilter(lookup_expr='icontains', field_name='customer__name')
    date = django_filters.DateFilter(lookup_expr='icontains', field_name='date')
    expires_at = django_filters.DateFilter(lookup_expr='icontains', field_name='expires_at')
    total_amount = django_filters.DateFilter(lookup_expr='gte', field_name='total_amount')

    class Meta:
        model = Estimate
        fields = ['estimate_id', 'customer', 'customer_name', 'date', 'expires_at', 'total_amount']
