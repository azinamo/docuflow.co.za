from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='picking_slips'),
    path('create/from/<int:estimate_id>/estimate/', views.CreateFromEstimateView.as_view(),
         name='create_estimate_picking_slip'),
    path('create/from/<int:back_order_id>/back-order/', views.CreateFromBackOrderView.as_view(),
         name='create_back_order_picking_slip'),
    path('create/<int:pk>/delivery/note/', csrf_exempt(views.CreateDeliveryNoteView.as_view()),
         name='create_picking_slip_delivery_note'),
    path('<int:pk>/detail/', views.DetailView.as_view(), name='picking_detail'),
    path('create/from/<int:delivery_id>/delivery/', csrf_exempt(views.CreateFromDeliveryView.as_view()),
         name='create_picking_slip_from_delivery'),
    path('create/from/<int:invoice_id>/invoice/', views.CreateFromInvoiceView.as_view(),
         name='create_picking_slip_from_invoice'),
    path('<int:pk>/tracking/', views.TrackingView.as_view(), name='picking_slip_tracking'),
    path('<str:action>/', views.SinglePickingSlipActionView.as_view(), name='single_picking_slip_action')
]
