from django.apps import AppConfig


class PickingslipConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.pickingslip'
