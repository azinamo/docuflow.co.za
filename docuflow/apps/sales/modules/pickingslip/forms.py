import logging

from django import forms
from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse

from docuflow.apps.sales.models import PickingSlip, Delivery, EstimateItem, PickingSlipItem, Tracking
from docuflow.apps.sales.enums import InvoiceItemType, TrackingType, CommentAction
from docuflow.apps.sales.forms import BaseSalesDescriptionForm, BaseSalesInventoryForm
from docuflow.apps.sales.modules.backorder.services import CreateBackOrderFromSalesInvoice

logger = logging.getLogger(__name__)


class BasePickingSlipForm(forms.ModelForm):
    sales_invoice = None

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        # self.profile = kwargs.pop('profile')
        super(BasePickingSlipForm, self).__init__(*args, **kwargs)
        self.fields['delivery_method'].queryset = self.fields['delivery_method'].queryset.filter(company=self.company)
        self.fields['delivery_term'].queryset = self.fields['delivery_term'].queryset.filter(company=self.company)
        if self.sales_invoice and self.sales_invoice.is_pick_up:
            self.fields['delivery_method'].label = 'Collection Method'
            self.fields['delivery_term'].label = 'Collection Term'
            self.fields['date'].label = 'Collection Date'
            self.fields['delivery_time'].label = 'Collection Time'
        else:
            self.fields['date'].label = 'Delivery Date'

    class Meta:
        model = PickingSlip
        fields = ('delivery_term', 'delivery_method', 'date', 'purchase_order', 'delivery_time', 'is_pick_up',
                  'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount', 'customer_discount',
                  'line_discount')
        widgets = {
            'delivery_term': forms.Select(attrs={'class': 'form-control'}),
            'delivery_method': forms.Select(attrs={'class': 'form-control'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }


class PickingSlipFromEstimateForm(BasePickingSlipForm):

    def __init__(self, *args, **kwargs):
        self.estimate = kwargs.pop('estimate')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.request = kwargs.pop('request')
        super(PickingSlipFromEstimateForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        picking_slip = super().save(commit=False)
        picking_slip.branch = self.estimate.branch
        picking_slip.profile = self.profile
        picking_slip.estimate = self.estimate
        picking_slip.customer = self.estimate.customer
        if self.estimate.salesman:
            picking_slip.salesman = self.estimate.salesman
        picking_slip.discount_percentage = self.estimate.discount_percentage
        picking_slip.vat_number = self.estimate.vat_number
        picking_slip.customer_name = self.estimate.customer_name
        picking_slip.period = self.estimate.period
        picking_slip.date = self.cleaned_data['date']
        picking_slip.is_pick_up = self.cleaned_data['is_pick_up']
        picking_slip.delivery_to = self.estimate.delivery_to
        picking_slip.postal = self.estimate.postal
        picking_slip.vat_on_customer_discount = self.estimate.cal_vat_on_discount(self.estimate.customer_discount)
        picking_slip.save()

        picking_slip.mark_as_send_for_picking()

        self.create_picking_items_from_estimate_items(picking_slip)

        self.complete_estimate(picking_slip=picking_slip)

        return picking_slip

    def create_picking_items_from_estimate_items(self, picking_slip):
        logger.info("------------create_picking_items_from_source_items---------------")
        picked_count = 0
        counter = 0
        back_order_items = []
        non_inventory_items = []
        total_net_amount = 0
        total_amount = 0
        total_vat = 0
        total_discount = 0
        sub_total = 0
        for estimate_item in EstimateItem.objects.filter(estimate=self.estimate).all():
            if estimate_item.is_account:
                non_inventory_items.append({'item': estimate_item, 'quantity': estimate_item.quantity})
                self.save_account_item(picking_slip=picking_slip, estimate_item=estimate_item)
                counter += 1
            elif estimate_item.is_description:
                non_inventory_items.append({'item': estimate_item, 'quantity': estimate_item.quantity})
                self.save_description_item(picking_slip=picking_slip, estimate_item=estimate_item)
                counter += 1
            else:
                counter += 1
                is_picked = bool(self.request.POST.get(f'is_picked_{estimate_item.id}', False))
                is_back_order = self.request.POST.get(f'back_order_{estimate_item.id}', False)
                if is_picked:
                    logger.info("---- Allow picking ---")
                    qty = self.request.POST.get(f'quantity_{estimate_item.id}', 0)
                    logger.info(f"Quantity -> {qty}, is_back order {is_back_order}  and is picking {is_picked}")
                    data = {
                        'quantity': qty,
                        'ordered': estimate_item.quantity,
                        'picked': qty,
                        'price': estimate_item.price,
                        'discount': estimate_item.discount,
                        'vat': estimate_item.vat,
                        'inventory': estimate_item.inventory,
                        'unit': estimate_item.unit,
                        'vat_code': estimate_item.vat_code,
                        'picking_slip': picking_slip,
                        'vat_amount': self.request.POST.get(f'vat_amount_{estimate_item.id}', 0),
                        'net_price': self.request.POST.get(f'total_price_excluding_{estimate_item.id}', 0),
                        'total_amount': self.request.POST.get(f'total_price_including_{estimate_item.id}', 0)
                    }
                    logger.info(data)
                    inventory_picking_slip_form = InventoryPickingSlipItemForm(data=data, branch=picking_slip.branch)
                    if inventory_picking_slip_form.is_valid():
                        picking_item = inventory_picking_slip_form.save(commit=False)

                        total_vat += picking_item.vat_amount
                        total_net_amount += picking_item.net_price
                        total_amount += picking_item.total_amount
                        total_discount += picking_item.discount_amount
                        sub_total += picking_item.net_price + picking_item.discount_amount

                        if is_back_order:
                            back_order_qty = estimate_item.quantity - picking_item.quantity
                            logger.info(f"Back order quantity = {back_order_qty}")
                            if back_order_qty > 0:
                                back_order_items.append({'item': picking_item, 'quantity': back_order_qty})

                    else:
                        logger.info(inventory_picking_slip_form.errors)
                        raise ValueError('Error occurred saving picking items')

                    picked_count += 1
                    estimate_item.status = PickingSlip.SEND_FOR_PICKING
                    estimate_item.save()
                elif is_back_order:
                    back_order_qty = float(self.request.POST.get(f'picking_balance_{estimate_item.id}'))
                    logger.info(f"Back order quantity = {back_order_qty}")
                    if back_order_qty > 0:
                        back_order_items.append({'item': estimate_item, 'quantity': back_order_qty})
        picking_slip.vat_amount = total_vat
        picking_slip.net_amount = total_net_amount
        picking_slip.total_amount = total_amount
        picking_slip.discount_amount = total_discount
        picking_slip.sub_total = sub_total
        picking_slip.save()

        if back_order_items:
            back_order_items = back_order_items + non_inventory_items
            create_back_order = CreateBackOrderFromSalesInvoice(picking_slip, back_order_items)
            back_order = create_back_order.execute()

            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(back_order),
                object_id=back_order.id,
                comment='Back order created from picking slip',
                profile=self.profile,
                data={
                    'tracking_type': 'picking_slip',
                    'reference': str(picking_slip),
                    'link_url': reverse('picking_detail', args=(picking_slip.id,))
                }
            )
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(picking_slip),
                object_id=picking_slip.id,
                comment='Back order created ',
                profile=self.profile,
                data={
                    'tracking_type': 'back_order',
                    'reference': str(back_order),
                    'link_url': reverse('backorder_detail', args=(back_order.id,))
                }
            )

        return picked_count == counter

    # noinspection PyMethodMayBeStatic
    def save_account_item(self, picking_slip, estimate_item):
        logger.info("Added account item to the picking slip")
        PickingSlipItem.objects.create(
            picking_slip=picking_slip,
            received=estimate_item.quantity,
            picked=estimate_item.quantity,
            quantity=estimate_item.quantity,
            price=estimate_item.price,
            discount=estimate_item.discount,
            net_price=estimate_item.net_price,
            total_amount=estimate_item.total_amount,
            vat_amount=estimate_item.vat_amount,
            vat=estimate_item.vat,
            account=estimate_item.account,
            unit=estimate_item.unit,
            vat_code=estimate_item.vat_code,
            item_type=InvoiceItemType.GENERAL,
            description=estimate_item.description
        )

    # noinspection PyMethodMayBeStatic
    def save_description_item(self, picking_slip, estimate_item):
        logger.info("Added description item to the picking slip")
        PickingSlipItem.objects.create(
            picking_slip=picking_slip,
            item_type=InvoiceItemType.TEXT,
            description=estimate_item.description
        )

    def complete_estimate(self, picking_slip):
        self.estimate.mark_as_completed()

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.estimate),
            object_id=self.estimate.pk,
            comment=f'Estimate {str(self.estimate.status)}',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'picking_slip',
                'reference': str(picking_slip),
                'comment_type': str(TrackingType.ACTION),
                'link_url': reverse('picking_detail', args=(picking_slip.pk, )),
                'action': str(CommentAction.UPDATED)
            }
        )
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(picking_slip),
            object_id=picking_slip.pk,
            comment=f'Picking slip created from estimate',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'estimate',
                'reference': str(self.estimate),
                'comment_type': str(TrackingType.ACTION),
                'link_url': reverse('estimate_detail', args=(self.estimate.pk, )),
                'action': str(CommentAction.UPDATED)
            }
        )


class BackOrderPickingSlipForm(BasePickingSlipForm):

    def __init__(self, *args, **kwargs):
        self.back_order = kwargs.pop('back_order')
        self.profile = kwargs.pop('profile')
        super(BackOrderPickingSlipForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        picking_slip = super().save(commit=False)
        picking_slip.branch = self.back_order.branch
        picking_slip.profile = self.profile
        picking_slip.back_order = self.back_order
        picking_slip.customer = self.back_order.customer
        picking_slip.customer_name = self.back_order.customer_name
        picking_slip.period = self.back_order.period
        picking_slip.date = self.cleaned_data['date']
        picking_slip.delivery_to = self.back_order.delivery_to
        picking_slip.postal = self.back_order.postal
        picking_slip.discount_percentage = self.back_order.discount_percentage
        picking_slip.vat_on_customer_discount = self.back_order.cal_vat_on_discount(self.back_order.customer_discount)
        picking_slip.save()
        if picking_slip:
            picking_slip.mark_as_send_for_picking()
        return picking_slip


class InvoicePickingSlipForm(BasePickingSlipForm):

    def __init__(self, *args, **kwargs):
        self.invoice = kwargs.pop('invoice')
        self.profile = kwargs.pop('profile')
        super(InvoicePickingSlipForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        picking_slip = super().save(commit=False)
        picking_slip.branch = self.invoice.branch
        picking_slip.profile = self.profile
        picking_slip.invoice = self.invoice
        picking_slip.customer = self.invoice.customer
        if self.invoice.salesman:
            picking_slip.salesman = self.invoice.salesman
        picking_slip.discount_percentage = self.invoice.discount_percentage
        picking_slip.vat_number = self.invoice.vat_number
        picking_slip.customer_name = self.invoice.customer_name
        picking_slip.period = self.invoice.period
        picking_slip.date = self.cleaned_data['date']
        picking_slip.is_pick_up = self.invoice.is_pick_up
        picking_slip.delivery_to = self.invoice.delivery_to
        picking_slip.postal = self.invoice.postal
        picking_slip.vat_on_customer_discount = self.invoice.cal_vat_on_discount(self.invoice.customer_discount)
        picking_slip.save()
        if picking_slip:
            picking_slip.mark_as_send_for_picking()
        return picking_slip


class PickingSlipForm(forms.Form):
    pass


class PickingSlipDeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile')
        self.picking_slip = kwargs.pop('picking_slip')
        super(PickingSlipDeliveryForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Delivery
        fields = ('net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount', 'customer_discount',
                  'line_discount')
        widgets = {
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def save(self, commit=True):
        delivery = super().save(commit=False)
        if self.picking_slip.is_pick_up:
            delivery.category = Delivery.COLLECTION
        delivery.picking_slip = self.picking_slip
        delivery.branch = self.picking_slip.branch
        delivery.profile = self.profile
        delivery.period = self.picking_slip.period
        delivery.date = self.picking_slip.date
        delivery.customer = self.picking_slip.customer
        delivery.customer_name = self.picking_slip.customer_name
        delivery.vat_number = self.picking_slip.vat_number
        delivery.phone_number = self.picking_slip.phone_number
        delivery.salesman = self.picking_slip.salesman
        delivery.purchase_order = self.picking_slip.purchase_order
        delivery.delivery_time = self.picking_slip.delivery_time
        # delivery.net_amount = self.picking_slip.net_amount
        # delivery.vat_amount = self.picking_slip.vat_amount
        # delivery.total_amount = self.picking_slip.total_amount
        # delivery.sub_total = self.picking_slip.sub_total
        delivery.customer_discount = self.picking_slip.customer_discount
        delivery.vat_on_customer_discount = self.picking_slip.vat_on_customer_discount
        delivery.vat = self.picking_slip.vat
        delivery.line_discount = self.picking_slip.line_discount
        delivery.discount_percentage = self.picking_slip.discount_percentage
        if self.picking_slip.delivery_method:
            delivery.delivery_method = self.picking_slip.delivery_method
        if self.picking_slip.delivery_term:
            delivery.delivery_term = self.picking_slip.delivery_term
        if self.picking_slip.delivery_to:
            delivery.delivery_to = self.picking_slip.delivery_to
        if self.picking_slip.postal:
            delivery.postal = self.picking_slip.postal
        delivery.purchase_order = self.picking_slip.purchase_order
        delivery.save()
        return delivery


class DeliveryPickingSlipForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(DeliveryPickingSlipForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Delivery
        fields = ('branch',  'period', 'date', 'customer', 'delivery_time', 'purchase_order', 'delivery_method',
                  'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount', 'customer_discount',
                  'line_discount')
        widgets = {
            'period': forms.HiddenInput(),
            'date': forms.HiddenInput(),
            'branch': forms.HiddenInput(),
            'customer': forms.HiddenInput(),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }


class InventoryPickingSlipItemForm(BaseSalesInventoryForm):

    class Meta:
        model = PickingSlipItem
        fields = ('price', 'quantity', 'received', 'unit', 'inventory', 'vat_code', 'vat', 'discount', 'net_price',
                  'vat_amount', 'picking_slip', 'total_amount')

    def save(self, commit=True):
        picking_item = super().save(commit=False)
        picking_item.item_type = InvoiceItemType.INVENTORY
        picking_item.save()

        picking_item.inventory.recalculate_balances(year=picking_item.picking_slip.period.period_year)
        return picking_item


class TextPickingSlipItemForm(BaseSalesDescriptionForm):

    class Meta:
        model = PickingSlipItem
        fields = ('description', 'picking_slip')

    def save(self, commit=True):
        delivery_item = super().save(commit=False)
        delivery_item.item_type = InvoiceItemType.TEXT
        delivery_item.save()
        return delivery_item
