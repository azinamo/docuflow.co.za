import logging
import pprint
from collections import defaultdict

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.utils.timezone import now
from django.views.generic import FormView, View, TemplateView, CreateView

from docuflow.apps.accounts.models import Role
from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.models import VatCode
from docuflow.apps.inventory.models import BranchInventory
from docuflow.apps.sales.enums import InvoiceItemType
from docuflow.apps.sales.models import (Delivery, EstimateItem, Estimate, PickingSlip, PickingSlipItem, BackOrder,
                                        Invoice, Tracking)
from .forms import PickingSlipFromEstimateForm, PickingSlipDeliveryForm, BackOrderPickingSlipForm, \
    InvoicePickingSlipForm
from .services import CreateFromPickingSlip, CreateFromBackOrder, CreateFromInvoice, remove_picking_slip

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class IndexView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'pickingslip/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        context['branch'] = self.get_branch()
        return context


class PickingSlipBackOrderView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'pickingslip/index.html'

    def get_deliveries(self, branch):
        return PickingSlip.objects.filter(branch=branch, status=PickingSlip.IN_PROGRESS, back_order__isnull=False)

    def get_context_data(self, **kwargs):
        context = super(PickingSlipBackOrderView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        context['picking_slips'] = self.get_deliveries(branch)
        context['branch'] = branch
        return context


class CreateFromEstimateView(ProfileMixin, LoginRequiredMixin, CreateView):
    template_name = 'pickingslip/estimate/create.html'
    form_class = PickingSlipFromEstimateForm
    model = PickingSlip

    def get_estimate(self):
        return Estimate.objects.get_detail(self.kwargs['estimate_id'], estimate_item_type='inventory',
                                           is_picked_estimate_item=False)

    def get_initial(self):
        initial = super(CreateFromEstimateView, self).get_initial()
        initial['date'] = now().date()
        estimate = self.get_estimate()
        if estimate.customer.delivery_term:
            initial['delivery_term'] = estimate.customer.delivery_term
        if estimate.customer.delivery_method:
            initial['delivery_method'] = estimate.customer.delivery_method
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateFromEstimateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['estimate'] = self.get_estimate()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateFromEstimateView, self).get_context_data(**kwargs)
        company = self.get_company()
        estimate = self.get_estimate()
        context['company'] = company
        context['estimate'] = estimate
        context['estimate_items'] = estimate.estimate_items.all()
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['created_at'] = now()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the picking slip, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateFromEstimateView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            with transaction.atomic():
                form.save()
                return JsonResponse({'text': 'Picking slip saved successfully',
                                     'error': False,
                                     'redirect': reverse('picking_slips')
                                     })


class CreateFromBackOrderView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'pickingslip/backorder/create.html'
    form_class = BackOrderPickingSlipForm

    def get_back_order(self):
        return BackOrder.objects.prefetch_related(
            'back_order_items'
        ).filter(
            pk=self.kwargs['back_order_id']
        ).first()

    def get_initial(self):
        initial = super(CreateFromBackOrderView, self).get_initial()
        initial['branch'] = self.get_branch()
        back_order = self.get_back_order()
        initial['period'] = back_order.period
        initial['status'] = PickingSlip.IN_PROGRESS
        initial['date'] = now().date()

        if back_order.customer:
            if back_order.customer.delivery_term:
                initial['delivery_term'] = back_order.customer.delivery_term
            if back_order.customer.delivery_method:
                initial['delivery_method'] = back_order.customer.delivery_method
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateFromBackOrderView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['back_order'] = self.get_back_order()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateFromBackOrderView, self).get_context_data(**kwargs)
        company = self.get_company()
        back_order = self.get_back_order()
        context['company'] = company
        context['back_order'] = back_order
        context['back_order_items'] = back_order.back_order_items.filter(
            inventory__isnull=False
        ).all()
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['created_at'] = now()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            logger.info(form.cleaned_data)
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the picking slip, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateFromBackOrderView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    picking_slip = form.save()
                    profile = self.get_profile()
                    back_order = self.get_back_order()
                    create_picking = CreateFromBackOrder(back_order, profile, picking_slip, self.request.POST)
                    create_picking.execute()

                    return JsonResponse({'text': 'Picking slip saved successfully',
                                         'error': False,
                                         'redirect': reverse('picking_slips')
                                         })

            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the picking slip',
                                     'details': str(exception),
                                     'error': True
                                     })


class CreateFromDeliveryView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'pickingslip/delivery/create.html'

    def get_delivery(self):
        return Delivery.objects.prefetch_related(
            'delivery_items'
        ).filter(pk=self.kwargs['delivery_id']).first()

    def get_initial(self):
        initial = super(CreateFromDeliveryView, self).get_initial()
        initial['branch'] = self.get_branch()
        delivery = self.get_delivery()
        initial['period'] = delivery.period
        initial['status'] = PickingSlip.IN_PROGRESS
        if delivery.customer:
            if delivery.customer.delivery_term:
                initial['delivery_term'] = delivery.customer.delivery_term
            if delivery.customer.delivery_method:
                initial['delivery_method'] = delivery.customer.delivery_method
        return initial

    def get_form_class(self):
        return EstimatePickingSlipForm

    def get_form_kwargs(self):
        kwargs = super(CreateFromDeliveryView, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateFromDeliveryView, self).get_context_data(**kwargs)
        company = self.get_company()
        delivery = self.get_delivery()
        context['company'] = company
        context['delivery'] = delivery
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['created_at'] = now()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            print("Validate -> Form data")
            print(form.cleaned_data)
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the picking slip, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateFromDeliveryView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    branch = self.get_branch()
                    profile = self.get_profile()
                    delivery = self.get_delivery()
                    content_type = ContentType.objects.filter(app_label='sales', model='delivery').first()

                    picking_slip = form.save(commit=False)
                    picking_slip.branch = branch
                    picking_slip.profile = profile
                    picking_slip.content_type = content_type
                    picking_slip.object_id = delivery.id
                    picking_slip.period = delivery.period
                    picking_slip.date = delivery.date
                    picking_slip.save()

                    if picking_slip:
                        delivery_items = delivery.delivery_items.all()
                        picking_slip.create_picking_items_from_estimate_items(delivery_items, self.request.POST)

                        delivery.status = Delivery.PICKED
                        delivery.save()

                    return JsonResponse({'text': 'Picking slip saved successfully',
                                         'error': False,
                                         'redirect': reverse('picking_slips')
                                         })

            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the delivery',
                                     'details': exception.__str__(),
                                     'error': True
                                     })


class CreateDeliveryNoteView(LoginRequiredMixin, ProfileMixin, View):

    def get_picking_slip(self):
        return PickingSlip.objects.filter(id=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            picking_slip = self.get_picking_slip()
            redirect_url = reverse('picking_slips')
            if picking_slip:
                redirect_url = reverse('create_picking_slip_delivery', kwargs={'picking_slip_id': picking_slip.id})
            return JsonResponse({
                'text': 'Redirecting ....',
                'error': False,
                'redirect': redirect_url
            })
        except Estimate.DoesNotExist:
            return JsonResponse({
                'text': 'Picking slip not found',
                'error': True
            })
        except Exception as exception:
            logger.exception(exception)
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class AddDeliveryItemView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'deliveries/delivery_items.html'

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch())
        return branch_inventories

    def get_vat_codes(self):
        return VatCode.objects.output().filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(AddDeliveryItemView, self).get_context_data(**kwargs)
        context['inventory_items'] = self.get_inventory_items()
        context['vat_codes'] = self.get_vat_codes()
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        return context


class EstimateDeliveryItemView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'deliveries/estimate_delivery_items.html'

    def get_inventory_items(self):
        branch_inventories = BranchInventory.objects.filter(branch=self.get_branch())
        return branch_inventories

    def get_vat_codes(self):
        return VatCode.objects.output().filter(company=self.get_company())

    def get_estimate_items(self):
        estimate_items = EstimateItem.objects.select_related(
            'inventory', 'estimate', 'vat_code'
        ).filter(estimate_id=self.kwargs['estimate_id'])
        return estimate_items

    def get_context_data(self, **kwargs):
        context = super(EstimateDeliveryItemView, self).get_context_data(**kwargs)
        context['inventory_items'] = self.get_inventory_items()
        context['vat_codes'] = self.get_vat_codes()
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['line_type'] = str(self.request.GET.get('line_type', 'i')).lower()
        context['estimate_items'] = self.get_estimate_items()
        return context


class DetailView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'pickingslip/detail.html'
    form_class = PickingSlipDeliveryForm

    def get_template_names(self):
        if self.request.GET.get('print', 0) == '1':
            return 'pickingslip/print.html'
        return 'pickingslip/detail.html'

    def get_picking_slip(self):
        return PickingSlip.objects.prefetch_related(
            'picking_slip_items', 'picking_slip_items__inventory', 'picking_slip_items__inventory__inventory',
            'picking_slip_items__vat_code', 'picking_slip_items__inventory__history'
        ).filter(pk=self.kwargs['pk']).first()

    def get_initial(self):
        initial = super(DetailView, self).get_initial()
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(DetailView, self).get_form_kwargs()
        kwargs['profile'] = self.get_profile()
        kwargs['picking_slip'] = self.get_picking_slip()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        picking_slip = self.get_picking_slip()
        context['picking_slip'] = picking_slip
        context['picking_slip_items'] = PickingSlipItem.objects.inventory().filter(picking_slip=picking_slip)
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        cols = 11
        context['cols'] = cols
        context['range'] = range(0, cols)
        context['is_print'] = self.request.GET.get('print', 0) == '1'
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the picking slip, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(DetailView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            with transaction.atomic():
                delivery = form.save()
                profile = self.get_profile()
                picking_slip = self.get_picking_slip()
                create_delivery = CreateFromPickingSlip(picking_slip, profile, delivery, self.request)
                create_delivery.execute()
                return JsonResponse({'text': 'Delivery note saved successfully',
                                     'error': False,
                                     'redirect': reverse('picking_slips')
                                     })


class CreateDeliveryInvoiceView(LoginRequiredMixin, ProfileMixin, View):

    def get_delivery(self):
        return Delivery.objects.filter(pk=self.kwargs['pk']).first()

    def get(self, request, *args, **kwargs):
        try:
            print("Create invoices from deliveries")
            delivery = self.get_delivery()
            if delivery:
                redirect_url = reverse('create_delivery_invoice', kwargs={'delivery_id': delivery.id})
                return JsonResponse({
                    'text': 'Redirecting to invoice ....',
                    'error': False,
                    'redirect': redirect_url
                })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': exception.__str__()
            })


class CreateFromInvoiceView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'pickingslip/invoice/create.html'
    form_class = InvoicePickingSlipForm

    def get_invoice(self):
        return Invoice.objects.prefetch_related(
            'invoice_items'
        ).filter(
            pk=self.kwargs['invoice_id']
        ).first()

    def get_invoice_items(self, invoice):
        return invoice.invoice_items.exclude(item_type=InvoiceItemType.GENERAL).all()

    def get_initial(self):
        initial = super(CreateFromInvoiceView, self).get_initial()
        initial['date'] = now().date()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateFromInvoiceView, self).get_form_kwargs()
        kwargs['invoice'] = self.get_invoice()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateFromInvoiceView, self).get_context_data(**kwargs)
        company = self.get_company()
        invoice = self.get_invoice()
        context['company'] = company
        context['invoice'] = invoice
        context['invoice_items'] = self.get_invoice_items(invoice)
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['created_at'] = now()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the picking slip, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateFromInvoiceView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    picking_slip = form.save()
                    create_picking = CreateFromInvoice(self.get_profile(), self.get_invoice(), picking_slip, self.request.POST)
                    create_picking.execute()
                    return JsonResponse({'text': 'Picking slip saved successfully',
                                         'error': False,
                                         'redirect': reverse('picking_slips')
                                         })
            except Exception as exception:
                logger.exception(exception)
                return JsonResponse({'text': 'Unexpected error occurred saving the delivery',
                                     'details': str(exception),
                                     'error': True
                                     })


class SinglePickingSlipActionView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            action = self.kwargs['action']
            if action == 'remove':
                picking_slip = PickingSlip.objects.get(pk=self.request.GET['picking_slip_id'])
                role = Role.objects.get(pk=self.request.session['role'])
                remove_picking_slip(picking_slip, self.get_profile(), role)

                return JsonResponse({
                    'text': 'Picking slip removed successfully',
                    'error': False,
                    'redirect': reverse('picking_slips')
                })
            else:
                return JsonResponse({
                    'text': 'No action not found',
                    'warning': True
                })
        except PickingSlip.DoesNotExist:
            return JsonResponse({
                'text': 'Picking slip not found',
                'error': False
            })


class TrackingView(TemplateView):
    template_name = 'pickingslip/tracking.html'

    def get_picking_slip(self):
        return PickingSlip.objects.filter(pk=self.kwargs['pk']).first()

    def get_tracking(self, picking_slip):
        content_type = ContentType.objects.filter(app_label='sales', model='pickingslip').first()
        trackings = Tracking.objects.select_related(
            'profile', 'role'
        ).filter(
            object_id=picking_slip.id, content_type=content_type
        )
        tracking_by_types = defaultdict(list)
        for tracking in trackings:
            tracking_type = tracking.data.get('tracking_type')
            if tracking_type:
                tracking_by_types[tracking_type].append(tracking)
        return tracking_by_types

    def get_context_data(self, **kwargs):
        context = super(TrackingView, self).get_context_data(**kwargs)
        picking_slip = self.get_picking_slip()
        tracking = self.get_tracking(picking_slip)
        context['picking_slip'] = picking_slip
        context['picking_tracking'] = tracking.get('self')
        context['delivery_tracking'] = tracking.get('delivery')
        context['back_order_tracking'] = tracking.get('back_order')
        context['estimate_tracking'] = tracking.get('estimate')
        context['invoice_tracking'] = tracking.get('invoice')
        return context
