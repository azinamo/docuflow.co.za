import logging

from django.contrib.contenttypes.fields import ContentType
from django.urls import reverse

from docuflow.apps.sales.models import PickingSlip, PickingSlipItem, DeliveryItem, Tracking
from docuflow.apps.sales.modules.backorder.services import CreateBackOrderFromSalesInvoice
from docuflow.apps.sales.services import SaleAccountLine
from docuflow.apps.sales import enums
from docuflow.apps.sales.modules.deliveries.forms import InventoryDeliveryItemForm
from .forms import InventoryPickingSlipItemForm

logger = logging.getLogger(__name__)


class CreateFromEstimate:

    def __init__(self, picking_slip, profile, role, estimate, request):
        logger.info("Create picking slip from estimate")
        self.profile = profile
        self.role = role
        self.estimate = estimate
        self.picking_slip = picking_slip
        self.request = request

    def execute(self):
        logger.info("Execute")
        # self.create_sales_items_from_source_items()
        self.complete_estimate(self.picking_slip)

    def complete_estimate(self, picking_slip):
        self.estimate.mark_as_completed()

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.estimate),
            object_id=self.estimate.pk,
            comment=f'Estimate {str(self.estimate.status)}',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'picking_slip',
                'reference': str(self.picking_slip),
                'comment_type': str(enums.TrackingType.ACTION),
                'link_url': reverse('picking_detail', args=(picking_slip.pk, )),
                'action': str(enums.CommentAction.UPDATED)
            }
        )
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(picking_slip),
            object_id=picking_slip.pk,
            comment=f'Picking slip created from estimate',
            profile=self.profile,
            role=self.role,
            data={
                'tracking_type': 'estimate',
                'reference': str(self.estimate),
                'comment_type': str(enums.TrackingType.ACTION),
                'link_url': reverse('estimate_detail', args=(self.estimate.pk, )),
                'action': str(enums.CommentAction.UPDATED)
            }
        )

    def calculate_net_price(self, quantity, price):
        logger.info('calculate_net_price')
        return float(quantity) + float(price)

    def calculate_vat_amount(self, vat_code, price, quantity):
        logger.info('calculate_vat_amount')
        if vat_code:
            return (float(vat_code.percentage / 100) * float(price)) * quantity
        return 0

    def calculate_discount_amount(self, discount, net_price):
        logger.info('calculate_discount_amount')
        if discount > 0:
            return (float(discount / 100) * net_price)
        return 0


class CreateFromPickingSlip:

    def __init__(self, picking_slip, profile, delivery, request):
        self.picking_slip = picking_slip
        self.profile = profile
        self.delivery = delivery
        self.request = request

    def execute(self):
        self.create_picking_items()

        Tracking.objects.create(**{'object_id': self.picking_slip.id,
                                   'content_type': ContentType.objects.get_for_model(self.picking_slip),
                                   'comment': 'Delivery created',
                                   'profile': self.profile,
                                   'data': {
                                           'tracking_type': 'delivery',
                                           'reference': str(self.delivery),
                                           'link_url': reverse('delivery_detail', args=(self.delivery.pk,))
                                       }
                                   })

        Tracking.objects.create(**{'object_id': self.delivery.id,
                                   'content_type': ContentType.objects.get_for_model(self.delivery),
                                   'comment': 'Delivery created',
                                   'profile': self.profile,
                                   'data': {
                                       'tracking_type': 'pickingslip',
                                       'reference': str(self.picking_slip),
                                       'link_url': reverse('picking_detail', args=(self.picking_slip.pk, ))
                                       }
                                   })

    def create_picking_items(self):
        self.mark_picking_as_completed()

        picking_slip_items = self.picking_slip.picking_slip_items.all()

        self.create_delivery_items_from_picking_slip_items(picking_slip_items)

    def mark_picking_as_completed(self):
        self.picking_slip.picked_by = self.profile
        self.picking_slip.status = PickingSlip.COMPLETED
        self.picking_slip.save()

    def create_delivery_items_from_picking_slip_items(self, picking_slip_items):
        back_order_items = []
        non_inventory_items = []
        for picking_slip_item in picking_slip_items:
            if picking_slip_item.is_account:
                self.add_account_item(picking_slip_item)
                non_inventory_items.append({'item': picking_slip_item, 'quantity': picking_slip_item.quantity})
            elif picking_slip_item.is_description:
                self.add_description_item(picking_slip_item)
                non_inventory_items.append({'item': picking_slip_item, 'quantity': picking_slip_item.quantity})
            else:
                is_back_order = self.request.POST.get(f'back_order_{picking_slip_item.id}', False)
                data = {
                    'quantity': self.request.POST.get(f'quantity_{picking_slip_item.id}', 0),
                    'ordered':  picking_slip_item.quantity,
                    'received': picking_slip_item.quantity,
                    'price': picking_slip_item.price,
                    'discount': picking_slip_item.discount,
                    'vat': picking_slip_item.vat,
                    'inventory': picking_slip_item.inventory,
                    'unit': picking_slip_item.unit,
                    'vat_code': picking_slip_item.vat_code,
                    'delivery': self.delivery,
                    'vat_amount': self.request.POST.get(f'vat_amount_{picking_slip_item.id}', 0),
                    'net_price': self.request.POST.get(f'total_price_excluding_{picking_slip_item.id}', 0),
                    'total_amount': self.request.POST.get(f'total_price_including_{picking_slip_item.id}', 0)
                }
                delivery_inventory_item_form = InventoryDeliveryItemForm(data=data, branch=self.picking_slip.branch)
                if delivery_inventory_item_form.is_valid():
                    delivery_inventory_item = delivery_inventory_item_form.save()
                    if is_back_order:
                        back_order_qty = picking_slip_item.quantity - delivery_inventory_item.quantity
                        back_order_items.append({'item': delivery_inventory_item, 'quantity': back_order_qty})

                    picking_slip_item.picked = delivery_inventory_item.quantity
                    picking_slip_item.save()

                else:
                    logger.info(delivery_inventory_item_form.errors)

        if len(back_order_items) > 0:
            back_order_items = back_order_items + non_inventory_items
            create_back_order = CreateBackOrderFromSalesInvoice(self.picking_slip, back_order_items)
            create_back_order.execute()
        return back_order_items

    def add_account_item(self, picking_slip_item):
        logger.info(f"Adding delivery account item from picking slip item {picking_slip_item.quantity}")
        DeliveryItem.objects.create(
            delivery=self.delivery,
            description=picking_slip_item.description,
            quantity=picking_slip_item.quantity,
            ordered=picking_slip_item.quantity,
            price=picking_slip_item.price,
            discount=picking_slip_item.discount,
            net_price=picking_slip_item.net_price,
            total_amount=picking_slip_item.total_amount,
            vat_amount=picking_slip_item.vat_amount,
            vat=picking_slip_item.vat,
            account=picking_slip_item.account,
            unit=picking_slip_item.unit,
            vat_code=picking_slip_item.vat_code,
            item_type=enums.InvoiceItemType.GENERAL
        )

    def add_description_item(self, picking_slip_item):
        logger.info(f"Adding delivery description item from picking slip item {picking_slip_item.quantity}")
        DeliveryItem.objects.create(
            delivery=self.delivery,
            description=picking_slip_item.description,
            item_type=enums.InvoiceItemType.TEXT
        )


class CreateFromBackOrder:

    def __init__(self, back_order, profile, picking_slip, post_request):
        logger.info("Create picking slip from back_order")
        self.profile = profile
        self.back_order = back_order
        self.picking_slip = picking_slip
        self.post_request = post_request

    def execute(self):
        logger.info("--- Execute Start ----")
        self.create_sales_items_from_source_items(self.back_order.back_order_items.all())

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.picking_slip),
            object_id=self.picking_slip.id,
            comment='Picking slip created from back order',
            profile=self.profile,
            data={
                'tracing_type': 'back_order',
                'reference': str(self.back_order),
                'link_url': reverse('backorder_detail', args=(self.back_order.id, ))
            }
        )
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.back_order),
            object_id=self.back_order.id,
            comment='Back order completed',
            profile=self.profile,
            data={
                'tracing_type': 'picking_slip',
                'reference': str(self.picking_slip),
                'link_url': reverse('picking_detail', args=(self.picking_slip.id, ))
            }
        )

    def calculate_net_price(self, quantity, price):
        logger.info('calculate_net_price')
        return float(quantity) + float(price)

    def calculate_vat_amount(self, vat_code, price, quantity):
        logger.info('calculate_vat_amount')
        if vat_code:
            return (float(vat_code.percentage / 100) * float(price)) * quantity
        return 0

    def calculate_discount_amount(self, discount, net_price):
        logger.info('calculate_discount_amount')
        if discount > 0:
            return (float(discount / 100) * net_price)
        return 0

    def create_sales_items_from_source_items(self, sales_items, **kwargs):
        logger.info("------------create_picking_items_from_source_items---------------")
        picked_count = 0
        counter = 0
        back_order_items = []
        non_inventory_items = []
        total_net_amount = 0
        total_amount = 0
        total_vat = 0
        total_discount = 0
        sub_total = 0
        for sales_item in sales_items:
            if sales_item.is_account:
                non_inventory_items.append({'item': sales_item, 'quantity': sales_item.quantity})
                create_invoice_item = SaleAccountLine(self.picking_slip, PickingSlipItem, sales_item.account,
                                                      sales_item.price, sales_item.quantity, sales_item.vat_code)
                create_invoice_item.execute()
                counter += 1
            if sales_item.inventory:
                counter += 1
                data = {
                    'quantity': self.post_request.get(f'quantity_{sales_item.id}', 0),
                    'vat_amount': self.post_request.get(f'vat_amount_{sales_item.id}', 0),
                    'total_amount': self.post_request.get(f'price_including_{sales_item.id}', 0),
                    'inventory': sales_item.inventory,
                    'price': sales_item.price,
                    'vat_code': sales_item.vat_code,
                    'vat': sales_item.vat,
                    'discount': sales_item.discount,
                    'unit': sales_item.unit,
                    'picking_slip': self.picking_slip
                }
                is_back_order = self.post_request.get(f'back_order_{sales_item.id}', False)
                is_picked = bool(self.post_request.get(f'is_picked_{sales_item.id}', False))
                inventory_picking_form = InventoryPickingSlipItemForm(data=data, branch=self.picking_slip.branch)
                if inventory_picking_form.is_valid():
                    if inventory_picking_form.cleaned_data['quantity'] > 0 and is_picked:
                        picking_item = inventory_picking_form.save()
                        back_order_qty = sales_item.quantity - picking_item.quantity
                        if is_back_order:
                            logger.info("Allow back order")
                            back_order_items.append({'item': sales_item, 'quantity': back_order_qty})
                        elif back_order_qty == 0:
                            back_order_items.append({'item': sales_item, 'quantity': picking_item.quantity * -1})
                        logger.info("---- Allow picking ---")
                        logger.info(back_order_items)
                        net_price = picking_item.net_price if picking_item.net_price else 0
                        total_vat += picking_item.vat_amount
                        total_net_amount += net_price
                        total_amount += picking_item.total_amount
                        total_discount += picking_item.discount_amount
                        sub_total += net_price + picking_item.discount_amount

                        logger.info("Done picking slip item")
                        picked_count += 1
                        sales_item.status = PickingSlip.SEND_FOR_PICKING
                        sales_item.save()
                        logger.info("Updated sales item status")
                else:
                    logger.info(inventory_picking_form.errors)

        self.picking_slip.vat_amount = total_vat
        self.picking_slip.net_amount = total_net_amount
        self.picking_slip.total_amount = total_amount
        self.picking_slip.discount_amount = total_discount
        self.picking_slip.sub_total = sub_total
        self.picking_slip.save()

        if back_order_items:
            back_order_items = back_order_items + non_inventory_items
            logger.info("---- back_order_items ---")
            logger.info(back_order_items)
            create_back_order = CreateBackOrderFromSalesInvoice(self.back_order, back_order_items)
            create_back_order.execute()
        else:
            self.back_order.mark_as_completed()
        return picked_count == counter

    def save_account_item(self, sales_item, picking_slip):
        logger.info("Added account item to the picking slip")
        PickingSlipItem.objects.create(
            picking_slip=picking_slip,
            received=sales_item.quantity,
            picked=sales_item.quantity,
            quantity=sales_item.quantity,
            price=sales_item.price,
            discount=sales_item.discount,
            net_price=sales_item.net_price,
            total_amount=sales_item.total_amount,
            vat=sales_item.vat,
            account=sales_item.account,
            unit=sales_item.unit,
            vat_code=sales_item.vat_code,
            item_type=enums.InvoiceItemType.GENERAL
        )


class CreateFromInvoice:

    def __init__(self, profile, invoice, picking_slip, post_request):
        logger.info("Create picking slip from invoice")
        self.profile = profile
        self.invoice = invoice
        self.picking_slip = picking_slip
        self.post_request = post_request

    def execute(self):
        logger.info("Execute")
        self.create_sales_items_from_source_items(self.invoice.invoice_items.all())
        self.invoice.mark_as_completed()

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.invoice),
            object_id=self.invoice.id,
            comment='Invoice send for picking',
            profile=self.profile,
            data={
                'tracking_type': 'pickingslip',
                'reference': str(self.picking_slip)
            }
        )

        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(self.picking_slip),
            object_id=self.picking_slip.id,
            comment='Created from invoice',
            profile=self.profile,
            data={
                'tracking_type': 'invoice',
                'reference': str(self.invoice)
            }
        )

        self.invoice.picking_slip = self.picking_slip
        self.invoice.save()


    def calculate_net_price(self, quantity, price):
        logger.info('calculate_net_price')
        return float(quantity) + float(price)

    def calculate_vat_amount(self, vat_code, price, quantity):
        logger.info('calculate_vat_amount')
        if vat_code:
            return (float(vat_code.percentage / 100) * float(price)) * quantity
        return 0

    def calculate_discount_amount(self, discount, net_price):
        logger.info('calculate_discount_amount')
        if discount > 0:
            return (float(discount / 100) * net_price)
        return 0

    def create_sales_items_from_source_items(self, sales_items, **kwargs):
        logger.info("------------create_picking_items_from_source_items---------------")
        picked_count = 0
        counter = 0
        back_order_items = []
        non_inventory_items = []
        total_net_amount = 0
        total_amount = 0
        total_vat = 0
        total_discount = 0
        sub_total = 0
        for sales_item in sales_items:
            if sales_item.is_account:
                non_inventory_items.append({'item': sales_item, 'quantity': sales_item.quantity})
                self.save_account_item(sales_item, self.picking_slip)
                counter += 1
            else:
                counter += 1
                qty = float(self.post_request.get('quantity_{}'.format(sales_item.id), 0))
                is_back_order = self.post_request.get('back_order_{}'.format(sales_item.id), False)
                is_picked = bool(self.post_request.get('is_picked_{}'.format(sales_item.id), False))
                logger.info(f"Original quantity -> {qty}, is_back order {is_back_order}  and is picking {is_picked}")
                if is_back_order:
                    back_order_qty = float(sales_item.quantity) - qty
                    logger.info(f"Back order quantity = {back_order_qty}")
                    if back_order_qty > 0:
                        back_order_items.append({'item': sales_item, 'quantity': back_order_qty})
                if is_picked:
                    logger.info("---- Allow picking ---")
                    net_price = self.calculate_net_price(qty, sales_item.price)
                    vat_amount = self.calculate_vat_amount(sales_item.vat_code, sales_item.price, qty)
                    discount_amount = self.calculate_discount_amount(sales_item.discount, net_price)
                    total_amount = vat_amount + net_price
                    logger.info('total amount {}'.format(total_amount))
                    PickingSlipItem.objects.create(
                        picking_slip=self.picking_slip,
                        received=sales_item.quantity,
                        picked=qty,
                        quantity=qty,
                        price=sales_item.price,
                        discount=sales_item.discount,
                        net_price=net_price,
                        total_amount=total_amount,
                        vat=sales_item.vat,
                        inventory=sales_item.inventory,
                        unit=sales_item.unit,
                        vat_code=sales_item.vat_code
                    )
                    total_vat += vat_amount
                    total_net_amount += net_price
                    total_amount += total_amount
                    total_discount += discount_amount
                    sub_total += net_price + discount_amount

                    picked_count += 1
                    sales_item.status = PickingSlip.SEND_FOR_PICKING
                    sales_item.save()
        self.picking_slip.vat_amount = total_vat
        self.picking_slip.net_amount = total_net_amount
        self.picking_slip.total_amount = total_amount
        self.picking_slip.discount_amount = total_discount
        self.picking_slip.sub_total = sub_total
        self.picking_slip.save()

        if back_order_items:
            back_order_items = back_order_items + non_inventory_items
            create_back_order = CreateBackOrderFromSalesInvoice(self.picking_slip, back_order_items)
            create_back_order.execute()
        return picked_count == counter

    def save_account_item(self, sales_item, picking_slip):
        logger.info("Added account item to the picking slip")
        PickingSlipItem.objects.create(
            picking_slip=picking_slip,
            received=sales_item.quantity,
            picked=sales_item.quantity,
            quantity=sales_item.quantity,
            price=sales_item.price,
            discount=sales_item.discount,
            net_price=sales_item.net_price,
            total_amount=sales_item.total_amount,
            vat=sales_item.vat,
            account=sales_item.account,
            unit=sales_item.unit,
            vat_code=sales_item.vat_code,
            item_type=enums.InvoiceItemType.GENERAL
        )


def remove_picking_slip(picking_slip: PickingSlip, profile, role):

    if picking_slip.picking_slip_back_order.exists():
        for back_order in picking_slip.picking_slip_back_order.all():
            Tracking.objects.create(
                content_type=ContentType.objects.get_for_model(back_order),
                object_id=back_order.id,
                comment='Picking slip removed',
                profile=profile,
                role=role,
                data={
                    'tracking_type': 'picking_slip',
                    'reference': str(picking_slip),
                    'link_url': reverse('picking_detail', args=(picking_slip.id, ))
                }
            )
            back_order.mark_as_removed()

    if picking_slip.estimate:
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(picking_slip.estimate),
            object_id=picking_slip.estimate.pk,
            comment=f'Picking slip removed',
            profile=profile,
            role=role,
            data={
                'tracking_type': 'picking_slip',
                'reference': str(picking_slip),
                'comment_type': str(enums.TrackingType.ACTION),
                'action': str(enums.CommentAction.DELETED)
            }
        )
        picking_slip.estimate.mark_as_open()
        picking_slip.estimate = None
        picking_slip.save()

    Tracking.objects.create(
        content_type=ContentType.objects.get_for_model(picking_slip),
        object_id=picking_slip.pk,
        comment=f'Picking slip removed',
        profile=profile,
        role=role,
        data={
            'tracking_type': 'self',
            'reference': str(picking_slip),
            'comment_type': str(enums.TrackingType.ACTION),
            'action': str(enums.CommentAction.DELETED)
        }
    )
    picking_slip.mark_as_removed()
    for picking_slip_item in picking_slip.picking_slip_items.filter(item_type=enums.InvoiceItemType.INVENTORY):
        picking_slip_item.inventory.recalculate_balances(year=picking_slip.period.period_year)


