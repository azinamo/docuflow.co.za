from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='sales_updates_index'),
    path('create/<str:invoice_type>/', views.CreateUpdateView.as_view(), name='create_sales_invoice_update'),
    path('<int:pk>/detail/', views.UpdateDetailView.as_view(), name='sales_update_detail'),
]
