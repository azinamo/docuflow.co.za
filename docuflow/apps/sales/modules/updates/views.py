import pprint
import logging

from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, FormView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.db import transaction
from django.utils.timezone import now

from docuflow.apps.common import utils
from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.journals.exceptions import JournalNotBalancingException
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.sales.models import Update, InvoiceUpdate
from docuflow.apps.sales.exceptions import SalesException
from docuflow.apps.sales.enums import InvoiceType
from .forms import UpdateInvoiceForm

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'updates/index.html'
    context_object_name = 'updates'
    model = Update
    paginate_by = 30

    def get_queryset(self):
        return Update.objects.invoice_updates(self.get_company(), self.get_year()).select_related(
            'role', 'profile', 'period'
        )

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['company'] = company
        context['year'] = year
        context['tax_invoice'] = InvoiceType.TAX_INVOICE.value
        context['tax_credit_note'] = InvoiceType.CREDIT_INVOICE.value
        return context


class CreateUpdateView(LoginRequiredMixin, ProfileMixin,  FormView):
    template_name = 'updates/create.html'
    form_class = UpdateInvoiceForm
    success_url = reverse_lazy('sales_updates_index')

    def get_initial(self):
        initial = super(CreateUpdateView, self).get_initial()
        initial['date'] = now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateUpdateView, self).get_form_kwargs()
        kwargs['year'] = self.get_year()
        kwargs['branch'] = self.get_branch()
        kwargs['profile'] = self.get_profile()
        kwargs['invoice_type'] = InvoiceType.get_by_value(val=self.kwargs['invoice_type'])
        kwargs['role'] = self.get_role()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateUpdateView, self).get_context_data(**kwargs)
        company = self.get_company()
        year = self.get_year()
        context['company'] = company
        context['year'] = year
        context['invoice_type'] = InvoiceType.get_by_value(val=self.kwargs['invoice_type'])
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({
                'error': True,
                'text': 'Error occurred saving the invoice updates, please fix the errors.',
                'errors': form.errors
            }, status=400)
        return super(CreateUpdateView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax(request=self.request):
            try:
                with transaction.atomic():
                    form.save()

                    return JsonResponse({
                        'text': 'Sales update successfully created',
                        'error': False,
                        'redirect': reverse('sales_updates_index')
                    })
            except (SalesException, InventoryException, JournalNotBalancingException) as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True
                                     })
        return super(CreateUpdateView, self).form_valid(form)


class UpdateDetailView(ProfileMixin, DetailView):
    template_name = 'updates/detail.html'
    model = Update

    def get_queryset(self):
        return super().get_queryset().select_related('profile', 'role', 'journal')

    # noinspection PyMethodMayBeStatic
    def is_via_credit(self, returned_item):
        if returned_item.inventory_received and returned_item.inventory_received.invoice_item:
            logger.info("We received this via invoice, check if its a credit note and ignore")
            logger.info(returned_item.inventory_received.invoice_item)
            # TODO Investigate and check why we did this, hide items received by credit note
            # return returned_item.inventory_received.invoice_item.invoice.is_credit
        return False

    def get_invoices(self):
        updated_invoices = InvoiceUpdate.objects.select_related(
            'invoice', 'invoice__invoice_type'
        ).prefetch_related(
            'invoice__invoice_items', 'invoice__invoice_items__inventory', 'invoice__invoice_items__inventory_sold',
            'invoice__invoice_items__inventory__inventory', 'invoice__invoice_items__inventory__sales_account',
            'invoice__invoice_items__inventory__gl_account', 'invoice__invoice_items__inventory__cost_of_sales_account',
            'invoice__invoice_items__inventory_bought', 'invoice__invoice_items__returns',
            'invoice__invoice_items__received'
        ).filter(update_id=self.kwargs['pk'])
        invoices = []
        for updated_invoice in updated_invoices:
            invoice = updated_invoice.invoice
            multiplier = -1 if invoice.is_credit else 1
            invoice_detail = {'invoice': invoice, 'breakdown': [], 'breakdown_total': 0, 'total_quantity': 0}
            for invoice_item in invoice.invoice_items.all():
                line_total_sold = 0
                for returned_item in invoice_item.returns.all():
                    received_via_credit = self.is_via_credit(returned_item)
                    if not received_via_credit:
                        quantity = returned_item.quantity * multiplier
                        price = returned_item.price
                        invoice_item_breakdown = {}
                        opening, closing = returned_item.stock_movement(invoice_item)
                        invoice_item_breakdown['invoice_item'] = invoice_item
                        invoice_item_breakdown['returned'] = returned_item
                        invoice_item_breakdown['quantity'] = quantity
                        invoice_item_breakdown['opening'] = opening
                        invoice_item_breakdown['closing'] = closing
                        total = price * quantity
                        line_total_sold += quantity
                        invoice_item_breakdown['price'] = price
                        invoice_item_breakdown['total'] = total
                        invoice_item_breakdown['quantity'] = quantity
                        invoice_detail['breakdown_total'] += total
                        invoice_detail['breakdown'].append(invoice_item_breakdown)
                    invoice_detail['total_quantity'] += line_total_sold
                logger.info("----------------------------------------------")

                for received_item in invoice_item.received.all():
                    quantity = received_item.quantity * multiplier
                    price = received_item.price
                    invoice_item_breakdown = {}
                    opening, closing = received_item.stock_movement(invoice_item)
                    invoice_item_breakdown['invoice_item'] = invoice_item
                    invoice_item_breakdown['returned'] = received_item
                    invoice_item_breakdown['quantity'] = quantity
                    invoice_item_breakdown['opening'] = opening
                    invoice_item_breakdown['closing'] = closing
                    total = price * quantity
                    line_total_sold += quantity
                    invoice_item_breakdown['price'] = price
                    invoice_item_breakdown['total'] = total
                    invoice_item_breakdown['quantity'] = quantity
                    invoice_detail['breakdown_total'] += total
                    invoice_detail['breakdown'].append(invoice_item_breakdown)
                    invoice_detail['total_quantity'] += quantity
            invoices.append(invoice_detail)
        return invoices

    def get_context_data(self, **kwargs):
        context = super(UpdateDetailView, self).get_context_data(**kwargs)
        context['update_invoices'] = self.get_invoices()
        return context
