from django.apps import AppConfig


class UpdatesConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.updates'
