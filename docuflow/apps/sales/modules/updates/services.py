import logging
import pprint
from typing import List
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.utils.timezone import now
from django.utils.translation import gettext as __

from docuflow.apps.common.enums import Module
from docuflow.apps.common.services import get_debit_amount, get_credit_amount
from docuflow.apps.customer.services import is_customer_balancing
from docuflow.apps.inventory.exceptions import InventoryException
from docuflow.apps.inventory.models import BranchInventory, Returned, InventoryReturned, Ledger as InventoryLedger, \
    InventoryReceivedReturned
from docuflow.apps.inventory.services import CreateReturnedInventory, ReceiveSoldInventoryItem, FiFoReturned, \
    ReceiveReturnedInventoryItem, is_valuation_balancing
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.sales import enums
from docuflow.apps.sales.exceptions import SalesException
from docuflow.apps.sales.models import Invoice, InvoiceUpdate, Update, InvoiceItem

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)


class InvoiceUpdater(object):

    def __init__(self, update: Update, invoice_type_code: str):
        self.update = update
        self.journal_line_type = ''
        self.invoice_type = self.get_invoice_type(invoice_type_code)
        logger.info(f"Updating {str(self.invoice_type)} invoice")

    def get_invoice_type(self, invoice_type_code):
        return InvoiceType.objects.sales(self.update.branch.company).filter(code=invoice_type_code).first()

    def get_invoices(self):
        return Invoice.objects.pending_updates(
            self.update.branch.company,
            period=self.update.period,
            invoice_type=self.invoice_type,
            end_date=self.update.date
        ).order_by('created_at')

    def has_invoices_pending_update(self):
        invoice_type = self.get_invoice_type(enums.InvoiceType.TAX_INVOICE.value)
        return Invoice.objects.pending_updates(
            company=self.update.branch.company, period=self.update.period, invoice_type=invoice_type,
            end_date=self.update.date
        ).exists()

    def execute(self):
        if self.invoice_type.code == enums.InvoiceType.CREDIT_INVOICE.value and self.has_invoices_pending_update():
            raise SalesException(__('There are invoices with pending updates, please update them first.'))

        invoices = self.get_invoices()

        if invoices.count() == 0:
            raise SalesException(__("No invoices found to process"))

        self.create_invoices_update(invoices=invoices)

    def create_invoices_update(self, invoices: List):
        for invoice in invoices:
            InvoiceUpdate.objects.get_or_create(
                invoice=invoice,
                update=self.update
            )

        movement = InvoiceUpdateInventoryMovement(update=self.update, invoices=invoices)
        movement.execute()

        self.create_ledger(invoices=invoices, inventory_movements=movement.invoice_items_adjustments)

        if not is_customer_balancing(company=self.update.branch.company, year=self.update.period.period_year):
            raise SalesException(__('A variation in age analysis and balance sheet detected.'))

        if not is_valuation_balancing(branch=self.update.branch, year=self.update.period.period_year):
            raise InventoryException(__('A variation in inventory accounts and inventory valuation detected.'))

    def create_ledger(self, invoices,  inventory_movements):
        update_report = InvoiceUpdateReport(
            company=self.update.branch.company,
            invoices=invoices,
            inventory_movements=inventory_movements,
            journal_type=self.journal_line_type
        )
        update_report.generate()

        ledger_lines = self.prepare_ledger_lines(update_report=update_report)
        ledger = CreateLedger(
            company=self.update.branch.company,
            year=self.update.period.period_year,
            period=self.update.period,
            profile=self.update.profile,
            role=self.update.role,
            text=str(self.update),
            journal_lines=ledger_lines,
            date=self.update.date,
            module=Module.SALES
        )
        journal = ledger.execute()
        if journal:
            self.update.journal = journal
            self.update.save()
        return journal

    # noinspection PyMethodMayBeStatic
    def prepare_ledger_lines(self, update_report):
        lines = []
        for line_type, line in update_report.ledger_lines.items():
            for line_key, account_line in line.items():
                account = account_line.get('account', None)
                if not account:
                    account = line_key
                _line = account_line
                _line['account'] = account
                _line['object_items'] = account_line.get('object_items', None)
                lines.append(_line)
        return lines


class InvoiceUpdateReport(object):

    def __init__(self, company, invoices, inventory_movements, journal_type):
        self.invoices = invoices
        self.inventory_movements = inventory_movements
        self.company = company
        self.ledger_lines = {'vats': {}, 'costs': {}, 'discounts': {}, 'controls': {}, 'sales': {}, 'invoices': {},
                             'disposals': {}}
        self.discount_allowed_account = self.get_discount_allowed_account()
        self.customer_account = self.get_customer_account()
        self.vat_code = self.get_sales_vat_account()
        self.journal_type = journal_type

    def get_sales_vat_account(self):
        if not self.company.sales_vat_code:
            raise SalesException(__('Default sales vat code does not exist, please set it up'))
        return self.company.sales_vat_code

    def get_discount_allowed_account(self):
        if not self.company.default_discount_allowed_account:
            raise SalesException(__('Default allowed discount account does not exist, please set it up'))
        return self.company.default_discount_allowed_account

    def get_customer_account(self):
        if not self.company.default_customer_account:
            raise SalesException(__('Default customer account does not exist, please set it up'))
        return self.company.default_customer_account

    # noinspection PyMethodMayBeStatic
    def get_inventory_sales_account(self, inventory):
        if not inventory.sales_account:
            raise SalesException(__(f'Sales account for inventory {inventory} does not exist, please set it up'))
        return inventory.sales_account

    # noinspection PyMethodMayBeStatic
    def get_inventory_cost_of_sale_account(self, inventory):
        if not inventory.cost_of_sales_account:
            raise SalesException(__(f'Cost of sales account for inventory {inventory} does not exist, please set it up'))
        return inventory.cost_of_sales_account

    # noinspection PyMethodMayBeStatic
    def get_inventory_control_account(self, inventory):
        if not inventory.gl_account:
            raise SalesException(__(f'Inventory control account for inventory {inventory} does not exist, please set it up'))
        return inventory.gl_account

    def generate(self):
        invoice_content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        customer_content_type = ContentType.objects.filter(app_label='customer', model='customer').first()
        for invoice in self.invoices:
            object_items = invoice.object_items
            invoice_items = invoice.invoice_items.all()
            if len(invoice_items) > 0:
                self.add_discount_allowed_line(invoice=invoice, object_items=object_items)

                if invoice.vat_amount:
                    self.add_vat_line(
                        invoice=invoice,
                        object_items=object_items,
                        content_type=invoice_content_type
                    )
                self.add_invoice_line(
                    invoice=invoice,
                    object_items=object_items,
                    customer_content_type=customer_content_type,
                )

                for invoice_item in invoice_items:
                    if invoice_item.inventory:
                        adjustment = self.inventory_movements.get(invoice_item, None)

                        total_cost = 0
                        if adjustment:
                            total_cost = adjustment.inventory_total

                        self.add_inventory_sales_line(
                            invoice_item=invoice_item,
                            object_items=object_items,
                            is_expense=self.company.is_expense_discount
                        )
                        self.add_inventory_control_line(
                            invoice_item=invoice_item,
                            amount=total_cost,
                            object_items=object_items
                        )

                        self.add_inventory_costs_line(
                            invoice_item=invoice_item,
                            amount=total_cost,
                            object_items=object_items
                        )
                    elif invoice_item.account:
                        self.add_account_line(
                            invoice_item=invoice_item,
                            object_items=object_items,
                            content_type=invoice_content_type
                        )

                if invoice.disposal:
                    self.add_disposal_lines(invoice=invoice)

    def add_disposal_lines(self, invoice: Invoice):
        for disposed_asset in invoice.disposal.disposed_fixed_assets.all():
            fixed_asset = disposed_asset.fixed_asset
            object_items = fixed_asset.object_items

            account = fixed_asset.asset_account
            line_key = f"{fixed_asset.id}_{account.id}"
            self.ledger_lines['disposals'][line_key] = {
                'debit': get_debit_amount(debit=0, credit=disposed_asset.asset_value),
                'credit': get_credit_amount(debit=0, credit=disposed_asset.asset_value),
                'text': str(fixed_asset),
                'suffix': invoice.suffix if account.is_income_statement else "",
                'account': account,
                'accounting_date': invoice.invoice_date,
                'ledger_type_reference': invoice.customer.name,
                'object_items': self.get_line_object_items(account, object_items),
                'sales_invoice': invoice
            }

            account = fixed_asset.accumulated_depreciation_account
            line_key = f"{fixed_asset.id}_{account.id}"
            self.ledger_lines['disposals'][line_key] = {
                'credit': get_credit_amount(credit=0, debit=disposed_asset.depreciation_value),
                'debit': get_debit_amount(credit=0, debit=disposed_asset.depreciation_value),
                'account': account,
                'text': str(disposed_asset.fixed_asset),
                'suffix': invoice.suffix if account.is_income_statement else "",
                'accounting_date': invoice.invoice_date,
                'ledger_type_reference': invoice.customer.name,
                'object_items': self.get_line_object_items(account, object_items),
                'sales_invoice': invoice
            }

            # This account is determined by whether there is a profit or a loss
            account = disposed_asset.invoice_item.account
            line_key = f"{fixed_asset.id}_{account.id}"
            self.ledger_lines['disposals'][line_key] = {
                'credit': get_credit_amount(credit=0, debit=disposed_asset.book_value),
                'account': account,
                'debit': get_debit_amount(credit=0, debit=disposed_asset.book_value),
                'text': f'{disposed_asset.fixed_asset} NBV',
                'suffix': invoice.suffix if account.is_income_statement else "",
                'accounting_date': invoice.invoice_date,
                'ledger_type_reference': invoice.customer.name,
                'object_items': self.get_line_object_items(account, object_items),
                'sales_invoice': invoice
            }

            fixed_asset.status = 1
            fixed_asset.save()

    def add_account_line(self, invoice_item, object_items, content_type):
        account = invoice_item.account
        amount = invoice_item.sub_total
        if account in self.ledger_lines['costs']:
            self.ledger_lines['costs'][account]['credit'] += get_credit_amount(credit=amount, debit=0)
            self.ledger_lines['costs'][account]['debit'] += get_debit_amount(credit=amount, debit=0)
        else:
            self.ledger_lines['costs'][account] = {
                'debit': get_debit_amount(credit=amount, debit=0),
                'credit': get_credit_amount(credit=amount, debit=0),
                'accounting_date': invoice_item.invoice.invoice_date,
                'text': f"{invoice_item.description}",
                'suffix': invoice_item.invoice.suffix if account.is_income_statement else "",
                'object_id': invoice_item.invoice.id,
                'content_type': content_type,
                'ledger_type_reference': invoice_item.invoice.customer.name,
                'object_items': self.get_line_object_items(account, object_items),
                'sales_invoice': invoice_item.invoice
            }

    def add_vat_line(self, invoice, object_items, content_type):
        amount = invoice.vat_amount
        vat_account = self.vat_code.account
        self.ledger_lines['vats'][invoice] = {
            'credit': get_credit_amount(credit=amount, debit=0),
            'accounting_date': invoice.invoice_date,
            'debit': get_debit_amount(credit=amount, debit=0),
            'vat_code': self.vat_code,
            'text': str(invoice),
            'suffix': invoice.suffix if vat_account.is_income_statement else "",
            'account': vat_account,
            'object_id': invoice.id,
            'content_type': content_type,
            'ledger_type_reference': invoice.customer.name,
            'object_items': self.get_line_object_items(vat_account, object_items),
            'sales_invoice': invoice
        }

    def get_default_customer_account(self, invoice: Invoice):
        if invoice.customer and invoice.customer.default_account:
            return invoice.customer.default_account
        else:
            return self.customer_account

    def add_invoice_line(self, invoice, object_items, customer_content_type):
        default_account = self.get_default_customer_account(invoice)
        if self.journal_type == 'totals':
            amount = invoice.total_amount
            self.summarize_invoice_lines(
                account=default_account,
                amount=amount,
                object_items=object_items,
                customer_content_type=customer_content_type
            )
        else:
            self.separate_invoice_lines(
                account=default_account,
                invoice=invoice,
                customer_content_type=customer_content_type
            )

    def separate_invoice_lines(self, account, invoice, customer_content_type):
        if invoice.total_amount:
            self.ledger_lines['invoices'][invoice.id] = {
                'debit': get_debit_amount(debit=invoice.total_amount, credit=0),
                'credit': get_credit_amount(debit=invoice.total_amount, credit=0),
                'accounting_date': invoice.invoice_date,
                'text': str(invoice),
                'suffix': invoice.suffix if account.is_income_statement else "",
                'account': account,
                'object_id': invoice.customer_id,
                'content_type': customer_content_type,
                'object_items': invoice.object_items.all(),
                'ledger_type_reference': invoice.customer.name,
                'sales_invoice': invoice
            }

    def summarize_invoice_lines(self, account, amount, object_items, customer_content_type):
        if object_items.count() > 0:
            invoice_line_key = "_".join([str(object_item.id) for object_item in object_items.all()])
            if invoice_line_key in self.ledger_lines['invoices']:
                self.ledger_lines['invoices'][invoice_line_key]['total'] += amount
            else:
                self.ledger_lines['invoices'][invoice_line_key] = {
                    'debit': get_debit_amount(debit=amount, credit=0),
                    'credit': get_credit_amount(debit=amount, credit=0),
                    'text': str(account),
                    'account': account,
                    'object_items': object_items.all()
                }
        else:
            if account in self.ledger_lines['invoices']:
                self.ledger_lines['invoices'][account]['debit'] += get_debit_amount(debit=amount, credit=0)
                self.ledger_lines['invoices'][account]['credit'] += get_credit_amount(debit=amount, credit=0)
            else:
                _object_items = []
                if account.is_income_statement:
                    _object_items = object_items.all()
                self.ledger_lines['invoices'][account] = {
                    'debit': get_debit_amount(debit=amount, credit=0),
                    'credit': get_credit_amount(debit=amount, credit=0),
                    'text': str(account),
                    'account': account,
                    'object_items': _object_items
                }

    def add_discount_allowed_line(self, invoice, object_items):
        amount = 0
        if invoice.customer_discount:
            amount = invoice.customer_discount
        if invoice.line_discount and self.company.is_expense_discount:
            amount = invoice.line_discount

        amount = amount * -1 if invoice.is_credit else amount
        if self.discount_allowed_account in self.ledger_lines['discounts']:
            self.ledger_lines['discounts'][self.discount_allowed_account]['credit'] += get_credit_amount(
                credit=amount, debit=0)
            self.ledger_lines['discounts'][self.discount_allowed_account]['debit'] += get_debit_amount(
                credit=amount, debit=0)
        else:
            self.ledger_lines['discounts'][self.discount_allowed_account] = {
                'credit': get_credit_amount(credit=amount, debit=0),
                'debit': get_debit_amount(credit=amount, debit=0),
                'accounting_date': invoice.invoice_date,
                'text':  str(self.discount_allowed_account),
                'suffix': invoice.suffix if self.discount_allowed_account.is_income_statement else "",
                'object_items': self.get_line_object_items(account=self.discount_allowed_account, object_items=object_items)
            }
        return amount

    def add_inventory_control_line(self, invoice_item, amount, object_items):
        is_reverse = invoice_item.invoice.is_credit
        account = self.get_inventory_control_account(invoice_item.inventory)
        logger.info(f"{'Debit' if is_reverse else 'Credit'} to {account} inventory {invoice_item.inventory}({invoice_item.inventory.id}) control line --> {amount}")
        if account in self.ledger_lines['controls']:
            if is_reverse:
                self.ledger_lines['controls'][account]['debit'] += get_debit_amount(credit=0, debit=amount)
                self.ledger_lines['controls'][account]['credit'] += get_credit_amount(credit=0, debit=amount)
            else:
                self.ledger_lines['controls'][account]['credit'] += get_credit_amount(credit=amount, debit=0)
                self.ledger_lines['controls'][account]['debit'] += get_debit_amount(credit=amount, debit=0)
        else:
            self.ledger_lines['controls'][account] = {
                'debit': get_debit_amount(credit=0, debit=amount if is_reverse else 0),
                'credit': get_credit_amount(credit=0 if is_reverse else amount, debit=0),
                'text': 'Inventory Control',
                'suffix': invoice_item.invoice.suffix if account.is_income_statement else "",
                'accounting_date': invoice_item.invoice.invoice_date,
                'object_items': self.get_line_object_items(account, object_items),
                'sales_invoice': invoice_item.invoice
            }
        _, fifo_value, _ = invoice_item.inventory.calculate_summary_values()
        logger.info(f"\t\t\t\t Now value of {invoice_item.inventory} is {invoice_item.inventory.total_value} vs "
                    f"fifo value {fifo_value}")

    def add_inventory_sales_line(self, invoice_item,  object_items, is_expense=False):
        if is_expense:
            amount = invoice_item.sub_total
        else:
            amount = invoice_item.total_price_excluding
        account = self.get_inventory_sales_account(inventory=invoice_item.inventory)

        if account in self.ledger_lines['sales']:
            self.ledger_lines['sales'][account]['credit'] += get_credit_amount(credit=amount, debit=0)
            self.ledger_lines['sales'][account]['debit'] += get_debit_amount(credit=amount, debit=0)
        else:
            self.ledger_lines['sales'][account] = {
                'credit': get_credit_amount(credit=amount, debit=0),
                'debit': get_debit_amount(credit=amount, debit=0),
                'accounting_date': invoice_item.invoice.invoice_date,
                'text': "Inventory Sales",
                'suffix': invoice_item.invoice.suffix if account.is_income_statement else "",
                'object_items': self.get_line_object_items(account, object_items),
                'sales_invoice': invoice_item.invoice
            }

    def add_inventory_costs_line(self, invoice_item, amount, object_items):
        is_reverse = invoice_item.invoice.is_credit
        account = self.get_inventory_cost_of_sale_account(invoice_item.inventory)
        if account in self.ledger_lines['costs']:
            if is_reverse:
                self.ledger_lines['costs'][account]['credit'] += get_credit_amount(credit=amount, debit=0)
                self.ledger_lines['costs'][account]['debit'] += get_debit_amount(credit=amount, debit=0)
            else:
                self.ledger_lines['costs'][account]['debit'] += get_debit_amount(debit=amount, credit=0)
                self.ledger_lines['costs'][account]['credit'] += get_credit_amount(debit=amount, credit=0)
        else:
            self.ledger_lines['costs'][account] = {
                'debit': get_debit_amount(debit=0 if is_reverse else amount, credit=0),
                'credit': get_credit_amount(debit=0, credit=amount if is_reverse else 0),
                'text': "Inventory cost of sales",
                'accounting_date': invoice_item.invoice.invoice_date,
                'suffix': invoice_item.invoice.suffix if account.is_income_statement else "",
                'object_items': self.get_line_object_items(account, object_items),
                'sales_invoice': invoice_item.invoice
            }

    # noinspection PyMethodMayBeStatic
    def get_line_object_items(self, account, object_items):
        if account.is_income_statement:
            return object_items.all()
        return []


class InvoiceUpdateInventoryMovement(object):
    is_summarized = True

    def __init__(self, update, invoices):
        self.update = update
        self.invoices = invoices
        self.invoice_items_adjustments = {}
        self.content_type = self.get_content_type()

    # noinspection PyMethodMayBeStatic
    def get_content_type(self):
        return ContentType.objects.get_for_model(InvoiceItem, for_concrete_model=False)

    def split_invoices_for_update(self):
        credit_linked_invoices = []
        main_invoices = []

        for invoice in self.invoices:
            if invoice.is_credit:
                credit_linked_invoices.append(invoice)
            else:
                main_invoices.append(invoice)
        return main_invoices, credit_linked_invoices

    def execute(self):
        main_invoices, credit_invoices = self.split_invoices_for_update()

        for invoice in main_invoices:
            self.create_returned_transaction(invoice)

        for invoice in credit_invoices:
            self.create_received_transaction(invoice)

    def create_transaction(self, invoice):
        return Returned.objects.create(
            branch=self.update.branch,
            period=self.update.period,
            module=Module.SALES,
            date=now(),
            profile=self.update.profile,
            role=self.update.role,
            invoice=invoice
        )

    # noinspection PyMethodMayBeStatic
    def get_received_quantity(self, quantity):
        if quantity:
            return quantity * -1
        return quantity

    def create_received_transaction(self, invoice):
        content_type = ContentType.objects.get_for_model(model=InvoiceItem, for_concrete_model=False)

        for invoice_item in invoice.invoice_items.all():
            if invoice_item.inventory:
                if invoice_item.inventory.is_service_sku:
                    invoice_item.calculate_service_cost()
                self.get_received_quantity(invoice_item.quantity)

                fifo = ReceiveReturnedInventoryItem(
                    content_type=content_type,
                    received_object_item=invoice_item,
                    sold_object_item=invoice_item.item_link,
                    year=invoice.period.period_year
                )
                fifo.execute()

                # adjustment = self.create_received_invoice_item(transaction, invoice_item)

                self.invoice_items_adjustments[invoice_item] = fifo

                ledger_lines_count = len(fifo.ledger_lines)
                current_ledger_lines = self.get_current_ledger_lines(invoice_item=invoice_item)
                if current_ledger_lines == 0:
                    for inventory_ledger_line in fifo.ledger_lines:
                        quantity = inventory_ledger_line.quantity
                        if quantity != 0:
                            unit_price = inventory_ledger_line.price
                            self.create_inventory_ledger(
                                invoice_item=invoice_item,
                                invoice=invoice,
                                quantity=quantity,
                                price=unit_price,
                                is_new=True
                            )
                else:
                    counter = 1
                    for inventory_ledger_line in fifo.ledger_lines:
                        quantity = inventory_ledger_line.quantity
                        if quantity != 0:
                            unit_price = inventory_ledger_line.price
                            is_new = self.is_new_ledger_line(counter, ledger_lines_count)
                            self.create_inventory_ledger(
                                invoice_item=invoice_item,
                                invoice=invoice,
                                quantity=quantity,
                                price=unit_price,
                                is_new=is_new
                            )
                        counter += 1
                invoice_item.inventory.recalculate_balances(year=invoice.period.period_year)

    # noinspection PyMethodMayBeStatic
    def is_new_ledger_line(self, line_counter, ledger_lines_count):
        if ledger_lines_count > 0 and line_counter == 1:
            return False
        return line_counter <= ledger_lines_count

    def create_returned_transaction(self, invoice):
        for invoice_item in invoice.invoice_items.all():
            if invoice_item.inventory:
                fifo = FiFoReturned(
                    inventory=invoice_item.inventory,
                    quantity=invoice_item.quantity,
                    unit_price=invoice_item.price,
                    content_object=invoice_item
                )
                fifo.execute()

                self.invoice_items_adjustments[invoice_item] = fifo
                service_cost = None
                if invoice_item.inventory.is_service_sku:
                    service_cost = invoice_item.calculate_service_cost()

                invoice_item.inventory.recalculate_balances(year=invoice.period.period_year)

                # inventory_ledger_lines = self.get_ledger_lines(adjustment.returned_lines, service_cost)
                inventory_ledger_lines = self.group_ledger_line(fifo.returned_lines, service_cost, True)
                ledger_lines_count = len(inventory_ledger_lines)
                current_ledger_lines = self.get_current_ledger_lines(invoice_item=invoice_item)
                if current_ledger_lines == 0:
                    for inventory_ledger_line in inventory_ledger_lines:
                        quantity = inventory_ledger_line.get('quantity', 0)
                        if quantity != 0:
                            unit_price = inventory_ledger_line.get('price')
                            self.create_inventory_ledger(
                                invoice_item=invoice_item,
                                invoice=invoice,
                                quantity=quantity,
                                price=unit_price,
                                is_new=True
                            )
                else:
                    counter = 1
                    for inventory_ledger_line in inventory_ledger_lines:
                        quantity = inventory_ledger_line.get('quantity', 0)
                        if quantity != 0:
                            unit_price = inventory_ledger_line.get('price')
                            is_new = self.is_new_ledger_line(counter, ledger_lines_count)
                            self.create_inventory_ledger(
                                invoice_item=invoice_item,
                                invoice=invoice,
                                quantity=quantity,
                                price=unit_price,
                                is_new=is_new
                            )
                        counter += 1

    def get_current_ledger_lines(self, invoice_item):
        filters = {'inventory': invoice_item.inventory}
        if self.content_type:
            filters['content_type'] = ContentType.objects.get_for_model(invoice_item)
            filters['object_id'] = invoice_item.id
        return InventoryLedger.objects.filter(**filters).count()

    # noinspection PyMethodMayBeStatic
    def group_ledger_line(self, received_returned_lines, service_cost, is_return=False):
        ledger_lines = []
        for received_returned_line in received_returned_lines:
            if service_cost:
                unit_price = service_cost
            else:
                unit_price = received_returned_line.price
            ledger_lines.append({'quantity': received_returned_line.quantity, 'price': unit_price})
        return ledger_lines

    # noinspection PyMethodMayBeStatic
    def check_is_return(self, invoice: Invoice):
        if invoice.is_credit:
            return False
        return True

    def create_inventory_ledger(self, invoice_item: InvoiceItem, invoice: Invoice, quantity: Decimal, price: Decimal,
                                is_new=True):
        is_return = self.check_is_return(invoice=invoice)
        if not invoice_item.inventory:
            return None
        ledger = InventoryLedger.objects.add_invoice_item_line(
            invoice_item=invoice_item,
            module=Module.SALES,
            unit_price=price,
            profile=self.update.profile,
            quantity=quantity,
            status=InventoryLedger.COMPLETE,
            is_new=is_new
        )
        return ledger

    # noinspection PyMethodMayBeStatic
    def create_received_invoice_item(self, transaction, invoice_item):
        received_item = ReceiveSoldInventoryItem(transaction, invoice_item, invoice_item.item_link)
        received_item.execute()
        return received_item

    # noinspection PyMethodMayBeStatic
    def create_returned_invoice_item(self, transaction, invoice, invoice_item):
        returned_item = CreateReturnedInventory(transaction, invoice_item.inventory, invoice_item.quantity,
                                                invoice.is_credit, invoice_item)
        inventory_adjustment = returned_item.execute()
        return inventory_adjustment


class ReturnInvoiceItemInventory:

    def __init__(self, transaction, invoice_item):
        self.transaction = transaction
        self.invoice_item = invoice_item
        self.returned_lines = []
        self.received_returned_cost = 0

    def execute(self):
        quantity = self.invoice_item.quantity
        inventory = BranchInventory.objects.get(id=self.invoice_item.inventory.id)
        inventory_returned = InventoryReturned.objects.create(
            returned=self.transaction,
            inventory=inventory,
            quantity=quantity,
            invoice_item=self.invoice_item
        )
        opening_quantity = float(inventory.in_stock)
        qty = abs(quantity)
        if inventory_returned:
            inventory_invoice_item_received = self.invoice_item.item_link

            closing_quantity = float(opening_quantity) - float(opening_quantity)
            received_returned = InventoryReceivedReturned.objects.create(
                inventory_returned=inventory_returned, quantity=qty, price=inventory_invoice_item_received.price,
                opening_balance=opening_quantity, balance=closing_quantity
            )
            self.received_returned_cost = float(qty) * float(inventory_invoice_item_received.price)
            self.returned_lines.append(received_returned)
