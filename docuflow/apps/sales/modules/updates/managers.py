from collections import defaultdict

from django.db.models import Manager
from django.db.models.query import QuerySet

from django.utils import timezone


class UpdateQuerySet(QuerySet):
    def paid(self):
        return self.filter(status__slug='paid')

    def status(self, status):
        return self.filter(status=status)

    def company(self, company):
        return self.filter(company=company)

    def purchase_order(self):
        return self.filter(invoice_type__code='purchase-order')

    def open(self):
        return self.filter(open_amount__gt=0)

    def invoice(self):
        return self.filter(invoice_type__code='invoice')

    def in_flow(self):
        return self.filter(open_amount__gt=0)


class PurchaseOrderManager(Manager):

    def get_queryset(self, company, supplier=None, status_slug=None):
        q = self.filter(company=company, invoice_type__code='purchase-order')
        if supplier:
            q = q.filter(supplier=supplier)
        if status_slug:
            q = q.filter(status__slug=status_slug)
        return q


class InvoiceManager(Manager):
    def get_queryset(self):
        return InvoiceQuerySet(self.model, using=self._db)  # Important!

    def paid(self):
        return self.get_queryset().paid()

    def company(self, company):
        return self.get_queryset().company(company)

    def status(self, status):
        return self.get_queryset().status(status)

    def purchase_order(self, company, supplier=None, status_slug=None):
        q = self.filter(company=company, invoice_type__code='purchase-order')
        if supplier:
            q = q.filter(supplier=supplier)
        if status_slug:
            q = q.filter(status__slug=status_slug)
        return q

    def receipts(self, company, supplier):
        return self.filter(company=company, supplier=supplier, invoice_type__code='receipt')

    def fetch(self):
        return self.get_queryset().select_related(
            'company',
            'status',
            'invoice_type',
            'workflow',
            'supplier'
        ).prefetch_related(
            'invoice_references',
            'flows',
            'flows__status'
        )