from django import forms

from docuflow.apps.sales.models import Update
from docuflow.apps.period.models import Period
from .services import InvoiceUpdater


class UpdateInvoiceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        self.year = kwargs.pop('year')
        self.invoice_type = kwargs.pop('invoice_type')
        super(UpdateInvoiceForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.branch.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None

    date = forms.DateTimeField(label='Upto and including', required=True)

    class Meta:
        model = Update
        fields = ('date', 'period')
        widgets = {
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'period': forms.Select(attrs={'class': 'form-control chosen-select'}),
        }

    def clean(self):
        cleaned_data = super(UpdateInvoiceForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date.date()):
                self.add_error('date', 'Period and payment date do not match')

        return cleaned_data

    def save(self, commit=True):
        update = super().save(commit=False)
        update.profile = self.profile
        update.branch = self.branch
        update.role = self.role
        update.save()

        sales_update = InvoiceUpdater(update=update, invoice_type_code=self.invoice_type.value)
        sales_update.execute()

        return update
