import logging
from decimal import Decimal
from typing import Any

from django import forms
from django.contrib.contenttypes.fields import ContentType
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy

from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.customer.models import Ledger as CustomerLedger
from docuflow.apps.common.widgets import DataAttributesSelect
from docuflow.apps.customer.models import Customer
from docuflow.apps.customer.fields import CustomerModelChoiceField
from docuflow.apps.company.models import PaymentMethod
from docuflow.apps.sales.models import Receipt, ReceiptAccount, InvoiceReceipt, SundryReceiptAllocation, ReceiptPayment
from docuflow.apps.period.models import Period
from docuflow.apps.sales.enums import ReceiptType
from .exceptions import PaymentException

logger = logging.getLogger(__name__)


def payment_method_select_data(company):
    payment_data = {'data-has-change': {'': ''}}

    payment_methods = PaymentMethod.objects.filter(company=company).all()
    for payment_method in payment_methods:
        payment_data['data-has-change'][payment_method.id] = '1' if payment_method.has_change else '0'
    return DataAttributesSelect(choices=[('', '-----------')] + [(p.id, str(p)) for p in payment_methods],
                                data=payment_data)


class CreateReceiptForm(forms.ModelForm):
    customer_ledger_lines = []
    ledger_total = 0
    total_discounts = 0

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.request = kwargs.pop('request')
        self.profile = kwargs.pop('profile')
        super().__init__(*args, **kwargs)
        self.fields['customer'].queryset = Customer.objects.filter(company=self.branch.company).order_by('name')
        self.fields['payment_method'].queryset = PaymentMethod.objects.filter(company=self.branch.company)
        self.fields['payment_method'].widget = payment_method_select_data(self.branch.company)
        self.fields['amount'].label = 'Amount tendered'

    customer = CustomerModelChoiceField(queryset=None, required=True, widget=forms.Select(
        attrs={'class': 'form-control', 'data-customer-invoices-url': reverse_lazy('customer_receipt_invoices')}))

    class Meta:
        model = Receipt
        fields = ('customer', 'date', 'amount', 'reference', 'change', 'balance', 'payment_method')
        widgets = {
            'amount': forms.TextInput(attrs={'class': 'form-control',
                                             'data-payment-line-url': reverse_lazy('receipt_payment_line')}),
        }

    def clean(self):
        cleaned_data = super(CreateReceiptForm, self).clean()

        if not cleaned_data['payment_method']:
            self.add_error('payment_method', 'Payment method is required')

        return cleaned_data

    def get_period(self, branch):
        period = Period.objects.get_by_date(branch.company, self.cleaned_data['date']).first()
        if not period:
            raise forms.ValidationError('Period for selected date does not exists')
        return period

    def save(self, commit=True):
        receipt = super().save(commit=False)
        receipt_amount = receipt.amount
        receipt.receipt_type = ReceiptType.ALLOCATION
        receipt.amount = receipt_amount * -1
        receipt.branch = self.branch
        receipt.created_by = self.profile
        receipt.period = self.get_period(self.branch)
        receipt.save()

        self.create_receipt_payment(receipt, receipt_amount)

        # invoice_formsets = InvoiceReceiptFormset(data=self.request.POST)
        for field, value in self.request.POST.items():
            if field.startswith('pay_ledger_', 0, 11):
                amount = Decimal(self.request.POST.get(f'amount_{value}', 0)) * -1
                CustomerLedger.objects.create_payment(
                    ledger_id=value,
                    receipt=receipt,
                    amount=amount
                )
            elif field.startswith('payment_amount_', 0, 15):
                self.create_receipt_account(receipt, field, value)
            elif field.startswith('receipt_allocate_', 0, 17):
                self.create_sundry_allocation(receipt, field, value)
            elif field.startswith('pay_', 0, 4):
                self.create_invoice_receipt(receipt, field, value)

        self.create_customer_ledger(receipt)
        return receipt

    def create_receipt_payment(self, receipt, amount):
        ReceiptPayment.objects.create(
            receipt=receipt,
            payment_method=self.cleaned_data['payment_method'],
            amount=amount
        )

    def create_receipt_account(self, receipt, field, value):
        if value:
            payment_line_id = field[15:]
            logger.info(f"payment line {payment_line_id}")
            payment_data = {
                'account': self.request.POST.get(f'account_{payment_line_id}'),
                'vat_code': self.request.POST.get(f'vat_code_{payment_line_id}'),
                'vat_amount': self.request.POST.get(f'vat_amount_{payment_line_id}'),
                'amount': value
            }
            logger.info(payment_data)
            receipt_payment_form = ReceiptAccountForm(data=payment_data, company=self.branch.company)
            if receipt_payment_form.is_valid():
                receipt_payment = receipt_payment_form.save(commit=False)
                receipt_payment.receipt = receipt
                receipt_payment.save()
                logger.info(f"Account is {receipt_payment.account}({receipt_payment.account.id}) for amount {receipt_payment.amount}")
                # ledger_total += receipt_payment.amount
                CustomerLedger.new(
                    receipt=receipt,
                    receipt_payment=receipt_payment,
                    profile=self.profile
                )
            else:
                raise ValidationError(receipt_payment_form.errors)

    def create_sundry_allocation(self, receipt: Receipt, field: str, value: Any):
        line_id = field[17:]
        amount = self.request.POST.get(f'receipt_balance_{line_id}')
        if amount:
            receipt_allocation_data = {'amount': amount, 'receipt': value, 'parent': receipt.id}

            receipt_allocation_form = SundryReceiptAllocationForm(data=receipt_allocation_data)
            if receipt_allocation_form.is_valid():
                sundry_allocation = receipt_allocation_form.save(commit=False)

                self.ledger_total += sundry_allocation.amount
            else:
                raise ValidationError(receipt_allocation_form.errors)

    def create_invoice_receipt(self, receipt: Receipt, field: str, value: Any):
        if value:
            is_discounted = bool(self.request.POST.get(f"is_discounted_{value}", False))
            data = {
                'open_amount': self.request.POST.get(f'open_amount_{value}', 0),
                'invoice': value,
                'amount': self.request.POST.get(f'amount_{value}', 0),
                'discount': self.request.POST.get(f'discount_{value}', 0) if is_discounted else 0
            }
            receipt_invoice_form = InvoiceReceiptForm(data=data, company=self.branch.company)
            if receipt_invoice_form.is_valid():
                invoice_receipt = receipt_invoice_form.save(commit=False)
                invoice_receipt.receipt = receipt
                invoice_receipt.save()

                self.total_discounts += invoice_receipt.discount

                invoice = invoice_receipt.invoice
                invoice.open_amount = Decimal(data.get('open_amount', 0))
                invoice.save()
            else:
                raise ValidationError(receipt_invoice_form.errors)

    def create_customer_ledger(self, receipt: Receipt):
        logger.info(f"Create ledger for receipt {receipt.id}")
        CustomerLedger.objects.update_or_create(
            customer=receipt.customer,
            object_id=receipt.id,
            content_type=ContentType.objects.get_for_model(receipt),
            defaults={
                'amount': receipt.amount,
                'text': str(receipt),
                'date': receipt.date,
                'period': receipt.period,
                'created_by': self.profile,
                'status': LedgerStatus.PENDING
            }
        )
        if self.total_discounts:
            logger.info(
                f"Account is {receipt.branch.company.default_discount_allowed_account} for amount {self.total_discounts}")
            CustomerLedger.objects.update_or_create(
                customer=receipt.customer,
                journal_number=str(receipt.branch.company.default_discount_allowed_account),
                text=str(receipt),
                defaults={
                    'amount': self.total_discounts * -1,
                    'date': receipt.date,
                    'period': receipt.period,
                    'created_by': self.profile,
                    'status': LedgerStatus.PENDING
                }
            )


class ReceiptForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.invoice = kwargs.pop('invoice')
        self.request = kwargs.pop('request')
        self.branch = kwargs.pop('branch')
        super().__init__(*args, **kwargs)

    class Meta:
        fields = ('amount', 'date')
        model = Receipt

    def get_payment_methods_total(self, payment_methods):
        total = Decimal(0)
        for payment_method_id in payment_methods:
            amount = self.request.POST.get(f'amount_{payment_method_id}', 0)
            if amount:
                total += Decimal(amount)
        for receipt_id in self.request.POST.getlist('receipts'):
            amount = self.request.POST.get(f'receipt_amount_{receipt_id}', 0)
            if amount:
                total += Decimal(amount)
        return total

    def clean(self):
        cleaned_data = super().clean()
        invoices_total = self.cleaned_data['amount']
        payment_methods = self.request.POST.getlist('payment_method')
        if len(payment_methods) == 0:
            raise ValidationError("No payment method selected, please allocate the money to appropriate payment method")
        total_allocated = self.get_payment_methods_total(payment_methods)
        if total_allocated != invoices_total:
            raise ValidationError('Please enter all the payment amount')
        return cleaned_data

    def save(self, commit=True):
        receipt = super().save(commit=False)
        receipt.branch = self.branch
        receipt.reference = str(self.invoice)
        receipt.customer = self.invoice.customer
        receipt.period = Period.objects.get_by_date(self.branch.company, self.cleaned_data['date']).first()
        receipt.save()

        self.create_invoice_receipt(receipt, self.invoice)

        self.create_receipt_payment_methods(receipt=receipt)

        return receipt

    def create_invoice_receipt(self, receipt, invoice):
        invoice_receipt, _ = InvoiceReceipt.objects.update_or_create(
            invoice=invoice,
            receipt=receipt,
            defaults={
                'amount': self.cleaned_data['amount']
            }
        )

        open_amount = invoice.calculate_open_amount(invoice.open_amount)
        invoice.open_amount = open_amount
        invoice.save()

        return invoice_receipt

    def create_receipt_payment_methods(self, receipt: Receipt):
        payment_methods = self.request.POST.getlist('payment_method')
        total_amount = 0
        for payment_method_id in payment_methods:
            data = {
                'payment_method': payment_method_id,
                'amount': self.request.POST.get(f'amount_{payment_method_id}', 0),
                'receipt': receipt
            }
            try:
                receipt_payment = ReceiptPayment.objects.get(receipt=receipt, payment_method_id=data['payment_method'])
                receipt_payment_form = ReceiptPaymentForm(data=data, company=self.branch.company,
                                                          instance=receipt_payment)
            except ReceiptPayment.DoesNotExist:
                receipt_payment_form = ReceiptPaymentForm(data=data, company=self.branch.company)
            if receipt_payment_form.is_valid():
                receipt_payment = receipt_payment_form.save()
                total_amount += receipt_payment.amount

    def create_sundry_receipt_payment_allocation(self, receipt: Receipt):
        for sundry_receipt_id in self.request.POST.getlist('receipts'):
            SundryReceiptAllocation.objects.get_or_create(
                parent_id=sundry_receipt_id, receipt=receipt,
                defaults={
                    'amount': Decimal(self.request.POST.get(f'receipt_amount_{sundry_receipt_id}', 0))
                }
            )


class InvoiceReceiptForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(InvoiceReceiptForm, self).__init__(*args, **kwargs)
        self.fields['invoice'].queryset = self.fields['invoice'].queryset.filter(branch__company=self.company)
    open_amount = forms.DecimalField(decimal_places=2, required=False)

    class Meta:
        model = InvoiceReceipt
        fields = ('invoice', 'amount', 'discount')

    def save(self, commit=True):
        invoice = self.cleaned_data['invoice']
        if invoice.open_amount == 0:
            raise PaymentException(f'Invoice {invoice} is already paid')
        invoice_receipt = super(InvoiceReceiptForm, self).save(commit=commit)
        return invoice_receipt


class CashReceiptForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.company = kwargs.pop('company')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Receipt
        fields = ('date', 'amount', 'balance', 'change')
        widgets = {
            'amount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['balance'] > 0:
            self.add_error('balance', 'Balance must be zero, please allocate all the amount to available payment'
                                      ' methods')

        allocations = self.request.POST.getlist('payment_method')
        if len(allocations) == 0 and self.cleaned_data['amount'] > 0:
            self.add_error('amount', 'Please allocate the amount to relevant payment method')
        return cleaned_data

    def save(self, commit=True):
        receipt = super().save(commit=False)
        invoice_receipt_data = self.request.session.get('sales_invoice_receipt', {})

        invoice_receipt_data['change'] = round(float(receipt.change), 2) if receipt.change else 0
        invoice_receipt_data['amount'] = round(float(receipt.amount), 2)
        invoice_receipt_data['balance'] = round(float(receipt.balance), 2)
        invoice_receipt_data['date'] = receipt.date.strftime("%Y-%m-%d")
        invoice_receipt_data['payment_methods'] = []
        invoice_receipt_data['receipts'] = []

        allocations = self.request.POST.getlist('payment_method')
        for allocation_method_id in allocations:
            amount_str = self.request.POST.get(f'amount_{allocation_method_id}')
            if amount_str != '':
                amount = Decimal(amount_str.replace(',', ''))
                if amount != 0:
                    payment_method = PaymentMethod.objects.get(pk=allocation_method_id)
                    data = {
                        'amount': round(float(amount), 2),
                        'payment_method': allocation_method_id,
                    }
                    if payment_method.payment_option and payment_method.payment_option.has_change:
                        data['amount'] = round(float(amount - receipt.change), 2)
                        invoice_receipt_data['payment_methods'].append(data)
                    else:
                        invoice_receipt_data['payment_methods'].append(data)

        credit_allocations = self.request.POST.getlist('receipts')
        for receipt_id in credit_allocations:
            data = {
                'receipt': receipt_id,
                'amount': self.request.POST.get(f'receipt_amount_{receipt_id}')
            }
            invoice_receipt_data['receipts'].append(data)

        self.request.session['sales_invoice_receipt'] = invoice_receipt_data
        return receipt


class ReceiptAccountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = self.fields['account'].queryset.filter(company=company)
        self.fields['vat_code'].queryset = self.fields['vat_code'].queryset.filter(company=company)

    class Meta:
        fields = ('amount', 'vat_amount', 'vat_code', 'account')
        model = ReceiptAccount


class ReceiptPaymentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        super().__init__(*args, **kwargs)
        self.fields['payment_method'].queryset = self.fields['payment_method'].queryset.filter(company=company)

    class Meta:
        model = ReceiptPayment
        fields = ('payment_method', 'amount', 'receipt')


class SundryReceiptAllocationForm(forms.ModelForm):

    class Meta:
        fields = ('amount', 'receipt', 'parent')
        model = SundryReceiptAllocation

    def save(self, commit=True):
        sundry_receipt_allocation = super().save(commit=False)

        amount = sundry_receipt_allocation.amount
        if sundry_receipt_allocation.amount < 0:
            amount = sundry_receipt_allocation.amount * -1

        sundry_receipt_allocation.amount = amount
        sundry_receipt_allocation.save()

        allocated_receipt = sundry_receipt_allocation.receipt
        allocated_receipt.balance += sundry_receipt_allocation.amount
        allocated_receipt.save()

        return sundry_receipt_allocation


InvoiceReceiptFormset = forms.inlineformset_factory(parent_model=Receipt, model=InvoiceReceipt, form=InvoiceReceiptForm,
                                                    can_delete=True)
ReceiptPaymentFormset = forms.inlineformset_factory(parent_model=Receipt, model=ReceiptAccount, form=ReceiptAccountForm,
                                                    extra=1)


class AllocateReceiptForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        if 'company' in kwargs:
            self.company = kwargs.pop('company')
        if 'year' in kwargs:
            self.year = kwargs.pop('year')
        super(AllocateReceiptForm, self).__init__(*args, **kwargs)
        if self.company:
            self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
            self.fields['period'].empty_label = None
            self.fields['customer'].queryset = Customer.objects.invoicable().filter(company=self.company)

    customer = CustomerModelChoiceField(queryset=None, required=True, widget=forms.Select(
        attrs={'class': 'form-control', 'data-customer-invoices-url': reverse_lazy('customer_allocate_receipts')}))

    class Meta:
        model = Receipt
        fields = ('customer', 'period', 'date')
        widgets = {
            'date': forms.TextInput(attrs={'class': 'datepicker'})
        }
