from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.ReceiptsView.as_view(), name='receipts_index'),
    path('create/', views.CreateReceiptsView.as_view(), name='create_receipt'),
    path('invoices/', views.CustomerInvoicesView.as_view(), name='customer_receipt_invoices'),
    path('payment/line/', views.PaymentLineView.as_view(), name='receipt_payment_line'),
    path('create/for-cash/', views.CreateCashInvoiceReceiptView.as_view(),
         name='create_cash_invoice_receipt'),
    path('<int:invoice_id>/create/payments/', csrf_exempt(views.CreateCashPaymentsView.as_view()),
         name='cash_invoice_payments'),
    path('<int:pk>/detail/', views.ReceiptDetailView.as_view(), name='receipt_detail'),
    path('<int:pk>/edit/', views.EditReceiptView.as_view(), name='edit_receipt'),
    path('<int:pk>/delete/', csrf_exempt(views.DeleteReceiptView.as_view()), name='delete_receipt'),
    path('customer/<int:customer_id>/invoices/', csrf_exempt(views.CreateCustomerInvoicesReceiptView.as_view()),
         name='create_customer_invoices_receipt'),
    path('view/<int:invoice_id>/payments/', csrf_exempt(views.ViewInvoicePaymentsView.as_view()),
         name='view_invoice_payment'),
    path('pay/invoices/', csrf_exempt(views.CreateInvoicesReceiptView.as_view()),
         name='create_invoices_receipt'),
    path('invoice/form/', views.CashInvoiceReceiptFormView.as_view(),
         name='cash_invoice_receipt_form'),
    path('allocate/', views.AllocateView.as_view(), name='allocate_receipt'),
    path('allocate/documents/', views.AllocateDocumentsView.as_view(), name='customer_allocate_receipts'),
    path('document/<str:signature>/', csrf_exempt(views.ReceiptDocumentView.as_view()),
         name='receipt_document'),
    path('invoice/<int:pk>/create/', views.CreateInvoiceReceiptView.as_view(),
         name='create_invoice_receipt'),
    path('invoice/<int:pk>/edit/', views.EditInvoiceReceiptView.as_view(), name='edit_cash_invoice_receipt'),
]
