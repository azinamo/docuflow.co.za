from django.db.models import Sum, F, DecimalField
from django.db.models.functions import Coalesce

from docuflow.apps.sales.models import Receipt


def get_receipt_with_totals():
    return Receipt.objects.annotate(
        invoices_total=Coalesce(Sum('invoices__invoice__total_amount'), 0, output_field=DecimalField()),
        invoices_open_total=Coalesce(Sum('invoices__invoice__open_amount'), 0, output_field=DecimalField()),
        invoice_allocated=Coalesce(Sum('invoices__amount'), 0, output_field=DecimalField()),
        ledgers_total=Coalesce(Sum('customer_ledgers__amount'), 0, output_field=DecimalField()),
        ledgers_open_total=Coalesce(Sum('customer_ledgers__amount'), 0, output_field=DecimalField()),
        allocations_total=Coalesce(Sum('allocations__receipt__amount'), 0, output_field=DecimalField()),
        allocations_open_total=Coalesce(Sum('allocations__receipt__balance'), 0, output_field=DecimalField()),
        payment_allocation=Coalesce(Sum('allocations__amount'), 0, output_field=DecimalField()),
        total_original=F('invoices_total') + F('ledgers_total') + F('allocations_total'),
        total_open=F('invoices_open_total') + F('ledgers_open_total') + F('allocations_open_total'),
        allocation_amount=(F('ledgers_open_total') * -1) + F('invoice_allocated') + (F('payment_allocation') * -1)
    ).select_related(
        'customer', 'branch__company', 'branch__company__default_discount_allowed_account'
    ).prefetch_related(
        'customer__addresses', 'invoices', 'invoices__invoice', 'allocations', 'invoices__invoice__invoice_type',
        'allocations__receipt', 'customer_ledgers', 'accounts', 'payments'
    )
