import logging
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.urls import reverse

from docuflow.apps.common.enums import Module
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.customer.models import Customer, Ledger as CustomerLedger, LedgerPayment
from docuflow.apps.customer.services import age_invoice_receipt
from docuflow.apps.period.models import Period
from docuflow.apps.sales.enums import ReceiptType, InvoiceStatus
from docuflow.apps.sales.models import Invoice, Receipt, ReceiptPayment, InvoiceReceipt, Tracking, \
    SundryReceiptAllocation

logger = logging.getLogger(__name__)


class CreateInvoiceReceipt:

    def __init__(self, invoice, branch, date, amount, payments):
        logger.info("==============START CreateInvoiceReceipt+==============")
        self.invoice = invoice
        self.date = date
        self.amount = amount
        self.payments = payments
        self.branch = branch
        self.period = self.get_period_from_date()

    def create_receipt(self):
        logger.info(f"------------START CREATE RECEIPT-----------------{self.amount} at {self.date}")
        return Receipt.objects.create(
            branch=self.branch,
            date=self.date,
            amount=self.amount,
            reference=str(self.invoice),
            customer=self.invoice.customer,
            period=self.period,
            created_by=self.invoice.profile,
            content_type=ContentType.objects.get_for_model(self.invoice),
            object_id=self.invoice.pk
        )

    def execute(self):
        receipt = self.create_receipt()
        if receipt:
            self.create_invoice_payments(receipt)
            CustomerLedger.objects.create_from_receipt(
                receipt=receipt, period=self.period, amount=self.amount, date=self.date, profile=self.invoice.profile
            )
        return receipt

    def create_invoice_payments(self, receipt):
        logger.info("------------START CREATE RECEIPT PAYMENT METHOD SPLIT-----------------")
        for payment_method_id, amount in self.payments.items():
            if amount != 0:
                ReceiptPayment.objects.create(
                    amount=amount * -1 if self.invoice.is_credit else amount,
                    receipt=receipt,
                    payment_method_id=payment_method_id
                )

    def get_default_customer(self):
        customer = self.invoice.customer
        if not customer:
            customer = Customer.objects.filter(
                company=self.branch.company,
                is_default=True
            ).first()
        return customer

    def get_period_from_date(self):
        logger.info(f"date --> {self.date}")
        period = Period.objects.get_by_date(self.branch.company, self.date).first()
        if not period:
            raise ValueError(f"Period for date {self.date} not found. Please ensure the period is available.")
        return period


class CreateSundryPayment:

    def __init__(self, company, branch, profile, role, form, post_request, balance=0):
        logger.info("CreateSundryPayment")
        self.company = company
        self.branch = branch
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request
        self.balance = self.get_balance_amount(balance)

    def get_balance_amount(self, amount):
        if amount != '':
            return float(amount)
        return 0

    def monitize(self, string):
        return Decimal(string.replace(',', ''))

    def get_comment(self):
        return self.post_request.get('comment', None)

    def has_complete_payment(self):
        return True
        # logger.info("Has complete payment {} -->".format(self.balance))
        # if self.balance:
        #     comment = self.get_comment()
        #     if self.balance > 0 and not comment:
        #         raise NotAllocatedAmountException('You have not allocated the amount of payment, should it'
        #                                           ' be left unallocated')
        # return True

    def create(self):
        logger.info("Create payment receipt")
        receipt = self.form.save(commit=False)
        receipt.receipt_type = ReceiptType.SUNDRY
        receipt.branch = self.branch
        receipt.balance = self.balance
        receipt.role = self.role
        receipt.profile = self.profile
        receipt.company = self.company
        receipt.notes = self.get_comment()
        receipt.save()
        logger.info("Done creating payment receipt {}".format(receipt))

        CustomerLedger.objects.create_from_receipt(receipt, receipt.period, self.balance, receipt.date,
                                                   self.profile, Module.SALES)

        return receipt

    def execute(self):
        logger.info("Execute")
        if self.has_complete_payment():
            receipt = self.create()
            if receipt:
                self.create_invoice_payments(receipt)

    def create_invoice_payments(self, receipt):
        logger.info("start creating invoice payments")
        content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()
        for field in self.post_request:
            if field.startswith('invoice_amount', 0, 14):
                invoice_id = field[15:]

                allocated = 0
                allocate_id = "allocate_{}".format(invoice_id)
                if allocate_id in self.post_request and self.post_request[allocate_id]:
                    allocated = Decimal(self.post_request[allocate_id])

                invoice = Invoice.objects.prefetch_related(
                    'receipts'
                ).filter(
                    id=int(invoice_id)
                ).first()

                if invoice and allocated:
                    invoice_receipt = InvoiceReceipt.objects.create(
                        invoice_id=invoice_id,
                        receipt=receipt,
                        amount=allocated
                    )
                    if invoice_receipt:
                        open_amount = invoice.open_amount - invoice_receipt.amount
                        if open_amount > 0:
                            invoice.status = InvoiceStatus.PARTIALLY_PAID
                        else:
                            invoice.status = InvoiceStatus.PAID_NOT_PRINTED
                        invoice.open_amount = open_amount
                        invoice_receipt.save()

                    Tracking.objects.create(
                        content_type=content_type,
                        object_id=invoice.id,
                        comment=f"Payment of {invoice_receipt.amount} made by {str(receipt)}",
                        profile=self.profile,
                        role=self.role,
                        data={'link_url': reverse('receipt_detail', kwargs={'pk': receipt.id})}
                    )


class AllocateSundryPayment:

    def __init__(self, company, branch, profile, role, form, post_request):
        logger.info("AllocateSundryPayment")
        self.branch = branch
        self.company = company
        self.profile = profile
        self.role = role
        self.form = form
        self.post_request = post_request
        logger.info("Complete --- AllocateSundryPayment")

    def execute(self):
        logger.info("EXECUTE")
        with transaction.atomic():
            receipt = self.create()
            if receipt:
                for field, value in self.post_request.items():
                    if value != '':
                        if field.startswith('payment_allocated_', 0, 18):
                            self.create_receipt_payments(receipt, field, value)
                        if field.startswith('invoice_amount_paid', 0, 20):
                            self.create_invoice_receipts(receipt, field, value)
                        if field.startswith('ledger_payment_allocated', 0, 24):
                            self.create_ledger_payment(receipt, field, value)
            return receipt

    def create(self):
        logger.info("Create allocation receipt")
        receipt = self.form.save(commit=False)
        receipt.role = self.role
        receipt.branch = self.branch
        receipt.profile = self.profile
        receipt.company = self.company
        receipt.payment_type = ReceiptType.ALLOCATION
        receipt.save()
        logger.info(f"Done creating allocation receipt {receipt}")

        CustomerLedger.objects.create(
            customer=receipt.customer,
            period=receipt.period,
            quantity=1,
            amount=receipt.amount,
            module=Module.SALES,
            created_by=self.profile,
            date=receipt.date,
            status=LedgerStatus.PENDING,
            text=str(receipt),
            content_type=ContentType.objects.get_for_model(receipt),
            object_id=receipt.id
        )

        return receipt

    def create_receipt_payments(self, parent_receipt, field, value):
        logger.info("CREATE receipt payments")
        payment_id = field[18:]
        receipt = Receipt.objects.filter(id=payment_id).first()
        if receipt and value:
            payment_amount = Decimal(value) * -1
            child_payment = SundryReceiptAllocation.objects.create(
                parent=parent_receipt,
                receipt=receipt,
                amount=payment_amount
            )
            if child_payment:
                receipt.balance = receipt.balance - child_payment.amount
                receipt.save()

    def create_invoice_receipts(self, receipt, field, value):
        logger.info("CREATE receipt invoices")
        invoice_id = field[20:]
        amount_allocated = self.post_request.get('invoice_amount_paid_{}'.format(invoice_id))
        if amount_allocated:
            amount_allocated = Decimal(amount_allocated)
            invoice_payment = InvoiceReceipt.objects.create(
                receipt=receipt,
                invoice_id=invoice_id,
                amount=amount_allocated
            )
            if invoice_payment:
                invoice = invoice_payment.invoice

                invoice.open_amount = invoice.open_amount - invoice_payment.amount
                invoice.save()

    def create_ledger_payment(self, parent_receipt, field, value):
        ledger_id = field[25:]
        logger.info(f"CREATE ledger payments {value} on ledger {ledger_id}")
        ledger = CustomerLedger.objects.filter(
            id=ledger_id
        ).first()
        if ledger and value:
            payment_amount = Decimal(value) * -1
            ledger_payment = LedgerPayment.objects.create(
                payment=parent_receipt,
                ledger=ledger,
                amount=payment_amount
            )
            if ledger_payment:
                ledger.balance = ledger.balance - ledger_payment.amount
                ledger.save()


def delete_receipt(receipt: Receipt):
    content_type = ContentType.objects.get_for_model(model=receipt)

    invoices = []
    invoice_receipts = []
    for invoice_receipt in receipt.invoices.all():
        invoices.append(invoice_receipt.invoice)
        if receipt.cash_up:
            invoice_receipts.append(invoice_receipt)

    allocations = []
    for allocation in receipt.allocations.all():
        allocations.append(allocation.receipt)

    CustomerLedger.objects.filter(object_id=receipt.id, content_type=content_type).delete()

    receipt.delete()

    for receipt_allocation in allocations:
        balance = receipt_allocation.calculate_balance()
        receipt_allocation.balance = balance * -1 if balance else 0
        receipt_allocation.save()

    for invoice_receipt in invoice_receipts:
        age_invoice_receipt(invoice_receipt=invoice_receipt, is_reverse=True)

    for invoice in invoices:
        invoice.calculate_open_amount(invoice.total_amount * -1)
