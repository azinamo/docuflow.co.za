class PaymentException(Exception):
    pass


class AccountNotAvailableException(PaymentException):
    pass


class InvalidPeriodAccountingDateException(PaymentException):
    pass


class NotAllocatedAmountException(PaymentException):
    pass
