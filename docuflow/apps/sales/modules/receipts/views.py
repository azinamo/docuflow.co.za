import logging
import pprint
from datetime import datetime
from decimal import Decimal

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.core import signing
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, TemplateView, CreateView, UpdateView, DeleteView, DetailView

from docuflow.apps.common import utils
from docuflow.apps.common.enums import Module
from docuflow.apps.common.mixins import ProfileMixin, AddRowMixin, ActiveYearRequiredMixin, DataTableMixin
from docuflow.apps.company.models import PaymentMethod, VatCode
from docuflow.apps.company.modules.paymentmethod.selectors import get_payment_options
from docuflow.apps.customer.models import Customer, Ledger as CustomerLedger
from docuflow.apps.customer.utils import calculate_statement_discount
from docuflow.apps.period.models import Period
from docuflow.apps.sales.exceptions import InsufficientAmount, SalesException
from docuflow.apps.sales.models import Invoice, Receipt, ReceiptPayment, InvoiceReceipt
from . import forms
from . import selectors
from .exceptions import InvalidPeriodAccountingDateException, NotAllocatedAmountException, PaymentException
from .services import delete_receipt

logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=4)


class ReceiptsView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'receipts/index.html'
    context_object_name = 'receipts'

    def get_context_data(self, **kwargs):
        context = super(ReceiptsView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        context['year'] = self.get_year()
        return context


class CreateReceiptsView(LoginRequiredMixin, ActiveYearRequiredMixin, CreateView):
    model = Receipt
    form_class = forms.CreateReceiptForm
    template_name = 'receipts/create.html'

    def get_context_data(self, **kwargs):
        context = super(CreateReceiptsView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def get_initial(self):
        initial = super(CreateReceiptsView, self).get_initial()
        initial['date'] = datetime.now().date()
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        return kwargs

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving receipt',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateReceiptsView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    form.save()

                return JsonResponse({'text': 'Cash sale payment receipts saved successfully',
                                     'error': False,
                                     'redirect': reverse('receipts_index')
                                     })
            except PaymentException as e:
                return JsonResponse({'text': str(e),
                                     'error': True,
                                     })
        return super(CreateReceiptsView, self).form_invalid(form)


class OpenDocumentMixin(object):

    # noinspection PyMethodMayBeStatic
    def get_open_invoices(self, customer_id, receipt_date):
        invoices = Invoice.objects.not_paid().customer(customer_id)
        receipt_invoices = {}

        for invoice in invoices:
            discount = calculate_statement_discount(invoice.customer, invoice.open_amount, invoice.due_date,
                                                    receipt_date)
            discounted_amount = invoice.open_amount - discount
            receipt_invoices[invoice] = {'discount': discount, 'amount': discounted_amount}
        return receipt_invoices

    # noinspection PyMethodMayBeStatic
    def get_open_ledgers(self, customer_id, year):
        ledgers = CustomerLedger.objects.with_available_balance().for_customer(customer_id).open().select_related(
            'journal_line', 'journal_line__journal'
        ).filter(
            period__period_year=year
        )
        receipt_ledgers = {}
        for ledger in ledgers:
            receipt_ledgers[ledger] = {'discount': 0, 'amount': (ledger.balance if ledger.balance else 0)}
        return receipt_ledgers

    # noinspection PyMethodMayBeStatic
    def get_open_receipts(self, customer_id, exclude_ids=None):
        if not exclude_ids:
            exclude_ids = []
        return Receipt.objects.open().filter(customer_id=customer_id).exclude(id__in=exclude_ids)


class CustomerInvoicesView(ProfileMixin, OpenDocumentMixin, TemplateView):
    template_name = 'receipts/open_invoices.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        receipt_date = datetime.strptime(self.request.GET.get('receipt_date'), '%Y-%m-%d').date()
        context['receipt_date'] = receipt_date
        context['row_id'] = self.request.GET.get('row_id', 0)
        context['invoices'] = self.get_open_invoices(customer_id=self.request.GET['customer_id'], receipt_date=receipt_date)
        context['ledgers'] = self.get_open_ledgers(customer_id=self.request.GET['customer_id'], year=self.get_year())
        context['year'] = self.get_year()
        context['receipts'] = self.get_open_receipts(self.request.GET['customer_id'])
        return context


class PaymentLineView(ProfileMixin, AddRowMixin, TemplateView):
    template_name = 'receipts/payment_lines.html'

    def get_vat_code(self):
        return VatCode.objects.output().filter(company=self.get_company(), is_default=True).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['row_id'] = self.request.GET.get('row_id', 0)
        row_count = self.get_rows()
        row_id = int(self.request.GET.get('row_id', 0))
        vat_code = self.get_vat_code()
        col_count = 3
        tabs_start = 26
        is_reload = True if self.request.GET.get('reload', 0) == '1' else False
        rows = utils.generate_tabbing_matrix(col_count, row_count, tabs_start, is_reload, row_id)
        context['is_deletable'] = int(row_id) > 0
        context['row_id'] = row_id
        context['default_vat_code'] = vat_code
        context['start'] = 0
        context['rows'] = rows
        context['is_reload'] = is_reload
        context['row_start'] = row_id
        return context


class CreateCashPaymentsView(ProfileMixin, FormView):
    template_name = 'receipts/payments.html'
    form_class = forms.InvoiceReceiptForm

    def get_payment_methods(self):
        return PaymentMethod.objects.filter(company=self.get_company(), modules__contains=Module.SALES)

    def get_invoice(self):
        return Invoice.objects.filter(pk=self.kwargs['invoice_id']).first()

    def get_context_data(self, **kwargs):
        context = super(CreateCashPaymentsView, self).get_context_data(**kwargs)
        context['payment_methods'] = self.get_payment_methods()
        context['invoice'] = self.get_invoice()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving payments',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateCashPaymentsView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    branch = self.get_branch()
                    profile = self.get_profile()
                    invoice = self.get_invoice()

                    content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()

                    receipt = form.save(commit=False)
                    receipt.branch = branch
                    receipt.reference = invoice.__str__()
                    if content_type:
                        receipt.content_type = content_type
                        receipt.object_id = invoice.id
                    receipt.save()

                    if receipt:
                        InvoiceReceipt.objects.create(
                            invoice=invoice,
                            receipt=receipt,
                            amount=invoice.total_amount
                        )
                        open_amount = invoice.calculate_open_amount(invoice.total_amount)
                        invoice.open_amount = open_amount
                        invoice.save()

                        payment_methods = self.request.POST.getlist('payment_method')
                        total_amount = 0
                        invoice_total = float(invoice.total_amount)
                        for payment_method_id in payment_methods:
                            amount = self.request.POST.get('amount_{}'.format(payment_method_id), 0)
                            if amount:
                                amount = float(amount)
                                receipt_payment = ReceiptPayment.objects.create(
                                    amount=amount,
                                    receipt=receipt,
                                    payment_method_id=payment_method_id
                                )
                                total_amount += amount
                        print("Total amount --> {} vs  {}".format(total_amount, invoice_total))
                        if total_amount != invoice_total:
                            raise InsufficientAmount('Please enter all the payment amount, please fix')

                        pp.pprint(self.request.POST)
                        pp.pprint(self.request.POST['payment_method'])
                        print(self.request.POST.getlist('amounts'))

                    return JsonResponse({'text': 'Cash sale payment receipts saved successfully',
                                         'error': False,
                                         'redirect': reverse('sales_documents_index')
                                         })
            except SalesException as exception:
                return JsonResponse({'text': exception.__str__(),
                                     'details': exception.__str__(),
                                     'error': True
                                     })

            except Exception as exception:
                return JsonResponse({'text': 'Unexpected error occurred saving the invoice payments',
                                     'details': exception.__str__(),
                                     'error': True
                                     })


class CreateCustomerInvoicesReceiptView(ProfileMixin, FormView):
    template_name = 'receipts/create.html'
    form_class = forms.InvoiceReceiptForm

    def get_initial(self):
        initial = super(CreateCustomerInvoicesReceiptView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_payment_methods(self):
        return PaymentMethod.objects.filter(company=self.get_company(), modules__contains=Module.SALES.value)

    def get_customer(self):
        return Customer.objects.filter(pk=self.kwargs['customer_id']).first()

    def get_invoices(self):
        invoice_ids = self.request.session.get('customer_invoices', [])
        return Invoice.objects.filter(id__in=invoice_ids)

    def calculate_invoices_total(self, invoices):
        return sum([invoice.outstanding for invoice in invoices if invoice.outstanding])

    def get_context_data(self, **kwargs):
        context = super(CreateCustomerInvoicesReceiptView, self).get_context_data(**kwargs)
        context['payment_methods'] = self.get_payment_methods()
        invoices = self.get_invoices()
        customer = self.get_customer()
        context['invoices'] = invoices
        context['customer'] = customer
        context['total'] = self.calculate_invoices_total(invoices)
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving payments',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateCustomerInvoicesReceiptView, self).form_invalid(form)

    # noinspection PyMethodMayBeStatic
    def add_customer_ledger(self, receipt, customer, invoices_total, profile):
        logger.info("CREATE customer receipt ledger")
        content_type = ContentType.objects.filter(
            app_label='sales',
            model='receipt'
        ).first()
        receipt_date = receipt.date
        period = Period.objects.get_by_date(receipt.branch.company, receipt_date).first()
        amount = invoices_total * -1
        logger.info("Period --> {}, amount -> {}, date -> {}, content {}".format(period, amount, receipt_date,
                                                                                 content_type))
        CustomerLedger.objects.create(
            customer=customer,
            period=period,
            quantity=1,
            amount=amount,
            module=Module.SALES,
            created_by=profile,
            date=receipt_date,
            status=CustomerLedger.PENDING,
            text=receipt.__str__(),
            content_type=content_type,
            object_id=receipt.id
        )
        logger.info("Done creating the ledger-")

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    branch = self.get_branch()
                    profile = self.get_profile()
                    customer = self.get_customer()
                    invoices = self.get_invoices()
                    invoices_total = self.calculate_invoices_total(invoices)

                    content_type = ContentType.objects.filter(
                        app_label='customer',
                        model='customer'
                    ).first()

                    receipt = form.save(commit=False)
                    receipt.amount = receipt.amount * -1
                    receipt.branch = branch
                    receipt.reference = customer.__str__()
                    if content_type:
                        receipt.content_type = content_type
                        receipt.object_id = customer.id
                    receipt.save()

                    if receipt:
                        for invoice in invoices:
                            InvoiceReceipt.objects.create(
                                invoice=invoice,
                                receipt=receipt,
                                amount=invoice.open_amount
                            )

                            open_amount = invoice.calculate_open_amount(invoice.open_amount)
                            invoice.open_amount = open_amount
                            invoice.save()

                        total_amount = 0
                        invoices_total = float(invoices_total)

                        self.add_customer_ledger(receipt, customer, invoices_total, profile)

                        payment_methods = self.request.POST.getlist('payment_method')

                        for payment_method_id in payment_methods:
                            amount = self.request.POST.get('amount_{}'.format(payment_method_id), 0)
                            if amount:
                                amount = float(amount)
                                receipt_payment = ReceiptPayment.objects.create(
                                    amount=amount,
                                    receipt=receipt,
                                    payment_method_id=payment_method_id
                                )
                                total_amount += amount
                        logger.info("Total amount --> {} vs  {}".format(total_amount, invoices_total))
                        if total_amount != invoices_total:
                            raise InsufficientAmount('Please enter all the payment amount, please fix')

                        logger.info(self.request.POST)
                        logger.info(self.request.POST['payment_method'])
                        logger.info(self.request.POST.getlist('amounts'))

                    return JsonResponse({'text': 'Cash sale payment receipts saved successfully',
                                         'error': False,
                                         'redirect': reverse('sales_documents_index')
                                         })
            except SalesException as exception:
                return JsonResponse({'text': exception.__str__(),
                                     'details': exception.__str__(),
                                     'error': True
                                     })

            except Exception as exception:
                return JsonResponse({'text': 'Unexpected error occurred saving the invoice payments',
                                     'details': exception.__str__(),
                                     'error': True
                                     })


class EditReceiptView(ProfileMixin, OpenDocumentMixin, UpdateView):
    template_name = 'receipts/edit.html'
    form_class = forms.CreateReceiptForm
    context_object_name = 'receipt'
    model = Receipt
    success_url = reverse_lazy('receipts_index')

    def get_form_kwargs(self):
        kwargs = super(EditReceiptView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['request'] = self.request
        kwargs['profile'] = self.get_profile()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditReceiptView, self).get_context_data(**kwargs)
        receipt = self.get_object()
        context['invoices'] = self.get_open_invoices(receipt.customer_id, receipt.date)
        context['ledgers'] = self.get_open_ledgers(customer_id=receipt.customer_id, year=self.get_year())
        context['receipts'] = self.get_open_receipts(customer_id=receipt.customer_id, exclude_ids=[receipt.id])
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving receipt',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(EditReceiptView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    form.save(commit=False)

                    return JsonResponse({'text': 'Receipts updated successfully',
                                         'error': False,
                                         'redirect': reverse('receipts_index')
                                         })
            except SalesException as exception:
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True
                                     })


class DeleteReceiptView(ProfileMixin, LoginRequiredMixin, DeleteView):
    model = Receipt

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the success URL.
        """
        with transaction.atomic():
            receipt = Receipt.objects.get(pk=self.kwargs['pk'])
            delete_receipt(receipt=receipt)

        return JsonResponse({
            'text': 'Receipt successfully deleted.',
            'error': False,
            'reload': True
        })


class AllocateView(ProfileMixin, FormView):
    template_name = 'receipts/allocate.html'

    def get_initial(self):
        initial = super(AllocateView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_class(self):
        return forms.AllocateReceiptForm

    def get_form_kwargs(self):
        kwargs = super(AllocateView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AllocateView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred creating the sundry payment, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(AllocateView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            try:
                company = self.get_company()
                branch = self.get_branch()
                profile = self.get_profile()
                # parent_payment.payment_date = datetime.now().date()
                allocate_sundry = AllocateSundryPayment(company, branch, profile, self.get_role(), form, self.request.POST)
                receipt = allocate_sundry.execute()

                kwargs = {'pk': receipt.id}
                redirect_url = "{}?redirect_url={}".format(reverse('receipt_detail', kwargs=kwargs),
                                                           reverse('sundry_payment'))
                return JsonResponse({'error': False,
                                     'text': 'Sundry payment allocated successfully',
                                     'redirect': redirect_url
                                     })
            except NotAllocatedAmountException as ex:
                return JsonResponse({'error': True,
                                     'text': ex.__str__(),
                                     'details': 'Error occurred {}'.format(ex.__str__()),
                                     'url': reverse('sundry_payment_not_allocated')
                                     })
            except InvalidPeriodAccountingDateException as ex:
                return JsonResponse({'error': True,
                                     'text': ex.__str__(),
                                     'details': 'Error occurred {}'.format(ex.__str__()),
                                     'errors': {'period': ['Period and payment date do not match']}
                                     })
            except Exception as exception:
                return JsonResponse({'error': True,
                                     'text': 'An unexpected error occurred, please try again',
                                     'details': 'Error saving the invoice {}'.format(exception.__str__()),
                                     'alert': True
                                     })
        # return HttpResponse('Not allowed')


class AllocateDocumentsView(ProfileMixin, OpenDocumentMixin, TemplateView):
    template_name = 'receipts/allocate_documents.html'

    def get_requested_customer(self):
        return self.request.GET.get('customer_id', None)

    def get_receipt_date(self):
        return datetime.strptime(self.request.GET.get('receipt_date', None), '%Y-%m-%d').date()

    # def get_ledger_payments(self):
    #     journal_filters = {}
    #     journal_filters['customer_id'] = self.get_requested_customer()
    #     logger.info("Ledger payments")
    #     logger.info(journal_filters)
    #     general_ledgers = CustomerLedger.objects.with_available_payments(journal_filters)
    #     logger.info(general_ledgers)
    #     return general_ledgers

    # def get_payments(self, branch):
    #     journal_filters = {'branch': branch}
    #     journal_filters['customer_id'] = self.get_requested_customer()
    #     journal_filters['receipt_type'] = ReceiptType.SUNDRY
    #     journal_filters['balance__gt'] = 0
    #     return Receipt.objects.filter(**journal_filters)

    def get_context_data(self, **kwargs):
        context = super(AllocateDocumentsView, self).get_context_data(**kwargs)
        company = self.get_company()
        receipt_date = self.get_receipt_date()
        logger.info(receipt_date)
        context['company'] = company
        context['payments'] = self.get_open_receipts(self.request.GET.get('customer_id'))
        context['ledger_payments'] = self.get_open_ledgers(self.request.GET.get('customer_id'))
        context['invoices'] = self.get_open_invoices(self.request.GET.get('customer_id'), receipt_date)
        return context


class CreateInvoicesReceiptView(ProfileMixin, FormView):
    template_name = 'receipts/create_invoices_payment.html'
    form_class = forms.InvoiceReceiptForm

    def get_payment_methods(self):
        return PaymentMethod.objects.filter(company=self.get_company(), modules__contains=Module.SALES.value)

    def get_invoices(self):
        invoice_ids = self.request.session.get('invoices', [])
        return Invoice.objects.filter(id__in=invoice_ids)

    def calculate_invoices_total(self, invoices):
        return sum([invoice.outstanding for invoice in invoices if invoice.outstanding])

    def get_context_data(self, **kwargs):
        context = super(CreateInvoicesReceiptView, self).get_context_data(**kwargs)
        context['payment_methods'] = self.get_payment_methods()
        invoices = self.get_invoices()
        context['invoices'] = invoices
        context['total'] = self.calculate_invoices_total(invoices)
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving payments',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateInvoicesReceiptView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    branch = self.get_branch()
                    profile = self.get_profile()
                    invoices = self.get_invoices()
                    invoices_total = self.calculate_invoices_total(invoices)

                    content_type = ContentType.objects.filter(app_label='sales', model='invoice').first()

                    receipt = form.save(commit=False)
                    receipt.branch = branch
                    receipt.save()

                    if receipt:
                        for invoice in invoices:
                            InvoiceReceipt.objects.create(
                                invoice=invoice,
                                receipt=receipt,
                                amount=invoice.open_amount
                            )

                            open_amount = invoice.calculate_open_amount(invoice.open_amount)
                            invoice.open_amount = open_amount
                            invoice.save()

                        payment_methods = self.request.POST.getlist('payment_method')
                        total_amount = 0
                        invoices_total = float(invoices_total)
                        for payment_method_id in payment_methods:
                            amount = self.request.POST.get('amount_{}'.format(payment_method_id), 0)
                            if amount:
                                amount = float(amount)
                                receipt_payment = ReceiptPayment.objects.create(
                                    amount=amount,
                                    receipt=receipt,
                                    payment_method_id=payment_method_id
                                )
                                total_amount += amount
                        print("Total amount --> {} vs  {}".format(total_amount, invoices_total))
                        if total_amount != invoices_total:
                            raise InsufficientAmount('Please enter all the payment amount, please fix')

                        pp.pprint(self.request.POST)
                        pp.pprint(self.request.POST['payment_method'])
                        print(self.request.POST.getlist('amounts'))

                    return JsonResponse({'text': 'Cash sale payment receipts saved successfully',
                                         'error': False,
                                         'redirect': reverse('sales_documents_index')
                                         })
            except SalesException as exception:
                return JsonResponse({'text': exception.__str__(),
                                     'details': exception.__str__(),
                                     'error': True
                                     })

            except Exception as exception:
                return JsonResponse({'text': 'Unexpected error occurred saving the invoice payments',
                                     'details': exception.__str__(),
                                     'error': True
                                     })


class ReceiptDetailView(LoginRequiredMixin, DetailView):
    model = Receipt
    context_object_name = 'receipt'
    template_name = 'receipts/detail.html'

    def get_template_names(self):
        if self.request.GET.get('print') == '1':
            return 'receipts/print.html'
        return 'receipts/detail.html'

    def get_queryset(self):
        return selectors.get_receipt_with_totals()

    def get_context_data(self, **kwargs):
        context = super(ReceiptDetailView, self).get_context_data(**kwargs)
        context['discount_account'] = self.get_object().branch.company.default_discount_allowed_account
        context['is_print'] = self.request.GET.get('print') == '1'
        return context


class ViewInvoicePaymentsView(LoginRequiredMixin, TemplateView):
    template_name = 'receipts/invoice_receipt.html'

    def get_invoice(self):
        return Invoice.objects.filter(id=self.kwargs['invoice_id']).first()

    def get_context_data(self, **kwargs):
        context = super(ViewInvoicePaymentsView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        context['receipt'] = invoice.receipt
        context['invoice'] = invoice
        return context


class CashInvoiceReceiptMixin(object):

    # noinspection PyUnresolvedReferences
    def get_payment_methods(self):
        return PaymentMethod.objects.sales().filter(company=self.get_company())

    # noinspection PyUnresolvedReferences
    def get_invoice_amount(self):
        return Decimal(self.request.GET.get('amount', 0))

    # noinspection PyUnresolvedReferences
    def get_customer_pending_receipts(self):
        return Receipt.objects.open().filter(
            customer_id=self.request.GET.get('customer_id')
        )


class CreateCashInvoiceReceiptView(LoginRequiredMixin, ProfileMixin, CashInvoiceReceiptMixin, FormView):
    template_name = 'receipts/create_cash_invoice_receipt.html'
    form_class = forms.CashReceiptForm

    def get_initial(self):
        initial = super(CreateCashInvoiceReceiptView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        initial['amount'] = self.get_invoice_amount()
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['company'] = self.get_company()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateCashInvoiceReceiptView, self).get_context_data(**kwargs)
        context['payment_methods'] = self.get_payment_methods()
        context['total'] = self.get_invoice_amount()
        context['invoice_type'] = self.request.GET.get('invoice_type', 'tax-invoice')
        context['receipts'] = self.get_customer_pending_receipts()
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving payments',
                                 'errors': form.errors
                                 }, status=400)
        return super(CreateCashInvoiceReceiptView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            with transaction.atomic():

                form.save()

                return JsonResponse({'text': 'Cash payment receipts created ...',
                                     'error': False
                                     })
        return super(CreateCashInvoiceReceiptView, self).form_valid(form)


class CreateInvoiceReceiptView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'receipts/create_invoice_receipt.html'
    form_class = forms.ReceiptForm

    def get_initial(self):
        initial = super(CreateInvoiceReceiptView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['invoice'] = self.get_invoice()
        kwargs['request'] = self.request
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_payment_methods(self):
        return PaymentMethod.objects.filter(company=self.get_company(), modules__contains=Module.SALES.value)

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(CreateInvoiceReceiptView, self).get_context_data(**kwargs)
        context['payment_methods'] = self.get_payment_methods()
        context['invoice'] = self.get_invoice()
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving payments',
                                 'errors': form.errors
                                 })
        return super(CreateInvoiceReceiptView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():

                    form.save()

                    return JsonResponse({'text': 'Cash sale payment receipts saved successfully',
                                         'error': False,
                                         'redirect': reverse('sales_documents_index')
                                         })
            except SalesException as exception:
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True
                                     })
        return super(CreateInvoiceReceiptView, self).form_valid(form)


class EditInvoiceReceiptView(LoginRequiredMixin, ProfileMixin, CashInvoiceReceiptMixin, UpdateView):
    template_name = 'receipts/edit_invoice_receipt.html'
    form_class = forms.ReceiptForm
    model = Receipt

    def get_invoice_receipt(self):
        return InvoiceReceipt.objects.get(invoice=self.get_invoice())

    def get_object(self, queryset=None):
        return self.get_invoice_receipt().receipt

    def get_initial(self):
        initial = super(EditInvoiceReceiptView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['invoice'] = self.get_invoice()
        kwargs['request'] = self.request
        kwargs['branch'] = self.get_branch()
        return kwargs

    def get_invoice_receipts(self, invoice):
        payments = {}
        for receipt_payment in ReceiptPayment.objects.filter(receipt=self.get_object()):
            payments[receipt_payment.payment_method_id] = receipt_payment
        return payments

    def get_invoice(self):
        return Invoice.objects.get(pk=self.kwargs['pk'])

    def get_invoice_amount(self):
        return Decimal(self.request.GET.get('amount', 0))

    def get_context_data(self, **kwargs):
        context = super(EditInvoiceReceiptView, self).get_context_data(**kwargs)
        invoice = self.get_invoice()
        payments = self.get_invoice_receipts(invoice)
        context['payment_methods'] = get_payment_options(self.get_company(), payments)
        context['invoice'] = invoice
        context['payments'] = payments
        context['total'] = self.get_invoice_amount()
        context['receipts'] = self.get_customer_pending_receipts()
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving payments',
                                 'errors': form.errors
                                 })
        return super(EditInvoiceReceiptView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():

                    form.save()

                    return JsonResponse({'text': 'Cash sale payment receipts saved successfully',
                                         'error': False,
                                         # 'redirect': reverse('sales_documents_index')
                                         })
            except SalesException as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True
                                     }, status=500)
        return super(EditInvoiceReceiptView, self).form_valid(form)


class CashInvoiceReceiptFormView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'receipts/invoice_receipt_form.html'
    form_class = forms.InvoiceReceiptForm

    def get_initial(self):
        initial = super(CashInvoiceReceiptFormView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_payment_methods(self):
        return PaymentMethod.objects.filter(company=self.get_company(), modules__contains=Module.SALES.value)

    def get_invoice_amount(self):
        return Decimal(float(self.request.GET.get('amount', 0)))

    def get_context_data(self, **kwargs):
        context = super(CashInvoiceReceiptFormView, self).get_context_data(**kwargs)
        context['payment_methods'] = self.get_payment_methods()
        context['total'] = self.get_invoice_amount()
        return context


class ReceiptDocumentView(TemplateView):
    template_name = 'receipts/view.html'

    def get_receipt(self, receipt_id):
        return Receipt.objects.select_related(
            'customer', 'branch__company'
        ).prefetch_related(
            'customer__addresses', 'invoices', 'invoices__invoice', 'allocations', 'invoices__invoice',
            'allocations__receipt', 'customer_ledgers', 'accounts', 'payments'
        ).filter(
            id=receipt_id
        ).first()

    def get_context_data(self, **kwargs):
        context = super(ReceiptDocumentView, self).get_context_data(**kwargs)
        data = signing.loads(self.kwargs['signature'])
        receipt = self.get_receipt(data['document_id'])
        context['receipt'] = receipt
        context['discount_account'] = receipt.branch.company.default_discount_allowed_account
        return context
