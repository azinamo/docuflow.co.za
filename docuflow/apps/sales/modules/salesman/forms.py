from django import forms
from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.customer.models import Customer
from docuflow.apps.sales.models import Salesman, Tracking


class SalesmanForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        super(SalesmanForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Salesman
        fields = ('code', 'name')

    def save(self, commit=True):
        salesman = super(SalesmanForm, self).save(commit=False)
        salesman.company = self.company
        salesman.save()


class TransferSalesmanForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.profile = kwargs.pop('profile')
        self.role = kwargs.pop('role')
        super(TransferSalesmanForm, self).__init__(*args, **kwargs)
        salesmans = Salesman.objects.filter(company=self.company)
        self.fields['from_salesman'].queryset = salesmans
        self.fields['to_salesman'].queryset = salesmans
        self.fields['customers'].queryset = Customer.objects.filter(company=self.company)

    from_salesman = forms.ModelChoiceField(label='From', queryset=None, required=True)
    to_salesman = forms.ModelChoiceField(label='To', queryset=None, required=True)
    customers = forms.ModelMultipleChoiceField(queryset=None)

    def clean(self):
        cleaned_data = super(TransferSalesmanForm, self).clean()
        print(cleaned_data)
        print(cleaned_data.get('to_salesman'))
        print(not cleaned_data.get('to_salesman'))
        if not cleaned_data.get('to_salesman'):
            self.add_error('to_salesman', 'To is required')
        if not cleaned_data.get('from_salesman'):
            self.add_error('from_salesman', 'From is required')

        if cleaned_data.get('from_salesman') == cleaned_data.get('to_salesman'):
            self.add_error('from_salesman', 'From cannot be the same as to')
            self.add_error('to_salesman', 'To cannot be the same as from')
        return cleaned_data

    def execute(self):
        to_salesman = self.cleaned_data['to_salesman']
        customers = self.cleaned_data['customers']
        for customer in customers:
            customer.salesman = to_salesman
            customer.save()
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(to_salesman),
            object_id=to_salesman.id,
            comment=f"Customers transferred to {to_salesman} from {self.cleaned_data['from_salesman']}",
            profile=self.profile,
            role=self.role,
            data={
                'customers': [(customer.id, customer.name) for customer in customers]
            }
        )
