from django.apps import AppConfig


class SalesmanConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.salesman'
