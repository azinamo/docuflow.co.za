import logging
import pprint

from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, View, FormView
from django.contrib.messages.views import SuccessMessageMixin, messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponseRedirect

from docuflow.apps.common.mixins import ProfileMixin
from docuflow.apps.customer.models import Customer
from docuflow.apps.sales.models import Salesman
from .forms import SalesmanForm, TransferSalesmanForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class IndexView(LoginRequiredMixin, ProfileMixin, ListView):
    template_name = 'salesman/index.html'
    context_object_name = 'salesmans'

    def get_queryset(self):
        return Salesman.objects.select_related('company').filter(company=self.get_company())

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context


class CreateSalesmanView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, CreateView):
    model = Salesman
    form_class = SalesmanForm
    template_name = 'salesman/create.html'
    success_message = 'Salesman successfully saved'
    success_url = reverse_lazy('salesman_index')

    def get_context_data(self, **kwargs):
        context = super(CreateSalesmanView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateSalesmanView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the salesman, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateSalesmanView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            form.save()
            return JsonResponse({'error': False,
                                 'text': 'Salesman saved successfully',
                                 'reload': True
                                 })
        return HttpResponseRedirect(reverse_lazy('salesman_index'))


class EditSalesmanView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, UpdateView):
    model = Salesman
    form_class = SalesmanForm
    success_url = reverse_lazy('salesman_index')
    template_name = 'salesman/edit.html'
    success_message = 'Salesman successfully saved'

    def get_context_data(self, **kwargs):
        context = super(EditSalesmanView, self).get_context_data(**kwargs)
        context['company'] = self.get_company()
        return context

    def get_initial(self):
        initial = super(EditSalesmanView, self).get_initial()
        salesman = self.get_object()
        initial['code'] = salesman.code
        initial['name'] = salesman.name
        return initial

    def get_form_kwargs(self):
        kwargs = super(EditSalesmanView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the salesman, please fix the errors.',
                                 'errors': form.errors
                                 })
        return super(EditSalesmanView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            form.save()

            return JsonResponse({'error': False,
                                 'text': 'Salesman updated successfully',
                                 'reload': True
                                 })
        return HttpResponseRedirect(reverse_lazy('salesman_index'))


class DeleteSalesmanView(LoginRequiredMixin, SuccessMessageMixin, ProfileMixin, View):
    success_message = 'Salesman successfully saved'

    def get(self, request, **kwargs):
        salesman = Salesman.objects.get(pk=kwargs['pk'])
        salesman.delete()
        messages.success(request, 'Salesman deleted successfully')
        return HttpResponseRedirect(reverse_lazy('salesman_index'))


class TransferSalesmanView(ProfileMixin, FormView):
    form_class = TransferSalesmanForm
    template_name = 'salesman/transfer.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred transferring the salesman customers, please fix the errors.',
                                 'errors': form.errors
                                 }, status=400)
        return super(TransferSalesmanView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            form.execute()
            return JsonResponse({
                'text': 'Salesman customers successfully transferred',
                'error': False,
                'reload': True
            })


class SalesmanCustomersView(ProfileMixin, TemplateView):
    template_name = 'salesman/customers.html'

    def get_customers(self):
        return Customer.objects.filter(salesman_id=int(self.request.GET['salesman_id']))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customers'] = self.get_customers()
        return context


