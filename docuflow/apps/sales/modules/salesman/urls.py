from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='salesman_index'),
    path('create/', views.CreateSalesmanView.as_view(), name='create_salesman'),
    path('<int:pk>/edit/', views.EditSalesmanView.as_view(), name='edit_salesman'),
    path('<int:pk>/delete/', views.DeleteSalesmanView.as_view(), name='delete_salesman'),
    path('transfer/', views.TransferSalesmanView.as_view(), name='transfer_salesman'),
    path('customers/', views.SalesmanCustomersView.as_view(), name='salesman_customers'),
]