from django import forms
from django.urls import reverse_lazy

from docuflow.apps.common.widgets import DataAttributesSelect
from docuflow.apps.customer.models import Customer
from docuflow.apps.period.models import Period
from docuflow.apps.sales.models import Delivery


class DeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        super(DeliveryForm, self).__init__(*args, **kwargs)

        customer_data = {'data-detail-url': {'': ''},
                         'data-payment-term': {'': ''},
                         'data-payment-days': {'': ''}
                         }

        customers = Customer.objects.invoicable().filter(company=company).all()
        for c in customers:
            customer_data['data-detail-url'][c.id] = reverse_lazy('customer_details', kwargs={'pk': c.id})
            customer_data['data-payment-term'][c.id] = c.payment_term
            customer_data['data-payment-days'][c.id] = c.days

        self.fields['period'].queryset = Period.objects.open(company, year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=company)

        self.fields['customer'].widget = DataAttributesSelect(
            choices=[('', '-----------')] + [(c.id, str(c)) for c in customers], data=customer_data)

    class Meta:
        model = Delivery
        fields = ('branch',  'period', 'date', 'customer', 'delivery_time', 'purchase_order', 'delivery_method',
                  'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount', 'customer_discount',
                  'line_discount')
        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(DeliveryForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        return cleaned_data


class EstimateDeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        super(EstimateDeliveryForm, self).__init__(*args, **kwargs)

        customer_data = {'data-detail-url': {'': ''},
                         'data-payment-term': {'': ''},
                         'data-payment-days': {'': ''}
                         }

        customers = Customer.objects.filter(company=company).all()
        for c in customers:
            customer_data['data-detail-url'][c.id] = reverse_lazy('customer_details', kwargs={'pk': c.id})
            customer_data['data-payment-term'][c.id] = c.payment_term
            customer_data['data-payment-days'][c.id] = c.days

        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=company)
        self.fields['customer'].widget = DataAttributesSelect(
            choices=[('', '-----------')] + [(c.id, str(c)) for c in customers], data=customer_data)

    class Meta:
        model = Delivery
        fields = ('branch',  'date', 'customer', 'estimate', 'delivery_time', 'purchase_order', 'delivery_method',
                  'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount', 'customer_discount',
                  'line_discount', 'customer_name')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput(),
            'customer_name': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(EstimateDeliveryForm, self).clean()

        delivery_method = cleaned_data['delivery_method']
        if not delivery_method:
            self.add_error('delivery_method', 'Delivery method is required')

        return cleaned_data


class PickingSlipDeliveryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        year = kwargs.pop('year')
        super(PickingSlipDeliveryForm, self).__init__(*args, **kwargs)

        customer_data = {'data-detail-url': {'': ''},
                         'data-payment-term': {'': ''},
                         'data-payment-days': {'': ''}
                         }

        customers = Customer.objects.invoicable().filter(company=company).all()
        for c in customers:
            customer_data['data-detail-url'][c.id] = reverse_lazy('customer_details', kwargs={'pk': c.id})
            customer_data['data-payment-term'][c.id] = c.payment_term
            customer_data['data-payment-days'][c.id] = c.days
        self.fields['period'].queryset = Period.objects.open(company, year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['branch'].queryset = self.fields['branch'].queryset.filter(company=company)
        self.fields['customer'].widget = DataAttributesSelect(
            choices=[('', '-----------')] + [(c.id, str(c)) for c in customers], data=customer_data)

    class Meta:
        model = Delivery
        fields = ('branch',  'period', 'date', 'customer', 'delivery_time', 'purchase_order', 'delivery_method',
                  'net_amount', 'total_amount', 'vat_amount', 'sub_total', 'discount_amount', 'customer_discount',
                  'line_discount')

        widgets = {
            'reference': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
            'branch': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'customer': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'net_amount': forms.HiddenInput(),
            'total_amount': forms.HiddenInput(),
            'vat_amount': forms.HiddenInput(),
            'sub_total': forms.HiddenInput(),
            'discount_amount': forms.HiddenInput(),
            'customer_discount': forms.HiddenInput(),
            'line_discount': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(PickingSlipDeliveryForm, self).clean()

        delivery_method = cleaned_data['delivery_method']
        if not delivery_method:
            self.add_error('delivery_method', 'Delivery method is required')
        return cleaned_data


class PickingSlipForm(forms.Form):
    pass
