import logging
import pprint
from collections import defaultdict
from datetime import datetime
from decimal import Decimal

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.fields import ContentType
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import FormView, View, DetailView, TemplateView

from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.inventory.models import BranchInventory
from docuflow.apps.sales.enums import InvoiceItemType
from docuflow.apps.sales.models import DeliveryItem, Estimate, BackOrder, Tracking
from . import services
from .forms import DeliveryForm, EstimateDeliveryForm

pp = pprint.PrettyPrinter(indent=4)


logger = logging.getLogger(__name__)


class IndexView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'backorder/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['year'] = self.get_year()
        context['branch'] = self.get_branch()
        return context


class CreateDeliveryBackOrderView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'deliveries/estimate/create.html'
    form_class = EstimateDeliveryForm

    def get_initial(self):
        initial = super(CreateDeliveryBackOrderView, self).get_initial()
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateDeliveryBackOrderView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateDeliveryBackOrderView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        context['cols'] = 12
        context['default_vat'] = company.vat_percentage
        context['profile'] = self.get_profile()
        context['date'] = datetime.now()
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            print("Validate -> Form data")
            print(form.cleaned_data)
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the sale invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 })
        return super(CreateDeliveryBackOrderView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            # try:
            with transaction.atomic():
                print("POSTING")
                company = self.get_company()
                profile = self.get_profile()
                estimate = self.get_estimate()

                content_type = ContentType.objects.filter(app_label='sales', model='customerestimate').first()

                delivery = form.save(commit=False)
                delivery.profile = profile
                delivery.estimate = estimate
                delivery.period = estimate.period
                delivery.content_type = content_type
                delivery.object_id = estimate.id
                delivery.vat_on_customer_discount = delivery.cal_vat_on_discount(delivery.customer_discount)
                delivery.role_id = self.request.session['role']
                delivery.save()

                if delivery:
                    postal_address = {}
                    delivery_address = {}
                    for k, v in self.request.POST.items():
                        if v != '':
                            if k[0:7] == 'postal_':
                                postal_key = k[7:]
                                postal_address[postal_key] = v
                            if k[0:9] == 'delivery_':
                                delivery_key = k[9:]
                                delivery_address[delivery_key] = v

                    delivery.save_address(delivery_address, Address.DELIVERY)
                    delivery.save_address(postal_address, Address.POSTAL)

                    estimate_items = estimate.estimate_items.all()
                    delivery.create_delivery_items_from_estimates(estimate_items, self.request.POST)
                    if delivery.is_picking_slip_required:
                        delivery.created_picking_slip()
                    estimate.status = Estimate.COMPLETED
                    estimate.save()

                return JsonResponse({'text': 'Estimate delivery saved successfully',
                                     'error': False,
                                     'redirect': reverse('sales_delivery_index')
                                     })


class CreateEstimateBackOrderView(ProfileMixin, LoginRequiredMixin, FormView):
    template_name = 'deliveries/create.html'
    form_class = DeliveryForm

    def get_initial(self):
        initial = super(CreateEstimateBackOrderView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        initial['branch'] = self.get_branch()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateEstimateBackOrderView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateEstimateBackOrderView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        context['branch'] = self.get_branch()
        context['profile'] = self.get_profile()
        context['date'] = datetime.now()
        context['default_vat'] = company.vat_percentage
        context['cols'] = 12
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({'error': True,
                                 'text': 'Error occurred saving the sale invoice, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CreateEstimateBackOrderView, self).form_invalid(form)

    def add_inventory_delivery(self, delivery, row_id):
        inventory_item_id = self.request.POST.get('inventory_item_{}'.format(row_id), None)
        logger.info("Row {} inventory id is {} of type {}".format(row_id, inventory_item_id, type(inventory_item_id)))
        logger.info((inventory_item_id or inventory_item_id != ""))
        logger.info("------------ Innnnn, chappinda ----------------")
        inventory_item = BranchInventory.objects.filter(id=inventory_item_id).first()
        logger.info("add inventory estimate {}  for row {} for inventory {}".format(delivery, row_id, inventory_item))

        if not inventory_item:
            raise ValueError('Inventory does not exits')

        ordered = float(self.request.POST.get('ordered_{}'.format(row_id), 0))
        quantity = float(self.request.POST.get('quantity_{}'.format(row_id), 0))
        price = float(self.request.POST.get('price_{}'.format(row_id)))
        discount = float(self.request.POST.get('discount_percentage_{}'.format(row_id), 0))
        vat_code_id = self.request.POST.get('vat_code_{}'.format(row_id), None)
        vat = float(self.request.POST.get('vat_percentage_{}'.format(row_id), 0))
        net_price = float(self.request.POST.get('total_price_excluding_{}'.format(row_id), 0))
        unit_id = self.request.POST.get('unit_id_{}'.format(row_id), None)
        back_order = self.request.POST.get('back_order_{}'.format(row_id), False)
        sub_total = float(self.request.POST.get('sub_total_{}'.format(row_id), 0))
        vat_amount = float(self.request.POST.get('total_vat_{}'.format(row_id), 0))
        discount_amount = float(self.request.POST.get('total_discount_{}'.format(row_id), 0))
        total_amount = float(self.request.POST.get('total_price_including_{}'.format(row_id), 0))
        logger.info("is back order {} of type ".format(back_order, type(back_order)))
        delivery_item = DeliveryItem.objects.create(
            delivery=delivery,
            inventory=inventory_item,
            price=price,
            ordered=ordered,
            quantity=quantity,
            discount=discount,
            vat_code_id=vat_code_id,
            unit_id=unit_id,
            vat=vat,
            net_price=net_price,
            delivery_type=DeliveryItem.INVENTORY
        )
        logger.info("Delivery item saved {}".format(delivery_item))
        if delivery_item:
            inventory_stock = BranchInventory.objects.filter(id=inventory_item.id).first()
            if not inventory_stock:
                inventory_stock.inventory = inventory_item
            if inventory_stock.on_delivery:
                inventory_stock.on_delivery += Decimal(quantity)
            else:
                inventory_stock.on_delivery = Decimal(quantity)
            inventory_stock.save()
        logger.info("Finished updating the inventory on delivery --> ".format(delivery_item))
        back_order_item = {'item': None, 'quantity': 0}

        logger.info("Back order {}".format(back_order))
        if back_order:
            back_order_qty = ordered - quantity
            back_order_item = {'item': delivery_item, 'quantity': back_order_qty}

        return delivery_item, back_order_item

    def add_general_delivery_item(self, delivery, row_id):
        description = self.request.POST.get('item_description_{}'.format(row_id))
        if description:
            logger.info("add generaal inventory delivery {}  for row {} for inventory {}".format(delivery, row_id, description))
            quantity = float(self.request.POST.get('quantity_{}'.format(row_id), 0))
            price = float(self.request.POST.get('price_{}'.format(row_id), 0))
            discount = float(self.request.POST.get('discount_percentage_{}'.format(row_id), 0))
            vat_code_id = self.request.POST.get('vat_code_{}'.format(row_id), None)
            vat = float(self.request.POST.get('vat_percentage_{}'.format(row_id), 0))
            net_price = float(self.request.POST.get('total_price_excluding_{}'.format(row_id), 0))
            unit_id = self.request.POST.get('unit_id_{}'.format(row_id), None)

            sub_total = float(self.request.POST.get('sub_total_{}'.format(row_id), 0))
            vat_amount = float(self.request.POST.get('total_vat_{}'.format(row_id), 0))
            discount_amount = float(self.request.POST.get('total_discount_{}'.format(row_id), 0))
            total_amount = float(self.request.POST.get('total_price_including_{}'.format(row_id), 0))

            delivery_item = DeliveryItem.objects.create(
                delivery=delivery,
                description=description,
                price=price,
                net_price=net_price,
                quantity=quantity,
                unit_id=unit_id,
                vat_code_id=vat_code_id,
                vat=vat,
                discount=discount,
                discount_amount=discount_amount,
                delivery_type=DeliveryItem.TEXT
            )
            return delivery_item

    def save_delivery_items(self, delivery, is_edit=False):
        back_order_items = []
        for field, value in self.request.POST.items():
            if field.startswith('inventory_item', 0, 14):
                row_id = field[15:]
                inventory_item_id = self.request.POST.get('inventory_item_{}'.format(row_id), None)
                if inventory_item_id or inventory_item_id != "":
                    delivery_item, back_order_item = self.add_inventory_delivery(delivery, row_id)
                    back_order_items.append(back_order_item)
            elif field.startswith('item_description', 0, 16):
                row_id = field[17:]
                delivery_item = self.add_general_delivery_item(delivery, row_id)
        logger.info("Done saving delivery items")
        if delivery.customer:
            delivery.vat_on_customer_discount = delivery.cal_vat_on_discount(delivery.customer_discount)
        delivery.save()

        logger.info("Back order items")
        logger.info(back_order_items)

        if len(back_order_items) > 0:
            delivery.created_back_order_delivery(back_order_items)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            try:
                with transaction.atomic():
                    company = self.get_company()
                    profile = self.get_profile()

                    delivery = form.save(commit=False)
                    delivery.profile = profile
                    # invoice.category = self.get_invoice_category()
                    delivery.role_id = self.request.session['role']
                    delivery.save()

                    if delivery:
                        self.save_delivery_items(delivery)
                        if delivery.is_picking_slip_required:
                            delivery.created_picking_slip()
                            delivery.save()

                    return JsonResponse({'text': 'Sales delivery saved successfully',
                                         'error': False,
                                         'redirect': reverse('sales_delivery_index')
                                         })

            except Exception as exception:
                return JsonResponse({'text': 'Unexpected error occurred saving the delivery',
                                     'details': exception.__str__(),
                                     'error': True
                                     })


class BackOrderDetailView(LoginRequiredMixin, ProfileMixin, DetailView):
    model = BackOrder
    context_object_name = 'back_order'

    def is_print(self):
        return self.request.GET.get('print') == '1'

    def get_template_names(self):
        if self.is_print():
            return "backorder/print.html"
        return 'backorder/detail.html'

    def get_queryset(self):
        return BackOrder.objects.prefetch_related(
            'back_order_items'
        ).filter(
            pk=self.kwargs['pk']
        )

    def get_context_data(self, **kwargs):
        context = super(BackOrderDetailView, self).get_context_data(**kwargs)
        context['back_order_items'] = self.get_object().back_order_items.filter(
            item_type=InvoiceItemType.INVENTORY,
            inventory__isnull=False
        )
        context['branch'] = self.get_branch()
        context['company'] = self.get_company()
        cols = 11
        context['cols'] = cols
        context['range'] = range(0, cols)
        context['is_print'] = self.is_print()
        return context


class BulkBackOrderActionView(LoginRequiredMixin, ProfileMixin, View):

    def get(self, request, *args, **kwargs):
        back_orders = BackOrder.objects.filter(id__in=[int(back_order_id) for back_order_id in self.request.GET.getlist('back_orders[]')])
        if back_orders:
            action = self.kwargs['action']
            if action == 'remove':
                services.delete_back_orders(back_orders, self.get_profile(), self.get_role())
                return JsonResponse({
                    'text': 'Back orders deleted',
                    'error': False,
                    'reload': True
                })
            else:
                return JsonResponse({
                    'text': 'Action does not exist yet, please try again later',
                    'warning': True
                })
        else:
            return JsonResponse({
                'text': 'Back orders not found',
                'error': True
            })


class SingleBackOrderActionView(LoginRequiredMixin, ProfileMixin, View):

    def get_back_order(self):
        return BackOrder.objects.filter(pk=self.request.GET.get('back_order_id')).first()

    def get(self, request, *args, **kwargs):
        try:
            back_order = self.get_back_order()
            redirect_url = ''
            action = self.request.GET.get('action', None)
            if back_order.is_removed:
                return JsonResponse({
                    'text': 'Back order has been removed',
                    'warning': True
                })
            elif back_order.is_completed:
                return JsonResponse({
                    'text': 'Back order has already been processed',
                    'error': False,
                    'redirect': redirect_url
                })
            elif action == 'create_picking':
                redirect_url = reverse('create_back_order_picking_slip', kwargs={'back_order_id': back_order.id})
            elif action == 'create_invoice':
                redirect_url = reverse('create_back_order_invoice', kwargs={'back_order_id': back_order.id})
            elif action == 'create_delivery':
                redirect_url = reverse('create_back_order_delivery_note', kwargs={'back_order_id': back_order.id})
            return JsonResponse({
                'text': 'Redirecting ....',
                'error': False,
                'redirect': redirect_url
            })
        except Exception as exception:
            return JsonResponse({
                'text': 'Unexpected error occurred, please try again',
                'error': True,
                'details': str(exception)
            })


class TrackingView(LoginRequiredMixin, ProfileMixin, TemplateView):
    template_name = 'backorder/tracking.html'

    def get_back_order(self):
        return BackOrder.objects.filter(pk=self.kwargs['pk']).first()

    def get_tracking(self, back_order):
        trackings = Tracking.objects.select_related(
            'profile', 'role'
        ).filter(
            object_id=back_order.id, content_type=ContentType.objects.get_for_model(back_order)
        )
        tracking_by_types = defaultdict(list)
        for tracking in trackings:
            tracking_type = tracking.data.get('tracking_type')
            if tracking_type:
                tracking_by_types[tracking_type].append(tracking)
        return tracking_by_types

    def get_context_data(self, **kwargs):
        context = super(TrackingView, self).get_context_data(**kwargs)
        back_order = self.get_back_order()
        tracking = self.get_tracking(back_order)
        context['back_order'] = back_order
        context['backorder_tracking'] = tracking.get('self')
        context['picking_tracking'] = tracking.get('picking_slip')
        context['delivery_tracking'] = tracking.get('delivery')
        context['estimate_tracking'] = tracking.get('estimate')
        context['invoice_tracking'] = tracking.get('invoice')
        return context
