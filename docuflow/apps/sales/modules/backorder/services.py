import logging
from decimal import Decimal

from django.contrib.contenttypes.fields import ContentType

from docuflow.apps.sales.models import (SalesInvoice, BackOrder, Estimate, Delivery, BackOrderItem, PickingSlip,
                                         Invoice, Tracking)
from docuflow.apps.sales.enums import InvoiceItemType

logger = logging.getLogger(__name__)


class CreateBackOrderFromSalesInvoice:

    def __init__(self, sales_invoice: SalesInvoice, sales_invoice_items, **kwargs):
        self.sales_invoice = sales_invoice
        self.sales_invoice_items = sales_invoice_items
        self.kwargs = kwargs

    def create_back_order(self):
        is_new = False
        if isinstance(self.sales_invoice, Estimate):
            if self.sales_invoice.estimate_back_order.exists():
                back_order = self.sales_invoice.estimate_back_order.first()
            else:
                back_order = BackOrder()
                back_order.estimate = self.sales_invoice
                is_new = True
            return back_order, is_new

        if isinstance(self.sales_invoice, Delivery):
            if self.sales_invoice.delivery_back_order.exists():
                back_order = self.sales_invoice.delivery_back_order.first()
            else:
                back_order = BackOrder()
                back_order.delivery = self.sales_invoice
                is_new = True
            back_order.delivery_method = self.sales_invoice.delivery_method
            back_order.delivery_term = self.sales_invoice.delivery_term
            return back_order, is_new

        if isinstance(self.sales_invoice, BackOrder):
            return self.sales_invoice, is_new

        if isinstance(self.sales_invoice, PickingSlip):
            if self.sales_invoice.picking_slip_back_order.exists():
                back_order = self.sales_invoice.picking_slip_back_order.first()
            else:
                back_order = BackOrder()
                back_order.picking_slip = self.sales_invoice
                is_new = True
            back_order.delivery_method = self.sales_invoice.delivery_method
            back_order.delivery_term = self.sales_invoice.delivery_term
            return back_order, is_new
        return BackOrder(), False

    def execute(self):
        back_order, is_new = self.create_back_order()
        logger.info(f"We are using back order {back_order}({back_order.id}) and is new {is_new}")

        back_order.branch = self.sales_invoice.branch
        back_order.profile = self.sales_invoice.profile
        back_order.discount_percentage = getattr(self.sales_invoice, 'discount_percentage', None)
        back_order.is_pick_up = self.sales_invoice.is_pick_up
        back_order.postal = self.sales_invoice.postal
        back_order.delivery_to = self.sales_invoice.delivery_to
        back_order.vat_number = getattr(self.sales_invoice, 'vat_number', None)
        back_order.customer_name = getattr(self.sales_invoice, 'customer_name', None)
        back_order.customer = getattr(self.sales_invoice, 'customer', None)
        back_order.period = getattr(self.sales_invoice, 'period', None)
        if isinstance(self.sales_invoice, Invoice):
            back_order.date = self.sales_invoice.invoice_date
        else:
            back_order.date = getattr(self.sales_invoice, 'date', None)
        back_order.save()
        if back_order:
            self.create_back_order_items(back_order, is_new)
        return back_order

    def create_back_order_items(self, back_order, is_new_back_order):
        total_net_amount = 0
        total_amount = 0
        total_vat = 0
        total_discount = 0
        sub_total = 0
        for sales_item in self.sales_invoice_items:
            if 'back_order_item' in sales_item:
                sales_invoice_item = sales_item.get('back_order_item')
            else:
                sales_invoice_item = sales_item.get('item')

            if sales_invoice_item.is_description:
                back_order_item = BackOrderItem.objects.create(
                    back_order=back_order,
                    description=sales_invoice_item.description,
                    item_type=InvoiceItemType.TEXT
                )
            else:
                qty = Decimal(sales_item.get('quantity', 0))
                vat_amount = 0
                price = sales_invoice_item.price
                net_price = price * qty
                discount_amount = 0
                discount = sales_invoice_item.discount
                if sales_invoice_item.is_account:
                    back_order_item = BackOrderItem.objects.create(
                        back_order=back_order,
                        received=qty,
                        quantity=qty,
                        price=sales_invoice_item.price,
                        discount=sales_invoice_item.discount,
                        net_price=net_price,
                        vat=sales_invoice_item.vat,
                        account=sales_invoice_item.account,
                        unit=sales_invoice_item.unit,
                        vat_code=sales_invoice_item.vat_code,
                        description=sales_invoice_item.description,
                        item_type=InvoiceItemType.GENERAL
                    )
                else:
                    back_order_item, is_new = BackOrderItem.objects.get_or_create(
                        back_order=back_order,
                        inventory=sales_invoice_item.inventory,
                        defaults={
                            'price': price,
                            'discount': sales_invoice_item.discount,
                            'unit': sales_invoice_item.unit,
                            'vat_code': sales_invoice_item.vat_code,
                            'vat': sales_invoice_item.vat,
                            'item_type': InvoiceItemType.INVENTORY
                        }
                    )
                    if is_new:
                        back_order_item.quantity = qty
                        back_order_item.received = qty
                    else:
                        back_order_item.quantity += qty
                        back_order_item.received += qty
                    back_order_item.net_price = net_price
                    back_order_item.save()

                if sales_invoice_item.vat_code:
                    vat_amount = (Decimal(sales_invoice_item.vat_code.percentage / 100) * price) * qty
                if discount and discount > 0:
                    discount_amount = Decimal(discount / 100) * net_price

                total = net_price + vat_amount
                back_order_item.total_amount = total
                back_order_item.save()

                if hasattr(sales_invoice_item, 'back_order_item') and not sales_invoice_item.back_order_item and is_new:
                    logger.info(f"Creating a picking {sales_invoice_item}({sales_invoice_item.id}) with inventory {sales_invoice_item.inventory.id} back order and linking it to {back_order_item}")
                    sales_invoice_item.back_order_item = back_order_item
                    sales_invoice_item.save()

                total_amount += total
                total_vat += vat_amount
                total_net_amount += net_price
                total_discount += discount_amount
                sub_total += net_price + discount_amount

        if is_new_back_order:
            back_order.sub_total = sub_total
            back_order.total_amount = total_amount
            back_order.net_amount = total_net_amount
            back_order.discount_amount = total_discount
            back_order.vat_amount = total_vat
        else:
            back_order.sub_total += sub_total
            back_order.total_amount += total_amount
            back_order.net_amount += total_net_amount
            back_order.discount_amount += total_discount
            back_order.vat_amount += total_vat
        back_order.save()


def delete_back_orders(back_orders, profile, role):
    for back_order in back_orders:
        logger.info(f"Delete back_order {back_order} --- ({back_order.id})---> ")
        Tracking.objects.create(
            content_type=ContentType.objects.get_for_model(back_order),
            object_id=back_order.id,
            comment="Back order deleted",
            profile=profile,
            role=role,
            data={
                'tracking_type': 'self',
                'reference': str(back_order)
            }
        )
        back_order.mark_as_removed()
