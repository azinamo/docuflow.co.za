from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='sales_back_order_index'),
    path('estimate/create/', views.CreateEstimateBackOrderView.as_view(), name='create_delivery_backorder'),
    path('delivery/create/', views.CreateDeliveryBackOrderView.as_view(), name='create_estimate_backorder'),
    path('<str:pk>/detail/', views.BackOrderDetailView.as_view(), name='backorder_detail'),
    path('<str:pk>/tracking/', views.TrackingView.as_view(), name='back_order_tracking'),
    path('bulk/<str:action>/', csrf_exempt(views.BulkBackOrderActionView.as_view()), name='bulk_back_order_action'),
    path('<str:action>/', views.SingleBackOrderActionView.as_view(), name='single_back_order_action'),
]
