from django.apps import AppConfig


class BackorderConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.backorder'
