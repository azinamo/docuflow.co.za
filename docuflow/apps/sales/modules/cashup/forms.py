import logging
import pprint
from collections import defaultdict
from decimal import Decimal
from typing import List

from django import forms
from django.utils.translation import gettext as __

from docuflow.apps.customer.services import age_invoice_receipt, age_receipt, age_receipt_allocation, is_customer_balancing
from docuflow.apps.customer.models import Ledger as CustomerLedger
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.company.models import Account, VatCode
from docuflow.apps.common.enums import Module
from docuflow.apps.journals.exceptions import JournalException
from docuflow.apps.journals.services import CreateLedger, get_total_for_account
from docuflow.apps.period.models import Period, Year
from docuflow.apps.sales.models import CashUp, CashAccount, Receipt
from docuflow.apps.sales.exceptions import SalesException
from .services import get_cashup_summary

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class CreateCashUpForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company')
        self.year = kwargs.pop('year')
        super(CreateCashUpForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(
            self.company,
            self.year
        ).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['date'].label = 'Upto and Including'

    class Meta:
        model = CashUp
        fields = ('period', 'date')

        widgets = {
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
        }

    def clean(self):
        cleaned_data = super(CreateCashUpForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')
        return cleaned_data


class CashUpForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.branch = kwargs.pop('branch')
        self.year = kwargs.pop('year')
        self.role = kwargs.pop('role')
        self.profile = kwargs.pop('profile')
        self.company = self.branch.company
        self.summary = {}
        self.ledger_lines = []
        self.receipts = []
        super(CashUpForm, self).__init__(*args, **kwargs)
        self.fields['period'].queryset = Period.objects.open(self.company, self.year).order_by('-period')
        self.fields['period'].empty_label = None
        self.fields['petty_cash_account'].queryset = Account.objects.active(self.company)
        self.fields['bank_account'].queryset = Account.objects.active(self.company)

    petty_cash_account = forms.ModelChoiceField(queryset=None, label='Account', required=False)
    bank_account = forms.ModelChoiceField(queryset=None, label='Account', required=False)
    petty_cash = forms.DecimalField(label='Petty Cash', required=False)
    bank_cash = forms.DecimalField(label='Cash Deposited', widget=forms.HiddenInput(), required=False)

    class Meta:
        model = CashUp
        fields = ('period', 'date')

        widgets = {
            'date': forms.TextInput(attrs={'class': 'form-control datepicker'}),
        }

    def clean(self):
        cleaned_data = super(CashUpForm, self).clean()

        date = cleaned_data['date']
        period = cleaned_data['period']
        if not period:
            self.add_error('period', 'Period is required')

        if not date:
            self.add_error('date', 'Date is required')

        if period and date:
            if not period.is_valid(date):
                self.add_error('date', 'Period and payment date do not match')

        if cleaned_data['petty_cash'] and not cleaned_data['petty_cash_account']:
            self.add_error('petty_cash_account', 'Account is required')

        if cleaned_data['bank_cash'] and not cleaned_data['bank_account']:
            self.add_error('bank_account', 'Bank account is required')

        return cleaned_data

    def get_receipts(self, end_date, period):
        return Receipt.objects.pending_cash_up(branch=self.branch, period=period, end_date=end_date).all()

    def save(self, commit=True):
        cash_up = super(CashUpForm, self).save(commit=False)
        cash_up.branch = self.branch
        cash_up.profile = self.profile
        cash_up.role = self.role
        cash_up.save()

        receipts = self.get_receipts(end_date=self.cleaned_data['date'], period=self.cleaned_data['period'])
        self.summary = get_cashup_summary(receipts=receipts)

        self.update_receipts(cash_up=cash_up)

        petty_cash = self.cleaned_data.get('petty_cash', None)
        petty_cash_account = self.cleaned_data.get('petty_cash_account', None)
        if petty_cash and petty_cash_account:
            self.create_cash_account(cash_up, petty_cash, petty_cash_account, 'petty_cash')

        bank_cash = self.cleaned_data.get('bank_cash', None)
        bank_account = self.cleaned_data.get('bank_account', None)
        if bank_cash and bank_account:
            self.create_cash_account(cash_up, bank_cash, bank_account, 'bank_slip')
        self.prepare_ledger(cash_up)

        self.create_ledger(cash_up)

        logger.info('Complete updating customer receipts')
        self.complete_customer_ledger_entries(receipts=receipts)

        if not is_customer_balancing(company=self.branch.company, year=cash_up.period.period_year):
            raise SalesException(__('A variation in age analysis and balance sheet detected.'))
        return cash_up

    def update_receipts(self, cash_up: CashUp):
        for receipt in self.summary['receipts']:
            receipt.cash_up = cash_up
            receipt.save()

            if receipt.invoices.exists():
                for invoice_receipt in receipt.invoices.all():
                    logger.info(f"Age invoice receipt {receipt} -> {invoice_receipt.amount} -> {invoice_receipt.invoice} ")
                    age_invoice_receipt(invoice_receipt=invoice_receipt)
            if receipt.balance:
                logger.info(f"Age receipt balance {receipt} -> {receipt.amount}")
                age_receipt(receipt=receipt)
            # When an updated unallocated receipt is the one linked to invoice on payment, then clear
            # the unallocated amount
            for sundry_receipt in receipt.allocations.filter(receipt__cash_up__isnull=False).all():
                logger.info(f"Age sundry_receipt balance {sundry_receipt} -> {sundry_receipt.amount}")
                age_receipt_allocation(sundry_receipt=sundry_receipt)

    def create_cash_account(self, cash_up, amount, account, account_type):
        debit = 0
        credit = 0
        if amount > 0:
            debit = amount
        elif amount < 0:
            credit = amount * -1

        self.ledger_lines.append({
            'account': account,
            'credit': credit,
            'debit': debit,
            'accounting_date': cash_up.date,
            'text': 'Bank Slip' if account_type == 'bank_slip' else 'Petty Cash'
        })
        return CashAccount.objects.create(
            total_amount=amount,
            account=account,
            cash_up=cash_up
        )

    # noinspection PyMethodMayBeStatic
    def complete_customer_ledger_entries(self, receipts: List):
        logger.info("Complete complete_customer_ledger_entries---")
        receipt_ids = [receipt.id for receipt in receipts]
        customer_ledgers = CustomerLedger.objects.filter(
            status=LedgerStatus.PENDING
        )
        for customer_ledger in customer_ledgers:
            customer_ledger.status = LedgerStatus.COMPLETE
            customer_ledger.save()

    def get_customer_account(self):
        default_customer_account = self.branch.company.default_customer_account
        if not default_customer_account:
            raise JournalException(__('Default customer account not available'))
        return default_customer_account

    def create_ledger(self, cash_up: CashUp):
        create_ledger = CreateLedger(
            company=cash_up.branch.company,
            module=Module.SALES.value,
            year=self.cleaned_data['period'].period_year,
            period=self.cleaned_data['period'],
            profile=self.profile,
            role=cash_up.role,
            text=f"Cash Up Summary {str(cash_up)}",
            journal_lines=self.ledger_lines,
            date=cash_up.date
        )
        journal = create_ledger.execute()

        cash_up.journal = journal
        cash_up.save()
        return journal

    def prepare_ledger(self, cash_up: CashUp):
        customer_account = self.get_customer_account()
        customer_total = self.summary['total']
        vat_allocations = defaultdict(Decimal)
        allocations = defaultdict(Decimal)
        payment_methods = defaultdict(Decimal)
        default_vat_code = VatCode.objects.input().filter(company=self.branch.company, is_default=True).first()
        default_discount_allowed_account = self.branch.company.default_discount_allowed_account
        discount_amount_excluding = 0
        logger.info(self.summary)
        for receipt in self.summary['receipts']:
            if receipt.total_discounts:
                discount_vat_amount = receipt.total_discounts_vat
                discount_amount_excluding += receipt.total_discounts - discount_vat_amount
                customer_total -= receipt.total_discounts
                vat_allocations[default_vat_code] += receipt.total_discounts_vat

            for account_allocation in receipt.accounts.all():
                customer_total -= account_allocation.amount
                vat_amount = Decimal(0)
                if account_allocation.vat_code and account_allocation.vat_amount:
                    if not account_allocation.vat_code.account:
                        raise SalesException(f"Vat code {account_allocation.vat_code} does not have an account "
                                             f"associated with it.")
                    vat_amount = account_allocation.vat_amount
                    vat_allocations[account_allocation.vat_code] += account_allocation.vat_amount
                amount_excluding = account_allocation.amount - vat_amount
                allocations[account_allocation.account] += amount_excluding

            for receipt_payment in receipt.payments.all():
                if not receipt_payment.payment_method.account:
                    raise SalesException(__(f"Account for payment method {receipt_payment.payment_method}, not found "))
                logger.info(f"<-- Receipt payment {receipt_payment} for an amount of {receipt_payment.amount} on account {receipt_payment.payment_method.account} -->")
                total = receipt_payment.amount
                if receipt_payment.payment_method.is_summarized:
                    payment_methods[receipt_payment.payment_method] += total
                else:
                    pass
                    # Commented after we had an issue with cash up with REC0005 and invoice K007
                    # TODO - Add tests
                    self.ledger_lines.append({
                        'account': receipt_payment.payment_method.account,
                        'credit': 0 if total > 0 else total * -1,
                        'debit': total if total > 0 else 0,
                        'text': receipt_payment.receipt.reference,
                        'accounting_date': receipt.date
                    })

        if discount_amount_excluding:
            self.ledger_lines.append({
                'account': default_discount_allowed_account,
                'debit': discount_amount_excluding if discount_amount_excluding > 0 else 0,
                'credit': 0 if discount_amount_excluding > 0 else discount_amount_excluding * -1,
                'text': str(default_discount_allowed_account),
                'accounting_date': cash_up.date
            })

        for payment_method, method_amount in payment_methods.items():
            logger.info(f"{payment_method} is depositable {payment_method.is_depositable}, its account is selected")
            if not payment_method.is_depositable:
                self.ledger_lines.append({
                    'account': payment_method.account,
                    'credit': 0 if method_amount > 0 else method_amount * -1,
                    'debit': method_amount if method_amount > 0 else 0,
                    'text': str(payment_method),
                    'accounting_date': cash_up.date
                })

        for vat_code, vat_total in vat_allocations.items():
            self.ledger_lines.append({
                'account': vat_code.account,
                'vat_code': vat_code,
                'credit': 0 if vat_total > 0 else vat_total * -1,
                'debit': vat_total if vat_total > 0 else 0,
                'text': 'Vat Account',
                'accounting_date': cash_up.date
            })
        for allocation_account, allocated_total in allocations.items():
            self.ledger_lines.append({
                'account': allocation_account,
                'credit': 0 if allocated_total > 0 else allocated_total * -1,
                'debit': allocated_total if allocated_total > 0 else 0,
                'text': allocation_account.name,
                'accounting_date': cash_up.date
            })
        logger.info(f"<-- Customer totals {str(cash_up)} for an amount of {customer_total} on account"
                    f" {customer_account} -->")
        self.ledger_lines.append({
            'account': customer_account,
            'credit': 0 if customer_total > 0 else customer_total * -1,
            'debit': customer_total if customer_total > 0 else 0,
            'text': f"Cash Up {str(cash_up)}",
            'accounting_date': cash_up.date
        })
