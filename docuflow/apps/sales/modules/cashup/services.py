import logging
import pprint
from collections import defaultdict
from decimal import Decimal

from django.utils.translation import gettext as __

from docuflow.apps.common.enums import Module
from docuflow.apps.company.models import VatCode
from docuflow.apps.customer.enums import LedgerStatus
from docuflow.apps.customer.models import Ledger as CustomerLedger
from docuflow.apps.customer.services import age_invoice_receipt, age_receipt, age_receipt_allocation, \
    is_customer_balancing
from docuflow.apps.journals.exceptions import JournalException
from docuflow.apps.journals.services import CreateLedger
from docuflow.apps.sales.exceptions import SalesException
from docuflow.apps.sales.models import CashAccount, Receipt

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


class CreateCashUp:

    def __init__(self, branch, profile, role, summary, form, receipts):
        self.branch = branch
        self.summary = summary
        self.profile = profile
        self.role = role
        self.form = form
        self.total = summary['total']
        self.receipts = receipts
        self.ledger_lines = []
        logger.info("===========create cash up=================")

    def create(self):
        cash_up = self.form.save(commit=False)
        cash_up.branch = self.branch
        cash_up.profile = self.profile
        cash_up.role = self.role
        cash_up.save()
        logger.info(f"created cash up {cash_up}")
        return cash_up

    def update_receipts(self, cash_up):
        for receipt in self.summary['receipts']:
            receipt.cash_up = cash_up
            receipt.save()
            logger.info(f"updated receipt {receipt}")

            if receipt.invoices.exists():
                for invoice_receipt in receipt.invoices.all():
                    age_invoice_receipt(invoice_receipt=invoice_receipt)
            if receipt.balance:
                age_receipt(receipt=receipt)
            if receipt.allocations.exists():
                for sundry_receipt in receipt.allocations.all():
                    age_receipt_allocation(sundry_receipt=sundry_receipt)

    def create_cash_account(self, cash_up, amount, account, account_type):
        logger.info(f"create cash account {amount} --> {account}")
        debit = 0
        credit = 0
        if amount > 0:
            debit = amount
        elif amount < 0:
            credit = amount * -1

        self.ledger_lines.append({
            'account': account,
            'credit': credit,
            'debit': debit,
            'accounting_date': cash_up.date,
            'text': 'Bank Slip' if account_type == 'bank_slip' else 'Petty Cash'
        })
        return CashAccount.objects.create(
            total_amount=amount,
            account=account,
            cash_up=cash_up
        )

    def execute(self):
        cash_up = self.create()

        self.update_receipts(cash_up)

        petty_cash = self.form.cleaned_data.get('petty_cash', None)
        petty_cash_account = self.form.cleaned_data.get('petty_cash_account', None)
        if petty_cash and petty_cash_account:
            self.create_cash_account(cash_up, petty_cash, petty_cash_account, 'petty_cash')

        bank_cash = self.form.cleaned_data.get('bank_cash', None)
        bank_account = self.form.cleaned_data.get('bank_account', None)
        if bank_cash and bank_account:
            self.create_cash_account(cash_up, bank_cash, bank_account, 'bank_slip')
        self.prepare_ledger(cash_up)

        self.create_ledger(cash_up)

        logger.info('Complete updating customer receipts')
        self.complete_customer_ledger_entries()

        if not is_customer_balancing(company=self.branch.company, year=cash_up.period.period_year):
            raise SalesException(__('A variation in age analysis and balance sheet detected.'))

    def complete_customer_ledger_entries(self):
        logger.info("Complete complete_customer_ledger_entries---")
        receipt_ids = [receipt.id for receipt in self.receipts]
        logger.info(receipt_ids)
        # content_type = ContentType.objects.filter(
        #     app_label='sales',
        #     model='receipt'
        # ).first()
        customer_ledgers = CustomerLedger.objects.filter(
            status=LedgerStatus.PENDING
        )
        for customer_ledger in customer_ledgers:
            customer_ledger.status = LedgerStatus.COMPLETE
            customer_ledger.save()

    def get_customer_account(self):
        default_customer_account = self.branch.company.default_customer_account
        if not default_customer_account:
            raise JournalException(__('Default customer account not available'))
        return default_customer_account

    def create_ledger(self, cash_up):
        create_ledger = CreateLedger(
            company=cash_up.branch.company,
            module=Module.SALES,
            year=self.form.cleaned_data['period'].period_year,
            period=self.form.cleaned_data['period'],
            profile=self.profile,
            role=cash_up.role,
            text=f"Cash Up Summary {str(cash_up)}",
            journal_lines=self.ledger_lines,
            date=cash_up.date
        )
        create_ledger.execute()

    def prepare_ledger(self, cash_up):
        customer_account = self.get_customer_account()
        customer_total = self.summary['total']
        vat_allocations = defaultdict(Decimal)
        allocations = defaultdict(Decimal)
        payment_methods = defaultdict(Decimal)
        default_vat_code = VatCode.objects.input().filter(company=self.branch.company, is_default=True).first()
        default_discount_allowed_account = self.branch.company.default_discount_allowed_account
        discount_amount_excluding = 0
        logger.info(self.summary['receipts'])
        for receipt in self.summary['receipts']:
            if receipt.total_discounts:
                discount_vat_amount = receipt.total_discounts_vat
                discount_amount_excluding += receipt.total_discounts - discount_vat_amount
                customer_total -= receipt.total_discounts
                vat_allocations[default_vat_code] += receipt.total_discounts_vat

            for account_allocation in receipt.accounts.all():
                customer_total -= account_allocation.amount
                vat_amount = Decimal(0)
                if account_allocation.vat_code and account_allocation.vat_amount:
                    if not account_allocation.vat_code.account:
                        raise SalesException(f"Vat code {account_allocation.vat_code} does not have an account "
                                             f"associated with it.")
                    vat_amount = account_allocation.vat_amount
                    vat_allocations[account_allocation.vat_code] += account_allocation.vat_amount
                amount_excluding = account_allocation.amount - vat_amount
                allocations[account_allocation.account] += amount_excluding

            for receipt_payment in receipt.payments.all():
                if not receipt_payment.payment_method.account:
                    raise SalesException(__(f"Account for payment method {receipt_payment.payment_method}, not found "))
                logger.info(f"<-- Receipt payment {receipt_payment} for an amount of {receipt_payment.amount} on account {receipt_payment.payment_method.account} -->")
                total = receipt_payment.amount
                if receipt_payment.payment_method.is_summarized:
                    payment_methods[receipt_payment.payment_method] += total
                else:
                    self.ledger_lines.append({
                        'account': receipt_payment.payment_method.account,
                        'credit': 0 if total > 0 else total * -1,
                        'debit': total if total > 0 else 0,
                        'text': receipt_payment.receipt.reference,
                        'accounting_date': receipt.date
                    })
        if discount_amount_excluding:
            self.ledger_lines.append({
                'account': default_discount_allowed_account,
                'debit': discount_amount_excluding if discount_amount_excluding > 0 else 0,
                'credit': 0 if discount_amount_excluding > 0 else discount_amount_excluding * -1,
                'text': str(default_discount_allowed_account),
                'accounting_date': cash_up.date
            })

        for payment_method, method_amount in payment_methods.items():
            logger.info(f"{payment_method} is depositable {payment_method.is_depositable}, its account is selected")
            if not payment_method.is_depositable:
                self.ledger_lines.append({
                    'account': payment_method.account,
                    'credit': 0 if method_amount > 0 else method_amount * -1,
                    'debit': method_amount if method_amount > 0 else 0,
                    'text': str(payment_method),
                    'accounting_date': cash_up.date
                })

        for vat_code, vat_total in vat_allocations.items():
            self.ledger_lines.append({
                'account': vat_code.account,
                'vat_code': vat_code,
                'credit': 0 if vat_total > 0 else vat_total * -1,
                'debit': vat_total if vat_total > 0 else 0,
                'text': 'Vat Account',
                'accounting_date': cash_up.date
            })
        for allocation_account, allocated_total in allocations.items():
            self.ledger_lines.append({
                'account': allocation_account,
                'credit': 0 if allocated_total > 0 else allocated_total * -1,
                'debit': allocated_total if allocated_total > 0 else 0,
                'text': allocation_account.name,
                'accounting_date': cash_up.date
            })
        logger.info(
            f"<-- Customer totals {str(cash_up)} for an amount of {customer_total} on account {customer_account} -->")
        self.ledger_lines.append({
            'account': customer_account,
            'credit': 0 if customer_total > 0 else customer_total * -1,
            'debit': customer_total if customer_total > 0 else 0,
            'text': f"Cash Up {str(cash_up)}",
            'accounting_date': cash_up.date
        })


class CashUpSummary:
    depositable_receipts = {'total': 0, 'methods': 0}
    non_depositable_receipts = {'total': 0, 'methods': 0}

    def __init__(self, branch):
        self. branch = branch

    def get_receipts(self):
        return Receipt.objects.prefetch_related(
            'payment_methods'
        ).select_related(
            'content_type'
        ).filter(
            branch=self.branch,
            cash_up__isnull=True
        )

    def execute(self):
        total_cash_up = Decimal(0)
        receipts_total = Decimal(0)
        receipts = self.get_receipts()
        for receipt in receipts:
            if receipt.amount:
                total_cash_up += receipt.amount
            for receipt_payment_method in receipt.payment_methods.all():
                depositable = 0 if receipt_payment_method.payment_method.is_depositable else 1
                logger.info(receipt_payment_method)
                if receipt_payment_method.payment_method.is_depositable:
                    if receipt_payment_method.payment_method in receipt_payment_method.payment_method['methods']:
                        self.depositable_receipts['methods'][receipt_payment_method.payment_method][
                            'receipts'].append(receipt_payment_method)
                        self.depositable_receipts['methods'][receipt_payment_method.payment_method][
                            'total'] += receipt_payment_method.amount

                    else:
                        receipt_payment_method.payment_method['methods'][receipt_payment_method.payment_method] = {
                            'receipts': [receipt_payment_method], 'total': receipt_payment_method.amount}
        return {'receipts': receipts, 'total': total_cash_up, 'receipts_total': receipts_total}


def get_cashup_summary(receipts):
    cashup_receipts = {0: {'total': 0, 'methods': {}}, 1: {'total': 0, 'methods': {}}}
    total_cash_up = Decimal(0)
    receipts_total = Decimal(0)
    for receipt in receipts:
        if receipt.amount:
            total_cash_up += receipt.amount
        for payment in receipt.payments.all():
            depositable = 0 if payment.payment_method and payment.payment_method.is_depositable else 1
            cashup_receipts[depositable]['total'] += payment.amount
            if payment.payment_method in cashup_receipts[depositable]['methods']:
                cashup_receipts[depositable]['methods'][payment.payment_method]['receipts'].append(payment)
                cashup_receipts[depositable]['methods'][payment.payment_method]['total'] += payment.amount

            else:
                cashup_receipts[depositable]['methods'][payment.payment_method] = {
                    'receipts': [payment],
                    'total': payment.amount
                }
    return {'receipts': receipts, 'cashup': cashup_receipts, 'total': total_cash_up, 'receipts_total': receipts_total}
