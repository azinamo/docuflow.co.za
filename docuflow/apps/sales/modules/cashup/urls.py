from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.CashUpView.as_view(), name='cash_up_index'),
    path('<int:pk>/', views.CashUpDetailView.as_view(), name='cash_up_detail'),
    path('create/', csrf_exempt(views.CreateCashUpView.as_view()), name='create_cash_up'),
    path('complete/', views.CompleteCashUpView.as_view(), name='complete_cash_up'),
    path('add/credit/slip/', csrf_exempt(views.AddCreditSlipView.as_view()), name='add_credit_card_slips'),
    path('add/cash/slip/', csrf_exempt(views.AddCashSlipView.as_view()), name='add_cash_slips'),
]
