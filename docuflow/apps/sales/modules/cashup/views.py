import logging
import pprint
from datetime import datetime
from decimal import Decimal

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import FormView, TemplateView, CreateView

from docuflow.apps.common import utils
from docuflow.apps.common.mixins import ProfileMixin, DataTableMixin
from docuflow.apps.company.models import Account
from docuflow.apps.journals.exceptions import JournalNotBalancingException
from docuflow.apps.period.models import Period
from docuflow.apps.sales.exceptions import SalesException
from docuflow.apps.sales.models import Receipt, CashUp
from .forms import CashUpForm, CreateCashUpForm
from .services import get_cashup_summary

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger(__name__)


class CashUpView(LoginRequiredMixin, ProfileMixin, DataTableMixin, TemplateView):
    template_name = 'cashup/index.html'
    context_object_name = 'cash_ups'

    def get_context_data(self, **kwargs):
        context = super(CashUpView, self).get_context_data(**kwargs)
        context['branch'] = self.get_branch()
        return context


class CashUpDetailView(LoginRequiredMixin, ProfileMixin, TemplateView):

    def is_print(self):
        return self.request.GET.get('print') == '1'

    def get_template_names(self):
        if self.is_print():
            return 'cashup/print.html'
        return 'cashup/detail.html'

    def get_cash_up(self):
        return CashUp.objects.select_related(
            'role', 'profile', 'period', 'branch'
        ).prefetch_related(
            'cash_accounts'
        ).filter(
            id=self.kwargs['pk']
        ).first()

    def get_receipts(self, cash_up):
        return Receipt.objects.prefetch_related(
            'payments'
        ).select_related(
            'content_type'
        ).filter(
            cash_up=cash_up
        )

    def get_cashup_summary(self, cash_up):
        receipts = self.get_receipts(cash_up)
        cashup_receipts = {0: {'total': 0, 'methods': {}}, 1: {'total': 0, 'methods': {}}}
        total_cash_up = Decimal(0)
        receipts_total = Decimal(0)
        total_vat = Decimal(0)
        transactions_total = Decimal(0)
        for receipt in receipts:
            if receipt.amount:
                total_cash_up += receipt.total_allocated
            if receipt.total_vat:
                total_vat += receipt.total_vat
            if receipt.total_discounts:
                transactions_total += receipt.total_discounts
            if receipt.total_account:
                transactions_total += receipt.total_account
            for receipt_payment_method in receipt.payments.all():
                depositable = 0 if receipt_payment_method.payment_method.is_depositable else 1
                cashup_receipts[depositable]['total'] += receipt_payment_method.amount
                if receipt_payment_method.payment_method in cashup_receipts[depositable]['methods']:
                    cashup_receipts[depositable]['methods'][receipt_payment_method.payment_method]['receipts'].append(receipt_payment_method)
                    cashup_receipts[depositable]['methods'][receipt_payment_method.payment_method]['total'] += receipt_payment_method.amount

                else:
                    cashup_receipts[depositable]['methods'][receipt_payment_method.payment_method] = {
                        'receipts': [receipt_payment_method], 'total': receipt_payment_method.amount}
        return {'receipts': receipts,
                'cashup': cashup_receipts,
                'total': total_cash_up,
                'receipts_total': receipts_total,
                'transactions_total': transactions_total,
                'total_vat': total_vat,
                'net_to_be_banked': total_cash_up + transactions_total
                }

    def get_accounts(self, company):
        return Account.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(CashUpDetailView, self).get_context_data(**kwargs)
        branch = self.get_branch()
        cash_up = self.get_cash_up()
        accounts = self.get_accounts(branch.company)
        context['cash_up'] = cash_up
        context['branch'] = branch
        context['summary'] = self.get_cashup_summary(cash_up)
        context['accounts'] = accounts
        context['discount_account'] = branch.company.default_discount_allowed_account
        context['is_print'] = self.is_print()
        return context


class CreateCashUpView(ProfileMixin, FormView):
    template_name = 'cashup/create.html'
    form_class = CreateCashUpForm

    def get_initial(self):
        initial = super(CreateCashUpView, self).get_initial()
        initial['date'] = datetime.now().strftime('%Y-%m-%d')
        return initial

    def get_form_kwargs(self):
        kwargs = super(CreateCashUpView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        kwargs['year'] = self.get_year()
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'error': True,
                'text': 'Error occurred generating the cashup, please fix the errors.',
                'errors': form.errors
            })
        return super(CreateCashUpView, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax() and self.request.method == 'POST':
            period = form.cleaned_data['period']
            date = form.cleaned_data['date']
            return JsonResponse({
                'redirect': reverse('complete_cash_up') + "?period={}&date={}".format(period.id, date),
                'text': 'Redirecting ...'
            })


class CompleteCashUpView(ProfileMixin, CreateView):
    template_name = 'cashup/complete.html'
    form_class = CashUpForm
    model = CashUp

    def get_initial(self):
        initial = super(CompleteCashUpView, self).get_initial()
        initial['period'] = self.request.GET.get('period')
        initial['date'] = self.request.GET.get('date')
        initial['profile'] = self.get_profile()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CompleteCashUpView, self).get_form_kwargs()
        kwargs['branch'] = self.get_branch()
        kwargs['year'] = self.get_year()
        kwargs['profile'] = self.get_profile()
        kwargs['role'] = self.get_role()
        return kwargs

    def get_receipts(self, end_date, period):
        return Receipt.objects.pending_cash_up(branch=self.get_branch(), period=period, end_date=end_date)

    # noinspection PyMethodMayBeStatic
    def get_accounts(self, company):
        return Account.objects.filter(company=company)

    def get_context_data(self, **kwargs):
        context = super(CompleteCashUpView, self).get_context_data(**kwargs)
        company = self.get_company()
        period = Period.objects.filter(pk=self.request.GET.get('period')).first()
        receipts = self.get_receipts(self.request.GET.get('date'), period)
        context['company'] = company
        context['profile'] = self.get_profile()
        context['branch'] = self.get_branch()
        context['summary'] = get_cashup_summary(receipts)
        context['accounts'] = self.get_accounts(company)
        return context

    def form_invalid(self, form):
        if utils.is_ajax(request=self.request):
            return JsonResponse({'error': True,
                                 'text': 'Error occurred generating the cashup, please fix the errors.',
                                 'errors': form.errors,
                                 'alert': True
                                 }, status=400)
        return super(CompleteCashUpView, self).form_invalid(form)

    def form_valid(self, form):
        if utils.is_ajax_post(request=self.request):
            try:
                with transaction.atomic():
                    form.save()
                    return JsonResponse({'text': 'Cash up saved successfully',
                                         'error': False,
                                         'redirect': reverse('cash_up_index')
                                         })
            except (JournalNotBalancingException, SalesException) as exception:
                logger.exception(exception)
                return JsonResponse({'text': str(exception),
                                     'details': str(exception),
                                     'error': True
                                     })
        return super(CompleteCashUpView, self).form_valid(form)


class AddCreditSlipView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'cashup/creditslip.html'


class AddCashSlipView(LoginRequiredMixin, ProfileMixin, FormView):
    template_name = 'cashup/cashslip.html'

