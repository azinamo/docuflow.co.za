from django.apps import AppConfig


class CashupConfig(AppConfig):
    name = 'docuflow.apps.sales.modules.cashup'
