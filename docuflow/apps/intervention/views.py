from django.shortcuts import get_object_or_404, reverse
from django.http import HttpResponseRedirect
from django.views.generic.edit import UpdateView, FormView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django_filters.views import FilterView

from docuflow.apps.company.models import Company
from .models import Intervention
from .forms import InterventionForm
from .filters import InterventionFilter


# Create your views here.
class SearchWorkflowRuleView(LoginRequiredMixin, FormView):
    template_name = 'intervention/search.html'

    def get_form_class(self):
        return InterventionForm

    def get_queryset(self):
        return Intervention.objects.filter(company=self.get_company()).order_by('id')

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(SearchWorkflowRuleView, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_company_object_categories(self, company):
        return company.company_objects.all()

    def get_context_data(self, **kwargs):
        context = super(SearchWorkflowRuleView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['object_categories'] = self.get_company_object_categories(company)
        return context


class WorkflowRuleView(LoginRequiredMixin, FilterView):
    model = Intervention
    filterset_class = InterventionFilter
    template_name = 'intervention/index.html'
    context_object_name = 'rules'

    def get_queryset(self):
        return Intervention.objects.select_related(
            'company', 'account', 'document_type', 'role', 'supplier', 'company__currency'
        ).filter(company=self.get_company()).order_by('id')

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_context_data(self, **kwargs):
        context = super(WorkflowRuleView, self).get_context_data(**kwargs)
        company = self.get_company()
        context['company'] = company
        return context


class CreateWorkflowRule(LoginRequiredMixin, FormView):
    template_name = 'intervention/create.html'
    success_message = 'Intervention rule successfully saved'

    def get_form_class(self):
        return InterventionForm

    def get_form_kwargs(self):
        kwargs = super(CreateWorkflowRule, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_company_object_categories(self, company):
        return company.company_objects.all()

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_context_data(self, **kwargs):
        context = super(CreateWorkflowRule, self).get_context_data(**kwargs)
        company = self.get_company()
        context['object_categories'] = self.get_company_object_categories(company)
        return context

    def form_invalid(self, form):
        messages.error(self.request, 'Error occured saving the intervention.')
        return HttpResponseRedirect(reverse('create_intervention'))

    def form_valid(self, form):
        workflow_rule = form.save(commit=False)
        workflow_rule.company = self.get_company()
        try:
            workflow_rule.save()
            form.save_m2m()
        except Exception as ex:
            messages.error(self.request, 'Unable to save intervention rule; integrity error {}'.format(ex))
            return super(CreateWorkflowRule, self).form_invalid(form)
        else:
            messages.success(self.request, 'Intervention saved successfully.')
            return HttpResponseRedirect(reverse('intervention_index'))


class EditWorkflowRule(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Intervention
    template_name = 'intervention/edit.html'
    success_message = 'Workflow rule successfully updated'

    def get_context_data(self, **kwargs):
        context = super(EditWorkflowRule, self).get_context_data(**kwargs)
        company = self.get_company()
        context['workflow_rule'] = Intervention.objects.get(pk=self.kwargs['pk'])
        return context

    def get_company(self):
        return Company.objects.get(pk=self.request.session['company'])

    def get_form_kwargs(self):
        kwargs = super(EditWorkflowRule, self).get_form_kwargs()
        kwargs['company'] = self.get_company()
        return kwargs

    def get_success_url(self):
        return reverse('intervention_index')

    def get_form_class(self):
        return InterventionForm

    def form_valid(self, form):
        workflow_rule = form.save(commit=False)
        workflow_rule.company = self.get_company()
        try:
            workflow_rule.account = form.cleaned_data['account']
            workflow_rule.save()

            form.save_m2m()
        except Exception as ex:
            messages.error(self.request, 'Unable to save intervention rule; integrity error {}'.format(ex))
            return super(EditWorkflowRule, self).form_invalid(form)
        else:
            messages.success(self.request, 'Intervention saved successfully.')
            return HttpResponseRedirect(reverse('intervention_index'))


class DeleteWorkflowRuleView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Intervention
    success_message = 'Intervention rule succesfully deleted'

    def get(self, request, *args, **kwargs):
        rule = get_object_or_404(Intervention, pk=self.kwargs['pk'])
        if rule:
            rule.delete()
        messages.success(self.request, 'Intervention successfully deleted.')
        return HttpResponseRedirect(reverse('intervention_index'))

    def get_success_url(self):
        return reverse('intervention_index')