from django.apps import AppConfig


class InterventionConfig(AppConfig):
    name = 'docuflow.apps.intervention'
