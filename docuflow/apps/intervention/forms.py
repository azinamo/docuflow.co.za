from django import forms
from django.forms.utils import ErrorList
from django.utils.translation import ugettext as _

# Workflow models
from .models import *
from docuflow.apps.accounts.models import Profile, Role
from docuflow.apps.supplier.models import Supplier


class InterventionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        company = None
        if 'company' in kwargs:
            company = kwargs.pop('company')

        super(InterventionForm, self).__init__(*args, **kwargs)
        self.fields['supplier'] = forms.ModelChoiceField(
            required=False,
            label='Supplier',
            queryset=Supplier.objects.filter(company=company)
        )
        self.fields['role'] = forms.ModelChoiceField(
            required=False,
            label='Role',
            queryset=Role.objects.filter(company=company)
        )
        self.fields['flow_position'] = forms.ChoiceField(
            required=False,
            label='Add to flow/ Flow position',
            choices=Intervention.FLOW_POSITIONS
        )

    class Meta:
        fields = ('account', 'event', 'amount_check', 'amount', 'supplier', 'comment', 'role', 'item_type', 'reinvoice',
                  'expense_type', 'is_blocked', 'distribution', 'manager_role_to_flow', 'document_type', 'flow_position')
        model = Intervention
