import django_filters
from .models import Intervention
from docuflow.apps.supplier.models import Supplier
from docuflow.apps.invoice.models import InvoiceType
from docuflow.apps.company.models import Account
from docuflow.apps.accounts.models import Role


def suppliers(request):
    if request is None:
        return Supplier.objects.none()
    return Supplier.objects.filter(company=request.session['company'])


def document_types(request):
    if request is None:
        return InvoiceType.objects.none()
    return InvoiceType.objects.filter(company=request.session['company'])


def accounts(request):
    if request is None:
        return Account.objects.none()
    return Account.objects.filter(company=request.session['company'])


def roles(request):
    if request is None:
        return Role.objects.none()
    return Role.objects.filter(company=request.session['company'])


class InterventionFilter(django_filters.FilterSet):
    supplier = django_filters.ModelChoiceFilter(queryset=suppliers)
    document_type = django_filters.ModelChoiceFilter(queryset=document_types)
    account = django_filters.ModelChoiceFilter(queryset=accounts)
    role = django_filters.ModelChoiceFilter(queryset=roles)

    class Meta:
        model = Intervention
        fields = ['event', 'supplier', 'document_type', 'account', 'role', 'item_type', 'expense_type',
                  'amount_check', 'amount', 'flow_position']
