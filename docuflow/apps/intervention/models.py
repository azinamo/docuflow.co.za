from django.db import models
from django.utils.translation import ugettext_lazy as _

from docuflow.apps.company.models import Company, ObjectItem, Account
from docuflow.apps.accounts.models import Role


# Create your models here.
class Intervention(models.Model):

    GREATER_THAN = 1
    LESS_THAN = 2
    EQUAL = 3
    NOT_USED = 4

    AMOUNT_CHECKS = (
            (GREATER_THAN, _('Greater than')),
            (LESS_THAN, _('Less than')),
            (EQUAL, _('Equal')),
            (NOT_USED, _('Not Used')),
            )

    EVENTS = (
        ('signing', 'Signing'),
        ('save', 'On Save'),
        ('park_it', 'Park It'),
    )

    AT_NEXT_STEP = 1
    AT_END = 2
    AT_CURRENT_LEVEL = 3
    FLOW_POSITIONS = (
        (AT_NEXT_STEP, 'At next step'),
        (AT_END, 'At the end'),
        (AT_CURRENT_LEVEL, 'Same level'),
    )

    company = models.ForeignKey(Company, related_name='interventions', on_delete=models.CASCADE)
    document_type = models.ForeignKey('invoice.InvoiceType', related_name='document_type', null=True, blank=True,
                                      on_delete=models.DO_NOTHING)
    event = models.CharField(choices=EVENTS, null=True, blank=True, max_length=255)
    supplier = models.ForeignKey('supplier.Supplier', null=True, blank=True, related_name='rule',
                                 on_delete=models.DO_NOTHING)
    role = models.ForeignKey(Role, null=True, blank=True, related_name='intervention_role', on_delete=models.DO_NOTHING)
    account = models.ForeignKey(Account, null=True, blank=True, related_name='account_workflow_rules', on_delete=models.DO_NOTHING)
    object_items = models.ManyToManyField(ObjectItem)
    item_type = models.CharField(blank=True, max_length=255, default='')
    expense_type = models.CharField(blank=True, max_length=255, default='')
    amount = models.DecimalField(decimal_places=2, max_digits=12, null=True, blank=True)
    amount_check = models.IntegerField(choices=AMOUNT_CHECKS, default=NOT_USED, null=True, blank=True)
    flow_position = models.IntegerField(choices=FLOW_POSITIONS, null=True, blank=True)
    manager_role_to_flow = models.BooleanField(default=False)
    distribution = models.BooleanField(default=False)
    reinvoice = models.BooleanField(default=False)
    is_blocked = models.BooleanField(default=False)
    comment = models.TextField(_('Comment'), blank=True)

    @property
    def amount_check_value(self):
        if self.GREATER_THAN == self.amount_check:
            return 'Greater Than'
        if self.LESS_THAN == self.amount_check:
            return 'Less Than'
        if self.EQUAL == self.amount_check:
            return 'Equal'

    @property
    def intervention_position(self):
        for _position in self.FLOW_POSITIONS:
            if self.flow_position == _position[0]:
                return _position[1]
