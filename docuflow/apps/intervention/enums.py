from enumfields import IntEnum, Enum


class FlowPosition(IntEnum):
    AT_NEXT_STEP = 1
    AT_END = 2
    AT_CURRENT_LEVEL = 3

    class Labels:
        AT_NEXT_STEP = 'At next step'
        AT_END = 'At the end'
        AT_CURRENT_LEVEL = 'Same level'


class Event(Enum):
    SIGNING = 'signing'
    SAVE = 'save'
    PARK_IT = 'park_it'

    class Labels:
        SIGNING = 'Signing'
        SAVE = 'On Save'
        PARK_IT = 'Park It'


class AmountCheck(IntEnum):
    GREATER_THAN = 1
    LESS_THAN = 2
    EQUAL = 3
    NOT_USED = 4

    class Label:
        GREATER_THAN = 'Greater than'
        LESS_THAN = 'Less than'
        EQUAL = 'Equal'
        NOT_USED = 'Not Used'
