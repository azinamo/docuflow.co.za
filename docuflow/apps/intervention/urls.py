from django.urls import path

from . import views

urlpatterns = [
    path('', views.WorkflowRuleView.as_view(), name='intervention_index'),
    path('search/', views.SearchWorkflowRuleView.as_view(), name='search_interventions'),
    path('create/', views.CreateWorkflowRule.as_view(), name='create_intervention'),
    path('edit/<int:pk>/', views.EditWorkflowRule.as_view(), name='edit_intervention'),
    path('delete/<int:pk>/', views.DeleteWorkflowRuleView.as_view(), name='delete_intervention'),
]
